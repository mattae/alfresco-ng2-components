/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../services/authentication.service';
import { BpmProductVersionModel, EcmProductVersionModel } from '../models/product-version.model';
import { DiscoveryApiService } from '../services/discovery-api.service';
import { ObjectDataTableAdapter } from '../datatable/data/object-datatable-adapter';
import { AppConfigService } from '../app-config/app-config.service';
import { Observable } from 'rxjs';
import { ExtensionRef, AppExtensionService } from '@alfresco/adf-extensions';
export declare class AboutComponent implements OnInit {
    private http;
    private appConfig;
    private authService;
    private discovery;
    data: ObjectDataTableAdapter;
    status: ObjectDataTableAdapter;
    license: ObjectDataTableAdapter;
    modules: ObjectDataTableAdapter;
    extensionColumns: string[];
    extensions$: Observable<ExtensionRef[]>;
    /** Commit corresponding to the version of ADF to be used. */
    githubUrlCommitAlpha: string;
    /** Toggles showing/hiding of extensions block. */
    showExtensions: boolean;
    /** Regular expression for filtering dependencies packages. */
    regexp: string;
    ecmHost: string;
    bpmHost: string;
    ecmVersion: EcmProductVersionModel;
    bpmVersion: BpmProductVersionModel;
    constructor(http: HttpClient, appConfig: AppConfigService, authService: AuthenticationService, discovery: DiscoveryApiService, appExtensions: AppExtensionService);
    ngOnInit(): void;
    private gitHubLinkCreation;
}
