/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { EventEmitter, OnInit, OnDestroy } from '@angular/core';
import { CommentModel } from '../models/comment.model';
import { EcmUserService } from '../userinfo/services/ecm-user.service';
import { PeopleProcessService } from '../services/people-process.service';
import { UserPreferencesService } from '../services/user-preferences.service';
export declare class CommentListComponent implements OnInit, OnDestroy {
    peopleProcessService: PeopleProcessService;
    ecmUserService: EcmUserService;
    userPreferenceService: UserPreferencesService;
    /** The comments data used to populate the list. */
    comments: CommentModel[];
    /** Emitted when the user clicks on one of the comment rows. */
    clickRow: EventEmitter<CommentModel>;
    selectedComment: CommentModel;
    currentLocale: any;
    private onDestroy$;
    constructor(peopleProcessService: PeopleProcessService, ecmUserService: EcmUserService, userPreferenceService: UserPreferencesService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    selectComment(comment: CommentModel): void;
    getUserShortName(user: any): string;
    isPictureDefined(user: any): boolean;
    getUserImage(user: any): string;
    private isAContentUsers;
}
