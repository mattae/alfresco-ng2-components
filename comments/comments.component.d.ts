/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommentProcessService } from '../services/comment-process.service';
import { CommentContentService } from '../services/comment-content.service';
import { CommentModel } from '../models/comment.model';
import { EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { Observable } from 'rxjs';
export declare class CommentsComponent implements OnChanges {
    private commentProcessService;
    private commentContentService;
    /** The numeric ID of the task. */
    taskId: string;
    /** The numeric ID of the node. */
    nodeId: string;
    /** Are the comments read only? */
    readOnly: boolean;
    /** Emitted when an error occurs while displaying/adding a comment. */
    error: EventEmitter<any>;
    comments: CommentModel[];
    private commentObserver;
    comment$: Observable<CommentModel>;
    message: string;
    beingAdded: boolean;
    constructor(commentProcessService: CommentProcessService, commentContentService: CommentContentService);
    ngOnChanges(changes: SimpleChanges): void;
    private getComments;
    private resetComments;
    add(): void;
    clear(): void;
    isReadOnly(): boolean;
    isATask(): boolean;
    isANode(): boolean;
    private sanitize;
}
