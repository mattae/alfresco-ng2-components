/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ViewEncapsulation, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../services/authentication.service';
import { DiscoveryApiService } from '../services/discovery-api.service';
import { ObjectDataTableAdapter } from '../datatable/data/object-datatable-adapter';
import { AppConfigService, AppConfigValues } from '../app-config/app-config.service';
import { AppExtensionService } from '@alfresco/adf-extensions';
export class AboutComponent {
    /**
     * @param {?} http
     * @param {?} appConfig
     * @param {?} authService
     * @param {?} discovery
     * @param {?} appExtensions
     */
    constructor(http, appConfig, authService, discovery, appExtensions) {
        this.http = http;
        this.appConfig = appConfig;
        this.authService = authService;
        this.discovery = discovery;
        this.extensionColumns = ['$id', '$name', '$version', '$vendor', '$license', '$runtime', '$description'];
        /**
         * Commit corresponding to the version of ADF to be used.
         */
        this.githubUrlCommitAlpha = 'https://github.com/Alfresco/alfresco-ng2-components/commits/';
        /**
         * Toggles showing/hiding of extensions block.
         */
        this.showExtensions = true;
        /**
         * Regular expression for filtering dependencies packages.
         */
        this.regexp = '^(@alfresco)';
        this.ecmHost = '';
        this.bpmHost = '';
        this.ecmVersion = null;
        this.bpmVersion = null;
        this.extensions$ = appExtensions.references$;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.authService.isEcmLoggedIn()) {
            this.discovery.getEcmProductInfo().subscribe((/**
             * @param {?} ecmVers
             * @return {?}
             */
            (ecmVers) => {
                this.ecmVersion = ecmVers;
                this.modules = new ObjectDataTableAdapter(this.ecmVersion.modules, [
                    { type: 'text', key: 'id', title: 'ABOUT.TABLE_HEADERS.MODULES.ID', sortable: true },
                    { type: 'text', key: 'title', title: 'ABOUT.TABLE_HEADERS.MODULES.TITLE', sortable: true },
                    { type: 'text', key: 'version', title: 'ABOUT.TABLE_HEADERS.MODULES.DESCRIPTION', sortable: true },
                    {
                        type: 'text',
                        key: 'installDate',
                        title: 'ABOUT.TABLE_HEADERS.MODULES.INSTALL_DATE',
                        sortable: true
                    },
                    {
                        type: 'text',
                        key: 'installState',
                        title: 'ABOUT.TABLE_HEADERS.MODULES.INSTALL_STATE',
                        sortable: true
                    },
                    {
                        type: 'text',
                        key: 'versionMin',
                        title: 'ABOUT.TABLE_HEADERS.MODULES.VERSION_MIN',
                        sortable: true
                    },
                    {
                        type: 'text',
                        key: 'versionMax',
                        title: 'ABOUT.TABLE_HEADERS.MODULES.VERSION_MAX',
                        sortable: true
                    }
                ]);
                this.status = new ObjectDataTableAdapter([this.ecmVersion.status], [
                    { type: 'text', key: 'isReadOnly', title: 'ABOUT.TABLE_HEADERS.STATUS.READ_ONLY', sortable: true },
                    {
                        type: 'text',
                        key: 'isAuditEnabled',
                        title: 'ABOUT.TABLE_HEADERS.STATUS.AUDIT_ENABLED',
                        sortable: true
                    },
                    {
                        type: 'text',
                        key: 'isQuickShareEnabled',
                        title: 'ABOUT.TABLE_HEADERS.STATUS.QUICK_SHARE_ENABLED',
                        sortable: true
                    },
                    {
                        type: 'text',
                        key: 'isThumbnailGenerationEnabled',
                        title: 'ABOUT.TABLE_HEADERS.STATUS.THUMBNAIL_ENABLED',
                        sortable: true
                    }
                ]);
                this.license = new ObjectDataTableAdapter([this.ecmVersion.license], [
                    { type: 'text', key: 'issuedAt', title: 'ABOUT.TABLE_HEADERS.LICENSE.ISSUES_AT', sortable: true },
                    { type: 'text', key: 'expiresAt', title: 'ABOUT.TABLE_HEADERS.LICENSE.EXPIRES_AT', sortable: true },
                    {
                        type: 'text',
                        key: 'remainingDays',
                        title: 'ABOUT.TABLE_HEADERS.LICENSE.REMAINING_DAYS',
                        sortable: true
                    },
                    { type: 'text', key: 'holder', title: 'ABOUT.TABLE_HEADERS.LICENSE.HOLDER', sortable: true },
                    { type: 'text', key: 'mode', title: 'ABOUT.TABLE_HEADERS.LICENSE.MODE', sortable: true },
                    {
                        type: 'text',
                        key: 'isClusterEnabled',
                        title: 'ABOUT.TABLE_HEADERS.LICENSE.CLUSTER_ENABLED',
                        sortable: true
                    },
                    {
                        type: 'text',
                        key: 'isCryptodocEnabled',
                        title: 'ABOUT.TABLE_HEADERS.LICENSE.CRYPTODOC_ENABLED',
                        sortable: true
                    }
                ]);
            }));
        }
        if (this.authService.isBpmLoggedIn()) {
            this.discovery.getBpmProductInfo().subscribe((/**
             * @param {?} bpmVers
             * @return {?}
             */
            (bpmVers) => {
                this.bpmVersion = bpmVers;
            }));
        }
        this.http.get('./versions.json?' + new Date()).subscribe((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            /** @type {?} */
            const alfrescoPackages = Object.keys(response.dependencies).filter((/**
             * @param {?} val
             * @return {?}
             */
            (val) => {
                return new RegExp(this.regexp).test(val);
            }));
            /** @type {?} */
            const alfrescoPackagesTableRepresentation = [];
            alfrescoPackages.forEach((/**
             * @param {?} val
             * @return {?}
             */
            (val) => {
                alfrescoPackagesTableRepresentation.push({
                    name: val,
                    version: (response.dependencies[val].version || response.dependencies[val].required.version)
                });
            }));
            this.gitHubLinkCreation(alfrescoPackagesTableRepresentation);
            this.data = new ObjectDataTableAdapter(alfrescoPackagesTableRepresentation, [
                { type: 'text', key: 'name', title: 'Name', sortable: true },
                { type: 'text', key: 'version', title: 'Version', sortable: true }
            ]);
        }));
        this.ecmHost = this.appConfig.get(AppConfigValues.ECMHOST);
        this.bpmHost = this.appConfig.get(AppConfigValues.BPMHOST);
    }
    /**
     * @private
     * @param {?} alfrescoPackagesTableRepresentation
     * @return {?}
     */
    gitHubLinkCreation(alfrescoPackagesTableRepresentation) {
        /** @type {?} */
        const corePackage = alfrescoPackagesTableRepresentation.find((/**
         * @param {?} packageUp
         * @return {?}
         */
        (packageUp) => {
            return packageUp.name === '@alfresco/adf-core';
        }));
        if (corePackage) {
            /** @type {?} */
            const commitIsh = corePackage.version.split('-');
            if (commitIsh.length > 1) {
                this.githubUrlCommitAlpha = this.githubUrlCommitAlpha + commitIsh[1];
            }
            else {
                this.githubUrlCommitAlpha = this.githubUrlCommitAlpha + corePackage.version;
            }
        }
    }
}
AboutComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-about',
                template: "<div class=\"adf-about-container\">\r\n    <div class=\"adf-extension-details-container\" *ngIf=\"showExtensions\">\r\n        <h3>{{ 'ABOUT.TITLE' | translate }}</h3>\r\n        <mat-table [dataSource]=\"extensions$ | async\">\r\n            <!-- $id Column -->\r\n            <ng-container matColumnDef=\"$id\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'ABOUT.TABLE_HEADERS.ID' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.$id}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <!-- $name Column -->\r\n            <ng-container matColumnDef=\"$name\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'ABOUT.TABLE_HEADERS.NAME' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.$name}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <!-- $version Column -->\r\n            <ng-container matColumnDef=\"$version\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'ABOUT.TABLE_HEADERS.VERSION' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.$version}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <!-- $vendor Column -->\r\n            <ng-container matColumnDef=\"$vendor\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'ABOUT.TABLE_HEADERS.VENDOR' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.$vendor}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <!-- $license Column -->\r\n            <ng-container matColumnDef=\"$license\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'ABOUT.TABLE_HEADERS.LICENSE' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.$license}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <!-- $runtime Column -->\r\n            <ng-container matColumnDef=\"$runtime\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'ABOUT.TABLE_HEADERS.RUNTIME' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.$runtime}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <!-- $description Column -->\r\n            <ng-container matColumnDef=\"$description\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'ABOUT.TABLE_HEADERS.DESCRIPTION' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.$description}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <mat-header-row *matHeaderRowDef=\"extensionColumns\"></mat-header-row>\r\n            <mat-row *matRowDef=\"let row; columns: extensionColumns;\"></mat-row>\r\n        </mat-table>\r\n    </div>\r\n    <h3>{{ 'ABOUT.SERVER_SETTINGS.TITLE' | translate }}</h3>\r\n    <mat-list>\r\n        <small>{{ 'ABOUT.SERVER_SETTINGS.DESCRIPTION' | translate }}</small>\r\n        <mat-list-item>\r\n            <h4 matLine> {{ 'ABOUT.SERVER_SETTINGS.PROCESS_SERVICE_HOST' | translate: { value: bpmHost } }}</h4>\r\n        </mat-list-item>\r\n        <mat-divider></mat-divider>\r\n        <mat-list-item>\r\n            <h4 matLine>{{ 'ABOUT.SERVER_SETTINGS.CONTENT_SERVICE_HOST' | translate: { value: ecmHost } }}</h4>\r\n        </mat-list-item>\r\n    </mat-list>\r\n\r\n    <h3>{{ 'ABOUT.VERSIONS.TITLE' | translate }}</h3>\r\n    <div *ngIf=\"bpmVersion\">\r\n        <h3>{{ 'ABOUT.VERSIONS.PROCESS_SERVICE' | translate }}</h3>\r\n        <div> {{ 'ABOUT.VERSIONS.divS.EDITION' | translate }} </div> {{ bpmVersion.edition }}\r\n        <p></p>\r\n        <div> {{ 'ABOUT.VERSIONS.divS.VERSION' | translate }} </div> {{ bpmVersion.majorVersion }}.{{\r\n        bpmVersion.minorVersion }}.{{ bpmVersion.revisionVersion }}\r\n    </div>\r\n    <div *ngIf=\"ecmVersion\">\r\n        <h3>{{ 'ABOUT.VERSIONS.CONTENT_SERVICE' | translate }}</h3>\r\n        <div>{{ 'ABOUT.VERSIONS.divS.EDITION' | translate }}</div> {{ ecmVersion.edition }}\r\n        <p></p>\r\n        <div> {{ 'ABOUT.VERSIONS.divS.VERSION' | translate }} </div> {{ ecmVersion.version.display }}\r\n        <p></p>\r\n        <h4>{{ 'ABOUT.VERSIONS.divS.LICENSE' | translate }}</h4>\r\n        <adf-datatable [data]=\"license\"></adf-datatable>\r\n\r\n        <h4> {{ 'ABOUT.VERSIONS.divS.STATUS' | translate }}</h4>\r\n        <adf-datatable [data]=\"status\"></adf-datatable>\r\n\r\n        <h4>{{ 'ABOUT.VERSIONS.divS.MODULES' | translate }}</h4>\r\n\r\n        <adf-datatable [data]=\"modules\"></adf-datatable>\r\n    </div>\r\n\r\n    <div *ngIf=\"githubUrlCommitAlpha\">\r\n        <h3>{{ 'ABOUT.SOURCE_CODE.TITLE' | translate }}</h3>\r\n        <small>{{ 'ABOUT.SOURCE_CODE.DESCRIPTION' | translate }}</small>\r\n        <div>\r\n            <a [href]=\"githubUrlCommitAlpha\">{{githubUrlCommitAlpha}}</a>\r\n        </div>\r\n    </div>\r\n\r\n    <h3>{{ 'ABOUT.PACKAGES.TITLE' | translate }}</h3>\r\n    <small>{{ 'ABOUT.PACKAGES.DESCRIPTION' | translate }}</small>\r\n    <adf-datatable [data]=\"data\"></adf-datatable>\r\n</div>\r\n",
                encapsulation: ViewEncapsulation.None,
                styles: [".adf-about-container{padding:10px}.adf-table-version{width:60%;border:0;border-spacing:0;text-align:center}"]
            }] }
];
/** @nocollapse */
AboutComponent.ctorParameters = () => [
    { type: HttpClient },
    { type: AppConfigService },
    { type: AuthenticationService },
    { type: DiscoveryApiService },
    { type: AppExtensionService }
];
AboutComponent.propDecorators = {
    githubUrlCommitAlpha: [{ type: Input }],
    showExtensions: [{ type: Input }],
    regexp: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    AboutComponent.prototype.data;
    /** @type {?} */
    AboutComponent.prototype.status;
    /** @type {?} */
    AboutComponent.prototype.license;
    /** @type {?} */
    AboutComponent.prototype.modules;
    /** @type {?} */
    AboutComponent.prototype.extensionColumns;
    /** @type {?} */
    AboutComponent.prototype.extensions$;
    /**
     * Commit corresponding to the version of ADF to be used.
     * @type {?}
     */
    AboutComponent.prototype.githubUrlCommitAlpha;
    /**
     * Toggles showing/hiding of extensions block.
     * @type {?}
     */
    AboutComponent.prototype.showExtensions;
    /**
     * Regular expression for filtering dependencies packages.
     * @type {?}
     */
    AboutComponent.prototype.regexp;
    /** @type {?} */
    AboutComponent.prototype.ecmHost;
    /** @type {?} */
    AboutComponent.prototype.bpmHost;
    /** @type {?} */
    AboutComponent.prototype.ecmVersion;
    /** @type {?} */
    AboutComponent.prototype.bpmVersion;
    /**
     * @type {?}
     * @private
     */
    AboutComponent.prototype.http;
    /**
     * @type {?}
     * @private
     */
    AboutComponent.prototype.appConfig;
    /**
     * @type {?}
     * @private
     */
    AboutComponent.prototype.authService;
    /**
     * @type {?}
     * @private
     */
    AboutComponent.prototype.discovery;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWJvdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiYWJvdXQvYWJvdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQVUsaUJBQWlCLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzVFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNsRCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUUzRSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUN4RSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUNwRixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsZUFBZSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFFckYsT0FBTyxFQUFnQixtQkFBbUIsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBUTdFLE1BQU0sT0FBTyxjQUFjOzs7Ozs7OztJQTBCdkIsWUFBb0IsSUFBZ0IsRUFDaEIsU0FBMkIsRUFDM0IsV0FBa0MsRUFDbEMsU0FBOEIsRUFDdEMsYUFBa0M7UUFKMUIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQUNoQixjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQUMzQixnQkFBVyxHQUFYLFdBQVcsQ0FBdUI7UUFDbEMsY0FBUyxHQUFULFNBQVMsQ0FBcUI7UUF2QmxELHFCQUFnQixHQUFhLENBQUMsS0FBSyxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsY0FBYyxDQUFDLENBQUM7Ozs7UUFLN0cseUJBQW9CLEdBQUcsOERBQThELENBQUM7Ozs7UUFJdEYsbUJBQWMsR0FBRyxJQUFJLENBQUM7Ozs7UUFHYixXQUFNLEdBQUcsY0FBYyxDQUFDO1FBRWpDLFlBQU8sR0FBRyxFQUFFLENBQUM7UUFDYixZQUFPLEdBQUcsRUFBRSxDQUFDO1FBRWIsZUFBVSxHQUEyQixJQUFJLENBQUM7UUFDMUMsZUFBVSxHQUEyQixJQUFJLENBQUM7UUFPdEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxhQUFhLENBQUMsV0FBVyxDQUFDO0lBQ2pELENBQUM7Ozs7SUFFRCxRQUFRO1FBRUosSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxFQUFFO1lBQ2xDLElBQUksQ0FBQyxTQUFTLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxTQUFTOzs7O1lBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFDckQsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUM7Z0JBRTFCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRTtvQkFDL0QsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsS0FBSyxFQUFFLGdDQUFnQyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUU7b0JBQ3BGLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxtQ0FBbUMsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFO29CQUMxRixFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUseUNBQXlDLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRTtvQkFDbEc7d0JBQ0ksSUFBSSxFQUFFLE1BQU07d0JBQ1osR0FBRyxFQUFFLGFBQWE7d0JBQ2xCLEtBQUssRUFBRSwwQ0FBMEM7d0JBQ2pELFFBQVEsRUFBRSxJQUFJO3FCQUNqQjtvQkFDRDt3QkFDSSxJQUFJLEVBQUUsTUFBTTt3QkFDWixHQUFHLEVBQUUsY0FBYzt3QkFDbkIsS0FBSyxFQUFFLDJDQUEyQzt3QkFDbEQsUUFBUSxFQUFFLElBQUk7cUJBQ2pCO29CQUNEO3dCQUNJLElBQUksRUFBRSxNQUFNO3dCQUNaLEdBQUcsRUFBRSxZQUFZO3dCQUNqQixLQUFLLEVBQUUseUNBQXlDO3dCQUNoRCxRQUFRLEVBQUUsSUFBSTtxQkFDakI7b0JBQ0Q7d0JBQ0ksSUFBSSxFQUFFLE1BQU07d0JBQ1osR0FBRyxFQUFFLFlBQVk7d0JBQ2pCLEtBQUssRUFBRSx5Q0FBeUM7d0JBQ2hELFFBQVEsRUFBRSxJQUFJO3FCQUNqQjtpQkFDSixDQUFDLENBQUM7Z0JBRUgsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLHNCQUFzQixDQUFDLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsRUFBRTtvQkFDL0QsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLHNDQUFzQyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUU7b0JBQ2xHO3dCQUNJLElBQUksRUFBRSxNQUFNO3dCQUNaLEdBQUcsRUFBRSxnQkFBZ0I7d0JBQ3JCLEtBQUssRUFBRSwwQ0FBMEM7d0JBQ2pELFFBQVEsRUFBRSxJQUFJO3FCQUNqQjtvQkFDRDt3QkFDSSxJQUFJLEVBQUUsTUFBTTt3QkFDWixHQUFHLEVBQUUscUJBQXFCO3dCQUMxQixLQUFLLEVBQUUsZ0RBQWdEO3dCQUN2RCxRQUFRLEVBQUUsSUFBSTtxQkFDakI7b0JBQ0Q7d0JBQ0ksSUFBSSxFQUFFLE1BQU07d0JBQ1osR0FBRyxFQUFFLDhCQUE4Qjt3QkFDbkMsS0FBSyxFQUFFLDhDQUE4Qzt3QkFDckQsUUFBUSxFQUFFLElBQUk7cUJBQ2pCO2lCQUNKLENBQUMsQ0FBQztnQkFFSCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksc0JBQXNCLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNqRSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsdUNBQXVDLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRTtvQkFDakcsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxXQUFXLEVBQUUsS0FBSyxFQUFFLHdDQUF3QyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUU7b0JBQ25HO3dCQUNJLElBQUksRUFBRSxNQUFNO3dCQUNaLEdBQUcsRUFBRSxlQUFlO3dCQUNwQixLQUFLLEVBQUUsNENBQTRDO3dCQUNuRCxRQUFRLEVBQUUsSUFBSTtxQkFDakI7b0JBQ0QsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLG9DQUFvQyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUU7b0JBQzVGLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxrQ0FBa0MsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFO29CQUN4Rjt3QkFDSSxJQUFJLEVBQUUsTUFBTTt3QkFDWixHQUFHLEVBQUUsa0JBQWtCO3dCQUN2QixLQUFLLEVBQUUsNkNBQTZDO3dCQUNwRCxRQUFRLEVBQUUsSUFBSTtxQkFDakI7b0JBQ0Q7d0JBQ0ksSUFBSSxFQUFFLE1BQU07d0JBQ1osR0FBRyxFQUFFLG9CQUFvQjt3QkFDekIsS0FBSyxFQUFFLCtDQUErQzt3QkFDdEQsUUFBUSxFQUFFLElBQUk7cUJBQ2pCO2lCQUNKLENBQUMsQ0FBQztZQUNQLENBQUMsRUFBQyxDQUFDO1NBQ047UUFFRCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLEVBQUU7WUFDbEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLFNBQVM7Ozs7WUFBQyxDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUNyRCxJQUFJLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQztZQUM5QixDQUFDLEVBQUMsQ0FBQztTQUNOO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLFNBQVM7Ozs7UUFBQyxDQUFDLFFBQWEsRUFBRSxFQUFFOztrQkFFakUsZ0JBQWdCLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTTs7OztZQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQ3ZFLE9BQU8sSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM3QyxDQUFDLEVBQUM7O2tCQUVJLG1DQUFtQyxHQUFHLEVBQUU7WUFDOUMsZ0JBQWdCLENBQUMsT0FBTzs7OztZQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQzdCLG1DQUFtQyxDQUFDLElBQUksQ0FBQztvQkFDckMsSUFBSSxFQUFFLEdBQUc7b0JBQ1QsT0FBTyxFQUFFLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLElBQUksUUFBUSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDO2lCQUMvRixDQUFDLENBQUM7WUFDUCxDQUFDLEVBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO1lBRTdELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxzQkFBc0IsQ0FBQyxtQ0FBbUMsRUFBRTtnQkFDeEUsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFO2dCQUM1RCxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUU7YUFDckUsQ0FBQyxDQUFDO1FBRVAsQ0FBQyxFQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFTLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFTLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN2RSxDQUFDOzs7Ozs7SUFFTyxrQkFBa0IsQ0FBQyxtQ0FBbUM7O2NBQ3BELFdBQVcsR0FBRyxtQ0FBbUMsQ0FBQyxJQUFJOzs7O1FBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBRTtZQUN2RSxPQUFPLFNBQVMsQ0FBQyxJQUFJLEtBQUssb0JBQW9CLENBQUM7UUFDbkQsQ0FBQyxFQUFDO1FBRUYsSUFBSSxXQUFXLEVBQUU7O2tCQUNQLFNBQVMsR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7WUFDaEQsSUFBSSxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDdEIsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDeEU7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLG9CQUFvQixHQUFHLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxXQUFXLENBQUMsT0FBTyxDQUFDO2FBQy9FO1NBQ0o7SUFDTCxDQUFDOzs7WUEzS0osU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxXQUFXO2dCQUNyQiw0L0pBQXFDO2dCQUVyQyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7YUFDeEM7Ozs7WUFkUSxVQUFVO1lBS1YsZ0JBQWdCO1lBSmhCLHFCQUFxQjtZQUVyQixtQkFBbUI7WUFJTCxtQkFBbUI7OzttQ0FrQnJDLEtBQUs7NkJBSUwsS0FBSztxQkFJTCxLQUFLOzs7O0lBaEJOLDhCQUE2Qjs7SUFDN0IsZ0NBQStCOztJQUMvQixpQ0FBZ0M7O0lBQ2hDLGlDQUFnQzs7SUFDaEMsMENBQTZHOztJQUM3RyxxQ0FBd0M7Ozs7O0lBR3hDLDhDQUNzRjs7Ozs7SUFHdEYsd0NBQ3NCOzs7OztJQUd0QixnQ0FBaUM7O0lBRWpDLGlDQUFhOztJQUNiLGlDQUFhOztJQUViLG9DQUEwQzs7SUFDMUMsb0NBQTBDOzs7OztJQUU5Qiw4QkFBd0I7Ozs7O0lBQ3hCLG1DQUFtQzs7Ozs7SUFDbkMscUNBQTBDOzs7OztJQUMxQyxtQ0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdFbmNhcHN1bGF0aW9uLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBBdXRoZW50aWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQnBtUHJvZHVjdFZlcnNpb25Nb2RlbCwgRWNtUHJvZHVjdFZlcnNpb25Nb2RlbCB9IGZyb20gJy4uL21vZGVscy9wcm9kdWN0LXZlcnNpb24ubW9kZWwnO1xyXG5pbXBvcnQgeyBEaXNjb3ZlcnlBcGlTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvZGlzY292ZXJ5LWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JqZWN0RGF0YVRhYmxlQWRhcHRlciB9IGZyb20gJy4uL2RhdGF0YWJsZS9kYXRhL29iamVjdC1kYXRhdGFibGUtYWRhcHRlcic7XHJcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UsIEFwcENvbmZpZ1ZhbHVlcyB9IGZyb20gJy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBFeHRlbnNpb25SZWYsIEFwcEV4dGVuc2lvblNlcnZpY2UgfSBmcm9tICdAYWxmcmVzY28vYWRmLWV4dGVuc2lvbnMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1hYm91dCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vYWJvdXQuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vYWJvdXQuY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIEFib3V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBkYXRhOiBPYmplY3REYXRhVGFibGVBZGFwdGVyO1xyXG4gICAgc3RhdHVzOiBPYmplY3REYXRhVGFibGVBZGFwdGVyO1xyXG4gICAgbGljZW5zZTogT2JqZWN0RGF0YVRhYmxlQWRhcHRlcjtcclxuICAgIG1vZHVsZXM6IE9iamVjdERhdGFUYWJsZUFkYXB0ZXI7XHJcbiAgICBleHRlbnNpb25Db2x1bW5zOiBzdHJpbmdbXSA9IFsnJGlkJywgJyRuYW1lJywgJyR2ZXJzaW9uJywgJyR2ZW5kb3InLCAnJGxpY2Vuc2UnLCAnJHJ1bnRpbWUnLCAnJGRlc2NyaXB0aW9uJ107XHJcbiAgICBleHRlbnNpb25zJDogT2JzZXJ2YWJsZTxFeHRlbnNpb25SZWZbXT47XHJcblxyXG4gICAgLyoqIENvbW1pdCBjb3JyZXNwb25kaW5nIHRvIHRoZSB2ZXJzaW9uIG9mIEFERiB0byBiZSB1c2VkLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGdpdGh1YlVybENvbW1pdEFscGhhID0gJ2h0dHBzOi8vZ2l0aHViLmNvbS9BbGZyZXNjby9hbGZyZXNjby1uZzItY29tcG9uZW50cy9jb21taXRzLyc7XHJcblxyXG4gICAgLyoqIFRvZ2dsZXMgc2hvd2luZy9oaWRpbmcgb2YgZXh0ZW5zaW9ucyBibG9jay4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzaG93RXh0ZW5zaW9ucyA9IHRydWU7XHJcblxyXG4gICAgLyoqIFJlZ3VsYXIgZXhwcmVzc2lvbiBmb3IgZmlsdGVyaW5nIGRlcGVuZGVuY2llcyBwYWNrYWdlcy4gKi9cclxuICAgIEBJbnB1dCgpIHJlZ2V4cCA9ICdeKEBhbGZyZXNjbyknO1xyXG5cclxuICAgIGVjbUhvc3QgPSAnJztcclxuICAgIGJwbUhvc3QgPSAnJztcclxuXHJcbiAgICBlY21WZXJzaW9uOiBFY21Qcm9kdWN0VmVyc2lvbk1vZGVsID0gbnVsbDtcclxuICAgIGJwbVZlcnNpb246IEJwbVByb2R1Y3RWZXJzaW9uTW9kZWwgPSBudWxsO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgYXBwQ29uZmlnOiBBcHBDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBhdXRoU2VydmljZTogQXV0aGVudGljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBkaXNjb3Zlcnk6IERpc2NvdmVyeUFwaVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBhcHBFeHRlbnNpb25zOiBBcHBFeHRlbnNpb25TZXJ2aWNlKSB7XHJcbiAgICAgICAgdGhpcy5leHRlbnNpb25zJCA9IGFwcEV4dGVuc2lvbnMucmVmZXJlbmNlcyQ7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmF1dGhTZXJ2aWNlLmlzRWNtTG9nZ2VkSW4oKSkge1xyXG4gICAgICAgICAgICB0aGlzLmRpc2NvdmVyeS5nZXRFY21Qcm9kdWN0SW5mbygpLnN1YnNjcmliZSgoZWNtVmVycykgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lY21WZXJzaW9uID0gZWNtVmVycztcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLm1vZHVsZXMgPSBuZXcgT2JqZWN0RGF0YVRhYmxlQWRhcHRlcih0aGlzLmVjbVZlcnNpb24ubW9kdWxlcywgW1xyXG4gICAgICAgICAgICAgICAgICAgIHsgdHlwZTogJ3RleHQnLCBrZXk6ICdpZCcsIHRpdGxlOiAnQUJPVVQuVEFCTEVfSEVBREVSUy5NT0RVTEVTLklEJywgc29ydGFibGU6IHRydWUgfSxcclxuICAgICAgICAgICAgICAgICAgICB7IHR5cGU6ICd0ZXh0Jywga2V5OiAndGl0bGUnLCB0aXRsZTogJ0FCT1VULlRBQkxFX0hFQURFUlMuTU9EVUxFUy5USVRMRScsIHNvcnRhYmxlOiB0cnVlIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgeyB0eXBlOiAndGV4dCcsIGtleTogJ3ZlcnNpb24nLCB0aXRsZTogJ0FCT1VULlRBQkxFX0hFQURFUlMuTU9EVUxFUy5ERVNDUklQVElPTicsIHNvcnRhYmxlOiB0cnVlIH0sXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJ2luc3RhbGxEYXRlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6ICdBQk9VVC5UQUJMRV9IRUFERVJTLk1PRFVMRVMuSU5TVEFMTF9EQVRFJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc29ydGFibGU6IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3RleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBrZXk6ICdpbnN0YWxsU3RhdGUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0FCT1VULlRBQkxFX0hFQURFUlMuTU9EVUxFUy5JTlNUQUxMX1NUQVRFJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc29ydGFibGU6IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3RleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBrZXk6ICd2ZXJzaW9uTWluJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6ICdBQk9VVC5UQUJMRV9IRUFERVJTLk1PRFVMRVMuVkVSU0lPTl9NSU4nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzb3J0YWJsZTogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJ3ZlcnNpb25NYXgnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0FCT1VULlRBQkxFX0hFQURFUlMuTU9EVUxFUy5WRVJTSU9OX01BWCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNvcnRhYmxlOiB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgXSk7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5zdGF0dXMgPSBuZXcgT2JqZWN0RGF0YVRhYmxlQWRhcHRlcihbdGhpcy5lY21WZXJzaW9uLnN0YXR1c10sIFtcclxuICAgICAgICAgICAgICAgICAgICB7IHR5cGU6ICd0ZXh0Jywga2V5OiAnaXNSZWFkT25seScsIHRpdGxlOiAnQUJPVVQuVEFCTEVfSEVBREVSUy5TVEFUVVMuUkVBRF9PTkxZJywgc29ydGFibGU6IHRydWUgfSxcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICd0ZXh0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAnaXNBdWRpdEVuYWJsZWQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0FCT1VULlRBQkxFX0hFQURFUlMuU1RBVFVTLkFVRElUX0VOQUJMRUQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzb3J0YWJsZTogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJ2lzUXVpY2tTaGFyZUVuYWJsZWQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0FCT1VULlRBQkxFX0hFQURFUlMuU1RBVFVTLlFVSUNLX1NIQVJFX0VOQUJMRUQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzb3J0YWJsZTogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJ2lzVGh1bWJuYWlsR2VuZXJhdGlvbkVuYWJsZWQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0FCT1VULlRBQkxFX0hFQURFUlMuU1RBVFVTLlRIVU1CTkFJTF9FTkFCTEVEJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc29ydGFibGU6IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBdKTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLmxpY2Vuc2UgPSBuZXcgT2JqZWN0RGF0YVRhYmxlQWRhcHRlcihbdGhpcy5lY21WZXJzaW9uLmxpY2Vuc2VdLCBbXHJcbiAgICAgICAgICAgICAgICAgICAgeyB0eXBlOiAndGV4dCcsIGtleTogJ2lzc3VlZEF0JywgdGl0bGU6ICdBQk9VVC5UQUJMRV9IRUFERVJTLkxJQ0VOU0UuSVNTVUVTX0FUJywgc29ydGFibGU6IHRydWUgfSxcclxuICAgICAgICAgICAgICAgICAgICB7IHR5cGU6ICd0ZXh0Jywga2V5OiAnZXhwaXJlc0F0JywgdGl0bGU6ICdBQk9VVC5UQUJMRV9IRUFERVJTLkxJQ0VOU0UuRVhQSVJFU19BVCcsIHNvcnRhYmxlOiB0cnVlIH0sXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJ3JlbWFpbmluZ0RheXMnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0FCT1VULlRBQkxFX0hFQURFUlMuTElDRU5TRS5SRU1BSU5JTkdfREFZUycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNvcnRhYmxlOiB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICB7IHR5cGU6ICd0ZXh0Jywga2V5OiAnaG9sZGVyJywgdGl0bGU6ICdBQk9VVC5UQUJMRV9IRUFERVJTLkxJQ0VOU0UuSE9MREVSJywgc29ydGFibGU6IHRydWUgfSxcclxuICAgICAgICAgICAgICAgICAgICB7IHR5cGU6ICd0ZXh0Jywga2V5OiAnbW9kZScsIHRpdGxlOiAnQUJPVVQuVEFCTEVfSEVBREVSUy5MSUNFTlNFLk1PREUnLCBzb3J0YWJsZTogdHJ1ZSB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3RleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBrZXk6ICdpc0NsdXN0ZXJFbmFibGVkJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6ICdBQk9VVC5UQUJMRV9IRUFERVJTLkxJQ0VOU0UuQ0xVU1RFUl9FTkFCTEVEJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc29ydGFibGU6IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3RleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBrZXk6ICdpc0NyeXB0b2RvY0VuYWJsZWQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0FCT1VULlRBQkxFX0hFQURFUlMuTElDRU5TRS5DUllQVE9ET0NfRU5BQkxFRCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNvcnRhYmxlOiB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgXSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuYXV0aFNlcnZpY2UuaXNCcG1Mb2dnZWRJbigpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzY292ZXJ5LmdldEJwbVByb2R1Y3RJbmZvKCkuc3Vic2NyaWJlKChicG1WZXJzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmJwbVZlcnNpb24gPSBicG1WZXJzO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuaHR0cC5nZXQoJy4vdmVyc2lvbnMuanNvbj8nICsgbmV3IERhdGUoKSkuc3Vic2NyaWJlKChyZXNwb25zZTogYW55KSA9PiB7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBhbGZyZXNjb1BhY2thZ2VzID0gT2JqZWN0LmtleXMocmVzcG9uc2UuZGVwZW5kZW5jaWVzKS5maWx0ZXIoKHZhbCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBSZWdFeHAodGhpcy5yZWdleHApLnRlc3QodmFsKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBhbGZyZXNjb1BhY2thZ2VzVGFibGVSZXByZXNlbnRhdGlvbiA9IFtdO1xyXG4gICAgICAgICAgICBhbGZyZXNjb1BhY2thZ2VzLmZvckVhY2goKHZhbCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgYWxmcmVzY29QYWNrYWdlc1RhYmxlUmVwcmVzZW50YXRpb24ucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogdmFsLFxyXG4gICAgICAgICAgICAgICAgICAgIHZlcnNpb246IChyZXNwb25zZS5kZXBlbmRlbmNpZXNbdmFsXS52ZXJzaW9uIHx8IHJlc3BvbnNlLmRlcGVuZGVuY2llc1t2YWxdLnJlcXVpcmVkLnZlcnNpb24pXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmdpdEh1YkxpbmtDcmVhdGlvbihhbGZyZXNjb1BhY2thZ2VzVGFibGVSZXByZXNlbnRhdGlvbik7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmRhdGEgPSBuZXcgT2JqZWN0RGF0YVRhYmxlQWRhcHRlcihhbGZyZXNjb1BhY2thZ2VzVGFibGVSZXByZXNlbnRhdGlvbiwgW1xyXG4gICAgICAgICAgICAgICAgeyB0eXBlOiAndGV4dCcsIGtleTogJ25hbWUnLCB0aXRsZTogJ05hbWUnLCBzb3J0YWJsZTogdHJ1ZSB9LFxyXG4gICAgICAgICAgICAgICAgeyB0eXBlOiAndGV4dCcsIGtleTogJ3ZlcnNpb24nLCB0aXRsZTogJ1ZlcnNpb24nLCBzb3J0YWJsZTogdHJ1ZSB9XHJcbiAgICAgICAgICAgIF0pO1xyXG5cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5lY21Ib3N0ID0gdGhpcy5hcHBDb25maWcuZ2V0PHN0cmluZz4oQXBwQ29uZmlnVmFsdWVzLkVDTUhPU1QpO1xyXG4gICAgICAgIHRoaXMuYnBtSG9zdCA9IHRoaXMuYXBwQ29uZmlnLmdldDxzdHJpbmc+KEFwcENvbmZpZ1ZhbHVlcy5CUE1IT1NUKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdpdEh1YkxpbmtDcmVhdGlvbihhbGZyZXNjb1BhY2thZ2VzVGFibGVSZXByZXNlbnRhdGlvbik6IHZvaWQge1xyXG4gICAgICAgIGNvbnN0IGNvcmVQYWNrYWdlID0gYWxmcmVzY29QYWNrYWdlc1RhYmxlUmVwcmVzZW50YXRpb24uZmluZCgocGFja2FnZVVwKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiBwYWNrYWdlVXAubmFtZSA9PT0gJ0BhbGZyZXNjby9hZGYtY29yZSc7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGlmIChjb3JlUGFja2FnZSkge1xyXG4gICAgICAgICAgICBjb25zdCBjb21taXRJc2ggPSBjb3JlUGFja2FnZS52ZXJzaW9uLnNwbGl0KCctJyk7XHJcbiAgICAgICAgICAgIGlmIChjb21taXRJc2gubGVuZ3RoID4gMSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5naXRodWJVcmxDb21taXRBbHBoYSA9IHRoaXMuZ2l0aHViVXJsQ29tbWl0QWxwaGEgKyBjb21taXRJc2hbMV07XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdpdGh1YlVybENvbW1pdEFscGhhID0gdGhpcy5naXRodWJVcmxDb21taXRBbHBoYSArIGNvcmVQYWNrYWdlLnZlcnNpb247XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19