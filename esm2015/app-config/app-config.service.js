/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ObjectUtils } from '../utils/object-utils';
import { Subject } from 'rxjs';
import { map, distinctUntilChanged } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
/** @enum {string} */
const AppConfigValues = {
    APP_CONFIG_LANGUAGES_KEY: 'languages',
    PROVIDERS: 'providers',
    OAUTHCONFIG: 'oauth2',
    ECMHOST: 'ecmHost',
    BASESHAREURL: 'baseShareUrl',
    BPMHOST: 'bpmHost',
    IDENTITY_HOST: 'identityHost',
    AUTHTYPE: 'authType',
    CONTEXTROOTECM: 'contextRootEcm',
    CONTEXTROOTBPM: 'contextRootBpm',
    ALFRESCO_REPOSITORY_NAME: 'alfrescoRepositoryName',
    LOG_LEVEL: 'logLevel',
    LOGIN_ROUTE: 'loginRoute',
    DISABLECSRF: 'disableCSRF',
    AUTH_WITH_CREDENTIALS: 'auth.withCredentials',
    APPLICATION: 'application',
    STORAGE_PREFIX: 'application.storagePrefix',
    NOTIFY_DURATION: 'notificationDefaultDuration',
};
export { AppConfigValues };
/** @enum {string} */
const Status = {
    INIT: 'init',
    LOADING: 'loading',
    LOADED: 'loaded',
};
export { Status };
/* spellchecker: enable */
export class AppConfigService {
    /**
     * @param {?} http
     */
    constructor(http) {
        this.http = http;
        this.config = {
            application: {
                name: 'Alfresco ADF Application'
            },
            ecmHost: 'http://{hostname}{:port}/ecm',
            bpmHost: 'http://{hostname}{:port}/bpm',
            logLevel: 'silent',
            alfrescoRepositoryName: 'alfresco-1'
        };
        this.status = Status.INIT;
        this.onLoadSubject = new Subject();
        this.onLoad = this.onLoadSubject.asObservable();
    }
    /**
     * Requests notification of a property value when it is loaded.
     * @param {?} property The desired property value
     * @return {?} Property value, when loaded
     */
    select(property) {
        return this.onLoadSubject
            .pipe(map((/**
         * @param {?} config
         * @return {?}
         */
        (config) => config[property])), distinctUntilChanged());
    }
    /**
     * Gets the value of a named property.
     * @template T
     * @param {?} key Name of the property
     * @param {?=} defaultValue Value to return if the key is not found
     * @return {?} Value of the property
     */
    get(key, defaultValue) {
        /** @type {?} */
        let result = ObjectUtils.getValue(this.config, key);
        if (typeof result === 'string') {
            /** @type {?} */
            const keywords = new Map();
            keywords.set('hostname', this.getLocationHostname());
            keywords.set(':port', this.getLocationPort(':'));
            keywords.set('port', this.getLocationPort());
            keywords.set('protocol', this.getLocationProtocol());
            result = this.formatString(result, keywords);
        }
        if (result === undefined) {
            return defaultValue;
        }
        return (/** @type {?} */ (result));
    }
    /**
     * Gets the location.protocol value.
     * @return {?} The location.protocol string
     */
    getLocationProtocol() {
        return location.protocol;
    }
    /**
     * Gets the location.hostname property.
     * @return {?} Value of the property
     */
    getLocationHostname() {
        return location.hostname;
    }
    /**
     * Gets the location.port property.
     * @param {?=} prefix Text added before port value
     * @return {?} Port with prefix
     */
    getLocationPort(prefix = '') {
        return location.port ? prefix + location.port : '';
    }
    /**
     * Loads the config file.
     * @return {?} Notification when loading is complete
     */
    load() {
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        (resolve) => tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const configUrl = `app.config.json?v=${Date.now()}`;
            if (this.status === Status.INIT) {
                this.status = Status.LOADING;
                yield this.http.get(configUrl).subscribe((/**
                 * @param {?} data
                 * @return {?}
                 */
                (data) => {
                    this.status = Status.LOADED;
                    this.config = Object.assign({}, this.config, data || {});
                    this.onLoadSubject.next(this.config);
                    resolve(this.config);
                }), (/**
                 * @return {?}
                 */
                () => {
                    resolve(this.config);
                }));
            }
            else if (this.status === Status.LOADED) {
                resolve(this.config);
            }
            else if (this.status === Status.LOADING) {
                this.onLoad.subscribe((/**
                 * @return {?}
                 */
                () => {
                    resolve(this.config);
                }));
            }
        })));
    }
    /**
     * @private
     * @param {?} str
     * @param {?} keywords
     * @return {?}
     */
    formatString(str, keywords) {
        /** @type {?} */
        let result = str;
        keywords.forEach((/**
         * @param {?} value
         * @param {?} key
         * @return {?}
         */
        (value, key) => {
            /** @type {?} */
            const expr = new RegExp('{' + key + '}', 'gm');
            result = result.replace(expr, value);
        }));
        return result;
    }
}
AppConfigService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
AppConfigService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ AppConfigService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AppConfigService_Factory() { return new AppConfigService(i0.ɵɵinject(i1.HttpClient)); }, token: AppConfigService, providedIn: "root" });
if (false) {
    /** @type {?} */
    AppConfigService.prototype.config;
    /** @type {?} */
    AppConfigService.prototype.status;
    /**
     * @type {?}
     * @protected
     */
    AppConfigService.prototype.onLoadSubject;
    /** @type {?} */
    AppConfigService.prototype.onLoad;
    /**
     * @type {?}
     * @private
     */
    AppConfigService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLWNvbmZpZy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiYXBwLWNvbmZpZy9hcHAtY29uZmlnLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNsRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUNwRCxPQUFPLEVBQWMsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxHQUFHLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7Ozs7SUFJdkQsMEJBQTJCLFdBQVc7SUFDdEMsV0FBWSxXQUFXO0lBQ3ZCLGFBQWMsUUFBUTtJQUN0QixTQUFVLFNBQVM7SUFDbkIsY0FBZSxjQUFjO0lBQzdCLFNBQVUsU0FBUztJQUNuQixlQUFnQixjQUFjO0lBQzlCLFVBQVcsVUFBVTtJQUNyQixnQkFBaUIsZ0JBQWdCO0lBQ2pDLGdCQUFpQixnQkFBZ0I7SUFDakMsMEJBQTJCLHdCQUF3QjtJQUNuRCxXQUFZLFVBQVU7SUFDdEIsYUFBYyxZQUFZO0lBQzFCLGFBQWMsYUFBYTtJQUMzQix1QkFBd0Isc0JBQXNCO0lBQzlDLGFBQWMsYUFBYTtJQUMzQixnQkFBaUIsMkJBQTJCO0lBQzVDLGlCQUFrQiw2QkFBNkI7Ozs7O0lBSS9DLE1BQU8sTUFBTTtJQUNiLFNBQVUsU0FBUztJQUNuQixRQUFTLFFBQVE7Ozs7QUFRckIsTUFBTSxPQUFPLGdCQUFnQjs7OztJQWdCekIsWUFBb0IsSUFBZ0I7UUFBaEIsU0FBSSxHQUFKLElBQUksQ0FBWTtRQWRwQyxXQUFNLEdBQVE7WUFDVixXQUFXLEVBQUU7Z0JBQ1QsSUFBSSxFQUFFLDBCQUEwQjthQUNuQztZQUNELE9BQU8sRUFBRSw4QkFBOEI7WUFDdkMsT0FBTyxFQUFFLDhCQUE4QjtZQUN2QyxRQUFRLEVBQUUsUUFBUTtZQUNsQixzQkFBc0IsRUFBRSxZQUFZO1NBQ3ZDLENBQUM7UUFFRixXQUFNLEdBQVcsTUFBTSxDQUFDLElBQUksQ0FBQztRQUt6QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7UUFDbkMsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3BELENBQUM7Ozs7OztJQU9ELE1BQU0sQ0FBQyxRQUFnQjtRQUNuQixPQUFPLElBQUksQ0FBQyxhQUFhO2FBQ3BCLElBQUksQ0FDRCxHQUFHOzs7O1FBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsRUFBQyxFQUNqQyxvQkFBb0IsRUFBRSxDQUN6QixDQUFDO0lBQ1YsQ0FBQzs7Ozs7Ozs7SUFRRCxHQUFHLENBQUksR0FBVyxFQUFFLFlBQWdCOztZQUM1QixNQUFNLEdBQVEsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQztRQUN4RCxJQUFJLE9BQU8sTUFBTSxLQUFLLFFBQVEsRUFBRTs7a0JBQ3RCLFFBQVEsR0FBRyxJQUFJLEdBQUcsRUFBa0I7WUFDMUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUMsQ0FBQztZQUNyRCxRQUFRLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDakQsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7WUFDN0MsUUFBUSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUMsQ0FBQztZQUNyRCxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUM7U0FDaEQ7UUFDRCxJQUFJLE1BQU0sS0FBSyxTQUFTLEVBQUU7WUFDdEIsT0FBTyxZQUFZLENBQUM7U0FDdkI7UUFDRCxPQUFPLG1CQUFJLE1BQU0sRUFBQSxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBTUQsbUJBQW1CO1FBQ2YsT0FBTyxRQUFRLENBQUMsUUFBUSxDQUFDO0lBQzdCLENBQUM7Ozs7O0lBTUQsbUJBQW1CO1FBQ2YsT0FBTyxRQUFRLENBQUMsUUFBUSxDQUFDO0lBQzdCLENBQUM7Ozs7OztJQU9ELGVBQWUsQ0FBQyxTQUFpQixFQUFFO1FBQy9CLE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUN2RCxDQUFDOzs7OztJQU1ELElBQUk7UUFDQSxPQUFPLElBQUksT0FBTzs7OztRQUFDLENBQU8sT0FBTyxFQUFFLEVBQUU7O2tCQUMzQixTQUFTLEdBQUcscUJBQXFCLElBQUksQ0FBQyxHQUFHLEVBQUUsRUFBRTtZQUVuRCxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssTUFBTSxDQUFDLElBQUksRUFBRTtnQkFDN0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDO2dCQUM3QixNQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLFNBQVM7Ozs7Z0JBQ3BDLENBQUMsSUFBUyxFQUFFLEVBQUU7b0JBQ1YsSUFBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDO29CQUM1QixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDO29CQUN6RCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7b0JBQ3JDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7Z0JBQ3pCLENBQUM7OztnQkFDRCxHQUFHLEVBQUU7b0JBQ0QsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztnQkFDekIsQ0FBQyxFQUNKLENBQUM7YUFDTDtpQkFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssTUFBTSxDQUFDLE1BQU0sRUFBRTtnQkFDdEMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUN4QjtpQkFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssTUFBTSxDQUFDLE9BQU8sRUFBRTtnQkFDdkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTOzs7Z0JBQUMsR0FBRyxFQUFFO29CQUN2QixPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN6QixDQUFDLEVBQUMsQ0FBQzthQUNOO1FBQ0wsQ0FBQyxDQUFBLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7SUFFTyxZQUFZLENBQUMsR0FBVyxFQUFFLFFBQTZCOztZQUN2RCxNQUFNLEdBQUcsR0FBRztRQUVoQixRQUFRLENBQUMsT0FBTzs7Ozs7UUFBQyxDQUFDLEtBQUssRUFBRSxHQUFHLEVBQUUsRUFBRTs7a0JBQ3RCLElBQUksR0FBRyxJQUFJLE1BQU0sQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsRUFBRSxJQUFJLENBQUM7WUFDOUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3pDLENBQUMsRUFBQyxDQUFDO1FBRUgsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQzs7O1lBNUhKLFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQjs7OztZQXRDUSxVQUFVOzs7OztJQXlDZixrQ0FRRTs7SUFFRixrQ0FBNkI7Ozs7O0lBQzdCLHlDQUFzQzs7SUFDdEMsa0NBQXdCOzs7OztJQUVaLGdDQUF3QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9iamVjdFV0aWxzIH0gZnJvbSAnLi4vdXRpbHMvb2JqZWN0LXV0aWxzJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBtYXAsIGRpc3RpbmN0VW50aWxDaGFuZ2VkIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuLyogc3BlbGxjaGVja2VyOiBkaXNhYmxlICovXHJcbmV4cG9ydCBlbnVtIEFwcENvbmZpZ1ZhbHVlcyB7XHJcbiAgICBBUFBfQ09ORklHX0xBTkdVQUdFU19LRVkgPSAnbGFuZ3VhZ2VzJyxcclxuICAgIFBST1ZJREVSUyA9ICdwcm92aWRlcnMnLFxyXG4gICAgT0FVVEhDT05GSUcgPSAnb2F1dGgyJyxcclxuICAgIEVDTUhPU1QgPSAnZWNtSG9zdCcsXHJcbiAgICBCQVNFU0hBUkVVUkwgPSAnYmFzZVNoYXJlVXJsJyxcclxuICAgIEJQTUhPU1QgPSAnYnBtSG9zdCcsXHJcbiAgICBJREVOVElUWV9IT1NUID0gJ2lkZW50aXR5SG9zdCcsXHJcbiAgICBBVVRIVFlQRSA9ICdhdXRoVHlwZScsXHJcbiAgICBDT05URVhUUk9PVEVDTSA9ICdjb250ZXh0Um9vdEVjbScsXHJcbiAgICBDT05URVhUUk9PVEJQTSA9ICdjb250ZXh0Um9vdEJwbScsXHJcbiAgICBBTEZSRVNDT19SRVBPU0lUT1JZX05BTUUgPSAnYWxmcmVzY29SZXBvc2l0b3J5TmFtZScsXHJcbiAgICBMT0dfTEVWRUwgPSAnbG9nTGV2ZWwnLFxyXG4gICAgTE9HSU5fUk9VVEUgPSAnbG9naW5Sb3V0ZScsXHJcbiAgICBESVNBQkxFQ1NSRiA9ICdkaXNhYmxlQ1NSRicsXHJcbiAgICBBVVRIX1dJVEhfQ1JFREVOVElBTFMgPSAnYXV0aC53aXRoQ3JlZGVudGlhbHMnLFxyXG4gICAgQVBQTElDQVRJT04gPSAnYXBwbGljYXRpb24nLFxyXG4gICAgU1RPUkFHRV9QUkVGSVggPSAnYXBwbGljYXRpb24uc3RvcmFnZVByZWZpeCcsXHJcbiAgICBOT1RJRllfRFVSQVRJT04gPSAnbm90aWZpY2F0aW9uRGVmYXVsdER1cmF0aW9uJ1xyXG59XHJcblxyXG5leHBvcnQgZW51bSBTdGF0dXMge1xyXG4gICAgSU5JVCA9ICdpbml0JyxcclxuICAgIExPQURJTkcgPSAnbG9hZGluZycsXHJcbiAgICBMT0FERUQgPSAnbG9hZGVkJ1xyXG59XHJcblxyXG4vKiBzcGVsbGNoZWNrZXI6IGVuYWJsZSAqL1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBcHBDb25maWdTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25maWc6IGFueSA9IHtcclxuICAgICAgICBhcHBsaWNhdGlvbjoge1xyXG4gICAgICAgICAgICBuYW1lOiAnQWxmcmVzY28gQURGIEFwcGxpY2F0aW9uJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZWNtSG9zdDogJ2h0dHA6Ly97aG9zdG5hbWV9ezpwb3J0fS9lY20nLFxyXG4gICAgICAgIGJwbUhvc3Q6ICdodHRwOi8ve2hvc3RuYW1lfXs6cG9ydH0vYnBtJyxcclxuICAgICAgICBsb2dMZXZlbDogJ3NpbGVudCcsXHJcbiAgICAgICAgYWxmcmVzY29SZXBvc2l0b3J5TmFtZTogJ2FsZnJlc2NvLTEnXHJcbiAgICB9O1xyXG5cclxuICAgIHN0YXR1czogU3RhdHVzID0gU3RhdHVzLklOSVQ7XHJcbiAgICBwcm90ZWN0ZWQgb25Mb2FkU3ViamVjdDogU3ViamVjdDxhbnk+O1xyXG4gICAgb25Mb2FkOiBPYnNlcnZhYmxlPGFueT47XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50KSB7XHJcbiAgICAgICAgdGhpcy5vbkxvYWRTdWJqZWN0ID0gbmV3IFN1YmplY3QoKTtcclxuICAgICAgICB0aGlzLm9uTG9hZCA9IHRoaXMub25Mb2FkU3ViamVjdC5hc09ic2VydmFibGUoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlcXVlc3RzIG5vdGlmaWNhdGlvbiBvZiBhIHByb3BlcnR5IHZhbHVlIHdoZW4gaXQgaXMgbG9hZGVkLlxyXG4gICAgICogQHBhcmFtIHByb3BlcnR5IFRoZSBkZXNpcmVkIHByb3BlcnR5IHZhbHVlXHJcbiAgICAgKiBAcmV0dXJucyBQcm9wZXJ0eSB2YWx1ZSwgd2hlbiBsb2FkZWRcclxuICAgICAqL1xyXG4gICAgc2VsZWN0KHByb3BlcnR5OiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm9uTG9hZFN1YmplY3RcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAoKGNvbmZpZykgPT4gY29uZmlnW3Byb3BlcnR5XSksXHJcbiAgICAgICAgICAgICAgICBkaXN0aW5jdFVudGlsQ2hhbmdlZCgpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSB2YWx1ZSBvZiBhIG5hbWVkIHByb3BlcnR5LlxyXG4gICAgICogQHBhcmFtIGtleSBOYW1lIG9mIHRoZSBwcm9wZXJ0eVxyXG4gICAgICogQHBhcmFtIGRlZmF1bHRWYWx1ZSBWYWx1ZSB0byByZXR1cm4gaWYgdGhlIGtleSBpcyBub3QgZm91bmRcclxuICAgICAqIEByZXR1cm5zIFZhbHVlIG9mIHRoZSBwcm9wZXJ0eVxyXG4gICAgICovXHJcbiAgICBnZXQ8VD4oa2V5OiBzdHJpbmcsIGRlZmF1bHRWYWx1ZT86IFQpOiBUIHtcclxuICAgICAgICBsZXQgcmVzdWx0OiBhbnkgPSBPYmplY3RVdGlscy5nZXRWYWx1ZSh0aGlzLmNvbmZpZywga2V5KTtcclxuICAgICAgICBpZiAodHlwZW9mIHJlc3VsdCA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgY29uc3Qga2V5d29yZHMgPSBuZXcgTWFwPHN0cmluZywgc3RyaW5nPigpO1xyXG4gICAgICAgICAgICBrZXl3b3Jkcy5zZXQoJ2hvc3RuYW1lJywgdGhpcy5nZXRMb2NhdGlvbkhvc3RuYW1lKCkpO1xyXG4gICAgICAgICAgICBrZXl3b3Jkcy5zZXQoJzpwb3J0JywgdGhpcy5nZXRMb2NhdGlvblBvcnQoJzonKSk7XHJcbiAgICAgICAgICAgIGtleXdvcmRzLnNldCgncG9ydCcsIHRoaXMuZ2V0TG9jYXRpb25Qb3J0KCkpO1xyXG4gICAgICAgICAgICBrZXl3b3Jkcy5zZXQoJ3Byb3RvY29sJywgdGhpcy5nZXRMb2NhdGlvblByb3RvY29sKCkpO1xyXG4gICAgICAgICAgICByZXN1bHQgPSB0aGlzLmZvcm1hdFN0cmluZyhyZXN1bHQsIGtleXdvcmRzKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHJlc3VsdCA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBkZWZhdWx0VmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiA8VD4gcmVzdWx0O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgbG9jYXRpb24ucHJvdG9jb2wgdmFsdWUuXHJcbiAgICAgKiBAcmV0dXJucyBUaGUgbG9jYXRpb24ucHJvdG9jb2wgc3RyaW5nXHJcbiAgICAgKi9cclxuICAgIGdldExvY2F0aW9uUHJvdG9jb2woKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gbG9jYXRpb24ucHJvdG9jb2w7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBsb2NhdGlvbi5ob3N0bmFtZSBwcm9wZXJ0eS5cclxuICAgICAqIEByZXR1cm5zIFZhbHVlIG9mIHRoZSBwcm9wZXJ0eVxyXG4gICAgICovXHJcbiAgICBnZXRMb2NhdGlvbkhvc3RuYW1lKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIGxvY2F0aW9uLmhvc3RuYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgbG9jYXRpb24ucG9ydCBwcm9wZXJ0eS5cclxuICAgICAqIEBwYXJhbSBwcmVmaXggVGV4dCBhZGRlZCBiZWZvcmUgcG9ydCB2YWx1ZVxyXG4gICAgICogQHJldHVybnMgUG9ydCB3aXRoIHByZWZpeFxyXG4gICAgICovXHJcbiAgICBnZXRMb2NhdGlvblBvcnQocHJlZml4OiBzdHJpbmcgPSAnJyk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIGxvY2F0aW9uLnBvcnQgPyBwcmVmaXggKyBsb2NhdGlvbi5wb3J0IDogJyc7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBMb2FkcyB0aGUgY29uZmlnIGZpbGUuXHJcbiAgICAgKiBAcmV0dXJucyBOb3RpZmljYXRpb24gd2hlbiBsb2FkaW5nIGlzIGNvbXBsZXRlXHJcbiAgICAgKi9cclxuICAgIGxvYWQoKTogUHJvbWlzZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoYXN5bmMgKHJlc29sdmUpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgY29uZmlnVXJsID0gYGFwcC5jb25maWcuanNvbj92PSR7RGF0ZS5ub3coKX1gO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuc3RhdHVzID09PSBTdGF0dXMuSU5JVCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zdGF0dXMgPSBTdGF0dXMuTE9BRElORztcclxuICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuaHR0cC5nZXQoY29uZmlnVXJsKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgKGRhdGE6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnN0YXR1cyA9IFN0YXR1cy5MT0FERUQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29uZmlnID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5jb25maWcsIGRhdGEgfHwge30pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9uTG9hZFN1YmplY3QubmV4dCh0aGlzLmNvbmZpZyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUodGhpcy5jb25maWcpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHRoaXMuY29uZmlnKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdHVzID09PSBTdGF0dXMuTE9BREVEKSB7XHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKHRoaXMuY29uZmlnKTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnN0YXR1cyA9PT0gU3RhdHVzLkxPQURJTkcpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMub25Mb2FkLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh0aGlzLmNvbmZpZyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZm9ybWF0U3RyaW5nKHN0cjogc3RyaW5nLCBrZXl3b3JkczogTWFwPHN0cmluZywgc3RyaW5nPik6IHN0cmluZyB7XHJcbiAgICAgICAgbGV0IHJlc3VsdCA9IHN0cjtcclxuXHJcbiAgICAgICAga2V5d29yZHMuZm9yRWFjaCgodmFsdWUsIGtleSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBleHByID0gbmV3IFJlZ0V4cCgneycgKyBrZXkgKyAnfScsICdnbScpO1xyXG4gICAgICAgICAgICByZXN1bHQgPSByZXN1bHQucmVwbGFjZShleHByLCB2YWx1ZSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcbn1cclxuIl19