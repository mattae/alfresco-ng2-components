/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../services/storage.service';
import { AppConfigService, AppConfigValues } from '../app-config/app-config.service';
export class DebugAppConfigService extends AppConfigService {
    /**
     * @param {?} storage
     * @param {?} http
     */
    constructor(storage, http) {
        super(http);
        this.storage = storage;
    }
    /**
     * @override
     * @template T
     * @param {?} key
     * @param {?=} defaultValue
     * @return {?}
     */
    get(key, defaultValue) {
        if (key === AppConfigValues.OAUTHCONFIG) {
            return (/** @type {?} */ ((JSON.parse(this.storage.getItem(key)) || super.get(key, defaultValue))));
        }
        else if (key === AppConfigValues.APPLICATION) {
            return undefined;
        }
        else {
            return (/** @type {?} */ (((/** @type {?} */ (this.storage.getItem(key))) || super.get(key, defaultValue))));
        }
    }
}
DebugAppConfigService.decorators = [
    { type: Injectable }
];
/** @nocollapse */
DebugAppConfigService.ctorParameters = () => [
    { type: StorageService },
    { type: HttpClient }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    DebugAppConfigService.prototype.storage;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVidWctYXBwLWNvbmZpZy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiYXBwLWNvbmZpZy9kZWJ1Zy1hcHAtY29uZmlnLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzdELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxlQUFlLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUdyRixNQUFNLE9BQU8scUJBQXNCLFNBQVEsZ0JBQWdCOzs7OztJQUN2RCxZQUFvQixPQUF1QixFQUFFLElBQWdCO1FBQ3pELEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQURJLFlBQU8sR0FBUCxPQUFPLENBQWdCO0lBRTNDLENBQUM7Ozs7Ozs7O0lBR0QsR0FBRyxDQUFJLEdBQVcsRUFBRSxZQUFnQjtRQUNoQyxJQUFJLEdBQUcsS0FBSyxlQUFlLENBQUMsV0FBVyxFQUFFO1lBQ3JDLE9BQU8sbUJBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksS0FBSyxDQUFDLEdBQUcsQ0FBSSxHQUFHLEVBQUUsWUFBWSxDQUFDLENBQUMsRUFBQSxDQUFDO1NBQ3pGO2FBQU0sSUFBSSxHQUFHLEtBQUssZUFBZSxDQUFDLFdBQVcsRUFBRTtZQUM1QyxPQUFPLFNBQVMsQ0FBQztTQUNwQjthQUFNO1lBQ0gsT0FBTyxtQkFBSSxDQUFDLG1CQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFBLElBQUksS0FBSyxDQUFDLEdBQUcsQ0FBSSxHQUFHLEVBQUUsWUFBWSxDQUFDLENBQUMsRUFBQSxDQUFDO1NBQ25GO0lBQ0wsQ0FBQzs7O1lBZkosVUFBVTs7OztZQUhGLGNBQWM7WUFEZCxVQUFVOzs7Ozs7O0lBTUgsd0NBQStCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgU3RvcmFnZVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9zdG9yYWdlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBDb25maWdTZXJ2aWNlLCBBcHBDb25maWdWYWx1ZXMgfSBmcm9tICcuLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBEZWJ1Z0FwcENvbmZpZ1NlcnZpY2UgZXh0ZW5kcyBBcHBDb25maWdTZXJ2aWNlIHtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgc3RvcmFnZTogU3RvcmFnZVNlcnZpY2UsIGh0dHA6IEh0dHBDbGllbnQpIHtcclxuICAgICAgICBzdXBlcihodHRwKTtcclxuICAgIH1cclxuXHJcbiAgICAvKiogQG92ZXJyaWRlICovXHJcbiAgICBnZXQ8VD4oa2V5OiBzdHJpbmcsIGRlZmF1bHRWYWx1ZT86IFQpOiBUIHtcclxuICAgICAgICBpZiAoa2V5ID09PSBBcHBDb25maWdWYWx1ZXMuT0FVVEhDT05GSUcpIHtcclxuICAgICAgICAgICAgcmV0dXJuIDxUPiAoSlNPTi5wYXJzZSh0aGlzLnN0b3JhZ2UuZ2V0SXRlbShrZXkpKSB8fCBzdXBlci5nZXQ8VD4oa2V5LCBkZWZhdWx0VmFsdWUpKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGtleSA9PT0gQXBwQ29uZmlnVmFsdWVzLkFQUExJQ0FUSU9OKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB1bmRlZmluZWQ7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIDxUPiAoPGFueT4gdGhpcy5zdG9yYWdlLmdldEl0ZW0oa2V5KSB8fCBzdXBlci5nZXQ8VD4oa2V5LCBkZWZhdWx0VmFsdWUpKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19