/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ContentChildren, QueryList } from '@angular/core';
import { MatMenuItem } from '@angular/material';
export class ButtonsMenuComponent {
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        if (this.buttons.length > 0) {
            this.isMenuEmpty = false;
        }
        else {
            this.isMenuEmpty = true;
        }
    }
    /**
     * @return {?}
     */
    isMobile() {
        return !!/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
    }
}
ButtonsMenuComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-buttons-action-menu',
                template: "<div id=\"adf-buttons-menu\" class=\"adf-buttons-menu\" *ngIf=\"!isMenuEmpty\">\r\n    <div *ngIf=\"isMobile()\">\r\n        <button mat-icon-button [matMenuTriggerFor]=\"editReportMenu\">\r\n            <mat-icon>more_vert</mat-icon>\r\n        </button>\r\n        <mat-menu #editReportMenu=\"matMenu\" class=\"adf-buttons-menu-mobile\">\r\n            <ng-content *ngTemplateOutlet=\"desktop\">\r\n            </ng-content>\r\n        </mat-menu>\r\n    </div>\r\n\r\n    <div *ngIf=\"!isMobile()\" class=\"adf-buttons-menu-desktop\">\r\n        <ng-content *ngTemplateOutlet=\"desktop\">\r\n        </ng-content>\r\n    </div>\r\n</div>\r\n\r\n<ng-template #desktop>\r\n    <ng-content></ng-content>\r\n</ng-template>",
                styles: [""]
            }] }
];
ButtonsMenuComponent.propDecorators = {
    buttons: [{ type: ContentChildren, args: [MatMenuItem,] }]
};
if (false) {
    /** @type {?} */
    ButtonsMenuComponent.prototype.buttons;
    /** @type {?} */
    ButtonsMenuComponent.prototype.isMenuEmpty;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnV0dG9ucy1tZW51LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImJ1dHRvbnMtbWVudS9idXR0b25zLW1lbnUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsZUFBZSxFQUFFLFNBQVMsRUFBb0IsTUFBTSxlQUFlLENBQUM7QUFDeEYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBUWhELE1BQU0sT0FBTyxvQkFBb0I7Ozs7SUFNN0Isa0JBQWtCO1FBQ2QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDekIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7U0FDNUI7YUFBTTtZQUNILElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1NBQzNCO0lBQ0wsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixPQUFPLENBQUMsQ0FBQyxnRUFBZ0UsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3hHLENBQUM7OztZQXRCSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLHlCQUF5QjtnQkFDbkMsNnRCQUE0Qzs7YUFFL0M7OztzQkFJSSxlQUFlLFNBQUMsV0FBVzs7OztJQUE1Qix1Q0FBOEQ7O0lBRTlELDJDQUFxQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIENvbnRlbnRDaGlsZHJlbiwgUXVlcnlMaXN0LCBBZnRlckNvbnRlbnRJbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdE1lbnVJdGVtIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1idXR0b25zLWFjdGlvbi1tZW51JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9idXR0b25zLW1lbnUuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vYnV0dG9ucy1tZW51LmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBCdXR0b25zTWVudUNvbXBvbmVudCBpbXBsZW1lbnRzICBBZnRlckNvbnRlbnRJbml0IHtcclxuXHJcbiAgICBAQ29udGVudENoaWxkcmVuKE1hdE1lbnVJdGVtKSBidXR0b25zOiBRdWVyeUxpc3Q8TWF0TWVudUl0ZW0+O1xyXG5cclxuICAgIGlzTWVudUVtcHR5OiBib29sZWFuO1xyXG5cclxuICAgIG5nQWZ0ZXJDb250ZW50SW5pdCgpIHtcclxuICAgICAgICBpZiAodGhpcy5idXR0b25zLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgdGhpcy5pc01lbnVFbXB0eSA9IGZhbHNlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNNZW51RW1wdHkgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpc01vYmlsZSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gISEvQW5kcm9pZHx3ZWJPU3xpUGhvbmV8aVBhZHxpUG9kfEJsYWNrQmVycnl8SUVNb2JpbGV8T3BlcmEgTWluaS9pLnRlc3QobmF2aWdhdG9yLnVzZXJBZ2VudCk7XHJcbiAgICB9XHJcbn1cclxuIl19