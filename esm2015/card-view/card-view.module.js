/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MatButtonModule, MatDatepickerModule, MatTableModule, MatIconModule, MatInputModule, MatCheckboxModule, MatNativeDateModule, MatSelectModule } from '@angular/material';
import { MatDatetimepickerModule, MatNativeDatetimeModule } from '@mat-datetimepicker/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { TranslateModule } from '@ngx-translate/core';
import { CardViewContentProxyDirective } from './directives/card-view-content-proxy.directive';
import { CardViewComponent } from './components/card-view/card-view.component';
import { CardViewBoolItemComponent } from './components/card-view-boolitem/card-view-boolitem.component';
import { CardViewDateItemComponent } from './components/card-view-dateitem/card-view-dateitem.component';
import { CardViewItemDispatcherComponent } from './components/card-view-item-dispatcher/card-view-item-dispatcher.component';
import { CardViewMapItemComponent } from './components/card-view-mapitem/card-view-mapitem.component';
import { CardViewTextItemComponent } from './components/card-view-textitem/card-view-textitem.component';
import { CardViewKeyValuePairsItemComponent } from './components/card-view-keyvaluepairsitem/card-view-keyvaluepairsitem.component';
import { CardViewSelectItemComponent } from './components/card-view-selectitem/card-view-selectitem.component';
export class CardViewModule {
}
CardViewModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    FormsModule,
                    FlexLayoutModule,
                    TranslateModule.forChild(),
                    MatDatepickerModule,
                    MatNativeDateModule,
                    MatCheckboxModule,
                    MatInputModule,
                    MatTableModule,
                    MatIconModule,
                    MatSelectModule,
                    MatButtonModule,
                    MatDatetimepickerModule,
                    MatNativeDatetimeModule
                ],
                declarations: [
                    CardViewComponent,
                    CardViewBoolItemComponent,
                    CardViewDateItemComponent,
                    CardViewMapItemComponent,
                    CardViewTextItemComponent,
                    CardViewKeyValuePairsItemComponent,
                    CardViewSelectItemComponent,
                    CardViewItemDispatcherComponent,
                    CardViewContentProxyDirective
                ],
                entryComponents: [
                    CardViewBoolItemComponent,
                    CardViewDateItemComponent,
                    CardViewMapItemComponent,
                    CardViewTextItemComponent,
                    CardViewSelectItemComponent,
                    CardViewKeyValuePairsItemComponent
                ],
                exports: [
                    CardViewComponent,
                    CardViewBoolItemComponent,
                    CardViewDateItemComponent,
                    CardViewMapItemComponent,
                    CardViewTextItemComponent,
                    CardViewSelectItemComponent,
                    CardViewKeyValuePairsItemComponent
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNhcmQtdmlldy9jYXJkLXZpZXcubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQ0gsZUFBZSxFQUNmLG1CQUFtQixFQUNuQixjQUFjLEVBQ2QsYUFBYSxFQUNiLGNBQWMsRUFDZCxpQkFBaUIsRUFDakIsbUJBQW1CLEVBQ25CLGVBQWUsRUFDbEIsTUFBTSxtQkFBbUIsQ0FBQztBQUMzQixPQUFPLEVBQUUsdUJBQXVCLEVBQUUsdUJBQXVCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUM1RixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFdEQsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDL0YsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDL0UsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sOERBQThELENBQUM7QUFDekcsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sOERBQThELENBQUM7QUFDekcsT0FBTyxFQUFFLCtCQUErQixFQUFFLE1BQU0sNEVBQTRFLENBQUM7QUFDN0gsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sNERBQTRELENBQUM7QUFDdEcsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sOERBQThELENBQUM7QUFDekcsT0FBTyxFQUFFLGtDQUFrQyxFQUFFLE1BQU0sZ0ZBQWdGLENBQUM7QUFDcEksT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sa0VBQWtFLENBQUM7QUFnRC9HLE1BQU0sT0FBTyxjQUFjOzs7WUE5QzFCLFFBQVEsU0FBQztnQkFDTixPQUFPLEVBQUU7b0JBQ0wsWUFBWTtvQkFDWixXQUFXO29CQUNYLGdCQUFnQjtvQkFDaEIsZUFBZSxDQUFDLFFBQVEsRUFBRTtvQkFDMUIsbUJBQW1CO29CQUNuQixtQkFBbUI7b0JBQ25CLGlCQUFpQjtvQkFDakIsY0FBYztvQkFDZCxjQUFjO29CQUNkLGFBQWE7b0JBQ2IsZUFBZTtvQkFDZixlQUFlO29CQUNmLHVCQUF1QjtvQkFDdkIsdUJBQXVCO2lCQUMxQjtnQkFDRCxZQUFZLEVBQUU7b0JBQ1YsaUJBQWlCO29CQUNqQix5QkFBeUI7b0JBQ3pCLHlCQUF5QjtvQkFDekIsd0JBQXdCO29CQUN4Qix5QkFBeUI7b0JBQ3pCLGtDQUFrQztvQkFDbEMsMkJBQTJCO29CQUMzQiwrQkFBK0I7b0JBQy9CLDZCQUE2QjtpQkFDaEM7Z0JBQ0QsZUFBZSxFQUFFO29CQUNiLHlCQUF5QjtvQkFDekIseUJBQXlCO29CQUN6Qix3QkFBd0I7b0JBQ3hCLHlCQUF5QjtvQkFDekIsMkJBQTJCO29CQUMzQixrQ0FBa0M7aUJBQ3JDO2dCQUNELE9BQU8sRUFBRTtvQkFDTCxpQkFBaUI7b0JBQ2pCLHlCQUF5QjtvQkFDekIseUJBQXlCO29CQUN6Qix3QkFBd0I7b0JBQ3hCLHlCQUF5QjtvQkFDekIsMkJBQTJCO29CQUMzQixrQ0FBa0M7aUJBQ3JDO2FBQ0oiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7XHJcbiAgICBNYXRCdXR0b25Nb2R1bGUsXHJcbiAgICBNYXREYXRlcGlja2VyTW9kdWxlLFxyXG4gICAgTWF0VGFibGVNb2R1bGUsXHJcbiAgICBNYXRJY29uTW9kdWxlLFxyXG4gICAgTWF0SW5wdXRNb2R1bGUsXHJcbiAgICBNYXRDaGVja2JveE1vZHVsZSxcclxuICAgIE1hdE5hdGl2ZURhdGVNb2R1bGUsXHJcbiAgICBNYXRTZWxlY3RNb2R1bGVcclxufSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IE1hdERhdGV0aW1lcGlja2VyTW9kdWxlLCBNYXROYXRpdmVEYXRldGltZU1vZHVsZSB9IGZyb20gJ0BtYXQtZGF0ZXRpbWVwaWNrZXIvY29yZSc7XHJcbmltcG9ydCB7IEZsZXhMYXlvdXRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mbGV4LWxheW91dCc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZU1vZHVsZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgQ2FyZFZpZXdDb250ZW50UHJveHlEaXJlY3RpdmUgfSBmcm9tICcuL2RpcmVjdGl2ZXMvY2FyZC12aWV3LWNvbnRlbnQtcHJveHkuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY2FyZC12aWV3L2NhcmQtdmlldy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDYXJkVmlld0Jvb2xJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NhcmQtdmlldy1ib29saXRlbS9jYXJkLXZpZXctYm9vbGl0ZW0uY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ2FyZFZpZXdEYXRlSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jYXJkLXZpZXctZGF0ZWl0ZW0vY2FyZC12aWV3LWRhdGVpdGVtLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENhcmRWaWV3SXRlbURpc3BhdGNoZXJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvY2FyZC12aWV3LWl0ZW0tZGlzcGF0Y2hlci9jYXJkLXZpZXctaXRlbS1kaXNwYXRjaGVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENhcmRWaWV3TWFwSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jYXJkLXZpZXctbWFwaXRlbS9jYXJkLXZpZXctbWFwaXRlbS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDYXJkVmlld1RleHRJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2NhcmQtdmlldy10ZXh0aXRlbS9jYXJkLXZpZXctdGV4dGl0ZW0uY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ2FyZFZpZXdLZXlWYWx1ZVBhaXJzSXRlbUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jYXJkLXZpZXcta2V5dmFsdWVwYWlyc2l0ZW0vY2FyZC12aWV3LWtleXZhbHVlcGFpcnNpdGVtLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENhcmRWaWV3U2VsZWN0SXRlbUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9jYXJkLXZpZXctc2VsZWN0aXRlbS9jYXJkLXZpZXctc2VsZWN0aXRlbS5jb21wb25lbnQnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgRm9ybXNNb2R1bGUsXHJcbiAgICAgICAgRmxleExheW91dE1vZHVsZSxcclxuICAgICAgICBUcmFuc2xhdGVNb2R1bGUuZm9yQ2hpbGQoKSxcclxuICAgICAgICBNYXREYXRlcGlja2VyTW9kdWxlLFxyXG4gICAgICAgIE1hdE5hdGl2ZURhdGVNb2R1bGUsXHJcbiAgICAgICAgTWF0Q2hlY2tib3hNb2R1bGUsXHJcbiAgICAgICAgTWF0SW5wdXRNb2R1bGUsXHJcbiAgICAgICAgTWF0VGFibGVNb2R1bGUsXHJcbiAgICAgICAgTWF0SWNvbk1vZHVsZSxcclxuICAgICAgICBNYXRTZWxlY3RNb2R1bGUsXHJcbiAgICAgICAgTWF0QnV0dG9uTW9kdWxlLFxyXG4gICAgICAgIE1hdERhdGV0aW1lcGlja2VyTW9kdWxlLFxyXG4gICAgICAgIE1hdE5hdGl2ZURhdGV0aW1lTW9kdWxlXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgQ2FyZFZpZXdDb21wb25lbnQsXHJcbiAgICAgICAgQ2FyZFZpZXdCb29sSXRlbUNvbXBvbmVudCxcclxuICAgICAgICBDYXJkVmlld0RhdGVJdGVtQ29tcG9uZW50LFxyXG4gICAgICAgIENhcmRWaWV3TWFwSXRlbUNvbXBvbmVudCxcclxuICAgICAgICBDYXJkVmlld1RleHRJdGVtQ29tcG9uZW50LFxyXG4gICAgICAgIENhcmRWaWV3S2V5VmFsdWVQYWlyc0l0ZW1Db21wb25lbnQsXHJcbiAgICAgICAgQ2FyZFZpZXdTZWxlY3RJdGVtQ29tcG9uZW50LFxyXG4gICAgICAgIENhcmRWaWV3SXRlbURpc3BhdGNoZXJDb21wb25lbnQsXHJcbiAgICAgICAgQ2FyZFZpZXdDb250ZW50UHJveHlEaXJlY3RpdmVcclxuICAgIF0sXHJcbiAgICBlbnRyeUNvbXBvbmVudHM6IFtcclxuICAgICAgICBDYXJkVmlld0Jvb2xJdGVtQ29tcG9uZW50LFxyXG4gICAgICAgIENhcmRWaWV3RGF0ZUl0ZW1Db21wb25lbnQsXHJcbiAgICAgICAgQ2FyZFZpZXdNYXBJdGVtQ29tcG9uZW50LFxyXG4gICAgICAgIENhcmRWaWV3VGV4dEl0ZW1Db21wb25lbnQsXHJcbiAgICAgICAgQ2FyZFZpZXdTZWxlY3RJdGVtQ29tcG9uZW50LFxyXG4gICAgICAgIENhcmRWaWV3S2V5VmFsdWVQYWlyc0l0ZW1Db21wb25lbnRcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgQ2FyZFZpZXdDb21wb25lbnQsXHJcbiAgICAgICAgQ2FyZFZpZXdCb29sSXRlbUNvbXBvbmVudCxcclxuICAgICAgICBDYXJkVmlld0RhdGVJdGVtQ29tcG9uZW50LFxyXG4gICAgICAgIENhcmRWaWV3TWFwSXRlbUNvbXBvbmVudCxcclxuICAgICAgICBDYXJkVmlld1RleHRJdGVtQ29tcG9uZW50LFxyXG4gICAgICAgIENhcmRWaWV3U2VsZWN0SXRlbUNvbXBvbmVudCxcclxuICAgICAgICBDYXJkVmlld0tleVZhbHVlUGFpcnNJdGVtQ29tcG9uZW50XHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDYXJkVmlld01vZHVsZSB7fVxyXG4iXX0=