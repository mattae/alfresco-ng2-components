/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input } from '@angular/core';
import { CardViewBoolItemModel } from '../../models/card-view-boolitem.model';
import { CardViewUpdateService } from '../../services/card-view-update.service';
export class CardViewBoolItemComponent {
    /**
     * @param {?} cardViewUpdateService
     */
    constructor(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
    }
    /**
     * @return {?}
     */
    isEditable() {
        return this.editable && this.property.editable;
    }
    /**
     * @param {?} change
     * @return {?}
     */
    changed(change) {
        this.cardViewUpdateService.update(this.property, change.checked);
        this.property.value = change.checked;
    }
}
CardViewBoolItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-card-view-boolitem',
                template: "<ng-container *ngIf=\"!property.isEmpty() || isEditable()\">\r\n    <div [attr.data-automation-id]=\"'card-boolean-label-' + property.key\" class=\"adf-property-label\">{{ property.label | translate }}</div>\r\n    <div class=\"adf-property-value\">\r\n        <mat-checkbox\r\n            [attr.data-automation-id]=\"'card-boolean-' + property.key\"\r\n            [attr.title]=\"'CORE.METADATA.ACTIONS.TOGGLE' | translate\"\r\n            [checked]=\"property.displayValue\"\r\n            [disabled]=\"!isEditable()\"\r\n            (change)=\"changed($event)\">\r\n        </mat-checkbox>\r\n    </div>\r\n</ng-container>\r\n",
                styles: [""]
            }] }
];
/** @nocollapse */
CardViewBoolItemComponent.ctorParameters = () => [
    { type: CardViewUpdateService }
];
CardViewBoolItemComponent.propDecorators = {
    property: [{ type: Input }],
    editable: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    CardViewBoolItemComponent.prototype.property;
    /** @type {?} */
    CardViewBoolItemComponent.prototype.editable;
    /**
     * @type {?}
     * @private
     */
    CardViewBoolItemComponent.prototype.cardViewUpdateService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWJvb2xpdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNhcmQtdmlldy9jb21wb25lbnRzL2NhcmQtdmlldy1ib29saXRlbS9jYXJkLXZpZXctYm9vbGl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRWpELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBUWhGLE1BQU0sT0FBTyx5QkFBeUI7Ozs7SUFRbEMsWUFBb0IscUJBQTRDO1FBQTVDLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBdUI7SUFBRyxDQUFDOzs7O0lBRXBFLFVBQVU7UUFDTixPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7SUFDbkQsQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsTUFBeUI7UUFDN0IsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUUsQ0FBQztRQUNsRSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQ3pDLENBQUM7OztZQXZCSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLHdCQUF3QjtnQkFDbEMsaW9CQUFrRDs7YUFFckQ7Ozs7WUFOUSxxQkFBcUI7Ozt1QkFVekIsS0FBSzt1QkFHTCxLQUFLOzs7O0lBSE4sNkNBQ2dDOztJQUVoQyw2Q0FDa0I7Ozs7O0lBRU4sMERBQW9EIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0Q2hlY2tib3hDaGFuZ2UgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IENhcmRWaWV3Qm9vbEl0ZW1Nb2RlbCB9IGZyb20gJy4uLy4uL21vZGVscy9jYXJkLXZpZXctYm9vbGl0ZW0ubW9kZWwnO1xyXG5pbXBvcnQgeyBDYXJkVmlld1VwZGF0ZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9jYXJkLXZpZXctdXBkYXRlLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1jYXJkLXZpZXctYm9vbGl0ZW0nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2NhcmQtdmlldy1ib29saXRlbS5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9jYXJkLXZpZXctYm9vbGl0ZW0uY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3Qm9vbEl0ZW1Db21wb25lbnQge1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBwcm9wZXJ0eTogQ2FyZFZpZXdCb29sSXRlbU1vZGVsO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBlZGl0YWJsZTogYm9vbGVhbjtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNhcmRWaWV3VXBkYXRlU2VydmljZTogQ2FyZFZpZXdVcGRhdGVTZXJ2aWNlKSB7fVxyXG5cclxuICAgIGlzRWRpdGFibGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZWRpdGFibGUgJiYgdGhpcy5wcm9wZXJ0eS5lZGl0YWJsZTtcclxuICAgIH1cclxuXHJcbiAgICBjaGFuZ2VkKGNoYW5nZTogTWF0Q2hlY2tib3hDaGFuZ2UpIHtcclxuICAgICAgICB0aGlzLmNhcmRWaWV3VXBkYXRlU2VydmljZS51cGRhdGUodGhpcy5wcm9wZXJ0eSwgY2hhbmdlLmNoZWNrZWQgKTtcclxuICAgICAgICB0aGlzLnByb3BlcnR5LnZhbHVlID0gY2hhbmdlLmNoZWNrZWQ7XHJcbiAgICB9XHJcbn1cclxuIl19