/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewChild } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { DatetimeAdapter, MAT_DATETIME_FORMATS, MatDatetimepicker } from '@mat-datetimepicker/core';
import { MAT_MOMENT_DATETIME_FORMATS, MomentDatetimeAdapter } from '@mat-datetimepicker/moment';
import moment from 'moment-es6';
import { CardViewDateItemModel } from '../../models/card-view-dateitem.model';
import { CardViewUpdateService } from '../../services/card-view-update.service';
import { UserPreferencesService, UserPreferenceValues } from '../../../services/user-preferences.service';
import { MomentDateAdapter } from '../../../utils/momentDateAdapter';
import { MOMENT_DATE_FORMATS } from '../../../utils/moment-date-formats.model';
import { AppConfigService } from '../../../app-config/app-config.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
const ɵ0 = MOMENT_DATE_FORMATS, ɵ1 = MAT_MOMENT_DATETIME_FORMATS;
export class CardViewDateItemComponent {
    /**
     * @param {?} cardViewUpdateService
     * @param {?} dateAdapter
     * @param {?} userPreferencesService
     * @param {?} appConfig
     */
    constructor(cardViewUpdateService, dateAdapter, userPreferencesService, appConfig) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.dateAdapter = dateAdapter;
        this.userPreferencesService = userPreferencesService;
        this.appConfig = appConfig;
        this.editable = false;
        this.displayEmpty = true;
        this.onDestroy$ = new Subject();
        this.dateFormat = this.appConfig.get('dateValues.defaultDateFormat');
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.userPreferencesService
            .select(UserPreferenceValues.Locale)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} locale
         * @return {?}
         */
        locale => this.dateAdapter.setLocale(locale)));
        ((/** @type {?} */ (this.dateAdapter))).overrideDisplayFormat = 'MMM DD';
        if (this.property.value) {
            this.valueDate = moment(this.property.value, this.dateFormat);
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    }
    /**
     * @return {?}
     */
    showProperty() {
        return this.displayEmpty || !this.property.isEmpty();
    }
    /**
     * @return {?}
     */
    isEditable() {
        return this.editable && this.property.editable;
    }
    /**
     * @return {?}
     */
    showDatePicker() {
        this.datepicker.open();
    }
    /**
     * @param {?} newDateValue
     * @return {?}
     */
    onDateChanged(newDateValue) {
        if (newDateValue) {
            /** @type {?} */
            const momentDate = moment(newDateValue.value, this.dateFormat, true);
            if (momentDate.isValid()) {
                this.valueDate = momentDate;
                this.cardViewUpdateService.update(this.property, momentDate.toDate());
                this.property.value = momentDate.toDate();
            }
        }
    }
}
CardViewDateItemComponent.decorators = [
    { type: Component, args: [{
                providers: [
                    { provide: DateAdapter, useClass: MomentDateAdapter },
                    { provide: MAT_DATE_FORMATS, useValue: ɵ0 },
                    { provide: DatetimeAdapter, useClass: MomentDatetimeAdapter },
                    { provide: MAT_DATETIME_FORMATS, useValue: ɵ1 }
                ],
                selector: 'adf-card-view-dateitem',
                template: "<div [attr.data-automation-id]=\"'card-dateitem-label-' + property.key\" class=\"adf-property-label\" *ngIf=\"showProperty() || isEditable()\">{{ property.label | translate }}</div>\r\n<div class=\"adf-property-value\">\r\n    <span *ngIf=\"!isEditable()\" [attr.data-automation-id]=\"'card-' + property.type + '-value-' + property.key\">\r\n        <span [attr.data-automation-id]=\"'card-dateitem-' + property.key\">\r\n            <span *ngIf=\"showProperty()\">{{ property.displayValue }}</span>\r\n        </span>\r\n    </span>\r\n    <div *ngIf=\"isEditable()\" class=\"adf-dateitem-editable\">\r\n        <div class=\"adf-dateitem-editable-controls\">\r\n            <span\r\n                class=\"adf-datepicker-toggle\"\r\n                [attr.data-automation-id]=\"'datepicker-label-toggle-' + property.key\"\r\n                (click)=\"showDatePicker()\">\r\n                <span [attr.data-automation-id]=\"'card-' + property.type + '-value-' + property.key\" *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ property.displayValue }}</span>\r\n            </span>\r\n            <mat-datetimepicker-toggle\r\n                [attr.title]=\"'CORE.METADATA.ACTIONS.EDIT' | translate\"\r\n                [attr.data-automation-id]=\"'datepickertoggle-' + property.key\"\r\n                [for]=\"datetimePicker\">\r\n            </mat-datetimepicker-toggle>\r\n        </div>\r\n\r\n        <input class=\"adf-invisible-date-input\"\r\n            [matDatetimepicker]=\"datetimePicker\"\r\n            [value]=\"valueDate\"\r\n            (dateChange)=\"onDateChanged($event)\">\r\n\r\n        <mat-datetimepicker #datetimePicker\r\n            [type]=\"property.type\"\r\n            timeInterval=\"5\"\r\n            [attr.data-automation-id]=\"'datepicker-' + property.key\"\r\n            [startAt]=\"valueDate\">\r\n        </mat-datetimepicker>\r\n    </div>\r\n    <ng-template #elseEmptyValueBlock>\r\n        {{ property.default | translate }}\r\n    </ng-template>\r\n</div>\r\n",
                styles: [""]
            }] }
];
/** @nocollapse */
CardViewDateItemComponent.ctorParameters = () => [
    { type: CardViewUpdateService },
    { type: DateAdapter },
    { type: UserPreferencesService },
    { type: AppConfigService }
];
CardViewDateItemComponent.propDecorators = {
    property: [{ type: Input }],
    editable: [{ type: Input }],
    displayEmpty: [{ type: Input }],
    datepicker: [{ type: ViewChild, args: ['datetimePicker', { static: true },] }]
};
if (false) {
    /** @type {?} */
    CardViewDateItemComponent.prototype.property;
    /** @type {?} */
    CardViewDateItemComponent.prototype.editable;
    /** @type {?} */
    CardViewDateItemComponent.prototype.displayEmpty;
    /** @type {?} */
    CardViewDateItemComponent.prototype.datepicker;
    /** @type {?} */
    CardViewDateItemComponent.prototype.valueDate;
    /** @type {?} */
    CardViewDateItemComponent.prototype.dateFormat;
    /**
     * @type {?}
     * @private
     */
    CardViewDateItemComponent.prototype.onDestroy$;
    /**
     * @type {?}
     * @private
     */
    CardViewDateItemComponent.prototype.cardViewUpdateService;
    /**
     * @type {?}
     * @private
     */
    CardViewDateItemComponent.prototype.dateAdapter;
    /**
     * @type {?}
     * @private
     */
    CardViewDateItemComponent.prototype.userPreferencesService;
    /**
     * @type {?}
     * @private
     */
    CardViewDateItemComponent.prototype.appConfig;
}
export { ɵ0, ɵ1 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWRhdGVpdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNhcmQtdmlldy9jb21wb25lbnRzL2NhcmQtdmlldy1kYXRlaXRlbS9jYXJkLXZpZXctZGF0ZWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBcUIsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxXQUFXLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRSxPQUFPLEVBQUUsZUFBZSxFQUFFLG9CQUFvQixFQUFFLGlCQUFpQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDcEcsT0FBTyxFQUFFLDJCQUEyQixFQUFFLHFCQUFxQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDaEcsT0FBTyxNQUFNLE1BQU0sWUFBWSxDQUFDO0FBRWhDLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQzFHLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO1dBS0ksbUJBQW1CLE9BRWYsMkJBQTJCO0FBTTlFLE1BQU0sT0FBTyx5QkFBeUI7Ozs7Ozs7SUFtQmxDLFlBQW9CLHFCQUE0QyxFQUM1QyxXQUFnQyxFQUNoQyxzQkFBOEMsRUFDOUMsU0FBMkI7UUFIM0IsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtRQUM1QyxnQkFBVyxHQUFYLFdBQVcsQ0FBcUI7UUFDaEMsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF3QjtRQUM5QyxjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQWhCL0MsYUFBUSxHQUFZLEtBQUssQ0FBQztRQUcxQixpQkFBWSxHQUFZLElBQUksQ0FBQztRQVFyQixlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztRQU14QyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLDhCQUE4QixDQUFDLENBQUM7SUFDekUsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsc0JBQXNCO2FBQ3RCLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUM7YUFDbkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDaEMsU0FBUzs7OztRQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUMsQ0FBQztRQUU3RCxDQUFDLG1CQUFvQixJQUFJLENBQUMsV0FBVyxFQUFBLENBQUMsQ0FBQyxxQkFBcUIsR0FBRyxRQUFRLENBQUM7UUFFeEUsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRTtZQUNyQixJQUFJLENBQUMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDakU7SUFDTCxDQUFDOzs7O0lBRUQsV0FBVztRQUNQLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDL0IsQ0FBQzs7OztJQUVELFlBQVk7UUFDUixPQUFPLElBQUksQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3pELENBQUM7Ozs7SUFFRCxVQUFVO1FBQ04sT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO0lBQ25ELENBQUM7Ozs7SUFFRCxjQUFjO1FBQ1YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMzQixDQUFDOzs7OztJQUVELGFBQWEsQ0FBQyxZQUFZO1FBQ3RCLElBQUksWUFBWSxFQUFFOztrQkFDUixVQUFVLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUM7WUFDcEUsSUFBSSxVQUFVLENBQUMsT0FBTyxFQUFFLEVBQUU7Z0JBQ3RCLElBQUksQ0FBQyxTQUFTLEdBQUcsVUFBVSxDQUFDO2dCQUM1QixJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLE1BQU0sRUFBRSxDQUFDLENBQUM7Z0JBQ3RFLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQzthQUM3QztTQUNKO0lBQ0wsQ0FBQzs7O1lBNUVKLFNBQVMsU0FBQztnQkFDUCxTQUFTLEVBQUU7b0JBQ1AsRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFBRSxpQkFBaUIsRUFBRTtvQkFDckQsRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsUUFBUSxJQUFxQixFQUFFO29CQUM1RCxFQUFFLE9BQU8sRUFBRSxlQUFlLEVBQUUsUUFBUSxFQUFFLHFCQUFxQixFQUFFO29CQUM3RCxFQUFFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxRQUFRLElBQTZCLEVBQUU7aUJBQzNFO2dCQUNELFFBQVEsRUFBRSx3QkFBd0I7Z0JBQ2xDLHErREFBa0Q7O2FBRXJEOzs7O1lBbEJRLHFCQUFxQjtZQU5yQixXQUFXO1lBT1gsc0JBQXNCO1lBR3RCLGdCQUFnQjs7O3VCQWlCcEIsS0FBSzt1QkFHTCxLQUFLOzJCQUdMLEtBQUs7eUJBR0wsU0FBUyxTQUFDLGdCQUFnQixFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQzs7OztJQVQzQyw2Q0FDZ0M7O0lBRWhDLDZDQUMwQjs7SUFFMUIsaURBQzZCOztJQUU3QiwrQ0FDMEM7O0lBRTFDLDhDQUFrQjs7SUFDbEIsK0NBQW1COzs7OztJQUVuQiwrQ0FBNEM7Ozs7O0lBRWhDLDBEQUFvRDs7Ozs7SUFDcEQsZ0RBQXdDOzs7OztJQUN4QywyREFBc0Q7Ozs7O0lBQ3RELDhDQUFtQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxuXG4vKiFcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxuICpcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cbiAqL1xuXG5pbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkRlc3Ryb3ksIE9uSW5pdCwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBEYXRlQWRhcHRlciwgTUFUX0RBVEVfRk9STUFUUyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7IERhdGV0aW1lQWRhcHRlciwgTUFUX0RBVEVUSU1FX0ZPUk1BVFMsIE1hdERhdGV0aW1lcGlja2VyIH0gZnJvbSAnQG1hdC1kYXRldGltZXBpY2tlci9jb3JlJztcbmltcG9ydCB7IE1BVF9NT01FTlRfREFURVRJTUVfRk9STUFUUywgTW9tZW50RGF0ZXRpbWVBZGFwdGVyIH0gZnJvbSAnQG1hdC1kYXRldGltZXBpY2tlci9tb21lbnQnO1xuaW1wb3J0IG1vbWVudCBmcm9tICdtb21lbnQtZXM2JztcbmltcG9ydCB7IE1vbWVudCB9IGZyb20gJ21vbWVudCc7XG5pbXBvcnQgeyBDYXJkVmlld0RhdGVJdGVtTW9kZWwgfSBmcm9tICcuLi8uLi9tb2RlbHMvY2FyZC12aWV3LWRhdGVpdGVtLm1vZGVsJztcbmltcG9ydCB7IENhcmRWaWV3VXBkYXRlU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2NhcmQtdmlldy11cGRhdGUuc2VydmljZSc7XG5pbXBvcnQgeyBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlLCBVc2VyUHJlZmVyZW5jZVZhbHVlcyB9IGZyb20gJy4uLy4uLy4uL3NlcnZpY2VzL3VzZXItcHJlZmVyZW5jZXMuc2VydmljZSc7XG5pbXBvcnQgeyBNb21lbnREYXRlQWRhcHRlciB9IGZyb20gJy4uLy4uLy4uL3V0aWxzL21vbWVudERhdGVBZGFwdGVyJztcbmltcG9ydCB7IE1PTUVOVF9EQVRFX0ZPUk1BVFMgfSBmcm9tICcuLi8uLi8uLi91dGlscy9tb21lbnQtZGF0ZS1mb3JtYXRzLm1vZGVsJztcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbkBDb21wb25lbnQoe1xuICAgIHByb3ZpZGVyczogW1xuICAgICAgICB7IHByb3ZpZGU6IERhdGVBZGFwdGVyLCB1c2VDbGFzczogTW9tZW50RGF0ZUFkYXB0ZXIgfSxcbiAgICAgICAgeyBwcm92aWRlOiBNQVRfREFURV9GT1JNQVRTLCB1c2VWYWx1ZTogTU9NRU5UX0RBVEVfRk9STUFUUyB9LFxuICAgICAgICB7IHByb3ZpZGU6IERhdGV0aW1lQWRhcHRlciwgdXNlQ2xhc3M6IE1vbWVudERhdGV0aW1lQWRhcHRlciB9LFxuICAgICAgICB7IHByb3ZpZGU6IE1BVF9EQVRFVElNRV9GT1JNQVRTLCB1c2VWYWx1ZTogTUFUX01PTUVOVF9EQVRFVElNRV9GT1JNQVRTIH1cbiAgICBdLFxuICAgIHNlbGVjdG9yOiAnYWRmLWNhcmQtdmlldy1kYXRlaXRlbScsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2NhcmQtdmlldy1kYXRlaXRlbS5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vY2FyZC12aWV3LWRhdGVpdGVtLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgQ2FyZFZpZXdEYXRlSXRlbUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcblxuICAgIEBJbnB1dCgpXG4gICAgcHJvcGVydHk6IENhcmRWaWV3RGF0ZUl0ZW1Nb2RlbDtcblxuICAgIEBJbnB1dCgpXG4gICAgZWRpdGFibGU6IGJvb2xlYW4gPSBmYWxzZTtcblxuICAgIEBJbnB1dCgpXG4gICAgZGlzcGxheUVtcHR5OiBib29sZWFuID0gdHJ1ZTtcblxuICAgIEBWaWV3Q2hpbGQoJ2RhdGV0aW1lUGlja2VyJywge3N0YXRpYzogdHJ1ZX0pXG4gICAgcHVibGljIGRhdGVwaWNrZXI6IE1hdERhdGV0aW1lcGlja2VyPGFueT47XG5cbiAgICB2YWx1ZURhdGU6IE1vbWVudDtcbiAgICBkYXRlRm9ybWF0OiBzdHJpbmc7XG5cbiAgICBwcml2YXRlIG9uRGVzdHJveSQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBjYXJkVmlld1VwZGF0ZVNlcnZpY2U6IENhcmRWaWV3VXBkYXRlU2VydmljZSxcbiAgICAgICAgICAgICAgICBwcml2YXRlIGRhdGVBZGFwdGVyOiBEYXRlQWRhcHRlcjxNb21lbnQ+LFxuICAgICAgICAgICAgICAgIHByaXZhdGUgdXNlclByZWZlcmVuY2VzU2VydmljZTogVXNlclByZWZlcmVuY2VzU2VydmljZSxcbiAgICAgICAgICAgICAgICBwcml2YXRlIGFwcENvbmZpZzogQXBwQ29uZmlnU2VydmljZSkge1xuICAgICAgICB0aGlzLmRhdGVGb3JtYXQgPSB0aGlzLmFwcENvbmZpZy5nZXQoJ2RhdGVWYWx1ZXMuZGVmYXVsdERhdGVGb3JtYXQnKTtcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZXNTZXJ2aWNlXG4gICAgICAgICAgICAuc2VsZWN0KFVzZXJQcmVmZXJlbmNlVmFsdWVzLkxvY2FsZSlcbiAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLm9uRGVzdHJveSQpKVxuICAgICAgICAgICAgLnN1YnNjcmliZShsb2NhbGUgPT4gdGhpcy5kYXRlQWRhcHRlci5zZXRMb2NhbGUobG9jYWxlKSk7XG5cbiAgICAgICAgKDxNb21lbnREYXRlQWRhcHRlcj4gdGhpcy5kYXRlQWRhcHRlcikub3ZlcnJpZGVEaXNwbGF5Rm9ybWF0ID0gJ01NTSBERCc7XG5cbiAgICAgICAgaWYgKHRoaXMucHJvcGVydHkudmFsdWUpIHtcbiAgICAgICAgICAgIHRoaXMudmFsdWVEYXRlID0gbW9tZW50KHRoaXMucHJvcGVydHkudmFsdWUsIHRoaXMuZGF0ZUZvcm1hdCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBuZ09uRGVzdHJveSgpIHtcbiAgICAgICAgdGhpcy5vbkRlc3Ryb3kkLm5leHQodHJ1ZSk7XG4gICAgICAgIHRoaXMub25EZXN0cm95JC5jb21wbGV0ZSgpO1xuICAgIH1cblxuICAgIHNob3dQcm9wZXJ0eSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGlzcGxheUVtcHR5IHx8ICF0aGlzLnByb3BlcnR5LmlzRW1wdHkoKTtcbiAgICB9XG5cbiAgICBpc0VkaXRhYmxlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5lZGl0YWJsZSAmJiB0aGlzLnByb3BlcnR5LmVkaXRhYmxlO1xuICAgIH1cblxuICAgIHNob3dEYXRlUGlja2VyKCkge1xuICAgICAgICB0aGlzLmRhdGVwaWNrZXIub3BlbigpO1xuICAgIH1cblxuICAgIG9uRGF0ZUNoYW5nZWQobmV3RGF0ZVZhbHVlKSB7XG4gICAgICAgIGlmIChuZXdEYXRlVmFsdWUpIHtcbiAgICAgICAgICAgIGNvbnN0IG1vbWVudERhdGUgPSBtb21lbnQobmV3RGF0ZVZhbHVlLnZhbHVlLCB0aGlzLmRhdGVGb3JtYXQsIHRydWUpO1xuICAgICAgICAgICAgaWYgKG1vbWVudERhdGUuaXNWYWxpZCgpKSB7XG4gICAgICAgICAgICAgICAgdGhpcy52YWx1ZURhdGUgPSBtb21lbnREYXRlO1xuICAgICAgICAgICAgICAgIHRoaXMuY2FyZFZpZXdVcGRhdGVTZXJ2aWNlLnVwZGF0ZSh0aGlzLnByb3BlcnR5LCBtb21lbnREYXRlLnRvRGF0ZSgpKTtcbiAgICAgICAgICAgICAgICB0aGlzLnByb3BlcnR5LnZhbHVlID0gbW9tZW50RGF0ZS50b0RhdGUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxufVxuIl19