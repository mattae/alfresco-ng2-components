/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ComponentFactoryResolver, Input, ViewChild } from '@angular/core';
import { CardItemTypeService } from '../../services/card-item-types.service';
import { CardViewContentProxyDirective } from '../../directives/card-view-content-proxy.directive';
export class CardViewItemDispatcherComponent {
    /**
     * @param {?} cardItemTypeService
     * @param {?} resolver
     */
    constructor(cardItemTypeService, resolver) {
        this.cardItemTypeService = cardItemTypeService;
        this.resolver = resolver;
        this.displayEmpty = true;
        this.loaded = false;
        this.componentReference = null;
        /** @type {?} */
        const dynamicLifeCycleMethods = [
            'ngOnInit',
            'ngDoCheck',
            'ngAfterContentInit',
            'ngAfterContentChecked',
            'ngAfterViewInit',
            'ngAfterViewChecked',
            'ngOnDestroy'
        ];
        dynamicLifeCycleMethods.forEach((/**
         * @param {?} method
         * @return {?}
         */
        (method) => {
            this[method] = this.proxy.bind(this, method);
        }));
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (!this.loaded) {
            this.loadComponent();
            this.loaded = true;
        }
        Object.keys(changes)
            .map((/**
         * @param {?} changeName
         * @return {?}
         */
        (changeName) => [changeName, changes[changeName]]))
            .forEach((/**
         * @param {?} __0
         * @return {?}
         */
        ([inputParamName, simpleChange]) => {
            this.componentReference.instance[inputParamName] = simpleChange.currentValue;
        }));
        this.proxy('ngOnChanges', changes);
    }
    /**
     * @private
     * @return {?}
     */
    loadComponent() {
        /** @type {?} */
        const factoryClass = this.cardItemTypeService.resolveComponentType(this.property);
        /** @type {?} */
        const factory = this.resolver.resolveComponentFactory(factoryClass);
        this.componentReference = this.content.viewContainerRef.createComponent(factory);
        this.componentReference.instance.editable = this.editable;
        this.componentReference.instance.property = this.property;
        this.componentReference.instance.displayEmpty = this.displayEmpty;
    }
    /**
     * @private
     * @param {?} methodName
     * @param {...?} args
     * @return {?}
     */
    proxy(methodName, ...args) {
        if (this.componentReference.instance[methodName]) {
            this.componentReference.instance[methodName].apply(this.componentReference.instance, args);
        }
    }
}
CardViewItemDispatcherComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-card-view-item-dispatcher',
                template: '<ng-template adf-card-view-content-proxy></ng-template>'
            }] }
];
/** @nocollapse */
CardViewItemDispatcherComponent.ctorParameters = () => [
    { type: CardItemTypeService },
    { type: ComponentFactoryResolver }
];
CardViewItemDispatcherComponent.propDecorators = {
    property: [{ type: Input }],
    editable: [{ type: Input }],
    displayEmpty: [{ type: Input }],
    content: [{ type: ViewChild, args: [CardViewContentProxyDirective, { static: true },] }]
};
if (false) {
    /** @type {?} */
    CardViewItemDispatcherComponent.prototype.property;
    /** @type {?} */
    CardViewItemDispatcherComponent.prototype.editable;
    /** @type {?} */
    CardViewItemDispatcherComponent.prototype.displayEmpty;
    /**
     * @type {?}
     * @private
     */
    CardViewItemDispatcherComponent.prototype.content;
    /**
     * @type {?}
     * @private
     */
    CardViewItemDispatcherComponent.prototype.loaded;
    /**
     * @type {?}
     * @private
     */
    CardViewItemDispatcherComponent.prototype.componentReference;
    /** @type {?} */
    CardViewItemDispatcherComponent.prototype.ngOnInit;
    /** @type {?} */
    CardViewItemDispatcherComponent.prototype.ngDoCheck;
    /**
     * @type {?}
     * @private
     */
    CardViewItemDispatcherComponent.prototype.cardItemTypeService;
    /**
     * @type {?}
     * @private
     */
    CardViewItemDispatcherComponent.prototype.resolver;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWl0ZW0tZGlzcGF0Y2hlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJjYXJkLXZpZXcvY29tcG9uZW50cy9jYXJkLXZpZXctaXRlbS1kaXNwYXRjaGVyL2NhcmQtdmlldy1pdGVtLWRpc3BhdGNoZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxPQUFPLEVBQ0gsU0FBUyxFQUNULHdCQUF3QixFQUN4QixLQUFLLEVBSUwsU0FBUyxFQUNaLE1BQU0sZUFBZSxDQUFDO0FBRXZCLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQzdFLE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLG9EQUFvRCxDQUFDO0FBTW5HLE1BQU0sT0FBTywrQkFBK0I7Ozs7O0lBbUJ4QyxZQUFvQixtQkFBd0MsRUFDeEMsUUFBa0M7UUFEbEMsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxhQUFRLEdBQVIsUUFBUSxDQUEwQjtRQVp0RCxpQkFBWSxHQUFZLElBQUksQ0FBQztRQUtyQixXQUFNLEdBQVksS0FBSyxDQUFDO1FBQ3hCLHVCQUFrQixHQUFRLElBQUksQ0FBQzs7Y0FPN0IsdUJBQXVCLEdBQUc7WUFDNUIsVUFBVTtZQUNWLFdBQVc7WUFDWCxvQkFBb0I7WUFDcEIsdUJBQXVCO1lBQ3ZCLGlCQUFpQjtZQUNqQixvQkFBb0I7WUFDcEIsYUFBYTtTQUNoQjtRQUVELHVCQUF1QixDQUFDLE9BQU87Ozs7UUFBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO1lBQ3ZDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDakQsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxPQUFzQjtRQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNkLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUNyQixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztTQUN0QjtRQUVELE1BQU0sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO2FBQ2YsR0FBRzs7OztRQUFDLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBQzthQUN0RCxPQUFPOzs7O1FBQUMsQ0FBQyxDQUFDLGNBQWMsRUFBRSxZQUFZLENBQXlCLEVBQUUsRUFBRTtZQUNoRSxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxHQUFHLFlBQVksQ0FBQyxZQUFZLENBQUM7UUFDakYsQ0FBQyxFQUFDLENBQUM7UUFFUCxJQUFJLENBQUMsS0FBSyxDQUFDLGFBQWEsRUFBRSxPQUFPLENBQUMsQ0FBQztJQUN2QyxDQUFDOzs7OztJQUVPLGFBQWE7O2NBQ1gsWUFBWSxHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDOztjQUUzRSxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyx1QkFBdUIsQ0FBQyxZQUFZLENBQUM7UUFDbkUsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRWpGLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDMUQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUMxRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQ3RFLENBQUM7Ozs7Ozs7SUFFTyxLQUFLLENBQUMsVUFBVSxFQUFFLEdBQUcsSUFBSTtRQUM3QixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDOUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUM5RjtJQUNMLENBQUM7OztZQXRFSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLCtCQUErQjtnQkFDekMsUUFBUSxFQUFFLHlEQUF5RDthQUN0RTs7OztZQU5RLG1CQUFtQjtZQVJ4Qix3QkFBd0I7Ozt1QkFnQnZCLEtBQUs7dUJBR0wsS0FBSzsyQkFHTCxLQUFLO3NCQUdMLFNBQVMsU0FBQyw2QkFBNkIsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7Ozs7SUFUeEQsbURBQ3VCOztJQUV2QixtREFDa0I7O0lBRWxCLHVEQUM2Qjs7Ozs7SUFFN0Isa0RBQytDOzs7OztJQUUvQyxpREFBZ0M7Ozs7O0lBQ2hDLDZEQUF1Qzs7SUFFdkMsbURBQWdCOztJQUNoQixvREFBaUI7Ozs7O0lBRUwsOERBQWdEOzs7OztJQUNoRCxtREFBMEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cblxuLyohXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cbiAqXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4gKlxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuICpcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXG4gKi9cblxuaW1wb3J0IHtcbiAgICBDb21wb25lbnQsXG4gICAgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLFxuICAgIElucHV0LFxuICAgIE9uQ2hhbmdlcyxcbiAgICBTaW1wbGVDaGFuZ2UsXG4gICAgU2ltcGxlQ2hhbmdlcyxcbiAgICBWaWV3Q2hpbGRcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDYXJkVmlld0l0ZW0gfSBmcm9tICcuLi8uLi9pbnRlcmZhY2VzL2NhcmQtdmlldy1pdGVtLmludGVyZmFjZSc7XG5pbXBvcnQgeyBDYXJkSXRlbVR5cGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvY2FyZC1pdGVtLXR5cGVzLnNlcnZpY2UnO1xuaW1wb3J0IHsgQ2FyZFZpZXdDb250ZW50UHJveHlEaXJlY3RpdmUgfSBmcm9tICcuLi8uLi9kaXJlY3RpdmVzL2NhcmQtdmlldy1jb250ZW50LXByb3h5LmRpcmVjdGl2ZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYWRmLWNhcmQtdmlldy1pdGVtLWRpc3BhdGNoZXInLFxuICAgIHRlbXBsYXRlOiAnPG5nLXRlbXBsYXRlIGFkZi1jYXJkLXZpZXctY29udGVudC1wcm94eT48L25nLXRlbXBsYXRlPidcbn0pXG5leHBvcnQgY2xhc3MgQ2FyZFZpZXdJdGVtRGlzcGF0Y2hlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XG4gICAgQElucHV0KClcbiAgICBwcm9wZXJ0eTogQ2FyZFZpZXdJdGVtO1xuXG4gICAgQElucHV0KClcbiAgICBlZGl0YWJsZTogYm9vbGVhbjtcblxuICAgIEBJbnB1dCgpXG4gICAgZGlzcGxheUVtcHR5OiBib29sZWFuID0gdHJ1ZTtcblxuICAgIEBWaWV3Q2hpbGQoQ2FyZFZpZXdDb250ZW50UHJveHlEaXJlY3RpdmUsIHtzdGF0aWM6IHRydWV9KVxuICAgIHByaXZhdGUgY29udGVudDogQ2FyZFZpZXdDb250ZW50UHJveHlEaXJlY3RpdmU7XG5cbiAgICBwcml2YXRlIGxvYWRlZDogYm9vbGVhbiA9IGZhbHNlO1xuICAgIHByaXZhdGUgY29tcG9uZW50UmVmZXJlbmNlOiBhbnkgPSBudWxsO1xuXG4gICAgcHVibGljIG5nT25Jbml0O1xuICAgIHB1YmxpYyBuZ0RvQ2hlY2s7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNhcmRJdGVtVHlwZVNlcnZpY2U6IENhcmRJdGVtVHlwZVNlcnZpY2UsXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSByZXNvbHZlcjogQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyKSB7XG4gICAgICAgIGNvbnN0IGR5bmFtaWNMaWZlQ3ljbGVNZXRob2RzID0gW1xuICAgICAgICAgICAgJ25nT25Jbml0JyxcbiAgICAgICAgICAgICduZ0RvQ2hlY2snLFxuICAgICAgICAgICAgJ25nQWZ0ZXJDb250ZW50SW5pdCcsXG4gICAgICAgICAgICAnbmdBZnRlckNvbnRlbnRDaGVja2VkJyxcbiAgICAgICAgICAgICduZ0FmdGVyVmlld0luaXQnLFxuICAgICAgICAgICAgJ25nQWZ0ZXJWaWV3Q2hlY2tlZCcsXG4gICAgICAgICAgICAnbmdPbkRlc3Ryb3knXG4gICAgICAgIF07XG5cbiAgICAgICAgZHluYW1pY0xpZmVDeWNsZU1ldGhvZHMuZm9yRWFjaCgobWV0aG9kKSA9PiB7XG4gICAgICAgICAgICB0aGlzW21ldGhvZF0gPSB0aGlzLnByb3h5LmJpbmQodGhpcywgbWV0aG9kKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xuICAgICAgICBpZiAoIXRoaXMubG9hZGVkKSB7XG4gICAgICAgICAgICB0aGlzLmxvYWRDb21wb25lbnQoKTtcbiAgICAgICAgICAgIHRoaXMubG9hZGVkID0gdHJ1ZTtcbiAgICAgICAgfVxuXG4gICAgICAgIE9iamVjdC5rZXlzKGNoYW5nZXMpXG4gICAgICAgICAgICAubWFwKChjaGFuZ2VOYW1lKSA9PiBbY2hhbmdlTmFtZSwgY2hhbmdlc1tjaGFuZ2VOYW1lXV0pXG4gICAgICAgICAgICAuZm9yRWFjaCgoW2lucHV0UGFyYW1OYW1lLCBzaW1wbGVDaGFuZ2VdOiBbc3RyaW5nLCBTaW1wbGVDaGFuZ2VdKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5jb21wb25lbnRSZWZlcmVuY2UuaW5zdGFuY2VbaW5wdXRQYXJhbU5hbWVdID0gc2ltcGxlQ2hhbmdlLmN1cnJlbnRWYWx1ZTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMucHJveHkoJ25nT25DaGFuZ2VzJywgY2hhbmdlcyk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBsb2FkQ29tcG9uZW50KCkge1xuICAgICAgICBjb25zdCBmYWN0b3J5Q2xhc3MgPSB0aGlzLmNhcmRJdGVtVHlwZVNlcnZpY2UucmVzb2x2ZUNvbXBvbmVudFR5cGUodGhpcy5wcm9wZXJ0eSk7XG5cbiAgICAgICAgY29uc3QgZmFjdG9yeSA9IHRoaXMucmVzb2x2ZXIucmVzb2x2ZUNvbXBvbmVudEZhY3RvcnkoZmFjdG9yeUNsYXNzKTtcbiAgICAgICAgdGhpcy5jb21wb25lbnRSZWZlcmVuY2UgPSB0aGlzLmNvbnRlbnQudmlld0NvbnRhaW5lclJlZi5jcmVhdGVDb21wb25lbnQoZmFjdG9yeSk7XG5cbiAgICAgICAgdGhpcy5jb21wb25lbnRSZWZlcmVuY2UuaW5zdGFuY2UuZWRpdGFibGUgPSB0aGlzLmVkaXRhYmxlO1xuICAgICAgICB0aGlzLmNvbXBvbmVudFJlZmVyZW5jZS5pbnN0YW5jZS5wcm9wZXJ0eSA9IHRoaXMucHJvcGVydHk7XG4gICAgICAgIHRoaXMuY29tcG9uZW50UmVmZXJlbmNlLmluc3RhbmNlLmRpc3BsYXlFbXB0eSA9IHRoaXMuZGlzcGxheUVtcHR5O1xuICAgIH1cblxuICAgIHByaXZhdGUgcHJveHkobWV0aG9kTmFtZSwgLi4uYXJncykge1xuICAgICAgICBpZiAodGhpcy5jb21wb25lbnRSZWZlcmVuY2UuaW5zdGFuY2VbbWV0aG9kTmFtZV0pIHtcbiAgICAgICAgICAgIHRoaXMuY29tcG9uZW50UmVmZXJlbmNlLmluc3RhbmNlW21ldGhvZE5hbWVdLmFwcGx5KHRoaXMuY29tcG9uZW50UmVmZXJlbmNlLmluc3RhbmNlLCBhcmdzKTtcbiAgICAgICAgfVxuICAgIH1cbn1cbiJdfQ==