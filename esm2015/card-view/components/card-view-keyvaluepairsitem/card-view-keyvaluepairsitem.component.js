/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input } from '@angular/core';
import { CardViewUpdateService } from '../../services/card-view-update.service';
import { CardViewKeyValuePairsItemModel } from '../../models/card-view.models';
import { MatTableDataSource } from '@angular/material';
export class CardViewKeyValuePairsItemComponent {
    /**
     * @param {?} cardViewUpdateService
     */
    constructor(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.editable = false;
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        this.values = this.property.value || [];
        this.matTableValues = new MatTableDataSource(this.values);
    }
    /**
     * @return {?}
     */
    isEditable() {
        return this.editable && this.property.editable;
    }
    /**
     * @return {?}
     */
    add() {
        this.values.push({ name: '', value: '' });
    }
    /**
     * @param {?} index
     * @return {?}
     */
    remove(index) {
        this.values.splice(index, 1);
        this.save(true);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    onBlur(value) {
        if (value.length) {
            this.save();
        }
    }
    /**
     * @param {?=} remove
     * @return {?}
     */
    save(remove) {
        /** @type {?} */
        const validValues = this.values.filter((/**
         * @param {?} i
         * @return {?}
         */
        (i) => i.name.length && i.value.length));
        if (remove || validValues.length) {
            this.cardViewUpdateService.update(this.property, validValues);
            this.property.value = validValues;
        }
    }
}
CardViewKeyValuePairsItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-card-view-boolitem',
                template: "<div [attr.data-automation-id]=\"'card-key-value-pairs-label-' + property.key\" class=\"adf-property-label\">{{ property.label | translate }}</div>\r\n<div class=\"adf-property-value\">\r\n\r\n    <div *ngIf=\"isEditable()\">\r\n        {{ 'CORE.CARDVIEW.KEYVALUEPAIRS.ADD' | translate }}\r\n        <button (click)=\"add()\" mat-icon-button class=\"adf-card-view__key-value-pairs__add-btn\" [attr.data-automation-id]=\"'card-key-value-pairs-button-' + property.key\">\r\n            <mat-icon>add</mat-icon>\r\n        </button>\r\n    </div>\r\n\r\n    <div *ngIf=\"!isEditable()\" class=\"adf-card-view__key-value-pairs__read-only\">\r\n        <mat-table #table [dataSource]=\"matTableValues\" class=\"mat-elevation-z8\">\r\n            <ng-container matColumnDef=\"name\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.NAME' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let item\">{{item.name}}</mat-cell>\r\n            </ng-container>\r\n            <ng-container matColumnDef=\"value\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.VALUE' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let item\">{{item.value}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <mat-header-row *matHeaderRowDef=\"['name', 'value']\"></mat-header-row>\r\n            <mat-row *matRowDef=\"let row; columns: ['name', 'value'];\"></mat-row>\r\n        </mat-table>\r\n    </div>\r\n\r\n\r\n    <div class=\"adf-card-view__key-value-pairs\" *ngIf=\"isEditable() && values && values.length\">\r\n        <div class=\"adf-card-view__key-value-pairs__row\">\r\n            <div class=\"adf-card-view__key-value-pairs__col\">{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.NAME' | translate }}</div>\r\n            <div class=\"adf-card-view__key-value-pairs__col\">{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.VALUE' | translate }}</div>\r\n        </div>\r\n\r\n        <div class=\"adf-card-view__key-value-pairs__row\" *ngFor=\"let item of values; let i = index\">\r\n            <div class=\"adf-card-view__key-value-pairs__col\">\r\n                <mat-form-field class=\"adf-example-full-width\">\r\n                    <input matInput\r\n                           placeholder=\"{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.NAME' | translate }}\"\r\n                           (blur)=\"onBlur(item.value)\"\r\n                           [attr.data-automation-id]=\"'card-'+ property.key +'-name-input-' + i\"\r\n                           [(ngModel)]=\"values[i].name\">\r\n                </mat-form-field>\r\n            </div>\r\n            <div class=\"adf-card-view__key-value-pairs__col\">\r\n                <mat-form-field class=\"adf-example-full-width\">\r\n                    <input matInput\r\n                           placeholder=\"{{ 'CORE.CARDVIEW.KEYVALUEPAIRS.VALUE' | translate }}\"\r\n                           (blur)=\"onBlur(item.value)\"\r\n                           [attr.data-automation-id]=\"'card-'+ property.key +'-value-input-' + i\"\r\n                           [(ngModel)]=\"values[i].value\">\r\n                </mat-form-field>\r\n            </div>\r\n            <button mat-icon-button (click)=\"remove(i)\" class=\"adf-card-view__key-value-pairs__remove-btn\">\r\n                <mat-icon>close</mat-icon>\r\n            </button>\r\n        </div>\r\n    </div>\r\n</div>\r\n",
                styles: [".adf-card-view__key-value-pairs__col{display:inline-block;width:39%}.adf-card-view__key-value-pairs__col .mat-form-field{width:100%}.adf-card-view__key-value-pairs__read-only .mat-table{box-shadow:none}.adf-card-view__key-value-pairs__read-only .mat-header-row,.adf-card-view__key-value-pairs__read-only .mat-row{padding:0}"]
            }] }
];
/** @nocollapse */
CardViewKeyValuePairsItemComponent.ctorParameters = () => [
    { type: CardViewUpdateService }
];
CardViewKeyValuePairsItemComponent.propDecorators = {
    property: [{ type: Input }],
    editable: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    CardViewKeyValuePairsItemComponent.prototype.property;
    /** @type {?} */
    CardViewKeyValuePairsItemComponent.prototype.editable;
    /** @type {?} */
    CardViewKeyValuePairsItemComponent.prototype.values;
    /** @type {?} */
    CardViewKeyValuePairsItemComponent.prototype.matTableValues;
    /**
     * @type {?}
     * @private
     */
    CardViewKeyValuePairsItemComponent.prototype.cardViewUpdateService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWtleXZhbHVlcGFpcnNpdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNhcmQtdmlldy9jb21wb25lbnRzL2NhcmQtdmlldy1rZXl2YWx1ZXBhaXJzaXRlbS9jYXJkLXZpZXcta2V5dmFsdWVwYWlyc2l0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBQzVELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSw4QkFBOEIsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBRS9FLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBUXZELE1BQU0sT0FBTyxrQ0FBa0M7Ozs7SUFXM0MsWUFBb0IscUJBQTRDO1FBQTVDLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBdUI7UUFMaEUsYUFBUSxHQUFZLEtBQUssQ0FBQztJQUt5QyxDQUFDOzs7O0lBRXBFLFdBQVc7UUFDUCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxJQUFJLEVBQUUsQ0FBQztRQUN4QyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksa0JBQWtCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzlELENBQUM7Ozs7SUFFRCxVQUFVO1FBQ04sT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO0lBQ25ELENBQUM7Ozs7SUFFRCxHQUFHO1FBQ0MsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQzlDLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLEtBQWE7UUFDaEIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDO1FBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDcEIsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsS0FBSztRQUNSLElBQUksS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNkLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUNmO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxJQUFJLENBQUMsTUFBZ0I7O2NBQ1gsV0FBVyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTTs7OztRQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBQztRQUU5RSxJQUFJLE1BQU0sSUFBSSxXQUFXLENBQUMsTUFBTSxFQUFFO1lBQzlCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxXQUFXLENBQUMsQ0FBQztZQUM5RCxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUM7U0FDckM7SUFDTCxDQUFDOzs7WUFsREosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSx3QkFBd0I7Z0JBQ2xDLDQyR0FBMkQ7O2FBRTlEOzs7O1lBVFEscUJBQXFCOzs7dUJBYXpCLEtBQUs7dUJBR0wsS0FBSzs7OztJQUhOLHNEQUN5Qzs7SUFFekMsc0RBQzBCOztJQUUxQixvREFBd0M7O0lBQ3hDLDREQUFrRTs7Ozs7SUFFdEQsbUVBQW9EIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDYXJkVmlld1VwZGF0ZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9jYXJkLXZpZXctdXBkYXRlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDYXJkVmlld0tleVZhbHVlUGFpcnNJdGVtTW9kZWwgfSBmcm9tICcuLi8uLi9tb2RlbHMvY2FyZC12aWV3Lm1vZGVscyc7XHJcbmltcG9ydCB7IENhcmRWaWV3S2V5VmFsdWVQYWlyc0l0ZW1UeXBlIH0gZnJvbSAnLi4vLi4vaW50ZXJmYWNlcy9jYXJkLXZpZXcuaW50ZXJmYWNlcyc7XHJcbmltcG9ydCB7IE1hdFRhYmxlRGF0YVNvdXJjZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtY2FyZC12aWV3LWJvb2xpdGVtJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9jYXJkLXZpZXcta2V5dmFsdWVwYWlyc2l0ZW0uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vY2FyZC12aWV3LWtleXZhbHVlcGFpcnNpdGVtLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBDYXJkVmlld0tleVZhbHVlUGFpcnNJdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzIHtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgcHJvcGVydHk6IENhcmRWaWV3S2V5VmFsdWVQYWlyc0l0ZW1Nb2RlbDtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgZWRpdGFibGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICB2YWx1ZXM6IENhcmRWaWV3S2V5VmFsdWVQYWlyc0l0ZW1UeXBlW107XHJcbiAgICBtYXRUYWJsZVZhbHVlczogTWF0VGFibGVEYXRhU291cmNlPENhcmRWaWV3S2V5VmFsdWVQYWlyc0l0ZW1UeXBlPjtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNhcmRWaWV3VXBkYXRlU2VydmljZTogQ2FyZFZpZXdVcGRhdGVTZXJ2aWNlKSB7fVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKCkge1xyXG4gICAgICAgIHRoaXMudmFsdWVzID0gdGhpcy5wcm9wZXJ0eS52YWx1ZSB8fCBbXTtcclxuICAgICAgICB0aGlzLm1hdFRhYmxlVmFsdWVzID0gbmV3IE1hdFRhYmxlRGF0YVNvdXJjZSh0aGlzLnZhbHVlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgaXNFZGl0YWJsZSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lZGl0YWJsZSAmJiB0aGlzLnByb3BlcnR5LmVkaXRhYmxlO1xyXG4gICAgfVxyXG5cclxuICAgIGFkZCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnZhbHVlcy5wdXNoKHsgbmFtZTogJycsIHZhbHVlOiAnJyB9KTtcclxuICAgIH1cclxuXHJcbiAgICByZW1vdmUoaW5kZXg6IG51bWJlcik6IHZvaWQge1xyXG4gICAgICAgIHRoaXMudmFsdWVzLnNwbGljZShpbmRleCwgMSk7XHJcbiAgICAgICAgdGhpcy5zYXZlKHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uQmx1cih2YWx1ZSk6IHZvaWQge1xyXG4gICAgICAgIGlmICh2YWx1ZS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgdGhpcy5zYXZlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHNhdmUocmVtb3ZlPzogYm9vbGVhbik6IHZvaWQge1xyXG4gICAgICAgIGNvbnN0IHZhbGlkVmFsdWVzID0gdGhpcy52YWx1ZXMuZmlsdGVyKChpKSA9PiBpLm5hbWUubGVuZ3RoICYmIGkudmFsdWUubGVuZ3RoKTtcclxuXHJcbiAgICAgICAgaWYgKHJlbW92ZSB8fCB2YWxpZFZhbHVlcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgdGhpcy5jYXJkVmlld1VwZGF0ZVNlcnZpY2UudXBkYXRlKHRoaXMucHJvcGVydHksIHZhbGlkVmFsdWVzKTtcclxuICAgICAgICAgICAgdGhpcy5wcm9wZXJ0eS52YWx1ZSA9IHZhbGlkVmFsdWVzO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=