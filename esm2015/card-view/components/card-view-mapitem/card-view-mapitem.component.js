/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input } from '@angular/core';
import { CardViewMapItemModel } from '../../models/card-view-mapitem.model';
import { CardViewUpdateService } from '../../services/card-view-update.service';
export class CardViewMapItemComponent {
    /**
     * @param {?} cardViewUpdateService
     */
    constructor(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.displayEmpty = true;
    }
    /**
     * @return {?}
     */
    showProperty() {
        return this.displayEmpty || !this.property.isEmpty();
    }
    /**
     * @return {?}
     */
    isClickable() {
        return this.property.clickable;
    }
    /**
     * @return {?}
     */
    clicked() {
        this.cardViewUpdateService.clicked(this.property);
    }
}
CardViewMapItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-card-view-mapitem',
                template: "<div [attr.data-automation-id]=\"'card-mapitem-label-' + property.key\" class=\"adf-property-label\" *ngIf=\"showProperty()\">{{ property.label | translate }}</div>\r\n<div class=\"adf-property-value\">\r\n    <div>\r\n        <span *ngIf=\"!isClickable(); else elseBlock\" [attr.data-automation-id]=\"'card-mapitem-value-' + property.key\">\r\n            <span *ngIf=\"showProperty();\">{{ property.displayValue }}</span>\r\n        </span>\r\n        <ng-template #elseBlock>\r\n            <span class=\"adf-mapitem-clickable-value\" (click)=\"clicked()\" [attr.data-automation-id]=\"'card-mapitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ property.displayValue }}</span>\r\n            </span>\r\n        </ng-template>\r\n    </div>\r\n    <ng-template #elseEmptyValueBlock>\r\n        {{ property.default | translate }}\r\n    </ng-template>\r\n</div>\r\n",
                styles: [".adf-mapitem-clickable-value{cursor:pointer!important}"]
            }] }
];
/** @nocollapse */
CardViewMapItemComponent.ctorParameters = () => [
    { type: CardViewUpdateService }
];
CardViewMapItemComponent.propDecorators = {
    property: [{ type: Input }],
    displayEmpty: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    CardViewMapItemComponent.prototype.property;
    /** @type {?} */
    CardViewMapItemComponent.prototype.displayEmpty;
    /**
     * @type {?}
     * @private
     */
    CardViewMapItemComponent.prototype.cardViewUpdateService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LW1hcGl0ZW0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiY2FyZC12aWV3L2NvbXBvbmVudHMvY2FyZC12aWV3LW1hcGl0ZW0vY2FyZC12aWV3LW1hcGl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2pELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBUWhGLE1BQU0sT0FBTyx3QkFBd0I7Ozs7SUFPakMsWUFBb0IscUJBQTRDO1FBQTVDLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBdUI7UUFGaEUsaUJBQVksR0FBWSxJQUFJLENBQUM7SUFFc0MsQ0FBQzs7OztJQUVwRSxZQUFZO1FBQ1IsT0FBTyxJQUFJLENBQUMsWUFBWSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUN6RCxDQUFDOzs7O0lBRUQsV0FBVztRQUNQLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7SUFDbkMsQ0FBQzs7OztJQUVELE9BQU87UUFDSCxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN0RCxDQUFDOzs7WUF6QkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSx1QkFBdUI7Z0JBQ2pDLHk2QkFBaUQ7O2FBRXBEOzs7O1lBTlEscUJBQXFCOzs7dUJBU3pCLEtBQUs7MkJBR0wsS0FBSzs7OztJQUhOLDRDQUMrQjs7SUFFL0IsZ0RBQzZCOzs7OztJQUVqQix5REFBb0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDYXJkVmlld01hcEl0ZW1Nb2RlbCB9IGZyb20gJy4uLy4uL21vZGVscy9jYXJkLXZpZXctbWFwaXRlbS5tb2RlbCc7XHJcbmltcG9ydCB7IENhcmRWaWV3VXBkYXRlU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2NhcmQtdmlldy11cGRhdGUuc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLWNhcmQtdmlldy1tYXBpdGVtJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9jYXJkLXZpZXctbWFwaXRlbS5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9jYXJkLXZpZXctbWFwaXRlbS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQ2FyZFZpZXdNYXBJdGVtQ29tcG9uZW50IHtcclxuICAgIEBJbnB1dCgpXHJcbiAgICBwcm9wZXJ0eTogQ2FyZFZpZXdNYXBJdGVtTW9kZWw7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGRpc3BsYXlFbXB0eTogYm9vbGVhbiA9IHRydWU7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBjYXJkVmlld1VwZGF0ZVNlcnZpY2U6IENhcmRWaWV3VXBkYXRlU2VydmljZSkge31cclxuXHJcbiAgICBzaG93UHJvcGVydHkoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZGlzcGxheUVtcHR5IHx8ICF0aGlzLnByb3BlcnR5LmlzRW1wdHkoKTtcclxuICAgIH1cclxuXHJcbiAgICBpc0NsaWNrYWJsZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm9wZXJ0eS5jbGlja2FibGU7XHJcbiAgICB9XHJcblxyXG4gICAgY2xpY2tlZCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmNhcmRWaWV3VXBkYXRlU2VydmljZS5jbGlja2VkKHRoaXMucHJvcGVydHkpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==