/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input } from '@angular/core';
import { CardViewSelectItemModel } from '../../models/card-view-selectitem.model';
import { CardViewUpdateService } from '../../services/card-view-update.service';
import { Observable } from 'rxjs';
export class CardViewSelectItemComponent {
    /**
     * @param {?} cardViewUpdateService
     */
    constructor(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.editable = false;
    }
    /**
     * @return {?}
     */
    ngOnChanges() {
        this.value = this.property.value;
    }
    /**
     * @return {?}
     */
    isEditable() {
        return this.editable && this.property.editable;
    }
    /**
     * @return {?}
     */
    getOptions() {
        return this.options$ || this.property.options$;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onChange(event) {
        this.cardViewUpdateService.update(this.property, event.value);
        this.property.value = event.value;
    }
}
CardViewSelectItemComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-card-view-selectitem',
                template: "<div [attr.data-automation-id]=\"'card-select-label-' + property.key\" class=\"adf-property-label\">{{ property.label | translate }}</div>\r\n<div class=\"adf-property-value\">\r\n    <div *ngIf=\"!isEditable()\" data-automation-class=\"read-only-value\">{{ property.displayValue | async }}</div>\r\n    <div *ngIf=\"isEditable()\">\r\n        <mat-form-field>\r\n            <mat-select [(value)]=\"value\" (selectionChange)=\"onChange($event)\" data-automation-class=\"select-box\">\r\n              <mat-option *ngFor=\"let option of getOptions() | async\" [value]=\"option.key\">\r\n                {{ option.label | translate }}\r\n              </mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n    </div>\r\n</div>\r\n",
                styles: [".mat-form-field-type-mat-select{width:100%}"]
            }] }
];
/** @nocollapse */
CardViewSelectItemComponent.ctorParameters = () => [
    { type: CardViewUpdateService }
];
CardViewSelectItemComponent.propDecorators = {
    property: [{ type: Input }],
    editable: [{ type: Input }],
    options$: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    CardViewSelectItemComponent.prototype.property;
    /** @type {?} */
    CardViewSelectItemComponent.prototype.editable;
    /** @type {?} */
    CardViewSelectItemComponent.prototype.options$;
    /** @type {?} */
    CardViewSelectItemComponent.prototype.value;
    /**
     * @type {?}
     * @private
     */
    CardViewSelectItemComponent.prototype.cardViewUpdateService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LXNlbGVjdGl0ZW0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiY2FyZC12aWV3L2NvbXBvbmVudHMvY2FyZC12aWV3LXNlbGVjdGl0ZW0vY2FyZC12aWV3LXNlbGVjdGl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBQzVELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFTbEMsTUFBTSxPQUFPLDJCQUEyQjs7OztJQVNwQyxZQUFvQixxQkFBNEM7UUFBNUMsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtRQU52RCxhQUFRLEdBQVksS0FBSyxDQUFDO0lBTWdDLENBQUM7Ozs7SUFFcEUsV0FBVztRQUNQLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7SUFDckMsQ0FBQzs7OztJQUVELFVBQVU7UUFDTixPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7SUFDbkQsQ0FBQzs7OztJQUVELFVBQVU7UUFDTixPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7SUFDbkQsQ0FBQzs7Ozs7SUFFRCxRQUFRLENBQUMsS0FBc0I7UUFDM0IsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM5RCxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO0lBQ3RDLENBQUM7OztZQS9CSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLDBCQUEwQjtnQkFDcEMsNHZCQUFvRDs7YUFFdkQ7Ozs7WUFUUSxxQkFBcUI7Ozt1QkFXekIsS0FBSzt1QkFFTCxLQUFLO3VCQUVMLEtBQUs7Ozs7SUFKTiwrQ0FBbUQ7O0lBRW5ELCtDQUFtQzs7SUFFbkMsK0NBQWtFOztJQUVsRSw0Q0FBYzs7Ozs7SUFFRiw0REFBb0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25DaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENhcmRWaWV3U2VsZWN0SXRlbU1vZGVsIH0gZnJvbSAnLi4vLi4vbW9kZWxzL2NhcmQtdmlldy1zZWxlY3RpdGVtLm1vZGVsJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdVcGRhdGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvY2FyZC12aWV3LXVwZGF0ZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBDYXJkVmlld1NlbGVjdEl0ZW1PcHRpb24gfSBmcm9tICcuLi8uLi9pbnRlcmZhY2VzL2NhcmQtdmlldy5pbnRlcmZhY2VzJztcclxuaW1wb3J0IHsgTWF0U2VsZWN0Q2hhbmdlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1jYXJkLXZpZXctc2VsZWN0aXRlbScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vY2FyZC12aWV3LXNlbGVjdGl0ZW0uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vY2FyZC12aWV3LXNlbGVjdGl0ZW0uY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ2FyZFZpZXdTZWxlY3RJdGVtQ29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzIHtcclxuICAgIEBJbnB1dCgpIHByb3BlcnR5OiBDYXJkVmlld1NlbGVjdEl0ZW1Nb2RlbDxzdHJpbmc+O1xyXG5cclxuICAgIEBJbnB1dCgpIGVkaXRhYmxlOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgQElucHV0KCkgb3B0aW9ucyQ6IE9ic2VydmFibGU8Q2FyZFZpZXdTZWxlY3RJdGVtT3B0aW9uPHN0cmluZz5bXT47XHJcblxyXG4gICAgdmFsdWU6IHN0cmluZztcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNhcmRWaWV3VXBkYXRlU2VydmljZTogQ2FyZFZpZXdVcGRhdGVTZXJ2aWNlKSB7fVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMudmFsdWUgPSB0aGlzLnByb3BlcnR5LnZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzRWRpdGFibGUoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZWRpdGFibGUgJiYgdGhpcy5wcm9wZXJ0eS5lZGl0YWJsZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRPcHRpb25zKCk6IE9ic2VydmFibGU8Q2FyZFZpZXdTZWxlY3RJdGVtT3B0aW9uPHN0cmluZz5bXT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm9wdGlvbnMkIHx8IHRoaXMucHJvcGVydHkub3B0aW9ucyQ7XHJcbiAgICB9XHJcblxyXG4gICAgb25DaGFuZ2UoZXZlbnQ6IE1hdFNlbGVjdENoYW5nZSk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuY2FyZFZpZXdVcGRhdGVTZXJ2aWNlLnVwZGF0ZSh0aGlzLnByb3BlcnR5LCBldmVudC52YWx1ZSk7XHJcbiAgICAgICAgdGhpcy5wcm9wZXJ0eS52YWx1ZSA9IGV2ZW50LnZhbHVlO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==