/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CardViewBaseItemModel } from './card-view-baseitem.model';
export class CardViewBoolItemModel extends CardViewBaseItemModel {
    /**
     * @param {?} cardViewBoolItemProperties
     */
    constructor(cardViewBoolItemProperties) {
        super(cardViewBoolItemProperties);
        this.type = 'bool';
        this.value = false;
        if (cardViewBoolItemProperties.value !== undefined) {
            this.value = !!JSON.parse(cardViewBoolItemProperties.value);
        }
    }
    /**
     * @return {?}
     */
    get displayValue() {
        if (this.isEmpty()) {
            return this.default;
        }
        else {
            return this.value;
        }
    }
}
if (false) {
    /** @type {?} */
    CardViewBoolItemModel.prototype.type;
    /** @type {?} */
    CardViewBoolItemModel.prototype.value;
    /** @type {?} */
    CardViewBoolItemModel.prototype.default;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWJvb2xpdGVtLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiY2FyZC12aWV3L21vZGVscy9jYXJkLXZpZXctYm9vbGl0ZW0ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFHbkUsTUFBTSxPQUFPLHFCQUFzQixTQUFRLHFCQUFxQjs7OztJQUs1RCxZQUFZLDBCQUFzRDtRQUM5RCxLQUFLLENBQUMsMEJBQTBCLENBQUMsQ0FBQztRQUx0QyxTQUFJLEdBQVcsTUFBTSxDQUFDO1FBQ3RCLFVBQUssR0FBWSxLQUFLLENBQUM7UUFNbkIsSUFBSSwwQkFBMEIsQ0FBQyxLQUFLLEtBQUssU0FBUyxFQUFFO1lBQ2hELElBQUksQ0FBQyxLQUFLLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsMEJBQTBCLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDL0Q7SUFDTCxDQUFDOzs7O0lBRUQsSUFBSSxZQUFZO1FBQ1osSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUU7WUFDaEIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO1NBQ3ZCO2FBQU07WUFDSCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7U0FDckI7SUFDTCxDQUFDO0NBQ0o7OztJQW5CRyxxQ0FBc0I7O0lBQ3RCLHNDQUF1Qjs7SUFDdkIsd0NBQWlCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENhcmRWaWV3SXRlbSB9IGZyb20gJy4uL2ludGVyZmFjZXMvY2FyZC12aWV3LWl0ZW0uaW50ZXJmYWNlJztcclxuaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudE1vZGVsIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvZHluYW1pYy1jb21wb25lbnQtbWFwcGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDYXJkVmlld0Jhc2VJdGVtTW9kZWwgfSBmcm9tICcuL2NhcmQtdmlldy1iYXNlaXRlbS5tb2RlbCc7XHJcbmltcG9ydCB7IENhcmRWaWV3Qm9vbEl0ZW1Qcm9wZXJ0aWVzIH0gZnJvbSAnLi4vaW50ZXJmYWNlcy9jYXJkLXZpZXcuaW50ZXJmYWNlcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgQ2FyZFZpZXdCb29sSXRlbU1vZGVsIGV4dGVuZHMgQ2FyZFZpZXdCYXNlSXRlbU1vZGVsIGltcGxlbWVudHMgQ2FyZFZpZXdJdGVtLCBEeW5hbWljQ29tcG9uZW50TW9kZWwge1xyXG4gICAgdHlwZTogc3RyaW5nID0gJ2Jvb2wnO1xyXG4gICAgdmFsdWU6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGRlZmF1bHQ6IGJvb2xlYW47XHJcblxyXG4gICAgY29uc3RydWN0b3IoY2FyZFZpZXdCb29sSXRlbVByb3BlcnRpZXM6IENhcmRWaWV3Qm9vbEl0ZW1Qcm9wZXJ0aWVzKSB7XHJcbiAgICAgICAgc3VwZXIoY2FyZFZpZXdCb29sSXRlbVByb3BlcnRpZXMpO1xyXG5cclxuICAgICAgICBpZiAoY2FyZFZpZXdCb29sSXRlbVByb3BlcnRpZXMudmFsdWUgIT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICB0aGlzLnZhbHVlID0gISFKU09OLnBhcnNlKGNhcmRWaWV3Qm9vbEl0ZW1Qcm9wZXJ0aWVzLnZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRpc3BsYXlWYWx1ZSgpIHtcclxuICAgICAgICBpZiAodGhpcy5pc0VtcHR5KCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZGVmYXVsdDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy52YWx1ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19