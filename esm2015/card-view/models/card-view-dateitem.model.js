/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CardViewBaseItemModel } from './card-view-baseitem.model';
import { LocalizedDatePipe } from '../../pipes/localized-date.pipe';
export class CardViewDateItemModel extends CardViewBaseItemModel {
    /**
     * @param {?} cardViewDateItemProperties
     */
    constructor(cardViewDateItemProperties) {
        super(cardViewDateItemProperties);
        this.type = 'date';
        if (cardViewDateItemProperties.format) {
            this.format = cardViewDateItemProperties.format;
        }
        if (cardViewDateItemProperties.locale) {
            this.format = cardViewDateItemProperties.locale;
        }
    }
    /**
     * @return {?}
     */
    get displayValue() {
        if (!this.value) {
            return this.default;
        }
        else {
            this.localizedDatePipe = new LocalizedDatePipe();
            return this.localizedDatePipe.transform(this.value, this.format, this.locale);
        }
    }
}
if (false) {
    /** @type {?} */
    CardViewDateItemModel.prototype.type;
    /** @type {?} */
    CardViewDateItemModel.prototype.format;
    /** @type {?} */
    CardViewDateItemModel.prototype.locale;
    /** @type {?} */
    CardViewDateItemModel.prototype.localizedDatePipe;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWRhdGVpdGVtLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiY2FyZC12aWV3L21vZGVscy9jYXJkLXZpZXctZGF0ZWl0ZW0ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFFbkUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFFcEUsTUFBTSxPQUFPLHFCQUFzQixTQUFRLHFCQUFxQjs7OztJQU81RCxZQUFZLDBCQUFzRDtRQUM5RCxLQUFLLENBQUMsMEJBQTBCLENBQUMsQ0FBQztRQVB0QyxTQUFJLEdBQVcsTUFBTSxDQUFDO1FBU2xCLElBQUksMEJBQTBCLENBQUMsTUFBTSxFQUFFO1lBQ25DLElBQUksQ0FBQyxNQUFNLEdBQUcsMEJBQTBCLENBQUMsTUFBTSxDQUFDO1NBQ25EO1FBRUQsSUFBSSwwQkFBMEIsQ0FBQyxNQUFNLEVBQUU7WUFDbkMsSUFBSSxDQUFDLE1BQU0sR0FBRywwQkFBMEIsQ0FBQyxNQUFNLENBQUM7U0FDbkQ7SUFFTCxDQUFDOzs7O0lBRUQsSUFBSSxZQUFZO1FBQ1osSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDYixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7U0FDdkI7YUFBTTtZQUNILElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLGlCQUFpQixFQUFFLENBQUM7WUFDakQsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDakY7SUFDTCxDQUFDO0NBQ0o7OztJQTNCRyxxQ0FBc0I7O0lBQ3RCLHVDQUFlOztJQUNmLHVDQUFlOztJQUVmLGtEQUFxQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDYXJkVmlld0l0ZW0gfSBmcm9tICcuLi9pbnRlcmZhY2VzL2NhcmQtdmlldy1pdGVtLmludGVyZmFjZSc7XHJcbmltcG9ydCB7IER5bmFtaWNDb21wb25lbnRNb2RlbCB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2R5bmFtaWMtY29tcG9uZW50LW1hcHBlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdCYXNlSXRlbU1vZGVsIH0gZnJvbSAnLi9jYXJkLXZpZXctYmFzZWl0ZW0ubW9kZWwnO1xyXG5pbXBvcnQgeyBDYXJkVmlld0RhdGVJdGVtUHJvcGVydGllcyB9IGZyb20gJy4uL2ludGVyZmFjZXMvY2FyZC12aWV3LmludGVyZmFjZXMnO1xyXG5pbXBvcnQgeyBMb2NhbGl6ZWREYXRlUGlwZSB9IGZyb20gJy4uLy4uL3BpcGVzL2xvY2FsaXplZC1kYXRlLnBpcGUnO1xyXG5cclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3RGF0ZUl0ZW1Nb2RlbCBleHRlbmRzIENhcmRWaWV3QmFzZUl0ZW1Nb2RlbCBpbXBsZW1lbnRzIENhcmRWaWV3SXRlbSwgRHluYW1pY0NvbXBvbmVudE1vZGVsIHtcclxuICAgIHR5cGU6IHN0cmluZyA9ICdkYXRlJztcclxuICAgIGZvcm1hdDogc3RyaW5nO1xyXG4gICAgbG9jYWxlOiBzdHJpbmc7XHJcblxyXG4gICAgbG9jYWxpemVkRGF0ZVBpcGU6IExvY2FsaXplZERhdGVQaXBlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGNhcmRWaWV3RGF0ZUl0ZW1Qcm9wZXJ0aWVzOiBDYXJkVmlld0RhdGVJdGVtUHJvcGVydGllcykge1xyXG4gICAgICAgIHN1cGVyKGNhcmRWaWV3RGF0ZUl0ZW1Qcm9wZXJ0aWVzKTtcclxuXHJcbiAgICAgICAgaWYgKGNhcmRWaWV3RGF0ZUl0ZW1Qcm9wZXJ0aWVzLmZvcm1hdCkge1xyXG4gICAgICAgICAgICB0aGlzLmZvcm1hdCA9IGNhcmRWaWV3RGF0ZUl0ZW1Qcm9wZXJ0aWVzLmZvcm1hdDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChjYXJkVmlld0RhdGVJdGVtUHJvcGVydGllcy5sb2NhbGUpIHtcclxuICAgICAgICAgICAgdGhpcy5mb3JtYXQgPSBjYXJkVmlld0RhdGVJdGVtUHJvcGVydGllcy5sb2NhbGU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBnZXQgZGlzcGxheVZhbHVlKCkge1xyXG4gICAgICAgIGlmICghdGhpcy52YWx1ZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5kZWZhdWx0O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMubG9jYWxpemVkRGF0ZVBpcGUgPSBuZXcgTG9jYWxpemVkRGF0ZVBpcGUoKTtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxpemVkRGF0ZVBpcGUudHJhbnNmb3JtKHRoaXMudmFsdWUsIHRoaXMuZm9ybWF0LCB0aGlzLmxvY2FsZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==