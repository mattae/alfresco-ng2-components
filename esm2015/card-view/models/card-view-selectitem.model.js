/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CardViewBaseItemModel } from './card-view-baseitem.model';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
/**
 * @template T
 */
export class CardViewSelectItemModel extends CardViewBaseItemModel {
    /**
     * @param {?} cardViewSelectItemProperties
     */
    constructor(cardViewSelectItemProperties) {
        super(cardViewSelectItemProperties);
        this.type = 'select';
        this.options$ = cardViewSelectItemProperties.options$;
    }
    /**
     * @return {?}
     */
    get displayValue() {
        return this.options$.pipe(switchMap((/**
         * @param {?} options
         * @return {?}
         */
        (options) => {
            /** @type {?} */
            const option = options.find((/**
             * @param {?} o
             * @return {?}
             */
            (o) => o.key === this.value));
            return of(option ? option.label : '');
        })));
    }
}
if (false) {
    /** @type {?} */
    CardViewSelectItemModel.prototype.type;
    /** @type {?} */
    CardViewSelectItemModel.prototype.options$;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LXNlbGVjdGl0ZW0ubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJjYXJkLXZpZXcvbW9kZWxzL2NhcmQtdmlldy1zZWxlY3RpdGVtLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBRW5FLE9BQU8sRUFBYyxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDdEMsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBRTNDLE1BQU0sT0FBTyx1QkFBMkIsU0FBUSxxQkFBcUI7Ozs7SUFJakUsWUFBWSw0QkFBNkQ7UUFDckUsS0FBSyxDQUFDLDRCQUE0QixDQUFDLENBQUM7UUFKeEMsU0FBSSxHQUFXLFFBQVEsQ0FBQztRQU1wQixJQUFJLENBQUMsUUFBUSxHQUFHLDRCQUE0QixDQUFDLFFBQVEsQ0FBQztJQUMxRCxDQUFDOzs7O0lBRUQsSUFBSSxZQUFZO1FBQ1osT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FDckIsU0FBUzs7OztRQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7O2tCQUNaLE1BQU0sR0FBRyxPQUFPLENBQUMsSUFBSTs7OztZQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLElBQUksQ0FBQyxLQUFLLEVBQUM7WUFDeEQsT0FBTyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUMxQyxDQUFDLEVBQUMsQ0FDTCxDQUFDO0lBQ04sQ0FBQztDQUNKOzs7SUFqQkcsdUNBQXdCOztJQUN4QiwyQ0FBb0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ2FyZFZpZXdJdGVtIH0gZnJvbSAnLi4vaW50ZXJmYWNlcy9jYXJkLXZpZXctaXRlbS5pbnRlcmZhY2UnO1xyXG5pbXBvcnQgeyBEeW5hbWljQ29tcG9uZW50TW9kZWwgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9keW5hbWljLWNvbXBvbmVudC1tYXBwZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IENhcmRWaWV3QmFzZUl0ZW1Nb2RlbCB9IGZyb20gJy4vY2FyZC12aWV3LWJhc2VpdGVtLm1vZGVsJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdTZWxlY3RJdGVtUHJvcGVydGllcywgQ2FyZFZpZXdTZWxlY3RJdGVtT3B0aW9uIH0gZnJvbSAnLi4vaW50ZXJmYWNlcy9jYXJkLXZpZXcuaW50ZXJmYWNlcyc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIG9mIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHN3aXRjaE1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbmV4cG9ydCBjbGFzcyBDYXJkVmlld1NlbGVjdEl0ZW1Nb2RlbDxUPiBleHRlbmRzIENhcmRWaWV3QmFzZUl0ZW1Nb2RlbCBpbXBsZW1lbnRzIENhcmRWaWV3SXRlbSwgRHluYW1pY0NvbXBvbmVudE1vZGVsIHtcclxuICAgIHR5cGU6IHN0cmluZyA9ICdzZWxlY3QnO1xyXG4gICAgb3B0aW9ucyQ6IE9ic2VydmFibGU8Q2FyZFZpZXdTZWxlY3RJdGVtT3B0aW9uPFQ+W10+O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGNhcmRWaWV3U2VsZWN0SXRlbVByb3BlcnRpZXM6IENhcmRWaWV3U2VsZWN0SXRlbVByb3BlcnRpZXM8VD4pIHtcclxuICAgICAgICBzdXBlcihjYXJkVmlld1NlbGVjdEl0ZW1Qcm9wZXJ0aWVzKTtcclxuXHJcbiAgICAgICAgdGhpcy5vcHRpb25zJCA9IGNhcmRWaWV3U2VsZWN0SXRlbVByb3BlcnRpZXMub3B0aW9ucyQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRpc3BsYXlWYWx1ZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5vcHRpb25zJC5waXBlKFxyXG4gICAgICAgICAgICBzd2l0Y2hNYXAoKG9wdGlvbnMpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG9wdGlvbiA9IG9wdGlvbnMuZmluZCgobykgPT4gby5rZXkgPT09IHRoaXMudmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG9mKG9wdGlvbiA/IG9wdGlvbi5sYWJlbCA6ICcnKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==