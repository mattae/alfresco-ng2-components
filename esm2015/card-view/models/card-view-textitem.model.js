/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CardViewBaseItemModel } from './card-view-baseitem.model';
export class CardViewTextItemModel extends CardViewBaseItemModel {
    /**
     * @param {?} cardViewTextItemProperties
     */
    constructor(cardViewTextItemProperties) {
        super(cardViewTextItemProperties);
        this.type = 'text';
        this.multiline = !!cardViewTextItemProperties.multiline;
        this.multivalued = !!cardViewTextItemProperties.multivalued;
        this.pipes = cardViewTextItemProperties.pipes || [];
        this.clickCallBack = cardViewTextItemProperties.clickCallBack ? cardViewTextItemProperties.clickCallBack : null;
    }
    /**
     * @return {?}
     */
    get displayValue() {
        if (this.isEmpty()) {
            return this.default;
        }
        else {
            return this.applyPipes(this.value);
        }
    }
    /**
     * @private
     * @param {?} displayValue
     * @return {?}
     */
    applyPipes(displayValue) {
        if (this.pipes.length) {
            displayValue = this.pipes.reduce((/**
             * @param {?} accumulator
             * @param {?} __1
             * @return {?}
             */
            (accumulator, { pipe, params = [] }) => {
                return pipe.transform(accumulator, ...params);
            }), displayValue);
        }
        return displayValue;
    }
}
if (false) {
    /** @type {?} */
    CardViewTextItemModel.prototype.type;
    /** @type {?} */
    CardViewTextItemModel.prototype.multiline;
    /** @type {?} */
    CardViewTextItemModel.prototype.multivalued;
    /** @type {?} */
    CardViewTextItemModel.prototype.pipes;
    /** @type {?} */
    CardViewTextItemModel.prototype.clickCallBack;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LXRleHRpdGVtLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiY2FyZC12aWV3L21vZGVscy9jYXJkLXZpZXctdGV4dGl0ZW0ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFHbkUsTUFBTSxPQUFPLHFCQUFzQixTQUFRLHFCQUFxQjs7OztJQU81RCxZQUFZLDBCQUFzRDtRQUM5RCxLQUFLLENBQUMsMEJBQTBCLENBQUMsQ0FBQztRQVB0QyxTQUFJLEdBQVcsTUFBTSxDQUFDO1FBUWxCLElBQUksQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLDBCQUEwQixDQUFDLFNBQVMsQ0FBQztRQUN4RCxJQUFJLENBQUMsV0FBVyxHQUFHLENBQUMsQ0FBQywwQkFBMEIsQ0FBQyxXQUFXLENBQUM7UUFDNUQsSUFBSSxDQUFDLEtBQUssR0FBRywwQkFBMEIsQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDO1FBQ3BELElBQUksQ0FBQyxhQUFhLEdBQUcsMEJBQTBCLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQywwQkFBMEIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUNwSCxDQUFDOzs7O0lBRUQsSUFBSSxZQUFZO1FBQ1osSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUU7WUFDaEIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO1NBQ3ZCO2FBQU07WUFDSCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3RDO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sVUFBVSxDQUFDLFlBQVk7UUFDM0IsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNuQixZQUFZLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNOzs7OztZQUFDLENBQUMsV0FBVyxFQUFFLEVBQUUsSUFBSSxFQUFFLE1BQU0sR0FBRyxFQUFFLEVBQUUsRUFBRSxFQUFFO2dCQUNwRSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLEdBQUcsTUFBTSxDQUFDLENBQUM7WUFDbEQsQ0FBQyxHQUFFLFlBQVksQ0FBQyxDQUFDO1NBQ3BCO1FBRUQsT0FBTyxZQUFZLENBQUM7SUFDeEIsQ0FBQztDQUNKOzs7SUEvQkcscUNBQXNCOztJQUN0QiwwQ0FBb0I7O0lBQ3BCLDRDQUFzQjs7SUFDdEIsc0NBQXVDOztJQUN2Qyw4Q0FBb0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ2FyZFZpZXdJdGVtIH0gZnJvbSAnLi4vaW50ZXJmYWNlcy9jYXJkLXZpZXctaXRlbS5pbnRlcmZhY2UnO1xyXG5pbXBvcnQgeyBEeW5hbWljQ29tcG9uZW50TW9kZWwgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9keW5hbWljLWNvbXBvbmVudC1tYXBwZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IENhcmRWaWV3QmFzZUl0ZW1Nb2RlbCB9IGZyb20gJy4vY2FyZC12aWV3LWJhc2VpdGVtLm1vZGVsJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdUZXh0SXRlbVBpcGVQcm9wZXJ0eSwgQ2FyZFZpZXdUZXh0SXRlbVByb3BlcnRpZXMgfSBmcm9tICcuLi9pbnRlcmZhY2VzL2NhcmQtdmlldy5pbnRlcmZhY2VzJztcclxuXHJcbmV4cG9ydCBjbGFzcyBDYXJkVmlld1RleHRJdGVtTW9kZWwgZXh0ZW5kcyBDYXJkVmlld0Jhc2VJdGVtTW9kZWwgaW1wbGVtZW50cyBDYXJkVmlld0l0ZW0sIER5bmFtaWNDb21wb25lbnRNb2RlbCB7XHJcbiAgICB0eXBlOiBzdHJpbmcgPSAndGV4dCc7XHJcbiAgICBtdWx0aWxpbmU/OiBib29sZWFuO1xyXG4gICAgbXVsdGl2YWx1ZWQ/OiBib29sZWFuO1xyXG4gICAgcGlwZXM/OiBDYXJkVmlld1RleHRJdGVtUGlwZVByb3BlcnR5W107XHJcbiAgICBjbGlja0NhbGxCYWNrPzogYW55O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGNhcmRWaWV3VGV4dEl0ZW1Qcm9wZXJ0aWVzOiBDYXJkVmlld1RleHRJdGVtUHJvcGVydGllcykge1xyXG4gICAgICAgIHN1cGVyKGNhcmRWaWV3VGV4dEl0ZW1Qcm9wZXJ0aWVzKTtcclxuICAgICAgICB0aGlzLm11bHRpbGluZSA9ICEhY2FyZFZpZXdUZXh0SXRlbVByb3BlcnRpZXMubXVsdGlsaW5lO1xyXG4gICAgICAgIHRoaXMubXVsdGl2YWx1ZWQgPSAhIWNhcmRWaWV3VGV4dEl0ZW1Qcm9wZXJ0aWVzLm11bHRpdmFsdWVkO1xyXG4gICAgICAgIHRoaXMucGlwZXMgPSBjYXJkVmlld1RleHRJdGVtUHJvcGVydGllcy5waXBlcyB8fCBbXTtcclxuICAgICAgICB0aGlzLmNsaWNrQ2FsbEJhY2sgPSBjYXJkVmlld1RleHRJdGVtUHJvcGVydGllcy5jbGlja0NhbGxCYWNrID8gY2FyZFZpZXdUZXh0SXRlbVByb3BlcnRpZXMuY2xpY2tDYWxsQmFjayA6IG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRpc3BsYXlWYWx1ZSgpIHtcclxuICAgICAgICBpZiAodGhpcy5pc0VtcHR5KCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZGVmYXVsdDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5hcHBseVBpcGVzKHRoaXMudmFsdWUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGFwcGx5UGlwZXMoZGlzcGxheVZhbHVlKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucGlwZXMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIGRpc3BsYXlWYWx1ZSA9IHRoaXMucGlwZXMucmVkdWNlKChhY2N1bXVsYXRvciwgeyBwaXBlLCBwYXJhbXMgPSBbXSB9KSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcGlwZS50cmFuc2Zvcm0oYWNjdW11bGF0b3IsIC4uLnBhcmFtcyk7XHJcbiAgICAgICAgICAgIH0sIGRpc3BsYXlWYWx1ZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZGlzcGxheVZhbHVlO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==