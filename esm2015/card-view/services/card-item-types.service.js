/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { CardViewDateItemComponent } from '../components/card-view-dateitem/card-view-dateitem.component';
import { CardViewMapItemComponent } from '../components/card-view-mapitem/card-view-mapitem.component';
import { CardViewTextItemComponent } from '../components/card-view-textitem/card-view-textitem.component';
import { CardViewSelectItemComponent } from '../components/card-view-selectitem/card-view-selectitem.component';
import { CardViewBoolItemComponent } from '../components/card-view-boolitem/card-view-boolitem.component';
import { CardViewKeyValuePairsItemComponent } from '../components/card-view-keyvaluepairsitem/card-view-keyvaluepairsitem.component';
import { DynamicComponentMapper, DynamicComponentResolver } from '../../services/dynamic-component-mapper.service';
import * as i0 from "@angular/core";
export class CardItemTypeService extends DynamicComponentMapper {
    constructor() {
        super(...arguments);
        this.defaultValue = CardViewTextItemComponent;
        this.types = {
            'text': DynamicComponentResolver.fromType(CardViewTextItemComponent),
            'select': DynamicComponentResolver.fromType(CardViewSelectItemComponent),
            'int': DynamicComponentResolver.fromType(CardViewTextItemComponent),
            'float': DynamicComponentResolver.fromType(CardViewTextItemComponent),
            'date': DynamicComponentResolver.fromType(CardViewDateItemComponent),
            'datetime': DynamicComponentResolver.fromType(CardViewDateItemComponent),
            'bool': DynamicComponentResolver.fromType(CardViewBoolItemComponent),
            'map': DynamicComponentResolver.fromType(CardViewMapItemComponent),
            'keyvaluepairs': DynamicComponentResolver.fromType(CardViewKeyValuePairsItemComponent)
        };
    }
}
CardItemTypeService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */ CardItemTypeService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function CardItemTypeService_Factory() { return new CardItemTypeService(); }, token: CardItemTypeService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @protected
     */
    CardItemTypeService.prototype.defaultValue;
    /**
     * @type {?}
     * @protected
     */
    CardItemTypeService.prototype.types;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC1pdGVtLXR5cGVzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJjYXJkLXZpZXcvc2VydmljZXMvY2FyZC1pdGVtLXR5cGVzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBUSxNQUFNLGVBQWUsQ0FBQztBQUNqRCxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSwrREFBK0QsQ0FBQztBQUMxRyxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSw2REFBNkQsQ0FBQztBQUN2RyxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSwrREFBK0QsQ0FBQztBQUMxRyxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSxtRUFBbUUsQ0FBQztBQUNoSCxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSwrREFBK0QsQ0FBQztBQUMxRyxPQUFPLEVBQUUsa0NBQWtDLEVBQUUsTUFBTSxpRkFBaUYsQ0FBQztBQUNySSxPQUFPLEVBQUUsc0JBQXNCLEVBQW1DLHdCQUF3QixFQUFFLE1BQU0saURBQWlELENBQUM7O0FBS3BKLE1BQU0sT0FBTyxtQkFBb0IsU0FBUSxzQkFBc0I7SUFIL0Q7O1FBS2MsaUJBQVksR0FBYSx5QkFBeUIsQ0FBQztRQUVuRCxVQUFLLEdBQXVEO1lBQ2xFLE1BQU0sRUFBRSx3QkFBd0IsQ0FBQyxRQUFRLENBQUMseUJBQXlCLENBQUM7WUFDcEUsUUFBUSxFQUFFLHdCQUF3QixDQUFDLFFBQVEsQ0FBQywyQkFBMkIsQ0FBQztZQUN4RSxLQUFLLEVBQUUsd0JBQXdCLENBQUMsUUFBUSxDQUFDLHlCQUF5QixDQUFDO1lBQ25FLE9BQU8sRUFBRSx3QkFBd0IsQ0FBQyxRQUFRLENBQUMseUJBQXlCLENBQUM7WUFDckUsTUFBTSxFQUFFLHdCQUF3QixDQUFDLFFBQVEsQ0FBQyx5QkFBeUIsQ0FBQztZQUNwRSxVQUFVLEVBQUUsd0JBQXdCLENBQUMsUUFBUSxDQUFDLHlCQUF5QixDQUFDO1lBQ3hFLE1BQU0sRUFBRSx3QkFBd0IsQ0FBQyxRQUFRLENBQUMseUJBQXlCLENBQUM7WUFDcEUsS0FBSyxFQUFFLHdCQUF3QixDQUFDLFFBQVEsQ0FBQyx3QkFBd0IsQ0FBQztZQUNsRSxlQUFlLEVBQUUsd0JBQXdCLENBQUMsUUFBUSxDQUFDLGtDQUFrQyxDQUFDO1NBQ3pGLENBQUM7S0FDTDs7O1lBbEJBLFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQjs7Ozs7Ozs7SUFHRywyQ0FBNkQ7Ozs7O0lBRTdELG9DQVVFIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUsIFR5cGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdEYXRlSXRlbUNvbXBvbmVudCB9IGZyb20gJy4uL2NvbXBvbmVudHMvY2FyZC12aWV3LWRhdGVpdGVtL2NhcmQtdmlldy1kYXRlaXRlbS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDYXJkVmlld01hcEl0ZW1Db21wb25lbnQgfSBmcm9tICcuLi9jb21wb25lbnRzL2NhcmQtdmlldy1tYXBpdGVtL2NhcmQtdmlldy1tYXBpdGVtLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENhcmRWaWV3VGV4dEl0ZW1Db21wb25lbnQgfSBmcm9tICcuLi9jb21wb25lbnRzL2NhcmQtdmlldy10ZXh0aXRlbS9jYXJkLXZpZXctdGV4dGl0ZW0uY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ2FyZFZpZXdTZWxlY3RJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi4vY29tcG9uZW50cy9jYXJkLXZpZXctc2VsZWN0aXRlbS9jYXJkLXZpZXctc2VsZWN0aXRlbS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDYXJkVmlld0Jvb2xJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi4vY29tcG9uZW50cy9jYXJkLXZpZXctYm9vbGl0ZW0vY2FyZC12aWV3LWJvb2xpdGVtLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENhcmRWaWV3S2V5VmFsdWVQYWlyc0l0ZW1Db21wb25lbnQgfSBmcm9tICcuLi9jb21wb25lbnRzL2NhcmQtdmlldy1rZXl2YWx1ZXBhaXJzaXRlbS9jYXJkLXZpZXcta2V5dmFsdWVwYWlyc2l0ZW0uY29tcG9uZW50JztcclxuaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudE1hcHBlciwgRHluYW1pY0NvbXBvbmVudFJlc29sdmVGdW5jdGlvbiwgRHluYW1pY0NvbXBvbmVudFJlc29sdmVyIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvZHluYW1pYy1jb21wb25lbnQtbWFwcGVyLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDYXJkSXRlbVR5cGVTZXJ2aWNlIGV4dGVuZHMgRHluYW1pY0NvbXBvbmVudE1hcHBlciB7XHJcblxyXG4gICAgcHJvdGVjdGVkIGRlZmF1bHRWYWx1ZTogVHlwZTx7fT4gPSBDYXJkVmlld1RleHRJdGVtQ29tcG9uZW50O1xyXG5cclxuICAgIHByb3RlY3RlZCB0eXBlczogeyBba2V5OiBzdHJpbmddOiBEeW5hbWljQ29tcG9uZW50UmVzb2x2ZUZ1bmN0aW9uIH0gPSB7XHJcbiAgICAgICAgJ3RleHQnOiBEeW5hbWljQ29tcG9uZW50UmVzb2x2ZXIuZnJvbVR5cGUoQ2FyZFZpZXdUZXh0SXRlbUNvbXBvbmVudCksXHJcbiAgICAgICAgJ3NlbGVjdCc6IER5bmFtaWNDb21wb25lbnRSZXNvbHZlci5mcm9tVHlwZShDYXJkVmlld1NlbGVjdEl0ZW1Db21wb25lbnQpLFxyXG4gICAgICAgICdpbnQnOiBEeW5hbWljQ29tcG9uZW50UmVzb2x2ZXIuZnJvbVR5cGUoQ2FyZFZpZXdUZXh0SXRlbUNvbXBvbmVudCksXHJcbiAgICAgICAgJ2Zsb2F0JzogRHluYW1pY0NvbXBvbmVudFJlc29sdmVyLmZyb21UeXBlKENhcmRWaWV3VGV4dEl0ZW1Db21wb25lbnQpLFxyXG4gICAgICAgICdkYXRlJzogRHluYW1pY0NvbXBvbmVudFJlc29sdmVyLmZyb21UeXBlKENhcmRWaWV3RGF0ZUl0ZW1Db21wb25lbnQpLFxyXG4gICAgICAgICdkYXRldGltZSc6IER5bmFtaWNDb21wb25lbnRSZXNvbHZlci5mcm9tVHlwZShDYXJkVmlld0RhdGVJdGVtQ29tcG9uZW50KSxcclxuICAgICAgICAnYm9vbCc6IER5bmFtaWNDb21wb25lbnRSZXNvbHZlci5mcm9tVHlwZShDYXJkVmlld0Jvb2xJdGVtQ29tcG9uZW50KSxcclxuICAgICAgICAnbWFwJzogRHluYW1pY0NvbXBvbmVudFJlc29sdmVyLmZyb21UeXBlKENhcmRWaWV3TWFwSXRlbUNvbXBvbmVudCksXHJcbiAgICAgICAgJ2tleXZhbHVlcGFpcnMnOiBEeW5hbWljQ29tcG9uZW50UmVzb2x2ZXIuZnJvbVR5cGUoQ2FyZFZpZXdLZXlWYWx1ZVBhaXJzSXRlbUNvbXBvbmVudClcclxuICAgIH07XHJcbn1cclxuIl19