/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import * as i0 from "@angular/core";
/**
 * @record
 */
export function UpdateNotification() { }
if (false) {
    /** @type {?} */
    UpdateNotification.prototype.target;
    /** @type {?} */
    UpdateNotification.prototype.changed;
}
/**
 * @record
 */
export function ClickNotification() { }
if (false) {
    /** @type {?} */
    ClickNotification.prototype.target;
}
/**
 * @param {?} key
 * @param {?} value
 * @return {?}
 */
export function transformKeyToObject(key, value) {
    /** @type {?} */
    const objectLevels = key.split('.').reverse();
    return objectLevels.reduce((/**
     * @param {?} previousValue
     * @param {?} currentValue
     * @return {?}
     */
    (previousValue, currentValue) => {
        return { [currentValue]: previousValue };
    }), value);
}
export class CardViewUpdateService {
    constructor() {
        this.itemUpdated$ = new Subject();
        this.itemClicked$ = new Subject();
    }
    /**
     * @param {?} property
     * @param {?} newValue
     * @return {?}
     */
    update(property, newValue) {
        this.itemUpdated$.next({
            target: property,
            changed: transformKeyToObject(property.key, newValue)
        });
    }
    /**
     * @param {?} property
     * @return {?}
     */
    clicked(property) {
        this.itemClicked$.next({
            target: property
        });
    }
}
CardViewUpdateService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */ CardViewUpdateService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function CardViewUpdateService_Factory() { return new CardViewUpdateService(); }, token: CardViewUpdateService, providedIn: "root" });
if (false) {
    /** @type {?} */
    CardViewUpdateService.prototype.itemUpdated$;
    /** @type {?} */
    CardViewUpdateService.prototype.itemClicked$;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LXVwZGF0ZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiY2FyZC12aWV3L3NlcnZpY2VzL2NhcmQtdmlldy11cGRhdGUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7Ozs7O0FBRy9CLHdDQUdDOzs7SUFGRyxvQ0FBWTs7SUFDWixxQ0FBYTs7Ozs7QUFHakIsdUNBRUM7OztJQURHLG1DQUFZOzs7Ozs7O0FBR2hCLE1BQU0sVUFBVSxvQkFBb0IsQ0FBQyxHQUFXLEVBQUUsS0FBSzs7VUFDN0MsWUFBWSxHQUFhLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsT0FBTyxFQUFFO0lBRXZELE9BQU8sWUFBWSxDQUFDLE1BQU07Ozs7O0lBQUssQ0FBQyxhQUFhLEVBQUUsWUFBWSxFQUFFLEVBQUU7UUFDM0QsT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDLEVBQUUsYUFBYSxFQUFDLENBQUM7SUFDNUMsQ0FBQyxHQUFFLEtBQUssQ0FBQyxDQUFDO0FBQ2QsQ0FBQztBQUtELE1BQU0sT0FBTyxxQkFBcUI7SUFIbEM7UUFLSSxpQkFBWSxHQUFHLElBQUksT0FBTyxFQUFzQixDQUFDO1FBQ2pELGlCQUFZLEdBQUcsSUFBSSxPQUFPLEVBQXFCLENBQUM7S0FjbkQ7Ozs7OztJQVpHLE1BQU0sQ0FBQyxRQUErQixFQUFFLFFBQWE7UUFDakQsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUM7WUFDbkIsTUFBTSxFQUFFLFFBQVE7WUFDaEIsT0FBTyxFQUFFLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDO1NBQ3hELENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsT0FBTyxDQUFDLFFBQStCO1FBQ25DLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO1lBQ25CLE1BQU0sRUFBRSxRQUFRO1NBQ25CLENBQUMsQ0FBQztJQUNQLENBQUM7OztZQW5CSixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7O0lBR0csNkNBQWlEOztJQUNqRCw2Q0FBZ0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IENhcmRWaWV3QmFzZUl0ZW1Nb2RlbCB9IGZyb20gJy4uL21vZGVscy9jYXJkLXZpZXctYmFzZWl0ZW0ubW9kZWwnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBVcGRhdGVOb3RpZmljYXRpb24ge1xyXG4gICAgdGFyZ2V0OiBhbnk7XHJcbiAgICBjaGFuZ2VkOiBhbnk7XHJcbn1cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgQ2xpY2tOb3RpZmljYXRpb24ge1xyXG4gICAgdGFyZ2V0OiBhbnk7XHJcbn1cclxuXHJcbmV4cG9ydCBmdW5jdGlvbiB0cmFuc2Zvcm1LZXlUb09iamVjdChrZXk6IHN0cmluZywgdmFsdWUpOiBPYmplY3Qge1xyXG4gICAgY29uc3Qgb2JqZWN0TGV2ZWxzOiBzdHJpbmdbXSA9IGtleS5zcGxpdCgnLicpLnJldmVyc2UoKTtcclxuXHJcbiAgICByZXR1cm4gb2JqZWN0TGV2ZWxzLnJlZHVjZTx7fT4oKHByZXZpb3VzVmFsdWUsIGN1cnJlbnRWYWx1ZSkgPT4ge1xyXG4gICAgICAgIHJldHVybiB7IFtjdXJyZW50VmFsdWVdOiBwcmV2aW91c1ZhbHVlfTtcclxuICAgIH0sIHZhbHVlKTtcclxufVxyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDYXJkVmlld1VwZGF0ZVNlcnZpY2Uge1xyXG5cclxuICAgIGl0ZW1VcGRhdGVkJCA9IG5ldyBTdWJqZWN0PFVwZGF0ZU5vdGlmaWNhdGlvbj4oKTtcclxuICAgIGl0ZW1DbGlja2VkJCA9IG5ldyBTdWJqZWN0PENsaWNrTm90aWZpY2F0aW9uPigpO1xyXG5cclxuICAgIHVwZGF0ZShwcm9wZXJ0eTogQ2FyZFZpZXdCYXNlSXRlbU1vZGVsLCBuZXdWYWx1ZTogYW55KSB7XHJcbiAgICAgICAgdGhpcy5pdGVtVXBkYXRlZCQubmV4dCh7XHJcbiAgICAgICAgICAgIHRhcmdldDogcHJvcGVydHksXHJcbiAgICAgICAgICAgIGNoYW5nZWQ6IHRyYW5zZm9ybUtleVRvT2JqZWN0KHByb3BlcnR5LmtleSwgbmV3VmFsdWUpXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2xpY2tlZChwcm9wZXJ0eTogQ2FyZFZpZXdCYXNlSXRlbU1vZGVsKSB7XHJcbiAgICAgICAgdGhpcy5pdGVtQ2xpY2tlZCQubmV4dCh7XHJcbiAgICAgICAgICAgIHRhcmdldDogcHJvcGVydHlcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=