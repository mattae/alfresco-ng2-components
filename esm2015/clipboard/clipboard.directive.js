/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Directive, Input, HostListener, Component, ViewContainerRef, ComponentFactoryResolver, ViewEncapsulation } from '@angular/core';
import { ClipboardService } from './clipboard.service';
export class ClipboardDirective {
    /**
     * @param {?} clipboardService
     * @param {?} viewContainerRef
     * @param {?} resolver
     */
    constructor(clipboardService, viewContainerRef, resolver) {
        this.clipboardService = clipboardService;
        this.viewContainerRef = viewContainerRef;
        this.resolver = resolver;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    handleClickEvent(event) {
        event.preventDefault();
        event.stopPropagation();
        this.copyToClipboard();
    }
    /**
     * @return {?}
     */
    showTooltip() {
        if (this.placeholder) {
            /** @type {?} */
            const componentFactory = this.resolver.resolveComponentFactory(ClipboardComponent);
            /** @type {?} */
            const componentRef = this.viewContainerRef.createComponent(componentFactory).instance;
            componentRef.placeholder = this.placeholder;
        }
    }
    /**
     * @return {?}
     */
    closeTooltip() {
        this.viewContainerRef.remove();
    }
    /**
     * @private
     * @return {?}
     */
    copyToClipboard() {
        /** @type {?} */
        const isValidTarget = this.clipboardService.isTargetValid(this.target);
        if (isValidTarget) {
            this.clipboardService.copyToClipboard(this.target, this.message);
        }
        else {
            this.copyContentToClipboard(this.viewContainerRef.element.nativeElement.innerHTML);
        }
    }
    /**
     * @private
     * @param {?} content
     * @return {?}
     */
    copyContentToClipboard(content) {
        this.clipboardService.copyContentToClipboard(content, this.message);
    }
}
ClipboardDirective.decorators = [
    { type: Directive, args: [{
                selector: '[adf-clipboard]',
                exportAs: 'adfClipboard'
            },] }
];
/** @nocollapse */
ClipboardDirective.ctorParameters = () => [
    { type: ClipboardService },
    { type: ViewContainerRef },
    { type: ComponentFactoryResolver }
];
ClipboardDirective.propDecorators = {
    placeholder: [{ type: Input, args: ['adf-clipboard',] }],
    target: [{ type: Input }],
    message: [{ type: Input, args: ['clipboard-notification',] }],
    handleClickEvent: [{ type: HostListener, args: ['click', ['$event'],] }],
    showTooltip: [{ type: HostListener, args: ['mouseenter',] }],
    closeTooltip: [{ type: HostListener, args: ['mouseleave',] }]
};
if (false) {
    /**
     * Translation key or message for the tooltip.
     * @type {?}
     */
    ClipboardDirective.prototype.placeholder;
    /**
     * Reference to the HTML element containing the text to copy.
     * @type {?}
     */
    ClipboardDirective.prototype.target;
    /**
     * Translation key or message for snackbar notification.
     * @type {?}
     */
    ClipboardDirective.prototype.message;
    /**
     * @type {?}
     * @private
     */
    ClipboardDirective.prototype.clipboardService;
    /** @type {?} */
    ClipboardDirective.prototype.viewContainerRef;
    /**
     * @type {?}
     * @private
     */
    ClipboardDirective.prototype.resolver;
}
export class ClipboardComponent {
    /**
     * @return {?}
     */
    ngOnInit() {
        this.placeholder = this.placeholder || 'CLIPBOARD.CLICK_TO_COPY';
    }
}
ClipboardComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-copy-content-tooltip',
                template: `
        <span class='adf-copy-tooltip'>{{ placeholder | translate }} </span>
        `,
                encapsulation: ViewEncapsulation.None,
                styles: [""]
            }] }
];
if (false) {
    /** @type {?} */
    ClipboardComponent.prototype.placeholder;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2xpcGJvYXJkLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNsaXBib2FyZC9jbGlwYm9hcmQuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLEVBQUUsd0JBQXdCLEVBQUUsaUJBQWlCLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDakosT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFNdkQsTUFBTSxPQUFPLGtCQUFrQjs7Ozs7O0lBYzNCLFlBQW9CLGdCQUFrQyxFQUNuQyxnQkFBa0MsRUFDakMsUUFBa0M7UUFGbEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNuQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2pDLGFBQVEsR0FBUixRQUFRLENBQTBCO0lBQUcsQ0FBQzs7Ozs7SUFHMUQsZ0JBQWdCLENBQUMsS0FBaUI7UUFDOUIsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3ZCLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDM0IsQ0FBQzs7OztJQUdELFdBQVc7UUFDUCxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7O2tCQUNaLGdCQUFnQixHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsdUJBQXVCLENBQUMsa0JBQWtCLENBQUM7O2tCQUM1RSxZQUFZLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDLFFBQVE7WUFDckYsWUFBWSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1NBQy9DO0lBQ0wsQ0FBQzs7OztJQUdELFlBQVk7UUFDUixJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDbkMsQ0FBQzs7Ozs7SUFFTyxlQUFlOztjQUNiLGFBQWEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUM7UUFFdEUsSUFBSSxhQUFhLEVBQUU7WUFDZixJQUFJLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ3BFO2FBQU07WUFDSCxJQUFJLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsU0FBUyxDQUFDLENBQUM7U0FDdEY7SUFDTCxDQUFDOzs7Ozs7SUFFTyxzQkFBc0IsQ0FBQyxPQUFPO1FBQ2xDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxzQkFBc0IsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3hFLENBQUM7OztZQXZESixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGlCQUFpQjtnQkFDM0IsUUFBUSxFQUFFLGNBQWM7YUFDM0I7Ozs7WUFMUSxnQkFBZ0I7WUFEMkIsZ0JBQWdCO1lBQUUsd0JBQXdCOzs7MEJBVXpGLEtBQUssU0FBQyxlQUFlO3FCQUlyQixLQUFLO3NCQUtMLEtBQUssU0FBQyx3QkFBd0I7K0JBTTlCLFlBQVksU0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUM7MEJBT2hDLFlBQVksU0FBQyxZQUFZOzJCQVN6QixZQUFZLFNBQUMsWUFBWTs7Ozs7OztJQS9CMUIseUNBQ29COzs7OztJQUdwQixvQ0FDK0M7Ozs7O0lBSS9DLHFDQUFpRDs7Ozs7SUFFckMsOENBQTBDOztJQUMxQyw4Q0FBeUM7Ozs7O0lBQ3pDLHNDQUEwQzs7QUE4QzFELE1BQU0sT0FBTyxrQkFBa0I7Ozs7SUFHM0IsUUFBUTtRQUNKLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsSUFBSSx5QkFBeUIsQ0FBQztJQUNyRSxDQUFDOzs7WUFiSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLDBCQUEwQjtnQkFDcEMsUUFBUSxFQUFFOztTQUVMO2dCQUVMLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOzthQUN4Qzs7OztJQUVHLHlDQUFvQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBEaXJlY3RpdmUsIElucHV0LCBIb3N0TGlzdGVuZXIsIENvbXBvbmVudCwgVmlld0NvbnRhaW5lclJlZiwgQ29tcG9uZW50RmFjdG9yeVJlc29sdmVyLCBWaWV3RW5jYXBzdWxhdGlvbiwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENsaXBib2FyZFNlcnZpY2UgfSBmcm9tICcuL2NsaXBib2FyZC5zZXJ2aWNlJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbYWRmLWNsaXBib2FyZF0nLFxyXG4gICAgZXhwb3J0QXM6ICdhZGZDbGlwYm9hcmQnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDbGlwYm9hcmREaXJlY3RpdmUge1xyXG4gICAgLyoqIFRyYW5zbGF0aW9uIGtleSBvciBtZXNzYWdlIGZvciB0aGUgdG9vbHRpcC4gKi9cclxuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpuby1pbnB1dC1yZW5hbWVcclxuICAgIEBJbnB1dCgnYWRmLWNsaXBib2FyZCcpXHJcbiAgICBwbGFjZWhvbGRlcjogc3RyaW5nO1xyXG5cclxuICAgIC8qKiBSZWZlcmVuY2UgdG8gdGhlIEhUTUwgZWxlbWVudCBjb250YWluaW5nIHRoZSB0ZXh0IHRvIGNvcHkuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgdGFyZ2V0OiBIVE1MSW5wdXRFbGVtZW50IHwgSFRNTFRleHRBcmVhRWxlbWVudDtcclxuXHJcbiAgICAvKiogVHJhbnNsYXRpb24ga2V5IG9yIG1lc3NhZ2UgZm9yIHNuYWNrYmFyIG5vdGlmaWNhdGlvbi4gKi9cclxuICAgIC8vIHRzbGludDpkaXNhYmxlLW5leHQtbGluZTpuby1pbnB1dC1yZW5hbWVcclxuICAgIEBJbnB1dCgnY2xpcGJvYXJkLW5vdGlmaWNhdGlvbicpIG1lc3NhZ2U6IHN0cmluZztcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNsaXBib2FyZFNlcnZpY2U6IENsaXBib2FyZFNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgdmlld0NvbnRhaW5lclJlZjogVmlld0NvbnRhaW5lclJlZixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgcmVzb2x2ZXI6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlcikge31cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdjbGljaycsIFsnJGV2ZW50J10pXHJcbiAgICBoYW5kbGVDbGlja0V2ZW50KGV2ZW50OiBNb3VzZUV2ZW50KSB7XHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICB0aGlzLmNvcHlUb0NsaXBib2FyZCgpO1xyXG4gICAgfVxyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ21vdXNlZW50ZXInKVxyXG4gICAgc2hvd1Rvb2x0aXAoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucGxhY2Vob2xkZXIpIHtcclxuICAgICAgICAgICAgY29uc3QgY29tcG9uZW50RmFjdG9yeSA9IHRoaXMucmVzb2x2ZXIucmVzb2x2ZUNvbXBvbmVudEZhY3RvcnkoQ2xpcGJvYXJkQ29tcG9uZW50KTtcclxuICAgICAgICAgICAgY29uc3QgY29tcG9uZW50UmVmID0gdGhpcy52aWV3Q29udGFpbmVyUmVmLmNyZWF0ZUNvbXBvbmVudChjb21wb25lbnRGYWN0b3J5KS5pbnN0YW5jZTtcclxuICAgICAgICAgICAgY29tcG9uZW50UmVmLnBsYWNlaG9sZGVyID0gdGhpcy5wbGFjZWhvbGRlcjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignbW91c2VsZWF2ZScpXHJcbiAgICBjbG9zZVRvb2x0aXAoKSB7XHJcbiAgICAgICAgdGhpcy52aWV3Q29udGFpbmVyUmVmLnJlbW92ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgY29weVRvQ2xpcGJvYXJkKCkge1xyXG4gICAgICAgIGNvbnN0IGlzVmFsaWRUYXJnZXQgPSB0aGlzLmNsaXBib2FyZFNlcnZpY2UuaXNUYXJnZXRWYWxpZCh0aGlzLnRhcmdldCk7XHJcblxyXG4gICAgICAgIGlmIChpc1ZhbGlkVGFyZ2V0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2xpcGJvYXJkU2VydmljZS5jb3B5VG9DbGlwYm9hcmQodGhpcy50YXJnZXQsIHRoaXMubWVzc2FnZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5jb3B5Q29udGVudFRvQ2xpcGJvYXJkKHRoaXMudmlld0NvbnRhaW5lclJlZi5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQuaW5uZXJIVE1MKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBjb3B5Q29udGVudFRvQ2xpcGJvYXJkKGNvbnRlbnQpIHtcclxuICAgICAgICB0aGlzLmNsaXBib2FyZFNlcnZpY2UuY29weUNvbnRlbnRUb0NsaXBib2FyZChjb250ZW50LCB0aGlzLm1lc3NhZ2UpO1xyXG4gICAgfVxyXG59XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLWNvcHktY29udGVudC10b29sdGlwJyxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgPHNwYW4gY2xhc3M9J2FkZi1jb3B5LXRvb2x0aXAnPnt7IHBsYWNlaG9sZGVyIHwgdHJhbnNsYXRlIH19IDwvc3Bhbj5cclxuICAgICAgICBgLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vY2xpcGJvYXJkLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDbGlwYm9hcmRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgcGxhY2Vob2xkZXI6IHN0cmluZztcclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLnBsYWNlaG9sZGVyID0gdGhpcy5wbGFjZWhvbGRlciB8fCAnQ0xJUEJPQVJELkNMSUNLX1RPX0NPUFknO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==