/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { EcmUserService } from '../userinfo/services/ecm-user.service';
import { PeopleProcessService } from '../services/people-process.service';
import { UserPreferencesService, UserPreferenceValues } from '../services/user-preferences.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
export class CommentListComponent {
    /**
     * @param {?} peopleProcessService
     * @param {?} ecmUserService
     * @param {?} userPreferenceService
     */
    constructor(peopleProcessService, ecmUserService, userPreferenceService) {
        this.peopleProcessService = peopleProcessService;
        this.ecmUserService = ecmUserService;
        this.userPreferenceService = userPreferenceService;
        /**
         * Emitted when the user clicks on one of the comment rows.
         */
        this.clickRow = new EventEmitter();
        this.onDestroy$ = new Subject();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.userPreferenceService
            .select(UserPreferenceValues.Locale)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} locale
         * @return {?}
         */
        locale => this.currentLocale = locale));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    }
    /**
     * @param {?} comment
     * @return {?}
     */
    selectComment(comment) {
        if (this.selectedComment) {
            this.selectedComment.isSelected = false;
        }
        comment.isSelected = true;
        this.selectedComment = comment;
        this.clickRow.emit(this.selectedComment);
    }
    /**
     * @param {?} user
     * @return {?}
     */
    getUserShortName(user) {
        /** @type {?} */
        let shortName = '';
        if (user) {
            if (user.firstName) {
                shortName = user.firstName[0].toUpperCase();
            }
            if (user.lastName) {
                shortName += user.lastName[0].toUpperCase();
            }
        }
        return shortName;
    }
    /**
     * @param {?} user
     * @return {?}
     */
    isPictureDefined(user) {
        return user.pictureId || user.avatarId;
    }
    /**
     * @param {?} user
     * @return {?}
     */
    getUserImage(user) {
        if (this.isAContentUsers(user)) {
            return this.ecmUserService.getUserProfileImage(user.avatarId);
        }
        else {
            return this.peopleProcessService.getUserImage(user);
        }
    }
    /**
     * @private
     * @param {?} user
     * @return {?}
     */
    isAContentUsers(user) {
        return user.avatarId;
    }
}
CommentListComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-comment-list',
                template: " <mat-list class=\"adf-comment-list\">\r\n    <mat-list-item *ngFor=\"let comment of comments\"\r\n                  (click)=\"selectComment(comment)\"\r\n                  class=\"adf-comment-list-item\"\r\n                  [class.adf-is-selected]=\"comment.isSelected\"\r\n                  id=\"adf-comment-{{comment?.id}}\">\r\n        <div id=\"comment-user-icon\" class=\"adf-comment-img-container\">\r\n            <div\r\n                *ngIf=\"!isPictureDefined(comment.createdBy)\"\r\n                class=\"adf-comment-user-icon\">\r\n                {{getUserShortName(comment.createdBy)}}\r\n            </div>\r\n            <div>\r\n                <img [alt]=\"comment.createdBy\" *ngIf=\"isPictureDefined(comment.createdBy)\"\r\n                      class=\"adf-people-img\"\r\n                     [src]=\"getUserImage(comment.createdBy)\" />\r\n            </div>\r\n        </div>\r\n        <div class=\"adf-comment-contents\">\r\n            <div matLine id=\"comment-user\" class=\"adf-comment-user-name\">\r\n                {{comment.createdBy?.firstName}} {{comment.createdBy?.lastName}}\r\n            </div>\r\n            <div matLine id=\"comment-message\" class=\"adf-comment-message\" [innerHTML]=\"comment.message\"></div>\r\n            <div matLine id=\"comment-time\" class=\"adf-comment-message-time\">\r\n                {{ comment.created | adfTimeAgo: currentLocale }}\r\n            </div>\r\n        </div>\r\n    </mat-list-item>\r\n</mat-list>\r\n",
                encapsulation: ViewEncapsulation.None,
                styles: [""]
            }] }
];
/** @nocollapse */
CommentListComponent.ctorParameters = () => [
    { type: PeopleProcessService },
    { type: EcmUserService },
    { type: UserPreferencesService }
];
CommentListComponent.propDecorators = {
    comments: [{ type: Input }],
    clickRow: [{ type: Output }]
};
if (false) {
    /**
     * The comments data used to populate the list.
     * @type {?}
     */
    CommentListComponent.prototype.comments;
    /**
     * Emitted when the user clicks on one of the comment rows.
     * @type {?}
     */
    CommentListComponent.prototype.clickRow;
    /** @type {?} */
    CommentListComponent.prototype.selectedComment;
    /** @type {?} */
    CommentListComponent.prototype.currentLocale;
    /**
     * @type {?}
     * @private
     */
    CommentListComponent.prototype.onDestroy$;
    /** @type {?} */
    CommentListComponent.prototype.peopleProcessService;
    /** @type {?} */
    CommentListComponent.prototype.ecmUserService;
    /** @type {?} */
    CommentListComponent.prototype.userPreferenceService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudC1saXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNvbW1lbnRzL2NvbW1lbnQtbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxpQkFBaUIsRUFBcUIsTUFBTSxlQUFlLENBQUM7QUFFN0csT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ3BHLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBUzNDLE1BQU0sT0FBTyxvQkFBb0I7Ozs7OztJQWM3QixZQUFtQixvQkFBMEMsRUFDMUMsY0FBOEIsRUFDOUIscUJBQTZDO1FBRjdDLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7UUFDMUMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBd0I7Ozs7UUFSaEUsYUFBUSxHQUErQixJQUFJLFlBQVksRUFBZ0IsQ0FBQztRQUloRSxlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztJQUs1QyxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxxQkFBcUI7YUFDckIsTUFBTSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQzthQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNoQyxTQUFTOzs7O1FBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sRUFBQyxDQUFDO0lBQzFELENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDOzs7OztJQUVELGFBQWEsQ0FBQyxPQUFxQjtRQUMvQixJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDdEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1NBQzNDO1FBQ0QsT0FBTyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBSSxDQUFDLGVBQWUsR0FBRyxPQUFPLENBQUM7UUFDL0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQzdDLENBQUM7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsSUFBUzs7WUFDbEIsU0FBUyxHQUFHLEVBQUU7UUFDbEIsSUFBSSxJQUFJLEVBQUU7WUFDTixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ2hCLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQy9DO1lBQ0QsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNmLFNBQVMsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLFdBQVcsRUFBRSxDQUFDO2FBQy9DO1NBQ0o7UUFDRCxPQUFPLFNBQVMsQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELGdCQUFnQixDQUFDLElBQVM7UUFDdEIsT0FBTyxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDM0MsQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUMsSUFBUztRQUNsQixJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDNUIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUNqRTthQUFNO1lBQ0gsT0FBTyxJQUFJLENBQUMsb0JBQW9CLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3ZEO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sZUFBZSxDQUFDLElBQVM7UUFDN0IsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3pCLENBQUM7OztZQTFFSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGtCQUFrQjtnQkFDNUIsbStDQUE0QztnQkFFNUMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2FBQ3hDOzs7O1lBVlEsb0JBQW9CO1lBRHBCLGNBQWM7WUFFZCxzQkFBc0I7Ozt1QkFjMUIsS0FBSzt1QkFJTCxNQUFNOzs7Ozs7O0lBSlAsd0NBQ3lCOzs7OztJQUd6Qix3Q0FDd0U7O0lBRXhFLCtDQUE4Qjs7SUFDOUIsNkNBQWM7Ozs7O0lBQ2QsMENBQTRDOztJQUVoQyxvREFBaUQ7O0lBQ2pELDhDQUFxQzs7SUFDckMscURBQW9EIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT3V0cHV0LCBWaWV3RW5jYXBzdWxhdGlvbiwgT25Jbml0LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29tbWVudE1vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL2NvbW1lbnQubW9kZWwnO1xyXG5pbXBvcnQgeyBFY21Vc2VyU2VydmljZSB9IGZyb20gJy4uL3VzZXJpbmZvL3NlcnZpY2VzL2VjbS11c2VyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQZW9wbGVQcm9jZXNzU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3Blb3BsZS1wcm9jZXNzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlLCBVc2VyUHJlZmVyZW5jZVZhbHVlcyB9IGZyb20gJy4uL3NlcnZpY2VzL3VzZXItcHJlZmVyZW5jZXMuc2VydmljZSc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1jb21tZW50LWxpc3QnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2NvbW1lbnQtbGlzdC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9jb21tZW50LWxpc3QuY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuXHJcbmV4cG9ydCBjbGFzcyBDb21tZW50TGlzdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuXHJcbiAgICAvKiogVGhlIGNvbW1lbnRzIGRhdGEgdXNlZCB0byBwb3B1bGF0ZSB0aGUgbGlzdC4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBjb21tZW50czogQ29tbWVudE1vZGVsW107XHJcblxyXG4gICAgLyoqIEVtaXR0ZWQgd2hlbiB0aGUgdXNlciBjbGlja3Mgb24gb25lIG9mIHRoZSBjb21tZW50IHJvd3MuICovXHJcbiAgICBAT3V0cHV0KClcclxuICAgIGNsaWNrUm93OiBFdmVudEVtaXR0ZXI8Q29tbWVudE1vZGVsPiA9IG5ldyBFdmVudEVtaXR0ZXI8Q29tbWVudE1vZGVsPigpO1xyXG5cclxuICAgIHNlbGVjdGVkQ29tbWVudDogQ29tbWVudE1vZGVsO1xyXG4gICAgY3VycmVudExvY2FsZTtcclxuICAgIHByaXZhdGUgb25EZXN0cm95JCA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIHBlb3BsZVByb2Nlc3NTZXJ2aWNlOiBQZW9wbGVQcm9jZXNzU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBlY21Vc2VyU2VydmljZTogRWNtVXNlclNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgdXNlclByZWZlcmVuY2VTZXJ2aWNlOiBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZVNlcnZpY2VcclxuICAgICAgICAgICAgLnNlbGVjdChVc2VyUHJlZmVyZW5jZVZhbHVlcy5Mb2NhbGUpXHJcbiAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLm9uRGVzdHJveSQpKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKGxvY2FsZSA9PiB0aGlzLmN1cnJlbnRMb2NhbGUgPSBsb2NhbGUpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIHRoaXMub25EZXN0cm95JC5uZXh0KHRydWUpO1xyXG4gICAgICAgIHRoaXMub25EZXN0cm95JC5jb21wbGV0ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHNlbGVjdENvbW1lbnQoY29tbWVudDogQ29tbWVudE1vZGVsKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKHRoaXMuc2VsZWN0ZWRDb21tZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRDb21tZW50LmlzU2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29tbWVudC5pc1NlbGVjdGVkID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLnNlbGVjdGVkQ29tbWVudCA9IGNvbW1lbnQ7XHJcbiAgICAgICAgdGhpcy5jbGlja1Jvdy5lbWl0KHRoaXMuc2VsZWN0ZWRDb21tZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRVc2VyU2hvcnROYW1lKHVzZXI6IGFueSk6IHN0cmluZyB7XHJcbiAgICAgICAgbGV0IHNob3J0TmFtZSA9ICcnO1xyXG4gICAgICAgIGlmICh1c2VyKSB7XHJcbiAgICAgICAgICAgIGlmICh1c2VyLmZpcnN0TmFtZSkge1xyXG4gICAgICAgICAgICAgICAgc2hvcnROYW1lID0gdXNlci5maXJzdE5hbWVbMF0udG9VcHBlckNhc2UoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAodXNlci5sYXN0TmFtZSkge1xyXG4gICAgICAgICAgICAgICAgc2hvcnROYW1lICs9IHVzZXIubGFzdE5hbWVbMF0udG9VcHBlckNhc2UoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gc2hvcnROYW1lO1xyXG4gICAgfVxyXG5cclxuICAgIGlzUGljdHVyZURlZmluZWQodXNlcjogYW55KTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHVzZXIucGljdHVyZUlkIHx8IHVzZXIuYXZhdGFySWQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXNlckltYWdlKHVzZXI6IGFueSk6IHN0cmluZyB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNBQ29udGVudFVzZXJzKHVzZXIpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmVjbVVzZXJTZXJ2aWNlLmdldFVzZXJQcm9maWxlSW1hZ2UodXNlci5hdmF0YXJJZCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMucGVvcGxlUHJvY2Vzc1NlcnZpY2UuZ2V0VXNlckltYWdlKHVzZXIpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGlzQUNvbnRlbnRVc2Vycyh1c2VyOiBhbnkpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdXNlci5hdmF0YXJJZDtcclxuICAgIH1cclxufVxyXG4iXX0=