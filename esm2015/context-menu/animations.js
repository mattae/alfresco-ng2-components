/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { state, style, animate, transition, query, group, sequence } from '@angular/animations';
/** @type {?} */
export const contextMenuAnimation = [
    state('void', style({
        opacity: 0,
        transform: 'scale(0.01, 0.01)'
    })),
    transition('void => *', sequence([
        query('.mat-menu-content', style({ opacity: 0 })),
        animate('100ms linear', style({ opacity: 1, transform: 'scale(1, 0.5)' })),
        group([
            query('.mat-menu-content', animate('400ms cubic-bezier(0.55, 0, 0.55, 0.2)', style({ opacity: 1 }))),
            animate('300ms cubic-bezier(0.25, 0.8, 0.25, 1)', style({ transform: 'scale(1, 1)' }))
        ])
    ])),
    transition('* => void', animate('150ms 50ms linear', style({ opacity: 0 })))
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5pbWF0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNvbnRleHQtbWVudS9hbmltYXRpb25zLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFDSCxLQUFLLEVBQ0wsS0FBSyxFQUNMLE9BQU8sRUFDUCxVQUFVLEVBQ1YsS0FBSyxFQUNMLEtBQUssRUFDTCxRQUFRLEVBR1gsTUFBTSxxQkFBcUIsQ0FBQzs7QUFFN0IsTUFBTSxPQUFPLG9CQUFvQixHQUE4RDtJQUMzRixLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQztRQUNoQixPQUFPLEVBQUUsQ0FBQztRQUNWLFNBQVMsRUFBRSxtQkFBbUI7S0FDakMsQ0FBQyxDQUFDO0lBQ0gsVUFBVSxDQUFDLFdBQVcsRUFBRSxRQUFRLENBQUM7UUFDN0IsS0FBSyxDQUFDLG1CQUFtQixFQUFFLEtBQUssQ0FBQyxFQUFFLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ2pELE9BQU8sQ0FBQyxjQUFjLEVBQUUsS0FBSyxDQUFDLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBRSxTQUFTLEVBQUUsZUFBZSxFQUFFLENBQUMsQ0FBQztRQUMxRSxLQUFLLENBQUM7WUFDRixLQUFLLENBQUMsbUJBQW1CLEVBQUUsT0FBTyxDQUFDLHdDQUF3QyxFQUN2RSxLQUFLLENBQUMsRUFBRSxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FDeEIsQ0FBQztZQUNGLE9BQU8sQ0FBQyx3Q0FBd0MsRUFBRSxLQUFLLENBQUMsRUFBRSxTQUFTLEVBQUUsYUFBYSxFQUFFLENBQUMsQ0FBQztTQUN6RixDQUFDO0tBQ0wsQ0FBQyxDQUFDO0lBQ0gsVUFBVSxDQUFDLFdBQVcsRUFBRSxPQUFPLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxDQUFDLEVBQUUsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztDQUMvRSIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQge1xyXG4gICAgc3RhdGUsXHJcbiAgICBzdHlsZSxcclxuICAgIGFuaW1hdGUsXHJcbiAgICB0cmFuc2l0aW9uLFxyXG4gICAgcXVlcnksXHJcbiAgICBncm91cCxcclxuICAgIHNlcXVlbmNlLFxyXG4gICAgQW5pbWF0aW9uU3RhdGVNZXRhZGF0YSxcclxuICAgIEFuaW1hdGlvblRyYW5zaXRpb25NZXRhZGF0YVxyXG59IGZyb20gJ0Bhbmd1bGFyL2FuaW1hdGlvbnMnO1xyXG5cclxuZXhwb3J0IGNvbnN0IGNvbnRleHRNZW51QW5pbWF0aW9uOiAoIEFuaW1hdGlvblN0YXRlTWV0YWRhdGEgfCBBbmltYXRpb25UcmFuc2l0aW9uTWV0YWRhdGEpW10gPSBbXHJcbiAgICBzdGF0ZSgndm9pZCcsIHN0eWxlKHtcclxuICAgICAgICBvcGFjaXR5OiAwLFxyXG4gICAgICAgIHRyYW5zZm9ybTogJ3NjYWxlKDAuMDEsIDAuMDEpJ1xyXG4gICAgfSkpLFxyXG4gICAgdHJhbnNpdGlvbigndm9pZCA9PiAqJywgc2VxdWVuY2UoW1xyXG4gICAgICAgIHF1ZXJ5KCcubWF0LW1lbnUtY29udGVudCcsIHN0eWxlKHsgb3BhY2l0eTogMCB9KSksXHJcbiAgICAgICAgYW5pbWF0ZSgnMTAwbXMgbGluZWFyJywgc3R5bGUoeyBvcGFjaXR5OiAxLCB0cmFuc2Zvcm06ICdzY2FsZSgxLCAwLjUpJyB9KSksXHJcbiAgICAgICAgZ3JvdXAoW1xyXG4gICAgICAgICAgICBxdWVyeSgnLm1hdC1tZW51LWNvbnRlbnQnLCBhbmltYXRlKCc0MDBtcyBjdWJpYy1iZXppZXIoMC41NSwgMCwgMC41NSwgMC4yKScsXHJcbiAgICAgICAgICAgICAgICBzdHlsZSh7IG9wYWNpdHk6IDEgfSlcclxuICAgICAgICAgICAgKSksXHJcbiAgICAgICAgICAgIGFuaW1hdGUoJzMwMG1zIGN1YmljLWJlemllcigwLjI1LCAwLjgsIDAuMjUsIDEpJywgc3R5bGUoeyB0cmFuc2Zvcm06ICdzY2FsZSgxLCAxKScgfSkpXHJcbiAgICAgICAgXSlcclxuICAgIF0pKSxcclxuICAgIHRyYW5zaXRpb24oJyogPT4gdm9pZCcsIGFuaW1hdGUoJzE1MG1zIDUwbXMgbGluZWFyJywgc3R5bGUoeyBvcGFjaXR5OiAwIH0pKSlcclxuXTtcclxuIl19