/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { OverlayContainer } from '@angular/cdk/overlay';
import { ViewportRuler } from '@angular/cdk/scrolling';
import { Component, HostListener, Input, Renderer2, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material';
import { ContextMenuService } from './context-menu.service';
export class ContextMenuHolderComponent {
    /**
     * @param {?} viewport
     * @param {?} overlayContainer
     * @param {?} contextMenuService
     * @param {?} renderer
     */
    constructor(viewport, overlayContainer, contextMenuService, renderer) {
        this.viewport = viewport;
        this.overlayContainer = overlayContainer;
        this.contextMenuService = contextMenuService;
        this.renderer = renderer;
        this.links = [];
        this.mouseLocation = { left: 0, top: 0 };
        this.menuElement = null;
        this.subscriptions = [];
        this.showIcons = false;
    }
    /**
     * @param {?=} event
     * @return {?}
     */
    onShowContextMenu(event) {
        if (event) {
            event.preventDefault();
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onResize(event) {
        if (this.mdMenuElement) {
            this.updatePosition();
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.subscriptions.push(this.contextMenuService.show.subscribe((/**
         * @param {?} mouseEvent
         * @return {?}
         */
        (mouseEvent) => this.showMenu(mouseEvent.event, mouseEvent.obj))), this.menuTrigger.menuOpened.subscribe((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const container = this.overlayContainer.getContainerElement();
            if (container) {
                this.contextMenuListenerFn = this.renderer.listen(container, 'contextmenu', (/**
                 * @param {?} contextmenuEvent
                 * @return {?}
                 */
                (contextmenuEvent) => {
                    contextmenuEvent.preventDefault();
                }));
            }
            this.menuElement = this.getContextMenuElement();
        })), this.menuTrigger.menuClosed.subscribe((/**
         * @return {?}
         */
        () => {
            this.menuElement = null;
            if (this.contextMenuListenerFn) {
                this.contextMenuListenerFn();
            }
        })));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.contextMenuListenerFn) {
            this.contextMenuListenerFn();
        }
        this.subscriptions.forEach((/**
         * @param {?} subscription
         * @return {?}
         */
        (subscription) => subscription.unsubscribe()));
        this.subscriptions = [];
        this.menuElement = null;
    }
    /**
     * @param {?} event
     * @param {?} menuItem
     * @return {?}
     */
    onMenuItemClick(event, menuItem) {
        if (menuItem && menuItem.model && menuItem.model.disabled) {
            event.preventDefault();
            event.stopImmediatePropagation();
            return;
        }
        menuItem.subject.next(menuItem);
    }
    /**
     * @param {?} mouseEvent
     * @param {?} links
     * @return {?}
     */
    showMenu(mouseEvent, links) {
        this.links = links;
        if (mouseEvent) {
            this.mouseLocation = {
                left: mouseEvent.clientX,
                top: mouseEvent.clientY
            };
        }
        this.menuTrigger.openMenu();
        if (this.mdMenuElement) {
            this.updatePosition();
        }
    }
    /**
     * @return {?}
     */
    get mdMenuElement() {
        return this.menuElement;
    }
    /**
     * @private
     * @return {?}
     */
    locationCss() {
        return {
            left: this.mouseLocation.left + 'px',
            top: this.mouseLocation.top + 'px'
        };
    }
    /**
     * @private
     * @return {?}
     */
    updatePosition() {
        setTimeout((/**
         * @return {?}
         */
        () => {
            if (this.mdMenuElement.parentElement) {
                if (this.mdMenuElement.clientWidth + this.mouseLocation.left > this.viewport.getViewportRect().width) {
                    this.menuTrigger.menu.xPosition = 'before';
                    this.mdMenuElement.parentElement.style.left = this.mouseLocation.left - this.mdMenuElement.clientWidth + 'px';
                }
                else {
                    this.menuTrigger.menu.xPosition = 'after';
                    this.mdMenuElement.parentElement.style.left = this.locationCss().left;
                }
                if (this.mdMenuElement.clientHeight + this.mouseLocation.top > this.viewport.getViewportRect().height) {
                    this.menuTrigger.menu.yPosition = 'above';
                    this.mdMenuElement.parentElement.style.top = this.mouseLocation.top - this.mdMenuElement.clientHeight + 'px';
                }
                else {
                    this.menuTrigger.menu.yPosition = 'below';
                    this.mdMenuElement.parentElement.style.top = this.locationCss().top;
                }
            }
        }), 0);
    }
    /**
     * @private
     * @return {?}
     */
    getContextMenuElement() {
        return this.overlayContainer.getContainerElement().querySelector('.context-menu');
    }
}
ContextMenuHolderComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-context-menu-holder',
                template: `
        <button mat-button [matMenuTriggerFor]="contextMenu"></button>
        <mat-menu #contextMenu="matMenu" class="context-menu">
            <ng-container *ngFor="let link of links">
                <button *ngIf="link.model?.visible"
                        [attr.data-automation-id]="'context-'+((link.title || link.model?.title) | translate)"
                        mat-menu-item
                        [disabled]="link.model?.disabled"
                        (click)="onMenuItemClick($event, link)">
                    <mat-icon *ngIf="showIcons && link.model?.icon">{{ link.model.icon }}</mat-icon>
                    {{ (link.title || link.model?.title) | translate }}
                </button>
            </ng-container>
        </mat-menu>
    `
            }] }
];
/** @nocollapse */
ContextMenuHolderComponent.ctorParameters = () => [
    { type: ViewportRuler },
    { type: OverlayContainer },
    { type: ContextMenuService },
    { type: Renderer2 }
];
ContextMenuHolderComponent.propDecorators = {
    showIcons: [{ type: Input }],
    menuTrigger: [{ type: ViewChild, args: [MatMenuTrigger, { static: true },] }],
    onShowContextMenu: [{ type: HostListener, args: ['contextmenu', ['$event'],] }],
    onResize: [{ type: HostListener, args: ['window:resize', ['$event'],] }]
};
if (false) {
    /** @type {?} */
    ContextMenuHolderComponent.prototype.links;
    /**
     * @type {?}
     * @private
     */
    ContextMenuHolderComponent.prototype.mouseLocation;
    /**
     * @type {?}
     * @private
     */
    ContextMenuHolderComponent.prototype.menuElement;
    /**
     * @type {?}
     * @private
     */
    ContextMenuHolderComponent.prototype.subscriptions;
    /**
     * @type {?}
     * @private
     */
    ContextMenuHolderComponent.prototype.contextMenuListenerFn;
    /** @type {?} */
    ContextMenuHolderComponent.prototype.showIcons;
    /** @type {?} */
    ContextMenuHolderComponent.prototype.menuTrigger;
    /**
     * @type {?}
     * @private
     */
    ContextMenuHolderComponent.prototype.viewport;
    /**
     * @type {?}
     * @private
     */
    ContextMenuHolderComponent.prototype.overlayContainer;
    /**
     * @type {?}
     * @private
     */
    ContextMenuHolderComponent.prototype.contextMenuService;
    /**
     * @type {?}
     * @private
     */
    ContextMenuHolderComponent.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGV4dC1tZW51LWhvbGRlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJjb250ZXh0LW1lbnUvY29udGV4dC1tZW51LWhvbGRlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUNBLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN2RCxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQXFCLFNBQVMsRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDeEcsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBRW5ELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBb0I1RCxNQUFNLE9BQU8sMEJBQTBCOzs7Ozs7O0lBNEJuQyxZQUNZLFFBQXVCLEVBQ3ZCLGdCQUFrQyxFQUNsQyxrQkFBc0MsRUFDdEMsUUFBbUI7UUFIbkIsYUFBUSxHQUFSLFFBQVEsQ0FBZTtRQUN2QixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsYUFBUSxHQUFSLFFBQVEsQ0FBVztRQS9CL0IsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUVILGtCQUFhLEdBQWtDLEVBQUMsSUFBSSxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFDLENBQUM7UUFDakUsZ0JBQVcsR0FBRyxJQUFJLENBQUM7UUFDbkIsa0JBQWEsR0FBbUIsRUFBRSxDQUFDO1FBSTNDLGNBQVMsR0FBWSxLQUFLLENBQUM7SUF5QjNCLENBQUM7Ozs7O0lBbkJELGlCQUFpQixDQUFDLEtBQWtCO1FBQ2hDLElBQUksS0FBSyxFQUFFO1lBQ1AsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1NBQzFCO0lBQ0wsQ0FBQzs7Ozs7SUFHRCxRQUFRLENBQUMsS0FBSztRQUNWLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUNwQixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7U0FDekI7SUFDTCxDQUFDOzs7O0lBVUQsUUFBUTtRQUNKLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUNuQixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVM7Ozs7UUFBQyxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBQyxFQUV2RyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxTQUFTOzs7UUFBQyxHQUFHLEVBQUU7O2tCQUNqQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixFQUFFO1lBQzdELElBQUksU0FBUyxFQUFFO2dCQUNYLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsYUFBYTs7OztnQkFBRSxDQUFDLGdCQUF1QixFQUFFLEVBQUU7b0JBQ3BHLGdCQUFnQixDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUN0QyxDQUFDLEVBQUMsQ0FBQzthQUNOO1lBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztRQUNwRCxDQUFDLEVBQUMsRUFFRixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxTQUFTOzs7UUFBQyxHQUFHLEVBQUU7WUFDdkMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDeEIsSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7Z0JBQzVCLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO2FBQ2hDO1FBQ0wsQ0FBQyxFQUFDLENBQ0wsQ0FBQztJQUNOLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDNUIsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7U0FDaEM7UUFFRCxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU87Ozs7UUFBQyxDQUFDLFlBQVksRUFBRSxFQUFFLENBQUMsWUFBWSxDQUFDLFdBQVcsRUFBRSxFQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFFeEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7SUFDNUIsQ0FBQzs7Ozs7O0lBRUQsZUFBZSxDQUFDLEtBQVksRUFBRSxRQUFhO1FBQ3ZDLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxLQUFLLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUU7WUFDdkQsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3ZCLEtBQUssQ0FBQyx3QkFBd0IsRUFBRSxDQUFDO1lBQ2pDLE9BQU87U0FDVjtRQUNELFFBQVEsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3BDLENBQUM7Ozs7OztJQUVELFFBQVEsQ0FBQyxVQUFVLEVBQUUsS0FBSztRQUN0QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUVuQixJQUFJLFVBQVUsRUFBRTtZQUNaLElBQUksQ0FBQyxhQUFhLEdBQUc7Z0JBQ2pCLElBQUksRUFBRSxVQUFVLENBQUMsT0FBTztnQkFDeEIsR0FBRyxFQUFFLFVBQVUsQ0FBQyxPQUFPO2FBQzFCLENBQUM7U0FDTDtRQUVELElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFNUIsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFO1lBQ3BCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUN6QjtJQUNMLENBQUM7Ozs7SUFFRCxJQUFJLGFBQWE7UUFDYixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDNUIsQ0FBQzs7Ozs7SUFFTyxXQUFXO1FBQ2YsT0FBTztZQUNILElBQUksRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksR0FBRyxJQUFJO1lBQ3BDLEdBQUcsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsR0FBRyxJQUFJO1NBQ3JDLENBQUM7SUFDTixDQUFDOzs7OztJQUVPLGNBQWM7UUFDbEIsVUFBVTs7O1FBQUMsR0FBRyxFQUFFO1lBQ1osSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsRUFBRTtnQkFDbEMsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxDQUFDLEtBQUssRUFBRTtvQkFDbEcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztvQkFDM0MsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7aUJBQ2pIO3FCQUFNO29CQUNILElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUM7b0JBQzFDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQztpQkFDekU7Z0JBRUQsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxDQUFDLE1BQU0sRUFBRTtvQkFDbkcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQztvQkFDMUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7aUJBQ2hIO3FCQUFNO29CQUNILElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUM7b0JBQzFDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLEdBQUcsQ0FBQztpQkFDdkU7YUFDSjtRQUNMLENBQUMsR0FBRSxDQUFDLENBQUMsQ0FBQztJQUNWLENBQUM7Ozs7O0lBRU8scUJBQXFCO1FBQ3pCLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixFQUFFLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQ3RGLENBQUM7OztZQXJKSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLHlCQUF5QjtnQkFDbkMsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7OztLQWNUO2FBQ0o7Ozs7WUF2QlEsYUFBYTtZQURiLGdCQUFnQjtZQUtoQixrQkFBa0I7WUFIaUMsU0FBUzs7O3dCQStCaEUsS0FBSzswQkFHTCxTQUFTLFNBQUMsY0FBYyxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQztnQ0FHeEMsWUFBWSxTQUFDLGFBQWEsRUFBRSxDQUFDLFFBQVEsQ0FBQzt1QkFPdEMsWUFBWSxTQUFDLGVBQWUsRUFBRSxDQUFDLFFBQVEsQ0FBQzs7OztJQXBCekMsMkNBQVc7Ozs7O0lBRVgsbURBQXlFOzs7OztJQUN6RSxpREFBMkI7Ozs7O0lBQzNCLG1EQUEyQzs7Ozs7SUFDM0MsMkRBQTBDOztJQUUxQywrQ0FDMkI7O0lBRTNCLGlEQUM0Qjs7Ozs7SUFpQnhCLDhDQUErQjs7Ozs7SUFDL0Isc0RBQTBDOzs7OztJQUMxQyx3REFBOEM7Ozs7O0lBQzlDLDhDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxuXG4vKiFcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxuICpcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cbiAqL1xuXG5pbXBvcnQgeyBPdmVybGF5Q29udGFpbmVyIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL292ZXJsYXknO1xuaW1wb3J0IHsgVmlld3BvcnRSdWxlciB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9zY3JvbGxpbmcnO1xuaW1wb3J0IHsgQ29tcG9uZW50LCBIb3N0TGlzdGVuZXIsIElucHV0LCBPbkRlc3Ryb3ksIE9uSW5pdCwgUmVuZGVyZXIyLCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1hdE1lbnVUcmlnZ2VyIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBDb250ZXh0TWVudVNlcnZpY2UgfSBmcm9tICcuL2NvbnRleHQtbWVudS5zZXJ2aWNlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdhZGYtY29udGV4dC1tZW51LWhvbGRlcicsXG4gICAgdGVtcGxhdGU6IGBcbiAgICAgICAgPGJ1dHRvbiBtYXQtYnV0dG9uIFttYXRNZW51VHJpZ2dlckZvcl09XCJjb250ZXh0TWVudVwiPjwvYnV0dG9uPlxuICAgICAgICA8bWF0LW1lbnUgI2NvbnRleHRNZW51PVwibWF0TWVudVwiIGNsYXNzPVwiY29udGV4dC1tZW51XCI+XG4gICAgICAgICAgICA8bmctY29udGFpbmVyICpuZ0Zvcj1cImxldCBsaW5rIG9mIGxpbmtzXCI+XG4gICAgICAgICAgICAgICAgPGJ1dHRvbiAqbmdJZj1cImxpbmsubW9kZWw/LnZpc2libGVcIlxuICAgICAgICAgICAgICAgICAgICAgICAgW2F0dHIuZGF0YS1hdXRvbWF0aW9uLWlkXT1cIidjb250ZXh0LScrKChsaW5rLnRpdGxlIHx8IGxpbmsubW9kZWw/LnRpdGxlKSB8IHRyYW5zbGF0ZSlcIlxuICAgICAgICAgICAgICAgICAgICAgICAgbWF0LW1lbnUtaXRlbVxuICAgICAgICAgICAgICAgICAgICAgICAgW2Rpc2FibGVkXT1cImxpbmsubW9kZWw/LmRpc2FibGVkXCJcbiAgICAgICAgICAgICAgICAgICAgICAgIChjbGljayk9XCJvbk1lbnVJdGVtQ2xpY2soJGV2ZW50LCBsaW5rKVwiPlxuICAgICAgICAgICAgICAgICAgICA8bWF0LWljb24gKm5nSWY9XCJzaG93SWNvbnMgJiYgbGluay5tb2RlbD8uaWNvblwiPnt7IGxpbmsubW9kZWwuaWNvbiB9fTwvbWF0LWljb24+XG4gICAgICAgICAgICAgICAgICAgIHt7IChsaW5rLnRpdGxlIHx8IGxpbmsubW9kZWw/LnRpdGxlKSB8IHRyYW5zbGF0ZSB9fVxuICAgICAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgPC9uZy1jb250YWluZXI+XG4gICAgICAgIDwvbWF0LW1lbnU+XG4gICAgYFxufSlcbmV4cG9ydCBjbGFzcyBDb250ZXh0TWVudUhvbGRlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcbiAgICBsaW5rcyA9IFtdO1xuXG4gICAgcHJpdmF0ZSBtb3VzZUxvY2F0aW9uOiB7IGxlZnQ6IG51bWJlciwgdG9wOiBudW1iZXIgfSA9IHtsZWZ0OiAwLCB0b3A6IDB9O1xuICAgIHByaXZhdGUgbWVudUVsZW1lbnQgPSBudWxsO1xuICAgIHByaXZhdGUgc3Vic2NyaXB0aW9uczogU3Vic2NyaXB0aW9uW10gPSBbXTtcbiAgICBwcml2YXRlIGNvbnRleHRNZW51TGlzdGVuZXJGbjogKCkgPT4gdm9pZDtcblxuICAgIEBJbnB1dCgpXG4gICAgc2hvd0ljb25zOiBib29sZWFuID0gZmFsc2U7XG5cbiAgICBAVmlld0NoaWxkKE1hdE1lbnVUcmlnZ2VyLCB7c3RhdGljOiB0cnVlfSlcbiAgICBtZW51VHJpZ2dlcjogTWF0TWVudVRyaWdnZXI7XG5cbiAgICBASG9zdExpc3RlbmVyKCdjb250ZXh0bWVudScsIFsnJGV2ZW50J10pXG4gICAgb25TaG93Q29udGV4dE1lbnUoZXZlbnQ/OiBNb3VzZUV2ZW50KSB7XG4gICAgICAgIGlmIChldmVudCkge1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzpyZXNpemUnLCBbJyRldmVudCddKVxuICAgIG9uUmVzaXplKGV2ZW50KSB7XG4gICAgICAgIGlmICh0aGlzLm1kTWVudUVsZW1lbnQpIHtcbiAgICAgICAgICAgIHRoaXMudXBkYXRlUG9zaXRpb24oKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIHZpZXdwb3J0OiBWaWV3cG9ydFJ1bGVyLFxuICAgICAgICBwcml2YXRlIG92ZXJsYXlDb250YWluZXI6IE92ZXJsYXlDb250YWluZXIsXG4gICAgICAgIHByaXZhdGUgY29udGV4dE1lbnVTZXJ2aWNlOiBDb250ZXh0TWVudVNlcnZpY2UsXG4gICAgICAgIHByaXZhdGUgcmVuZGVyZXI6IFJlbmRlcmVyMlxuICAgICkge1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaChcbiAgICAgICAgICAgIHRoaXMuY29udGV4dE1lbnVTZXJ2aWNlLnNob3cuc3Vic2NyaWJlKChtb3VzZUV2ZW50KSA9PiB0aGlzLnNob3dNZW51KG1vdXNlRXZlbnQuZXZlbnQsIG1vdXNlRXZlbnQub2JqKSksXG5cbiAgICAgICAgICAgIHRoaXMubWVudVRyaWdnZXIubWVudU9wZW5lZC5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IGNvbnRhaW5lciA9IHRoaXMub3ZlcmxheUNvbnRhaW5lci5nZXRDb250YWluZXJFbGVtZW50KCk7XG4gICAgICAgICAgICAgICAgaWYgKGNvbnRhaW5lcikge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRleHRNZW51TGlzdGVuZXJGbiA9IHRoaXMucmVuZGVyZXIubGlzdGVuKGNvbnRhaW5lciwgJ2NvbnRleHRtZW51JywgKGNvbnRleHRtZW51RXZlbnQ6IEV2ZW50KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb250ZXh0bWVudUV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLm1lbnVFbGVtZW50ID0gdGhpcy5nZXRDb250ZXh0TWVudUVsZW1lbnQoKTtcbiAgICAgICAgICAgIH0pLFxuXG4gICAgICAgICAgICB0aGlzLm1lbnVUcmlnZ2VyLm1lbnVDbG9zZWQuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLm1lbnVFbGVtZW50ID0gbnVsbDtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5jb250ZXh0TWVudUxpc3RlbmVyRm4pIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb250ZXh0TWVudUxpc3RlbmVyRm4oKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9KVxuICAgICAgICApO1xuICAgIH1cblxuICAgIG5nT25EZXN0cm95KCkge1xuICAgICAgICBpZiAodGhpcy5jb250ZXh0TWVudUxpc3RlbmVyRm4pIHtcbiAgICAgICAgICAgIHRoaXMuY29udGV4dE1lbnVMaXN0ZW5lckZuKCk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMuZm9yRWFjaCgoc3Vic2NyaXB0aW9uKSA9PiBzdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKSk7XG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucyA9IFtdO1xuXG4gICAgICAgIHRoaXMubWVudUVsZW1lbnQgPSBudWxsO1xuICAgIH1cblxuICAgIG9uTWVudUl0ZW1DbGljayhldmVudDogRXZlbnQsIG1lbnVJdGVtOiBhbnkpOiB2b2lkIHtcbiAgICAgICAgaWYgKG1lbnVJdGVtICYmIG1lbnVJdGVtLm1vZGVsICYmIG1lbnVJdGVtLm1vZGVsLmRpc2FibGVkKSB7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgZXZlbnQuc3RvcEltbWVkaWF0ZVByb3BhZ2F0aW9uKCk7XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cbiAgICAgICAgbWVudUl0ZW0uc3ViamVjdC5uZXh0KG1lbnVJdGVtKTtcbiAgICB9XG5cbiAgICBzaG93TWVudShtb3VzZUV2ZW50LCBsaW5rcykge1xuICAgICAgICB0aGlzLmxpbmtzID0gbGlua3M7XG5cbiAgICAgICAgaWYgKG1vdXNlRXZlbnQpIHtcbiAgICAgICAgICAgIHRoaXMubW91c2VMb2NhdGlvbiA9IHtcbiAgICAgICAgICAgICAgICBsZWZ0OiBtb3VzZUV2ZW50LmNsaWVudFgsXG4gICAgICAgICAgICAgICAgdG9wOiBtb3VzZUV2ZW50LmNsaWVudFlcbiAgICAgICAgICAgIH07XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLm1lbnVUcmlnZ2VyLm9wZW5NZW51KCk7XG5cbiAgICAgICAgaWYgKHRoaXMubWRNZW51RWxlbWVudCkge1xuICAgICAgICAgICAgdGhpcy51cGRhdGVQb3NpdGlvbigpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgZ2V0IG1kTWVudUVsZW1lbnQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLm1lbnVFbGVtZW50O1xuICAgIH1cblxuICAgIHByaXZhdGUgbG9jYXRpb25Dc3MoKSB7XG4gICAgICAgIHJldHVybiB7XG4gICAgICAgICAgICBsZWZ0OiB0aGlzLm1vdXNlTG9jYXRpb24ubGVmdCArICdweCcsXG4gICAgICAgICAgICB0b3A6IHRoaXMubW91c2VMb2NhdGlvbi50b3AgKyAncHgnXG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgcHJpdmF0ZSB1cGRhdGVQb3NpdGlvbigpIHtcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICBpZiAodGhpcy5tZE1lbnVFbGVtZW50LnBhcmVudEVsZW1lbnQpIHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5tZE1lbnVFbGVtZW50LmNsaWVudFdpZHRoICsgdGhpcy5tb3VzZUxvY2F0aW9uLmxlZnQgPiB0aGlzLnZpZXdwb3J0LmdldFZpZXdwb3J0UmVjdCgpLndpZHRoKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWVudVRyaWdnZXIubWVudS54UG9zaXRpb24gPSAnYmVmb3JlJztcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tZE1lbnVFbGVtZW50LnBhcmVudEVsZW1lbnQuc3R5bGUubGVmdCA9IHRoaXMubW91c2VMb2NhdGlvbi5sZWZ0IC0gdGhpcy5tZE1lbnVFbGVtZW50LmNsaWVudFdpZHRoICsgJ3B4JztcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm1lbnVUcmlnZ2VyLm1lbnUueFBvc2l0aW9uID0gJ2FmdGVyJztcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tZE1lbnVFbGVtZW50LnBhcmVudEVsZW1lbnQuc3R5bGUubGVmdCA9IHRoaXMubG9jYXRpb25Dc3MoKS5sZWZ0O1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIGlmICh0aGlzLm1kTWVudUVsZW1lbnQuY2xpZW50SGVpZ2h0ICsgdGhpcy5tb3VzZUxvY2F0aW9uLnRvcCA+IHRoaXMudmlld3BvcnQuZ2V0Vmlld3BvcnRSZWN0KCkuaGVpZ2h0KSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWVudVRyaWdnZXIubWVudS55UG9zaXRpb24gPSAnYWJvdmUnO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm1kTWVudUVsZW1lbnQucGFyZW50RWxlbWVudC5zdHlsZS50b3AgPSB0aGlzLm1vdXNlTG9jYXRpb24udG9wIC0gdGhpcy5tZE1lbnVFbGVtZW50LmNsaWVudEhlaWdodCArICdweCc7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tZW51VHJpZ2dlci5tZW51LnlQb3NpdGlvbiA9ICdiZWxvdyc7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWRNZW51RWxlbWVudC5wYXJlbnRFbGVtZW50LnN0eWxlLnRvcCA9IHRoaXMubG9jYXRpb25Dc3MoKS50b3A7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9LCAwKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldENvbnRleHRNZW51RWxlbWVudCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMub3ZlcmxheUNvbnRhaW5lci5nZXRDb250YWluZXJFbGVtZW50KCkucXVlcnlTZWxlY3RvcignLmNvbnRleHQtbWVudScpO1xuICAgIH1cbn1cbiJdfQ==