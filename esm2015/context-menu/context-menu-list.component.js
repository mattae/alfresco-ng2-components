/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ViewEncapsulation, HostListener, Optional, Inject, QueryList, ViewChildren } from '@angular/core';
import { trigger } from '@angular/animations';
import { DOWN_ARROW, UP_ARROW } from '@angular/cdk/keycodes';
import { FocusKeyManager } from '@angular/cdk/a11y';
import { MatMenuItem } from '@angular/material';
import { ContextMenuOverlayRef } from './context-menu-overlay';
import { contextMenuAnimation } from './animations';
import { CONTEXT_MENU_DATA } from './context-menu.tokens';
export class ContextMenuListComponent {
    /**
     * @param {?} contextMenuOverlayRef
     * @param {?} data
     */
    constructor(contextMenuOverlayRef, data) {
        this.contextMenuOverlayRef = contextMenuOverlayRef;
        this.data = data;
        this.links = this.data;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    handleKeydownEscape(event) {
        if (event) {
            this.contextMenuOverlayRef.close();
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    handleKeydownEvent(event) {
        if (event) {
            /** @type {?} */
            const keyCode = event.keyCode;
            if (keyCode === UP_ARROW || keyCode === DOWN_ARROW) {
                this.keyManager.onKeydown(event);
            }
        }
    }
    /**
     * @param {?} event
     * @param {?} menuItem
     * @return {?}
     */
    onMenuItemClick(event, menuItem) {
        if (menuItem && menuItem.model && menuItem.model.disabled) {
            event.preventDefault();
            event.stopImmediatePropagation();
            return;
        }
        menuItem.subject.next(menuItem);
        this.contextMenuOverlayRef.close();
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.keyManager = new FocusKeyManager(this.items);
        this.keyManager.setFirstItemActive();
    }
}
ContextMenuListComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-context-menu',
                template: `
        <div mat-menu class="mat-menu-panel" @panelAnimation>
            <div id="adf-context-menu-content" class="mat-menu-content">
                <ng-container *ngFor="let link of links">
                    <button *ngIf="link.model?.visible"
                            [attr.data-automation-id]="'context-'+((link.title || link.model?.title) | translate)"
                            mat-menu-item
                            [disabled]="link.model?.disabled"
                            (click)="onMenuItemClick($event, link)">
                        <mat-icon *ngIf="link.model?.icon">{{ link.model.icon }}</mat-icon>
                        <span>{{ (link.title || link.model?.title) | translate }}</span>
                    </button>
                </ng-container>
            </div>
        </div>
    `,
                host: {
                    role: 'menu',
                    class: 'adf-context-menu'
                },
                encapsulation: ViewEncapsulation.None,
                animations: [
                    trigger('panelAnimation', contextMenuAnimation)
                ]
            }] }
];
/** @nocollapse */
ContextMenuListComponent.ctorParameters = () => [
    { type: ContextMenuOverlayRef, decorators: [{ type: Inject, args: [ContextMenuOverlayRef,] }] },
    { type: undefined, decorators: [{ type: Optional }, { type: Inject, args: [CONTEXT_MENU_DATA,] }] }
];
ContextMenuListComponent.propDecorators = {
    items: [{ type: ViewChildren, args: [MatMenuItem,] }],
    handleKeydownEscape: [{ type: HostListener, args: ['document:keydown.Escape', ['$event'],] }],
    handleKeydownEvent: [{ type: HostListener, args: ['document:keydown', ['$event'],] }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    ContextMenuListComponent.prototype.keyManager;
    /** @type {?} */
    ContextMenuListComponent.prototype.items;
    /** @type {?} */
    ContextMenuListComponent.prototype.links;
    /**
     * @type {?}
     * @private
     */
    ContextMenuListComponent.prototype.contextMenuOverlayRef;
    /**
     * @type {?}
     * @private
     */
    ContextMenuListComponent.prototype.data;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGV4dC1tZW51LWxpc3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiY29udGV4dC1tZW51L2NvbnRleHQtbWVudS1saXN0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQ0gsU0FBUyxFQUFFLGlCQUFpQixFQUFFLFlBQVksRUFDMUMsUUFBUSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUM1QyxNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDOUMsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDcEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ2hELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQy9ELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUNwRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQTZCMUQsTUFBTSxPQUFPLHdCQUF3Qjs7Ozs7SUFzQmpDLFlBQzJDLHFCQUE0QyxFQUNwQyxJQUFTO1FBRGpCLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBdUI7UUFDcEMsU0FBSSxHQUFKLElBQUksQ0FBSztRQUV4RCxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7SUFDM0IsQ0FBQzs7Ozs7SUFyQkQsbUJBQW1CLENBQUMsS0FBb0I7UUFDcEMsSUFBSSxLQUFLLEVBQUU7WUFDUCxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDdEM7SUFDTCxDQUFDOzs7OztJQUdELGtCQUFrQixDQUFDLEtBQW9CO1FBQ25DLElBQUksS0FBSyxFQUFFOztrQkFDRCxPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU87WUFDN0IsSUFBSSxPQUFPLEtBQUssUUFBUSxJQUFJLE9BQU8sS0FBSyxVQUFVLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3BDO1NBQ0o7SUFDTCxDQUFDOzs7Ozs7SUFTRCxlQUFlLENBQUMsS0FBWSxFQUFFLFFBQWE7UUFDdkMsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLEtBQUssSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRTtZQUN2RCxLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdkIsS0FBSyxDQUFDLHdCQUF3QixFQUFFLENBQUM7WUFDakMsT0FBTztTQUNWO1FBRUQsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3ZDLENBQUM7Ozs7SUFFRCxlQUFlO1FBQ1gsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLGVBQWUsQ0FBYyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO0lBQ3pDLENBQUM7OztZQXRFSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGtCQUFrQjtnQkFDNUIsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7Ozs7S0FlVDtnQkFDRCxJQUFJLEVBQUU7b0JBQ0YsSUFBSSxFQUFFLE1BQU07b0JBQ1osS0FBSyxFQUFFLGtCQUFrQjtpQkFDNUI7Z0JBQ0QsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7Z0JBQ3JDLFVBQVUsRUFBRTtvQkFDUixPQUFPLENBQUMsZ0JBQWdCLEVBQUUsb0JBQW9CLENBQUM7aUJBQ2xEO2FBQ0o7Ozs7WUE5QlEscUJBQXFCLHVCQXNEckIsTUFBTSxTQUFDLHFCQUFxQjs0Q0FDNUIsUUFBUSxZQUFJLE1BQU0sU0FBQyxpQkFBaUI7OztvQkF0QnhDLFlBQVksU0FBQyxXQUFXO2tDQUd4QixZQUFZLFNBQUMseUJBQXlCLEVBQUUsQ0FBQyxRQUFRLENBQUM7aUNBT2xELFlBQVksU0FBQyxrQkFBa0IsRUFBRSxDQUFDLFFBQVEsQ0FBQzs7Ozs7OztJQVg1Qyw4Q0FBaUQ7O0lBQ2pELHlDQUF5RDs7SUFDekQseUNBQWE7Ozs7O0lBb0JULHlEQUFtRjs7Ozs7SUFDbkYsd0NBQXdEIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7XHJcbiAgICBDb21wb25lbnQsIFZpZXdFbmNhcHN1bGF0aW9uLCBIb3N0TGlzdGVuZXIsIEFmdGVyVmlld0luaXQsXHJcbiAgICBPcHRpb25hbCwgSW5qZWN0LCBRdWVyeUxpc3QsIFZpZXdDaGlsZHJlblxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyB0cmlnZ2VyIH0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucyc7XHJcbmltcG9ydCB7IERPV05fQVJST1csIFVQX0FSUk9XIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2tleWNvZGVzJztcclxuaW1wb3J0IHsgRm9jdXNLZXlNYW5hZ2VyIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2ExMXknO1xyXG5pbXBvcnQgeyBNYXRNZW51SXRlbSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgQ29udGV4dE1lbnVPdmVybGF5UmVmIH0gZnJvbSAnLi9jb250ZXh0LW1lbnUtb3ZlcmxheSc7XHJcbmltcG9ydCB7IGNvbnRleHRNZW51QW5pbWF0aW9uIH0gZnJvbSAnLi9hbmltYXRpb25zJztcclxuaW1wb3J0IHsgQ09OVEVYVF9NRU5VX0RBVEEgfSBmcm9tICcuL2NvbnRleHQtbWVudS50b2tlbnMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1jb250ZXh0LW1lbnUnLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuICAgICAgICA8ZGl2IG1hdC1tZW51IGNsYXNzPVwibWF0LW1lbnUtcGFuZWxcIiBAcGFuZWxBbmltYXRpb24+XHJcbiAgICAgICAgICAgIDxkaXYgaWQ9XCJhZGYtY29udGV4dC1tZW51LWNvbnRlbnRcIiBjbGFzcz1cIm1hdC1tZW51LWNvbnRlbnRcIj5cclxuICAgICAgICAgICAgICAgIDxuZy1jb250YWluZXIgKm5nRm9yPVwibGV0IGxpbmsgb2YgbGlua3NcIj5cclxuICAgICAgICAgICAgICAgICAgICA8YnV0dG9uICpuZ0lmPVwibGluay5tb2RlbD8udmlzaWJsZVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBbYXR0ci5kYXRhLWF1dG9tYXRpb24taWRdPVwiJ2NvbnRleHQtJysoKGxpbmsudGl0bGUgfHwgbGluay5tb2RlbD8udGl0bGUpIHwgdHJhbnNsYXRlKVwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXQtbWVudS1pdGVtXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBbZGlzYWJsZWRdPVwibGluay5tb2RlbD8uZGlzYWJsZWRcIlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKGNsaWNrKT1cIm9uTWVudUl0ZW1DbGljaygkZXZlbnQsIGxpbmspXCI+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxtYXQtaWNvbiAqbmdJZj1cImxpbmsubW9kZWw/Lmljb25cIj57eyBsaW5rLm1vZGVsLmljb24gfX08L21hdC1pY29uPlxyXG4gICAgICAgICAgICAgICAgICAgICAgICA8c3Bhbj57eyAobGluay50aXRsZSB8fCBsaW5rLm1vZGVsPy50aXRsZSkgfCB0cmFuc2xhdGUgfX08L3NwYW4+XHJcbiAgICAgICAgICAgICAgICAgICAgPC9idXR0b24+XHJcbiAgICAgICAgICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgICAgICAgICAgPC9kaXY+XHJcbiAgICAgICAgPC9kaXY+XHJcbiAgICBgLFxyXG4gICAgaG9zdDoge1xyXG4gICAgICAgIHJvbGU6ICdtZW51JyxcclxuICAgICAgICBjbGFzczogJ2FkZi1jb250ZXh0LW1lbnUnXHJcbiAgICB9LFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICAgIGFuaW1hdGlvbnM6IFtcclxuICAgICAgICB0cmlnZ2VyKCdwYW5lbEFuaW1hdGlvbicsIGNvbnRleHRNZW51QW5pbWF0aW9uKVxyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29udGV4dE1lbnVMaXN0Q29tcG9uZW50IGltcGxlbWVudHMgQWZ0ZXJWaWV3SW5pdCB7XHJcbiAgICBwcml2YXRlIGtleU1hbmFnZXI6IEZvY3VzS2V5TWFuYWdlcjxNYXRNZW51SXRlbT47XHJcbiAgICBAVmlld0NoaWxkcmVuKE1hdE1lbnVJdGVtKSBpdGVtczogUXVlcnlMaXN0PE1hdE1lbnVJdGVtPjtcclxuICAgIGxpbmtzOiBhbnlbXTtcclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdkb2N1bWVudDprZXlkb3duLkVzY2FwZScsIFsnJGV2ZW50J10pXHJcbiAgICBoYW5kbGVLZXlkb3duRXNjYXBlKGV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29udGV4dE1lbnVPdmVybGF5UmVmLmNsb3NlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2RvY3VtZW50OmtleWRvd24nLCBbJyRldmVudCddKVxyXG4gICAgaGFuZGxlS2V5ZG93bkV2ZW50KGV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XHJcbiAgICAgICAgaWYgKGV2ZW50KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGtleUNvZGUgPSBldmVudC5rZXlDb2RlO1xyXG4gICAgICAgICAgICBpZiAoa2V5Q29kZSA9PT0gVVBfQVJST1cgfHwga2V5Q29kZSA9PT0gRE9XTl9BUlJPVykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5rZXlNYW5hZ2VyLm9uS2V5ZG93bihldmVudCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgQEluamVjdChDb250ZXh0TWVudU92ZXJsYXlSZWYpIHByaXZhdGUgY29udGV4dE1lbnVPdmVybGF5UmVmOiBDb250ZXh0TWVudU92ZXJsYXlSZWYsXHJcbiAgICAgICAgQE9wdGlvbmFsKCkgQEluamVjdChDT05URVhUX01FTlVfREFUQSkgcHJpdmF0ZSBkYXRhOiBhbnlcclxuICAgICkge1xyXG4gICAgICAgIHRoaXMubGlua3MgPSB0aGlzLmRhdGE7XHJcbiAgICB9XHJcblxyXG4gICAgb25NZW51SXRlbUNsaWNrKGV2ZW50OiBFdmVudCwgbWVudUl0ZW06IGFueSkge1xyXG4gICAgICAgIGlmIChtZW51SXRlbSAmJiBtZW51SXRlbS5tb2RlbCAmJiBtZW51SXRlbS5tb2RlbC5kaXNhYmxlZCkge1xyXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICBldmVudC5zdG9wSW1tZWRpYXRlUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgbWVudUl0ZW0uc3ViamVjdC5uZXh0KG1lbnVJdGVtKTtcclxuICAgICAgICB0aGlzLmNvbnRleHRNZW51T3ZlcmxheVJlZi5jbG9zZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcclxuICAgICAgICB0aGlzLmtleU1hbmFnZXIgPSBuZXcgRm9jdXNLZXlNYW5hZ2VyPE1hdE1lbnVJdGVtPih0aGlzLml0ZW1zKTtcclxuICAgICAgICB0aGlzLmtleU1hbmFnZXIuc2V0Rmlyc3RJdGVtQWN0aXZlKCk7XHJcbiAgICB9XHJcbn1cclxuIl19