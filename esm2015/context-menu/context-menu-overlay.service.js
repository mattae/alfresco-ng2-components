/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable, Injector, ElementRef } from '@angular/core';
import { Overlay, OverlayConfig } from '@angular/cdk/overlay';
import { PortalInjector, ComponentPortal } from '@angular/cdk/portal';
import { ContextMenuOverlayRef } from './context-menu-overlay';
import { CONTEXT_MENU_DATA } from './context-menu.tokens';
import { ContextMenuListComponent } from './context-menu-list.component';
import * as i0 from "@angular/core";
import * as i1 from "@angular/cdk/overlay";
/** @type {?} */
const DEFAULT_CONFIG = {
    panelClass: 'cdk-overlay-pane',
    backdropClass: 'cdk-overlay-transparent-backdrop',
    hasBackdrop: true
};
export class ContextMenuOverlayService {
    /**
     * @param {?} injector
     * @param {?} overlay
     */
    constructor(injector, overlay) {
        this.injector = injector;
        this.overlay = overlay;
    }
    /**
     * @param {?} config
     * @return {?}
     */
    open(config) {
        /** @type {?} */
        const overlayConfig = Object.assign({}, DEFAULT_CONFIG, config);
        /** @type {?} */
        const overlay = this.createOverlay(overlayConfig);
        /** @type {?} */
        const overlayRef = new ContextMenuOverlayRef(overlay);
        this.attachDialogContainer(overlay, config, overlayRef);
        overlay.backdropClick().subscribe((/**
         * @return {?}
         */
        () => overlayRef.close()));
        // prevent native contextmenu on overlay element if config.hasBackdrop is true
        if (overlayConfig.hasBackdrop) {
            ((/** @type {?} */ (overlay)))._backdropElement
                .addEventListener('contextmenu', (/**
             * @param {?} event
             * @return {?}
             */
            (event) => {
                event.preventDefault();
                ((/** @type {?} */ (overlay)))._backdropClick.next(null);
            }), true);
        }
        return overlayRef;
    }
    /**
     * @private
     * @param {?} config
     * @return {?}
     */
    createOverlay(config) {
        /** @type {?} */
        const overlayConfig = this.getOverlayConfig(config);
        return this.overlay.create(overlayConfig);
    }
    /**
     * @private
     * @param {?} overlay
     * @param {?} config
     * @param {?} contextMenuOverlayRef
     * @return {?}
     */
    attachDialogContainer(overlay, config, contextMenuOverlayRef) {
        /** @type {?} */
        const injector = this.createInjector(config, contextMenuOverlayRef);
        /** @type {?} */
        const containerPortal = new ComponentPortal(ContextMenuListComponent, null, injector);
        /** @type {?} */
        const containerRef = overlay.attach(containerPortal);
        return containerRef.instance;
    }
    /**
     * @private
     * @param {?} config
     * @param {?} contextMenuOverlayRef
     * @return {?}
     */
    createInjector(config, contextMenuOverlayRef) {
        /** @type {?} */
        const injectionTokens = new WeakMap();
        injectionTokens.set(ContextMenuOverlayRef, contextMenuOverlayRef);
        injectionTokens.set(CONTEXT_MENU_DATA, config.data);
        return new PortalInjector(this.injector, injectionTokens);
    }
    /**
     * @private
     * @param {?} config
     * @return {?}
     */
    getOverlayConfig(config) {
        const { clientY, clientX } = config.source;
        /** @type {?} */
        const fakeElement = {
            getBoundingClientRect: (/**
             * @return {?}
             */
            () => ({
                bottom: clientY,
                height: 0,
                left: clientX,
                right: clientX,
                top: clientY,
                width: 0
            }))
        };
        /** @type {?} */
        const positionStrategy = this.overlay.position()
            .connectedTo(new ElementRef(fakeElement), { originX: 'start', originY: 'bottom' }, { overlayX: 'start', overlayY: 'top' })
            .withFallbackPosition({ originX: 'start', originY: 'top' }, { overlayX: 'start', overlayY: 'bottom' })
            .withFallbackPosition({ originX: 'end', originY: 'top' }, { overlayX: 'start', overlayY: 'top' })
            .withFallbackPosition({ originX: 'start', originY: 'top' }, { overlayX: 'end', overlayY: 'top' })
            .withFallbackPosition({ originX: 'end', originY: 'center' }, { overlayX: 'start', overlayY: 'center' })
            .withFallbackPosition({ originX: 'start', originY: 'center' }, { overlayX: 'end', overlayY: 'center' });
        /** @type {?} */
        const overlayConfig = new OverlayConfig({
            hasBackdrop: config.hasBackdrop,
            backdropClass: config.backdropClass,
            panelClass: config.panelClass,
            scrollStrategy: this.overlay.scrollStrategies.close(),
            positionStrategy
        });
        return overlayConfig;
    }
}
ContextMenuOverlayService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
ContextMenuOverlayService.ctorParameters = () => [
    { type: Injector },
    { type: Overlay }
];
/** @nocollapse */ ContextMenuOverlayService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ContextMenuOverlayService_Factory() { return new ContextMenuOverlayService(i0.ɵɵinject(i0.INJECTOR), i0.ɵɵinject(i1.Overlay)); }, token: ContextMenuOverlayService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    ContextMenuOverlayService.prototype.injector;
    /**
     * @type {?}
     * @private
     */
    ContextMenuOverlayService.prototype.overlay;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGV4dC1tZW51LW92ZXJsYXkuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNvbnRleHQtbWVudS9jb250ZXh0LW1lbnUtb3ZlcmxheS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBZ0IsTUFBTSxlQUFlLENBQUM7QUFDL0UsT0FBTyxFQUFFLE9BQU8sRUFBRSxhQUFhLEVBQWMsTUFBTSxzQkFBc0IsQ0FBQztBQUMxRSxPQUFPLEVBQUUsY0FBYyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3RFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBRS9ELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQzFELE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLCtCQUErQixDQUFDOzs7O01BRW5FLGNBQWMsR0FBNkI7SUFDN0MsVUFBVSxFQUFFLGtCQUFrQjtJQUM5QixhQUFhLEVBQUUsa0NBQWtDO0lBQ2pELFdBQVcsRUFBRSxJQUFJO0NBQ3BCO0FBS0QsTUFBTSxPQUFPLHlCQUF5Qjs7Ozs7SUFFbEMsWUFDWSxRQUFrQixFQUNsQixPQUFnQjtRQURoQixhQUFRLEdBQVIsUUFBUSxDQUFVO1FBQ2xCLFlBQU8sR0FBUCxPQUFPLENBQVM7SUFDekIsQ0FBQzs7Ozs7SUFFSixJQUFJLENBQUMsTUFBZ0M7O2NBQzNCLGFBQWEscUJBQVEsY0FBYyxFQUFLLE1BQU0sQ0FBRTs7Y0FFaEQsT0FBTyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDOztjQUUzQyxVQUFVLEdBQUcsSUFBSSxxQkFBcUIsQ0FBQyxPQUFPLENBQUM7UUFFckQsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxDQUFDLENBQUM7UUFFeEQsT0FBTyxDQUFDLGFBQWEsRUFBRSxDQUFDLFNBQVM7OztRQUFDLEdBQUcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsRUFBQyxDQUFDO1FBRTVELDhFQUE4RTtRQUM5RSxJQUFJLGFBQWEsQ0FBQyxXQUFXLEVBQUU7WUFDM0IsQ0FBQyxtQkFBTSxPQUFPLEVBQUEsQ0FBQyxDQUFDLGdCQUFnQjtpQkFDM0IsZ0JBQWdCLENBQUMsYUFBYTs7OztZQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7Z0JBQ3ZDLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDdkIsQ0FBQyxtQkFBTSxPQUFPLEVBQUEsQ0FBQyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDOUMsQ0FBQyxHQUFFLElBQUksQ0FBQyxDQUFDO1NBQ2hCO1FBRUQsT0FBTyxVQUFVLENBQUM7SUFDdEIsQ0FBQzs7Ozs7O0lBRU8sYUFBYSxDQUFDLE1BQWdDOztjQUM1QyxhQUFhLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQztRQUNuRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQzlDLENBQUM7Ozs7Ozs7O0lBRU8scUJBQXFCLENBQUMsT0FBbUIsRUFBRSxNQUFnQyxFQUFFLHFCQUE0Qzs7Y0FDdkgsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLHFCQUFxQixDQUFDOztjQUU3RCxlQUFlLEdBQUcsSUFBSSxlQUFlLENBQUMsd0JBQXdCLEVBQUUsSUFBSSxFQUFFLFFBQVEsQ0FBQzs7Y0FDL0UsWUFBWSxHQUEyQyxPQUFPLENBQUMsTUFBTSxDQUFDLGVBQWUsQ0FBQztRQUU1RixPQUFPLFlBQVksQ0FBQyxRQUFRLENBQUM7SUFDakMsQ0FBQzs7Ozs7OztJQUVPLGNBQWMsQ0FBQyxNQUFnQyxFQUFFLHFCQUE0Qzs7Y0FDM0YsZUFBZSxHQUFHLElBQUksT0FBTyxFQUFFO1FBRXJDLGVBQWUsQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUscUJBQXFCLENBQUMsQ0FBQztRQUNsRSxlQUFlLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUVwRCxPQUFPLElBQUksY0FBYyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsZUFBZSxDQUFDLENBQUM7SUFDOUQsQ0FBQzs7Ozs7O0lBRU8sZ0JBQWdCLENBQUMsTUFBZ0M7Y0FDL0MsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFHLEdBQUcsTUFBTSxDQUFDLE1BQU07O2NBRXJDLFdBQVcsR0FBUTtZQUNyQixxQkFBcUI7OztZQUFFLEdBQWUsRUFBRSxDQUFDLENBQUM7Z0JBQ3RDLE1BQU0sRUFBRSxPQUFPO2dCQUNmLE1BQU0sRUFBRSxDQUFDO2dCQUNULElBQUksRUFBRSxPQUFPO2dCQUNiLEtBQUssRUFBRSxPQUFPO2dCQUNkLEdBQUcsRUFBRSxPQUFPO2dCQUNaLEtBQUssRUFBRSxDQUFDO2FBQ1gsQ0FBQyxDQUFBO1NBQ0w7O2NBRUssZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLEVBQUU7YUFDM0MsV0FBVyxDQUNSLElBQUksVUFBVSxDQUFDLFdBQVcsQ0FBQyxFQUMzQixFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxFQUN2QyxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxDQUFDO2FBQzFDLG9CQUFvQixDQUNqQixFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxFQUNwQyxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxDQUFDO2FBQzdDLG9CQUFvQixDQUNqQixFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxFQUNsQyxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxDQUFDO2FBQzFDLG9CQUFvQixDQUNqQixFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxFQUNwQyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxDQUFDO2FBQ3hDLG9CQUFvQixDQUNqQixFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxFQUNyQyxFQUFFLFFBQVEsRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxDQUFDO2FBQzdDLG9CQUFvQixDQUNqQixFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxFQUN2QyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRSxDQUMxQzs7Y0FFQyxhQUFhLEdBQUcsSUFBSSxhQUFhLENBQUM7WUFDcEMsV0FBVyxFQUFFLE1BQU0sQ0FBQyxXQUFXO1lBQy9CLGFBQWEsRUFBRSxNQUFNLENBQUMsYUFBYTtZQUNuQyxVQUFVLEVBQUUsTUFBTSxDQUFDLFVBQVU7WUFDN0IsY0FBYyxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxFQUFFO1lBQ3JELGdCQUFnQjtTQUNuQixDQUFDO1FBRUYsT0FBTyxhQUFhLENBQUM7SUFDekIsQ0FBQzs7O1lBckdKLFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQjs7OztZQWhCb0IsUUFBUTtZQUNwQixPQUFPOzs7Ozs7OztJQW1CUiw2Q0FBMEI7Ozs7O0lBQzFCLDRDQUF3QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlLCBJbmplY3RvciwgRWxlbWVudFJlZiwgQ29tcG9uZW50UmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE92ZXJsYXksIE92ZXJsYXlDb25maWcsIE92ZXJsYXlSZWYgfSBmcm9tICdAYW5ndWxhci9jZGsvb3ZlcmxheSc7XHJcbmltcG9ydCB7IFBvcnRhbEluamVjdG9yLCBDb21wb25lbnRQb3J0YWwgfSBmcm9tICdAYW5ndWxhci9jZGsvcG9ydGFsJztcclxuaW1wb3J0IHsgQ29udGV4dE1lbnVPdmVybGF5UmVmIH0gZnJvbSAnLi9jb250ZXh0LW1lbnUtb3ZlcmxheSc7XHJcbmltcG9ydCB7IENvbnRleHRNZW51T3ZlcmxheUNvbmZpZyB9IGZyb20gJy4vaW50ZXJmYWNlcyc7XHJcbmltcG9ydCB7IENPTlRFWFRfTUVOVV9EQVRBIH0gZnJvbSAnLi9jb250ZXh0LW1lbnUudG9rZW5zJztcclxuaW1wb3J0IHsgQ29udGV4dE1lbnVMaXN0Q29tcG9uZW50IH0gZnJvbSAnLi9jb250ZXh0LW1lbnUtbGlzdC5jb21wb25lbnQnO1xyXG5cclxuY29uc3QgREVGQVVMVF9DT05GSUc6IENvbnRleHRNZW51T3ZlcmxheUNvbmZpZyA9IHtcclxuICAgIHBhbmVsQ2xhc3M6ICdjZGstb3ZlcmxheS1wYW5lJyxcclxuICAgIGJhY2tkcm9wQ2xhc3M6ICdjZGstb3ZlcmxheS10cmFuc3BhcmVudC1iYWNrZHJvcCcsXHJcbiAgICBoYXNCYWNrZHJvcDogdHJ1ZVxyXG59O1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb250ZXh0TWVudU92ZXJsYXlTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIGluamVjdG9yOiBJbmplY3RvcixcclxuICAgICAgICBwcml2YXRlIG92ZXJsYXk6IE92ZXJsYXlcclxuICAgICkge31cclxuXHJcbiAgICBvcGVuKGNvbmZpZzogQ29udGV4dE1lbnVPdmVybGF5Q29uZmlnKTogQ29udGV4dE1lbnVPdmVybGF5UmVmIHtcclxuICAgICAgICBjb25zdCBvdmVybGF5Q29uZmlnID0geyAuLi5ERUZBVUxUX0NPTkZJRywgLi4uY29uZmlnIH07XHJcblxyXG4gICAgICAgIGNvbnN0IG92ZXJsYXkgPSB0aGlzLmNyZWF0ZU92ZXJsYXkob3ZlcmxheUNvbmZpZyk7XHJcblxyXG4gICAgICAgIGNvbnN0IG92ZXJsYXlSZWYgPSBuZXcgQ29udGV4dE1lbnVPdmVybGF5UmVmKG92ZXJsYXkpO1xyXG5cclxuICAgICAgICB0aGlzLmF0dGFjaERpYWxvZ0NvbnRhaW5lcihvdmVybGF5LCBjb25maWcsIG92ZXJsYXlSZWYpO1xyXG5cclxuICAgICAgICBvdmVybGF5LmJhY2tkcm9wQ2xpY2soKS5zdWJzY3JpYmUoKCkgPT4gb3ZlcmxheVJlZi5jbG9zZSgpKTtcclxuXHJcbiAgICAgICAgLy8gcHJldmVudCBuYXRpdmUgY29udGV4dG1lbnUgb24gb3ZlcmxheSBlbGVtZW50IGlmIGNvbmZpZy5oYXNCYWNrZHJvcCBpcyB0cnVlXHJcbiAgICAgICAgaWYgKG92ZXJsYXlDb25maWcuaGFzQmFja2Ryb3ApIHtcclxuICAgICAgICAgICAgKDxhbnk+IG92ZXJsYXkpLl9iYWNrZHJvcEVsZW1lbnRcclxuICAgICAgICAgICAgICAgIC5hZGRFdmVudExpc3RlbmVyKCdjb250ZXh0bWVudScsIChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgKDxhbnk+IG92ZXJsYXkpLl9iYWNrZHJvcENsaWNrLm5leHQobnVsbCk7XHJcbiAgICAgICAgICAgICAgICB9LCB0cnVlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBvdmVybGF5UmVmO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgY3JlYXRlT3ZlcmxheShjb25maWc6IENvbnRleHRNZW51T3ZlcmxheUNvbmZpZyk6IE92ZXJsYXlSZWYge1xyXG4gICAgICAgIGNvbnN0IG92ZXJsYXlDb25maWcgPSB0aGlzLmdldE92ZXJsYXlDb25maWcoY29uZmlnKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5vdmVybGF5LmNyZWF0ZShvdmVybGF5Q29uZmlnKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGF0dGFjaERpYWxvZ0NvbnRhaW5lcihvdmVybGF5OiBPdmVybGF5UmVmLCBjb25maWc6IENvbnRleHRNZW51T3ZlcmxheUNvbmZpZywgY29udGV4dE1lbnVPdmVybGF5UmVmOiBDb250ZXh0TWVudU92ZXJsYXlSZWYpIHtcclxuICAgICAgICBjb25zdCBpbmplY3RvciA9IHRoaXMuY3JlYXRlSW5qZWN0b3IoY29uZmlnLCBjb250ZXh0TWVudU92ZXJsYXlSZWYpO1xyXG5cclxuICAgICAgICBjb25zdCBjb250YWluZXJQb3J0YWwgPSBuZXcgQ29tcG9uZW50UG9ydGFsKENvbnRleHRNZW51TGlzdENvbXBvbmVudCwgbnVsbCwgaW5qZWN0b3IpO1xyXG4gICAgICAgIGNvbnN0IGNvbnRhaW5lclJlZjogQ29tcG9uZW50UmVmPENvbnRleHRNZW51TGlzdENvbXBvbmVudD4gPSBvdmVybGF5LmF0dGFjaChjb250YWluZXJQb3J0YWwpO1xyXG5cclxuICAgICAgICByZXR1cm4gY29udGFpbmVyUmVmLmluc3RhbmNlO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgY3JlYXRlSW5qZWN0b3IoY29uZmlnOiBDb250ZXh0TWVudU92ZXJsYXlDb25maWcsIGNvbnRleHRNZW51T3ZlcmxheVJlZjogQ29udGV4dE1lbnVPdmVybGF5UmVmKTogUG9ydGFsSW5qZWN0b3Ige1xyXG4gICAgICAgIGNvbnN0IGluamVjdGlvblRva2VucyA9IG5ldyBXZWFrTWFwKCk7XHJcblxyXG4gICAgICAgIGluamVjdGlvblRva2Vucy5zZXQoQ29udGV4dE1lbnVPdmVybGF5UmVmLCBjb250ZXh0TWVudU92ZXJsYXlSZWYpO1xyXG4gICAgICAgIGluamVjdGlvblRva2Vucy5zZXQoQ09OVEVYVF9NRU5VX0RBVEEsIGNvbmZpZy5kYXRhKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBQb3J0YWxJbmplY3Rvcih0aGlzLmluamVjdG9yLCBpbmplY3Rpb25Ub2tlbnMpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0T3ZlcmxheUNvbmZpZyhjb25maWc6IENvbnRleHRNZW51T3ZlcmxheUNvbmZpZyk6IE92ZXJsYXlDb25maWcge1xyXG4gICAgICAgIGNvbnN0IHsgY2xpZW50WSwgY2xpZW50WCAgfSA9IGNvbmZpZy5zb3VyY2U7XHJcblxyXG4gICAgICAgIGNvbnN0IGZha2VFbGVtZW50OiBhbnkgPSB7XHJcbiAgICAgICAgICAgIGdldEJvdW5kaW5nQ2xpZW50UmVjdDogKCk6IENsaWVudFJlY3QgPT4gKHtcclxuICAgICAgICAgICAgICAgIGJvdHRvbTogY2xpZW50WSxcclxuICAgICAgICAgICAgICAgIGhlaWdodDogMCxcclxuICAgICAgICAgICAgICAgIGxlZnQ6IGNsaWVudFgsXHJcbiAgICAgICAgICAgICAgICByaWdodDogY2xpZW50WCxcclxuICAgICAgICAgICAgICAgIHRvcDogY2xpZW50WSxcclxuICAgICAgICAgICAgICAgIHdpZHRoOiAwXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgY29uc3QgcG9zaXRpb25TdHJhdGVneSA9IHRoaXMub3ZlcmxheS5wb3NpdGlvbigpXHJcbiAgICAgICAgICAgIC5jb25uZWN0ZWRUbyhcclxuICAgICAgICAgICAgICAgIG5ldyBFbGVtZW50UmVmKGZha2VFbGVtZW50KSxcclxuICAgICAgICAgICAgICAgIHsgb3JpZ2luWDogJ3N0YXJ0Jywgb3JpZ2luWTogJ2JvdHRvbScgfSxcclxuICAgICAgICAgICAgICAgIHsgb3ZlcmxheVg6ICdzdGFydCcsIG92ZXJsYXlZOiAndG9wJyB9KVxyXG4gICAgICAgICAgICAud2l0aEZhbGxiYWNrUG9zaXRpb24oXHJcbiAgICAgICAgICAgICAgICB7IG9yaWdpblg6ICdzdGFydCcsIG9yaWdpblk6ICd0b3AnIH0sXHJcbiAgICAgICAgICAgICAgICB7IG92ZXJsYXlYOiAnc3RhcnQnLCBvdmVybGF5WTogJ2JvdHRvbScgfSlcclxuICAgICAgICAgICAgLndpdGhGYWxsYmFja1Bvc2l0aW9uKFxyXG4gICAgICAgICAgICAgICAgeyBvcmlnaW5YOiAnZW5kJywgb3JpZ2luWTogJ3RvcCcgfSxcclxuICAgICAgICAgICAgICAgIHsgb3ZlcmxheVg6ICdzdGFydCcsIG92ZXJsYXlZOiAndG9wJyB9KVxyXG4gICAgICAgICAgICAud2l0aEZhbGxiYWNrUG9zaXRpb24oXHJcbiAgICAgICAgICAgICAgICB7IG9yaWdpblg6ICdzdGFydCcsIG9yaWdpblk6ICd0b3AnIH0sXHJcbiAgICAgICAgICAgICAgICB7IG92ZXJsYXlYOiAnZW5kJywgb3ZlcmxheVk6ICd0b3AnIH0pXHJcbiAgICAgICAgICAgIC53aXRoRmFsbGJhY2tQb3NpdGlvbihcclxuICAgICAgICAgICAgICAgIHsgb3JpZ2luWDogJ2VuZCcsIG9yaWdpblk6ICdjZW50ZXInIH0sXHJcbiAgICAgICAgICAgICAgICB7IG92ZXJsYXlYOiAnc3RhcnQnLCBvdmVybGF5WTogJ2NlbnRlcicgfSlcclxuICAgICAgICAgICAgLndpdGhGYWxsYmFja1Bvc2l0aW9uKFxyXG4gICAgICAgICAgICAgICAgeyBvcmlnaW5YOiAnc3RhcnQnLCBvcmlnaW5ZOiAnY2VudGVyJyB9LFxyXG4gICAgICAgICAgICAgICAgeyBvdmVybGF5WDogJ2VuZCcsIG92ZXJsYXlZOiAnY2VudGVyJyB9XHJcbiAgICAgICAgICAgICk7XHJcblxyXG4gICAgICAgIGNvbnN0IG92ZXJsYXlDb25maWcgPSBuZXcgT3ZlcmxheUNvbmZpZyh7XHJcbiAgICAgICAgICAgIGhhc0JhY2tkcm9wOiBjb25maWcuaGFzQmFja2Ryb3AsXHJcbiAgICAgICAgICAgIGJhY2tkcm9wQ2xhc3M6IGNvbmZpZy5iYWNrZHJvcENsYXNzLFxyXG4gICAgICAgICAgICBwYW5lbENsYXNzOiBjb25maWcucGFuZWxDbGFzcyxcclxuICAgICAgICAgICAgc2Nyb2xsU3RyYXRlZ3k6IHRoaXMub3ZlcmxheS5zY3JvbGxTdHJhdGVnaWVzLmNsb3NlKCksXHJcbiAgICAgICAgICAgIHBvc2l0aW9uU3RyYXRlZ3lcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG92ZXJsYXlDb25maWc7XHJcbiAgICB9XHJcbn1cclxuIl19