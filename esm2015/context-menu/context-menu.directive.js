/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:no-input-rename  */
import { Directive, HostListener, Input } from '@angular/core';
import { ContextMenuOverlayService } from './context-menu-overlay.service';
export class ContextMenuDirective {
    /**
     * @param {?} contextMenuService
     */
    constructor(contextMenuService) {
        this.contextMenuService = contextMenuService;
        /**
         * Is the menu enabled?
         */
        this.enabled = false;
    }
    /**
     * @param {?=} event
     * @return {?}
     */
    onShowContextMenu(event) {
        if (this.enabled) {
            if (event) {
                event.preventDefault();
            }
            if (this.links && this.links.length > 0) {
                this.contextMenuService.open({
                    source: event,
                    data: this.links
                });
            }
        }
    }
}
ContextMenuDirective.decorators = [
    { type: Directive, args: [{
                selector: '[adf-context-menu], [context-menu]'
            },] }
];
/** @nocollapse */
ContextMenuDirective.ctorParameters = () => [
    { type: ContextMenuOverlayService }
];
ContextMenuDirective.propDecorators = {
    links: [{ type: Input, args: ['adf-context-menu',] }],
    enabled: [{ type: Input, args: ['adf-context-menu-enabled',] }],
    onShowContextMenu: [{ type: HostListener, args: ['contextmenu', ['$event'],] }]
};
if (false) {
    /**
     * Items for the menu.
     * @type {?}
     */
    ContextMenuDirective.prototype.links;
    /**
     * Is the menu enabled?
     * @type {?}
     */
    ContextMenuDirective.prototype.enabled;
    /**
     * @type {?}
     * @private
     */
    ContextMenuDirective.prototype.contextMenuService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGV4dC1tZW51LmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNvbnRleHQtbWVudS9jb250ZXh0LW1lbnUuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0QsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFLM0UsTUFBTSxPQUFPLG9CQUFvQjs7OztJQVM3QixZQUFvQixrQkFBNkM7UUFBN0MsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUEyQjs7OztRQUZqRSxZQUFPLEdBQVksS0FBSyxDQUFDO0lBRTJDLENBQUM7Ozs7O0lBR3JFLGlCQUFpQixDQUFDLEtBQWtCO1FBQ2hDLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNkLElBQUksS0FBSyxFQUFFO2dCQUNQLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQzthQUMxQjtZQUVELElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3JDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUM7b0JBQ3pCLE1BQU0sRUFBRSxLQUFLO29CQUNiLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSztpQkFDbkIsQ0FBQyxDQUFDO2FBQ047U0FDSjtJQUNMLENBQUM7OztZQTVCSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLG9DQUFvQzthQUNqRDs7OztZQUpRLHlCQUF5Qjs7O29CQU83QixLQUFLLFNBQUMsa0JBQWtCO3NCQUl4QixLQUFLLFNBQUMsMEJBQTBCO2dDQUtoQyxZQUFZLFNBQUMsYUFBYSxFQUFFLENBQUMsUUFBUSxDQUFDOzs7Ozs7O0lBVHZDLHFDQUNhOzs7OztJQUdiLHVDQUN5Qjs7Ozs7SUFFYixrREFBcUQiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6bm8taW5wdXQtcmVuYW1lICAqL1xyXG5cclxuaW1wb3J0IHsgRGlyZWN0aXZlLCBIb3N0TGlzdGVuZXIsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbnRleHRNZW51T3ZlcmxheVNlcnZpY2UgfSBmcm9tICcuL2NvbnRleHQtbWVudS1vdmVybGF5LnNlcnZpY2UnO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1thZGYtY29udGV4dC1tZW51XSwgW2NvbnRleHQtbWVudV0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb250ZXh0TWVudURpcmVjdGl2ZSB7XHJcbiAgICAvKiogSXRlbXMgZm9yIHRoZSBtZW51LiAqL1xyXG4gICAgQElucHV0KCdhZGYtY29udGV4dC1tZW51JylcclxuICAgIGxpbmtzOiBhbnlbXTtcclxuXHJcbiAgICAvKiogSXMgdGhlIG1lbnUgZW5hYmxlZD8gKi9cclxuICAgIEBJbnB1dCgnYWRmLWNvbnRleHQtbWVudS1lbmFibGVkJylcclxuICAgIGVuYWJsZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNvbnRleHRNZW51U2VydmljZTogQ29udGV4dE1lbnVPdmVybGF5U2VydmljZSkge31cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdjb250ZXh0bWVudScsIFsnJGV2ZW50J10pXHJcbiAgICBvblNob3dDb250ZXh0TWVudShldmVudD86IE1vdXNlRXZlbnQpIHtcclxuICAgICAgICBpZiAodGhpcy5lbmFibGVkKSB7XHJcbiAgICAgICAgICAgIGlmIChldmVudCkge1xyXG4gICAgICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMubGlua3MgJiYgdGhpcy5saW5rcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRleHRNZW51U2VydmljZS5vcGVuKHtcclxuICAgICAgICAgICAgICAgICAgICBzb3VyY2U6IGV2ZW50LFxyXG4gICAgICAgICAgICAgICAgICAgIGRhdGE6IHRoaXMubGlua3NcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==