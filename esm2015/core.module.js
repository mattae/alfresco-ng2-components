/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule, TranslateLoader, TranslateStore, TranslateService } from '@ngx-translate/core';
import { MaterialModule } from './material.module';
import { AboutModule } from './about/about.module';
import { AppConfigModule } from './app-config/app-config.module';
import { CardViewModule } from './card-view/card-view.module';
import { ContextMenuModule } from './context-menu/context-menu.module';
import { DataColumnModule } from './data-column/data-column.module';
import { DataTableModule } from './datatable/datatable.module';
import { InfoDrawerModule } from './info-drawer/info-drawer.module';
import { LanguageMenuModule } from './language-menu/language-menu.module';
import { LoginModule } from './login/login.module';
import { PaginationModule } from './pagination/pagination.module';
import { HostSettingsModule } from './settings/host-settings.module';
import { ToolbarModule } from './toolbar/toolbar.module';
import { UserInfoModule } from './userinfo/userinfo.module';
import { ViewerModule } from './viewer/viewer.module';
import { FormBaseModule } from './form/form-base.module';
import { SidenavLayoutModule } from './layout/layout.module';
import { CommentsModule } from './comments/comments.module';
import { ButtonsMenuModule } from './buttons-menu/buttons-menu.module';
import { TemplateModule } from './templates/template.module';
import { ClipboardModule } from './clipboard/clipboard.module';
import { NotificationHistoryModule } from './notification-history/notification-history.module';
import { DirectiveModule } from './directives/directive.module';
import { DialogModule } from './dialogs/dialog.module';
import { PipeModule } from './pipes/pipe.module';
import { AlfrescoApiService } from './services/alfresco-api.service';
import { TranslationService } from './services/translation.service';
import { startupServiceFactory } from './services/startup-service-factory';
import { SortingPickerModule } from './sorting-picker/sorting-picker.module';
import { IconModule } from './icon/icon.module';
import { TranslateLoaderService } from './services/translate-loader.service';
import { ExtensionsModule } from '@alfresco/adf-extensions';
import { directionalityConfigFactory } from './services/directionality-config-factory';
import { DirectionalityConfigService } from './services/directionality-config.service';
export class CoreModule {
    /**
     * @param {?} translation
     */
    constructor(translation) {
        translation.addTranslationFolder('adf-core', 'assets/adf-core');
    }
    /**
     * @return {?}
     */
    static forRoot() {
        return {
            ngModule: CoreModule,
            providers: [
                TranslateStore,
                TranslateService,
                { provide: TranslateLoader, useClass: TranslateLoaderService },
                {
                    provide: APP_INITIALIZER,
                    useFactory: startupServiceFactory,
                    deps: [
                        AlfrescoApiService
                    ],
                    multi: true
                },
                {
                    provide: APP_INITIALIZER,
                    useFactory: directionalityConfigFactory,
                    deps: [DirectionalityConfigService],
                    multi: true
                }
            ]
        };
    }
    /**
     * @return {?}
     */
    static forChild() {
        return {
            ngModule: CoreModule
        };
    }
}
CoreModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    TranslateModule,
                    ExtensionsModule.forChild(),
                    AboutModule,
                    ViewerModule,
                    SidenavLayoutModule,
                    PipeModule,
                    CommonModule,
                    DirectiveModule,
                    DialogModule,
                    FormsModule,
                    ReactiveFormsModule,
                    HostSettingsModule,
                    UserInfoModule,
                    MaterialModule,
                    AppConfigModule,
                    PaginationModule,
                    ToolbarModule,
                    ContextMenuModule,
                    CardViewModule,
                    FormBaseModule,
                    CommentsModule,
                    LoginModule,
                    LanguageMenuModule,
                    InfoDrawerModule,
                    DataColumnModule,
                    DataTableModule,
                    ButtonsMenuModule,
                    TemplateModule,
                    IconModule,
                    SortingPickerModule,
                    NotificationHistoryModule
                ],
                exports: [
                    AboutModule,
                    ViewerModule,
                    SidenavLayoutModule,
                    PipeModule,
                    CommonModule,
                    DirectiveModule,
                    DialogModule,
                    ClipboardModule,
                    FormsModule,
                    ReactiveFormsModule,
                    HostSettingsModule,
                    UserInfoModule,
                    MaterialModule,
                    AppConfigModule,
                    PaginationModule,
                    ToolbarModule,
                    ContextMenuModule,
                    CardViewModule,
                    FormBaseModule,
                    CommentsModule,
                    LoginModule,
                    LanguageMenuModule,
                    InfoDrawerModule,
                    DataColumnModule,
                    DataTableModule,
                    TranslateModule,
                    ButtonsMenuModule,
                    TemplateModule,
                    SortingPickerModule,
                    IconModule,
                    NotificationHistoryModule
                ]
            },] }
];
/** @nocollapse */
CoreModule.ctorParameters = () => [
    { type: TranslationService }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29yZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJjb3JlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLGVBQWUsRUFBRSxRQUFRLEVBQXVCLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsZUFBZSxFQUFFLGVBQWUsRUFBRSxjQUFjLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUV6RyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbkQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNqRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDOUQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDdkUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDcEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBQy9ELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNuRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNsRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNyRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDekQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzVELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN0RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDekQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDN0QsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzVELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDL0QsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sb0RBQW9ELENBQUM7QUFFL0YsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQ2hFLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFakQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDckUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDcEUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDM0UsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDN0UsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ2hELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzVELE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBc0V2RixNQUFNLE9BQU8sVUFBVTs7OztJQWdDbkIsWUFBWSxXQUErQjtRQUN2QyxXQUFXLENBQUMsb0JBQW9CLENBQUMsVUFBVSxFQUFFLGlCQUFpQixDQUFDLENBQUM7SUFDcEUsQ0FBQzs7OztJQWpDRCxNQUFNLENBQUMsT0FBTztRQUNWLE9BQU87WUFDSCxRQUFRLEVBQUUsVUFBVTtZQUNwQixTQUFTLEVBQUU7Z0JBQ1AsY0FBYztnQkFDZCxnQkFBZ0I7Z0JBQ2hCLEVBQUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxRQUFRLEVBQUUsc0JBQXNCLEVBQUU7Z0JBQzlEO29CQUNJLE9BQU8sRUFBRSxlQUFlO29CQUN4QixVQUFVLEVBQUUscUJBQXFCO29CQUNqQyxJQUFJLEVBQUU7d0JBQ0Ysa0JBQWtCO3FCQUNyQjtvQkFDRCxLQUFLLEVBQUUsSUFBSTtpQkFDZDtnQkFDRDtvQkFDSSxPQUFPLEVBQUUsZUFBZTtvQkFDeEIsVUFBVSxFQUFFLDJCQUEyQjtvQkFDdkMsSUFBSSxFQUFFLENBQUUsMkJBQTJCLENBQUU7b0JBQ3JDLEtBQUssRUFBRSxJQUFJO2lCQUNkO2FBQ0o7U0FDSixDQUFDO0lBQ04sQ0FBQzs7OztJQUVELE1BQU0sQ0FBQyxRQUFRO1FBQ1gsT0FBTztZQUNILFFBQVEsRUFBRSxVQUFVO1NBQ3ZCLENBQUM7SUFDTixDQUFDOzs7WUFsR0osUUFBUSxTQUFDO2dCQUNOLE9BQU8sRUFBRTtvQkFDTCxlQUFlO29CQUNmLGdCQUFnQixDQUFDLFFBQVEsRUFBRTtvQkFDM0IsV0FBVztvQkFDWCxZQUFZO29CQUNaLG1CQUFtQjtvQkFDbkIsVUFBVTtvQkFDVixZQUFZO29CQUNaLGVBQWU7b0JBQ2YsWUFBWTtvQkFDWixXQUFXO29CQUNYLG1CQUFtQjtvQkFDbkIsa0JBQWtCO29CQUNsQixjQUFjO29CQUNkLGNBQWM7b0JBQ2QsZUFBZTtvQkFDZixnQkFBZ0I7b0JBQ2hCLGFBQWE7b0JBQ2IsaUJBQWlCO29CQUNqQixjQUFjO29CQUNkLGNBQWM7b0JBQ2QsY0FBYztvQkFDZCxXQUFXO29CQUNYLGtCQUFrQjtvQkFDbEIsZ0JBQWdCO29CQUNoQixnQkFBZ0I7b0JBQ2hCLGVBQWU7b0JBQ2YsaUJBQWlCO29CQUNqQixjQUFjO29CQUNkLFVBQVU7b0JBQ1YsbUJBQW1CO29CQUNuQix5QkFBeUI7aUJBQzVCO2dCQUNELE9BQU8sRUFBRTtvQkFDTCxXQUFXO29CQUNYLFlBQVk7b0JBQ1osbUJBQW1CO29CQUNuQixVQUFVO29CQUNWLFlBQVk7b0JBQ1osZUFBZTtvQkFDZixZQUFZO29CQUNaLGVBQWU7b0JBQ2YsV0FBVztvQkFDWCxtQkFBbUI7b0JBQ25CLGtCQUFrQjtvQkFDbEIsY0FBYztvQkFDZCxjQUFjO29CQUNkLGVBQWU7b0JBQ2YsZ0JBQWdCO29CQUNoQixhQUFhO29CQUNiLGlCQUFpQjtvQkFDakIsY0FBYztvQkFDZCxjQUFjO29CQUNkLGNBQWM7b0JBQ2QsV0FBVztvQkFDWCxrQkFBa0I7b0JBQ2xCLGdCQUFnQjtvQkFDaEIsZ0JBQWdCO29CQUNoQixlQUFlO29CQUNmLGVBQWU7b0JBQ2YsaUJBQWlCO29CQUNqQixjQUFjO29CQUNkLG1CQUFtQjtvQkFDbkIsVUFBVTtvQkFDVix5QkFBeUI7aUJBQzVCO2FBQ0o7Ozs7WUE1RVEsa0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IEFQUF9JTklUSUFMSVpFUiwgTmdNb2R1bGUsIE1vZHVsZVdpdGhQcm92aWRlcnMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybXNNb2R1bGUsIFJlYWN0aXZlRm9ybXNNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZU1vZHVsZSwgVHJhbnNsYXRlTG9hZGVyLCBUcmFuc2xhdGVTdG9yZSwgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuL21hdGVyaWFsLm1vZHVsZSc7XHJcbmltcG9ydCB7IEFib3V0TW9kdWxlIH0gZnJvbSAnLi9hYm91dC9hYm91dC5tb2R1bGUnO1xyXG5pbXBvcnQgeyBBcHBDb25maWdNb2R1bGUgfSBmcm9tICcuL2FwcC1jb25maWcvYXBwLWNvbmZpZy5tb2R1bGUnO1xyXG5pbXBvcnQgeyBDYXJkVmlld01vZHVsZSB9IGZyb20gJy4vY2FyZC12aWV3L2NhcmQtdmlldy5tb2R1bGUnO1xyXG5pbXBvcnQgeyBDb250ZXh0TWVudU1vZHVsZSB9IGZyb20gJy4vY29udGV4dC1tZW51L2NvbnRleHQtbWVudS5tb2R1bGUnO1xyXG5pbXBvcnQgeyBEYXRhQ29sdW1uTW9kdWxlIH0gZnJvbSAnLi9kYXRhLWNvbHVtbi9kYXRhLWNvbHVtbi5tb2R1bGUnO1xyXG5pbXBvcnQgeyBEYXRhVGFibGVNb2R1bGUgfSBmcm9tICcuL2RhdGF0YWJsZS9kYXRhdGFibGUubW9kdWxlJztcclxuaW1wb3J0IHsgSW5mb0RyYXdlck1vZHVsZSB9IGZyb20gJy4vaW5mby1kcmF3ZXIvaW5mby1kcmF3ZXIubW9kdWxlJztcclxuaW1wb3J0IHsgTGFuZ3VhZ2VNZW51TW9kdWxlIH0gZnJvbSAnLi9sYW5ndWFnZS1tZW51L2xhbmd1YWdlLW1lbnUubW9kdWxlJztcclxuaW1wb3J0IHsgTG9naW5Nb2R1bGUgfSBmcm9tICcuL2xvZ2luL2xvZ2luLm1vZHVsZSc7XHJcbmltcG9ydCB7IFBhZ2luYXRpb25Nb2R1bGUgfSBmcm9tICcuL3BhZ2luYXRpb24vcGFnaW5hdGlvbi5tb2R1bGUnO1xyXG5pbXBvcnQgeyBIb3N0U2V0dGluZ3NNb2R1bGUgfSBmcm9tICcuL3NldHRpbmdzL2hvc3Qtc2V0dGluZ3MubW9kdWxlJztcclxuaW1wb3J0IHsgVG9vbGJhck1vZHVsZSB9IGZyb20gJy4vdG9vbGJhci90b29sYmFyLm1vZHVsZSc7XHJcbmltcG9ydCB7IFVzZXJJbmZvTW9kdWxlIH0gZnJvbSAnLi91c2VyaW5mby91c2VyaW5mby5tb2R1bGUnO1xyXG5pbXBvcnQgeyBWaWV3ZXJNb2R1bGUgfSBmcm9tICcuL3ZpZXdlci92aWV3ZXIubW9kdWxlJztcclxuaW1wb3J0IHsgRm9ybUJhc2VNb2R1bGUgfSBmcm9tICcuL2Zvcm0vZm9ybS1iYXNlLm1vZHVsZSc7XHJcbmltcG9ydCB7IFNpZGVuYXZMYXlvdXRNb2R1bGUgfSBmcm9tICcuL2xheW91dC9sYXlvdXQubW9kdWxlJztcclxuaW1wb3J0IHsgQ29tbWVudHNNb2R1bGUgfSBmcm9tICcuL2NvbW1lbnRzL2NvbW1lbnRzLm1vZHVsZSc7XHJcbmltcG9ydCB7IEJ1dHRvbnNNZW51TW9kdWxlIH0gZnJvbSAnLi9idXR0b25zLW1lbnUvYnV0dG9ucy1tZW51Lm1vZHVsZSc7XHJcbmltcG9ydCB7IFRlbXBsYXRlTW9kdWxlIH0gZnJvbSAnLi90ZW1wbGF0ZXMvdGVtcGxhdGUubW9kdWxlJztcclxuaW1wb3J0IHsgQ2xpcGJvYXJkTW9kdWxlIH0gZnJvbSAnLi9jbGlwYm9hcmQvY2xpcGJvYXJkLm1vZHVsZSc7XHJcbmltcG9ydCB7IE5vdGlmaWNhdGlvbkhpc3RvcnlNb2R1bGUgfSBmcm9tICcuL25vdGlmaWNhdGlvbi1oaXN0b3J5L25vdGlmaWNhdGlvbi1oaXN0b3J5Lm1vZHVsZSc7XHJcblxyXG5pbXBvcnQgeyBEaXJlY3RpdmVNb2R1bGUgfSBmcm9tICcuL2RpcmVjdGl2ZXMvZGlyZWN0aXZlLm1vZHVsZSc7XHJcbmltcG9ydCB7IERpYWxvZ01vZHVsZSB9IGZyb20gJy4vZGlhbG9ncy9kaWFsb2cubW9kdWxlJztcclxuaW1wb3J0IHsgUGlwZU1vZHVsZSB9IGZyb20gJy4vcGlwZXMvcGlwZS5tb2R1bGUnO1xyXG5cclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi9zZXJ2aWNlcy9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IFRyYW5zbGF0aW9uU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvdHJhbnNsYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IHN0YXJ0dXBTZXJ2aWNlRmFjdG9yeSB9IGZyb20gJy4vc2VydmljZXMvc3RhcnR1cC1zZXJ2aWNlLWZhY3RvcnknO1xyXG5pbXBvcnQgeyBTb3J0aW5nUGlja2VyTW9kdWxlIH0gZnJvbSAnLi9zb3J0aW5nLXBpY2tlci9zb3J0aW5nLXBpY2tlci5tb2R1bGUnO1xyXG5pbXBvcnQgeyBJY29uTW9kdWxlIH0gZnJvbSAnLi9pY29uL2ljb24ubW9kdWxlJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlTG9hZGVyU2VydmljZSB9IGZyb20gJy4vc2VydmljZXMvdHJhbnNsYXRlLWxvYWRlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRXh0ZW5zaW9uc01vZHVsZSB9IGZyb20gJ0BhbGZyZXNjby9hZGYtZXh0ZW5zaW9ucyc7XHJcbmltcG9ydCB7IGRpcmVjdGlvbmFsaXR5Q29uZmlnRmFjdG9yeSB9IGZyb20gJy4vc2VydmljZXMvZGlyZWN0aW9uYWxpdHktY29uZmlnLWZhY3RvcnknO1xyXG5pbXBvcnQgeyBEaXJlY3Rpb25hbGl0eUNvbmZpZ1NlcnZpY2UgfSBmcm9tICcuL3NlcnZpY2VzL2RpcmVjdGlvbmFsaXR5LWNvbmZpZy5zZXJ2aWNlJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgVHJhbnNsYXRlTW9kdWxlLFxyXG4gICAgICAgIEV4dGVuc2lvbnNNb2R1bGUuZm9yQ2hpbGQoKSxcclxuICAgICAgICBBYm91dE1vZHVsZSxcclxuICAgICAgICBWaWV3ZXJNb2R1bGUsXHJcbiAgICAgICAgU2lkZW5hdkxheW91dE1vZHVsZSxcclxuICAgICAgICBQaXBlTW9kdWxlLFxyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICBEaXJlY3RpdmVNb2R1bGUsXHJcbiAgICAgICAgRGlhbG9nTW9kdWxlLFxyXG4gICAgICAgIEZvcm1zTW9kdWxlLFxyXG4gICAgICAgIFJlYWN0aXZlRm9ybXNNb2R1bGUsXHJcbiAgICAgICAgSG9zdFNldHRpbmdzTW9kdWxlLFxyXG4gICAgICAgIFVzZXJJbmZvTW9kdWxlLFxyXG4gICAgICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgICAgIEFwcENvbmZpZ01vZHVsZSxcclxuICAgICAgICBQYWdpbmF0aW9uTW9kdWxlLFxyXG4gICAgICAgIFRvb2xiYXJNb2R1bGUsXHJcbiAgICAgICAgQ29udGV4dE1lbnVNb2R1bGUsXHJcbiAgICAgICAgQ2FyZFZpZXdNb2R1bGUsXHJcbiAgICAgICAgRm9ybUJhc2VNb2R1bGUsXHJcbiAgICAgICAgQ29tbWVudHNNb2R1bGUsXHJcbiAgICAgICAgTG9naW5Nb2R1bGUsXHJcbiAgICAgICAgTGFuZ3VhZ2VNZW51TW9kdWxlLFxyXG4gICAgICAgIEluZm9EcmF3ZXJNb2R1bGUsXHJcbiAgICAgICAgRGF0YUNvbHVtbk1vZHVsZSxcclxuICAgICAgICBEYXRhVGFibGVNb2R1bGUsXHJcbiAgICAgICAgQnV0dG9uc01lbnVNb2R1bGUsXHJcbiAgICAgICAgVGVtcGxhdGVNb2R1bGUsXHJcbiAgICAgICAgSWNvbk1vZHVsZSxcclxuICAgICAgICBTb3J0aW5nUGlja2VyTW9kdWxlLFxyXG4gICAgICAgIE5vdGlmaWNhdGlvbkhpc3RvcnlNb2R1bGVcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgQWJvdXRNb2R1bGUsXHJcbiAgICAgICAgVmlld2VyTW9kdWxlLFxyXG4gICAgICAgIFNpZGVuYXZMYXlvdXRNb2R1bGUsXHJcbiAgICAgICAgUGlwZU1vZHVsZSxcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgRGlyZWN0aXZlTW9kdWxlLFxyXG4gICAgICAgIERpYWxvZ01vZHVsZSxcclxuICAgICAgICBDbGlwYm9hcmRNb2R1bGUsXHJcbiAgICAgICAgRm9ybXNNb2R1bGUsXHJcbiAgICAgICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcclxuICAgICAgICBIb3N0U2V0dGluZ3NNb2R1bGUsXHJcbiAgICAgICAgVXNlckluZm9Nb2R1bGUsXHJcbiAgICAgICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICAgICAgQXBwQ29uZmlnTW9kdWxlLFxyXG4gICAgICAgIFBhZ2luYXRpb25Nb2R1bGUsXHJcbiAgICAgICAgVG9vbGJhck1vZHVsZSxcclxuICAgICAgICBDb250ZXh0TWVudU1vZHVsZSxcclxuICAgICAgICBDYXJkVmlld01vZHVsZSxcclxuICAgICAgICBGb3JtQmFzZU1vZHVsZSxcclxuICAgICAgICBDb21tZW50c01vZHVsZSxcclxuICAgICAgICBMb2dpbk1vZHVsZSxcclxuICAgICAgICBMYW5ndWFnZU1lbnVNb2R1bGUsXHJcbiAgICAgICAgSW5mb0RyYXdlck1vZHVsZSxcclxuICAgICAgICBEYXRhQ29sdW1uTW9kdWxlLFxyXG4gICAgICAgIERhdGFUYWJsZU1vZHVsZSxcclxuICAgICAgICBUcmFuc2xhdGVNb2R1bGUsXHJcbiAgICAgICAgQnV0dG9uc01lbnVNb2R1bGUsXHJcbiAgICAgICAgVGVtcGxhdGVNb2R1bGUsXHJcbiAgICAgICAgU29ydGluZ1BpY2tlck1vZHVsZSxcclxuICAgICAgICBJY29uTW9kdWxlLFxyXG4gICAgICAgIE5vdGlmaWNhdGlvbkhpc3RvcnlNb2R1bGVcclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIENvcmVNb2R1bGUge1xyXG4gICAgc3RhdGljIGZvclJvb3QoKTogTW9kdWxlV2l0aFByb3ZpZGVycyB7XHJcbiAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgbmdNb2R1bGU6IENvcmVNb2R1bGUsXHJcbiAgICAgICAgICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgICAgICAgICAgVHJhbnNsYXRlU3RvcmUsXHJcbiAgICAgICAgICAgICAgICBUcmFuc2xhdGVTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgeyBwcm92aWRlOiBUcmFuc2xhdGVMb2FkZXIsIHVzZUNsYXNzOiBUcmFuc2xhdGVMb2FkZXJTZXJ2aWNlIH0sXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgcHJvdmlkZTogQVBQX0lOSVRJQUxJWkVSLFxyXG4gICAgICAgICAgICAgICAgICAgIHVzZUZhY3Rvcnk6IHN0YXJ0dXBTZXJ2aWNlRmFjdG9yeSxcclxuICAgICAgICAgICAgICAgICAgICBkZXBzOiBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIEFsZnJlc2NvQXBpU2VydmljZVxyXG4gICAgICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICAgICAgbXVsdGk6IHRydWVcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgcHJvdmlkZTogQVBQX0lOSVRJQUxJWkVSLFxyXG4gICAgICAgICAgICAgICAgICAgIHVzZUZhY3Rvcnk6IGRpcmVjdGlvbmFsaXR5Q29uZmlnRmFjdG9yeSxcclxuICAgICAgICAgICAgICAgICAgICBkZXBzOiBbIERpcmVjdGlvbmFsaXR5Q29uZmlnU2VydmljZSBdLFxyXG4gICAgICAgICAgICAgICAgICAgIG11bHRpOiB0cnVlXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIF1cclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBmb3JDaGlsZCgpOiBNb2R1bGVXaXRoUHJvdmlkZXJzIHtcclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICBuZ01vZHVsZTogQ29yZU1vZHVsZVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IodHJhbnNsYXRpb246IFRyYW5zbGF0aW9uU2VydmljZSkge1xyXG4gICAgICAgIHRyYW5zbGF0aW9uLmFkZFRyYW5zbGF0aW9uRm9sZGVyKCdhZGYtY29yZScsICdhc3NldHMvYWRmLWNvcmUnKTtcclxuICAgIH1cclxufVxyXG4iXX0=