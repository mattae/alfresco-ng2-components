/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector no-input-rename  */
import { Component, ContentChild, Input, TemplateRef } from '@angular/core';
export class DataColumnComponent {
    constructor() {
        /**
         * Value type for the column. Possible settings are 'text', 'image',
         * 'date', 'fileSize', 'location', and 'json'.
         */
        this.type = 'text';
        /**
         * Toggles ability to sort by this column, for example by clicking the column header.
         */
        this.sortable = true;
        /**
         * Display title of the column, typically used for column headers. You can use the
         * i18n resource key to get it translated automatically.
         */
        this.title = '';
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (!this.srTitle && this.key === '$thumbnail') {
            this.srTitle = 'Thumbnail';
        }
    }
}
DataColumnComponent.decorators = [
    { type: Component, args: [{
                selector: 'data-column',
                template: ''
            }] }
];
DataColumnComponent.propDecorators = {
    key: [{ type: Input }],
    type: [{ type: Input }],
    format: [{ type: Input }],
    sortable: [{ type: Input }],
    title: [{ type: Input }],
    template: [{ type: ContentChild, args: [TemplateRef, { static: true },] }],
    formatTooltip: [{ type: Input }],
    srTitle: [{ type: Input, args: ['sr-title',] }],
    cssClass: [{ type: Input, args: ['class',] }],
    copyContent: [{ type: Input }]
};
if (false) {
    /**
     * Data source key. Can be either a column/property key like `title`
     *  or a property path like `createdBy.name`.
     * @type {?}
     */
    DataColumnComponent.prototype.key;
    /**
     * Value type for the column. Possible settings are 'text', 'image',
     * 'date', 'fileSize', 'location', and 'json'.
     * @type {?}
     */
    DataColumnComponent.prototype.type;
    /**
     * Value format (if supported by the parent component), for example format of the date.
     * @type {?}
     */
    DataColumnComponent.prototype.format;
    /**
     * Toggles ability to sort by this column, for example by clicking the column header.
     * @type {?}
     */
    DataColumnComponent.prototype.sortable;
    /**
     * Display title of the column, typically used for column headers. You can use the
     * i18n resource key to get it translated automatically.
     * @type {?}
     */
    DataColumnComponent.prototype.title;
    /** @type {?} */
    DataColumnComponent.prototype.template;
    /**
     * Custom tooltip formatter function.
     * @type {?}
     */
    DataColumnComponent.prototype.formatTooltip;
    /**
     * Title to be used for screen readers.
     * @type {?}
     */
    DataColumnComponent.prototype.srTitle;
    /**
     * Additional CSS class to be applied to column (header and cells).
     * @type {?}
     */
    DataColumnComponent.prototype.cssClass;
    /**
     * Enables/disables a Clipboard directive to allow copying of cell contents.
     * @type {?}
     */
    DataColumnComponent.prototype.copyContent;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS1jb2x1bW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZGF0YS1jb2x1bW4vZGF0YS1jb2x1bW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQ0EsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFVLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU1wRixNQUFNLE9BQU8sbUJBQW1CO0lBSmhDOzs7OztRQWdCSSxTQUFJLEdBQVcsTUFBTSxDQUFDOzs7O1FBUXRCLGFBQVEsR0FBWSxJQUFJLENBQUM7Ozs7O1FBTXpCLFVBQUssR0FBVyxFQUFFLENBQUM7SUEwQnZCLENBQUM7Ozs7SUFMRyxRQUFRO1FBQ0osSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLEdBQUcsS0FBSyxZQUFZLEVBQUU7WUFDNUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxXQUFXLENBQUM7U0FDOUI7SUFDTCxDQUFDOzs7WUF2REosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxhQUFhO2dCQUN2QixRQUFRLEVBQUUsRUFBRTthQUNmOzs7a0JBTUksS0FBSzttQkFNTCxLQUFLO3FCQUlMLEtBQUs7dUJBSUwsS0FBSztvQkFNTCxLQUFLO3VCQUdMLFlBQVksU0FBQyxXQUFXLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDOzRCQUl4QyxLQUFLO3NCQUlMLEtBQUssU0FBQyxVQUFVO3VCQUloQixLQUFLLFNBQUMsT0FBTzswQkFJYixLQUFLOzs7Ozs7OztJQXZDTixrQ0FDWTs7Ozs7O0lBS1osbUNBQ3NCOzs7OztJQUd0QixxQ0FDZTs7Ozs7SUFHZix1Q0FDeUI7Ozs7OztJQUt6QixvQ0FDbUI7O0lBRW5CLHVDQUNjOzs7OztJQUdkLDRDQUN3Qjs7Ozs7SUFHeEIsc0NBQ2dCOzs7OztJQUdoQix1Q0FDaUI7Ozs7O0lBR2pCLDBDQUNxQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxuXG4vKiFcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxuICpcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cbiAqL1xuXG4gLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yIG5vLWlucHV0LXJlbmFtZSAgKi9cblxuaW1wb3J0IHsgQ29tcG9uZW50LCBDb250ZW50Q2hpbGQsIElucHV0LCBPbkluaXQsIFRlbXBsYXRlUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnZGF0YS1jb2x1bW4nLFxuICAgIHRlbXBsYXRlOiAnJ1xufSlcbmV4cG9ydCBjbGFzcyBEYXRhQ29sdW1uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAgIC8qKiBEYXRhIHNvdXJjZSBrZXkuIENhbiBiZSBlaXRoZXIgYSBjb2x1bW4vcHJvcGVydHkga2V5IGxpa2UgYHRpdGxlYFxuICAgICAqICBvciBhIHByb3BlcnR5IHBhdGggbGlrZSBgY3JlYXRlZEJ5Lm5hbWVgLlxuICAgICAqL1xuICAgIEBJbnB1dCgpXG4gICAga2V5OiBzdHJpbmc7XG5cbiAgICAvKiogVmFsdWUgdHlwZSBmb3IgdGhlIGNvbHVtbi4gUG9zc2libGUgc2V0dGluZ3MgYXJlICd0ZXh0JywgJ2ltYWdlJyxcbiAgICAgKiAnZGF0ZScsICdmaWxlU2l6ZScsICdsb2NhdGlvbicsIGFuZCAnanNvbicuXG4gICAgICovXG4gICAgQElucHV0KClcbiAgICB0eXBlOiBzdHJpbmcgPSAndGV4dCc7XG5cbiAgICAvKiogVmFsdWUgZm9ybWF0IChpZiBzdXBwb3J0ZWQgYnkgdGhlIHBhcmVudCBjb21wb25lbnQpLCBmb3IgZXhhbXBsZSBmb3JtYXQgb2YgdGhlIGRhdGUuICovXG4gICAgQElucHV0KClcbiAgICBmb3JtYXQ6IHN0cmluZztcblxuICAgIC8qKiBUb2dnbGVzIGFiaWxpdHkgdG8gc29ydCBieSB0aGlzIGNvbHVtbiwgZm9yIGV4YW1wbGUgYnkgY2xpY2tpbmcgdGhlIGNvbHVtbiBoZWFkZXIuICovXG4gICAgQElucHV0KClcbiAgICBzb3J0YWJsZTogYm9vbGVhbiA9IHRydWU7XG5cbiAgICAvKiogRGlzcGxheSB0aXRsZSBvZiB0aGUgY29sdW1uLCB0eXBpY2FsbHkgdXNlZCBmb3IgY29sdW1uIGhlYWRlcnMuIFlvdSBjYW4gdXNlIHRoZVxuICAgICAqIGkxOG4gcmVzb3VyY2Uga2V5IHRvIGdldCBpdCB0cmFuc2xhdGVkIGF1dG9tYXRpY2FsbHkuXG4gICAgICovXG4gICAgQElucHV0KClcbiAgICB0aXRsZTogc3RyaW5nID0gJyc7XG5cbiAgICBAQ29udGVudENoaWxkKFRlbXBsYXRlUmVmLCB7c3RhdGljOiB0cnVlfSlcbiAgICB0ZW1wbGF0ZTogYW55O1xuXG4gICAgLyoqIEN1c3RvbSB0b29sdGlwIGZvcm1hdHRlciBmdW5jdGlvbi4gKi9cbiAgICBASW5wdXQoKVxuICAgIGZvcm1hdFRvb2x0aXA6IEZ1bmN0aW9uO1xuXG4gICAgLyoqIFRpdGxlIHRvIGJlIHVzZWQgZm9yIHNjcmVlbiByZWFkZXJzLiAqL1xuICAgIEBJbnB1dCgnc3ItdGl0bGUnKVxuICAgIHNyVGl0bGU6IHN0cmluZztcblxuICAgIC8qKiBBZGRpdGlvbmFsIENTUyBjbGFzcyB0byBiZSBhcHBsaWVkIHRvIGNvbHVtbiAoaGVhZGVyIGFuZCBjZWxscykuICovXG4gICAgQElucHV0KCdjbGFzcycpXG4gICAgY3NzQ2xhc3M6IHN0cmluZztcblxuICAgICAvKiogRW5hYmxlcy9kaXNhYmxlcyBhIENsaXBib2FyZCBkaXJlY3RpdmUgdG8gYWxsb3cgY29weWluZyBvZiBjZWxsIGNvbnRlbnRzLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgY29weUNvbnRlbnQ6IGJvb2xlYW47XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgaWYgKCF0aGlzLnNyVGl0bGUgJiYgdGhpcy5rZXkgPT09ICckdGh1bWJuYWlsJykge1xuICAgICAgICAgICAgdGhpcy5zclRpdGxlID0gJ1RodW1ibmFpbCc7XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=