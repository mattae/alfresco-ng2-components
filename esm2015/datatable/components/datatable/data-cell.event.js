/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { BaseEvent } from '../../../events';
export class DataCellEventModel {
    /**
     * @param {?} row
     * @param {?} col
     * @param {?} actions
     */
    constructor(row, col, actions) {
        this.row = row;
        this.col = col;
        this.actions = actions || [];
    }
}
if (false) {
    /** @type {?} */
    DataCellEventModel.prototype.row;
    /** @type {?} */
    DataCellEventModel.prototype.col;
    /** @type {?} */
    DataCellEventModel.prototype.actions;
}
export class DataCellEvent extends BaseEvent {
    /**
     * @param {?} row
     * @param {?} col
     * @param {?} actions
     */
    constructor(row, col, actions) {
        super();
        this.value = new DataCellEventModel(row, col, actions);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS1jZWxsLmV2ZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZGF0YXRhYmxlL2NvbXBvbmVudHMvZGF0YXRhYmxlL2RhdGEtY2VsbC5ldmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFJNUMsTUFBTSxPQUFPLGtCQUFrQjs7Ozs7O0lBTTNCLFlBQVksR0FBWSxFQUFFLEdBQWUsRUFBRSxPQUFjO1FBQ3JELElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBQ2YsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDZixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sSUFBSSxFQUFFLENBQUM7SUFDakMsQ0FBQztDQUVKOzs7SUFWRyxpQ0FBc0I7O0lBQ3RCLGlDQUF5Qjs7SUFDekIscUNBQWU7O0FBVW5CLE1BQU0sT0FBTyxhQUFjLFNBQVEsU0FBNkI7Ozs7OztJQUU1RCxZQUFZLEdBQVksRUFBRSxHQUFlLEVBQUUsT0FBYztRQUNyRCxLQUFLLEVBQUUsQ0FBQztRQUNSLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQzNELENBQUM7Q0FFSiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBCYXNlRXZlbnQgfSBmcm9tICcuLi8uLi8uLi9ldmVudHMnO1xyXG5pbXBvcnQgeyBEYXRhQ29sdW1uIH0gZnJvbSAnLi4vLi4vZGF0YS9kYXRhLWNvbHVtbi5tb2RlbCc7XHJcbmltcG9ydCB7IERhdGFSb3cgfSBmcm9tICcuLi8uLi9kYXRhL2RhdGEtcm93Lm1vZGVsJztcclxuXHJcbmV4cG9ydCBjbGFzcyBEYXRhQ2VsbEV2ZW50TW9kZWwge1xyXG5cclxuICAgIHJlYWRvbmx5IHJvdzogRGF0YVJvdztcclxuICAgIHJlYWRvbmx5IGNvbDogRGF0YUNvbHVtbjtcclxuICAgIGFjdGlvbnM6IGFueVtdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHJvdzogRGF0YVJvdywgY29sOiBEYXRhQ29sdW1uLCBhY3Rpb25zOiBhbnlbXSkge1xyXG4gICAgICAgIHRoaXMucm93ID0gcm93O1xyXG4gICAgICAgIHRoaXMuY29sID0gY29sO1xyXG4gICAgICAgIHRoaXMuYWN0aW9ucyA9IGFjdGlvbnMgfHwgW107XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgRGF0YUNlbGxFdmVudCBleHRlbmRzIEJhc2VFdmVudDxEYXRhQ2VsbEV2ZW50TW9kZWw+IHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihyb3c6IERhdGFSb3csIGNvbDogRGF0YUNvbHVtbiwgYWN0aW9uczogYW55W10pIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIHRoaXMudmFsdWUgPSBuZXcgRGF0YUNlbGxFdmVudE1vZGVsKHJvdywgY29sLCBhY3Rpb25zKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19