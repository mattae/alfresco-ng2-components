/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { BaseEvent } from '../../../events';
export class DataRowActionModel {
    /**
     * @param {?} row
     * @param {?} action
     */
    constructor(row, action) {
        this.row = row;
        this.action = action;
    }
}
if (false) {
    /** @type {?} */
    DataRowActionModel.prototype.row;
    /** @type {?} */
    DataRowActionModel.prototype.action;
}
export class DataRowActionEvent extends BaseEvent {
    // backwards compatibility with 1.2.0 and earlier
    /**
     * @return {?}
     */
    get args() {
        return this.value;
    }
    /**
     * @param {?} row
     * @param {?} action
     */
    constructor(row, action) {
        super();
        this.value = new DataRowActionModel(row, action);
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS1yb3ctYWN0aW9uLmV2ZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZGF0YXRhYmxlL2NvbXBvbmVudHMvZGF0YXRhYmxlL2RhdGEtcm93LWFjdGlvbi5ldmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFHNUMsTUFBTSxPQUFPLGtCQUFrQjs7Ozs7SUFLM0IsWUFBWSxHQUFZLEVBQUUsTUFBVztRQUNqQyxJQUFJLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztRQUNmLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO0lBQ3pCLENBQUM7Q0FDSjs7O0lBUEcsaUNBQWE7O0lBQ2Isb0NBQVk7O0FBUWhCLE1BQU0sT0FBTyxrQkFBbUIsU0FBUSxTQUE2Qjs7Ozs7SUFHakUsSUFBSSxJQUFJO1FBQ0osT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBRUQsWUFBWSxHQUFZLEVBQUUsTUFBVztRQUNqQyxLQUFLLEVBQUUsQ0FBQztRQUNSLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxrQkFBa0IsQ0FBQyxHQUFHLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDckQsQ0FBQztDQUVKIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEJhc2VFdmVudCB9IGZyb20gJy4uLy4uLy4uL2V2ZW50cyc7XHJcbmltcG9ydCB7IERhdGFSb3cgfSBmcm9tICcuLi8uLi9kYXRhL2RhdGEtcm93Lm1vZGVsJztcclxuXHJcbmV4cG9ydCBjbGFzcyBEYXRhUm93QWN0aW9uTW9kZWwge1xyXG5cclxuICAgIHJvdzogRGF0YVJvdztcclxuICAgIGFjdGlvbjogYW55O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHJvdzogRGF0YVJvdywgYWN0aW9uOiBhbnkpIHtcclxuICAgICAgICB0aGlzLnJvdyA9IHJvdztcclxuICAgICAgICB0aGlzLmFjdGlvbiA9IGFjdGlvbjtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIERhdGFSb3dBY3Rpb25FdmVudCBleHRlbmRzIEJhc2VFdmVudDxEYXRhUm93QWN0aW9uTW9kZWw+IHtcclxuXHJcbiAgICAvLyBiYWNrd2FyZHMgY29tcGF0aWJpbGl0eSB3aXRoIDEuMi4wIGFuZCBlYXJsaWVyXHJcbiAgICBnZXQgYXJncygpOiBEYXRhUm93QWN0aW9uTW9kZWwge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHJvdzogRGF0YVJvdywgYWN0aW9uOiBhbnkpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIHRoaXMudmFsdWUgPSBuZXcgRGF0YVJvd0FjdGlvbk1vZGVsKHJvdywgYWN0aW9uKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19