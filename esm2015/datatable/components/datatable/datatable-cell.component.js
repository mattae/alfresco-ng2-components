/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { AlfrescoApiService } from '../../../services/alfresco-api.service';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
export class DataTableCellComponent {
    /**
     * @param {?} alfrescoApiService
     */
    constructor(alfrescoApiService) {
        this.alfrescoApiService = alfrescoApiService;
        this.value$ = new BehaviorSubject('');
        this.onDestroy$ = new Subject();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.updateValue();
        this.alfrescoApiService.nodeUpdated
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} node
         * @return {?}
         */
        node => {
            if (this.row) {
                if (this.row['node'].entry.id === node.id) {
                    this.row['node'].entry = node;
                    this.row['cache'][this.column.key] = this.column.key.split('.').reduce((/**
                     * @param {?} source
                     * @param {?} key
                     * @return {?}
                     */
                    (source, key) => source[key]), node);
                    this.updateValue();
                }
            }
        }));
    }
    /**
     * @protected
     * @return {?}
     */
    updateValue() {
        if (this.column && this.column.key && this.row && this.data) {
            /** @type {?} */
            const value = this.data.getValue(this.row, this.column);
            this.value$.next(value);
            if (!this.tooltip) {
                this.tooltip = value;
            }
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    }
}
DataTableCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-datatable-cell',
                changeDetection: ChangeDetectionStrategy.OnPush,
                template: `
        <ng-container>
            <span *ngIf="copyContent; else defaultCell"
                adf-clipboard="CLIPBOARD.CLICK_TO_COPY"
                [clipboard-notification]="'CLIPBOARD.SUCCESS_COPY'"
                [attr.aria-label]="value$ | async"
                [title]="tooltip"
                class="adf-datatable-cell-value"
                >{{ value$ | async }}</span>
        </ng-container>
        <ng-template #defaultCell>
            <span
                [attr.aria-label]="value$ | async"
                [title]="tooltip"
                class="adf-datatable-cell-value"
            >{{ value$ | async }}</span>
        </ng-template>
    `,
                encapsulation: ViewEncapsulation.None,
                host: { class: 'adf-datatable-content-cell' }
            }] }
];
/** @nocollapse */
DataTableCellComponent.ctorParameters = () => [
    { type: AlfrescoApiService }
];
DataTableCellComponent.propDecorators = {
    data: [{ type: Input }],
    column: [{ type: Input }],
    row: [{ type: Input }],
    copyContent: [{ type: Input }],
    tooltip: [{ type: Input }]
};
if (false) {
    /**
     * Data table adapter instance.
     * @type {?}
     */
    DataTableCellComponent.prototype.data;
    /**
     * Data that defines the column.
     * @type {?}
     */
    DataTableCellComponent.prototype.column;
    /**
     * Data that defines the row.
     * @type {?}
     */
    DataTableCellComponent.prototype.row;
    /** @type {?} */
    DataTableCellComponent.prototype.value$;
    /**
     * Enables/disables a Clipboard directive to allow copying of the cell's content.
     * @type {?}
     */
    DataTableCellComponent.prototype.copyContent;
    /**
     * Text for the cell's tooltip.
     * @type {?}
     */
    DataTableCellComponent.prototype.tooltip;
    /**
     * @type {?}
     * @protected
     */
    DataTableCellComponent.prototype.onDestroy$;
    /**
     * @type {?}
     * @protected
     */
    DataTableCellComponent.prototype.alfrescoApiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YXRhYmxlLWNlbGwuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZGF0YXRhYmxlL2NvbXBvbmVudHMvZGF0YXRhYmxlL2RhdGF0YWJsZS1jZWxsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQ0gsdUJBQXVCLEVBQ3ZCLFNBQVMsRUFDVCxLQUFLLEVBRUwsaUJBQWlCLEVBRXBCLE1BQU0sZUFBZSxDQUFDO0FBSXZCLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxlQUFlLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ2hELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQTBCM0MsTUFBTSxPQUFPLHNCQUFzQjs7OztJQXlCL0IsWUFBc0Isa0JBQXNDO1FBQXRDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFaNUQsV0FBTSxHQUFHLElBQUksZUFBZSxDQUFNLEVBQUUsQ0FBQyxDQUFDO1FBVTVCLGVBQVUsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDO0lBRWlCLENBQUM7Ozs7SUFFaEUsUUFBUTtRQUNKLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNuQixJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVzthQUM5QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNoQyxTQUFTOzs7O1FBQUMsSUFBSSxDQUFDLEVBQUU7WUFDZCxJQUFJLElBQUksQ0FBQyxHQUFHLEVBQUU7Z0JBQ1YsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLEtBQUssSUFBSSxDQUFDLEVBQUUsRUFBRTtvQkFDdkMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxNQUFNLENBQUMsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO29CQUM5QixJQUFJLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLE1BQU07Ozs7O29CQUFDLENBQUMsTUFBTSxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFFLElBQUksQ0FBQyxDQUFDO29CQUMzRyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7aUJBQ3RCO2FBQ0o7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNYLENBQUM7Ozs7O0lBRVMsV0FBVztRQUNqQixJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFOztrQkFDbkQsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQztZQUV2RCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUV4QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDZixJQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUN4QjtTQUNKO0lBQ0wsQ0FBQzs7OztJQUVELFdBQVc7UUFDUCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQy9CLENBQUM7OztZQWpGSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07Z0JBQy9DLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7Ozs7S0FpQlQ7Z0JBQ0QsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7Z0JBQ3JDLElBQUksRUFBRSxFQUFFLEtBQUssRUFBRSw0QkFBNEIsRUFBRTthQUNoRDs7OztZQTNCUSxrQkFBa0I7OzttQkE4QnRCLEtBQUs7cUJBSUwsS0FBSztrQkFJTCxLQUFLOzBCQU1MLEtBQUs7c0JBSUwsS0FBSzs7Ozs7OztJQWxCTixzQ0FDdUI7Ozs7O0lBR3ZCLHdDQUNtQjs7Ozs7SUFHbkIscUNBQ2E7O0lBRWIsd0NBQXNDOzs7OztJQUd0Qyw2Q0FDcUI7Ozs7O0lBR3JCLHlDQUNnQjs7Ozs7SUFFaEIsNENBQThDOzs7OztJQUVsQyxvREFBZ0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHtcclxuICAgIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LFxyXG4gICAgQ29tcG9uZW50LFxyXG4gICAgSW5wdXQsXHJcbiAgICBPbkluaXQsXHJcbiAgICBWaWV3RW5jYXBzdWxhdGlvbixcclxuICAgIE9uRGVzdHJveVxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEYXRhQ29sdW1uIH0gZnJvbSAnLi4vLi4vZGF0YS9kYXRhLWNvbHVtbi5tb2RlbCc7XHJcbmltcG9ydCB7IERhdGFSb3cgfSBmcm9tICcuLi8uLi9kYXRhL2RhdGEtcm93Lm1vZGVsJztcclxuaW1wb3J0IHsgRGF0YVRhYmxlQWRhcHRlciB9IGZyb20gJy4uLy4uL2RhdGEvZGF0YXRhYmxlLWFkYXB0ZXInO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IEJlaGF2aW9yU3ViamVjdCwgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLWRhdGF0YWJsZS1jZWxsJyxcclxuICAgIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxyXG4gICAgdGVtcGxhdGU6IGBcclxuICAgICAgICA8bmctY29udGFpbmVyPlxyXG4gICAgICAgICAgICA8c3BhbiAqbmdJZj1cImNvcHlDb250ZW50OyBlbHNlIGRlZmF1bHRDZWxsXCJcclxuICAgICAgICAgICAgICAgIGFkZi1jbGlwYm9hcmQ9XCJDTElQQk9BUkQuQ0xJQ0tfVE9fQ09QWVwiXHJcbiAgICAgICAgICAgICAgICBbY2xpcGJvYXJkLW5vdGlmaWNhdGlvbl09XCInQ0xJUEJPQVJELlNVQ0NFU1NfQ09QWSdcIlxyXG4gICAgICAgICAgICAgICAgW2F0dHIuYXJpYS1sYWJlbF09XCJ2YWx1ZSQgfCBhc3luY1wiXHJcbiAgICAgICAgICAgICAgICBbdGl0bGVdPVwidG9vbHRpcFwiXHJcbiAgICAgICAgICAgICAgICBjbGFzcz1cImFkZi1kYXRhdGFibGUtY2VsbC12YWx1ZVwiXHJcbiAgICAgICAgICAgICAgICA+e3sgdmFsdWUkIHwgYXN5bmMgfX08L3NwYW4+XHJcbiAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgICAgPG5nLXRlbXBsYXRlICNkZWZhdWx0Q2VsbD5cclxuICAgICAgICAgICAgPHNwYW5cclxuICAgICAgICAgICAgICAgIFthdHRyLmFyaWEtbGFiZWxdPVwidmFsdWUkIHwgYXN5bmNcIlxyXG4gICAgICAgICAgICAgICAgW3RpdGxlXT1cInRvb2x0aXBcIlxyXG4gICAgICAgICAgICAgICAgY2xhc3M9XCJhZGYtZGF0YXRhYmxlLWNlbGwtdmFsdWVcIlxyXG4gICAgICAgICAgICA+e3sgdmFsdWUkIHwgYXN5bmMgfX08L3NwYW4+XHJcbiAgICAgICAgPC9uZy10ZW1wbGF0ZT5cclxuICAgIGAsXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gICAgaG9zdDogeyBjbGFzczogJ2FkZi1kYXRhdGFibGUtY29udGVudC1jZWxsJyB9XHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEYXRhVGFibGVDZWxsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG4gICAgLyoqIERhdGEgdGFibGUgYWRhcHRlciBpbnN0YW5jZS4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBkYXRhOiBEYXRhVGFibGVBZGFwdGVyO1xyXG5cclxuICAgIC8qKiBEYXRhIHRoYXQgZGVmaW5lcyB0aGUgY29sdW1uLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGNvbHVtbjogRGF0YUNvbHVtbjtcclxuXHJcbiAgICAvKiogRGF0YSB0aGF0IGRlZmluZXMgdGhlIHJvdy4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICByb3c6IERhdGFSb3c7XHJcblxyXG4gICAgdmFsdWUkID0gbmV3IEJlaGF2aW9yU3ViamVjdDxhbnk+KCcnKTtcclxuXHJcbiAgICAvKiogRW5hYmxlcy9kaXNhYmxlcyBhIENsaXBib2FyZCBkaXJlY3RpdmUgdG8gYWxsb3cgY29weWluZyBvZiB0aGUgY2VsbCdzIGNvbnRlbnQuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgY29weUNvbnRlbnQ6IGJvb2xlYW47XHJcblxyXG4gICAgLyoqIFRleHQgZm9yIHRoZSBjZWxsJ3MgdG9vbHRpcC4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICB0b29sdGlwOiBzdHJpbmc7XHJcblxyXG4gICAgcHJvdGVjdGVkIG9uRGVzdHJveSQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBhbGZyZXNjb0FwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSkge31cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLnVwZGF0ZVZhbHVlKCk7XHJcbiAgICAgICAgdGhpcy5hbGZyZXNjb0FwaVNlcnZpY2Uubm9kZVVwZGF0ZWRcclxuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMub25EZXN0cm95JCkpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUobm9kZSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5yb3cpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5yb3dbJ25vZGUnXS5lbnRyeS5pZCA9PT0gbm9kZS5pZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJvd1snbm9kZSddLmVudHJ5ID0gbm9kZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3dbJ2NhY2hlJ11bdGhpcy5jb2x1bW4ua2V5XSA9IHRoaXMuY29sdW1uLmtleS5zcGxpdCgnLicpLnJlZHVjZSgoc291cmNlLCBrZXkpID0+IHNvdXJjZVtrZXldLCBub2RlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51cGRhdGVWYWx1ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJvdGVjdGVkIHVwZGF0ZVZhbHVlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvbHVtbiAmJiB0aGlzLmNvbHVtbi5rZXkgJiYgdGhpcy5yb3cgJiYgdGhpcy5kYXRhKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gdGhpcy5kYXRhLmdldFZhbHVlKHRoaXMucm93LCB0aGlzLmNvbHVtbik7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnZhbHVlJC5uZXh0KHZhbHVlKTtcclxuXHJcbiAgICAgICAgICAgIGlmICghdGhpcy50b29sdGlwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRvb2x0aXAgPSB2YWx1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQubmV4dCh0cnVlKTtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQuY29tcGxldGUoKTtcclxuICAgIH1cclxufVxyXG4iXX0=