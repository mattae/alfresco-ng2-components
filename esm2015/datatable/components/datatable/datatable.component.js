/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ContentChild, ElementRef, EventEmitter, Input, IterableDiffers, Output, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';
import { DataColumnListComponent } from '../../../data-column/data-column-list.component';
import { DataRowEvent } from '../../data/data-row-event.model';
import { DataSorting } from '../../data/data-sorting.model';
import { ObjectDataRow } from '../../data/object-datarow.model';
import { ObjectDataTableAdapter } from '../../data/object-datatable-adapter';
import { DataCellEvent } from './data-cell.event';
import { DataRowActionEvent } from './data-row-action.event';
import { share, buffer, map, filter, debounceTime } from 'rxjs/operators';
/** @enum {string} */
const DisplayMode = {
    List: 'list',
    Gallery: 'gallery',
};
export { DisplayMode };
export class DataTableComponent {
    /**
     * @param {?} elementRef
     * @param {?} differs
     */
    constructor(elementRef, differs) {
        this.elementRef = elementRef;
        /**
         * Selects the display mode of the table. Can be "list" or "gallery".
         */
        this.display = DisplayMode.List;
        /**
         * The rows that the datatable will show.
         */
        this.rows = [];
        /**
         * Define the sort order of the datatable. Possible values are :
         * [`created`, `desc`], [`created`, `asc`], [`due`, `desc`], [`due`, `asc`]
         */
        this.sorting = [];
        /**
         * The columns that the datatable will show.
         */
        this.columns = [];
        /**
         * Row selection mode. Can be none, `single` or `multiple`. For `multiple` mode,
         * you can use Cmd (macOS) or Ctrl (Win) modifier key to toggle selection for multiple rows.
         */
        this.selectionMode = 'single'; // none|single|multiple
        // none|single|multiple
        /**
         * Toggles multiple row selection, which renders checkboxes at the beginning of each row.
         */
        this.multiselect = false;
        /**
         * Toggles the data actions column.
         */
        this.actions = false;
        /**
         * Position of the actions dropdown menu. Can be "left" or "right".
         */
        this.actionsPosition = 'right'; // left|right
        /**
         * Toggles custom context menu for the component.
         */
        this.contextMenu = false;
        /**
         * Toggles file drop support for rows (see
         * [Upload directive](upload.directive.md) for further details).
         */
        this.allowDropFiles = false;
        /**
         * The CSS class to apply to every row.
         */
        this.rowStyleClass = '';
        /**
         * Toggles the header.
         */
        this.showHeader = true;
        /**
         * Toggles the sticky header mode.
         */
        this.stickyHeader = false;
        /**
         * Emitted when the user clicks a row.
         */
        this.rowClick = new EventEmitter();
        /**
         * Emitted when the user double-clicks a row.
         */
        this.rowDblClick = new EventEmitter();
        /**
         * Emitted before the context menu is displayed for a row.
         */
        this.showRowContextMenu = new EventEmitter();
        /**
         * Emitted before the actions menu is displayed for a row.
         */
        this.showRowActionsMenu = new EventEmitter();
        /**
         * Emitted when the user executes a row action.
         */
        this.executeRowAction = new EventEmitter();
        /**
         * Flag that indicates if the datatable is in loading state and needs to show the
         * loading template (see the docs to learn how to configure a loading template).
         */
        this.loading = false;
        /**
         * Flag that indicates if the datatable should show the "no permission" template.
         */
        this.noPermission = false;
        /**
         * Should the items for the row actions menu be cached for reuse after they are loaded
         * the first time?
         */
        this.rowMenuCacheEnabled = true;
        this.isSelectAllChecked = false;
        this.selection = new Array();
        /**
         * This array of fake rows fix the flex layout for the gallery view
         */
        this.fakeRows = [];
        this.rowMenuCache = {};
        this.subscriptions = [];
        if (differs) {
            this.differ = differs.find([]).create(null);
        }
        this.click$ = new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        (observer) => this.clickObserver = observer))
            .pipe(share());
    }
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        if (this.columnList) {
            this.subscriptions.push(this.columnList.columns.changes.subscribe((/**
             * @return {?}
             */
            () => {
                this.setTableSchema();
            })));
        }
        this.datatableLayoutFix();
        this.setTableSchema();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        this.initAndSubscribeClickStream();
        if (this.isPropertyChanged(changes['data'])) {
            if (this.isTableEmpty()) {
                this.initTable();
            }
            else {
                this.data = changes['data'].currentValue;
                this.resetSelection();
            }
            return;
        }
        if (this.isPropertyChanged(changes['rows'])) {
            if (this.isTableEmpty()) {
                this.initTable();
            }
            else {
                this.setTableRows(changes['rows'].currentValue);
            }
            return;
        }
        if (changes.selectionMode && !changes.selectionMode.isFirstChange()) {
            this.resetSelection();
            this.emitRowSelectionEvent('row-unselect', null);
        }
        if (this.isPropertyChanged(changes['sorting'])) {
            this.setTableSorting(changes['sorting'].currentValue);
        }
        if (this.isPropertyChanged(changes['display'])) {
            this.datatableLayoutFix();
        }
    }
    /**
     * @return {?}
     */
    ngDoCheck() {
        /** @type {?} */
        const changes = this.differ.diff(this.rows);
        if (changes) {
            this.setTableRows(this.rows);
        }
    }
    /**
     * @param {?} property
     * @return {?}
     */
    isPropertyChanged(property) {
        return property && property.currentValue ? true : false;
    }
    /**
     * @param {?} rows
     * @return {?}
     */
    convertToRowsData(rows) {
        return rows.map((/**
         * @param {?} row
         * @return {?}
         */
        (row) => new ObjectDataRow(row, row.isSelected)));
    }
    /**
     * @param {?} sorting
     * @return {?}
     */
    convertToDataSorting(sorting) {
        if (sorting && sorting.length > 0) {
            return new DataSorting(sorting[0], sorting[1]);
        }
    }
    /**
     * @private
     * @return {?}
     */
    initAndSubscribeClickStream() {
        this.unsubscribeClickStream();
        /** @type {?} */
        const singleClickStream = this.click$
            .pipe(buffer(this.click$.pipe(debounceTime(250))), map((/**
         * @param {?} list
         * @return {?}
         */
        (list) => list)), filter((/**
         * @param {?} x
         * @return {?}
         */
        (x) => x.length === 1)));
        this.singleClickStreamSub = singleClickStream.subscribe((/**
         * @param {?} dataRowEvents
         * @return {?}
         */
        (dataRowEvents) => {
            /** @type {?} */
            const event = dataRowEvents[0];
            this.handleRowSelection(event.value, (/** @type {?} */ (event.event)));
            this.rowClick.emit(event);
            if (!event.defaultPrevented) {
                this.elementRef.nativeElement.dispatchEvent(new CustomEvent('row-click', {
                    detail: event,
                    bubbles: true
                }));
            }
        }));
        /** @type {?} */
        const multiClickStream = this.click$
            .pipe(buffer(this.click$.pipe(debounceTime(250))), map((/**
         * @param {?} list
         * @return {?}
         */
        (list) => list)), filter((/**
         * @param {?} x
         * @return {?}
         */
        (x) => x.length >= 2)));
        this.multiClickStreamSub = multiClickStream.subscribe((/**
         * @param {?} dataRowEvents
         * @return {?}
         */
        (dataRowEvents) => {
            /** @type {?} */
            const event = dataRowEvents[0];
            this.rowDblClick.emit(event);
            if (!event.defaultPrevented) {
                this.elementRef.nativeElement.dispatchEvent(new CustomEvent('row-dblclick', {
                    detail: event,
                    bubbles: true
                }));
            }
        }));
    }
    /**
     * @private
     * @return {?}
     */
    unsubscribeClickStream() {
        if (this.singleClickStreamSub) {
            this.singleClickStreamSub.unsubscribe();
            this.singleClickStreamSub = null;
        }
        if (this.multiClickStreamSub) {
            this.multiClickStreamSub.unsubscribe();
            this.multiClickStreamSub = null;
        }
    }
    /**
     * @private
     * @return {?}
     */
    initTable() {
        this.data = new ObjectDataTableAdapter(this.rows, this.columns);
        this.setTableSorting(this.sorting);
        this.resetSelection();
        this.rowMenuCache = {};
    }
    /**
     * @return {?}
     */
    isTableEmpty() {
        return this.data === undefined || this.data === null;
    }
    /**
     * @private
     * @param {?} rows
     * @return {?}
     */
    setTableRows(rows) {
        if (this.data) {
            this.resetSelection();
            this.data.setRows(this.convertToRowsData(rows));
        }
    }
    /**
     * @private
     * @return {?}
     */
    setTableSchema() {
        /** @type {?} */
        let schema = [];
        if (!this.columns || this.columns.length === 0) {
            schema = this.getSchemaFromHtml();
        }
        else {
            schema = this.columns.concat(this.getSchemaFromHtml());
        }
        this.columns = schema;
        if (this.data && this.columns && this.columns.length > 0) {
            this.data.setColumns(this.columns);
        }
    }
    /**
     * @private
     * @param {?} sorting
     * @return {?}
     */
    setTableSorting(sorting) {
        if (this.data) {
            this.data.setSorting(this.convertToDataSorting(sorting));
        }
    }
    /**
     * @return {?}
     */
    getSchemaFromHtml() {
        /** @type {?} */
        let schema = [];
        if (this.columnList && this.columnList.columns && this.columnList.columns.length > 0) {
            schema = this.columnList.columns.map((/**
             * @param {?} c
             * @return {?}
             */
            (c) => (/** @type {?} */ (c))));
        }
        return schema;
    }
    /**
     * @param {?} row
     * @param {?} mouseEvent
     * @return {?}
     */
    onRowClick(row, mouseEvent) {
        if (mouseEvent) {
            mouseEvent.preventDefault();
        }
        if (row) {
            /** @type {?} */
            const dataRowEvent = new DataRowEvent(row, mouseEvent, this);
            this.clickObserver.next(dataRowEvent);
        }
    }
    /**
     * @param {?} row
     * @param {?} e
     * @return {?}
     */
    onEnterKeyPressed(row, e) {
        if (row) {
            this.handleRowSelection(row, e);
        }
    }
    /**
     * @private
     * @param {?} row
     * @param {?} e
     * @return {?}
     */
    handleRowSelection(row, e) {
        if (this.data) {
            if (this.isSingleSelectionMode()) {
                this.resetSelection();
                this.selectRow(row, true);
                this.emitRowSelectionEvent('row-select', row);
            }
            if (this.isMultiSelectionMode()) {
                /** @type {?} */
                const modifier = e && (e.metaKey || e.ctrlKey);
                /** @type {?} */
                let newValue;
                if (this.selection.length === 1) {
                    newValue = !row.isSelected;
                }
                else {
                    newValue = modifier ? !row.isSelected : true;
                }
                /** @type {?} */
                const domEventName = newValue ? 'row-select' : 'row-unselect';
                if (!modifier) {
                    this.resetSelection();
                }
                this.selectRow(row, newValue);
                this.emitRowSelectionEvent(domEventName, row);
            }
        }
    }
    /**
     * @return {?}
     */
    resetSelection() {
        if (this.data) {
            /** @type {?} */
            const rows = this.data.getRows();
            if (rows && rows.length > 0) {
                rows.forEach((/**
                 * @param {?} r
                 * @return {?}
                 */
                (r) => r.isSelected = false));
            }
            this.selection = [];
        }
        this.isSelectAllChecked = false;
    }
    /**
     * @param {?} row
     * @param {?=} event
     * @return {?}
     */
    onRowDblClick(row, event) {
        if (event) {
            event.preventDefault();
        }
        /** @type {?} */
        const dataRowEvent = new DataRowEvent(row, event, this);
        this.clickObserver.next(dataRowEvent);
    }
    /**
     * @param {?} row
     * @param {?} e
     * @return {?}
     */
    onRowKeyUp(row, e) {
        /** @type {?} */
        const event = new CustomEvent('row-keyup', {
            detail: {
                row: row,
                keyboardEvent: e,
                sender: this
            },
            bubbles: true
        });
        this.elementRef.nativeElement.dispatchEvent(event);
        if (event.defaultPrevented) {
            e.preventDefault();
        }
        else {
            if (e.key === 'Enter') {
                this.onKeyboardNavigate(row, e);
            }
        }
    }
    /**
     * @private
     * @param {?} row
     * @param {?} keyboardEvent
     * @return {?}
     */
    onKeyboardNavigate(row, keyboardEvent) {
        if (keyboardEvent) {
            keyboardEvent.preventDefault();
        }
        /** @type {?} */
        const event = new DataRowEvent(row, keyboardEvent, this);
        this.rowDblClick.emit(event);
        this.elementRef.nativeElement.dispatchEvent(new CustomEvent('row-dblclick', {
            detail: event,
            bubbles: true
        }));
    }
    /**
     * @param {?} column
     * @return {?}
     */
    onColumnHeaderClick(column) {
        if (column && column.sortable) {
            /** @type {?} */
            const current = this.data.getSorting();
            /** @type {?} */
            let newDirection = 'asc';
            if (current && column.key === current.key) {
                newDirection = current.direction === 'asc' ? 'desc' : 'asc';
            }
            this.data.setSorting(new DataSorting(column.key, newDirection));
            this.emitSortingChangedEvent(column.key, newDirection);
        }
    }
    /**
     * @param {?} matCheckboxChange
     * @return {?}
     */
    onSelectAllClick(matCheckboxChange) {
        this.isSelectAllChecked = matCheckboxChange.checked;
        if (this.multiselect) {
            /** @type {?} */
            const rows = this.data.getRows();
            if (rows && rows.length > 0) {
                for (let i = 0; i < rows.length; i++) {
                    this.selectRow(rows[i], matCheckboxChange.checked);
                }
            }
            /** @type {?} */
            const domEventName = matCheckboxChange.checked ? 'row-select' : 'row-unselect';
            /** @type {?} */
            const row = this.selection.length > 0 ? this.selection[0] : null;
            this.emitRowSelectionEvent(domEventName, row);
        }
    }
    /**
     * @param {?} row
     * @param {?} event
     * @return {?}
     */
    onCheckboxChange(row, event) {
        /** @type {?} */
        const newValue = event.checked;
        this.selectRow(row, newValue);
        /** @type {?} */
        const domEventName = newValue ? 'row-select' : 'row-unselect';
        this.emitRowSelectionEvent(domEventName, row);
    }
    /**
     * @param {?} event
     * @param {?} row
     * @return {?}
     */
    onImageLoadingError(event, row) {
        if (event) {
            /** @type {?} */
            const element = (/** @type {?} */ (event.target));
            if (this.fallbackThumbnail) {
                element.src = this.fallbackThumbnail;
            }
            else {
                element.src = row.imageErrorResolver(event);
            }
        }
    }
    /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    isIconValue(row, col) {
        if (row && col) {
            /** @type {?} */
            const value = row.getValue(col.key);
            return value && value.startsWith('material-icons://');
        }
        return false;
    }
    /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    asIconValue(row, col) {
        if (this.isIconValue(row, col)) {
            /** @type {?} */
            const value = row.getValue(col.key) || '';
            return value.replace('material-icons://', '');
        }
        return null;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    iconAltTextKey(value) {
        return value ? 'ICONS.' + value.substring(value.lastIndexOf('/') + 1).replace(/\.[a-z]+/, '') : '';
    }
    /**
     * @param {?} col
     * @param {?} direction
     * @return {?}
     */
    isColumnSorted(col, direction) {
        if (col && direction) {
            /** @type {?} */
            const sorting = this.data.getSorting();
            return sorting && sorting.key === col.key && sorting.direction === direction;
        }
        return false;
    }
    /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    getContextMenuActions(row, col) {
        /** @type {?} */
        const event = new DataCellEvent(row, col, []);
        this.showRowContextMenu.emit(event);
        return event.value.actions;
    }
    /**
     * @param {?} row
     * @param {?=} col
     * @return {?}
     */
    getRowActions(row, col) {
        /** @type {?} */
        const id = row.getValue('id');
        if (!this.rowMenuCache[id]) {
            /** @type {?} */
            const event = new DataCellEvent(row, col, []);
            this.showRowActionsMenu.emit(event);
            if (!this.rowMenuCacheEnabled) {
                return event.value.actions;
            }
            this.rowMenuCache[id] = event.value.actions;
        }
        return this.rowMenuCache[id];
    }
    /**
     * @param {?} row
     * @param {?} action
     * @return {?}
     */
    onExecuteRowAction(row, action) {
        if (action.disabled || action.disabled) {
            event.stopPropagation();
        }
        else {
            this.executeRowAction.emit(new DataRowActionEvent(row, action));
        }
    }
    /**
     * @param {?} row
     * @return {?}
     */
    rowAllowsDrop(row) {
        return row.isDropTarget === true;
    }
    /**
     * @return {?}
     */
    hasSelectionMode() {
        return this.isSingleSelectionMode() || this.isMultiSelectionMode();
    }
    /**
     * @return {?}
     */
    isSingleSelectionMode() {
        return this.selectionMode && this.selectionMode.toLowerCase() === 'single';
    }
    /**
     * @return {?}
     */
    isMultiSelectionMode() {
        return this.selectionMode && this.selectionMode.toLowerCase() === 'multiple';
    }
    /**
     * @param {?} row
     * @return {?}
     */
    getRowStyle(row) {
        row.cssClass = row.cssClass ? row.cssClass : '';
        this.rowStyleClass = this.rowStyleClass ? this.rowStyleClass : '';
        return `${row.cssClass} ${this.rowStyleClass}`;
    }
    /**
     * @return {?}
     */
    getSortingKey() {
        if (this.data.getSorting()) {
            return this.data.getSorting().key;
        }
    }
    /**
     * @param {?} row
     * @param {?} value
     * @return {?}
     */
    selectRow(row, value) {
        if (row) {
            row.isSelected = value;
            /** @type {?} */
            const idx = this.selection.indexOf(row);
            if (value) {
                if (idx < 0) {
                    this.selection.push(row);
                }
            }
            else {
                if (idx > -1) {
                    this.selection.splice(idx, 1);
                }
            }
        }
    }
    /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    getCellTooltip(row, col) {
        if (row && col && col.formatTooltip) {
            /** @type {?} */
            const result = col.formatTooltip(row, col);
            if (result) {
                return result;
            }
        }
        return null;
    }
    /**
     * @return {?}
     */
    getSortableColumns() {
        return this.data.getColumns().filter((/**
         * @param {?} column
         * @return {?}
         */
        (column) => {
            return column.sortable === true;
        }));
    }
    /**
     * @return {?}
     */
    isEmpty() {
        return this.data.getRows().length === 0;
    }
    /**
     * @return {?}
     */
    isHeaderVisible() {
        return !this.loading && !this.isEmpty() && !this.noPermission;
    }
    /**
     * @return {?}
     */
    isStickyHeaderEnabled() {
        return this.stickyHeader && this.isHeaderVisible();
    }
    /**
     * @private
     * @param {?} name
     * @param {?} row
     * @return {?}
     */
    emitRowSelectionEvent(name, row) {
        /** @type {?} */
        const domEvent = new CustomEvent(name, {
            detail: {
                row: row,
                selection: this.selection
            },
            bubbles: true
        });
        this.elementRef.nativeElement.dispatchEvent(domEvent);
    }
    /**
     * @private
     * @param {?} key
     * @param {?} direction
     * @return {?}
     */
    emitSortingChangedEvent(key, direction) {
        /** @type {?} */
        const domEvent = new CustomEvent('sorting-changed', {
            detail: {
                key,
                direction
            },
            bubbles: true
        });
        this.elementRef.nativeElement.dispatchEvent(domEvent);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.unsubscribeClickStream();
        this.subscriptions.forEach((/**
         * @param {?} s
         * @return {?}
         */
        (s) => s.unsubscribe()));
        this.subscriptions = [];
        if (this.dataRowsChanged) {
            this.dataRowsChanged.unsubscribe();
            this.dataRowsChanged = null;
        }
    }
    /**
     * @return {?}
     */
    datatableLayoutFix() {
        /** @type {?} */
        const maxGalleryRows = 25;
        if (this.display === 'gallery') {
            for (let i = 0; i < maxGalleryRows; i++) {
                this.fakeRows.push('');
            }
        }
        else {
            this.fakeRows = [];
        }
    }
    /**
     * @return {?}
     */
    getNameColumnValue() {
        return this.data.getColumns().find((/**
         * @param {?} el
         * @return {?}
         */
        (el) => {
            return el.key.includes('name');
        }));
    }
    /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    getAutomationValue(row, col) {
        /** @type {?} */
        const name = this.getNameColumnValue();
        return name ? row.getValue(name.key) : '';
    }
}
DataTableComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-datatable',
                template: "<div\r\n    role=\"grid\"\r\n    *ngIf=\"data\" class=\"adf-full-width\"\r\n    [class.adf-datatable-card]=\"display === 'gallery'\"\r\n    [class.adf-datatable-list]=\"display === 'list'\"\r\n    [class.adf-sticky-header]=\"isStickyHeaderEnabled()\"\r\n    [class.adf-datatable--empty]=\"!isHeaderVisible()\">\r\n    <div *ngIf=\"showHeader && isHeaderVisible()\" class=\"adf-datatable-header\" role=\"rowgroup\">\r\n        <div class=\"adf-datatable-row\" *ngIf=\"display === 'list'\" role=\"row\">\r\n            <!-- Actions (left) -->\r\n            <div *ngIf=\"actions && actionsPosition === 'left'\" class=\"adf-actions-column adf-datatable-cell-header\">\r\n                <span class=\"adf-sr-only\">Actions</span>\r\n            </div>\r\n            <!-- Columns -->\r\n            <div *ngIf=\"multiselect\" class=\"adf-datatable-cell-header adf-datatable-checkbox\">\r\n                <mat-checkbox [checked]=\"isSelectAllChecked\" (change)=\"onSelectAllClick($event)\" class=\"adf-checkbox-sr-only\">{{ 'ADF-DATATABLE.ACCESSIBILITY.SELECT_ALL' | translate }}</mat-checkbox>\r\n            </div>\r\n            <div class=\"adf-datatable-cell--{{col.type || 'text'}} {{col.cssClass}} adf-datatable-cell-header\"\r\n                 *ngFor=\"let col of data.getColumns()\"\r\n                 [class.adf-sortable]=\"col.sortable\"\r\n                 [attr.data-automation-id]=\"'auto_id_' + col.key\"\r\n                 [class.adf-datatable__header--sorted-asc]=\"isColumnSorted(col, 'asc')\"\r\n                 [class.adf-datatable__header--sorted-desc]=\"isColumnSorted(col, 'desc')\"\r\n                 (click)=\"onColumnHeaderClick(col)\"\r\n                 (keyup.enter)=\"onColumnHeaderClick(col)\"\r\n                 role=\"columnheader\"\r\n                 tabindex=\"0\"\r\n                 title=\"{{ col.title | translate }}\"\r\n                 adf-drop-zone dropTarget=\"header\" [dropColumn]=\"col\">\r\n                <span *ngIf=\"col.srTitle\" class=\"adf-sr-only\">{{ col.srTitle | translate }}</span>\r\n                <span *ngIf=\"col.title\" class=\"adf-datatable-cell-value\">{{ col.title | translate}}</span>\r\n            </div>\r\n            <!-- Actions (right) -->\r\n            <div *ngIf=\"actions && actionsPosition === 'right'\" class=\"adf-actions-column adf-datatable-cell-header adf-datatable__actions-cell\">\r\n                <span class=\"adf-sr-only\">Actions</span>\r\n            </div>\r\n        </div>\r\n        <mat-form-field *ngIf=\"display === 'gallery'\">\r\n            <mat-select [value]=\"getSortingKey()\" [attr.data-automation-id]=\"'grid-view-sorting'\">\r\n                <mat-option *ngFor=\"let col of getSortableColumns()\"\r\n                            [value]=\"col.key\"\r\n                            [attr.data-automation-id]=\"'grid-view-sorting-'+col.title\"\r\n                            (click)=\"onColumnHeaderClick(col)\"\r\n                            (keyup.enter)=\"onColumnHeaderClick(col)\">\r\n                    {{ col.title | translate}}\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"adf-datatable-body\" role=\"rowgroup\">\r\n        <ng-container *ngIf=\"!loading && !noPermission\">\r\n            <div *ngFor=\"let row of data.getRows(); let idx = index\"\r\n                 class=\"adf-datatable-row\"\r\n                 role=\"row\"\r\n                 [class.adf-is-selected]=\"row.isSelected\"\r\n                 [adf-upload]=\"allowDropFiles && rowAllowsDrop(row)\" [adf-upload-data]=\"row\"\r\n                 [ngStyle]=\"rowStyle\"\r\n                 [ngClass]=\"getRowStyle(row)\"\r\n                 (keyup)=\"onRowKeyUp(row, $event)\">\r\n                <!-- Actions (left) -->\r\n                <div *ngIf=\"actions && actionsPosition === 'left'\" role=\"gridcell\" class=\"adf-datatable-cell\">\r\n                    <button mat-icon-button [matMenuTriggerFor]=\"menu\"\r\n                            [title]=\"'ADF-DATATABLE.CONTENT-ACTIONS.TOOLTIP' | translate\"\r\n                            [attr.id]=\"'action_menu_left_' + idx\"\r\n                            [attr.data-automation-id]=\"'action_menu_' + idx\">\r\n                        <mat-icon>more_vert</mat-icon>\r\n                    </button>\r\n                    <mat-menu #menu=\"matMenu\">\r\n                        <button mat-menu-item *ngFor=\"let action of getRowActions(row)\"\r\n                                [attr.data-automation-id]=\"action.title\"\r\n                                [disabled]=\"action.disabled\"\r\n                                (click)=\"onExecuteRowAction(row, action)\">\r\n                            <mat-icon *ngIf=\"action.icon\">{{ action.icon }}</mat-icon>\r\n                            <span>{{ action.title | translate }}</span>\r\n                        </button>\r\n                    </mat-menu>\r\n                </div>\r\n\r\n                <div *ngIf=\"multiselect\" class=\"adf-datatable-cell adf-datatable-checkbox\">\r\n                    <mat-checkbox\r\n                        [checked]=\"row.isSelected\"\r\n                        [attr.aria-checked]=\"row.isSelected\"\r\n                        role=\"checkbox\"\r\n                        (change)=\"onCheckboxChange(row, $event)\"\r\n                        class=\"adf-checkbox-sr-only\">\r\n                        {{ 'ADF-DATATABLE.ACCESSIBILITY.SELECT_FILE' | translate }}\r\n                    </mat-checkbox>\r\n                </div>\r\n                <div *ngFor=\"let col of data.getColumns()\"\r\n                     role=\"gridcell\"\r\n                     class=\" adf-datatable-cell adf-datatable-cell--{{col.type || 'text'}} {{col.cssClass}}\"\r\n                     [attr.title]=\"col.title | translate\"\r\n                     [attr.data-automation-id]=\"getAutomationValue(row, col)\"\r\n                     tabindex=\"0\"\r\n                     (click)=\"onRowClick(row, $event)\"\r\n                     (keydown.enter)=\"onEnterKeyPressed(row, $event)\"\r\n                     [adf-context-menu]=\"getContextMenuActions(row, col)\"\r\n                     [adf-context-menu-enabled]=\"contextMenu\"\r\n                     adf-drop-zone dropTarget=\"cell\" [dropColumn]=\"col\" [dropRow]=\"row\">\r\n                    <div *ngIf=\"!col.template\" class=\"adf-datatable-cell-container\">\r\n                        <ng-container [ngSwitch]=\"col.type\">\r\n                            <div *ngSwitchCase=\"'image'\" class=\"adf-cell-value\">\r\n                                <mat-icon *ngIf=\"isIconValue(row, col); else no_iconvalue\">{{ asIconValue(row, col) }}\r\n                                </mat-icon>\r\n                                <ng-template #no_iconvalue>\r\n                                    <mat-icon class=\"adf-datatable-selected\"\r\n                                              *ngIf=\"row.isSelected && !multiselect; else no_selected_row\" svgIcon=\"selected\">\r\n                                    </mat-icon>\r\n                                    <ng-template #no_selected_row>\r\n                                        <img class=\"adf-datatable-center-img-ie\"\r\n                                            [attr.aria-label]=\"data.getValue(row, col) | fileType\"\r\n                                            alt=\"{{ iconAltTextKey(data.getValue(row, col)) | translate }}\"\r\n                                            src=\"{{ data.getValue(row, col) }}\"\r\n                                            (error)=\"onImageLoadingError($event, row)\">\r\n                                    </ng-template>\r\n                                </ng-template>\r\n                            </div>\r\n                            <div *ngSwitchCase=\"'icon'\" class=\"adf-cell-value\">\r\n                                <span class=\"adf-sr-only\">{{ iconAltTextKey(data.getValue(row, col)) | translate }}</span>\r\n                                <mat-icon>{{ data.getValue(row, col) }}</mat-icon>\r\n                            </div>\r\n                            <div *ngSwitchCase=\"'date'\" class=\"adf-cell-value\"\r\n                                 [attr.data-automation-id]=\"'date_' + (data.getValue(row, col) | adfLocalizedDate: 'medium') \">\r\n                                <adf-date-cell class=\"adf-datatable-center-date-column-ie\"\r\n                                    [data]=\"data\"\r\n                                    [column]=\"col\"\r\n                                    [row]=\"row\"\r\n                                    [tooltip]=\"getCellTooltip(row, col)\">\r\n                                </adf-date-cell>\r\n                            </div>\r\n                            <div *ngSwitchCase=\"'location'\" class=\"adf-cell-value\"\r\n                                 [attr.data-automation-id]=\"'location' + data.getValue(row, col)\">\r\n                                <adf-location-cell\r\n                                    [data]=\"data\"\r\n                                    [column]=\"col\"\r\n                                    [row]=\"row\"\r\n                                    [tooltip]=\"getCellTooltip(row, col)\">\r\n                                </adf-location-cell>\r\n                            </div>\r\n                            <div *ngSwitchCase=\"'fileSize'\" class=\"adf-cell-value\"\r\n                                 [attr.data-automation-id]=\"'fileSize_' + data.getValue(row, col)\">\r\n                                <adf-filesize-cell class=\"adf-datatable-center-size-column-ie\"\r\n                                    [data]=\"data\"\r\n                                    [column]=\"col\"\r\n                                    [row]=\"row\"\r\n                                    [tooltip]=\"getCellTooltip(row, col)\">\r\n                                </adf-filesize-cell>\r\n                            </div>\r\n                            <div *ngSwitchCase=\"'text'\" class=\"adf-cell-value\"\r\n                                 [attr.data-automation-id]=\"'text_' + data.getValue(row, col)\">\r\n                                <adf-datatable-cell\r\n                                    [copyContent]=\"col.copyContent\"\r\n                                    [data]=\"data\"\r\n                                    [column]=\"col\"\r\n                                    [row]=\"row\"\r\n                                    [tooltip]=\"getCellTooltip(row, col)\">\r\n                                </adf-datatable-cell>\r\n                            </div>\r\n                            <div *ngSwitchCase=\"'json'\" class=\"adf-cell-value\">\r\n                                <adf-json-cell\r\n                                    [copyContent]=\"col.copyContent\"\r\n                                    [data]=\"data\"\r\n                                    [column]=\"col\"\r\n                                    [row]=\"row\">\r\n                                </adf-json-cell>\r\n                            </div>\r\n                            <span *ngSwitchDefault class=\"adf-cell-value\">\r\n                    <!-- empty cell for unknown column type -->\r\n                    </span>\r\n                        </ng-container>\r\n                    </div>\r\n                    <div *ngIf=\"col.template\" class=\"adf-datatable-cell-container\">\r\n                        <div class=\"adf-cell-value\">\r\n                            <ng-container\r\n                                [ngTemplateOutlet]=\"col.template\"\r\n                                [ngTemplateOutletContext]=\"{ $implicit: { data: data, row: row, col: col }, value: data.getValue(row, col) }\">\r\n                            </ng-container>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <!-- Actions (right) -->\r\n                <div *ngIf=\"actions && actionsPosition === 'right'\"\r\n                     role=\"gridcell\"\r\n                     class=\"adf-datatable-cell adf-datatable__actions-cell adf-datatable-center-actions-column-ie\">\r\n                    <button mat-icon-button [matMenuTriggerFor]=\"menu\"\r\n                            [title]=\"'ADF-DATATABLE.CONTENT-ACTIONS.TOOLTIP' | translate\"\r\n                            [attr.id]=\"'action_menu_right_' + idx\"\r\n                            [attr.data-automation-id]=\"'action_menu_' + idx\">\r\n                        <mat-icon>more_vert</mat-icon>\r\n                    </button>\r\n                    <mat-menu #menu=\"matMenu\">\r\n                        <button mat-menu-item *ngFor=\"let action of getRowActions(row)\"\r\n                                [attr.data-automation-id]=\"action.title\"\r\n                                [attr.aria-label]=\"action.title | translate\"\r\n                                [disabled]=\"action.disabled\"\r\n                                (click)=\"onExecuteRowAction(row, action)\">\r\n                            <mat-icon *ngIf=\"action.icon\">{{ action.icon }}</mat-icon>\r\n                            <span>{{ action.title | translate }}</span>\r\n                        </button>\r\n                    </mat-menu>\r\n                </div>\r\n\r\n            </div>\r\n            <div *ngIf=\"isEmpty()\"\r\n                 role=\"row\"\r\n                 [class.adf-datatable-row]=\"display === 'list'\"\r\n                 [class.adf-datatable-card-empty]=\"display === 'gallery'\">\r\n                <div class=\"adf-no-content-container adf-datatable-cell\" role=\"gridcell\">\r\n                    <ng-template *ngIf=\"noContentTemplate\"\r\n                                 ngFor [ngForOf]=\"[data]\"\r\n                                 [ngForTemplate]=\"noContentTemplate\">\r\n                    </ng-template>\r\n                    <ng-content select=\"adf-empty-list\"></ng-content>\r\n                </div>\r\n            </div>\r\n            <div *ngFor=\"let row of fakeRows\"\r\n                 class=\"adf-datatable-row adf-datatable-row-empty-card\">\r\n            </div>\r\n        </ng-container>\r\n        <div *ngIf=\"!loading && noPermission\"\r\n             role=\"row\"\r\n             [class.adf-datatable-row]=\"display === 'list'\"\r\n             [class.adf-datatable-card-permissions]=\"display === 'gallery'\"\r\n             class=\"adf-no-permission__row\">\r\n            <div class=\"adf-no-permission__cell adf-no-content-container adf-datatable-cell\">\r\n                <ng-template *ngIf=\"noPermissionTemplate\"\r\n                             ngFor [ngForOf]=\"[data]\"\r\n                             [ngForTemplate]=\"noPermissionTemplate\">\r\n                </ng-template>\r\n            </div>\r\n        </div>\r\n        <div *ngIf=\"loading\"\r\n             [class.adf-datatable-row]=\"display === 'list'\"\r\n             [class.adf-datatable-card-loading]=\"display === 'gallery'\">\r\n            <div class=\"adf-no-content-container adf-datatable-cell\">\r\n                <ng-template *ngIf=\"loadingTemplate\"\r\n                             ngFor [ngForOf]=\"[data]\"\r\n                             [ngForTemplate]=\"loadingTemplate\">\r\n                </ng-template>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n",
                encapsulation: ViewEncapsulation.None,
                host: { class: 'adf-datatable' },
                styles: [""]
            }] }
];
/** @nocollapse */
DataTableComponent.ctorParameters = () => [
    { type: ElementRef },
    { type: IterableDiffers }
];
DataTableComponent.propDecorators = {
    columnList: [{ type: ContentChild, args: [DataColumnListComponent, { static: true },] }],
    data: [{ type: Input }],
    display: [{ type: Input }],
    rows: [{ type: Input }],
    sorting: [{ type: Input }],
    columns: [{ type: Input }],
    selectionMode: [{ type: Input }],
    multiselect: [{ type: Input }],
    actions: [{ type: Input }],
    actionsPosition: [{ type: Input }],
    fallbackThumbnail: [{ type: Input }],
    contextMenu: [{ type: Input }],
    allowDropFiles: [{ type: Input }],
    rowStyle: [{ type: Input }],
    rowStyleClass: [{ type: Input }],
    showHeader: [{ type: Input }],
    stickyHeader: [{ type: Input }],
    rowClick: [{ type: Output }],
    rowDblClick: [{ type: Output }],
    showRowContextMenu: [{ type: Output }],
    showRowActionsMenu: [{ type: Output }],
    executeRowAction: [{ type: Output }],
    loading: [{ type: Input }],
    noPermission: [{ type: Input }],
    rowMenuCacheEnabled: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    DataTableComponent.prototype.columnList;
    /**
     * Data source for the table
     * @type {?}
     */
    DataTableComponent.prototype.data;
    /**
     * Selects the display mode of the table. Can be "list" or "gallery".
     * @type {?}
     */
    DataTableComponent.prototype.display;
    /**
     * The rows that the datatable will show.
     * @type {?}
     */
    DataTableComponent.prototype.rows;
    /**
     * Define the sort order of the datatable. Possible values are :
     * [`created`, `desc`], [`created`, `asc`], [`due`, `desc`], [`due`, `asc`]
     * @type {?}
     */
    DataTableComponent.prototype.sorting;
    /**
     * The columns that the datatable will show.
     * @type {?}
     */
    DataTableComponent.prototype.columns;
    /**
     * Row selection mode. Can be none, `single` or `multiple`. For `multiple` mode,
     * you can use Cmd (macOS) or Ctrl (Win) modifier key to toggle selection for multiple rows.
     * @type {?}
     */
    DataTableComponent.prototype.selectionMode;
    /**
     * Toggles multiple row selection, which renders checkboxes at the beginning of each row.
     * @type {?}
     */
    DataTableComponent.prototype.multiselect;
    /**
     * Toggles the data actions column.
     * @type {?}
     */
    DataTableComponent.prototype.actions;
    /**
     * Position of the actions dropdown menu. Can be "left" or "right".
     * @type {?}
     */
    DataTableComponent.prototype.actionsPosition;
    /**
     * Fallback image for rows where the thumbnail is missing.
     * @type {?}
     */
    DataTableComponent.prototype.fallbackThumbnail;
    /**
     * Toggles custom context menu for the component.
     * @type {?}
     */
    DataTableComponent.prototype.contextMenu;
    /**
     * Toggles file drop support for rows (see
     * [Upload directive](upload.directive.md) for further details).
     * @type {?}
     */
    DataTableComponent.prototype.allowDropFiles;
    /**
     * The inline style to apply to every row. See
     * [NgStyle](https://angular.io/docs/ts/latest/api/common/index/NgStyle-directive.html)
     * docs for more details and usage examples.
     * @type {?}
     */
    DataTableComponent.prototype.rowStyle;
    /**
     * The CSS class to apply to every row.
     * @type {?}
     */
    DataTableComponent.prototype.rowStyleClass;
    /**
     * Toggles the header.
     * @type {?}
     */
    DataTableComponent.prototype.showHeader;
    /**
     * Toggles the sticky header mode.
     * @type {?}
     */
    DataTableComponent.prototype.stickyHeader;
    /**
     * Emitted when the user clicks a row.
     * @type {?}
     */
    DataTableComponent.prototype.rowClick;
    /**
     * Emitted when the user double-clicks a row.
     * @type {?}
     */
    DataTableComponent.prototype.rowDblClick;
    /**
     * Emitted before the context menu is displayed for a row.
     * @type {?}
     */
    DataTableComponent.prototype.showRowContextMenu;
    /**
     * Emitted before the actions menu is displayed for a row.
     * @type {?}
     */
    DataTableComponent.prototype.showRowActionsMenu;
    /**
     * Emitted when the user executes a row action.
     * @type {?}
     */
    DataTableComponent.prototype.executeRowAction;
    /**
     * Flag that indicates if the datatable is in loading state and needs to show the
     * loading template (see the docs to learn how to configure a loading template).
     * @type {?}
     */
    DataTableComponent.prototype.loading;
    /**
     * Flag that indicates if the datatable should show the "no permission" template.
     * @type {?}
     */
    DataTableComponent.prototype.noPermission;
    /**
     * Should the items for the row actions menu be cached for reuse after they are loaded
     * the first time?
     * @type {?}
     */
    DataTableComponent.prototype.rowMenuCacheEnabled;
    /** @type {?} */
    DataTableComponent.prototype.noContentTemplate;
    /** @type {?} */
    DataTableComponent.prototype.noPermissionTemplate;
    /** @type {?} */
    DataTableComponent.prototype.loadingTemplate;
    /** @type {?} */
    DataTableComponent.prototype.isSelectAllChecked;
    /** @type {?} */
    DataTableComponent.prototype.selection;
    /**
     * This array of fake rows fix the flex layout for the gallery view
     * @type {?}
     */
    DataTableComponent.prototype.fakeRows;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.clickObserver;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.click$;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.differ;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.rowMenuCache;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.subscriptions;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.singleClickStreamSub;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.multiClickStreamSub;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.dataRowsChanged;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.elementRef;
}
/**
 * @record
 */
export function DataTableDropEvent() { }
if (false) {
    /** @type {?} */
    DataTableDropEvent.prototype.detail;
    /**
     * @return {?}
     */
    DataTableDropEvent.prototype.preventDefault = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YXRhYmxlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRhdGF0YWJsZS9jb21wb25lbnRzL2RhdGF0YWJsZS9kYXRhdGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxPQUFPLEVBQ2UsU0FBUyxFQUFFLFlBQVksRUFBVyxVQUFVLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFDbkYsZUFBZSxFQUFhLE1BQU0sRUFBNEMsaUJBQWlCLEVBQ2xHLE1BQU0sZUFBZSxDQUFDO0FBRXZCLE9BQU8sRUFBZ0IsVUFBVSxFQUFZLE1BQU0sTUFBTSxDQUFDO0FBQzFELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBRTFGLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUUvRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFHNUQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7SUFHdEUsTUFBTyxNQUFNO0lBQ2IsU0FBVSxTQUFTOzs7QUFVdkIsTUFBTSxPQUFPLGtCQUFrQjs7Ozs7SUF3STNCLFlBQW9CLFVBQXNCLEVBQzlCLE9BQXdCO1FBRGhCLGVBQVUsR0FBVixVQUFVLENBQVk7Ozs7UUE3SDFDLFlBQU8sR0FBVyxXQUFXLENBQUMsSUFBSSxDQUFDOzs7O1FBSW5DLFNBQUksR0FBVSxFQUFFLENBQUM7Ozs7O1FBTWpCLFlBQU8sR0FBVSxFQUFFLENBQUM7Ozs7UUFJcEIsWUFBTyxHQUFVLEVBQUUsQ0FBQzs7Ozs7UUFNcEIsa0JBQWEsR0FBVyxRQUFRLENBQUMsQ0FBQyx1QkFBdUI7Ozs7O1FBSXpELGdCQUFXLEdBQVksS0FBSyxDQUFDOzs7O1FBSTdCLFlBQU8sR0FBWSxLQUFLLENBQUM7Ozs7UUFJekIsb0JBQWUsR0FBVyxPQUFPLENBQUMsQ0FBQyxhQUFhOzs7O1FBUWhELGdCQUFXLEdBQVksS0FBSyxDQUFDOzs7OztRQU03QixtQkFBYyxHQUFZLEtBQUssQ0FBQzs7OztRQVdoQyxrQkFBYSxHQUFXLEVBQUUsQ0FBQzs7OztRQUkzQixlQUFVLEdBQVksSUFBSSxDQUFDOzs7O1FBSTNCLGlCQUFZLEdBQVksS0FBSyxDQUFDOzs7O1FBSTlCLGFBQVEsR0FBRyxJQUFJLFlBQVksRUFBZ0IsQ0FBQzs7OztRQUk1QyxnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFnQixDQUFDOzs7O1FBSS9DLHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFpQixDQUFDOzs7O1FBSXZELHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFpQixDQUFDOzs7O1FBSXZELHFCQUFnQixHQUFHLElBQUksWUFBWSxFQUFzQixDQUFDOzs7OztRQU0xRCxZQUFPLEdBQVksS0FBSyxDQUFDOzs7O1FBSXpCLGlCQUFZLEdBQVksS0FBSyxDQUFDOzs7OztRQU85Qix3QkFBbUIsR0FBRyxJQUFJLENBQUM7UUFNM0IsdUJBQWtCLEdBQVksS0FBSyxDQUFDO1FBQ3BDLGNBQVMsR0FBRyxJQUFJLEtBQUssRUFBVyxDQUFDOzs7O1FBR2pDLGFBQVEsR0FBRyxFQUFFLENBQUM7UUFNTixpQkFBWSxHQUFXLEVBQUUsQ0FBQztRQUUxQixrQkFBYSxHQUFtQixFQUFFLENBQUM7UUFPdkMsSUFBSSxPQUFPLEVBQUU7WUFDVCxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQy9DO1FBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLFVBQVU7Ozs7UUFBZSxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxRQUFRLEVBQUM7YUFDbEYsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7SUFDdkIsQ0FBQzs7OztJQUVELGtCQUFrQjtRQUNkLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNqQixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FDbkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFNBQVM7OztZQUFDLEdBQUcsRUFBRTtnQkFDM0MsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQzFCLENBQUMsRUFBQyxDQUNMLENBQUM7U0FDTDtRQUNELElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1FBQzFCLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUMxQixDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxPQUFzQjtRQUM5QixJQUFJLENBQUMsMkJBQTJCLEVBQUUsQ0FBQztRQUNuQyxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsRUFBRTtZQUN6QyxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUUsRUFBRTtnQkFDckIsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2FBQ3BCO2lCQUFNO2dCQUNILElBQUksQ0FBQyxJQUFJLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLFlBQVksQ0FBQztnQkFDekMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQ3pCO1lBQ0QsT0FBTztTQUNWO1FBRUQsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUU7WUFDekMsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFLEVBQUU7Z0JBQ3JCLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQzthQUNwQjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQzthQUNuRDtZQUNELE9BQU87U0FDVjtRQUVELElBQUksT0FBTyxDQUFDLGFBQWEsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsYUFBYSxFQUFFLEVBQUU7WUFDakUsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDcEQ7UUFFRCxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRTtZQUM1QyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUN6RDtRQUVELElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsQ0FBQyxFQUFFO1lBQzVDLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO1NBQzdCO0lBQ0wsQ0FBQzs7OztJQUVELFNBQVM7O2NBQ0MsT0FBTyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDM0MsSUFBSSxPQUFPLEVBQUU7WUFDVCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNoQztJQUNMLENBQUM7Ozs7O0lBRUQsaUJBQWlCLENBQUMsUUFBc0I7UUFDcEMsT0FBTyxRQUFRLElBQUksUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDNUQsQ0FBQzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxJQUFZO1FBQzFCLE9BQU8sSUFBSSxDQUFDLEdBQUc7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxhQUFhLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxVQUFVLENBQUMsRUFBQyxDQUFDO0lBQ3JFLENBQUM7Ozs7O0lBRUQsb0JBQW9CLENBQUMsT0FBYztRQUMvQixJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUMvQixPQUFPLElBQUksV0FBVyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsRUFBRSxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNsRDtJQUNMLENBQUM7Ozs7O0lBRU8sMkJBQTJCO1FBQy9CLElBQUksQ0FBQyxzQkFBc0IsRUFBRSxDQUFDOztjQUN4QixpQkFBaUIsR0FBRyxJQUFJLENBQUMsTUFBTTthQUNoQyxJQUFJLENBQ0QsTUFBTSxDQUNGLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUNaLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FDcEIsQ0FDSixFQUNELEdBQUc7Ozs7UUFBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxFQUFDLEVBQ25CLE1BQU07Ozs7UUFBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUMsQ0FDaEM7UUFFTCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsaUJBQWlCLENBQUMsU0FBUzs7OztRQUFDLENBQUMsYUFBNkIsRUFBRSxFQUFFOztrQkFDaEYsS0FBSyxHQUFpQixhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQzVDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLG1CQUE2QixLQUFLLENBQUMsS0FBSyxFQUFBLENBQUMsQ0FBQztZQUMvRSxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUMxQixJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixFQUFFO2dCQUN6QixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQ3ZDLElBQUksV0FBVyxDQUFDLFdBQVcsRUFBRTtvQkFDekIsTUFBTSxFQUFFLEtBQUs7b0JBQ2IsT0FBTyxFQUFFLElBQUk7aUJBQ2hCLENBQUMsQ0FDTCxDQUFDO2FBQ0w7UUFDTCxDQUFDLEVBQUMsQ0FBQzs7Y0FFRyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsTUFBTTthQUMvQixJQUFJLENBQ0QsTUFBTSxDQUNGLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUNaLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FDcEIsQ0FDSixFQUNELEdBQUc7Ozs7UUFBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxFQUFDLEVBQ25CLE1BQU07Ozs7UUFBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLE1BQU0sSUFBSSxDQUFDLEVBQUMsQ0FDL0I7UUFFTCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsZ0JBQWdCLENBQUMsU0FBUzs7OztRQUFDLENBQUMsYUFBNkIsRUFBRSxFQUFFOztrQkFDOUUsS0FBSyxHQUFpQixhQUFhLENBQUMsQ0FBQyxDQUFDO1lBQzVDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzdCLElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FDdkMsSUFBSSxXQUFXLENBQUMsY0FBYyxFQUFFO29CQUM1QixNQUFNLEVBQUUsS0FBSztvQkFDYixPQUFPLEVBQUUsSUFBSTtpQkFDaEIsQ0FBQyxDQUNMLENBQUM7YUFDTDtRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFTyxzQkFBc0I7UUFDMUIsSUFBSSxJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDM0IsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3hDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUMxQixJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDdkMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztTQUNuQztJQUNMLENBQUM7Ozs7O0lBRU8sU0FBUztRQUNiLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNoRSxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNuQyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdEIsSUFBSSxDQUFDLFlBQVksR0FBRyxFQUFFLENBQUM7SUFDM0IsQ0FBQzs7OztJQUVELFlBQVk7UUFDUixPQUFPLElBQUksQ0FBQyxJQUFJLEtBQUssU0FBUyxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDO0lBQ3pELENBQUM7Ozs7OztJQUVPLFlBQVksQ0FBQyxJQUFXO1FBQzVCLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtZQUNYLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUNuRDtJQUNMLENBQUM7Ozs7O0lBRU8sY0FBYzs7WUFDZCxNQUFNLEdBQUcsRUFBRTtRQUNmLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUM1QyxNQUFNLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7U0FDckM7YUFBTTtZQUNILE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxDQUFDO1NBQzFEO1FBRUQsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7UUFFdEIsSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3RELElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUN0QztJQUNMLENBQUM7Ozs7OztJQUVPLGVBQWUsQ0FBQyxPQUFPO1FBQzNCLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtZQUNYLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDO1NBQzVEO0lBQ0wsQ0FBQzs7OztJQUVNLGlCQUFpQjs7WUFDaEIsTUFBTSxHQUFHLEVBQUU7UUFDZixJQUFJLElBQUksQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNsRixNQUFNLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRzs7OztZQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxtQkFBYSxDQUFDLEVBQUEsRUFBQyxDQUFDO1NBQy9EO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQzs7Ozs7O0lBRUQsVUFBVSxDQUFDLEdBQVksRUFBRSxVQUFzQjtRQUMzQyxJQUFJLFVBQVUsRUFBRTtZQUNaLFVBQVUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUMvQjtRQUVELElBQUksR0FBRyxFQUFFOztrQkFDQyxZQUFZLEdBQUcsSUFBSSxZQUFZLENBQUMsR0FBRyxFQUFFLFVBQVUsRUFBRSxJQUFJLENBQUM7WUFDNUQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDekM7SUFDTCxDQUFDOzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxHQUFZLEVBQUUsQ0FBZ0I7UUFDNUMsSUFBSSxHQUFHLEVBQUU7WUFDTCxJQUFJLENBQUMsa0JBQWtCLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ25DO0lBQ0wsQ0FBQzs7Ozs7OztJQUVPLGtCQUFrQixDQUFDLEdBQVksRUFBRSxDQUE2QjtRQUNsRSxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDWCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxFQUFFO2dCQUM5QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUMxQixJQUFJLENBQUMscUJBQXFCLENBQUMsWUFBWSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2FBQ2pEO1lBRUQsSUFBSSxJQUFJLENBQUMsb0JBQW9CLEVBQUUsRUFBRTs7c0JBQ3ZCLFFBQVEsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUM7O29CQUMxQyxRQUFpQjtnQkFDckIsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7b0JBQzdCLFFBQVEsR0FBRyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUM7aUJBQzlCO3FCQUFNO29CQUNILFFBQVEsR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2lCQUNoRDs7c0JBQ0ssWUFBWSxHQUFHLFFBQVEsQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxjQUFjO2dCQUU3RCxJQUFJLENBQUMsUUFBUSxFQUFFO29CQUNYLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztpQkFDekI7Z0JBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7Z0JBQzlCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLEVBQUUsR0FBRyxDQUFDLENBQUM7YUFDakQ7U0FDSjtJQUNMLENBQUM7Ozs7SUFFRCxjQUFjO1FBQ1YsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFOztrQkFDTCxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEMsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxPQUFPOzs7O2dCQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsVUFBVSxHQUFHLEtBQUssRUFBQyxDQUFDO2FBQzdDO1lBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxFQUFFLENBQUM7U0FDdkI7UUFDRCxJQUFJLENBQUMsa0JBQWtCLEdBQUcsS0FBSyxDQUFDO0lBQ3BDLENBQUM7Ozs7OztJQUVELGFBQWEsQ0FBQyxHQUFZLEVBQUUsS0FBYTtRQUNyQyxJQUFJLEtBQUssRUFBRTtZQUNQLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUMxQjs7Y0FDSyxZQUFZLEdBQUcsSUFBSSxZQUFZLENBQUMsR0FBRyxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUM7UUFDdkQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7Ozs7O0lBRUQsVUFBVSxDQUFDLEdBQVksRUFBRSxDQUFnQjs7Y0FDL0IsS0FBSyxHQUFHLElBQUksV0FBVyxDQUFDLFdBQVcsRUFBRTtZQUN2QyxNQUFNLEVBQUU7Z0JBQ0osR0FBRyxFQUFFLEdBQUc7Z0JBQ1IsYUFBYSxFQUFFLENBQUM7Z0JBQ2hCLE1BQU0sRUFBRSxJQUFJO2FBQ2Y7WUFDRCxPQUFPLEVBQUUsSUFBSTtTQUNoQixDQUFDO1FBRUYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRW5ELElBQUksS0FBSyxDQUFDLGdCQUFnQixFQUFFO1lBQ3hCLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUN0QjthQUFNO1lBQ0gsSUFBSSxDQUFDLENBQUMsR0FBRyxLQUFLLE9BQU8sRUFBRTtnQkFDbkIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUNuQztTQUNKO0lBQ0wsQ0FBQzs7Ozs7OztJQUVPLGtCQUFrQixDQUFDLEdBQVksRUFBRSxhQUE0QjtRQUNqRSxJQUFJLGFBQWEsRUFBRTtZQUNmLGFBQWEsQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUNsQzs7Y0FFSyxLQUFLLEdBQUcsSUFBSSxZQUFZLENBQUMsR0FBRyxFQUFFLGFBQWEsRUFBRSxJQUFJLENBQUM7UUFFeEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUN2QyxJQUFJLFdBQVcsQ0FBQyxjQUFjLEVBQUU7WUFDNUIsTUFBTSxFQUFFLEtBQUs7WUFDYixPQUFPLEVBQUUsSUFBSTtTQUNoQixDQUFDLENBQ0wsQ0FBQztJQUNOLENBQUM7Ozs7O0lBRUQsbUJBQW1CLENBQUMsTUFBa0I7UUFDbEMsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLFFBQVEsRUFBRTs7a0JBQ3JCLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTs7Z0JBQ2xDLFlBQVksR0FBRyxLQUFLO1lBQ3hCLElBQUksT0FBTyxJQUFJLE1BQU0sQ0FBQyxHQUFHLEtBQUssT0FBTyxDQUFDLEdBQUcsRUFBRTtnQkFDdkMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxTQUFTLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQzthQUMvRDtZQUNELElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksV0FBVyxDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsWUFBWSxDQUFDLENBQUMsQ0FBQztZQUNoRSxJQUFJLENBQUMsdUJBQXVCLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxZQUFZLENBQUMsQ0FBQztTQUMxRDtJQUNMLENBQUM7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsaUJBQW9DO1FBQ2pELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxpQkFBaUIsQ0FBQyxPQUFPLENBQUM7UUFFcEQsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFOztrQkFDWixJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEMsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3pCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO29CQUNsQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsRUFBRSxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsQ0FBQztpQkFDdEQ7YUFDSjs7a0JBRUssWUFBWSxHQUFHLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxjQUFjOztrQkFDeEUsR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtZQUVoRSxJQUFJLENBQUMscUJBQXFCLENBQUMsWUFBWSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1NBQ2pEO0lBQ0wsQ0FBQzs7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsR0FBWSxFQUFFLEtBQXdCOztjQUM3QyxRQUFRLEdBQUcsS0FBSyxDQUFDLE9BQU87UUFFOUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7O2NBRXhCLFlBQVksR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsY0FBYztRQUM3RCxJQUFJLENBQUMscUJBQXFCLENBQUMsWUFBWSxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ2xELENBQUM7Ozs7OztJQUVELG1CQUFtQixDQUFDLEtBQVksRUFBRSxHQUFZO1FBQzFDLElBQUksS0FBSyxFQUFFOztrQkFDRCxPQUFPLEdBQUcsbUJBQU0sS0FBSyxDQUFDLE1BQU0sRUFBQTtZQUVsQyxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtnQkFDeEIsT0FBTyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7YUFDeEM7aUJBQU07Z0JBQ0gsT0FBTyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDL0M7U0FDSjtJQUNMLENBQUM7Ozs7OztJQUVELFdBQVcsQ0FBQyxHQUFZLEVBQUUsR0FBZTtRQUNyQyxJQUFJLEdBQUcsSUFBSSxHQUFHLEVBQUU7O2tCQUNOLEtBQUssR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUM7WUFDbkMsT0FBTyxLQUFLLElBQUksS0FBSyxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1NBQ3pEO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7Ozs7O0lBRUQsV0FBVyxDQUFDLEdBQVksRUFBRSxHQUFlO1FBQ3JDLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEVBQUU7O2tCQUN0QixLQUFLLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRTtZQUN6QyxPQUFPLEtBQUssQ0FBQyxPQUFPLENBQUMsbUJBQW1CLEVBQUUsRUFBRSxDQUFDLENBQUM7U0FDakQ7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOzs7OztJQUVELGNBQWMsQ0FBQyxLQUFhO1FBQ3hCLE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUN2RyxDQUFDOzs7Ozs7SUFFRCxjQUFjLENBQUMsR0FBZSxFQUFFLFNBQWlCO1FBQzdDLElBQUksR0FBRyxJQUFJLFNBQVMsRUFBRTs7a0JBQ1osT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ3RDLE9BQU8sT0FBTyxJQUFJLE9BQU8sQ0FBQyxHQUFHLEtBQUssR0FBRyxDQUFDLEdBQUcsSUFBSSxPQUFPLENBQUMsU0FBUyxLQUFLLFNBQVMsQ0FBQztTQUNoRjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7OztJQUVELHFCQUFxQixDQUFDLEdBQVksRUFBRSxHQUFlOztjQUN6QyxLQUFLLEdBQUcsSUFBSSxhQUFhLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxFQUFFLENBQUM7UUFDN0MsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNwQyxPQUFPLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDO0lBQy9CLENBQUM7Ozs7OztJQUVELGFBQWEsQ0FBQyxHQUFZLEVBQUUsR0FBZ0I7O2NBQ2xDLEVBQUUsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztRQUU3QixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsRUFBRTs7a0JBQ2xCLEtBQUssR0FBRyxJQUFJLGFBQWEsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQztZQUM3QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUU7Z0JBQzNCLE9BQU8sS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7YUFDOUI7WUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDO1NBQy9DO1FBRUQsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7Ozs7OztJQUVELGtCQUFrQixDQUFDLEdBQVksRUFBRSxNQUFXO1FBQ3hDLElBQUksTUFBTSxDQUFDLFFBQVEsSUFBSSxNQUFNLENBQUMsUUFBUSxFQUFFO1lBQ3BDLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztTQUMzQjthQUFNO1lBQ0gsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxJQUFJLGtCQUFrQixDQUFDLEdBQUcsRUFBRSxNQUFNLENBQUMsQ0FBQyxDQUFDO1NBQ25FO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxhQUFhLENBQUMsR0FBWTtRQUN0QixPQUFPLEdBQUcsQ0FBQyxZQUFZLEtBQUssSUFBSSxDQUFDO0lBQ3JDLENBQUM7Ozs7SUFFRCxnQkFBZ0I7UUFDWixPQUFPLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0lBQ3ZFLENBQUM7Ozs7SUFFRCxxQkFBcUI7UUFDakIsT0FBTyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLEtBQUssUUFBUSxDQUFDO0lBQy9FLENBQUM7Ozs7SUFFRCxvQkFBb0I7UUFDaEIsT0FBTyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLEtBQUssVUFBVSxDQUFDO0lBQ2pGLENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLEdBQVk7UUFDcEIsR0FBRyxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDaEQsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDbEUsT0FBTyxHQUFHLEdBQUcsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ25ELENBQUM7Ozs7SUFFRCxhQUFhO1FBQ1QsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFO1lBQ3hCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxHQUFHLENBQUM7U0FDckM7SUFDTCxDQUFDOzs7Ozs7SUFFRCxTQUFTLENBQUMsR0FBWSxFQUFFLEtBQWM7UUFDbEMsSUFBSSxHQUFHLEVBQUU7WUFDTCxHQUFHLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQzs7a0JBQ2pCLEdBQUcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUM7WUFDdkMsSUFBSSxLQUFLLEVBQUU7Z0JBQ1AsSUFBSSxHQUFHLEdBQUcsQ0FBQyxFQUFFO29CQUNULElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUM1QjthQUNKO2lCQUFNO2dCQUNILElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxFQUFFO29CQUNWLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztpQkFDakM7YUFDSjtTQUNKO0lBQ0wsQ0FBQzs7Ozs7O0lBRUQsY0FBYyxDQUFDLEdBQVksRUFBRSxHQUFlO1FBQ3hDLElBQUksR0FBRyxJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsYUFBYSxFQUFFOztrQkFDM0IsTUFBTSxHQUFXLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQztZQUNsRCxJQUFJLE1BQU0sRUFBRTtnQkFDUixPQUFPLE1BQU0sQ0FBQzthQUNqQjtTQUNKO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQzs7OztJQUVELGtCQUFrQjtRQUNkLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxNQUFNOzs7O1FBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTtZQUM1QyxPQUFPLE1BQU0sQ0FBQyxRQUFRLEtBQUssSUFBSSxDQUFDO1FBQ3BDLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELE9BQU87UUFDSCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQztJQUM1QyxDQUFDOzs7O0lBRUQsZUFBZTtRQUNYLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQztJQUNsRSxDQUFDOzs7O0lBRUQscUJBQXFCO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDdkQsQ0FBQzs7Ozs7OztJQUVPLHFCQUFxQixDQUFDLElBQVksRUFBRSxHQUFZOztjQUM5QyxRQUFRLEdBQUcsSUFBSSxXQUFXLENBQUMsSUFBSSxFQUFFO1lBQ25DLE1BQU0sRUFBRTtnQkFDSixHQUFHLEVBQUUsR0FBRztnQkFDUixTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7YUFDNUI7WUFDRCxPQUFPLEVBQUUsSUFBSTtTQUNoQixDQUFDO1FBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzFELENBQUM7Ozs7Ozs7SUFFTyx1QkFBdUIsQ0FBQyxHQUFXLEVBQUUsU0FBaUI7O2NBQ3BELFFBQVEsR0FBRyxJQUFJLFdBQVcsQ0FBQyxpQkFBaUIsRUFBRTtZQUNoRCxNQUFNLEVBQUU7Z0JBQ0osR0FBRztnQkFDSCxTQUFTO2FBQ1o7WUFDRCxPQUFPLEVBQUUsSUFBSTtTQUNoQixDQUFDO1FBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzFELENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFFOUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPOzs7O1FBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsRUFBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxhQUFhLEdBQUcsRUFBRSxDQUFDO1FBRXhCLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN0QixJQUFJLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ25DLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDO1NBQy9CO0lBQ0wsQ0FBQzs7OztJQUVELGtCQUFrQjs7Y0FDUixjQUFjLEdBQUcsRUFBRTtRQUV6QixJQUFJLElBQUksQ0FBQyxPQUFPLEtBQUssU0FBUyxFQUFFO1lBQzVCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxjQUFjLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3RDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ3pCO1NBQ0o7YUFBTTtZQUNILElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1NBQ3RCO0lBQ0wsQ0FBQzs7OztJQUVELGtCQUFrQjtRQUNkLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxJQUFJOzs7O1FBQUUsQ0FBQyxFQUFPLEVBQUUsRUFBRTtZQUM1QyxPQUFPLEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ25DLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7O0lBRUQsa0JBQWtCLENBQUMsR0FBWSxFQUFFLEdBQWU7O2NBQ3RDLElBQUksR0FBRyxJQUFJLENBQUMsa0JBQWtCLEVBQUU7UUFDdEMsT0FBTyxJQUFJLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDOUMsQ0FBQzs7O1lBenBCSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGVBQWU7Z0JBRXpCLHdpZUFBeUM7Z0JBQ3pDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO2dCQUNyQyxJQUFJLEVBQUUsRUFBRSxLQUFLLEVBQUUsZUFBZSxFQUFFOzthQUNuQzs7OztZQTdCdUQsVUFBVTtZQUM5RCxlQUFlOzs7eUJBK0JkLFlBQVksU0FBQyx1QkFBdUIsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7bUJBSXBELEtBQUs7c0JBSUwsS0FBSzttQkFJTCxLQUFLO3NCQU1MLEtBQUs7c0JBSUwsS0FBSzs0QkFNTCxLQUFLOzBCQUlMLEtBQUs7c0JBSUwsS0FBSzs4QkFJTCxLQUFLO2dDQUlMLEtBQUs7MEJBSUwsS0FBSzs2QkFNTCxLQUFLO3VCQU9MLEtBQUs7NEJBSUwsS0FBSzt5QkFJTCxLQUFLOzJCQUlMLEtBQUs7dUJBSUwsTUFBTTswQkFJTixNQUFNO2lDQUlOLE1BQU07aUNBSU4sTUFBTTsrQkFJTixNQUFNO3NCQU1OLEtBQUs7MkJBSUwsS0FBSztrQ0FPTCxLQUFLOzs7O0lBOUdOLHdDQUNvQzs7Ozs7SUFHcEMsa0NBQ3VCOzs7OztJQUd2QixxQ0FDbUM7Ozs7O0lBR25DLGtDQUNpQjs7Ozs7O0lBS2pCLHFDQUNvQjs7Ozs7SUFHcEIscUNBQ29COzs7Ozs7SUFLcEIsMkNBQ2lDOzs7OztJQUdqQyx5Q0FDNkI7Ozs7O0lBRzdCLHFDQUN5Qjs7Ozs7SUFHekIsNkNBQ2tDOzs7OztJQUdsQywrQ0FDMEI7Ozs7O0lBRzFCLHlDQUM2Qjs7Ozs7O0lBSzdCLDRDQUNnQzs7Ozs7OztJQU1oQyxzQ0FDaUI7Ozs7O0lBR2pCLDJDQUMyQjs7Ozs7SUFHM0Isd0NBQzJCOzs7OztJQUczQiwwQ0FDOEI7Ozs7O0lBRzlCLHNDQUM0Qzs7Ozs7SUFHNUMseUNBQytDOzs7OztJQUcvQyxnREFDdUQ7Ozs7O0lBR3ZELGdEQUN1RDs7Ozs7SUFHdkQsOENBQzBEOzs7Ozs7SUFLMUQscUNBQ3lCOzs7OztJQUd6QiwwQ0FDOEI7Ozs7OztJQU05QixpREFDMkI7O0lBRTNCLCtDQUFvQzs7SUFDcEMsa0RBQXVDOztJQUN2Qyw2Q0FBa0M7O0lBRWxDLGdEQUFvQzs7SUFDcEMsdUNBQWlDOzs7OztJQUdqQyxzQ0FBYzs7Ozs7SUFFZCwyQ0FBOEM7Ozs7O0lBQzlDLG9DQUF5Qzs7Ozs7SUFFekMsb0NBQW9COzs7OztJQUNwQiwwQ0FBa0M7Ozs7O0lBRWxDLDJDQUEyQzs7Ozs7SUFDM0Msa0RBQTJDOzs7OztJQUMzQyxpREFBMEM7Ozs7O0lBQzFDLDZDQUFzQzs7Ozs7SUFFMUIsd0NBQThCOzs7OztBQTZnQjlDLHdDQVNDOzs7SUFSRyxvQ0FLRTs7OztJQUVGLDhEQUF1QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxuXG4vKiFcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxuICpcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cbiAqL1xuXG5pbXBvcnQge1xuICAgIEFmdGVyQ29udGVudEluaXQsIENvbXBvbmVudCwgQ29udGVudENoaWxkLCBEb0NoZWNrLCBFbGVtZW50UmVmLCBFdmVudEVtaXR0ZXIsIElucHV0LFxuICAgIEl0ZXJhYmxlRGlmZmVycywgT25DaGFuZ2VzLCBPdXRwdXQsIFNpbXBsZUNoYW5nZSwgU2ltcGxlQ2hhbmdlcywgVGVtcGxhdGVSZWYsIFZpZXdFbmNhcHN1bGF0aW9uLCBPbkRlc3Ryb3lcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNYXRDaGVja2JveENoYW5nZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiwgT2JzZXJ2YWJsZSwgT2JzZXJ2ZXIgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IERhdGFDb2x1bW5MaXN0Q29tcG9uZW50IH0gZnJvbSAnLi4vLi4vLi4vZGF0YS1jb2x1bW4vZGF0YS1jb2x1bW4tbGlzdC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRGF0YUNvbHVtbiB9IGZyb20gJy4uLy4uL2RhdGEvZGF0YS1jb2x1bW4ubW9kZWwnO1xuaW1wb3J0IHsgRGF0YVJvd0V2ZW50IH0gZnJvbSAnLi4vLi4vZGF0YS9kYXRhLXJvdy1ldmVudC5tb2RlbCc7XG5pbXBvcnQgeyBEYXRhUm93IH0gZnJvbSAnLi4vLi4vZGF0YS9kYXRhLXJvdy5tb2RlbCc7XG5pbXBvcnQgeyBEYXRhU29ydGluZyB9IGZyb20gJy4uLy4uL2RhdGEvZGF0YS1zb3J0aW5nLm1vZGVsJztcbmltcG9ydCB7IERhdGFUYWJsZUFkYXB0ZXIgfSBmcm9tICcuLi8uLi9kYXRhL2RhdGF0YWJsZS1hZGFwdGVyJztcblxuaW1wb3J0IHsgT2JqZWN0RGF0YVJvdyB9IGZyb20gJy4uLy4uL2RhdGEvb2JqZWN0LWRhdGFyb3cubW9kZWwnO1xuaW1wb3J0IHsgT2JqZWN0RGF0YVRhYmxlQWRhcHRlciB9IGZyb20gJy4uLy4uL2RhdGEvb2JqZWN0LWRhdGF0YWJsZS1hZGFwdGVyJztcbmltcG9ydCB7IERhdGFDZWxsRXZlbnQgfSBmcm9tICcuL2RhdGEtY2VsbC5ldmVudCc7XG5pbXBvcnQgeyBEYXRhUm93QWN0aW9uRXZlbnQgfSBmcm9tICcuL2RhdGEtcm93LWFjdGlvbi5ldmVudCc7XG5pbXBvcnQgeyBzaGFyZSwgYnVmZmVyLCBtYXAsIGZpbHRlciwgZGVib3VuY2VUaW1lIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuXG5leHBvcnQgZW51bSBEaXNwbGF5TW9kZSB7XG4gICAgTGlzdCA9ICdsaXN0JyxcbiAgICBHYWxsZXJ5ID0gJ2dhbGxlcnknXG59XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYWRmLWRhdGF0YWJsZScsXG4gICAgc3R5bGVVcmxzOiBbJy4vZGF0YXRhYmxlLmNvbXBvbmVudC5zY3NzJ10sXG4gICAgdGVtcGxhdGVVcmw6ICcuL2RhdGF0YWJsZS5jb21wb25lbnQuaHRtbCcsXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcbiAgICBob3N0OiB7IGNsYXNzOiAnYWRmLWRhdGF0YWJsZScgfVxufSlcbmV4cG9ydCBjbGFzcyBEYXRhVGFibGVDb21wb25lbnQgaW1wbGVtZW50cyBBZnRlckNvbnRlbnRJbml0LCBPbkNoYW5nZXMsIERvQ2hlY2ssIE9uRGVzdHJveSB7XG5cbiAgICBAQ29udGVudENoaWxkKERhdGFDb2x1bW5MaXN0Q29tcG9uZW50LCB7c3RhdGljOiB0cnVlfSlcbiAgICBjb2x1bW5MaXN0OiBEYXRhQ29sdW1uTGlzdENvbXBvbmVudDtcblxuICAgIC8qKiBEYXRhIHNvdXJjZSBmb3IgdGhlIHRhYmxlICovXG4gICAgQElucHV0KClcbiAgICBkYXRhOiBEYXRhVGFibGVBZGFwdGVyO1xuXG4gICAgLyoqIFNlbGVjdHMgdGhlIGRpc3BsYXkgbW9kZSBvZiB0aGUgdGFibGUuIENhbiBiZSBcImxpc3RcIiBvciBcImdhbGxlcnlcIi4gKi9cbiAgICBASW5wdXQoKVxuICAgIGRpc3BsYXk6IHN0cmluZyA9IERpc3BsYXlNb2RlLkxpc3Q7XG5cbiAgICAvKiogVGhlIHJvd3MgdGhhdCB0aGUgZGF0YXRhYmxlIHdpbGwgc2hvdy4gKi9cbiAgICBASW5wdXQoKVxuICAgIHJvd3M6IGFueVtdID0gW107XG5cbiAgICAvKiogRGVmaW5lIHRoZSBzb3J0IG9yZGVyIG9mIHRoZSBkYXRhdGFibGUuIFBvc3NpYmxlIHZhbHVlcyBhcmUgOlxuICAgICAqIFtgY3JlYXRlZGAsIGBkZXNjYF0sIFtgY3JlYXRlZGAsIGBhc2NgXSwgW2BkdWVgLCBgZGVzY2BdLCBbYGR1ZWAsIGBhc2NgXVxuICAgICAqL1xuICAgIEBJbnB1dCgpXG4gICAgc29ydGluZzogYW55W10gPSBbXTtcblxuICAgIC8qKiBUaGUgY29sdW1ucyB0aGF0IHRoZSBkYXRhdGFibGUgd2lsbCBzaG93LiAqL1xuICAgIEBJbnB1dCgpXG4gICAgY29sdW1uczogYW55W10gPSBbXTtcblxuICAgIC8qKiBSb3cgc2VsZWN0aW9uIG1vZGUuIENhbiBiZSBub25lLCBgc2luZ2xlYCBvciBgbXVsdGlwbGVgLiBGb3IgYG11bHRpcGxlYCBtb2RlLFxuICAgICAqIHlvdSBjYW4gdXNlIENtZCAobWFjT1MpIG9yIEN0cmwgKFdpbikgbW9kaWZpZXIga2V5IHRvIHRvZ2dsZSBzZWxlY3Rpb24gZm9yIG11bHRpcGxlIHJvd3MuXG4gICAgICovXG4gICAgQElucHV0KClcbiAgICBzZWxlY3Rpb25Nb2RlOiBzdHJpbmcgPSAnc2luZ2xlJzsgLy8gbm9uZXxzaW5nbGV8bXVsdGlwbGVcblxuICAgIC8qKiBUb2dnbGVzIG11bHRpcGxlIHJvdyBzZWxlY3Rpb24sIHdoaWNoIHJlbmRlcnMgY2hlY2tib3hlcyBhdCB0aGUgYmVnaW5uaW5nIG9mIGVhY2ggcm93LiAqL1xuICAgIEBJbnB1dCgpXG4gICAgbXVsdGlzZWxlY3Q6IGJvb2xlYW4gPSBmYWxzZTtcblxuICAgIC8qKiBUb2dnbGVzIHRoZSBkYXRhIGFjdGlvbnMgY29sdW1uLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgYWN0aW9uczogYm9vbGVhbiA9IGZhbHNlO1xuXG4gICAgLyoqIFBvc2l0aW9uIG9mIHRoZSBhY3Rpb25zIGRyb3Bkb3duIG1lbnUuIENhbiBiZSBcImxlZnRcIiBvciBcInJpZ2h0XCIuICovXG4gICAgQElucHV0KClcbiAgICBhY3Rpb25zUG9zaXRpb246IHN0cmluZyA9ICdyaWdodCc7IC8vIGxlZnR8cmlnaHRcblxuICAgIC8qKiBGYWxsYmFjayBpbWFnZSBmb3Igcm93cyB3aGVyZSB0aGUgdGh1bWJuYWlsIGlzIG1pc3NpbmcuICovXG4gICAgQElucHV0KClcbiAgICBmYWxsYmFja1RodW1ibmFpbDogc3RyaW5nO1xuXG4gICAgLyoqIFRvZ2dsZXMgY3VzdG9tIGNvbnRleHQgbWVudSBmb3IgdGhlIGNvbXBvbmVudC4gKi9cbiAgICBASW5wdXQoKVxuICAgIGNvbnRleHRNZW51OiBib29sZWFuID0gZmFsc2U7XG5cbiAgICAvKiogVG9nZ2xlcyBmaWxlIGRyb3Agc3VwcG9ydCBmb3Igcm93cyAoc2VlXG4gICAgICogW1VwbG9hZCBkaXJlY3RpdmVdKHVwbG9hZC5kaXJlY3RpdmUubWQpIGZvciBmdXJ0aGVyIGRldGFpbHMpLlxuICAgICAqL1xuICAgIEBJbnB1dCgpXG4gICAgYWxsb3dEcm9wRmlsZXM6IGJvb2xlYW4gPSBmYWxzZTtcblxuICAgIC8qKiBUaGUgaW5saW5lIHN0eWxlIHRvIGFwcGx5IHRvIGV2ZXJ5IHJvdy4gU2VlXG4gICAgICogW05nU3R5bGVdKGh0dHBzOi8vYW5ndWxhci5pby9kb2NzL3RzL2xhdGVzdC9hcGkvY29tbW9uL2luZGV4L05nU3R5bGUtZGlyZWN0aXZlLmh0bWwpXG4gICAgICogZG9jcyBmb3IgbW9yZSBkZXRhaWxzIGFuZCB1c2FnZSBleGFtcGxlcy5cbiAgICAgKi9cbiAgICBASW5wdXQoKVxuICAgIHJvd1N0eWxlOiBzdHJpbmc7XG5cbiAgICAvKiogVGhlIENTUyBjbGFzcyB0byBhcHBseSB0byBldmVyeSByb3cuICovXG4gICAgQElucHV0KClcbiAgICByb3dTdHlsZUNsYXNzOiBzdHJpbmcgPSAnJztcblxuICAgIC8qKiBUb2dnbGVzIHRoZSBoZWFkZXIuICovXG4gICAgQElucHV0KClcbiAgICBzaG93SGVhZGVyOiBib29sZWFuID0gdHJ1ZTtcblxuICAgIC8qKiBUb2dnbGVzIHRoZSBzdGlja3kgaGVhZGVyIG1vZGUuICovXG4gICAgQElucHV0KClcbiAgICBzdGlja3lIZWFkZXI6IGJvb2xlYW4gPSBmYWxzZTtcblxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIHVzZXIgY2xpY2tzIGEgcm93LiAqL1xuICAgIEBPdXRwdXQoKVxuICAgIHJvd0NsaWNrID0gbmV3IEV2ZW50RW1pdHRlcjxEYXRhUm93RXZlbnQ+KCk7XG5cbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSB1c2VyIGRvdWJsZS1jbGlja3MgYSByb3cuICovXG4gICAgQE91dHB1dCgpXG4gICAgcm93RGJsQ2xpY2sgPSBuZXcgRXZlbnRFbWl0dGVyPERhdGFSb3dFdmVudD4oKTtcblxuICAgIC8qKiBFbWl0dGVkIGJlZm9yZSB0aGUgY29udGV4dCBtZW51IGlzIGRpc3BsYXllZCBmb3IgYSByb3cuICovXG4gICAgQE91dHB1dCgpXG4gICAgc2hvd1Jvd0NvbnRleHRNZW51ID0gbmV3IEV2ZW50RW1pdHRlcjxEYXRhQ2VsbEV2ZW50PigpO1xuXG4gICAgLyoqIEVtaXR0ZWQgYmVmb3JlIHRoZSBhY3Rpb25zIG1lbnUgaXMgZGlzcGxheWVkIGZvciBhIHJvdy4gKi9cbiAgICBAT3V0cHV0KClcbiAgICBzaG93Um93QWN0aW9uc01lbnUgPSBuZXcgRXZlbnRFbWl0dGVyPERhdGFDZWxsRXZlbnQ+KCk7XG5cbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSB1c2VyIGV4ZWN1dGVzIGEgcm93IGFjdGlvbi4gKi9cbiAgICBAT3V0cHV0KClcbiAgICBleGVjdXRlUm93QWN0aW9uID0gbmV3IEV2ZW50RW1pdHRlcjxEYXRhUm93QWN0aW9uRXZlbnQ+KCk7XG5cbiAgICAvKiogRmxhZyB0aGF0IGluZGljYXRlcyBpZiB0aGUgZGF0YXRhYmxlIGlzIGluIGxvYWRpbmcgc3RhdGUgYW5kIG5lZWRzIHRvIHNob3cgdGhlXG4gICAgICogbG9hZGluZyB0ZW1wbGF0ZSAoc2VlIHRoZSBkb2NzIHRvIGxlYXJuIGhvdyB0byBjb25maWd1cmUgYSBsb2FkaW5nIHRlbXBsYXRlKS5cbiAgICAgKi9cbiAgICBASW5wdXQoKVxuICAgIGxvYWRpbmc6IGJvb2xlYW4gPSBmYWxzZTtcblxuICAgIC8qKiBGbGFnIHRoYXQgaW5kaWNhdGVzIGlmIHRoZSBkYXRhdGFibGUgc2hvdWxkIHNob3cgdGhlIFwibm8gcGVybWlzc2lvblwiIHRlbXBsYXRlLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgbm9QZXJtaXNzaW9uOiBib29sZWFuID0gZmFsc2U7XG5cbiAgICAvKipcbiAgICAgKiBTaG91bGQgdGhlIGl0ZW1zIGZvciB0aGUgcm93IGFjdGlvbnMgbWVudSBiZSBjYWNoZWQgZm9yIHJldXNlIGFmdGVyIHRoZXkgYXJlIGxvYWRlZFxuICAgICAqIHRoZSBmaXJzdCB0aW1lP1xuICAgICAqL1xuICAgIEBJbnB1dCgpXG4gICAgcm93TWVudUNhY2hlRW5hYmxlZCA9IHRydWU7XG5cbiAgICBub0NvbnRlbnRUZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcbiAgICBub1Blcm1pc3Npb25UZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcbiAgICBsb2FkaW5nVGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XG5cbiAgICBpc1NlbGVjdEFsbENoZWNrZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgICBzZWxlY3Rpb24gPSBuZXcgQXJyYXk8RGF0YVJvdz4oKTtcblxuICAgIC8qKiBUaGlzIGFycmF5IG9mIGZha2Ugcm93cyBmaXggdGhlIGZsZXggbGF5b3V0IGZvciB0aGUgZ2FsbGVyeSB2aWV3ICovXG4gICAgZmFrZVJvd3MgPSBbXTtcblxuICAgIHByaXZhdGUgY2xpY2tPYnNlcnZlcjogT2JzZXJ2ZXI8RGF0YVJvd0V2ZW50PjtcbiAgICBwcml2YXRlIGNsaWNrJDogT2JzZXJ2YWJsZTxEYXRhUm93RXZlbnQ+O1xuXG4gICAgcHJpdmF0ZSBkaWZmZXI6IGFueTtcbiAgICBwcml2YXRlIHJvd01lbnVDYWNoZTogb2JqZWN0ID0ge307XG5cbiAgICBwcml2YXRlIHN1YnNjcmlwdGlvbnM6IFN1YnNjcmlwdGlvbltdID0gW107XG4gICAgcHJpdmF0ZSBzaW5nbGVDbGlja1N0cmVhbVN1YjogU3Vic2NyaXB0aW9uO1xuICAgIHByaXZhdGUgbXVsdGlDbGlja1N0cmVhbVN1YjogU3Vic2NyaXB0aW9uO1xuICAgIHByaXZhdGUgZGF0YVJvd3NDaGFuZ2VkOiBTdWJzY3JpcHRpb247XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGVsZW1lbnRSZWY6IEVsZW1lbnRSZWYsXG4gICAgICAgICAgICAgICAgZGlmZmVyczogSXRlcmFibGVEaWZmZXJzKSB7XG4gICAgICAgIGlmIChkaWZmZXJzKSB7XG4gICAgICAgICAgICB0aGlzLmRpZmZlciA9IGRpZmZlcnMuZmluZChbXSkuY3JlYXRlKG51bGwpO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuY2xpY2skID0gbmV3IE9ic2VydmFibGU8RGF0YVJvd0V2ZW50Pigob2JzZXJ2ZXIpID0+IHRoaXMuY2xpY2tPYnNlcnZlciA9IG9ic2VydmVyKVxuICAgICAgICAgICAgLnBpcGUoc2hhcmUoKSk7XG4gICAgfVxuXG4gICAgbmdBZnRlckNvbnRlbnRJbml0KCkge1xuICAgICAgICBpZiAodGhpcy5jb2x1bW5MaXN0KSB7XG4gICAgICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaChcbiAgICAgICAgICAgICAgICB0aGlzLmNvbHVtbkxpc3QuY29sdW1ucy5jaGFuZ2VzLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0VGFibGVTY2hlbWEoKTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLmRhdGF0YWJsZUxheW91dEZpeCgpO1xuICAgICAgICB0aGlzLnNldFRhYmxlU2NoZW1hKCk7XG4gICAgfVxuXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xuICAgICAgICB0aGlzLmluaXRBbmRTdWJzY3JpYmVDbGlja1N0cmVhbSgpO1xuICAgICAgICBpZiAodGhpcy5pc1Byb3BlcnR5Q2hhbmdlZChjaGFuZ2VzWydkYXRhJ10pKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5pc1RhYmxlRW1wdHkoKSkge1xuICAgICAgICAgICAgICAgIHRoaXMuaW5pdFRhYmxlKCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuZGF0YSA9IGNoYW5nZXNbJ2RhdGEnXS5jdXJyZW50VmFsdWU7XG4gICAgICAgICAgICAgICAgdGhpcy5yZXNldFNlbGVjdGlvbigpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuaXNQcm9wZXJ0eUNoYW5nZWQoY2hhbmdlc1sncm93cyddKSkge1xuICAgICAgICAgICAgaWYgKHRoaXMuaXNUYWJsZUVtcHR5KCkpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmluaXRUYWJsZSgpO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldFRhYmxlUm93cyhjaGFuZ2VzWydyb3dzJ10uY3VycmVudFZhbHVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChjaGFuZ2VzLnNlbGVjdGlvbk1vZGUgJiYgIWNoYW5nZXMuc2VsZWN0aW9uTW9kZS5pc0ZpcnN0Q2hhbmdlKCkpIHtcbiAgICAgICAgICAgIHRoaXMucmVzZXRTZWxlY3Rpb24oKTtcbiAgICAgICAgICAgIHRoaXMuZW1pdFJvd1NlbGVjdGlvbkV2ZW50KCdyb3ctdW5zZWxlY3QnLCBudWxsKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0aGlzLmlzUHJvcGVydHlDaGFuZ2VkKGNoYW5nZXNbJ3NvcnRpbmcnXSkpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0VGFibGVTb3J0aW5nKGNoYW5nZXNbJ3NvcnRpbmcnXS5jdXJyZW50VmFsdWUpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuaXNQcm9wZXJ0eUNoYW5nZWQoY2hhbmdlc1snZGlzcGxheSddKSkge1xuICAgICAgICAgICAgdGhpcy5kYXRhdGFibGVMYXlvdXRGaXgoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG5nRG9DaGVjaygpIHtcbiAgICAgICAgY29uc3QgY2hhbmdlcyA9IHRoaXMuZGlmZmVyLmRpZmYodGhpcy5yb3dzKTtcbiAgICAgICAgaWYgKGNoYW5nZXMpIHtcbiAgICAgICAgICAgIHRoaXMuc2V0VGFibGVSb3dzKHRoaXMucm93cyk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBpc1Byb3BlcnR5Q2hhbmdlZChwcm9wZXJ0eTogU2ltcGxlQ2hhbmdlKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiBwcm9wZXJ0eSAmJiBwcm9wZXJ0eS5jdXJyZW50VmFsdWUgPyB0cnVlIDogZmFsc2U7XG4gICAgfVxuXG4gICAgY29udmVydFRvUm93c0RhdGEocm93czogYW55IFtdKTogT2JqZWN0RGF0YVJvd1tdIHtcbiAgICAgICAgcmV0dXJuIHJvd3MubWFwKChyb3cpID0+IG5ldyBPYmplY3REYXRhUm93KHJvdywgcm93LmlzU2VsZWN0ZWQpKTtcbiAgICB9XG5cbiAgICBjb252ZXJ0VG9EYXRhU29ydGluZyhzb3J0aW5nOiBhbnlbXSk6IERhdGFTb3J0aW5nIHtcbiAgICAgICAgaWYgKHNvcnRpbmcgJiYgc29ydGluZy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICByZXR1cm4gbmV3IERhdGFTb3J0aW5nKHNvcnRpbmdbMF0sIHNvcnRpbmdbMV0pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBpbml0QW5kU3Vic2NyaWJlQ2xpY2tTdHJlYW0oKSB7XG4gICAgICAgIHRoaXMudW5zdWJzY3JpYmVDbGlja1N0cmVhbSgpO1xuICAgICAgICBjb25zdCBzaW5nbGVDbGlja1N0cmVhbSA9IHRoaXMuY2xpY2skXG4gICAgICAgICAgICAucGlwZShcbiAgICAgICAgICAgICAgICBidWZmZXIoXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2xpY2skLnBpcGUoXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWJvdW5jZVRpbWUoMjUwKVxuICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICBtYXAoKGxpc3QpID0+IGxpc3QpLFxuICAgICAgICAgICAgICAgIGZpbHRlcigoeCkgPT4geC5sZW5ndGggPT09IDEpXG4gICAgICAgICAgICApO1xuXG4gICAgICAgIHRoaXMuc2luZ2xlQ2xpY2tTdHJlYW1TdWIgPSBzaW5nbGVDbGlja1N0cmVhbS5zdWJzY3JpYmUoKGRhdGFSb3dFdmVudHM6IERhdGFSb3dFdmVudFtdKSA9PiB7XG4gICAgICAgICAgICBjb25zdCBldmVudDogRGF0YVJvd0V2ZW50ID0gZGF0YVJvd0V2ZW50c1swXTtcbiAgICAgICAgICAgIHRoaXMuaGFuZGxlUm93U2VsZWN0aW9uKGV2ZW50LnZhbHVlLCA8TW91c2VFdmVudCB8IEtleWJvYXJkRXZlbnQ+IGV2ZW50LmV2ZW50KTtcbiAgICAgICAgICAgIHRoaXMucm93Q2xpY2suZW1pdChldmVudCk7XG4gICAgICAgICAgICBpZiAoIWV2ZW50LmRlZmF1bHRQcmV2ZW50ZWQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5kaXNwYXRjaEV2ZW50KFxuICAgICAgICAgICAgICAgICAgICBuZXcgQ3VzdG9tRXZlbnQoJ3Jvdy1jbGljaycsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRldGFpbDogZXZlbnQsXG4gICAgICAgICAgICAgICAgICAgICAgICBidWJibGVzOiB0cnVlXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG5cbiAgICAgICAgY29uc3QgbXVsdGlDbGlja1N0cmVhbSA9IHRoaXMuY2xpY2skXG4gICAgICAgICAgICAucGlwZShcbiAgICAgICAgICAgICAgICBidWZmZXIoXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2xpY2skLnBpcGUoXG4gICAgICAgICAgICAgICAgICAgICAgICBkZWJvdW5jZVRpbWUoMjUwKVxuICAgICAgICAgICAgICAgICAgICApXG4gICAgICAgICAgICAgICAgKSxcbiAgICAgICAgICAgICAgICBtYXAoKGxpc3QpID0+IGxpc3QpLFxuICAgICAgICAgICAgICAgIGZpbHRlcigoeCkgPT4geC5sZW5ndGggPj0gMilcbiAgICAgICAgICAgICk7XG5cbiAgICAgICAgdGhpcy5tdWx0aUNsaWNrU3RyZWFtU3ViID0gbXVsdGlDbGlja1N0cmVhbS5zdWJzY3JpYmUoKGRhdGFSb3dFdmVudHM6IERhdGFSb3dFdmVudFtdKSA9PiB7XG4gICAgICAgICAgICBjb25zdCBldmVudDogRGF0YVJvd0V2ZW50ID0gZGF0YVJvd0V2ZW50c1swXTtcbiAgICAgICAgICAgIHRoaXMucm93RGJsQ2xpY2suZW1pdChldmVudCk7XG4gICAgICAgICAgICBpZiAoIWV2ZW50LmRlZmF1bHRQcmV2ZW50ZWQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5kaXNwYXRjaEV2ZW50KFxuICAgICAgICAgICAgICAgICAgICBuZXcgQ3VzdG9tRXZlbnQoJ3Jvdy1kYmxjbGljaycsIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGRldGFpbDogZXZlbnQsXG4gICAgICAgICAgICAgICAgICAgICAgICBidWJibGVzOiB0cnVlXG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSB1bnN1YnNjcmliZUNsaWNrU3RyZWFtKCkge1xuICAgICAgICBpZiAodGhpcy5zaW5nbGVDbGlja1N0cmVhbVN1Yikge1xuICAgICAgICAgICAgdGhpcy5zaW5nbGVDbGlja1N0cmVhbVN1Yi51bnN1YnNjcmliZSgpO1xuICAgICAgICAgICAgdGhpcy5zaW5nbGVDbGlja1N0cmVhbVN1YiA9IG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgaWYgKHRoaXMubXVsdGlDbGlja1N0cmVhbVN1Yikge1xuICAgICAgICAgICAgdGhpcy5tdWx0aUNsaWNrU3RyZWFtU3ViLnVuc3Vic2NyaWJlKCk7XG4gICAgICAgICAgICB0aGlzLm11bHRpQ2xpY2tTdHJlYW1TdWIgPSBudWxsO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBpbml0VGFibGUoKSB7XG4gICAgICAgIHRoaXMuZGF0YSA9IG5ldyBPYmplY3REYXRhVGFibGVBZGFwdGVyKHRoaXMucm93cywgdGhpcy5jb2x1bW5zKTtcbiAgICAgICAgdGhpcy5zZXRUYWJsZVNvcnRpbmcodGhpcy5zb3J0aW5nKTtcbiAgICAgICAgdGhpcy5yZXNldFNlbGVjdGlvbigpO1xuICAgICAgICB0aGlzLnJvd01lbnVDYWNoZSA9IHt9O1xuICAgIH1cblxuICAgIGlzVGFibGVFbXB0eSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGF0YSA9PT0gdW5kZWZpbmVkIHx8IHRoaXMuZGF0YSA9PT0gbnVsbDtcbiAgICB9XG5cbiAgICBwcml2YXRlIHNldFRhYmxlUm93cyhyb3dzOiBhbnlbXSkge1xuICAgICAgICBpZiAodGhpcy5kYXRhKSB7XG4gICAgICAgICAgICB0aGlzLnJlc2V0U2VsZWN0aW9uKCk7XG4gICAgICAgICAgICB0aGlzLmRhdGEuc2V0Um93cyh0aGlzLmNvbnZlcnRUb1Jvd3NEYXRhKHJvd3MpKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgc2V0VGFibGVTY2hlbWEoKSB7XG4gICAgICAgIGxldCBzY2hlbWEgPSBbXTtcbiAgICAgICAgaWYgKCF0aGlzLmNvbHVtbnMgfHwgdGhpcy5jb2x1bW5zLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgc2NoZW1hID0gdGhpcy5nZXRTY2hlbWFGcm9tSHRtbCgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgc2NoZW1hID0gdGhpcy5jb2x1bW5zLmNvbmNhdCh0aGlzLmdldFNjaGVtYUZyb21IdG1sKCkpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5jb2x1bW5zID0gc2NoZW1hO1xuXG4gICAgICAgIGlmICh0aGlzLmRhdGEgJiYgdGhpcy5jb2x1bW5zICYmIHRoaXMuY29sdW1ucy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICB0aGlzLmRhdGEuc2V0Q29sdW1ucyh0aGlzLmNvbHVtbnMpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBzZXRUYWJsZVNvcnRpbmcoc29ydGluZykge1xuICAgICAgICBpZiAodGhpcy5kYXRhKSB7XG4gICAgICAgICAgICB0aGlzLmRhdGEuc2V0U29ydGluZyh0aGlzLmNvbnZlcnRUb0RhdGFTb3J0aW5nKHNvcnRpbmcpKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBnZXRTY2hlbWFGcm9tSHRtbCgpOiBhbnkge1xuICAgICAgICBsZXQgc2NoZW1hID0gW107XG4gICAgICAgIGlmICh0aGlzLmNvbHVtbkxpc3QgJiYgdGhpcy5jb2x1bW5MaXN0LmNvbHVtbnMgJiYgdGhpcy5jb2x1bW5MaXN0LmNvbHVtbnMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgc2NoZW1hID0gdGhpcy5jb2x1bW5MaXN0LmNvbHVtbnMubWFwKChjKSA9PiA8RGF0YUNvbHVtbj4gYyk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHNjaGVtYTtcbiAgICB9XG5cbiAgICBvblJvd0NsaWNrKHJvdzogRGF0YVJvdywgbW91c2VFdmVudDogTW91c2VFdmVudCkge1xuICAgICAgICBpZiAobW91c2VFdmVudCkge1xuICAgICAgICAgICAgbW91c2VFdmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHJvdykge1xuICAgICAgICAgICAgY29uc3QgZGF0YVJvd0V2ZW50ID0gbmV3IERhdGFSb3dFdmVudChyb3csIG1vdXNlRXZlbnQsIHRoaXMpO1xuICAgICAgICAgICAgdGhpcy5jbGlja09ic2VydmVyLm5leHQoZGF0YVJvd0V2ZW50KTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG9uRW50ZXJLZXlQcmVzc2VkKHJvdzogRGF0YVJvdywgZTogS2V5Ym9hcmRFdmVudCkge1xuICAgICAgICBpZiAocm93KSB7XG4gICAgICAgICAgICB0aGlzLmhhbmRsZVJvd1NlbGVjdGlvbihyb3csIGUpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBoYW5kbGVSb3dTZWxlY3Rpb24ocm93OiBEYXRhUm93LCBlOiBLZXlib2FyZEV2ZW50IHwgTW91c2VFdmVudCkge1xuICAgICAgICBpZiAodGhpcy5kYXRhKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5pc1NpbmdsZVNlbGVjdGlvbk1vZGUoKSkge1xuICAgICAgICAgICAgICAgIHRoaXMucmVzZXRTZWxlY3Rpb24oKTtcbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdFJvdyhyb3csIHRydWUpO1xuICAgICAgICAgICAgICAgIHRoaXMuZW1pdFJvd1NlbGVjdGlvbkV2ZW50KCdyb3ctc2VsZWN0Jywgcm93KTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMuaXNNdWx0aVNlbGVjdGlvbk1vZGUoKSkge1xuICAgICAgICAgICAgICAgIGNvbnN0IG1vZGlmaWVyID0gZSAmJiAoZS5tZXRhS2V5IHx8IGUuY3RybEtleSk7XG4gICAgICAgICAgICAgICAgbGV0IG5ld1ZhbHVlOiBib29sZWFuO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLnNlbGVjdGlvbi5sZW5ndGggPT09IDEpIHtcbiAgICAgICAgICAgICAgICAgICAgbmV3VmFsdWUgPSAhcm93LmlzU2VsZWN0ZWQ7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgbmV3VmFsdWUgPSBtb2RpZmllciA/ICFyb3cuaXNTZWxlY3RlZCA6IHRydWU7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIGNvbnN0IGRvbUV2ZW50TmFtZSA9IG5ld1ZhbHVlID8gJ3Jvdy1zZWxlY3QnIDogJ3Jvdy11bnNlbGVjdCc7XG5cbiAgICAgICAgICAgICAgICBpZiAoIW1vZGlmaWVyKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmVzZXRTZWxlY3Rpb24oKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgdGhpcy5zZWxlY3RSb3cocm93LCBuZXdWYWx1ZSk7XG4gICAgICAgICAgICAgICAgdGhpcy5lbWl0Um93U2VsZWN0aW9uRXZlbnQoZG9tRXZlbnROYW1lLCByb3cpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgcmVzZXRTZWxlY3Rpb24oKTogdm9pZCB7XG4gICAgICAgIGlmICh0aGlzLmRhdGEpIHtcbiAgICAgICAgICAgIGNvbnN0IHJvd3MgPSB0aGlzLmRhdGEuZ2V0Um93cygpO1xuICAgICAgICAgICAgaWYgKHJvd3MgJiYgcm93cy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgcm93cy5mb3JFYWNoKChyKSA9PiByLmlzU2VsZWN0ZWQgPSBmYWxzZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLnNlbGVjdGlvbiA9IFtdO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuaXNTZWxlY3RBbGxDaGVja2VkID0gZmFsc2U7XG4gICAgfVxuXG4gICAgb25Sb3dEYmxDbGljayhyb3c6IERhdGFSb3csIGV2ZW50PzogRXZlbnQpIHtcbiAgICAgICAgaWYgKGV2ZW50KSB7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB9XG4gICAgICAgIGNvbnN0IGRhdGFSb3dFdmVudCA9IG5ldyBEYXRhUm93RXZlbnQocm93LCBldmVudCwgdGhpcyk7XG4gICAgICAgIHRoaXMuY2xpY2tPYnNlcnZlci5uZXh0KGRhdGFSb3dFdmVudCk7XG4gICAgfVxuXG4gICAgb25Sb3dLZXlVcChyb3c6IERhdGFSb3csIGU6IEtleWJvYXJkRXZlbnQpIHtcbiAgICAgICAgY29uc3QgZXZlbnQgPSBuZXcgQ3VzdG9tRXZlbnQoJ3Jvdy1rZXl1cCcsIHtcbiAgICAgICAgICAgIGRldGFpbDoge1xuICAgICAgICAgICAgICAgIHJvdzogcm93LFxuICAgICAgICAgICAgICAgIGtleWJvYXJkRXZlbnQ6IGUsXG4gICAgICAgICAgICAgICAgc2VuZGVyOiB0aGlzXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgYnViYmxlczogdHJ1ZVxuICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5kaXNwYXRjaEV2ZW50KGV2ZW50KTtcblxuICAgICAgICBpZiAoZXZlbnQuZGVmYXVsdFByZXZlbnRlZCkge1xuICAgICAgICAgICAgZS5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWYgKGUua2V5ID09PSAnRW50ZXInKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5vbktleWJvYXJkTmF2aWdhdGUocm93LCBlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgb25LZXlib2FyZE5hdmlnYXRlKHJvdzogRGF0YVJvdywga2V5Ym9hcmRFdmVudDogS2V5Ym9hcmRFdmVudCkge1xuICAgICAgICBpZiAoa2V5Ym9hcmRFdmVudCkge1xuICAgICAgICAgICAga2V5Ym9hcmRFdmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB9XG5cbiAgICAgICAgY29uc3QgZXZlbnQgPSBuZXcgRGF0YVJvd0V2ZW50KHJvdywga2V5Ym9hcmRFdmVudCwgdGhpcyk7XG5cbiAgICAgICAgdGhpcy5yb3dEYmxDbGljay5lbWl0KGV2ZW50KTtcbiAgICAgICAgdGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQuZGlzcGF0Y2hFdmVudChcbiAgICAgICAgICAgIG5ldyBDdXN0b21FdmVudCgncm93LWRibGNsaWNrJywge1xuICAgICAgICAgICAgICAgIGRldGFpbDogZXZlbnQsXG4gICAgICAgICAgICAgICAgYnViYmxlczogdHJ1ZVxuICAgICAgICAgICAgfSlcbiAgICAgICAgKTtcbiAgICB9XG5cbiAgICBvbkNvbHVtbkhlYWRlckNsaWNrKGNvbHVtbjogRGF0YUNvbHVtbikge1xuICAgICAgICBpZiAoY29sdW1uICYmIGNvbHVtbi5zb3J0YWJsZSkge1xuICAgICAgICAgICAgY29uc3QgY3VycmVudCA9IHRoaXMuZGF0YS5nZXRTb3J0aW5nKCk7XG4gICAgICAgICAgICBsZXQgbmV3RGlyZWN0aW9uID0gJ2FzYyc7XG4gICAgICAgICAgICBpZiAoY3VycmVudCAmJiBjb2x1bW4ua2V5ID09PSBjdXJyZW50LmtleSkge1xuICAgICAgICAgICAgICAgIG5ld0RpcmVjdGlvbiA9IGN1cnJlbnQuZGlyZWN0aW9uID09PSAnYXNjJyA/ICdkZXNjJyA6ICdhc2MnO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5kYXRhLnNldFNvcnRpbmcobmV3IERhdGFTb3J0aW5nKGNvbHVtbi5rZXksIG5ld0RpcmVjdGlvbikpO1xuICAgICAgICAgICAgdGhpcy5lbWl0U29ydGluZ0NoYW5nZWRFdmVudChjb2x1bW4ua2V5LCBuZXdEaXJlY3Rpb24pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgb25TZWxlY3RBbGxDbGljayhtYXRDaGVja2JveENoYW5nZTogTWF0Q2hlY2tib3hDaGFuZ2UpIHtcbiAgICAgICAgdGhpcy5pc1NlbGVjdEFsbENoZWNrZWQgPSBtYXRDaGVja2JveENoYW5nZS5jaGVja2VkO1xuXG4gICAgICAgIGlmICh0aGlzLm11bHRpc2VsZWN0KSB7XG4gICAgICAgICAgICBjb25zdCByb3dzID0gdGhpcy5kYXRhLmdldFJvd3MoKTtcbiAgICAgICAgICAgIGlmIChyb3dzICYmIHJvd3MubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgcm93cy5sZW5ndGg7IGkrKykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdFJvdyhyb3dzW2ldLCBtYXRDaGVja2JveENoYW5nZS5jaGVja2VkKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGNvbnN0IGRvbUV2ZW50TmFtZSA9IG1hdENoZWNrYm94Q2hhbmdlLmNoZWNrZWQgPyAncm93LXNlbGVjdCcgOiAncm93LXVuc2VsZWN0JztcbiAgICAgICAgICAgIGNvbnN0IHJvdyA9IHRoaXMuc2VsZWN0aW9uLmxlbmd0aCA+IDAgPyB0aGlzLnNlbGVjdGlvblswXSA6IG51bGw7XG5cbiAgICAgICAgICAgIHRoaXMuZW1pdFJvd1NlbGVjdGlvbkV2ZW50KGRvbUV2ZW50TmFtZSwgcm93KTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG9uQ2hlY2tib3hDaGFuZ2Uocm93OiBEYXRhUm93LCBldmVudDogTWF0Q2hlY2tib3hDaGFuZ2UpIHtcbiAgICAgICAgY29uc3QgbmV3VmFsdWUgPSBldmVudC5jaGVja2VkO1xuXG4gICAgICAgIHRoaXMuc2VsZWN0Um93KHJvdywgbmV3VmFsdWUpO1xuXG4gICAgICAgIGNvbnN0IGRvbUV2ZW50TmFtZSA9IG5ld1ZhbHVlID8gJ3Jvdy1zZWxlY3QnIDogJ3Jvdy11bnNlbGVjdCc7XG4gICAgICAgIHRoaXMuZW1pdFJvd1NlbGVjdGlvbkV2ZW50KGRvbUV2ZW50TmFtZSwgcm93KTtcbiAgICB9XG5cbiAgICBvbkltYWdlTG9hZGluZ0Vycm9yKGV2ZW50OiBFdmVudCwgcm93OiBEYXRhUm93KSB7XG4gICAgICAgIGlmIChldmVudCkge1xuICAgICAgICAgICAgY29uc3QgZWxlbWVudCA9IDxhbnk+IGV2ZW50LnRhcmdldDtcblxuICAgICAgICAgICAgaWYgKHRoaXMuZmFsbGJhY2tUaHVtYm5haWwpIHtcbiAgICAgICAgICAgICAgICBlbGVtZW50LnNyYyA9IHRoaXMuZmFsbGJhY2tUaHVtYm5haWw7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGVsZW1lbnQuc3JjID0gcm93LmltYWdlRXJyb3JSZXNvbHZlcihldmVudCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBpc0ljb25WYWx1ZShyb3c6IERhdGFSb3csIGNvbDogRGF0YUNvbHVtbik6IGJvb2xlYW4ge1xuICAgICAgICBpZiAocm93ICYmIGNvbCkge1xuICAgICAgICAgICAgY29uc3QgdmFsdWUgPSByb3cuZ2V0VmFsdWUoY29sLmtleSk7XG4gICAgICAgICAgICByZXR1cm4gdmFsdWUgJiYgdmFsdWUuc3RhcnRzV2l0aCgnbWF0ZXJpYWwtaWNvbnM6Ly8nKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gZmFsc2U7XG4gICAgfVxuXG4gICAgYXNJY29uVmFsdWUocm93OiBEYXRhUm93LCBjb2w6IERhdGFDb2x1bW4pOiBzdHJpbmcge1xuICAgICAgICBpZiAodGhpcy5pc0ljb25WYWx1ZShyb3csIGNvbCkpIHtcbiAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gcm93LmdldFZhbHVlKGNvbC5rZXkpIHx8ICcnO1xuICAgICAgICAgICAgcmV0dXJuIHZhbHVlLnJlcGxhY2UoJ21hdGVyaWFsLWljb25zOi8vJywgJycpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIGljb25BbHRUZXh0S2V5KHZhbHVlOiBzdHJpbmcpOiBzdHJpbmcge1xuICAgICAgICByZXR1cm4gdmFsdWUgPyAnSUNPTlMuJyArIHZhbHVlLnN1YnN0cmluZyh2YWx1ZS5sYXN0SW5kZXhPZignLycpICsgMSkucmVwbGFjZSgvXFwuW2Etel0rLywgJycpIDogJyc7XG4gICAgfVxuXG4gICAgaXNDb2x1bW5Tb3J0ZWQoY29sOiBEYXRhQ29sdW1uLCBkaXJlY3Rpb246IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgICAgICBpZiAoY29sICYmIGRpcmVjdGlvbikge1xuICAgICAgICAgICAgY29uc3Qgc29ydGluZyA9IHRoaXMuZGF0YS5nZXRTb3J0aW5nKCk7XG4gICAgICAgICAgICByZXR1cm4gc29ydGluZyAmJiBzb3J0aW5nLmtleSA9PT0gY29sLmtleSAmJiBzb3J0aW5nLmRpcmVjdGlvbiA9PT0gZGlyZWN0aW9uO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBnZXRDb250ZXh0TWVudUFjdGlvbnMocm93OiBEYXRhUm93LCBjb2w6IERhdGFDb2x1bW4pOiBhbnlbXSB7XG4gICAgICAgIGNvbnN0IGV2ZW50ID0gbmV3IERhdGFDZWxsRXZlbnQocm93LCBjb2wsIFtdKTtcbiAgICAgICAgdGhpcy5zaG93Um93Q29udGV4dE1lbnUuZW1pdChldmVudCk7XG4gICAgICAgIHJldHVybiBldmVudC52YWx1ZS5hY3Rpb25zO1xuICAgIH1cblxuICAgIGdldFJvd0FjdGlvbnMocm93OiBEYXRhUm93LCBjb2w/OiBEYXRhQ29sdW1uKTogYW55W10ge1xuICAgICAgICBjb25zdCBpZCA9IHJvdy5nZXRWYWx1ZSgnaWQnKTtcblxuICAgICAgICBpZiAoIXRoaXMucm93TWVudUNhY2hlW2lkXSkge1xuICAgICAgICAgICAgY29uc3QgZXZlbnQgPSBuZXcgRGF0YUNlbGxFdmVudChyb3csIGNvbCwgW10pO1xuICAgICAgICAgICAgdGhpcy5zaG93Um93QWN0aW9uc01lbnUuZW1pdChldmVudCk7XG4gICAgICAgICAgICBpZiAoIXRoaXMucm93TWVudUNhY2hlRW5hYmxlZCkge1xuICAgICAgICAgICAgICAgIHJldHVybiBldmVudC52YWx1ZS5hY3Rpb25zO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5yb3dNZW51Q2FjaGVbaWRdID0gZXZlbnQudmFsdWUuYWN0aW9ucztcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiB0aGlzLnJvd01lbnVDYWNoZVtpZF07XG4gICAgfVxuXG4gICAgb25FeGVjdXRlUm93QWN0aW9uKHJvdzogRGF0YVJvdywgYWN0aW9uOiBhbnkpIHtcbiAgICAgICAgaWYgKGFjdGlvbi5kaXNhYmxlZCB8fCBhY3Rpb24uZGlzYWJsZWQpIHtcbiAgICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5leGVjdXRlUm93QWN0aW9uLmVtaXQobmV3IERhdGFSb3dBY3Rpb25FdmVudChyb3csIGFjdGlvbikpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcm93QWxsb3dzRHJvcChyb3c6IERhdGFSb3cpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHJvdy5pc0Ryb3BUYXJnZXQgPT09IHRydWU7XG4gICAgfVxuXG4gICAgaGFzU2VsZWN0aW9uTW9kZSgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNTaW5nbGVTZWxlY3Rpb25Nb2RlKCkgfHwgdGhpcy5pc011bHRpU2VsZWN0aW9uTW9kZSgpO1xuICAgIH1cblxuICAgIGlzU2luZ2xlU2VsZWN0aW9uTW9kZSgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VsZWN0aW9uTW9kZSAmJiB0aGlzLnNlbGVjdGlvbk1vZGUudG9Mb3dlckNhc2UoKSA9PT0gJ3NpbmdsZSc7XG4gICAgfVxuXG4gICAgaXNNdWx0aVNlbGVjdGlvbk1vZGUoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLnNlbGVjdGlvbk1vZGUgJiYgdGhpcy5zZWxlY3Rpb25Nb2RlLnRvTG93ZXJDYXNlKCkgPT09ICdtdWx0aXBsZSc7XG4gICAgfVxuXG4gICAgZ2V0Um93U3R5bGUocm93OiBEYXRhUm93KTogc3RyaW5nIHtcbiAgICAgICAgcm93LmNzc0NsYXNzID0gcm93LmNzc0NsYXNzID8gcm93LmNzc0NsYXNzIDogJyc7XG4gICAgICAgIHRoaXMucm93U3R5bGVDbGFzcyA9IHRoaXMucm93U3R5bGVDbGFzcyA/IHRoaXMucm93U3R5bGVDbGFzcyA6ICcnO1xuICAgICAgICByZXR1cm4gYCR7cm93LmNzc0NsYXNzfSAke3RoaXMucm93U3R5bGVDbGFzc31gO1xuICAgIH1cblxuICAgIGdldFNvcnRpbmdLZXkoKTogc3RyaW5nIHtcbiAgICAgICAgaWYgKHRoaXMuZGF0YS5nZXRTb3J0aW5nKCkpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmRhdGEuZ2V0U29ydGluZygpLmtleTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHNlbGVjdFJvdyhyb3c6IERhdGFSb3csIHZhbHVlOiBib29sZWFuKSB7XG4gICAgICAgIGlmIChyb3cpIHtcbiAgICAgICAgICAgIHJvdy5pc1NlbGVjdGVkID0gdmFsdWU7XG4gICAgICAgICAgICBjb25zdCBpZHggPSB0aGlzLnNlbGVjdGlvbi5pbmRleE9mKHJvdyk7XG4gICAgICAgICAgICBpZiAodmFsdWUpIHtcbiAgICAgICAgICAgICAgICBpZiAoaWR4IDwgMCkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGlvbi5wdXNoKHJvdyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICBpZiAoaWR4ID4gLTEpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZWxlY3Rpb24uc3BsaWNlKGlkeCwgMSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgZ2V0Q2VsbFRvb2x0aXAocm93OiBEYXRhUm93LCBjb2w6IERhdGFDb2x1bW4pOiBzdHJpbmcge1xuICAgICAgICBpZiAocm93ICYmIGNvbCAmJiBjb2wuZm9ybWF0VG9vbHRpcCkge1xuICAgICAgICAgICAgY29uc3QgcmVzdWx0OiBzdHJpbmcgPSBjb2wuZm9ybWF0VG9vbHRpcChyb3csIGNvbCk7XG4gICAgICAgICAgICBpZiAocmVzdWx0KSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHJlc3VsdDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gbnVsbDtcbiAgICB9XG5cbiAgICBnZXRTb3J0YWJsZUNvbHVtbnMoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmRhdGEuZ2V0Q29sdW1ucygpLmZpbHRlcigoY29sdW1uKSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gY29sdW1uLnNvcnRhYmxlID09PSB0cnVlO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBpc0VtcHR5KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5kYXRhLmdldFJvd3MoKS5sZW5ndGggPT09IDA7XG4gICAgfVxuXG4gICAgaXNIZWFkZXJWaXNpYmxlKCkge1xuICAgICAgICByZXR1cm4gIXRoaXMubG9hZGluZyAmJiAhdGhpcy5pc0VtcHR5KCkgJiYgIXRoaXMubm9QZXJtaXNzaW9uO1xuICAgIH1cblxuICAgIGlzU3RpY2t5SGVhZGVyRW5hYmxlZCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RpY2t5SGVhZGVyICYmIHRoaXMuaXNIZWFkZXJWaXNpYmxlKCk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBlbWl0Um93U2VsZWN0aW9uRXZlbnQobmFtZTogc3RyaW5nLCByb3c6IERhdGFSb3cpIHtcbiAgICAgICAgY29uc3QgZG9tRXZlbnQgPSBuZXcgQ3VzdG9tRXZlbnQobmFtZSwge1xuICAgICAgICAgICAgZGV0YWlsOiB7XG4gICAgICAgICAgICAgICAgcm93OiByb3csXG4gICAgICAgICAgICAgICAgc2VsZWN0aW9uOiB0aGlzLnNlbGVjdGlvblxuICAgICAgICAgICAgfSxcbiAgICAgICAgICAgIGJ1YmJsZXM6IHRydWVcbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LmRpc3BhdGNoRXZlbnQoZG9tRXZlbnQpO1xuICAgIH1cblxuICAgIHByaXZhdGUgZW1pdFNvcnRpbmdDaGFuZ2VkRXZlbnQoa2V5OiBzdHJpbmcsIGRpcmVjdGlvbjogc3RyaW5nKSB7XG4gICAgICAgIGNvbnN0IGRvbUV2ZW50ID0gbmV3IEN1c3RvbUV2ZW50KCdzb3J0aW5nLWNoYW5nZWQnLCB7XG4gICAgICAgICAgICBkZXRhaWw6IHtcbiAgICAgICAgICAgICAgICBrZXksXG4gICAgICAgICAgICAgICAgZGlyZWN0aW9uXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgYnViYmxlczogdHJ1ZVxuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQuZGlzcGF0Y2hFdmVudChkb21FdmVudCk7XG4gICAgfVxuXG4gICAgbmdPbkRlc3Ryb3koKSB7XG4gICAgICAgIHRoaXMudW5zdWJzY3JpYmVDbGlja1N0cmVhbSgpO1xuXG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5mb3JFYWNoKChzKSA9PiBzLnVuc3Vic2NyaWJlKCkpO1xuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMgPSBbXTtcblxuICAgICAgICBpZiAodGhpcy5kYXRhUm93c0NoYW5nZWQpIHtcbiAgICAgICAgICAgIHRoaXMuZGF0YVJvd3NDaGFuZ2VkLnVuc3Vic2NyaWJlKCk7XG4gICAgICAgICAgICB0aGlzLmRhdGFSb3dzQ2hhbmdlZCA9IG51bGw7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBkYXRhdGFibGVMYXlvdXRGaXgoKSB7XG4gICAgICAgIGNvbnN0IG1heEdhbGxlcnlSb3dzID0gMjU7XG5cbiAgICAgICAgaWYgKHRoaXMuZGlzcGxheSA9PT0gJ2dhbGxlcnknKSB7XG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IG1heEdhbGxlcnlSb3dzOyBpKyspIHtcbiAgICAgICAgICAgICAgIHRoaXMuZmFrZVJvd3MucHVzaCgnJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmZha2VSb3dzID0gW107XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBnZXROYW1lQ29sdW1uVmFsdWUoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmRhdGEuZ2V0Q29sdW1ucygpLmZpbmQoIChlbDogYW55KSA9PiB7XG4gICAgICAgICAgICByZXR1cm4gZWwua2V5LmluY2x1ZGVzKCduYW1lJyk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGdldEF1dG9tYXRpb25WYWx1ZShyb3c6IERhdGFSb3csIGNvbDogRGF0YUNvbHVtbikge1xuICAgICAgICBjb25zdCBuYW1lID0gdGhpcy5nZXROYW1lQ29sdW1uVmFsdWUoKTtcbiAgICAgICAgcmV0dXJuIG5hbWUgPyByb3cuZ2V0VmFsdWUobmFtZS5rZXkpIDogJyc7XG4gICAgfVxufVxuXG5leHBvcnQgaW50ZXJmYWNlIERhdGFUYWJsZURyb3BFdmVudCB7XG4gICAgZGV0YWlsOiB7XG4gICAgICAgIHRhcmdldDogJ2NlbGwnIHwgJ2hlYWRlcic7XG4gICAgICAgIGV2ZW50OiBFdmVudDtcbiAgICAgICAgY29sdW1uOiBEYXRhQ29sdW1uO1xuICAgICAgICByb3c/OiBEYXRhUm93XG4gICAgfTtcblxuICAgIHByZXZlbnREZWZhdWx0KCk6IHZvaWQ7XG59XG4iXX0=