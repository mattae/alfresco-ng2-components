/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ViewEncapsulation } from '@angular/core';
import { DataTableCellComponent } from './datatable-cell.component';
import { UserPreferencesService, UserPreferenceValues } from '../../../services/user-preferences.service';
import { AlfrescoApiService } from '../../../services/alfresco-api.service';
import { AppConfigService } from '../../../app-config/app-config.service';
import { takeUntil } from 'rxjs/operators';
export class DateCellComponent extends DataTableCellComponent {
    /**
     * @param {?} userPreferenceService
     * @param {?} alfrescoApiService
     * @param {?} appConfig
     */
    constructor(userPreferenceService, alfrescoApiService, appConfig) {
        super(alfrescoApiService);
        this.dateFormat = appConfig.get('dateValues.defaultDateFormat', DateCellComponent.DATE_FORMAT);
        if (userPreferenceService) {
            userPreferenceService
                .select(UserPreferenceValues.Locale)
                .pipe(takeUntil(this.onDestroy$))
                .subscribe((/**
             * @param {?} locale
             * @return {?}
             */
            locale => this.currentLocale = locale));
        }
    }
    /**
     * @return {?}
     */
    get format() {
        if (this.column) {
            return this.column.format || this.dateFormat;
        }
        return this.dateFormat;
    }
}
DateCellComponent.DATE_FORMAT = 'medium';
DateCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-date-cell',
                template: `
        <ng-container>
            <span
                [attr.aria-label]="value$ | async | adfTimeAgo: currentLocale"
                title="{{ tooltip | adfLocalizedDate: 'medium' }}"
                class="adf-datatable-cell-value"
                *ngIf="format === 'timeAgo'; else standard_date">
                {{ value$ | async | adfTimeAgo: currentLocale }}
            </span>
        </ng-container>
        <ng-template #standard_date>
            <span
                class="adf-datatable-cell-value"
                title="{{ tooltip | adfLocalizedDate: format }}"
                class="adf-datatable-cell-value"
                [attr.aria-label]="value$ | async | adfLocalizedDate: format">
                {{ value$ | async | adfLocalizedDate: format }}
            </span>
        </ng-template>
    `,
                encapsulation: ViewEncapsulation.None,
                host: { class: 'adf-date-cell adf-datatable-content-cell' }
            }] }
];
/** @nocollapse */
DateCellComponent.ctorParameters = () => [
    { type: UserPreferencesService },
    { type: AlfrescoApiService },
    { type: AppConfigService }
];
if (false) {
    /** @type {?} */
    DateCellComponent.DATE_FORMAT;
    /** @type {?} */
    DateCellComponent.prototype.currentLocale;
    /** @type {?} */
    DateCellComponent.prototype.dateFormat;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRhdGF0YWJsZS9jb21wb25lbnRzL2RhdGF0YWJsZS9kYXRlLWNlbGwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDN0QsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDcEUsT0FBTyxFQUNILHNCQUFzQixFQUN0QixvQkFBb0IsRUFDdkIsTUFBTSw0Q0FBNEMsQ0FBQztBQUNwRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUM1RSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUMxRSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUE0QjNDLE1BQU0sT0FBTyxpQkFBa0IsU0FBUSxzQkFBc0I7Ozs7OztJQWN6RCxZQUNJLHFCQUE2QyxFQUM3QyxrQkFBc0MsRUFDdEMsU0FBMkI7UUFFM0IsS0FBSyxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFFMUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDLDhCQUE4QixFQUFFLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQy9GLElBQUkscUJBQXFCLEVBQUU7WUFDdkIscUJBQXFCO2lCQUNoQixNQUFNLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDO2lCQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztpQkFDaEMsU0FBUzs7OztZQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLEVBQUMsQ0FBQztTQUN6RDtJQUNMLENBQUM7Ozs7SUFyQkQsSUFBSSxNQUFNO1FBQ04sSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2IsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDO1NBQ2hEO1FBQ0QsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO0lBQzNCLENBQUM7O0FBVk0sNkJBQVcsR0FBRyxRQUFRLENBQUM7O1lBNUJqQyxTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGVBQWU7Z0JBRXpCLFFBQVEsRUFBRTs7Ozs7Ozs7Ozs7Ozs7Ozs7OztLQW1CVDtnQkFDRCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtnQkFDckMsSUFBSSxFQUFFLEVBQUUsS0FBSyxFQUFFLDBDQUEwQyxFQUFFO2FBQzlEOzs7O1lBaENHLHNCQUFzQjtZQUdqQixrQkFBa0I7WUFDbEIsZ0JBQWdCOzs7O0lBK0JyQiw4QkFBOEI7O0lBRTlCLDBDQUFzQjs7SUFDdEIsdUNBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRGF0YVRhYmxlQ2VsbENvbXBvbmVudCB9IGZyb20gJy4vZGF0YXRhYmxlLWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHtcclxuICAgIFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UsXHJcbiAgICBVc2VyUHJlZmVyZW5jZVZhbHVlc1xyXG59IGZyb20gJy4uLy4uLy4uL3NlcnZpY2VzL3VzZXItcHJlZmVyZW5jZXMuc2VydmljZSc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NlcnZpY2VzL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1kYXRlLWNlbGwnLFxyXG5cclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgPG5nLWNvbnRhaW5lcj5cclxuICAgICAgICAgICAgPHNwYW5cclxuICAgICAgICAgICAgICAgIFthdHRyLmFyaWEtbGFiZWxdPVwidmFsdWUkIHwgYXN5bmMgfCBhZGZUaW1lQWdvOiBjdXJyZW50TG9jYWxlXCJcclxuICAgICAgICAgICAgICAgIHRpdGxlPVwie3sgdG9vbHRpcCB8IGFkZkxvY2FsaXplZERhdGU6ICdtZWRpdW0nIH19XCJcclxuICAgICAgICAgICAgICAgIGNsYXNzPVwiYWRmLWRhdGF0YWJsZS1jZWxsLXZhbHVlXCJcclxuICAgICAgICAgICAgICAgICpuZ0lmPVwiZm9ybWF0ID09PSAndGltZUFnbyc7IGVsc2Ugc3RhbmRhcmRfZGF0ZVwiPlxyXG4gICAgICAgICAgICAgICAge3sgdmFsdWUkIHwgYXN5bmMgfCBhZGZUaW1lQWdvOiBjdXJyZW50TG9jYWxlIH19XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgICAgICA8bmctdGVtcGxhdGUgI3N0YW5kYXJkX2RhdGU+XHJcbiAgICAgICAgICAgIDxzcGFuXHJcbiAgICAgICAgICAgICAgICBjbGFzcz1cImFkZi1kYXRhdGFibGUtY2VsbC12YWx1ZVwiXHJcbiAgICAgICAgICAgICAgICB0aXRsZT1cInt7IHRvb2x0aXAgfCBhZGZMb2NhbGl6ZWREYXRlOiBmb3JtYXQgfX1cIlxyXG4gICAgICAgICAgICAgICAgY2xhc3M9XCJhZGYtZGF0YXRhYmxlLWNlbGwtdmFsdWVcIlxyXG4gICAgICAgICAgICAgICAgW2F0dHIuYXJpYS1sYWJlbF09XCJ2YWx1ZSQgfCBhc3luYyB8IGFkZkxvY2FsaXplZERhdGU6IGZvcm1hdFwiPlxyXG4gICAgICAgICAgICAgICAge3sgdmFsdWUkIHwgYXN5bmMgfCBhZGZMb2NhbGl6ZWREYXRlOiBmb3JtYXQgfX1cclxuICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgIDwvbmctdGVtcGxhdGU+XHJcbiAgICBgLFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICAgIGhvc3Q6IHsgY2xhc3M6ICdhZGYtZGF0ZS1jZWxsIGFkZi1kYXRhdGFibGUtY29udGVudC1jZWxsJyB9XHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEYXRlQ2VsbENvbXBvbmVudCBleHRlbmRzIERhdGFUYWJsZUNlbGxDb21wb25lbnQge1xyXG5cclxuICAgIHN0YXRpYyBEQVRFX0ZPUk1BVCA9ICdtZWRpdW0nO1xyXG5cclxuICAgIGN1cnJlbnRMb2NhbGU6IHN0cmluZztcclxuICAgIGRhdGVGb3JtYXQ6IHN0cmluZztcclxuXHJcbiAgICBnZXQgZm9ybWF0KCk6IHN0cmluZyB7XHJcbiAgICAgICAgaWYgKHRoaXMuY29sdW1uKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbHVtbi5mb3JtYXQgfHwgdGhpcy5kYXRlRm9ybWF0O1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5kYXRlRm9ybWF0O1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHVzZXJQcmVmZXJlbmNlU2VydmljZTogVXNlclByZWZlcmVuY2VzU2VydmljZSxcclxuICAgICAgICBhbGZyZXNjb0FwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICBhcHBDb25maWc6IEFwcENvbmZpZ1NlcnZpY2VcclxuICAgICkge1xyXG4gICAgICAgIHN1cGVyKGFsZnJlc2NvQXBpU2VydmljZSk7XHJcblxyXG4gICAgICAgIHRoaXMuZGF0ZUZvcm1hdCA9IGFwcENvbmZpZy5nZXQoJ2RhdGVWYWx1ZXMuZGVmYXVsdERhdGVGb3JtYXQnLCBEYXRlQ2VsbENvbXBvbmVudC5EQVRFX0ZPUk1BVCk7XHJcbiAgICAgICAgaWYgKHVzZXJQcmVmZXJlbmNlU2VydmljZSkge1xyXG4gICAgICAgICAgICB1c2VyUHJlZmVyZW5jZVNlcnZpY2VcclxuICAgICAgICAgICAgICAgIC5zZWxlY3QoVXNlclByZWZlcmVuY2VWYWx1ZXMuTG9jYWxlKVxyXG4gICAgICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMub25EZXN0cm95JCkpXHJcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKGxvY2FsZSA9PiB0aGlzLmN1cnJlbnRMb2NhbGUgPSBsb2NhbGUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=