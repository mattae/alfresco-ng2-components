/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Directive, Input, ElementRef, NgZone } from '@angular/core';
export class DropZoneDirective {
    /**
     * @param {?} elementRef
     * @param {?} ngZone
     */
    constructor(elementRef, ngZone) {
        this.ngZone = ngZone;
        this.dropTarget = 'cell';
        this.element = elementRef.nativeElement;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.ngZone.runOutsideAngular((/**
         * @return {?}
         */
        () => {
            this.element.addEventListener('dragover', this.onDragOver.bind(this));
            this.element.addEventListener('drop', this.onDrop.bind(this));
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.element.removeEventListener('dragover', this.onDragOver);
        this.element.removeEventListener('drop', this.onDrop);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onDragOver(event) {
        /** @type {?} */
        const domEvent = new CustomEvent(`${this.dropTarget}-dragover`, {
            detail: {
                target: this.dropTarget,
                event,
                column: this.dropColumn,
                row: this.dropRow
            },
            bubbles: true
        });
        this.element.dispatchEvent(domEvent);
        if (domEvent.defaultPrevented) {
            event.preventDefault();
            event.stopPropagation();
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onDrop(event) {
        /** @type {?} */
        const domEvent = new CustomEvent(`${this.dropTarget}-drop`, {
            detail: {
                target: this.dropTarget,
                event,
                column: this.dropColumn,
                row: this.dropRow
            },
            bubbles: true
        });
        this.element.dispatchEvent(domEvent);
        if (domEvent.defaultPrevented) {
            event.preventDefault();
            event.stopPropagation();
        }
    }
}
DropZoneDirective.decorators = [
    { type: Directive, args: [{
                selector: '[adf-drop-zone]'
            },] }
];
/** @nocollapse */
DropZoneDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: NgZone }
];
DropZoneDirective.propDecorators = {
    dropTarget: [{ type: Input }],
    dropRow: [{ type: Input }],
    dropColumn: [{ type: Input }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    DropZoneDirective.prototype.element;
    /** @type {?} */
    DropZoneDirective.prototype.dropTarget;
    /** @type {?} */
    DropZoneDirective.prototype.dropRow;
    /** @type {?} */
    DropZoneDirective.prototype.dropColumn;
    /**
     * @type {?}
     * @private
     */
    DropZoneDirective.prototype.ngZone;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcC16b25lLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRhdGF0YWJsZS9jb21wb25lbnRzL2RhdGF0YWJsZS9kcm9wLXpvbmUuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQXFCLE1BQU0sZUFBZSxDQUFDO0FBT3hGLE1BQU0sT0FBTyxpQkFBaUI7Ozs7O0lBWTFCLFlBQVksVUFBc0IsRUFBVSxNQUFjO1FBQWQsV0FBTSxHQUFOLE1BQU0sQ0FBUTtRQVIxRCxlQUFVLEdBQXNCLE1BQU0sQ0FBQztRQVNuQyxJQUFJLENBQUMsT0FBTyxHQUFHLFVBQVUsQ0FBQyxhQUFhLENBQUM7SUFDNUMsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFpQjs7O1FBQUMsR0FBRyxFQUFFO1lBQy9CLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDdEUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztRQUNsRSxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzlELElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUMxRCxDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxLQUFZOztjQUNiLFFBQVEsR0FBRyxJQUFJLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLFdBQVcsRUFBRTtZQUM1RCxNQUFNLEVBQUU7Z0JBQ0osTUFBTSxFQUFFLElBQUksQ0FBQyxVQUFVO2dCQUN2QixLQUFLO2dCQUNMLE1BQU0sRUFBRSxJQUFJLENBQUMsVUFBVTtnQkFDdkIsR0FBRyxFQUFFLElBQUksQ0FBQyxPQUFPO2FBQ3BCO1lBQ0QsT0FBTyxFQUFFLElBQUk7U0FDaEIsQ0FBQztRQUVGLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRXJDLElBQUksUUFBUSxDQUFDLGdCQUFnQixFQUFFO1lBQzNCLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN2QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7U0FDM0I7SUFDTCxDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxLQUFZOztjQUNULFFBQVEsR0FBRyxJQUFJLFdBQVcsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLE9BQU8sRUFBRTtZQUN4RCxNQUFNLEVBQUU7Z0JBQ0osTUFBTSxFQUFFLElBQUksQ0FBQyxVQUFVO2dCQUN2QixLQUFLO2dCQUNMLE1BQU0sRUFBRSxJQUFJLENBQUMsVUFBVTtnQkFDdkIsR0FBRyxFQUFFLElBQUksQ0FBQyxPQUFPO2FBQ3BCO1lBQ0QsT0FBTyxFQUFFLElBQUk7U0FDaEIsQ0FBQztRQUVGLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRXJDLElBQUksUUFBUSxDQUFDLGdCQUFnQixFQUFFO1lBQzNCLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN2QixLQUFLLENBQUMsZUFBZSxFQUFFLENBQUM7U0FDM0I7SUFDTCxDQUFDOzs7WUFuRUosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxpQkFBaUI7YUFDOUI7Ozs7WUFOMEIsVUFBVTtZQUFFLE1BQU07Ozt5QkFVeEMsS0FBSztzQkFHTCxLQUFLO3lCQUdMLEtBQUs7Ozs7Ozs7SUFSTixvQ0FBNkI7O0lBRTdCLHVDQUN1Qzs7SUFFdkMsb0NBQ2lCOztJQUVqQix1Q0FDdUI7Ozs7O0lBRWEsbUNBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IERpcmVjdGl2ZSwgSW5wdXQsIEVsZW1lbnRSZWYsIE5nWm9uZSwgT25Jbml0LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRGF0YVJvdyB9IGZyb20gJy4uLy4uL2RhdGEvZGF0YS1yb3cubW9kZWwnO1xyXG5pbXBvcnQgeyBEYXRhQ29sdW1uIH0gZnJvbSAnLi4vLi4vZGF0YS9kYXRhLWNvbHVtbi5tb2RlbCc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW2FkZi1kcm9wLXpvbmVdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRHJvcFpvbmVEaXJlY3RpdmUgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgICBwcml2YXRlIGVsZW1lbnQ6IEhUTUxFbGVtZW50O1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBkcm9wVGFyZ2V0OiAnaGVhZGVyJyB8ICdjZWxsJyA9ICdjZWxsJztcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgZHJvcFJvdzogRGF0YVJvdztcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgZHJvcENvbHVtbjogRGF0YUNvbHVtbjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihlbGVtZW50UmVmOiBFbGVtZW50UmVmLCBwcml2YXRlIG5nWm9uZTogTmdab25lKSB7XHJcbiAgICAgICAgdGhpcy5lbGVtZW50ID0gZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50O1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMubmdab25lLnJ1bk91dHNpZGVBbmd1bGFyKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5lbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2RyYWdvdmVyJywgdGhpcy5vbkRyYWdPdmVyLmJpbmQodGhpcykpO1xyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignZHJvcCcsIHRoaXMub25Ecm9wLmJpbmQodGhpcykpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIHRoaXMuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdkcmFnb3ZlcicsIHRoaXMub25EcmFnT3Zlcik7XHJcbiAgICAgICAgdGhpcy5lbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2Ryb3AnLCB0aGlzLm9uRHJvcCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25EcmFnT3ZlcihldmVudDogRXZlbnQpIHtcclxuICAgICAgICBjb25zdCBkb21FdmVudCA9IG5ldyBDdXN0b21FdmVudChgJHt0aGlzLmRyb3BUYXJnZXR9LWRyYWdvdmVyYCwge1xyXG4gICAgICAgICAgICBkZXRhaWw6IHtcclxuICAgICAgICAgICAgICAgIHRhcmdldDogdGhpcy5kcm9wVGFyZ2V0LFxyXG4gICAgICAgICAgICAgICAgZXZlbnQsXHJcbiAgICAgICAgICAgICAgICBjb2x1bW46IHRoaXMuZHJvcENvbHVtbixcclxuICAgICAgICAgICAgICAgIHJvdzogdGhpcy5kcm9wUm93XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGJ1YmJsZXM6IHRydWVcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5lbGVtZW50LmRpc3BhdGNoRXZlbnQoZG9tRXZlbnQpO1xyXG5cclxuICAgICAgICBpZiAoZG9tRXZlbnQuZGVmYXVsdFByZXZlbnRlZCkge1xyXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25Ecm9wKGV2ZW50OiBFdmVudCkge1xyXG4gICAgICAgIGNvbnN0IGRvbUV2ZW50ID0gbmV3IEN1c3RvbUV2ZW50KGAke3RoaXMuZHJvcFRhcmdldH0tZHJvcGAsIHtcclxuICAgICAgICAgICAgZGV0YWlsOiB7XHJcbiAgICAgICAgICAgICAgICB0YXJnZXQ6IHRoaXMuZHJvcFRhcmdldCxcclxuICAgICAgICAgICAgICAgIGV2ZW50LFxyXG4gICAgICAgICAgICAgICAgY29sdW1uOiB0aGlzLmRyb3BDb2x1bW4sXHJcbiAgICAgICAgICAgICAgICByb3c6IHRoaXMuZHJvcFJvd1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBidWJibGVzOiB0cnVlXHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHRoaXMuZWxlbWVudC5kaXNwYXRjaEV2ZW50KGRvbUV2ZW50KTtcclxuXHJcbiAgICAgICAgaWYgKGRvbUV2ZW50LmRlZmF1bHRQcmV2ZW50ZWQpIHtcclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==