/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, Component, ViewEncapsulation, Input } from '@angular/core';
import { DataTableCellComponent } from './datatable-cell.component';
export class JsonCellComponent extends DataTableCellComponent {
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.column && this.column.key && this.row && this.data) {
            this.value$.next(this.data.getValue(this.row, this.column));
        }
    }
}
JsonCellComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-json-cell',
                changeDetection: ChangeDetectionStrategy.OnPush,
                template: `
        <ng-container>
            <span *ngIf="copyContent; else defaultJsonTemplate" class="adf-datatable-cell-value">
                <pre
                    class="adf-datatable-json-cell"
                    [adf-clipboard]="'CLIPBOARD.CLICK_TO_COPY'"
                    [clipboard-notification]="'CLIPBOARD.SUCCESS_COPY'">{{ value$ | async | json }}</pre>
            </span>
        </ng-container>
        <ng-template #defaultJsonTemplate>
            <span class="adf-datatable-cell-value">
                <pre class="adf-datatable-json-cell">{{ value$ | async | json }}</pre>
            </span>
        </ng-template>
    `,
                encapsulation: ViewEncapsulation.None,
                host: { class: 'adf-datatable-content-cell' },
                styles: [".adf-datatable-json-cell{white-space:pre-wrap;word-wrap:break-word}.adf-datatable-cell-value{position:relative}"]
            }] }
];
JsonCellComponent.propDecorators = {
    copyContent: [{ type: Input }]
};
if (false) {
    /**
     * Enables/disables a Clipboard directive to allow copying of the cell's content.
     * @type {?}
     */
    JsonCellComponent.prototype.copyContent;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoianNvbi1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRhdGF0YWJsZS9jb21wb25lbnRzL2RhdGF0YWJsZS9qc29uLWNlbGwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxTQUFTLEVBQVUsaUJBQWlCLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3JHLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBd0JwRSxNQUFNLE9BQU8saUJBQWtCLFNBQVEsc0JBQXNCOzs7O0lBTXpELFFBQVE7UUFDSixJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ3pELElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7U0FDL0Q7SUFDTCxDQUFDOzs7WUFoQ0osU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxlQUFlO2dCQUN6QixlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtnQkFDL0MsUUFBUSxFQUFFOzs7Ozs7Ozs7Ozs7OztLQWNUO2dCQUVELGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO2dCQUNyQyxJQUFJLEVBQUUsRUFBRSxLQUFLLEVBQUUsNEJBQTRCLEVBQUU7O2FBQ2hEOzs7MEJBSUssS0FBSzs7Ozs7OztJQUFOLHdDQUNxQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSwgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdFbmNhcHN1bGF0aW9uLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEYXRhVGFibGVDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi9kYXRhdGFibGUtY2VsbC5jb21wb25lbnQnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1qc29uLWNlbGwnLFxyXG4gICAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxuZy1jb250YWluZXI+XHJcbiAgICAgICAgICAgIDxzcGFuICpuZ0lmPVwiY29weUNvbnRlbnQ7IGVsc2UgZGVmYXVsdEpzb25UZW1wbGF0ZVwiIGNsYXNzPVwiYWRmLWRhdGF0YWJsZS1jZWxsLXZhbHVlXCI+XHJcbiAgICAgICAgICAgICAgICA8cHJlXHJcbiAgICAgICAgICAgICAgICAgICAgY2xhc3M9XCJhZGYtZGF0YXRhYmxlLWpzb24tY2VsbFwiXHJcbiAgICAgICAgICAgICAgICAgICAgW2FkZi1jbGlwYm9hcmRdPVwiJ0NMSVBCT0FSRC5DTElDS19UT19DT1BZJ1wiXHJcbiAgICAgICAgICAgICAgICAgICAgW2NsaXBib2FyZC1ub3RpZmljYXRpb25dPVwiJ0NMSVBCT0FSRC5TVUNDRVNTX0NPUFknXCI+e3sgdmFsdWUkIHwgYXN5bmMgfCBqc29uIH19PC9wcmU+XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgICAgICA8bmctdGVtcGxhdGUgI2RlZmF1bHRKc29uVGVtcGxhdGU+XHJcbiAgICAgICAgICAgIDxzcGFuIGNsYXNzPVwiYWRmLWRhdGF0YWJsZS1jZWxsLXZhbHVlXCI+XHJcbiAgICAgICAgICAgICAgICA8cHJlIGNsYXNzPVwiYWRmLWRhdGF0YWJsZS1qc29uLWNlbGxcIj57eyB2YWx1ZSQgfCBhc3luYyB8IGpzb24gfX08L3ByZT5cclxuICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgIDwvbmctdGVtcGxhdGU+XHJcbiAgICBgLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vanNvbi1jZWxsLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gICAgaG9zdDogeyBjbGFzczogJ2FkZi1kYXRhdGFibGUtY29udGVudC1jZWxsJyB9XHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBKc29uQ2VsbENvbXBvbmVudCBleHRlbmRzIERhdGFUYWJsZUNlbGxDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgICAvKiogRW5hYmxlcy9kaXNhYmxlcyBhIENsaXBib2FyZCBkaXJlY3RpdmUgdG8gYWxsb3cgY29weWluZyBvZiB0aGUgY2VsbCdzIGNvbnRlbnQuICovXHJcbiAgICAgQElucHV0KClcclxuICAgICBjb3B5Q29udGVudDogYm9vbGVhbjtcclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBpZiAodGhpcy5jb2x1bW4gJiYgdGhpcy5jb2x1bW4ua2V5ICYmIHRoaXMucm93ICYmIHRoaXMuZGF0YSkge1xyXG4gICAgICAgICAgICB0aGlzLnZhbHVlJC5uZXh0KHRoaXMuZGF0YS5nZXRWYWx1ZSh0aGlzLnJvdywgdGhpcy5jb2x1bW4pKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19