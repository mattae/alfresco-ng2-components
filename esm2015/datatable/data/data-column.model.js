/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @record
 */
export function DataColumn() { }
if (false) {
    /** @type {?} */
    DataColumn.prototype.key;
    /** @type {?} */
    DataColumn.prototype.type;
    /** @type {?|undefined} */
    DataColumn.prototype.format;
    /** @type {?|undefined} */
    DataColumn.prototype.sortable;
    /** @type {?|undefined} */
    DataColumn.prototype.title;
    /** @type {?|undefined} */
    DataColumn.prototype.srTitle;
    /** @type {?|undefined} */
    DataColumn.prototype.cssClass;
    /** @type {?|undefined} */
    DataColumn.prototype.template;
    /** @type {?|undefined} */
    DataColumn.prototype.formatTooltip;
    /** @type {?|undefined} */
    DataColumn.prototype.copyContent;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS1jb2x1bW4ubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJkYXRhdGFibGUvZGF0YS9kYXRhLWNvbHVtbi5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxnQ0FXQzs7O0lBVkcseUJBQVk7O0lBQ1osMEJBQWE7O0lBQ2IsNEJBQWdCOztJQUNoQiw4QkFBbUI7O0lBQ25CLDJCQUFlOztJQUNmLDZCQUFpQjs7SUFDakIsOEJBQWtCOztJQUNsQiw4QkFBNEI7O0lBQzVCLG1DQUF5Qjs7SUFDekIsaUNBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IFRlbXBsYXRlUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIERhdGFDb2x1bW4ge1xyXG4gICAga2V5OiBzdHJpbmc7XHJcbiAgICB0eXBlOiBzdHJpbmc7IC8vIHRleHR8aW1hZ2V8ZGF0ZVxyXG4gICAgZm9ybWF0Pzogc3RyaW5nO1xyXG4gICAgc29ydGFibGU/OiBib29sZWFuO1xyXG4gICAgdGl0bGU/OiBzdHJpbmc7XHJcbiAgICBzclRpdGxlPzogc3RyaW5nO1xyXG4gICAgY3NzQ2xhc3M/OiBzdHJpbmc7XHJcbiAgICB0ZW1wbGF0ZT86IFRlbXBsYXRlUmVmPGFueT47XHJcbiAgICBmb3JtYXRUb29sdGlwPzogRnVuY3Rpb247XHJcbiAgICBjb3B5Q29udGVudD86IGJvb2xlYW47XHJcbn1cclxuIl19