/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ContentChild, Input } from '@angular/core';
import { DataColumnListComponent } from '../../data-column/data-column-list.component';
import { ObjectDataColumn } from './object-datacolumn.model';
/**
 * @abstract
 */
export class DataTableSchema {
    /**
     * @param {?} appConfigService
     * @param {?} presetKey
     * @param {?} presetsModel
     */
    constructor(appConfigService, presetKey, presetsModel) {
        this.appConfigService = appConfigService;
        this.presetKey = presetKey;
        this.presetsModel = presetsModel;
        this.layoutPresets = {};
    }
    /**
     * @return {?}
     */
    createDatatableSchema() {
        this.loadLayoutPresets();
        if (!this.columns || this.columns.length === 0) {
            this.columns = this.mergeJsonAndHtmlSchema();
        }
    }
    /**
     * @return {?}
     */
    loadLayoutPresets() {
        /** @type {?} */
        const externalSettings = this.appConfigService.get(this.presetKey, null);
        if (externalSettings) {
            this.layoutPresets = Object.assign({}, this.presetsModel, externalSettings);
        }
        else {
            this.layoutPresets = this.presetsModel;
        }
    }
    /**
     * @return {?}
     */
    mergeJsonAndHtmlSchema() {
        /** @type {?} */
        let customSchemaColumns = this.getSchemaFromConfig(this.presetColumn).concat(this.getSchemaFromHtml(this.columnList));
        if (customSchemaColumns.length === 0) {
            customSchemaColumns = this.getDefaultLayoutPreset();
        }
        return customSchemaColumns;
    }
    /**
     * @param {?} columnList
     * @return {?}
     */
    getSchemaFromHtml(columnList) {
        /** @type {?} */
        let schema = [];
        if (columnList && columnList.columns && columnList.columns.length > 0) {
            schema = columnList.columns.map((/**
             * @param {?} c
             * @return {?}
             */
            (c) => (/** @type {?} */ (c))));
        }
        return schema;
    }
    /**
     * @param {?} presetColumn
     * @return {?}
     */
    getSchemaFromConfig(presetColumn) {
        return presetColumn ? (this.layoutPresets[presetColumn]).map((/**
         * @param {?} col
         * @return {?}
         */
        (col) => new ObjectDataColumn(col))) : [];
    }
    /**
     * @private
     * @return {?}
     */
    getDefaultLayoutPreset() {
        return (this.layoutPresets['default']).map((/**
         * @param {?} col
         * @return {?}
         */
        (col) => new ObjectDataColumn(col)));
    }
}
DataTableSchema.propDecorators = {
    columnList: [{ type: ContentChild, args: [DataColumnListComponent, { static: true },] }],
    presetColumn: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    DataTableSchema.prototype.columnList;
    /**
     * Custom preset column schema in JSON format.
     * @type {?}
     */
    DataTableSchema.prototype.presetColumn;
    /** @type {?} */
    DataTableSchema.prototype.columns;
    /**
     * @type {?}
     * @private
     */
    DataTableSchema.prototype.layoutPresets;
    /**
     * @type {?}
     * @private
     */
    DataTableSchema.prototype.appConfigService;
    /**
     * @type {?}
     * @protected
     */
    DataTableSchema.prototype.presetKey;
    /**
     * @type {?}
     * @protected
     */
    DataTableSchema.prototype.presetsModel;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS10YWJsZS5zY2hlbWEuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJkYXRhdGFibGUvZGF0YS9kYXRhLXRhYmxlLnNjaGVtYS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQ0EsT0FBTyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFcEQsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFFdkYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7Ozs7QUFFN0QsTUFBTSxPQUFnQixlQUFlOzs7Ozs7SUFZakMsWUFBb0IsZ0JBQWtDLEVBQ2hDLFNBQWlCLEVBQ2pCLFlBQWlCO1FBRm5CLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDaEMsY0FBUyxHQUFULFNBQVMsQ0FBUTtRQUNqQixpQkFBWSxHQUFaLFlBQVksQ0FBSztRQUovQixrQkFBYSxHQUFHLEVBQUUsQ0FBQztJQUlnQixDQUFDOzs7O0lBRXJDLHFCQUFxQjtRQUN4QixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDNUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztTQUNoRDtJQUNMLENBQUM7Ozs7SUFFTSxpQkFBaUI7O2NBQ2QsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQztRQUN4RSxJQUFJLGdCQUFnQixFQUFFO1lBQ2xCLElBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1NBQy9FO2FBQU07WUFDSCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7U0FDMUM7SUFDTCxDQUFDOzs7O0lBRU0sc0JBQXNCOztZQUNyQixtQkFBbUIsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3JILElBQUksbUJBQW1CLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUNsQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztTQUN2RDtRQUNELE9BQU8sbUJBQW1CLENBQUM7SUFDL0IsQ0FBQzs7Ozs7SUFFTSxpQkFBaUIsQ0FBQyxVQUFtQzs7WUFDcEQsTUFBTSxHQUFHLEVBQUU7UUFDZixJQUFJLFVBQVUsSUFBSSxVQUFVLENBQUMsT0FBTyxJQUFJLFVBQVUsQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNuRSxNQUFNLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHOzs7O1lBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLG1CQUFhLENBQUMsRUFBQSxFQUFDLENBQUM7U0FDMUQ7UUFDRCxPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDOzs7OztJQUVLLG1CQUFtQixDQUFDLFlBQW9CO1FBQzFDLE9BQU8sWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxHQUFHOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksZ0JBQWdCLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQzFHLENBQUM7Ozs7O0lBRU8sc0JBQXNCO1FBQzFCLE9BQU8sQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsR0FBRzs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQUM7SUFDbkYsQ0FBQzs7O3lCQXBEQSxZQUFZLFNBQUMsdUJBQXVCLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDOzJCQUdwRCxLQUFLOzs7O0lBSE4scUNBQTJGOzs7OztJQUczRix1Q0FDcUI7O0lBRXJCLGtDQUFhOzs7OztJQUViLHdDQUEyQjs7Ozs7SUFFZiwyQ0FBMEM7Ozs7O0lBQzFDLG9DQUEyQjs7Ozs7SUFDM0IsdUNBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXG5cbi8qIVxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXG4gKlxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuICpcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbiAqXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxuICovXG5cbmltcG9ydCB7IENvbnRlbnRDaGlsZCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XG5pbXBvcnQgeyBEYXRhQ29sdW1uTGlzdENvbXBvbmVudCB9IGZyb20gJy4uLy4uL2RhdGEtY29sdW1uL2RhdGEtY29sdW1uLWxpc3QuY29tcG9uZW50JztcbmltcG9ydCB7IERhdGFDb2x1bW4gfSBmcm9tICcuL2RhdGEtY29sdW1uLm1vZGVsJztcbmltcG9ydCB7IE9iamVjdERhdGFDb2x1bW4gfSBmcm9tICcuL29iamVjdC1kYXRhY29sdW1uLm1vZGVsJztcblxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIERhdGFUYWJsZVNjaGVtYSB7XG5cbiAgICBAQ29udGVudENoaWxkKERhdGFDb2x1bW5MaXN0Q29tcG9uZW50LCB7c3RhdGljOiB0cnVlfSkgY29sdW1uTGlzdDogRGF0YUNvbHVtbkxpc3RDb21wb25lbnQ7XG5cbiAgICAvKiogQ3VzdG9tIHByZXNldCBjb2x1bW4gc2NoZW1hIGluIEpTT04gZm9ybWF0LiAqL1xuICAgIEBJbnB1dCgpXG4gICAgcHJlc2V0Q29sdW1uOiBzdHJpbmc7XG5cbiAgICBjb2x1bW5zOiBhbnk7XG5cbiAgICBwcml2YXRlIGxheW91dFByZXNldHMgPSB7fTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBwQ29uZmlnU2VydmljZTogQXBwQ29uZmlnU2VydmljZSxcbiAgICAgICAgICAgICAgICBwcm90ZWN0ZWQgcHJlc2V0S2V5OiBzdHJpbmcsXG4gICAgICAgICAgICAgICAgcHJvdGVjdGVkIHByZXNldHNNb2RlbDogYW55KSB7IH1cblxuICAgIHB1YmxpYyBjcmVhdGVEYXRhdGFibGVTY2hlbWEoKTogdm9pZCB7XG4gICAgICAgIHRoaXMubG9hZExheW91dFByZXNldHMoKTtcbiAgICAgICAgaWYgKCF0aGlzLmNvbHVtbnMgfHwgdGhpcy5jb2x1bW5zLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgdGhpcy5jb2x1bW5zID0gdGhpcy5tZXJnZUpzb25BbmRIdG1sU2NoZW1hKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwdWJsaWMgbG9hZExheW91dFByZXNldHMoKTogdm9pZCB7XG4gICAgICAgIGNvbnN0IGV4dGVybmFsU2V0dGluZ3MgPSB0aGlzLmFwcENvbmZpZ1NlcnZpY2UuZ2V0KHRoaXMucHJlc2V0S2V5LCBudWxsKTtcbiAgICAgICAgaWYgKGV4dGVybmFsU2V0dGluZ3MpIHtcbiAgICAgICAgICAgIHRoaXMubGF5b3V0UHJlc2V0cyA9IE9iamVjdC5hc3NpZ24oe30sIHRoaXMucHJlc2V0c01vZGVsLCBleHRlcm5hbFNldHRpbmdzKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMubGF5b3V0UHJlc2V0cyA9IHRoaXMucHJlc2V0c01vZGVsO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIG1lcmdlSnNvbkFuZEh0bWxTY2hlbWEoKTogYW55IHtcbiAgICAgICAgbGV0IGN1c3RvbVNjaGVtYUNvbHVtbnMgPSB0aGlzLmdldFNjaGVtYUZyb21Db25maWcodGhpcy5wcmVzZXRDb2x1bW4pLmNvbmNhdCh0aGlzLmdldFNjaGVtYUZyb21IdG1sKHRoaXMuY29sdW1uTGlzdCkpO1xuICAgICAgICBpZiAoY3VzdG9tU2NoZW1hQ29sdW1ucy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgIGN1c3RvbVNjaGVtYUNvbHVtbnMgPSB0aGlzLmdldERlZmF1bHRMYXlvdXRQcmVzZXQoKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gY3VzdG9tU2NoZW1hQ29sdW1ucztcbiAgICB9XG5cbiAgICBwdWJsaWMgZ2V0U2NoZW1hRnJvbUh0bWwoY29sdW1uTGlzdDogRGF0YUNvbHVtbkxpc3RDb21wb25lbnQpOiBhbnkge1xuICAgICAgICBsZXQgc2NoZW1hID0gW107XG4gICAgICAgIGlmIChjb2x1bW5MaXN0ICYmIGNvbHVtbkxpc3QuY29sdW1ucyAmJiBjb2x1bW5MaXN0LmNvbHVtbnMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgc2NoZW1hID0gY29sdW1uTGlzdC5jb2x1bW5zLm1hcCgoYykgPT4gPERhdGFDb2x1bW4+IGMpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBzY2hlbWE7XG4gICAgfVxuXG4gICBwdWJsaWMgZ2V0U2NoZW1hRnJvbUNvbmZpZyhwcmVzZXRDb2x1bW46IHN0cmluZyk6IERhdGFDb2x1bW5bXSB7XG4gICAgICAgIHJldHVybiBwcmVzZXRDb2x1bW4gPyAodGhpcy5sYXlvdXRQcmVzZXRzW3ByZXNldENvbHVtbl0pLm1hcCgoY29sKSA9PiBuZXcgT2JqZWN0RGF0YUNvbHVtbihjb2wpKSA6IFtdO1xuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0RGVmYXVsdExheW91dFByZXNldCgpOiBEYXRhQ29sdW1uW10ge1xuICAgICAgICByZXR1cm4gKHRoaXMubGF5b3V0UHJlc2V0c1snZGVmYXVsdCddKS5tYXAoKGNvbCkgPT4gbmV3IE9iamVjdERhdGFDb2x1bW4oY29sKSk7XG4gICAgfVxufVxuIl19