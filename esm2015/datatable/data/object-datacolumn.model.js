/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Simple implementation of the DataColumn interface.
export class ObjectDataColumn {
    /**
     * @param {?} input
     */
    constructor(input) {
        this.key = input.key;
        this.type = input.type || 'text';
        this.format = input.format;
        this.sortable = input.sortable;
        this.title = input.title;
        this.srTitle = input.srTitle;
        this.cssClass = input.cssClass;
        this.template = input.template;
        this.copyContent = input.copyContent;
    }
}
if (false) {
    /** @type {?} */
    ObjectDataColumn.prototype.key;
    /** @type {?} */
    ObjectDataColumn.prototype.type;
    /** @type {?} */
    ObjectDataColumn.prototype.format;
    /** @type {?} */
    ObjectDataColumn.prototype.sortable;
    /** @type {?} */
    ObjectDataColumn.prototype.title;
    /** @type {?} */
    ObjectDataColumn.prototype.srTitle;
    /** @type {?} */
    ObjectDataColumn.prototype.cssClass;
    /** @type {?} */
    ObjectDataColumn.prototype.template;
    /** @type {?} */
    ObjectDataColumn.prototype.copyContent;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2JqZWN0LWRhdGFjb2x1bW4ubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJkYXRhdGFibGUvZGF0YS9vYmplY3QtZGF0YWNvbHVtbi5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQkEsTUFBTSxPQUFPLGdCQUFnQjs7OztJQVl6QixZQUFZLEtBQVU7UUFDbEIsSUFBSSxDQUFDLEdBQUcsR0FBRyxLQUFLLENBQUMsR0FBRyxDQUFDO1FBQ3JCLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDLElBQUksSUFBSSxNQUFNLENBQUM7UUFDakMsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDO1FBQzNCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQztRQUMvQixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7UUFDekIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO1FBQzdCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDLFFBQVEsQ0FBQztRQUMvQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUM7UUFDL0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUMsV0FBVyxDQUFDO0lBQ3pDLENBQUM7Q0FDSjs7O0lBckJHLCtCQUFZOztJQUNaLGdDQUFhOztJQUNiLGtDQUFlOztJQUNmLG9DQUFrQjs7SUFDbEIsaUNBQWM7O0lBQ2QsbUNBQWdCOztJQUNoQixvQ0FBaUI7O0lBQ2pCLG9DQUE0Qjs7SUFDNUIsdUNBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IFRlbXBsYXRlUmVmIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERhdGFDb2x1bW4gfSBmcm9tICcuL2RhdGEtY29sdW1uLm1vZGVsJztcclxuXHJcbi8vIFNpbXBsZSBpbXBsZW1lbnRhdGlvbiBvZiB0aGUgRGF0YUNvbHVtbiBpbnRlcmZhY2UuXHJcbmV4cG9ydCBjbGFzcyBPYmplY3REYXRhQ29sdW1uIGltcGxlbWVudHMgRGF0YUNvbHVtbiB7XHJcblxyXG4gICAga2V5OiBzdHJpbmc7XHJcbiAgICB0eXBlOiBzdHJpbmc7IC8vIHRleHR8aW1hZ2VcclxuICAgIGZvcm1hdDogc3RyaW5nO1xyXG4gICAgc29ydGFibGU6IGJvb2xlYW47XHJcbiAgICB0aXRsZTogc3RyaW5nO1xyXG4gICAgc3JUaXRsZTogc3RyaW5nO1xyXG4gICAgY3NzQ2xhc3M6IHN0cmluZztcclxuICAgIHRlbXBsYXRlPzogVGVtcGxhdGVSZWY8YW55PjtcclxuICAgIGNvcHlDb250ZW50PzogYm9vbGVhbjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihpbnB1dDogYW55KSB7XHJcbiAgICAgICAgdGhpcy5rZXkgPSBpbnB1dC5rZXk7XHJcbiAgICAgICAgdGhpcy50eXBlID0gaW5wdXQudHlwZSB8fCAndGV4dCc7XHJcbiAgICAgICAgdGhpcy5mb3JtYXQgPSBpbnB1dC5mb3JtYXQ7XHJcbiAgICAgICAgdGhpcy5zb3J0YWJsZSA9IGlucHV0LnNvcnRhYmxlO1xyXG4gICAgICAgIHRoaXMudGl0bGUgPSBpbnB1dC50aXRsZTtcclxuICAgICAgICB0aGlzLnNyVGl0bGUgPSBpbnB1dC5zclRpdGxlO1xyXG4gICAgICAgIHRoaXMuY3NzQ2xhc3MgPSBpbnB1dC5jc3NDbGFzcztcclxuICAgICAgICB0aGlzLnRlbXBsYXRlID0gaW5wdXQudGVtcGxhdGU7XHJcbiAgICAgICAgdGhpcy5jb3B5Q29udGVudCA9IGlucHV0LmNvcHlDb250ZW50O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==