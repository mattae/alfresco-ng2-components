/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ObjectDataRow } from './object-datarow.model';
import { ObjectDataColumn } from './object-datacolumn.model';
import { DataSorting } from './data-sorting.model';
import { Subject } from 'rxjs';
// Simple implementation of the DataTableAdapter interface.
export class ObjectDataTableAdapter {
    /**
     * @param {?} data
     * @return {?}
     */
    static generateSchema(data) {
        /** @type {?} */
        const schema = [];
        if (data && data.length) {
            /** @type {?} */
            const rowToExaminate = data[0];
            if (typeof rowToExaminate === 'object') {
                for (const key in rowToExaminate) {
                    if (rowToExaminate.hasOwnProperty(key)) {
                        schema.push({
                            type: 'text',
                            key: key,
                            title: key,
                            sortable: false
                        });
                    }
                }
            }
        }
        return schema;
    }
    /**
     * @param {?=} data
     * @param {?=} schema
     */
    constructor(data = [], schema = []) {
        this._rows = [];
        this._columns = [];
        if (data && data.length > 0) {
            this._rows = data.map((/**
             * @param {?} item
             * @return {?}
             */
            (item) => {
                return new ObjectDataRow(item);
            }));
        }
        if (schema && schema.length > 0) {
            this._columns = schema.map((/**
             * @param {?} item
             * @return {?}
             */
            (item) => {
                return new ObjectDataColumn(item);
            }));
            // Sort by first sortable or just first column
            /** @type {?} */
            const sortable = this._columns.filter((/**
             * @param {?} column
             * @return {?}
             */
            (column) => column.sortable));
            if (sortable.length > 0) {
                this.sort(sortable[0].key, 'asc');
            }
        }
        this.rowsChanged = new Subject();
    }
    /**
     * @return {?}
     */
    getRows() {
        return this._rows;
    }
    /**
     * @param {?} rows
     * @return {?}
     */
    setRows(rows) {
        this._rows = rows || [];
        this.sort();
        this.rowsChanged.next(this._rows);
    }
    /**
     * @return {?}
     */
    getColumns() {
        return this._columns;
    }
    /**
     * @param {?} columns
     * @return {?}
     */
    setColumns(columns) {
        this._columns = columns || [];
    }
    /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    getValue(row, col) {
        if (!row) {
            throw new Error('Row not found');
        }
        if (!col) {
            throw new Error('Column not found');
        }
        /** @type {?} */
        const value = row.getValue(col.key);
        if (col.type === 'icon') {
            /** @type {?} */
            const icon = row.getValue(col.key);
            return icon;
        }
        return value;
    }
    /**
     * @return {?}
     */
    getSorting() {
        return this._sorting;
    }
    /**
     * @param {?} sorting
     * @return {?}
     */
    setSorting(sorting) {
        this._sorting = sorting;
        if (sorting && sorting.key) {
            this._rows.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            (a, b) => {
                /** @type {?} */
                let left = a.getValue(sorting.key);
                if (left) {
                    left = (left instanceof Date) ? left.valueOf().toString() : left.toString();
                }
                else {
                    left = '';
                }
                /** @type {?} */
                let right = b.getValue(sorting.key);
                if (right) {
                    right = (right instanceof Date) ? right.valueOf().toString() : right.toString();
                }
                else {
                    right = '';
                }
                return sorting.direction === 'asc'
                    ? left.localeCompare(right)
                    : right.localeCompare(left);
            }));
        }
    }
    /**
     * @param {?=} key
     * @param {?=} direction
     * @return {?}
     */
    sort(key, direction) {
        /** @type {?} */
        const sorting = this._sorting || new DataSorting();
        if (key) {
            sorting.key = key;
            sorting.direction = direction || 'asc';
        }
        this.setSorting(sorting);
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    ObjectDataTableAdapter.prototype._sorting;
    /**
     * @type {?}
     * @private
     */
    ObjectDataTableAdapter.prototype._rows;
    /**
     * @type {?}
     * @private
     */
    ObjectDataTableAdapter.prototype._columns;
    /** @type {?} */
    ObjectDataTableAdapter.prototype.selectedRow;
    /** @type {?} */
    ObjectDataTableAdapter.prototype.rowsChanged;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2JqZWN0LWRhdGF0YWJsZS1hZGFwdGVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZGF0YXRhYmxlL2RhdGEvb2JqZWN0LWRhdGF0YWJsZS1hZGFwdGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN2RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFbkQsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQzs7QUFHL0IsTUFBTSxPQUFPLHNCQUFzQjs7Ozs7SUFTL0IsTUFBTSxDQUFDLGNBQWMsQ0FBQyxJQUFXOztjQUN2QixNQUFNLEdBQUcsRUFBRTtRQUVqQixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFOztrQkFDZixjQUFjLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUU5QixJQUFJLE9BQU8sY0FBYyxLQUFLLFFBQVEsRUFBRTtnQkFDcEMsS0FBSyxNQUFNLEdBQUcsSUFBSSxjQUFjLEVBQUU7b0JBQzlCLElBQUksY0FBYyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDcEMsTUFBTSxDQUFDLElBQUksQ0FBQzs0QkFDUixJQUFJLEVBQUUsTUFBTTs0QkFDWixHQUFHLEVBQUUsR0FBRzs0QkFDUixLQUFLLEVBQUUsR0FBRzs0QkFDVixRQUFRLEVBQUUsS0FBSzt5QkFDbEIsQ0FBQyxDQUFDO3FCQUNOO2lCQUNKO2FBQ0o7U0FFSjtRQUNELE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7Ozs7O0lBRUQsWUFBWSxPQUFjLEVBQUUsRUFBRSxTQUF1QixFQUFFO1FBQ25ELElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBRW5CLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3pCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUc7Ozs7WUFBQyxDQUFDLElBQUksRUFBRSxFQUFFO2dCQUMzQixPQUFPLElBQUksYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ25DLENBQUMsRUFBQyxDQUFDO1NBQ047UUFFRCxJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQyxHQUFHOzs7O1lBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtnQkFDaEMsT0FBTyxJQUFJLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3RDLENBQUMsRUFBQyxDQUFDOzs7a0JBR0csUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTTs7OztZQUFDLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFDO1lBQ2xFLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3JCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQzthQUNyQztTQUNKO1FBRUQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLE9BQU8sRUFBa0IsQ0FBQztJQUNyRCxDQUFDOzs7O0lBRUQsT0FBTztRQUNILE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDOzs7OztJQUVELE9BQU8sQ0FBQyxJQUFvQjtRQUN4QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ1osSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3RDLENBQUM7Ozs7SUFFRCxVQUFVO1FBQ04sT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3pCLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLE9BQTBCO1FBQ2pDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxJQUFJLEVBQUUsQ0FBQztJQUNsQyxDQUFDOzs7Ozs7SUFFRCxRQUFRLENBQUMsR0FBWSxFQUFFLEdBQWU7UUFDbEMsSUFBSSxDQUFDLEdBQUcsRUFBRTtZQUNOLE1BQU0sSUFBSSxLQUFLLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDcEM7UUFDRCxJQUFJLENBQUMsR0FBRyxFQUFFO1lBQ04sTUFBTSxJQUFJLEtBQUssQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1NBQ3ZDOztjQUVLLEtBQUssR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUM7UUFFbkMsSUFBSSxHQUFHLENBQUMsSUFBSSxLQUFLLE1BQU0sRUFBRTs7a0JBQ2YsSUFBSSxHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQztZQUNsQyxPQUFPLElBQUksQ0FBQztTQUNmO1FBRUQsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7OztJQUVELFVBQVU7UUFDTixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsT0FBb0I7UUFDM0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7UUFFeEIsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLEdBQUcsRUFBRTtZQUN4QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUk7Ozs7O1lBQUMsQ0FBQyxDQUFVLEVBQUUsQ0FBVSxFQUFFLEVBQUU7O29CQUNuQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDO2dCQUNsQyxJQUFJLElBQUksRUFBRTtvQkFDTixJQUFJLEdBQUcsQ0FBQyxJQUFJLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUMvRTtxQkFBTTtvQkFDSCxJQUFJLEdBQUcsRUFBRSxDQUFDO2lCQUNiOztvQkFFRyxLQUFLLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDO2dCQUNuQyxJQUFJLEtBQUssRUFBRTtvQkFDUCxLQUFLLEdBQUcsQ0FBQyxLQUFLLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUNuRjtxQkFBTTtvQkFDSCxLQUFLLEdBQUcsRUFBRSxDQUFDO2lCQUNkO2dCQUVELE9BQU8sT0FBTyxDQUFDLFNBQVMsS0FBSyxLQUFLO29CQUM5QixDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUM7b0JBQzNCLENBQUMsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BDLENBQUMsRUFBQyxDQUFDO1NBQ047SUFDTCxDQUFDOzs7Ozs7SUFFRCxJQUFJLENBQUMsR0FBWSxFQUFFLFNBQWtCOztjQUMzQixPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLFdBQVcsRUFBRTtRQUNsRCxJQUFJLEdBQUcsRUFBRTtZQUNMLE9BQU8sQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1lBQ2xCLE9BQU8sQ0FBQyxTQUFTLEdBQUcsU0FBUyxJQUFJLEtBQUssQ0FBQztTQUMxQztRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDN0IsQ0FBQztDQUNKOzs7Ozs7SUFqSUcsMENBQThCOzs7OztJQUM5Qix1Q0FBeUI7Ozs7O0lBQ3pCLDBDQUErQjs7SUFFL0IsNkNBQXFCOztJQUNyQiw2Q0FBcUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgRGF0YUNvbHVtbiB9IGZyb20gJy4vZGF0YS1jb2x1bW4ubW9kZWwnO1xyXG5pbXBvcnQgeyBEYXRhUm93IH0gZnJvbSAnLi9kYXRhLXJvdy5tb2RlbCc7XHJcbmltcG9ydCB7IE9iamVjdERhdGFSb3cgfSBmcm9tICcuL29iamVjdC1kYXRhcm93Lm1vZGVsJztcclxuaW1wb3J0IHsgT2JqZWN0RGF0YUNvbHVtbiB9IGZyb20gJy4vb2JqZWN0LWRhdGFjb2x1bW4ubW9kZWwnO1xyXG5pbXBvcnQgeyBEYXRhU29ydGluZyB9IGZyb20gJy4vZGF0YS1zb3J0aW5nLm1vZGVsJztcclxuaW1wb3J0IHsgRGF0YVRhYmxlQWRhcHRlciB9IGZyb20gJy4vZGF0YXRhYmxlLWFkYXB0ZXInO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcblxyXG4vLyBTaW1wbGUgaW1wbGVtZW50YXRpb24gb2YgdGhlIERhdGFUYWJsZUFkYXB0ZXIgaW50ZXJmYWNlLlxyXG5leHBvcnQgY2xhc3MgT2JqZWN0RGF0YVRhYmxlQWRhcHRlciBpbXBsZW1lbnRzIERhdGFUYWJsZUFkYXB0ZXIge1xyXG5cclxuICAgIHByaXZhdGUgX3NvcnRpbmc6IERhdGFTb3J0aW5nO1xyXG4gICAgcHJpdmF0ZSBfcm93czogRGF0YVJvd1tdO1xyXG4gICAgcHJpdmF0ZSBfY29sdW1uczogRGF0YUNvbHVtbltdO1xyXG5cclxuICAgIHNlbGVjdGVkUm93OiBEYXRhUm93O1xyXG4gICAgcm93c0NoYW5nZWQ6IFN1YmplY3Q8QXJyYXk8RGF0YVJvdz4+O1xyXG5cclxuICAgIHN0YXRpYyBnZW5lcmF0ZVNjaGVtYShkYXRhOiBhbnlbXSkge1xyXG4gICAgICAgIGNvbnN0IHNjaGVtYSA9IFtdO1xyXG5cclxuICAgICAgICBpZiAoZGF0YSAmJiBkYXRhLmxlbmd0aCkge1xyXG4gICAgICAgICAgICBjb25zdCByb3dUb0V4YW1pbmF0ZSA9IGRhdGFbMF07XHJcblxyXG4gICAgICAgICAgICBpZiAodHlwZW9mIHJvd1RvRXhhbWluYXRlID09PSAnb2JqZWN0Jykge1xyXG4gICAgICAgICAgICAgICAgZm9yIChjb25zdCBrZXkgaW4gcm93VG9FeGFtaW5hdGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocm93VG9FeGFtaW5hdGUuaGFzT3duUHJvcGVydHkoa2V5KSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzY2hlbWEucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBrZXk6IGtleSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiBrZXksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBzb3J0YWJsZTogZmFsc2VcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gc2NoZW1hO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKGRhdGE6IGFueVtdID0gW10sIHNjaGVtYTogRGF0YUNvbHVtbltdID0gW10pIHtcclxuICAgICAgICB0aGlzLl9yb3dzID0gW107XHJcbiAgICAgICAgdGhpcy5fY29sdW1ucyA9IFtdO1xyXG5cclxuICAgICAgICBpZiAoZGF0YSAmJiBkYXRhLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgdGhpcy5fcm93cyA9IGRhdGEubWFwKChpdGVtKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IE9iamVjdERhdGFSb3coaXRlbSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHNjaGVtYSAmJiBzY2hlbWEubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICB0aGlzLl9jb2x1bW5zID0gc2NoZW1hLm1hcCgoaXRlbSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBPYmplY3REYXRhQ29sdW1uKGl0ZW0pO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIC8vIFNvcnQgYnkgZmlyc3Qgc29ydGFibGUgb3IganVzdCBmaXJzdCBjb2x1bW5cclxuICAgICAgICAgICAgY29uc3Qgc29ydGFibGUgPSB0aGlzLl9jb2x1bW5zLmZpbHRlcigoY29sdW1uKSA9PiBjb2x1bW4uc29ydGFibGUpO1xyXG4gICAgICAgICAgICBpZiAoc29ydGFibGUubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zb3J0KHNvcnRhYmxlWzBdLmtleSwgJ2FzYycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnJvd3NDaGFuZ2VkID0gbmV3IFN1YmplY3Q8QXJyYXk8RGF0YVJvdz4+KCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Um93cygpOiBBcnJheTxEYXRhUm93PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3Jvd3M7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0Um93cyhyb3dzOiBBcnJheTxEYXRhUm93Pikge1xyXG4gICAgICAgIHRoaXMuX3Jvd3MgPSByb3dzIHx8IFtdO1xyXG4gICAgICAgIHRoaXMuc29ydCgpO1xyXG4gICAgICAgIHRoaXMucm93c0NoYW5nZWQubmV4dCh0aGlzLl9yb3dzKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDb2x1bW5zKCk6IEFycmF5PERhdGFDb2x1bW4+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fY29sdW1ucztcclxuICAgIH1cclxuXHJcbiAgICBzZXRDb2x1bW5zKGNvbHVtbnM6IEFycmF5PERhdGFDb2x1bW4+KSB7XHJcbiAgICAgICAgdGhpcy5fY29sdW1ucyA9IGNvbHVtbnMgfHwgW107XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VmFsdWUocm93OiBEYXRhUm93LCBjb2w6IERhdGFDb2x1bW4pOiBhbnkge1xyXG4gICAgICAgIGlmICghcm93KSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignUm93IG5vdCBmb3VuZCcpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIWNvbCkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0NvbHVtbiBub3QgZm91bmQnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHZhbHVlID0gcm93LmdldFZhbHVlKGNvbC5rZXkpO1xyXG5cclxuICAgICAgICBpZiAoY29sLnR5cGUgPT09ICdpY29uJykge1xyXG4gICAgICAgICAgICBjb25zdCBpY29uID0gcm93LmdldFZhbHVlKGNvbC5rZXkpO1xyXG4gICAgICAgICAgICByZXR1cm4gaWNvbjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRTb3J0aW5nKCk6IERhdGFTb3J0aW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fc29ydGluZztcclxuICAgIH1cclxuXHJcbiAgICBzZXRTb3J0aW5nKHNvcnRpbmc6IERhdGFTb3J0aW5nKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5fc29ydGluZyA9IHNvcnRpbmc7XHJcblxyXG4gICAgICAgIGlmIChzb3J0aW5nICYmIHNvcnRpbmcua2V5KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3Jvd3Muc29ydCgoYTogRGF0YVJvdywgYjogRGF0YVJvdykgPT4ge1xyXG4gICAgICAgICAgICAgICAgbGV0IGxlZnQgPSBhLmdldFZhbHVlKHNvcnRpbmcua2V5KTtcclxuICAgICAgICAgICAgICAgIGlmIChsZWZ0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGVmdCA9IChsZWZ0IGluc3RhbmNlb2YgRGF0ZSkgPyBsZWZ0LnZhbHVlT2YoKS50b1N0cmluZygpIDogbGVmdC50b1N0cmluZygpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBsZWZ0ID0gJyc7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgbGV0IHJpZ2h0ID0gYi5nZXRWYWx1ZShzb3J0aW5nLmtleSk7XHJcbiAgICAgICAgICAgICAgICBpZiAocmlnaHQpIHtcclxuICAgICAgICAgICAgICAgICAgICByaWdodCA9IChyaWdodCBpbnN0YW5jZW9mIERhdGUpID8gcmlnaHQudmFsdWVPZigpLnRvU3RyaW5nKCkgOiByaWdodC50b1N0cmluZygpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICByaWdodCA9ICcnO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHJldHVybiBzb3J0aW5nLmRpcmVjdGlvbiA9PT0gJ2FzYydcclxuICAgICAgICAgICAgICAgICAgICA/IGxlZnQubG9jYWxlQ29tcGFyZShyaWdodClcclxuICAgICAgICAgICAgICAgICAgICA6IHJpZ2h0LmxvY2FsZUNvbXBhcmUobGVmdCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzb3J0KGtleT86IHN0cmluZywgZGlyZWN0aW9uPzogc3RyaW5nKTogdm9pZCB7XHJcbiAgICAgICAgY29uc3Qgc29ydGluZyA9IHRoaXMuX3NvcnRpbmcgfHwgbmV3IERhdGFTb3J0aW5nKCk7XHJcbiAgICAgICAgaWYgKGtleSkge1xyXG4gICAgICAgICAgICBzb3J0aW5nLmtleSA9IGtleTtcclxuICAgICAgICAgICAgc29ydGluZy5kaXJlY3Rpb24gPSBkaXJlY3Rpb24gfHwgJ2FzYyc7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc2V0U29ydGluZyhzb3J0aW5nKTtcclxuICAgIH1cclxufVxyXG4iXX0=