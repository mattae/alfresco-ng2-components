/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { TranslateModule } from '@ngx-translate/core';
import { MaterialModule } from '../material.module';
import { ContextMenuModule } from '../context-menu/context-menu.module';
import { PipeModule } from '../pipes/pipe.module';
import { DirectiveModule } from '../directives/directive.module';
import { DataTableCellComponent } from './components/datatable/datatable-cell.component';
import { DataTableComponent } from './components/datatable/datatable.component';
import { DateCellComponent } from './components/datatable/date-cell.component';
import { EmptyListBodyDirective, EmptyListComponent, EmptyListFooterDirective, EmptyListHeaderDirective } from './components/datatable/empty-list.component';
import { FileSizeCellComponent } from './components/datatable/filesize-cell.component';
import { LocationCellComponent } from './components/datatable/location-cell.component';
import { LoadingContentTemplateDirective } from './directives/loading-template.directive';
import { NoContentTemplateDirective } from './directives/no-content-template.directive';
import { NoPermissionTemplateDirective } from './directives/no-permission-template.directive';
import { CustomEmptyContentTemplateDirective } from './directives/custom-empty-content-template.directive';
import { CustomLoadingContentTemplateDirective } from './directives/custom-loading-template.directive';
import { CustomNoPermissionTemplateDirective } from './directives/custom-no-permission-template.directive';
import { JsonCellComponent } from './components/datatable/json-cell.component';
import { ClipboardModule } from '../clipboard/clipboard.module';
import { DropZoneDirective } from './components/datatable/drop-zone.directive';
export class DataTableModule {
}
DataTableModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    RouterModule,
                    MaterialModule,
                    CommonModule,
                    TranslateModule.forChild(),
                    ContextMenuModule,
                    PipeModule,
                    DirectiveModule,
                    ClipboardModule
                ],
                declarations: [
                    DataTableComponent,
                    EmptyListComponent,
                    EmptyListHeaderDirective,
                    EmptyListBodyDirective,
                    EmptyListFooterDirective,
                    DataTableCellComponent,
                    DateCellComponent,
                    FileSizeCellComponent,
                    LocationCellComponent,
                    JsonCellComponent,
                    NoContentTemplateDirective,
                    NoPermissionTemplateDirective,
                    LoadingContentTemplateDirective,
                    CustomEmptyContentTemplateDirective,
                    CustomLoadingContentTemplateDirective,
                    CustomNoPermissionTemplateDirective,
                    DropZoneDirective
                ],
                exports: [
                    DataTableComponent,
                    EmptyListComponent,
                    EmptyListHeaderDirective,
                    EmptyListBodyDirective,
                    EmptyListFooterDirective,
                    DataTableCellComponent,
                    DateCellComponent,
                    FileSizeCellComponent,
                    LocationCellComponent,
                    JsonCellComponent,
                    NoContentTemplateDirective,
                    NoPermissionTemplateDirective,
                    LoadingContentTemplateDirective,
                    CustomEmptyContentTemplateDirective,
                    CustomLoadingContentTemplateDirective,
                    CustomNoPermissionTemplateDirective,
                    DropZoneDirective
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YXRhYmxlLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRhdGF0YWJsZS9kYXRhdGFibGUubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFdEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUVsRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDakUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0saURBQWlELENBQUM7QUFDekYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDaEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDL0UsT0FBTyxFQUFFLHNCQUFzQixFQUMzQixrQkFBa0IsRUFDbEIsd0JBQXdCLEVBQ3hCLHdCQUF3QixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDbEYsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDdkYsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDdkYsT0FBTyxFQUFFLCtCQUErQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDMUYsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDeEYsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDOUYsT0FBTyxFQUFFLG1DQUFtQyxFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDM0csT0FBTyxFQUFFLHFDQUFxQyxFQUFFLE1BQU0sZ0RBQWdELENBQUM7QUFDdkcsT0FBTyxFQUFFLG1DQUFtQyxFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDM0csT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDL0UsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQ2hFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBcUQvRSxNQUFNLE9BQU8sZUFBZTs7O1lBbkQzQixRQUFRLFNBQUM7Z0JBQ04sT0FBTyxFQUFFO29CQUNMLFlBQVk7b0JBQ1osY0FBYztvQkFDZCxZQUFZO29CQUNaLGVBQWUsQ0FBQyxRQUFRLEVBQUU7b0JBQzFCLGlCQUFpQjtvQkFDakIsVUFBVTtvQkFDVixlQUFlO29CQUNmLGVBQWU7aUJBQ2xCO2dCQUNELFlBQVksRUFBRTtvQkFDVixrQkFBa0I7b0JBQ2xCLGtCQUFrQjtvQkFDbEIsd0JBQXdCO29CQUN4QixzQkFBc0I7b0JBQ3RCLHdCQUF3QjtvQkFDeEIsc0JBQXNCO29CQUN0QixpQkFBaUI7b0JBQ2pCLHFCQUFxQjtvQkFDckIscUJBQXFCO29CQUNyQixpQkFBaUI7b0JBQ2pCLDBCQUEwQjtvQkFDMUIsNkJBQTZCO29CQUM3QiwrQkFBK0I7b0JBQy9CLG1DQUFtQztvQkFDbkMscUNBQXFDO29CQUNyQyxtQ0FBbUM7b0JBQ25DLGlCQUFpQjtpQkFDcEI7Z0JBQ0QsT0FBTyxFQUFFO29CQUNMLGtCQUFrQjtvQkFDbEIsa0JBQWtCO29CQUNsQix3QkFBd0I7b0JBQ3hCLHNCQUFzQjtvQkFDdEIsd0JBQXdCO29CQUN4QixzQkFBc0I7b0JBQ3RCLGlCQUFpQjtvQkFDakIscUJBQXFCO29CQUNyQixxQkFBcUI7b0JBQ3JCLGlCQUFpQjtvQkFDakIsMEJBQTBCO29CQUMxQiw2QkFBNkI7b0JBQzdCLCtCQUErQjtvQkFDL0IsbUNBQW1DO29CQUNuQyxxQ0FBcUM7b0JBQ3JDLG1DQUFtQztvQkFDbkMsaUJBQWlCO2lCQUNwQjthQUVKIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZU1vZHVsZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuLi9tYXRlcmlhbC5tb2R1bGUnO1xyXG5pbXBvcnQgeyBDb250ZXh0TWVudU1vZHVsZSB9IGZyb20gJy4uL2NvbnRleHQtbWVudS9jb250ZXh0LW1lbnUubW9kdWxlJztcclxuaW1wb3J0IHsgUGlwZU1vZHVsZSB9IGZyb20gJy4uL3BpcGVzL3BpcGUubW9kdWxlJztcclxuXHJcbmltcG9ydCB7IERpcmVjdGl2ZU1vZHVsZSB9IGZyb20gJy4uL2RpcmVjdGl2ZXMvZGlyZWN0aXZlLm1vZHVsZSc7XHJcbmltcG9ydCB7IERhdGFUYWJsZUNlbGxDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZGF0YXRhYmxlL2RhdGF0YWJsZS1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IERhdGFUYWJsZUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9kYXRhdGFibGUvZGF0YXRhYmxlLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IERhdGVDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2RhdGF0YWJsZS9kYXRlLWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRW1wdHlMaXN0Qm9keURpcmVjdGl2ZSxcclxuICAgIEVtcHR5TGlzdENvbXBvbmVudCxcclxuICAgIEVtcHR5TGlzdEZvb3RlckRpcmVjdGl2ZSxcclxuICAgIEVtcHR5TGlzdEhlYWRlckRpcmVjdGl2ZSB9IGZyb20gJy4vY29tcG9uZW50cy9kYXRhdGFibGUvZW1wdHktbGlzdC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBGaWxlU2l6ZUNlbGxDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZGF0YXRhYmxlL2ZpbGVzaXplLWNlbGwuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTG9jYXRpb25DZWxsQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2RhdGF0YWJsZS9sb2NhdGlvbi1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IExvYWRpbmdDb250ZW50VGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2RpcmVjdGl2ZXMvbG9hZGluZy10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBOb0NvbnRlbnRUZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vZGlyZWN0aXZlcy9uby1jb250ZW50LXRlbXBsYXRlLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IE5vUGVybWlzc2lvblRlbXBsYXRlRGlyZWN0aXZlIH0gZnJvbSAnLi9kaXJlY3RpdmVzL25vLXBlcm1pc3Npb24tdGVtcGxhdGUuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgQ3VzdG9tRW1wdHlDb250ZW50VGVtcGxhdGVEaXJlY3RpdmUgfSBmcm9tICcuL2RpcmVjdGl2ZXMvY3VzdG9tLWVtcHR5LWNvbnRlbnQtdGVtcGxhdGUuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgQ3VzdG9tTG9hZGluZ0NvbnRlbnRUZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vZGlyZWN0aXZlcy9jdXN0b20tbG9hZGluZy10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBDdXN0b21Ob1Blcm1pc3Npb25UZW1wbGF0ZURpcmVjdGl2ZSB9IGZyb20gJy4vZGlyZWN0aXZlcy9jdXN0b20tbm8tcGVybWlzc2lvbi10ZW1wbGF0ZS5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBKc29uQ2VsbENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9kYXRhdGFibGUvanNvbi1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENsaXBib2FyZE1vZHVsZSB9IGZyb20gJy4uL2NsaXBib2FyZC9jbGlwYm9hcmQubW9kdWxlJztcclxuaW1wb3J0IHsgRHJvcFpvbmVEaXJlY3RpdmUgfSBmcm9tICcuL2NvbXBvbmVudHMvZGF0YXRhYmxlL2Ryb3Atem9uZS5kaXJlY3RpdmUnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBSb3V0ZXJNb2R1bGUsXHJcbiAgICAgICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgICAgIFRyYW5zbGF0ZU1vZHVsZS5mb3JDaGlsZCgpLFxyXG4gICAgICAgIENvbnRleHRNZW51TW9kdWxlLFxyXG4gICAgICAgIFBpcGVNb2R1bGUsXHJcbiAgICAgICAgRGlyZWN0aXZlTW9kdWxlLFxyXG4gICAgICAgIENsaXBib2FyZE1vZHVsZVxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIERhdGFUYWJsZUNvbXBvbmVudCxcclxuICAgICAgICBFbXB0eUxpc3RDb21wb25lbnQsXHJcbiAgICAgICAgRW1wdHlMaXN0SGVhZGVyRGlyZWN0aXZlLFxyXG4gICAgICAgIEVtcHR5TGlzdEJvZHlEaXJlY3RpdmUsXHJcbiAgICAgICAgRW1wdHlMaXN0Rm9vdGVyRGlyZWN0aXZlLFxyXG4gICAgICAgIERhdGFUYWJsZUNlbGxDb21wb25lbnQsXHJcbiAgICAgICAgRGF0ZUNlbGxDb21wb25lbnQsXHJcbiAgICAgICAgRmlsZVNpemVDZWxsQ29tcG9uZW50LFxyXG4gICAgICAgIExvY2F0aW9uQ2VsbENvbXBvbmVudCxcclxuICAgICAgICBKc29uQ2VsbENvbXBvbmVudCxcclxuICAgICAgICBOb0NvbnRlbnRUZW1wbGF0ZURpcmVjdGl2ZSxcclxuICAgICAgICBOb1Blcm1pc3Npb25UZW1wbGF0ZURpcmVjdGl2ZSxcclxuICAgICAgICBMb2FkaW5nQ29udGVudFRlbXBsYXRlRGlyZWN0aXZlLFxyXG4gICAgICAgIEN1c3RvbUVtcHR5Q29udGVudFRlbXBsYXRlRGlyZWN0aXZlLFxyXG4gICAgICAgIEN1c3RvbUxvYWRpbmdDb250ZW50VGVtcGxhdGVEaXJlY3RpdmUsXHJcbiAgICAgICAgQ3VzdG9tTm9QZXJtaXNzaW9uVGVtcGxhdGVEaXJlY3RpdmUsXHJcbiAgICAgICAgRHJvcFpvbmVEaXJlY3RpdmVcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgRGF0YVRhYmxlQ29tcG9uZW50LFxyXG4gICAgICAgIEVtcHR5TGlzdENvbXBvbmVudCxcclxuICAgICAgICBFbXB0eUxpc3RIZWFkZXJEaXJlY3RpdmUsXHJcbiAgICAgICAgRW1wdHlMaXN0Qm9keURpcmVjdGl2ZSxcclxuICAgICAgICBFbXB0eUxpc3RGb290ZXJEaXJlY3RpdmUsXHJcbiAgICAgICAgRGF0YVRhYmxlQ2VsbENvbXBvbmVudCxcclxuICAgICAgICBEYXRlQ2VsbENvbXBvbmVudCxcclxuICAgICAgICBGaWxlU2l6ZUNlbGxDb21wb25lbnQsXHJcbiAgICAgICAgTG9jYXRpb25DZWxsQ29tcG9uZW50LFxyXG4gICAgICAgIEpzb25DZWxsQ29tcG9uZW50LFxyXG4gICAgICAgIE5vQ29udGVudFRlbXBsYXRlRGlyZWN0aXZlLFxyXG4gICAgICAgIE5vUGVybWlzc2lvblRlbXBsYXRlRGlyZWN0aXZlLFxyXG4gICAgICAgIExvYWRpbmdDb250ZW50VGVtcGxhdGVEaXJlY3RpdmUsXHJcbiAgICAgICAgQ3VzdG9tRW1wdHlDb250ZW50VGVtcGxhdGVEaXJlY3RpdmUsXHJcbiAgICAgICAgQ3VzdG9tTG9hZGluZ0NvbnRlbnRUZW1wbGF0ZURpcmVjdGl2ZSxcclxuICAgICAgICBDdXN0b21Ob1Blcm1pc3Npb25UZW1wbGF0ZURpcmVjdGl2ZSxcclxuICAgICAgICBEcm9wWm9uZURpcmVjdGl2ZVxyXG4gICAgXVxyXG5cclxufSlcclxuZXhwb3J0IGNsYXNzIERhdGFUYWJsZU1vZHVsZSB7fVxyXG4iXX0=