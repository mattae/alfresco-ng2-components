/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Inject, ViewEncapsulation } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { LogService } from '../services/log.service';
import { DownloadZipService } from '../services/download-zip.service';
export class DownloadZipDialogComponent {
    /**
     * @param {?} dialogRef
     * @param {?} data
     * @param {?} logService
     * @param {?} downloadZipService
     */
    constructor(dialogRef, data, logService, downloadZipService) {
        this.dialogRef = dialogRef;
        this.data = data;
        this.logService = logService;
        this.downloadZipService = downloadZipService;
        // flag for async threads
        this.cancelled = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.data && this.data.nodeIds && this.data.nodeIds.length > 0) {
            if (!this.cancelled) {
                this.downloadZip(this.data.nodeIds);
            }
            else {
                this.logService.log('Cancelled');
            }
        }
    }
    /**
     * @return {?}
     */
    cancelDownload() {
        this.cancelled = true;
        this.downloadZipService.cancelDownload(this.downloadId);
        this.dialogRef.close(false);
    }
    /**
     * @param {?} nodeIds
     * @return {?}
     */
    downloadZip(nodeIds) {
        if (nodeIds && nodeIds.length > 0) {
            this.downloadZipService.createDownload({ nodeIds }).subscribe((/**
             * @param {?} data
             * @return {?}
             */
            (data) => {
                if (data && data.entry && data.entry.id) {
                    /** @type {?} */
                    const url = this.downloadZipService.getContentUrl(data.entry.id, true);
                    this.downloadZipService.getNode(data.entry.id).subscribe((/**
                     * @param {?} downloadNode
                     * @return {?}
                     */
                    (downloadNode) => {
                        this.logService.log(downloadNode);
                        /** @type {?} */
                        const fileName = downloadNode.entry.name;
                        this.downloadId = data.entry.id;
                        this.waitAndDownload(data.entry.id, url, fileName);
                    }));
                }
            }));
        }
    }
    /**
     * @param {?} downloadId
     * @param {?} url
     * @param {?} fileName
     * @return {?}
     */
    waitAndDownload(downloadId, url, fileName) {
        if (this.cancelled) {
            return;
        }
        this.downloadZipService.getDownload(downloadId).subscribe((/**
         * @param {?} downloadEntry
         * @return {?}
         */
        (downloadEntry) => {
            if (downloadEntry.entry) {
                if (downloadEntry.entry.status === 'DONE') {
                    this.download(url, fileName);
                }
                else {
                    setTimeout((/**
                     * @return {?}
                     */
                    () => {
                        this.waitAndDownload(downloadId, url, fileName);
                    }), 1000);
                }
            }
        }));
    }
    /**
     * @param {?} url
     * @param {?} fileName
     * @return {?}
     */
    download(url, fileName) {
        if (url && fileName) {
            /** @type {?} */
            const link = document.createElement('a');
            link.style.display = 'none';
            link.download = fileName;
            link.href = url;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
        this.dialogRef.close(true);
    }
}
DownloadZipDialogComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-download-zip-dialog',
                template: "<h1 matDialogTitle>{{ 'CORE.DIALOG.DOWNLOAD_ZIP.TITLE' | translate }}</h1>\r\n<div mat-dialog-content>\r\n    <mat-progress-bar color=\"primary\" mode=\"indeterminate\"></mat-progress-bar>\r\n</div>\r\n<div mat-dialog-actions>\r\n    <span class=\"adf-spacer\"></span>\r\n    <button mat-button color=\"primary\" id=\"cancel-button\" (click)=\"cancelDownload()\">\r\n        {{ 'CORE.DIALOG.DOWNLOAD_ZIP.ACTIONS.CANCEL' | translate }}\r\n    </button>\r\n</div>\r\n",
                host: { 'class': 'adf-download-zip-dialog' },
                encapsulation: ViewEncapsulation.None,
                styles: [".adf-spacer{flex:1 1 auto}.adf-download-zip-dialog .mat-dialog-actions .mat-button-wrapper{text-transform:uppercase}"]
            }] }
];
/** @nocollapse */
DownloadZipDialogComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] },
    { type: LogService },
    { type: DownloadZipService }
];
if (false) {
    /** @type {?} */
    DownloadZipDialogComponent.prototype.cancelled;
    /** @type {?} */
    DownloadZipDialogComponent.prototype.downloadId;
    /**
     * @type {?}
     * @private
     */
    DownloadZipDialogComponent.prototype.dialogRef;
    /** @type {?} */
    DownloadZipDialogComponent.prototype.data;
    /**
     * @type {?}
     * @private
     */
    DownloadZipDialogComponent.prototype.logService;
    /**
     * @type {?}
     * @private
     */
    DownloadZipDialogComponent.prototype.downloadZipService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG93bmxvYWQtemlwLmRpYWxvZy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRpYWxvZ3MvZG93bmxvYWQtemlwLmRpYWxvZy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBVSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3RSxPQUFPLEVBQUUsZUFBZSxFQUFFLFlBQVksRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBRWxFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUNyRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQVN0RSxNQUFNLE9BQU8sMEJBQTBCOzs7Ozs7O0lBTW5DLFlBQW9CLFNBQW1ELEVBRXBELElBQVMsRUFDUixVQUFzQixFQUN0QixrQkFBc0M7UUFKdEMsY0FBUyxHQUFULFNBQVMsQ0FBMEM7UUFFcEQsU0FBSSxHQUFKLElBQUksQ0FBSztRQUNSLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjs7UUFQMUQsY0FBUyxHQUFHLEtBQUssQ0FBQztJQVFsQixDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ2hFLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFO2dCQUNqQixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7YUFDdkM7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDcEM7U0FDSjtJQUNMLENBQUM7Ozs7SUFFRCxjQUFjO1FBQ1YsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7UUFDdEIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDaEMsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsT0FBaUI7UUFDekIsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFFL0IsSUFBSSxDQUFDLGtCQUFrQixDQUFDLGNBQWMsQ0FBQyxFQUFFLE9BQU8sRUFBRSxDQUFDLENBQUMsU0FBUzs7OztZQUFDLENBQUMsSUFBbUIsRUFBRSxFQUFFO2dCQUNsRixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFOzswQkFDL0IsR0FBRyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDO29CQUV0RSxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsU0FBUzs7OztvQkFBQyxDQUFDLFlBQXVCLEVBQUUsRUFBRTt3QkFDakYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsWUFBWSxDQUFDLENBQUM7OzhCQUM1QixRQUFRLEdBQUcsWUFBWSxDQUFDLEtBQUssQ0FBQyxJQUFJO3dCQUN4QyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO3dCQUNoQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxFQUFFLEdBQUcsRUFBRSxRQUFRLENBQUMsQ0FBQztvQkFDdkQsQ0FBQyxFQUFDLENBQUM7aUJBQ047WUFDTCxDQUFDLEVBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQzs7Ozs7OztJQUVELGVBQWUsQ0FBQyxVQUFrQixFQUFFLEdBQVcsRUFBRSxRQUFnQjtRQUM3RCxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDaEIsT0FBTztTQUNWO1FBRUQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsQ0FBQyxhQUE0QixFQUFFLEVBQUU7WUFDdkYsSUFBSSxhQUFhLENBQUMsS0FBSyxFQUFFO2dCQUNyQixJQUFJLGFBQWEsQ0FBQyxLQUFLLENBQUMsTUFBTSxLQUFLLE1BQU0sRUFBRTtvQkFDdkMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7aUJBQ2hDO3FCQUFNO29CQUNILFVBQVU7OztvQkFBQyxHQUFHLEVBQUU7d0JBQ1osSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsR0FBRyxFQUFFLFFBQVEsQ0FBQyxDQUFDO29CQUNwRCxDQUFDLEdBQUUsSUFBSSxDQUFDLENBQUM7aUJBQ1o7YUFDSjtRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7O0lBRUQsUUFBUSxDQUFDLEdBQVcsRUFBRSxRQUFnQjtRQUNsQyxJQUFJLEdBQUcsSUFBSSxRQUFRLEVBQUU7O2tCQUNYLElBQUksR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLEdBQUcsQ0FBQztZQUV4QyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUM7WUFDNUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7WUFDekIsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUM7WUFFaEIsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDaEMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2IsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDbkM7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUMvQixDQUFDOzs7WUFyRkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSx5QkFBeUI7Z0JBQ25DLDZkQUF5QztnQkFFekMsSUFBSSxFQUFFLEVBQUUsT0FBTyxFQUFFLHlCQUF5QixFQUFFO2dCQUM1QyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7YUFDeEM7Ozs7WUFYeUIsWUFBWTs0Q0FtQnJCLE1BQU0sU0FBQyxlQUFlO1lBakI5QixVQUFVO1lBQ1Ysa0JBQWtCOzs7O0lBWXZCLCtDQUFrQjs7SUFDbEIsZ0RBQW1COzs7OztJQUVQLCtDQUEyRDs7SUFDM0QsMENBQ2dCOzs7OztJQUNoQixnREFBOEI7Ozs7O0lBQzlCLHdEQUE4QyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIEluamVjdCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNQVRfRElBTE9HX0RBVEEsIE1hdERpYWxvZ1JlZiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgRG93bmxvYWRFbnRyeSwgTm9kZUVudHJ5IH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9sb2cuc2VydmljZSc7XHJcbmltcG9ydCB7IERvd25sb2FkWmlwU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Rvd25sb2FkLXppcC5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtZG93bmxvYWQtemlwLWRpYWxvZycsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vZG93bmxvYWQtemlwLmRpYWxvZy5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2Rvd25sb2FkLXppcC5kaWFsb2cuc2NzcyddLFxyXG4gICAgaG9zdDogeyAnY2xhc3MnOiAnYWRmLWRvd25sb2FkLXppcC1kaWFsb2cnIH0sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEb3dubG9hZFppcERpYWxvZ0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgLy8gZmxhZyBmb3IgYXN5bmMgdGhyZWFkc1xyXG4gICAgY2FuY2VsbGVkID0gZmFsc2U7XHJcbiAgICBkb3dubG9hZElkOiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBkaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxEb3dubG9hZFppcERpYWxvZ0NvbXBvbmVudD4sXHJcbiAgICAgICAgICAgICAgICBASW5qZWN0KE1BVF9ESUFMT0dfREFUQSlcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBkYXRhOiBhbnksXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGRvd25sb2FkWmlwU2VydmljZTogRG93bmxvYWRaaXBTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZGF0YSAmJiB0aGlzLmRhdGEubm9kZUlkcyAmJiB0aGlzLmRhdGEubm9kZUlkcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5jYW5jZWxsZWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZG93bmxvYWRaaXAodGhpcy5kYXRhLm5vZGVJZHMpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmxvZygnQ2FuY2VsbGVkJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY2FuY2VsRG93bmxvYWQoKSB7XHJcbiAgICAgICAgdGhpcy5jYW5jZWxsZWQgPSB0cnVlO1xyXG4gICAgICAgIHRoaXMuZG93bmxvYWRaaXBTZXJ2aWNlLmNhbmNlbERvd25sb2FkKHRoaXMuZG93bmxvYWRJZCk7XHJcbiAgICAgICAgdGhpcy5kaWFsb2dSZWYuY2xvc2UoZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIGRvd25sb2FkWmlwKG5vZGVJZHM6IHN0cmluZ1tdKSB7XHJcbiAgICAgICAgaWYgKG5vZGVJZHMgJiYgbm9kZUlkcy5sZW5ndGggPiAwKSB7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmRvd25sb2FkWmlwU2VydmljZS5jcmVhdGVEb3dubG9hZCh7IG5vZGVJZHMgfSkuc3Vic2NyaWJlKChkYXRhOiBEb3dubG9hZEVudHJ5KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZGF0YSAmJiBkYXRhLmVudHJ5ICYmIGRhdGEuZW50cnkuaWQpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmRvd25sb2FkWmlwU2VydmljZS5nZXRDb250ZW50VXJsKGRhdGEuZW50cnkuaWQsIHRydWUpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRvd25sb2FkWmlwU2VydmljZS5nZXROb2RlKGRhdGEuZW50cnkuaWQpLnN1YnNjcmliZSgoZG93bmxvYWROb2RlOiBOb2RlRW50cnkpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmxvZyhkb3dubG9hZE5vZGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWxlTmFtZSA9IGRvd25sb2FkTm9kZS5lbnRyeS5uYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRvd25sb2FkSWQgPSBkYXRhLmVudHJ5LmlkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLndhaXRBbmREb3dubG9hZChkYXRhLmVudHJ5LmlkLCB1cmwsIGZpbGVOYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHdhaXRBbmREb3dubG9hZChkb3dubG9hZElkOiBzdHJpbmcsIHVybDogc3RyaW5nLCBmaWxlTmFtZTogc3RyaW5nKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY2FuY2VsbGVkKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuZG93bmxvYWRaaXBTZXJ2aWNlLmdldERvd25sb2FkKGRvd25sb2FkSWQpLnN1YnNjcmliZSgoZG93bmxvYWRFbnRyeTogRG93bmxvYWRFbnRyeSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZG93bmxvYWRFbnRyeS5lbnRyeSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKGRvd25sb2FkRW50cnkuZW50cnkuc3RhdHVzID09PSAnRE9ORScpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRvd25sb2FkKHVybCwgZmlsZU5hbWUpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy53YWl0QW5kRG93bmxvYWQoZG93bmxvYWRJZCwgdXJsLCBmaWxlTmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgMTAwMCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBkb3dubG9hZCh1cmw6IHN0cmluZywgZmlsZU5hbWU6IHN0cmluZykge1xyXG4gICAgICAgIGlmICh1cmwgJiYgZmlsZU5hbWUpIHtcclxuICAgICAgICAgICAgY29uc3QgbGluayA9IGRvY3VtZW50LmNyZWF0ZUVsZW1lbnQoJ2EnKTtcclxuXHJcbiAgICAgICAgICAgIGxpbmsuc3R5bGUuZGlzcGxheSA9ICdub25lJztcclxuICAgICAgICAgICAgbGluay5kb3dubG9hZCA9IGZpbGVOYW1lO1xyXG4gICAgICAgICAgICBsaW5rLmhyZWYgPSB1cmw7XHJcblxyXG4gICAgICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGxpbmspO1xyXG4gICAgICAgICAgICBsaW5rLmNsaWNrKCk7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmJvZHkucmVtb3ZlQ2hpbGQobGluayk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKHRydWUpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==