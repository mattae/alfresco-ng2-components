/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:no-input-rename  */
import { ChangeDetectorRef, Directive, ElementRef, Host, Inject, Input, Optional, Renderer2 } from '@angular/core';
import { ContentService } from './../services/content.service';
import { EXTENDIBLE_COMPONENT } from './../interface/injection.tokens';
/**
 * @record
 */
export function NodeAllowableOperationSubject() { }
if (false) {
    /** @type {?} */
    NodeAllowableOperationSubject.prototype.disabled;
}
export class CheckAllowableOperationDirective {
    /**
     * @param {?} elementRef
     * @param {?} renderer
     * @param {?} contentService
     * @param {?} changeDetector
     * @param {?=} parentComponent
     */
    constructor(elementRef, renderer, contentService, changeDetector, parentComponent) {
        this.elementRef = elementRef;
        this.renderer = renderer;
        this.contentService = contentService;
        this.changeDetector = changeDetector;
        this.parentComponent = parentComponent;
        /**
         * Node permission to check (create, delete, update, updatePermissions,
         * !create, !delete, !update, !updatePermissions).
         */
        this.permission = null;
        /**
         * Nodes to check permission for.
         */
        this.nodes = [];
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes.nodes && !changes.nodes.firstChange) {
            this.updateElement();
        }
    }
    /**
     * Updates disabled state for the decorated element
     *
     * \@memberof CheckAllowableOperationDirective
     * @return {?}
     */
    updateElement() {
        /** @type {?} */
        const enable = this.hasAllowableOperations(this.nodes, this.permission);
        if (enable) {
            this.enable();
        }
        else {
            this.disable();
        }
        return enable;
    }
    /**
     * @private
     * @return {?}
     */
    enable() {
        if (this.parentComponent) {
            this.parentComponent.disabled = false;
            this.changeDetector.detectChanges();
        }
        else {
            this.enableElement();
        }
    }
    /**
     * @private
     * @return {?}
     */
    disable() {
        if (this.parentComponent) {
            this.parentComponent.disabled = true;
            this.changeDetector.detectChanges();
        }
        else {
            this.disableElement();
        }
    }
    /**
     * Enables decorated element
     *
     * \@memberof CheckAllowableOperationDirective
     * @return {?}
     */
    enableElement() {
        this.renderer.removeAttribute(this.elementRef.nativeElement, 'disabled');
    }
    /**
     * Disables decorated element
     *
     * \@memberof CheckAllowableOperationDirective
     * @return {?}
     */
    disableElement() {
        this.renderer.setAttribute(this.elementRef.nativeElement, 'disabled', 'true');
    }
    /**
     * Checks whether all nodes have a particular permission
     *
     * \@memberof CheckAllowableOperationDirective
     * @param {?} nodes Node collection to check
     * @param {?} permission Permission to check for each node
     * @return {?}
     */
    hasAllowableOperations(nodes, permission) {
        if (nodes && nodes.length > 0) {
            return nodes.every((/**
             * @param {?} node
             * @return {?}
             */
            (node) => this.contentService.hasAllowableOperations(node.entry, permission)));
        }
        return false;
    }
}
CheckAllowableOperationDirective.decorators = [
    { type: Directive, args: [{
                selector: '[adf-check-allowable-operation]'
            },] }
];
/** @nocollapse */
CheckAllowableOperationDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 },
    { type: ContentService },
    { type: ChangeDetectorRef },
    { type: undefined, decorators: [{ type: Host }, { type: Optional }, { type: Inject, args: [EXTENDIBLE_COMPONENT,] }] }
];
CheckAllowableOperationDirective.propDecorators = {
    permission: [{ type: Input, args: ['adf-check-allowable-operation',] }],
    nodes: [{ type: Input, args: ['adf-nodes',] }]
};
if (false) {
    /**
     * Node permission to check (create, delete, update, updatePermissions,
     * !create, !delete, !update, !updatePermissions).
     * @type {?}
     */
    CheckAllowableOperationDirective.prototype.permission;
    /**
     * Nodes to check permission for.
     * @type {?}
     */
    CheckAllowableOperationDirective.prototype.nodes;
    /**
     * @type {?}
     * @private
     */
    CheckAllowableOperationDirective.prototype.elementRef;
    /**
     * @type {?}
     * @private
     */
    CheckAllowableOperationDirective.prototype.renderer;
    /**
     * @type {?}
     * @private
     */
    CheckAllowableOperationDirective.prototype.contentService;
    /**
     * @type {?}
     * @private
     */
    CheckAllowableOperationDirective.prototype.changeDetector;
    /**
     * @type {?}
     * @private
     */
    CheckAllowableOperationDirective.prototype.parentComponent;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2stYWxsb3dhYmxlLW9wZXJhdGlvbi5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJkaXJlY3RpdmVzL2NoZWNrLWFsbG93YWJsZS1vcGVyYXRpb24uZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBYSxRQUFRLEVBQUUsU0FBUyxFQUFrQixNQUFNLGVBQWUsQ0FBQztBQUU5SSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDL0QsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0saUNBQWlDLENBQUM7Ozs7QUFFdkUsbURBRUM7OztJQURHLGlEQUFrQjs7QUFNdEIsTUFBTSxPQUFPLGdDQUFnQzs7Ozs7Ozs7SUFZekMsWUFBb0IsVUFBc0IsRUFDdEIsUUFBbUIsRUFDbkIsY0FBOEIsRUFDOUIsY0FBaUMsRUFJSCxlQUErQztRQVA3RSxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDbkIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFtQjtRQUlILG9CQUFlLEdBQWYsZUFBZSxDQUFnQzs7Ozs7UUFiakcsZUFBVSxHQUFZLElBQUksQ0FBQzs7OztRQUkzQixVQUFLLEdBQWdCLEVBQUUsQ0FBQztJQVV4QixDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxPQUFzQjtRQUM5QixJQUFJLE9BQU8sQ0FBQyxLQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRTtZQUM3QyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7U0FDeEI7SUFDTCxDQUFDOzs7Ozs7O0lBT0QsYUFBYTs7Y0FDSCxNQUFNLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUV2RSxJQUFJLE1BQU0sRUFBRTtZQUNSLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztTQUNqQjthQUFNO1lBQ0gsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ2xCO1FBRUQsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQzs7Ozs7SUFFTyxNQUFNO1FBQ1YsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztZQUN0QyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxDQUFDO1NBQ3ZDO2FBQU07WUFDSCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7U0FDeEI7SUFDTCxDQUFDOzs7OztJQUVPLE9BQU87UUFDWCxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDdEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3JDLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLENBQUM7U0FDdkM7YUFBTTtZQUNILElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUN6QjtJQUNMLENBQUM7Ozs7Ozs7SUFPRCxhQUFhO1FBQ1QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDN0UsQ0FBQzs7Ozs7OztJQU9ELGNBQWM7UUFDVixJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDbEYsQ0FBQzs7Ozs7Ozs7O0lBU0Qsc0JBQXNCLENBQUMsS0FBa0IsRUFBRSxVQUFrQjtRQUN6RCxJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUMzQixPQUFPLEtBQUssQ0FBQyxLQUFLOzs7O1lBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsc0JBQXNCLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsRUFBQyxDQUFDO1NBQ3BHO1FBRUQsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7O1lBakdKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsaUNBQWlDO2FBQzlDOzs7O1lBWHNDLFVBQVU7WUFBNEMsU0FBUztZQUU3RixjQUFjO1lBRmQsaUJBQWlCOzRDQTZCVCxJQUFJLFlBQ0osUUFBUSxZQUNSLE1BQU0sU0FBQyxvQkFBb0I7Ozt5QkFkdkMsS0FBSyxTQUFDLCtCQUErQjtvQkFJckMsS0FBSyxTQUFDLFdBQVc7Ozs7Ozs7O0lBSmxCLHNEQUMyQjs7Ozs7SUFHM0IsaURBQ3dCOzs7OztJQUVaLHNEQUE4Qjs7Ozs7SUFDOUIsb0RBQTJCOzs7OztJQUMzQiwwREFBc0M7Ozs7O0lBQ3RDLDBEQUF5Qzs7Ozs7SUFFekMsMkRBRXFGIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOm5vLWlucHV0LXJlbmFtZSAgKi9cclxuXHJcbmltcG9ydCB7IENoYW5nZURldGVjdG9yUmVmLCBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIEhvc3QsIEluamVjdCwgSW5wdXQsIE9uQ2hhbmdlcywgT3B0aW9uYWwsIFJlbmRlcmVyMiwgIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTm9kZUVudHJ5IH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IENvbnRlbnRTZXJ2aWNlIH0gZnJvbSAnLi8uLi9zZXJ2aWNlcy9jb250ZW50LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBFWFRFTkRJQkxFX0NPTVBPTkVOVCB9IGZyb20gJy4vLi4vaW50ZXJmYWNlL2luamVjdGlvbi50b2tlbnMnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBOb2RlQWxsb3dhYmxlT3BlcmF0aW9uU3ViamVjdCB7XHJcbiAgICBkaXNhYmxlZDogYm9vbGVhbjtcclxufVxyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1thZGYtY2hlY2stYWxsb3dhYmxlLW9wZXJhdGlvbl0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDaGVja0FsbG93YWJsZU9wZXJhdGlvbkRpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XHJcblxyXG4gICAgLyoqIE5vZGUgcGVybWlzc2lvbiB0byBjaGVjayAoY3JlYXRlLCBkZWxldGUsIHVwZGF0ZSwgdXBkYXRlUGVybWlzc2lvbnMsXHJcbiAgICAgKiAhY3JlYXRlLCAhZGVsZXRlLCAhdXBkYXRlLCAhdXBkYXRlUGVybWlzc2lvbnMpLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoJ2FkZi1jaGVjay1hbGxvd2FibGUtb3BlcmF0aW9uJylcclxuICAgIHBlcm1pc3Npb246IHN0cmluZyAgPSBudWxsO1xyXG5cclxuICAgIC8qKiBOb2RlcyB0byBjaGVjayBwZXJtaXNzaW9uIGZvci4gKi9cclxuICAgIEBJbnB1dCgnYWRmLW5vZGVzJylcclxuICAgIG5vZGVzOiBOb2RlRW50cnlbXSA9IFtdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgZWxlbWVudFJlZjogRWxlbWVudFJlZixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgcmVuZGVyZXI6IFJlbmRlcmVyMixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgY29udGVudFNlcnZpY2U6IENvbnRlbnRTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBjaGFuZ2VEZXRlY3RvcjogQ2hhbmdlRGV0ZWN0b3JSZWYsXHJcblxyXG4gICAgICAgICAgICAgICAgQEhvc3QoKVxyXG4gICAgICAgICAgICAgICAgQE9wdGlvbmFsKClcclxuICAgICAgICAgICAgICAgIEBJbmplY3QoRVhURU5ESUJMRV9DT01QT05FTlQpIHByaXZhdGUgcGFyZW50Q29tcG9uZW50PzogTm9kZUFsbG93YWJsZU9wZXJhdGlvblN1YmplY3QpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICAgICAgaWYgKGNoYW5nZXMubm9kZXMgJiYgIWNoYW5nZXMubm9kZXMuZmlyc3RDaGFuZ2UpIHtcclxuICAgICAgICAgICAgdGhpcy51cGRhdGVFbGVtZW50KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVXBkYXRlcyBkaXNhYmxlZCBzdGF0ZSBmb3IgdGhlIGRlY29yYXRlZCBlbGVtZW50XHJcbiAgICAgKlxyXG4gICAgICogQG1lbWJlcm9mIENoZWNrQWxsb3dhYmxlT3BlcmF0aW9uRGlyZWN0aXZlXHJcbiAgICAgKi9cclxuICAgIHVwZGF0ZUVsZW1lbnQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgY29uc3QgZW5hYmxlID0gdGhpcy5oYXNBbGxvd2FibGVPcGVyYXRpb25zKHRoaXMubm9kZXMsIHRoaXMucGVybWlzc2lvbik7XHJcblxyXG4gICAgICAgIGlmIChlbmFibGUpIHtcclxuICAgICAgICAgICAgdGhpcy5lbmFibGUoKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmRpc2FibGUoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBlbmFibGU7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBlbmFibGUoKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKHRoaXMucGFyZW50Q29tcG9uZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMucGFyZW50Q29tcG9uZW50LmRpc2FibGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuY2hhbmdlRGV0ZWN0b3IuZGV0ZWN0Q2hhbmdlcygpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuZW5hYmxlRWxlbWVudCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGRpc2FibGUoKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKHRoaXMucGFyZW50Q29tcG9uZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMucGFyZW50Q29tcG9uZW50LmRpc2FibGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5jaGFuZ2VEZXRlY3Rvci5kZXRlY3RDaGFuZ2VzKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5kaXNhYmxlRWxlbWVudCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEVuYWJsZXMgZGVjb3JhdGVkIGVsZW1lbnRcclxuICAgICAqXHJcbiAgICAgKiBAbWVtYmVyb2YgQ2hlY2tBbGxvd2FibGVPcGVyYXRpb25EaXJlY3RpdmVcclxuICAgICAqL1xyXG4gICAgZW5hYmxlRWxlbWVudCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnJlbmRlcmVyLnJlbW92ZUF0dHJpYnV0ZSh0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudCwgJ2Rpc2FibGVkJyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEaXNhYmxlcyBkZWNvcmF0ZWQgZWxlbWVudFxyXG4gICAgICpcclxuICAgICAqIEBtZW1iZXJvZiBDaGVja0FsbG93YWJsZU9wZXJhdGlvbkRpcmVjdGl2ZVxyXG4gICAgICovXHJcbiAgICBkaXNhYmxlRWxlbWVudCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnJlbmRlcmVyLnNldEF0dHJpYnV0ZSh0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudCwgJ2Rpc2FibGVkJywgJ3RydWUnKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrcyB3aGV0aGVyIGFsbCBub2RlcyBoYXZlIGEgcGFydGljdWxhciBwZXJtaXNzaW9uXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtICBub2RlcyBOb2RlIGNvbGxlY3Rpb24gdG8gY2hlY2tcclxuICAgICAqIEBwYXJhbSAgcGVybWlzc2lvbiBQZXJtaXNzaW9uIHRvIGNoZWNrIGZvciBlYWNoIG5vZGVcclxuICAgICAqIEBtZW1iZXJvZiBDaGVja0FsbG93YWJsZU9wZXJhdGlvbkRpcmVjdGl2ZVxyXG4gICAgICovXHJcbiAgICBoYXNBbGxvd2FibGVPcGVyYXRpb25zKG5vZGVzOiBOb2RlRW50cnlbXSwgcGVybWlzc2lvbjogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKG5vZGVzICYmIG5vZGVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgcmV0dXJuIG5vZGVzLmV2ZXJ5KChub2RlKSA9PiB0aGlzLmNvbnRlbnRTZXJ2aWNlLmhhc0FsbG93YWJsZU9wZXJhdGlvbnMobm9kZS5lbnRyeSwgcGVybWlzc2lvbikpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==