/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:no-input-rename  */
import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { HighlightTransformService } from '../services/highlight-transform.service';
export class HighlightDirective {
    /**
     * @param {?} el
     * @param {?} renderer
     * @param {?} highlightTransformService
     */
    constructor(el, renderer, highlightTransformService) {
        this.el = el;
        this.renderer = renderer;
        this.highlightTransformService = highlightTransformService;
        /**
         * Class selector for highlightable elements.
         */
        this.selector = '';
        /**
         * Text to highlight.
         */
        this.search = '';
        /**
         * CSS class used to apply highlighting.
         */
        this.classToApply = 'adf-highlight';
    }
    /**
     * @return {?}
     */
    ngAfterViewChecked() {
        this.highlight();
    }
    /**
     * @param {?=} search
     * @param {?=} selector
     * @param {?=} classToApply
     * @return {?}
     */
    highlight(search = this.search, selector = this.selector, classToApply = this.classToApply) {
        if (search && selector) {
            /** @type {?} */
            const elements = this.el.nativeElement.querySelectorAll(selector);
            elements.forEach((/**
             * @param {?} element
             * @return {?}
             */
            (element) => {
                /** @type {?} */
                const highlightTransformResult = this.highlightTransformService.highlight(element.innerHTML, search, classToApply);
                if (highlightTransformResult.changed) {
                    this.renderer.setProperty(element, 'innerHTML', highlightTransformResult.text);
                }
            }));
        }
    }
}
HighlightDirective.decorators = [
    { type: Directive, args: [{
                selector: '[adf-highlight]'
            },] }
];
/** @nocollapse */
HighlightDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 },
    { type: HighlightTransformService }
];
HighlightDirective.propDecorators = {
    selector: [{ type: Input, args: ['adf-highlight-selector',] }],
    search: [{ type: Input, args: ['adf-highlight',] }],
    classToApply: [{ type: Input, args: ['adf-highlight-class',] }]
};
if (false) {
    /**
     * Class selector for highlightable elements.
     * @type {?}
     */
    HighlightDirective.prototype.selector;
    /**
     * Text to highlight.
     * @type {?}
     */
    HighlightDirective.prototype.search;
    /**
     * CSS class used to apply highlighting.
     * @type {?}
     */
    HighlightDirective.prototype.classToApply;
    /**
     * @type {?}
     * @private
     */
    HighlightDirective.prototype.el;
    /**
     * @type {?}
     * @private
     */
    HighlightDirective.prototype.renderer;
    /**
     * @type {?}
     * @private
     */
    HighlightDirective.prototype.highlightTransformService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGlnaGxpZ2h0LmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvaGlnaGxpZ2h0LmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBb0IsTUFBTSxlQUFlLENBQUM7QUFDMUYsT0FBTyxFQUFFLHlCQUF5QixFQUE0QixNQUFNLHlDQUF5QyxDQUFDO0FBSzlHLE1BQU0sT0FBTyxrQkFBa0I7Ozs7OztJQWMzQixZQUNZLEVBQWMsRUFDZCxRQUFtQixFQUNuQix5QkFBb0Q7UUFGcEQsT0FBRSxHQUFGLEVBQUUsQ0FBWTtRQUNkLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDbkIsOEJBQXlCLEdBQXpCLHlCQUF5QixDQUEyQjs7OztRQWJoRSxhQUFRLEdBQVcsRUFBRSxDQUFDOzs7O1FBSXRCLFdBQU0sR0FBVyxFQUFFLENBQUM7Ozs7UUFJcEIsaUJBQVksR0FBVyxlQUFlLENBQUM7SUFNdkMsQ0FBQzs7OztJQUVELGtCQUFrQjtRQUNkLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztJQUNyQixDQUFDOzs7Ozs7O0lBRU0sU0FBUyxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxFQUFFLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWTtRQUM3RixJQUFJLE1BQU0sSUFBSSxRQUFRLEVBQUU7O2tCQUNkLFFBQVEsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7WUFFakUsUUFBUSxDQUFDLE9BQU87Ozs7WUFBQyxDQUFDLE9BQU8sRUFBRSxFQUFFOztzQkFDbkIsd0JBQXdCLEdBQTZCLElBQUksQ0FBQyx5QkFBeUIsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxNQUFNLEVBQUUsWUFBWSxDQUFDO2dCQUM1SSxJQUFJLHdCQUF3QixDQUFDLE9BQU8sRUFBRTtvQkFDbEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLFdBQVcsRUFBRSx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDbEY7WUFDTCxDQUFDLEVBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQzs7O1lBdENKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsaUJBQWlCO2FBQzlCOzs7O1lBTG1CLFVBQVU7WUFBUyxTQUFTO1lBQ3ZDLHlCQUF5Qjs7O3VCQVE3QixLQUFLLFNBQUMsd0JBQXdCO3FCQUk5QixLQUFLLFNBQUMsZUFBZTsyQkFJckIsS0FBSyxTQUFDLHFCQUFxQjs7Ozs7OztJQVI1QixzQ0FDc0I7Ozs7O0lBR3RCLG9DQUNvQjs7Ozs7SUFHcEIsMENBQ3VDOzs7OztJQUduQyxnQ0FBc0I7Ozs7O0lBQ3RCLHNDQUEyQjs7Ozs7SUFDM0IsdURBQTREIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOm5vLWlucHV0LXJlbmFtZSAgKi9cclxuXHJcbmltcG9ydCB7IERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgSW5wdXQsIFJlbmRlcmVyMiwgQWZ0ZXJWaWV3Q2hlY2tlZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBIaWdobGlnaHRUcmFuc2Zvcm1TZXJ2aWNlLCBIaWdobGlnaHRUcmFuc2Zvcm1SZXN1bHQgfSBmcm9tICcuLi9zZXJ2aWNlcy9oaWdobGlnaHQtdHJhbnNmb3JtLnNlcnZpY2UnO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1thZGYtaGlnaGxpZ2h0XSdcclxufSlcclxuZXhwb3J0IGNsYXNzIEhpZ2hsaWdodERpcmVjdGl2ZSBpbXBsZW1lbnRzIEFmdGVyVmlld0NoZWNrZWQge1xyXG5cclxuICAgIC8qKiBDbGFzcyBzZWxlY3RvciBmb3IgaGlnaGxpZ2h0YWJsZSBlbGVtZW50cy4gKi9cclxuICAgIEBJbnB1dCgnYWRmLWhpZ2hsaWdodC1zZWxlY3RvcicpXHJcbiAgICBzZWxlY3Rvcjogc3RyaW5nID0gJyc7XHJcblxyXG4gICAgLyoqIFRleHQgdG8gaGlnaGxpZ2h0LiAqL1xyXG4gICAgQElucHV0KCdhZGYtaGlnaGxpZ2h0JylcclxuICAgIHNlYXJjaDogc3RyaW5nID0gJyc7XHJcblxyXG4gICAgLyoqIENTUyBjbGFzcyB1c2VkIHRvIGFwcGx5IGhpZ2hsaWdodGluZy4gKi9cclxuICAgIEBJbnB1dCgnYWRmLWhpZ2hsaWdodC1jbGFzcycpXHJcbiAgICBjbGFzc1RvQXBwbHk6IHN0cmluZyA9ICdhZGYtaGlnaGxpZ2h0JztcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIGVsOiBFbGVtZW50UmVmLFxyXG4gICAgICAgIHByaXZhdGUgcmVuZGVyZXI6IFJlbmRlcmVyMixcclxuICAgICAgICBwcml2YXRlIGhpZ2hsaWdodFRyYW5zZm9ybVNlcnZpY2U6IEhpZ2hsaWdodFRyYW5zZm9ybVNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ0FmdGVyVmlld0NoZWNrZWQoKSB7XHJcbiAgICAgICAgdGhpcy5oaWdobGlnaHQoKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgaGlnaGxpZ2h0KHNlYXJjaCA9IHRoaXMuc2VhcmNoLCBzZWxlY3RvciA9IHRoaXMuc2VsZWN0b3IsIGNsYXNzVG9BcHBseSA9IHRoaXMuY2xhc3NUb0FwcGx5KSB7XHJcbiAgICAgICAgaWYgKHNlYXJjaCAmJiBzZWxlY3Rvcikge1xyXG4gICAgICAgICAgICBjb25zdCBlbGVtZW50cyA9IHRoaXMuZWwubmF0aXZlRWxlbWVudC5xdWVyeVNlbGVjdG9yQWxsKHNlbGVjdG9yKTtcclxuXHJcbiAgICAgICAgICAgIGVsZW1lbnRzLmZvckVhY2goKGVsZW1lbnQpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGhpZ2hsaWdodFRyYW5zZm9ybVJlc3VsdDogSGlnaGxpZ2h0VHJhbnNmb3JtUmVzdWx0ID0gdGhpcy5oaWdobGlnaHRUcmFuc2Zvcm1TZXJ2aWNlLmhpZ2hsaWdodChlbGVtZW50LmlubmVySFRNTCwgc2VhcmNoLCBjbGFzc1RvQXBwbHkpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGhpZ2hsaWdodFRyYW5zZm9ybVJlc3VsdC5jaGFuZ2VkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yZW5kZXJlci5zZXRQcm9wZXJ0eShlbGVtZW50LCAnaW5uZXJIVE1MJywgaGlnaGxpZ2h0VHJhbnNmb3JtUmVzdWx0LnRleHQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19