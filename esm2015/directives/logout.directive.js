/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Input, Directive, ElementRef, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
export class LogoutDirective {
    /**
     * @param {?} elementRef
     * @param {?} renderer
     * @param {?} router
     * @param {?} auth
     */
    constructor(elementRef, renderer, router, auth) {
        this.elementRef = elementRef;
        this.renderer = renderer;
        this.router = router;
        this.auth = auth;
        /**
         * URI to redirect to after logging out.
         */
        this.redirectUri = '/login';
        /**
         * Enable redirecting after logout
         */
        this.enableRedirect = true;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.elementRef.nativeElement) {
            this.renderer.listen(this.elementRef.nativeElement, 'click', (/**
             * @param {?} evt
             * @return {?}
             */
            (evt) => {
                evt.preventDefault();
                this.logout();
            }));
        }
    }
    /**
     * @return {?}
     */
    logout() {
        this.auth.logout().subscribe((/**
         * @return {?}
         */
        () => this.redirectToUri()), (/**
         * @return {?}
         */
        () => this.redirectToUri()));
    }
    /**
     * @return {?}
     */
    redirectToUri() {
        if (this.enableRedirect) {
            this.router.navigate([this.redirectUri]);
        }
    }
}
LogoutDirective.decorators = [
    { type: Directive, args: [{
                selector: '[adf-logout]'
            },] }
];
/** @nocollapse */
LogoutDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 },
    { type: Router },
    { type: AuthenticationService }
];
LogoutDirective.propDecorators = {
    redirectUri: [{ type: Input }],
    enableRedirect: [{ type: Input }]
};
if (false) {
    /**
     * URI to redirect to after logging out.
     * @type {?}
     */
    LogoutDirective.prototype.redirectUri;
    /**
     * Enable redirecting after logout
     * @type {?}
     */
    LogoutDirective.prototype.enableRedirect;
    /**
     * @type {?}
     * @private
     */
    LogoutDirective.prototype.elementRef;
    /**
     * @type {?}
     * @private
     */
    LogoutDirective.prototype.renderer;
    /**
     * @type {?}
     * @private
     */
    LogoutDirective.prototype.router;
    /**
     * @type {?}
     * @private
     */
    LogoutDirective.prototype.auth;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nb3V0LmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvbG9nb3V0LmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQVUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUszRSxNQUFNLE9BQU8sZUFBZTs7Ozs7OztJQVV4QixZQUFvQixVQUFzQixFQUN0QixRQUFtQixFQUNuQixNQUFjLEVBQ2QsSUFBMkI7UUFIM0IsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixhQUFRLEdBQVIsUUFBUSxDQUFXO1FBQ25CLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxTQUFJLEdBQUosSUFBSSxDQUF1Qjs7OztRQVQvQyxnQkFBVyxHQUFXLFFBQVEsQ0FBQzs7OztRQUkvQixtQkFBYyxHQUFZLElBQUksQ0FBQztJQU0vQixDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUU7WUFDL0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUUsT0FBTzs7OztZQUFFLENBQUMsR0FBRyxFQUFFLEVBQUU7Z0JBQ2pFLEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDckIsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ2xCLENBQUMsRUFBQyxDQUFDO1NBQ047SUFDTCxDQUFDOzs7O0lBRUQsTUFBTTtRQUNGLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUMsU0FBUzs7O1FBQ3hCLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUU7OztRQUMxQixHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQzdCLENBQUM7SUFDTixDQUFDOzs7O0lBRUQsYUFBYTtRQUNULElBQUksSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUNyQixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDO1NBQzVDO0lBQ0wsQ0FBQzs7O1lBdkNKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsY0FBYzthQUMzQjs7OztZQU4wQixVQUFVO1lBQVUsU0FBUztZQUMvQyxNQUFNO1lBQ04scUJBQXFCOzs7MEJBUXpCLEtBQUs7NkJBSUwsS0FBSzs7Ozs7OztJQUpOLHNDQUMrQjs7Ozs7SUFHL0IseUNBQytCOzs7OztJQUVuQixxQ0FBOEI7Ozs7O0lBQzlCLG1DQUEyQjs7Ozs7SUFDM0IsaUNBQXNCOzs7OztJQUN0QiwrQkFBbUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5wdXQsIERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgT25Jbml0LCBSZW5kZXJlcjIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgQXV0aGVudGljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYXV0aGVudGljYXRpb24uc2VydmljZSc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW2FkZi1sb2dvdXRdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTG9nb3V0RGlyZWN0aXZlIGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICAvKiogVVJJIHRvIHJlZGlyZWN0IHRvIGFmdGVyIGxvZ2dpbmcgb3V0LiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHJlZGlyZWN0VXJpOiBzdHJpbmcgPSAnL2xvZ2luJztcclxuXHJcbiAgICAvKiogRW5hYmxlIHJlZGlyZWN0aW5nIGFmdGVyIGxvZ291dCAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGVuYWJsZVJlZGlyZWN0OiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGVsZW1lbnRSZWY6IEVsZW1lbnRSZWYsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHJlbmRlcmVyOiBSZW5kZXJlcjIsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBhdXRoOiBBdXRoZW50aWNhdGlvblNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBpZiAodGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5yZW5kZXJlci5saXN0ZW4odGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsICdjbGljaycsIChldnQpID0+IHtcclxuICAgICAgICAgICAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2dvdXQoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGxvZ291dCgpIHtcclxuICAgICAgICB0aGlzLmF1dGgubG9nb3V0KCkuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAoKSA9PiB0aGlzLnJlZGlyZWN0VG9VcmkoKSxcclxuICAgICAgICAgICAgKCkgPT4gdGhpcy5yZWRpcmVjdFRvVXJpKClcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIHJlZGlyZWN0VG9VcmkoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZW5hYmxlUmVkaXJlY3QpIHtcclxuICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW3RoaXMucmVkaXJlY3RVcmldKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19