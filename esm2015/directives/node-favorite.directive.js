/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:no-input-rename  */
import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { from, forkJoin, of } from 'rxjs';
import { AlfrescoApiService } from '../services/alfresco-api.service';
import { catchError, map } from 'rxjs/operators';
export class NodeFavoriteDirective {
    /**
     * @param {?} alfrescoApiService
     */
    constructor(alfrescoApiService) {
        this.alfrescoApiService = alfrescoApiService;
        this.favorites = [];
        /**
         * Array of nodes to toggle as favorites.
         */
        this.selection = [];
        /**
         * Emitted when the favorite setting is complete.
         */
        this.toggle = new EventEmitter();
        /**
         * Emitted when the favorite setting fails.
         */
        this.error = new EventEmitter();
    }
    /**
     * @return {?}
     */
    onClick() {
        this.toggleFavorite();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (!changes.selection.currentValue.length) {
            this.favorites = [];
            return;
        }
        this.markFavoritesNodes(changes.selection.currentValue);
    }
    /**
     * @return {?}
     */
    toggleFavorite() {
        if (!this.favorites.length) {
            return;
        }
        /** @type {?} */
        const every = this.favorites.every((/**
         * @param {?} selected
         * @return {?}
         */
        (selected) => selected.entry.isFavorite));
        if (every) {
            /** @type {?} */
            const batch = this.favorites.map((/**
             * @param {?} selected
             * @return {?}
             */
            (selected) => {
                // shared files have nodeId
                /** @type {?} */
                const id = ((/** @type {?} */ (selected))).entry.nodeId || selected.entry.id;
                return from(this.alfrescoApiService.favoritesApi.removeFavoriteSite('-me-', id));
            }));
            forkJoin(batch).subscribe((/**
             * @return {?}
             */
            () => {
                this.favorites.map((/**
                 * @param {?} selected
                 * @return {?}
                 */
                (selected) => selected.entry.isFavorite = false));
                this.toggle.emit();
            }), (/**
             * @param {?} error
             * @return {?}
             */
            (error) => this.error.emit(error)));
        }
        if (!every) {
            /** @type {?} */
            const notFavorite = this.favorites.filter((/**
             * @param {?} node
             * @return {?}
             */
            (node) => !node.entry.isFavorite));
            /** @type {?} */
            const body = notFavorite.map((/**
             * @param {?} node
             * @return {?}
             */
            (node) => this.createFavoriteBody(node)));
            from(this.alfrescoApiService.favoritesApi.addFavorite('-me-', (/** @type {?} */ (body))))
                .subscribe((/**
             * @return {?}
             */
            () => {
                notFavorite.map((/**
                 * @param {?} selected
                 * @return {?}
                 */
                (selected) => selected.entry.isFavorite = true));
                this.toggle.emit();
            }), (/**
             * @param {?} error
             * @return {?}
             */
            (error) => this.error.emit(error)));
        }
    }
    /**
     * @param {?} selection
     * @return {?}
     */
    markFavoritesNodes(selection) {
        if (selection.length <= this.favorites.length) {
            /** @type {?} */
            const newFavorites = this.reduce(this.favorites, selection);
            this.favorites = newFavorites;
        }
        /** @type {?} */
        const result = this.diff(selection, this.favorites);
        /** @type {?} */
        const batch = this.getProcessBatch(result);
        forkJoin(batch).subscribe((/**
         * @param {?} data
         * @return {?}
         */
        (data) => {
            this.favorites.push(...data);
        }));
    }
    /**
     * @return {?}
     */
    hasFavorites() {
        if (this.favorites && !this.favorites.length) {
            return false;
        }
        return this.favorites.every((/**
         * @param {?} selected
         * @return {?}
         */
        (selected) => selected.entry.isFavorite));
    }
    /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    getProcessBatch(selection) {
        return selection.map((/**
         * @param {?} selected
         * @return {?}
         */
        (selected) => this.getFavorite(selected)));
    }
    /**
     * @private
     * @param {?} selected
     * @return {?}
     */
    getFavorite(selected) {
        /** @type {?} */
        const node = selected.entry;
        // ACS 6.x with 'isFavorite' include
        if (node && node.hasOwnProperty('isFavorite')) {
            return of(selected);
        }
        // ACS 5.x and 6.x without 'isFavorite' include
        const { name, isFile, isFolder } = (/** @type {?} */ (node));
        /** @type {?} */
        const id = ((/** @type {?} */ (node))).nodeId || node.id;
        /** @type {?} */
        const promise = this.alfrescoApiService.favoritesApi.getFavorite('-me-', id);
        return from(promise).pipe(map((/**
         * @return {?}
         */
        () => ({
            entry: {
                id,
                isFolder,
                isFile,
                name,
                isFavorite: true
            }
        }))), catchError((/**
         * @return {?}
         */
        () => {
            return of({
                entry: {
                    id,
                    isFolder,
                    isFile,
                    name,
                    isFavorite: false
                }
            });
        })));
    }
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    createFavoriteBody(node) {
        /** @type {?} */
        const type = this.getNodeType(node);
        // shared files have nodeId
        /** @type {?} */
        const id = node.entry.nodeId || node.entry.id;
        return {
            target: {
                [type]: {
                    guid: id
                }
            }
        };
    }
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    getNodeType(node) {
        // shared could only be files
        if (!node.entry.isFile && !node.entry.isFolder) {
            return 'file';
        }
        return node.entry.isFile ? 'file' : 'folder';
    }
    /**
     * @private
     * @param {?} list
     * @param {?} patch
     * @return {?}
     */
    diff(list, patch) {
        /** @type {?} */
        const ids = patch.map((/**
         * @param {?} item
         * @return {?}
         */
        (item) => item.entry.id));
        return list.filter((/**
         * @param {?} item
         * @return {?}
         */
        (item) => ids.includes(item.entry.id) ? null : item));
    }
    /**
     * @private
     * @param {?} patch
     * @param {?} comparator
     * @return {?}
     */
    reduce(patch, comparator) {
        /** @type {?} */
        const ids = comparator.map((/**
         * @param {?} item
         * @return {?}
         */
        (item) => item.entry.id));
        return patch.filter((/**
         * @param {?} item
         * @return {?}
         */
        (item) => ids.includes(item.entry.id) ? item : null));
    }
}
NodeFavoriteDirective.decorators = [
    { type: Directive, args: [{
                selector: '[adf-node-favorite]',
                exportAs: 'adfFavorite'
            },] }
];
/** @nocollapse */
NodeFavoriteDirective.ctorParameters = () => [
    { type: AlfrescoApiService }
];
NodeFavoriteDirective.propDecorators = {
    selection: [{ type: Input, args: ['adf-node-favorite',] }],
    toggle: [{ type: Output }],
    error: [{ type: Output }],
    onClick: [{ type: HostListener, args: ['click',] }]
};
if (false) {
    /** @type {?} */
    NodeFavoriteDirective.prototype.favorites;
    /**
     * Array of nodes to toggle as favorites.
     * @type {?}
     */
    NodeFavoriteDirective.prototype.selection;
    /**
     * Emitted when the favorite setting is complete.
     * @type {?}
     */
    NodeFavoriteDirective.prototype.toggle;
    /**
     * Emitted when the favorite setting fails.
     * @type {?}
     */
    NodeFavoriteDirective.prototype.error;
    /**
     * @type {?}
     * @private
     */
    NodeFavoriteDirective.prototype.alfrescoApiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9kZS1mYXZvcml0ZS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJkaXJlY3RpdmVzL25vZGUtZmF2b3JpdGUuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFhLE1BQU0sRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVoRyxPQUFPLEVBQWMsSUFBSSxFQUFFLFFBQVEsRUFBRSxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDdEQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDdEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQU1qRCxNQUFNLE9BQU8scUJBQXFCOzs7O0lBa0I5QixZQUFvQixrQkFBc0M7UUFBdEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQWpCMUQsY0FBUyxHQUFVLEVBQUUsQ0FBQzs7OztRQUl0QixjQUFTLEdBQWdCLEVBQUUsQ0FBQzs7OztRQUdsQixXQUFNLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7Ozs7UUFHL0MsVUFBSyxHQUFzQixJQUFJLFlBQVksRUFBRSxDQUFDO0lBUXhELENBQUM7Ozs7SUFMRCxPQUFPO1FBQ0gsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQzFCLENBQUM7Ozs7O0lBS0QsV0FBVyxDQUFDLE9BQU87UUFDZixJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFO1lBQ3hDLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1lBRXBCLE9BQU87U0FDVjtRQUVELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQzVELENBQUM7Ozs7SUFFRCxjQUFjO1FBQ1YsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFO1lBQ3hCLE9BQU87U0FDVjs7Y0FFSyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLOzs7O1FBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFDO1FBRTNFLElBQUksS0FBSyxFQUFFOztrQkFDRCxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHOzs7O1lBQUMsQ0FBQyxRQUFxQyxFQUFFLEVBQUU7OztzQkFFakUsRUFBRSxHQUFHLENBQUMsbUJBQWtCLFFBQVEsRUFBQSxDQUFDLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBRXpFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsa0JBQWtCLENBQUMsTUFBTSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDckYsQ0FBQyxFQUFDO1lBRUYsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVM7OztZQUNyQixHQUFHLEVBQUU7Z0JBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHOzs7O2dCQUFDLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFVBQVUsR0FBRyxLQUFLLEVBQUMsQ0FBQztnQkFDcEUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUN2QixDQUFDOzs7O1lBQ0QsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUNwQyxDQUFDO1NBQ0w7UUFFRCxJQUFJLENBQUMsS0FBSyxFQUFFOztrQkFDRixXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNOzs7O1lBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUM7O2tCQUNyRSxJQUFJLEdBQW1CLFdBQVcsQ0FBQyxHQUFHOzs7O1lBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsRUFBQztZQUVyRixJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsTUFBTSxFQUFFLG1CQUFNLElBQUksRUFBQSxDQUFDLENBQUM7aUJBQ3JFLFNBQVM7OztZQUNOLEdBQUcsRUFBRTtnQkFDRCxXQUFXLENBQUMsR0FBRzs7OztnQkFBQyxDQUFDLFFBQVEsRUFBRSxFQUFFLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsSUFBSSxFQUFDLENBQUM7Z0JBQ2hFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDdkIsQ0FBQzs7OztZQUNELENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFDcEMsQ0FBQztTQUNUO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxrQkFBa0IsQ0FBQyxTQUFzQjtRQUNyQyxJQUFJLFNBQVMsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUU7O2tCQUNyQyxZQUFZLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFNBQVMsQ0FBQztZQUMzRCxJQUFJLENBQUMsU0FBUyxHQUFHLFlBQVksQ0FBQztTQUNqQzs7Y0FFSyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQzs7Y0FDN0MsS0FBSyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDO1FBRTFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTtZQUMvQixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxHQUFHLElBQUksQ0FBQyxDQUFDO1FBQ2pDLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELFlBQVk7UUFDUixJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRTtZQUMxQyxPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUVELE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLOzs7O1FBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFDLENBQUM7SUFDekUsQ0FBQzs7Ozs7O0lBRU8sZUFBZSxDQUFDLFNBQVM7UUFDN0IsT0FBTyxTQUFTLENBQUMsR0FBRzs7OztRQUFDLENBQUMsUUFBbUIsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLENBQUMsRUFBQyxDQUFDO0lBQzlFLENBQUM7Ozs7OztJQUVPLFdBQVcsQ0FBQyxRQUFxQzs7Y0FDL0MsSUFBSSxHQUFzQixRQUFRLENBQUMsS0FBSztRQUU5QyxvQ0FBb0M7UUFDcEMsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsRUFBRTtZQUMzQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUN2Qjs7Y0FHSyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFFLEdBQUcsbUJBQU8sSUFBSSxFQUFBOztjQUN4QyxFQUFFLEdBQUksQ0FBQyxtQkFBYSxJQUFJLEVBQUEsQ0FBQyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsRUFBRTs7Y0FFM0MsT0FBTyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUM7UUFFNUUsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUNyQixHQUFHOzs7UUFBQyxHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBQ1AsS0FBSyxFQUFFO2dCQUNILEVBQUU7Z0JBQ0YsUUFBUTtnQkFDUixNQUFNO2dCQUNOLElBQUk7Z0JBQ0osVUFBVSxFQUFFLElBQUk7YUFDbkI7U0FDSixDQUFDLEVBQUMsRUFDSCxVQUFVOzs7UUFBQyxHQUFHLEVBQUU7WUFDWixPQUFPLEVBQUUsQ0FBQztnQkFDTixLQUFLLEVBQUU7b0JBQ0gsRUFBRTtvQkFDRixRQUFRO29CQUNSLE1BQU07b0JBQ04sSUFBSTtvQkFDSixVQUFVLEVBQUUsS0FBSztpQkFDcEI7YUFDSixDQUFDLENBQUM7UUFDUCxDQUFDLEVBQUMsQ0FDTCxDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBRU8sa0JBQWtCLENBQUMsSUFBSTs7Y0FDckIsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDOzs7Y0FFN0IsRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtRQUU3QyxPQUFPO1lBQ0gsTUFBTSxFQUFFO2dCQUNKLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ0osSUFBSSxFQUFFLEVBQUU7aUJBQ1g7YUFDSjtTQUNKLENBQUM7SUFDTixDQUFDOzs7Ozs7SUFFTyxXQUFXLENBQUMsSUFBSTtRQUNwQiw2QkFBNkI7UUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUU7WUFDNUMsT0FBTyxNQUFNLENBQUM7U0FDakI7UUFFRCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztJQUNqRCxDQUFDOzs7Ozs7O0lBRU8sSUFBSSxDQUFDLElBQUksRUFBRSxLQUFLOztjQUNkLEdBQUcsR0FBRyxLQUFLLENBQUMsR0FBRzs7OztRQUFDLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBQztRQUU5QyxPQUFPLElBQUksQ0FBQyxNQUFNOzs7O1FBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLEVBQUMsQ0FBQztJQUM1RSxDQUFDOzs7Ozs7O0lBRU8sTUFBTSxDQUFDLEtBQUssRUFBRSxVQUFVOztjQUN0QixHQUFHLEdBQUcsVUFBVSxDQUFDLEdBQUc7Ozs7UUFBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUM7UUFFbkQsT0FBTyxLQUFLLENBQUMsTUFBTTs7OztRQUFDLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxFQUFDLENBQUM7SUFDN0UsQ0FBQzs7O1lBM0tKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUscUJBQXFCO2dCQUMvQixRQUFRLEVBQUUsYUFBYTthQUMxQjs7OztZQU5RLGtCQUFrQjs7O3dCQVd0QixLQUFLLFNBQUMsbUJBQW1CO3FCQUl6QixNQUFNO29CQUdOLE1BQU07c0JBRU4sWUFBWSxTQUFDLE9BQU87Ozs7SUFackIsMENBQXNCOzs7OztJQUd0QiwwQ0FDNEI7Ozs7O0lBRzVCLHVDQUF5RDs7Ozs7SUFHekQsc0NBQXdEOzs7OztJQU81QyxtREFBOEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6bm8taW5wdXQtcmVuYW1lICAqL1xyXG5cclxuaW1wb3J0IHsgRGlyZWN0aXZlLCBFdmVudEVtaXR0ZXIsIEhvc3RMaXN0ZW5lciwgSW5wdXQsIE9uQ2hhbmdlcywgT3V0cHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZhdm9yaXRlQm9keSwgTm9kZUVudHJ5LCBTaGFyZWRMaW5rRW50cnksIE5vZGUsIFNoYXJlZExpbmsgfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgZnJvbSwgZm9ya0pvaW4sIG9mIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1thZGYtbm9kZS1mYXZvcml0ZV0nLFxyXG4gICAgZXhwb3J0QXM6ICdhZGZGYXZvcml0ZSdcclxufSlcclxuZXhwb3J0IGNsYXNzIE5vZGVGYXZvcml0ZURpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XHJcbiAgICBmYXZvcml0ZXM6IGFueVtdID0gW107XHJcblxyXG4gICAgLyoqIEFycmF5IG9mIG5vZGVzIHRvIHRvZ2dsZSBhcyBmYXZvcml0ZXMuICovXHJcbiAgICBASW5wdXQoJ2FkZi1ub2RlLWZhdm9yaXRlJylcclxuICAgIHNlbGVjdGlvbjogTm9kZUVudHJ5W10gPSBbXTtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSBmYXZvcml0ZSBzZXR0aW5nIGlzIGNvbXBsZXRlLiAqL1xyXG4gICAgQE91dHB1dCgpIHRvZ2dsZTogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblxyXG4gICAgLyoqIEVtaXR0ZWQgd2hlbiB0aGUgZmF2b3JpdGUgc2V0dGluZyBmYWlscy4gKi9cclxuICAgIEBPdXRwdXQoKSBlcnJvcjogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyKCk7XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignY2xpY2snKVxyXG4gICAgb25DbGljaygpIHtcclxuICAgICAgICB0aGlzLnRvZ2dsZUZhdm9yaXRlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhbGZyZXNjb0FwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXMpIHtcclxuICAgICAgICBpZiAoIWNoYW5nZXMuc2VsZWN0aW9uLmN1cnJlbnRWYWx1ZS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgdGhpcy5mYXZvcml0ZXMgPSBbXTtcclxuXHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMubWFya0Zhdm9yaXRlc05vZGVzKGNoYW5nZXMuc2VsZWN0aW9uLmN1cnJlbnRWYWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgdG9nZ2xlRmF2b3JpdGUoKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLmZhdm9yaXRlcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgZXZlcnkgPSB0aGlzLmZhdm9yaXRlcy5ldmVyeSgoc2VsZWN0ZWQpID0+IHNlbGVjdGVkLmVudHJ5LmlzRmF2b3JpdGUpO1xyXG5cclxuICAgICAgICBpZiAoZXZlcnkpIHtcclxuICAgICAgICAgICAgY29uc3QgYmF0Y2ggPSB0aGlzLmZhdm9yaXRlcy5tYXAoKHNlbGVjdGVkOiBOb2RlRW50cnkgfCBTaGFyZWRMaW5rRW50cnkpID0+IHtcclxuICAgICAgICAgICAgICAgIC8vIHNoYXJlZCBmaWxlcyBoYXZlIG5vZGVJZFxyXG4gICAgICAgICAgICAgICAgY29uc3QgaWQgPSAoPFNoYXJlZExpbmtFbnRyeT4gc2VsZWN0ZWQpLmVudHJ5Lm5vZGVJZCB8fCBzZWxlY3RlZC5lbnRyeS5pZDtcclxuXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5mYXZvcml0ZXNBcGkucmVtb3ZlRmF2b3JpdGVTaXRlKCctbWUtJywgaWQpKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBmb3JrSm9pbihiYXRjaCkuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmF2b3JpdGVzLm1hcCgoc2VsZWN0ZWQpID0+IHNlbGVjdGVkLmVudHJ5LmlzRmF2b3JpdGUgPSBmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy50b2dnbGUuZW1pdCgpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIChlcnJvcikgPT4gdGhpcy5lcnJvci5lbWl0KGVycm9yKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCFldmVyeSkge1xyXG4gICAgICAgICAgICBjb25zdCBub3RGYXZvcml0ZSA9IHRoaXMuZmF2b3JpdGVzLmZpbHRlcigobm9kZSkgPT4gIW5vZGUuZW50cnkuaXNGYXZvcml0ZSk7XHJcbiAgICAgICAgICAgIGNvbnN0IGJvZHk6IEZhdm9yaXRlQm9keVtdID0gbm90RmF2b3JpdGUubWFwKChub2RlKSA9PiB0aGlzLmNyZWF0ZUZhdm9yaXRlQm9keShub2RlKSk7XHJcblxyXG4gICAgICAgICAgICBmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmZhdm9yaXRlc0FwaS5hZGRGYXZvcml0ZSgnLW1lLScsIDxhbnk+IGJvZHkpKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgICAgICAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5vdEZhdm9yaXRlLm1hcCgoc2VsZWN0ZWQpID0+IHNlbGVjdGVkLmVudHJ5LmlzRmF2b3JpdGUgPSB0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50b2dnbGUuZW1pdCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgKGVycm9yKSA9PiB0aGlzLmVycm9yLmVtaXQoZXJyb3IpXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBtYXJrRmF2b3JpdGVzTm9kZXMoc2VsZWN0aW9uOiBOb2RlRW50cnlbXSkge1xyXG4gICAgICAgIGlmIChzZWxlY3Rpb24ubGVuZ3RoIDw9IHRoaXMuZmF2b3JpdGVzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICBjb25zdCBuZXdGYXZvcml0ZXMgPSB0aGlzLnJlZHVjZSh0aGlzLmZhdm9yaXRlcywgc2VsZWN0aW9uKTtcclxuICAgICAgICAgICAgdGhpcy5mYXZvcml0ZXMgPSBuZXdGYXZvcml0ZXM7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCByZXN1bHQgPSB0aGlzLmRpZmYoc2VsZWN0aW9uLCB0aGlzLmZhdm9yaXRlcyk7XHJcbiAgICAgICAgY29uc3QgYmF0Y2ggPSB0aGlzLmdldFByb2Nlc3NCYXRjaChyZXN1bHQpO1xyXG5cclxuICAgICAgICBmb3JrSm9pbihiYXRjaCkuc3Vic2NyaWJlKChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZmF2b3JpdGVzLnB1c2goLi4uZGF0YSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgaGFzRmF2b3JpdGVzKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLmZhdm9yaXRlcyAmJiAhdGhpcy5mYXZvcml0ZXMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLmZhdm9yaXRlcy5ldmVyeSgoc2VsZWN0ZWQpID0+IHNlbGVjdGVkLmVudHJ5LmlzRmF2b3JpdGUpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0UHJvY2Vzc0JhdGNoKHNlbGVjdGlvbik6IGFueVtdIHtcclxuICAgICAgICByZXR1cm4gc2VsZWN0aW9uLm1hcCgoc2VsZWN0ZWQ6IE5vZGVFbnRyeSkgPT4gdGhpcy5nZXRGYXZvcml0ZShzZWxlY3RlZCkpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0RmF2b3JpdGUoc2VsZWN0ZWQ6IE5vZGVFbnRyeSB8IFNoYXJlZExpbmtFbnRyeSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3Qgbm9kZTogTm9kZSB8IFNoYXJlZExpbmsgPSBzZWxlY3RlZC5lbnRyeTtcclxuXHJcbiAgICAgICAgLy8gQUNTIDYueCB3aXRoICdpc0Zhdm9yaXRlJyBpbmNsdWRlXHJcbiAgICAgICAgaWYgKG5vZGUgJiYgbm9kZS5oYXNPd25Qcm9wZXJ0eSgnaXNGYXZvcml0ZScpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBvZihzZWxlY3RlZCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBBQ1MgNS54IGFuZCA2Lnggd2l0aG91dCAnaXNGYXZvcml0ZScgaW5jbHVkZVxyXG4gICAgICAgIGNvbnN0IHsgbmFtZSwgaXNGaWxlLCBpc0ZvbGRlciB9ID0gPE5vZGU+IG5vZGU7XHJcbiAgICAgICAgY29uc3QgaWQgPSAgKDxTaGFyZWRMaW5rPiBub2RlKS5ub2RlSWQgfHwgbm9kZS5pZDtcclxuXHJcbiAgICAgICAgY29uc3QgcHJvbWlzZSA9IHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmZhdm9yaXRlc0FwaS5nZXRGYXZvcml0ZSgnLW1lLScsIGlkKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20ocHJvbWlzZSkucGlwZShcclxuICAgICAgICAgICAgbWFwKCgpID0+ICh7XHJcbiAgICAgICAgICAgICAgICBlbnRyeToge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzRm9sZGVyLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzRmlsZSxcclxuICAgICAgICAgICAgICAgICAgICBuYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzRmF2b3JpdGU6IHRydWVcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSkpLFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBvZih7XHJcbiAgICAgICAgICAgICAgICAgICAgZW50cnk6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzRm9sZGVyLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0ZpbGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzRmF2b3JpdGU6IGZhbHNlXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNyZWF0ZUZhdm9yaXRlQm9keShub2RlKTogRmF2b3JpdGVCb2R5IHtcclxuICAgICAgICBjb25zdCB0eXBlID0gdGhpcy5nZXROb2RlVHlwZShub2RlKTtcclxuICAgICAgICAvLyBzaGFyZWQgZmlsZXMgaGF2ZSBub2RlSWRcclxuICAgICAgICBjb25zdCBpZCA9IG5vZGUuZW50cnkubm9kZUlkIHx8IG5vZGUuZW50cnkuaWQ7XHJcblxyXG4gICAgICAgIHJldHVybiB7XHJcbiAgICAgICAgICAgIHRhcmdldDoge1xyXG4gICAgICAgICAgICAgICAgW3R5cGVdOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZ3VpZDogaWRcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXROb2RlVHlwZShub2RlKTogc3RyaW5nIHtcclxuICAgICAgICAvLyBzaGFyZWQgY291bGQgb25seSBiZSBmaWxlc1xyXG4gICAgICAgIGlmICghbm9kZS5lbnRyeS5pc0ZpbGUgJiYgIW5vZGUuZW50cnkuaXNGb2xkZXIpIHtcclxuICAgICAgICAgICAgcmV0dXJuICdmaWxlJztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBub2RlLmVudHJ5LmlzRmlsZSA/ICdmaWxlJyA6ICdmb2xkZXInO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZGlmZihsaXN0LCBwYXRjaCk6IGFueVtdIHtcclxuICAgICAgICBjb25zdCBpZHMgPSBwYXRjaC5tYXAoKGl0ZW0pID0+IGl0ZW0uZW50cnkuaWQpO1xyXG5cclxuICAgICAgICByZXR1cm4gbGlzdC5maWx0ZXIoKGl0ZW0pID0+IGlkcy5pbmNsdWRlcyhpdGVtLmVudHJ5LmlkKSA/IG51bGwgOiBpdGVtKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHJlZHVjZShwYXRjaCwgY29tcGFyYXRvcik6IGFueVtdIHtcclxuICAgICAgICBjb25zdCBpZHMgPSBjb21wYXJhdG9yLm1hcCgoaXRlbSkgPT4gaXRlbS5lbnRyeS5pZCk7XHJcblxyXG4gICAgICAgIHJldHVybiBwYXRjaC5maWx0ZXIoKGl0ZW0pID0+IGlkcy5pbmNsdWRlcyhpdGVtLmVudHJ5LmlkKSA/IGl0ZW0gOiBudWxsKTtcclxuICAgIH1cclxufVxyXG4iXX0=