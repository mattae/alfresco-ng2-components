/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector no-input-rename */
import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { forkJoin, from, of } from 'rxjs';
import { AlfrescoApiService } from '../services/alfresco-api.service';
import { TranslationService } from '../services/translation.service';
import { tap, mergeMap, map, catchError } from 'rxjs/operators';
export class RestoreMessageModel {
}
if (false) {
    /** @type {?} */
    RestoreMessageModel.prototype.message;
    /** @type {?} */
    RestoreMessageModel.prototype.path;
    /** @type {?} */
    RestoreMessageModel.prototype.action;
}
export class NodeRestoreDirective {
    /**
     * @param {?} alfrescoApiService
     * @param {?} translation
     */
    constructor(alfrescoApiService, translation) {
        this.alfrescoApiService = alfrescoApiService;
        this.translation = translation;
        /**
         * Emitted when restoration is complete.
         */
        this.restore = new EventEmitter();
        this.restoreProcessStatus = this.processStatus();
    }
    /**
     * @return {?}
     */
    onClick() {
        this.recover(this.selection);
    }
    /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    recover(selection) {
        if (!selection.length) {
            return;
        }
        /** @type {?} */
        const nodesWithPath = this.getNodesWithPath(selection);
        if (selection.length && nodesWithPath.length) {
            this.restoreNodesBatch(nodesWithPath).pipe(tap((/**
             * @param {?} restoredNodes
             * @return {?}
             */
            (restoredNodes) => {
                /** @type {?} */
                const status = this.processStatus(restoredNodes);
                this.restoreProcessStatus.fail.push(...status.fail);
                this.restoreProcessStatus.success.push(...status.success);
            })), mergeMap((/**
             * @return {?}
             */
            () => this.getDeletedNodes())))
                .subscribe((/**
             * @param {?} deletedNodesList
             * @return {?}
             */
            (deletedNodesList) => {
                const { entries: nodeList } = deletedNodesList.list;
                const { fail: restoreErrorNodes } = this.restoreProcessStatus;
                /** @type {?} */
                const selectedNodes = this.diff(restoreErrorNodes, selection, false);
                /** @type {?} */
                const remainingNodes = this.diff(selectedNodes, nodeList);
                if (!remainingNodes.length) {
                    this.notification();
                }
                else {
                    this.recover(remainingNodes);
                }
            }));
        }
        else {
            this.restoreProcessStatus.fail.push(...selection);
            this.notification();
            return;
        }
    }
    /**
     * @private
     * @param {?} batch
     * @return {?}
     */
    restoreNodesBatch(batch) {
        return forkJoin(batch.map((/**
         * @param {?} node
         * @return {?}
         */
        (node) => this.restoreNode(node))));
    }
    /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    getNodesWithPath(selection) {
        return selection.filter((/**
         * @param {?} node
         * @return {?}
         */
        (node) => node.entry.path));
    }
    /**
     * @private
     * @return {?}
     */
    getDeletedNodes() {
        /** @type {?} */
        const promise = this.alfrescoApiService.getInstance()
            .core.nodesApi.getDeletedNodes({ include: ['path'] });
        return from(promise);
    }
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    restoreNode(node) {
        const { entry } = node;
        /** @type {?} */
        const promise = this.alfrescoApiService.getInstance().nodes.restoreNode(entry.id);
        return from(promise).pipe(map((/**
         * @return {?}
         */
        () => ({
            status: 1,
            entry
        }))), catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => {
            const { statusCode } = (JSON.parse(error.message)).error;
            return of({
                status: 0,
                statusCode,
                entry
            });
        })));
    }
    /**
     * @private
     * @param {?} selection
     * @param {?} list
     * @param {?=} fromList
     * @return {?}
     */
    diff(selection, list, fromList = true) {
        /** @type {?} */
        const ids = selection.map((/**
         * @param {?} item
         * @return {?}
         */
        (item) => item.entry.id));
        return list.filter((/**
         * @param {?} item
         * @return {?}
         */
        (item) => {
            if (fromList) {
                return ids.includes(item.entry.id) ? item : null;
            }
            else {
                return !ids.includes(item.entry.id) ? item : null;
            }
        }));
    }
    /**
     * @private
     * @param {?=} data
     * @return {?}
     */
    processStatus(data = []) {
        /** @type {?} */
        const status = {
            fail: [],
            success: [],
            /**
             * @return {?}
             */
            get someFailed() {
                return !!(this.fail.length);
            },
            /**
             * @return {?}
             */
            get someSucceeded() {
                return !!(this.success.length);
            },
            /**
             * @return {?}
             */
            get oneFailed() {
                return this.fail.length === 1;
            },
            /**
             * @return {?}
             */
            get oneSucceeded() {
                return this.success.length === 1;
            },
            /**
             * @return {?}
             */
            get allSucceeded() {
                return this.someSucceeded && !this.someFailed;
            },
            /**
             * @return {?}
             */
            get allFailed() {
                return this.someFailed && !this.someSucceeded;
            },
            /**
             * @return {?}
             */
            reset() {
                this.fail = [];
                this.success = [];
            }
        };
        return data.reduce((/**
         * @param {?} acc
         * @param {?} node
         * @return {?}
         */
        (acc, node) => {
            if (node.status) {
                acc.success.push(node);
            }
            else {
                acc.fail.push(node);
            }
            return acc;
        }), status);
    }
    /**
     * @private
     * @return {?}
     */
    getRestoreMessage() {
        const { restoreProcessStatus: status } = this;
        if (status.someFailed && !status.oneFailed) {
            return this.translation.instant('CORE.RESTORE_NODE.PARTIAL_PLURAL', {
                number: status.fail.length
            });
        }
        if (status.oneFailed && status.fail[0].statusCode) {
            if (status.fail[0].statusCode === 409) {
                return this.translation.instant('CORE.RESTORE_NODE.NODE_EXISTS', {
                    name: status.fail[0].entry.name
                });
            }
            else {
                return this.translation.instant('CORE.RESTORE_NODE.GENERIC', {
                    name: status.fail[0].entry.name
                });
            }
        }
        if (status.oneFailed && !status.fail[0].statusCode) {
            return this.translation.instant('CORE.RESTORE_NODE.LOCATION_MISSING', {
                name: status.fail[0].entry.name
            });
        }
        if (status.allSucceeded && !status.oneSucceeded) {
            return this.translation.instant('CORE.RESTORE_NODE.PLURAL');
        }
        if (status.allSucceeded && status.oneSucceeded) {
            return this.translation.instant('CORE.RESTORE_NODE.SINGULAR', {
                name: status.success[0].entry.name
            });
        }
    }
    /**
     * @private
     * @return {?}
     */
    notification() {
        /** @type {?} */
        const status = Object.assign({}, this.restoreProcessStatus);
        /** @type {?} */
        const message = this.getRestoreMessage();
        this.reset();
        /** @type {?} */
        const action = (status.oneSucceeded && !status.someFailed) ? this.translation.instant('CORE.RESTORE_NODE.VIEW') : '';
        /** @type {?} */
        let path;
        if (status.success && status.success.length > 0) {
            path = status.success[0].entry.path;
        }
        this.restore.emit({
            message: message,
            action: action,
            path: path
        });
    }
    /**
     * @private
     * @return {?}
     */
    reset() {
        this.restoreProcessStatus.reset();
        this.selection = [];
    }
}
NodeRestoreDirective.decorators = [
    { type: Directive, args: [{
                selector: '[adf-restore]'
            },] }
];
/** @nocollapse */
NodeRestoreDirective.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: TranslationService }
];
NodeRestoreDirective.propDecorators = {
    selection: [{ type: Input, args: ['adf-restore',] }],
    restore: [{ type: Output }],
    onClick: [{ type: HostListener, args: ['click',] }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    NodeRestoreDirective.prototype.restoreProcessStatus;
    /**
     * Array of deleted nodes to restore.
     * @type {?}
     */
    NodeRestoreDirective.prototype.selection;
    /**
     * Emitted when restoration is complete.
     * @type {?}
     */
    NodeRestoreDirective.prototype.restore;
    /**
     * @type {?}
     * @private
     */
    NodeRestoreDirective.prototype.alfrescoApiService;
    /**
     * @type {?}
     * @private
     */
    NodeRestoreDirective.prototype.translation;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9kZS1yZXN0b3JlLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvbm9kZS1yZXN0b3JlLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFckYsT0FBTyxFQUFjLFFBQVEsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3RELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3RFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUVoRSxNQUFNLE9BQU8sbUJBQW1CO0NBSS9COzs7SUFIRyxzQ0FBZ0I7O0lBQ2hCLG1DQUFxQjs7SUFDckIscUNBQWU7O0FBTW5CLE1BQU0sT0FBTyxvQkFBb0I7Ozs7O0lBZ0I3QixZQUFvQixrQkFBc0MsRUFDdEMsV0FBK0I7UUFEL0IsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxnQkFBVyxHQUFYLFdBQVcsQ0FBb0I7Ozs7UUFSbkQsWUFBTyxHQUFzQyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBUzVELElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDckQsQ0FBQzs7OztJQVBELE9BQU87UUFDSCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7Ozs7SUFPTyxPQUFPLENBQUMsU0FBYztRQUMxQixJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRTtZQUNuQixPQUFPO1NBQ1Y7O2NBRUssYUFBYSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUM7UUFFdEQsSUFBSSxTQUFTLENBQUMsTUFBTSxJQUFJLGFBQWEsQ0FBQyxNQUFNLEVBQUU7WUFFMUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGFBQWEsQ0FBQyxDQUFDLElBQUksQ0FDdEMsR0FBRzs7OztZQUFDLENBQUMsYUFBYSxFQUFFLEVBQUU7O3NCQUNaLE1BQU0sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQztnQkFFaEQsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ3BELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzlELENBQUMsRUFBQyxFQUNGLFFBQVE7OztZQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsRUFBQyxDQUN6QztpQkFDQSxTQUFTOzs7O1lBQUMsQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFO3NCQUN0QixFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsR0FBRyxnQkFBZ0IsQ0FBQyxJQUFJO3NCQUM3QyxFQUFFLElBQUksRUFBRSxpQkFBaUIsRUFBRSxHQUFHLElBQUksQ0FBQyxvQkFBb0I7O3NCQUN2RCxhQUFhLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxTQUFTLEVBQUUsS0FBSyxDQUFDOztzQkFDOUQsY0FBYyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxFQUFFLFFBQVEsQ0FBQztnQkFFekQsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUU7b0JBQ3hCLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztpQkFDdkI7cUJBQU07b0JBQ0gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsQ0FBQztpQkFDaEM7WUFDTCxDQUFDLEVBQUMsQ0FBQztTQUNOO2FBQU07WUFDSCxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLFNBQVMsQ0FBQyxDQUFDO1lBQ2xELElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztZQUNwQixPQUFPO1NBQ1Y7SUFDTCxDQUFDOzs7Ozs7SUFFTyxpQkFBaUIsQ0FBQyxLQUF5QjtRQUMvQyxPQUFPLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRzs7OztRQUFDLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUFDLENBQUMsQ0FBQztJQUNqRSxDQUFDOzs7Ozs7SUFFTyxnQkFBZ0IsQ0FBQyxTQUFTO1FBQzlCLE9BQU8sU0FBUyxDQUFDLE1BQU07Ozs7UUFBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUMsQ0FBQztJQUN2RCxDQUFDOzs7OztJQUVPLGVBQWU7O2NBQ2IsT0FBTyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUU7YUFDaEQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsRUFBRSxPQUFPLEVBQUUsQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDO1FBRXpELE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3pCLENBQUM7Ozs7OztJQUVPLFdBQVcsQ0FBQyxJQUFJO2NBQ2QsRUFBRSxLQUFLLEVBQUUsR0FBRyxJQUFJOztjQUVoQixPQUFPLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQztRQUVqRixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQ3JCLEdBQUc7OztRQUFDLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDUCxNQUFNLEVBQUUsQ0FBQztZQUNULEtBQUs7U0FDUixDQUFDLEVBQUMsRUFDSCxVQUFVOzs7O1FBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTtrQkFDWCxFQUFFLFVBQVUsRUFBRSxHQUFHLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFLO1lBRXhELE9BQU8sRUFBRSxDQUFDO2dCQUNOLE1BQU0sRUFBRSxDQUFDO2dCQUNULFVBQVU7Z0JBQ1YsS0FBSzthQUNSLENBQUMsQ0FBQztRQUNQLENBQUMsRUFBQyxDQUNMLENBQUM7SUFDTixDQUFDOzs7Ozs7OztJQUVPLElBQUksQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLFFBQVEsR0FBRyxJQUFJOztjQUNuQyxHQUFHLEdBQUcsU0FBUyxDQUFDLEdBQUc7Ozs7UUFBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUM7UUFFbEQsT0FBTyxJQUFJLENBQUMsTUFBTTs7OztRQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7WUFDeEIsSUFBSSxRQUFRLEVBQUU7Z0JBQ1YsT0FBTyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2FBQ3BEO2lCQUFNO2dCQUNILE9BQU8sQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2FBQ3JEO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFFTyxhQUFhLENBQUMsSUFBSSxHQUFHLEVBQUU7O2NBQ3JCLE1BQU0sR0FBRztZQUNYLElBQUksRUFBRSxFQUFFO1lBQ1IsT0FBTyxFQUFFLEVBQUU7Ozs7WUFDWCxJQUFJLFVBQVU7Z0JBQ1YsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ2hDLENBQUM7Ozs7WUFDRCxJQUFJLGFBQWE7Z0JBQ2IsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ25DLENBQUM7Ozs7WUFDRCxJQUFJLFNBQVM7Z0JBQ1QsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUM7WUFDbEMsQ0FBQzs7OztZQUNELElBQUksWUFBWTtnQkFDWixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQztZQUNyQyxDQUFDOzs7O1lBQ0QsSUFBSSxZQUFZO2dCQUNaLE9BQU8sSUFBSSxDQUFDLGFBQWEsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDbEQsQ0FBQzs7OztZQUNELElBQUksU0FBUztnQkFDVCxPQUFPLElBQUksQ0FBQyxVQUFVLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDO1lBQ2xELENBQUM7Ozs7WUFDRCxLQUFLO2dCQUNELElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO2dCQUNmLElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO1lBQ3RCLENBQUM7U0FDSjtRQUVELE9BQU8sSUFBSSxDQUFDLE1BQU07Ozs7O1FBQ2QsQ0FBQyxHQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUU7WUFDVixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ2IsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDMUI7aUJBQU07Z0JBQ0gsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDdkI7WUFFRCxPQUFPLEdBQUcsQ0FBQztRQUNmLENBQUMsR0FDRCxNQUFNLENBQ1QsQ0FBQztJQUNOLENBQUM7Ozs7O0lBRU8saUJBQWlCO2NBQ2YsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLEVBQUUsR0FBRyxJQUFJO1FBRTdDLElBQUksTUFBTSxDQUFDLFVBQVUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUU7WUFDeEMsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FDM0Isa0NBQWtDLEVBQ2xDO2dCQUNJLE1BQU0sRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU07YUFDN0IsQ0FDSixDQUFDO1NBQ0w7UUFFRCxJQUFJLE1BQU0sQ0FBQyxTQUFTLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEVBQUU7WUFDL0MsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsS0FBSyxHQUFHLEVBQUU7Z0JBQ25DLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQzNCLCtCQUErQixFQUMvQjtvQkFDSSxJQUFJLEVBQUUsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSTtpQkFDbEMsQ0FDSixDQUFDO2FBQ0w7aUJBQU07Z0JBQ0gsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FDM0IsMkJBQTJCLEVBQzNCO29CQUNJLElBQUksRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJO2lCQUNsQyxDQUNKLENBQUM7YUFDTDtTQUNKO1FBRUQsSUFBSSxNQUFNLENBQUMsU0FBUyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVLEVBQUU7WUFDaEQsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FDM0Isb0NBQW9DLEVBQ3BDO2dCQUNJLElBQUksRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJO2FBQ2xDLENBQ0osQ0FBQztTQUNMO1FBRUQsSUFBSSxNQUFNLENBQUMsWUFBWSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRTtZQUM3QyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLDBCQUEwQixDQUFDLENBQUM7U0FDL0Q7UUFFRCxJQUFJLE1BQU0sQ0FBQyxZQUFZLElBQUksTUFBTSxDQUFDLFlBQVksRUFBRTtZQUM1QyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUMzQiw0QkFBNEIsRUFDNUI7Z0JBQ0ksSUFBSSxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUk7YUFDckMsQ0FDSixDQUFDO1NBQ0w7SUFDTCxDQUFDOzs7OztJQUVPLFlBQVk7O2NBQ1YsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQzs7Y0FFckQsT0FBTyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtRQUN4QyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7O2NBRVAsTUFBTSxHQUFHLENBQUMsTUFBTSxDQUFDLFlBQVksSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTs7WUFFaEgsSUFBSTtRQUNSLElBQUksTUFBTSxDQUFDLE9BQU8sSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDN0MsSUFBSSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztTQUN2QztRQUNELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ2QsT0FBTyxFQUFFLE9BQU87WUFDaEIsTUFBTSxFQUFFLE1BQU07WUFDZCxJQUFJLEVBQUUsSUFBSTtTQUNiLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRU8sS0FBSztRQUNULElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNsQyxJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN4QixDQUFDOzs7WUFuT0osU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxlQUFlO2FBQzVCOzs7O1lBWlEsa0JBQWtCO1lBQ2xCLGtCQUFrQjs7O3dCQWdCdEIsS0FBSyxTQUFDLGFBQWE7c0JBSW5CLE1BQU07c0JBR04sWUFBWSxTQUFDLE9BQU87Ozs7Ozs7SUFWckIsb0RBQTZCOzs7OztJQUc3Qix5Q0FDOEI7Ozs7O0lBRzlCLHVDQUNnRTs7Ozs7SUFPcEQsa0RBQThDOzs7OztJQUM5QywyQ0FBdUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yIG5vLWlucHV0LXJlbmFtZSAqL1xyXG5cclxuaW1wb3J0IHsgRGlyZWN0aXZlLCBFdmVudEVtaXR0ZXIsIEhvc3RMaXN0ZW5lciwgSW5wdXQsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEZWxldGVkTm9kZUVudHJ5LCBEZWxldGVkTm9kZXNQYWdpbmcsIFBhdGhJbmZvRW50aXR5IH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZvcmtKb2luLCBmcm9tLCBvZiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IFRyYW5zbGF0aW9uU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3RyYW5zbGF0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyB0YXAsIG1lcmdlTWFwLCBtYXAsIGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5leHBvcnQgY2xhc3MgUmVzdG9yZU1lc3NhZ2VNb2RlbCB7XHJcbiAgICBtZXNzYWdlOiBzdHJpbmc7XHJcbiAgICBwYXRoOiBQYXRoSW5mb0VudGl0eTtcclxuICAgIGFjdGlvbjogc3RyaW5nO1xyXG59XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW2FkZi1yZXN0b3JlXSdcclxufSlcclxuZXhwb3J0IGNsYXNzIE5vZGVSZXN0b3JlRGlyZWN0aXZlIHtcclxuICAgIHByaXZhdGUgcmVzdG9yZVByb2Nlc3NTdGF0dXM7XHJcblxyXG4gICAgLyoqIEFycmF5IG9mIGRlbGV0ZWQgbm9kZXMgdG8gcmVzdG9yZS4gKi9cclxuICAgIEBJbnB1dCgnYWRmLXJlc3RvcmUnKVxyXG4gICAgc2VsZWN0aW9uOiBEZWxldGVkTm9kZUVudHJ5W107XHJcblxyXG4gICAgLyoqIEVtaXR0ZWQgd2hlbiByZXN0b3JhdGlvbiBpcyBjb21wbGV0ZS4gKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgcmVzdG9yZTogRXZlbnRFbWl0dGVyPFJlc3RvcmVNZXNzYWdlTW9kZWw+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2NsaWNrJylcclxuICAgIG9uQ2xpY2soKSB7XHJcbiAgICAgICAgdGhpcy5yZWNvdmVyKHRoaXMuc2VsZWN0aW9uKTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFsZnJlc2NvQXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSB0cmFuc2xhdGlvbjogVHJhbnNsYXRpb25TZXJ2aWNlKSB7XHJcbiAgICAgICAgdGhpcy5yZXN0b3JlUHJvY2Vzc1N0YXR1cyA9IHRoaXMucHJvY2Vzc1N0YXR1cygpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgcmVjb3ZlcihzZWxlY3Rpb246IGFueSkge1xyXG4gICAgICAgIGlmICghc2VsZWN0aW9uLmxlbmd0aCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBub2Rlc1dpdGhQYXRoID0gdGhpcy5nZXROb2Rlc1dpdGhQYXRoKHNlbGVjdGlvbik7XHJcblxyXG4gICAgICAgIGlmIChzZWxlY3Rpb24ubGVuZ3RoICYmIG5vZGVzV2l0aFBhdGgubGVuZ3RoKSB7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnJlc3RvcmVOb2Rlc0JhdGNoKG5vZGVzV2l0aFBhdGgpLnBpcGUoXHJcbiAgICAgICAgICAgICAgICB0YXAoKHJlc3RvcmVkTm9kZXMpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBzdGF0dXMgPSB0aGlzLnByb2Nlc3NTdGF0dXMocmVzdG9yZWROb2Rlcyk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmVzdG9yZVByb2Nlc3NTdGF0dXMuZmFpbC5wdXNoKC4uLnN0YXR1cy5mYWlsKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJlc3RvcmVQcm9jZXNzU3RhdHVzLnN1Y2Nlc3MucHVzaCguLi5zdGF0dXMuc3VjY2Vzcyk7XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIG1lcmdlTWFwKCgpID0+IHRoaXMuZ2V0RGVsZXRlZE5vZGVzKCkpXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICAgLnN1YnNjcmliZSgoZGVsZXRlZE5vZGVzTGlzdCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgeyBlbnRyaWVzOiBub2RlTGlzdCB9ID0gZGVsZXRlZE5vZGVzTGlzdC5saXN0O1xyXG4gICAgICAgICAgICAgICAgY29uc3QgeyBmYWlsOiByZXN0b3JlRXJyb3JOb2RlcyB9ID0gdGhpcy5yZXN0b3JlUHJvY2Vzc1N0YXR1cztcclxuICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkTm9kZXMgPSB0aGlzLmRpZmYocmVzdG9yZUVycm9yTm9kZXMsIHNlbGVjdGlvbiwgZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcmVtYWluaW5nTm9kZXMgPSB0aGlzLmRpZmYoc2VsZWN0ZWROb2Rlcywgbm9kZUxpc3QpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmICghcmVtYWluaW5nTm9kZXMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb24oKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yZWNvdmVyKHJlbWFpbmluZ05vZGVzKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5yZXN0b3JlUHJvY2Vzc1N0YXR1cy5mYWlsLnB1c2goLi4uc2VsZWN0aW9uKTtcclxuICAgICAgICAgICAgdGhpcy5ub3RpZmljYXRpb24oKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHJlc3RvcmVOb2Rlc0JhdGNoKGJhdGNoOiBEZWxldGVkTm9kZUVudHJ5W10pOiBPYnNlcnZhYmxlPERlbGV0ZWROb2RlRW50cnlbXT4ge1xyXG4gICAgICAgIHJldHVybiBmb3JrSm9pbihiYXRjaC5tYXAoKG5vZGUpID0+IHRoaXMucmVzdG9yZU5vZGUobm9kZSkpKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldE5vZGVzV2l0aFBhdGgoc2VsZWN0aW9uKTogRGVsZXRlZE5vZGVFbnRyeVtdIHtcclxuICAgICAgICByZXR1cm4gc2VsZWN0aW9uLmZpbHRlcigobm9kZSkgPT4gbm9kZS5lbnRyeS5wYXRoKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldERlbGV0ZWROb2RlcygpOiBPYnNlcnZhYmxlPERlbGV0ZWROb2Rlc1BhZ2luZz4ge1xyXG4gICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5nZXRJbnN0YW5jZSgpXHJcbiAgICAgICAgICAgIC5jb3JlLm5vZGVzQXBpLmdldERlbGV0ZWROb2Rlcyh7IGluY2x1ZGU6IFsncGF0aCddIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gZnJvbShwcm9taXNlKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHJlc3RvcmVOb2RlKG5vZGUpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHsgZW50cnkgfSA9IG5vZGU7XHJcblxyXG4gICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLm5vZGVzLnJlc3RvcmVOb2RlKGVudHJ5LmlkKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20ocHJvbWlzZSkucGlwZShcclxuICAgICAgICAgICAgbWFwKCgpID0+ICh7XHJcbiAgICAgICAgICAgICAgICBzdGF0dXM6IDEsXHJcbiAgICAgICAgICAgICAgICBlbnRyeVxyXG4gICAgICAgICAgICB9KSksXHJcbiAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB7IHN0YXR1c0NvZGUgfSA9IChKU09OLnBhcnNlKGVycm9yLm1lc3NhZ2UpKS5lcnJvcjtcclxuXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gb2Yoe1xyXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1czogMCxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXNDb2RlLFxyXG4gICAgICAgICAgICAgICAgICAgIGVudHJ5XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZGlmZihzZWxlY3Rpb24sIGxpc3QsIGZyb21MaXN0ID0gdHJ1ZSk6IGFueSB7XHJcbiAgICAgICAgY29uc3QgaWRzID0gc2VsZWN0aW9uLm1hcCgoaXRlbSkgPT4gaXRlbS5lbnRyeS5pZCk7XHJcblxyXG4gICAgICAgIHJldHVybiBsaXN0LmZpbHRlcigoaXRlbSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAoZnJvbUxpc3QpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBpZHMuaW5jbHVkZXMoaXRlbS5lbnRyeS5pZCkgPyBpdGVtIDogbnVsbDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAhaWRzLmluY2x1ZGVzKGl0ZW0uZW50cnkuaWQpID8gaXRlbSA6IG51bGw7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHByb2Nlc3NTdGF0dXMoZGF0YSA9IFtdKTogYW55IHtcclxuICAgICAgICBjb25zdCBzdGF0dXMgPSB7XHJcbiAgICAgICAgICAgIGZhaWw6IFtdLFxyXG4gICAgICAgICAgICBzdWNjZXNzOiBbXSxcclxuICAgICAgICAgICAgZ2V0IHNvbWVGYWlsZWQoKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gISEodGhpcy5mYWlsLmxlbmd0aCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGdldCBzb21lU3VjY2VlZGVkKCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICEhKHRoaXMuc3VjY2Vzcy5sZW5ndGgpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBnZXQgb25lRmFpbGVkKCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZmFpbC5sZW5ndGggPT09IDE7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGdldCBvbmVTdWNjZWVkZWQoKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5zdWNjZXNzLmxlbmd0aCA9PT0gMTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZ2V0IGFsbFN1Y2NlZWRlZCgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnNvbWVTdWNjZWVkZWQgJiYgIXRoaXMuc29tZUZhaWxlZDtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZ2V0IGFsbEZhaWxlZCgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnNvbWVGYWlsZWQgJiYgIXRoaXMuc29tZVN1Y2NlZWRlZDtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgcmVzZXQoKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZhaWwgPSBbXTtcclxuICAgICAgICAgICAgICAgIHRoaXMuc3VjY2VzcyA9IFtdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGRhdGEucmVkdWNlKFxyXG4gICAgICAgICAgICAoYWNjLCBub2RlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAobm9kZS5zdGF0dXMpIHtcclxuICAgICAgICAgICAgICAgICAgICBhY2Muc3VjY2Vzcy5wdXNoKG5vZGUpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBhY2MuZmFpbC5wdXNoKG5vZGUpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHJldHVybiBhY2M7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHN0YXR1c1xyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRSZXN0b3JlTWVzc2FnZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIGNvbnN0IHsgcmVzdG9yZVByb2Nlc3NTdGF0dXM6IHN0YXR1cyB9ID0gdGhpcztcclxuXHJcbiAgICAgICAgaWYgKHN0YXR1cy5zb21lRmFpbGVkICYmICFzdGF0dXMub25lRmFpbGVkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnRyYW5zbGF0aW9uLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAnQ09SRS5SRVNUT1JFX05PREUuUEFSVElBTF9QTFVSQUwnLFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIG51bWJlcjogc3RhdHVzLmZhaWwubGVuZ3RoXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoc3RhdHVzLm9uZUZhaWxlZCAmJiBzdGF0dXMuZmFpbFswXS5zdGF0dXNDb2RlKSB7XHJcbiAgICAgICAgICAgIGlmIChzdGF0dXMuZmFpbFswXS5zdGF0dXNDb2RlID09PSA0MDkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnRyYW5zbGF0aW9uLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgICAgJ0NPUkUuUkVTVE9SRV9OT0RFLk5PREVfRVhJU1RTJyxcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHN0YXR1cy5mYWlsWzBdLmVudHJ5Lm5hbWVcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMudHJhbnNsYXRpb24uaW5zdGFudChcclxuICAgICAgICAgICAgICAgICAgICAnQ09SRS5SRVNUT1JFX05PREUuR0VORVJJQycsXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBzdGF0dXMuZmFpbFswXS5lbnRyeS5uYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHN0YXR1cy5vbmVGYWlsZWQgJiYgIXN0YXR1cy5mYWlsWzBdLnN0YXR1c0NvZGUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMudHJhbnNsYXRpb24uaW5zdGFudChcclxuICAgICAgICAgICAgICAgICdDT1JFLlJFU1RPUkVfTk9ERS5MT0NBVElPTl9NSVNTSU5HJyxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiBzdGF0dXMuZmFpbFswXS5lbnRyeS5uYW1lXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoc3RhdHVzLmFsbFN1Y2NlZWRlZCAmJiAhc3RhdHVzLm9uZVN1Y2NlZWRlZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy50cmFuc2xhdGlvbi5pbnN0YW50KCdDT1JFLlJFU1RPUkVfTk9ERS5QTFVSQUwnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChzdGF0dXMuYWxsU3VjY2VlZGVkICYmIHN0YXR1cy5vbmVTdWNjZWVkZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMudHJhbnNsYXRpb24uaW5zdGFudChcclxuICAgICAgICAgICAgICAgICdDT1JFLlJFU1RPUkVfTk9ERS5TSU5HVUxBUicsXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogc3RhdHVzLnN1Y2Nlc3NbMF0uZW50cnkubmFtZVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG5vdGlmaWNhdGlvbigpOiB2b2lkIHtcclxuICAgICAgICBjb25zdCBzdGF0dXMgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLnJlc3RvcmVQcm9jZXNzU3RhdHVzKTtcclxuXHJcbiAgICAgICAgY29uc3QgbWVzc2FnZSA9IHRoaXMuZ2V0UmVzdG9yZU1lc3NhZ2UoKTtcclxuICAgICAgICB0aGlzLnJlc2V0KCk7XHJcblxyXG4gICAgICAgIGNvbnN0IGFjdGlvbiA9IChzdGF0dXMub25lU3VjY2VlZGVkICYmICFzdGF0dXMuc29tZUZhaWxlZCkgPyB0aGlzLnRyYW5zbGF0aW9uLmluc3RhbnQoJ0NPUkUuUkVTVE9SRV9OT0RFLlZJRVcnKSA6ICcnO1xyXG5cclxuICAgICAgICBsZXQgcGF0aDtcclxuICAgICAgICBpZiAoc3RhdHVzLnN1Y2Nlc3MgJiYgc3RhdHVzLnN1Y2Nlc3MubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBwYXRoID0gc3RhdHVzLnN1Y2Nlc3NbMF0uZW50cnkucGF0aDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5yZXN0b3JlLmVtaXQoe1xyXG4gICAgICAgICAgICBtZXNzYWdlOiBtZXNzYWdlLFxyXG4gICAgICAgICAgICBhY3Rpb246IGFjdGlvbixcclxuICAgICAgICAgICAgcGF0aDogcGF0aFxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgcmVzZXQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5yZXN0b3JlUHJvY2Vzc1N0YXR1cy5yZXNldCgpO1xyXG4gICAgICAgIHRoaXMuc2VsZWN0aW9uID0gW107XHJcbiAgICB9XHJcbn1cclxuIl19