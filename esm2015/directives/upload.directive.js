/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:no-input-rename  */
import { Directive, ElementRef, HostListener, Input, NgZone, Renderer2 } from '@angular/core';
import { FileUtils } from '../utils/file-utils';
export class UploadDirective {
    /**
     * @param {?} el
     * @param {?} renderer
     * @param {?} ngZone
     */
    constructor(el, renderer, ngZone) {
        this.el = el;
        this.renderer = renderer;
        this.ngZone = ngZone;
        /**
         * Enables/disables uploading.
         */
        this.enabled = true;
        /**
         * Upload mode. Can be "drop" (receives dropped files) or "click"
         * (clicking opens a file dialog). Both modes can be active at once.
         */
        this.mode = ['drop']; // click|drop
        this.isDragging = false;
        this.cssClassName = 'adf-upload__dragging';
        this.element = el.nativeElement;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.isClickMode() && this.renderer) {
            /** @type {?} */
            const inputUpload = this.renderer.createElement('input');
            this.upload = this.el.nativeElement.parentElement.appendChild(inputUpload);
            this.upload.type = 'file';
            this.upload.style.display = 'none';
            this.upload.addEventListener('change', (/**
             * @param {?} event
             * @return {?}
             */
            (event) => this.onSelectFiles(event)));
            if (this.multiple) {
                this.upload.setAttribute('multiple', '');
            }
            if (this.accept) {
                this.upload.setAttribute('accept', this.accept);
            }
            if (this.directory) {
                this.upload.setAttribute('webkitdirectory', '');
            }
        }
        if (this.isDropMode()) {
            this.ngZone.runOutsideAngular((/**
             * @return {?}
             */
            () => {
                this.element.addEventListener('dragenter', this.onDragEnter.bind(this));
                this.element.addEventListener('dragover', this.onDragOver.bind(this));
                this.element.addEventListener('dragleave', this.onDragLeave.bind(this));
                this.element.addEventListener('drop', this.onDrop.bind(this));
            }));
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.element.removeEventListener('dragenter', this.onDragEnter);
        this.element.removeEventListener('dragover', this.onDragOver);
        this.element.removeEventListener('dragleave', this.onDragLeave);
        this.element.removeEventListener('drop', this.onDrop);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onClick(event) {
        if (this.isClickMode() && this.upload) {
            event.preventDefault();
            this.upload.click();
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onDragEnter(event) {
        if (this.isDropMode()) {
            this.element.classList.add(this.cssClassName);
            this.isDragging = true;
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onDragOver(event) {
        event.preventDefault();
        if (this.isDropMode()) {
            this.element.classList.add(this.cssClassName);
            this.isDragging = true;
        }
        return false;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onDragLeave(event) {
        if (this.isDropMode()) {
            this.element.classList.remove(this.cssClassName);
            this.isDragging = false;
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onDrop(event) {
        if (this.isDropMode()) {
            event.stopPropagation();
            event.preventDefault();
            this.element.classList.remove(this.cssClassName);
            this.isDragging = false;
            /** @type {?} */
            const dataTransfer = this.getDataTransfer(event);
            if (dataTransfer) {
                this.getFilesDropped(dataTransfer).then((/**
                 * @param {?} files
                 * @return {?}
                 */
                (files) => {
                    this.onUploadFiles(files);
                }));
            }
        }
        return false;
    }
    /**
     * @param {?} files
     * @return {?}
     */
    onUploadFiles(files) {
        if (this.enabled && files.length > 0) {
            /** @type {?} */
            const customEvent = new CustomEvent('upload-files', {
                detail: {
                    sender: this,
                    data: this.data,
                    files: files
                },
                bubbles: true
            });
            this.el.nativeElement.dispatchEvent(customEvent);
        }
    }
    /**
     * @protected
     * @param {?} mode
     * @return {?}
     */
    hasMode(mode) {
        return this.enabled && mode && this.mode && this.mode.indexOf(mode) > -1;
    }
    /**
     * @protected
     * @return {?}
     */
    isDropMode() {
        return this.hasMode('drop');
    }
    /**
     * @protected
     * @return {?}
     */
    isClickMode() {
        return this.hasMode('click');
    }
    /**
     * @param {?} event
     * @return {?}
     */
    getDataTransfer(event) {
        if (event && event.dataTransfer) {
            return event.dataTransfer;
        }
        if (event && event.originalEvent && event.originalEvent.dataTransfer) {
            return event.originalEvent.dataTransfer;
        }
        return null;
    }
    /**
     * Extract files from the DataTransfer object used to hold the data that is being dragged during a drag and drop operation.
     * @param {?} dataTransfer DataTransfer object
     * @return {?}
     */
    getFilesDropped(dataTransfer) {
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        (resolve) => {
            /** @type {?} */
            const iterations = [];
            if (dataTransfer) {
                /** @type {?} */
                const items = dataTransfer.items;
                if (items) {
                    for (let i = 0; i < items.length; i++) {
                        if (typeof items[i].webkitGetAsEntry !== 'undefined') {
                            /** @type {?} */
                            const item = items[i].webkitGetAsEntry();
                            if (item) {
                                if (item.isFile) {
                                    iterations.push(Promise.resolve((/** @type {?} */ ({
                                        entry: item,
                                        file: items[i].getAsFile(),
                                        relativeFolder: '/'
                                    }))));
                                }
                                else if (item.isDirectory) {
                                    iterations.push(new Promise((/**
                                     * @param {?} resolveFolder
                                     * @return {?}
                                     */
                                    (resolveFolder) => {
                                        FileUtils.flatten(item).then((/**
                                         * @param {?} files
                                         * @return {?}
                                         */
                                        (files) => resolveFolder(files)));
                                    })));
                                }
                            }
                        }
                        else {
                            iterations.push(Promise.resolve((/** @type {?} */ ({
                                entry: null,
                                file: items[i].getAsFile(),
                                relativeFolder: '/'
                            }))));
                        }
                    }
                }
                else {
                    // safari or FF
                    /** @type {?} */
                    const files = FileUtils
                        .toFileArray(dataTransfer.files)
                        .map((/**
                     * @param {?} file
                     * @return {?}
                     */
                    (file) => (/** @type {?} */ ({
                        entry: null,
                        file: file,
                        relativeFolder: '/'
                    }))));
                    iterations.push(Promise.resolve(files));
                }
            }
            Promise.all(iterations).then((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                resolve(result.reduce((/**
                 * @param {?} a
                 * @param {?} b
                 * @return {?}
                 */
                (a, b) => a.concat(b)), []));
            }));
        }));
    }
    /**
     * Invoked when user selects files or folders by means of File Dialog
     * @param {?} event DOM event
     * @return {?}
     */
    onSelectFiles(event) {
        if (this.isClickMode()) {
            /** @type {?} */
            const input = ((/** @type {?} */ (event.currentTarget)));
            /** @type {?} */
            const files = FileUtils.toFileArray(input.files);
            this.onUploadFiles(files.map((/**
             * @param {?} file
             * @return {?}
             */
            (file) => (/** @type {?} */ ({
                entry: null,
                file: file,
                relativeFolder: '/'
            })))));
            event.target.value = '';
        }
    }
}
UploadDirective.decorators = [
    { type: Directive, args: [{
                selector: '[adf-upload]'
            },] }
];
/** @nocollapse */
UploadDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 },
    { type: NgZone }
];
UploadDirective.propDecorators = {
    enabled: [{ type: Input, args: ['adf-upload',] }],
    data: [{ type: Input, args: ['adf-upload-data',] }],
    mode: [{ type: Input }],
    multiple: [{ type: Input }],
    accept: [{ type: Input }],
    directory: [{ type: Input }],
    onClick: [{ type: HostListener, args: ['click', ['$event'],] }]
};
if (false) {
    /**
     * Enables/disables uploading.
     * @type {?}
     */
    UploadDirective.prototype.enabled;
    /**
     * Data to upload.
     * @type {?}
     */
    UploadDirective.prototype.data;
    /**
     * Upload mode. Can be "drop" (receives dropped files) or "click"
     * (clicking opens a file dialog). Both modes can be active at once.
     * @type {?}
     */
    UploadDirective.prototype.mode;
    /**
     * Toggles multiple file uploads.
     * @type {?}
     */
    UploadDirective.prototype.multiple;
    /**
     * (Click mode only) MIME type filter for files to accept.
     * @type {?}
     */
    UploadDirective.prototype.accept;
    /**
     * (Click mode only) Toggles uploading of directories.
     * @type {?}
     */
    UploadDirective.prototype.directory;
    /** @type {?} */
    UploadDirective.prototype.isDragging;
    /**
     * @type {?}
     * @private
     */
    UploadDirective.prototype.cssClassName;
    /**
     * @type {?}
     * @private
     */
    UploadDirective.prototype.upload;
    /**
     * @type {?}
     * @private
     */
    UploadDirective.prototype.element;
    /**
     * @type {?}
     * @private
     */
    UploadDirective.prototype.el;
    /**
     * @type {?}
     * @private
     */
    UploadDirective.prototype.renderer;
    /**
     * @type {?}
     * @private
     */
    UploadDirective.prototype.ngZone;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvdXBsb2FkLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQXFCLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNqSCxPQUFPLEVBQVksU0FBUyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFLMUQsTUFBTSxPQUFPLGVBQWU7Ozs7OztJQWtDeEIsWUFBb0IsRUFBYyxFQUFVLFFBQW1CLEVBQVUsTUFBYztRQUFuRSxPQUFFLEdBQUYsRUFBRSxDQUFZO1FBQVUsYUFBUSxHQUFSLFFBQVEsQ0FBVztRQUFVLFdBQU0sR0FBTixNQUFNLENBQVE7Ozs7UUE5QnZGLFlBQU8sR0FBWSxJQUFJLENBQUM7Ozs7O1FBVXhCLFNBQUksR0FBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsYUFBYTtRQWN4QyxlQUFVLEdBQVksS0FBSyxDQUFDO1FBRXBCLGlCQUFZLEdBQVcsc0JBQXNCLENBQUM7UUFLbEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUMsYUFBYSxDQUFDO0lBQ3BDLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTs7a0JBQy9CLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsQ0FBQyxPQUFPLENBQUM7WUFDeEQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBRTNFLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxHQUFHLE1BQU0sQ0FBQztZQUMxQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1lBQ25DLElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsUUFBUTs7OztZQUFFLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQUM7WUFFN0UsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNmLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FBQzthQUM1QztZQUVELElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDYixJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ25EO1lBRUQsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO2dCQUNoQixJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLENBQUMsQ0FBQzthQUNuRDtTQUNKO1FBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUU7WUFDbkIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxpQkFBaUI7OztZQUFDLEdBQUcsRUFBRTtnQkFDL0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDeEUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDdEUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztnQkFDeEUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNsRSxDQUFDLEVBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQzs7OztJQUVELFdBQVc7UUFDUCxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDaEUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzlELElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNoRSxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDMUQsQ0FBQzs7Ozs7SUFHRCxPQUFPLENBQUMsS0FBWTtRQUNoQixJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ25DLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ3ZCO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsS0FBWTtRQUNwQixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRTtZQUNuQixJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1NBQzFCO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsS0FBWTtRQUNuQixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdkIsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUU7WUFDbkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUM5QyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztTQUMxQjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLEtBQUs7UUFDYixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRTtZQUNuQixJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2pELElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1NBQzNCO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsS0FBWTtRQUNmLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFO1lBRW5CLEtBQUssQ0FBQyxlQUFlLEVBQUUsQ0FBQztZQUN4QixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7WUFFdkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNqRCxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQzs7a0JBRWxCLFlBQVksR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQztZQUNoRCxJQUFJLFlBQVksRUFBRTtnQkFDZCxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUk7Ozs7Z0JBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTtvQkFDOUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDOUIsQ0FBQyxFQUFDLENBQUM7YUFFTjtTQUNKO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7Ozs7SUFFRCxhQUFhLENBQUMsS0FBaUI7UUFDM0IsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOztrQkFDNUIsV0FBVyxHQUFHLElBQUksV0FBVyxDQUFDLGNBQWMsRUFBRTtnQkFDaEQsTUFBTSxFQUFFO29CQUNKLE1BQU0sRUFBRSxJQUFJO29CQUNaLElBQUksRUFBRSxJQUFJLENBQUMsSUFBSTtvQkFDZixLQUFLLEVBQUUsS0FBSztpQkFDZjtnQkFDRCxPQUFPLEVBQUUsSUFBSTthQUNoQixDQUFDO1lBRUYsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQ3BEO0lBQ0wsQ0FBQzs7Ozs7O0lBRVMsT0FBTyxDQUFDLElBQVk7UUFDMUIsT0FBTyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQzdFLENBQUM7Ozs7O0lBRVMsVUFBVTtRQUNoQixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDaEMsQ0FBQzs7Ozs7SUFFUyxXQUFXO1FBQ2pCLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7OztJQUVELGVBQWUsQ0FBQyxLQUFrQjtRQUM5QixJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsWUFBWSxFQUFFO1lBQzdCLE9BQU8sS0FBSyxDQUFDLFlBQVksQ0FBQztTQUM3QjtRQUNELElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxhQUFhLElBQUksS0FBSyxDQUFDLGFBQWEsQ0FBQyxZQUFZLEVBQUU7WUFDbEUsT0FBTyxLQUFLLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQztTQUMzQztRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7OztJQU1ELGVBQWUsQ0FBQyxZQUEwQjtRQUN0QyxPQUFPLElBQUksT0FBTzs7OztRQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7O2tCQUNyQixVQUFVLEdBQUcsRUFBRTtZQUVyQixJQUFJLFlBQVksRUFBRTs7c0JBQ1IsS0FBSyxHQUFHLFlBQVksQ0FBQyxLQUFLO2dCQUNoQyxJQUFJLEtBQUssRUFBRTtvQkFDUCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTt3QkFDbkMsSUFBSSxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsS0FBSyxXQUFXLEVBQUU7O2tDQUM1QyxJQUFJLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLGdCQUFnQixFQUFFOzRCQUN4QyxJQUFJLElBQUksRUFBRTtnQ0FDTixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7b0NBQ2IsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLG1CQUFXO3dDQUN2QyxLQUFLLEVBQUUsSUFBSTt3Q0FDWCxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsRUFBRTt3Q0FDMUIsY0FBYyxFQUFFLEdBQUc7cUNBQ3RCLEVBQUEsQ0FBQyxDQUFDLENBQUM7aUNBQ1A7cUNBQU0sSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFO29DQUN6QixVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksT0FBTzs7OztvQ0FBQyxDQUFDLGFBQWEsRUFBRSxFQUFFO3dDQUMxQyxTQUFTLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUk7Ozs7d0NBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsRUFBQyxDQUFDO29DQUNsRSxDQUFDLEVBQUMsQ0FBQyxDQUFDO2lDQUNQOzZCQUNKO3lCQUNKOzZCQUFNOzRCQUNILFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBVztnQ0FDdkMsS0FBSyxFQUFFLElBQUk7Z0NBQ1gsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7Z0NBQzFCLGNBQWMsRUFBRSxHQUFHOzZCQUN0QixFQUFBLENBQUMsQ0FBQyxDQUFDO3lCQUNQO3FCQUNKO2lCQUNKO3FCQUFNOzs7MEJBRUcsS0FBSyxHQUFHLFNBQVM7eUJBQ2xCLFdBQVcsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDO3lCQUMvQixHQUFHOzs7O29CQUFDLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxtQkFBVzt3QkFDdEIsS0FBSyxFQUFFLElBQUk7d0JBQ1gsSUFBSSxFQUFFLElBQUk7d0JBQ1YsY0FBYyxFQUFFLEdBQUc7cUJBQ3RCLEVBQUEsRUFBQztvQkFFTixVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztpQkFDM0M7YUFDSjtZQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSTs7OztZQUFDLENBQUMsTUFBTSxFQUFFLEVBQUU7Z0JBQ3BDLE9BQU8sQ0FBQyxNQUFNLENBQUMsTUFBTTs7Ozs7Z0JBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDdEQsQ0FBQyxFQUFDLENBQUM7UUFDUCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQU1ELGFBQWEsQ0FBQyxLQUFVO1FBQ3BCLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFFOztrQkFDZCxLQUFLLEdBQUcsQ0FBQyxtQkFBbUIsS0FBSyxDQUFDLGFBQWEsRUFBQSxDQUFDOztrQkFDaEQsS0FBSyxHQUFHLFNBQVMsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztZQUNoRCxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxHQUFHOzs7O1lBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLG1CQUFXO2dCQUM5QyxLQUFLLEVBQUUsSUFBSTtnQkFDWCxJQUFJLEVBQUUsSUFBSTtnQkFDVixjQUFjLEVBQUUsR0FBRzthQUN0QixFQUFBLEVBQUMsQ0FBQyxDQUFDO1lBQ0osS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1NBQzNCO0lBQ0wsQ0FBQzs7O1lBOU9KLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsY0FBYzthQUMzQjs7OztZQUxtQixVQUFVO1lBQWtELFNBQVM7WUFBcEMsTUFBTTs7O3NCQVN0RCxLQUFLLFNBQUMsWUFBWTttQkFJbEIsS0FBSyxTQUFDLGlCQUFpQjttQkFNdkIsS0FBSzt1QkFJTCxLQUFLO3FCQUlMLEtBQUs7d0JBSUwsS0FBSztzQkFvREwsWUFBWSxTQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsQ0FBQzs7Ozs7OztJQTFFakMsa0NBQ3dCOzs7OztJQUd4QiwrQkFDVTs7Ozs7O0lBS1YsK0JBQzBCOzs7OztJQUcxQixtQ0FDa0I7Ozs7O0lBR2xCLGlDQUNlOzs7OztJQUdmLG9DQUNtQjs7SUFFbkIscUNBQTRCOzs7OztJQUU1Qix1Q0FBc0Q7Ozs7O0lBQ3RELGlDQUFpQzs7Ozs7SUFDakMsa0NBQTZCOzs7OztJQUVqQiw2QkFBc0I7Ozs7O0lBQUUsbUNBQTJCOzs7OztJQUFFLGlDQUFzQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4vKiB0c2xpbnQ6ZGlzYWJsZTpuby1pbnB1dC1yZW5hbWUgICovXHJcblxyXG5pbXBvcnQgeyBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIEhvc3RMaXN0ZW5lciwgSW5wdXQsIE5nWm9uZSwgT25EZXN0cm95LCBPbkluaXQsIFJlbmRlcmVyMiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGaWxlSW5mbywgRmlsZVV0aWxzIH0gZnJvbSAnLi4vdXRpbHMvZmlsZS11dGlscyc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW2FkZi11cGxvYWRdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgVXBsb2FkRGlyZWN0aXZlIGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG5cclxuICAgIC8qKiBFbmFibGVzL2Rpc2FibGVzIHVwbG9hZGluZy4gKi9cclxuICAgIEBJbnB1dCgnYWRmLXVwbG9hZCcpXHJcbiAgICBlbmFibGVkOiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgICAvKiogRGF0YSB0byB1cGxvYWQuICovXHJcbiAgICBASW5wdXQoJ2FkZi11cGxvYWQtZGF0YScpXHJcbiAgICBkYXRhOiBhbnk7XHJcblxyXG4gICAgLyoqIFVwbG9hZCBtb2RlLiBDYW4gYmUgXCJkcm9wXCIgKHJlY2VpdmVzIGRyb3BwZWQgZmlsZXMpIG9yIFwiY2xpY2tcIlxyXG4gICAgICogKGNsaWNraW5nIG9wZW5zIGEgZmlsZSBkaWFsb2cpLiBCb3RoIG1vZGVzIGNhbiBiZSBhY3RpdmUgYXQgb25jZS5cclxuICAgICAqL1xyXG4gICAgQElucHV0KClcclxuICAgIG1vZGU6IHN0cmluZ1tdID0gWydkcm9wJ107IC8vIGNsaWNrfGRyb3BcclxuXHJcbiAgICAvKiogVG9nZ2xlcyBtdWx0aXBsZSBmaWxlIHVwbG9hZHMuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgbXVsdGlwbGU6IGJvb2xlYW47XHJcblxyXG4gICAgLyoqIChDbGljayBtb2RlIG9ubHkpIE1JTUUgdHlwZSBmaWx0ZXIgZm9yIGZpbGVzIHRvIGFjY2VwdC4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBhY2NlcHQ6IHN0cmluZztcclxuXHJcbiAgICAvKiogKENsaWNrIG1vZGUgb25seSkgVG9nZ2xlcyB1cGxvYWRpbmcgb2YgZGlyZWN0b3JpZXMuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgZGlyZWN0b3J5OiBib29sZWFuO1xyXG5cclxuICAgIGlzRHJhZ2dpbmc6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBwcml2YXRlIGNzc0NsYXNzTmFtZTogc3RyaW5nID0gJ2FkZi11cGxvYWRfX2RyYWdnaW5nJztcclxuICAgIHByaXZhdGUgdXBsb2FkOiBIVE1MSW5wdXRFbGVtZW50O1xyXG4gICAgcHJpdmF0ZSBlbGVtZW50OiBIVE1MRWxlbWVudDtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGVsOiBFbGVtZW50UmVmLCBwcml2YXRlIHJlbmRlcmVyOiBSZW5kZXJlcjIsIHByaXZhdGUgbmdab25lOiBOZ1pvbmUpIHtcclxuICAgICAgICB0aGlzLmVsZW1lbnQgPSBlbC5uYXRpdmVFbGVtZW50O1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzQ2xpY2tNb2RlKCkgJiYgdGhpcy5yZW5kZXJlcikge1xyXG4gICAgICAgICAgICBjb25zdCBpbnB1dFVwbG9hZCA9IHRoaXMucmVuZGVyZXIuY3JlYXRlRWxlbWVudCgnaW5wdXQnKTtcclxuICAgICAgICAgICAgdGhpcy51cGxvYWQgPSB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQucGFyZW50RWxlbWVudC5hcHBlbmRDaGlsZChpbnB1dFVwbG9hZCk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnVwbG9hZC50eXBlID0gJ2ZpbGUnO1xyXG4gICAgICAgICAgICB0aGlzLnVwbG9hZC5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xyXG4gICAgICAgICAgICB0aGlzLnVwbG9hZC5hZGRFdmVudExpc3RlbmVyKCdjaGFuZ2UnLCAoZXZlbnQpID0+IHRoaXMub25TZWxlY3RGaWxlcyhldmVudCkpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMubXVsdGlwbGUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudXBsb2FkLnNldEF0dHJpYnV0ZSgnbXVsdGlwbGUnLCAnJyk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmFjY2VwdCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy51cGxvYWQuc2V0QXR0cmlidXRlKCdhY2NlcHQnLCB0aGlzLmFjY2VwdCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmRpcmVjdG9yeSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy51cGxvYWQuc2V0QXR0cmlidXRlKCd3ZWJraXRkaXJlY3RvcnknLCAnJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmlzRHJvcE1vZGUoKSkge1xyXG4gICAgICAgICAgICB0aGlzLm5nWm9uZS5ydW5PdXRzaWRlQW5ndWxhcigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignZHJhZ2VudGVyJywgdGhpcy5vbkRyYWdFbnRlci5iaW5kKHRoaXMpKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdkcmFnb3ZlcicsIHRoaXMub25EcmFnT3Zlci5iaW5kKHRoaXMpKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdkcmFnbGVhdmUnLCB0aGlzLm9uRHJhZ0xlYXZlLmJpbmQodGhpcykpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2Ryb3AnLCB0aGlzLm9uRHJvcC5iaW5kKHRoaXMpKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIHRoaXMuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdkcmFnZW50ZXInLCB0aGlzLm9uRHJhZ0VudGVyKTtcclxuICAgICAgICB0aGlzLmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcignZHJhZ292ZXInLCB0aGlzLm9uRHJhZ092ZXIpO1xyXG4gICAgICAgIHRoaXMuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdkcmFnbGVhdmUnLCB0aGlzLm9uRHJhZ0xlYXZlKTtcclxuICAgICAgICB0aGlzLmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcignZHJvcCcsIHRoaXMub25Ecm9wKTtcclxuICAgIH1cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdjbGljaycsIFsnJGV2ZW50J10pXHJcbiAgICBvbkNsaWNrKGV2ZW50OiBFdmVudCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzQ2xpY2tNb2RlKCkgJiYgdGhpcy51cGxvYWQpIHtcclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgdGhpcy51cGxvYWQuY2xpY2soKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25EcmFnRW50ZXIoZXZlbnQ6IEV2ZW50KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNEcm9wTW9kZSgpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QuYWRkKHRoaXMuY3NzQ2xhc3NOYW1lKTtcclxuICAgICAgICAgICAgdGhpcy5pc0RyYWdnaW5nID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25EcmFnT3ZlcihldmVudDogRXZlbnQpIHtcclxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgIGlmICh0aGlzLmlzRHJvcE1vZGUoKSkge1xyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LmFkZCh0aGlzLmNzc0NsYXNzTmFtZSk7XHJcbiAgICAgICAgICAgIHRoaXMuaXNEcmFnZ2luZyA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRyYWdMZWF2ZShldmVudCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzRHJvcE1vZGUoKSkge1xyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQuY2xhc3NMaXN0LnJlbW92ZSh0aGlzLmNzc0NsYXNzTmFtZSk7XHJcbiAgICAgICAgICAgIHRoaXMuaXNEcmFnZ2luZyA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvbkRyb3AoZXZlbnQ6IEV2ZW50KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNEcm9wTW9kZSgpKSB7XHJcblxyXG4gICAgICAgICAgICBldmVudC5zdG9wUHJvcGFnYXRpb24oKTtcclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKHRoaXMuY3NzQ2xhc3NOYW1lKTtcclxuICAgICAgICAgICAgdGhpcy5pc0RyYWdnaW5nID0gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBkYXRhVHJhbnNmZXIgPSB0aGlzLmdldERhdGFUcmFuc2ZlcihldmVudCk7XHJcbiAgICAgICAgICAgIGlmIChkYXRhVHJhbnNmZXIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0RmlsZXNEcm9wcGVkKGRhdGFUcmFuc2ZlcikudGhlbigoZmlsZXMpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9uVXBsb2FkRmlsZXMoZmlsZXMpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBvblVwbG9hZEZpbGVzKGZpbGVzOiBGaWxlSW5mb1tdKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZW5hYmxlZCAmJiBmaWxlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGN1c3RvbUV2ZW50ID0gbmV3IEN1c3RvbUV2ZW50KCd1cGxvYWQtZmlsZXMnLCB7XHJcbiAgICAgICAgICAgICAgICBkZXRhaWw6IHtcclxuICAgICAgICAgICAgICAgICAgICBzZW5kZXI6IHRoaXMsXHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YTogdGhpcy5kYXRhLFxyXG4gICAgICAgICAgICAgICAgICAgIGZpbGVzOiBmaWxlc1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGJ1YmJsZXM6IHRydWVcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuZGlzcGF0Y2hFdmVudChjdXN0b21FdmVudCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByb3RlY3RlZCBoYXNNb2RlKG1vZGU6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVuYWJsZWQgJiYgbW9kZSAmJiB0aGlzLm1vZGUgJiYgdGhpcy5tb2RlLmluZGV4T2YobW9kZSkgPiAtMTtcclxuICAgIH1cclxuXHJcbiAgICBwcm90ZWN0ZWQgaXNEcm9wTW9kZSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5oYXNNb2RlKCdkcm9wJyk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJvdGVjdGVkIGlzQ2xpY2tNb2RlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmhhc01vZGUoJ2NsaWNrJyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGF0YVRyYW5zZmVyKGV2ZW50OiBFdmVudCB8IGFueSk6IERhdGFUcmFuc2ZlciB7XHJcbiAgICAgICAgaWYgKGV2ZW50ICYmIGV2ZW50LmRhdGFUcmFuc2Zlcikge1xyXG4gICAgICAgICAgICByZXR1cm4gZXZlbnQuZGF0YVRyYW5zZmVyO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZXZlbnQgJiYgZXZlbnQub3JpZ2luYWxFdmVudCAmJiBldmVudC5vcmlnaW5hbEV2ZW50LmRhdGFUcmFuc2Zlcikge1xyXG4gICAgICAgICAgICByZXR1cm4gZXZlbnQub3JpZ2luYWxFdmVudC5kYXRhVHJhbnNmZXI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRXh0cmFjdCBmaWxlcyBmcm9tIHRoZSBEYXRhVHJhbnNmZXIgb2JqZWN0IHVzZWQgdG8gaG9sZCB0aGUgZGF0YSB0aGF0IGlzIGJlaW5nIGRyYWdnZWQgZHVyaW5nIGEgZHJhZyBhbmQgZHJvcCBvcGVyYXRpb24uXHJcbiAgICAgKiBAcGFyYW0gZGF0YVRyYW5zZmVyIERhdGFUcmFuc2ZlciBvYmplY3RcclxuICAgICAqL1xyXG4gICAgZ2V0RmlsZXNEcm9wcGVkKGRhdGFUcmFuc2ZlcjogRGF0YVRyYW5zZmVyKTogUHJvbWlzZTxGaWxlSW5mb1tdPiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGl0ZXJhdGlvbnMgPSBbXTtcclxuXHJcbiAgICAgICAgICAgIGlmIChkYXRhVHJhbnNmZXIpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGl0ZW1zID0gZGF0YVRyYW5zZmVyLml0ZW1zO1xyXG4gICAgICAgICAgICAgICAgaWYgKGl0ZW1zKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBpdGVtcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodHlwZW9mIGl0ZW1zW2ldLndlYmtpdEdldEFzRW50cnkgIT09ICd1bmRlZmluZWQnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBpdGVtID0gaXRlbXNbaV0ud2Via2l0R2V0QXNFbnRyeSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGl0ZW0pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoaXRlbS5pc0ZpbGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXRlcmF0aW9ucy5wdXNoKFByb21pc2UucmVzb2x2ZSg8RmlsZUluZm8+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVudHJ5OiBpdGVtLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsZTogaXRlbXNbaV0uZ2V0QXNGaWxlKCksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWxhdGl2ZUZvbGRlcjogJy8nXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGl0ZW0uaXNEaXJlY3RvcnkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaXRlcmF0aW9ucy5wdXNoKG5ldyBQcm9taXNlKChyZXNvbHZlRm9sZGVyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBGaWxlVXRpbHMuZmxhdHRlbihpdGVtKS50aGVuKChmaWxlcykgPT4gcmVzb2x2ZUZvbGRlcihmaWxlcykpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaXRlcmF0aW9ucy5wdXNoKFByb21pc2UucmVzb2x2ZSg8RmlsZUluZm8+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbnRyeTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlOiBpdGVtc1tpXS5nZXRBc0ZpbGUoKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWxhdGl2ZUZvbGRlcjogJy8nXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIHNhZmFyaSBvciBGRlxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGZpbGVzID0gRmlsZVV0aWxzXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC50b0ZpbGVBcnJheShkYXRhVHJhbnNmZXIuZmlsZXMpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC5tYXAoKGZpbGUpID0+IDxGaWxlSW5mbz4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZW50cnk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlOiBmaWxlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVsYXRpdmVGb2xkZXI6ICcvJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaXRlcmF0aW9ucy5wdXNoKFByb21pc2UucmVzb2x2ZShmaWxlcykpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBQcm9taXNlLmFsbChpdGVyYXRpb25zKS50aGVuKChyZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgICAgIHJlc29sdmUocmVzdWx0LnJlZHVjZSgoYSwgYikgPT4gYS5jb25jYXQoYiksIFtdKSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogSW52b2tlZCB3aGVuIHVzZXIgc2VsZWN0cyBmaWxlcyBvciBmb2xkZXJzIGJ5IG1lYW5zIG9mIEZpbGUgRGlhbG9nXHJcbiAgICAgKiBAcGFyYW0gZXZlbnQgRE9NIGV2ZW50XHJcbiAgICAgKi9cclxuICAgIG9uU2VsZWN0RmlsZXMoZXZlbnQ6IGFueSk6IHZvaWQge1xyXG4gICAgICAgIGlmICh0aGlzLmlzQ2xpY2tNb2RlKCkpIHtcclxuICAgICAgICAgICAgY29uc3QgaW5wdXQgPSAoPEhUTUxJbnB1dEVsZW1lbnQ+IGV2ZW50LmN1cnJlbnRUYXJnZXQpO1xyXG4gICAgICAgICAgICBjb25zdCBmaWxlcyA9IEZpbGVVdGlscy50b0ZpbGVBcnJheShpbnB1dC5maWxlcyk7XHJcbiAgICAgICAgICAgIHRoaXMub25VcGxvYWRGaWxlcyhmaWxlcy5tYXAoKGZpbGUpID0+IDxGaWxlSW5mbz4ge1xyXG4gICAgICAgICAgICAgICAgZW50cnk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBmaWxlOiBmaWxlLFxyXG4gICAgICAgICAgICAgICAgcmVsYXRpdmVGb2xkZXI6ICcvJ1xyXG4gICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgICAgIGV2ZW50LnRhcmdldC52YWx1ZSA9ICcnO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=