/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FileUploadStatus } from '../models/file.model';
export class FileUploadEvent {
    /**
     * @param {?} file
     * @param {?=} status
     * @param {?=} error
     */
    constructor(file, status = FileUploadStatus.Pending, error = null) {
        this.file = file;
        this.status = status;
        this.error = error;
    }
}
if (false) {
    /** @type {?} */
    FileUploadEvent.prototype.file;
    /** @type {?} */
    FileUploadEvent.prototype.status;
    /** @type {?} */
    FileUploadEvent.prototype.error;
}
export class FileUploadCompleteEvent extends FileUploadEvent {
    /**
     * @param {?} file
     * @param {?=} totalComplete
     * @param {?=} data
     * @param {?=} totalAborted
     */
    constructor(file, totalComplete = 0, data, totalAborted = 0) {
        super(file, FileUploadStatus.Complete);
        this.totalComplete = totalComplete;
        this.data = data;
        this.totalAborted = totalAborted;
    }
}
if (false) {
    /** @type {?} */
    FileUploadCompleteEvent.prototype.totalComplete;
    /** @type {?} */
    FileUploadCompleteEvent.prototype.data;
    /** @type {?} */
    FileUploadCompleteEvent.prototype.totalAborted;
}
export class FileUploadDeleteEvent extends FileUploadEvent {
    /**
     * @param {?} file
     * @param {?=} totalComplete
     */
    constructor(file, totalComplete = 0) {
        super(file, FileUploadStatus.Deleted);
        this.totalComplete = totalComplete;
    }
}
if (false) {
    /** @type {?} */
    FileUploadDeleteEvent.prototype.totalComplete;
}
export class FileUploadErrorEvent extends FileUploadEvent {
    /**
     * @param {?} file
     * @param {?} error
     * @param {?=} totalError
     */
    constructor(file, error, totalError = 0) {
        super(file, FileUploadStatus.Error);
        this.error = error;
        this.totalError = totalError;
    }
}
if (false) {
    /** @type {?} */
    FileUploadErrorEvent.prototype.error;
    /** @type {?} */
    FileUploadErrorEvent.prototype.totalError;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZS5ldmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImV2ZW50cy9maWxlLmV2ZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBYSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRW5FLE1BQU0sT0FBTyxlQUFlOzs7Ozs7SUFFeEIsWUFDb0IsSUFBZSxFQUNmLFNBQTJCLGdCQUFnQixDQUFDLE9BQU8sRUFDbkQsUUFBYSxJQUFJO1FBRmpCLFNBQUksR0FBSixJQUFJLENBQVc7UUFDZixXQUFNLEdBQU4sTUFBTSxDQUE2QztRQUNuRCxVQUFLLEdBQUwsS0FBSyxDQUFZO0lBQ3JDLENBQUM7Q0FFSjs7O0lBTE8sK0JBQStCOztJQUMvQixpQ0FBbUU7O0lBQ25FLGdDQUFpQzs7QUFLekMsTUFBTSxPQUFPLHVCQUF3QixTQUFRLGVBQWU7Ozs7Ozs7SUFFeEQsWUFBWSxJQUFlLEVBQVMsZ0JBQXdCLENBQUMsRUFBUyxJQUFVLEVBQVMsZUFBdUIsQ0FBQztRQUM3RyxLQUFLLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRFAsa0JBQWEsR0FBYixhQUFhLENBQVk7UUFBUyxTQUFJLEdBQUosSUFBSSxDQUFNO1FBQVMsaUJBQVksR0FBWixZQUFZLENBQVk7SUFFakgsQ0FBQztDQUVKOzs7SUFKZ0MsZ0RBQWdDOztJQUFFLHVDQUFpQjs7SUFBRSwrQ0FBK0I7O0FBTXJILE1BQU0sT0FBTyxxQkFBc0IsU0FBUSxlQUFlOzs7OztJQUV0RCxZQUFZLElBQWUsRUFBUyxnQkFBd0IsQ0FBQztRQUN6RCxLQUFLLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRE4sa0JBQWEsR0FBYixhQUFhLENBQVk7SUFFN0QsQ0FBQztDQUVKOzs7SUFKZ0MsOENBQWdDOztBQU1qRSxNQUFNLE9BQU8sb0JBQXFCLFNBQVEsZUFBZTs7Ozs7O0lBRXJELFlBQVksSUFBZSxFQUFTLEtBQVUsRUFBUyxhQUFxQixDQUFDO1FBQ3pFLEtBQUssQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLENBQUM7UUFESixVQUFLLEdBQUwsS0FBSyxDQUFLO1FBQVMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUU3RSxDQUFDO0NBRUo7OztJQUpnQyxxQ0FBaUI7O0lBQUUsMENBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEZpbGVNb2RlbCwgRmlsZVVwbG9hZFN0YXR1cyB9IGZyb20gJy4uL21vZGVscy9maWxlLm1vZGVsJztcclxuXHJcbmV4cG9ydCBjbGFzcyBGaWxlVXBsb2FkRXZlbnQge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHB1YmxpYyByZWFkb25seSBmaWxlOiBGaWxlTW9kZWwsXHJcbiAgICAgICAgcHVibGljIHJlYWRvbmx5IHN0YXR1czogRmlsZVVwbG9hZFN0YXR1cyA9IEZpbGVVcGxvYWRTdGF0dXMuUGVuZGluZyxcclxuICAgICAgICBwdWJsaWMgcmVhZG9ubHkgZXJyb3I6IGFueSA9IG51bGwpIHtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBGaWxlVXBsb2FkQ29tcGxldGVFdmVudCBleHRlbmRzIEZpbGVVcGxvYWRFdmVudCB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoZmlsZTogRmlsZU1vZGVsLCBwdWJsaWMgdG90YWxDb21wbGV0ZTogbnVtYmVyID0gMCwgcHVibGljIGRhdGE/OiBhbnksIHB1YmxpYyB0b3RhbEFib3J0ZWQ6IG51bWJlciA9IDApIHtcclxuICAgICAgICBzdXBlcihmaWxlLCBGaWxlVXBsb2FkU3RhdHVzLkNvbXBsZXRlKTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBGaWxlVXBsb2FkRGVsZXRlRXZlbnQgZXh0ZW5kcyBGaWxlVXBsb2FkRXZlbnQge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGZpbGU6IEZpbGVNb2RlbCwgcHVibGljIHRvdGFsQ29tcGxldGU6IG51bWJlciA9IDApIHtcclxuICAgICAgICBzdXBlcihmaWxlLCBGaWxlVXBsb2FkU3RhdHVzLkRlbGV0ZWQpO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIEZpbGVVcGxvYWRFcnJvckV2ZW50IGV4dGVuZHMgRmlsZVVwbG9hZEV2ZW50IHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihmaWxlOiBGaWxlTW9kZWwsIHB1YmxpYyBlcnJvcjogYW55LCBwdWJsaWMgdG90YWxFcnJvcjogbnVtYmVyID0gMCkge1xyXG4gICAgICAgIHN1cGVyKGZpbGUsIEZpbGVVcGxvYWRTdGF0dXMuRXJyb3IpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=