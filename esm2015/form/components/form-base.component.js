/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FormOutcomeModel } from './widgets';
import { EventEmitter, Input, Output } from '@angular/core';
/**
 * @abstract
 */
export class FormBaseComponent {
    constructor() {
        /**
         * Toggle rendering of the form title.
         */
        this.showTitle = true;
        /**
         * Toggle rendering of the `Complete` outcome button.
         */
        this.showCompleteButton = true;
        /**
         * If true then the `Complete` outcome button is shown but it will be disabled.
         */
        this.disableCompleteButton = false;
        /**
         * If true then the `Save` outcome button is shown but will be disabled.
         */
        this.disableSaveButton = false;
        /**
         * If true then the `Start Process` outcome button is shown but it will be disabled.
         */
        this.disableStartProcessButton = false;
        /**
         * Toggle rendering of the `Save` outcome button.
         */
        this.showSaveButton = true;
        /**
         * Toggle readonly state of the form. Forces all form widgets to render as readonly if enabled.
         */
        this.readOnly = false;
        /**
         * Toggle rendering of the `Refresh` button.
         */
        this.showRefreshButton = true;
        /**
         * Toggle rendering of the validation icon next to the form title.
         */
        this.showValidationIcon = true;
        /**
         * Emitted when the supplied form values have a validation error.
         */
        this.formError = new EventEmitter();
        /**
         * Emitted when any outcome is executed. Default behaviour can be prevented
         * via `event.preventDefault()`.
         */
        this.executeOutcome = new EventEmitter();
        /**
         * Emitted when any error occurs.
         */
        this.error = new EventEmitter();
    }
    /**
     * @return {?}
     */
    getParsedFormDefinition() {
        return this;
    }
    /**
     * @return {?}
     */
    hasForm() {
        return this.form ? true : false;
    }
    /**
     * @return {?}
     */
    isTitleEnabled() {
        /** @type {?} */
        let titleEnabled = false;
        if (this.showTitle && this.form) {
            titleEnabled = true;
        }
        return titleEnabled;
    }
    /**
     * @param {?} outcomeName
     * @return {?}
     */
    getColorForOutcome(outcomeName) {
        return outcomeName === FormBaseComponent.COMPLETE_OUTCOME_NAME ? FormBaseComponent.COMPLETE_BUTTON_COLOR : '';
    }
    /**
     * @param {?} outcome
     * @return {?}
     */
    isOutcomeButtonEnabled(outcome) {
        if (this.form.readOnly) {
            return false;
        }
        if (outcome) {
            if (outcome.name === FormOutcomeModel.SAVE_ACTION) {
                return this.disableSaveButton ? false : this.form.isValid;
            }
            if (outcome.name === FormOutcomeModel.COMPLETE_ACTION) {
                return this.disableCompleteButton ? false : this.form.isValid;
            }
            if (outcome.name === FormOutcomeModel.START_PROCESS_ACTION) {
                return this.disableStartProcessButton ? false : this.form.isValid;
            }
            return this.form.isValid;
        }
        return false;
    }
    /**
     * @param {?} outcome
     * @param {?} isFormReadOnly
     * @return {?}
     */
    isOutcomeButtonVisible(outcome, isFormReadOnly) {
        if (outcome && outcome.name) {
            if (outcome.name === FormOutcomeModel.COMPLETE_ACTION) {
                return this.showCompleteButton;
            }
            if (isFormReadOnly) {
                return outcome.isSelected;
            }
            if (outcome.name === FormOutcomeModel.SAVE_ACTION) {
                return this.showSaveButton;
            }
            if (outcome.name === FormOutcomeModel.START_PROCESS_ACTION) {
                return false;
            }
            return true;
        }
        return false;
    }
    /**
     * Invoked when user clicks outcome button.
     * @param {?} outcome Form outcome model
     * @return {?}
     */
    onOutcomeClicked(outcome) {
        if (!this.readOnly && outcome && this.form) {
            if (!this.onExecuteOutcome(outcome)) {
                return false;
            }
            if (outcome.isSystem) {
                if (outcome.id === FormBaseComponent.SAVE_OUTCOME_ID) {
                    this.saveTaskForm();
                    return true;
                }
                if (outcome.id === FormBaseComponent.COMPLETE_OUTCOME_ID) {
                    this.completeTaskForm();
                    return true;
                }
                if (outcome.id === FormBaseComponent.START_PROCESS_OUTCOME_ID) {
                    this.completeTaskForm();
                    return true;
                }
                if (outcome.id === FormBaseComponent.CUSTOM_OUTCOME_ID) {
                    this.onTaskSaved(this.form);
                    this.storeFormAsMetadata();
                    return true;
                }
            }
            else {
                // Note: Activiti is using NAME field rather than ID for outcomes
                if (outcome.name) {
                    this.onTaskSaved(this.form);
                    this.completeTaskForm(outcome.name);
                    return true;
                }
            }
        }
        return false;
    }
    /**
     * @param {?} err
     * @return {?}
     */
    handleError(err) {
        this.error.emit(err);
    }
}
FormBaseComponent.SAVE_OUTCOME_ID = '$save';
FormBaseComponent.COMPLETE_OUTCOME_ID = '$complete';
FormBaseComponent.START_PROCESS_OUTCOME_ID = '$startProcess';
FormBaseComponent.CUSTOM_OUTCOME_ID = '$custom';
FormBaseComponent.COMPLETE_BUTTON_COLOR = 'primary';
FormBaseComponent.COMPLETE_OUTCOME_NAME = 'COMPLETE';
FormBaseComponent.propDecorators = {
    path: [{ type: Input }],
    nameNode: [{ type: Input }],
    showTitle: [{ type: Input }],
    showCompleteButton: [{ type: Input }],
    disableCompleteButton: [{ type: Input }],
    disableSaveButton: [{ type: Input }],
    disableStartProcessButton: [{ type: Input }],
    showSaveButton: [{ type: Input }],
    readOnly: [{ type: Input }],
    showRefreshButton: [{ type: Input }],
    showValidationIcon: [{ type: Input }],
    fieldValidators: [{ type: Input }],
    formError: [{ type: Output }],
    executeOutcome: [{ type: Output }],
    error: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    FormBaseComponent.SAVE_OUTCOME_ID;
    /** @type {?} */
    FormBaseComponent.COMPLETE_OUTCOME_ID;
    /** @type {?} */
    FormBaseComponent.START_PROCESS_OUTCOME_ID;
    /** @type {?} */
    FormBaseComponent.CUSTOM_OUTCOME_ID;
    /** @type {?} */
    FormBaseComponent.COMPLETE_BUTTON_COLOR;
    /** @type {?} */
    FormBaseComponent.COMPLETE_OUTCOME_NAME;
    /**
     * Path of the folder where the metadata will be stored.
     * @type {?}
     */
    FormBaseComponent.prototype.path;
    /**
     * Name to assign to the new node where the metadata are stored.
     * @type {?}
     */
    FormBaseComponent.prototype.nameNode;
    /**
     * Toggle rendering of the form title.
     * @type {?}
     */
    FormBaseComponent.prototype.showTitle;
    /**
     * Toggle rendering of the `Complete` outcome button.
     * @type {?}
     */
    FormBaseComponent.prototype.showCompleteButton;
    /**
     * If true then the `Complete` outcome button is shown but it will be disabled.
     * @type {?}
     */
    FormBaseComponent.prototype.disableCompleteButton;
    /**
     * If true then the `Save` outcome button is shown but will be disabled.
     * @type {?}
     */
    FormBaseComponent.prototype.disableSaveButton;
    /**
     * If true then the `Start Process` outcome button is shown but it will be disabled.
     * @type {?}
     */
    FormBaseComponent.prototype.disableStartProcessButton;
    /**
     * Toggle rendering of the `Save` outcome button.
     * @type {?}
     */
    FormBaseComponent.prototype.showSaveButton;
    /**
     * Toggle readonly state of the form. Forces all form widgets to render as readonly if enabled.
     * @type {?}
     */
    FormBaseComponent.prototype.readOnly;
    /**
     * Toggle rendering of the `Refresh` button.
     * @type {?}
     */
    FormBaseComponent.prototype.showRefreshButton;
    /**
     * Toggle rendering of the validation icon next to the form title.
     * @type {?}
     */
    FormBaseComponent.prototype.showValidationIcon;
    /**
     * Contains a list of form field validator instances.
     * @type {?}
     */
    FormBaseComponent.prototype.fieldValidators;
    /**
     * Emitted when the supplied form values have a validation error.
     * @type {?}
     */
    FormBaseComponent.prototype.formError;
    /**
     * Emitted when any outcome is executed. Default behaviour can be prevented
     * via `event.preventDefault()`.
     * @type {?}
     */
    FormBaseComponent.prototype.executeOutcome;
    /**
     * Emitted when any error occurs.
     * @type {?}
     */
    FormBaseComponent.prototype.error;
    /** @type {?} */
    FormBaseComponent.prototype.form;
    /**
     * @abstract
     * @return {?}
     */
    FormBaseComponent.prototype.onRefreshClicked = function () { };
    /**
     * @abstract
     * @return {?}
     */
    FormBaseComponent.prototype.saveTaskForm = function () { };
    /**
     * @abstract
     * @param {?=} outcome
     * @return {?}
     */
    FormBaseComponent.prototype.completeTaskForm = function (outcome) { };
    /**
     * @abstract
     * @protected
     * @param {?} form
     * @return {?}
     */
    FormBaseComponent.prototype.onTaskSaved = function (form) { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    FormBaseComponent.prototype.storeFormAsMetadata = function () { };
    /**
     * @abstract
     * @protected
     * @param {?} outcome
     * @return {?}
     */
    FormBaseComponent.prototype.onExecuteOutcome = function (outcome) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1iYXNlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy9mb3JtLWJhc2UuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBLE9BQU8sRUFBRSxnQkFBZ0IsRUFBd0QsTUFBTSxXQUFXLENBQUM7QUFDbkcsT0FBTyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7O0FBRTVELE1BQU0sT0FBZ0IsaUJBQWlCO0lBQXZDOzs7O1FBbUJJLGNBQVMsR0FBWSxJQUFJLENBQUM7Ozs7UUFJMUIsdUJBQWtCLEdBQVksSUFBSSxDQUFDOzs7O1FBSW5DLDBCQUFxQixHQUFZLEtBQUssQ0FBQzs7OztRQUl2QyxzQkFBaUIsR0FBWSxLQUFLLENBQUM7Ozs7UUFJbkMsOEJBQXlCLEdBQVksS0FBSyxDQUFDOzs7O1FBSTNDLG1CQUFjLEdBQVksSUFBSSxDQUFDOzs7O1FBSS9CLGFBQVEsR0FBWSxLQUFLLENBQUM7Ozs7UUFJMUIsc0JBQWlCLEdBQVksSUFBSSxDQUFDOzs7O1FBSWxDLHVCQUFrQixHQUFZLElBQUksQ0FBQzs7OztRQVFuQyxjQUFTLEdBQW1DLElBQUksWUFBWSxFQUFvQixDQUFDOzs7OztRQU1qRixtQkFBYyxHQUFtQyxJQUFJLFlBQVksRUFBb0IsQ0FBQzs7OztRQU10RixVQUFLLEdBQXNCLElBQUksWUFBWSxFQUFPLENBQUM7SUEySHZELENBQUM7Ozs7SUF2SEcsdUJBQXVCO1FBQ25CLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7SUFFRCxPQUFPO1FBQ0gsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUNwQyxDQUFDOzs7O0lBRUQsY0FBYzs7WUFDTixZQUFZLEdBQUcsS0FBSztRQUN4QixJQUFJLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtZQUM3QixZQUFZLEdBQUcsSUFBSSxDQUFDO1NBQ3ZCO1FBQ0QsT0FBTyxZQUFZLENBQUM7SUFDeEIsQ0FBQzs7Ozs7SUFFRCxrQkFBa0IsQ0FBQyxXQUFtQjtRQUNsQyxPQUFPLFdBQVcsS0FBSyxpQkFBaUIsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsaUJBQWlCLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUNsSCxDQUFDOzs7OztJQUVELHNCQUFzQixDQUFDLE9BQXlCO1FBQzVDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDcEIsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxJQUFJLE9BQU8sRUFBRTtZQUNULElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUU7Z0JBQy9DLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO2FBQzdEO1lBQ0QsSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLGdCQUFnQixDQUFDLGVBQWUsRUFBRTtnQkFDbkQsT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7YUFDakU7WUFDRCxJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssZ0JBQWdCLENBQUMsb0JBQW9CLEVBQUU7Z0JBQ3hELE9BQU8sSUFBSSxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO2FBQ3JFO1lBQ0QsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztTQUM1QjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7OztJQUVELHNCQUFzQixDQUFDLE9BQXlCLEVBQUUsY0FBdUI7UUFDckUsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLElBQUksRUFBRTtZQUN6QixJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssZ0JBQWdCLENBQUMsZUFBZSxFQUFFO2dCQUNuRCxPQUFPLElBQUksQ0FBQyxrQkFBa0IsQ0FBQzthQUNsQztZQUNELElBQUksY0FBYyxFQUFFO2dCQUNoQixPQUFPLE9BQU8sQ0FBQyxVQUFVLENBQUM7YUFDN0I7WUFDRCxJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssZ0JBQWdCLENBQUMsV0FBVyxFQUFFO2dCQUMvQyxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUM7YUFDOUI7WUFDRCxJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssZ0JBQWdCLENBQUMsb0JBQW9CLEVBQUU7Z0JBQ3hELE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1lBQ0QsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7OztJQU1ELGdCQUFnQixDQUFDLE9BQXlCO1FBQ3RDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxJQUFJLE9BQU8sSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFO1lBRXhDLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLEVBQUU7Z0JBQ2pDLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1lBRUQsSUFBSSxPQUFPLENBQUMsUUFBUSxFQUFFO2dCQUNsQixJQUFJLE9BQU8sQ0FBQyxFQUFFLEtBQUssaUJBQWlCLENBQUMsZUFBZSxFQUFFO29CQUNsRCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7b0JBQ3BCLE9BQU8sSUFBSSxDQUFDO2lCQUNmO2dCQUVELElBQUksT0FBTyxDQUFDLEVBQUUsS0FBSyxpQkFBaUIsQ0FBQyxtQkFBbUIsRUFBRTtvQkFDdEQsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7b0JBQ3hCLE9BQU8sSUFBSSxDQUFDO2lCQUNmO2dCQUVELElBQUksT0FBTyxDQUFDLEVBQUUsS0FBSyxpQkFBaUIsQ0FBQyx3QkFBd0IsRUFBRTtvQkFDM0QsSUFBSSxDQUFDLGdCQUFnQixFQUFFLENBQUM7b0JBQ3hCLE9BQU8sSUFBSSxDQUFDO2lCQUNmO2dCQUVELElBQUksT0FBTyxDQUFDLEVBQUUsS0FBSyxpQkFBaUIsQ0FBQyxpQkFBaUIsRUFBRTtvQkFDcEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7b0JBQzVCLElBQUksQ0FBQyxtQkFBbUIsRUFBRSxDQUFDO29CQUMzQixPQUFPLElBQUksQ0FBQztpQkFDZjthQUNKO2lCQUFNO2dCQUNILGlFQUFpRTtnQkFDakUsSUFBSSxPQUFPLENBQUMsSUFBSSxFQUFFO29CQUNkLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUM1QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUNwQyxPQUFPLElBQUksQ0FBQztpQkFDZjthQUNKO1NBQ0o7UUFFRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxHQUFRO1FBQ2hCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ3pCLENBQUM7O0FBbkxNLGlDQUFlLEdBQVcsT0FBTyxDQUFDO0FBQ2xDLHFDQUFtQixHQUFXLFdBQVcsQ0FBQztBQUMxQywwQ0FBd0IsR0FBVyxlQUFlLENBQUM7QUFDbkQsbUNBQWlCLEdBQVcsU0FBUyxDQUFDO0FBQ3RDLHVDQUFxQixHQUFXLFNBQVMsQ0FBQztBQUMxQyx1Q0FBcUIsR0FBVyxVQUFVLENBQUM7O21CQUdqRCxLQUFLO3VCQUlMLEtBQUs7d0JBSUwsS0FBSztpQ0FJTCxLQUFLO29DQUlMLEtBQUs7Z0NBSUwsS0FBSzt3Q0FJTCxLQUFLOzZCQUlMLEtBQUs7dUJBSUwsS0FBSztnQ0FJTCxLQUFLO2lDQUlMLEtBQUs7OEJBSUwsS0FBSzt3QkFJTCxNQUFNOzZCQU1OLE1BQU07b0JBTU4sTUFBTTs7OztJQXBFUCxrQ0FBeUM7O0lBQ3pDLHNDQUFpRDs7SUFDakQsMkNBQTBEOztJQUMxRCxvQ0FBNkM7O0lBQzdDLHdDQUFpRDs7SUFDakQsd0NBQWtEOzs7OztJQUdsRCxpQ0FDYTs7Ozs7SUFHYixxQ0FDaUI7Ozs7O0lBR2pCLHNDQUMwQjs7Ozs7SUFHMUIsK0NBQ21DOzs7OztJQUduQyxrREFDdUM7Ozs7O0lBR3ZDLDhDQUNtQzs7Ozs7SUFHbkMsc0RBQzJDOzs7OztJQUczQywyQ0FDK0I7Ozs7O0lBRy9CLHFDQUMwQjs7Ozs7SUFHMUIsOENBQ2tDOzs7OztJQUdsQywrQ0FDbUM7Ozs7O0lBR25DLDRDQUNzQzs7Ozs7SUFHdEMsc0NBQ2lGOzs7Ozs7SUFLakYsMkNBQ3NGOzs7OztJQUt0RixrQ0FDbUQ7O0lBRW5ELGlDQUFvQjs7Ozs7SUE4R3BCLCtEQUE0Qjs7Ozs7SUFFNUIsMkRBQXdCOzs7Ozs7SUFFeEIsc0VBQTRDOzs7Ozs7O0lBRTVDLDhEQUFvRDs7Ozs7O0lBRXBELGtFQUF5Qzs7Ozs7OztJQUV6QyxzRUFBK0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgRm9ybUJhc2VNb2RlbCB9IGZyb20gJy4vZm9ybS1iYXNlLm1vZGVsJztcclxuaW1wb3J0IHsgRm9ybU91dGNvbWVNb2RlbCwgRm9ybUZpZWxkVmFsaWRhdG9yLCBGb3JtRmllbGRNb2RlbCwgRm9ybU91dGNvbWVFdmVudCB9IGZyb20gJy4vd2lkZ2V0cyc7XHJcbmltcG9ydCB7IEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEZvcm1CYXNlQ29tcG9uZW50IHtcclxuXHJcbiAgICBzdGF0aWMgU0FWRV9PVVRDT01FX0lEOiBzdHJpbmcgPSAnJHNhdmUnO1xyXG4gICAgc3RhdGljIENPTVBMRVRFX09VVENPTUVfSUQ6IHN0cmluZyA9ICckY29tcGxldGUnO1xyXG4gICAgc3RhdGljIFNUQVJUX1BST0NFU1NfT1VUQ09NRV9JRDogc3RyaW5nID0gJyRzdGFydFByb2Nlc3MnO1xyXG4gICAgc3RhdGljIENVU1RPTV9PVVRDT01FX0lEOiBzdHJpbmcgPSAnJGN1c3RvbSc7XHJcbiAgICBzdGF0aWMgQ09NUExFVEVfQlVUVE9OX0NPTE9SOiBzdHJpbmcgPSAncHJpbWFyeSc7XHJcbiAgICBzdGF0aWMgQ09NUExFVEVfT1VUQ09NRV9OQU1FOiBzdHJpbmcgPSAnQ09NUExFVEUnO1xyXG5cclxuICAgIC8qKiBQYXRoIG9mIHRoZSBmb2xkZXIgd2hlcmUgdGhlIG1ldGFkYXRhIHdpbGwgYmUgc3RvcmVkLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHBhdGg6IHN0cmluZztcclxuXHJcbiAgICAvKiogTmFtZSB0byBhc3NpZ24gdG8gdGhlIG5ldyBub2RlIHdoZXJlIHRoZSBtZXRhZGF0YSBhcmUgc3RvcmVkLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIG5hbWVOb2RlOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqIFRvZ2dsZSByZW5kZXJpbmcgb2YgdGhlIGZvcm0gdGl0bGUuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgc2hvd1RpdGxlOiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgICAvKiogVG9nZ2xlIHJlbmRlcmluZyBvZiB0aGUgYENvbXBsZXRlYCBvdXRjb21lIGJ1dHRvbi4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzaG93Q29tcGxldGVCdXR0b246IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICAgIC8qKiBJZiB0cnVlIHRoZW4gdGhlIGBDb21wbGV0ZWAgb3V0Y29tZSBidXR0b24gaXMgc2hvd24gYnV0IGl0IHdpbGwgYmUgZGlzYWJsZWQuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgZGlzYWJsZUNvbXBsZXRlQnV0dG9uOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgLyoqIElmIHRydWUgdGhlbiB0aGUgYFNhdmVgIG91dGNvbWUgYnV0dG9uIGlzIHNob3duIGJ1dCB3aWxsIGJlIGRpc2FibGVkLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGRpc2FibGVTYXZlQnV0dG9uOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgLyoqIElmIHRydWUgdGhlbiB0aGUgYFN0YXJ0IFByb2Nlc3NgIG91dGNvbWUgYnV0dG9uIGlzIHNob3duIGJ1dCBpdCB3aWxsIGJlIGRpc2FibGVkLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGRpc2FibGVTdGFydFByb2Nlc3NCdXR0b246IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICAvKiogVG9nZ2xlIHJlbmRlcmluZyBvZiB0aGUgYFNhdmVgIG91dGNvbWUgYnV0dG9uLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHNob3dTYXZlQnV0dG9uOiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgICAvKiogVG9nZ2xlIHJlYWRvbmx5IHN0YXRlIG9mIHRoZSBmb3JtLiBGb3JjZXMgYWxsIGZvcm0gd2lkZ2V0cyB0byByZW5kZXIgYXMgcmVhZG9ubHkgaWYgZW5hYmxlZC4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICByZWFkT25seTogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIC8qKiBUb2dnbGUgcmVuZGVyaW5nIG9mIHRoZSBgUmVmcmVzaGAgYnV0dG9uLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHNob3dSZWZyZXNoQnV0dG9uOiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgICAvKiogVG9nZ2xlIHJlbmRlcmluZyBvZiB0aGUgdmFsaWRhdGlvbiBpY29uIG5leHQgdG8gdGhlIGZvcm0gdGl0bGUuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgc2hvd1ZhbGlkYXRpb25JY29uOiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgICAvKiogQ29udGFpbnMgYSBsaXN0IG9mIGZvcm0gZmllbGQgdmFsaWRhdG9yIGluc3RhbmNlcy4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBmaWVsZFZhbGlkYXRvcnM6IEZvcm1GaWVsZFZhbGlkYXRvcltdO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIHN1cHBsaWVkIGZvcm0gdmFsdWVzIGhhdmUgYSB2YWxpZGF0aW9uIGVycm9yLiAqL1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICBmb3JtRXJyb3I6IEV2ZW50RW1pdHRlcjxGb3JtRmllbGRNb2RlbFtdPiA9IG5ldyBFdmVudEVtaXR0ZXI8Rm9ybUZpZWxkTW9kZWxbXT4oKTtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIGFueSBvdXRjb21lIGlzIGV4ZWN1dGVkLiBEZWZhdWx0IGJlaGF2aW91ciBjYW4gYmUgcHJldmVudGVkXHJcbiAgICAgKiB2aWEgYGV2ZW50LnByZXZlbnREZWZhdWx0KClgLlxyXG4gICAgICovXHJcbiAgICBAT3V0cHV0KClcclxuICAgIGV4ZWN1dGVPdXRjb21lOiBFdmVudEVtaXR0ZXI8Rm9ybU91dGNvbWVFdmVudD4gPSBuZXcgRXZlbnRFbWl0dGVyPEZvcm1PdXRjb21lRXZlbnQ+KCk7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBFbWl0dGVkIHdoZW4gYW55IGVycm9yIG9jY3Vycy5cclxuICAgICAqL1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICBlcnJvcjogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICBmb3JtOiBGb3JtQmFzZU1vZGVsO1xyXG5cclxuICAgIGdldFBhcnNlZEZvcm1EZWZpbml0aW9uKCk6IEZvcm1CYXNlQ29tcG9uZW50IHtcclxuICAgICAgICByZXR1cm4gdGhpcztcclxuICAgIH1cclxuXHJcbiAgICBoYXNGb3JtKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZvcm0gPyB0cnVlIDogZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaXNUaXRsZUVuYWJsZWQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgbGV0IHRpdGxlRW5hYmxlZCA9IGZhbHNlO1xyXG4gICAgICAgIGlmICh0aGlzLnNob3dUaXRsZSAmJiB0aGlzLmZvcm0pIHtcclxuICAgICAgICAgICAgdGl0bGVFbmFibGVkID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRpdGxlRW5hYmxlZDtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDb2xvckZvck91dGNvbWUob3V0Y29tZU5hbWU6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIG91dGNvbWVOYW1lID09PSBGb3JtQmFzZUNvbXBvbmVudC5DT01QTEVURV9PVVRDT01FX05BTUUgPyBGb3JtQmFzZUNvbXBvbmVudC5DT01QTEVURV9CVVRUT05fQ09MT1IgOiAnJztcclxuICAgIH1cclxuXHJcbiAgICBpc091dGNvbWVCdXR0b25FbmFibGVkKG91dGNvbWU6IEZvcm1PdXRjb21lTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodGhpcy5mb3JtLnJlYWRPbmx5KSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChvdXRjb21lKSB7XHJcbiAgICAgICAgICAgIGlmIChvdXRjb21lLm5hbWUgPT09IEZvcm1PdXRjb21lTW9kZWwuU0FWRV9BQ1RJT04pIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmRpc2FibGVTYXZlQnV0dG9uID8gZmFsc2UgOiB0aGlzLmZvcm0uaXNWYWxpZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAob3V0Y29tZS5uYW1lID09PSBGb3JtT3V0Y29tZU1vZGVsLkNPTVBMRVRFX0FDVElPTikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZGlzYWJsZUNvbXBsZXRlQnV0dG9uID8gZmFsc2UgOiB0aGlzLmZvcm0uaXNWYWxpZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAob3V0Y29tZS5uYW1lID09PSBGb3JtT3V0Y29tZU1vZGVsLlNUQVJUX1BST0NFU1NfQUNUSU9OKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5kaXNhYmxlU3RhcnRQcm9jZXNzQnV0dG9uID8gZmFsc2UgOiB0aGlzLmZvcm0uaXNWYWxpZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5mb3JtLmlzVmFsaWQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBpc091dGNvbWVCdXR0b25WaXNpYmxlKG91dGNvbWU6IEZvcm1PdXRjb21lTW9kZWwsIGlzRm9ybVJlYWRPbmx5OiBib29sZWFuKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKG91dGNvbWUgJiYgb3V0Y29tZS5uYW1lKSB7XHJcbiAgICAgICAgICAgIGlmIChvdXRjb21lLm5hbWUgPT09IEZvcm1PdXRjb21lTW9kZWwuQ09NUExFVEVfQUNUSU9OKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5zaG93Q29tcGxldGVCdXR0b247XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKGlzRm9ybVJlYWRPbmx5KSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gb3V0Y29tZS5pc1NlbGVjdGVkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChvdXRjb21lLm5hbWUgPT09IEZvcm1PdXRjb21lTW9kZWwuU0FWRV9BQ1RJT04pIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnNob3dTYXZlQnV0dG9uO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChvdXRjb21lLm5hbWUgPT09IEZvcm1PdXRjb21lTW9kZWwuU1RBUlRfUFJPQ0VTU19BQ1RJT04pIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogSW52b2tlZCB3aGVuIHVzZXIgY2xpY2tzIG91dGNvbWUgYnV0dG9uLlxyXG4gICAgICogQHBhcmFtIG91dGNvbWUgRm9ybSBvdXRjb21lIG1vZGVsXHJcbiAgICAgKi9cclxuICAgIG9uT3V0Y29tZUNsaWNrZWQob3V0Y29tZTogRm9ybU91dGNvbWVNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICghdGhpcy5yZWFkT25seSAmJiBvdXRjb21lICYmIHRoaXMuZm9ybSkge1xyXG5cclxuICAgICAgICAgICAgaWYgKCF0aGlzLm9uRXhlY3V0ZU91dGNvbWUob3V0Y29tZSkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKG91dGNvbWUuaXNTeXN0ZW0pIHtcclxuICAgICAgICAgICAgICAgIGlmIChvdXRjb21lLmlkID09PSBGb3JtQmFzZUNvbXBvbmVudC5TQVZFX09VVENPTUVfSUQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnNhdmVUYXNrRm9ybSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmIChvdXRjb21lLmlkID09PSBGb3JtQmFzZUNvbXBvbmVudC5DT01QTEVURV9PVVRDT01FX0lEKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb21wbGV0ZVRhc2tGb3JtKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKG91dGNvbWUuaWQgPT09IEZvcm1CYXNlQ29tcG9uZW50LlNUQVJUX1BST0NFU1NfT1VUQ09NRV9JRCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29tcGxldGVUYXNrRm9ybSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmIChvdXRjb21lLmlkID09PSBGb3JtQmFzZUNvbXBvbmVudC5DVVNUT01fT1VUQ09NRV9JRCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub25UYXNrU2F2ZWQodGhpcy5mb3JtKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN0b3JlRm9ybUFzTWV0YWRhdGEoKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIC8vIE5vdGU6IEFjdGl2aXRpIGlzIHVzaW5nIE5BTUUgZmllbGQgcmF0aGVyIHRoYW4gSUQgZm9yIG91dGNvbWVzXHJcbiAgICAgICAgICAgICAgICBpZiAob3V0Y29tZS5uYW1lKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vblRhc2tTYXZlZCh0aGlzLmZvcm0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29tcGxldGVUYXNrRm9ybShvdXRjb21lLm5hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaGFuZGxlRXJyb3IoZXJyOiBhbnkpOiBhbnkge1xyXG4gICAgICAgIHRoaXMuZXJyb3IuZW1pdChlcnIpO1xyXG4gICAgfVxyXG5cclxuICAgIGFic3RyYWN0IG9uUmVmcmVzaENsaWNrZWQoKTtcclxuXHJcbiAgICBhYnN0cmFjdCBzYXZlVGFza0Zvcm0oKTtcclxuXHJcbiAgICBhYnN0cmFjdCBjb21wbGV0ZVRhc2tGb3JtKG91dGNvbWU/OiBzdHJpbmcpO1xyXG5cclxuICAgIHByb3RlY3RlZCBhYnN0cmFjdCBvblRhc2tTYXZlZChmb3JtOiBGb3JtQmFzZU1vZGVsKTtcclxuXHJcbiAgICBwcm90ZWN0ZWQgYWJzdHJhY3Qgc3RvcmVGb3JtQXNNZXRhZGF0YSgpO1xyXG5cclxuICAgIHByb3RlY3RlZCBhYnN0cmFjdCBvbkV4ZWN1dGVPdXRjb21lKG91dGNvbWU6IEZvcm1PdXRjb21lTW9kZWwpO1xyXG59XHJcbiJdfQ==