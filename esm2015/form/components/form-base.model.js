/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ContainerModel } from './widgets/core/container.model';
/**
 * @abstract
 */
export class FormBaseModel {
    constructor() {
        this.values = {};
        this.tabs = [];
        this.fields = [];
        this.outcomes = [];
        this.readOnly = false;
        this.isValid = true;
    }
    /**
     * @return {?}
     */
    hasTabs() {
        return this.tabs && this.tabs.length > 0;
    }
    /**
     * @return {?}
     */
    hasFields() {
        return this.fields && this.fields.length > 0;
    }
    /**
     * @return {?}
     */
    hasOutcomes() {
        return this.outcomes && this.outcomes.length > 0;
    }
    /**
     * @param {?} fieldId
     * @return {?}
     */
    getFieldById(fieldId) {
        return this.getFormFields().find((/**
         * @param {?} field
         * @return {?}
         */
        (field) => field.id === fieldId));
    }
    // TODO: consider evaluating and caching once the form is loaded
    /**
     * @return {?}
     */
    getFormFields() {
        /** @type {?} */
        const formFieldModel = [];
        for (let i = 0; i < this.fields.length; i++) {
            /** @type {?} */
            const field = this.fields[i];
            if (field instanceof ContainerModel) {
                /** @type {?} */
                const container = (/** @type {?} */ (field));
                formFieldModel.push(container.field);
                container.field.columns.forEach((/**
                 * @param {?} column
                 * @return {?}
                 */
                (column) => {
                    formFieldModel.push(...column.fields);
                }));
            }
        }
        return formFieldModel;
    }
    /**
     * @return {?}
     */
    markAsInvalid() {
        this.isValid = false;
    }
}
FormBaseModel.UNSET_TASK_NAME = 'Nameless task';
FormBaseModel.SAVE_OUTCOME = '$save';
FormBaseModel.COMPLETE_OUTCOME = '$complete';
FormBaseModel.START_PROCESS_OUTCOME = '$startProcess';
if (false) {
    /** @type {?} */
    FormBaseModel.UNSET_TASK_NAME;
    /** @type {?} */
    FormBaseModel.SAVE_OUTCOME;
    /** @type {?} */
    FormBaseModel.COMPLETE_OUTCOME;
    /** @type {?} */
    FormBaseModel.START_PROCESS_OUTCOME;
    /** @type {?} */
    FormBaseModel.prototype.json;
    /** @type {?} */
    FormBaseModel.prototype.values;
    /** @type {?} */
    FormBaseModel.prototype.tabs;
    /** @type {?} */
    FormBaseModel.prototype.fields;
    /** @type {?} */
    FormBaseModel.prototype.outcomes;
    /** @type {?} */
    FormBaseModel.prototype.className;
    /** @type {?} */
    FormBaseModel.prototype.readOnly;
    /** @type {?} */
    FormBaseModel.prototype.taskName;
    /** @type {?} */
    FormBaseModel.prototype.isValid;
    /**
     * @abstract
     * @return {?}
     */
    FormBaseModel.prototype.validateForm = function () { };
    /**
     * @abstract
     * @param {?} field
     * @return {?}
     */
    FormBaseModel.prototype.validateField = function (field) { };
    /**
     * @abstract
     * @param {?} field
     * @return {?}
     */
    FormBaseModel.prototype.onFormFieldChanged = function (field) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1iYXNlLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL2Zvcm0tYmFzZS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXNCQSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7Ozs7QUFFaEUsTUFBTSxPQUFnQixhQUFhO0lBQW5DO1FBU0ksV0FBTSxHQUFlLEVBQUUsQ0FBQztRQUN4QixTQUFJLEdBQWUsRUFBRSxDQUFDO1FBQ3RCLFdBQU0sR0FBc0IsRUFBRSxDQUFDO1FBQy9CLGFBQVEsR0FBdUIsRUFBRSxDQUFDO1FBR2xDLGFBQVEsR0FBWSxLQUFLLENBQUM7UUFHMUIsWUFBTyxHQUFZLElBQUksQ0FBQztJQTZDNUIsQ0FBQzs7OztJQTNDRyxPQUFPO1FBQ0gsT0FBTyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUM3QyxDQUFDOzs7O0lBRUQsU0FBUztRQUNMLE9BQU8sSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDakQsQ0FBQzs7OztJQUVELFdBQVc7UUFDUCxPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQ3JELENBQUM7Ozs7O0lBRUQsWUFBWSxDQUFDLE9BQWU7UUFDeEIsT0FBTyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUMsSUFBSTs7OztRQUFDLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxLQUFLLENBQUMsRUFBRSxLQUFLLE9BQU8sRUFBQyxDQUFDO0lBQ3RFLENBQUM7Ozs7O0lBR0QsYUFBYTs7Y0FDSCxjQUFjLEdBQXFCLEVBQUU7UUFFM0MsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOztrQkFDbkMsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO1lBRTVCLElBQUksS0FBSyxZQUFZLGNBQWMsRUFBRTs7c0JBQzNCLFNBQVMsR0FBRyxtQkFBaUIsS0FBSyxFQUFBO2dCQUN4QyxjQUFjLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFFckMsU0FBUyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsT0FBTzs7OztnQkFBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO29CQUN2QyxjQUFjLENBQUMsSUFBSSxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUMxQyxDQUFDLEVBQUMsQ0FBQzthQUNOO1NBQ0o7UUFFRCxPQUFPLGNBQWMsQ0FBQztJQUMxQixDQUFDOzs7O0lBRUQsYUFBYTtRQUNULElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO0lBQ3pCLENBQUM7O0FBeERNLDZCQUFlLEdBQVcsZUFBZSxDQUFDO0FBQzFDLDBCQUFZLEdBQVcsT0FBTyxDQUFDO0FBQy9CLDhCQUFnQixHQUFXLFdBQVcsQ0FBQztBQUN2QyxtQ0FBcUIsR0FBVyxlQUFlLENBQUM7OztJQUh2RCw4QkFBaUQ7O0lBQ2pELDJCQUFzQzs7SUFDdEMsK0JBQThDOztJQUM5QyxvQ0FBdUQ7O0lBRXZELDZCQUFVOztJQUVWLCtCQUF3Qjs7SUFDeEIsNkJBQXNCOztJQUN0QiwrQkFBK0I7O0lBQy9CLGlDQUFrQzs7SUFFbEMsa0NBQWtCOztJQUNsQixpQ0FBMEI7O0lBQzFCLGlDQUFTOztJQUVULGdDQUF3Qjs7Ozs7SUEwQ3hCLHVEQUF3Qjs7Ozs7O0lBQ3hCLDZEQUE4Qzs7Ozs7O0lBQzlDLGtFQUFtRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBGb3JtVmFsdWVzIH0gZnJvbSAnLi93aWRnZXRzL2NvcmUvZm9ybS12YWx1ZXMnO1xyXG5pbXBvcnQgeyBUYWJNb2RlbCB9IGZyb20gJy4vd2lkZ2V0cy9jb3JlL3RhYi5tb2RlbCc7XHJcbmltcG9ydCB7IEZvcm1XaWRnZXRNb2RlbCB9IGZyb20gJy4vd2lkZ2V0cy9jb3JlL2Zvcm0td2lkZ2V0Lm1vZGVsJztcclxuaW1wb3J0IHsgRm9ybU91dGNvbWVNb2RlbCB9IGZyb20gJy4vd2lkZ2V0cy9jb3JlL2Zvcm0tb3V0Y29tZS5tb2RlbCc7XHJcbmltcG9ydCB7IEZvcm1GaWVsZE1vZGVsIH0gZnJvbSAnLi93aWRnZXRzL2NvcmUvZm9ybS1maWVsZC5tb2RlbCc7XHJcbmltcG9ydCB7IENvbnRhaW5lck1vZGVsIH0gZnJvbSAnLi93aWRnZXRzL2NvcmUvY29udGFpbmVyLm1vZGVsJztcclxuXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBGb3JtQmFzZU1vZGVsIHtcclxuXHJcbiAgICBzdGF0aWMgVU5TRVRfVEFTS19OQU1FOiBzdHJpbmcgPSAnTmFtZWxlc3MgdGFzayc7XHJcbiAgICBzdGF0aWMgU0FWRV9PVVRDT01FOiBzdHJpbmcgPSAnJHNhdmUnO1xyXG4gICAgc3RhdGljIENPTVBMRVRFX09VVENPTUU6IHN0cmluZyA9ICckY29tcGxldGUnO1xyXG4gICAgc3RhdGljIFNUQVJUX1BST0NFU1NfT1VUQ09NRTogc3RyaW5nID0gJyRzdGFydFByb2Nlc3MnO1xyXG5cclxuICAgIGpzb246IGFueTtcclxuXHJcbiAgICB2YWx1ZXM6IEZvcm1WYWx1ZXMgPSB7fTtcclxuICAgIHRhYnM6IFRhYk1vZGVsW10gPSBbXTtcclxuICAgIGZpZWxkczogRm9ybVdpZGdldE1vZGVsW10gPSBbXTtcclxuICAgIG91dGNvbWVzOiBGb3JtT3V0Y29tZU1vZGVsW10gPSBbXTtcclxuXHJcbiAgICBjbGFzc05hbWU6IHN0cmluZztcclxuICAgIHJlYWRPbmx5OiBib29sZWFuID0gZmFsc2U7XHJcbiAgICB0YXNrTmFtZTtcclxuXHJcbiAgICBpc1ZhbGlkOiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgICBoYXNUYWJzKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnRhYnMgJiYgdGhpcy50YWJzLmxlbmd0aCA+IDA7XHJcbiAgICB9XHJcblxyXG4gICAgaGFzRmllbGRzKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZpZWxkcyAmJiB0aGlzLmZpZWxkcy5sZW5ndGggPiAwO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc091dGNvbWVzKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm91dGNvbWVzICYmIHRoaXMub3V0Y29tZXMubGVuZ3RoID4gMDtcclxuICAgIH1cclxuXHJcbiAgICBnZXRGaWVsZEJ5SWQoZmllbGRJZDogc3RyaW5nKTogRm9ybUZpZWxkTW9kZWwge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldEZvcm1GaWVsZHMoKS5maW5kKChmaWVsZCkgPT4gZmllbGQuaWQgPT09IGZpZWxkSWQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFRPRE86IGNvbnNpZGVyIGV2YWx1YXRpbmcgYW5kIGNhY2hpbmcgb25jZSB0aGUgZm9ybSBpcyBsb2FkZWRcclxuICAgIGdldEZvcm1GaWVsZHMoKTogRm9ybUZpZWxkTW9kZWxbXSB7XHJcbiAgICAgICAgY29uc3QgZm9ybUZpZWxkTW9kZWw6IEZvcm1GaWVsZE1vZGVsW10gPSBbXTtcclxuXHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCB0aGlzLmZpZWxkcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBjb25zdCBmaWVsZCA9IHRoaXMuZmllbGRzW2ldO1xyXG5cclxuICAgICAgICAgICAgaWYgKGZpZWxkIGluc3RhbmNlb2YgQ29udGFpbmVyTW9kZWwpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNvbnRhaW5lciA9IDxDb250YWluZXJNb2RlbD4gZmllbGQ7XHJcbiAgICAgICAgICAgICAgICBmb3JtRmllbGRNb2RlbC5wdXNoKGNvbnRhaW5lci5maWVsZCk7XHJcblxyXG4gICAgICAgICAgICAgICAgY29udGFpbmVyLmZpZWxkLmNvbHVtbnMuZm9yRWFjaCgoY29sdW1uKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9ybUZpZWxkTW9kZWwucHVzaCguLi5jb2x1bW4uZmllbGRzKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZm9ybUZpZWxkTW9kZWw7XHJcbiAgICB9XHJcblxyXG4gICAgbWFya0FzSW52YWxpZCgpIHtcclxuICAgICAgICB0aGlzLmlzVmFsaWQgPSBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBhYnN0cmFjdCB2YWxpZGF0ZUZvcm0oKTtcclxuICAgIGFic3RyYWN0IHZhbGlkYXRlRmllbGQoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTtcclxuICAgIGFic3RyYWN0IG9uRm9ybUZpZWxkQ2hhbmdlZChmaWVsZDogRm9ybUZpZWxkTW9kZWwpO1xyXG59XHJcbiJdfQ==