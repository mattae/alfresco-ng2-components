/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Compiler, Component, ComponentFactoryResolver, Input, NgModule, ViewChild, ViewContainerRef, ViewEncapsulation } from '@angular/core';
import { FormRenderingService } from './../../services/form-rendering.service';
import { WidgetVisibilityService } from './../../services/widget-visibility.service';
import { FormFieldModel } from './../widgets/core/form-field.model';
export class FormFieldComponent {
    /**
     * @param {?} formRenderingService
     * @param {?} componentFactoryResolver
     * @param {?} visibilityService
     * @param {?} compiler
     */
    constructor(formRenderingService, componentFactoryResolver, visibilityService, compiler) {
        this.formRenderingService = formRenderingService;
        this.componentFactoryResolver = componentFactoryResolver;
        this.visibilityService = visibilityService;
        this.compiler = compiler;
        /**
         * Contains all the necessary data needed to determine what UI Widget
         * to use when rendering the field in the form. You would typically not
         * create this data manually but instead create the form in APS and export
         * it to get to all the `FormFieldModel` definitions.
         */
        this.field = null;
        this.focus = false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        /** @type {?} */
        const w = window;
        if (w.adf === undefined) {
            w.adf = {};
        }
        /** @type {?} */
        const originalField = this.getField();
        if (originalField) {
            /** @type {?} */
            const customTemplate = this.field.form.customFieldTemplates[originalField.type];
            if (customTemplate && this.hasController(originalField.type)) {
                /** @type {?} */
                const factory = this.getComponentFactorySync(originalField.type, customTemplate);
                this.componentRef = this.container.createComponent(factory);
                /** @type {?} */
                const instance = this.componentRef.instance;
                if (instance) {
                    instance.field = originalField;
                }
            }
            else {
                /** @type {?} */
                const componentType = this.formRenderingService.resolveComponentType(originalField);
                if (componentType) {
                    /** @type {?} */
                    const factory = this.componentFactoryResolver.resolveComponentFactory(componentType);
                    this.componentRef = this.container.createComponent(factory);
                    /** @type {?} */
                    const instance = (/** @type {?} */ (this.componentRef.instance));
                    instance.field = this.field;
                    instance.fieldChanged.subscribe((/**
                     * @param {?} field
                     * @return {?}
                     */
                    (field) => {
                        if (field && this.field.form) {
                            this.visibilityService.refreshVisibility(field.form);
                            field.form.onFormFieldChanged(field);
                        }
                    }));
                }
            }
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.componentRef) {
            this.componentRef.destroy();
            this.componentRef = null;
        }
    }
    /**
     * @private
     * @return {?}
     */
    getField() {
        if (this.field && this.field.params) {
            /** @type {?} */
            const wrappedField = this.field.params.field;
            if (wrappedField && wrappedField.type) {
                return wrappedField;
            }
        }
        return this.field;
    }
    /**
     * @private
     * @param {?} type
     * @return {?}
     */
    hasController(type) {
        return (adf && adf.components && adf.components[type]);
    }
    /**
     * @private
     * @param {?} type
     * @param {?} template
     * @return {?}
     */
    getComponentFactorySync(type, template) {
        /** @type {?} */
        const componentInfo = adf.components[type];
        if (componentInfo.factory) {
            return componentInfo.factory;
        }
        /** @type {?} */
        const metadata = {
            selector: `runtime-component-${type}`,
            template: template
        };
        /** @type {?} */
        const factory = this.createComponentFactorySync(this.compiler, metadata, componentInfo.class);
        componentInfo.factory = factory;
        return factory;
    }
    /**
     * @private
     * @param {?} compiler
     * @param {?} metadata
     * @param {?} componentClass
     * @return {?}
     */
    createComponentFactorySync(compiler, metadata, componentClass) {
        /** @type {?} */
        const cmpClass = componentClass || class RuntimeComponent {
        };
        /** @type {?} */
        const decoratedCmp = Component(metadata)(cmpClass);
        class RuntimeComponentModule {
        }
        RuntimeComponentModule.decorators = [
            { type: NgModule, args: [{ imports: [], declarations: [decoratedCmp] },] },
        ];
        /** @type {?} */
        const module = compiler.compileModuleAndAllComponentsSync(RuntimeComponentModule);
        return module.componentFactories.find((/**
         * @param {?} x
         * @return {?}
         */
        (x) => x.componentType === decoratedCmp));
    }
    /**
     * @return {?}
     */
    focusToggle() {
        this.focus = !this.focus;
    }
}
FormFieldComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-form-field',
                template: `
        <div [id]="'field-'+field?.id+'-container'"
            [hidden]="!field?.isVisible"
            [class.adf-focus]="focus"
            (focusin)="focusToggle()"
            (focusout)="focusToggle()">
            <div #container></div>
        </div>
    `,
                encapsulation: ViewEncapsulation.None
            }] }
];
/** @nocollapse */
FormFieldComponent.ctorParameters = () => [
    { type: FormRenderingService },
    { type: ComponentFactoryResolver },
    { type: WidgetVisibilityService },
    { type: Compiler }
];
FormFieldComponent.propDecorators = {
    container: [{ type: ViewChild, args: ['container', { read: ViewContainerRef, static: true },] }],
    field: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    FormFieldComponent.prototype.container;
    /**
     * Contains all the necessary data needed to determine what UI Widget
     * to use when rendering the field in the form. You would typically not
     * create this data manually but instead create the form in APS and export
     * it to get to all the `FormFieldModel` definitions.
     * @type {?}
     */
    FormFieldComponent.prototype.field;
    /** @type {?} */
    FormFieldComponent.prototype.componentRef;
    /** @type {?} */
    FormFieldComponent.prototype.focus;
    /**
     * @type {?}
     * @private
     */
    FormFieldComponent.prototype.formRenderingService;
    /**
     * @type {?}
     * @private
     */
    FormFieldComponent.prototype.componentFactoryResolver;
    /**
     * @type {?}
     * @private
     */
    FormFieldComponent.prototype.visibilityService;
    /**
     * @type {?}
     * @private
     */
    FormFieldComponent.prototype.compiler;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvZm9ybS1maWVsZC9mb3JtLWZpZWxkLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQ0EsT0FBTyxFQUNILFFBQVEsRUFDUixTQUFTLEVBQ1Qsd0JBQXdCLEVBRXhCLEtBQUssRUFFTCxRQUFRLEVBR1IsU0FBUyxFQUNULGdCQUFnQixFQUNoQixpQkFBaUIsRUFDcEIsTUFBTSxlQUFlLENBQUM7QUFFdkIsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDL0UsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDckYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBa0JwRSxNQUFNLE9BQU8sa0JBQWtCOzs7Ozs7O0lBaUIzQixZQUFvQixvQkFBMEMsRUFDMUMsd0JBQWtELEVBQ2xELGlCQUEwQyxFQUMxQyxRQUFrQjtRQUhsQix5QkFBb0IsR0FBcEIsb0JBQW9CLENBQXNCO1FBQzFDLDZCQUF3QixHQUF4Qix3QkFBd0IsQ0FBMEI7UUFDbEQsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUF5QjtRQUMxQyxhQUFRLEdBQVIsUUFBUSxDQUFVOzs7Ozs7O1FBVHRDLFVBQUssR0FBbUIsSUFBSSxDQUFDO1FBSTdCLFVBQUssR0FBWSxLQUFLLENBQUM7SUFNdkIsQ0FBQzs7OztJQUVELFFBQVE7O2NBQ0UsQ0FBQyxHQUFRLE1BQU07UUFDckIsSUFBSSxDQUFDLENBQUMsR0FBRyxLQUFLLFNBQVMsRUFBRTtZQUNyQixDQUFDLENBQUMsR0FBRyxHQUFHLEVBQUUsQ0FBQztTQUNkOztjQUNLLGFBQWEsR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFO1FBQ3JDLElBQUksYUFBYSxFQUFFOztrQkFDVCxjQUFjLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQztZQUMvRSxJQUFJLGNBQWMsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBRTs7c0JBQ3BELE9BQU8sR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxjQUFjLENBQUM7Z0JBQ2hGLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7O3NCQUN0RCxRQUFRLEdBQVEsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRO2dCQUNoRCxJQUFJLFFBQVEsRUFBRTtvQkFDVixRQUFRLENBQUMsS0FBSyxHQUFHLGFBQWEsQ0FBQztpQkFDbEM7YUFDSjtpQkFBTTs7c0JBQ0csYUFBYSxHQUFHLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxvQkFBb0IsQ0FBQyxhQUFhLENBQUM7Z0JBQ25GLElBQUksYUFBYSxFQUFFOzswQkFDVCxPQUFPLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLHVCQUF1QixDQUFDLGFBQWEsQ0FBQztvQkFDcEYsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQzs7MEJBQ3RELFFBQVEsR0FBRyxtQkFBa0IsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLEVBQUE7b0JBQzdELFFBQVEsQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztvQkFDNUIsUUFBUSxDQUFDLFlBQVksQ0FBQyxTQUFTOzs7O29CQUFDLENBQUMsS0FBSyxFQUFFLEVBQUU7d0JBQ3RDLElBQUksS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFOzRCQUMxQixJQUFJLENBQUMsaUJBQWlCLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDOzRCQUNyRCxLQUFLLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEtBQUssQ0FBQyxDQUFDO3lCQUN4QztvQkFDTCxDQUFDLEVBQUMsQ0FBQztpQkFDTjthQUNKO1NBQ0o7SUFDTCxDQUFDOzs7O0lBRUQsV0FBVztRQUNQLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtZQUNuQixJQUFJLENBQUMsWUFBWSxDQUFDLE9BQU8sRUFBRSxDQUFDO1lBQzVCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO1NBQzVCO0lBQ0wsQ0FBQzs7Ozs7SUFFTyxRQUFRO1FBQ1osSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFOztrQkFDM0IsWUFBWSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUs7WUFDNUMsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLElBQUksRUFBRTtnQkFDbkMsT0FBTyxZQUFZLENBQUM7YUFDdkI7U0FDSjtRQUNELE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDOzs7Ozs7SUFFTyxhQUFhLENBQUMsSUFBWTtRQUM5QixPQUFPLENBQUMsR0FBRyxJQUFJLEdBQUcsQ0FBQyxVQUFVLElBQUksR0FBRyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQzNELENBQUM7Ozs7Ozs7SUFFTyx1QkFBdUIsQ0FBQyxJQUFZLEVBQUUsUUFBZ0I7O2NBQ3BELGFBQWEsR0FBRyxHQUFHLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQztRQUUxQyxJQUFJLGFBQWEsQ0FBQyxPQUFPLEVBQUU7WUFDdkIsT0FBTyxhQUFhLENBQUMsT0FBTyxDQUFDO1NBQ2hDOztjQUVLLFFBQVEsR0FBRztZQUNiLFFBQVEsRUFBRSxxQkFBcUIsSUFBSSxFQUFFO1lBQ3JDLFFBQVEsRUFBRSxRQUFRO1NBQ3JCOztjQUVLLE9BQU8sR0FBRyxJQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxRQUFRLEVBQUUsYUFBYSxDQUFDLEtBQUssQ0FBQztRQUM3RixhQUFhLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztRQUNoQyxPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDOzs7Ozs7OztJQUVPLDBCQUEwQixDQUFDLFFBQWtCLEVBQUUsUUFBbUIsRUFBRSxjQUFtQjs7Y0FDckYsUUFBUSxHQUFHLGNBQWMsSUFBSSxNQUFNLGdCQUFnQjtTQUN4RDs7Y0FDSyxZQUFZLEdBQUcsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQztRQUVsRCxNQUNNLHNCQUFzQjs7O29CQUQzQixRQUFRLFNBQUMsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLFlBQVksRUFBRSxDQUFDLFlBQVksQ0FBQyxFQUFFOzs7Y0FJakQsTUFBTSxHQUFzQyxRQUFRLENBQUMsaUNBQWlDLENBQUMsc0JBQXNCLENBQUM7UUFDcEgsT0FBTyxNQUFNLENBQUMsa0JBQWtCLENBQUMsSUFBSTs7OztRQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsYUFBYSxLQUFLLFlBQVksRUFBQyxDQUFDO0lBQ25GLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLEtBQUssR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDN0IsQ0FBQzs7O1lBMUhKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsZ0JBQWdCO2dCQUMxQixRQUFRLEVBQUU7Ozs7Ozs7O0tBUVQ7Z0JBQ0QsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7YUFDeEM7Ozs7WUFuQlEsb0JBQW9CO1lBWnpCLHdCQUF3QjtZQWFuQix1QkFBdUI7WUFmNUIsUUFBUTs7O3dCQW9DUCxTQUFTLFNBQUMsV0FBVyxFQUFFLEVBQUUsSUFBSSxFQUFFLGdCQUFnQixFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUU7b0JBUS9ELEtBQUs7Ozs7SUFSTix1Q0FDNEI7Ozs7Ozs7O0lBTzVCLG1DQUM2Qjs7SUFFN0IsMENBQStCOztJQUUvQixtQ0FBdUI7Ozs7O0lBRVgsa0RBQWtEOzs7OztJQUNsRCxzREFBMEQ7Ozs7O0lBQzFELCtDQUFrRDs7Ozs7SUFDbEQsc0NBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXG5cbi8qIVxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXG4gKlxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuICpcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbiAqXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxuICovXG5cbmltcG9ydCB7XG4gICAgQ29tcGlsZXIsXG4gICAgQ29tcG9uZW50LCBDb21wb25lbnRGYWN0b3J5LFxuICAgIENvbXBvbmVudEZhY3RvcnlSZXNvbHZlcixcbiAgICBDb21wb25lbnRSZWYsXG4gICAgSW5wdXQsXG4gICAgTW9kdWxlV2l0aENvbXBvbmVudEZhY3RvcmllcyxcbiAgICBOZ01vZHVsZSxcbiAgICBPbkRlc3Ryb3ksXG4gICAgT25Jbml0LFxuICAgIFZpZXdDaGlsZCxcbiAgICBWaWV3Q29udGFpbmVyUmVmLFxuICAgIFZpZXdFbmNhcHN1bGF0aW9uXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBGb3JtUmVuZGVyaW5nU2VydmljZSB9IGZyb20gJy4vLi4vLi4vc2VydmljZXMvZm9ybS1yZW5kZXJpbmcuc2VydmljZSc7XG5pbXBvcnQgeyBXaWRnZXRWaXNpYmlsaXR5U2VydmljZSB9IGZyb20gJy4vLi4vLi4vc2VydmljZXMvd2lkZ2V0LXZpc2liaWxpdHkuc2VydmljZSc7XG5pbXBvcnQgeyBGb3JtRmllbGRNb2RlbCB9IGZyb20gJy4vLi4vd2lkZ2V0cy9jb3JlL2Zvcm0tZmllbGQubW9kZWwnO1xuaW1wb3J0IHsgV2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi8uLi93aWRnZXRzL3dpZGdldC5jb21wb25lbnQnO1xuXG5kZWNsYXJlIHZhciBhZGY6IGFueTtcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdhZGYtZm9ybS1maWVsZCcsXG4gICAgdGVtcGxhdGU6IGBcbiAgICAgICAgPGRpdiBbaWRdPVwiJ2ZpZWxkLScrZmllbGQ/LmlkKyctY29udGFpbmVyJ1wiXG4gICAgICAgICAgICBbaGlkZGVuXT1cIiFmaWVsZD8uaXNWaXNpYmxlXCJcbiAgICAgICAgICAgIFtjbGFzcy5hZGYtZm9jdXNdPVwiZm9jdXNcIlxuICAgICAgICAgICAgKGZvY3VzaW4pPVwiZm9jdXNUb2dnbGUoKVwiXG4gICAgICAgICAgICAoZm9jdXNvdXQpPVwiZm9jdXNUb2dnbGUoKVwiPlxuICAgICAgICAgICAgPGRpdiAjY29udGFpbmVyPjwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICBgLFxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcbn0pXG5leHBvcnQgY2xhc3MgRm9ybUZpZWxkQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuXG4gICAgQFZpZXdDaGlsZCgnY29udGFpbmVyJywgeyByZWFkOiBWaWV3Q29udGFpbmVyUmVmLCBzdGF0aWM6IHRydWUgfSlcbiAgICBjb250YWluZXI6IFZpZXdDb250YWluZXJSZWY7XG5cbiAgICAvKiogQ29udGFpbnMgYWxsIHRoZSBuZWNlc3NhcnkgZGF0YSBuZWVkZWQgdG8gZGV0ZXJtaW5lIHdoYXQgVUkgV2lkZ2V0XG4gICAgICogdG8gdXNlIHdoZW4gcmVuZGVyaW5nIHRoZSBmaWVsZCBpbiB0aGUgZm9ybS4gWW91IHdvdWxkIHR5cGljYWxseSBub3RcbiAgICAgKiBjcmVhdGUgdGhpcyBkYXRhIG1hbnVhbGx5IGJ1dCBpbnN0ZWFkIGNyZWF0ZSB0aGUgZm9ybSBpbiBBUFMgYW5kIGV4cG9ydFxuICAgICAqIGl0IHRvIGdldCB0byBhbGwgdGhlIGBGb3JtRmllbGRNb2RlbGAgZGVmaW5pdGlvbnMuXG4gICAgICovXG4gICAgQElucHV0KClcbiAgICBmaWVsZDogRm9ybUZpZWxkTW9kZWwgPSBudWxsO1xuXG4gICAgY29tcG9uZW50UmVmOiBDb21wb25lbnRSZWY8e30+O1xuXG4gICAgZm9jdXM6IGJvb2xlYW4gPSBmYWxzZTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgZm9ybVJlbmRlcmluZ1NlcnZpY2U6IEZvcm1SZW5kZXJpbmdTZXJ2aWNlLFxuICAgICAgICAgICAgICAgIHByaXZhdGUgY29tcG9uZW50RmFjdG9yeVJlc29sdmVyOiBDb21wb25lbnRGYWN0b3J5UmVzb2x2ZXIsXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSB2aXNpYmlsaXR5U2VydmljZTogV2lkZ2V0VmlzaWJpbGl0eVNlcnZpY2UsXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBjb21waWxlcjogQ29tcGlsZXIpIHtcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgY29uc3QgdzogYW55ID0gd2luZG93O1xuICAgICAgICBpZiAody5hZGYgPT09IHVuZGVmaW5lZCkge1xuICAgICAgICAgICAgdy5hZGYgPSB7fTtcbiAgICAgICAgfVxuICAgICAgICBjb25zdCBvcmlnaW5hbEZpZWxkID0gdGhpcy5nZXRGaWVsZCgpO1xuICAgICAgICBpZiAob3JpZ2luYWxGaWVsZCkge1xuICAgICAgICAgICAgY29uc3QgY3VzdG9tVGVtcGxhdGUgPSB0aGlzLmZpZWxkLmZvcm0uY3VzdG9tRmllbGRUZW1wbGF0ZXNbb3JpZ2luYWxGaWVsZC50eXBlXTtcbiAgICAgICAgICAgIGlmIChjdXN0b21UZW1wbGF0ZSAmJiB0aGlzLmhhc0NvbnRyb2xsZXIob3JpZ2luYWxGaWVsZC50eXBlKSkge1xuICAgICAgICAgICAgICAgIGNvbnN0IGZhY3RvcnkgPSB0aGlzLmdldENvbXBvbmVudEZhY3RvcnlTeW5jKG9yaWdpbmFsRmllbGQudHlwZSwgY3VzdG9tVGVtcGxhdGUpO1xuICAgICAgICAgICAgICAgIHRoaXMuY29tcG9uZW50UmVmID0gdGhpcy5jb250YWluZXIuY3JlYXRlQ29tcG9uZW50KGZhY3RvcnkpO1xuICAgICAgICAgICAgICAgIGNvbnN0IGluc3RhbmNlOiBhbnkgPSB0aGlzLmNvbXBvbmVudFJlZi5pbnN0YW5jZTtcbiAgICAgICAgICAgICAgICBpZiAoaW5zdGFuY2UpIHtcbiAgICAgICAgICAgICAgICAgICAgaW5zdGFuY2UuZmllbGQgPSBvcmlnaW5hbEZpZWxkO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgY29uc3QgY29tcG9uZW50VHlwZSA9IHRoaXMuZm9ybVJlbmRlcmluZ1NlcnZpY2UucmVzb2x2ZUNvbXBvbmVudFR5cGUob3JpZ2luYWxGaWVsZCk7XG4gICAgICAgICAgICAgICAgaWYgKGNvbXBvbmVudFR5cGUpIHtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZmFjdG9yeSA9IHRoaXMuY29tcG9uZW50RmFjdG9yeVJlc29sdmVyLnJlc29sdmVDb21wb25lbnRGYWN0b3J5KGNvbXBvbmVudFR5cGUpO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbXBvbmVudFJlZiA9IHRoaXMuY29udGFpbmVyLmNyZWF0ZUNvbXBvbmVudChmYWN0b3J5KTtcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgaW5zdGFuY2UgPSA8V2lkZ2V0Q29tcG9uZW50PiB0aGlzLmNvbXBvbmVudFJlZi5pbnN0YW5jZTtcbiAgICAgICAgICAgICAgICAgICAgaW5zdGFuY2UuZmllbGQgPSB0aGlzLmZpZWxkO1xuICAgICAgICAgICAgICAgICAgICBpbnN0YW5jZS5maWVsZENoYW5nZWQuc3Vic2NyaWJlKChmaWVsZCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGZpZWxkICYmIHRoaXMuZmllbGQuZm9ybSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudmlzaWJpbGl0eVNlcnZpY2UucmVmcmVzaFZpc2liaWxpdHkoZmllbGQuZm9ybSk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZmllbGQuZm9ybS5vbkZvcm1GaWVsZENoYW5nZWQoZmllbGQpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBuZ09uRGVzdHJveSgpIHtcbiAgICAgICAgaWYgKHRoaXMuY29tcG9uZW50UmVmKSB7XG4gICAgICAgICAgICB0aGlzLmNvbXBvbmVudFJlZi5kZXN0cm95KCk7XG4gICAgICAgICAgICB0aGlzLmNvbXBvbmVudFJlZiA9IG51bGw7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIGdldEZpZWxkKCk6IEZvcm1GaWVsZE1vZGVsIHtcbiAgICAgICAgaWYgKHRoaXMuZmllbGQgJiYgdGhpcy5maWVsZC5wYXJhbXMpIHtcbiAgICAgICAgICAgIGNvbnN0IHdyYXBwZWRGaWVsZCA9IHRoaXMuZmllbGQucGFyYW1zLmZpZWxkO1xuICAgICAgICAgICAgaWYgKHdyYXBwZWRGaWVsZCAmJiB3cmFwcGVkRmllbGQudHlwZSkge1xuICAgICAgICAgICAgICAgIHJldHVybiB3cmFwcGVkRmllbGQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIHRoaXMuZmllbGQ7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBoYXNDb250cm9sbGVyKHR5cGU6IHN0cmluZyk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gKGFkZiAmJiBhZGYuY29tcG9uZW50cyAmJiBhZGYuY29tcG9uZW50c1t0eXBlXSk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXRDb21wb25lbnRGYWN0b3J5U3luYyh0eXBlOiBzdHJpbmcsIHRlbXBsYXRlOiBzdHJpbmcpOiBDb21wb25lbnRGYWN0b3J5PGFueT4ge1xuICAgICAgICBjb25zdCBjb21wb25lbnRJbmZvID0gYWRmLmNvbXBvbmVudHNbdHlwZV07XG5cbiAgICAgICAgaWYgKGNvbXBvbmVudEluZm8uZmFjdG9yeSkge1xuICAgICAgICAgICAgcmV0dXJuIGNvbXBvbmVudEluZm8uZmFjdG9yeTtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IG1ldGFkYXRhID0ge1xuICAgICAgICAgICAgc2VsZWN0b3I6IGBydW50aW1lLWNvbXBvbmVudC0ke3R5cGV9YCxcbiAgICAgICAgICAgIHRlbXBsYXRlOiB0ZW1wbGF0ZVxuICAgICAgICB9O1xuXG4gICAgICAgIGNvbnN0IGZhY3RvcnkgPSB0aGlzLmNyZWF0ZUNvbXBvbmVudEZhY3RvcnlTeW5jKHRoaXMuY29tcGlsZXIsIG1ldGFkYXRhLCBjb21wb25lbnRJbmZvLmNsYXNzKTtcbiAgICAgICAgY29tcG9uZW50SW5mby5mYWN0b3J5ID0gZmFjdG9yeTtcbiAgICAgICAgcmV0dXJuIGZhY3Rvcnk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBjcmVhdGVDb21wb25lbnRGYWN0b3J5U3luYyhjb21waWxlcjogQ29tcGlsZXIsIG1ldGFkYXRhOiBDb21wb25lbnQsIGNvbXBvbmVudENsYXNzOiBhbnkpOiBDb21wb25lbnRGYWN0b3J5PGFueT4ge1xuICAgICAgICBjb25zdCBjbXBDbGFzcyA9IGNvbXBvbmVudENsYXNzIHx8IGNsYXNzIFJ1bnRpbWVDb21wb25lbnQge1xuICAgICAgICB9O1xuICAgICAgICBjb25zdCBkZWNvcmF0ZWRDbXAgPSBDb21wb25lbnQobWV0YWRhdGEpKGNtcENsYXNzKTtcblxuICAgICAgICBATmdNb2R1bGUoeyBpbXBvcnRzOiBbXSwgZGVjbGFyYXRpb25zOiBbZGVjb3JhdGVkQ21wXSB9KVxuICAgICAgICBjbGFzcyBSdW50aW1lQ29tcG9uZW50TW9kdWxlIHtcbiAgICAgICAgfVxuXG4gICAgICAgIGNvbnN0IG1vZHVsZTogTW9kdWxlV2l0aENvbXBvbmVudEZhY3Rvcmllczxhbnk+ID0gY29tcGlsZXIuY29tcGlsZU1vZHVsZUFuZEFsbENvbXBvbmVudHNTeW5jKFJ1bnRpbWVDb21wb25lbnRNb2R1bGUpO1xuICAgICAgICByZXR1cm4gbW9kdWxlLmNvbXBvbmVudEZhY3Rvcmllcy5maW5kKCh4KSA9PiB4LmNvbXBvbmVudFR5cGUgPT09IGRlY29yYXRlZENtcCk7XG4gICAgfVxuXG4gICAgZm9jdXNUb2dnbGUoKSB7XG4gICAgICAgIHRoaXMuZm9jdXMgPSAhdGhpcy5mb2N1cztcbiAgICB9XG59XG4iXX0=