/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { FormService } from './../services/form.service';
export class FormListComponent {
    /**
     * @param {?} formService
     */
    constructor(formService) {
        this.formService = formService;
        /**
         * The array that contains the information to show inside the list.
         */
        this.forms = [];
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        this.getForms();
    }
    /**
     * @return {?}
     */
    isEmpty() {
        return this.forms && this.forms.length === 0;
    }
    /**
     * @return {?}
     */
    getForms() {
        this.formService.getForms().subscribe((/**
         * @param {?} forms
         * @return {?}
         */
        (forms) => {
            this.forms.push(...forms);
        }));
    }
}
FormListComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-form-list',
                template: "<adf-datatable *ngIf=\"!isEmpty()\"\r\n    [rows]=\"forms\">\r\n    <data-columns>\r\n        <data-column key=\"name\" type=\"text\" title=\"Name\" class=\"adf-ellipsis-cell\" [sortable]=\"true\"></data-column>\r\n        <data-column key=\"lastUpdatedByFullName\" type=\"text\" title=\"User\" class=\"adf-ellipsis-cell\" [sortable]=\"true\"></data-column>\r\n        <data-column key=\"lastUpdated\" type=\"date\" format=\"shortDate\" title=\"Date\"></data-column>\r\n    </data-columns>\r\n</adf-datatable>\r\n",
                encapsulation: ViewEncapsulation.None,
                styles: [""]
            }] }
];
/** @nocollapse */
FormListComponent.ctorParameters = () => [
    { type: FormService }
];
FormListComponent.propDecorators = {
    forms: [{ type: Input }]
};
if (false) {
    /**
     * The array that contains the information to show inside the list.
     * @type {?}
     */
    FormListComponent.prototype.forms;
    /**
     * @type {?}
     * @protected
     */
    FormListComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1saXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy9mb3JtLWxpc3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUE0QixpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5RixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFRekQsTUFBTSxPQUFPLGlCQUFpQjs7OztJQU0xQixZQUFzQixXQUF3QjtRQUF4QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTs7OztRQUY5QyxVQUFLLEdBQVcsRUFBRSxDQUFDO0lBR25CLENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLE9BQXNCO1FBQzlCLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNwQixDQUFDOzs7O0lBRUQsT0FBTztRQUNILE9BQU8sSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUM7SUFDakQsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxDQUFDLFNBQVM7Ozs7UUFBQyxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQzVDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUM7UUFDOUIsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7WUEzQkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxlQUFlO2dCQUN6Qiw2Z0JBQXlDO2dCQUV6QyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7YUFDeEM7Ozs7WUFQUSxXQUFXOzs7b0JBV2YsS0FBSzs7Ozs7OztJQUFOLGtDQUNtQjs7Ozs7SUFFUCx3Q0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25DaGFuZ2VzLCBTaW1wbGVDaGFuZ2VzLCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4vLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtZm9ybS1saXN0JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9mb3JtLWxpc3QuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vZm9ybS1saXN0LmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb3JtTGlzdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XHJcblxyXG4gICAgLyoqIFRoZSBhcnJheSB0aGF0IGNvbnRhaW5zIHRoZSBpbmZvcm1hdGlvbiB0byBzaG93IGluc2lkZSB0aGUgbGlzdC4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBmb3JtczogYW55IFtdID0gW107XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvdGVjdGVkIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgICAgICB0aGlzLmdldEZvcm1zKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaXNFbXB0eSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5mb3JtcyAmJiB0aGlzLmZvcm1zLmxlbmd0aCA9PT0gMDtcclxuICAgIH1cclxuXHJcbiAgICBnZXRGb3JtcygpIHtcclxuICAgICAgICB0aGlzLmZvcm1TZXJ2aWNlLmdldEZvcm1zKCkuc3Vic2NyaWJlKChmb3JtcykgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZvcm1zLnB1c2goLi4uZm9ybXMpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=