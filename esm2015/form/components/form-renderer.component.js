/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ViewEncapsulation, Input } from '@angular/core';
import { FormBaseModel } from './form-base.model';
export class FormRendererComponent {
    constructor() {
        /**
         * Toggle debug options.
         */
        this.showDebugButton = false;
    }
}
FormRendererComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-form-renderer',
                template: "<div class=\"{{formDefinition.className}}\" [ngClass]=\"{'adf-readonly-form': formDefinition.readOnly }\">\r\n    <div *ngIf=\"formDefinition.hasTabs()\">\r\n        <tabs-widget [tabs]=\"formDefinition.tabs\"></tabs-widget>\r\n    </div>\r\n\r\n    <div *ngIf=\"!formDefinition.hasTabs() && formDefinition.hasFields()\">\r\n        <div *ngFor=\"let field of formDefinition.fields\">\r\n            <adf-form-field [field]=\"field.field\"></adf-form-field>\r\n        </div>\r\n    </div>\r\n</div>\r\n<!--\r\nFor debugging and data visualisation purposes,\r\nwill be removed during future revisions\r\n-->\r\n<div *ngIf=\"showDebugButton\" class=\"adf-form-debug-container\">\r\n    <mat-slide-toggle [(ngModel)]=\"debugMode\">Debug mode</mat-slide-toggle>\r\n    <div *ngIf=\"debugMode\">\r\n        <h4>Values</h4>\r\n        <pre>{{formDefinition.values | json}}</pre>\r\n\r\n        <h4>Form</h4>\r\n        <pre>{{formDefinition.json | json}}</pre>\r\n    </div>\r\n</div>\r\n",
                encapsulation: ViewEncapsulation.None,
                styles: [""]
            }] }
];
FormRendererComponent.propDecorators = {
    showDebugButton: [{ type: Input }],
    formDefinition: [{ type: Input }]
};
if (false) {
    /**
     * Toggle debug options.
     * @type {?}
     */
    FormRendererComponent.prototype.showDebugButton;
    /** @type {?} */
    FormRendererComponent.prototype.formDefinition;
    /** @type {?} */
    FormRendererComponent.prototype.debugMode;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1yZW5kZXJlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvZm9ybS1yZW5kZXJlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxpQkFBaUIsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDcEUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBUWxELE1BQU0sT0FBTyxxQkFBcUI7SUFObEM7Ozs7UUFVSSxvQkFBZSxHQUFZLEtBQUssQ0FBQztJQU9yQyxDQUFDOzs7WUFqQkEsU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxtQkFBbUI7Z0JBQzdCLG0rQkFBNkM7Z0JBRTdDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOzthQUN4Qzs7OzhCQUlJLEtBQUs7NkJBR0wsS0FBSzs7Ozs7OztJQUhOLGdEQUNpQzs7SUFFakMsK0NBQzhCOztJQUU5QiwwQ0FBbUIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ29tcG9uZW50LCBWaWV3RW5jYXBzdWxhdGlvbiwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybUJhc2VNb2RlbCB9IGZyb20gJy4vZm9ybS1iYXNlLm1vZGVsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtZm9ybS1yZW5kZXJlcicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vZm9ybS1yZW5kZXJlci5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9mb3JtLXJlbmRlcmVyLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb3JtUmVuZGVyZXJDb21wb25lbnQge1xyXG5cclxuICAgIC8qKiBUb2dnbGUgZGVidWcgb3B0aW9ucy4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzaG93RGVidWdCdXR0b246IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgZm9ybURlZmluaXRpb246IEZvcm1CYXNlTW9kZWw7XHJcblxyXG4gICAgZGVidWdNb2RlOiBib29sZWFuO1xyXG5cclxufVxyXG4iXX0=