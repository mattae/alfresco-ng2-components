/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, ViewEncapsulation } from '@angular/core';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
export class AmountWidgetComponent extends WidgetComponent {
    /**
     * @param {?} formService
     */
    constructor(formService) {
        super(formService);
        this.formService = formService;
        this.currency = AmountWidgetComponent.DEFAULT_CURRENCY;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.field && this.field.currency) {
            this.currency = this.field.currency;
        }
    }
}
AmountWidgetComponent.DEFAULT_CURRENCY = '$';
AmountWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'amount-widget',
                template: "<div class=\"adf-amount-widget__container adf-amount-widget {{field.className}}\" [class.adf-invalid]=\"!field.isValid\" [class.adf-readonly]=\"field.readOnly\">\r\n    <mat-form-field class=\"adf-amount-widget__input\">\r\n        <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}<span *ngIf=\"isRequired()\">*</span></label>\r\n        <span matPrefix class=\"adf-amount-widget__prefix-spacing\"> {{currency }}</span>\r\n        <input matInput\r\n                class=\"adf-amount-widget\"\r\n                type=\"text\"\r\n                [id]=\"field.id\"\r\n                [required]=\"isRequired()\"\r\n                [placeholder]=\"field.placeholder\"\r\n                [value]=\"field.value\"\r\n                [(ngModel)]=\"field.value\"\r\n                (ngModelChange)=\"onFieldChanged(field)\"\r\n                [disabled]=\"field.readOnly\">\r\n    </mat-form-field>\r\n    <error-widget [error]=\"field.validationSummary\" ></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n</div>\r\n",
                host: baseHost,
                encapsulation: ViewEncapsulation.None,
                styles: [".adf-amount-widget{width:100%;vertical-align:baseline!important}.adf-amount-widget .mat-input-placeholder{margin-top:5px;display:none}.adf-amount-widget .mat-form-field-flex{position:relative;padding-top:18.5px}.adf-amount-widget .mat-form-field-infix{position:static;padding-top:19px}.adf-amount-widget .adf-label{position:absolute;top:18.5px;left:0}.adf-amount-widget__container{max-width:100%}.adf-amount-widget__container .mat-form-field-label-wrapper{top:34px!important;left:13px}.adf-amount-widget__input .mat-focused{transition:none}.adf-amount-widget__prefix-spacing{padding-right:5px}"]
            }] }
];
/** @nocollapse */
AmountWidgetComponent.ctorParameters = () => [
    { type: FormService }
];
if (false) {
    /** @type {?} */
    AmountWidgetComponent.DEFAULT_CURRENCY;
    /** @type {?} */
    AmountWidgetComponent.prototype.currency;
    /** @type {?} */
    AmountWidgetComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW1vdW50LndpZGdldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2Ftb3VudC9hbW91bnQud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsU0FBUyxFQUFVLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUMvRCxPQUFPLEVBQUUsUUFBUSxFQUFHLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBU25FLE1BQU0sT0FBTyxxQkFBc0IsU0FBUSxlQUFlOzs7O0lBTXRELFlBQW1CLFdBQXdCO1FBQ3ZDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztRQURKLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBRjNDLGFBQVEsR0FBVyxxQkFBcUIsQ0FBQyxnQkFBZ0IsQ0FBQztJQUkxRCxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRTtZQUNuQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO1NBQ3ZDO0lBQ0wsQ0FBQzs7QUFaTSxzQ0FBZ0IsR0FBVyxHQUFHLENBQUM7O1lBVHpDLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsZUFBZTtnQkFDekIseW5DQUFtQztnQkFFbkMsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2FBQ3hDOzs7O1lBVFEsV0FBVzs7OztJQVloQix1Q0FBc0M7O0lBRXRDLHlDQUEwRDs7SUFFOUMsNENBQStCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbiAvKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLy4uLy4uLy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XHJcbmltcG9ydCB7IGJhc2VIb3N0ICwgV2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi8uLi93aWRnZXQuY29tcG9uZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhbW91bnQtd2lkZ2V0JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9hbW91bnQud2lkZ2V0Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vYW1vdW50LndpZGdldC5zY3NzJ10sXHJcbiAgICBob3N0OiBiYXNlSG9zdCxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIEFtb3VudFdpZGdldENvbXBvbmVudCBleHRlbmRzIFdpZGdldENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgc3RhdGljIERFRkFVTFRfQ1VSUkVOQ1k6IHN0cmluZyA9ICckJztcclxuXHJcbiAgICBjdXJyZW5jeTogc3RyaW5nID0gQW1vdW50V2lkZ2V0Q29tcG9uZW50LkRFRkFVTFRfQ1VSUkVOQ1k7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSkge1xyXG4gICAgICAgIHN1cGVyKGZvcm1TZXJ2aWNlKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBpZiAodGhpcy5maWVsZCAmJiB0aGlzLmZpZWxkLmN1cnJlbmN5KSB7XHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVuY3kgPSB0aGlzLmZpZWxkLmN1cnJlbmN5O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuIl19