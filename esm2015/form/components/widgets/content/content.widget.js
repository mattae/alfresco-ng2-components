/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ContentService } from '../../../../services/content.service';
import { LogService } from '../../../../services/log.service';
import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { ProcessContentService } from '../../../services/process-content.service';
import { ContentLinkModel } from '../core/content-link.model';
import { FormService } from './../../../services/form.service';
export class ContentWidgetComponent {
    /**
     * @param {?} formService
     * @param {?} logService
     * @param {?} contentService
     * @param {?} processContentService
     */
    constructor(formService, logService, contentService, processContentService) {
        this.formService = formService;
        this.logService = logService;
        this.contentService = contentService;
        this.processContentService = processContentService;
        /**
         * Toggles showing document content.
         */
        this.showDocumentContent = true;
        /**
         * Emitted when the content is clicked.
         */
        this.contentClick = new EventEmitter();
        /**
         * Emitted when the thumbnail has loaded.
         */
        this.thumbnailLoaded = new EventEmitter();
        /**
         * Emitted when the content has loaded.
         */
        this.contentLoaded = new EventEmitter();
        /**
         * Emitted when an error occurs.
         */
        this.error = new EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        /** @type {?} */
        const contentId = changes['id'];
        if (contentId && contentId.currentValue) {
            this.loadContent(contentId.currentValue);
        }
    }
    /**
     * @param {?} id
     * @return {?}
     */
    loadContent(id) {
        this.processContentService
            .getFileContent(id)
            .subscribe((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            this.content = new ContentLinkModel(response);
            this.contentLoaded.emit(this.content);
            this.loadThumbnailUrl(this.content);
        }), (/**
         * @param {?} error
         * @return {?}
         */
        (error) => {
            this.error.emit(error);
        }));
    }
    /**
     * @param {?} content
     * @return {?}
     */
    loadThumbnailUrl(content) {
        if (this.content.isThumbnailSupported()) {
            /** @type {?} */
            let observable;
            if (this.content.isTypeImage()) {
                observable = this.processContentService.getFileRawContent(content.id);
            }
            else {
                observable = this.processContentService.getContentThumbnail(content.id);
            }
            if (observable) {
                observable.subscribe((/**
                 * @param {?} response
                 * @return {?}
                 */
                (response) => {
                    this.content.thumbnailUrl = this.contentService.createTrustedUrl(response);
                    this.thumbnailLoaded.emit(this.content.thumbnailUrl);
                }), (/**
                 * @param {?} error
                 * @return {?}
                 */
                (error) => {
                    this.error.emit(error);
                }));
            }
        }
    }
    /**
     * @param {?} content
     * @return {?}
     */
    openViewer(content) {
        /** @type {?} */
        let fetch = this.processContentService.getContentPreview(content.id);
        if (content.isTypeImage() || content.isTypePdf()) {
            fetch = this.processContentService.getFileRawContent(content.id);
        }
        fetch.subscribe((/**
         * @param {?} blob
         * @return {?}
         */
        (blob) => {
            content.contentBlob = blob;
            this.contentClick.emit(content);
            this.logService.info('Content clicked' + content.id);
            this.formService.formContentClicked.next(content);
        }), (/**
         * @param {?} error
         * @return {?}
         */
        (error) => {
            this.error.emit(error);
        }));
    }
    /**
     * Invoke content download.
     * @param {?} content
     * @return {?}
     */
    download(content) {
        this.processContentService.getFileRawContent(content.id).subscribe((/**
         * @param {?} blob
         * @return {?}
         */
        (blob) => this.contentService.downloadBlob(blob, content.name)), (/**
         * @param {?} error
         * @return {?}
         */
        (error) => {
            this.error.emit(error);
        }));
    }
}
ContentWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-content',
                template: "<mat-card class=\"adf-content-container\" *ngIf=\"content\">\r\n    <mat-card-content *ngIf=\"showDocumentContent\">\r\n        <div *ngIf=\"content.isThumbnailSupported()\" >\r\n            <img id=\"thumbnailPreview\" class=\"adf-img-upload-widget\" [src]=\"content.thumbnailUrl\" alt=\"{{content.name}}\">\r\n        </div>\r\n        <div *ngIf=\"!content.isThumbnailSupported()\">\r\n            <mat-icon>image</mat-icon>\r\n            <div id=\"unsupported-thumbnail\" class=\"adf-content-widget-preview-text\">{{ 'FORM.PREVIEW.IMAGE_NOT_AVAILABLE' | translate }}\r\n            </div>\r\n        </div>\r\n        <div class=\"mdl-card__supporting-text upload-widget__content-text\">{{content.name | translate }}</div>\r\n    </mat-card-content>\r\n\r\n    <mat-card-actions>\r\n        <button mat-icon-button id=\"view\" (click)=\"openViewer(content)\">\r\n            <mat-icon class=\"mat-24\">zoom_in</mat-icon>\r\n        </button>\r\n        <button mat-icon-button id=\"download\" (click)=\"download(content)\">\r\n            <mat-icon class=\"mat-24\">file_download</mat-icon>\r\n        </button>\r\n    </mat-card-actions>\r\n</mat-card>\r\n",
                encapsulation: ViewEncapsulation.None,
                styles: [".adf-img-upload-widget{width:100%;height:100%;border:1px solid rgba(117,117,117,.57);box-shadow:1px 1px 2px #ddd;background-color:#fff}.adf-content-widget-preview-text{word-wrap:break-word;word-break:break-all;text-align:center}"]
            }] }
];
/** @nocollapse */
ContentWidgetComponent.ctorParameters = () => [
    { type: FormService },
    { type: LogService },
    { type: ContentService },
    { type: ProcessContentService }
];
ContentWidgetComponent.propDecorators = {
    id: [{ type: Input }],
    showDocumentContent: [{ type: Input }],
    contentClick: [{ type: Output }],
    thumbnailLoaded: [{ type: Output }],
    contentLoaded: [{ type: Output }],
    error: [{ type: Output }]
};
if (false) {
    /**
     * The content id to show.
     * @type {?}
     */
    ContentWidgetComponent.prototype.id;
    /**
     * Toggles showing document content.
     * @type {?}
     */
    ContentWidgetComponent.prototype.showDocumentContent;
    /**
     * Emitted when the content is clicked.
     * @type {?}
     */
    ContentWidgetComponent.prototype.contentClick;
    /**
     * Emitted when the thumbnail has loaded.
     * @type {?}
     */
    ContentWidgetComponent.prototype.thumbnailLoaded;
    /**
     * Emitted when the content has loaded.
     * @type {?}
     */
    ContentWidgetComponent.prototype.contentLoaded;
    /**
     * Emitted when an error occurs.
     * @type {?}
     */
    ContentWidgetComponent.prototype.error;
    /** @type {?} */
    ContentWidgetComponent.prototype.content;
    /**
     * @type {?}
     * @protected
     */
    ContentWidgetComponent.prototype.formService;
    /**
     * @type {?}
     * @private
     */
    ContentWidgetComponent.prototype.logService;
    /**
     * @type {?}
     * @private
     */
    ContentWidgetComponent.prototype.contentService;
    /**
     * @type {?}
     * @private
     */
    ContentWidgetComponent.prototype.processContentService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC53aWRnZXQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9jb250ZW50L2NvbnRlbnQud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUN0RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDOUQsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFhLE1BQU0sRUFBaUIsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFcEgsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFDbEYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDOUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBUS9ELE1BQU0sT0FBTyxzQkFBc0I7Ozs7Ozs7SUE0Qi9CLFlBQXNCLFdBQXdCLEVBQzFCLFVBQXNCLEVBQ3RCLGNBQThCLEVBQzlCLHFCQUE0QztRQUgxQyxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUMxQixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QiwwQkFBcUIsR0FBckIscUJBQXFCLENBQXVCOzs7O1FBdkJoRSx3QkFBbUIsR0FBWSxJQUFJLENBQUM7Ozs7UUFJcEMsaUJBQVksR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDOzs7O1FBSWxDLG9CQUFlLEdBQXNCLElBQUksWUFBWSxFQUFPLENBQUM7Ozs7UUFJN0Qsa0JBQWEsR0FBc0IsSUFBSSxZQUFZLEVBQU8sQ0FBQzs7OztRQUkzRCxVQUFLLEdBQXNCLElBQUksWUFBWSxFQUFPLENBQUM7SUFRbkQsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsT0FBc0I7O2NBQ3hCLFNBQVMsR0FBRyxPQUFPLENBQUMsSUFBSSxDQUFDO1FBQy9CLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxZQUFZLEVBQUU7WUFDckMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDNUM7SUFDTCxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxFQUFVO1FBQ2xCLElBQUksQ0FBQyxxQkFBcUI7YUFDckIsY0FBYyxDQUFDLEVBQUUsQ0FBQzthQUNsQixTQUFTOzs7O1FBQ04sQ0FBQyxRQUEwQixFQUFFLEVBQUU7WUFDM0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUN0QyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3hDLENBQUM7Ozs7UUFDRCxDQUFDLEtBQUssRUFBRSxFQUFFO1lBQ04sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUNKLENBQUM7SUFDVixDQUFDOzs7OztJQUVELGdCQUFnQixDQUFDLE9BQXlCO1FBQ3RDLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSxFQUFFOztnQkFDakMsVUFBMkI7WUFFL0IsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxFQUFFO2dCQUM1QixVQUFVLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUN6RTtpQkFBTTtnQkFDSCxVQUFVLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUMzRTtZQUVELElBQUksVUFBVSxFQUFFO2dCQUNaLFVBQVUsQ0FBQyxTQUFTOzs7O2dCQUNoQixDQUFDLFFBQWMsRUFBRSxFQUFFO29CQUNmLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQzNFLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLENBQUM7Z0JBQ3pELENBQUM7Ozs7Z0JBQ0QsQ0FBQyxLQUFLLEVBQUUsRUFBRTtvQkFDTixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFFM0IsQ0FBQyxFQUNKLENBQUM7YUFDTDtTQUNKO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsT0FBeUI7O1lBQzVCLEtBQUssR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztRQUNwRSxJQUFJLE9BQU8sQ0FBQyxXQUFXLEVBQUUsSUFBSSxPQUFPLENBQUMsU0FBUyxFQUFFLEVBQUU7WUFDOUMsS0FBSyxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDcEU7UUFDRCxLQUFLLENBQUMsU0FBUzs7OztRQUNYLENBQUMsSUFBVSxFQUFFLEVBQUU7WUFDWCxPQUFPLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztZQUMzQixJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUNoQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDckQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDdEQsQ0FBQzs7OztRQUNELENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDTixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMzQixDQUFDLEVBQ0osQ0FBQztJQUNOLENBQUM7Ozs7OztJQUtELFFBQVEsQ0FBQyxPQUF5QjtRQUM5QixJQUFJLENBQUMscUJBQXFCLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVM7Ozs7UUFDOUQsQ0FBQyxJQUFVLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsSUFBSSxDQUFDOzs7O1FBQ3BFLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDTixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMzQixDQUFDLEVBQ0osQ0FBQztJQUNOLENBQUM7OztZQW5ISixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGFBQWE7Z0JBQ3ZCLHNwQ0FBb0M7Z0JBRXBDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOzthQUN4Qzs7OztZQVBRLFdBQVc7WUFMWCxVQUFVO1lBRFYsY0FBYztZQUlkLHFCQUFxQjs7O2lCQWF6QixLQUFLO2tDQUlMLEtBQUs7MkJBSUwsTUFBTTs4QkFJTixNQUFNOzRCQUlOLE1BQU07b0JBSU4sTUFBTTs7Ozs7OztJQXBCUCxvQ0FDVzs7Ozs7SUFHWCxxREFDb0M7Ozs7O0lBR3BDLDhDQUNrQzs7Ozs7SUFHbEMsaURBQzZEOzs7OztJQUc3RCwrQ0FDMkQ7Ozs7O0lBRzNELHVDQUNtRDs7SUFFbkQseUNBQTBCOzs7OztJQUVkLDZDQUFrQzs7Ozs7SUFDbEMsNENBQThCOzs7OztJQUM5QixnREFBc0M7Ozs7O0lBQ3RDLHVEQUFvRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDb250ZW50U2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL3NlcnZpY2VzL2NvbnRlbnQuc2VydmljZSc7XHJcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9zZXJ2aWNlcy9sb2cuc2VydmljZSc7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25DaGFuZ2VzLCBPdXRwdXQsIFNpbXBsZUNoYW5nZXMsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgUHJvY2Vzc0NvbnRlbnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2VydmljZXMvcHJvY2Vzcy1jb250ZW50LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDb250ZW50TGlua01vZGVsIH0gZnJvbSAnLi4vY29yZS9jb250ZW50LWxpbmsubW9kZWwnO1xyXG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4vLi4vLi4vLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtY29udGVudCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vY29udGVudC53aWRnZXQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9jb250ZW50LndpZGdldC5zY3NzJ10sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb250ZW50V2lkZ2V0Q29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzIHtcclxuXHJcbiAgICAvKiogVGhlIGNvbnRlbnQgaWQgdG8gc2hvdy4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBpZDogc3RyaW5nO1xyXG5cclxuICAgIC8qKiBUb2dnbGVzIHNob3dpbmcgZG9jdW1lbnQgY29udGVudC4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzaG93RG9jdW1lbnRDb250ZW50OiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSBjb250ZW50IGlzIGNsaWNrZWQuICovXHJcbiAgICBAT3V0cHV0KClcclxuICAgIGNvbnRlbnRDbGljayA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSB0aHVtYm5haWwgaGFzIGxvYWRlZC4gKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgdGh1bWJuYWlsTG9hZGVkOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIGNvbnRlbnQgaGFzIGxvYWRlZC4gKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgY29udGVudExvYWRlZDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIGFuIGVycm9yIG9jY3Vycy4gKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgZXJyb3I6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgY29udGVudDogQ29udGVudExpbmtNb2RlbDtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgZm9ybVNlcnZpY2U6IEZvcm1TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBjb250ZW50U2VydmljZTogQ29udGVudFNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHByb2Nlc3NDb250ZW50U2VydmljZTogUHJvY2Vzc0NvbnRlbnRTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgICAgIGNvbnN0IGNvbnRlbnRJZCA9IGNoYW5nZXNbJ2lkJ107XHJcbiAgICAgICAgaWYgKGNvbnRlbnRJZCAmJiBjb250ZW50SWQuY3VycmVudFZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZENvbnRlbnQoY29udGVudElkLmN1cnJlbnRWYWx1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGxvYWRDb250ZW50KGlkOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLnByb2Nlc3NDb250ZW50U2VydmljZVxyXG4gICAgICAgICAgICAuZ2V0RmlsZUNvbnRlbnQoaWQpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAocmVzcG9uc2U6IENvbnRlbnRMaW5rTW9kZWwpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRlbnQgPSBuZXcgQ29udGVudExpbmtNb2RlbChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb250ZW50TG9hZGVkLmVtaXQodGhpcy5jb250ZW50KTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvYWRUaHVtYm5haWxVcmwodGhpcy5jb250ZW50KTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmVycm9yLmVtaXQoZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIGxvYWRUaHVtYm5haWxVcmwoY29udGVudDogQ29udGVudExpbmtNb2RlbCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvbnRlbnQuaXNUaHVtYm5haWxTdXBwb3J0ZWQoKSkge1xyXG4gICAgICAgICAgICBsZXQgb2JzZXJ2YWJsZTogT2JzZXJ2YWJsZTxhbnk+O1xyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuY29udGVudC5pc1R5cGVJbWFnZSgpKSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZhYmxlID0gdGhpcy5wcm9jZXNzQ29udGVudFNlcnZpY2UuZ2V0RmlsZVJhd0NvbnRlbnQoY29udGVudC5pZCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZhYmxlID0gdGhpcy5wcm9jZXNzQ29udGVudFNlcnZpY2UuZ2V0Q29udGVudFRodW1ibmFpbChjb250ZW50LmlkKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKG9ic2VydmFibGUpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmFibGUuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgICAgIChyZXNwb25zZTogQmxvYikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRlbnQudGh1bWJuYWlsVXJsID0gdGhpcy5jb250ZW50U2VydmljZS5jcmVhdGVUcnVzdGVkVXJsKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy50aHVtYm5haWxMb2FkZWQuZW1pdCh0aGlzLmNvbnRlbnQudGh1bWJuYWlsVXJsKTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVycm9yLmVtaXQoZXJyb3IpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9wZW5WaWV3ZXIoY29udGVudDogQ29udGVudExpbmtNb2RlbCk6IHZvaWQge1xyXG4gICAgICAgIGxldCBmZXRjaCA9IHRoaXMucHJvY2Vzc0NvbnRlbnRTZXJ2aWNlLmdldENvbnRlbnRQcmV2aWV3KGNvbnRlbnQuaWQpO1xyXG4gICAgICAgIGlmIChjb250ZW50LmlzVHlwZUltYWdlKCkgfHwgY29udGVudC5pc1R5cGVQZGYoKSkge1xyXG4gICAgICAgICAgICBmZXRjaCA9IHRoaXMucHJvY2Vzc0NvbnRlbnRTZXJ2aWNlLmdldEZpbGVSYXdDb250ZW50KGNvbnRlbnQuaWQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBmZXRjaC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgIChibG9iOiBCbG9iKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb250ZW50LmNvbnRlbnRCbG9iID0gYmxvYjtcclxuICAgICAgICAgICAgICAgIHRoaXMuY29udGVudENsaWNrLmVtaXQoY29udGVudCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuaW5mbygnQ29udGVudCBjbGlja2VkJyArIGNvbnRlbnQuaWQpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mb3JtU2VydmljZS5mb3JtQ29udGVudENsaWNrZWQubmV4dChjb250ZW50KTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVycm9yLmVtaXQoZXJyb3IpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEludm9rZSBjb250ZW50IGRvd25sb2FkLlxyXG4gICAgICovXHJcbiAgICBkb3dubG9hZChjb250ZW50OiBDb250ZW50TGlua01vZGVsKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5wcm9jZXNzQ29udGVudFNlcnZpY2UuZ2V0RmlsZVJhd0NvbnRlbnQoY29udGVudC5pZCkuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAoYmxvYjogQmxvYikgPT4gdGhpcy5jb250ZW50U2VydmljZS5kb3dubG9hZEJsb2IoYmxvYiwgY29udGVudC5uYW1lKSxcclxuICAgICAgICAgICAgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVycm9yLmVtaXQoZXJyb3IpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=