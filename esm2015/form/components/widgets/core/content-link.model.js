/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
export class ContentLinkModel {
    /**
     * @param {?=} obj
     */
    constructor(obj) {
        this.contentAvailable = obj && obj.contentAvailable;
        this.created = obj && obj.created;
        this.createdBy = obj && obj.createdBy || {};
        this.id = obj && obj.id;
        this.link = obj && obj.link;
        this.mimeType = obj && obj.mimeType;
        this.name = obj && obj.name;
        this.previewStatus = obj && obj.previewStatus;
        this.relatedContent = obj && obj.relatedContent;
        this.simpleType = obj && obj.simpleType;
        this.thumbnailStatus = obj && obj.thumbnailStatus;
    }
    /**
     * @return {?}
     */
    hasPreviewStatus() {
        return this.previewStatus === 'supported' ? true : false;
    }
    /**
     * @return {?}
     */
    isTypeImage() {
        return this.simpleType === 'image' ? true : false;
    }
    /**
     * @return {?}
     */
    isTypePdf() {
        return this.simpleType === 'pdf' ? true : false;
    }
    /**
     * @return {?}
     */
    isTypeDoc() {
        return this.simpleType === 'word' || this.simpleType === 'content' ? true : false;
    }
    /**
     * @return {?}
     */
    isThumbnailReady() {
        return this.thumbnailStatus === 'created';
    }
    /**
     * @return {?}
     */
    isThumbnailSupported() {
        return this.isTypeImage() || ((this.isTypePdf() || this.isTypeDoc()) && this.isThumbnailReady());
    }
}
if (false) {
    /** @type {?} */
    ContentLinkModel.prototype.contentAvailable;
    /** @type {?} */
    ContentLinkModel.prototype.created;
    /** @type {?} */
    ContentLinkModel.prototype.createdBy;
    /** @type {?} */
    ContentLinkModel.prototype.id;
    /** @type {?} */
    ContentLinkModel.prototype.link;
    /** @type {?} */
    ContentLinkModel.prototype.mimeType;
    /** @type {?} */
    ContentLinkModel.prototype.name;
    /** @type {?} */
    ContentLinkModel.prototype.previewStatus;
    /** @type {?} */
    ContentLinkModel.prototype.relatedContent;
    /** @type {?} */
    ContentLinkModel.prototype.simpleType;
    /** @type {?} */
    ContentLinkModel.prototype.thumbnailUrl;
    /** @type {?} */
    ContentLinkModel.prototype.contentRawUrl;
    /** @type {?} */
    ContentLinkModel.prototype.contentBlob;
    /** @type {?} */
    ContentLinkModel.prototype.thumbnailStatus;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC1saW5rLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvY29yZS9jb250ZW50LWxpbmsubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBcUJDLE1BQU0sT0FBTyxnQkFBZ0I7Ozs7SUFpQjFCLFlBQVksR0FBUztRQUNqQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQztRQUNwRCxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxTQUFTLElBQUksRUFBRSxDQUFDO1FBQzVDLElBQUksQ0FBQyxFQUFFLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxFQUFFLENBQUM7UUFDeEIsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLElBQUksQ0FBQztRQUM1QixJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUM7UUFDNUIsSUFBSSxDQUFDLGFBQWEsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLGFBQWEsQ0FBQztRQUM5QyxJQUFJLENBQUMsY0FBYyxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsY0FBYyxDQUFDO1FBQ2hELElBQUksQ0FBQyxVQUFVLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxVQUFVLENBQUM7UUFDeEMsSUFBSSxDQUFDLGVBQWUsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLGVBQWUsQ0FBQztJQUN0RCxDQUFDOzs7O0lBRUQsZ0JBQWdCO1FBQ1osT0FBTyxJQUFJLENBQUMsYUFBYSxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDN0QsQ0FBQzs7OztJQUVELFdBQVc7UUFDUCxPQUFPLElBQUksQ0FBQyxVQUFVLEtBQUssT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUN0RCxDQUFDOzs7O0lBRUQsU0FBUztRQUNMLE9BQU8sSUFBSSxDQUFDLFVBQVUsS0FBSyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ3BELENBQUM7Ozs7SUFFRCxTQUFTO1FBQ0wsT0FBTyxJQUFJLENBQUMsVUFBVSxLQUFLLE1BQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDdEYsQ0FBQzs7OztJQUVELGdCQUFnQjtRQUNaLE9BQU8sSUFBSSxDQUFDLGVBQWUsS0FBSyxTQUFTLENBQUM7SUFDOUMsQ0FBQzs7OztJQUVELG9CQUFvQjtRQUNoQixPQUFPLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7SUFDckcsQ0FBQztDQUNKOzs7SUFwREcsNENBQTBCOztJQUMxQixtQ0FBYzs7SUFDZCxxQ0FBZTs7SUFDZiw4QkFBVzs7SUFDWCxnQ0FBYzs7SUFDZCxvQ0FBaUI7O0lBQ2pCLGdDQUFhOztJQUNiLHlDQUFzQjs7SUFDdEIsMENBQXdCOztJQUN4QixzQ0FBbUI7O0lBQ25CLHdDQUFxQjs7SUFDckIseUNBQXNCOztJQUN0Qix1Q0FBa0I7O0lBQ2xCLDJDQUF3QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4gLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuIGltcG9ydCB7IFJlbGF0ZWRDb250ZW50UmVwcmVzZW50YXRpb24gfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuXHJcbiBleHBvcnQgY2xhc3MgQ29udGVudExpbmtNb2RlbCBpbXBsZW1lbnRzIFJlbGF0ZWRDb250ZW50UmVwcmVzZW50YXRpb24ge1xyXG5cclxuICAgIGNvbnRlbnRBdmFpbGFibGU6IGJvb2xlYW47XHJcbiAgICBjcmVhdGVkOiBEYXRlO1xyXG4gICAgY3JlYXRlZEJ5OiBhbnk7XHJcbiAgICBpZDogbnVtYmVyO1xyXG4gICAgbGluazogYm9vbGVhbjtcclxuICAgIG1pbWVUeXBlOiBzdHJpbmc7XHJcbiAgICBuYW1lOiBzdHJpbmc7XHJcbiAgICBwcmV2aWV3U3RhdHVzOiBzdHJpbmc7XHJcbiAgICByZWxhdGVkQ29udGVudDogYm9vbGVhbjtcclxuICAgIHNpbXBsZVR5cGU6IHN0cmluZztcclxuICAgIHRodW1ibmFpbFVybDogc3RyaW5nO1xyXG4gICAgY29udGVudFJhd1VybDogc3RyaW5nO1xyXG4gICAgY29udGVudEJsb2I6IEJsb2I7XHJcbiAgICB0aHVtYm5haWxTdGF0dXM6IHN0cmluZztcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihvYmo/OiBhbnkpIHtcclxuICAgICAgICB0aGlzLmNvbnRlbnRBdmFpbGFibGUgPSBvYmogJiYgb2JqLmNvbnRlbnRBdmFpbGFibGU7XHJcbiAgICAgICAgdGhpcy5jcmVhdGVkID0gb2JqICYmIG9iai5jcmVhdGVkO1xyXG4gICAgICAgIHRoaXMuY3JlYXRlZEJ5ID0gb2JqICYmIG9iai5jcmVhdGVkQnkgfHwge307XHJcbiAgICAgICAgdGhpcy5pZCA9IG9iaiAmJiBvYmouaWQ7XHJcbiAgICAgICAgdGhpcy5saW5rID0gb2JqICYmIG9iai5saW5rO1xyXG4gICAgICAgIHRoaXMubWltZVR5cGUgPSBvYmogJiYgb2JqLm1pbWVUeXBlO1xyXG4gICAgICAgIHRoaXMubmFtZSA9IG9iaiAmJiBvYmoubmFtZTtcclxuICAgICAgICB0aGlzLnByZXZpZXdTdGF0dXMgPSBvYmogJiYgb2JqLnByZXZpZXdTdGF0dXM7XHJcbiAgICAgICAgdGhpcy5yZWxhdGVkQ29udGVudCA9IG9iaiAmJiBvYmoucmVsYXRlZENvbnRlbnQ7XHJcbiAgICAgICAgdGhpcy5zaW1wbGVUeXBlID0gb2JqICYmIG9iai5zaW1wbGVUeXBlO1xyXG4gICAgICAgIHRoaXMudGh1bWJuYWlsU3RhdHVzID0gb2JqICYmIG9iai50aHVtYm5haWxTdGF0dXM7XHJcbiAgICB9XHJcblxyXG4gICAgaGFzUHJldmlld1N0YXR1cygpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcmV2aWV3U3RhdHVzID09PSAnc3VwcG9ydGVkJyA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBpc1R5cGVJbWFnZSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zaW1wbGVUeXBlID09PSAnaW1hZ2UnID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzVHlwZVBkZigpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zaW1wbGVUeXBlID09PSAncGRmJyA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBpc1R5cGVEb2MoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2ltcGxlVHlwZSA9PT0gJ3dvcmQnIHx8IHRoaXMuc2ltcGxlVHlwZSA9PT0gJ2NvbnRlbnQnID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzVGh1bWJuYWlsUmVhZHkoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudGh1bWJuYWlsU3RhdHVzID09PSAnY3JlYXRlZCc7XHJcbiAgICB9XHJcblxyXG4gICAgaXNUaHVtYm5haWxTdXBwb3J0ZWQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNUeXBlSW1hZ2UoKSB8fCAoKHRoaXMuaXNUeXBlUGRmKCkgfHwgdGhpcy5pc1R5cGVEb2MoKSkgJiYgdGhpcy5pc1RodW1ibmFpbFJlYWR5KCkpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==