/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
/**
 * @record
 */
export function ExternalContentLink() { }
if (false) {
    /** @type {?} */
    ExternalContentLink.prototype.contentAvailable;
    /** @type {?} */
    ExternalContentLink.prototype.created;
    /** @type {?} */
    ExternalContentLink.prototype.createdBy;
    /** @type {?} */
    ExternalContentLink.prototype.id;
    /** @type {?} */
    ExternalContentLink.prototype.link;
    /** @type {?} */
    ExternalContentLink.prototype.mimeType;
    /** @type {?} */
    ExternalContentLink.prototype.name;
    /** @type {?} */
    ExternalContentLink.prototype.previewStatus;
    /** @type {?} */
    ExternalContentLink.prototype.relatedContent;
    /** @type {?} */
    ExternalContentLink.prototype.simpleType;
    /** @type {?} */
    ExternalContentLink.prototype.source;
    /** @type {?} */
    ExternalContentLink.prototype.sourceId;
    /** @type {?} */
    ExternalContentLink.prototype.thumbnailStatus;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXh0ZXJuYWwtY29udGVudC1saW5rLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvY29yZS9leHRlcm5hbC1jb250ZW50LWxpbmsudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLHlDQWNDOzs7SUFiRywrQ0FBMEI7O0lBQzFCLHNDQUFnQjs7SUFDaEIsd0NBQWU7O0lBQ2YsaUNBQVc7O0lBQ1gsbUNBQWM7O0lBQ2QsdUNBQWlCOztJQUNqQixtQ0FBYTs7SUFDYiw0Q0FBc0I7O0lBQ3RCLDZDQUF3Qjs7SUFDeEIseUNBQW1COztJQUNuQixxQ0FBZTs7SUFDZix1Q0FBaUI7O0lBQ2pCLDhDQUF3QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4gLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBFeHRlcm5hbENvbnRlbnRMaW5rIHtcclxuICAgIGNvbnRlbnRBdmFpbGFibGU6IGJvb2xlYW47XHJcbiAgICBjcmVhdGVkOiBzdHJpbmc7XHJcbiAgICBjcmVhdGVkQnk6IGFueTtcclxuICAgIGlkOiBudW1iZXI7XHJcbiAgICBsaW5rOiBib29sZWFuO1xyXG4gICAgbWltZVR5cGU6IHN0cmluZztcclxuICAgIG5hbWU6IHN0cmluZztcclxuICAgIHByZXZpZXdTdGF0dXM6IHN0cmluZztcclxuICAgIHJlbGF0ZWRDb250ZW50OiBib29sZWFuO1xyXG4gICAgc2ltcGxlVHlwZTogc3RyaW5nO1xyXG4gICAgc291cmNlOiBzdHJpbmc7XHJcbiAgICBzb3VyY2VJZDogc3RyaW5nO1xyXG4gICAgdGh1bWJuYWlsU3RhdHVzOiBzdHJpbmc7XHJcbn1cclxuIl19