/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import moment from 'moment-es6';
import { FormFieldTypes } from './form-field-types';
/**
 * @record
 */
export function FormFieldValidator() { }
if (false) {
    /**
     * @param {?} field
     * @return {?}
     */
    FormFieldValidator.prototype.isSupported = function (field) { };
    /**
     * @param {?} field
     * @return {?}
     */
    FormFieldValidator.prototype.validate = function (field) { };
}
export class RequiredFieldValidator {
    constructor() {
        this.supportedTypes = [
            FormFieldTypes.TEXT,
            FormFieldTypes.MULTILINE_TEXT,
            FormFieldTypes.NUMBER,
            FormFieldTypes.BOOLEAN,
            FormFieldTypes.TYPEAHEAD,
            FormFieldTypes.DROPDOWN,
            FormFieldTypes.PEOPLE,
            FormFieldTypes.FUNCTIONAL_GROUP,
            FormFieldTypes.RADIO_BUTTONS,
            FormFieldTypes.UPLOAD,
            FormFieldTypes.AMOUNT,
            FormFieldTypes.DYNAMIC_TABLE,
            FormFieldTypes.DATE,
            FormFieldTypes.DATETIME,
            FormFieldTypes.ATTACH_FOLDER
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    isSupported(field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 &&
            field.required;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    validate(field) {
        if (this.isSupported(field) && field.isVisible) {
            if (field.type === FormFieldTypes.DROPDOWN) {
                if (field.hasEmptyValue && field.emptyOption) {
                    if (field.value === field.emptyOption.id) {
                        return false;
                    }
                }
            }
            if (field.type === FormFieldTypes.RADIO_BUTTONS) {
                /** @type {?} */
                const option = field.options.find((/**
                 * @param {?} opt
                 * @return {?}
                 */
                (opt) => opt.id === field.value));
                return !!option;
            }
            if (field.type === FormFieldTypes.UPLOAD) {
                return field.value && field.value.length > 0;
            }
            if (field.type === FormFieldTypes.DYNAMIC_TABLE) {
                return field.value && field.value instanceof Array && field.value.length > 0;
            }
            if (field.type === FormFieldTypes.BOOLEAN) {
                return field.value ? true : false;
            }
            if (field.value === null || field.value === undefined || field.value === '') {
                return false;
            }
        }
        return true;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    RequiredFieldValidator.prototype.supportedTypes;
}
export class NumberFieldValidator {
    constructor() {
        this.supportedTypes = [
            FormFieldTypes.NUMBER,
            FormFieldTypes.AMOUNT
        ];
    }
    /**
     * @param {?} value
     * @return {?}
     */
    static isNumber(value) {
        if (value === null || value === undefined || value === '') {
            return false;
        }
        return !isNaN(+value);
    }
    /**
     * @param {?} field
     * @return {?}
     */
    isSupported(field) {
        return field && this.supportedTypes.indexOf(field.type) > -1;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    validate(field) {
        if (this.isSupported(field) && field.isVisible) {
            if (field.value === null ||
                field.value === undefined ||
                field.value === '') {
                return true;
            }
            /** @type {?} */
            const valueStr = '' + field.value;
            /** @type {?} */
            let pattern = new RegExp(/^-?\d+$/);
            if (field.enableFractions) {
                pattern = new RegExp(/^-?[0-9]+(\.[0-9]{1,2})?$/);
            }
            if (valueStr.match(pattern)) {
                return true;
            }
            field.validationSummary.message = 'FORM.FIELD.VALIDATOR.INVALID_NUMBER';
            return false;
        }
        return true;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    NumberFieldValidator.prototype.supportedTypes;
}
export class DateFieldValidator {
    constructor() {
        this.supportedTypes = [
            FormFieldTypes.DATE
        ];
    }
    // Validates that the input string is a valid date formatted as <dateFormat> (default D-M-YYYY)
    /**
     * @param {?} inputDate
     * @param {?=} dateFormat
     * @return {?}
     */
    static isValidDate(inputDate, dateFormat = 'D-M-YYYY') {
        if (inputDate) {
            /** @type {?} */
            const d = moment(inputDate, dateFormat, true);
            return d.isValid();
        }
        return false;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    isSupported(field) {
        return field && this.supportedTypes.indexOf(field.type) > -1;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    validate(field) {
        if (this.isSupported(field) && field.value && field.isVisible) {
            if (DateFieldValidator.isValidDate(field.value, field.dateDisplayFormat)) {
                return true;
            }
            field.validationSummary.message = field.dateDisplayFormat;
            return false;
        }
        return true;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    DateFieldValidator.prototype.supportedTypes;
}
export class MinDateFieldValidator {
    constructor() {
        this.supportedTypes = [
            FormFieldTypes.DATE
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    isSupported(field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 && !!field.minValue;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    validate(field) {
        /** @type {?} */
        let isValid = true;
        if (this.isSupported(field) && field.value && field.isVisible) {
            /** @type {?} */
            const dateFormat = field.dateDisplayFormat;
            if (!DateFieldValidator.isValidDate(field.value, dateFormat)) {
                field.validationSummary.message = 'FORM.FIELD.VALIDATOR.INVALID_DATE';
                isValid = false;
            }
            else {
                isValid = this.checkDate(field, dateFormat);
            }
        }
        return isValid;
    }
    /**
     * @private
     * @param {?} field
     * @param {?} dateFormat
     * @return {?}
     */
    checkDate(field, dateFormat) {
        /** @type {?} */
        const MIN_DATE_FORMAT = 'DD-MM-YYYY';
        /** @type {?} */
        let isValid = true;
        // remove time and timezone info
        /** @type {?} */
        let fieldValueData;
        if (typeof field.value === 'string') {
            fieldValueData = moment(field.value.split('T')[0], dateFormat);
        }
        else {
            fieldValueData = field.value;
        }
        /** @type {?} */
        const min = moment(field.minValue, MIN_DATE_FORMAT);
        if (fieldValueData.isBefore(min)) {
            field.validationSummary.message = `FORM.FIELD.VALIDATOR.NOT_LESS_THAN`;
            field.validationSummary.attributes.set('minValue', min.format(field.dateDisplayFormat).toLocaleUpperCase());
            isValid = false;
        }
        return isValid;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MinDateFieldValidator.prototype.supportedTypes;
}
export class MaxDateFieldValidator {
    constructor() {
        this.MAX_DATE_FORMAT = 'DD-MM-YYYY';
        this.supportedTypes = [
            FormFieldTypes.DATE
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    isSupported(field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 && !!field.maxValue;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    validate(field) {
        if (this.isSupported(field) && field.value && field.isVisible) {
            /** @type {?} */
            const dateFormat = field.dateDisplayFormat;
            if (!DateFieldValidator.isValidDate(field.value, dateFormat)) {
                field.validationSummary.message = 'FORM.FIELD.VALIDATOR.INVALID_DATE';
                return false;
            }
            // remove time and timezone info
            /** @type {?} */
            let d;
            if (typeof field.value === 'string') {
                d = moment(field.value.split('T')[0], dateFormat);
            }
            else {
                d = field.value;
            }
            /** @type {?} */
            const max = moment(field.maxValue, this.MAX_DATE_FORMAT);
            if (d.isAfter(max)) {
                field.validationSummary.message = `FORM.FIELD.VALIDATOR.NOT_GREATER_THAN`;
                field.validationSummary.attributes.set('maxValue', max.format(field.dateDisplayFormat).toLocaleUpperCase());
                return false;
            }
        }
        return true;
    }
}
if (false) {
    /** @type {?} */
    MaxDateFieldValidator.prototype.MAX_DATE_FORMAT;
    /**
     * @type {?}
     * @private
     */
    MaxDateFieldValidator.prototype.supportedTypes;
}
export class MinDateTimeFieldValidator {
    constructor() {
        this.supportedTypes = [
            FormFieldTypes.DATETIME
        ];
        this.MIN_DATETIME_FORMAT = 'YYYY-MM-DD hh:mm AZ';
    }
    /**
     * @param {?} field
     * @return {?}
     */
    isSupported(field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 && !!field.minValue;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    validate(field) {
        /** @type {?} */
        let isValid = true;
        if (this.isSupported(field) && field.value && field.isVisible) {
            /** @type {?} */
            const dateFormat = field.dateDisplayFormat;
            if (!DateFieldValidator.isValidDate(field.value, dateFormat)) {
                field.validationSummary.message = 'FORM.FIELD.VALIDATOR.INVALID_DATE';
                isValid = false;
            }
            else {
                isValid = this.checkDateTime(field, dateFormat);
            }
        }
        return isValid;
    }
    /**
     * @private
     * @param {?} field
     * @param {?} dateFormat
     * @return {?}
     */
    checkDateTime(field, dateFormat) {
        /** @type {?} */
        let isValid = true;
        /** @type {?} */
        let fieldValueDate;
        if (typeof field.value === 'string') {
            fieldValueDate = moment(field.value, dateFormat);
        }
        else {
            fieldValueDate = field.value;
        }
        /** @type {?} */
        const min = moment(field.minValue, this.MIN_DATETIME_FORMAT);
        if (fieldValueDate.isBefore(min)) {
            field.validationSummary.message = `FORM.FIELD.VALIDATOR.NOT_LESS_THAN`;
            field.validationSummary.attributes.set('minValue', min.format(field.dateDisplayFormat).replace(':', '-'));
            isValid = false;
        }
        return isValid;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MinDateTimeFieldValidator.prototype.supportedTypes;
    /** @type {?} */
    MinDateTimeFieldValidator.prototype.MIN_DATETIME_FORMAT;
}
export class MaxDateTimeFieldValidator {
    constructor() {
        this.supportedTypes = [
            FormFieldTypes.DATETIME
        ];
        this.MAX_DATETIME_FORMAT = 'YYYY-MM-DD hh:mm AZ';
    }
    /**
     * @param {?} field
     * @return {?}
     */
    isSupported(field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 && !!field.maxValue;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    validate(field) {
        /** @type {?} */
        let isValid = true;
        if (this.isSupported(field) && field.value && field.isVisible) {
            /** @type {?} */
            const dateFormat = field.dateDisplayFormat;
            if (!DateFieldValidator.isValidDate(field.value, dateFormat)) {
                field.validationSummary.message = 'FORM.FIELD.VALIDATOR.INVALID_DATE';
                isValid = false;
            }
            else {
                isValid = this.checkDateTime(field, dateFormat);
            }
        }
        return isValid;
    }
    /**
     * @private
     * @param {?} field
     * @param {?} dateFormat
     * @return {?}
     */
    checkDateTime(field, dateFormat) {
        /** @type {?} */
        let isValid = true;
        /** @type {?} */
        let fieldValueDate;
        if (typeof field.value === 'string') {
            fieldValueDate = moment(field.value, dateFormat);
        }
        else {
            fieldValueDate = field.value;
        }
        /** @type {?} */
        const max = moment(field.maxValue, this.MAX_DATETIME_FORMAT);
        if (fieldValueDate.isAfter(max)) {
            field.validationSummary.message = `FORM.FIELD.VALIDATOR.NOT_GREATER_THAN`;
            field.validationSummary.attributes.set('maxValue', max.format(field.dateDisplayFormat).replace(':', '-'));
            isValid = false;
        }
        return isValid;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MaxDateTimeFieldValidator.prototype.supportedTypes;
    /** @type {?} */
    MaxDateTimeFieldValidator.prototype.MAX_DATETIME_FORMAT;
}
export class MinLengthFieldValidator {
    constructor() {
        this.supportedTypes = [
            FormFieldTypes.TEXT,
            FormFieldTypes.MULTILINE_TEXT
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    isSupported(field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 &&
            field.minLength > 0;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    validate(field) {
        if (this.isSupported(field) && field.value && field.isVisible) {
            if (field.value.length >= field.minLength) {
                return true;
            }
            field.validationSummary.message = `FORM.FIELD.VALIDATOR.AT_LEAST_LONG`;
            field.validationSummary.attributes.set('minLength', field.minLength.toLocaleString());
            return false;
        }
        return true;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MinLengthFieldValidator.prototype.supportedTypes;
}
export class MaxLengthFieldValidator {
    constructor() {
        this.supportedTypes = [
            FormFieldTypes.TEXT,
            FormFieldTypes.MULTILINE_TEXT
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    isSupported(field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 &&
            field.maxLength > 0;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    validate(field) {
        if (this.isSupported(field) && field.value && field.isVisible) {
            if (field.value.length <= field.maxLength) {
                return true;
            }
            field.validationSummary.message = `FORM.FIELD.VALIDATOR.NO_LONGER_THAN`;
            field.validationSummary.attributes.set('maxLength', field.maxLength.toLocaleString());
            return false;
        }
        return true;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MaxLengthFieldValidator.prototype.supportedTypes;
}
export class MinValueFieldValidator {
    constructor() {
        this.supportedTypes = [
            FormFieldTypes.NUMBER,
            FormFieldTypes.AMOUNT
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    isSupported(field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 &&
            NumberFieldValidator.isNumber(field.minValue);
    }
    /**
     * @param {?} field
     * @return {?}
     */
    validate(field) {
        if (this.isSupported(field) && field.value && field.isVisible) {
            /** @type {?} */
            const value = +field.value;
            /** @type {?} */
            const minValue = +field.minValue;
            if (value >= minValue) {
                return true;
            }
            field.validationSummary.message = `FORM.FIELD.VALIDATOR.NOT_LESS_THAN`;
            field.validationSummary.attributes.set('minValue', field.minValue.toLocaleString());
            return false;
        }
        return true;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MinValueFieldValidator.prototype.supportedTypes;
}
export class MaxValueFieldValidator {
    constructor() {
        this.supportedTypes = [
            FormFieldTypes.NUMBER,
            FormFieldTypes.AMOUNT
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    isSupported(field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 &&
            NumberFieldValidator.isNumber(field.maxValue);
    }
    /**
     * @param {?} field
     * @return {?}
     */
    validate(field) {
        if (this.isSupported(field) && field.value && field.isVisible) {
            /** @type {?} */
            const value = +field.value;
            /** @type {?} */
            const maxValue = +field.maxValue;
            if (value <= maxValue) {
                return true;
            }
            field.validationSummary.message = `FORM.FIELD.VALIDATOR.NOT_GREATER_THAN`;
            field.validationSummary.attributes.set('maxValue', field.maxValue.toLocaleString());
            return false;
        }
        return true;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MaxValueFieldValidator.prototype.supportedTypes;
}
export class RegExFieldValidator {
    constructor() {
        this.supportedTypes = [
            FormFieldTypes.TEXT,
            FormFieldTypes.MULTILINE_TEXT
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    isSupported(field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 && !!field.regexPattern;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    validate(field) {
        if (this.isSupported(field) && field.value && field.isVisible) {
            if (field.value.length > 0 && field.value.match(new RegExp('^' + field.regexPattern + '$'))) {
                return true;
            }
            field.validationSummary.message = 'FORM.FIELD.VALIDATOR.INVALID_VALUE';
            return false;
        }
        return true;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    RegExFieldValidator.prototype.supportedTypes;
}
export class FixedValueFieldValidator {
    constructor() {
        this.supportedTypes = [
            FormFieldTypes.TYPEAHEAD
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    isSupported(field) {
        return field && this.supportedTypes.indexOf(field.type) > -1;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    hasValidNameOrValidId(field) {
        return this.hasValidName(field) || this.hasValidId(field);
    }
    /**
     * @param {?} field
     * @return {?}
     */
    hasValidName(field) {
        return field.options.find((/**
         * @param {?} item
         * @return {?}
         */
        (item) => item.name && item.name.toLocaleLowerCase() === field.value.toLocaleLowerCase())) ? true : false;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    hasValidId(field) {
        return field.options.find((/**
         * @param {?} item
         * @return {?}
         */
        (item) => item.id === field.value)) ? true : false;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    hasStringValue(field) {
        return field.value && typeof field.value === 'string';
    }
    /**
     * @param {?} field
     * @return {?}
     */
    hasOptions(field) {
        return field.options && field.options.length > 0;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    validate(field) {
        if (this.isSupported(field) && field.isVisible) {
            if (this.hasStringValue(field) && this.hasOptions(field) && !this.hasValidNameOrValidId(field)) {
                field.validationSummary.message = 'FORM.FIELD.VALIDATOR.INVALID_VALUE';
                return false;
            }
        }
        return true;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    FixedValueFieldValidator.prototype.supportedTypes;
}
/** @type {?} */
export const FORM_FIELD_VALIDATORS = [
    new RequiredFieldValidator(),
    new NumberFieldValidator(),
    new MinLengthFieldValidator(),
    new MaxLengthFieldValidator(),
    new MinValueFieldValidator(),
    new MaxValueFieldValidator(),
    new RegExFieldValidator(),
    new DateFieldValidator(),
    new MinDateFieldValidator(),
    new MaxDateFieldValidator(),
    new FixedValueFieldValidator(),
    new MinDateTimeFieldValidator(),
    new MaxDateTimeFieldValidator()
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC12YWxpZGF0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9jb3JlL2Zvcm0tZmllbGQtdmFsaWRhdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLE1BQU0sTUFBTSxZQUFZLENBQUM7QUFDaEMsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDOzs7O0FBR3BELHdDQU1DOzs7Ozs7SUFKRyxnRUFBNEM7Ozs7O0lBRTVDLDZEQUF5Qzs7QUFJN0MsTUFBTSxPQUFPLHNCQUFzQjtJQUFuQztRQUVZLG1CQUFjLEdBQUc7WUFDckIsY0FBYyxDQUFDLElBQUk7WUFDbkIsY0FBYyxDQUFDLGNBQWM7WUFDN0IsY0FBYyxDQUFDLE1BQU07WUFDckIsY0FBYyxDQUFDLE9BQU87WUFDdEIsY0FBYyxDQUFDLFNBQVM7WUFDeEIsY0FBYyxDQUFDLFFBQVE7WUFDdkIsY0FBYyxDQUFDLE1BQU07WUFDckIsY0FBYyxDQUFDLGdCQUFnQjtZQUMvQixjQUFjLENBQUMsYUFBYTtZQUM1QixjQUFjLENBQUMsTUFBTTtZQUNyQixjQUFjLENBQUMsTUFBTTtZQUNyQixjQUFjLENBQUMsYUFBYTtZQUM1QixjQUFjLENBQUMsSUFBSTtZQUNuQixjQUFjLENBQUMsUUFBUTtZQUN2QixjQUFjLENBQUMsYUFBYTtTQUMvQixDQUFDO0lBMkNOLENBQUM7Ozs7O0lBekNHLFdBQVcsQ0FBQyxLQUFxQjtRQUM3QixPQUFPLEtBQUs7WUFDUixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzVDLEtBQUssQ0FBQyxRQUFRLENBQUM7SUFDdkIsQ0FBQzs7Ozs7SUFFRCxRQUFRLENBQUMsS0FBcUI7UUFDMUIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFFNUMsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3hDLElBQUksS0FBSyxDQUFDLGFBQWEsSUFBSSxLQUFLLENBQUMsV0FBVyxFQUFFO29CQUMxQyxJQUFJLEtBQUssQ0FBQyxLQUFLLEtBQUssS0FBSyxDQUFDLFdBQVcsQ0FBQyxFQUFFLEVBQUU7d0JBQ3RDLE9BQU8sS0FBSyxDQUFDO3FCQUNoQjtpQkFDSjthQUNKO1lBRUQsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxhQUFhLEVBQUU7O3NCQUN2QyxNQUFNLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJOzs7O2dCQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLEtBQUssQ0FBQyxLQUFLLEVBQUM7Z0JBQ2xFLE9BQU8sQ0FBQyxDQUFDLE1BQU0sQ0FBQzthQUNuQjtZQUVELElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxjQUFjLENBQUMsTUFBTSxFQUFFO2dCQUN0QyxPQUFPLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO2FBQ2hEO1lBRUQsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxhQUFhLEVBQUU7Z0JBQzdDLE9BQU8sS0FBSyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsS0FBSyxZQUFZLEtBQUssSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7YUFDaEY7WUFFRCxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssY0FBYyxDQUFDLE9BQU8sRUFBRTtnQkFDdkMsT0FBTyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQzthQUNyQztZQUVELElBQUksS0FBSyxDQUFDLEtBQUssS0FBSyxJQUFJLElBQUksS0FBSyxDQUFDLEtBQUssS0FBSyxTQUFTLElBQUksS0FBSyxDQUFDLEtBQUssS0FBSyxFQUFFLEVBQUU7Z0JBQ3pFLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1NBQ0o7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0NBRUo7Ozs7OztJQTNERyxnREFnQkU7O0FBNkNOLE1BQU0sT0FBTyxvQkFBb0I7SUFBakM7UUFFWSxtQkFBYyxHQUFHO1lBQ3JCLGNBQWMsQ0FBQyxNQUFNO1lBQ3JCLGNBQWMsQ0FBQyxNQUFNO1NBQ3hCLENBQUM7SUFrQ04sQ0FBQzs7Ozs7SUFoQ0csTUFBTSxDQUFDLFFBQVEsQ0FBQyxLQUFVO1FBQ3RCLElBQUksS0FBSyxLQUFLLElBQUksSUFBSSxLQUFLLEtBQUssU0FBUyxJQUFJLEtBQUssS0FBSyxFQUFFLEVBQUU7WUFDdkQsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDMUIsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsS0FBcUI7UUFDN0IsT0FBTyxLQUFLLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ2pFLENBQUM7Ozs7O0lBRUQsUUFBUSxDQUFDLEtBQXFCO1FBQzFCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsU0FBUyxFQUFFO1lBQzVDLElBQUksS0FBSyxDQUFDLEtBQUssS0FBSyxJQUFJO2dCQUNwQixLQUFLLENBQUMsS0FBSyxLQUFLLFNBQVM7Z0JBQ3pCLEtBQUssQ0FBQyxLQUFLLEtBQUssRUFBRSxFQUFFO2dCQUNwQixPQUFPLElBQUksQ0FBQzthQUNmOztrQkFDSyxRQUFRLEdBQUcsRUFBRSxHQUFHLEtBQUssQ0FBQyxLQUFLOztnQkFDN0IsT0FBTyxHQUFHLElBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQztZQUNuQyxJQUFJLEtBQUssQ0FBQyxlQUFlLEVBQUU7Z0JBQ3ZCLE9BQU8sR0FBRyxJQUFJLE1BQU0sQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO2FBQ3JEO1lBQ0QsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUN6QixPQUFPLElBQUksQ0FBQzthQUNmO1lBQ0QsS0FBSyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sR0FBRyxxQ0FBcUMsQ0FBQztZQUN4RSxPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Q0FDSjs7Ozs7O0lBckNHLDhDQUdFOztBQW9DTixNQUFNLE9BQU8sa0JBQWtCO0lBQS9CO1FBRVksbUJBQWMsR0FBRztZQUNyQixjQUFjLENBQUMsSUFBSTtTQUN0QixDQUFDO0lBMEJOLENBQUM7Ozs7Ozs7SUF2QkcsTUFBTSxDQUFDLFdBQVcsQ0FBQyxTQUFpQixFQUFFLGFBQXFCLFVBQVU7UUFDakUsSUFBSSxTQUFTLEVBQUU7O2tCQUNMLENBQUMsR0FBRyxNQUFNLENBQUMsU0FBUyxFQUFFLFVBQVUsRUFBRSxJQUFJLENBQUM7WUFDN0MsT0FBTyxDQUFDLENBQUMsT0FBTyxFQUFFLENBQUM7U0FDdEI7UUFFRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxLQUFxQjtRQUM3QixPQUFPLEtBQUssSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDakUsQ0FBQzs7Ozs7SUFFRCxRQUFRLENBQUMsS0FBcUI7UUFDMUIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLFNBQVMsRUFBRTtZQUMzRCxJQUFJLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxFQUFFO2dCQUN0RSxPQUFPLElBQUksQ0FBQzthQUNmO1lBQ0QsS0FBSyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsaUJBQWlCLENBQUM7WUFDMUQsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0NBQ0o7Ozs7OztJQTVCRyw0Q0FFRTs7QUE0Qk4sTUFBTSxPQUFPLHFCQUFxQjtJQUFsQztRQUVZLG1CQUFjLEdBQUc7WUFDckIsY0FBYyxDQUFDLElBQUk7U0FDdEIsQ0FBQztJQXlDTixDQUFDOzs7OztJQXZDRyxXQUFXLENBQUMsS0FBcUI7UUFDN0IsT0FBTyxLQUFLO1lBQ1IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO0lBQ3pFLENBQUM7Ozs7O0lBRUQsUUFBUSxDQUFDLEtBQXFCOztZQUN0QixPQUFPLEdBQUcsSUFBSTtRQUNsQixJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsU0FBUyxFQUFFOztrQkFDckQsVUFBVSxHQUFHLEtBQUssQ0FBQyxpQkFBaUI7WUFFMUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxFQUFFO2dCQUMxRCxLQUFLLENBQUMsaUJBQWlCLENBQUMsT0FBTyxHQUFHLG1DQUFtQyxDQUFDO2dCQUN0RSxPQUFPLEdBQUcsS0FBSyxDQUFDO2FBQ25CO2lCQUFNO2dCQUNILE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsQ0FBQzthQUMvQztTQUNKO1FBQ0QsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQzs7Ozs7OztJQUVPLFNBQVMsQ0FBQyxLQUFxQixFQUFFLFVBQWtCOztjQUNqRCxlQUFlLEdBQUcsWUFBWTs7WUFDaEMsT0FBTyxHQUFHLElBQUk7OztZQUVkLGNBQWM7UUFDbEIsSUFBSSxPQUFPLEtBQUssQ0FBQyxLQUFLLEtBQUssUUFBUSxFQUFFO1lBQ2pDLGNBQWMsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsVUFBVSxDQUFDLENBQUM7U0FDbEU7YUFBTTtZQUNILGNBQWMsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1NBQ2hDOztjQUNLLEdBQUcsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxlQUFlLENBQUM7UUFFbkQsSUFBSSxjQUFjLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQzlCLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEdBQUcsb0NBQW9DLENBQUM7WUFDdkUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxDQUFDO1lBQzVHLE9BQU8sR0FBRyxLQUFLLENBQUM7U0FDbkI7UUFDRCxPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDO0NBQ0o7Ozs7OztJQTNDRywrQ0FFRTs7QUEyQ04sTUFBTSxPQUFPLHFCQUFxQjtJQUFsQztRQUVJLG9CQUFlLEdBQUcsWUFBWSxDQUFDO1FBRXZCLG1CQUFjLEdBQUc7WUFDckIsY0FBYyxDQUFDLElBQUk7U0FDdEIsQ0FBQztJQWlDTixDQUFDOzs7OztJQS9CRyxXQUFXLENBQUMsS0FBcUI7UUFDN0IsT0FBTyxLQUFLO1lBQ1IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO0lBQ3pFLENBQUM7Ozs7O0lBRUQsUUFBUSxDQUFDLEtBQXFCO1FBQzFCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7O2tCQUNyRCxVQUFVLEdBQUcsS0FBSyxDQUFDLGlCQUFpQjtZQUUxQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLEVBQUU7Z0JBQzFELEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEdBQUcsbUNBQW1DLENBQUM7Z0JBQ3RFLE9BQU8sS0FBSyxDQUFDO2FBQ2hCOzs7Z0JBR0csQ0FBQztZQUNMLElBQUksT0FBTyxLQUFLLENBQUMsS0FBSyxLQUFLLFFBQVEsRUFBRTtnQkFDakMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsQ0FBQzthQUNyRDtpQkFBTTtnQkFDSCxDQUFDLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQzthQUNuQjs7a0JBQ0ssR0FBRyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUM7WUFFeEQsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNoQixLQUFLLENBQUMsaUJBQWlCLENBQUMsT0FBTyxHQUFHLHVDQUF1QyxDQUFDO2dCQUMxRSxLQUFLLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLENBQUM7Z0JBQzVHLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1NBQ0o7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0NBQ0o7OztJQXJDRyxnREFBK0I7Ozs7O0lBRS9CLCtDQUVFOztBQW1DTixNQUFNLE9BQU8seUJBQXlCO0lBQXRDO1FBRVksbUJBQWMsR0FBRztZQUNyQixjQUFjLENBQUMsUUFBUTtTQUMxQixDQUFDO1FBQ0Ysd0JBQW1CLEdBQUcscUJBQXFCLENBQUM7SUF1Q2hELENBQUM7Ozs7O0lBckNHLFdBQVcsQ0FBQyxLQUFxQjtRQUM3QixPQUFPLEtBQUs7WUFDUixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7SUFDekUsQ0FBQzs7Ozs7SUFFRCxRQUFRLENBQUMsS0FBcUI7O1lBQ3RCLE9BQU8sR0FBRyxJQUFJO1FBQ2xCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7O2tCQUNyRCxVQUFVLEdBQUcsS0FBSyxDQUFDLGlCQUFpQjtZQUUxQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLEVBQUU7Z0JBQzFELEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEdBQUcsbUNBQW1DLENBQUM7Z0JBQ3RFLE9BQU8sR0FBRyxLQUFLLENBQUM7YUFDbkI7aUJBQU07Z0JBQ0gsT0FBTyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxDQUFDO2FBQ25EO1NBQ0o7UUFDRCxPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDOzs7Ozs7O0lBRU8sYUFBYSxDQUFDLEtBQXFCLEVBQUUsVUFBa0I7O1lBQ3ZELE9BQU8sR0FBRyxJQUFJOztZQUNkLGNBQWM7UUFDbEIsSUFBSSxPQUFPLEtBQUssQ0FBQyxLQUFLLEtBQUssUUFBUSxFQUFFO1lBQ2pDLGNBQWMsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsQ0FBQztTQUNwRDthQUFNO1lBQ0gsY0FBYyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7U0FDaEM7O2NBQ0ssR0FBRyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztRQUU1RCxJQUFJLGNBQWMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDOUIsS0FBSyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sR0FBRyxvQ0FBb0MsQ0FBQztZQUN2RSxLQUFLLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDMUcsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUNuQjtRQUNELE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7Q0FDSjs7Ozs7O0lBMUNHLG1EQUVFOztJQUNGLHdEQUE0Qzs7QUF5Q2hELE1BQU0sT0FBTyx5QkFBeUI7SUFBdEM7UUFFWSxtQkFBYyxHQUFHO1lBQ3JCLGNBQWMsQ0FBQyxRQUFRO1NBQzFCLENBQUM7UUFDRix3QkFBbUIsR0FBRyxxQkFBcUIsQ0FBQztJQXdDaEQsQ0FBQzs7Ozs7SUF0Q0csV0FBVyxDQUFDLEtBQXFCO1FBQzdCLE9BQU8sS0FBSztZQUNSLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztJQUN6RSxDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxLQUFxQjs7WUFDdEIsT0FBTyxHQUFHLElBQUk7UUFDbEIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLFNBQVMsRUFBRTs7a0JBQ3JELFVBQVUsR0FBRyxLQUFLLENBQUMsaUJBQWlCO1lBRTFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsRUFBRTtnQkFDMUQsS0FBSyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sR0FBRyxtQ0FBbUMsQ0FBQztnQkFDdEUsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUNuQjtpQkFBTTtnQkFDSCxPQUFPLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUM7YUFDbkQ7U0FDSjtRQUNELE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7Ozs7Ozs7SUFFTyxhQUFhLENBQUMsS0FBcUIsRUFBRSxVQUFrQjs7WUFDdkQsT0FBTyxHQUFHLElBQUk7O1lBQ2QsY0FBYztRQUVsQixJQUFJLE9BQU8sS0FBSyxDQUFDLEtBQUssS0FBSyxRQUFRLEVBQUU7WUFDakMsY0FBYyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1NBQ3BEO2FBQU07WUFDSCxjQUFjLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztTQUNoQzs7Y0FDSyxHQUFHLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDO1FBRTVELElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUM3QixLQUFLLENBQUMsaUJBQWlCLENBQUMsT0FBTyxHQUFHLHVDQUF1QyxDQUFDO1lBQzFFLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUMxRyxPQUFPLEdBQUcsS0FBSyxDQUFDO1NBQ25CO1FBQ0QsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztDQUNKOzs7Ozs7SUEzQ0csbURBRUU7O0lBQ0Ysd0RBQTRDOztBQTBDaEQsTUFBTSxPQUFPLHVCQUF1QjtJQUFwQztRQUVZLG1CQUFjLEdBQUc7WUFDckIsY0FBYyxDQUFDLElBQUk7WUFDbkIsY0FBYyxDQUFDLGNBQWM7U0FDaEMsQ0FBQztJQW1CTixDQUFDOzs7OztJQWpCRyxXQUFXLENBQUMsS0FBcUI7UUFDN0IsT0FBTyxLQUFLO1lBQ1IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM1QyxLQUFLLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQztJQUM1QixDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxLQUFxQjtRQUMxQixJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsU0FBUyxFQUFFO1lBQzNELElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksS0FBSyxDQUFDLFNBQVMsRUFBRTtnQkFDdkMsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUNELEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEdBQUcsb0NBQW9DLENBQUM7WUFDdkUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQztZQUN0RixPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Q0FDSjs7Ozs7O0lBdEJHLGlEQUdFOztBQXFCTixNQUFNLE9BQU8sdUJBQXVCO0lBQXBDO1FBRVksbUJBQWMsR0FBRztZQUNyQixjQUFjLENBQUMsSUFBSTtZQUNuQixjQUFjLENBQUMsY0FBYztTQUNoQyxDQUFDO0lBbUJOLENBQUM7Ozs7O0lBakJHLFdBQVcsQ0FBQyxLQUFxQjtRQUM3QixPQUFPLEtBQUs7WUFDUixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzVDLEtBQUssQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO0lBQzVCLENBQUM7Ozs7O0lBRUQsUUFBUSxDQUFDLEtBQXFCO1FBQzFCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFDM0QsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsU0FBUyxFQUFFO2dCQUN2QyxPQUFPLElBQUksQ0FBQzthQUNmO1lBQ0QsS0FBSyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sR0FBRyxxQ0FBcUMsQ0FBQztZQUN4RSxLQUFLLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDO1lBQ3RGLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztDQUNKOzs7Ozs7SUF0QkcsaURBR0U7O0FBcUJOLE1BQU0sT0FBTyxzQkFBc0I7SUFBbkM7UUFFWSxtQkFBYyxHQUFHO1lBQ3JCLGNBQWMsQ0FBQyxNQUFNO1lBQ3JCLGNBQWMsQ0FBQyxNQUFNO1NBQ3hCLENBQUM7SUF1Qk4sQ0FBQzs7Ozs7SUFyQkcsV0FBVyxDQUFDLEtBQXFCO1FBQzdCLE9BQU8sS0FBSztZQUNSLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDNUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN0RCxDQUFDOzs7OztJQUVELFFBQVEsQ0FBQyxLQUFxQjtRQUMxQixJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsU0FBUyxFQUFFOztrQkFDckQsS0FBSyxHQUFXLENBQUMsS0FBSyxDQUFDLEtBQUs7O2tCQUM1QixRQUFRLEdBQVcsQ0FBQyxLQUFLLENBQUMsUUFBUTtZQUV4QyxJQUFJLEtBQUssSUFBSSxRQUFRLEVBQUU7Z0JBQ25CLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7WUFDRCxLQUFLLENBQUMsaUJBQWlCLENBQUMsT0FBTyxHQUFHLG9DQUFvQyxDQUFDO1lBQ3ZFLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUM7WUFDcEYsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0NBQ0o7Ozs7OztJQTFCRyxnREFHRTs7QUF5Qk4sTUFBTSxPQUFPLHNCQUFzQjtJQUFuQztRQUVZLG1CQUFjLEdBQUc7WUFDckIsY0FBYyxDQUFDLE1BQU07WUFDckIsY0FBYyxDQUFDLE1BQU07U0FDeEIsQ0FBQztJQXVCTixDQUFDOzs7OztJQXJCRyxXQUFXLENBQUMsS0FBcUI7UUFDN0IsT0FBTyxLQUFLO1lBQ1IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM1QyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3RELENBQUM7Ozs7O0lBRUQsUUFBUSxDQUFDLEtBQXFCO1FBQzFCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7O2tCQUNyRCxLQUFLLEdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSzs7a0JBQzVCLFFBQVEsR0FBVyxDQUFDLEtBQUssQ0FBQyxRQUFRO1lBRXhDLElBQUksS0FBSyxJQUFJLFFBQVEsRUFBRTtnQkFDbkIsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUNELEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEdBQUcsdUNBQXVDLENBQUM7WUFDMUUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsVUFBVSxFQUFFLEtBQUssQ0FBQyxRQUFRLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQztZQUNwRixPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Q0FDSjs7Ozs7O0lBMUJHLGdEQUdFOztBQXlCTixNQUFNLE9BQU8sbUJBQW1CO0lBQWhDO1FBRVksbUJBQWMsR0FBRztZQUNyQixjQUFjLENBQUMsSUFBSTtZQUNuQixjQUFjLENBQUMsY0FBYztTQUNoQyxDQUFDO0lBa0JOLENBQUM7Ozs7O0lBaEJHLFdBQVcsQ0FBQyxLQUFxQjtRQUM3QixPQUFPLEtBQUs7WUFDUixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUM7SUFDN0UsQ0FBQzs7Ozs7SUFFRCxRQUFRLENBQUMsS0FBcUI7UUFDMUIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLFNBQVMsRUFBRTtZQUMzRCxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLE1BQU0sQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDLFlBQVksR0FBRyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUN6RixPQUFPLElBQUksQ0FBQzthQUNmO1lBQ0QsS0FBSyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sR0FBRyxvQ0FBb0MsQ0FBQztZQUN2RSxPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Q0FFSjs7Ozs7O0lBckJHLDZDQUdFOztBQW9CTixNQUFNLE9BQU8sd0JBQXdCO0lBQXJDO1FBRVksbUJBQWMsR0FBRztZQUNyQixjQUFjLENBQUMsU0FBUztTQUMzQixDQUFDO0lBbUNOLENBQUM7Ozs7O0lBakNHLFdBQVcsQ0FBQyxLQUFxQjtRQUM3QixPQUFPLEtBQUssSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDakUsQ0FBQzs7Ozs7SUFFRCxxQkFBcUIsQ0FBQyxLQUFxQjtRQUN2QyxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUM5RCxDQUFDOzs7OztJQUVELFlBQVksQ0FBQyxLQUFxQjtRQUM5QixPQUFPLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSTs7OztRQUFDLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsS0FBSyxLQUFLLENBQUMsS0FBSyxDQUFDLGlCQUFpQixFQUFFLEVBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDdkksQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsS0FBcUI7UUFDNUIsT0FBTyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUk7Ozs7UUFBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxLQUFLLENBQUMsS0FBSyxFQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ2hGLENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLEtBQXFCO1FBQ2hDLE9BQU8sS0FBSyxDQUFDLEtBQUssSUFBSSxPQUFPLEtBQUssQ0FBQyxLQUFLLEtBQUssUUFBUSxDQUFDO0lBQzFELENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLEtBQXFCO1FBQzVCLE9BQU8sS0FBSyxDQUFDLE9BQU8sSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDckQsQ0FBQzs7Ozs7SUFFRCxRQUFRLENBQUMsS0FBcUI7UUFDMUIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFDNUMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQzVGLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEdBQUcsb0NBQW9DLENBQUM7Z0JBQ3ZFLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1NBQ0o7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0NBQ0o7Ozs7OztJQXJDRyxrREFFRTs7O0FBcUNOLE1BQU0sT0FBTyxxQkFBcUIsR0FBRztJQUNqQyxJQUFJLHNCQUFzQixFQUFFO0lBQzVCLElBQUksb0JBQW9CLEVBQUU7SUFDMUIsSUFBSSx1QkFBdUIsRUFBRTtJQUM3QixJQUFJLHVCQUF1QixFQUFFO0lBQzdCLElBQUksc0JBQXNCLEVBQUU7SUFDNUIsSUFBSSxzQkFBc0IsRUFBRTtJQUM1QixJQUFJLG1CQUFtQixFQUFFO0lBQ3pCLElBQUksa0JBQWtCLEVBQUU7SUFDeEIsSUFBSSxxQkFBcUIsRUFBRTtJQUMzQixJQUFJLHFCQUFxQixFQUFFO0lBQzNCLElBQUksd0JBQXdCLEVBQUU7SUFDOUIsSUFBSSx5QkFBeUIsRUFBRTtJQUMvQixJQUFJLHlCQUF5QixFQUFFO0NBQ2xDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50LWVzNic7XHJcbmltcG9ydCB7IEZvcm1GaWVsZFR5cGVzIH0gZnJvbSAnLi9mb3JtLWZpZWxkLXR5cGVzJztcclxuaW1wb3J0IHsgRm9ybUZpZWxkTW9kZWwgfSBmcm9tICcuL2Zvcm0tZmllbGQubW9kZWwnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBGb3JtRmllbGRWYWxpZGF0b3Ige1xyXG5cclxuICAgIGlzU3VwcG9ydGVkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW47XHJcblxyXG4gICAgdmFsaWRhdGUoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbjtcclxuXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBSZXF1aXJlZEZpZWxkVmFsaWRhdG9yIGltcGxlbWVudHMgRm9ybUZpZWxkVmFsaWRhdG9yIHtcclxuXHJcbiAgICBwcml2YXRlIHN1cHBvcnRlZFR5cGVzID0gW1xyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLlRFWFQsXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuTVVMVElMSU5FX1RFWFQsXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuTlVNQkVSLFxyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLkJPT0xFQU4sXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuVFlQRUFIRUFELFxyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLkRST1BET1dOLFxyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLlBFT1BMRSxcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5GVU5DVElPTkFMX0dST1VQLFxyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLlJBRElPX0JVVFRPTlMsXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuVVBMT0FELFxyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLkFNT1VOVCxcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5EWU5BTUlDX1RBQkxFLFxyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLkRBVEUsXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuREFURVRJTUUsXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuQVRUQUNIX0ZPTERFUlxyXG4gICAgXTtcclxuXHJcbiAgICBpc1N1cHBvcnRlZChmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gZmllbGQgJiZcclxuICAgICAgICAgICAgdGhpcy5zdXBwb3J0ZWRUeXBlcy5pbmRleE9mKGZpZWxkLnR5cGUpID4gLTEgJiZcclxuICAgICAgICAgICAgZmllbGQucmVxdWlyZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgdmFsaWRhdGUoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNTdXBwb3J0ZWQoZmllbGQpICYmIGZpZWxkLmlzVmlzaWJsZSkge1xyXG5cclxuICAgICAgICAgICAgaWYgKGZpZWxkLnR5cGUgPT09IEZvcm1GaWVsZFR5cGVzLkRST1BET1dOKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoZmllbGQuaGFzRW1wdHlWYWx1ZSAmJiBmaWVsZC5lbXB0eU9wdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChmaWVsZC52YWx1ZSA9PT0gZmllbGQuZW1wdHlPcHRpb24uaWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGZpZWxkLnR5cGUgPT09IEZvcm1GaWVsZFR5cGVzLlJBRElPX0JVVFRPTlMpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG9wdGlvbiA9IGZpZWxkLm9wdGlvbnMuZmluZCgob3B0KSA9PiBvcHQuaWQgPT09IGZpZWxkLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiAhIW9wdGlvbjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGZpZWxkLnR5cGUgPT09IEZvcm1GaWVsZFR5cGVzLlVQTE9BRCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZpZWxkLnZhbHVlICYmIGZpZWxkLnZhbHVlLmxlbmd0aCA+IDA7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChmaWVsZC50eXBlID09PSBGb3JtRmllbGRUeXBlcy5EWU5BTUlDX1RBQkxFKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmllbGQudmFsdWUgJiYgZmllbGQudmFsdWUgaW5zdGFuY2VvZiBBcnJheSAmJiBmaWVsZC52YWx1ZS5sZW5ndGggPiAwO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoZmllbGQudHlwZSA9PT0gRm9ybUZpZWxkVHlwZXMuQk9PTEVBTikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZpZWxkLnZhbHVlID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoZmllbGQudmFsdWUgPT09IG51bGwgfHwgZmllbGQudmFsdWUgPT09IHVuZGVmaW5lZCB8fCBmaWVsZC52YWx1ZSA9PT0gJycpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBOdW1iZXJGaWVsZFZhbGlkYXRvciBpbXBsZW1lbnRzIEZvcm1GaWVsZFZhbGlkYXRvciB7XHJcblxyXG4gICAgcHJpdmF0ZSBzdXBwb3J0ZWRUeXBlcyA9IFtcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5OVU1CRVIsXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuQU1PVU5UXHJcbiAgICBdO1xyXG5cclxuICAgIHN0YXRpYyBpc051bWJlcih2YWx1ZTogYW55KTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHZhbHVlID09PSBudWxsIHx8IHZhbHVlID09PSB1bmRlZmluZWQgfHwgdmFsdWUgPT09ICcnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiAhaXNOYU4oK3ZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICBpc1N1cHBvcnRlZChmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gZmllbGQgJiYgdGhpcy5zdXBwb3J0ZWRUeXBlcy5pbmRleE9mKGZpZWxkLnR5cGUpID4gLTE7XHJcbiAgICB9XHJcblxyXG4gICAgdmFsaWRhdGUoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNTdXBwb3J0ZWQoZmllbGQpICYmIGZpZWxkLmlzVmlzaWJsZSkge1xyXG4gICAgICAgICAgICBpZiAoZmllbGQudmFsdWUgPT09IG51bGwgfHxcclxuICAgICAgICAgICAgICAgIGZpZWxkLnZhbHVlID09PSB1bmRlZmluZWQgfHxcclxuICAgICAgICAgICAgICAgIGZpZWxkLnZhbHVlID09PSAnJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY29uc3QgdmFsdWVTdHIgPSAnJyArIGZpZWxkLnZhbHVlO1xyXG4gICAgICAgICAgICBsZXQgcGF0dGVybiA9IG5ldyBSZWdFeHAoL14tP1xcZCskLyk7XHJcbiAgICAgICAgICAgIGlmIChmaWVsZC5lbmFibGVGcmFjdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgIHBhdHRlcm4gPSBuZXcgUmVnRXhwKC9eLT9bMC05XSsoXFwuWzAtOV17MSwyfSk/JC8pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmICh2YWx1ZVN0ci5tYXRjaChwYXR0ZXJuKSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZmllbGQudmFsaWRhdGlvblN1bW1hcnkubWVzc2FnZSA9ICdGT1JNLkZJRUxELlZBTElEQVRPUi5JTlZBTElEX05VTUJFUic7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBEYXRlRmllbGRWYWxpZGF0b3IgaW1wbGVtZW50cyBGb3JtRmllbGRWYWxpZGF0b3Ige1xyXG5cclxuICAgIHByaXZhdGUgc3VwcG9ydGVkVHlwZXMgPSBbXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuREFURVxyXG4gICAgXTtcclxuXHJcbiAgICAvLyBWYWxpZGF0ZXMgdGhhdCB0aGUgaW5wdXQgc3RyaW5nIGlzIGEgdmFsaWQgZGF0ZSBmb3JtYXR0ZWQgYXMgPGRhdGVGb3JtYXQ+IChkZWZhdWx0IEQtTS1ZWVlZKVxyXG4gICAgc3RhdGljIGlzVmFsaWREYXRlKGlucHV0RGF0ZTogc3RyaW5nLCBkYXRlRm9ybWF0OiBzdHJpbmcgPSAnRC1NLVlZWVknKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKGlucHV0RGF0ZSkge1xyXG4gICAgICAgICAgICBjb25zdCBkID0gbW9tZW50KGlucHV0RGF0ZSwgZGF0ZUZvcm1hdCwgdHJ1ZSk7XHJcbiAgICAgICAgICAgIHJldHVybiBkLmlzVmFsaWQoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBpc1N1cHBvcnRlZChmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gZmllbGQgJiYgdGhpcy5zdXBwb3J0ZWRUeXBlcy5pbmRleE9mKGZpZWxkLnR5cGUpID4gLTE7XHJcbiAgICB9XHJcblxyXG4gICAgdmFsaWRhdGUoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNTdXBwb3J0ZWQoZmllbGQpICYmIGZpZWxkLnZhbHVlICYmIGZpZWxkLmlzVmlzaWJsZSkge1xyXG4gICAgICAgICAgICBpZiAoRGF0ZUZpZWxkVmFsaWRhdG9yLmlzVmFsaWREYXRlKGZpZWxkLnZhbHVlLCBmaWVsZC5kYXRlRGlzcGxheUZvcm1hdCkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5Lm1lc3NhZ2UgPSBmaWVsZC5kYXRlRGlzcGxheUZvcm1hdDtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIE1pbkRhdGVGaWVsZFZhbGlkYXRvciBpbXBsZW1lbnRzIEZvcm1GaWVsZFZhbGlkYXRvciB7XHJcblxyXG4gICAgcHJpdmF0ZSBzdXBwb3J0ZWRUeXBlcyA9IFtcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5EQVRFXHJcbiAgICBdO1xyXG5cclxuICAgIGlzU3VwcG9ydGVkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmaWVsZCAmJlxyXG4gICAgICAgICAgICB0aGlzLnN1cHBvcnRlZFR5cGVzLmluZGV4T2YoZmllbGQudHlwZSkgPiAtMSAmJiAhIWZpZWxkLm1pblZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIHZhbGlkYXRlKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGxldCBpc1ZhbGlkID0gdHJ1ZTtcclxuICAgICAgICBpZiAodGhpcy5pc1N1cHBvcnRlZChmaWVsZCkgJiYgZmllbGQudmFsdWUgJiYgZmllbGQuaXNWaXNpYmxlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGRhdGVGb3JtYXQgPSBmaWVsZC5kYXRlRGlzcGxheUZvcm1hdDtcclxuXHJcbiAgICAgICAgICAgIGlmICghRGF0ZUZpZWxkVmFsaWRhdG9yLmlzVmFsaWREYXRlKGZpZWxkLnZhbHVlLCBkYXRlRm9ybWF0KSkge1xyXG4gICAgICAgICAgICAgICAgZmllbGQudmFsaWRhdGlvblN1bW1hcnkubWVzc2FnZSA9ICdGT1JNLkZJRUxELlZBTElEQVRPUi5JTlZBTElEX0RBVEUnO1xyXG4gICAgICAgICAgICAgICAgaXNWYWxpZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaXNWYWxpZCA9IHRoaXMuY2hlY2tEYXRlKGZpZWxkLCBkYXRlRm9ybWF0KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gaXNWYWxpZDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNoZWNrRGF0ZShmaWVsZDogRm9ybUZpZWxkTW9kZWwsIGRhdGVGb3JtYXQ6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGNvbnN0IE1JTl9EQVRFX0ZPUk1BVCA9ICdERC1NTS1ZWVlZJztcclxuICAgICAgICBsZXQgaXNWYWxpZCA9IHRydWU7XHJcbiAgICAgICAgLy8gcmVtb3ZlIHRpbWUgYW5kIHRpbWV6b25lIGluZm9cclxuICAgICAgICBsZXQgZmllbGRWYWx1ZURhdGE7XHJcbiAgICAgICAgaWYgKHR5cGVvZiBmaWVsZC52YWx1ZSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgZmllbGRWYWx1ZURhdGEgPSBtb21lbnQoZmllbGQudmFsdWUuc3BsaXQoJ1QnKVswXSwgZGF0ZUZvcm1hdCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZmllbGRWYWx1ZURhdGEgPSBmaWVsZC52YWx1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgbWluID0gbW9tZW50KGZpZWxkLm1pblZhbHVlLCBNSU5fREFURV9GT1JNQVQpO1xyXG5cclxuICAgICAgICBpZiAoZmllbGRWYWx1ZURhdGEuaXNCZWZvcmUobWluKSkge1xyXG4gICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5tZXNzYWdlID0gYEZPUk0uRklFTEQuVkFMSURBVE9SLk5PVF9MRVNTX1RIQU5gO1xyXG4gICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5hdHRyaWJ1dGVzLnNldCgnbWluVmFsdWUnLCBtaW4uZm9ybWF0KGZpZWxkLmRhdGVEaXNwbGF5Rm9ybWF0KS50b0xvY2FsZVVwcGVyQ2FzZSgpKTtcclxuICAgICAgICAgICAgaXNWYWxpZCA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gaXNWYWxpZDtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIE1heERhdGVGaWVsZFZhbGlkYXRvciBpbXBsZW1lbnRzIEZvcm1GaWVsZFZhbGlkYXRvciB7XHJcblxyXG4gICAgTUFYX0RBVEVfRk9STUFUID0gJ0RELU1NLVlZWVknO1xyXG5cclxuICAgIHByaXZhdGUgc3VwcG9ydGVkVHlwZXMgPSBbXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuREFURVxyXG4gICAgXTtcclxuXHJcbiAgICBpc1N1cHBvcnRlZChmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gZmllbGQgJiZcclxuICAgICAgICAgICAgdGhpcy5zdXBwb3J0ZWRUeXBlcy5pbmRleE9mKGZpZWxkLnR5cGUpID4gLTEgJiYgISFmaWVsZC5tYXhWYWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZShmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodGhpcy5pc1N1cHBvcnRlZChmaWVsZCkgJiYgZmllbGQudmFsdWUgJiYgZmllbGQuaXNWaXNpYmxlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGRhdGVGb3JtYXQgPSBmaWVsZC5kYXRlRGlzcGxheUZvcm1hdDtcclxuXHJcbiAgICAgICAgICAgIGlmICghRGF0ZUZpZWxkVmFsaWRhdG9yLmlzVmFsaWREYXRlKGZpZWxkLnZhbHVlLCBkYXRlRm9ybWF0KSkge1xyXG4gICAgICAgICAgICAgICAgZmllbGQudmFsaWRhdGlvblN1bW1hcnkubWVzc2FnZSA9ICdGT1JNLkZJRUxELlZBTElEQVRPUi5JTlZBTElEX0RBVEUnO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvLyByZW1vdmUgdGltZSBhbmQgdGltZXpvbmUgaW5mb1xyXG4gICAgICAgICAgICBsZXQgZDtcclxuICAgICAgICAgICAgaWYgKHR5cGVvZiBmaWVsZC52YWx1ZSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgICAgIGQgPSBtb21lbnQoZmllbGQudmFsdWUuc3BsaXQoJ1QnKVswXSwgZGF0ZUZvcm1hdCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBkID0gZmllbGQudmFsdWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY29uc3QgbWF4ID0gbW9tZW50KGZpZWxkLm1heFZhbHVlLCB0aGlzLk1BWF9EQVRFX0ZPUk1BVCk7XHJcblxyXG4gICAgICAgICAgICBpZiAoZC5pc0FmdGVyKG1heCkpIHtcclxuICAgICAgICAgICAgICAgIGZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5Lm1lc3NhZ2UgPSBgRk9STS5GSUVMRC5WQUxJREFUT1IuTk9UX0dSRUFURVJfVEhBTmA7XHJcbiAgICAgICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5hdHRyaWJ1dGVzLnNldCgnbWF4VmFsdWUnLCBtYXguZm9ybWF0KGZpZWxkLmRhdGVEaXNwbGF5Rm9ybWF0KS50b0xvY2FsZVVwcGVyQ2FzZSgpKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIE1pbkRhdGVUaW1lRmllbGRWYWxpZGF0b3IgaW1wbGVtZW50cyBGb3JtRmllbGRWYWxpZGF0b3Ige1xyXG5cclxuICAgIHByaXZhdGUgc3VwcG9ydGVkVHlwZXMgPSBbXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuREFURVRJTUVcclxuICAgIF07XHJcbiAgICBNSU5fREFURVRJTUVfRk9STUFUID0gJ1lZWVktTU0tREQgaGg6bW0gQVonO1xyXG5cclxuICAgIGlzU3VwcG9ydGVkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmaWVsZCAmJlxyXG4gICAgICAgICAgICB0aGlzLnN1cHBvcnRlZFR5cGVzLmluZGV4T2YoZmllbGQudHlwZSkgPiAtMSAmJiAhIWZpZWxkLm1pblZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIHZhbGlkYXRlKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGxldCBpc1ZhbGlkID0gdHJ1ZTtcclxuICAgICAgICBpZiAodGhpcy5pc1N1cHBvcnRlZChmaWVsZCkgJiYgZmllbGQudmFsdWUgJiYgZmllbGQuaXNWaXNpYmxlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGRhdGVGb3JtYXQgPSBmaWVsZC5kYXRlRGlzcGxheUZvcm1hdDtcclxuXHJcbiAgICAgICAgICAgIGlmICghRGF0ZUZpZWxkVmFsaWRhdG9yLmlzVmFsaWREYXRlKGZpZWxkLnZhbHVlLCBkYXRlRm9ybWF0KSkge1xyXG4gICAgICAgICAgICAgICAgZmllbGQudmFsaWRhdGlvblN1bW1hcnkubWVzc2FnZSA9ICdGT1JNLkZJRUxELlZBTElEQVRPUi5JTlZBTElEX0RBVEUnO1xyXG4gICAgICAgICAgICAgICAgaXNWYWxpZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaXNWYWxpZCA9IHRoaXMuY2hlY2tEYXRlVGltZShmaWVsZCwgZGF0ZUZvcm1hdCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGlzVmFsaWQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBjaGVja0RhdGVUaW1lKGZpZWxkOiBGb3JtRmllbGRNb2RlbCwgZGF0ZUZvcm1hdDogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgbGV0IGlzVmFsaWQgPSB0cnVlO1xyXG4gICAgICAgIGxldCBmaWVsZFZhbHVlRGF0ZTtcclxuICAgICAgICBpZiAodHlwZW9mIGZpZWxkLnZhbHVlID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICBmaWVsZFZhbHVlRGF0ZSA9IG1vbWVudChmaWVsZC52YWx1ZSwgZGF0ZUZvcm1hdCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZmllbGRWYWx1ZURhdGUgPSBmaWVsZC52YWx1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgbWluID0gbW9tZW50KGZpZWxkLm1pblZhbHVlLCB0aGlzLk1JTl9EQVRFVElNRV9GT1JNQVQpO1xyXG5cclxuICAgICAgICBpZiAoZmllbGRWYWx1ZURhdGUuaXNCZWZvcmUobWluKSkge1xyXG4gICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5tZXNzYWdlID0gYEZPUk0uRklFTEQuVkFMSURBVE9SLk5PVF9MRVNTX1RIQU5gO1xyXG4gICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5hdHRyaWJ1dGVzLnNldCgnbWluVmFsdWUnLCBtaW4uZm9ybWF0KGZpZWxkLmRhdGVEaXNwbGF5Rm9ybWF0KS5yZXBsYWNlKCc6JywgJy0nKSk7XHJcbiAgICAgICAgICAgIGlzVmFsaWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGlzVmFsaWQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBNYXhEYXRlVGltZUZpZWxkVmFsaWRhdG9yIGltcGxlbWVudHMgRm9ybUZpZWxkVmFsaWRhdG9yIHtcclxuXHJcbiAgICBwcml2YXRlIHN1cHBvcnRlZFR5cGVzID0gW1xyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLkRBVEVUSU1FXHJcbiAgICBdO1xyXG4gICAgTUFYX0RBVEVUSU1FX0ZPUk1BVCA9ICdZWVlZLU1NLUREIGhoOm1tIEFaJztcclxuXHJcbiAgICBpc1N1cHBvcnRlZChmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gZmllbGQgJiZcclxuICAgICAgICAgICAgdGhpcy5zdXBwb3J0ZWRUeXBlcy5pbmRleE9mKGZpZWxkLnR5cGUpID4gLTEgJiYgISFmaWVsZC5tYXhWYWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZShmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICBsZXQgaXNWYWxpZCA9IHRydWU7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNTdXBwb3J0ZWQoZmllbGQpICYmIGZpZWxkLnZhbHVlICYmIGZpZWxkLmlzVmlzaWJsZSkge1xyXG4gICAgICAgICAgICBjb25zdCBkYXRlRm9ybWF0ID0gZmllbGQuZGF0ZURpc3BsYXlGb3JtYXQ7XHJcblxyXG4gICAgICAgICAgICBpZiAoIURhdGVGaWVsZFZhbGlkYXRvci5pc1ZhbGlkRGF0ZShmaWVsZC52YWx1ZSwgZGF0ZUZvcm1hdCkpIHtcclxuICAgICAgICAgICAgICAgIGZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5Lm1lc3NhZ2UgPSAnRk9STS5GSUVMRC5WQUxJREFUT1IuSU5WQUxJRF9EQVRFJztcclxuICAgICAgICAgICAgICAgIGlzVmFsaWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGlzVmFsaWQgPSB0aGlzLmNoZWNrRGF0ZVRpbWUoZmllbGQsIGRhdGVGb3JtYXQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBpc1ZhbGlkO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgY2hlY2tEYXRlVGltZShmaWVsZDogRm9ybUZpZWxkTW9kZWwsIGRhdGVGb3JtYXQ6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGxldCBpc1ZhbGlkID0gdHJ1ZTtcclxuICAgICAgICBsZXQgZmllbGRWYWx1ZURhdGU7XHJcblxyXG4gICAgICAgIGlmICh0eXBlb2YgZmllbGQudmFsdWUgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgIGZpZWxkVmFsdWVEYXRlID0gbW9tZW50KGZpZWxkLnZhbHVlLCBkYXRlRm9ybWF0KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBmaWVsZFZhbHVlRGF0ZSA9IGZpZWxkLnZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBtYXggPSBtb21lbnQoZmllbGQubWF4VmFsdWUsIHRoaXMuTUFYX0RBVEVUSU1FX0ZPUk1BVCk7XHJcblxyXG4gICAgICAgIGlmIChmaWVsZFZhbHVlRGF0ZS5pc0FmdGVyKG1heCkpIHtcclxuICAgICAgICAgICAgZmllbGQudmFsaWRhdGlvblN1bW1hcnkubWVzc2FnZSA9IGBGT1JNLkZJRUxELlZBTElEQVRPUi5OT1RfR1JFQVRFUl9USEFOYDtcclxuICAgICAgICAgICAgZmllbGQudmFsaWRhdGlvblN1bW1hcnkuYXR0cmlidXRlcy5zZXQoJ21heFZhbHVlJywgbWF4LmZvcm1hdChmaWVsZC5kYXRlRGlzcGxheUZvcm1hdCkucmVwbGFjZSgnOicsICctJykpO1xyXG4gICAgICAgICAgICBpc1ZhbGlkID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBpc1ZhbGlkO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgTWluTGVuZ3RoRmllbGRWYWxpZGF0b3IgaW1wbGVtZW50cyBGb3JtRmllbGRWYWxpZGF0b3Ige1xyXG5cclxuICAgIHByaXZhdGUgc3VwcG9ydGVkVHlwZXMgPSBbXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuVEVYVCxcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5NVUxUSUxJTkVfVEVYVFxyXG4gICAgXTtcclxuXHJcbiAgICBpc1N1cHBvcnRlZChmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gZmllbGQgJiZcclxuICAgICAgICAgICAgdGhpcy5zdXBwb3J0ZWRUeXBlcy5pbmRleE9mKGZpZWxkLnR5cGUpID4gLTEgJiZcclxuICAgICAgICAgICAgZmllbGQubWluTGVuZ3RoID4gMDtcclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZShmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodGhpcy5pc1N1cHBvcnRlZChmaWVsZCkgJiYgZmllbGQudmFsdWUgJiYgZmllbGQuaXNWaXNpYmxlKSB7XHJcbiAgICAgICAgICAgIGlmIChmaWVsZC52YWx1ZS5sZW5ndGggPj0gZmllbGQubWluTGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5tZXNzYWdlID0gYEZPUk0uRklFTEQuVkFMSURBVE9SLkFUX0xFQVNUX0xPTkdgO1xyXG4gICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5hdHRyaWJ1dGVzLnNldCgnbWluTGVuZ3RoJywgZmllbGQubWluTGVuZ3RoLnRvTG9jYWxlU3RyaW5nKCkpO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgTWF4TGVuZ3RoRmllbGRWYWxpZGF0b3IgaW1wbGVtZW50cyBGb3JtRmllbGRWYWxpZGF0b3Ige1xyXG5cclxuICAgIHByaXZhdGUgc3VwcG9ydGVkVHlwZXMgPSBbXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuVEVYVCxcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5NVUxUSUxJTkVfVEVYVFxyXG4gICAgXTtcclxuXHJcbiAgICBpc1N1cHBvcnRlZChmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gZmllbGQgJiZcclxuICAgICAgICAgICAgdGhpcy5zdXBwb3J0ZWRUeXBlcy5pbmRleE9mKGZpZWxkLnR5cGUpID4gLTEgJiZcclxuICAgICAgICAgICAgZmllbGQubWF4TGVuZ3RoID4gMDtcclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZShmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodGhpcy5pc1N1cHBvcnRlZChmaWVsZCkgJiYgZmllbGQudmFsdWUgJiYgZmllbGQuaXNWaXNpYmxlKSB7XHJcbiAgICAgICAgICAgIGlmIChmaWVsZC52YWx1ZS5sZW5ndGggPD0gZmllbGQubWF4TGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5tZXNzYWdlID0gYEZPUk0uRklFTEQuVkFMSURBVE9SLk5PX0xPTkdFUl9USEFOYDtcclxuICAgICAgICAgICAgZmllbGQudmFsaWRhdGlvblN1bW1hcnkuYXR0cmlidXRlcy5zZXQoJ21heExlbmd0aCcsIGZpZWxkLm1heExlbmd0aC50b0xvY2FsZVN0cmluZygpKTtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIE1pblZhbHVlRmllbGRWYWxpZGF0b3IgaW1wbGVtZW50cyBGb3JtRmllbGRWYWxpZGF0b3Ige1xyXG5cclxuICAgIHByaXZhdGUgc3VwcG9ydGVkVHlwZXMgPSBbXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuTlVNQkVSLFxyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLkFNT1VOVFxyXG4gICAgXTtcclxuXHJcbiAgICBpc1N1cHBvcnRlZChmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gZmllbGQgJiZcclxuICAgICAgICAgICAgdGhpcy5zdXBwb3J0ZWRUeXBlcy5pbmRleE9mKGZpZWxkLnR5cGUpID4gLTEgJiZcclxuICAgICAgICAgICAgTnVtYmVyRmllbGRWYWxpZGF0b3IuaXNOdW1iZXIoZmllbGQubWluVmFsdWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHZhbGlkYXRlKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLmlzU3VwcG9ydGVkKGZpZWxkKSAmJiBmaWVsZC52YWx1ZSAmJiBmaWVsZC5pc1Zpc2libGUpIHtcclxuICAgICAgICAgICAgY29uc3QgdmFsdWU6IG51bWJlciA9ICtmaWVsZC52YWx1ZTtcclxuICAgICAgICAgICAgY29uc3QgbWluVmFsdWU6IG51bWJlciA9ICtmaWVsZC5taW5WYWx1ZTtcclxuXHJcbiAgICAgICAgICAgIGlmICh2YWx1ZSA+PSBtaW5WYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZmllbGQudmFsaWRhdGlvblN1bW1hcnkubWVzc2FnZSA9IGBGT1JNLkZJRUxELlZBTElEQVRPUi5OT1RfTEVTU19USEFOYDtcclxuICAgICAgICAgICAgZmllbGQudmFsaWRhdGlvblN1bW1hcnkuYXR0cmlidXRlcy5zZXQoJ21pblZhbHVlJywgZmllbGQubWluVmFsdWUudG9Mb2NhbGVTdHJpbmcoKSk7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgTWF4VmFsdWVGaWVsZFZhbGlkYXRvciBpbXBsZW1lbnRzIEZvcm1GaWVsZFZhbGlkYXRvciB7XHJcblxyXG4gICAgcHJpdmF0ZSBzdXBwb3J0ZWRUeXBlcyA9IFtcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5OVU1CRVIsXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuQU1PVU5UXHJcbiAgICBdO1xyXG5cclxuICAgIGlzU3VwcG9ydGVkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmaWVsZCAmJlxyXG4gICAgICAgICAgICB0aGlzLnN1cHBvcnRlZFR5cGVzLmluZGV4T2YoZmllbGQudHlwZSkgPiAtMSAmJlxyXG4gICAgICAgICAgICBOdW1iZXJGaWVsZFZhbGlkYXRvci5pc051bWJlcihmaWVsZC5tYXhWYWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgdmFsaWRhdGUoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNTdXBwb3J0ZWQoZmllbGQpICYmIGZpZWxkLnZhbHVlICYmIGZpZWxkLmlzVmlzaWJsZSkge1xyXG4gICAgICAgICAgICBjb25zdCB2YWx1ZTogbnVtYmVyID0gK2ZpZWxkLnZhbHVlO1xyXG4gICAgICAgICAgICBjb25zdCBtYXhWYWx1ZTogbnVtYmVyID0gK2ZpZWxkLm1heFZhbHVlO1xyXG5cclxuICAgICAgICAgICAgaWYgKHZhbHVlIDw9IG1heFZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5tZXNzYWdlID0gYEZPUk0uRklFTEQuVkFMSURBVE9SLk5PVF9HUkVBVEVSX1RIQU5gO1xyXG4gICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5hdHRyaWJ1dGVzLnNldCgnbWF4VmFsdWUnLCBmaWVsZC5tYXhWYWx1ZS50b0xvY2FsZVN0cmluZygpKTtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBSZWdFeEZpZWxkVmFsaWRhdG9yIGltcGxlbWVudHMgRm9ybUZpZWxkVmFsaWRhdG9yIHtcclxuXHJcbiAgICBwcml2YXRlIHN1cHBvcnRlZFR5cGVzID0gW1xyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLlRFWFQsXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuTVVMVElMSU5FX1RFWFRcclxuICAgIF07XHJcblxyXG4gICAgaXNTdXBwb3J0ZWQoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGZpZWxkICYmXHJcbiAgICAgICAgICAgIHRoaXMuc3VwcG9ydGVkVHlwZXMuaW5kZXhPZihmaWVsZC50eXBlKSA+IC0xICYmICEhZmllbGQucmVnZXhQYXR0ZXJuO1xyXG4gICAgfVxyXG5cclxuICAgIHZhbGlkYXRlKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLmlzU3VwcG9ydGVkKGZpZWxkKSAmJiBmaWVsZC52YWx1ZSAmJiBmaWVsZC5pc1Zpc2libGUpIHtcclxuICAgICAgICAgICAgaWYgKGZpZWxkLnZhbHVlLmxlbmd0aCA+IDAgJiYgZmllbGQudmFsdWUubWF0Y2gobmV3IFJlZ0V4cCgnXicgKyBmaWVsZC5yZWdleFBhdHRlcm4gKyAnJCcpKSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZmllbGQudmFsaWRhdGlvblN1bW1hcnkubWVzc2FnZSA9ICdGT1JNLkZJRUxELlZBTElEQVRPUi5JTlZBTElEX1ZBTFVFJztcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBGaXhlZFZhbHVlRmllbGRWYWxpZGF0b3IgaW1wbGVtZW50cyBGb3JtRmllbGRWYWxpZGF0b3Ige1xyXG5cclxuICAgIHByaXZhdGUgc3VwcG9ydGVkVHlwZXMgPSBbXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuVFlQRUFIRUFEXHJcbiAgICBdO1xyXG5cclxuICAgIGlzU3VwcG9ydGVkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmaWVsZCAmJiB0aGlzLnN1cHBvcnRlZFR5cGVzLmluZGV4T2YoZmllbGQudHlwZSkgPiAtMTtcclxuICAgIH1cclxuXHJcbiAgICBoYXNWYWxpZE5hbWVPclZhbGlkSWQoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaGFzVmFsaWROYW1lKGZpZWxkKSB8fCB0aGlzLmhhc1ZhbGlkSWQoZmllbGQpO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc1ZhbGlkTmFtZShmaWVsZDogRm9ybUZpZWxkTW9kZWwpIHtcclxuICAgICAgICByZXR1cm4gZmllbGQub3B0aW9ucy5maW5kKChpdGVtKSA9PiBpdGVtLm5hbWUgJiYgaXRlbS5uYW1lLnRvTG9jYWxlTG93ZXJDYXNlKCkgPT09IGZpZWxkLnZhbHVlLnRvTG9jYWxlTG93ZXJDYXNlKCkpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc1ZhbGlkSWQoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGZpZWxkLm9wdGlvbnMuZmluZCgoaXRlbSkgPT4gaXRlbS5pZCA9PT0gZmllbGQudmFsdWUpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc1N0cmluZ1ZhbHVlKGZpZWxkOiBGb3JtRmllbGRNb2RlbCkge1xyXG4gICAgICAgIHJldHVybiBmaWVsZC52YWx1ZSAmJiB0eXBlb2YgZmllbGQudmFsdWUgPT09ICdzdHJpbmcnO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc09wdGlvbnMoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKSB7XHJcbiAgICAgICAgcmV0dXJuIGZpZWxkLm9wdGlvbnMgJiYgZmllbGQub3B0aW9ucy5sZW5ndGggPiAwO1xyXG4gICAgfVxyXG5cclxuICAgIHZhbGlkYXRlKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLmlzU3VwcG9ydGVkKGZpZWxkKSAmJiBmaWVsZC5pc1Zpc2libGUpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuaGFzU3RyaW5nVmFsdWUoZmllbGQpICYmIHRoaXMuaGFzT3B0aW9ucyhmaWVsZCkgJiYgIXRoaXMuaGFzVmFsaWROYW1lT3JWYWxpZElkKGZpZWxkKSkge1xyXG4gICAgICAgICAgICAgICAgZmllbGQudmFsaWRhdGlvblN1bW1hcnkubWVzc2FnZSA9ICdGT1JNLkZJRUxELlZBTElEQVRPUi5JTlZBTElEX1ZBTFVFJztcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNvbnN0IEZPUk1fRklFTERfVkFMSURBVE9SUyA9IFtcclxuICAgIG5ldyBSZXF1aXJlZEZpZWxkVmFsaWRhdG9yKCksXHJcbiAgICBuZXcgTnVtYmVyRmllbGRWYWxpZGF0b3IoKSxcclxuICAgIG5ldyBNaW5MZW5ndGhGaWVsZFZhbGlkYXRvcigpLFxyXG4gICAgbmV3IE1heExlbmd0aEZpZWxkVmFsaWRhdG9yKCksXHJcbiAgICBuZXcgTWluVmFsdWVGaWVsZFZhbGlkYXRvcigpLFxyXG4gICAgbmV3IE1heFZhbHVlRmllbGRWYWxpZGF0b3IoKSxcclxuICAgIG5ldyBSZWdFeEZpZWxkVmFsaWRhdG9yKCksXHJcbiAgICBuZXcgRGF0ZUZpZWxkVmFsaWRhdG9yKCksXHJcbiAgICBuZXcgTWluRGF0ZUZpZWxkVmFsaWRhdG9yKCksXHJcbiAgICBuZXcgTWF4RGF0ZUZpZWxkVmFsaWRhdG9yKCksXHJcbiAgICBuZXcgRml4ZWRWYWx1ZUZpZWxkVmFsaWRhdG9yKCksXHJcbiAgICBuZXcgTWluRGF0ZVRpbWVGaWVsZFZhbGlkYXRvcigpLFxyXG4gICAgbmV3IE1heERhdGVUaW1lRmllbGRWYWxpZGF0b3IoKVxyXG5dO1xyXG4iXX0=