/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import moment from 'moment-es6';
import { WidgetVisibilityModel } from '../../../models/widget-visibility.model';
import { ContainerColumnModel } from './container-column.model';
import { ErrorMessageModel } from './error-message.model';
import { FormFieldTypes } from './form-field-types';
import { NumberFieldValidator } from './form-field-validator';
import { FormWidgetModel } from './form-widget.model';
// Maps to FormFieldRepresentation
export class FormFieldModel extends FormWidgetModel {
    /**
     * @param {?} form
     * @param {?=} json
     */
    constructor(form, json) {
        super(form, json);
        this._readOnly = false;
        this._isValid = true;
        this._required = false;
        this.defaultDateFormat = 'D-M-YYYY';
        this.defaultDateTimeFormat = 'D-M-YYYY hh:mm A';
        this.rowspan = 1;
        this.colspan = 1;
        this.placeholder = null;
        this.minLength = 0;
        this.maxLength = 0;
        this.options = [];
        this.params = {};
        this.isVisible = true;
        this.visibilityCondition = null;
        this.enableFractions = false;
        this.currency = null;
        this.dateDisplayFormat = this.defaultDateFormat;
        // container model members
        this.numberOfColumns = 1;
        this.fields = [];
        this.columns = [];
        if (json) {
            this.fieldType = json.fieldType;
            this.id = json.id;
            this.name = json.name;
            this.type = json.type;
            this._required = (/** @type {?} */ (json.required));
            this._readOnly = (/** @type {?} */ (json.readOnly)) || json.type === 'readonly';
            this.overrideId = (/** @type {?} */ (json.overrideId));
            this.tab = json.tab;
            this.restUrl = json.restUrl;
            this.restResponsePath = json.restResponsePath;
            this.restIdProperty = json.restIdProperty;
            this.restLabelProperty = json.restLabelProperty;
            this.colspan = (/** @type {?} */ (json.colspan));
            this.minLength = (/** @type {?} */ (json.minLength)) || 0;
            this.maxLength = (/** @type {?} */ (json.maxLength)) || 0;
            this.minValue = json.minValue;
            this.maxValue = json.maxValue;
            this.regexPattern = json.regexPattern;
            this.options = (/** @type {?} */ (json.options)) || [];
            this.hasEmptyValue = (/** @type {?} */ (json.hasEmptyValue));
            this.className = json.className;
            this.optionType = json.optionType;
            this.params = (/** @type {?} */ (json.params)) || {};
            this.hyperlinkUrl = json.hyperlinkUrl;
            this.displayText = json.displayText;
            this.visibilityCondition = json.visibilityCondition ? new WidgetVisibilityModel(json.visibilityCondition) : undefined;
            this.enableFractions = (/** @type {?} */ (json.enableFractions));
            this.currency = json.currency;
            this.dateDisplayFormat = json.dateDisplayFormat || this.getDefaultDateFormat(json);
            this._value = this.parseValue(json);
            this.validationSummary = new ErrorMessageModel();
            if (json.placeholder && json.placeholder !== '' && json.placeholder !== 'null') {
                this.placeholder = json.placeholder;
            }
            if (FormFieldTypes.isReadOnlyType(json.type)) {
                if (json.params && json.params.field) {
                    if (form.processVariables) {
                        /** @type {?} */
                        const processVariable = this.getProcessVariableValue(json.params.field, form);
                        if (processVariable) {
                            this.value = processVariable;
                        }
                    }
                    else if (json.params.responseVariable && form.json.variables) {
                        /** @type {?} */
                        const formVariable = this.getVariablesValue(json.params.field.name, form);
                        if (formVariable) {
                            this.value = formVariable;
                        }
                    }
                }
            }
            if (FormFieldTypes.isContainerType(json.type)) {
                this.containerFactory(json, form);
            }
        }
        if (this.hasEmptyValue && this.options && this.options.length > 0) {
            this.emptyOption = this.options[0];
        }
        this.updateForm();
    }
    /**
     * @return {?}
     */
    get value() {
        return this._value;
    }
    /**
     * @param {?} v
     * @return {?}
     */
    set value(v) {
        this._value = v;
        this.updateForm();
    }
    /**
     * @return {?}
     */
    get readOnly() {
        if (this.form && this.form.readOnly) {
            return true;
        }
        return this._readOnly;
    }
    /**
     * @param {?} readOnly
     * @return {?}
     */
    set readOnly(readOnly) {
        this._readOnly = readOnly;
        this.updateForm();
    }
    /**
     * @return {?}
     */
    get required() {
        return this._required;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set required(value) {
        this._required = value;
        this.updateForm();
    }
    /**
     * @return {?}
     */
    get isValid() {
        return this._isValid;
    }
    /**
     * @return {?}
     */
    markAsInvalid() {
        this._isValid = false;
    }
    /**
     * @return {?}
     */
    validate() {
        this.validationSummary = new ErrorMessageModel();
        if (!this.readOnly) {
            /** @type {?} */
            const validators = this.form.fieldValidators || [];
            for (const validator of validators) {
                if (!validator.validate(this)) {
                    this._isValid = false;
                    return this._isValid;
                }
            }
        }
        this._isValid = true;
        return this._isValid;
    }
    /**
     * @private
     * @param {?} jsonField
     * @return {?}
     */
    getDefaultDateFormat(jsonField) {
        /** @type {?} */
        let originalType = jsonField.type;
        if (FormFieldTypes.isReadOnlyType(jsonField.type) &&
            jsonField.params &&
            jsonField.params.field) {
            originalType = jsonField.params.field.type;
        }
        return originalType === FormFieldTypes.DATETIME ? this.defaultDateTimeFormat : this.defaultDateFormat;
    }
    /**
     * @private
     * @param {?} type
     * @return {?}
     */
    isTypeaheadFieldType(type) {
        return type === 'typeahead' ? true : false;
    }
    /**
     * @private
     * @param {?} name
     * @return {?}
     */
    getFieldNameWithLabel(name) {
        return name += '_LABEL';
    }
    /**
     * @private
     * @param {?} field
     * @param {?} form
     * @return {?}
     */
    getProcessVariableValue(field, form) {
        /** @type {?} */
        let fieldName = field.name;
        if (this.isTypeaheadFieldType(field.type)) {
            fieldName = this.getFieldNameWithLabel(field.id);
        }
        return this.findProcessVariableValue(fieldName, form);
    }
    /**
     * @private
     * @param {?} variableName
     * @param {?} form
     * @return {?}
     */
    getVariablesValue(variableName, form) {
        /** @type {?} */
        const variable = form.json.variables.find((/**
         * @param {?} currentVariable
         * @return {?}
         */
        (currentVariable) => {
            return currentVariable.name === variableName;
        }));
        if (variable) {
            if (variable.type === 'boolean') {
                return JSON.parse(variable.value);
            }
            return variable.value;
        }
        return null;
    }
    /**
     * @private
     * @param {?} variableName
     * @param {?} form
     * @return {?}
     */
    findProcessVariableValue(variableName, form) {
        if (form.processVariables) {
            /** @type {?} */
            const variable = form.processVariables.find((/**
             * @param {?} currentVariable
             * @return {?}
             */
            (currentVariable) => {
                return currentVariable.name === variableName;
            }));
            if (variable) {
                return variable.type === 'boolean' ? JSON.parse(variable.value) : variable.value;
            }
        }
        return undefined;
    }
    /**
     * @private
     * @param {?} json
     * @param {?} form
     * @return {?}
     */
    containerFactory(json, form) {
        this.numberOfColumns = (/** @type {?} */ (json.numberOfColumns)) || 1;
        this.fields = json.fields;
        this.rowspan = 1;
        this.colspan = 1;
        if (json.fields) {
            for (const currentField in json.fields) {
                if (json.fields.hasOwnProperty(currentField)) {
                    /** @type {?} */
                    const col = new ContainerColumnModel();
                    /** @type {?} */
                    const fields = (json.fields[currentField] || []).map((/**
                     * @param {?} f
                     * @return {?}
                     */
                    (f) => new FormFieldModel(form, f)));
                    col.fields = fields;
                    col.rowspan = json.fields[currentField].length;
                    col.fields.forEach((/**
                     * @param {?} colFields
                     * @return {?}
                     */
                    (colFields) => {
                        this.colspan = colFields.colspan > this.colspan ? colFields.colspan : this.colspan;
                    }));
                    this.rowspan = this.rowspan < col.rowspan ? col.rowspan : this.rowspan;
                    this.columns.push(col);
                }
            }
        }
    }
    /**
     * @param {?} json
     * @return {?}
     */
    parseValue(json) {
        /** @type {?} */
        let value = json.hasOwnProperty('value') ? json.value : null;
        /*
         This is needed due to Activiti issue related to reading dropdown values as value string
         but saving back as object: { id: <id>, name: <name> }
         */
        if (json.type === FormFieldTypes.DROPDOWN) {
            if (json.hasEmptyValue && json.options) {
                /** @type {?} */
                const options = (/** @type {?} */ (json.options)) || [];
                if (options.length > 0) {
                    /** @type {?} */
                    const emptyOption = json.options[0];
                    if (value === '' || value === emptyOption.id || value === emptyOption.name) {
                        value = emptyOption.id;
                    }
                    else if (value.id && value.name) {
                        value = value.id;
                    }
                }
            }
        }
        /*
         This is needed due to Activiti issue related to reading radio button values as value string
         but saving back as object: { id: <id>, name: <name> }
         */
        if (json.type === FormFieldTypes.RADIO_BUTTONS) {
            // Activiti has a bug with default radio button value where initial selection passed as `name` value
            // so try resolving current one with a fallback to first entry via name or id
            // TODO: needs to be reported and fixed at Activiti side
            /** @type {?} */
            const entry = this.options.filter((/**
             * @param {?} opt
             * @return {?}
             */
            (opt) => opt.id === value || opt.name === value || (value && (opt.id === value.id || opt.name === value.name))));
            if (entry.length > 0) {
                value = entry[0].id;
            }
        }
        /*
         This is needed due to Activiti displaying/editing dates in d-M-YYYY format
         but storing on server in ISO8601 format (i.e. 2013-02-04T22:44:30.652Z)
         */
        if (this.isDateField(json) || this.isDateTimeField(json)) {
            if (value) {
                /** @type {?} */
                let dateValue;
                if (NumberFieldValidator.isNumber(value)) {
                    dateValue = moment(value);
                }
                else {
                    dateValue = this.isDateTimeField(json) ? moment(value, 'YYYY-MM-DD hh:mm A') : moment(value.split('T')[0], 'YYYY-M-D');
                }
                if (dateValue && dateValue.isValid()) {
                    value = dateValue.format(this.dateDisplayFormat);
                }
            }
        }
        return value;
    }
    /**
     * @return {?}
     */
    updateForm() {
        if (!this.form) {
            return;
        }
        switch (this.type) {
            case FormFieldTypes.DROPDOWN:
                /*
                 This is needed due to Activiti reading dropdown values as string
                 but saving back as object: { id: <id>, name: <name> }
                 */
                if (this.value === 'empty' || this.value === '') {
                    this.form.values[this.id] = {};
                }
                else {
                    /** @type {?} */
                    const entry = this.options.filter((/**
                     * @param {?} opt
                     * @return {?}
                     */
                    (opt) => opt.id === this.value));
                    if (entry.length > 0) {
                        this.form.values[this.id] = entry[0];
                    }
                }
                break;
            case FormFieldTypes.RADIO_BUTTONS:
                /*
                                 This is needed due to Activiti issue related to reading radio button values as value string
                                 but saving back as object: { id: <id>, name: <name> }
                                 */
                /** @type {?} */
                const rbEntry = this.options.filter((/**
                 * @param {?} opt
                 * @return {?}
                 */
                (opt) => opt.id === this.value));
                if (rbEntry.length > 0) {
                    this.form.values[this.id] = rbEntry[0];
                }
                break;
            case FormFieldTypes.UPLOAD:
                this.form.hasUpload = true;
                if (this.value && this.value.length > 0) {
                    this.form.values[this.id] = this.value.map((/**
                     * @param {?} elem
                     * @return {?}
                     */
                    (elem) => elem.id)).join(',');
                }
                else {
                    this.form.values[this.id] = null;
                }
                break;
            case FormFieldTypes.TYPEAHEAD:
                /** @type {?} */
                const taEntry = this.options.filter((/**
                 * @param {?} opt
                 * @return {?}
                 */
                (opt) => opt.id === this.value || opt.name === this.value));
                if (taEntry.length > 0) {
                    this.form.values[this.id] = taEntry[0];
                }
                else if (this.options.length > 0) {
                    this.form.values[this.id] = null;
                }
                break;
            case FormFieldTypes.DATE:
                /** @type {?} */
                const dateValue = moment(this.value, this.dateDisplayFormat, true);
                if (dateValue && dateValue.isValid()) {
                    this.form.values[this.id] = `${dateValue.format('YYYY-MM-DD')}T00:00:00.000Z`;
                }
                else {
                    this.form.values[this.id] = null;
                    this._value = this.value;
                }
                break;
            case FormFieldTypes.DATETIME:
                /** @type {?} */
                const dateTimeValue = moment(this.value, this.dateDisplayFormat, true);
                if (dateTimeValue && dateTimeValue.isValid()) {
                    /* cspell:disable-next-line */
                    this.form.values[this.id] = dateTimeValue.format('YYYY-MM-DDTHH:mm:ssZ');
                }
                else {
                    this.form.values[this.id] = null;
                    this._value = this.value;
                }
                break;
            case FormFieldTypes.NUMBER:
                this.form.values[this.id] = parseInt(this.value, 10);
                break;
            case FormFieldTypes.AMOUNT:
                this.form.values[this.id] = this.enableFractions ? parseFloat(this.value) : parseInt(this.value, 10);
                break;
            default:
                if (!FormFieldTypes.isReadOnlyType(this.type) && !this.isInvalidFieldType(this.type)) {
                    this.form.values[this.id] = this.value;
                }
        }
        this.form.onFormFieldChanged(this);
    }
    /**
     * Skip the invalid field type
     * @param {?} type
     * @return {?}
     */
    isInvalidFieldType(type) {
        if (type === 'container') {
            return true;
        }
        else {
            return false;
        }
    }
    /**
     * @return {?}
     */
    getOptionName() {
        /** @type {?} */
        const option = this.options.find((/**
         * @param {?} opt
         * @return {?}
         */
        (opt) => opt.id === this.value));
        return option ? option.name : null;
    }
    /**
     * @return {?}
     */
    hasOptions() {
        return this.options && this.options.length > 0;
    }
    /**
     * @private
     * @param {?} json
     * @return {?}
     */
    isDateField(json) {
        return (json.params &&
            json.params.field &&
            json.params.field.type === FormFieldTypes.DATE) ||
            json.type === FormFieldTypes.DATE;
    }
    /**
     * @private
     * @param {?} json
     * @return {?}
     */
    isDateTimeField(json) {
        return (json.params &&
            json.params.field &&
            json.params.field.type === FormFieldTypes.DATETIME) ||
            json.type === FormFieldTypes.DATETIME;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    FormFieldModel.prototype._value;
    /**
     * @type {?}
     * @private
     */
    FormFieldModel.prototype._readOnly;
    /**
     * @type {?}
     * @private
     */
    FormFieldModel.prototype._isValid;
    /**
     * @type {?}
     * @private
     */
    FormFieldModel.prototype._required;
    /** @type {?} */
    FormFieldModel.prototype.defaultDateFormat;
    /** @type {?} */
    FormFieldModel.prototype.defaultDateTimeFormat;
    /** @type {?} */
    FormFieldModel.prototype.fieldType;
    /** @type {?} */
    FormFieldModel.prototype.id;
    /** @type {?} */
    FormFieldModel.prototype.name;
    /** @type {?} */
    FormFieldModel.prototype.type;
    /** @type {?} */
    FormFieldModel.prototype.overrideId;
    /** @type {?} */
    FormFieldModel.prototype.tab;
    /** @type {?} */
    FormFieldModel.prototype.rowspan;
    /** @type {?} */
    FormFieldModel.prototype.colspan;
    /** @type {?} */
    FormFieldModel.prototype.placeholder;
    /** @type {?} */
    FormFieldModel.prototype.minLength;
    /** @type {?} */
    FormFieldModel.prototype.maxLength;
    /** @type {?} */
    FormFieldModel.prototype.minValue;
    /** @type {?} */
    FormFieldModel.prototype.maxValue;
    /** @type {?} */
    FormFieldModel.prototype.regexPattern;
    /** @type {?} */
    FormFieldModel.prototype.options;
    /** @type {?} */
    FormFieldModel.prototype.restUrl;
    /** @type {?} */
    FormFieldModel.prototype.restResponsePath;
    /** @type {?} */
    FormFieldModel.prototype.restIdProperty;
    /** @type {?} */
    FormFieldModel.prototype.restLabelProperty;
    /** @type {?} */
    FormFieldModel.prototype.hasEmptyValue;
    /** @type {?} */
    FormFieldModel.prototype.className;
    /** @type {?} */
    FormFieldModel.prototype.optionType;
    /** @type {?} */
    FormFieldModel.prototype.params;
    /** @type {?} */
    FormFieldModel.prototype.hyperlinkUrl;
    /** @type {?} */
    FormFieldModel.prototype.displayText;
    /** @type {?} */
    FormFieldModel.prototype.isVisible;
    /** @type {?} */
    FormFieldModel.prototype.visibilityCondition;
    /** @type {?} */
    FormFieldModel.prototype.enableFractions;
    /** @type {?} */
    FormFieldModel.prototype.currency;
    /** @type {?} */
    FormFieldModel.prototype.dateDisplayFormat;
    /** @type {?} */
    FormFieldModel.prototype.numberOfColumns;
    /** @type {?} */
    FormFieldModel.prototype.fields;
    /** @type {?} */
    FormFieldModel.prototype.columns;
    /** @type {?} */
    FormFieldModel.prototype.emptyOption;
    /** @type {?} */
    FormFieldModel.prototype.validationSummary;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2NvcmUvZm9ybS1maWVsZC5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkEsT0FBTyxNQUFNLE1BQU0sWUFBWSxDQUFDO0FBQ2hDLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ2hFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBRzFELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNwRCxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM5RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7O0FBSXRELE1BQU0sT0FBTyxjQUFlLFNBQVEsZUFBZTs7Ozs7SUEwRy9DLFlBQVksSUFBZSxFQUFFLElBQVU7UUFDbkMsS0FBSyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztRQXhHZCxjQUFTLEdBQVksS0FBSyxDQUFDO1FBQzNCLGFBQVEsR0FBWSxJQUFJLENBQUM7UUFDekIsY0FBUyxHQUFZLEtBQUssQ0FBQztRQUUxQixzQkFBaUIsR0FBVyxVQUFVLENBQUM7UUFDdkMsMEJBQXFCLEdBQVcsa0JBQWtCLENBQUM7UUFTNUQsWUFBTyxHQUFXLENBQUMsQ0FBQztRQUNwQixZQUFPLEdBQVcsQ0FBQyxDQUFDO1FBQ3BCLGdCQUFXLEdBQVcsSUFBSSxDQUFDO1FBQzNCLGNBQVMsR0FBVyxDQUFDLENBQUM7UUFDdEIsY0FBUyxHQUFXLENBQUMsQ0FBQztRQUl0QixZQUFPLEdBQXNCLEVBQUUsQ0FBQztRQVFoQyxXQUFNLEdBQXNCLEVBQUUsQ0FBQztRQUcvQixjQUFTLEdBQVksSUFBSSxDQUFDO1FBQzFCLHdCQUFtQixHQUEwQixJQUFJLENBQUM7UUFDbEQsb0JBQWUsR0FBWSxLQUFLLENBQUM7UUFDakMsYUFBUSxHQUFXLElBQUksQ0FBQztRQUN4QixzQkFBaUIsR0FBVyxJQUFJLENBQUMsaUJBQWlCLENBQUM7O1FBR25ELG9CQUFlLEdBQVcsQ0FBQyxDQUFDO1FBQzVCLFdBQU0sR0FBcUIsRUFBRSxDQUFDO1FBQzlCLFlBQU8sR0FBMkIsRUFBRSxDQUFDO1FBK0RqQyxJQUFJLElBQUksRUFBRTtZQUNOLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUNoQyxJQUFJLENBQUMsRUFBRSxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUM7WUFDbEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLG1CQUFVLElBQUksQ0FBQyxRQUFRLEVBQUEsQ0FBQztZQUN6QyxJQUFJLENBQUMsU0FBUyxHQUFHLG1CQUFVLElBQUksQ0FBQyxRQUFRLEVBQUEsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQztZQUNyRSxJQUFJLENBQUMsVUFBVSxHQUFHLG1CQUFVLElBQUksQ0FBQyxVQUFVLEVBQUEsQ0FBQztZQUM1QyxJQUFJLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUM7WUFDcEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1lBQzVCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7WUFDOUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDO1lBQzFDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7WUFDaEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxtQkFBUyxJQUFJLENBQUMsT0FBTyxFQUFBLENBQUM7WUFDckMsSUFBSSxDQUFDLFNBQVMsR0FBRyxtQkFBUyxJQUFJLENBQUMsU0FBUyxFQUFBLElBQUksQ0FBQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxTQUFTLEdBQUcsbUJBQVMsSUFBSSxDQUFDLFNBQVMsRUFBQSxJQUFJLENBQUMsQ0FBQztZQUM5QyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDOUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQzlCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUN0QyxJQUFJLENBQUMsT0FBTyxHQUFHLG1CQUFvQixJQUFJLENBQUMsT0FBTyxFQUFBLElBQUksRUFBRSxDQUFDO1lBQ3RELElBQUksQ0FBQyxhQUFhLEdBQUcsbUJBQVUsSUFBSSxDQUFDLGFBQWEsRUFBQSxDQUFDO1lBQ2xELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztZQUNoQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUM7WUFDbEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxtQkFBb0IsSUFBSSxDQUFDLE1BQU0sRUFBQSxJQUFJLEVBQUUsQ0FBQztZQUNwRCxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7WUFDdEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLElBQUkscUJBQXFCLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQztZQUN0SCxJQUFJLENBQUMsZUFBZSxHQUFHLG1CQUFVLElBQUksQ0FBQyxlQUFlLEVBQUEsQ0FBQztZQUN0RCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDOUIsSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbkYsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLGlCQUFpQixFQUFFLENBQUM7WUFFakQsSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssRUFBRSxJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssTUFBTSxFQUFFO2dCQUM1RSxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7YUFDdkM7WUFFRCxJQUFJLGNBQWMsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUMxQyxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUU7b0JBQ2xDLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFOzs4QkFDakIsZUFBZSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUM7d0JBQzdFLElBQUksZUFBZSxFQUFFOzRCQUNqQixJQUFJLENBQUMsS0FBSyxHQUFHLGVBQWUsQ0FBQzt5QkFDaEM7cUJBQ0o7eUJBQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFOzs4QkFDdEQsWUFBWSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDO3dCQUN6RSxJQUFJLFlBQVksRUFBRTs0QkFDZCxJQUFJLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQzt5QkFDN0I7cUJBQ0o7aUJBQ0o7YUFDSjtZQUVELElBQUksY0FBYyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzNDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDckM7U0FDSjtRQUVELElBQUksSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUMvRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDdEM7UUFFRCxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7OztJQXhIRCxJQUFJLEtBQUs7UUFDTCxPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7SUFDdkIsQ0FBQzs7Ozs7SUFFRCxJQUFJLEtBQUssQ0FBQyxDQUFNO1FBQ1osSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDaEIsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQ3RCLENBQUM7Ozs7SUFFRCxJQUFJLFFBQVE7UUFDUixJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakMsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUNELE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUMxQixDQUFDOzs7OztJQUVELElBQUksUUFBUSxDQUFDLFFBQWlCO1FBQzFCLElBQUksQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDO1FBQzFCLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUN0QixDQUFDOzs7O0lBRUQsSUFBSSxRQUFRO1FBQ1IsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQzFCLENBQUM7Ozs7O0lBRUQsSUFBSSxRQUFRLENBQUMsS0FBYztRQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztRQUN2QixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7OztJQUVELElBQUksT0FBTztRQUNQLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDOzs7O0lBRUQsYUFBYTtRQUNULElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO0lBQzFCLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksaUJBQWlCLEVBQUUsQ0FBQztRQUVqRCxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTs7a0JBQ1YsVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxJQUFJLEVBQUU7WUFDbEQsS0FBSyxNQUFNLFNBQVMsSUFBSSxVQUFVLEVBQUU7Z0JBQ2hDLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUMzQixJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztvQkFDdEIsT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO2lCQUN4QjthQUNKO1NBQ0o7UUFFRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztRQUNyQixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQzs7Ozs7O0lBcUVPLG9CQUFvQixDQUFDLFNBQWM7O1lBQ25DLFlBQVksR0FBRyxTQUFTLENBQUMsSUFBSTtRQUNqQyxJQUFJLGNBQWMsQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztZQUM3QyxTQUFTLENBQUMsTUFBTTtZQUNoQixTQUFTLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRTtZQUN4QixZQUFZLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO1NBQzlDO1FBQ0QsT0FBTyxZQUFZLEtBQUssY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUM7SUFDMUcsQ0FBQzs7Ozs7O0lBRU8sb0JBQW9CLENBQUMsSUFBWTtRQUNyQyxPQUFPLElBQUksS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQy9DLENBQUM7Ozs7OztJQUVPLHFCQUFxQixDQUFDLElBQVk7UUFDdEMsT0FBTyxJQUFJLElBQUksUUFBUSxDQUFDO0lBQzVCLENBQUM7Ozs7Ozs7SUFFTyx1QkFBdUIsQ0FBQyxLQUFVLEVBQUUsSUFBZTs7WUFDbkQsU0FBUyxHQUFHLEtBQUssQ0FBQyxJQUFJO1FBQzFCLElBQUksSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUN2QyxTQUFTLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUNwRDtRQUNELE9BQU8sSUFBSSxDQUFDLHdCQUF3QixDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUMxRCxDQUFDOzs7Ozs7O0lBRU8saUJBQWlCLENBQUMsWUFBb0IsRUFBRSxJQUFlOztjQUNyRCxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSTs7OztRQUFDLENBQUMsZUFBZSxFQUFFLEVBQUU7WUFDMUQsT0FBTyxlQUFlLENBQUMsSUFBSSxLQUFLLFlBQVksQ0FBQztRQUNqRCxDQUFDLEVBQUM7UUFFRixJQUFJLFFBQVEsRUFBRTtZQUNWLElBQUksUUFBUSxDQUFDLElBQUksS0FBSyxTQUFTLEVBQUU7Z0JBQzdCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDckM7WUFFRCxPQUFPLFFBQVEsQ0FBQyxLQUFLLENBQUM7U0FDekI7UUFFRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOzs7Ozs7O0lBRU8sd0JBQXdCLENBQUMsWUFBb0IsRUFBRSxJQUFlO1FBQ2xFLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFOztrQkFDakIsUUFBUSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJOzs7O1lBQUMsQ0FBQyxlQUFlLEVBQUUsRUFBRTtnQkFDNUQsT0FBTyxlQUFlLENBQUMsSUFBSSxLQUFLLFlBQVksQ0FBQztZQUNqRCxDQUFDLEVBQUM7WUFFRixJQUFJLFFBQVEsRUFBRTtnQkFDVixPQUFPLFFBQVEsQ0FBQyxJQUFJLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQzthQUNwRjtTQUNKO1FBRUQsT0FBTyxTQUFTLENBQUM7SUFDckIsQ0FBQzs7Ozs7OztJQUVPLGdCQUFnQixDQUFDLElBQVMsRUFBRSxJQUFlO1FBQy9DLElBQUksQ0FBQyxlQUFlLEdBQUcsbUJBQVMsSUFBSSxDQUFDLGVBQWUsRUFBQSxJQUFJLENBQUMsQ0FBQztRQUUxRCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7UUFFMUIsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDakIsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFFakIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2IsS0FBSyxNQUFNLFlBQVksSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUNwQyxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxFQUFFOzswQkFDcEMsR0FBRyxHQUFHLElBQUksb0JBQW9CLEVBQUU7OzBCQUVoQyxNQUFNLEdBQXFCLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQyxHQUFHOzs7O29CQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxJQUFJLGNBQWMsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLEVBQUM7b0JBQzFHLEdBQUcsQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDO29CQUNwQixHQUFHLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsTUFBTSxDQUFDO29CQUUvQyxHQUFHLENBQUMsTUFBTSxDQUFDLE9BQU87Ozs7b0JBQUMsQ0FBQyxTQUFjLEVBQUUsRUFBRTt3QkFDbEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxTQUFTLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7b0JBQ3ZGLENBQUMsRUFBQyxDQUFDO29CQUVILElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO29CQUN2RSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztpQkFDMUI7YUFDSjtTQUNKO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsSUFBUzs7WUFDWixLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSTtRQUU1RDs7O1dBR0c7UUFDSCxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssY0FBYyxDQUFDLFFBQVEsRUFBRTtZQUN2QyxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTs7c0JBQzlCLE9BQU8sR0FBRyxtQkFBb0IsSUFBSSxDQUFDLE9BQU8sRUFBQSxJQUFJLEVBQUU7Z0JBQ3RELElBQUksT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7OzBCQUNkLFdBQVcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztvQkFDbkMsSUFBSSxLQUFLLEtBQUssRUFBRSxJQUFJLEtBQUssS0FBSyxXQUFXLENBQUMsRUFBRSxJQUFJLEtBQUssS0FBSyxXQUFXLENBQUMsSUFBSSxFQUFFO3dCQUN4RSxLQUFLLEdBQUcsV0FBVyxDQUFDLEVBQUUsQ0FBQztxQkFDMUI7eUJBQU0sSUFBSSxLQUFLLENBQUMsRUFBRSxJQUFJLEtBQUssQ0FBQyxJQUFJLEVBQUU7d0JBQy9CLEtBQUssR0FBRyxLQUFLLENBQUMsRUFBRSxDQUFDO3FCQUNwQjtpQkFDSjthQUNKO1NBQ0o7UUFFRDs7O1dBR0c7UUFDSCxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssY0FBYyxDQUFDLGFBQWEsRUFBRTs7Ozs7a0JBSXRDLEtBQUssR0FBc0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNOzs7O1lBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUN6RCxHQUFHLENBQUMsRUFBRSxLQUFLLEtBQUssSUFBSSxHQUFHLENBQUMsSUFBSSxLQUFLLEtBQUssSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUssS0FBSyxDQUFDLEVBQUUsSUFBSSxHQUFHLENBQUMsSUFBSSxLQUFLLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxFQUFDO1lBQzFHLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ2xCLEtBQUssR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2FBQ3ZCO1NBQ0o7UUFFRDs7O1dBR0c7UUFDSCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUN0RCxJQUFJLEtBQUssRUFBRTs7b0JBQ0gsU0FBUztnQkFDYixJQUFJLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFDdEMsU0FBUyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztpQkFDN0I7cUJBQU07b0JBQ0gsU0FBUyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsb0JBQW9CLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsVUFBVSxDQUFDLENBQUM7aUJBQzFIO2dCQUNELElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtvQkFDbEMsS0FBSyxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7aUJBQ3BEO2FBQ0o7U0FDSjtRQUVELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7SUFFRCxVQUFVO1FBQ04sSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDWixPQUFPO1NBQ1Y7UUFFRCxRQUFRLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDZixLQUFLLGNBQWMsQ0FBQyxRQUFRO2dCQUN4Qjs7O21CQUdHO2dCQUNILElBQUksSUFBSSxDQUFDLEtBQUssS0FBSyxPQUFPLElBQUksSUFBSSxDQUFDLEtBQUssS0FBSyxFQUFFLEVBQUU7b0JBQzdDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUM7aUJBQ2xDO3FCQUFNOzswQkFDRyxLQUFLLEdBQXNCLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTTs7OztvQkFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsS0FBSyxFQUFDO29CQUNwRixJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUN4QztpQkFDSjtnQkFDRCxNQUFNO1lBQ1YsS0FBSyxjQUFjLENBQUMsYUFBYTs7Ozs7O3NCQUt2QixPQUFPLEdBQXNCLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTTs7OztnQkFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxJQUFJLENBQUMsS0FBSyxFQUFDO2dCQUN0RixJQUFJLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUMxQztnQkFDRCxNQUFNO1lBQ1YsS0FBSyxjQUFjLENBQUMsTUFBTTtnQkFDdEIsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO2dCQUMzQixJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUNyQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHOzs7O29CQUFDLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUMzRTtxQkFBTTtvQkFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDO2lCQUNwQztnQkFDRCxNQUFNO1lBQ1YsS0FBSyxjQUFjLENBQUMsU0FBUzs7c0JBQ25CLE9BQU8sR0FBc0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNOzs7O2dCQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxLQUFLLElBQUksR0FBRyxDQUFDLElBQUksS0FBSyxJQUFJLENBQUMsS0FBSyxFQUFDO2dCQUNqSCxJQUFJLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUNwQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO2lCQUMxQztxQkFBTSxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDaEMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQztpQkFDcEM7Z0JBQ0QsTUFBTTtZQUNWLEtBQUssY0FBYyxDQUFDLElBQUk7O3NCQUNkLFNBQVMsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxDQUFDO2dCQUNsRSxJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsT0FBTyxFQUFFLEVBQUU7b0JBQ2xDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLGdCQUFnQixDQUFDO2lCQUNqRjtxQkFBTTtvQkFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDO29CQUNqQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7aUJBQzVCO2dCQUNELE1BQU07WUFDVixLQUFLLGNBQWMsQ0FBQyxRQUFROztzQkFDbEIsYUFBYSxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUM7Z0JBQ3RFLElBQUksYUFBYSxJQUFJLGFBQWEsQ0FBQyxPQUFPLEVBQUUsRUFBRTtvQkFDMUMsOEJBQThCO29CQUM5QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsYUFBYSxDQUFDLE1BQU0sQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO2lCQUM1RTtxQkFBTTtvQkFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDO29CQUNqQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7aUJBQzVCO2dCQUNELE1BQU07WUFDVixLQUFLLGNBQWMsQ0FBQyxNQUFNO2dCQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ3JELE1BQU07WUFDVixLQUFLLGNBQWMsQ0FBQyxNQUFNO2dCQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLENBQUM7Z0JBQ3JHLE1BQU07WUFDVjtnQkFDSSxJQUFJLENBQUMsY0FBYyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUNsRixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztpQkFDMUM7U0FDUjtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdkMsQ0FBQzs7Ozs7O0lBTUQsa0JBQWtCLENBQUMsSUFBWTtRQUMzQixJQUFJLElBQUksS0FBSyxXQUFXLEVBQUU7WUFDdEIsT0FBTyxJQUFJLENBQUM7U0FDZjthQUFNO1lBQ0gsT0FBTyxLQUFLLENBQUM7U0FDaEI7SUFDTCxDQUFDOzs7O0lBRUQsYUFBYTs7Y0FDSCxNQUFNLEdBQW9CLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxLQUFLLEVBQUM7UUFDakYsT0FBTyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUN2QyxDQUFDOzs7O0lBRUQsVUFBVTtRQUNOLE9BQU8sSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDbkQsQ0FBQzs7Ozs7O0lBRU8sV0FBVyxDQUFDLElBQVM7UUFDekIsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNO1lBQ2YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLO1lBQ2pCLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxjQUFjLENBQUMsSUFBSSxDQUFDO1lBQy9DLElBQUksQ0FBQyxJQUFJLEtBQUssY0FBYyxDQUFDLElBQUksQ0FBQztJQUMxQyxDQUFDOzs7Ozs7SUFFTyxlQUFlLENBQUMsSUFBUztRQUM3QixPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU07WUFDZixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUs7WUFDakIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxRQUFRLENBQUM7WUFDbkQsSUFBSSxDQUFDLElBQUksS0FBSyxjQUFjLENBQUMsUUFBUSxDQUFDO0lBQzlDLENBQUM7Q0FFSjs7Ozs7O0lBM2FHLGdDQUF1Qjs7Ozs7SUFDdkIsbUNBQW1DOzs7OztJQUNuQyxrQ0FBaUM7Ozs7O0lBQ2pDLG1DQUFtQzs7SUFFbkMsMkNBQWdEOztJQUNoRCwrQ0FBNEQ7O0lBRzVELG1DQUFrQjs7SUFDbEIsNEJBQVc7O0lBQ1gsOEJBQWE7O0lBQ2IsOEJBQWE7O0lBQ2Isb0NBQW9COztJQUNwQiw2QkFBWTs7SUFDWixpQ0FBb0I7O0lBQ3BCLGlDQUFvQjs7SUFDcEIscUNBQTJCOztJQUMzQixtQ0FBc0I7O0lBQ3RCLG1DQUFzQjs7SUFDdEIsa0NBQWlCOztJQUNqQixrQ0FBaUI7O0lBQ2pCLHNDQUFxQjs7SUFDckIsaUNBQWdDOztJQUNoQyxpQ0FBZ0I7O0lBQ2hCLDBDQUF5Qjs7SUFDekIsd0NBQXVCOztJQUN2QiwyQ0FBMEI7O0lBQzFCLHVDQUF1Qjs7SUFDdkIsbUNBQWtCOztJQUNsQixvQ0FBbUI7O0lBQ25CLGdDQUErQjs7SUFDL0Isc0NBQXFCOztJQUNyQixxQ0FBb0I7O0lBQ3BCLG1DQUEwQjs7SUFDMUIsNkNBQWtEOztJQUNsRCx5Q0FBaUM7O0lBQ2pDLGtDQUF3Qjs7SUFDeEIsMkNBQW1EOztJQUduRCx5Q0FBNEI7O0lBQzVCLGdDQUE4Qjs7SUFDOUIsaUNBQXFDOztJQUdyQyxxQ0FBNkI7O0lBQzdCLDJDQUFxQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4vKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50LWVzNic7XHJcbmltcG9ydCB7IFdpZGdldFZpc2liaWxpdHlNb2RlbCB9IGZyb20gJy4uLy4uLy4uL21vZGVscy93aWRnZXQtdmlzaWJpbGl0eS5tb2RlbCc7XHJcbmltcG9ydCB7IENvbnRhaW5lckNvbHVtbk1vZGVsIH0gZnJvbSAnLi9jb250YWluZXItY29sdW1uLm1vZGVsJztcclxuaW1wb3J0IHsgRXJyb3JNZXNzYWdlTW9kZWwgfSBmcm9tICcuL2Vycm9yLW1lc3NhZ2UubW9kZWwnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRNZXRhZGF0YSB9IGZyb20gJy4vZm9ybS1maWVsZC1tZXRhZGF0YSc7XHJcbmltcG9ydCB7IEZvcm1GaWVsZE9wdGlvbiB9IGZyb20gJy4vZm9ybS1maWVsZC1vcHRpb24nO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRUeXBlcyB9IGZyb20gJy4vZm9ybS1maWVsZC10eXBlcyc7XHJcbmltcG9ydCB7IE51bWJlckZpZWxkVmFsaWRhdG9yIH0gZnJvbSAnLi9mb3JtLWZpZWxkLXZhbGlkYXRvcic7XHJcbmltcG9ydCB7IEZvcm1XaWRnZXRNb2RlbCB9IGZyb20gJy4vZm9ybS13aWRnZXQubW9kZWwnO1xyXG5pbXBvcnQgeyBGb3JtTW9kZWwgfSBmcm9tICcuL2Zvcm0ubW9kZWwnO1xyXG5cclxuLy8gTWFwcyB0byBGb3JtRmllbGRSZXByZXNlbnRhdGlvblxyXG5leHBvcnQgY2xhc3MgRm9ybUZpZWxkTW9kZWwgZXh0ZW5kcyBGb3JtV2lkZ2V0TW9kZWwge1xyXG5cclxuICAgIHByaXZhdGUgX3ZhbHVlOiBzdHJpbmc7XHJcbiAgICBwcml2YXRlIF9yZWFkT25seTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgcHJpdmF0ZSBfaXNWYWxpZDogYm9vbGVhbiA9IHRydWU7XHJcbiAgICBwcml2YXRlIF9yZXF1aXJlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIHJlYWRvbmx5IGRlZmF1bHREYXRlRm9ybWF0OiBzdHJpbmcgPSAnRC1NLVlZWVknO1xyXG4gICAgcmVhZG9ubHkgZGVmYXVsdERhdGVUaW1lRm9ybWF0OiBzdHJpbmcgPSAnRC1NLVlZWVkgaGg6bW0gQSc7XHJcblxyXG4gICAgLy8gbW9kZWwgbWVtYmVyc1xyXG4gICAgZmllbGRUeXBlOiBzdHJpbmc7XHJcbiAgICBpZDogc3RyaW5nO1xyXG4gICAgbmFtZTogc3RyaW5nO1xyXG4gICAgdHlwZTogc3RyaW5nO1xyXG4gICAgb3ZlcnJpZGVJZDogYm9vbGVhbjtcclxuICAgIHRhYjogc3RyaW5nO1xyXG4gICAgcm93c3BhbjogbnVtYmVyID0gMTtcclxuICAgIGNvbHNwYW46IG51bWJlciA9IDE7XHJcbiAgICBwbGFjZWhvbGRlcjogc3RyaW5nID0gbnVsbDtcclxuICAgIG1pbkxlbmd0aDogbnVtYmVyID0gMDtcclxuICAgIG1heExlbmd0aDogbnVtYmVyID0gMDtcclxuICAgIG1pblZhbHVlOiBzdHJpbmc7XHJcbiAgICBtYXhWYWx1ZTogc3RyaW5nO1xyXG4gICAgcmVnZXhQYXR0ZXJuOiBzdHJpbmc7XHJcbiAgICBvcHRpb25zOiBGb3JtRmllbGRPcHRpb25bXSA9IFtdO1xyXG4gICAgcmVzdFVybDogc3RyaW5nO1xyXG4gICAgcmVzdFJlc3BvbnNlUGF0aDogc3RyaW5nO1xyXG4gICAgcmVzdElkUHJvcGVydHk6IHN0cmluZztcclxuICAgIHJlc3RMYWJlbFByb3BlcnR5OiBzdHJpbmc7XHJcbiAgICBoYXNFbXB0eVZhbHVlOiBib29sZWFuO1xyXG4gICAgY2xhc3NOYW1lOiBzdHJpbmc7XHJcbiAgICBvcHRpb25UeXBlOiBzdHJpbmc7XHJcbiAgICBwYXJhbXM6IEZvcm1GaWVsZE1ldGFkYXRhID0ge307XHJcbiAgICBoeXBlcmxpbmtVcmw6IHN0cmluZztcclxuICAgIGRpc3BsYXlUZXh0OiBzdHJpbmc7XHJcbiAgICBpc1Zpc2libGU6IGJvb2xlYW4gPSB0cnVlO1xyXG4gICAgdmlzaWJpbGl0eUNvbmRpdGlvbjogV2lkZ2V0VmlzaWJpbGl0eU1vZGVsID0gbnVsbDtcclxuICAgIGVuYWJsZUZyYWN0aW9uczogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgY3VycmVuY3k6IHN0cmluZyA9IG51bGw7XHJcbiAgICBkYXRlRGlzcGxheUZvcm1hdDogc3RyaW5nID0gdGhpcy5kZWZhdWx0RGF0ZUZvcm1hdDtcclxuXHJcbiAgICAvLyBjb250YWluZXIgbW9kZWwgbWVtYmVyc1xyXG4gICAgbnVtYmVyT2ZDb2x1bW5zOiBudW1iZXIgPSAxO1xyXG4gICAgZmllbGRzOiBGb3JtRmllbGRNb2RlbFtdID0gW107XHJcbiAgICBjb2x1bW5zOiBDb250YWluZXJDb2x1bW5Nb2RlbFtdID0gW107XHJcblxyXG4gICAgLy8gdXRpbCBtZW1iZXJzXHJcbiAgICBlbXB0eU9wdGlvbjogRm9ybUZpZWxkT3B0aW9uO1xyXG4gICAgdmFsaWRhdGlvblN1bW1hcnk6IEVycm9yTWVzc2FnZU1vZGVsO1xyXG5cclxuICAgIGdldCB2YWx1ZSgpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl92YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgdmFsdWUodjogYW55KSB7XHJcbiAgICAgICAgdGhpcy5fdmFsdWUgPSB2O1xyXG4gICAgICAgIHRoaXMudXBkYXRlRm9ybSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCByZWFkT25seSgpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodGhpcy5mb3JtICYmIHRoaXMuZm9ybS5yZWFkT25seSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3JlYWRPbmx5O1xyXG4gICAgfVxyXG5cclxuICAgIHNldCByZWFkT25seShyZWFkT25seTogYm9vbGVhbikge1xyXG4gICAgICAgIHRoaXMuX3JlYWRPbmx5ID0gcmVhZE9ubHk7XHJcbiAgICAgICAgdGhpcy51cGRhdGVGb3JtKCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHJlcXVpcmVkKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9yZXF1aXJlZDtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgcmVxdWlyZWQodmFsdWU6IGJvb2xlYW4pIHtcclxuICAgICAgICB0aGlzLl9yZXF1aXJlZCA9IHZhbHVlO1xyXG4gICAgICAgIHRoaXMudXBkYXRlRm9ybSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBpc1ZhbGlkKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pc1ZhbGlkO1xyXG4gICAgfVxyXG5cclxuICAgIG1hcmtBc0ludmFsaWQoKSB7XHJcbiAgICAgICAgdGhpcy5faXNWYWxpZCA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHZhbGlkYXRlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHRoaXMudmFsaWRhdGlvblN1bW1hcnkgPSBuZXcgRXJyb3JNZXNzYWdlTW9kZWwoKTtcclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLnJlYWRPbmx5KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHZhbGlkYXRvcnMgPSB0aGlzLmZvcm0uZmllbGRWYWxpZGF0b3JzIHx8IFtdO1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IHZhbGlkYXRvciBvZiB2YWxpZGF0b3JzKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXZhbGlkYXRvci52YWxpZGF0ZSh0aGlzKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX2lzVmFsaWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5faXNWYWxpZDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5faXNWYWxpZCA9IHRydWU7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzVmFsaWQ7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoZm9ybTogRm9ybU1vZGVsLCBqc29uPzogYW55KSB7XHJcbiAgICAgICAgc3VwZXIoZm9ybSwganNvbik7XHJcbiAgICAgICAgaWYgKGpzb24pIHtcclxuICAgICAgICAgICAgdGhpcy5maWVsZFR5cGUgPSBqc29uLmZpZWxkVHlwZTtcclxuICAgICAgICAgICAgdGhpcy5pZCA9IGpzb24uaWQ7XHJcbiAgICAgICAgICAgIHRoaXMubmFtZSA9IGpzb24ubmFtZTtcclxuICAgICAgICAgICAgdGhpcy50eXBlID0ganNvbi50eXBlO1xyXG4gICAgICAgICAgICB0aGlzLl9yZXF1aXJlZCA9IDxib29sZWFuPiBqc29uLnJlcXVpcmVkO1xyXG4gICAgICAgICAgICB0aGlzLl9yZWFkT25seSA9IDxib29sZWFuPiBqc29uLnJlYWRPbmx5IHx8IGpzb24udHlwZSA9PT0gJ3JlYWRvbmx5JztcclxuICAgICAgICAgICAgdGhpcy5vdmVycmlkZUlkID0gPGJvb2xlYW4+IGpzb24ub3ZlcnJpZGVJZDtcclxuICAgICAgICAgICAgdGhpcy50YWIgPSBqc29uLnRhYjtcclxuICAgICAgICAgICAgdGhpcy5yZXN0VXJsID0ganNvbi5yZXN0VXJsO1xyXG4gICAgICAgICAgICB0aGlzLnJlc3RSZXNwb25zZVBhdGggPSBqc29uLnJlc3RSZXNwb25zZVBhdGg7XHJcbiAgICAgICAgICAgIHRoaXMucmVzdElkUHJvcGVydHkgPSBqc29uLnJlc3RJZFByb3BlcnR5O1xyXG4gICAgICAgICAgICB0aGlzLnJlc3RMYWJlbFByb3BlcnR5ID0ganNvbi5yZXN0TGFiZWxQcm9wZXJ0eTtcclxuICAgICAgICAgICAgdGhpcy5jb2xzcGFuID0gPG51bWJlcj4ganNvbi5jb2xzcGFuO1xyXG4gICAgICAgICAgICB0aGlzLm1pbkxlbmd0aCA9IDxudW1iZXI+IGpzb24ubWluTGVuZ3RoIHx8IDA7XHJcbiAgICAgICAgICAgIHRoaXMubWF4TGVuZ3RoID0gPG51bWJlcj4ganNvbi5tYXhMZW5ndGggfHwgMDtcclxuICAgICAgICAgICAgdGhpcy5taW5WYWx1ZSA9IGpzb24ubWluVmFsdWU7XHJcbiAgICAgICAgICAgIHRoaXMubWF4VmFsdWUgPSBqc29uLm1heFZhbHVlO1xyXG4gICAgICAgICAgICB0aGlzLnJlZ2V4UGF0dGVybiA9IGpzb24ucmVnZXhQYXR0ZXJuO1xyXG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMgPSA8Rm9ybUZpZWxkT3B0aW9uW10+IGpzb24ub3B0aW9ucyB8fCBbXTtcclxuICAgICAgICAgICAgdGhpcy5oYXNFbXB0eVZhbHVlID0gPGJvb2xlYW4+IGpzb24uaGFzRW1wdHlWYWx1ZTtcclxuICAgICAgICAgICAgdGhpcy5jbGFzc05hbWUgPSBqc29uLmNsYXNzTmFtZTtcclxuICAgICAgICAgICAgdGhpcy5vcHRpb25UeXBlID0ganNvbi5vcHRpb25UeXBlO1xyXG4gICAgICAgICAgICB0aGlzLnBhcmFtcyA9IDxGb3JtRmllbGRNZXRhZGF0YT4ganNvbi5wYXJhbXMgfHwge307XHJcbiAgICAgICAgICAgIHRoaXMuaHlwZXJsaW5rVXJsID0ganNvbi5oeXBlcmxpbmtVcmw7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheVRleHQgPSBqc29uLmRpc3BsYXlUZXh0O1xyXG4gICAgICAgICAgICB0aGlzLnZpc2liaWxpdHlDb25kaXRpb24gPSBqc29uLnZpc2liaWxpdHlDb25kaXRpb24gPyBuZXcgV2lkZ2V0VmlzaWJpbGl0eU1vZGVsKGpzb24udmlzaWJpbGl0eUNvbmRpdGlvbikgOiB1bmRlZmluZWQ7XHJcbiAgICAgICAgICAgIHRoaXMuZW5hYmxlRnJhY3Rpb25zID0gPGJvb2xlYW4+IGpzb24uZW5hYmxlRnJhY3Rpb25zO1xyXG4gICAgICAgICAgICB0aGlzLmN1cnJlbmN5ID0ganNvbi5jdXJyZW5jeTtcclxuICAgICAgICAgICAgdGhpcy5kYXRlRGlzcGxheUZvcm1hdCA9IGpzb24uZGF0ZURpc3BsYXlGb3JtYXQgfHwgdGhpcy5nZXREZWZhdWx0RGF0ZUZvcm1hdChqc29uKTtcclxuICAgICAgICAgICAgdGhpcy5fdmFsdWUgPSB0aGlzLnBhcnNlVmFsdWUoanNvbik7XHJcbiAgICAgICAgICAgIHRoaXMudmFsaWRhdGlvblN1bW1hcnkgPSBuZXcgRXJyb3JNZXNzYWdlTW9kZWwoKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChqc29uLnBsYWNlaG9sZGVyICYmIGpzb24ucGxhY2Vob2xkZXIgIT09ICcnICYmIGpzb24ucGxhY2Vob2xkZXIgIT09ICdudWxsJykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wbGFjZWhvbGRlciA9IGpzb24ucGxhY2Vob2xkZXI7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChGb3JtRmllbGRUeXBlcy5pc1JlYWRPbmx5VHlwZShqc29uLnR5cGUpKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoanNvbi5wYXJhbXMgJiYganNvbi5wYXJhbXMuZmllbGQpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZm9ybS5wcm9jZXNzVmFyaWFibGVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHByb2Nlc3NWYXJpYWJsZSA9IHRoaXMuZ2V0UHJvY2Vzc1ZhcmlhYmxlVmFsdWUoanNvbi5wYXJhbXMuZmllbGQsIGZvcm0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAocHJvY2Vzc1ZhcmlhYmxlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnZhbHVlID0gcHJvY2Vzc1ZhcmlhYmxlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChqc29uLnBhcmFtcy5yZXNwb25zZVZhcmlhYmxlICYmIGZvcm0uanNvbi52YXJpYWJsZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZm9ybVZhcmlhYmxlID0gdGhpcy5nZXRWYXJpYWJsZXNWYWx1ZShqc29uLnBhcmFtcy5maWVsZC5uYW1lLCBmb3JtKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGZvcm1WYXJpYWJsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy52YWx1ZSA9IGZvcm1WYXJpYWJsZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKEZvcm1GaWVsZFR5cGVzLmlzQ29udGFpbmVyVHlwZShqc29uLnR5cGUpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRhaW5lckZhY3RvcnkoanNvbiwgZm9ybSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmhhc0VtcHR5VmFsdWUgJiYgdGhpcy5vcHRpb25zICYmIHRoaXMub3B0aW9ucy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZW1wdHlPcHRpb24gPSB0aGlzLm9wdGlvbnNbMF07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnVwZGF0ZUZvcm0oKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldERlZmF1bHREYXRlRm9ybWF0KGpzb25GaWVsZDogYW55KTogc3RyaW5nIHtcclxuICAgICAgICBsZXQgb3JpZ2luYWxUeXBlID0ganNvbkZpZWxkLnR5cGU7XHJcbiAgICAgICAgaWYgKEZvcm1GaWVsZFR5cGVzLmlzUmVhZE9ubHlUeXBlKGpzb25GaWVsZC50eXBlKSAmJlxyXG4gICAgICAgICAgICBqc29uRmllbGQucGFyYW1zICYmXHJcbiAgICAgICAgICAgIGpzb25GaWVsZC5wYXJhbXMuZmllbGQpIHtcclxuICAgICAgICAgICAgb3JpZ2luYWxUeXBlID0ganNvbkZpZWxkLnBhcmFtcy5maWVsZC50eXBlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gb3JpZ2luYWxUeXBlID09PSBGb3JtRmllbGRUeXBlcy5EQVRFVElNRSA/IHRoaXMuZGVmYXVsdERhdGVUaW1lRm9ybWF0IDogdGhpcy5kZWZhdWx0RGF0ZUZvcm1hdDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGlzVHlwZWFoZWFkRmllbGRUeXBlKHR5cGU6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0eXBlID09PSAndHlwZWFoZWFkJyA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldEZpZWxkTmFtZVdpdGhMYWJlbChuYW1lOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBuYW1lICs9ICdfTEFCRUwnO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0UHJvY2Vzc1ZhcmlhYmxlVmFsdWUoZmllbGQ6IGFueSwgZm9ybTogRm9ybU1vZGVsKSB7XHJcbiAgICAgICAgbGV0IGZpZWxkTmFtZSA9IGZpZWxkLm5hbWU7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNUeXBlYWhlYWRGaWVsZFR5cGUoZmllbGQudHlwZSkpIHtcclxuICAgICAgICAgICAgZmllbGROYW1lID0gdGhpcy5nZXRGaWVsZE5hbWVXaXRoTGFiZWwoZmllbGQuaWQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5maW5kUHJvY2Vzc1ZhcmlhYmxlVmFsdWUoZmllbGROYW1lLCBmb3JtKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldFZhcmlhYmxlc1ZhbHVlKHZhcmlhYmxlTmFtZTogc3RyaW5nLCBmb3JtOiBGb3JtTW9kZWwpIHtcclxuICAgICAgICBjb25zdCB2YXJpYWJsZSA9IGZvcm0uanNvbi52YXJpYWJsZXMuZmluZCgoY3VycmVudFZhcmlhYmxlKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiBjdXJyZW50VmFyaWFibGUubmFtZSA9PT0gdmFyaWFibGVOYW1lO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAodmFyaWFibGUpIHtcclxuICAgICAgICAgICAgaWYgKHZhcmlhYmxlLnR5cGUgPT09ICdib29sZWFuJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIEpTT04ucGFyc2UodmFyaWFibGUudmFsdWUpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gdmFyaWFibGUudmFsdWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGZpbmRQcm9jZXNzVmFyaWFibGVWYWx1ZSh2YXJpYWJsZU5hbWU6IHN0cmluZywgZm9ybTogRm9ybU1vZGVsKSB7XHJcbiAgICAgICAgaWYgKGZvcm0ucHJvY2Vzc1ZhcmlhYmxlcykge1xyXG4gICAgICAgICAgICBjb25zdCB2YXJpYWJsZSA9IGZvcm0ucHJvY2Vzc1ZhcmlhYmxlcy5maW5kKChjdXJyZW50VmFyaWFibGUpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBjdXJyZW50VmFyaWFibGUubmFtZSA9PT0gdmFyaWFibGVOYW1lO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIGlmICh2YXJpYWJsZSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHZhcmlhYmxlLnR5cGUgPT09ICdib29sZWFuJyA/IEpTT04ucGFyc2UodmFyaWFibGUudmFsdWUpIDogdmFyaWFibGUudmFsdWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB1bmRlZmluZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBjb250YWluZXJGYWN0b3J5KGpzb246IGFueSwgZm9ybTogRm9ybU1vZGVsKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5udW1iZXJPZkNvbHVtbnMgPSA8bnVtYmVyPiBqc29uLm51bWJlck9mQ29sdW1ucyB8fCAxO1xyXG5cclxuICAgICAgICB0aGlzLmZpZWxkcyA9IGpzb24uZmllbGRzO1xyXG5cclxuICAgICAgICB0aGlzLnJvd3NwYW4gPSAxO1xyXG4gICAgICAgIHRoaXMuY29sc3BhbiA9IDE7XHJcblxyXG4gICAgICAgIGlmIChqc29uLmZpZWxkcykge1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IGN1cnJlbnRGaWVsZCBpbiBqc29uLmZpZWxkcykge1xyXG4gICAgICAgICAgICAgICAgaWYgKGpzb24uZmllbGRzLmhhc093blByb3BlcnR5KGN1cnJlbnRGaWVsZCkpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBjb2wgPSBuZXcgQ29udGFpbmVyQ29sdW1uTW9kZWwoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZmllbGRzOiBGb3JtRmllbGRNb2RlbFtdID0gKGpzb24uZmllbGRzW2N1cnJlbnRGaWVsZF0gfHwgW10pLm1hcCgoZikgPT4gbmV3IEZvcm1GaWVsZE1vZGVsKGZvcm0sIGYpKTtcclxuICAgICAgICAgICAgICAgICAgICBjb2wuZmllbGRzID0gZmllbGRzO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbC5yb3dzcGFuID0ganNvbi5maWVsZHNbY3VycmVudEZpZWxkXS5sZW5ndGg7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGNvbC5maWVsZHMuZm9yRWFjaCgoY29sRmllbGRzOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb2xzcGFuID0gY29sRmllbGRzLmNvbHNwYW4gPiB0aGlzLmNvbHNwYW4gPyBjb2xGaWVsZHMuY29sc3BhbiA6IHRoaXMuY29sc3BhbjtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3dzcGFuID0gdGhpcy5yb3dzcGFuIDwgY29sLnJvd3NwYW4gPyBjb2wucm93c3BhbiA6IHRoaXMucm93c3BhbjtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbHVtbnMucHVzaChjb2wpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHBhcnNlVmFsdWUoanNvbjogYW55KTogYW55IHtcclxuICAgICAgICBsZXQgdmFsdWUgPSBqc29uLmhhc093blByb3BlcnR5KCd2YWx1ZScpID8ganNvbi52YWx1ZSA6IG51bGw7XHJcblxyXG4gICAgICAgIC8qXHJcbiAgICAgICAgIFRoaXMgaXMgbmVlZGVkIGR1ZSB0byBBY3Rpdml0aSBpc3N1ZSByZWxhdGVkIHRvIHJlYWRpbmcgZHJvcGRvd24gdmFsdWVzIGFzIHZhbHVlIHN0cmluZ1xyXG4gICAgICAgICBidXQgc2F2aW5nIGJhY2sgYXMgb2JqZWN0OiB7IGlkOiA8aWQ+LCBuYW1lOiA8bmFtZT4gfVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIGlmIChqc29uLnR5cGUgPT09IEZvcm1GaWVsZFR5cGVzLkRST1BET1dOKSB7XHJcbiAgICAgICAgICAgIGlmIChqc29uLmhhc0VtcHR5VmFsdWUgJiYganNvbi5vcHRpb25zKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBvcHRpb25zID0gPEZvcm1GaWVsZE9wdGlvbltdPiBqc29uLm9wdGlvbnMgfHwgW107XHJcbiAgICAgICAgICAgICAgICBpZiAob3B0aW9ucy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZW1wdHlPcHRpb24gPSBqc29uLm9wdGlvbnNbMF07XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHZhbHVlID09PSAnJyB8fCB2YWx1ZSA9PT0gZW1wdHlPcHRpb24uaWQgfHwgdmFsdWUgPT09IGVtcHR5T3B0aW9uLm5hbWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSBlbXB0eU9wdGlvbi5pZDtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHZhbHVlLmlkICYmIHZhbHVlLm5hbWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSB2YWx1ZS5pZDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8qXHJcbiAgICAgICAgIFRoaXMgaXMgbmVlZGVkIGR1ZSB0byBBY3Rpdml0aSBpc3N1ZSByZWxhdGVkIHRvIHJlYWRpbmcgcmFkaW8gYnV0dG9uIHZhbHVlcyBhcyB2YWx1ZSBzdHJpbmdcclxuICAgICAgICAgYnV0IHNhdmluZyBiYWNrIGFzIG9iamVjdDogeyBpZDogPGlkPiwgbmFtZTogPG5hbWU+IH1cclxuICAgICAgICAgKi9cclxuICAgICAgICBpZiAoanNvbi50eXBlID09PSBGb3JtRmllbGRUeXBlcy5SQURJT19CVVRUT05TKSB7XHJcbiAgICAgICAgICAgIC8vIEFjdGl2aXRpIGhhcyBhIGJ1ZyB3aXRoIGRlZmF1bHQgcmFkaW8gYnV0dG9uIHZhbHVlIHdoZXJlIGluaXRpYWwgc2VsZWN0aW9uIHBhc3NlZCBhcyBgbmFtZWAgdmFsdWVcclxuICAgICAgICAgICAgLy8gc28gdHJ5IHJlc29sdmluZyBjdXJyZW50IG9uZSB3aXRoIGEgZmFsbGJhY2sgdG8gZmlyc3QgZW50cnkgdmlhIG5hbWUgb3IgaWRcclxuICAgICAgICAgICAgLy8gVE9ETzogbmVlZHMgdG8gYmUgcmVwb3J0ZWQgYW5kIGZpeGVkIGF0IEFjdGl2aXRpIHNpZGVcclxuICAgICAgICAgICAgY29uc3QgZW50cnk6IEZvcm1GaWVsZE9wdGlvbltdID0gdGhpcy5vcHRpb25zLmZpbHRlcigob3B0KSA9PlxyXG4gICAgICAgICAgICAgICAgb3B0LmlkID09PSB2YWx1ZSB8fCBvcHQubmFtZSA9PT0gdmFsdWUgfHwgKHZhbHVlICYmIChvcHQuaWQgPT09IHZhbHVlLmlkIHx8IG9wdC5uYW1lID09PSB2YWx1ZS5uYW1lKSkpO1xyXG4gICAgICAgICAgICBpZiAoZW50cnkubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgdmFsdWUgPSBlbnRyeVswXS5pZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLypcclxuICAgICAgICAgVGhpcyBpcyBuZWVkZWQgZHVlIHRvIEFjdGl2aXRpIGRpc3BsYXlpbmcvZWRpdGluZyBkYXRlcyBpbiBkLU0tWVlZWSBmb3JtYXRcclxuICAgICAgICAgYnV0IHN0b3Jpbmcgb24gc2VydmVyIGluIElTTzg2MDEgZm9ybWF0IChpLmUuIDIwMTMtMDItMDRUMjI6NDQ6MzAuNjUyWilcclxuICAgICAgICAgKi9cclxuICAgICAgICBpZiAodGhpcy5pc0RhdGVGaWVsZChqc29uKSB8fCB0aGlzLmlzRGF0ZVRpbWVGaWVsZChqc29uKSkge1xyXG4gICAgICAgICAgICBpZiAodmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIGxldCBkYXRlVmFsdWU7XHJcbiAgICAgICAgICAgICAgICBpZiAoTnVtYmVyRmllbGRWYWxpZGF0b3IuaXNOdW1iZXIodmFsdWUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0ZVZhbHVlID0gbW9tZW50KHZhbHVlKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0ZVZhbHVlID0gdGhpcy5pc0RhdGVUaW1lRmllbGQoanNvbikgPyBtb21lbnQodmFsdWUsICdZWVlZLU1NLUREIGhoOm1tIEEnKSA6IG1vbWVudCh2YWx1ZS5zcGxpdCgnVCcpWzBdLCAnWVlZWS1NLUQnKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmIChkYXRlVmFsdWUgJiYgZGF0ZVZhbHVlLmlzVmFsaWQoKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlID0gZGF0ZVZhbHVlLmZvcm1hdCh0aGlzLmRhdGVEaXNwbGF5Rm9ybWF0KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIHVwZGF0ZUZvcm0oKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLmZvcm0pIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgc3dpdGNoICh0aGlzLnR5cGUpIHtcclxuICAgICAgICAgICAgY2FzZSBGb3JtRmllbGRUeXBlcy5EUk9QRE9XTjpcclxuICAgICAgICAgICAgICAgIC8qXHJcbiAgICAgICAgICAgICAgICAgVGhpcyBpcyBuZWVkZWQgZHVlIHRvIEFjdGl2aXRpIHJlYWRpbmcgZHJvcGRvd24gdmFsdWVzIGFzIHN0cmluZ1xyXG4gICAgICAgICAgICAgICAgIGJ1dCBzYXZpbmcgYmFjayBhcyBvYmplY3Q6IHsgaWQ6IDxpZD4sIG5hbWU6IDxuYW1lPiB9XHJcbiAgICAgICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnZhbHVlID09PSAnZW1wdHknIHx8IHRoaXMudmFsdWUgPT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JtLnZhbHVlc1t0aGlzLmlkXSA9IHt9O1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBlbnRyeTogRm9ybUZpZWxkT3B0aW9uW10gPSB0aGlzLm9wdGlvbnMuZmlsdGVyKChvcHQpID0+IG9wdC5pZCA9PT0gdGhpcy52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGVudHJ5Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JtLnZhbHVlc1t0aGlzLmlkXSA9IGVudHJ5WzBdO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIEZvcm1GaWVsZFR5cGVzLlJBRElPX0JVVFRPTlM6XHJcbiAgICAgICAgICAgICAgICAvKlxyXG4gICAgICAgICAgICAgICAgIFRoaXMgaXMgbmVlZGVkIGR1ZSB0byBBY3Rpdml0aSBpc3N1ZSByZWxhdGVkIHRvIHJlYWRpbmcgcmFkaW8gYnV0dG9uIHZhbHVlcyBhcyB2YWx1ZSBzdHJpbmdcclxuICAgICAgICAgICAgICAgICBidXQgc2F2aW5nIGJhY2sgYXMgb2JqZWN0OiB7IGlkOiA8aWQ+LCBuYW1lOiA8bmFtZT4gfVxyXG4gICAgICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgICAgICBjb25zdCByYkVudHJ5OiBGb3JtRmllbGRPcHRpb25bXSA9IHRoaXMub3B0aW9ucy5maWx0ZXIoKG9wdCkgPT4gb3B0LmlkID09PSB0aGlzLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgIGlmIChyYkVudHJ5Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm0udmFsdWVzW3RoaXMuaWRdID0gcmJFbnRyeVswXTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIEZvcm1GaWVsZFR5cGVzLlVQTE9BRDpcclxuICAgICAgICAgICAgICAgIHRoaXMuZm9ybS5oYXNVcGxvYWQgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudmFsdWUgJiYgdGhpcy52YWx1ZS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JtLnZhbHVlc1t0aGlzLmlkXSA9IHRoaXMudmFsdWUubWFwKChlbGVtKSA9PiBlbGVtLmlkKS5qb2luKCcsJyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZm9ybS52YWx1ZXNbdGhpcy5pZF0gPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgRm9ybUZpZWxkVHlwZXMuVFlQRUFIRUFEOlxyXG4gICAgICAgICAgICAgICAgY29uc3QgdGFFbnRyeTogRm9ybUZpZWxkT3B0aW9uW10gPSB0aGlzLm9wdGlvbnMuZmlsdGVyKChvcHQpID0+IG9wdC5pZCA9PT0gdGhpcy52YWx1ZSB8fCBvcHQubmFtZSA9PT0gdGhpcy52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICBpZiAodGFFbnRyeS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JtLnZhbHVlc1t0aGlzLmlkXSA9IHRhRW50cnlbMF07XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMub3B0aW9ucy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JtLnZhbHVlc1t0aGlzLmlkXSA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBGb3JtRmllbGRUeXBlcy5EQVRFOlxyXG4gICAgICAgICAgICAgICAgY29uc3QgZGF0ZVZhbHVlID0gbW9tZW50KHRoaXMudmFsdWUsIHRoaXMuZGF0ZURpc3BsYXlGb3JtYXQsIHRydWUpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGRhdGVWYWx1ZSAmJiBkYXRlVmFsdWUuaXNWYWxpZCgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JtLnZhbHVlc1t0aGlzLmlkXSA9IGAke2RhdGVWYWx1ZS5mb3JtYXQoJ1lZWVktTU0tREQnKX1UMDA6MDA6MDAuMDAwWmA7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZm9ybS52YWx1ZXNbdGhpcy5pZF0gPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3ZhbHVlID0gdGhpcy52YWx1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIEZvcm1GaWVsZFR5cGVzLkRBVEVUSU1FOlxyXG4gICAgICAgICAgICAgICAgY29uc3QgZGF0ZVRpbWVWYWx1ZSA9IG1vbWVudCh0aGlzLnZhbHVlLCB0aGlzLmRhdGVEaXNwbGF5Rm9ybWF0LCB0cnVlKTtcclxuICAgICAgICAgICAgICAgIGlmIChkYXRlVGltZVZhbHVlICYmIGRhdGVUaW1lVmFsdWUuaXNWYWxpZCgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLyogY3NwZWxsOmRpc2FibGUtbmV4dC1saW5lICovXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JtLnZhbHVlc1t0aGlzLmlkXSA9IGRhdGVUaW1lVmFsdWUuZm9ybWF0KCdZWVlZLU1NLUREVEhIOm1tOnNzWicpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm0udmFsdWVzW3RoaXMuaWRdID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLl92YWx1ZSA9IHRoaXMudmFsdWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBGb3JtRmllbGRUeXBlcy5OVU1CRVI6XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZvcm0udmFsdWVzW3RoaXMuaWRdID0gcGFyc2VJbnQodGhpcy52YWx1ZSwgMTApO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgRm9ybUZpZWxkVHlwZXMuQU1PVU5UOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5mb3JtLnZhbHVlc1t0aGlzLmlkXSA9IHRoaXMuZW5hYmxlRnJhY3Rpb25zID8gcGFyc2VGbG9hdCh0aGlzLnZhbHVlKSA6IHBhcnNlSW50KHRoaXMudmFsdWUsIDEwKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgaWYgKCFGb3JtRmllbGRUeXBlcy5pc1JlYWRPbmx5VHlwZSh0aGlzLnR5cGUpICYmICF0aGlzLmlzSW52YWxpZEZpZWxkVHlwZSh0aGlzLnR5cGUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JtLnZhbHVlc1t0aGlzLmlkXSA9IHRoaXMudmFsdWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmZvcm0ub25Gb3JtRmllbGRDaGFuZ2VkKHRoaXMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2tpcCB0aGUgaW52YWxpZCBmaWVsZCB0eXBlXHJcbiAgICAgKiBAcGFyYW0gdHlwZVxyXG4gICAgICovXHJcbiAgICBpc0ludmFsaWRGaWVsZFR5cGUodHlwZTogc3RyaW5nKSB7XHJcbiAgICAgICAgaWYgKHR5cGUgPT09ICdjb250YWluZXInKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0T3B0aW9uTmFtZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIGNvbnN0IG9wdGlvbjogRm9ybUZpZWxkT3B0aW9uID0gdGhpcy5vcHRpb25zLmZpbmQoKG9wdCkgPT4gb3B0LmlkID09PSB0aGlzLnZhbHVlKTtcclxuICAgICAgICByZXR1cm4gb3B0aW9uID8gb3B0aW9uLm5hbWUgOiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc09wdGlvbnMoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub3B0aW9ucyAmJiB0aGlzLm9wdGlvbnMubGVuZ3RoID4gMDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGlzRGF0ZUZpZWxkKGpzb246IGFueSkge1xyXG4gICAgICAgIHJldHVybiAoanNvbi5wYXJhbXMgJiZcclxuICAgICAgICAgICAganNvbi5wYXJhbXMuZmllbGQgJiZcclxuICAgICAgICAgICAganNvbi5wYXJhbXMuZmllbGQudHlwZSA9PT0gRm9ybUZpZWxkVHlwZXMuREFURSkgfHxcclxuICAgICAgICAgICAganNvbi50eXBlID09PSBGb3JtRmllbGRUeXBlcy5EQVRFO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaXNEYXRlVGltZUZpZWxkKGpzb246IGFueSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiAoanNvbi5wYXJhbXMgJiZcclxuICAgICAgICAgICAganNvbi5wYXJhbXMuZmllbGQgJiZcclxuICAgICAgICAgICAganNvbi5wYXJhbXMuZmllbGQudHlwZSA9PT0gRm9ybUZpZWxkVHlwZXMuREFURVRJTUUpIHx8XHJcbiAgICAgICAgICAgIGpzb24udHlwZSA9PT0gRm9ybUZpZWxkVHlwZXMuREFURVRJTUU7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==