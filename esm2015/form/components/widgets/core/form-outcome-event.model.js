/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
export class FormOutcomeEvent {
    /**
     * @param {?} outcome
     */
    constructor(outcome) {
        this._defaultPrevented = false;
        this._outcome = outcome;
    }
    /**
     * @return {?}
     */
    get outcome() {
        return this._outcome;
    }
    /**
     * @return {?}
     */
    get defaultPrevented() {
        return this._defaultPrevented;
    }
    /**
     * @return {?}
     */
    preventDefault() {
        this._defaultPrevented = true;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    FormOutcomeEvent.prototype._outcome;
    /**
     * @type {?}
     * @private
     */
    FormOutcomeEvent.prototype._defaultPrevented;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1vdXRjb21lLWV2ZW50Lm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvY29yZS9mb3JtLW91dGNvbWUtZXZlbnQubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBcUJBLE1BQU0sT0FBTyxnQkFBZ0I7Ozs7SUFhekIsWUFBWSxPQUF5QjtRQVY3QixzQkFBaUIsR0FBWSxLQUFLLENBQUM7UUFXdkMsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUM7SUFDNUIsQ0FBQzs7OztJQVZELElBQUksT0FBTztRQUNQLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDOzs7O0lBRUQsSUFBSSxnQkFBZ0I7UUFDaEIsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUM7SUFDbEMsQ0FBQzs7OztJQU1ELGNBQWM7UUFDVixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO0lBQ2xDLENBQUM7Q0FFSjs7Ozs7O0lBbkJHLG9DQUFtQzs7Ozs7SUFDbkMsNkNBQTJDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbiAvKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgeyBGb3JtT3V0Y29tZU1vZGVsIH0gZnJvbSAnLi9mb3JtLW91dGNvbWUubW9kZWwnO1xyXG5cclxuZXhwb3J0IGNsYXNzIEZvcm1PdXRjb21lRXZlbnQge1xyXG5cclxuICAgIHByaXZhdGUgX291dGNvbWU6IEZvcm1PdXRjb21lTW9kZWw7XHJcbiAgICBwcml2YXRlIF9kZWZhdWx0UHJldmVudGVkOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgZ2V0IG91dGNvbWUoKTogRm9ybU91dGNvbWVNb2RlbCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX291dGNvbWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRlZmF1bHRQcmV2ZW50ZWQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2RlZmF1bHRQcmV2ZW50ZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3Iob3V0Y29tZTogRm9ybU91dGNvbWVNb2RlbCkge1xyXG4gICAgICAgIHRoaXMuX291dGNvbWUgPSBvdXRjb21lO1xyXG4gICAgfVxyXG5cclxuICAgIHByZXZlbnREZWZhdWx0KCkge1xyXG4gICAgICAgIHRoaXMuX2RlZmF1bHRQcmV2ZW50ZWQgPSB0cnVlO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=