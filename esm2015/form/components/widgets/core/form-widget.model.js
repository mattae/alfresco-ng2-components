/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
/**
 * @abstract
 */
export class FormWidgetModel {
    /**
     * @param {?} form
     * @param {?} json
     */
    constructor(form, json) {
        this.form = form;
        this.json = json;
        if (json) {
            this.fieldType = json.fieldType;
            this.id = json.id;
            this.name = json.name;
            this.type = json.type;
            this.tab = json.tab;
        }
    }
}
if (false) {
    /** @type {?} */
    FormWidgetModel.prototype.fieldType;
    /** @type {?} */
    FormWidgetModel.prototype.id;
    /** @type {?} */
    FormWidgetModel.prototype.name;
    /** @type {?} */
    FormWidgetModel.prototype.type;
    /** @type {?} */
    FormWidgetModel.prototype.tab;
    /** @type {?} */
    FormWidgetModel.prototype.form;
    /** @type {?} */
    FormWidgetModel.prototype.json;
}
/**
 * @record
 * @template T
 */
export function FormWidgetModelCache() { }
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS13aWRnZXQubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9jb3JlL2Zvcm0td2lkZ2V0Lm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXFCQSxNQUFNLE9BQWdCLGVBQWU7Ozs7O0lBV2pDLFlBQVksSUFBZSxFQUFFLElBQVM7UUFDbEMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFFakIsSUFBSSxJQUFJLEVBQUU7WUFDTixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDaEMsSUFBSSxDQUFDLEVBQUUsR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO1lBQ2xCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztZQUN0QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDdEIsSUFBSSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDO1NBQ3ZCO0lBQ0wsQ0FBQztDQUNKOzs7SUFyQkcsb0NBQTJCOztJQUMzQiw2QkFBb0I7O0lBQ3BCLCtCQUFzQjs7SUFDdEIsK0JBQXNCOztJQUN0Qiw4QkFBcUI7O0lBRXJCLCtCQUFtQjs7SUFDbkIsK0JBQW1COzs7Ozs7QUFnQnZCLDBDQUVDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbiAvKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgeyBGb3JtTW9kZWwgfSBmcm9tICcuL2Zvcm0ubW9kZWwnO1xyXG5cclxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEZvcm1XaWRnZXRNb2RlbCB7XHJcblxyXG4gICAgcmVhZG9ubHkgZmllbGRUeXBlOiBzdHJpbmc7XHJcbiAgICByZWFkb25seSBpZDogc3RyaW5nO1xyXG4gICAgcmVhZG9ubHkgbmFtZTogc3RyaW5nO1xyXG4gICAgcmVhZG9ubHkgdHlwZTogc3RyaW5nO1xyXG4gICAgcmVhZG9ubHkgdGFiOiBzdHJpbmc7XHJcblxyXG4gICAgcmVhZG9ubHkgZm9ybTogYW55O1xyXG4gICAgcmVhZG9ubHkganNvbjogYW55O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGZvcm06IEZvcm1Nb2RlbCwganNvbjogYW55KSB7XHJcbiAgICAgICAgdGhpcy5mb3JtID0gZm9ybTtcclxuICAgICAgICB0aGlzLmpzb24gPSBqc29uO1xyXG5cclxuICAgICAgICBpZiAoanNvbikge1xyXG4gICAgICAgICAgICB0aGlzLmZpZWxkVHlwZSA9IGpzb24uZmllbGRUeXBlO1xyXG4gICAgICAgICAgICB0aGlzLmlkID0ganNvbi5pZDtcclxuICAgICAgICAgICAgdGhpcy5uYW1lID0ganNvbi5uYW1lO1xyXG4gICAgICAgICAgICB0aGlzLnR5cGUgPSBqc29uLnR5cGU7XHJcbiAgICAgICAgICAgIHRoaXMudGFiID0ganNvbi50YWI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEZvcm1XaWRnZXRNb2RlbENhY2hlPFQgZXh0ZW5kcyBGb3JtV2lkZ2V0TW9kZWw+IHtcclxuICAgIFtrZXk6IHN0cmluZ106IFQ7XHJcbn1cclxuIl19