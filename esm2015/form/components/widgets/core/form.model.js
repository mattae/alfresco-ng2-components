/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { FormFieldEvent } from './../../../events/form-field.event';
import { ValidateFormFieldEvent } from './../../../events/validate-form-field.event';
import { ValidateFormEvent } from './../../../events/validate-form.event';
import { ContainerModel } from './container.model';
import { FormFieldTypes } from './form-field-types';
import { FormFieldModel } from './form-field.model';
import { FormOutcomeModel } from './form-outcome.model';
import { TabModel } from './tab.model';
import { FORM_FIELD_VALIDATORS } from './form-field-validator';
import { FormBaseModel } from '../../form-base.model';
export class FormModel extends FormBaseModel {
    /**
     * @param {?=} formRepresentationJSON
     * @param {?=} formValues
     * @param {?=} readOnly
     * @param {?=} formService
     */
    constructor(formRepresentationJSON, formValues, readOnly = false, formService) {
        super();
        this.formService = formService;
        this.taskName = FormModel.UNSET_TASK_NAME;
        this.customFieldTemplates = {};
        this.fieldValidators = [...FORM_FIELD_VALIDATORS];
        this.readOnly = readOnly;
        if (formRepresentationJSON) {
            this.json = formRepresentationJSON;
            this.id = formRepresentationJSON.id;
            this.name = formRepresentationJSON.name;
            this.taskId = formRepresentationJSON.taskId;
            this.taskName = formRepresentationJSON.taskName || formRepresentationJSON.name || FormModel.UNSET_TASK_NAME;
            this.processDefinitionId = formRepresentationJSON.processDefinitionId;
            this.customFieldTemplates = formRepresentationJSON.customFieldTemplates || {};
            this.selectedOutcome = formRepresentationJSON.selectedOutcome || {};
            this.className = formRepresentationJSON.className || '';
            /** @type {?} */
            const tabCache = {};
            this.processVariables = formRepresentationJSON.processVariables;
            this.tabs = (formRepresentationJSON.tabs || []).map((/**
             * @param {?} t
             * @return {?}
             */
            (t) => {
                /** @type {?} */
                const model = new TabModel(this, t);
                tabCache[model.id] = model;
                return model;
            }));
            this.fields = this.parseRootFields(formRepresentationJSON);
            if (formValues) {
                this.loadData(formValues);
            }
            for (let i = 0; i < this.fields.length; i++) {
                /** @type {?} */
                const field = this.fields[i];
                if (field.tab) {
                    /** @type {?} */
                    const tab = tabCache[field.tab];
                    if (tab) {
                        tab.fields.push(field);
                    }
                }
            }
            if (formRepresentationJSON.fields) {
                /** @type {?} */
                const saveOutcome = new FormOutcomeModel(this, {
                    id: FormModel.SAVE_OUTCOME,
                    name: 'SAVE',
                    isSystem: true
                });
                /** @type {?} */
                const completeOutcome = new FormOutcomeModel(this, {
                    id: FormModel.COMPLETE_OUTCOME,
                    name: 'COMPLETE',
                    isSystem: true
                });
                /** @type {?} */
                const startProcessOutcome = new FormOutcomeModel(this, {
                    id: FormModel.START_PROCESS_OUTCOME,
                    name: 'START PROCESS',
                    isSystem: true
                });
                /** @type {?} */
                const customOutcomes = (formRepresentationJSON.outcomes || []).map((/**
                 * @param {?} obj
                 * @return {?}
                 */
                (obj) => new FormOutcomeModel(this, obj)));
                this.outcomes = [saveOutcome].concat(customOutcomes.length > 0 ? customOutcomes : [completeOutcome, startProcessOutcome]);
            }
        }
        this.validateForm();
    }
    /**
     * @param {?} field
     * @return {?}
     */
    onFormFieldChanged(field) {
        this.validateField(field);
        if (this.formService) {
            this.formService.formFieldValueChanged.next(new FormFieldEvent(this, field));
        }
    }
    /**
     * Validates entire form and all form fields.
     *
     * \@memberof FormModel
     * @return {?}
     */
    validateForm() {
        /** @type {?} */
        const validateFormEvent = new ValidateFormEvent(this);
        /** @type {?} */
        const errorsField = [];
        /** @type {?} */
        const fields = this.getFormFields();
        for (let i = 0; i < fields.length; i++) {
            if (!fields[i].validate()) {
                errorsField.push(fields[i]);
            }
        }
        this.isValid = errorsField.length > 0 ? false : true;
        if (this.formService) {
            validateFormEvent.isValid = this.isValid;
            validateFormEvent.errorsField = errorsField;
            this.formService.validateForm.next(validateFormEvent);
        }
    }
    /**
     * Validates a specific form field, triggers form validation.
     *
     * \@memberof FormModel
     * @param {?} field Form field to validate.
     * @return {?}
     */
    validateField(field) {
        if (!field) {
            return;
        }
        /** @type {?} */
        const validateFieldEvent = new ValidateFormFieldEvent(this, field);
        if (this.formService) {
            this.formService.validateFormField.next(validateFieldEvent);
        }
        if (!validateFieldEvent.isValid) {
            this.markAsInvalid();
            return;
        }
        if (validateFieldEvent.defaultPrevented) {
            return;
        }
        if (!field.validate()) {
            this.markAsInvalid();
        }
        this.validateForm();
    }
    // Activiti supports 3 types of root fields: container|group|dynamic-table
    /**
     * @private
     * @param {?} json
     * @return {?}
     */
    parseRootFields(json) {
        /** @type {?} */
        let fields = [];
        if (json.fields) {
            fields = json.fields;
        }
        else if (json.formDefinition && json.formDefinition.fields) {
            fields = json.formDefinition.fields;
        }
        /** @type {?} */
        const formWidgetModel = [];
        for (const field of fields) {
            if (field.type === FormFieldTypes.DISPLAY_VALUE) {
                // workaround for dynamic table on a completed/readonly form
                if (field.params) {
                    /** @type {?} */
                    const originalField = field.params['field'];
                    if (originalField.type === FormFieldTypes.DYNAMIC_TABLE) {
                        formWidgetModel.push(new ContainerModel(new FormFieldModel(this, field)));
                    }
                }
            }
            else {
                formWidgetModel.push(new ContainerModel(new FormFieldModel(this, field)));
            }
        }
        return formWidgetModel;
    }
    // Loads external data and overrides field values
    // Typically used when form definition and form data coming from different sources
    /**
     * @private
     * @param {?} formValues
     * @return {?}
     */
    loadData(formValues) {
        for (const field of this.getFormFields()) {
            if (formValues[field.id]) {
                field.json.value = formValues[field.id];
                field.value = field.parseValue(field.json);
            }
        }
    }
}
if (false) {
    /** @type {?} */
    FormModel.prototype.id;
    /** @type {?} */
    FormModel.prototype.name;
    /** @type {?} */
    FormModel.prototype.taskId;
    /** @type {?} */
    FormModel.prototype.taskName;
    /** @type {?} */
    FormModel.prototype.processDefinitionId;
    /** @type {?} */
    FormModel.prototype.customFieldTemplates;
    /** @type {?} */
    FormModel.prototype.fieldValidators;
    /** @type {?} */
    FormModel.prototype.selectedOutcome;
    /** @type {?} */
    FormModel.prototype.processVariables;
    /**
     * @type {?}
     * @protected
     */
    FormModel.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2NvcmUvZm9ybS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ3JGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBRTFFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUVuRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBR3hELE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFFdkMsT0FBTyxFQUNILHFCQUFxQixFQUV4QixNQUFNLHdCQUF3QixDQUFDO0FBQ2hDLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUV0RCxNQUFNLE9BQU8sU0FBVSxTQUFRLGFBQWE7Ozs7Ozs7SUFjeEMsWUFBWSxzQkFBNEIsRUFBRSxVQUF1QixFQUFFLFdBQW9CLEtBQUssRUFBWSxXQUF5QjtRQUM3SCxLQUFLLEVBQUUsQ0FBQztRQUQ0RixnQkFBVyxHQUFYLFdBQVcsQ0FBYztRQVR4SCxhQUFRLEdBQVcsU0FBUyxDQUFDLGVBQWUsQ0FBQztRQUd0RCx5QkFBb0IsR0FBdUIsRUFBRSxDQUFDO1FBQzlDLG9CQUFlLEdBQXlCLENBQUMsR0FBRyxxQkFBcUIsQ0FBQyxDQUFDO1FBTy9ELElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBRXpCLElBQUksc0JBQXNCLEVBQUU7WUFDeEIsSUFBSSxDQUFDLElBQUksR0FBRyxzQkFBc0IsQ0FBQztZQUVuQyxJQUFJLENBQUMsRUFBRSxHQUFHLHNCQUFzQixDQUFDLEVBQUUsQ0FBQztZQUNwQyxJQUFJLENBQUMsSUFBSSxHQUFHLHNCQUFzQixDQUFDLElBQUksQ0FBQztZQUN4QyxJQUFJLENBQUMsTUFBTSxHQUFHLHNCQUFzQixDQUFDLE1BQU0sQ0FBQztZQUM1QyxJQUFJLENBQUMsUUFBUSxHQUFHLHNCQUFzQixDQUFDLFFBQVEsSUFBSSxzQkFBc0IsQ0FBQyxJQUFJLElBQUksU0FBUyxDQUFDLGVBQWUsQ0FBQztZQUM1RyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsc0JBQXNCLENBQUMsbUJBQW1CLENBQUM7WUFDdEUsSUFBSSxDQUFDLG9CQUFvQixHQUFHLHNCQUFzQixDQUFDLG9CQUFvQixJQUFJLEVBQUUsQ0FBQztZQUM5RSxJQUFJLENBQUMsZUFBZSxHQUFHLHNCQUFzQixDQUFDLGVBQWUsSUFBSSxFQUFFLENBQUM7WUFDcEUsSUFBSSxDQUFDLFNBQVMsR0FBRyxzQkFBc0IsQ0FBQyxTQUFTLElBQUksRUFBRSxDQUFDOztrQkFFbEQsUUFBUSxHQUFtQyxFQUFFO1lBRW5ELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxzQkFBc0IsQ0FBQyxnQkFBZ0IsQ0FBQztZQUVoRSxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsc0JBQXNCLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLEdBQUc7Ozs7WUFBQyxDQUFDLENBQUMsRUFBRSxFQUFFOztzQkFDaEQsS0FBSyxHQUFHLElBQUksUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7Z0JBQ25DLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDO2dCQUMzQixPQUFPLEtBQUssQ0FBQztZQUNqQixDQUFDLEVBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1lBRTNELElBQUksVUFBVSxFQUFFO2dCQUNaLElBQUksQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDN0I7WUFFRCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7O3NCQUNuQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7Z0JBQzVCLElBQUksS0FBSyxDQUFDLEdBQUcsRUFBRTs7MEJBQ0wsR0FBRyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO29CQUMvQixJQUFJLEdBQUcsRUFBRTt3QkFDTCxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztxQkFDMUI7aUJBQ0o7YUFDSjtZQUVELElBQUksc0JBQXNCLENBQUMsTUFBTSxFQUFFOztzQkFDekIsV0FBVyxHQUFHLElBQUksZ0JBQWdCLENBQUMsSUFBSSxFQUFFO29CQUMzQyxFQUFFLEVBQUUsU0FBUyxDQUFDLFlBQVk7b0JBQzFCLElBQUksRUFBRSxNQUFNO29CQUNaLFFBQVEsRUFBRSxJQUFJO2lCQUNqQixDQUFDOztzQkFDSSxlQUFlLEdBQUcsSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUU7b0JBQy9DLEVBQUUsRUFBRSxTQUFTLENBQUMsZ0JBQWdCO29CQUM5QixJQUFJLEVBQUUsVUFBVTtvQkFDaEIsUUFBUSxFQUFFLElBQUk7aUJBQ2pCLENBQUM7O3NCQUNJLG1CQUFtQixHQUFHLElBQUksZ0JBQWdCLENBQUMsSUFBSSxFQUFFO29CQUNuRCxFQUFFLEVBQUUsU0FBUyxDQUFDLHFCQUFxQjtvQkFDbkMsSUFBSSxFQUFFLGVBQWU7b0JBQ3JCLFFBQVEsRUFBRSxJQUFJO2lCQUNqQixDQUFDOztzQkFFSSxjQUFjLEdBQUcsQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLElBQUksRUFBRSxDQUFDLENBQUMsR0FBRzs7OztnQkFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLEVBQUM7Z0JBRTVHLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxNQUFNLENBQ2hDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxFQUFFLG1CQUFtQixDQUFDLENBQ3RGLENBQUM7YUFDTDtTQUNKO1FBRUQsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3hCLENBQUM7Ozs7O0lBRUQsa0JBQWtCLENBQUMsS0FBcUI7UUFDcEMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMxQixJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxjQUFjLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUM7U0FDaEY7SUFDTCxDQUFDOzs7Ozs7O0lBT0QsWUFBWTs7Y0FDRixpQkFBaUIsR0FBUSxJQUFJLGlCQUFpQixDQUFDLElBQUksQ0FBQzs7Y0FFcEQsV0FBVyxHQUFxQixFQUFFOztjQUVsQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRTtRQUNuQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNwQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO2dCQUN2QixXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQy9CO1NBQ0o7UUFFRCxJQUFJLENBQUMsT0FBTyxHQUFHLFdBQVcsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUVyRCxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDbEIsaUJBQWlCLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDekMsaUJBQWlCLENBQUMsV0FBVyxHQUFHLFdBQVcsQ0FBQztZQUM1QyxJQUFJLENBQUMsV0FBVyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUN6RDtJQUVMLENBQUM7Ozs7Ozs7O0lBUUQsYUFBYSxDQUFDLEtBQXFCO1FBQy9CLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDUixPQUFPO1NBQ1Y7O2NBRUssa0JBQWtCLEdBQUcsSUFBSSxzQkFBc0IsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDO1FBRWxFLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNsQixJQUFJLENBQUMsV0FBVyxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1NBQy9EO1FBRUQsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sRUFBRTtZQUM3QixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDckIsT0FBTztTQUNWO1FBRUQsSUFBSSxrQkFBa0IsQ0FBQyxnQkFBZ0IsRUFBRTtZQUNyQyxPQUFPO1NBQ1Y7UUFFRCxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxFQUFFO1lBQ25CLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztTQUN4QjtRQUVELElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN4QixDQUFDOzs7Ozs7O0lBR08sZUFBZSxDQUFDLElBQVM7O1lBQ3pCLE1BQU0sR0FBRyxFQUFFO1FBRWYsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2IsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7U0FDeEI7YUFBTSxJQUFJLElBQUksQ0FBQyxjQUFjLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUU7WUFDMUQsTUFBTSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDO1NBQ3ZDOztjQUVLLGVBQWUsR0FBc0IsRUFBRTtRQUU3QyxLQUFLLE1BQU0sS0FBSyxJQUFJLE1BQU0sRUFBRTtZQUN4QixJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssY0FBYyxDQUFDLGFBQWEsRUFBRTtnQkFDN0MsNERBQTREO2dCQUM1RCxJQUFJLEtBQUssQ0FBQyxNQUFNLEVBQUU7OzBCQUNSLGFBQWEsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztvQkFDM0MsSUFBSSxhQUFhLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxhQUFhLEVBQUU7d0JBQ3JELGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxjQUFjLENBQUMsSUFBSSxjQUFjLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDN0U7aUJBQ0o7YUFDSjtpQkFBTTtnQkFDSCxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksY0FBYyxDQUFDLElBQUksY0FBYyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDN0U7U0FDSjtRQUVELE9BQU8sZUFBZSxDQUFDO0lBQzNCLENBQUM7Ozs7Ozs7O0lBSU8sUUFBUSxDQUFDLFVBQXNCO1FBQ25DLEtBQUssTUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRSxFQUFFO1lBQ3RDLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsRUFBRTtnQkFDdEIsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztnQkFDeEMsS0FBSyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUM5QztTQUNKO0lBQ0wsQ0FBQztDQUNKOzs7SUE1TEcsdUJBQW9COztJQUNwQix5QkFBc0I7O0lBQ3RCLDJCQUF3Qjs7SUFDeEIsNkJBQXNEOztJQUN0RCx3Q0FBNEI7O0lBRTVCLHlDQUE4Qzs7SUFDOUMsb0NBQW1FOztJQUNuRSxvQ0FBaUM7O0lBRWpDLHFDQUFzQjs7Ozs7SUFFd0UsZ0NBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IEZvcm1GaWVsZEV2ZW50IH0gZnJvbSAnLi8uLi8uLi8uLi9ldmVudHMvZm9ybS1maWVsZC5ldmVudCc7XHJcbmltcG9ydCB7IFZhbGlkYXRlRm9ybUZpZWxkRXZlbnQgfSBmcm9tICcuLy4uLy4uLy4uL2V2ZW50cy92YWxpZGF0ZS1mb3JtLWZpZWxkLmV2ZW50JztcclxuaW1wb3J0IHsgVmFsaWRhdGVGb3JtRXZlbnQgfSBmcm9tICcuLy4uLy4uLy4uL2V2ZW50cy92YWxpZGF0ZS1mb3JtLmV2ZW50JztcclxuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLy4uLy4uLy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XHJcbmltcG9ydCB7IENvbnRhaW5lck1vZGVsIH0gZnJvbSAnLi9jb250YWluZXIubW9kZWwnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRUZW1wbGF0ZXMgfSBmcm9tICcuL2Zvcm0tZmllbGQtdGVtcGxhdGVzJztcclxuaW1wb3J0IHsgRm9ybUZpZWxkVHlwZXMgfSBmcm9tICcuL2Zvcm0tZmllbGQtdHlwZXMnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRNb2RlbCB9IGZyb20gJy4vZm9ybS1maWVsZC5tb2RlbCc7XHJcbmltcG9ydCB7IEZvcm1PdXRjb21lTW9kZWwgfSBmcm9tICcuL2Zvcm0tb3V0Y29tZS5tb2RlbCc7XHJcbmltcG9ydCB7IEZvcm1WYWx1ZXMgfSBmcm9tICcuL2Zvcm0tdmFsdWVzJztcclxuaW1wb3J0IHsgRm9ybVdpZGdldE1vZGVsLCBGb3JtV2lkZ2V0TW9kZWxDYWNoZSB9IGZyb20gJy4vZm9ybS13aWRnZXQubW9kZWwnO1xyXG5pbXBvcnQgeyBUYWJNb2RlbCB9IGZyb20gJy4vdGFiLm1vZGVsJztcclxuXHJcbmltcG9ydCB7XHJcbiAgICBGT1JNX0ZJRUxEX1ZBTElEQVRPUlMsXHJcbiAgICBGb3JtRmllbGRWYWxpZGF0b3JcclxufSBmcm9tICcuL2Zvcm0tZmllbGQtdmFsaWRhdG9yJztcclxuaW1wb3J0IHsgRm9ybUJhc2VNb2RlbCB9IGZyb20gJy4uLy4uL2Zvcm0tYmFzZS5tb2RlbCc7XHJcblxyXG5leHBvcnQgY2xhc3MgRm9ybU1vZGVsIGV4dGVuZHMgRm9ybUJhc2VNb2RlbCB7XHJcblxyXG4gICAgcmVhZG9ubHkgaWQ6IG51bWJlcjtcclxuICAgIHJlYWRvbmx5IG5hbWU6IHN0cmluZztcclxuICAgIHJlYWRvbmx5IHRhc2tJZDogc3RyaW5nO1xyXG4gICAgcmVhZG9ubHkgdGFza05hbWU6IHN0cmluZyA9IEZvcm1Nb2RlbC5VTlNFVF9UQVNLX05BTUU7XHJcbiAgICBwcm9jZXNzRGVmaW5pdGlvbklkOiBzdHJpbmc7XHJcblxyXG4gICAgY3VzdG9tRmllbGRUZW1wbGF0ZXM6IEZvcm1GaWVsZFRlbXBsYXRlcyA9IHt9O1xyXG4gICAgZmllbGRWYWxpZGF0b3JzOiBGb3JtRmllbGRWYWxpZGF0b3JbXSA9IFsuLi5GT1JNX0ZJRUxEX1ZBTElEQVRPUlNdO1xyXG4gICAgcmVhZG9ubHkgc2VsZWN0ZWRPdXRjb21lOiBzdHJpbmc7XHJcblxyXG4gICAgcHJvY2Vzc1ZhcmlhYmxlczogYW55O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGZvcm1SZXByZXNlbnRhdGlvbkpTT04/OiBhbnksIGZvcm1WYWx1ZXM/OiBGb3JtVmFsdWVzLCByZWFkT25seTogYm9vbGVhbiA9IGZhbHNlLCBwcm90ZWN0ZWQgZm9ybVNlcnZpY2U/OiBGb3JtU2VydmljZSkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgdGhpcy5yZWFkT25seSA9IHJlYWRPbmx5O1xyXG5cclxuICAgICAgICBpZiAoZm9ybVJlcHJlc2VudGF0aW9uSlNPTikge1xyXG4gICAgICAgICAgICB0aGlzLmpzb24gPSBmb3JtUmVwcmVzZW50YXRpb25KU09OO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5pZCA9IGZvcm1SZXByZXNlbnRhdGlvbkpTT04uaWQ7XHJcbiAgICAgICAgICAgIHRoaXMubmFtZSA9IGZvcm1SZXByZXNlbnRhdGlvbkpTT04ubmFtZTtcclxuICAgICAgICAgICAgdGhpcy50YXNrSWQgPSBmb3JtUmVwcmVzZW50YXRpb25KU09OLnRhc2tJZDtcclxuICAgICAgICAgICAgdGhpcy50YXNrTmFtZSA9IGZvcm1SZXByZXNlbnRhdGlvbkpTT04udGFza05hbWUgfHwgZm9ybVJlcHJlc2VudGF0aW9uSlNPTi5uYW1lIHx8IEZvcm1Nb2RlbC5VTlNFVF9UQVNLX05BTUU7XHJcbiAgICAgICAgICAgIHRoaXMucHJvY2Vzc0RlZmluaXRpb25JZCA9IGZvcm1SZXByZXNlbnRhdGlvbkpTT04ucHJvY2Vzc0RlZmluaXRpb25JZDtcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21GaWVsZFRlbXBsYXRlcyA9IGZvcm1SZXByZXNlbnRhdGlvbkpTT04uY3VzdG9tRmllbGRUZW1wbGF0ZXMgfHwge307XHJcbiAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRPdXRjb21lID0gZm9ybVJlcHJlc2VudGF0aW9uSlNPTi5zZWxlY3RlZE91dGNvbWUgfHwge307XHJcbiAgICAgICAgICAgIHRoaXMuY2xhc3NOYW1lID0gZm9ybVJlcHJlc2VudGF0aW9uSlNPTi5jbGFzc05hbWUgfHwgJyc7XHJcblxyXG4gICAgICAgICAgICBjb25zdCB0YWJDYWNoZTogRm9ybVdpZGdldE1vZGVsQ2FjaGU8VGFiTW9kZWw+ID0ge307XHJcblxyXG4gICAgICAgICAgICB0aGlzLnByb2Nlc3NWYXJpYWJsZXMgPSBmb3JtUmVwcmVzZW50YXRpb25KU09OLnByb2Nlc3NWYXJpYWJsZXM7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnRhYnMgPSAoZm9ybVJlcHJlc2VudGF0aW9uSlNPTi50YWJzIHx8IFtdKS5tYXAoKHQpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG1vZGVsID0gbmV3IFRhYk1vZGVsKHRoaXMsIHQpO1xyXG4gICAgICAgICAgICAgICAgdGFiQ2FjaGVbbW9kZWwuaWRdID0gbW9kZWw7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbW9kZWw7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5maWVsZHMgPSB0aGlzLnBhcnNlUm9vdEZpZWxkcyhmb3JtUmVwcmVzZW50YXRpb25KU09OKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChmb3JtVmFsdWVzKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWREYXRhKGZvcm1WYWx1ZXMpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuZmllbGRzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBmaWVsZCA9IHRoaXMuZmllbGRzW2ldO1xyXG4gICAgICAgICAgICAgICAgaWYgKGZpZWxkLnRhYikge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHRhYiA9IHRhYkNhY2hlW2ZpZWxkLnRhYl07XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRhYikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YWIuZmllbGRzLnB1c2goZmllbGQpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGZvcm1SZXByZXNlbnRhdGlvbkpTT04uZmllbGRzKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzYXZlT3V0Y29tZSA9IG5ldyBGb3JtT3V0Y29tZU1vZGVsKHRoaXMsIHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogRm9ybU1vZGVsLlNBVkVfT1VUQ09NRSxcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiAnU0FWRScsXHJcbiAgICAgICAgICAgICAgICAgICAgaXNTeXN0ZW06IHRydWVcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY29tcGxldGVPdXRjb21lID0gbmV3IEZvcm1PdXRjb21lTW9kZWwodGhpcywge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiBGb3JtTW9kZWwuQ09NUExFVEVfT1VUQ09NRSxcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiAnQ09NUExFVEUnLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzU3lzdGVtOiB0cnVlXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHN0YXJ0UHJvY2Vzc091dGNvbWUgPSBuZXcgRm9ybU91dGNvbWVNb2RlbCh0aGlzLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWQ6IEZvcm1Nb2RlbC5TVEFSVF9QUk9DRVNTX09VVENPTUUsXHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogJ1NUQVJUIFBST0NFU1MnLFxyXG4gICAgICAgICAgICAgICAgICAgIGlzU3lzdGVtOiB0cnVlXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICBjb25zdCBjdXN0b21PdXRjb21lcyA9IChmb3JtUmVwcmVzZW50YXRpb25KU09OLm91dGNvbWVzIHx8IFtdKS5tYXAoKG9iaikgPT4gbmV3IEZvcm1PdXRjb21lTW9kZWwodGhpcywgb2JqKSk7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5vdXRjb21lcyA9IFtzYXZlT3V0Y29tZV0uY29uY2F0KFxyXG4gICAgICAgICAgICAgICAgICAgIGN1c3RvbU91dGNvbWVzLmxlbmd0aCA+IDAgPyBjdXN0b21PdXRjb21lcyA6IFtjb21wbGV0ZU91dGNvbWUsIHN0YXJ0UHJvY2Vzc091dGNvbWVdXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnZhbGlkYXRlRm9ybSgpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRm9ybUZpZWxkQ2hhbmdlZChmaWVsZDogRm9ybUZpZWxkTW9kZWwpIHtcclxuICAgICAgICB0aGlzLnZhbGlkYXRlRmllbGQoZmllbGQpO1xyXG4gICAgICAgIGlmICh0aGlzLmZvcm1TZXJ2aWNlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZm9ybVNlcnZpY2UuZm9ybUZpZWxkVmFsdWVDaGFuZ2VkLm5leHQobmV3IEZvcm1GaWVsZEV2ZW50KHRoaXMsIGZpZWxkKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVmFsaWRhdGVzIGVudGlyZSBmb3JtIGFuZCBhbGwgZm9ybSBmaWVsZHMuXHJcbiAgICAgKlxyXG4gICAgICogQG1lbWJlcm9mIEZvcm1Nb2RlbFxyXG4gICAgICovXHJcbiAgICB2YWxpZGF0ZUZvcm0oKTogdm9pZCB7XHJcbiAgICAgICAgY29uc3QgdmFsaWRhdGVGb3JtRXZlbnQ6IGFueSA9IG5ldyBWYWxpZGF0ZUZvcm1FdmVudCh0aGlzKTtcclxuXHJcbiAgICAgICAgY29uc3QgZXJyb3JzRmllbGQ6IEZvcm1GaWVsZE1vZGVsW10gPSBbXTtcclxuXHJcbiAgICAgICAgY29uc3QgZmllbGRzID0gdGhpcy5nZXRGb3JtRmllbGRzKCk7XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBmaWVsZHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgaWYgKCFmaWVsZHNbaV0udmFsaWRhdGUoKSkge1xyXG4gICAgICAgICAgICAgICAgZXJyb3JzRmllbGQucHVzaChmaWVsZHNbaV0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLmlzVmFsaWQgPSBlcnJvcnNGaWVsZC5sZW5ndGggPiAwID8gZmFsc2UgOiB0cnVlO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5mb3JtU2VydmljZSkge1xyXG4gICAgICAgICAgICB2YWxpZGF0ZUZvcm1FdmVudC5pc1ZhbGlkID0gdGhpcy5pc1ZhbGlkO1xyXG4gICAgICAgICAgICB2YWxpZGF0ZUZvcm1FdmVudC5lcnJvcnNGaWVsZCA9IGVycm9yc0ZpZWxkO1xyXG4gICAgICAgICAgICB0aGlzLmZvcm1TZXJ2aWNlLnZhbGlkYXRlRm9ybS5uZXh0KHZhbGlkYXRlRm9ybUV2ZW50KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVmFsaWRhdGVzIGEgc3BlY2lmaWMgZm9ybSBmaWVsZCwgdHJpZ2dlcnMgZm9ybSB2YWxpZGF0aW9uLlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBmaWVsZCBGb3JtIGZpZWxkIHRvIHZhbGlkYXRlLlxyXG4gICAgICogQG1lbWJlcm9mIEZvcm1Nb2RlbFxyXG4gICAgICovXHJcbiAgICB2YWxpZGF0ZUZpZWxkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IHZvaWQge1xyXG4gICAgICAgIGlmICghZmllbGQpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgdmFsaWRhdGVGaWVsZEV2ZW50ID0gbmV3IFZhbGlkYXRlRm9ybUZpZWxkRXZlbnQodGhpcywgZmllbGQpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5mb3JtU2VydmljZSkge1xyXG4gICAgICAgICAgICB0aGlzLmZvcm1TZXJ2aWNlLnZhbGlkYXRlRm9ybUZpZWxkLm5leHQodmFsaWRhdGVGaWVsZEV2ZW50KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghdmFsaWRhdGVGaWVsZEV2ZW50LmlzVmFsaWQpIHtcclxuICAgICAgICAgICAgdGhpcy5tYXJrQXNJbnZhbGlkKCk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh2YWxpZGF0ZUZpZWxkRXZlbnQuZGVmYXVsdFByZXZlbnRlZCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIWZpZWxkLnZhbGlkYXRlKCkpIHtcclxuICAgICAgICAgICAgdGhpcy5tYXJrQXNJbnZhbGlkKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnZhbGlkYXRlRm9ybSgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIEFjdGl2aXRpIHN1cHBvcnRzIDMgdHlwZXMgb2Ygcm9vdCBmaWVsZHM6IGNvbnRhaW5lcnxncm91cHxkeW5hbWljLXRhYmxlXHJcbiAgICBwcml2YXRlIHBhcnNlUm9vdEZpZWxkcyhqc29uOiBhbnkpOiBGb3JtV2lkZ2V0TW9kZWxbXSB7XHJcbiAgICAgICAgbGV0IGZpZWxkcyA9IFtdO1xyXG5cclxuICAgICAgICBpZiAoanNvbi5maWVsZHMpIHtcclxuICAgICAgICAgICAgZmllbGRzID0ganNvbi5maWVsZHM7XHJcbiAgICAgICAgfSBlbHNlIGlmIChqc29uLmZvcm1EZWZpbml0aW9uICYmIGpzb24uZm9ybURlZmluaXRpb24uZmllbGRzKSB7XHJcbiAgICAgICAgICAgIGZpZWxkcyA9IGpzb24uZm9ybURlZmluaXRpb24uZmllbGRzO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgZm9ybVdpZGdldE1vZGVsOiBGb3JtV2lkZ2V0TW9kZWxbXSA9IFtdO1xyXG5cclxuICAgICAgICBmb3IgKGNvbnN0IGZpZWxkIG9mIGZpZWxkcykge1xyXG4gICAgICAgICAgICBpZiAoZmllbGQudHlwZSA9PT0gRm9ybUZpZWxkVHlwZXMuRElTUExBWV9WQUxVRSkge1xyXG4gICAgICAgICAgICAgICAgLy8gd29ya2Fyb3VuZCBmb3IgZHluYW1pYyB0YWJsZSBvbiBhIGNvbXBsZXRlZC9yZWFkb25seSBmb3JtXHJcbiAgICAgICAgICAgICAgICBpZiAoZmllbGQucGFyYW1zKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgb3JpZ2luYWxGaWVsZCA9IGZpZWxkLnBhcmFtc1snZmllbGQnXTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAob3JpZ2luYWxGaWVsZC50eXBlID09PSBGb3JtRmllbGRUeXBlcy5EWU5BTUlDX1RBQkxFKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZvcm1XaWRnZXRNb2RlbC5wdXNoKG5ldyBDb250YWluZXJNb2RlbChuZXcgRm9ybUZpZWxkTW9kZWwodGhpcywgZmllbGQpKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgZm9ybVdpZGdldE1vZGVsLnB1c2gobmV3IENvbnRhaW5lck1vZGVsKG5ldyBGb3JtRmllbGRNb2RlbCh0aGlzLCBmaWVsZCkpKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZvcm1XaWRnZXRNb2RlbDtcclxuICAgIH1cclxuXHJcbiAgICAvLyBMb2FkcyBleHRlcm5hbCBkYXRhIGFuZCBvdmVycmlkZXMgZmllbGQgdmFsdWVzXHJcbiAgICAvLyBUeXBpY2FsbHkgdXNlZCB3aGVuIGZvcm0gZGVmaW5pdGlvbiBhbmQgZm9ybSBkYXRhIGNvbWluZyBmcm9tIGRpZmZlcmVudCBzb3VyY2VzXHJcbiAgICBwcml2YXRlIGxvYWREYXRhKGZvcm1WYWx1ZXM6IEZvcm1WYWx1ZXMpIHtcclxuICAgICAgICBmb3IgKGNvbnN0IGZpZWxkIG9mIHRoaXMuZ2V0Rm9ybUZpZWxkcygpKSB7XHJcbiAgICAgICAgICAgIGlmIChmb3JtVmFsdWVzW2ZpZWxkLmlkXSkge1xyXG4gICAgICAgICAgICAgICAgZmllbGQuanNvbi52YWx1ZSA9IGZvcm1WYWx1ZXNbZmllbGQuaWRdO1xyXG4gICAgICAgICAgICAgICAgZmllbGQudmFsdWUgPSBmaWVsZC5wYXJzZVZhbHVlKGZpZWxkLmpzb24pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==