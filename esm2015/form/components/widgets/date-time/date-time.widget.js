/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, ViewEncapsulation } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { DatetimeAdapter, MAT_DATETIME_FORMATS } from '@mat-datetimepicker/core';
import { MomentDatetimeAdapter, MAT_MOMENT_DATETIME_FORMATS } from '@mat-datetimepicker/moment';
import moment from 'moment-es6';
import { UserPreferencesService, UserPreferenceValues } from '../../../../services/user-preferences.service';
import { MomentDateAdapter } from '../../../../utils/momentDateAdapter';
import { MOMENT_DATE_FORMATS } from '../../../../utils/moment-date-formats.model';
import { FormService } from './../../../services/form.service';
import { WidgetComponent } from './../widget.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
const ɵ0 = MOMENT_DATE_FORMATS, ɵ1 = MAT_MOMENT_DATETIME_FORMATS;
export class DateTimeWidgetComponent extends WidgetComponent {
    /**
     * @param {?} formService
     * @param {?} dateAdapter
     * @param {?} userPreferencesService
     */
    constructor(formService, dateAdapter, userPreferencesService) {
        super(formService);
        this.formService = formService;
        this.dateAdapter = dateAdapter;
        this.userPreferencesService = userPreferencesService;
        this.onDestroy$ = new Subject();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.userPreferencesService
            .select(UserPreferenceValues.Locale)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} locale
         * @return {?}
         */
        locale => this.dateAdapter.setLocale(locale)));
        /** @type {?} */
        const momentDateAdapter = (/** @type {?} */ (this.dateAdapter));
        momentDateAdapter.overrideDisplayFormat = this.field.dateDisplayFormat;
        if (this.field) {
            if (this.field.minValue) {
                this.minDate = moment(this.field.minValue, 'YYYY-MM-DDTHH:mm:ssZ');
            }
            if (this.field.maxValue) {
                this.maxDate = moment(this.field.maxValue, 'YYYY-MM-DDTHH:mm:ssZ');
            }
        }
        this.displayDate = moment(this.field.value);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    }
    /**
     * @param {?} newDateValue
     * @return {?}
     */
    onDateChanged(newDateValue) {
        if (newDateValue && newDateValue.value) {
            this.field.value = newDateValue.value.format(this.field.dateDisplayFormat);
        }
        else if (newDateValue) {
            this.field.value = newDateValue;
        }
        else {
            this.field.value = null;
        }
        this.onFieldChanged(this.field);
    }
}
DateTimeWidgetComponent.decorators = [
    { type: Component, args: [{
                providers: [
                    { provide: DateAdapter, useClass: MomentDateAdapter },
                    { provide: MAT_DATE_FORMATS, useValue: ɵ0 },
                    { provide: DatetimeAdapter, useClass: MomentDatetimeAdapter },
                    { provide: MAT_DATETIME_FORMATS, useValue: ɵ1 }
                ],
                selector: 'date-time-widget',
                template: "<div class=\"{{field.className}}\"\r\n     id=\"data-time-widget\" [class.adf-invalid]=\"!field.isValid\">\r\n    <mat-form-field class=\"adf-date-time-widget\">\r\n        <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }} ({{field.dateDisplayFormat}})<span *ngIf=\"isRequired()\">*</span></label>\r\n        <input matInput\r\n               [matDatetimepicker]=\"datetimePicker\"\r\n               [id]=\"field.id\"\r\n               [(ngModel)]=\"displayDate\"\r\n               [required]=\"isRequired()\"\r\n               [disabled]=\"field.readOnly\"\r\n               [min]=\"minDate\"\r\n               [max]=\"maxDate\"\r\n               (focusout)=\"onDateChanged($event.srcElement.value)\"\r\n               (dateChange)=\"onDateChanged($event)\"\r\n               placeholder=\"{{field.placeholder}}\">\r\n        <mat-datetimepicker-toggle matSuffix [for]=\"datetimePicker\" [disabled]=\"field.readOnly\"></mat-datetimepicker-toggle>\r\n    </mat-form-field>\r\n    <error-widget [error]=\"field.validationSummary\"></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n    <mat-datetimepicker #datetimePicker type=\"datetime\" openOnFocus=\"true\" timeInterval=\"5\"></mat-datetimepicker>\r\n</div>\r\n",
                encapsulation: ViewEncapsulation.None,
                styles: [".adf-date-time-widget .mat-form-field-suffix{text-align:right;position:absolute;margin-top:30px;width:100%}@media screen and (-ms-high-contrast:active),screen and (-ms-high-contrast:none){.adf-date-widget .mat-form-field-suffix{position:relative;width:auto}}"]
            }] }
];
/** @nocollapse */
DateTimeWidgetComponent.ctorParameters = () => [
    { type: FormService },
    { type: DateAdapter },
    { type: UserPreferencesService }
];
if (false) {
    /** @type {?} */
    DateTimeWidgetComponent.prototype.minDate;
    /** @type {?} */
    DateTimeWidgetComponent.prototype.maxDate;
    /** @type {?} */
    DateTimeWidgetComponent.prototype.displayDate;
    /**
     * @type {?}
     * @private
     */
    DateTimeWidgetComponent.prototype.onDestroy$;
    /** @type {?} */
    DateTimeWidgetComponent.prototype.formService;
    /**
     * @type {?}
     * @private
     */
    DateTimeWidgetComponent.prototype.dateAdapter;
    /**
     * @type {?}
     * @private
     */
    DateTimeWidgetComponent.prototype.userPreferencesService;
}
export { ɵ0, ɵ1 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS10aW1lLndpZGdldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2RhdGUtdGltZS9kYXRlLXRpbWUud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsU0FBUyxFQUFVLGlCQUFpQixFQUFhLE1BQU0sZUFBZSxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxXQUFXLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRSxPQUFPLEVBQUUsZUFBZSxFQUFFLG9CQUFvQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDakYsT0FBTyxFQUFFLHFCQUFxQixFQUFFLDJCQUEyQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDaEcsT0FBTyxNQUFNLE1BQU0sWUFBWSxDQUFDO0FBRWhDLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQzdHLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUMvRCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDeEQsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUMvQixPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7V0FLSSxtQkFBbUIsT0FFZiwyQkFBMkI7QUFPOUUsTUFBTSxPQUFPLHVCQUF3QixTQUFRLGVBQWU7Ozs7OztJQVF4RCxZQUFtQixXQUF3QixFQUN2QixXQUFnQyxFQUNoQyxzQkFBOEM7UUFDOUQsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBSEosZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDdkIsZ0JBQVcsR0FBWCxXQUFXLENBQXFCO1FBQ2hDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFKMUQsZUFBVSxHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7SUFNNUMsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsc0JBQXNCO2FBQ3RCLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUM7YUFDbkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDaEMsU0FBUzs7OztRQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUMsQ0FBQzs7Y0FFdkQsaUJBQWlCLEdBQUcsbUJBQW9CLElBQUksQ0FBQyxXQUFXLEVBQUE7UUFDOUQsaUJBQWlCLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQztRQUV2RSxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDWixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFO2dCQUNyQixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO2FBQ3RFO1lBRUQsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRTtnQkFDckIsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsc0JBQXNCLENBQUMsQ0FBQzthQUN0RTtTQUNKO1FBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoRCxDQUFDOzs7O0lBRUQsV0FBVztRQUNQLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDL0IsQ0FBQzs7Ozs7SUFFRCxhQUFhLENBQUMsWUFBWTtRQUN0QixJQUFJLFlBQVksSUFBSSxZQUFZLENBQUMsS0FBSyxFQUFFO1lBQ3BDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQztTQUM5RTthQUFNLElBQUksWUFBWSxFQUFFO1lBQ3JCLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLFlBQVksQ0FBQztTQUNuQzthQUFNO1lBQ0gsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1NBQzNCO1FBQ0QsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDcEMsQ0FBQzs7O1lBN0RKLFNBQVMsU0FBQztnQkFDUCxTQUFTLEVBQUU7b0JBQ1AsRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFBRSxpQkFBaUIsRUFBRTtvQkFDckQsRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsUUFBUSxJQUFxQixFQUFFO29CQUM1RCxFQUFFLE9BQU8sRUFBRSxlQUFlLEVBQUUsUUFBUSxFQUFFLHFCQUFxQixFQUFFO29CQUM3RCxFQUFFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxRQUFRLElBQTZCLEVBQUU7aUJBQzNFO2dCQUNELFFBQVEsRUFBRSxrQkFBa0I7Z0JBQzVCLG8wQ0FBc0M7Z0JBRXRDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOzthQUN4Qzs7OztZQWhCUSxXQUFXO1lBUlgsV0FBVztZQUtYLHNCQUFzQjs7OztJQXNCM0IsMENBQWdCOztJQUNoQiwwQ0FBZ0I7O0lBQ2hCLDhDQUFvQjs7Ozs7SUFFcEIsNkNBQTRDOztJQUVoQyw4Q0FBK0I7Ozs7O0lBQy9CLDhDQUF3Qzs7Ozs7SUFDeEMseURBQXNEIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbiAvKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0VuY2Fwc3VsYXRpb24sIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEYXRlQWRhcHRlciwgTUFUX0RBVEVfRk9STUFUUyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgRGF0ZXRpbWVBZGFwdGVyLCBNQVRfREFURVRJTUVfRk9STUFUUyB9IGZyb20gJ0BtYXQtZGF0ZXRpbWVwaWNrZXIvY29yZSc7XHJcbmltcG9ydCB7IE1vbWVudERhdGV0aW1lQWRhcHRlciwgTUFUX01PTUVOVF9EQVRFVElNRV9GT1JNQVRTIH0gZnJvbSAnQG1hdC1kYXRldGltZXBpY2tlci9tb21lbnQnO1xyXG5pbXBvcnQgbW9tZW50IGZyb20gJ21vbWVudC1lczYnO1xyXG5pbXBvcnQgeyBNb21lbnQgfSBmcm9tICdtb21lbnQnO1xyXG5pbXBvcnQgeyBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlLCBVc2VyUHJlZmVyZW5jZVZhbHVlcyB9IGZyb20gJy4uLy4uLy4uLy4uL3NlcnZpY2VzL3VzZXItcHJlZmVyZW5jZXMuc2VydmljZSc7XHJcbmltcG9ydCB7IE1vbWVudERhdGVBZGFwdGVyIH0gZnJvbSAnLi4vLi4vLi4vLi4vdXRpbHMvbW9tZW50RGF0ZUFkYXB0ZXInO1xyXG5pbXBvcnQgeyBNT01FTlRfREFURV9GT1JNQVRTIH0gZnJvbSAnLi4vLi4vLi4vLi4vdXRpbHMvbW9tZW50LWRhdGUtZm9ybWF0cy5tb2RlbCc7XHJcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi8uLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBXaWRnZXRDb21wb25lbnQgfSBmcm9tICcuLy4uL3dpZGdldC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgeyBwcm92aWRlOiBEYXRlQWRhcHRlciwgdXNlQ2xhc3M6IE1vbWVudERhdGVBZGFwdGVyIH0sXHJcbiAgICAgICAgeyBwcm92aWRlOiBNQVRfREFURV9GT1JNQVRTLCB1c2VWYWx1ZTogTU9NRU5UX0RBVEVfRk9STUFUUyB9LFxyXG4gICAgICAgIHsgcHJvdmlkZTogRGF0ZXRpbWVBZGFwdGVyLCB1c2VDbGFzczogTW9tZW50RGF0ZXRpbWVBZGFwdGVyIH0sXHJcbiAgICAgICAgeyBwcm92aWRlOiBNQVRfREFURVRJTUVfRk9STUFUUywgdXNlVmFsdWU6IE1BVF9NT01FTlRfREFURVRJTUVfRk9STUFUUyB9XHJcbiAgICBdLFxyXG4gICAgc2VsZWN0b3I6ICdkYXRlLXRpbWUtd2lkZ2V0JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9kYXRlLXRpbWUud2lkZ2V0Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vZGF0ZS10aW1lLndpZGdldC5zY3NzJ10sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEYXRlVGltZVdpZGdldENvbXBvbmVudCBleHRlbmRzIFdpZGdldENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuXHJcbiAgICBtaW5EYXRlOiBNb21lbnQ7XHJcbiAgICBtYXhEYXRlOiBNb21lbnQ7XHJcbiAgICBkaXNwbGF5RGF0ZTogTW9tZW50O1xyXG5cclxuICAgIHByaXZhdGUgb25EZXN0cm95JCA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgZGF0ZUFkYXB0ZXI6IERhdGVBZGFwdGVyPE1vbWVudD4sXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHVzZXJQcmVmZXJlbmNlc1NlcnZpY2U6IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UpIHtcclxuICAgICAgICBzdXBlcihmb3JtU2VydmljZSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZXNTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5zZWxlY3QoVXNlclByZWZlcmVuY2VWYWx1ZXMuTG9jYWxlKVxyXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5vbkRlc3Ryb3kkKSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZShsb2NhbGUgPT4gdGhpcy5kYXRlQWRhcHRlci5zZXRMb2NhbGUobG9jYWxlKSk7XHJcblxyXG4gICAgICAgIGNvbnN0IG1vbWVudERhdGVBZGFwdGVyID0gPE1vbWVudERhdGVBZGFwdGVyPiB0aGlzLmRhdGVBZGFwdGVyO1xyXG4gICAgICAgIG1vbWVudERhdGVBZGFwdGVyLm92ZXJyaWRlRGlzcGxheUZvcm1hdCA9IHRoaXMuZmllbGQuZGF0ZURpc3BsYXlGb3JtYXQ7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmZpZWxkKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmZpZWxkLm1pblZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1pbkRhdGUgPSBtb21lbnQodGhpcy5maWVsZC5taW5WYWx1ZSwgJ1lZWVktTU0tRERUSEg6bW06c3NaJyk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmZpZWxkLm1heFZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1heERhdGUgPSBtb21lbnQodGhpcy5maWVsZC5tYXhWYWx1ZSwgJ1lZWVktTU0tRERUSEg6bW06c3NaJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5kaXNwbGF5RGF0ZSA9IG1vbWVudCh0aGlzLmZpZWxkLnZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQubmV4dCh0cnVlKTtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQuY29tcGxldGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRhdGVDaGFuZ2VkKG5ld0RhdGVWYWx1ZSkge1xyXG4gICAgICAgIGlmIChuZXdEYXRlVmFsdWUgJiYgbmV3RGF0ZVZhbHVlLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZmllbGQudmFsdWUgPSBuZXdEYXRlVmFsdWUudmFsdWUuZm9ybWF0KHRoaXMuZmllbGQuZGF0ZURpc3BsYXlGb3JtYXQpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAobmV3RGF0ZVZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZmllbGQudmFsdWUgPSBuZXdEYXRlVmFsdWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5maWVsZC52YWx1ZSA9IG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMub25GaWVsZENoYW5nZWQodGhpcy5maWVsZCk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==