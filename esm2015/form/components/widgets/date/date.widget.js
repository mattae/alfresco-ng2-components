/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { UserPreferencesService, UserPreferenceValues } from '../../../../services/user-preferences.service';
import { MomentDateAdapter } from '../../../../utils/momentDateAdapter';
import { MOMENT_DATE_FORMATS } from '../../../../utils/moment-date-formats.model';
import { Component, ViewEncapsulation } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import moment from 'moment-es6';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
const ɵ0 = MOMENT_DATE_FORMATS;
export class DateWidgetComponent extends WidgetComponent {
    /**
     * @param {?} formService
     * @param {?} dateAdapter
     * @param {?} userPreferencesService
     */
    constructor(formService, dateAdapter, userPreferencesService) {
        super(formService);
        this.formService = formService;
        this.dateAdapter = dateAdapter;
        this.userPreferencesService = userPreferencesService;
        this.DATE_FORMAT = 'DD/MM/YYYY';
        this.onDestroy$ = new Subject();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.userPreferencesService
            .select(UserPreferenceValues.Locale)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} locale
         * @return {?}
         */
        locale => this.dateAdapter.setLocale(locale)));
        /** @type {?} */
        const momentDateAdapter = (/** @type {?} */ (this.dateAdapter));
        momentDateAdapter.overrideDisplayFormat = this.field.dateDisplayFormat;
        if (this.field) {
            if (this.field.minValue) {
                this.minDate = moment(this.field.minValue, this.DATE_FORMAT);
            }
            if (this.field.maxValue) {
                this.maxDate = moment(this.field.maxValue, this.DATE_FORMAT);
            }
        }
        this.displayDate = moment(this.field.value);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    }
    /**
     * @param {?} newDateValue
     * @return {?}
     */
    onDateChanged(newDateValue) {
        if (newDateValue && newDateValue.value) {
            this.field.value = newDateValue.value.format(this.field.dateDisplayFormat);
        }
        else if (newDateValue) {
            this.field.value = newDateValue;
        }
        else {
            this.field.value = null;
        }
        this.onFieldChanged(this.field);
    }
}
DateWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'date-widget',
                providers: [
                    { provide: DateAdapter, useClass: MomentDateAdapter },
                    { provide: MAT_DATE_FORMATS, useValue: ɵ0 }
                ],
                template: "<div class=\"{{field.className}}\" id=\"data-widget\" [class.adf-invalid]=\"!field.isValid\">\r\n    <mat-form-field class=\"adf-date-widget\">\r\n        <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }} ({{field.dateDisplayFormat}})<span *ngIf=\"isRequired()\">*</span></label>\r\n        <input matInput\r\n               [id]=\"field.id\"\r\n               [matDatepicker]=\"datePicker\"\r\n               [(ngModel)]=\"displayDate\"\r\n               [required]=\"isRequired()\"\r\n               [disabled]=\"field.readOnly\"\r\n               [min]=\"minDate\"\r\n               [max]=\"maxDate\"\r\n               (focusout)=\"onDateChanged($event.srcElement.value)\"\r\n               (dateChange)=\"onDateChanged($event)\"\r\n               placeholder=\"{{field.placeholder}}\">\r\n        <mat-datepicker-toggle  matSuffix [for]=\"datePicker\" [disabled]=\"field.readOnly\" ></mat-datepicker-toggle>\r\n    </mat-form-field>\r\n    <error-widget [error]=\"field.validationSummary\"></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n    <mat-datepicker #datePicker [touchUi]=\"true\" [startAt]=\"displayDate\" ></mat-datepicker>\r\n</div>\r\n\r\n",
                host: baseHost,
                encapsulation: ViewEncapsulation.None,
                styles: [".adf-date-widget .mat-form-field-suffix{text-align:right;position:absolute;margin-top:30px;width:100%}.adf-date-widget-date-widget-button{position:relative;float:right}.adf-date-widget-date-input{padding-top:5px;padding-bottom:5px}.adf-date-widget-grid-date-widget{align-items:center;padding:0}.adf-date-widget-date-widget-button__cell{margin-top:0;margin-bottom:0}@media screen and (-ms-high-contrast:active),screen and (-ms-high-contrast:none){.adf-date-widget .mat-form-field-suffix{position:relative;width:auto}}"]
            }] }
];
/** @nocollapse */
DateWidgetComponent.ctorParameters = () => [
    { type: FormService },
    { type: DateAdapter },
    { type: UserPreferencesService }
];
if (false) {
    /** @type {?} */
    DateWidgetComponent.prototype.DATE_FORMAT;
    /** @type {?} */
    DateWidgetComponent.prototype.minDate;
    /** @type {?} */
    DateWidgetComponent.prototype.maxDate;
    /** @type {?} */
    DateWidgetComponent.prototype.displayDate;
    /**
     * @type {?}
     * @private
     */
    DateWidgetComponent.prototype.onDestroy$;
    /** @type {?} */
    DateWidgetComponent.prototype.formService;
    /**
     * @type {?}
     * @private
     */
    DateWidgetComponent.prototype.dateAdapter;
    /**
     * @type {?}
     * @private
     */
    DateWidgetComponent.prototype.userPreferencesService;
}
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS53aWRnZXQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9kYXRlL2RhdGUud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUM3RyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUN4RSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUNsRixPQUFPLEVBQUUsU0FBUyxFQUFVLGlCQUFpQixFQUFhLE1BQU0sZUFBZSxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxXQUFXLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRSxPQUFPLE1BQU0sTUFBTSxZQUFZLENBQUM7QUFFaEMsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxRQUFRLEVBQUUsZUFBZSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDbEUsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUMvQixPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7V0FNSSxtQkFBbUI7QUFNbEUsTUFBTSxPQUFPLG1CQUFvQixTQUFRLGVBQWU7Ozs7OztJQVVwRCxZQUFtQixXQUF3QixFQUN2QixXQUFnQyxFQUNoQyxzQkFBOEM7UUFDOUQsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBSEosZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDdkIsZ0JBQVcsR0FBWCxXQUFXLENBQXFCO1FBQ2hDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFWbEUsZ0JBQVcsR0FBRyxZQUFZLENBQUM7UUFNbkIsZUFBVSxHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7SUFNNUMsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsc0JBQXNCO2FBQ3RCLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUM7YUFDbkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDaEMsU0FBUzs7OztRQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQUMsQ0FBQzs7Y0FFdkQsaUJBQWlCLEdBQUcsbUJBQW9CLElBQUksQ0FBQyxXQUFXLEVBQUE7UUFDOUQsaUJBQWlCLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQztRQUV2RSxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDWixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFO2dCQUNyQixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDaEU7WUFFRCxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFO2dCQUNyQixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDaEU7U0FDSjtRQUNELElBQUksQ0FBQyxXQUFXLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDaEQsQ0FBQzs7OztJQUVELFdBQVc7UUFDUCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQy9CLENBQUM7Ozs7O0lBRUQsYUFBYSxDQUFDLFlBQVk7UUFDdEIsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLEtBQUssRUFBRTtZQUNwQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxZQUFZLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7U0FDOUU7YUFBTSxJQUFJLFlBQVksRUFBRTtZQUNyQixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxZQUFZLENBQUM7U0FDbkM7YUFBTTtZQUNILElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztTQUMzQjtRQUNELElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3BDLENBQUM7OztZQTdESixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGFBQWE7Z0JBQ3ZCLFNBQVMsRUFBRTtvQkFDUCxFQUFFLE9BQU8sRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLGlCQUFpQixFQUFFO29CQUNyRCxFQUFFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLElBQXFCLEVBQUU7aUJBQUM7Z0JBQ2pFLDR3Q0FBaUM7Z0JBRWpDLElBQUksRUFBRSxRQUFRO2dCQUNkLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOzthQUN4Qzs7OztZQWRRLFdBQVc7WUFIWCxXQUFXO1lBSlgsc0JBQXNCOzs7O0lBd0IzQiwwQ0FBMkI7O0lBRTNCLHNDQUFnQjs7SUFDaEIsc0NBQWdCOztJQUNoQiwwQ0FBb0I7Ozs7O0lBRXBCLHlDQUE0Qzs7SUFFaEMsMENBQStCOzs7OztJQUMvQiwwQ0FBd0M7Ozs7O0lBQ3hDLHFEQUFzRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4vKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgeyBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlLCBVc2VyUHJlZmVyZW5jZVZhbHVlcyB9IGZyb20gJy4uLy4uLy4uLy4uL3NlcnZpY2VzL3VzZXItcHJlZmVyZW5jZXMuc2VydmljZSc7XHJcbmltcG9ydCB7IE1vbWVudERhdGVBZGFwdGVyIH0gZnJvbSAnLi4vLi4vLi4vLi4vdXRpbHMvbW9tZW50RGF0ZUFkYXB0ZXInO1xyXG5pbXBvcnQgeyBNT01FTlRfREFURV9GT1JNQVRTIH0gZnJvbSAnLi4vLi4vLi4vLi4vdXRpbHMvbW9tZW50LWRhdGUtZm9ybWF0cy5tb2RlbCc7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERhdGVBZGFwdGVyLCBNQVRfREFURV9GT1JNQVRTIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgbW9tZW50IGZyb20gJ21vbWVudC1lczYnO1xyXG5pbXBvcnQgeyBNb21lbnQgfSBmcm9tICdtb21lbnQnO1xyXG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4vLi4vLi4vLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgYmFzZUhvc3QsIFdpZGdldENvbXBvbmVudCB9IGZyb20gJy4vLi4vd2lkZ2V0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2RhdGUtd2lkZ2V0JyxcclxuICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIHsgcHJvdmlkZTogRGF0ZUFkYXB0ZXIsIHVzZUNsYXNzOiBNb21lbnREYXRlQWRhcHRlciB9LFxyXG4gICAgICAgIHsgcHJvdmlkZTogTUFUX0RBVEVfRk9STUFUUywgdXNlVmFsdWU6IE1PTUVOVF9EQVRFX0ZPUk1BVFMgfV0sXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vZGF0ZS53aWRnZXQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9kYXRlLndpZGdldC5zY3NzJ10sXHJcbiAgICBob3N0OiBiYXNlSG9zdCxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIERhdGVXaWRnZXRDb21wb25lbnQgZXh0ZW5kcyBXaWRnZXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcblxyXG4gICAgREFURV9GT1JNQVQgPSAnREQvTU0vWVlZWSc7XHJcblxyXG4gICAgbWluRGF0ZTogTW9tZW50O1xyXG4gICAgbWF4RGF0ZTogTW9tZW50O1xyXG4gICAgZGlzcGxheURhdGU6IE1vbWVudDtcclxuXHJcbiAgICBwcml2YXRlIG9uRGVzdHJveSQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBmb3JtU2VydmljZTogRm9ybVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGRhdGVBZGFwdGVyOiBEYXRlQWRhcHRlcjxNb21lbnQ+LFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSB1c2VyUHJlZmVyZW5jZXNTZXJ2aWNlOiBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlKSB7XHJcbiAgICAgICAgc3VwZXIoZm9ybVNlcnZpY2UpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VzU2VydmljZVxyXG4gICAgICAgICAgICAuc2VsZWN0KFVzZXJQcmVmZXJlbmNlVmFsdWVzLkxvY2FsZSlcclxuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMub25EZXN0cm95JCkpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUobG9jYWxlID0+IHRoaXMuZGF0ZUFkYXB0ZXIuc2V0TG9jYWxlKGxvY2FsZSkpO1xyXG5cclxuICAgICAgICBjb25zdCBtb21lbnREYXRlQWRhcHRlciA9IDxNb21lbnREYXRlQWRhcHRlcj4gdGhpcy5kYXRlQWRhcHRlcjtcclxuICAgICAgICBtb21lbnREYXRlQWRhcHRlci5vdmVycmlkZURpc3BsYXlGb3JtYXQgPSB0aGlzLmZpZWxkLmRhdGVEaXNwbGF5Rm9ybWF0O1xyXG5cclxuICAgICAgICBpZiAodGhpcy5maWVsZCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5maWVsZC5taW5WYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5taW5EYXRlID0gbW9tZW50KHRoaXMuZmllbGQubWluVmFsdWUsIHRoaXMuREFURV9GT1JNQVQpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5maWVsZC5tYXhWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5tYXhEYXRlID0gbW9tZW50KHRoaXMuZmllbGQubWF4VmFsdWUsIHRoaXMuREFURV9GT1JNQVQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZGlzcGxheURhdGUgPSBtb21lbnQodGhpcy5maWVsZC52YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAgICAgdGhpcy5vbkRlc3Ryb3kkLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5vbkRlc3Ryb3kkLmNvbXBsZXRlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25EYXRlQ2hhbmdlZChuZXdEYXRlVmFsdWUpIHtcclxuICAgICAgICBpZiAobmV3RGF0ZVZhbHVlICYmIG5ld0RhdGVWYWx1ZS52YWx1ZSkge1xyXG4gICAgICAgICAgICB0aGlzLmZpZWxkLnZhbHVlID0gbmV3RGF0ZVZhbHVlLnZhbHVlLmZvcm1hdCh0aGlzLmZpZWxkLmRhdGVEaXNwbGF5Rm9ybWF0KTtcclxuICAgICAgICB9IGVsc2UgaWYgKG5ld0RhdGVWYWx1ZSkge1xyXG4gICAgICAgICAgICB0aGlzLmZpZWxkLnZhbHVlID0gbmV3RGF0ZVZhbHVlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuZmllbGQudmFsdWUgPSBudWxsO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLm9uRmllbGRDaGFuZ2VkKHRoaXMuZmllbGQpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==