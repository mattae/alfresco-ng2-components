/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { LogService } from '../../../../services/log.service';
import { Component, ViewEncapsulation } from '@angular/core';
import { FormService } from '../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
export class DropdownWidgetComponent extends WidgetComponent {
    /**
     * @param {?} formService
     * @param {?} logService
     */
    constructor(formService, logService) {
        super(formService);
        this.formService = formService;
        this.logService = logService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.field && this.field.restUrl) {
            if (this.field.form.taskId) {
                this.getValuesByTaskId();
            }
            else {
                this.getValuesByProcessDefinitionId();
            }
        }
    }
    /**
     * @return {?}
     */
    getValuesByTaskId() {
        this.formService
            .getRestFieldValues(this.field.form.taskId, this.field.id)
            .subscribe((/**
         * @param {?} formFieldOption
         * @return {?}
         */
        (formFieldOption) => {
            /** @type {?} */
            const options = [];
            if (this.field.emptyOption) {
                options.push(this.field.emptyOption);
            }
            this.field.options = options.concat((formFieldOption || []));
            this.field.updateForm();
        }), (/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err)));
    }
    /**
     * @return {?}
     */
    getValuesByProcessDefinitionId() {
        this.formService
            .getRestFieldValuesByProcessId(this.field.form.processDefinitionId, this.field.id)
            .subscribe((/**
         * @param {?} formFieldOption
         * @return {?}
         */
        (formFieldOption) => {
            /** @type {?} */
            const options = [];
            if (this.field.emptyOption) {
                options.push(this.field.emptyOption);
            }
            this.field.options = options.concat((formFieldOption || []));
            this.field.updateForm();
        }), (/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err)));
    }
    /**
     * @param {?} option
     * @param {?} fieldValue
     * @return {?}
     */
    getOptionValue(option, fieldValue) {
        /** @type {?} */
        let optionValue = '';
        if (option.id === 'empty' || option.name !== fieldValue) {
            optionValue = option.id;
        }
        else {
            optionValue = option.name;
        }
        return optionValue;
    }
    /**
     * @param {?} error
     * @return {?}
     */
    handleError(error) {
        this.logService.error(error);
    }
    /**
     * @return {?}
     */
    isReadOnlyType() {
        return this.field.type === 'readonly' ? true : false;
    }
}
DropdownWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'dropdown-widget',
                template: "<div class=\"adf-dropdown-widget {{field.className}}\"\r\n     [class.adf-invalid]=\"!field.isValid\" [class.adf-readonly]=\"field.readOnly\">\r\n    <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}<span *ngIf=\"isRequired()\">*</span></label>\r\n    <mat-form-field>\r\n        <mat-select class=\"adf-select\"\r\n                    [id]=\"field.id\"\r\n                    [(ngModel)]=\"field.value\"\r\n                    [disabled]=\"field.readOnly\"\r\n                    (ngModelChange)=\"onFieldChanged(field)\">\r\n            <mat-option *ngFor=\"let opt of field.options\"\r\n                        [value]=\"getOptionValue(opt, field.value)\"\r\n                        [id]=\"opt.id\">{{opt.name}}\r\n            </mat-option>\r\n            <mat-option id=\"readonlyOption\" *ngIf=\"isReadOnlyType()\" [value]=\"field.value\">{{field.value}}</mat-option>\r\n        </mat-select>\r\n    </mat-form-field>\r\n    <error-widget [error]=\"field.validationSummary\"></error-widget>\r\n    <error-widget class=\"adf-dropdown-required-message\" *ngIf=\"isInvalidFieldRequired()\"\r\n                  required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n</div>\r\n",
                host: baseHost,
                encapsulation: ViewEncapsulation.None,
                styles: [".adf-dropdown-widget{width:100%}.adf-dropdown-widget .adf-select{padding-top:0!important;width:100%}.adf-dropdown-widget .mat-select-value-text{font-size:14px}.adf-dropdown-widget-select{width:100%}"]
            }] }
];
/** @nocollapse */
DropdownWidgetComponent.ctorParameters = () => [
    { type: FormService },
    { type: LogService }
];
if (false) {
    /** @type {?} */
    DropdownWidgetComponent.prototype.formService;
    /**
     * @type {?}
     * @private
     */
    DropdownWidgetComponent.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24ud2lkZ2V0LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvZHJvcGRvd24vZHJvcGRvd24ud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDOUQsT0FBTyxFQUFFLFNBQVMsRUFBVSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNyRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFFN0QsT0FBTyxFQUFFLFFBQVEsRUFBRyxlQUFlLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQVNuRSxNQUFNLE9BQU8sdUJBQXdCLFNBQVEsZUFBZTs7Ozs7SUFFeEQsWUFBbUIsV0FBd0IsRUFDdkIsVUFBc0I7UUFDckMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRkwsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDdkIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUUxQyxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRTtZQUNsQyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDeEIsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7YUFDNUI7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLDhCQUE4QixFQUFFLENBQUM7YUFDekM7U0FDSjtJQUNMLENBQUM7Ozs7SUFFRCxpQkFBaUI7UUFDYixJQUFJLENBQUMsV0FBVzthQUNYLGtCQUFrQixDQUNmLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQ2hCO2FBQ0EsU0FBUzs7OztRQUNOLENBQUMsZUFBa0MsRUFBRSxFQUFFOztrQkFDN0IsT0FBTyxHQUFHLEVBQUU7WUFDbEIsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRTtnQkFDeEIsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQ3hDO1lBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLGVBQWUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzdELElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDNUIsQ0FBQzs7OztRQUNELENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUNqQyxDQUFDO0lBQ1YsQ0FBQzs7OztJQUVELDhCQUE4QjtRQUMxQixJQUFJLENBQUMsV0FBVzthQUNYLDZCQUE2QixDQUMxQixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFDbkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQ2hCO2FBQ0EsU0FBUzs7OztRQUNOLENBQUMsZUFBa0MsRUFBRSxFQUFFOztrQkFDN0IsT0FBTyxHQUFHLEVBQUU7WUFDbEIsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRTtnQkFDeEIsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQ3hDO1lBQ0QsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLGVBQWUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQzdELElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDNUIsQ0FBQzs7OztRQUNELENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUNqQyxDQUFDO0lBQ1YsQ0FBQzs7Ozs7O0lBRUQsY0FBYyxDQUFDLE1BQXVCLEVBQUUsVUFBa0I7O1lBQ2xELFdBQVcsR0FBVyxFQUFFO1FBQzVCLElBQUksTUFBTSxDQUFDLEVBQUUsS0FBSyxPQUFPLElBQUksTUFBTSxDQUFDLElBQUksS0FBSyxVQUFVLEVBQUU7WUFDckQsV0FBVyxHQUFHLE1BQU0sQ0FBQyxFQUFFLENBQUM7U0FDM0I7YUFBTTtZQUNILFdBQVcsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO1NBQzdCO1FBQ0QsT0FBTyxXQUFXLENBQUM7SUFDdkIsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsS0FBVTtRQUNsQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7O0lBRUQsY0FBYztRQUNWLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUN6RCxDQUFDOzs7WUE5RUosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxpQkFBaUI7Z0JBQzNCLG90Q0FBcUM7Z0JBRXJDLElBQUksRUFBRSxRQUFRO2dCQUNkLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOzthQUN4Qzs7OztZQVZRLFdBQVc7WUFGWCxVQUFVOzs7O0lBZUgsOENBQStCOzs7OztJQUMvQiw2Q0FBOEIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuIC8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9zZXJ2aWNlcy9sb2cuc2VydmljZSc7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XHJcbmltcG9ydCB7IEZvcm1GaWVsZE9wdGlvbiB9IGZyb20gJy4vLi4vY29yZS9mb3JtLWZpZWxkLW9wdGlvbic7XHJcbmltcG9ydCB7IGJhc2VIb3N0ICwgV2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi8uLi93aWRnZXQuY29tcG9uZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdkcm9wZG93bi13aWRnZXQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Ryb3Bkb3duLndpZGdldC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2Ryb3Bkb3duLndpZGdldC5zY3NzJ10sXHJcbiAgICBob3N0OiBiYXNlSG9zdCxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIERyb3Bkb3duV2lkZ2V0Q29tcG9uZW50IGV4dGVuZHMgV2lkZ2V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZm9ybVNlcnZpY2U6IEZvcm1TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlKSB7XHJcbiAgICAgICAgIHN1cGVyKGZvcm1TZXJ2aWNlKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBpZiAodGhpcy5maWVsZCAmJiB0aGlzLmZpZWxkLnJlc3RVcmwpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZmllbGQuZm9ybS50YXNrSWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0VmFsdWVzQnlUYXNrSWQoKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0VmFsdWVzQnlQcm9jZXNzRGVmaW5pdGlvbklkKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VmFsdWVzQnlUYXNrSWQoKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtU2VydmljZVxyXG4gICAgICAgICAgICAuZ2V0UmVzdEZpZWxkVmFsdWVzKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5maWVsZC5mb3JtLnRhc2tJZCxcclxuICAgICAgICAgICAgICAgIHRoaXMuZmllbGQuaWRcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgKGZvcm1GaWVsZE9wdGlvbjogRm9ybUZpZWxkT3B0aW9uW10pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBvcHRpb25zID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuZmllbGQuZW1wdHlPcHRpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9ucy5wdXNoKHRoaXMuZmllbGQuZW1wdHlPcHRpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZpZWxkLm9wdGlvbnMgPSBvcHRpb25zLmNvbmNhdCgoZm9ybUZpZWxkT3B0aW9uIHx8IFtdKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5maWVsZC51cGRhdGVGb3JtKCk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VmFsdWVzQnlQcm9jZXNzRGVmaW5pdGlvbklkKCkge1xyXG4gICAgICAgIHRoaXMuZm9ybVNlcnZpY2VcclxuICAgICAgICAgICAgLmdldFJlc3RGaWVsZFZhbHVlc0J5UHJvY2Vzc0lkKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5maWVsZC5mb3JtLnByb2Nlc3NEZWZpbml0aW9uSWQsXHJcbiAgICAgICAgICAgICAgICB0aGlzLmZpZWxkLmlkXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIChmb3JtRmllbGRPcHRpb246IEZvcm1GaWVsZE9wdGlvbltdKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgb3B0aW9ucyA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmZpZWxkLmVtcHR5T3B0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnMucHVzaCh0aGlzLmZpZWxkLmVtcHR5T3B0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5maWVsZC5vcHRpb25zID0gb3B0aW9ucy5jb25jYXQoKGZvcm1GaWVsZE9wdGlvbiB8fCBbXSkpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmllbGQudXBkYXRlRm9ybSgpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE9wdGlvblZhbHVlKG9wdGlvbjogRm9ybUZpZWxkT3B0aW9uLCBmaWVsZFZhbHVlOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIGxldCBvcHRpb25WYWx1ZTogc3RyaW5nID0gJyc7XHJcbiAgICAgICAgaWYgKG9wdGlvbi5pZCA9PT0gJ2VtcHR5JyB8fCBvcHRpb24ubmFtZSAhPT0gZmllbGRWYWx1ZSkge1xyXG4gICAgICAgICAgICBvcHRpb25WYWx1ZSA9IG9wdGlvbi5pZDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBvcHRpb25WYWx1ZSA9IG9wdGlvbi5uYW1lO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gb3B0aW9uVmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgaGFuZGxlRXJyb3IoZXJyb3I6IGFueSkge1xyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJvcik7XHJcbiAgICB9XHJcblxyXG4gICAgaXNSZWFkT25seVR5cGUoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZmllbGQudHlwZSA9PT0gJ3JlYWRvbmx5JyA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19