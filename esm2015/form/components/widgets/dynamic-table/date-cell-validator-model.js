/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import moment from 'moment-es6';
export class DateCellValidator {
    constructor() {
        this.supportedTypes = [
            'Date'
        ];
    }
    /**
     * @param {?} column
     * @return {?}
     */
    isSupported(column) {
        return column && column.editable && this.supportedTypes.indexOf(column.type) > -1;
    }
    /**
     * @param {?} row
     * @param {?} column
     * @param {?=} summary
     * @return {?}
     */
    validate(row, column, summary) {
        if (this.isSupported(column)) {
            /** @type {?} */
            const value = row.value[column.id];
            if (!value && !column.required) {
                return true;
            }
            /** @type {?} */
            const dateValue = moment(value, 'YYYY-MM-DDTHH:mm:ss.SSSSZ', true);
            if (!dateValue.isValid()) {
                if (summary) {
                    summary.isValid = false;
                    summary.message = `Invalid '${column.name}' format.`;
                }
                return false;
            }
        }
        return true;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    DateCellValidator.prototype.supportedTypes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1jZWxsLXZhbGlkYXRvci1tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2R5bmFtaWMtdGFibGUvZGF0ZS1jZWxsLXZhbGlkYXRvci1tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxNQUFNLE1BQU0sWUFBWSxDQUFDO0FBTWhDLE1BQU0sT0FBTyxpQkFBaUI7SUFBOUI7UUFFWSxtQkFBYyxHQUFhO1lBQy9CLE1BQU07U0FDVCxDQUFDO0lBMkJOLENBQUM7Ozs7O0lBekJHLFdBQVcsQ0FBQyxNQUEwQjtRQUNsQyxPQUFPLE1BQU0sSUFBSSxNQUFNLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUN0RixDQUFDOzs7Ozs7O0lBRUQsUUFBUSxDQUFDLEdBQW9CLEVBQUUsTUFBMEIsRUFBRSxPQUFxQztRQUU1RixJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEVBQUU7O2tCQUNwQixLQUFLLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDO1lBRWxDLElBQUksQ0FBQyxLQUFLLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFO2dCQUM1QixPQUFPLElBQUksQ0FBQzthQUNmOztrQkFFSyxTQUFTLEdBQUcsTUFBTSxDQUFDLEtBQUssRUFBRSwyQkFBMkIsRUFBRSxJQUFJLENBQUM7WUFDbEUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFDdEIsSUFBSSxPQUFPLEVBQUU7b0JBQ1QsT0FBTyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7b0JBQ3hCLE9BQU8sQ0FBQyxPQUFPLEdBQUcsWUFBWSxNQUFNLENBQUMsSUFBSSxXQUFXLENBQUM7aUJBQ3hEO2dCQUNELE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1NBQ0o7UUFFRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0NBQ0o7Ozs7OztJQTdCRywyQ0FFRSIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4vKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgbW9tZW50IGZyb20gJ21vbWVudC1lczYnO1xyXG5pbXBvcnQgeyBDZWxsVmFsaWRhdG9yIH0gZnJvbSAnLi9jZWxsLXZhbGlkYXRvci5tb2RlbCc7XHJcbmltcG9ydCB7IER5bmFtaWNSb3dWYWxpZGF0aW9uU3VtbWFyeSB9IGZyb20gJy4vZHluYW1pYy1yb3ctdmFsaWRhdGlvbi1zdW1tYXJ5Lm1vZGVsJztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlQ29sdW1uIH0gZnJvbSAnLi9keW5hbWljLXRhYmxlLWNvbHVtbi5tb2RlbCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZVJvdyB9IGZyb20gJy4vZHluYW1pYy10YWJsZS1yb3cubW9kZWwnO1xyXG5cclxuZXhwb3J0IGNsYXNzIERhdGVDZWxsVmFsaWRhdG9yIGltcGxlbWVudHMgQ2VsbFZhbGlkYXRvciB7XHJcblxyXG4gICAgcHJpdmF0ZSBzdXBwb3J0ZWRUeXBlczogc3RyaW5nW10gPSBbXHJcbiAgICAgICAgJ0RhdGUnXHJcbiAgICBdO1xyXG5cclxuICAgIGlzU3VwcG9ydGVkKGNvbHVtbjogRHluYW1pY1RhYmxlQ29sdW1uKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGNvbHVtbiAmJiBjb2x1bW4uZWRpdGFibGUgJiYgdGhpcy5zdXBwb3J0ZWRUeXBlcy5pbmRleE9mKGNvbHVtbi50eXBlKSA+IC0xO1xyXG4gICAgfVxyXG5cclxuICAgIHZhbGlkYXRlKHJvdzogRHluYW1pY1RhYmxlUm93LCBjb2x1bW46IER5bmFtaWNUYWJsZUNvbHVtbiwgc3VtbWFyeT86IER5bmFtaWNSb3dWYWxpZGF0aW9uU3VtbWFyeSk6IGJvb2xlYW4ge1xyXG5cclxuICAgICAgICBpZiAodGhpcy5pc1N1cHBvcnRlZChjb2x1bW4pKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gcm93LnZhbHVlW2NvbHVtbi5pZF07XHJcblxyXG4gICAgICAgICAgICBpZiAoIXZhbHVlICYmICFjb2x1bW4ucmVxdWlyZWQpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb25zdCBkYXRlVmFsdWUgPSBtb21lbnQodmFsdWUsICdZWVlZLU1NLUREVEhIOm1tOnNzLlNTU1NaJywgdHJ1ZSk7XHJcbiAgICAgICAgICAgIGlmICghZGF0ZVZhbHVlLmlzVmFsaWQoKSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHN1bW1hcnkpIHtcclxuICAgICAgICAgICAgICAgICAgICBzdW1tYXJ5LmlzVmFsaWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICBzdW1tYXJ5Lm1lc3NhZ2UgPSBgSW52YWxpZCAnJHtjb2x1bW4ubmFtZX0nIGZvcm1hdC5gO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxufVxyXG4iXX0=