/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
/**
 * @record
 */
export function DynamicTableColumn() { }
if (false) {
    /** @type {?} */
    DynamicTableColumn.prototype.id;
    /** @type {?} */
    DynamicTableColumn.prototype.name;
    /** @type {?} */
    DynamicTableColumn.prototype.type;
    /** @type {?} */
    DynamicTableColumn.prototype.value;
    /** @type {?} */
    DynamicTableColumn.prototype.optionType;
    /** @type {?} */
    DynamicTableColumn.prototype.options;
    /** @type {?} */
    DynamicTableColumn.prototype.restResponsePath;
    /** @type {?} */
    DynamicTableColumn.prototype.restUrl;
    /** @type {?} */
    DynamicTableColumn.prototype.restIdProperty;
    /** @type {?} */
    DynamicTableColumn.prototype.restLabelProperty;
    /** @type {?} */
    DynamicTableColumn.prototype.amountCurrency;
    /** @type {?} */
    DynamicTableColumn.prototype.amountEnableFractions;
    /** @type {?} */
    DynamicTableColumn.prototype.required;
    /** @type {?} */
    DynamicTableColumn.prototype.editable;
    /** @type {?} */
    DynamicTableColumn.prototype.sortable;
    /** @type {?} */
    DynamicTableColumn.prototype.visible;
    /** @type {?} */
    DynamicTableColumn.prototype.endpoint;
    /** @type {?} */
    DynamicTableColumn.prototype.requestHeaders;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHluYW1pYy10YWJsZS1jb2x1bW4ubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9keW5hbWljLXRhYmxlL2R5bmFtaWMtdGFibGUtY29sdW1uLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXNCQSx3Q0F1QkM7OztJQXJCRyxnQ0FBVzs7SUFDWCxrQ0FBYTs7SUFDYixrQ0FBYTs7SUFDYixtQ0FBVzs7SUFDWCx3Q0FBbUI7O0lBQ25CLHFDQUFvQzs7SUFDcEMsOENBQXlCOztJQUN6QixxQ0FBZ0I7O0lBQ2hCLDRDQUF1Qjs7SUFDdkIsK0NBQTBCOztJQUMxQiw0Q0FBdUI7O0lBQ3ZCLG1EQUErQjs7SUFDL0Isc0NBQWtCOztJQUNsQixzQ0FBa0I7O0lBQ2xCLHNDQUFrQjs7SUFDbEIscUNBQWlCOztJQUdqQixzQ0FBYzs7SUFFZCw0Q0FBb0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlQ29sdW1uT3B0aW9uIH0gZnJvbSAnLi9keW5hbWljLXRhYmxlLWNvbHVtbi1vcHRpb24ubW9kZWwnO1xyXG5cclxuLy8gbWFwcyB0bzogY29tLmFjdGl2aXRpLm1vZGVsLmVkaXRvci5mb3JtLkNvbHVtbkRlZmluaXRpb25SZXByZXNlbnRhdGlvblxyXG5leHBvcnQgaW50ZXJmYWNlIER5bmFtaWNUYWJsZUNvbHVtbiB7XHJcblxyXG4gICAgaWQ6IHN0cmluZztcclxuICAgIG5hbWU6IHN0cmluZztcclxuICAgIHR5cGU6IHN0cmluZztcclxuICAgIHZhbHVlOiBhbnk7XHJcbiAgICBvcHRpb25UeXBlOiBzdHJpbmc7XHJcbiAgICBvcHRpb25zOiBEeW5hbWljVGFibGVDb2x1bW5PcHRpb25bXTtcclxuICAgIHJlc3RSZXNwb25zZVBhdGg6IHN0cmluZztcclxuICAgIHJlc3RVcmw6IHN0cmluZztcclxuICAgIHJlc3RJZFByb3BlcnR5OiBzdHJpbmc7XHJcbiAgICByZXN0TGFiZWxQcm9wZXJ0eTogc3RyaW5nO1xyXG4gICAgYW1vdW50Q3VycmVuY3k6IHN0cmluZztcclxuICAgIGFtb3VudEVuYWJsZUZyYWN0aW9uczogYm9vbGVhbjtcclxuICAgIHJlcXVpcmVkOiBib29sZWFuO1xyXG4gICAgZWRpdGFibGU6IGJvb2xlYW47XHJcbiAgICBzb3J0YWJsZTogYm9vbGVhbjtcclxuICAgIHZpc2libGU6IGJvb2xlYW47XHJcblxyXG4gICAgLy8gVE9ETzogY29tLmFjdGl2aXRpLmRvbWFpbi5pZG0uRW5kcG9pbnRDb25maWd1cmF0aW9uLkVuZHBvaW50Q29uZmlndXJhdGlvblJlcHJlc2VudGF0aW9uXHJcbiAgICBlbmRwb2ludDogYW55O1xyXG4gICAgLy8gVE9ETzogY29tLmFjdGl2aXRpLm1vZGVsLmVkaXRvci5mb3JtLlJlcXVlc3RIZWFkZXJSZXByZXNlbnRhdGlvblxyXG4gICAgcmVxdWVzdEhlYWRlcnM6IGFueTtcclxufVxyXG4iXX0=