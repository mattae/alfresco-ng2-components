/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { LogService } from '../../../../services/log.service';
import { ChangeDetectorRef, Component, ElementRef, ViewEncapsulation } from '@angular/core';
import { WidgetVisibilityService } from '../../../services/widget-visibility.service';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
import { DynamicTableModel } from './dynamic-table.widget.model';
export class DynamicTableWidgetComponent extends WidgetComponent {
    /**
     * @param {?} formService
     * @param {?} elementRef
     * @param {?} visibilityService
     * @param {?} logService
     * @param {?} cd
     */
    constructor(formService, elementRef, visibilityService, logService, cd) {
        super(formService);
        this.formService = formService;
        this.elementRef = elementRef;
        this.visibilityService = visibilityService;
        this.logService = logService;
        this.cd = cd;
        this.ERROR_MODEL_NOT_FOUND = 'Table model not found';
        this.editMode = false;
        this.editRow = null;
        this.selectArrayCode = [32, 0, 13];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.field) {
            this.content = new DynamicTableModel(this.field, this.formService);
            this.visibilityService.refreshVisibility(this.field.form);
        }
    }
    /**
     * @return {?}
     */
    forceFocusOnAddButton() {
        if (this.content) {
            this.cd.detectChanges();
            /** @type {?} */
            const buttonAddRow = (/** @type {?} */ (this.elementRef.nativeElement.querySelector('#' + this.content.id + '-add-row')));
            if (this.isDynamicTableReady(buttonAddRow)) {
                buttonAddRow.focus();
            }
        }
    }
    /**
     * @private
     * @param {?} buttonAddRow
     * @return {?}
     */
    isDynamicTableReady(buttonAddRow) {
        return this.field && !this.editMode && buttonAddRow;
    }
    /**
     * @return {?}
     */
    isValid() {
        /** @type {?} */
        let valid = true;
        if (this.content && this.content.field) {
            valid = this.content.field.isValid;
        }
        return valid;
    }
    /**
     * @param {?} row
     * @return {?}
     */
    onRowClicked(row) {
        if (this.content) {
            this.content.selectedRow = row;
        }
    }
    /**
     * @param {?} $event
     * @param {?} row
     * @return {?}
     */
    onKeyPressed($event, row) {
        if (this.content && this.isEnterOrSpacePressed($event.keyCode)) {
            this.content.selectedRow = row;
        }
    }
    /**
     * @private
     * @param {?} keyCode
     * @return {?}
     */
    isEnterOrSpacePressed(keyCode) {
        return this.selectArrayCode.indexOf(keyCode) !== -1;
    }
    /**
     * @return {?}
     */
    hasSelection() {
        return !!(this.content && this.content.selectedRow);
    }
    /**
     * @return {?}
     */
    moveSelectionUp() {
        if (this.content && !this.readOnly) {
            this.content.moveRow(this.content.selectedRow, -1);
            return true;
        }
        return false;
    }
    /**
     * @return {?}
     */
    moveSelectionDown() {
        if (this.content && !this.readOnly) {
            this.content.moveRow(this.content.selectedRow, 1);
            return true;
        }
        return false;
    }
    /**
     * @return {?}
     */
    deleteSelection() {
        if (this.content && !this.readOnly) {
            this.content.deleteRow(this.content.selectedRow);
            return true;
        }
        return false;
    }
    /**
     * @return {?}
     */
    addNewRow() {
        if (this.content && !this.readOnly) {
            this.editRow = (/** @type {?} */ ({
                isNew: true,
                selected: false,
                value: {}
            }));
            this.editMode = true;
            return true;
        }
        return false;
    }
    /**
     * @return {?}
     */
    editSelection() {
        if (this.content && !this.readOnly) {
            this.editRow = this.copyRow(this.content.selectedRow);
            this.editMode = true;
            return true;
        }
        return false;
    }
    /**
     * @param {?} row
     * @param {?} column
     * @return {?}
     */
    getCellValue(row, column) {
        if (this.content) {
            /** @type {?} */
            const cellValue = this.content.getCellValue(row, column);
            if (column.type === 'Amount') {
                return (column.amountCurrency || '$') + ' ' + (cellValue || 0);
            }
            return cellValue;
        }
        return null;
    }
    /**
     * @return {?}
     */
    onSaveChanges() {
        if (this.content) {
            if (this.editRow.isNew) {
                /** @type {?} */
                const row = this.copyRow(this.editRow);
                this.content.selectedRow = null;
                this.content.addRow(row);
                this.editRow.isNew = false;
            }
            else {
                this.content.selectedRow.value = this.copyObject(this.editRow.value);
            }
            this.content.flushValue();
        }
        else {
            this.logService.error(this.ERROR_MODEL_NOT_FOUND);
        }
        this.editMode = false;
        this.forceFocusOnAddButton();
    }
    /**
     * @return {?}
     */
    onCancelChanges() {
        this.editMode = false;
        this.editRow = null;
        this.forceFocusOnAddButton();
    }
    /**
     * @param {?} row
     * @return {?}
     */
    copyRow(row) {
        return (/** @type {?} */ ({
            value: this.copyObject(row.value)
        }));
    }
    /**
     * @private
     * @param {?} obj
     * @return {?}
     */
    copyObject(obj) {
        /** @type {?} */
        let result = obj;
        if (typeof obj === 'object' && obj !== null && obj !== undefined) {
            result = Object.assign({}, obj);
            Object.keys(obj).forEach((/**
             * @param {?} key
             * @return {?}
             */
            (key) => {
                if (typeof obj[key] === 'object') {
                    result[key] = this.copyObject(obj[key]);
                }
            }));
        }
        return result;
    }
}
DynamicTableWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'dynamic-table-widget',
                template: "<div class=\"adf-dynamic-table-scrolling {{field.className}}\"\r\n    [class.adf-invalid]=\"!isValid()\">\r\n    <div class=\"adf-label\">{{content.name | translate }}<span *ngIf=\"isRequired()\">*</span></div>\r\n\r\n    <div *ngIf=\"!editMode\">\r\n        <div class=\"adf-table-container\">\r\n            <table class=\"adf-full-width adf-dynamic-table\" id=\"dynamic-table-{{content.id}}\">\r\n                <thead>\r\n                    <tr>\r\n                        <th *ngFor=\"let column of content.visibleColumns\">\r\n                            {{column.name}}\r\n                        </th>\r\n                    </tr>\r\n                </thead>\r\n                <tbody>\r\n                    <tr *ngFor=\"let row of content.rows; let idx = index\" tabindex=\"0\" id=\"{{content.id}}-row-{{idx}}\"\r\n                        [class.adf-dynamic-table-widget__row-selected]=\"row.selected\" (keyup)=\"onKeyPressed($event, row)\">\r\n                        <td *ngFor=\"let column of content.visibleColumns\"\r\n                            (click)=\"onRowClicked(row)\">\r\n                            <span *ngIf=\"column.type !== 'Boolean' else checkbox\">\r\n                                {{ getCellValue(row, column) }}\r\n                            </span>\r\n                            <ng-template #checkbox>\r\n                                <mat-checkbox disabled [checked]=\"getCellValue(row, column)\">\r\n                                </mat-checkbox>\r\n                            </ng-template>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n\r\n        <div *ngIf=\"!readOnly\">\r\n            <button mat-button\r\n                    [disabled]=\"!hasSelection()\"\r\n                    (click)=\"moveSelectionUp()\">\r\n                <mat-icon>arrow_upward</mat-icon>\r\n            </button>\r\n            <button mat-button\r\n                    [disabled]=\"!hasSelection()\"\r\n                    (click)=\"moveSelectionDown()\">\r\n                <mat-icon>arrow_downward</mat-icon>\r\n            </button>\r\n            <button mat-button\r\n                    [disabled]=\"field.readOnly\"\r\n                    id=\"{{content.id}}-add-row\"\r\n                    (click)=\"addNewRow()\">\r\n                <mat-icon>add_circle_outline</mat-icon>\r\n            </button>\r\n            <button mat-button\r\n                    [disabled]=\"!hasSelection()\"\r\n                    (click)=\"deleteSelection()\">\r\n                <mat-icon>remove_circle_outline</mat-icon>\r\n            </button>\r\n            <button mat-button\r\n                    [disabled]=\"!hasSelection()\"\r\n                    (click)=\"editSelection()\">\r\n                <mat-icon>edit</mat-icon>\r\n            </button>\r\n        </div>\r\n     </div>\r\n\r\n     <row-editor *ngIf=\"editMode\"\r\n        [table]=\"content\"\r\n        [row]=\"editRow\"\r\n        (save)=\"onSaveChanges()\"\r\n        (cancel)=\"onCancelChanges()\">\r\n     </row-editor>\r\n    <error-widget [error]=\"field.validationSummary\" ></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n</div>\r\n",
                host: baseHost,
                encapsulation: ViewEncapsulation.None,
                styles: [""]
            }] }
];
/** @nocollapse */
DynamicTableWidgetComponent.ctorParameters = () => [
    { type: FormService },
    { type: ElementRef },
    { type: WidgetVisibilityService },
    { type: LogService },
    { type: ChangeDetectorRef }
];
if (false) {
    /** @type {?} */
    DynamicTableWidgetComponent.prototype.ERROR_MODEL_NOT_FOUND;
    /** @type {?} */
    DynamicTableWidgetComponent.prototype.content;
    /** @type {?} */
    DynamicTableWidgetComponent.prototype.editMode;
    /** @type {?} */
    DynamicTableWidgetComponent.prototype.editRow;
    /**
     * @type {?}
     * @private
     */
    DynamicTableWidgetComponent.prototype.selectArrayCode;
    /** @type {?} */
    DynamicTableWidgetComponent.prototype.formService;
    /** @type {?} */
    DynamicTableWidgetComponent.prototype.elementRef;
    /**
     * @type {?}
     * @private
     */
    DynamicTableWidgetComponent.prototype.visibilityService;
    /**
     * @type {?}
     * @private
     */
    DynamicTableWidgetComponent.prototype.logService;
    /**
     * @type {?}
     * @private
     */
    DynamicTableWidgetComponent.prototype.cd;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHluYW1pYy10YWJsZS53aWRnZXQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9keW5hbWljLXRhYmxlL2R5bmFtaWMtdGFibGUud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDOUQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQVUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDcEcsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDdEYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQy9ELE9BQU8sRUFBRSxRQUFRLEVBQUUsZUFBZSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFHbEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFTakUsTUFBTSxPQUFPLDJCQUE0QixTQUFRLGVBQWU7Ozs7Ozs7O0lBVzVELFlBQW1CLFdBQXdCLEVBQ3hCLFVBQXNCLEVBQ3JCLGlCQUEwQyxFQUMxQyxVQUFzQixFQUN0QixFQUFxQjtRQUNyQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7UUFMSixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3JCLHNCQUFpQixHQUFqQixpQkFBaUIsQ0FBeUI7UUFDMUMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixPQUFFLEdBQUYsRUFBRSxDQUFtQjtRQWJ6QywwQkFBcUIsR0FBRyx1QkFBdUIsQ0FBQztRQUloRCxhQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLFlBQU8sR0FBb0IsSUFBSSxDQUFDO1FBRXhCLG9CQUFlLEdBQUcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBUXRDLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1osSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLGlCQUFpQixDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ25FLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzdEO0lBQ0wsQ0FBQzs7OztJQUVELHFCQUFxQjtRQUNqQixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDZCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDOztrQkFDbEIsWUFBWSxHQUFHLG1CQUFvQixJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsRUFBRSxHQUFHLFVBQVUsQ0FBQyxFQUFBO1lBQ3hILElBQUksSUFBSSxDQUFDLG1CQUFtQixDQUFDLFlBQVksQ0FBQyxFQUFFO2dCQUN4QyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDeEI7U0FDSjtJQUNMLENBQUM7Ozs7OztJQUVPLG1CQUFtQixDQUFDLFlBQVk7UUFDcEMsT0FBTyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxZQUFZLENBQUM7SUFDeEQsQ0FBQzs7OztJQUVELE9BQU87O1lBQ0MsS0FBSyxHQUFHLElBQUk7UUFFaEIsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFO1lBQ3BDLEtBQUssR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7U0FDdEM7UUFFRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDOzs7OztJQUVELFlBQVksQ0FBQyxHQUFvQjtRQUM3QixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDZCxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7U0FDbEM7SUFDTCxDQUFDOzs7Ozs7SUFFRCxZQUFZLENBQUMsTUFBcUIsRUFBRSxHQUFvQjtRQUNwRCxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUM1RCxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7U0FDbEM7SUFDTCxDQUFDOzs7Ozs7SUFFTyxxQkFBcUIsQ0FBQyxPQUFPO1FBQ2pDLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDeEQsQ0FBQzs7OztJQUVELFlBQVk7UUFDUixPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUN4RCxDQUFDOzs7O0lBRUQsZUFBZTtRQUNYLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDaEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuRCxPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7OztJQUVELGlCQUFpQjtRQUNiLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDaEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDbEQsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7SUFFRCxlQUFlO1FBQ1gsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNoQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ2pELE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDOzs7O0lBRUQsU0FBUztRQUNMLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDaEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxtQkFBa0I7Z0JBQzdCLEtBQUssRUFBRSxJQUFJO2dCQUNYLFFBQVEsRUFBRSxLQUFLO2dCQUNmLEtBQUssRUFBRSxFQUFFO2FBQ1osRUFBQSxDQUFDO1lBQ0YsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDckIsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7SUFFRCxhQUFhO1FBQ1QsSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNoQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN0RCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztZQUNyQixPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7Ozs7O0lBRUQsWUFBWSxDQUFDLEdBQW9CLEVBQUUsTUFBMEI7UUFDekQsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFOztrQkFDUixTQUFTLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQztZQUN4RCxJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssUUFBUSxFQUFFO2dCQUMxQixPQUFPLENBQUMsTUFBTSxDQUFDLGNBQWMsSUFBSSxHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxTQUFTLElBQUksQ0FBQyxDQUFDLENBQUM7YUFDbEU7WUFDRCxPQUFPLFNBQVMsQ0FBQztTQUNwQjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7SUFFRCxhQUFhO1FBQ1QsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2QsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRTs7c0JBQ2QsR0FBRyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztnQkFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO2dCQUNoQyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztnQkFDekIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO2FBQzlCO2lCQUFNO2dCQUNILElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDeEU7WUFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxDQUFDO1NBQzdCO2FBQU07WUFDSCxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQztTQUNyRDtRQUNELElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0lBQ2pDLENBQUM7Ozs7SUFFRCxlQUFlO1FBQ1gsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7UUFDcEIsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsR0FBb0I7UUFDeEIsT0FBTyxtQkFBa0I7WUFDckIsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQztTQUNwQyxFQUFBLENBQUM7SUFDTixDQUFDOzs7Ozs7SUFFTyxVQUFVLENBQUMsR0FBUTs7WUFDbkIsTUFBTSxHQUFHLEdBQUc7UUFFaEIsSUFBSSxPQUFPLEdBQUcsS0FBSyxRQUFRLElBQUksR0FBRyxLQUFLLElBQUksSUFBSSxHQUFHLEtBQUssU0FBUyxFQUFFO1lBQzlELE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUNoQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU87Ozs7WUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO2dCQUM3QixJQUFJLE9BQU8sR0FBRyxDQUFDLEdBQUcsQ0FBQyxLQUFLLFFBQVEsRUFBRTtvQkFDOUIsTUFBTSxDQUFDLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7aUJBQzNDO1lBQ0wsQ0FBQyxFQUFDLENBQUM7U0FDTjtRQUVELE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7OztZQWpMSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLHNCQUFzQjtnQkFDaEMsbXdHQUEwQztnQkFFMUMsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2FBQ3hDOzs7O1lBWlEsV0FBVztZQUZtQixVQUFVO1lBQ3hDLHVCQUF1QjtZQUZ2QixVQUFVO1lBQ1YsaUJBQWlCOzs7O0lBaUJ0Qiw0REFBZ0Q7O0lBRWhELDhDQUEyQjs7SUFFM0IsK0NBQTBCOztJQUMxQiw4Q0FBZ0M7Ozs7O0lBRWhDLHNEQUFzQzs7SUFFMUIsa0RBQStCOztJQUMvQixpREFBNkI7Ozs7O0lBQzdCLHdEQUFrRDs7Ozs7SUFDbEQsaURBQThCOzs7OztJQUM5Qix5Q0FBNkIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL3NlcnZpY2VzL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ2hhbmdlRGV0ZWN0b3JSZWYsIENvbXBvbmVudCwgRWxlbWVudFJlZiwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBXaWRnZXRWaXNpYmlsaXR5U2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NlcnZpY2VzL3dpZGdldC12aXNpYmlsaXR5LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4vLi4vLi4vLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgYmFzZUhvc3QsIFdpZGdldENvbXBvbmVudCB9IGZyb20gJy4vLi4vd2lkZ2V0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZUNvbHVtbiB9IGZyb20gJy4vZHluYW1pYy10YWJsZS1jb2x1bW4ubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVSb3cgfSBmcm9tICcuL2R5bmFtaWMtdGFibGUtcm93Lm1vZGVsJztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlTW9kZWwgfSBmcm9tICcuL2R5bmFtaWMtdGFibGUud2lkZ2V0Lm1vZGVsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdkeW5hbWljLXRhYmxlLXdpZGdldCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vZHluYW1pYy10YWJsZS53aWRnZXQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9keW5hbWljLXRhYmxlLndpZGdldC5zY3NzJ10sXHJcbiAgICBob3N0OiBiYXNlSG9zdCxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIER5bmFtaWNUYWJsZVdpZGdldENvbXBvbmVudCBleHRlbmRzIFdpZGdldENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgRVJST1JfTU9ERUxfTk9UX0ZPVU5EID0gJ1RhYmxlIG1vZGVsIG5vdCBmb3VuZCc7XHJcblxyXG4gICAgY29udGVudDogRHluYW1pY1RhYmxlTW9kZWw7XHJcblxyXG4gICAgZWRpdE1vZGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGVkaXRSb3c6IER5bmFtaWNUYWJsZVJvdyA9IG51bGw7XHJcblxyXG4gICAgcHJpdmF0ZSBzZWxlY3RBcnJheUNvZGUgPSBbMzIsIDAsIDEzXTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZm9ybVNlcnZpY2U6IEZvcm1TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIGVsZW1lbnRSZWY6IEVsZW1lbnRSZWYsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHZpc2liaWxpdHlTZXJ2aWNlOiBXaWRnZXRWaXNpYmlsaXR5U2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgbG9nU2VydmljZTogTG9nU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgY2Q6IENoYW5nZURldGVjdG9yUmVmKSB7XHJcbiAgICAgICAgc3VwZXIoZm9ybVNlcnZpY2UpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmZpZWxkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29udGVudCA9IG5ldyBEeW5hbWljVGFibGVNb2RlbCh0aGlzLmZpZWxkLCB0aGlzLmZvcm1TZXJ2aWNlKTtcclxuICAgICAgICAgICAgdGhpcy52aXNpYmlsaXR5U2VydmljZS5yZWZyZXNoVmlzaWJpbGl0eSh0aGlzLmZpZWxkLmZvcm0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBmb3JjZUZvY3VzT25BZGRCdXR0b24oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY29udGVudCkge1xyXG4gICAgICAgICAgICB0aGlzLmNkLmRldGVjdENoYW5nZXMoKTtcclxuICAgICAgICAgICAgY29uc3QgYnV0dG9uQWRkUm93ID0gPEhUTUxCdXR0b25FbGVtZW50PiB0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5xdWVyeVNlbGVjdG9yKCcjJyArIHRoaXMuY29udGVudC5pZCArICctYWRkLXJvdycpO1xyXG4gICAgICAgICAgICBpZiAodGhpcy5pc0R5bmFtaWNUYWJsZVJlYWR5KGJ1dHRvbkFkZFJvdykpIHtcclxuICAgICAgICAgICAgICAgIGJ1dHRvbkFkZFJvdy5mb2N1cygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaXNEeW5hbWljVGFibGVSZWFkeShidXR0b25BZGRSb3cpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5maWVsZCAmJiAhdGhpcy5lZGl0TW9kZSAmJiBidXR0b25BZGRSb3c7XHJcbiAgICB9XHJcblxyXG4gICAgaXNWYWxpZCgpIHtcclxuICAgICAgICBsZXQgdmFsaWQgPSB0cnVlO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5jb250ZW50ICYmIHRoaXMuY29udGVudC5maWVsZCkge1xyXG4gICAgICAgICAgICB2YWxpZCA9IHRoaXMuY29udGVudC5maWVsZC5pc1ZhbGlkO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHZhbGlkO1xyXG4gICAgfVxyXG5cclxuICAgIG9uUm93Q2xpY2tlZChyb3c6IER5bmFtaWNUYWJsZVJvdykge1xyXG4gICAgICAgIGlmICh0aGlzLmNvbnRlbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5jb250ZW50LnNlbGVjdGVkUm93ID0gcm93O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvbktleVByZXNzZWQoJGV2ZW50OiBLZXlib2FyZEV2ZW50LCByb3c6IER5bmFtaWNUYWJsZVJvdykge1xyXG4gICAgICAgIGlmICh0aGlzLmNvbnRlbnQgJiYgdGhpcy5pc0VudGVyT3JTcGFjZVByZXNzZWQoJGV2ZW50LmtleUNvZGUpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29udGVudC5zZWxlY3RlZFJvdyA9IHJvdztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBpc0VudGVyT3JTcGFjZVByZXNzZWQoa2V5Q29kZSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNlbGVjdEFycmF5Q29kZS5pbmRleE9mKGtleUNvZGUpICE9PSAtMTtcclxuICAgIH1cclxuXHJcbiAgICBoYXNTZWxlY3Rpb24oKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuICEhKHRoaXMuY29udGVudCAmJiB0aGlzLmNvbnRlbnQuc2VsZWN0ZWRSb3cpO1xyXG4gICAgfVxyXG5cclxuICAgIG1vdmVTZWxlY3Rpb25VcCgpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodGhpcy5jb250ZW50ICYmICF0aGlzLnJlYWRPbmx5KSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29udGVudC5tb3ZlUm93KHRoaXMuY29udGVudC5zZWxlY3RlZFJvdywgLTEpO1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIG1vdmVTZWxlY3Rpb25Eb3duKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLmNvbnRlbnQgJiYgIXRoaXMucmVhZE9ubHkpIHtcclxuICAgICAgICAgICAgdGhpcy5jb250ZW50Lm1vdmVSb3codGhpcy5jb250ZW50LnNlbGVjdGVkUm93LCAxKTtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBkZWxldGVTZWxlY3Rpb24oKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHRoaXMuY29udGVudCAmJiAhdGhpcy5yZWFkT25seSkge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRlbnQuZGVsZXRlUm93KHRoaXMuY29udGVudC5zZWxlY3RlZFJvdyk7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgYWRkTmV3Um93KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLmNvbnRlbnQgJiYgIXRoaXMucmVhZE9ubHkpIHtcclxuICAgICAgICAgICAgdGhpcy5lZGl0Um93ID0gPER5bmFtaWNUYWJsZVJvdz4ge1xyXG4gICAgICAgICAgICAgICAgaXNOZXc6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBzZWxlY3RlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZToge31cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5lZGl0TW9kZSA9IHRydWU7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgZWRpdFNlbGVjdGlvbigpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodGhpcy5jb250ZW50ICYmICF0aGlzLnJlYWRPbmx5KSB7XHJcbiAgICAgICAgICAgIHRoaXMuZWRpdFJvdyA9IHRoaXMuY29weVJvdyh0aGlzLmNvbnRlbnQuc2VsZWN0ZWRSb3cpO1xyXG4gICAgICAgICAgICB0aGlzLmVkaXRNb2RlID0gdHJ1ZTtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDZWxsVmFsdWUocm93OiBEeW5hbWljVGFibGVSb3csIGNvbHVtbjogRHluYW1pY1RhYmxlQ29sdW1uKTogYW55IHtcclxuICAgICAgICBpZiAodGhpcy5jb250ZW50KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNlbGxWYWx1ZSA9IHRoaXMuY29udGVudC5nZXRDZWxsVmFsdWUocm93LCBjb2x1bW4pO1xyXG4gICAgICAgICAgICBpZiAoY29sdW1uLnR5cGUgPT09ICdBbW91bnQnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gKGNvbHVtbi5hbW91bnRDdXJyZW5jeSB8fCAnJCcpICsgJyAnICsgKGNlbGxWYWx1ZSB8fCAwKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gY2VsbFZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBvblNhdmVDaGFuZ2VzKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvbnRlbnQpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZWRpdFJvdy5pc05ldykge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgcm93ID0gdGhpcy5jb3B5Um93KHRoaXMuZWRpdFJvdyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRlbnQuc2VsZWN0ZWRSb3cgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jb250ZW50LmFkZFJvdyhyb3cpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lZGl0Um93LmlzTmV3ID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRlbnQuc2VsZWN0ZWRSb3cudmFsdWUgPSB0aGlzLmNvcHlPYmplY3QodGhpcy5lZGl0Um93LnZhbHVlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0aGlzLmNvbnRlbnQuZmx1c2hWYWx1ZSgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcih0aGlzLkVSUk9SX01PREVMX05PVF9GT1VORCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZWRpdE1vZGUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmZvcmNlRm9jdXNPbkFkZEJ1dHRvbigpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uQ2FuY2VsQ2hhbmdlcygpIHtcclxuICAgICAgICB0aGlzLmVkaXRNb2RlID0gZmFsc2U7XHJcbiAgICAgICAgdGhpcy5lZGl0Um93ID0gbnVsbDtcclxuICAgICAgICB0aGlzLmZvcmNlRm9jdXNPbkFkZEJ1dHRvbigpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvcHlSb3cocm93OiBEeW5hbWljVGFibGVSb3cpOiBEeW5hbWljVGFibGVSb3cge1xyXG4gICAgICAgIHJldHVybiA8RHluYW1pY1RhYmxlUm93PiB7XHJcbiAgICAgICAgICAgIHZhbHVlOiB0aGlzLmNvcHlPYmplY3Qocm93LnZhbHVlKVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBjb3B5T2JqZWN0KG9iajogYW55KTogYW55IHtcclxuICAgICAgICBsZXQgcmVzdWx0ID0gb2JqO1xyXG5cclxuICAgICAgICBpZiAodHlwZW9mIG9iaiA9PT0gJ29iamVjdCcgJiYgb2JqICE9PSBudWxsICYmIG9iaiAhPT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIHJlc3VsdCA9IE9iamVjdC5hc3NpZ24oe30sIG9iaik7XHJcbiAgICAgICAgICAgIE9iamVjdC5rZXlzKG9iaikuZm9yRWFjaCgoa2V5KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodHlwZW9mIG9ialtrZXldID09PSAnb2JqZWN0Jykge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdFtrZXldID0gdGhpcy5jb3B5T2JqZWN0KG9ialtrZXldKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==