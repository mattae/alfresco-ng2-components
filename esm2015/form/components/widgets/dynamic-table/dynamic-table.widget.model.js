/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import moment from 'moment-es6';
import { ValidateDynamicTableRowEvent } from '../../../events/validate-dynamic-table-row.event';
import { FormWidgetModel } from './../core/form-widget.model';
import { DateCellValidator } from './date-cell-validator-model';
import { DynamicRowValidationSummary } from './dynamic-row-validation-summary.model';
import { NumberCellValidator } from './number-cell-validator.model';
import { RequiredCellValidator } from './required-cell-validator.model';
export class DynamicTableModel extends FormWidgetModel {
    /**
     * @param {?} field
     * @param {?} formService
     */
    constructor(field, formService) {
        super(field.form, field.json);
        this.formService = formService;
        this.columns = [];
        this.visibleColumns = [];
        this.rows = [];
        this._validators = [];
        this.field = field;
        if (field.json) {
            /** @type {?} */
            const columns = this.getColumns(field);
            if (columns) {
                this.columns = columns;
                this.visibleColumns = this.columns.filter((/**
                 * @param {?} col
                 * @return {?}
                 */
                (col) => col.visible));
            }
            if (field.json.value) {
                this.rows = field.json.value.map((/**
                 * @param {?} obj
                 * @return {?}
                 */
                (obj) => (/** @type {?} */ ({ selected: false, value: obj }))));
            }
        }
        this._validators = [
            new RequiredCellValidator(),
            new DateCellValidator(),
            new NumberCellValidator()
        ];
    }
    /**
     * @return {?}
     */
    get selectedRow() {
        return this._selectedRow;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set selectedRow(value) {
        if (this._selectedRow && this._selectedRow === value) {
            this._selectedRow.selected = false;
            this._selectedRow = null;
            return;
        }
        this.rows.forEach((/**
         * @param {?} row
         * @return {?}
         */
        (row) => row.selected = false));
        this._selectedRow = value;
        if (value) {
            this._selectedRow.selected = true;
        }
    }
    /**
     * @private
     * @param {?} field
     * @return {?}
     */
    getColumns(field) {
        if (field && field.json) {
            /** @type {?} */
            let definitions = field.json.columnDefinitions;
            if (!definitions && field.json.params && field.json.params.field) {
                definitions = field.json.params.field.columnDefinitions;
            }
            if (definitions) {
                return definitions.map((/**
                 * @param {?} obj
                 * @return {?}
                 */
                (obj) => (/** @type {?} */ (obj))));
            }
        }
        return null;
    }
    /**
     * @return {?}
     */
    flushValue() {
        if (this.field) {
            this.field.value = this.rows.map((/**
             * @param {?} r
             * @return {?}
             */
            (r) => r.value));
            this.field.updateForm();
        }
    }
    /**
     * @param {?} row
     * @param {?} offset
     * @return {?}
     */
    moveRow(row, offset) {
        /** @type {?} */
        const oldIndex = this.rows.indexOf(row);
        if (oldIndex > -1) {
            /** @type {?} */
            let newIndex = (oldIndex + offset);
            if (newIndex < 0) {
                newIndex = 0;
            }
            else if (newIndex >= this.rows.length) {
                newIndex = this.rows.length;
            }
            /** @type {?} */
            const arr = this.rows.slice();
            arr.splice(oldIndex, 1);
            arr.splice(newIndex, 0, row);
            this.rows = arr;
            this.flushValue();
        }
    }
    /**
     * @param {?} row
     * @return {?}
     */
    deleteRow(row) {
        if (row) {
            if (this.selectedRow === row) {
                this.selectedRow = null;
            }
            /** @type {?} */
            const idx = this.rows.indexOf(row);
            if (idx > -1) {
                this.rows.splice(idx, 1);
                this.flushValue();
            }
        }
    }
    /**
     * @param {?} row
     * @return {?}
     */
    addRow(row) {
        if (row) {
            this.rows.push(row);
            // this.selectedRow = row;
        }
    }
    /**
     * @param {?} row
     * @return {?}
     */
    validateRow(row) {
        /** @type {?} */
        const summary = new DynamicRowValidationSummary({
            isValid: true,
            message: null
        });
        /** @type {?} */
        const event = new ValidateDynamicTableRowEvent(this.form, this.field, row, summary);
        this.formService.validateDynamicTableRow.next(event);
        if (event.defaultPrevented || !summary.isValid) {
            return summary;
        }
        if (row) {
            for (const col of this.columns) {
                for (const validator of this._validators) {
                    if (!validator.validate(row, col, summary)) {
                        return summary;
                    }
                }
            }
        }
        return summary;
    }
    /**
     * @param {?} row
     * @param {?} column
     * @return {?}
     */
    getCellValue(row, column) {
        /** @type {?} */
        const rowValue = row.value[column.id];
        if (column.type === 'Dropdown') {
            if (rowValue) {
                return rowValue.name;
            }
        }
        if (column.type === 'Boolean') {
            return rowValue ? true : false;
        }
        if (column.type === 'Date') {
            if (rowValue) {
                return moment(rowValue.split('T')[0], 'YYYY-MM-DD').format('DD-MM-YYYY');
            }
        }
        return rowValue || '';
    }
    /**
     * @param {?} column
     * @return {?}
     */
    getDisplayText(column) {
        /** @type {?} */
        let columnName = column.name;
        if (column.type === 'Amount') {
            /** @type {?} */
            const currency = column.amountCurrency || '$';
            columnName = `${column.name} (${currency})`;
        }
        return columnName;
    }
}
if (false) {
    /** @type {?} */
    DynamicTableModel.prototype.field;
    /** @type {?} */
    DynamicTableModel.prototype.columns;
    /** @type {?} */
    DynamicTableModel.prototype.visibleColumns;
    /** @type {?} */
    DynamicTableModel.prototype.rows;
    /**
     * @type {?}
     * @private
     */
    DynamicTableModel.prototype._selectedRow;
    /**
     * @type {?}
     * @private
     */
    DynamicTableModel.prototype._validators;
    /**
     * @type {?}
     * @private
     */
    DynamicTableModel.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHluYW1pYy10YWJsZS53aWRnZXQubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9keW5hbWljLXRhYmxlL2R5bmFtaWMtdGFibGUud2lkZ2V0Lm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLE1BQU0sTUFBTSxZQUFZLENBQUM7QUFDaEMsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFHaEcsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBRTlELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQ2hFLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBR3JGLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBRXhFLE1BQU0sT0FBTyxpQkFBa0IsU0FBUSxlQUFlOzs7OztJQThCbEQsWUFBWSxLQUFxQixFQUFVLFdBQXdCO1FBQy9ELEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQURTLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBM0JuRSxZQUFPLEdBQXlCLEVBQUUsQ0FBQztRQUNuQyxtQkFBYyxHQUF5QixFQUFFLENBQUM7UUFDMUMsU0FBSSxHQUFzQixFQUFFLENBQUM7UUFHckIsZ0JBQVcsR0FBb0IsRUFBRSxDQUFDO1FBd0J0QyxJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUVuQixJQUFJLEtBQUssQ0FBQyxJQUFJLEVBQUU7O2tCQUNOLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQztZQUN0QyxJQUFJLE9BQU8sRUFBRTtnQkFDVCxJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztnQkFDdkIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU07Ozs7Z0JBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUMsQ0FBQzthQUNuRTtZQUVELElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUU7Z0JBQ2xCLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRzs7OztnQkFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsbUJBQWtCLEVBQUMsUUFBUSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsR0FBRyxFQUFDLEVBQUEsRUFBQyxDQUFDO2FBQzlGO1NBQ0o7UUFFRCxJQUFJLENBQUMsV0FBVyxHQUFHO1lBQ2YsSUFBSSxxQkFBcUIsRUFBRTtZQUMzQixJQUFJLGlCQUFpQixFQUFFO1lBQ3ZCLElBQUksbUJBQW1CLEVBQUU7U0FDNUIsQ0FBQztJQUNOLENBQUM7Ozs7SUF6Q0QsSUFBSSxXQUFXO1FBQ1gsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQzdCLENBQUM7Ozs7O0lBRUQsSUFBSSxXQUFXLENBQUMsS0FBc0I7UUFDbEMsSUFBSSxJQUFJLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQyxZQUFZLEtBQUssS0FBSyxFQUFFO1lBQ2xELElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztZQUNuQyxJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQztZQUN6QixPQUFPO1NBQ1Y7UUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU87Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLFFBQVEsR0FBRyxLQUFLLEVBQUMsQ0FBQztRQUVqRCxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztRQUUxQixJQUFJLEtBQUssRUFBRTtZQUNQLElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztTQUNyQztJQUNMLENBQUM7Ozs7OztJQXlCTyxVQUFVLENBQUMsS0FBcUI7UUFDcEMsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLElBQUksRUFBRTs7Z0JBQ2pCLFdBQVcsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLGlCQUFpQjtZQUM5QyxJQUFJLENBQUMsV0FBVyxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRTtnQkFDOUQsV0FBVyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQzthQUMzRDtZQUVELElBQUksV0FBVyxFQUFFO2dCQUNiLE9BQU8sV0FBVyxDQUFDLEdBQUc7Ozs7Z0JBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLG1CQUFxQixHQUFHLEVBQUEsRUFBQyxDQUFDO2FBQzdEO1NBQ0o7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOzs7O0lBRUQsVUFBVTtRQUNOLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNaLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRzs7OztZQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFDLENBQUM7WUFDakQsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsQ0FBQztTQUMzQjtJQUNMLENBQUM7Ozs7OztJQUVELE9BQU8sQ0FBQyxHQUFvQixFQUFFLE1BQWM7O2NBQ2xDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUM7UUFDdkMsSUFBSSxRQUFRLEdBQUcsQ0FBQyxDQUFDLEVBQUU7O2dCQUNYLFFBQVEsR0FBRyxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUM7WUFFbEMsSUFBSSxRQUFRLEdBQUcsQ0FBQyxFQUFFO2dCQUNkLFFBQVEsR0FBRyxDQUFDLENBQUM7YUFDaEI7aUJBQU0sSUFBSSxRQUFRLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ3JDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQzthQUMvQjs7a0JBRUssR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQzdCLEdBQUcsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLEdBQUcsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztZQUVoQixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7U0FDckI7SUFDTCxDQUFDOzs7OztJQUVELFNBQVMsQ0FBQyxHQUFvQjtRQUMxQixJQUFJLEdBQUcsRUFBRTtZQUNMLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxHQUFHLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO2FBQzNCOztrQkFDSyxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDO1lBQ2xDLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUNWLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDekIsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO2FBQ3JCO1NBQ0o7SUFDTCxDQUFDOzs7OztJQUVELE1BQU0sQ0FBQyxHQUFvQjtRQUN2QixJQUFJLEdBQUcsRUFBRTtZQUNMLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BCLDBCQUEwQjtTQUM3QjtJQUNMLENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLEdBQW9COztjQUN0QixPQUFPLEdBQUcsSUFBSSwyQkFBMkIsQ0FBRTtZQUM3QyxPQUFPLEVBQUUsSUFBSTtZQUNiLE9BQU8sRUFBRSxJQUFJO1NBQ2hCLENBQUM7O2NBRUksS0FBSyxHQUFHLElBQUksNEJBQTRCLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxPQUFPLENBQUM7UUFDbkYsSUFBSSxDQUFDLFdBQVcsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFFckQsSUFBSSxLQUFLLENBQUMsZ0JBQWdCLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxFQUFFO1lBQzVDLE9BQU8sT0FBTyxDQUFDO1NBQ2xCO1FBRUQsSUFBSSxHQUFHLEVBQUU7WUFDTCxLQUFLLE1BQU0sR0FBRyxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7Z0JBQzVCLEtBQUssTUFBTSxTQUFTLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtvQkFDdEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxPQUFPLENBQUMsRUFBRTt3QkFDeEMsT0FBTyxPQUFPLENBQUM7cUJBQ2xCO2lCQUNKO2FBQ0o7U0FDSjtRQUVELE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7Ozs7OztJQUVELFlBQVksQ0FBQyxHQUFvQixFQUFFLE1BQTBCOztjQUNuRCxRQUFRLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDO1FBRXJDLElBQUksTUFBTSxDQUFDLElBQUksS0FBSyxVQUFVLEVBQUU7WUFDNUIsSUFBSSxRQUFRLEVBQUU7Z0JBQ1YsT0FBTyxRQUFRLENBQUMsSUFBSSxDQUFDO2FBQ3hCO1NBQ0o7UUFFRCxJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO1lBQzNCLE9BQU8sUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztTQUNsQztRQUVELElBQUksTUFBTSxDQUFDLElBQUksS0FBSyxNQUFNLEVBQUU7WUFDeEIsSUFBSSxRQUFRLEVBQUU7Z0JBQ1YsT0FBTyxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDNUU7U0FDSjtRQUVELE9BQU8sUUFBUSxJQUFJLEVBQUUsQ0FBQztJQUMxQixDQUFDOzs7OztJQUVELGNBQWMsQ0FBQyxNQUEwQjs7WUFDakMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxJQUFJO1FBQzVCLElBQUksTUFBTSxDQUFDLElBQUksS0FBSyxRQUFRLEVBQUU7O2tCQUNwQixRQUFRLEdBQUcsTUFBTSxDQUFDLGNBQWMsSUFBSSxHQUFHO1lBQzdDLFVBQVUsR0FBRyxHQUFHLE1BQU0sQ0FBQyxJQUFJLEtBQUssUUFBUSxHQUFHLENBQUM7U0FDL0M7UUFDRCxPQUFPLFVBQVUsQ0FBQztJQUN0QixDQUFDO0NBQ0o7OztJQXhLRyxrQ0FBc0I7O0lBQ3RCLG9DQUFtQzs7SUFDbkMsMkNBQTBDOztJQUMxQyxpQ0FBNkI7Ozs7O0lBRTdCLHlDQUFzQzs7Ozs7SUFDdEMsd0NBQTBDOzs7OztJQXNCUCx3Q0FBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuIC8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50LWVzNic7XHJcbmltcG9ydCB7IFZhbGlkYXRlRHluYW1pY1RhYmxlUm93RXZlbnQgfSBmcm9tICcuLi8uLi8uLi9ldmVudHMvdmFsaWRhdGUtZHluYW1pYy10YWJsZS1yb3cuZXZlbnQnO1xyXG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4vLi4vLi4vLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRm9ybUZpZWxkTW9kZWwgfSBmcm9tICcuLy4uL2NvcmUvZm9ybS1maWVsZC5tb2RlbCc7XHJcbmltcG9ydCB7IEZvcm1XaWRnZXRNb2RlbCB9IGZyb20gJy4vLi4vY29yZS9mb3JtLXdpZGdldC5tb2RlbCc7XHJcbmltcG9ydCB7IENlbGxWYWxpZGF0b3IgfSBmcm9tICcuL2NlbGwtdmFsaWRhdG9yLm1vZGVsJztcclxuaW1wb3J0IHsgRGF0ZUNlbGxWYWxpZGF0b3IgfSBmcm9tICcuL2RhdGUtY2VsbC12YWxpZGF0b3ItbW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljUm93VmFsaWRhdGlvblN1bW1hcnkgfSBmcm9tICcuL2R5bmFtaWMtcm93LXZhbGlkYXRpb24tc3VtbWFyeS5tb2RlbCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZUNvbHVtbiB9IGZyb20gJy4vZHluYW1pYy10YWJsZS1jb2x1bW4ubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVSb3cgfSBmcm9tICcuL2R5bmFtaWMtdGFibGUtcm93Lm1vZGVsJztcclxuaW1wb3J0IHsgTnVtYmVyQ2VsbFZhbGlkYXRvciB9IGZyb20gJy4vbnVtYmVyLWNlbGwtdmFsaWRhdG9yLm1vZGVsJztcclxuaW1wb3J0IHsgUmVxdWlyZWRDZWxsVmFsaWRhdG9yIH0gZnJvbSAnLi9yZXF1aXJlZC1jZWxsLXZhbGlkYXRvci5tb2RlbCc7XHJcblxyXG5leHBvcnQgY2xhc3MgRHluYW1pY1RhYmxlTW9kZWwgZXh0ZW5kcyBGb3JtV2lkZ2V0TW9kZWwge1xyXG5cclxuICAgIGZpZWxkOiBGb3JtRmllbGRNb2RlbDtcclxuICAgIGNvbHVtbnM6IER5bmFtaWNUYWJsZUNvbHVtbltdID0gW107XHJcbiAgICB2aXNpYmxlQ29sdW1uczogRHluYW1pY1RhYmxlQ29sdW1uW10gPSBbXTtcclxuICAgIHJvd3M6IER5bmFtaWNUYWJsZVJvd1tdID0gW107XHJcblxyXG4gICAgcHJpdmF0ZSBfc2VsZWN0ZWRSb3c6IER5bmFtaWNUYWJsZVJvdztcclxuICAgIHByaXZhdGUgX3ZhbGlkYXRvcnM6IENlbGxWYWxpZGF0b3JbXSA9IFtdO1xyXG5cclxuICAgIGdldCBzZWxlY3RlZFJvdygpOiBEeW5hbWljVGFibGVSb3cge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zZWxlY3RlZFJvdztcclxuICAgIH1cclxuXHJcbiAgICBzZXQgc2VsZWN0ZWRSb3codmFsdWU6IER5bmFtaWNUYWJsZVJvdykge1xyXG4gICAgICAgIGlmICh0aGlzLl9zZWxlY3RlZFJvdyAmJiB0aGlzLl9zZWxlY3RlZFJvdyA9PT0gdmFsdWUpIHtcclxuICAgICAgICAgICAgdGhpcy5fc2VsZWN0ZWRSb3cuc2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5fc2VsZWN0ZWRSb3cgPSBudWxsO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnJvd3MuZm9yRWFjaCgocm93KSA9PiByb3cuc2VsZWN0ZWQgPSBmYWxzZSk7XHJcblxyXG4gICAgICAgIHRoaXMuX3NlbGVjdGVkUm93ID0gdmFsdWU7XHJcblxyXG4gICAgICAgIGlmICh2YWx1ZSkge1xyXG4gICAgICAgICAgICB0aGlzLl9zZWxlY3RlZFJvdy5zZWxlY3RlZCA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKGZpZWxkOiBGb3JtRmllbGRNb2RlbCwgcHJpdmF0ZSBmb3JtU2VydmljZTogRm9ybVNlcnZpY2UpIHtcclxuICAgICAgICBzdXBlcihmaWVsZC5mb3JtLCBmaWVsZC5qc29uKTtcclxuICAgICAgICB0aGlzLmZpZWxkID0gZmllbGQ7XHJcblxyXG4gICAgICAgIGlmIChmaWVsZC5qc29uKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNvbHVtbnMgPSB0aGlzLmdldENvbHVtbnMoZmllbGQpO1xyXG4gICAgICAgICAgICBpZiAoY29sdW1ucykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jb2x1bW5zID0gY29sdW1ucztcclxuICAgICAgICAgICAgICAgIHRoaXMudmlzaWJsZUNvbHVtbnMgPSB0aGlzLmNvbHVtbnMuZmlsdGVyKChjb2wpID0+IGNvbC52aXNpYmxlKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGZpZWxkLmpzb24udmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucm93cyA9IGZpZWxkLmpzb24udmFsdWUubWFwKChvYmopID0+IDxEeW5hbWljVGFibGVSb3c+IHtzZWxlY3RlZDogZmFsc2UsIHZhbHVlOiBvYmp9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5fdmFsaWRhdG9ycyA9IFtcclxuICAgICAgICAgICAgbmV3IFJlcXVpcmVkQ2VsbFZhbGlkYXRvcigpLFxyXG4gICAgICAgICAgICBuZXcgRGF0ZUNlbGxWYWxpZGF0b3IoKSxcclxuICAgICAgICAgICAgbmV3IE51bWJlckNlbGxWYWxpZGF0b3IoKVxyXG4gICAgICAgIF07XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRDb2x1bW5zKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IER5bmFtaWNUYWJsZUNvbHVtbltdIHtcclxuICAgICAgICBpZiAoZmllbGQgJiYgZmllbGQuanNvbikge1xyXG4gICAgICAgICAgICBsZXQgZGVmaW5pdGlvbnMgPSBmaWVsZC5qc29uLmNvbHVtbkRlZmluaXRpb25zO1xyXG4gICAgICAgICAgICBpZiAoIWRlZmluaXRpb25zICYmIGZpZWxkLmpzb24ucGFyYW1zICYmIGZpZWxkLmpzb24ucGFyYW1zLmZpZWxkKSB7XHJcbiAgICAgICAgICAgICAgICBkZWZpbml0aW9ucyA9IGZpZWxkLmpzb24ucGFyYW1zLmZpZWxkLmNvbHVtbkRlZmluaXRpb25zO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoZGVmaW5pdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBkZWZpbml0aW9ucy5tYXAoKG9iaikgPT4gPER5bmFtaWNUYWJsZUNvbHVtbj4gb2JqKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBmbHVzaFZhbHVlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmZpZWxkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZmllbGQudmFsdWUgPSB0aGlzLnJvd3MubWFwKChyKSA9PiByLnZhbHVlKTtcclxuICAgICAgICAgICAgdGhpcy5maWVsZC51cGRhdGVGb3JtKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG1vdmVSb3cocm93OiBEeW5hbWljVGFibGVSb3csIG9mZnNldDogbnVtYmVyKSB7XHJcbiAgICAgICAgY29uc3Qgb2xkSW5kZXggPSB0aGlzLnJvd3MuaW5kZXhPZihyb3cpO1xyXG4gICAgICAgIGlmIChvbGRJbmRleCA+IC0xKSB7XHJcbiAgICAgICAgICAgIGxldCBuZXdJbmRleCA9IChvbGRJbmRleCArIG9mZnNldCk7XHJcblxyXG4gICAgICAgICAgICBpZiAobmV3SW5kZXggPCAwKSB7XHJcbiAgICAgICAgICAgICAgICBuZXdJbmRleCA9IDA7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAobmV3SW5kZXggPj0gdGhpcy5yb3dzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgbmV3SW5kZXggPSB0aGlzLnJvd3MubGVuZ3RoO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb25zdCBhcnIgPSB0aGlzLnJvd3Muc2xpY2UoKTtcclxuICAgICAgICAgICAgYXJyLnNwbGljZShvbGRJbmRleCwgMSk7XHJcbiAgICAgICAgICAgIGFyci5zcGxpY2UobmV3SW5kZXgsIDAsIHJvdyk7XHJcbiAgICAgICAgICAgIHRoaXMucm93cyA9IGFycjtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZmx1c2hWYWx1ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBkZWxldGVSb3cocm93OiBEeW5hbWljVGFibGVSb3cpIHtcclxuICAgICAgICBpZiAocm93KSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnNlbGVjdGVkUm93ID09PSByb3cpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRSb3cgPSBudWxsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IGlkeCA9IHRoaXMucm93cy5pbmRleE9mKHJvdyk7XHJcbiAgICAgICAgICAgIGlmIChpZHggPiAtMSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yb3dzLnNwbGljZShpZHgsIDEpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mbHVzaFZhbHVlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgYWRkUm93KHJvdzogRHluYW1pY1RhYmxlUm93KSB7XHJcbiAgICAgICAgaWYgKHJvdykge1xyXG4gICAgICAgICAgICB0aGlzLnJvd3MucHVzaChyb3cpO1xyXG4gICAgICAgICAgICAvLyB0aGlzLnNlbGVjdGVkUm93ID0gcm93O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZVJvdyhyb3c6IER5bmFtaWNUYWJsZVJvdyk6IER5bmFtaWNSb3dWYWxpZGF0aW9uU3VtbWFyeSB7XHJcbiAgICAgICAgY29uc3Qgc3VtbWFyeSA9IG5ldyBEeW5hbWljUm93VmFsaWRhdGlvblN1bW1hcnkoIHtcclxuICAgICAgICAgICAgaXNWYWxpZDogdHJ1ZSxcclxuICAgICAgICAgICAgbWVzc2FnZTogbnVsbFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb25zdCBldmVudCA9IG5ldyBWYWxpZGF0ZUR5bmFtaWNUYWJsZVJvd0V2ZW50KHRoaXMuZm9ybSwgdGhpcy5maWVsZCwgcm93LCBzdW1tYXJ5KTtcclxuICAgICAgICB0aGlzLmZvcm1TZXJ2aWNlLnZhbGlkYXRlRHluYW1pY1RhYmxlUm93Lm5leHQoZXZlbnQpO1xyXG5cclxuICAgICAgICBpZiAoZXZlbnQuZGVmYXVsdFByZXZlbnRlZCB8fCAhc3VtbWFyeS5pc1ZhbGlkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBzdW1tYXJ5O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHJvdykge1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IGNvbCBvZiB0aGlzLmNvbHVtbnMpIHtcclxuICAgICAgICAgICAgICAgIGZvciAoY29uc3QgdmFsaWRhdG9yIG9mIHRoaXMuX3ZhbGlkYXRvcnMpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIXZhbGlkYXRvci52YWxpZGF0ZShyb3csIGNvbCwgc3VtbWFyeSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHN1bW1hcnk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gc3VtbWFyeTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDZWxsVmFsdWUocm93OiBEeW5hbWljVGFibGVSb3csIGNvbHVtbjogRHluYW1pY1RhYmxlQ29sdW1uKTogYW55IHtcclxuICAgICAgICBjb25zdCByb3dWYWx1ZSA9IHJvdy52YWx1ZVtjb2x1bW4uaWRdO1xyXG5cclxuICAgICAgICBpZiAoY29sdW1uLnR5cGUgPT09ICdEcm9wZG93bicpIHtcclxuICAgICAgICAgICAgaWYgKHJvd1ZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcm93VmFsdWUubmFtZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGNvbHVtbi50eXBlID09PSAnQm9vbGVhbicpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHJvd1ZhbHVlID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGNvbHVtbi50eXBlID09PSAnRGF0ZScpIHtcclxuICAgICAgICAgICAgaWYgKHJvd1ZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbW9tZW50KHJvd1ZhbHVlLnNwbGl0KCdUJylbMF0sICdZWVlZLU1NLUREJykuZm9ybWF0KCdERC1NTS1ZWVlZJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiByb3dWYWx1ZSB8fCAnJztcclxuICAgIH1cclxuXHJcbiAgICBnZXREaXNwbGF5VGV4dChjb2x1bW46IER5bmFtaWNUYWJsZUNvbHVtbik6IHN0cmluZyB7XHJcbiAgICAgICAgbGV0IGNvbHVtbk5hbWUgPSBjb2x1bW4ubmFtZTtcclxuICAgICAgICBpZiAoY29sdW1uLnR5cGUgPT09ICdBbW91bnQnKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGN1cnJlbmN5ID0gY29sdW1uLmFtb3VudEN1cnJlbmN5IHx8ICckJztcclxuICAgICAgICAgICAgY29sdW1uTmFtZSA9IGAke2NvbHVtbi5uYW1lfSAoJHtjdXJyZW5jeX0pYDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGNvbHVtbk5hbWU7XHJcbiAgICB9XHJcbn1cclxuIl19