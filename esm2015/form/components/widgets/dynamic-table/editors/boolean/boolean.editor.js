/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, Input } from '@angular/core';
import { DynamicTableModel } from './../../dynamic-table.widget.model';
export class BooleanEditorComponent {
    /**
     * @param {?} row
     * @param {?} column
     * @param {?} event
     * @return {?}
     */
    onValueChanged(row, column, event) {
        /** @type {?} */
        const value = ((/** @type {?} */ (event))).checked;
        row.value[column.id] = value;
    }
}
BooleanEditorComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-boolean-editor',
                template: " <label [attr.for]=\"column.id\">\r\n    <mat-checkbox\r\n        color=\"primary\"\r\n        [id]=\"column.id\"\r\n        [checked]=\"table.getCellValue(row, column)\"\r\n        [required]=\"column.required\"\r\n        [disabled]=\"!column.editable\"\r\n        (change)=\"onValueChanged(row, column, $event)\">\r\n    <span class=\"adf-checkbox-label\">{{column.name}}</span>\r\n    </mat-checkbox>\r\n</label>\r\n",
                styles: [".adf-checkbox-label{position:relative;cursor:pointer;font-size:16px;line-height:24px;margin:0}"]
            }] }
];
BooleanEditorComponent.propDecorators = {
    table: [{ type: Input }],
    row: [{ type: Input }],
    column: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    BooleanEditorComponent.prototype.table;
    /** @type {?} */
    BooleanEditorComponent.prototype.row;
    /** @type {?} */
    BooleanEditorComponent.prototype.column;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vbGVhbi5lZGl0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9keW5hbWljLXRhYmxlL2VkaXRvcnMvYm9vbGVhbi9ib29sZWFuLmVkaXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHakQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFPdkUsTUFBTSxPQUFPLHNCQUFzQjs7Ozs7OztJQVcvQixjQUFjLENBQUMsR0FBb0IsRUFBRSxNQUEwQixFQUFFLEtBQVU7O2NBQ2pFLEtBQUssR0FBWSxDQUFDLG1CQUFtQixLQUFLLEVBQUEsQ0FBQyxDQUFDLE9BQU87UUFDekQsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDO0lBQ2pDLENBQUM7OztZQW5CSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLG9CQUFvQjtnQkFDOUIsZ2JBQW9DOzthQUV2Qzs7O29CQUdJLEtBQUs7a0JBR0wsS0FBSztxQkFHTCxLQUFLOzs7O0lBTk4sdUNBQ3lCOztJQUV6QixxQ0FDcUI7O0lBRXJCLHdDQUMyQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4vKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZUNvbHVtbiB9IGZyb20gJy4vLi4vLi4vZHluYW1pYy10YWJsZS1jb2x1bW4ubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVSb3cgfSBmcm9tICcuLy4uLy4uL2R5bmFtaWMtdGFibGUtcm93Lm1vZGVsJztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlTW9kZWwgfSBmcm9tICcuLy4uLy4uL2R5bmFtaWMtdGFibGUud2lkZ2V0Lm1vZGVsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtYm9vbGVhbi1lZGl0b3InLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Jvb2xlYW4uZWRpdG9yLmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vYm9vbGVhbi5lZGl0b3Iuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBCb29sZWFuRWRpdG9yQ29tcG9uZW50IHtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgdGFibGU6IER5bmFtaWNUYWJsZU1vZGVsO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICByb3c6IER5bmFtaWNUYWJsZVJvdztcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgY29sdW1uOiBEeW5hbWljVGFibGVDb2x1bW47XHJcblxyXG4gICAgb25WYWx1ZUNoYW5nZWQocm93OiBEeW5hbWljVGFibGVSb3csIGNvbHVtbjogRHluYW1pY1RhYmxlQ29sdW1uLCBldmVudDogYW55KSB7XHJcbiAgICAgICAgY29uc3QgdmFsdWU6IGJvb2xlYW4gPSAoPEhUTUxJbnB1dEVsZW1lbnQ+IGV2ZW50KS5jaGVja2VkO1xyXG4gICAgICAgIHJvdy52YWx1ZVtjb2x1bW4uaWRdID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==