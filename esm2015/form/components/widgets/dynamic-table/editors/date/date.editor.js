/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { UserPreferencesService, UserPreferenceValues } from '../../../../../../services/user-preferences.service';
import { MomentDateAdapter } from '../../../../../../utils/momentDateAdapter';
import { MOMENT_DATE_FORMATS } from '../../../../../../utils/moment-date-formats.model';
import { Component, Input } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import moment from 'moment-es6';
import { DynamicTableModel } from './../../dynamic-table.widget.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
const ɵ0 = MOMENT_DATE_FORMATS;
export class DateEditorComponent {
    /**
     * @param {?} dateAdapter
     * @param {?} userPreferencesService
     */
    constructor(dateAdapter, userPreferencesService) {
        this.dateAdapter = dateAdapter;
        this.userPreferencesService = userPreferencesService;
        this.DATE_FORMAT = 'DD-MM-YYYY';
        this.onDestroy$ = new Subject();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.userPreferencesService
            .select(UserPreferenceValues.Locale)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} locale
         * @return {?}
         */
        locale => this.dateAdapter.setLocale(locale)));
        /** @type {?} */
        const momentDateAdapter = (/** @type {?} */ (this.dateAdapter));
        momentDateAdapter.overrideDisplayFormat = this.DATE_FORMAT;
        this.value = moment(this.table.getCellValue(this.row, this.column), this.DATE_FORMAT);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    }
    /**
     * @param {?} newDateValue
     * @return {?}
     */
    onDateChanged(newDateValue) {
        if (newDateValue && newDateValue.value) {
            /* validates the user inputs */
            /** @type {?} */
            const momentDate = moment(newDateValue.value, this.DATE_FORMAT, true);
            if (!momentDate.isValid()) {
                this.row.value[this.column.id] = newDateValue.value;
            }
            else {
                this.row.value[this.column.id] = `${momentDate.format('YYYY-MM-DD')}T00:00:00.000Z`;
                this.table.flushValue();
            }
        }
        else {
            /* removes the date  */
            this.row.value[this.column.id] = '';
        }
    }
}
DateEditorComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-date-editor',
                template: "<div>\r\n    <mat-form-field class=\"adf-date-editor\">\r\n        <label [attr.for]=\"column.id\">{{column.name}} ({{DATE_FORMAT}})</label>\r\n        <input matInput\r\n            id=\"dateInput\"\r\n            type=\"text\"\r\n            [matDatepicker]=\"datePicker\"\r\n            [value]=\"value\"\r\n            [id]=\"column.id\"\r\n            [required]=\"column.required\"\r\n            [disabled]=\"!column.editable\"\r\n            (focusout)=\"onDateChanged($event.srcElement)\"\r\n            (dateChange)=\"onDateChanged($event)\">\r\n        <mat-datepicker-toggle  *ngIf=\"column.editable\" matSuffix [for]=\"datePicker\" class=\"adf-date-editor-button\" ></mat-datepicker-toggle>\r\n    </mat-form-field>\r\n    <mat-datepicker #datePicker [touchUi]=\"true\"></mat-datepicker>\r\n</div>\r\n",
                providers: [
                    { provide: DateAdapter, useClass: MomentDateAdapter },
                    { provide: MAT_DATE_FORMATS, useValue: ɵ0 }
                ],
                styles: [".adf-date-editor{width:100%}.adf-date-editor-button{position:relative;top:25px}"]
            }] }
];
/** @nocollapse */
DateEditorComponent.ctorParameters = () => [
    { type: DateAdapter },
    { type: UserPreferencesService }
];
DateEditorComponent.propDecorators = {
    table: [{ type: Input }],
    row: [{ type: Input }],
    column: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    DateEditorComponent.prototype.DATE_FORMAT;
    /** @type {?} */
    DateEditorComponent.prototype.value;
    /** @type {?} */
    DateEditorComponent.prototype.table;
    /** @type {?} */
    DateEditorComponent.prototype.row;
    /** @type {?} */
    DateEditorComponent.prototype.column;
    /** @type {?} */
    DateEditorComponent.prototype.minDate;
    /** @type {?} */
    DateEditorComponent.prototype.maxDate;
    /**
     * @type {?}
     * @private
     */
    DateEditorComponent.prototype.onDestroy$;
    /**
     * @type {?}
     * @private
     */
    DateEditorComponent.prototype.dateAdapter;
    /**
     * @type {?}
     * @private
     */
    DateEditorComponent.prototype.userPreferencesService;
}
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS5lZGl0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9keW5hbWljLXRhYmxlL2VkaXRvcnMvZGF0ZS9kYXRlLmVkaXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLHNCQUFzQixFQUFFLG9CQUFvQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFFbkgsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFDOUUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sbURBQW1ELENBQUM7QUFDeEYsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQXFCLE1BQU0sZUFBZSxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxXQUFXLEVBQUUsZ0JBQWdCLEVBQTJCLE1BQU0sbUJBQW1CLENBQUM7QUFDM0YsT0FBTyxNQUFNLE1BQU0sWUFBWSxDQUFDO0FBSWhDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO1dBT0csbUJBQW1CO0FBR2pFLE1BQU0sT0FBTyxtQkFBbUI7Ozs7O0lBb0I1QixZQUFvQixXQUFnQyxFQUNoQyxzQkFBOEM7UUFEOUMsZ0JBQVcsR0FBWCxXQUFXLENBQXFCO1FBQ2hDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFuQmxFLGdCQUFXLEdBQVcsWUFBWSxDQUFDO1FBZ0IzQixlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztJQUk1QyxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxzQkFBc0I7YUFDdEIsTUFBTSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQzthQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNoQyxTQUFTOzs7O1FBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBQyxDQUFDOztjQUV2RCxpQkFBaUIsR0FBRyxtQkFBb0IsSUFBSSxDQUFDLFdBQVcsRUFBQTtRQUM5RCxpQkFBaUIsQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBRTNELElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztJQUMxRixDQUFDOzs7O0lBRUQsV0FBVztRQUNQLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDL0IsQ0FBQzs7Ozs7SUFFRCxhQUFhLENBQUMsWUFBNkQ7UUFDdkUsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLEtBQUssRUFBRTs7O2tCQUU5QixVQUFVLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUM7WUFFckUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFDdkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsR0FBSSxZQUFZLENBQUMsS0FBSyxDQUFDO2FBQ3hEO2lCQUFNO2dCQUNILElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEdBQUcsR0FBRyxVQUFVLENBQUMsTUFBTSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQztnQkFDcEYsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsQ0FBQzthQUMzQjtTQUNKO2FBQU07WUFDSCx1QkFBdUI7WUFDdkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsR0FBRyxFQUFFLENBQUM7U0FDdkM7SUFDTCxDQUFDOzs7WUFoRUosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxpQkFBaUI7Z0JBQzNCLDB6QkFBaUM7Z0JBQ2pDLFNBQVMsRUFBRTtvQkFDUCxFQUFDLE9BQU8sRUFBRSxXQUFXLEVBQUUsUUFBUSxFQUFFLGlCQUFpQixFQUFDO29CQUNuRCxFQUFDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxRQUFRLElBQXFCLEVBQUM7aUJBQUM7O2FBRWxFOzs7O1lBaEJRLFdBQVc7WUFMWCxzQkFBc0I7OztvQkE0QjFCLEtBQUs7a0JBR0wsS0FBSztxQkFHTCxLQUFLOzs7O0lBVk4sMENBQW1DOztJQUVuQyxvQ0FBVzs7SUFFWCxvQ0FDeUI7O0lBRXpCLGtDQUNxQjs7SUFFckIscUNBQzJCOztJQUUzQixzQ0FBZ0I7O0lBQ2hCLHNDQUFnQjs7Ozs7SUFFaEIseUNBQTRDOzs7OztJQUVoQywwQ0FBd0M7Ozs7O0lBQ3hDLHFEQUFzRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4vKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgeyBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlLCBVc2VyUHJlZmVyZW5jZVZhbHVlcyB9IGZyb20gJy4uLy4uLy4uLy4uLy4uLy4uL3NlcnZpY2VzL3VzZXItcHJlZmVyZW5jZXMuc2VydmljZSc7XHJcblxyXG5pbXBvcnQgeyBNb21lbnREYXRlQWRhcHRlciB9IGZyb20gJy4uLy4uLy4uLy4uLy4uLy4uL3V0aWxzL21vbWVudERhdGVBZGFwdGVyJztcclxuaW1wb3J0IHsgTU9NRU5UX0RBVEVfRk9STUFUUyB9IGZyb20gJy4uLy4uLy4uLy4uLy4uLy4uL3V0aWxzL21vbWVudC1kYXRlLWZvcm1hdHMubW9kZWwnO1xyXG5pbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEYXRlQWRhcHRlciwgTUFUX0RBVEVfRk9STUFUUywgTWF0RGF0ZXBpY2tlcklucHV0RXZlbnQgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50LWVzNic7XHJcbmltcG9ydCB7IE1vbWVudCB9IGZyb20gJ21vbWVudCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZUNvbHVtbiB9IGZyb20gJy4vLi4vLi4vZHluYW1pYy10YWJsZS1jb2x1bW4ubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVSb3cgfSBmcm9tICcuLy4uLy4uL2R5bmFtaWMtdGFibGUtcm93Lm1vZGVsJztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlTW9kZWwgfSBmcm9tICcuLy4uLy4uL2R5bmFtaWMtdGFibGUud2lkZ2V0Lm1vZGVsJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLWRhdGUtZWRpdG9yJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9kYXRlLmVkaXRvci5odG1sJyxcclxuICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIHtwcm92aWRlOiBEYXRlQWRhcHRlciwgdXNlQ2xhc3M6IE1vbWVudERhdGVBZGFwdGVyfSxcclxuICAgICAgICB7cHJvdmlkZTogTUFUX0RBVEVfRk9STUFUUywgdXNlVmFsdWU6IE1PTUVOVF9EQVRFX0ZPUk1BVFN9XSxcclxuICAgIHN0eWxlVXJsczogWycuL2RhdGUuZWRpdG9yLnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRGF0ZUVkaXRvckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuXHJcbiAgICBEQVRFX0ZPUk1BVDogc3RyaW5nID0gJ0RELU1NLVlZWVknO1xyXG5cclxuICAgIHZhbHVlOiBhbnk7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHRhYmxlOiBEeW5hbWljVGFibGVNb2RlbDtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgcm93OiBEeW5hbWljVGFibGVSb3c7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGNvbHVtbjogRHluYW1pY1RhYmxlQ29sdW1uO1xyXG5cclxuICAgIG1pbkRhdGU6IE1vbWVudDtcclxuICAgIG1heERhdGU6IE1vbWVudDtcclxuXHJcbiAgICBwcml2YXRlIG9uRGVzdHJveSQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgZGF0ZUFkYXB0ZXI6IERhdGVBZGFwdGVyPE1vbWVudD4sXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHVzZXJQcmVmZXJlbmNlc1NlcnZpY2U6IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlc1NlcnZpY2VcclxuICAgICAgICAgICAgLnNlbGVjdChVc2VyUHJlZmVyZW5jZVZhbHVlcy5Mb2NhbGUpXHJcbiAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLm9uRGVzdHJveSQpKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKGxvY2FsZSA9PiB0aGlzLmRhdGVBZGFwdGVyLnNldExvY2FsZShsb2NhbGUpKTtcclxuXHJcbiAgICAgICAgY29uc3QgbW9tZW50RGF0ZUFkYXB0ZXIgPSA8TW9tZW50RGF0ZUFkYXB0ZXI+IHRoaXMuZGF0ZUFkYXB0ZXI7XHJcbiAgICAgICAgbW9tZW50RGF0ZUFkYXB0ZXIub3ZlcnJpZGVEaXNwbGF5Rm9ybWF0ID0gdGhpcy5EQVRFX0ZPUk1BVDtcclxuXHJcbiAgICAgICAgdGhpcy52YWx1ZSA9IG1vbWVudCh0aGlzLnRhYmxlLmdldENlbGxWYWx1ZSh0aGlzLnJvdywgdGhpcy5jb2x1bW4pLCB0aGlzLkRBVEVfRk9STUFUKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQubmV4dCh0cnVlKTtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQuY29tcGxldGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRhdGVDaGFuZ2VkKG5ld0RhdGVWYWx1ZTogTWF0RGF0ZXBpY2tlcklucHV0RXZlbnQ8YW55PiB8IEhUTUxJbnB1dEVsZW1lbnQpIHtcclxuICAgICAgICBpZiAobmV3RGF0ZVZhbHVlICYmIG5ld0RhdGVWYWx1ZS52YWx1ZSkge1xyXG4gICAgICAgICAgICAvKiB2YWxpZGF0ZXMgdGhlIHVzZXIgaW5wdXRzICovXHJcbiAgICAgICAgICAgIGNvbnN0IG1vbWVudERhdGUgPSBtb21lbnQobmV3RGF0ZVZhbHVlLnZhbHVlLCB0aGlzLkRBVEVfRk9STUFULCB0cnVlKTtcclxuXHJcbiAgICAgICAgICAgIGlmICghbW9tZW50RGF0ZS5pc1ZhbGlkKCkpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucm93LnZhbHVlW3RoaXMuY29sdW1uLmlkXSA9ICBuZXdEYXRlVmFsdWUudmFsdWU7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnJvdy52YWx1ZVt0aGlzLmNvbHVtbi5pZF0gPSBgJHttb21lbnREYXRlLmZvcm1hdCgnWVlZWS1NTS1ERCcpfVQwMDowMDowMC4wMDBaYDtcclxuICAgICAgICAgICAgICAgIHRoaXMudGFibGUuZmx1c2hWYWx1ZSgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgLyogcmVtb3ZlcyB0aGUgZGF0ZSAgKi9cclxuICAgICAgICAgICAgdGhpcy5yb3cudmFsdWVbdGhpcy5jb2x1bW4uaWRdID0gJyc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=