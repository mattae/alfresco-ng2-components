/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { UserPreferencesService, UserPreferenceValues } from '../../../../../../services/user-preferences.service';
import { MomentDateAdapter } from '../../../../../../utils/momentDateAdapter';
import { MOMENT_DATE_FORMATS } from '../../../../../../utils/moment-date-formats.model';
import { Component, Input } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import moment from 'moment-es6';
import { DynamicTableModel } from './../../dynamic-table.widget.model';
import { DatetimeAdapter, MAT_DATETIME_FORMATS } from '@mat-datetimepicker/core';
import { MomentDatetimeAdapter, MAT_MOMENT_DATETIME_FORMATS } from '@mat-datetimepicker/moment';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
const ɵ0 = MOMENT_DATE_FORMATS, ɵ1 = MAT_MOMENT_DATETIME_FORMATS;
export class DateTimeEditorComponent {
    /**
     * @param {?} dateAdapter
     * @param {?} userPreferencesService
     */
    constructor(dateAdapter, userPreferencesService) {
        this.dateAdapter = dateAdapter;
        this.userPreferencesService = userPreferencesService;
        this.DATE_TIME_FORMAT = 'DD/MM/YYYY HH:mm';
        this.onDestroy$ = new Subject();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.userPreferencesService
            .select(UserPreferenceValues.Locale)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} locale
         * @return {?}
         */
        locale => this.dateAdapter.setLocale(locale)));
        /** @type {?} */
        const momentDateAdapter = (/** @type {?} */ (this.dateAdapter));
        momentDateAdapter.overrideDisplayFormat = this.DATE_TIME_FORMAT;
        this.value = moment(this.table.getCellValue(this.row, this.column), this.DATE_TIME_FORMAT);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    }
    /**
     * @param {?} newDateValue
     * @return {?}
     */
    onDateChanged(newDateValue) {
        if (newDateValue && newDateValue.value) {
            /** @type {?} */
            const newValue = moment(newDateValue.value, this.DATE_TIME_FORMAT);
            this.row.value[this.column.id] = newDateValue.value.format(this.DATE_TIME_FORMAT);
            this.value = newValue;
            this.table.flushValue();
        }
        else if (newDateValue) {
            /** @type {?} */
            const newValue = moment(newDateValue, this.DATE_TIME_FORMAT);
            this.value = newValue;
            this.row.value[this.column.id] = newDateValue;
            this.table.flushValue();
        }
        else {
            this.row.value[this.column.id] = '';
        }
    }
}
DateTimeEditorComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-datetime-editor',
                template: "<div>\r\n    <mat-form-field class=\"adf-date-editor\">\r\n        <label [attr.for]=\"column.id\">{{column.name}} {{DATE_TIME_FORMAT}}</label>\r\n        <input matInput\r\n            [matDatetimepicker]=\"datetimePicker\"\r\n            [(ngModel)]=\"value\"\r\n            [id]=\"column.id\"\r\n            [required]=\"column.required\"\r\n            [disabled]=\"!column.editable\"\r\n            (focusout)=\"onDateChanged($event.srcElement.value)\"\r\n            (dateChange)=\"onDateChanged($event)\">\r\n            <mat-datetimepicker-toggle\r\n                matSuffix\r\n                [for]=\"datetimePicker\"\r\n                class=\"adf-date-editor-button\">\r\n            </mat-datetimepicker-toggle>\r\n    </mat-form-field>\r\n    <mat-datetimepicker\r\n        #datetimePicker\r\n        type=\"datetime\"\r\n        openOnFocus=\"true\"\r\n        timeInterval=\"5\">\r\n    </mat-datetimepicker>\r\n</div>\r\n",
                providers: [
                    { provide: DateAdapter, useClass: MomentDateAdapter },
                    { provide: MAT_DATE_FORMATS, useValue: ɵ0 },
                    { provide: DatetimeAdapter, useClass: MomentDatetimeAdapter },
                    { provide: MAT_DATETIME_FORMATS, useValue: ɵ1 }
                ],
                styles: [".adf-date-editor{width:100%}.adf-date-editor-button{position:relative;top:25px}"]
            }] }
];
/** @nocollapse */
DateTimeEditorComponent.ctorParameters = () => [
    { type: DateAdapter },
    { type: UserPreferencesService }
];
DateTimeEditorComponent.propDecorators = {
    table: [{ type: Input }],
    row: [{ type: Input }],
    column: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    DateTimeEditorComponent.prototype.DATE_TIME_FORMAT;
    /** @type {?} */
    DateTimeEditorComponent.prototype.value;
    /** @type {?} */
    DateTimeEditorComponent.prototype.table;
    /** @type {?} */
    DateTimeEditorComponent.prototype.row;
    /** @type {?} */
    DateTimeEditorComponent.prototype.column;
    /** @type {?} */
    DateTimeEditorComponent.prototype.minDate;
    /** @type {?} */
    DateTimeEditorComponent.prototype.maxDate;
    /**
     * @type {?}
     * @private
     */
    DateTimeEditorComponent.prototype.onDestroy$;
    /**
     * @type {?}
     * @private
     */
    DateTimeEditorComponent.prototype.dateAdapter;
    /**
     * @type {?}
     * @private
     */
    DateTimeEditorComponent.prototype.userPreferencesService;
}
export { ɵ0, ɵ1 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZXRpbWUuZWRpdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvZHluYW1pYy10YWJsZS9lZGl0b3JzL2RhdGV0aW1lL2RhdGV0aW1lLmVkaXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLHNCQUFzQixFQUFFLG9CQUFvQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFDbkgsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFDOUUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sbURBQW1ELENBQUM7QUFDeEYsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQXFCLE1BQU0sZUFBZSxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxXQUFXLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRSxPQUFPLE1BQU0sTUFBTSxZQUFZLENBQUM7QUFJaEMsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDdkUsT0FBTyxFQUFFLGVBQWUsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ2pGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSwyQkFBMkIsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ2hHLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO1dBT0ksbUJBQW1CLE9BRWYsMkJBQTJCO0FBSTlFLE1BQU0sT0FBTyx1QkFBdUI7Ozs7O0lBb0JoQyxZQUFvQixXQUFnQyxFQUNoQyxzQkFBOEM7UUFEOUMsZ0JBQVcsR0FBWCxXQUFXLENBQXFCO1FBQ2hDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFuQmxFLHFCQUFnQixHQUFXLGtCQUFrQixDQUFDO1FBZ0J0QyxlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztJQUk1QyxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxzQkFBc0I7YUFDdEIsTUFBTSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQzthQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNoQyxTQUFTOzs7O1FBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBQyxDQUFDOztjQUV2RCxpQkFBaUIsR0FBRyxtQkFBb0IsSUFBSSxDQUFDLFdBQVcsRUFBQTtRQUM5RCxpQkFBaUIsQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7UUFFaEUsSUFBSSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDL0YsQ0FBQzs7OztJQUVELFdBQVc7UUFDUCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQy9CLENBQUM7Ozs7O0lBRUQsYUFBYSxDQUFDLFlBQVk7UUFDdEIsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLEtBQUssRUFBRTs7a0JBQzlCLFFBQVEsR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUM7WUFDbEUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsR0FBRyxZQUFZLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUNsRixJQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQztZQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxDQUFDO1NBQzNCO2FBQU0sSUFBSSxZQUFZLEVBQUU7O2tCQUNmLFFBQVEsR0FBRyxNQUFNLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztZQUM1RCxJQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQztZQUN0QixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxHQUFHLFlBQVksQ0FBQztZQUM5QyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxDQUFDO1NBQzNCO2FBQU07WUFDSCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztTQUN2QztJQUNMLENBQUM7OztZQWxFSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLHFCQUFxQjtnQkFDL0IsczdCQUFxQztnQkFDckMsU0FBUyxFQUFFO29CQUNQLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUU7b0JBQ3JELEVBQUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFFBQVEsSUFBcUIsRUFBRTtvQkFDNUQsRUFBRSxPQUFPLEVBQUUsZUFBZSxFQUFFLFFBQVEsRUFBRSxxQkFBcUIsRUFBRTtvQkFDN0QsRUFBRSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsUUFBUSxJQUE2QixFQUFFO2lCQUMzRTs7YUFFSjs7OztZQXJCUSxXQUFXO1lBSlgsc0JBQXNCOzs7b0JBZ0MxQixLQUFLO2tCQUdMLEtBQUs7cUJBR0wsS0FBSzs7OztJQVZOLG1EQUE4Qzs7SUFFOUMsd0NBQVc7O0lBRVgsd0NBQ3lCOztJQUV6QixzQ0FDcUI7O0lBRXJCLHlDQUMyQjs7SUFFM0IsMENBQWdCOztJQUNoQiwwQ0FBZ0I7Ozs7O0lBRWhCLDZDQUE0Qzs7Ozs7SUFFaEMsOENBQXdDOzs7OztJQUN4Qyx5REFBc0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgVXNlclByZWZlcmVuY2VzU2VydmljZSwgVXNlclByZWZlcmVuY2VWYWx1ZXMgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi8uLi9zZXJ2aWNlcy91c2VyLXByZWZlcmVuY2VzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBNb21lbnREYXRlQWRhcHRlciB9IGZyb20gJy4uLy4uLy4uLy4uLy4uLy4uL3V0aWxzL21vbWVudERhdGVBZGFwdGVyJztcclxuaW1wb3J0IHsgTU9NRU5UX0RBVEVfRk9STUFUUyB9IGZyb20gJy4uLy4uLy4uLy4uLy4uLy4uL3V0aWxzL21vbWVudC1kYXRlLWZvcm1hdHMubW9kZWwnO1xyXG5pbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQsIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEYXRlQWRhcHRlciwgTUFUX0RBVEVfRk9STUFUUyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IG1vbWVudCBmcm9tICdtb21lbnQtZXM2JztcclxuaW1wb3J0IHsgTW9tZW50IH0gZnJvbSAnbW9tZW50JztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlQ29sdW1uIH0gZnJvbSAnLi8uLi8uLi9keW5hbWljLXRhYmxlLWNvbHVtbi5tb2RlbCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZVJvdyB9IGZyb20gJy4vLi4vLi4vZHluYW1pYy10YWJsZS1yb3cubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVNb2RlbCB9IGZyb20gJy4vLi4vLi4vZHluYW1pYy10YWJsZS53aWRnZXQubW9kZWwnO1xyXG5pbXBvcnQgeyBEYXRldGltZUFkYXB0ZXIsIE1BVF9EQVRFVElNRV9GT1JNQVRTIH0gZnJvbSAnQG1hdC1kYXRldGltZXBpY2tlci9jb3JlJztcclxuaW1wb3J0IHsgTW9tZW50RGF0ZXRpbWVBZGFwdGVyLCBNQVRfTU9NRU5UX0RBVEVUSU1FX0ZPUk1BVFMgfSBmcm9tICdAbWF0LWRhdGV0aW1lcGlja2VyL21vbWVudCc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1kYXRldGltZS1lZGl0b3InLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2RhdGV0aW1lLmVkaXRvci5odG1sJyxcclxuICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIHsgcHJvdmlkZTogRGF0ZUFkYXB0ZXIsIHVzZUNsYXNzOiBNb21lbnREYXRlQWRhcHRlciB9LFxyXG4gICAgICAgIHsgcHJvdmlkZTogTUFUX0RBVEVfRk9STUFUUywgdXNlVmFsdWU6IE1PTUVOVF9EQVRFX0ZPUk1BVFMgfSxcclxuICAgICAgICB7IHByb3ZpZGU6IERhdGV0aW1lQWRhcHRlciwgdXNlQ2xhc3M6IE1vbWVudERhdGV0aW1lQWRhcHRlciB9LFxyXG4gICAgICAgIHsgcHJvdmlkZTogTUFUX0RBVEVUSU1FX0ZPUk1BVFMsIHVzZVZhbHVlOiBNQVRfTU9NRU5UX0RBVEVUSU1FX0ZPUk1BVFMgfVxyXG4gICAgXSxcclxuICAgIHN0eWxlVXJsczogWycuL2RhdGV0aW1lLmVkaXRvci5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIERhdGVUaW1lRWRpdG9yQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG5cclxuICAgIERBVEVfVElNRV9GT1JNQVQ6IHN0cmluZyA9ICdERC9NTS9ZWVlZIEhIOm1tJztcclxuXHJcbiAgICB2YWx1ZTogYW55O1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICB0YWJsZTogRHluYW1pY1RhYmxlTW9kZWw7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHJvdzogRHluYW1pY1RhYmxlUm93O1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBjb2x1bW46IER5bmFtaWNUYWJsZUNvbHVtbjtcclxuXHJcbiAgICBtaW5EYXRlOiBNb21lbnQ7XHJcbiAgICBtYXhEYXRlOiBNb21lbnQ7XHJcblxyXG4gICAgcHJpdmF0ZSBvbkRlc3Ryb3kkID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGRhdGVBZGFwdGVyOiBEYXRlQWRhcHRlcjxNb21lbnQ+LFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSB1c2VyUHJlZmVyZW5jZXNTZXJ2aWNlOiBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZXNTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5zZWxlY3QoVXNlclByZWZlcmVuY2VWYWx1ZXMuTG9jYWxlKVxyXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5vbkRlc3Ryb3kkKSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZShsb2NhbGUgPT4gdGhpcy5kYXRlQWRhcHRlci5zZXRMb2NhbGUobG9jYWxlKSk7XHJcblxyXG4gICAgICAgIGNvbnN0IG1vbWVudERhdGVBZGFwdGVyID0gPE1vbWVudERhdGVBZGFwdGVyPiB0aGlzLmRhdGVBZGFwdGVyO1xyXG4gICAgICAgIG1vbWVudERhdGVBZGFwdGVyLm92ZXJyaWRlRGlzcGxheUZvcm1hdCA9IHRoaXMuREFURV9USU1FX0ZPUk1BVDtcclxuXHJcbiAgICAgICAgdGhpcy52YWx1ZSA9IG1vbWVudCh0aGlzLnRhYmxlLmdldENlbGxWYWx1ZSh0aGlzLnJvdywgdGhpcy5jb2x1bW4pLCB0aGlzLkRBVEVfVElNRV9GT1JNQVQpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIHRoaXMub25EZXN0cm95JC5uZXh0KHRydWUpO1xyXG4gICAgICAgIHRoaXMub25EZXN0cm95JC5jb21wbGV0ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRGF0ZUNoYW5nZWQobmV3RGF0ZVZhbHVlKSB7XHJcbiAgICAgICAgaWYgKG5ld0RhdGVWYWx1ZSAmJiBuZXdEYXRlVmFsdWUudmFsdWUpIHtcclxuICAgICAgICAgICAgY29uc3QgbmV3VmFsdWUgPSBtb21lbnQobmV3RGF0ZVZhbHVlLnZhbHVlLCB0aGlzLkRBVEVfVElNRV9GT1JNQVQpO1xyXG4gICAgICAgICAgICB0aGlzLnJvdy52YWx1ZVt0aGlzLmNvbHVtbi5pZF0gPSBuZXdEYXRlVmFsdWUudmFsdWUuZm9ybWF0KHRoaXMuREFURV9USU1FX0ZPUk1BVCk7XHJcbiAgICAgICAgICAgIHRoaXMudmFsdWUgPSBuZXdWYWx1ZTtcclxuICAgICAgICAgICAgdGhpcy50YWJsZS5mbHVzaFZhbHVlKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChuZXdEYXRlVmFsdWUpIHtcclxuICAgICAgICAgICAgY29uc3QgbmV3VmFsdWUgPSBtb21lbnQobmV3RGF0ZVZhbHVlLCB0aGlzLkRBVEVfVElNRV9GT1JNQVQpO1xyXG4gICAgICAgICAgICB0aGlzLnZhbHVlID0gbmV3VmFsdWU7XHJcbiAgICAgICAgICAgIHRoaXMucm93LnZhbHVlW3RoaXMuY29sdW1uLmlkXSA9IG5ld0RhdGVWYWx1ZTtcclxuICAgICAgICAgICAgdGhpcy50YWJsZS5mbHVzaFZhbHVlKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5yb3cudmFsdWVbdGhpcy5jb2x1bW4uaWRdID0gJyc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=