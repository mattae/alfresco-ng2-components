/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { LogService } from '../../../../../../services/log.service';
import { Component, Input } from '@angular/core';
import { FormService } from './../../../../../services/form.service';
import { DynamicTableModel } from './../../dynamic-table.widget.model';
export class DropdownEditorComponent {
    /**
     * @param {?} formService
     * @param {?} logService
     */
    constructor(formService, logService) {
        this.formService = formService;
        this.logService = logService;
        this.value = null;
        this.options = [];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        /** @type {?} */
        const field = this.table.field;
        if (field) {
            if (this.column.optionType === 'rest') {
                if (this.table.form && this.table.form.taskId) {
                    this.getValuesByTaskId(field);
                }
                else {
                    this.getValuesByProcessDefinitionId(field);
                }
            }
            else {
                this.options = this.column.options || [];
                this.value = this.table.getCellValue(this.row, this.column);
            }
        }
    }
    /**
     * @param {?} field
     * @return {?}
     */
    getValuesByTaskId(field) {
        this.formService
            .getRestFieldValuesColumn(field.form.taskId, field.id, this.column.id)
            .subscribe((/**
         * @param {?} dynamicTableColumnOption
         * @return {?}
         */
        (dynamicTableColumnOption) => {
            this.column.options = dynamicTableColumnOption || [];
            this.options = this.column.options;
            this.value = this.table.getCellValue(this.row, this.column);
        }), (/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err)));
    }
    /**
     * @param {?} field
     * @return {?}
     */
    getValuesByProcessDefinitionId(field) {
        this.formService
            .getRestFieldValuesColumnByProcessId(field.form.processDefinitionId, field.id, this.column.id)
            .subscribe((/**
         * @param {?} dynamicTableColumnOption
         * @return {?}
         */
        (dynamicTableColumnOption) => {
            this.column.options = dynamicTableColumnOption || [];
            this.options = this.column.options;
            this.value = this.table.getCellValue(this.row, this.column);
        }), (/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err)));
    }
    /**
     * @param {?} row
     * @param {?} column
     * @param {?} event
     * @return {?}
     */
    onValueChanged(row, column, event) {
        /** @type {?} */
        let value = ((/** @type {?} */ (event))).value;
        value = column.options.find((/**
         * @param {?} opt
         * @return {?}
         */
        (opt) => opt.name === value));
        row.value[column.id] = value;
    }
    /**
     * @param {?} error
     * @return {?}
     */
    handleError(error) {
        this.logService.error(error);
    }
}
DropdownEditorComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-dropdown-editor',
                template: "<div class=\"dropdown-editor\">\r\n    <label [attr.for]=\"column.id\">{{column.name}}</label>\r\n    <mat-form-field>\r\n        <mat-select\r\n            floatPlaceholder=\"never\"\r\n            class=\"adf-dropdown-editor-select\"\r\n            [id]=\"column.id\"\r\n            [(ngModel)]=\"value\"\r\n            [required]=\"column.required\"\r\n            [disabled]=\"!column.editable\"\r\n            (selectionChange)=\"onValueChanged(row, column, $event)\">\r\n            <mat-option></mat-option>\r\n            <mat-option *ngFor=\"let opt of options\" [value]=\"opt.name\" [id]=\"opt.id\">{{opt.name}}</mat-option>\r\n        </mat-select>\r\n    </mat-form-field>\r\n</div>\r\n",
                styles: [".adf-dropdown-editor-select{width:100%}"]
            }] }
];
/** @nocollapse */
DropdownEditorComponent.ctorParameters = () => [
    { type: FormService },
    { type: LogService }
];
DropdownEditorComponent.propDecorators = {
    table: [{ type: Input }],
    row: [{ type: Input }],
    column: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    DropdownEditorComponent.prototype.value;
    /** @type {?} */
    DropdownEditorComponent.prototype.options;
    /** @type {?} */
    DropdownEditorComponent.prototype.table;
    /** @type {?} */
    DropdownEditorComponent.prototype.row;
    /** @type {?} */
    DropdownEditorComponent.prototype.column;
    /** @type {?} */
    DropdownEditorComponent.prototype.formService;
    /**
     * @type {?}
     * @private
     */
    DropdownEditorComponent.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24uZWRpdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvZHluYW1pYy10YWJsZS9lZGl0b3JzL2Ryb3Bkb3duL2Ryb3Bkb3duLmVkaXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUlyRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQU92RSxNQUFNLE9BQU8sdUJBQXVCOzs7OztJQWNoQyxZQUFtQixXQUF3QixFQUN2QixVQUFzQjtRQUR2QixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN2QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBYjFDLFVBQUssR0FBUSxJQUFJLENBQUM7UUFDbEIsWUFBTyxHQUErQixFQUFFLENBQUM7SUFhekMsQ0FBQzs7OztJQUVELFFBQVE7O2NBQ0UsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSztRQUM5QixJQUFJLEtBQUssRUFBRTtZQUNQLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEtBQUssTUFBTSxFQUFFO2dCQUNuQyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtvQkFDM0MsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNqQztxQkFBTTtvQkFDSCxJQUFJLENBQUMsOEJBQThCLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzlDO2FBQ0o7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sSUFBSSxFQUFFLENBQUM7Z0JBQ3pDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDL0Q7U0FDSjtJQUNMLENBQUM7Ozs7O0lBRUQsaUJBQWlCLENBQUMsS0FBSztRQUNuQixJQUFJLENBQUMsV0FBVzthQUNYLHdCQUF3QixDQUNyQixLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFDakIsS0FBSyxDQUFDLEVBQUUsRUFDUixJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FDakI7YUFDQSxTQUFTOzs7O1FBQ04sQ0FBQyx3QkFBb0QsRUFBRSxFQUFFO1lBQ3JELElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLHdCQUF3QixJQUFJLEVBQUUsQ0FBQztZQUNyRCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO1lBQ25DLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDaEUsQ0FBQzs7OztRQUNELENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUNqQyxDQUFDO0lBQ1YsQ0FBQzs7Ozs7SUFFRCw4QkFBOEIsQ0FBQyxLQUFLO1FBQ2hDLElBQUksQ0FBQyxXQUFXO2FBQ1gsbUNBQW1DLENBQ2hDLEtBQUssQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQzlCLEtBQUssQ0FBQyxFQUFFLEVBQ1IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQ2pCO2FBQ0EsU0FBUzs7OztRQUNOLENBQUMsd0JBQW9ELEVBQUUsRUFBRTtZQUNyRCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sR0FBRyx3QkFBd0IsSUFBSSxFQUFFLENBQUM7WUFDckQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQztZQUNuQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQ2hFLENBQUM7Ozs7UUFDRCxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFDakMsQ0FBQztJQUNWLENBQUM7Ozs7Ozs7SUFFRCxjQUFjLENBQUMsR0FBb0IsRUFBRSxNQUEwQixFQUFFLEtBQVU7O1lBQ25FLEtBQUssR0FBUSxDQUFDLG1CQUFtQixLQUFLLEVBQUEsQ0FBQyxDQUFDLEtBQUs7UUFDakQsS0FBSyxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsSUFBSSxLQUFLLEtBQUssRUFBQyxDQUFDO1FBQ3pELEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQztJQUNqQyxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxLQUFVO1FBQ2xCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2pDLENBQUM7OztZQWpGSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLHFCQUFxQjtnQkFDL0Isc3NCQUFxQzs7YUFFeEM7Ozs7WUFWUSxXQUFXO1lBRlgsVUFBVTs7O29CQWtCZCxLQUFLO2tCQUdMLEtBQUs7cUJBR0wsS0FBSzs7OztJQVROLHdDQUFrQjs7SUFDbEIsMENBQXlDOztJQUV6Qyx3Q0FDeUI7O0lBRXpCLHNDQUNxQjs7SUFFckIseUNBQzJCOztJQUVmLDhDQUErQjs7Ozs7SUFDL0IsNkNBQThCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbiAvKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgeyBMb2dTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vLi4vc2VydmljZXMvbG9nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLy4uLy4uLy4uLy4uLy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZUNvbHVtbk9wdGlvbiB9IGZyb20gJy4vLi4vLi4vZHluYW1pYy10YWJsZS1jb2x1bW4tb3B0aW9uLm1vZGVsJztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlQ29sdW1uIH0gZnJvbSAnLi8uLi8uLi9keW5hbWljLXRhYmxlLWNvbHVtbi5tb2RlbCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZVJvdyB9IGZyb20gJy4vLi4vLi4vZHluYW1pYy10YWJsZS1yb3cubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVNb2RlbCB9IGZyb20gJy4vLi4vLi4vZHluYW1pYy10YWJsZS53aWRnZXQubW9kZWwnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1kcm9wZG93bi1lZGl0b3InLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Ryb3Bkb3duLmVkaXRvci5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2Ryb3Bkb3duLmVkaXRvci5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIERyb3Bkb3duRWRpdG9yQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICB2YWx1ZTogYW55ID0gbnVsbDtcclxuICAgIG9wdGlvbnM6IER5bmFtaWNUYWJsZUNvbHVtbk9wdGlvbltdID0gW107XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHRhYmxlOiBEeW5hbWljVGFibGVNb2RlbDtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgcm93OiBEeW5hbWljVGFibGVSb3c7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGNvbHVtbjogRHluYW1pY1RhYmxlQ29sdW1uO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBmb3JtU2VydmljZTogRm9ybVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBjb25zdCBmaWVsZCA9IHRoaXMudGFibGUuZmllbGQ7XHJcbiAgICAgICAgaWYgKGZpZWxkKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmNvbHVtbi5vcHRpb25UeXBlID09PSAncmVzdCcpIHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnRhYmxlLmZvcm0gJiYgdGhpcy50YWJsZS5mb3JtLnRhc2tJZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZ2V0VmFsdWVzQnlUYXNrSWQoZmllbGQpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmdldFZhbHVlc0J5UHJvY2Vzc0RlZmluaXRpb25JZChmaWVsZCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMgPSB0aGlzLmNvbHVtbi5vcHRpb25zIHx8IFtdO1xyXG4gICAgICAgICAgICAgICAgdGhpcy52YWx1ZSA9IHRoaXMudGFibGUuZ2V0Q2VsbFZhbHVlKHRoaXMucm93LCB0aGlzLmNvbHVtbik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VmFsdWVzQnlUYXNrSWQoZmllbGQpIHtcclxuICAgICAgICB0aGlzLmZvcm1TZXJ2aWNlXHJcbiAgICAgICAgICAgIC5nZXRSZXN0RmllbGRWYWx1ZXNDb2x1bW4oXHJcbiAgICAgICAgICAgICAgICBmaWVsZC5mb3JtLnRhc2tJZCxcclxuICAgICAgICAgICAgICAgIGZpZWxkLmlkLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5jb2x1bW4uaWRcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgKGR5bmFtaWNUYWJsZUNvbHVtbk9wdGlvbjogRHluYW1pY1RhYmxlQ29sdW1uT3B0aW9uW10pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbHVtbi5vcHRpb25zID0gZHluYW1pY1RhYmxlQ29sdW1uT3B0aW9uIHx8IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3B0aW9ucyA9IHRoaXMuY29sdW1uLm9wdGlvbnM7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52YWx1ZSA9IHRoaXMudGFibGUuZ2V0Q2VsbFZhbHVlKHRoaXMucm93LCB0aGlzLmNvbHVtbik7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VmFsdWVzQnlQcm9jZXNzRGVmaW5pdGlvbklkKGZpZWxkKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtU2VydmljZVxyXG4gICAgICAgICAgICAuZ2V0UmVzdEZpZWxkVmFsdWVzQ29sdW1uQnlQcm9jZXNzSWQoXHJcbiAgICAgICAgICAgICAgICBmaWVsZC5mb3JtLnByb2Nlc3NEZWZpbml0aW9uSWQsXHJcbiAgICAgICAgICAgICAgICBmaWVsZC5pZCxcclxuICAgICAgICAgICAgICAgIHRoaXMuY29sdW1uLmlkXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIChkeW5hbWljVGFibGVDb2x1bW5PcHRpb246IER5bmFtaWNUYWJsZUNvbHVtbk9wdGlvbltdKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb2x1bW4ub3B0aW9ucyA9IGR5bmFtaWNUYWJsZUNvbHVtbk9wdGlvbiB8fCBbXTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMgPSB0aGlzLmNvbHVtbi5vcHRpb25zO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudmFsdWUgPSB0aGlzLnRhYmxlLmdldENlbGxWYWx1ZSh0aGlzLnJvdywgdGhpcy5jb2x1bW4pO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIG9uVmFsdWVDaGFuZ2VkKHJvdzogRHluYW1pY1RhYmxlUm93LCBjb2x1bW46IER5bmFtaWNUYWJsZUNvbHVtbiwgZXZlbnQ6IGFueSkge1xyXG4gICAgICAgIGxldCB2YWx1ZTogYW55ID0gKDxIVE1MSW5wdXRFbGVtZW50PiBldmVudCkudmFsdWU7XHJcbiAgICAgICAgdmFsdWUgPSBjb2x1bW4ub3B0aW9ucy5maW5kKChvcHQpID0+IG9wdC5uYW1lID09PSB2YWx1ZSk7XHJcbiAgICAgICAgcm93LnZhbHVlW2NvbHVtbi5pZF0gPSB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBoYW5kbGVFcnJvcihlcnJvcjogYW55KSB7XHJcbiAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKGVycm9yKTtcclxuICAgIH1cclxufVxyXG4iXX0=