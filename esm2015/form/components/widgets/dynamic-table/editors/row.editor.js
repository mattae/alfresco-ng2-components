/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DynamicRowValidationSummary } from './../dynamic-row-validation-summary.model';
import { DynamicTableModel } from './../dynamic-table.widget.model';
export class RowEditorComponent {
    constructor() {
        this.save = new EventEmitter();
        this.cancel = new EventEmitter();
        this.validationSummary = new DynamicRowValidationSummary({ isValid: true, message: '' });
    }
    /**
     * @return {?}
     */
    onCancelChanges() {
        this.cancel.emit({
            table: this.table,
            row: this.row,
            column: this.column
        });
    }
    /**
     * @return {?}
     */
    onSaveChanges() {
        this.validate();
        if (this.isValid()) {
            this.save.emit({
                table: this.table,
                row: this.row,
                column: this.column
            });
        }
    }
    /**
     * @private
     * @return {?}
     */
    isValid() {
        return this.validationSummary && this.validationSummary.isValid;
    }
    /**
     * @private
     * @return {?}
     */
    validate() {
        this.validationSummary = this.table.validateRow(this.row);
    }
}
RowEditorComponent.decorators = [
    { type: Component, args: [{
                selector: 'row-editor',
                template: "<div class=\"row-editor mdl-shadow--2dp\"\r\n    [class.row-editor__invalid]=\"!validationSummary.isValid\">\r\n    <div class=\"mdl-grid\" *ngFor=\"let column of table.columns\">\r\n        <div class=\"mdl-cell mdl-cell--6-col\" [ngSwitch]=\"column.type\">\r\n            <div *ngSwitchCase=\"'Dropdown'\">\r\n                <adf-dropdown-editor\r\n                    [table]=\"table\"\r\n                    [row]=\"row\"\r\n                    [column]=\"column\">\r\n                </adf-dropdown-editor>\r\n                </div>\r\n                <div *ngSwitchCase=\"'Date'\">\r\n                    <adf-date-editor\r\n                    [table]=\"table\"\r\n                    [row]=\"row\"\r\n                    [column]=\"column\">\r\n                </adf-date-editor>\r\n                </div>\r\n                <div *ngSwitchCase=\"'Datetime'\">\r\n                    <adf-datetime-editor\r\n                        [table]=\"table\"\r\n                        [row]=\"row\"\r\n                        [column]=\"column\">\r\n                    </adf-datetime-editor>\r\n                </div>\r\n                <div *ngSwitchCase=\"'Boolean'\">\r\n                <adf-boolean-editor\r\n                    [table]=\"table\"\r\n                    [row]=\"row\"\r\n                    [column]=\"column\">\r\n                </adf-boolean-editor>\r\n                </div>\r\n                <div *ngSwitchDefault>\r\n                <adf-text-editor\r\n                    [table]=\"table\"\r\n                    [row]=\"row\"\r\n                    [column]=\"column\">\r\n                </adf-text-editor>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <error-widget [error]=\"validationSummary\"></error-widget>\r\n    <div>\r\n        <button mat-button (click)=\"onCancelChanges()\">Cancel</button>\r\n        <button mat-button (click)=\"onSaveChanges()\">Save</button>\r\n    </div>\r\n</div>\r\n",
                styles: [".row-editor{padding:8px}.row-editor__validation-summary{visibility:hidden}.row-editor__invalid .row-editor__validation-summary{color:#d50000;visibility:visible;padding:8px 16px}"]
            }] }
];
/** @nocollapse */
RowEditorComponent.ctorParameters = () => [];
RowEditorComponent.propDecorators = {
    table: [{ type: Input }],
    row: [{ type: Input }],
    column: [{ type: Input }],
    save: [{ type: Output }],
    cancel: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    RowEditorComponent.prototype.table;
    /** @type {?} */
    RowEditorComponent.prototype.row;
    /** @type {?} */
    RowEditorComponent.prototype.column;
    /** @type {?} */
    RowEditorComponent.prototype.save;
    /** @type {?} */
    RowEditorComponent.prototype.cancel;
    /** @type {?} */
    RowEditorComponent.prototype.validationSummary;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm93LmVkaXRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2R5bmFtaWMtdGFibGUvZWRpdG9ycy9yb3cuZWRpdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBR3hGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBT3BFLE1BQU0sT0FBTyxrQkFBa0I7SUFtQjNCO1FBUEEsU0FBSSxHQUFzQixJQUFJLFlBQVksRUFBTyxDQUFDO1FBR2xELFdBQU0sR0FBc0IsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUtoRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSwyQkFBMkIsQ0FBQyxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDN0YsQ0FBQzs7OztJQUVELGVBQWU7UUFDWCxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQztZQUNiLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7WUFDYixNQUFNLEVBQUUsSUFBSSxDQUFDLE1BQU07U0FDdEIsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELGFBQWE7UUFDVCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDaEIsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUU7WUFDaEIsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7Z0JBQ1gsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO2dCQUNqQixHQUFHLEVBQUUsSUFBSSxDQUFDLEdBQUc7Z0JBQ2IsTUFBTSxFQUFFLElBQUksQ0FBQyxNQUFNO2FBQ3RCLENBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQzs7Ozs7SUFFTyxPQUFPO1FBQ1gsT0FBTyxJQUFJLENBQUMsaUJBQWlCLElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQztJQUNwRSxDQUFDOzs7OztJQUVPLFFBQVE7UUFDWixJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzlELENBQUM7OztZQXJESixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLFlBQVk7Z0JBQ3RCLHU2REFBZ0M7O2FBRW5DOzs7OztvQkFHSSxLQUFLO2tCQUdMLEtBQUs7cUJBR0wsS0FBSzttQkFHTCxNQUFNO3FCQUdOLE1BQU07Ozs7SUFaUCxtQ0FDeUI7O0lBRXpCLGlDQUNxQjs7SUFFckIsb0NBQzJCOztJQUUzQixrQ0FDa0Q7O0lBRWxELG9DQUNvRDs7SUFFcEQsK0NBQStDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbiAvKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEeW5hbWljUm93VmFsaWRhdGlvblN1bW1hcnkgfSBmcm9tICcuLy4uL2R5bmFtaWMtcm93LXZhbGlkYXRpb24tc3VtbWFyeS5tb2RlbCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZUNvbHVtbiB9IGZyb20gJy4vLi4vZHluYW1pYy10YWJsZS1jb2x1bW4ubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVSb3cgfSBmcm9tICcuLy4uL2R5bmFtaWMtdGFibGUtcm93Lm1vZGVsJztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlTW9kZWwgfSBmcm9tICcuLy4uL2R5bmFtaWMtdGFibGUud2lkZ2V0Lm1vZGVsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdyb3ctZWRpdG9yJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9yb3cuZWRpdG9yLmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vcm93LmVkaXRvci5jc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUm93RWRpdG9yQ29tcG9uZW50IHtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgdGFibGU6IER5bmFtaWNUYWJsZU1vZGVsO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICByb3c6IER5bmFtaWNUYWJsZVJvdztcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgY29sdW1uOiBEeW5hbWljVGFibGVDb2x1bW47XHJcblxyXG4gICAgQE91dHB1dCgpXHJcbiAgICBzYXZlOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgY2FuY2VsOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIHZhbGlkYXRpb25TdW1tYXJ5OiBEeW5hbWljUm93VmFsaWRhdGlvblN1bW1hcnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy52YWxpZGF0aW9uU3VtbWFyeSA9IG5ldyBEeW5hbWljUm93VmFsaWRhdGlvblN1bW1hcnkoeyBpc1ZhbGlkOiB0cnVlLCBtZXNzYWdlOiAnJyB9KTtcclxuICAgIH1cclxuXHJcbiAgICBvbkNhbmNlbENoYW5nZXMoKSB7XHJcbiAgICAgICAgdGhpcy5jYW5jZWwuZW1pdCh7XHJcbiAgICAgICAgICAgIHRhYmxlOiB0aGlzLnRhYmxlLFxyXG4gICAgICAgICAgICByb3c6IHRoaXMucm93LFxyXG4gICAgICAgICAgICBjb2x1bW46IHRoaXMuY29sdW1uXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25TYXZlQ2hhbmdlcygpIHtcclxuICAgICAgICB0aGlzLnZhbGlkYXRlKCk7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNWYWxpZCgpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2F2ZS5lbWl0KHtcclxuICAgICAgICAgICAgICAgIHRhYmxlOiB0aGlzLnRhYmxlLFxyXG4gICAgICAgICAgICAgICAgcm93OiB0aGlzLnJvdyxcclxuICAgICAgICAgICAgICAgIGNvbHVtbjogdGhpcy5jb2x1bW5cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaXNWYWxpZCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy52YWxpZGF0aW9uU3VtbWFyeSAmJiB0aGlzLnZhbGlkYXRpb25TdW1tYXJ5LmlzVmFsaWQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSB2YWxpZGF0ZSgpIHtcclxuICAgICAgICB0aGlzLnZhbGlkYXRpb25TdW1tYXJ5ID0gdGhpcy50YWJsZS52YWxpZGF0ZVJvdyh0aGlzLnJvdyk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==