/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
export class NumberCellValidator {
    constructor() {
        this.supportedTypes = [
            'Number',
            'Amount'
        ];
    }
    /**
     * @param {?} column
     * @return {?}
     */
    isSupported(column) {
        return column && column.required && this.supportedTypes.indexOf(column.type) > -1;
    }
    /**
     * @param {?} value
     * @return {?}
     */
    isNumber(value) {
        if (value === null || value === undefined || value === '') {
            return false;
        }
        return !isNaN(+value);
    }
    /**
     * @param {?} row
     * @param {?} column
     * @param {?=} summary
     * @return {?}
     */
    validate(row, column, summary) {
        if (this.isSupported(column)) {
            /** @type {?} */
            const value = row.value[column.id];
            if (value === null ||
                value === undefined ||
                value === '' ||
                this.isNumber(value)) {
                return true;
            }
            if (summary) {
                summary.isValid = false;
                summary.message = `Field '${column.name}' must be a number.`;
            }
            return false;
        }
        return true;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    NumberCellValidator.prototype.supportedTypes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibnVtYmVyLWNlbGwtdmFsaWRhdG9yLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvZHluYW1pYy10YWJsZS9udW1iZXItY2VsbC12YWxpZGF0b3IubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBd0JBLE1BQU0sT0FBTyxtQkFBbUI7SUFBaEM7UUFFWSxtQkFBYyxHQUFhO1lBQy9CLFFBQVE7WUFDUixRQUFRO1NBQ1gsQ0FBQztJQWlDTixDQUFDOzs7OztJQS9CRyxXQUFXLENBQUMsTUFBMEI7UUFDbEMsT0FBTyxNQUFNLElBQUksTUFBTSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDdEYsQ0FBQzs7Ozs7SUFFRCxRQUFRLENBQUMsS0FBVTtRQUNmLElBQUksS0FBSyxLQUFLLElBQUksSUFBSSxLQUFLLEtBQUssU0FBUyxJQUFJLEtBQUssS0FBSyxFQUFFLEVBQUU7WUFDdkQsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDMUIsQ0FBQzs7Ozs7OztJQUVELFFBQVEsQ0FBQyxHQUFvQixFQUFFLE1BQTBCLEVBQUUsT0FBcUM7UUFFNUYsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxFQUFFOztrQkFDcEIsS0FBSyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQztZQUNsQyxJQUFJLEtBQUssS0FBSyxJQUFJO2dCQUNkLEtBQUssS0FBSyxTQUFTO2dCQUNuQixLQUFLLEtBQUssRUFBRTtnQkFDWixJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUN0QixPQUFPLElBQUksQ0FBQzthQUNmO1lBRUQsSUFBSSxPQUFPLEVBQUU7Z0JBQ1QsT0FBTyxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7Z0JBQ3hCLE9BQU8sQ0FBQyxPQUFPLEdBQUcsVUFBVSxNQUFNLENBQUMsSUFBSSxxQkFBcUIsQ0FBQzthQUNoRTtZQUNELE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztDQUNKOzs7Ozs7SUFwQ0csNkNBR0UiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgQ2VsbFZhbGlkYXRvciB9IGZyb20gJy4vY2VsbC12YWxpZGF0b3IubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljUm93VmFsaWRhdGlvblN1bW1hcnkgfSBmcm9tICcuL2R5bmFtaWMtcm93LXZhbGlkYXRpb24tc3VtbWFyeS5tb2RlbCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZUNvbHVtbiB9IGZyb20gJy4vZHluYW1pYy10YWJsZS1jb2x1bW4ubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVSb3cgfSBmcm9tICcuL2R5bmFtaWMtdGFibGUtcm93Lm1vZGVsJztcclxuXHJcbmV4cG9ydCBjbGFzcyBOdW1iZXJDZWxsVmFsaWRhdG9yIGltcGxlbWVudHMgQ2VsbFZhbGlkYXRvciB7XHJcblxyXG4gICAgcHJpdmF0ZSBzdXBwb3J0ZWRUeXBlczogc3RyaW5nW10gPSBbXHJcbiAgICAgICAgJ051bWJlcicsXHJcbiAgICAgICAgJ0Ftb3VudCdcclxuICAgIF07XHJcblxyXG4gICAgaXNTdXBwb3J0ZWQoY29sdW1uOiBEeW5hbWljVGFibGVDb2x1bW4pOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gY29sdW1uICYmIGNvbHVtbi5yZXF1aXJlZCAmJiB0aGlzLnN1cHBvcnRlZFR5cGVzLmluZGV4T2YoY29sdW1uLnR5cGUpID4gLTE7XHJcbiAgICB9XHJcblxyXG4gICAgaXNOdW1iZXIodmFsdWU6IGFueSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh2YWx1ZSA9PT0gbnVsbCB8fCB2YWx1ZSA9PT0gdW5kZWZpbmVkIHx8IHZhbHVlID09PSAnJykge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gIWlzTmFOKCt2YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgdmFsaWRhdGUocm93OiBEeW5hbWljVGFibGVSb3csIGNvbHVtbjogRHluYW1pY1RhYmxlQ29sdW1uLCBzdW1tYXJ5PzogRHluYW1pY1Jvd1ZhbGlkYXRpb25TdW1tYXJ5KTogYm9vbGVhbiB7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmlzU3VwcG9ydGVkKGNvbHVtbikpIHtcclxuICAgICAgICAgICAgY29uc3QgdmFsdWUgPSByb3cudmFsdWVbY29sdW1uLmlkXTtcclxuICAgICAgICAgICAgaWYgKHZhbHVlID09PSBudWxsIHx8XHJcbiAgICAgICAgICAgICAgICB2YWx1ZSA9PT0gdW5kZWZpbmVkIHx8XHJcbiAgICAgICAgICAgICAgICB2YWx1ZSA9PT0gJycgfHxcclxuICAgICAgICAgICAgICAgIHRoaXMuaXNOdW1iZXIodmFsdWUpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHN1bW1hcnkpIHtcclxuICAgICAgICAgICAgICAgIHN1bW1hcnkuaXNWYWxpZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgc3VtbWFyeS5tZXNzYWdlID0gYEZpZWxkICcke2NvbHVtbi5uYW1lfScgbXVzdCBiZSBhIG51bWJlci5gO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbn1cclxuIl19