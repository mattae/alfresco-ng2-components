/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
export class RequiredCellValidator {
    constructor() {
        this.supportedTypes = [
            'String',
            'Number',
            'Amount',
            'Date',
            'Dropdown'
        ];
    }
    /**
     * @param {?} column
     * @return {?}
     */
    isSupported(column) {
        return column && column.required && this.supportedTypes.indexOf(column.type) > -1;
    }
    /**
     * @param {?} row
     * @param {?} column
     * @param {?=} summary
     * @return {?}
     */
    validate(row, column, summary) {
        if (this.isSupported(column)) {
            /** @type {?} */
            const value = row.value[column.id];
            if (column.required) {
                if (value === null || value === undefined || value === '') {
                    if (summary) {
                        summary.isValid = false;
                        summary.message = `Field '${column.name}' is required.`;
                    }
                    return false;
                }
            }
        }
        return true;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    RequiredCellValidator.prototype.supportedTypes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVxdWlyZWQtY2VsbC12YWxpZGF0b3IubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9keW5hbWljLXRhYmxlL3JlcXVpcmVkLWNlbGwtdmFsaWRhdG9yLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXdCQSxNQUFNLE9BQU8scUJBQXFCO0lBQWxDO1FBRVksbUJBQWMsR0FBYTtZQUMvQixRQUFRO1lBQ1IsUUFBUTtZQUNSLFFBQVE7WUFDUixNQUFNO1lBQ04sVUFBVTtTQUNiLENBQUM7SUFzQk4sQ0FBQzs7Ozs7SUFwQkcsV0FBVyxDQUFDLE1BQTBCO1FBQ2xDLE9BQU8sTUFBTSxJQUFJLE1BQU0sQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ3RGLENBQUM7Ozs7Ozs7SUFFRCxRQUFRLENBQUMsR0FBb0IsRUFBRSxNQUEwQixFQUFFLE9BQXFDO1FBQzVGLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsRUFBRTs7a0JBQ3BCLEtBQUssR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUM7WUFDbEMsSUFBSSxNQUFNLENBQUMsUUFBUSxFQUFFO2dCQUNqQixJQUFJLEtBQUssS0FBSyxJQUFJLElBQUksS0FBSyxLQUFLLFNBQVMsSUFBSSxLQUFLLEtBQUssRUFBRSxFQUFFO29CQUN2RCxJQUFJLE9BQU8sRUFBRTt3QkFDVCxPQUFPLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQzt3QkFDeEIsT0FBTyxDQUFDLE9BQU8sR0FBRyxVQUFVLE1BQU0sQ0FBQyxJQUFJLGdCQUFnQixDQUFDO3FCQUMzRDtvQkFDRCxPQUFPLEtBQUssQ0FBQztpQkFDaEI7YUFDSjtTQUNKO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztDQUNKOzs7Ozs7SUE1QkcsK0NBTUUiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgQ2VsbFZhbGlkYXRvciB9IGZyb20gJy4vY2VsbC12YWxpZGF0b3IubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljUm93VmFsaWRhdGlvblN1bW1hcnkgfSBmcm9tICcuL2R5bmFtaWMtcm93LXZhbGlkYXRpb24tc3VtbWFyeS5tb2RlbCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZUNvbHVtbiB9IGZyb20gJy4vZHluYW1pYy10YWJsZS1jb2x1bW4ubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVSb3cgfSBmcm9tICcuL2R5bmFtaWMtdGFibGUtcm93Lm1vZGVsJztcclxuXHJcbmV4cG9ydCBjbGFzcyBSZXF1aXJlZENlbGxWYWxpZGF0b3IgaW1wbGVtZW50cyBDZWxsVmFsaWRhdG9yIHtcclxuXHJcbiAgICBwcml2YXRlIHN1cHBvcnRlZFR5cGVzOiBzdHJpbmdbXSA9IFtcclxuICAgICAgICAnU3RyaW5nJyxcclxuICAgICAgICAnTnVtYmVyJyxcclxuICAgICAgICAnQW1vdW50JyxcclxuICAgICAgICAnRGF0ZScsXHJcbiAgICAgICAgJ0Ryb3Bkb3duJ1xyXG4gICAgXTtcclxuXHJcbiAgICBpc1N1cHBvcnRlZChjb2x1bW46IER5bmFtaWNUYWJsZUNvbHVtbik6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBjb2x1bW4gJiYgY29sdW1uLnJlcXVpcmVkICYmIHRoaXMuc3VwcG9ydGVkVHlwZXMuaW5kZXhPZihjb2x1bW4udHlwZSkgPiAtMTtcclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZShyb3c6IER5bmFtaWNUYWJsZVJvdywgY29sdW1uOiBEeW5hbWljVGFibGVDb2x1bW4sIHN1bW1hcnk/OiBEeW5hbWljUm93VmFsaWRhdGlvblN1bW1hcnkpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodGhpcy5pc1N1cHBvcnRlZChjb2x1bW4pKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gcm93LnZhbHVlW2NvbHVtbi5pZF07XHJcbiAgICAgICAgICAgIGlmIChjb2x1bW4ucmVxdWlyZWQpIHtcclxuICAgICAgICAgICAgICAgIGlmICh2YWx1ZSA9PT0gbnVsbCB8fCB2YWx1ZSA9PT0gdW5kZWZpbmVkIHx8IHZhbHVlID09PSAnJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChzdW1tYXJ5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHN1bW1hcnkuaXNWYWxpZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdW1tYXJ5Lm1lc3NhZ2UgPSBgRmllbGQgJyR7Y29sdW1uLm5hbWV9JyBpcyByZXF1aXJlZC5gO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==