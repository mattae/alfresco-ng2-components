/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { ENTER, ESCAPE } from '@angular/cdk/keycodes';
import { Component, ElementRef, ViewEncapsulation } from '@angular/core';
import { FormService } from '../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
export class FunctionalGroupWidgetComponent extends WidgetComponent {
    /**
     * @param {?} formService
     * @param {?} elementRef
     */
    constructor(formService, elementRef) {
        super(formService);
        this.formService = formService;
        this.elementRef = elementRef;
        this.groups = [];
        this.minTermLength = 1;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.field) {
            /** @type {?} */
            const group = this.field.value;
            if (group) {
                this.value = group.name;
            }
            /** @type {?} */
            const params = this.field.params;
            if (params && params['restrictWithGroup']) {
                /** @type {?} */
                const restrictWithGroup = (/** @type {?} */ (params['restrictWithGroup']));
                this.groupId = restrictWithGroup.id;
            }
            // Load auto-completion for previously saved value
            if (this.value) {
                this.formService
                    .getWorkflowGroups(this.value, this.groupId)
                    .subscribe((/**
                 * @param {?} groupModel
                 * @return {?}
                 */
                (groupModel) => this.groups = groupModel || []));
            }
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeyUp(event) {
        if (this.value && this.value.length >= this.minTermLength && this.oldValue !== this.value) {
            if (event.keyCode !== ESCAPE && event.keyCode !== ENTER) {
                this.oldValue = this.value;
                this.formService.getWorkflowGroups(this.value, this.groupId)
                    .subscribe((/**
                 * @param {?} group
                 * @return {?}
                 */
                (group) => {
                    this.groups = group || [];
                }));
            }
        }
    }
    /**
     * @return {?}
     */
    flushValue() {
        /** @type {?} */
        const option = this.groups.find((/**
         * @param {?} item
         * @return {?}
         */
        (item) => item.name.toLocaleLowerCase() === this.value.toLocaleLowerCase()));
        if (option) {
            this.field.value = option;
            this.value = option.name;
        }
        else {
            this.field.value = null;
            this.value = null;
        }
        this.field.updateForm();
    }
    /**
     * @param {?} item
     * @param {?} event
     * @return {?}
     */
    onItemClick(item, event) {
        if (item) {
            this.field.value = item;
            this.value = item.name;
        }
        if (event) {
            event.preventDefault();
        }
    }
    /**
     * @param {?} item
     * @return {?}
     */
    onItemSelect(item) {
        if (item) {
            this.field.value = item;
            this.value = item.name;
        }
    }
}
FunctionalGroupWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'functional-group-widget',
                template: "<div class=\"adf-group-widget {{field.className}}\"\r\n     [class.is-dirty]=\"value\"\r\n     [class.adf-invalid]=\"!field.isValid\" [class.adf-readonly]=\"field.readOnly\" id=\"functional-group-div\">\r\n    <mat-form-field>\r\n        <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}<span *ngIf=\"isRequired()\">*</span></label>\r\n        <input matInput\r\n               class=\"adf-input\"\r\n               type=\"text\"\r\n               [id]=\"field.id\"\r\n               [(ngModel)]=\"value\"\r\n               (keyup)=\"onKeyUp($event)\"\r\n               [disabled]=\"field.readOnly\"\r\n               placeholder=\"{{field.placeholder}}\"\r\n               [matAutocomplete]=\"auto\">\r\n        <mat-autocomplete #auto=\"matAutocomplete\" (optionSelected)=\"onItemSelect($event.option.value)\">\r\n            <mat-option *ngFor=\"let item of groups\"\r\n                       [id]=\"field.id +'-'+item.id\"\r\n                       (click)=\"onItemClick(item, $event)\"  [value]=\"item\">\r\n                <span>{{item.name}}</span>\r\n            </mat-option>\r\n        </mat-autocomplete>\r\n\r\n    </mat-form-field>\r\n    <error-widget [error]=\"field.validationSummary\"></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n</div>\r\n",
                host: baseHost,
                encapsulation: ViewEncapsulation.None,
                styles: [".adf-group-widget{width:100%}"]
            }] }
];
/** @nocollapse */
FunctionalGroupWidgetComponent.ctorParameters = () => [
    { type: FormService },
    { type: ElementRef }
];
if (false) {
    /** @type {?} */
    FunctionalGroupWidgetComponent.prototype.value;
    /** @type {?} */
    FunctionalGroupWidgetComponent.prototype.oldValue;
    /** @type {?} */
    FunctionalGroupWidgetComponent.prototype.groups;
    /** @type {?} */
    FunctionalGroupWidgetComponent.prototype.minTermLength;
    /** @type {?} */
    FunctionalGroupWidgetComponent.prototype.groupId;
    /** @type {?} */
    FunctionalGroupWidgetComponent.prototype.formService;
    /** @type {?} */
    FunctionalGroupWidgetComponent.prototype.elementRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZnVuY3Rpb25hbC1ncm91cC53aWRnZXQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9mdW5jdGlvbmFsLWdyb3VwL2Z1bmN0aW9uYWwtZ3JvdXAud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3RELE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFVLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2pGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUU3RCxPQUFPLEVBQUUsUUFBUSxFQUFHLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBU25FLE1BQU0sT0FBTyw4QkFBK0IsU0FBUSxlQUFlOzs7OztJQVEvRCxZQUFtQixXQUF3QixFQUN4QixVQUFzQjtRQUNwQyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7UUFGTCxnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN4QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBTHpDLFdBQU0sR0FBaUIsRUFBRSxDQUFDO1FBQzFCLGtCQUFhLEdBQVcsQ0FBQyxDQUFDO0lBTTFCLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFOztrQkFDTixLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLO1lBQzlCLElBQUksS0FBSyxFQUFFO2dCQUNQLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQzthQUMzQjs7a0JBRUssTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTTtZQUNoQyxJQUFJLE1BQU0sSUFBSSxNQUFNLENBQUMsbUJBQW1CLENBQUMsRUFBRTs7c0JBQ2pDLGlCQUFpQixHQUFHLG1CQUFhLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxFQUFBO2dCQUNsRSxJQUFJLENBQUMsT0FBTyxHQUFHLGlCQUFpQixDQUFDLEVBQUUsQ0FBQzthQUN2QztZQUVELGtEQUFrRDtZQUNsRCxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7Z0JBQ1osSUFBSSxDQUFDLFdBQVc7cUJBQ1gsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDO3FCQUMzQyxTQUFTOzs7O2dCQUFDLENBQUMsVUFBd0IsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxVQUFVLElBQUksRUFBRSxFQUFDLENBQUM7YUFDaEY7U0FDSjtJQUNMLENBQUM7Ozs7O0lBRUQsT0FBTyxDQUFDLEtBQW9CO1FBQ3hCLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFLLElBQUksQ0FBQyxRQUFRLEtBQUssSUFBSSxDQUFDLEtBQUssRUFBRTtZQUN4RixJQUFJLEtBQUssQ0FBQyxPQUFPLEtBQUssTUFBTSxJQUFJLEtBQUssQ0FBQyxPQUFPLEtBQUssS0FBSyxFQUFFO2dCQUNyRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7Z0JBQzNCLElBQUksQ0FBQyxXQUFXLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDO3FCQUN2RCxTQUFTOzs7O2dCQUFDLENBQUMsS0FBbUIsRUFBRSxFQUFFO29CQUMvQixJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssSUFBSSxFQUFFLENBQUM7Z0JBQzlCLENBQUMsRUFBQyxDQUFDO2FBQ1Y7U0FDSjtJQUNMLENBQUM7Ozs7SUFFRCxVQUFVOztjQUNBLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUk7Ozs7UUFBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxLQUFLLElBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWlCLEVBQUUsRUFBQztRQUUzRyxJQUFJLE1BQU0sRUFBRTtZQUNSLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQztZQUMxQixJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUM7U0FDNUI7YUFBTTtZQUNILElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztZQUN4QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztTQUNyQjtRQUVELElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDNUIsQ0FBQzs7Ozs7O0lBRUQsV0FBVyxDQUFDLElBQWdCLEVBQUUsS0FBWTtRQUN0QyxJQUFJLElBQUksRUFBRTtZQUNOLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztZQUN4QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDMUI7UUFDRCxJQUFJLEtBQUssRUFBRTtZQUNQLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUMxQjtJQUNMLENBQUM7Ozs7O0lBRUQsWUFBWSxDQUFDLElBQWdCO1FBQ3pCLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1lBQ3hCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztTQUMxQjtJQUNMLENBQUM7OztZQW5GSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLHlCQUF5QjtnQkFDbkMscTNDQUE2QztnQkFFN0MsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2FBQ3hDOzs7O1lBVlEsV0FBVztZQURBLFVBQVU7Ozs7SUFjMUIsK0NBQWM7O0lBQ2Qsa0RBQWlCOztJQUNqQixnREFBMEI7O0lBQzFCLHVEQUEwQjs7SUFDMUIsaURBQWdCOztJQUVKLHFEQUErQjs7SUFDL0Isb0RBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbiAvKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgeyBFTlRFUiwgRVNDQVBFIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2tleWNvZGVzJztcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBFbGVtZW50UmVmLCBPbkluaXQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgR3JvdXBNb2RlbCB9IGZyb20gJy4vLi4vY29yZS9ncm91cC5tb2RlbCc7XHJcbmltcG9ydCB7IGJhc2VIb3N0ICwgV2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi8uLi93aWRnZXQuY29tcG9uZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdmdW5jdGlvbmFsLWdyb3VwLXdpZGdldCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vZnVuY3Rpb25hbC1ncm91cC53aWRnZXQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9mdW5jdGlvbmFsLWdyb3VwLndpZGdldC5zY3NzJ10sXHJcbiAgICBob3N0OiBiYXNlSG9zdCxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIEZ1bmN0aW9uYWxHcm91cFdpZGdldENvbXBvbmVudCBleHRlbmRzIFdpZGdldENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgdmFsdWU6IHN0cmluZztcclxuICAgIG9sZFZhbHVlOiBzdHJpbmc7XHJcbiAgICBncm91cHM6IEdyb3VwTW9kZWxbXSA9IFtdO1xyXG4gICAgbWluVGVybUxlbmd0aDogbnVtYmVyID0gMTtcclxuICAgIGdyb3VwSWQ6IHN0cmluZztcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZm9ybVNlcnZpY2U6IEZvcm1TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIGVsZW1lbnRSZWY6IEVsZW1lbnRSZWYpIHtcclxuICAgICAgICAgc3VwZXIoZm9ybVNlcnZpY2UpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmZpZWxkKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGdyb3VwID0gdGhpcy5maWVsZC52YWx1ZTtcclxuICAgICAgICAgICAgaWYgKGdyb3VwKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnZhbHVlID0gZ3JvdXAubmFtZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgcGFyYW1zID0gdGhpcy5maWVsZC5wYXJhbXM7XHJcbiAgICAgICAgICAgIGlmIChwYXJhbXMgJiYgcGFyYW1zWydyZXN0cmljdFdpdGhHcm91cCddKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByZXN0cmljdFdpdGhHcm91cCA9IDxHcm91cE1vZGVsPiBwYXJhbXNbJ3Jlc3RyaWN0V2l0aEdyb3VwJ107XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdyb3VwSWQgPSByZXN0cmljdFdpdGhHcm91cC5pZDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgLy8gTG9hZCBhdXRvLWNvbXBsZXRpb24gZm9yIHByZXZpb3VzbHkgc2F2ZWQgdmFsdWVcclxuICAgICAgICAgICAgaWYgKHRoaXMudmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZm9ybVNlcnZpY2VcclxuICAgICAgICAgICAgICAgICAgICAuZ2V0V29ya2Zsb3dHcm91cHModGhpcy52YWx1ZSwgdGhpcy5ncm91cElkKVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoKGdyb3VwTW9kZWw6IEdyb3VwTW9kZWxbXSkgPT4gdGhpcy5ncm91cHMgPSBncm91cE1vZGVsIHx8IFtdKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvbktleVVwKGV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XHJcbiAgICAgICAgaWYgKHRoaXMudmFsdWUgJiYgdGhpcy52YWx1ZS5sZW5ndGggPj0gdGhpcy5taW5UZXJtTGVuZ3RoICAmJiB0aGlzLm9sZFZhbHVlICE9PSB0aGlzLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIGlmIChldmVudC5rZXlDb2RlICE9PSBFU0NBUEUgJiYgZXZlbnQua2V5Q29kZSAhPT0gRU5URVIpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMub2xkVmFsdWUgPSB0aGlzLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mb3JtU2VydmljZS5nZXRXb3JrZmxvd0dyb3Vwcyh0aGlzLnZhbHVlLCB0aGlzLmdyb3VwSWQpXHJcbiAgICAgICAgICAgICAgICAgICAgLnN1YnNjcmliZSgoZ3JvdXA6IEdyb3VwTW9kZWxbXSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdyb3VwcyA9IGdyb3VwIHx8IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGZsdXNoVmFsdWUoKSB7XHJcbiAgICAgICAgY29uc3Qgb3B0aW9uID0gdGhpcy5ncm91cHMuZmluZCgoaXRlbSkgPT4gaXRlbS5uYW1lLnRvTG9jYWxlTG93ZXJDYXNlKCkgPT09IHRoaXMudmFsdWUudG9Mb2NhbGVMb3dlckNhc2UoKSk7XHJcblxyXG4gICAgICAgIGlmIChvcHRpb24pIHtcclxuICAgICAgICAgICAgdGhpcy5maWVsZC52YWx1ZSA9IG9wdGlvbjtcclxuICAgICAgICAgICAgdGhpcy52YWx1ZSA9IG9wdGlvbi5uYW1lO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuZmllbGQudmFsdWUgPSBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLnZhbHVlID0gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuZmllbGQudXBkYXRlRm9ybSgpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uSXRlbUNsaWNrKGl0ZW06IEdyb3VwTW9kZWwsIGV2ZW50OiBFdmVudCkge1xyXG4gICAgICAgIGlmIChpdGVtKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZmllbGQudmFsdWUgPSBpdGVtO1xyXG4gICAgICAgICAgICB0aGlzLnZhbHVlID0gaXRlbS5uYW1lO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25JdGVtU2VsZWN0KGl0ZW06IEdyb3VwTW9kZWwpIHtcclxuICAgICAgICBpZiAoaXRlbSkge1xyXG4gICAgICAgICAgICB0aGlzLmZpZWxkLnZhbHVlID0gaXRlbTtcclxuICAgICAgICAgICAgdGhpcy52YWx1ZSA9IGl0ZW0ubmFtZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19