/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, ViewEncapsulation } from '@angular/core';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
export class MultilineTextWidgetComponentComponent extends WidgetComponent {
    /**
     * @param {?} formService
     */
    constructor(formService) {
        super(formService);
        this.formService = formService;
    }
}
MultilineTextWidgetComponentComponent.decorators = [
    { type: Component, args: [{
                selector: 'multiline-text-widget',
                template: "<div class=\"adf-multiline-text-widget {{field.className}}\"\r\n     [class.adf-invalid]=\"!field.isValid\" [class.adf-readonly]=\"field.readOnly\">\r\n    <mat-form-field floatPlaceholder=\"never\">\r\n        <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}<span *ngIf=\"isRequired()\">*</span></label>\r\n        <textarea matInput class=\"adf-input\"\r\n                  matTextareaAutosize\r\n                  type=\"text\"\r\n                  rows=\"3\"\r\n                  [id]=\"field.id\"\r\n                  [required]=\"isRequired()\"\r\n                  [(ngModel)]=\"field.value\"\r\n                  (ngModelChange)=\"onFieldChanged(field)\"\r\n                  [disabled]=\"field.readOnly || readOnly\"\r\n                  placeholder=\"{{field.placeholder}}\">\r\n        </textarea>\r\n    </mat-form-field>\r\n    <div *ngIf=\"field.maxLength > 0\" class=\"adf-multiline-word-counter\">\r\n        <span>{{field?.value?.length || 0}}/{{field.maxLength}}</span>\r\n    </div>\r\n    <error-widget [error]=\"field.validationSummary\"></error-widget>\r\n    <error-widget class=\"adf-multiline-required-message\" *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n</div>\r\n\r\n",
                host: baseHost,
                encapsulation: ViewEncapsulation.None,
                styles: [".adf-multiline-text-widget{width:100%}.adf-multiline-word-counter{float:right;margin-top:-20px!important;min-height:24px;min-width:1px;font-size:12px;line-height:14px;overflow:hidden;transition:.3s cubic-bezier(.55,0,.55,.2);opacity:1;padding-top:5px;text-align:right;padding-right:2px;padding-left:0}.adf-multiline-required-message{display:flex}"]
            }] }
];
/** @nocollapse */
MultilineTextWidgetComponentComponent.ctorParameters = () => [
    { type: FormService }
];
if (false) {
    /** @type {?} */
    MultilineTextWidgetComponentComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXVsdGlsaW5lLXRleHQud2lkZ2V0LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvbXVsdGlsaW5lLXRleHQvbXVsdGlsaW5lLXRleHQud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsU0FBUyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzdELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUMvRCxPQUFPLEVBQUUsUUFBUSxFQUFHLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBU25FLE1BQU0sT0FBTyxxQ0FBc0MsU0FBUSxlQUFlOzs7O0lBRXRFLFlBQW1CLFdBQXdCO1FBQ3ZDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztRQURKLGdCQUFXLEdBQVgsV0FBVyxDQUFhO0lBRTNDLENBQUM7OztZQVhKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsdUJBQXVCO2dCQUNqQyxveENBQTJDO2dCQUUzQyxJQUFJLEVBQUUsUUFBUTtnQkFDZCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7YUFDeEM7Ozs7WUFUUSxXQUFXOzs7O0lBWUosNERBQStCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLy4uLy4uLy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XHJcbmltcG9ydCB7IGJhc2VIb3N0ICwgV2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi8uLi93aWRnZXQuY29tcG9uZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdtdWx0aWxpbmUtdGV4dC13aWRnZXQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL211bHRpbGluZS10ZXh0LndpZGdldC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL211bHRpbGluZS10ZXh0LndpZGdldC5zY3NzJ10sXHJcbiAgICBob3N0OiBiYXNlSG9zdCxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIE11bHRpbGluZVRleHRXaWRnZXRDb21wb25lbnRDb21wb25lbnQgZXh0ZW5kcyBXaWRnZXRDb21wb25lbnQgIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZm9ybVNlcnZpY2U6IEZvcm1TZXJ2aWNlKSB7XHJcbiAgICAgICAgc3VwZXIoZm9ybVNlcnZpY2UpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=