/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector no-input-rename   */
import { Component, ViewEncapsulation } from '@angular/core';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
import { DecimalNumberPipe } from '../../../../pipes/decimal-number.pipe';
export class NumberWidgetComponent extends WidgetComponent {
    /**
     * @param {?} formService
     * @param {?} decimalNumberPipe
     */
    constructor(formService, decimalNumberPipe) {
        super(formService);
        this.formService = formService;
        this.decimalNumberPipe = decimalNumberPipe;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.field.readOnly) {
            this.displayValue = this.decimalNumberPipe.transform(this.field.value);
        }
        else {
            this.displayValue = this.field.value;
        }
    }
}
NumberWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'number-widget',
                template: "<div class=\"adf-textfield adf-number-widget {{field.className}}\"\r\n     [class.adf-invalid]=\"!field.isValid\" [class.adf-readonly]=\"field.readOnly\">\r\n    <mat-form-field>\r\n        <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}<span *ngIf=\"isRequired()\">*</span></label>\r\n        <input matInput\r\n               class=\"adf-input\"\r\n               type=\"text\"\r\n               pattern=\"-?[0-9]*(\\.[0-9]+)?\"\r\n               [id]=\"field.id\"\r\n               [required]=\"isRequired()\"\r\n               [value]=\"displayValue\"\r\n               [(ngModel)]=\"field.value\"\r\n               (ngModelChange)=\"onFieldChanged(field)\"\r\n               [disabled]=\"field.readOnly\"\r\n               placeholder=\"{{field.placeholder}}\">\r\n    </mat-form-field>\r\n    <error-widget [error]=\"field.validationSummary\" ></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n</div>\r\n",
                host: baseHost,
                encapsulation: ViewEncapsulation.None,
                styles: [".adf-number-widget{width:100%}"]
            }] }
];
/** @nocollapse */
NumberWidgetComponent.ctorParameters = () => [
    { type: FormService },
    { type: DecimalNumberPipe }
];
if (false) {
    /** @type {?} */
    NumberWidgetComponent.prototype.displayValue;
    /** @type {?} */
    NumberWidgetComponent.prototype.formService;
    /**
     * @type {?}
     * @private
     */
    NumberWidgetComponent.prototype.decimalNumberPipe;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibnVtYmVyLndpZGdldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL251bWJlci9udW1iZXIud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsU0FBUyxFQUFFLGlCQUFpQixFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUMvRCxPQUFPLEVBQUUsUUFBUSxFQUFHLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ25FLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBUzFFLE1BQU0sT0FBTyxxQkFBc0IsU0FBUSxlQUFlOzs7OztJQUl0RCxZQUFtQixXQUF3QixFQUN2QixpQkFBb0M7UUFDbkQsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRkwsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDdkIsc0JBQWlCLEdBQWpCLGlCQUFpQixDQUFtQjtJQUV4RCxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUU7WUFDckIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDMUU7YUFBTTtZQUNILElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUM7U0FDeEM7SUFDTCxDQUFDOzs7WUF0QkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxlQUFlO2dCQUN6Qix1aENBQW1DO2dCQUVuQyxJQUFJLEVBQUUsUUFBUTtnQkFDZCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7YUFDeEM7Ozs7WUFWUSxXQUFXO1lBRVgsaUJBQWlCOzs7O0lBV3RCLDZDQUFxQjs7SUFFVCw0Q0FBK0I7Ozs7O0lBQy9CLGtEQUE0QyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4gLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yIG5vLWlucHV0LXJlbmFtZSAgICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIFZpZXdFbmNhcHN1bGF0aW9uLCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLy4uLy4uLy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XHJcbmltcG9ydCB7IGJhc2VIb3N0ICwgV2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi8uLi93aWRnZXQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRGVjaW1hbE51bWJlclBpcGUgfSBmcm9tICcuLi8uLi8uLi8uLi9waXBlcy9kZWNpbWFsLW51bWJlci5waXBlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdudW1iZXItd2lkZ2V0JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9udW1iZXIud2lkZ2V0Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vbnVtYmVyLndpZGdldC5zY3NzJ10sXHJcbiAgICBob3N0OiBiYXNlSG9zdCxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIE51bWJlcldpZGdldENvbXBvbmVudCBleHRlbmRzIFdpZGdldENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgZGlzcGxheVZhbHVlOiBudW1iZXI7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgZGVjaW1hbE51bWJlclBpcGU6IERlY2ltYWxOdW1iZXJQaXBlKSB7XHJcbiAgICAgICAgIHN1cGVyKGZvcm1TZXJ2aWNlKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBpZiAodGhpcy5maWVsZC5yZWFkT25seSkge1xyXG4gICAgICAgICAgICB0aGlzLmRpc3BsYXlWYWx1ZSA9IHRoaXMuZGVjaW1hbE51bWJlclBpcGUudHJhbnNmb3JtKHRoaXMuZmllbGQudmFsdWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheVZhbHVlID0gdGhpcy5maWVsZC52YWx1ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==