/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { PeopleProcessService } from '../../../../services/people-process.service';
import { Component, ElementRef, EventEmitter, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormService } from '../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
import { FormControl } from '@angular/forms';
import { of } from 'rxjs';
import { catchError, distinctUntilChanged, map, switchMap, tap } from 'rxjs/operators';
export class PeopleWidgetComponent extends WidgetComponent {
    /**
     * @param {?} formService
     * @param {?} peopleProcessService
     */
    constructor(formService, peopleProcessService) {
        super(formService);
        this.formService = formService;
        this.peopleProcessService = peopleProcessService;
        this.searchTerm = new FormControl();
        this.errorMsg = '';
        this.searchTerms$ = this.searchTerm.valueChanges;
        this.users$ = this.searchTerms$.pipe(tap((/**
         * @return {?}
         */
        () => {
            this.errorMsg = '';
        })), distinctUntilChanged(), switchMap((/**
         * @param {?} searchTerm
         * @return {?}
         */
        (searchTerm) => {
            /** @type {?} */
            const value = searchTerm.email ? this.getDisplayName(searchTerm) : searchTerm;
            return this.formService.getWorkflowUsers(value, this.groupId)
                .pipe(catchError((/**
             * @param {?} err
             * @return {?}
             */
            (err) => {
                this.errorMsg = err.message;
                return of();
            })));
        })), map((/**
         * @param {?} list
         * @return {?}
         */
        (list) => {
            /** @type {?} */
            const value = this.searchTerm.value.email ? this.getDisplayName(this.searchTerm.value) : this.searchTerm.value;
            this.checkUserAndValidateForm(list, value);
            return list;
        })));
        this.peopleSelected = new EventEmitter();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.field) {
            if (this.field.value) {
                this.searchTerm.setValue(this.field.value);
            }
            if (this.field.readOnly) {
                this.searchTerm.disable();
            }
            /** @type {?} */
            const params = this.field.params;
            if (params && params.restrictWithGroup) {
                /** @type {?} */
                const restrictWithGroup = (/** @type {?} */ (params.restrictWithGroup));
                this.groupId = restrictWithGroup.id;
            }
        }
    }
    /**
     * @param {?} list
     * @param {?} value
     * @return {?}
     */
    checkUserAndValidateForm(list, value) {
        /** @type {?} */
        const isValidUser = this.isValidUser(list, value);
        if (isValidUser || value === '') {
            this.field.validationSummary.message = '';
            this.field.validate();
            this.field.form.validateForm();
        }
        else {
            this.field.validationSummary.message = 'FORM.FIELD.VALIDATOR.INVALID_VALUE';
            this.field.markAsInvalid();
            this.field.form.markAsInvalid();
        }
    }
    /**
     * @param {?} users
     * @param {?} name
     * @return {?}
     */
    isValidUser(users, name) {
        if (users) {
            return users.find((/**
             * @param {?} user
             * @return {?}
             */
            (user) => {
                /** @type {?} */
                const selectedUser = this.getDisplayName(user).toLocaleLowerCase() === name.toLocaleLowerCase();
                if (selectedUser) {
                    this.peopleSelected.emit(user && user.id || undefined);
                }
                return selectedUser;
            }));
        }
    }
    /**
     * @param {?} model
     * @return {?}
     */
    getDisplayName(model) {
        if (model) {
            /** @type {?} */
            const displayName = `${model.firstName || ''} ${model.lastName || ''}`;
            return displayName.trim();
        }
        return '';
    }
    /**
     * @param {?} item
     * @return {?}
     */
    onItemSelect(item) {
        if (item) {
            this.field.value = item;
        }
    }
}
PeopleWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'people-widget',
                template: "<div class=\"adf-people-widget {{field.className}}\"\r\n     [class.adf-invalid]=\"!field.isValid\"\r\n     [class.adf-readonly]=\"field.readOnly\"\r\n     id=\"people-widget-content\">\r\n    <mat-form-field>\r\n        <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}<span *ngIf=\"isRequired()\">*</span></label>\r\n        <input #inputValue\r\n               matInput\r\n               class=\"adf-input\"\r\n               data-automation-id=\"adf-people-search-input\"\r\n               type=\"text\"\r\n               [id]=\"field.id\"\r\n               [formControl]=\"searchTerm\"\r\n               placeholder=\"{{field.placeholder}}\"\r\n               [matAutocomplete]=\"auto\">\r\n        <mat-autocomplete class=\"adf-people-widget-list\"\r\n                          #auto=\"matAutocomplete\"\r\n                          (optionSelected)=\"onItemSelect($event.option.value)\"\r\n                          [displayWith]=\"getDisplayName\">\r\n            <mat-option *ngFor=\"let user of users$ | async; let i = index\" [value]=\"user\">\r\n                <div class=\"adf-people-widget-row\" id=\"adf-people-widget-user-{{i}}\">\r\n                    <div [outerHTML]=\"user | usernameInitials:'adf-people-widget-pic'\"></div>\r\n                    <div *ngIf=\"user.pictureId\" class=\"adf-people-widget-image-row\">\r\n                        <img id=\"adf-people-widget-pic-{{i}}\" class=\"adf-people-widget-image\"\r\n                             [alt]=\"getDisplayName(user)\" [src]=\"peopleProcessService.getUserImage(user)\"/>\r\n                    </div>\r\n                    <span class=\"adf-people-label-name\">{{getDisplayName(user)}}</span>\r\n                </div>\r\n            </mat-option>\r\n        </mat-autocomplete>\r\n    </mat-form-field>\r\n    <error-widget [error]=\"field.validationSummary\"></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n</div>\r\n",
                host: baseHost,
                encapsulation: ViewEncapsulation.None,
                styles: [""]
            }] }
];
/** @nocollapse */
PeopleWidgetComponent.ctorParameters = () => [
    { type: FormService },
    { type: PeopleProcessService }
];
PeopleWidgetComponent.propDecorators = {
    input: [{ type: ViewChild, args: ['inputValue', { static: true },] }],
    peopleSelected: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    PeopleWidgetComponent.prototype.input;
    /** @type {?} */
    PeopleWidgetComponent.prototype.peopleSelected;
    /** @type {?} */
    PeopleWidgetComponent.prototype.groupId;
    /** @type {?} */
    PeopleWidgetComponent.prototype.value;
    /** @type {?} */
    PeopleWidgetComponent.prototype.searchTerm;
    /** @type {?} */
    PeopleWidgetComponent.prototype.errorMsg;
    /** @type {?} */
    PeopleWidgetComponent.prototype.searchTerms$;
    /** @type {?} */
    PeopleWidgetComponent.prototype.users$;
    /** @type {?} */
    PeopleWidgetComponent.prototype.formService;
    /** @type {?} */
    PeopleWidgetComponent.prototype.peopleProcessService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVvcGxlLndpZGdldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL3Blb3BsZS9wZW9wbGUud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQ0EsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFFbkYsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFVLE1BQU0sRUFBRSxTQUFTLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbEgsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBRTdELE9BQU8sRUFBRSxRQUFRLEVBQUUsZUFBZSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDbEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBYyxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDdEMsT0FBTyxFQUNILFVBQVUsRUFDVixvQkFBb0IsRUFDcEIsR0FBRyxFQUNILFNBQVMsRUFDVCxHQUFHLEVBQ04sTUFBTSxnQkFBZ0IsQ0FBQztBQVN4QixNQUFNLE9BQU8scUJBQXNCLFNBQVEsZUFBZTs7Ozs7SUFxQ3RELFlBQW1CLFdBQXdCLEVBQVMsb0JBQTBDO1FBQzFGLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztRQURKLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQVMseUJBQW9CLEdBQXBCLG9CQUFvQixDQUFzQjtRQTFCOUYsZUFBVSxHQUFHLElBQUksV0FBVyxFQUFFLENBQUM7UUFDL0IsYUFBUSxHQUFHLEVBQUUsQ0FBQztRQUNkLGlCQUFZLEdBQW9CLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxDQUFDO1FBRTdELFdBQU0sR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FDM0IsR0FBRzs7O1FBQUMsR0FBRyxFQUFFO1lBQ0wsSUFBSSxDQUFDLFFBQVEsR0FBRyxFQUFFLENBQUM7UUFDdkIsQ0FBQyxFQUFDLEVBQ0Ysb0JBQW9CLEVBQUUsRUFDdEIsU0FBUzs7OztRQUFDLENBQUMsVUFBVSxFQUFFLEVBQUU7O2tCQUNmLEtBQUssR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVO1lBQzdFLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQztpQkFDeEQsSUFBSSxDQUNELFVBQVU7Ozs7WUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFO2dCQUNmLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQztnQkFDNUIsT0FBTyxFQUFFLEVBQUUsQ0FBQztZQUNoQixDQUFDLEVBQUMsQ0FDTCxDQUFDO1FBQ1YsQ0FBQyxFQUFDLEVBQ0YsR0FBRzs7OztRQUFDLENBQUMsSUFBd0IsRUFBRSxFQUFFOztrQkFDdkIsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUs7WUFDOUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQztZQUMzQyxPQUFPLElBQUksQ0FBQztRQUNoQixDQUFDLEVBQUMsQ0FDTCxDQUFDO1FBSUUsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO0lBQzdDLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1osSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRTtnQkFDbEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUM5QztZQUNELElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUU7Z0JBQ3JCLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDN0I7O2tCQUNLLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU07WUFDaEMsSUFBSSxNQUFNLElBQUksTUFBTSxDQUFDLGlCQUFpQixFQUFFOztzQkFDOUIsaUJBQWlCLEdBQUcsbUJBQWEsTUFBTSxDQUFDLGlCQUFpQixFQUFBO2dCQUMvRCxJQUFJLENBQUMsT0FBTyxHQUFHLGlCQUFpQixDQUFDLEVBQUUsQ0FBQzthQUN2QztTQUNKO0lBQ0wsQ0FBQzs7Ozs7O0lBRUQsd0JBQXdCLENBQUMsSUFBSSxFQUFFLEtBQUs7O2NBQzFCLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUM7UUFDakQsSUFBSSxXQUFXLElBQUksS0FBSyxLQUFLLEVBQUUsRUFBRTtZQUM3QixJQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFDMUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztTQUNsQzthQUFNO1lBQ0gsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEdBQUcsb0NBQW9DLENBQUM7WUFDNUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUMzQixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztTQUNuQztJQUNMLENBQUM7Ozs7OztJQUVELFdBQVcsQ0FBQyxLQUF5QixFQUFFLElBQVk7UUFDL0MsSUFBSSxLQUFLLEVBQUU7WUFDUCxPQUFPLEtBQUssQ0FBQyxJQUFJOzs7O1lBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTs7c0JBQ2pCLFlBQVksR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLGlCQUFpQixFQUFFLEtBQUssSUFBSSxDQUFDLGlCQUFpQixFQUFFO2dCQUMvRixJQUFJLFlBQVksRUFBRTtvQkFDZCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLEVBQUUsSUFBSSxTQUFTLENBQUMsQ0FBQztpQkFDMUQ7Z0JBQ0QsT0FBTyxZQUFZLENBQUM7WUFDeEIsQ0FBQyxFQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLEtBQXVCO1FBQ2xDLElBQUksS0FBSyxFQUFFOztrQkFDRCxXQUFXLEdBQUcsR0FBRyxLQUFLLENBQUMsU0FBUyxJQUFJLEVBQUUsSUFBSSxLQUFLLENBQUMsUUFBUSxJQUFJLEVBQUUsRUFBRTtZQUN0RSxPQUFPLFdBQVcsQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUM3QjtRQUNELE9BQU8sRUFBRSxDQUFDO0lBQ2QsQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUMsSUFBc0I7UUFDL0IsSUFBSSxJQUFJLEVBQUU7WUFDTixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7U0FDM0I7SUFDTCxDQUFDOzs7WUF0R0osU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxlQUFlO2dCQUN6Qix5L0RBQW1DO2dCQUVuQyxJQUFJLEVBQUUsUUFBUTtnQkFDZCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7YUFDeEM7Ozs7WUFuQlEsV0FBVztZQUhYLG9CQUFvQjs7O29CQXlCeEIsU0FBUyxTQUFDLFlBQVksRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7NkJBR3RDLE1BQU07Ozs7SUFIUCxzQ0FDa0I7O0lBRWxCLCtDQUNxQzs7SUFFckMsd0NBQWdCOztJQUNoQixzQ0FBVzs7SUFFWCwyQ0FBK0I7O0lBQy9CLHlDQUFjOztJQUNkLDZDQUE2RDs7SUFFN0QsdUNBb0JFOztJQUVVLDRDQUErQjs7SUFBRSxxREFBaUQiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cblxuLyohXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cbiAqXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4gKlxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuICpcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXG4gKi9cblxuLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xuXG5pbXBvcnQgeyBQZW9wbGVQcm9jZXNzU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL3NlcnZpY2VzL3Blb3BsZS1wcm9jZXNzLnNlcnZpY2UnO1xuaW1wb3J0IHsgVXNlclByb2Nlc3NNb2RlbCB9IGZyb20gJy4uLy4uLy4uLy4uL21vZGVscyc7XG5pbXBvcnQgeyBDb21wb25lbnQsIEVsZW1lbnRSZWYsIEV2ZW50RW1pdHRlciwgT25Jbml0LCBPdXRwdXQsIFZpZXdDaGlsZCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcbmltcG9ydCB7IEdyb3VwTW9kZWwgfSBmcm9tICcuLi9jb3JlL2dyb3VwLm1vZGVsJztcbmltcG9ydCB7IGJhc2VIb3N0LCBXaWRnZXRDb21wb25lbnQgfSBmcm9tICcuLy4uL3dpZGdldC5jb21wb25lbnQnO1xuaW1wb3J0IHsgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHtcbiAgICBjYXRjaEVycm9yLFxuICAgIGRpc3RpbmN0VW50aWxDaGFuZ2VkLFxuICAgIG1hcCxcbiAgICBzd2l0Y2hNYXAsXG4gICAgdGFwXG59IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdwZW9wbGUtd2lkZ2V0JyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vcGVvcGxlLndpZGdldC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9wZW9wbGUud2lkZ2V0LnNjc3MnXSxcbiAgICBob3N0OiBiYXNlSG9zdCxcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXG59KVxuZXhwb3J0IGNsYXNzIFBlb3BsZVdpZGdldENvbXBvbmVudCBleHRlbmRzIFdpZGdldENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgICBAVmlld0NoaWxkKCdpbnB1dFZhbHVlJywge3N0YXRpYzogdHJ1ZX0pXG4gICAgaW5wdXQ6IEVsZW1lbnRSZWY7XG5cbiAgICBAT3V0cHV0KClcbiAgICBwZW9wbGVTZWxlY3RlZDogRXZlbnRFbWl0dGVyPG51bWJlcj47XG5cbiAgICBncm91cElkOiBzdHJpbmc7XG4gICAgdmFsdWU6IGFueTtcblxuICAgIHNlYXJjaFRlcm0gPSBuZXcgRm9ybUNvbnRyb2woKTtcbiAgICBlcnJvck1zZyA9ICcnO1xuICAgIHNlYXJjaFRlcm1zJDogT2JzZXJ2YWJsZTxhbnk+ID0gdGhpcy5zZWFyY2hUZXJtLnZhbHVlQ2hhbmdlcztcblxuICAgIHVzZXJzJCA9IHRoaXMuc2VhcmNoVGVybXMkLnBpcGUoXG4gICAgICAgIHRhcCgoKSA9PiB7XG4gICAgICAgICAgICB0aGlzLmVycm9yTXNnID0gJyc7XG4gICAgICAgIH0pLFxuICAgICAgICBkaXN0aW5jdFVudGlsQ2hhbmdlZCgpLFxuICAgICAgICBzd2l0Y2hNYXAoKHNlYXJjaFRlcm0pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gc2VhcmNoVGVybS5lbWFpbCA/IHRoaXMuZ2V0RGlzcGxheU5hbWUoc2VhcmNoVGVybSkgOiBzZWFyY2hUZXJtO1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZm9ybVNlcnZpY2UuZ2V0V29ya2Zsb3dVc2Vycyh2YWx1ZSwgdGhpcy5ncm91cElkKVxuICAgICAgICAgICAgICAgIC5waXBlKFxuICAgICAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZXJyb3JNc2cgPSBlcnIubWVzc2FnZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBvZigpO1xuICAgICAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgICAgICk7XG4gICAgICAgIH0pLFxuICAgICAgICBtYXAoKGxpc3Q6IFVzZXJQcm9jZXNzTW9kZWxbXSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgdmFsdWUgPSB0aGlzLnNlYXJjaFRlcm0udmFsdWUuZW1haWwgPyB0aGlzLmdldERpc3BsYXlOYW1lKHRoaXMuc2VhcmNoVGVybS52YWx1ZSkgOiB0aGlzLnNlYXJjaFRlcm0udmFsdWU7XG4gICAgICAgICAgICB0aGlzLmNoZWNrVXNlckFuZFZhbGlkYXRlRm9ybShsaXN0LCB2YWx1ZSk7XG4gICAgICAgICAgICByZXR1cm4gbGlzdDtcbiAgICAgICAgfSlcbiAgICApO1xuXG4gICAgY29uc3RydWN0b3IocHVibGljIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSwgcHVibGljIHBlb3BsZVByb2Nlc3NTZXJ2aWNlOiBQZW9wbGVQcm9jZXNzU2VydmljZSkge1xuICAgICAgICBzdXBlcihmb3JtU2VydmljZSk7XG4gICAgICAgIHRoaXMucGVvcGxlU2VsZWN0ZWQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIGlmICh0aGlzLmZpZWxkKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5maWVsZC52YWx1ZSkge1xuICAgICAgICAgICAgICAgIHRoaXMuc2VhcmNoVGVybS5zZXRWYWx1ZSh0aGlzLmZpZWxkLnZhbHVlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGlmICh0aGlzLmZpZWxkLnJlYWRPbmx5KSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hUZXJtLmRpc2FibGUoKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIGNvbnN0IHBhcmFtcyA9IHRoaXMuZmllbGQucGFyYW1zO1xuICAgICAgICAgICAgaWYgKHBhcmFtcyAmJiBwYXJhbXMucmVzdHJpY3RXaXRoR3JvdXApIHtcbiAgICAgICAgICAgICAgICBjb25zdCByZXN0cmljdFdpdGhHcm91cCA9IDxHcm91cE1vZGVsPiBwYXJhbXMucmVzdHJpY3RXaXRoR3JvdXA7XG4gICAgICAgICAgICAgICAgdGhpcy5ncm91cElkID0gcmVzdHJpY3RXaXRoR3JvdXAuaWQ7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBjaGVja1VzZXJBbmRWYWxpZGF0ZUZvcm0obGlzdCwgdmFsdWUpIHtcbiAgICAgICAgY29uc3QgaXNWYWxpZFVzZXIgPSB0aGlzLmlzVmFsaWRVc2VyKGxpc3QsIHZhbHVlKTtcbiAgICAgICAgaWYgKGlzVmFsaWRVc2VyIHx8IHZhbHVlID09PSAnJykge1xuICAgICAgICAgICAgdGhpcy5maWVsZC52YWxpZGF0aW9uU3VtbWFyeS5tZXNzYWdlID0gJyc7XG4gICAgICAgICAgICB0aGlzLmZpZWxkLnZhbGlkYXRlKCk7XG4gICAgICAgICAgICB0aGlzLmZpZWxkLmZvcm0udmFsaWRhdGVGb3JtKCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5Lm1lc3NhZ2UgPSAnRk9STS5GSUVMRC5WQUxJREFUT1IuSU5WQUxJRF9WQUxVRSc7XG4gICAgICAgICAgICB0aGlzLmZpZWxkLm1hcmtBc0ludmFsaWQoKTtcbiAgICAgICAgICAgIHRoaXMuZmllbGQuZm9ybS5tYXJrQXNJbnZhbGlkKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBpc1ZhbGlkVXNlcih1c2VyczogVXNlclByb2Nlc3NNb2RlbFtdLCBuYW1lOiBzdHJpbmcpIHtcbiAgICAgICAgaWYgKHVzZXJzKSB7XG4gICAgICAgICAgICByZXR1cm4gdXNlcnMuZmluZCgodXNlcikgPT4ge1xuICAgICAgICAgICAgICAgIGNvbnN0IHNlbGVjdGVkVXNlciA9IHRoaXMuZ2V0RGlzcGxheU5hbWUodXNlcikudG9Mb2NhbGVMb3dlckNhc2UoKSA9PT0gbmFtZS50b0xvY2FsZUxvd2VyQ2FzZSgpO1xuICAgICAgICAgICAgICAgIGlmIChzZWxlY3RlZFVzZXIpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wZW9wbGVTZWxlY3RlZC5lbWl0KHVzZXIgJiYgdXNlci5pZCB8fCB1bmRlZmluZWQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICByZXR1cm4gc2VsZWN0ZWRVc2VyO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBnZXREaXNwbGF5TmFtZShtb2RlbDogVXNlclByb2Nlc3NNb2RlbCkge1xuICAgICAgICBpZiAobW9kZWwpIHtcbiAgICAgICAgICAgIGNvbnN0IGRpc3BsYXlOYW1lID0gYCR7bW9kZWwuZmlyc3ROYW1lIHx8ICcnfSAke21vZGVsLmxhc3ROYW1lIHx8ICcnfWA7XG4gICAgICAgICAgICByZXR1cm4gZGlzcGxheU5hbWUudHJpbSgpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiAnJztcbiAgICB9XG5cbiAgICBvbkl0ZW1TZWxlY3QoaXRlbTogVXNlclByb2Nlc3NNb2RlbCkge1xuICAgICAgICBpZiAoaXRlbSkge1xuICAgICAgICAgICAgdGhpcy5maWVsZC52YWx1ZSA9IGl0ZW07XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=