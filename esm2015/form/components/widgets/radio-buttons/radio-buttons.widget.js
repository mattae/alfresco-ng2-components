/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { LogService } from '../../../../services/log.service';
import { Component, ViewEncapsulation } from '@angular/core';
import { FormService } from '../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
export class RadioButtonsWidgetComponent extends WidgetComponent {
    /**
     * @param {?} formService
     * @param {?} logService
     */
    constructor(formService, logService) {
        super(formService);
        this.formService = formService;
        this.logService = logService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.field && this.field.restUrl) {
            if (this.field.form.taskId) {
                this.getOptionsByTaskId();
            }
            else {
                this.getOptionsByProcessDefinitionId();
            }
        }
    }
    /**
     * @return {?}
     */
    getOptionsByTaskId() {
        this.formService
            .getRestFieldValues(this.field.form.taskId, this.field.id)
            .subscribe((/**
         * @param {?} formFieldOption
         * @return {?}
         */
        (formFieldOption) => {
            this.field.options = formFieldOption || [];
            this.field.updateForm();
        }), (/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err)));
    }
    /**
     * @return {?}
     */
    getOptionsByProcessDefinitionId() {
        this.formService
            .getRestFieldValuesByProcessId(this.field.form.processDefinitionId, this.field.id)
            .subscribe((/**
         * @param {?} formFieldOption
         * @return {?}
         */
        (formFieldOption) => {
            this.field.options = formFieldOption || [];
            this.field.updateForm();
        }), (/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err)));
    }
    /**
     * @param {?} optionSelected
     * @return {?}
     */
    onOptionClick(optionSelected) {
        this.field.value = optionSelected;
        this.fieldChanged.emit(this.field);
    }
    /**
     * @param {?} error
     * @return {?}
     */
    handleError(error) {
        this.logService.error(error);
    }
}
RadioButtonsWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'radio-buttons-widget',
                template: "<div class=\"adf-radio-buttons-widget {{field.className}}\"\r\n     [class.adf-invalid]=\"!field.isValid\" [class.adf-readonly]=\"field.readOnly\" [id]=\"field.id\">\r\n    <div class=\"adf-radio-button-container\">\r\n        <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}<span *ngIf=\"isRequired()\">*</span></label>\r\n        <mat-radio-group class=\"adf-radio-group\" [(ngModel)]=\"field.value\">\r\n            <mat-radio-button\r\n                [id]=\"field.id + '-' + opt.id\"\r\n                [name]=\"field.id\"\r\n                [value]=\"opt.id\"\r\n                [disabled]=\"field.readOnly\"\r\n                [checked]=\"field.value === opt.id\"\r\n                (change)=\"onOptionClick(opt.id)\"\r\n                color=\"primary\"\r\n                class=\"adf-radio-button\" *ngFor=\"let opt of field.options\" >\r\n                {{opt.name}}\r\n            </mat-radio-button>\r\n        </mat-radio-group>\r\n    </div>\r\n    <error-widget [error]=\"field.validationSummary\" ></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n</div>\r\n\r\n\r\n",
                host: baseHost,
                encapsulation: ViewEncapsulation.None,
                styles: [".adf-radio-button-container{margin-bottom:15px}.adf-radio-group{display:inline-flex;flex-direction:column}.adf-radio-button{margin:5px}"]
            }] }
];
/** @nocollapse */
RadioButtonsWidgetComponent.ctorParameters = () => [
    { type: FormService },
    { type: LogService }
];
if (false) {
    /** @type {?} */
    RadioButtonsWidgetComponent.prototype.formService;
    /**
     * @type {?}
     * @private
     */
    RadioButtonsWidgetComponent.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmFkaW8tYnV0dG9ucy53aWRnZXQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9yYWRpby1idXR0b25zL3JhZGlvLWJ1dHRvbnMud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDOUQsT0FBTyxFQUFFLFNBQVMsRUFBVSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNyRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFFN0QsT0FBTyxFQUFFLFFBQVEsRUFBRyxlQUFlLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQVNuRSxNQUFNLE9BQU8sMkJBQTRCLFNBQVEsZUFBZTs7Ozs7SUFFNUQsWUFBbUIsV0FBd0IsRUFDdkIsVUFBc0I7UUFDckMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRkwsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDdkIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUUxQyxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRTtZQUNsQyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDeEIsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7YUFDN0I7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLCtCQUErQixFQUFFLENBQUM7YUFDMUM7U0FDSjtJQUNMLENBQUM7Ozs7SUFFRCxrQkFBa0I7UUFDZCxJQUFJLENBQUMsV0FBVzthQUNYLGtCQUFrQixDQUNmLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQ2hCO2FBQ0EsU0FBUzs7OztRQUNOLENBQUMsZUFBa0MsRUFBRSxFQUFFO1lBQ25DLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLGVBQWUsSUFBSSxFQUFFLENBQUM7WUFDM0MsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUM1QixDQUFDOzs7O1FBQ0QsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQ2pDLENBQUM7SUFDVixDQUFDOzs7O0lBRUQsK0JBQStCO1FBQzNCLElBQUksQ0FBQyxXQUFXO2FBQ1gsNkJBQTZCLENBQzFCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FDaEI7YUFDQSxTQUFTOzs7O1FBQ04sQ0FBQyxlQUFrQyxFQUFFLEVBQUU7WUFDbkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsZUFBZSxJQUFJLEVBQUUsQ0FBQztZQUMzQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQzVCLENBQUM7Ozs7UUFDRCxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFDakMsQ0FBQztJQUNWLENBQUM7Ozs7O0lBRUQsYUFBYSxDQUFDLGNBQW1CO1FBQzdCLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLGNBQWMsQ0FBQztRQUNsQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdkMsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsS0FBVTtRQUNsQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7WUE3REosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxzQkFBc0I7Z0JBQ2hDLG1zQ0FBMEM7Z0JBRTFDLElBQUksRUFBRSxRQUFRO2dCQUNkLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOzthQUN4Qzs7OztZQVZRLFdBQVc7WUFGWCxVQUFVOzs7O0lBZUgsa0RBQStCOzs7OztJQUMvQixpREFBOEIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuIC8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9zZXJ2aWNlcy9sb2cuc2VydmljZSc7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XHJcbmltcG9ydCB7IEZvcm1GaWVsZE9wdGlvbiB9IGZyb20gJy4vLi4vY29yZS9mb3JtLWZpZWxkLW9wdGlvbic7XHJcbmltcG9ydCB7IGJhc2VIb3N0ICwgV2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi8uLi93aWRnZXQuY29tcG9uZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdyYWRpby1idXR0b25zLXdpZGdldCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vcmFkaW8tYnV0dG9ucy53aWRnZXQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9yYWRpby1idXR0b25zLndpZGdldC5zY3NzJ10sXHJcbiAgICBob3N0OiBiYXNlSG9zdCxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIFJhZGlvQnV0dG9uc1dpZGdldENvbXBvbmVudCBleHRlbmRzIFdpZGdldENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgbG9nU2VydmljZTogTG9nU2VydmljZSkge1xyXG4gICAgICAgICBzdXBlcihmb3JtU2VydmljZSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZmllbGQgJiYgdGhpcy5maWVsZC5yZXN0VXJsKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmZpZWxkLmZvcm0udGFza0lkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldE9wdGlvbnNCeVRhc2tJZCgpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5nZXRPcHRpb25zQnlQcm9jZXNzRGVmaW5pdGlvbklkKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0T3B0aW9uc0J5VGFza0lkKCkge1xyXG4gICAgICAgIHRoaXMuZm9ybVNlcnZpY2VcclxuICAgICAgICAgICAgLmdldFJlc3RGaWVsZFZhbHVlcyhcclxuICAgICAgICAgICAgICAgIHRoaXMuZmllbGQuZm9ybS50YXNrSWQsXHJcbiAgICAgICAgICAgICAgICB0aGlzLmZpZWxkLmlkXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIChmb3JtRmllbGRPcHRpb246IEZvcm1GaWVsZE9wdGlvbltdKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5maWVsZC5vcHRpb25zID0gZm9ybUZpZWxkT3B0aW9uIHx8IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmllbGQudXBkYXRlRm9ybSgpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE9wdGlvbnNCeVByb2Nlc3NEZWZpbml0aW9uSWQoKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtU2VydmljZVxyXG4gICAgICAgICAgICAuZ2V0UmVzdEZpZWxkVmFsdWVzQnlQcm9jZXNzSWQoXHJcbiAgICAgICAgICAgICAgICB0aGlzLmZpZWxkLmZvcm0ucHJvY2Vzc0RlZmluaXRpb25JZCxcclxuICAgICAgICAgICAgICAgIHRoaXMuZmllbGQuaWRcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgKGZvcm1GaWVsZE9wdGlvbjogRm9ybUZpZWxkT3B0aW9uW10pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZpZWxkLm9wdGlvbnMgPSBmb3JtRmllbGRPcHRpb24gfHwgW107XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5maWVsZC51cGRhdGVGb3JtKCk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgb25PcHRpb25DbGljayhvcHRpb25TZWxlY3RlZDogYW55KSB7XHJcbiAgICAgICAgdGhpcy5maWVsZC52YWx1ZSA9IG9wdGlvblNlbGVjdGVkO1xyXG4gICAgICAgIHRoaXMuZmllbGRDaGFuZ2VkLmVtaXQodGhpcy5maWVsZCk7XHJcbiAgICB9XHJcblxyXG4gICAgaGFuZGxlRXJyb3IoZXJyb3I6IGFueSkge1xyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJvcik7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==