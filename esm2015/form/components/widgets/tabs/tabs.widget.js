/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
export class TabsWidgetComponent {
    constructor() {
        this.tabs = [];
        this.formTabChanged = new EventEmitter();
        this.visibleTabs = [];
    }
    /**
     * @return {?}
     */
    hasTabs() {
        return this.tabs && this.tabs.length > 0;
    }
    /**
     * @return {?}
     */
    ngAfterContentChecked() {
        this.filterVisibleTabs();
    }
    /**
     * @return {?}
     */
    filterVisibleTabs() {
        this.visibleTabs = this.tabs.filter((/**
         * @param {?} tab
         * @return {?}
         */
        (tab) => {
            return tab.isVisible;
        }));
    }
    /**
     * @param {?} field
     * @return {?}
     */
    tabChanged(field) {
        this.formTabChanged.emit(field);
    }
}
TabsWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'tabs-widget',
                template: "<div *ngIf=\"hasTabs()\" class=\"alfresco-tabs-widget\">\r\n    <mat-tab-group>\r\n        <mat-tab *ngFor=\"let tab of visibleTabs\" [label]=\"tab.title | translate\">\r\n            <div *ngFor=\"let field of tab.fields\">\r\n                <adf-form-field [field]=\"field.field\"></adf-form-field>\r\n             </div>\r\n        </mat-tab>\r\n    </mat-tab-group>\r\n</div>\r\n",
                encapsulation: ViewEncapsulation.None
            }] }
];
TabsWidgetComponent.propDecorators = {
    tabs: [{ type: Input }],
    formTabChanged: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    TabsWidgetComponent.prototype.tabs;
    /** @type {?} */
    TabsWidgetComponent.prototype.formTabChanged;
    /** @type {?} */
    TabsWidgetComponent.prototype.visibleTabs;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFicy53aWRnZXQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy90YWJzL3RhYnMud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQXVCLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQVEvRyxNQUFNLE9BQU8sbUJBQW1CO0lBTGhDO1FBUUksU0FBSSxHQUFlLEVBQUUsQ0FBQztRQUd0QixtQkFBYyxHQUFpQyxJQUFJLFlBQVksRUFBa0IsQ0FBQztRQUVsRixnQkFBVyxHQUFlLEVBQUUsQ0FBQztJQW9CakMsQ0FBQzs7OztJQWxCRyxPQUFPO1FBQ0gsT0FBTyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUM3QyxDQUFDOzs7O0lBRUQscUJBQXFCO1FBQ2pCLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQzdCLENBQUM7Ozs7SUFFRCxpQkFBaUI7UUFDYixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7WUFDeEMsT0FBTyxHQUFHLENBQUMsU0FBUyxDQUFDO1FBQ3pCLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsS0FBcUI7UUFDNUIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDcEMsQ0FBQzs7O1lBL0JKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsYUFBYTtnQkFDdkIsNFlBQWlDO2dCQUNqQyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTthQUN4Qzs7O21CQUdJLEtBQUs7NkJBR0wsTUFBTTs7OztJQUhQLG1DQUNzQjs7SUFFdEIsNkNBQ2tGOztJQUVsRiwwQ0FBNkIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuIC8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IEFmdGVyQ29udGVudENoZWNrZWQsIENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT3V0cHV0LCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRNb2RlbCwgVGFiTW9kZWwgfSBmcm9tICcuLy4uL2NvcmUvaW5kZXgnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ3RhYnMtd2lkZ2V0JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi90YWJzLndpZGdldC5odG1sJyxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIFRhYnNXaWRnZXRDb21wb25lbnQgaW1wbGVtZW50cyBBZnRlckNvbnRlbnRDaGVja2VkIHtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgdGFiczogVGFiTW9kZWxbXSA9IFtdO1xyXG5cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgZm9ybVRhYkNoYW5nZWQ6IEV2ZW50RW1pdHRlcjxGb3JtRmllbGRNb2RlbD4gPSBuZXcgRXZlbnRFbWl0dGVyPEZvcm1GaWVsZE1vZGVsPigpO1xyXG5cclxuICAgIHZpc2libGVUYWJzOiBUYWJNb2RlbFtdID0gW107XHJcblxyXG4gICAgaGFzVGFicygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy50YWJzICYmIHRoaXMudGFicy5sZW5ndGggPiAwO1xyXG4gICAgfVxyXG5cclxuICAgIG5nQWZ0ZXJDb250ZW50Q2hlY2tlZCgpIHtcclxuICAgICAgICB0aGlzLmZpbHRlclZpc2libGVUYWJzKCk7XHJcbiAgICB9XHJcblxyXG4gICAgZmlsdGVyVmlzaWJsZVRhYnMoKSB7XHJcbiAgICAgICAgdGhpcy52aXNpYmxlVGFicyA9IHRoaXMudGFicy5maWx0ZXIoKHRhYikgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gdGFiLmlzVmlzaWJsZTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICB0YWJDaGFuZ2VkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCkge1xyXG4gICAgICAgIHRoaXMuZm9ybVRhYkNoYW5nZWQuZW1pdChmaWVsZCk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==