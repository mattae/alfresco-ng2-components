/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable: component-selector no-use-before-declare no-input-rename  */
import { Directive, ElementRef, forwardRef, HostListener, Input, Renderer2 } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
/** @type {?} */
export const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef((/**
     * @return {?}
     */
    () => InputMaskDirective)),
    multi: true
};
/**
 * Directive selectors without adf- prefix will be deprecated on 3.0.0
 */
export class InputMaskDirective {
    /**
     * @param {?} el
     * @param {?} render
     */
    constructor(el, render) {
        this.el = el;
        this.render = render;
        this.translationMask = {
            '0': { pattern: /\d/ },
            '9': { pattern: /\d/, optional: true },
            '#': { pattern: /\d/, recursive: true },
            'A': { pattern: /[a-zA-Z0-9]/ },
            'S': { pattern: /[a-zA-Z]/ }
        };
        this.byPassKeys = [9, 16, 17, 18, 36, 37, 38, 39, 40, 91];
        this.invalidCharacters = [];
        this._onChange = (/**
         * @param {?} _
         * @return {?}
         */
        (_) => {
        });
        this._onTouched = (/**
         * @return {?}
         */
        () => {
        });
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onTextInput(event) {
        if (this.inputMask && this.inputMask.mask) {
            this.maskValue(this.el.nativeElement.value, this.el.nativeElement.selectionStart, this.inputMask.mask, this.inputMask.isReversed, event.keyCode);
        }
        else {
            this._onChange(this.el.nativeElement.value);
        }
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes['inputMask'] && changes['inputMask'].currentValue['mask']) {
            this.inputMask = changes['inputMask'].currentValue;
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    writeValue(value) {
        this.el.nativeElement.value = value;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnChange(fn) {
        this._onChange = fn;
    }
    /**
     * @param {?} fn
     * @return {?}
     */
    registerOnTouched(fn) {
        this._onTouched = fn;
    }
    /**
     * @private
     * @param {?} actualValue
     * @param {?} startCaret
     * @param {?} maskToApply
     * @param {?} isMaskReversed
     * @param {?} keyCode
     * @return {?}
     */
    maskValue(actualValue, startCaret, maskToApply, isMaskReversed, keyCode) {
        if (this.byPassKeys.indexOf(keyCode) === -1) {
            /** @type {?} */
            const value = this.getMasked(false, actualValue, maskToApply, isMaskReversed);
            /** @type {?} */
            const calculatedCaret = this.calculateCaretPosition(startCaret, actualValue, keyCode);
            this.render.setAttribute(this.el.nativeElement, 'value', value);
            this.el.nativeElement.value = value;
            this.setValue(value);
            this._onChange(value);
            this.setCaretPosition(calculatedCaret);
        }
    }
    /**
     * @private
     * @param {?} caretPosition
     * @return {?}
     */
    setCaretPosition(caretPosition) {
        this.el.nativeElement.moveStart = caretPosition;
        this.el.nativeElement.moveEnd = caretPosition;
    }
    /**
     * @param {?} caretPosition
     * @param {?} newValue
     * @param {?} keyCode
     * @return {?}
     */
    calculateCaretPosition(caretPosition, newValue, keyCode) {
        /** @type {?} */
        const newValueLength = newValue.length;
        /** @type {?} */
        const oldValue = this.getValue() || '';
        /** @type {?} */
        const oldValueLength = oldValue.length;
        if (keyCode === 8 && oldValue !== newValue) {
            caretPosition = caretPosition - (newValue.slice(0, caretPosition).length - oldValue.slice(0, caretPosition).length);
        }
        else if (oldValue !== newValue) {
            if (caretPosition >= oldValueLength) {
                caretPosition = newValueLength;
            }
            else {
                caretPosition = caretPosition + (newValue.slice(0, caretPosition).length - oldValue.slice(0, caretPosition).length);
            }
        }
        return caretPosition;
    }
    /**
     * @param {?} skipMaskChars
     * @param {?} val
     * @param {?} mask
     * @param {?=} isReversed
     * @return {?}
     */
    getMasked(skipMaskChars, val, mask, isReversed = false) {
        /** @type {?} */
        const buf = [];
        /** @type {?} */
        const value = val;
        /** @type {?} */
        let maskIndex = 0;
        /** @type {?} */
        const maskLen = mask.length;
        /** @type {?} */
        let valueIndex = 0;
        /** @type {?} */
        const valueLength = value.length;
        /** @type {?} */
        let offset = 1;
        /** @type {?} */
        let addMethod = 'push';
        /** @type {?} */
        let resetPos = -1;
        /** @type {?} */
        let lastMaskChar;
        /** @type {?} */
        let lastUntranslatedMaskChar;
        /** @type {?} */
        let check;
        if (isReversed) {
            addMethod = 'unshift';
            offset = -1;
            lastMaskChar = 0;
            maskIndex = maskLen - 1;
            valueIndex = valueLength - 1;
        }
        else {
            lastMaskChar = maskLen - 1;
        }
        check = this.isToCheck(isReversed, maskIndex, maskLen, valueIndex, valueLength);
        while (check) {
            /** @type {?} */
            const maskDigit = mask.charAt(maskIndex);
            /** @type {?} */
            const valDigit = value.charAt(valueIndex);
            /** @type {?} */
            const translation = this.translationMask[maskDigit];
            if (translation) {
                if (valDigit.match(translation.pattern)) {
                    buf[addMethod](valDigit);
                    if (translation.recursive) {
                        if (resetPos === -1) {
                            resetPos = maskIndex;
                        }
                        else if (maskIndex === lastMaskChar) {
                            maskIndex = resetPos - offset;
                        }
                        if (lastMaskChar === resetPos) {
                            maskIndex -= offset;
                        }
                    }
                    maskIndex += offset;
                }
                else if (valDigit === lastUntranslatedMaskChar) {
                    lastUntranslatedMaskChar = undefined;
                }
                else if (translation.optional) {
                    maskIndex += offset;
                    valueIndex -= offset;
                }
                else {
                    this.invalidCharacters.push({
                        index: valueIndex,
                        digit: valDigit,
                        translated: translation.pattern
                    });
                }
                valueIndex += offset;
            }
            else {
                if (!skipMaskChars) {
                    buf[addMethod](maskDigit);
                }
                if (valDigit === maskDigit) {
                    valueIndex += offset;
                }
                else {
                    lastUntranslatedMaskChar = maskDigit;
                }
                maskIndex += offset;
            }
            check = this.isToCheck(isReversed, maskIndex, maskLen, valueIndex, valueLength);
        }
        /** @type {?} */
        const lastMaskCharDigit = mask.charAt(lastMaskChar);
        if (maskLen === valueLength + 1 && !this.translationMask[lastMaskCharDigit]) {
            buf.push(lastMaskCharDigit);
        }
        return buf.join('');
    }
    /**
     * @private
     * @param {?} isReversed
     * @param {?} maskIndex
     * @param {?} maskLen
     * @param {?} valueIndex
     * @param {?} valueLength
     * @return {?}
     */
    isToCheck(isReversed, maskIndex, maskLen, valueIndex, valueLength) {
        /** @type {?} */
        let check = false;
        if (isReversed) {
            check = (maskIndex > -1) && (valueIndex > -1);
        }
        else {
            check = (maskIndex < maskLen) && (valueIndex < valueLength);
        }
        return check;
    }
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    setValue(value) {
        this.value = value;
    }
    /**
     * @private
     * @return {?}
     */
    getValue() {
        return this.value;
    }
}
InputMaskDirective.decorators = [
    { type: Directive, args: [{
                selector: '[adf-text-mask], [textMask]',
                providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
            },] }
];
/** @nocollapse */
InputMaskDirective.ctorParameters = () => [
    { type: ElementRef },
    { type: Renderer2 }
];
InputMaskDirective.propDecorators = {
    inputMask: [{ type: Input, args: ['textMask',] }],
    onTextInput: [{ type: HostListener, args: ['input', ['$event'],] }, { type: HostListener, args: ['keyup', ['$event'],] }]
};
if (false) {
    /**
     * Object defining mask and "reversed" status.
     * @type {?}
     */
    InputMaskDirective.prototype.inputMask;
    /**
     * @type {?}
     * @private
     */
    InputMaskDirective.prototype.translationMask;
    /**
     * @type {?}
     * @private
     */
    InputMaskDirective.prototype.byPassKeys;
    /**
     * @type {?}
     * @private
     */
    InputMaskDirective.prototype.value;
    /**
     * @type {?}
     * @private
     */
    InputMaskDirective.prototype.invalidCharacters;
    /** @type {?} */
    InputMaskDirective.prototype._onChange;
    /** @type {?} */
    InputMaskDirective.prototype._onTouched;
    /**
     * @type {?}
     * @private
     */
    InputMaskDirective.prototype.el;
    /**
     * @type {?}
     * @private
     */
    InputMaskDirective.prototype.render;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC1tYXNrLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL3RleHQvdGV4dC1tYXNrLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUNILFNBQVMsRUFDVCxVQUFVLEVBQ1YsVUFBVSxFQUNWLFlBQVksRUFDWixLQUFLLEVBRUwsU0FBUyxFQUVaLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBd0IsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7QUFFekUsTUFBTSxPQUFPLG1DQUFtQyxHQUFRO0lBQ3BELE9BQU8sRUFBRSxpQkFBaUI7SUFDMUIsV0FBVyxFQUFFLFVBQVU7OztJQUFDLEdBQUcsRUFBRSxDQUFDLGtCQUFrQixFQUFDO0lBQ2pELEtBQUssRUFBRSxJQUFJO0NBQ2Q7Ozs7QUFTRCxNQUFNLE9BQU8sa0JBQWtCOzs7OztJQW9CM0IsWUFBb0IsRUFBYyxFQUFVLE1BQWlCO1FBQXpDLE9BQUUsR0FBRixFQUFFLENBQVk7UUFBVSxXQUFNLEdBQU4sTUFBTSxDQUFXO1FBWnJELG9CQUFlLEdBQUc7WUFDdEIsR0FBRyxFQUFFLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRTtZQUN0QixHQUFHLEVBQUUsRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUU7WUFDdEMsR0FBRyxFQUFFLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsSUFBSSxFQUFFO1lBQ3ZDLEdBQUcsRUFBRSxFQUFFLE9BQU8sRUFBRSxhQUFhLEVBQUU7WUFDL0IsR0FBRyxFQUFFLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRTtTQUMvQixDQUFDO1FBRU0sZUFBVSxHQUFHLENBQUMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7UUFFckQsc0JBQWlCLEdBQUcsRUFBRSxDQUFDO1FBSy9CLGNBQVM7Ozs7UUFBRyxDQUFDLENBQU0sRUFBRSxFQUFFO1FBQ3ZCLENBQUMsRUFBQTtRQUVELGVBQVU7OztRQUFHLEdBQUcsRUFBRTtRQUNsQixDQUFDLEVBQUE7SUFORCxDQUFDOzs7OztJQVNrQyxXQUFXLENBQUMsS0FBb0I7UUFDL0QsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFO1lBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGNBQWMsRUFDNUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ3RFO2FBQU07WUFDSCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQy9DO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsT0FBc0I7UUFDOUIsSUFBSSxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNuRSxJQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxZQUFZLENBQUM7U0FDdEQ7SUFDTCxDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxLQUFVO1FBQ2pCLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDeEMsQ0FBQzs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxFQUFPO1FBQ3BCLElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO0lBQ3hCLENBQUM7Ozs7O0lBRUQsaUJBQWlCLENBQUMsRUFBYTtRQUMzQixJQUFJLENBQUMsVUFBVSxHQUFHLEVBQUUsQ0FBQztJQUN6QixDQUFDOzs7Ozs7Ozs7O0lBRU8sU0FBUyxDQUFDLFdBQVcsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLGNBQWMsRUFBRSxPQUFPO1FBQzNFLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7O2tCQUNuQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxjQUFjLENBQUM7O2tCQUN2RSxlQUFlLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsRUFBRSxXQUFXLEVBQUUsT0FBTyxDQUFDO1lBQ3JGLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNoRSxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN0QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDMUM7SUFDTCxDQUFDOzs7Ozs7SUFFTyxnQkFBZ0IsQ0FBQyxhQUFhO1FBQ2xDLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLFNBQVMsR0FBRyxhQUFhLENBQUM7UUFDaEQsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsT0FBTyxHQUFHLGFBQWEsQ0FBQztJQUNsRCxDQUFDOzs7Ozs7O0lBRUQsc0JBQXNCLENBQUMsYUFBYSxFQUFFLFFBQVEsRUFBRSxPQUFPOztjQUM3QyxjQUFjLEdBQUcsUUFBUSxDQUFDLE1BQU07O2NBQ2hDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRTs7Y0FDaEMsY0FBYyxHQUFHLFFBQVEsQ0FBQyxNQUFNO1FBRXRDLElBQUksT0FBTyxLQUFLLENBQUMsSUFBSSxRQUFRLEtBQUssUUFBUSxFQUFFO1lBQ3hDLGFBQWEsR0FBRyxhQUFhLEdBQUcsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxhQUFhLENBQUMsQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsYUFBYSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDdkg7YUFBTSxJQUFJLFFBQVEsS0FBSyxRQUFRLEVBQUU7WUFDOUIsSUFBSSxhQUFhLElBQUksY0FBYyxFQUFFO2dCQUNqQyxhQUFhLEdBQUcsY0FBYyxDQUFDO2FBQ2xDO2lCQUFNO2dCQUNILGFBQWEsR0FBRyxhQUFhLEdBQUcsQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxhQUFhLENBQUMsQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUUsYUFBYSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDdkg7U0FDSjtRQUNELE9BQU8sYUFBYSxDQUFDO0lBQ3pCLENBQUM7Ozs7Ozs7O0lBRUQsU0FBUyxDQUFDLGFBQWEsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLFVBQVUsR0FBRyxLQUFLOztjQUM1QyxHQUFHLEdBQUcsRUFBRTs7Y0FDUixLQUFLLEdBQUcsR0FBRzs7WUFDYixTQUFTLEdBQUcsQ0FBQzs7Y0FDWCxPQUFPLEdBQUcsSUFBSSxDQUFDLE1BQU07O1lBQ3ZCLFVBQVUsR0FBRyxDQUFDOztjQUNaLFdBQVcsR0FBRyxLQUFLLENBQUMsTUFBTTs7WUFDNUIsTUFBTSxHQUFHLENBQUM7O1lBQ1YsU0FBUyxHQUFHLE1BQU07O1lBQ2xCLFFBQVEsR0FBRyxDQUFDLENBQUM7O1lBQ2IsWUFBWTs7WUFDWix3QkFBd0I7O1lBQ3hCLEtBQUs7UUFFVCxJQUFJLFVBQVUsRUFBRTtZQUNaLFNBQVMsR0FBRyxTQUFTLENBQUM7WUFDdEIsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDO1lBQ1osWUFBWSxHQUFHLENBQUMsQ0FBQztZQUNqQixTQUFTLEdBQUcsT0FBTyxHQUFHLENBQUMsQ0FBQztZQUN4QixVQUFVLEdBQUcsV0FBVyxHQUFHLENBQUMsQ0FBQztTQUNoQzthQUFNO1lBQ0gsWUFBWSxHQUFHLE9BQU8sR0FBRyxDQUFDLENBQUM7U0FDOUI7UUFDRCxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsV0FBVyxDQUFDLENBQUM7UUFDaEYsT0FBTyxLQUFLLEVBQUU7O2tCQUNKLFNBQVMsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQzs7a0JBQ3BDLFFBQVEsR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLFVBQVUsQ0FBQzs7a0JBQ25DLFdBQVcsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsQ0FBQztZQUVqRCxJQUFJLFdBQVcsRUFBRTtnQkFDYixJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxFQUFFO29CQUNyQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUM7b0JBQ3pCLElBQUksV0FBVyxDQUFDLFNBQVMsRUFBRTt3QkFDdkIsSUFBSSxRQUFRLEtBQUssQ0FBQyxDQUFDLEVBQUU7NEJBQ2pCLFFBQVEsR0FBRyxTQUFTLENBQUM7eUJBQ3hCOzZCQUFNLElBQUksU0FBUyxLQUFLLFlBQVksRUFBRTs0QkFDbkMsU0FBUyxHQUFHLFFBQVEsR0FBRyxNQUFNLENBQUM7eUJBQ2pDO3dCQUNELElBQUksWUFBWSxLQUFLLFFBQVEsRUFBRTs0QkFDM0IsU0FBUyxJQUFJLE1BQU0sQ0FBQzt5QkFDdkI7cUJBQ0o7b0JBQ0QsU0FBUyxJQUFJLE1BQU0sQ0FBQztpQkFDdkI7cUJBQU0sSUFBSSxRQUFRLEtBQUssd0JBQXdCLEVBQUU7b0JBQzlDLHdCQUF3QixHQUFHLFNBQVMsQ0FBQztpQkFDeEM7cUJBQU0sSUFBSSxXQUFXLENBQUMsUUFBUSxFQUFFO29CQUM3QixTQUFTLElBQUksTUFBTSxDQUFDO29CQUNwQixVQUFVLElBQUksTUFBTSxDQUFDO2lCQUN4QjtxQkFBTTtvQkFDSCxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDO3dCQUN4QixLQUFLLEVBQUUsVUFBVTt3QkFDakIsS0FBSyxFQUFFLFFBQVE7d0JBQ2YsVUFBVSxFQUFFLFdBQVcsQ0FBQyxPQUFPO3FCQUNsQyxDQUFDLENBQUM7aUJBQ047Z0JBQ0QsVUFBVSxJQUFJLE1BQU0sQ0FBQzthQUN4QjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsYUFBYSxFQUFFO29CQUNoQixHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUM7aUJBQzdCO2dCQUNELElBQUksUUFBUSxLQUFLLFNBQVMsRUFBRTtvQkFDeEIsVUFBVSxJQUFJLE1BQU0sQ0FBQztpQkFDeEI7cUJBQU07b0JBQ0gsd0JBQXdCLEdBQUcsU0FBUyxDQUFDO2lCQUN4QztnQkFDRCxTQUFTLElBQUksTUFBTSxDQUFDO2FBQ3ZCO1lBQ0QsS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsVUFBVSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLFdBQVcsQ0FBQyxDQUFDO1NBQ25GOztjQUVLLGlCQUFpQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDO1FBQ25ELElBQUksT0FBTyxLQUFLLFdBQVcsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLGlCQUFpQixDQUFDLEVBQUU7WUFDekUsR0FBRyxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1NBQy9CO1FBRUQsT0FBTyxHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ3hCLENBQUM7Ozs7Ozs7Ozs7SUFFTyxTQUFTLENBQUMsVUFBVSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLFdBQVc7O1lBQ2pFLEtBQUssR0FBRyxLQUFLO1FBQ2pCLElBQUksVUFBVSxFQUFFO1lBQ1osS0FBSyxHQUFHLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNqRDthQUFNO1lBQ0gsS0FBSyxHQUFHLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFHLFdBQVcsQ0FBQyxDQUFDO1NBQy9EO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7Ozs7O0lBRU8sUUFBUSxDQUFDLEtBQUs7UUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDdkIsQ0FBQzs7Ozs7SUFFTyxRQUFRO1FBQ1osT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3RCLENBQUM7OztZQTdMSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLDZCQUE2QjtnQkFDdkMsU0FBUyxFQUFFLENBQUMsbUNBQW1DLENBQUM7YUFDbkQ7Ozs7WUF0QkcsVUFBVTtZQUtWLFNBQVM7Ozt3QkFxQlIsS0FBSyxTQUFDLFVBQVU7MEJBMEJoQixZQUFZLFNBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxDQUFDLGNBQ2hDLFlBQVksU0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUM7Ozs7Ozs7SUEzQmpDLHVDQUdFOzs7OztJQUVGLDZDQU1FOzs7OztJQUVGLHdDQUE2RDs7Ozs7SUFDN0QsbUNBQWM7Ozs7O0lBQ2QsK0NBQStCOztJQUsvQix1Q0FDQzs7SUFFRCx3Q0FDQzs7Ozs7SUFQVyxnQ0FBc0I7Ozs7O0lBQUUsb0NBQXlCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbiAvKiB0c2xpbnQ6ZGlzYWJsZTogY29tcG9uZW50LXNlbGVjdG9yIG5vLXVzZS1iZWZvcmUtZGVjbGFyZSBuby1pbnB1dC1yZW5hbWUgICovXHJcblxyXG5pbXBvcnQge1xyXG4gICAgRGlyZWN0aXZlLFxyXG4gICAgRWxlbWVudFJlZixcclxuICAgIGZvcndhcmRSZWYsXHJcbiAgICBIb3N0TGlzdGVuZXIsXHJcbiAgICBJbnB1dCxcclxuICAgIE9uQ2hhbmdlcyxcclxuICAgIFJlbmRlcmVyMixcclxuICAgIFNpbXBsZUNoYW5nZXNcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ29udHJvbFZhbHVlQWNjZXNzb3IsIE5HX1ZBTFVFX0FDQ0VTU09SIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuZXhwb3J0IGNvbnN0IENVU1RPTV9JTlBVVF9DT05UUk9MX1ZBTFVFX0FDQ0VTU09SOiBhbnkgPSB7XHJcbiAgICBwcm92aWRlOiBOR19WQUxVRV9BQ0NFU1NPUixcclxuICAgIHVzZUV4aXN0aW5nOiBmb3J3YXJkUmVmKCgpID0+IElucHV0TWFza0RpcmVjdGl2ZSksXHJcbiAgICBtdWx0aTogdHJ1ZVxyXG59O1xyXG5cclxuLyoqXHJcbiAqIERpcmVjdGl2ZSBzZWxlY3RvcnMgd2l0aG91dCBhZGYtIHByZWZpeCB3aWxsIGJlIGRlcHJlY2F0ZWQgb24gMy4wLjBcclxuICovXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbYWRmLXRleHQtbWFza10sIFt0ZXh0TWFza10nLFxyXG4gICAgcHJvdmlkZXJzOiBbQ1VTVE9NX0lOUFVUX0NPTlRST0xfVkFMVUVfQUNDRVNTT1JdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBJbnB1dE1hc2tEaXJlY3RpdmUgaW1wbGVtZW50cyBPbkNoYW5nZXMsIENvbnRyb2xWYWx1ZUFjY2Vzc29yIHtcclxuXHJcbiAgICAvKiogT2JqZWN0IGRlZmluaW5nIG1hc2sgYW5kIFwicmV2ZXJzZWRcIiBzdGF0dXMuICovXHJcbiAgICBASW5wdXQoJ3RleHRNYXNrJykgaW5wdXRNYXNrOiB7XHJcbiAgICAgICAgbWFzazogJycsXHJcbiAgICAgICAgaXNSZXZlcnNlZDogZmFsc2VcclxuICAgIH07XHJcblxyXG4gICAgcHJpdmF0ZSB0cmFuc2xhdGlvbk1hc2sgPSB7XHJcbiAgICAgICAgJzAnOiB7IHBhdHRlcm46IC9cXGQvIH0sXHJcbiAgICAgICAgJzknOiB7IHBhdHRlcm46IC9cXGQvLCBvcHRpb25hbDogdHJ1ZSB9LFxyXG4gICAgICAgICcjJzogeyBwYXR0ZXJuOiAvXFxkLywgcmVjdXJzaXZlOiB0cnVlIH0sXHJcbiAgICAgICAgJ0EnOiB7IHBhdHRlcm46IC9bYS16QS1aMC05XS8gfSxcclxuICAgICAgICAnUyc6IHsgcGF0dGVybjogL1thLXpBLVpdLyB9XHJcbiAgICB9O1xyXG5cclxuICAgIHByaXZhdGUgYnlQYXNzS2V5cyA9IFs5LCAxNiwgMTcsIDE4LCAzNiwgMzcsIDM4LCAzOSwgNDAsIDkxXTtcclxuICAgIHByaXZhdGUgdmFsdWU7XHJcbiAgICBwcml2YXRlIGludmFsaWRDaGFyYWN0ZXJzID0gW107XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBlbDogRWxlbWVudFJlZiwgcHJpdmF0ZSByZW5kZXI6IFJlbmRlcmVyMikge1xyXG4gICAgfVxyXG5cclxuICAgIF9vbkNoYW5nZSA9IChfOiBhbnkpID0+IHtcclxuICAgIH1cclxuXHJcbiAgICBfb25Ub3VjaGVkID0gKCkgPT4ge1xyXG4gICAgfVxyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2lucHV0JywgWyckZXZlbnQnXSlcclxuICAgIEBIb3N0TGlzdGVuZXIoJ2tleXVwJywgWyckZXZlbnQnXSkgb25UZXh0SW5wdXQoZXZlbnQ6IEtleWJvYXJkRXZlbnQpIHtcclxuICAgICAgICBpZiAodGhpcy5pbnB1dE1hc2sgJiYgdGhpcy5pbnB1dE1hc2subWFzaykge1xyXG4gICAgICAgICAgICB0aGlzLm1hc2tWYWx1ZSh0aGlzLmVsLm5hdGl2ZUVsZW1lbnQudmFsdWUsIHRoaXMuZWwubmF0aXZlRWxlbWVudC5zZWxlY3Rpb25TdGFydCxcclxuICAgICAgICAgICAgICAgIHRoaXMuaW5wdXRNYXNrLm1hc2ssIHRoaXMuaW5wdXRNYXNrLmlzUmV2ZXJzZWQsIGV2ZW50LmtleUNvZGUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuX29uQ2hhbmdlKHRoaXMuZWwubmF0aXZlRWxlbWVudC52YWx1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgICAgICBpZiAoY2hhbmdlc1snaW5wdXRNYXNrJ10gJiYgY2hhbmdlc1snaW5wdXRNYXNrJ10uY3VycmVudFZhbHVlWydtYXNrJ10pIHtcclxuICAgICAgICAgICAgdGhpcy5pbnB1dE1hc2sgPSBjaGFuZ2VzWydpbnB1dE1hc2snXS5jdXJyZW50VmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHdyaXRlVmFsdWUodmFsdWU6IGFueSkge1xyXG4gICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC52YWx1ZSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIHJlZ2lzdGVyT25DaGFuZ2UoZm46IGFueSkge1xyXG4gICAgICAgIHRoaXMuX29uQ2hhbmdlID0gZm47XHJcbiAgICB9XHJcblxyXG4gICAgcmVnaXN0ZXJPblRvdWNoZWQoZm46ICgpID0+IGFueSk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX29uVG91Y2hlZCA9IGZuO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgbWFza1ZhbHVlKGFjdHVhbFZhbHVlLCBzdGFydENhcmV0LCBtYXNrVG9BcHBseSwgaXNNYXNrUmV2ZXJzZWQsIGtleUNvZGUpIHtcclxuICAgICAgICBpZiAodGhpcy5ieVBhc3NLZXlzLmluZGV4T2Yoa2V5Q29kZSkgPT09IC0xKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gdGhpcy5nZXRNYXNrZWQoZmFsc2UsIGFjdHVhbFZhbHVlLCBtYXNrVG9BcHBseSwgaXNNYXNrUmV2ZXJzZWQpO1xyXG4gICAgICAgICAgICBjb25zdCBjYWxjdWxhdGVkQ2FyZXQgPSB0aGlzLmNhbGN1bGF0ZUNhcmV0UG9zaXRpb24oc3RhcnRDYXJldCwgYWN0dWFsVmFsdWUsIGtleUNvZGUpO1xyXG4gICAgICAgICAgICB0aGlzLnJlbmRlci5zZXRBdHRyaWJ1dGUodGhpcy5lbC5uYXRpdmVFbGVtZW50LCAndmFsdWUnLCB2YWx1ZSk7XHJcbiAgICAgICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC52YWx1ZSA9IHZhbHVlO1xyXG4gICAgICAgICAgICB0aGlzLnNldFZhbHVlKHZhbHVlKTtcclxuICAgICAgICAgICAgdGhpcy5fb25DaGFuZ2UodmFsdWUpO1xyXG4gICAgICAgICAgICB0aGlzLnNldENhcmV0UG9zaXRpb24oY2FsY3VsYXRlZENhcmV0KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBzZXRDYXJldFBvc2l0aW9uKGNhcmV0UG9zaXRpb24pIHtcclxuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQubW92ZVN0YXJ0ID0gY2FyZXRQb3NpdGlvbjtcclxuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQubW92ZUVuZCA9IGNhcmV0UG9zaXRpb247XHJcbiAgICB9XHJcblxyXG4gICAgY2FsY3VsYXRlQ2FyZXRQb3NpdGlvbihjYXJldFBvc2l0aW9uLCBuZXdWYWx1ZSwga2V5Q29kZSkge1xyXG4gICAgICAgIGNvbnN0IG5ld1ZhbHVlTGVuZ3RoID0gbmV3VmFsdWUubGVuZ3RoO1xyXG4gICAgICAgIGNvbnN0IG9sZFZhbHVlID0gdGhpcy5nZXRWYWx1ZSgpIHx8ICcnO1xyXG4gICAgICAgIGNvbnN0IG9sZFZhbHVlTGVuZ3RoID0gb2xkVmFsdWUubGVuZ3RoO1xyXG5cclxuICAgICAgICBpZiAoa2V5Q29kZSA9PT0gOCAmJiBvbGRWYWx1ZSAhPT0gbmV3VmFsdWUpIHtcclxuICAgICAgICAgICAgY2FyZXRQb3NpdGlvbiA9IGNhcmV0UG9zaXRpb24gLSAobmV3VmFsdWUuc2xpY2UoMCwgY2FyZXRQb3NpdGlvbikubGVuZ3RoIC0gb2xkVmFsdWUuc2xpY2UoMCwgY2FyZXRQb3NpdGlvbikubGVuZ3RoKTtcclxuICAgICAgICB9IGVsc2UgaWYgKG9sZFZhbHVlICE9PSBuZXdWYWx1ZSkge1xyXG4gICAgICAgICAgICBpZiAoY2FyZXRQb3NpdGlvbiA+PSBvbGRWYWx1ZUxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgY2FyZXRQb3NpdGlvbiA9IG5ld1ZhbHVlTGVuZ3RoO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY2FyZXRQb3NpdGlvbiA9IGNhcmV0UG9zaXRpb24gKyAobmV3VmFsdWUuc2xpY2UoMCwgY2FyZXRQb3NpdGlvbikubGVuZ3RoIC0gb2xkVmFsdWUuc2xpY2UoMCwgY2FyZXRQb3NpdGlvbikubGVuZ3RoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gY2FyZXRQb3NpdGlvbjtcclxuICAgIH1cclxuXHJcbiAgICBnZXRNYXNrZWQoc2tpcE1hc2tDaGFycywgdmFsLCBtYXNrLCBpc1JldmVyc2VkID0gZmFsc2UpIHtcclxuICAgICAgICBjb25zdCBidWYgPSBbXTtcclxuICAgICAgICBjb25zdCB2YWx1ZSA9IHZhbDtcclxuICAgICAgICBsZXQgbWFza0luZGV4ID0gMDtcclxuICAgICAgICBjb25zdCBtYXNrTGVuID0gbWFzay5sZW5ndGg7XHJcbiAgICAgICAgbGV0IHZhbHVlSW5kZXggPSAwO1xyXG4gICAgICAgIGNvbnN0IHZhbHVlTGVuZ3RoID0gdmFsdWUubGVuZ3RoO1xyXG4gICAgICAgIGxldCBvZmZzZXQgPSAxO1xyXG4gICAgICAgIGxldCBhZGRNZXRob2QgPSAncHVzaCc7XHJcbiAgICAgICAgbGV0IHJlc2V0UG9zID0gLTE7XHJcbiAgICAgICAgbGV0IGxhc3RNYXNrQ2hhcjtcclxuICAgICAgICBsZXQgbGFzdFVudHJhbnNsYXRlZE1hc2tDaGFyO1xyXG4gICAgICAgIGxldCBjaGVjaztcclxuXHJcbiAgICAgICAgaWYgKGlzUmV2ZXJzZWQpIHtcclxuICAgICAgICAgICAgYWRkTWV0aG9kID0gJ3Vuc2hpZnQnO1xyXG4gICAgICAgICAgICBvZmZzZXQgPSAtMTtcclxuICAgICAgICAgICAgbGFzdE1hc2tDaGFyID0gMDtcclxuICAgICAgICAgICAgbWFza0luZGV4ID0gbWFza0xlbiAtIDE7XHJcbiAgICAgICAgICAgIHZhbHVlSW5kZXggPSB2YWx1ZUxlbmd0aCAtIDE7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgbGFzdE1hc2tDaGFyID0gbWFza0xlbiAtIDE7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNoZWNrID0gdGhpcy5pc1RvQ2hlY2soaXNSZXZlcnNlZCwgbWFza0luZGV4LCBtYXNrTGVuLCB2YWx1ZUluZGV4LCB2YWx1ZUxlbmd0aCk7XHJcbiAgICAgICAgd2hpbGUgKGNoZWNrKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG1hc2tEaWdpdCA9IG1hc2suY2hhckF0KG1hc2tJbmRleCksXHJcbiAgICAgICAgICAgICAgICB2YWxEaWdpdCA9IHZhbHVlLmNoYXJBdCh2YWx1ZUluZGV4KSxcclxuICAgICAgICAgICAgICAgIHRyYW5zbGF0aW9uID0gdGhpcy50cmFuc2xhdGlvbk1hc2tbbWFza0RpZ2l0XTtcclxuXHJcbiAgICAgICAgICAgIGlmICh0cmFuc2xhdGlvbikge1xyXG4gICAgICAgICAgICAgICAgaWYgKHZhbERpZ2l0Lm1hdGNoKHRyYW5zbGF0aW9uLnBhdHRlcm4pKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYnVmW2FkZE1ldGhvZF0odmFsRGlnaXQpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0cmFuc2xhdGlvbi5yZWN1cnNpdmUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlc2V0UG9zID09PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVzZXRQb3MgPSBtYXNrSW5kZXg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAobWFza0luZGV4ID09PSBsYXN0TWFza0NoYXIpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hc2tJbmRleCA9IHJlc2V0UG9zIC0gb2Zmc2V0O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChsYXN0TWFza0NoYXIgPT09IHJlc2V0UG9zKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXNrSW5kZXggLT0gb2Zmc2V0O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIG1hc2tJbmRleCArPSBvZmZzZXQ7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHZhbERpZ2l0ID09PSBsYXN0VW50cmFuc2xhdGVkTWFza0NoYXIpIHtcclxuICAgICAgICAgICAgICAgICAgICBsYXN0VW50cmFuc2xhdGVkTWFza0NoYXIgPSB1bmRlZmluZWQ7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRyYW5zbGF0aW9uLm9wdGlvbmFsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbWFza0luZGV4ICs9IG9mZnNldDtcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZUluZGV4IC09IG9mZnNldDtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pbnZhbGlkQ2hhcmFjdGVycy5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaW5kZXg6IHZhbHVlSW5kZXgsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpZ2l0OiB2YWxEaWdpdCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHJhbnNsYXRlZDogdHJhbnNsYXRpb24ucGF0dGVyblxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgdmFsdWVJbmRleCArPSBvZmZzZXQ7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIXNraXBNYXNrQ2hhcnMpIHtcclxuICAgICAgICAgICAgICAgICAgICBidWZbYWRkTWV0aG9kXShtYXNrRGlnaXQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKHZhbERpZ2l0ID09PSBtYXNrRGlnaXQpIHtcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZUluZGV4ICs9IG9mZnNldDtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGFzdFVudHJhbnNsYXRlZE1hc2tDaGFyID0gbWFza0RpZ2l0O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgbWFza0luZGV4ICs9IG9mZnNldDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjaGVjayA9IHRoaXMuaXNUb0NoZWNrKGlzUmV2ZXJzZWQsIG1hc2tJbmRleCwgbWFza0xlbiwgdmFsdWVJbmRleCwgdmFsdWVMZW5ndGgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgbGFzdE1hc2tDaGFyRGlnaXQgPSBtYXNrLmNoYXJBdChsYXN0TWFza0NoYXIpO1xyXG4gICAgICAgIGlmIChtYXNrTGVuID09PSB2YWx1ZUxlbmd0aCArIDEgJiYgIXRoaXMudHJhbnNsYXRpb25NYXNrW2xhc3RNYXNrQ2hhckRpZ2l0XSkge1xyXG4gICAgICAgICAgICBidWYucHVzaChsYXN0TWFza0NoYXJEaWdpdCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gYnVmLmpvaW4oJycpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaXNUb0NoZWNrKGlzUmV2ZXJzZWQsIG1hc2tJbmRleCwgbWFza0xlbiwgdmFsdWVJbmRleCwgdmFsdWVMZW5ndGgpIHtcclxuICAgICAgICBsZXQgY2hlY2sgPSBmYWxzZTtcclxuICAgICAgICBpZiAoaXNSZXZlcnNlZCkge1xyXG4gICAgICAgICAgICBjaGVjayA9IChtYXNrSW5kZXggPiAtMSkgJiYgKHZhbHVlSW5kZXggPiAtMSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY2hlY2sgPSAobWFza0luZGV4IDwgbWFza0xlbikgJiYgKHZhbHVlSW5kZXggPCB2YWx1ZUxlbmd0aCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBjaGVjaztcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHNldFZhbHVlKHZhbHVlKSB7XHJcbiAgICAgICAgdGhpcy52YWx1ZSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0VmFsdWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudmFsdWU7XHJcbiAgICB9XHJcbn1cclxuIl19