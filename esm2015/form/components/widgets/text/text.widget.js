/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, ViewEncapsulation } from '@angular/core';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
export class TextWidgetComponent extends WidgetComponent {
    /**
     * @param {?} formService
     */
    constructor(formService) {
        super(formService);
        this.formService = formService;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.field.params) {
            this.mask = this.field.params['inputMask'];
            this.placeholder = this.field.params['inputMask'] && this.field.params['inputMaskPlaceholder'] ? this.field.params['inputMaskPlaceholder'] : this.field.placeholder;
            this.isMaskReversed = this.field.params['inputMaskReversed'] ? this.field.params['inputMaskReversed'] : false;
        }
    }
}
TextWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'text-widget',
                template: "<div class=\"adf-textfield adf-text-widget {{field.className}}\"\r\n     [class.adf-invalid]=\"!field.isValid\" [class.adf-readonly]=\"field.readOnly\">\r\n    <mat-form-field>\r\n        <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}<span *ngIf=\"isRequired()\">*</span></label>\r\n        <input matInput\r\n               class=\"adf-input\"\r\n               type=\"text\"\r\n               [id]=\"field.id\"\r\n               [required]=\"isRequired()\"\r\n               [value]=\"field.value\"\r\n               [(ngModel)]=\"field.value\"\r\n               (ngModelChange)=\"onFieldChanged(field)\"\r\n               [disabled]=\"field.readOnly || readOnly\"\r\n               [textMask]=\"{mask: mask, isReversed: isMaskReversed}\"\r\n               [placeholder]=\"placeholder\">\r\n    </mat-form-field>\r\n    <error-widget [error]=\"field.validationSummary\"></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n</div>\r\n",
                host: baseHost,
                encapsulation: ViewEncapsulation.None,
                styles: [".adf-text-widget{width:100%}"]
            }] }
];
/** @nocollapse */
TextWidgetComponent.ctorParameters = () => [
    { type: FormService }
];
if (false) {
    /** @type {?} */
    TextWidgetComponent.prototype.mask;
    /** @type {?} */
    TextWidgetComponent.prototype.placeholder;
    /** @type {?} */
    TextWidgetComponent.prototype.isMaskReversed;
    /** @type {?} */
    TextWidgetComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC53aWRnZXQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy90ZXh0L3RleHQud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsU0FBUyxFQUFVLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUMvRCxPQUFPLEVBQUUsUUFBUSxFQUFHLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBU25FLE1BQU0sT0FBTyxtQkFBb0IsU0FBUSxlQUFlOzs7O0lBTXBELFlBQW1CLFdBQXdCO1FBQ3ZDLEtBQUssQ0FBQyxXQUFXLENBQUMsQ0FBQztRQURKLGdCQUFXLEdBQVgsV0FBVyxDQUFhO0lBRTNDLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNuQixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQzNDLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsc0JBQXNCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUM7WUFDcEssSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7U0FDakg7SUFDTCxDQUFDOzs7WUF2QkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxhQUFhO2dCQUN2Qiw4aUNBQWlDO2dCQUVqQyxJQUFJLEVBQUUsUUFBUTtnQkFDZCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7YUFDeEM7Ozs7WUFUUSxXQUFXOzs7O0lBWWhCLG1DQUFhOztJQUNiLDBDQUFvQjs7SUFDcEIsNkNBQXdCOztJQUVaLDBDQUErQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4vKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLy4uLy4uLy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XHJcbmltcG9ydCB7IGJhc2VIb3N0ICwgV2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi8uLi93aWRnZXQuY29tcG9uZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICd0ZXh0LXdpZGdldCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vdGV4dC53aWRnZXQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi90ZXh0LndpZGdldC5zY3NzJ10sXHJcbiAgICBob3N0OiBiYXNlSG9zdCxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIFRleHRXaWRnZXRDb21wb25lbnQgZXh0ZW5kcyBXaWRnZXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIG1hc2s6IHN0cmluZztcclxuICAgIHBsYWNlaG9sZGVyOiBzdHJpbmc7XHJcbiAgICBpc01hc2tSZXZlcnNlZDogYm9vbGVhbjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZm9ybVNlcnZpY2U6IEZvcm1TZXJ2aWNlKSB7XHJcbiAgICAgICAgc3VwZXIoZm9ybVNlcnZpY2UpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmZpZWxkLnBhcmFtcykge1xyXG4gICAgICAgICAgICB0aGlzLm1hc2sgPSB0aGlzLmZpZWxkLnBhcmFtc1snaW5wdXRNYXNrJ107XHJcbiAgICAgICAgICAgIHRoaXMucGxhY2Vob2xkZXIgPSB0aGlzLmZpZWxkLnBhcmFtc1snaW5wdXRNYXNrJ10gJiYgdGhpcy5maWVsZC5wYXJhbXNbJ2lucHV0TWFza1BsYWNlaG9sZGVyJ10gPyB0aGlzLmZpZWxkLnBhcmFtc1snaW5wdXRNYXNrUGxhY2Vob2xkZXInXSA6IHRoaXMuZmllbGQucGxhY2Vob2xkZXI7XHJcbiAgICAgICAgICAgIHRoaXMuaXNNYXNrUmV2ZXJzZWQgPSB0aGlzLmZpZWxkLnBhcmFtc1snaW5wdXRNYXNrUmV2ZXJzZWQnXSA/IHRoaXMuZmllbGQucGFyYW1zWydpbnB1dE1hc2tSZXZlcnNlZCddIDogZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==