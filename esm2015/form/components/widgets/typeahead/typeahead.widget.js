/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { LogService } from '../../../../services/log.service';
import { ENTER, ESCAPE } from '@angular/cdk/keycodes';
import { Component, ViewEncapsulation } from '@angular/core';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
export class TypeaheadWidgetComponent extends WidgetComponent {
    /**
     * @param {?} formService
     * @param {?} logService
     */
    constructor(formService, logService) {
        super(formService);
        this.formService = formService;
        this.logService = logService;
        this.minTermLength = 1;
        this.options = [];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.field.form.taskId && this.field.restUrl) {
            this.getValuesByTaskId();
        }
        else if (this.field.form.processDefinitionId && this.field.restUrl) {
            this.getValuesByProcessDefinitionId();
        }
        if (this.isReadOnlyType()) {
            this.value = this.field.value;
        }
    }
    /**
     * @return {?}
     */
    getValuesByTaskId() {
        this.formService
            .getRestFieldValues(this.field.form.taskId, this.field.id)
            .subscribe((/**
         * @param {?} formFieldOption
         * @return {?}
         */
        (formFieldOption) => {
            /** @type {?} */
            const options = formFieldOption || [];
            this.field.options = options;
            /** @type {?} */
            const fieldValue = this.field.value;
            if (fieldValue) {
                /** @type {?} */
                const toSelect = options.find((/**
                 * @param {?} item
                 * @return {?}
                 */
                (item) => item.id === fieldValue || item.name.toLocaleLowerCase() === fieldValue.toLocaleLowerCase()));
                if (toSelect) {
                    this.value = toSelect.name;
                }
            }
            this.onFieldChanged(this.field);
            this.field.updateForm();
        }), (/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err)));
    }
    /**
     * @return {?}
     */
    getValuesByProcessDefinitionId() {
        this.formService
            .getRestFieldValuesByProcessId(this.field.form.processDefinitionId, this.field.id)
            .subscribe((/**
         * @param {?} formFieldOption
         * @return {?}
         */
        (formFieldOption) => {
            /** @type {?} */
            const options = formFieldOption || [];
            this.field.options = options;
            /** @type {?} */
            const fieldValue = this.field.value;
            if (fieldValue) {
                /** @type {?} */
                const toSelect = options.find((/**
                 * @param {?} item
                 * @return {?}
                 */
                (item) => item.id === fieldValue));
                if (toSelect) {
                    this.value = toSelect.name;
                }
            }
            this.onFieldChanged(this.field);
            this.field.updateForm();
        }), (/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err)));
    }
    /**
     * @return {?}
     */
    getOptions() {
        /** @type {?} */
        const val = this.value.trim().toLocaleLowerCase();
        return this.field.options.filter((/**
         * @param {?} item
         * @return {?}
         */
        (item) => {
            /** @type {?} */
            const name = item.name.toLocaleLowerCase();
            return name.indexOf(val) > -1;
        }));
    }
    /**
     * @param {?} optionName
     * @return {?}
     */
    isValidOptionName(optionName) {
        /** @type {?} */
        const option = this.field.options.find((/**
         * @param {?} item
         * @return {?}
         */
        (item) => item.name && item.name.toLocaleLowerCase() === optionName.toLocaleLowerCase()));
        return option ? true : false;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeyUp(event) {
        if (this.value && this.value.trim().length >= this.minTermLength && this.oldValue !== this.value) {
            if (event.keyCode !== ESCAPE && event.keyCode !== ENTER) {
                if (this.value.length >= this.minTermLength) {
                    this.options = this.getOptions();
                    this.oldValue = this.value;
                    if (this.isValidOptionName(this.value)) {
                        this.field.value = this.options[0].id;
                    }
                }
            }
        }
        if (this.isValueDefined() && this.value.trim().length === 0) {
            this.oldValue = this.value;
            this.options = [];
        }
    }
    /**
     * @param {?} item
     * @return {?}
     */
    onItemSelect(item) {
        if (item) {
            this.field.value = item.id;
            this.value = item.name;
            this.onFieldChanged(this.field);
        }
    }
    /**
     * @return {?}
     */
    validate() {
        this.field.value = this.value;
    }
    /**
     * @return {?}
     */
    isValueDefined() {
        return this.value !== null && this.value !== undefined;
    }
    /**
     * @param {?} error
     * @return {?}
     */
    handleError(error) {
        this.logService.error(error);
    }
    /**
     * @return {?}
     */
    isReadOnlyType() {
        return this.field.type === 'readonly' ? true : false;
    }
}
TypeaheadWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'typeahead-widget',
                template: "<div class=\"adf-typeahead-widget-container\">\r\n    <div class=\"adf-typeahead-widget {{field.className}}\"\r\n        [class.is-dirty]=\"value\"\r\n        [class.adf-invalid]=\"!field.isValid\"\r\n        [class.adf-readonly]=\"field.readOnly\"\r\n        id=\"typehead-div\">\r\n        <mat-form-field>\r\n            <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}</label>\r\n            <input matInput class=\"adf-input\"\r\n                   type=\"text\"\r\n                   [id]=\"field.id\"\r\n                   [(ngModel)]=\"value\"\r\n                   (ngModelChange)=\"validate()\"\r\n                   (keyup)=\"onKeyUp($event)\"\r\n                   [disabled]=\"field.readOnly\"\r\n                   placeholder=\"{{field.placeholder}}\"\r\n                   [matAutocomplete]=\"auto\">\r\n            <mat-autocomplete #auto=\"matAutocomplete\" (optionSelected)=\"onItemSelect($event.option.value)\">\r\n                <mat-option *ngFor=\"let item of options\" [value]=\"item\">\r\n                    <span [id]=\"field.name+'_option_'+item.id\">{{item.name}}</span>\r\n                </mat-option>\r\n            </mat-autocomplete>\r\n        </mat-form-field>\r\n\r\n        <error-widget [error]=\"field.validationSummary\"></error-widget>\r\n        <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n    </div>\r\n</div>\r\n",
                host: baseHost,
                encapsulation: ViewEncapsulation.None,
                styles: [".adf-typeahead-widget-container{position:relative;display:block}.adf-typeahead-widget{width:100%}"]
            }] }
];
/** @nocollapse */
TypeaheadWidgetComponent.ctorParameters = () => [
    { type: FormService },
    { type: LogService }
];
if (false) {
    /** @type {?} */
    TypeaheadWidgetComponent.prototype.minTermLength;
    /** @type {?} */
    TypeaheadWidgetComponent.prototype.value;
    /** @type {?} */
    TypeaheadWidgetComponent.prototype.oldValue;
    /** @type {?} */
    TypeaheadWidgetComponent.prototype.options;
    /** @type {?} */
    TypeaheadWidgetComponent.prototype.formService;
    /**
     * @type {?}
     * @private
     */
    TypeaheadWidgetComponent.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHlwZWFoZWFkLndpZGdldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL3R5cGVhaGVhZC90eXBlYWhlYWQud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDOUQsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsU0FBUyxFQUFVLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUUvRCxPQUFPLEVBQUUsUUFBUSxFQUFFLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBU2xFLE1BQU0sT0FBTyx3QkFBeUIsU0FBUSxlQUFlOzs7OztJQU96RCxZQUFtQixXQUF3QixFQUN2QixVQUFzQjtRQUN0QyxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7UUFGSixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN2QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBTjFDLGtCQUFhLEdBQVcsQ0FBQyxDQUFDO1FBRzFCLFlBQU8sR0FBc0IsRUFBRSxDQUFDO0lBS2hDLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUU7WUFDOUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7U0FDNUI7YUFBTSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLG1CQUFtQixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFO1lBQ2xFLElBQUksQ0FBQyw4QkFBOEIsRUFBRSxDQUFDO1NBQ3pDO1FBQ0QsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFLEVBQUU7WUFDdkIsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQztTQUNqQztJQUNMLENBQUM7Ozs7SUFFRCxpQkFBaUI7UUFDYixJQUFJLENBQUMsV0FBVzthQUNYLGtCQUFrQixDQUNmLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQ2hCO2FBQ0EsU0FBUzs7OztRQUNOLENBQUMsZUFBa0MsRUFBRSxFQUFFOztrQkFDN0IsT0FBTyxHQUFHLGVBQWUsSUFBSSxFQUFFO1lBQ3JDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQzs7a0JBRXZCLFVBQVUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUs7WUFDbkMsSUFBSSxVQUFVLEVBQUU7O3NCQUNOLFFBQVEsR0FBRyxPQUFPLENBQUMsSUFBSTs7OztnQkFBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxVQUFVLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxLQUFLLFVBQVUsQ0FBQyxpQkFBaUIsRUFBRSxFQUFDO2dCQUNuSSxJQUFJLFFBQVEsRUFBRTtvQkFDVixJQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7aUJBQzlCO2FBQ0o7WUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNoQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQzVCLENBQUM7Ozs7UUFDRCxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFDakMsQ0FBQztJQUNWLENBQUM7Ozs7SUFFRCw4QkFBOEI7UUFDMUIsSUFBSSxDQUFDLFdBQVc7YUFDWCw2QkFBNkIsQ0FDMUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQ25DLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUNoQjthQUNBLFNBQVM7Ozs7UUFDTixDQUFDLGVBQWtDLEVBQUUsRUFBRTs7a0JBQzdCLE9BQU8sR0FBRyxlQUFlLElBQUksRUFBRTtZQUNyQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7O2tCQUV2QixVQUFVLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLO1lBQ25DLElBQUksVUFBVSxFQUFFOztzQkFDTixRQUFRLEdBQUcsT0FBTyxDQUFDLElBQUk7Ozs7Z0JBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssVUFBVSxFQUFDO2dCQUMvRCxJQUFJLFFBQVEsRUFBRTtvQkFDVixJQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUM7aUJBQzlCO2FBQ0o7WUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNoQyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQzVCLENBQUM7Ozs7UUFDRCxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFDakMsQ0FBQztJQUNWLENBQUM7Ozs7SUFFRCxVQUFVOztjQUNBLEdBQUcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDLGlCQUFpQixFQUFFO1FBQ2pELE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTTs7OztRQUFDLENBQUMsSUFBSSxFQUFFLEVBQUU7O2tCQUNoQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtZQUMxQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDbEMsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELGlCQUFpQixDQUFDLFVBQWtCOztjQUMxQixNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSTs7OztRQUFDLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsS0FBSyxVQUFVLENBQUMsaUJBQWlCLEVBQUUsRUFBQztRQUMvSCxPQUFPLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsS0FBb0I7UUFDeEIsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQzlGLElBQUksS0FBSyxDQUFDLE9BQU8sS0FBSyxNQUFNLElBQUksS0FBSyxDQUFDLE9BQU8sS0FBSyxLQUFLLEVBQUU7Z0JBQ3JELElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtvQkFDekMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7b0JBQ2pDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztvQkFDM0IsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO3dCQUNwQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztxQkFDekM7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ3pELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztZQUMzQixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztTQUNyQjtJQUNMLENBQUM7Ozs7O0lBRUQsWUFBWSxDQUFDLElBQXFCO1FBQzlCLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztZQUMzQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDbkM7SUFDTCxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDbEMsQ0FBQzs7OztJQUVELGNBQWM7UUFDVixPQUFPLElBQUksQ0FBQyxLQUFLLEtBQUssSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLEtBQUssU0FBUyxDQUFDO0lBQzNELENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLEtBQVU7UUFDbEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDakMsQ0FBQzs7OztJQUVELGNBQWM7UUFDVixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDekQsQ0FBQzs7O1lBcklKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1QixxOENBQXNDO2dCQUV0QyxJQUFJLEVBQUUsUUFBUTtnQkFDZCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7YUFDeEM7Ozs7WUFWUSxXQUFXO1lBSFgsVUFBVTs7OztJQWdCZixpREFBMEI7O0lBQzFCLHlDQUFjOztJQUNkLDRDQUFpQjs7SUFDakIsMkNBQWdDOztJQUVwQiwrQ0FBK0I7Ozs7O0lBQy9CLDhDQUE4QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4vKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgeyBMb2dTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vc2VydmljZXMvbG9nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBFTlRFUiwgRVNDQVBFIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2tleWNvZGVzJztcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi8uLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRPcHRpb24gfSBmcm9tICcuLy4uL2NvcmUvZm9ybS1maWVsZC1vcHRpb24nO1xyXG5pbXBvcnQgeyBiYXNlSG9zdCwgV2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi8uLi93aWRnZXQuY29tcG9uZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICd0eXBlYWhlYWQtd2lkZ2V0JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi90eXBlYWhlYWQud2lkZ2V0Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vdHlwZWFoZWFkLndpZGdldC5zY3NzJ10sXHJcbiAgICBob3N0OiBiYXNlSG9zdCxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIFR5cGVhaGVhZFdpZGdldENvbXBvbmVudCBleHRlbmRzIFdpZGdldENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgbWluVGVybUxlbmd0aDogbnVtYmVyID0gMTtcclxuICAgIHZhbHVlOiBzdHJpbmc7XHJcbiAgICBvbGRWYWx1ZTogc3RyaW5nO1xyXG4gICAgb3B0aW9uczogRm9ybUZpZWxkT3B0aW9uW10gPSBbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZm9ybVNlcnZpY2U6IEZvcm1TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlKSB7XHJcbiAgICAgICAgc3VwZXIoZm9ybVNlcnZpY2UpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmZpZWxkLmZvcm0udGFza0lkICYmIHRoaXMuZmllbGQucmVzdFVybCkge1xyXG4gICAgICAgICAgICB0aGlzLmdldFZhbHVlc0J5VGFza0lkKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLmZpZWxkLmZvcm0ucHJvY2Vzc0RlZmluaXRpb25JZCAmJiB0aGlzLmZpZWxkLnJlc3RVcmwpIHtcclxuICAgICAgICAgICAgdGhpcy5nZXRWYWx1ZXNCeVByb2Nlc3NEZWZpbml0aW9uSWQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHRoaXMuaXNSZWFkT25seVR5cGUoKSkge1xyXG4gICAgICAgICAgICB0aGlzLnZhbHVlID0gdGhpcy5maWVsZC52YWx1ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VmFsdWVzQnlUYXNrSWQoKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtU2VydmljZVxyXG4gICAgICAgICAgICAuZ2V0UmVzdEZpZWxkVmFsdWVzKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5maWVsZC5mb3JtLnRhc2tJZCxcclxuICAgICAgICAgICAgICAgIHRoaXMuZmllbGQuaWRcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgKGZvcm1GaWVsZE9wdGlvbjogRm9ybUZpZWxkT3B0aW9uW10pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBvcHRpb25zID0gZm9ybUZpZWxkT3B0aW9uIHx8IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmllbGQub3B0aW9ucyA9IG9wdGlvbnM7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGZpZWxkVmFsdWUgPSB0aGlzLmZpZWxkLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChmaWVsZFZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHRvU2VsZWN0ID0gb3B0aW9ucy5maW5kKChpdGVtKSA9PiBpdGVtLmlkID09PSBmaWVsZFZhbHVlIHx8IGl0ZW0ubmFtZS50b0xvY2FsZUxvd2VyQ2FzZSgpID09PSBmaWVsZFZhbHVlLnRvTG9jYWxlTG93ZXJDYXNlKCkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodG9TZWxlY3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudmFsdWUgPSB0b1NlbGVjdC5uYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub25GaWVsZENoYW5nZWQodGhpcy5maWVsZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5maWVsZC51cGRhdGVGb3JtKCk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VmFsdWVzQnlQcm9jZXNzRGVmaW5pdGlvbklkKCkge1xyXG4gICAgICAgIHRoaXMuZm9ybVNlcnZpY2VcclxuICAgICAgICAgICAgLmdldFJlc3RGaWVsZFZhbHVlc0J5UHJvY2Vzc0lkKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5maWVsZC5mb3JtLnByb2Nlc3NEZWZpbml0aW9uSWQsXHJcbiAgICAgICAgICAgICAgICB0aGlzLmZpZWxkLmlkXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIChmb3JtRmllbGRPcHRpb246IEZvcm1GaWVsZE9wdGlvbltdKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgb3B0aW9ucyA9IGZvcm1GaWVsZE9wdGlvbiB8fCBbXTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZpZWxkLm9wdGlvbnMgPSBvcHRpb25zO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWVsZFZhbHVlID0gdGhpcy5maWVsZC52YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZmllbGRWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB0b1NlbGVjdCA9IG9wdGlvbnMuZmluZCgoaXRlbSkgPT4gaXRlbS5pZCA9PT0gZmllbGRWYWx1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0b1NlbGVjdCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy52YWx1ZSA9IHRvU2VsZWN0Lm5hbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vbkZpZWxkQ2hhbmdlZCh0aGlzLmZpZWxkKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZpZWxkLnVwZGF0ZUZvcm0oKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycilcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRPcHRpb25zKCk6IEZvcm1GaWVsZE9wdGlvbltdIHtcclxuICAgICAgICBjb25zdCB2YWwgPSB0aGlzLnZhbHVlLnRyaW0oKS50b0xvY2FsZUxvd2VyQ2FzZSgpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZpZWxkLm9wdGlvbnMuZmlsdGVyKChpdGVtKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IG5hbWUgPSBpdGVtLm5hbWUudG9Mb2NhbGVMb3dlckNhc2UoKTtcclxuICAgICAgICAgICAgcmV0dXJuIG5hbWUuaW5kZXhPZih2YWwpID4gLTE7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgaXNWYWxpZE9wdGlvbk5hbWUob3B0aW9uTmFtZTogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgY29uc3Qgb3B0aW9uID0gdGhpcy5maWVsZC5vcHRpb25zLmZpbmQoKGl0ZW0pID0+IGl0ZW0ubmFtZSAmJiBpdGVtLm5hbWUudG9Mb2NhbGVMb3dlckNhc2UoKSA9PT0gb3B0aW9uTmFtZS50b0xvY2FsZUxvd2VyQ2FzZSgpKTtcclxuICAgICAgICByZXR1cm4gb3B0aW9uID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIG9uS2V5VXAoZXZlbnQ6IEtleWJvYXJkRXZlbnQpIHtcclxuICAgICAgICBpZiAodGhpcy52YWx1ZSAmJiB0aGlzLnZhbHVlLnRyaW0oKS5sZW5ndGggPj0gdGhpcy5taW5UZXJtTGVuZ3RoICYmIHRoaXMub2xkVmFsdWUgIT09IHRoaXMudmFsdWUpIHtcclxuICAgICAgICAgICAgaWYgKGV2ZW50LmtleUNvZGUgIT09IEVTQ0FQRSAmJiBldmVudC5rZXlDb2RlICE9PSBFTlRFUikge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudmFsdWUubGVuZ3RoID49IHRoaXMubWluVGVybUxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub3B0aW9ucyA9IHRoaXMuZ2V0T3B0aW9ucygpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub2xkVmFsdWUgPSB0aGlzLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICh0aGlzLmlzVmFsaWRPcHRpb25OYW1lKHRoaXMudmFsdWUpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZmllbGQudmFsdWUgPSB0aGlzLm9wdGlvbnNbMF0uaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLmlzVmFsdWVEZWZpbmVkKCkgJiYgdGhpcy52YWx1ZS50cmltKCkubGVuZ3RoID09PSAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMub2xkVmFsdWUgPSB0aGlzLnZhbHVlO1xyXG4gICAgICAgICAgICB0aGlzLm9wdGlvbnMgPSBbXTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25JdGVtU2VsZWN0KGl0ZW06IEZvcm1GaWVsZE9wdGlvbikge1xyXG4gICAgICAgIGlmIChpdGVtKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZmllbGQudmFsdWUgPSBpdGVtLmlkO1xyXG4gICAgICAgICAgICB0aGlzLnZhbHVlID0gaXRlbS5uYW1lO1xyXG4gICAgICAgICAgICB0aGlzLm9uRmllbGRDaGFuZ2VkKHRoaXMuZmllbGQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZSgpIHtcclxuICAgICAgICB0aGlzLmZpZWxkLnZhbHVlID0gdGhpcy52YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBpc1ZhbHVlRGVmaW5lZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy52YWx1ZSAhPT0gbnVsbCAmJiB0aGlzLnZhbHVlICE9PSB1bmRlZmluZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgaGFuZGxlRXJyb3IoZXJyb3I6IGFueSkge1xyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJvcik7XHJcbiAgICB9XHJcblxyXG4gICAgaXNSZWFkT25seVR5cGUoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZmllbGQudHlwZSA9PT0gJ3JlYWRvbmx5JyA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19