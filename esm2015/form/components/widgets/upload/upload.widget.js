/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { LogService } from '../../../../services/log.service';
import { ThumbnailService } from '../../../../services/thumbnail.service';
import { Component, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { from } from 'rxjs';
import { FormService } from '../../../services/form.service';
import { ProcessContentService } from '../../../services/process-content.service';
import { ContentLinkModel } from '../core/content-link.model';
import { baseHost, WidgetComponent } from './../widget.component';
import { map, mergeMap } from 'rxjs/operators';
export class UploadWidgetComponent extends WidgetComponent {
    /**
     * @param {?} formService
     * @param {?} logService
     * @param {?} thumbnailService
     * @param {?} processContentService
     */
    constructor(formService, logService, thumbnailService, processContentService) {
        super(formService);
        this.formService = formService;
        this.logService = logService;
        this.thumbnailService = thumbnailService;
        this.processContentService = processContentService;
        this.multipleOption = '';
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.field &&
            this.field.value &&
            this.field.value.length > 0) {
            this.hasFile = true;
        }
        this.getMultipleFileParam();
    }
    /**
     * @param {?} file
     * @return {?}
     */
    removeFile(file) {
        if (this.field) {
            this.removeElementFromList(file);
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onFileChanged(event) {
        /** @type {?} */
        const files = event.target.files;
        /** @type {?} */
        let filesSaved = [];
        if (this.field.json.value) {
            filesSaved = [...this.field.json.value];
        }
        if (files && files.length > 0) {
            from(files)
                .pipe(mergeMap((/**
             * @param {?} file
             * @return {?}
             */
            (file) => this.uploadRawContent(file))))
                .subscribe((/**
             * @param {?} res
             * @return {?}
             */
            (res) => filesSaved.push(res)), (/**
             * @return {?}
             */
            () => this.logService.error('Error uploading file. See console output for more details.')), (/**
             * @return {?}
             */
            () => {
                this.field.value = filesSaved;
                this.field.json.value = filesSaved;
                this.hasFile = true;
            }));
        }
    }
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    uploadRawContent(file) {
        return this.processContentService.createTemporaryRawRelatedContent(file)
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            this.logService.info(response);
            response.contentBlob = file;
            return response;
        })));
    }
    /**
     * @return {?}
     */
    getMultipleFileParam() {
        if (this.field &&
            this.field.params &&
            this.field.params.multiple) {
            this.multipleOption = this.field.params.multiple ? 'multiple' : '';
        }
    }
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    removeElementFromList(file) {
        /** @type {?} */
        const index = this.field.value.indexOf(file);
        if (index !== -1) {
            this.field.value.splice(index, 1);
            this.field.json.value = this.field.value;
            this.field.updateForm();
        }
        this.hasFile = this.field.value.length > 0;
        this.resetFormValueWithNoFiles();
    }
    /**
     * @private
     * @return {?}
     */
    resetFormValueWithNoFiles() {
        if (this.field.value.length === 0) {
            this.field.value = [];
            this.field.json.value = [];
        }
    }
    /**
     * @param {?} mimeType
     * @return {?}
     */
    getIcon(mimeType) {
        return this.thumbnailService.getMimeTypeIcon(mimeType);
    }
    /**
     * @param {?} contentLinkModel
     * @return {?}
     */
    fileClicked(contentLinkModel) {
        /** @type {?} */
        const file = new ContentLinkModel(contentLinkModel);
        /** @type {?} */
        let fetch = this.processContentService.getContentPreview(file.id);
        if (file.isTypeImage() || file.isTypePdf()) {
            fetch = this.processContentService.getFileRawContent(file.id);
        }
        fetch.subscribe((/**
         * @param {?} blob
         * @return {?}
         */
        (blob) => {
            file.contentBlob = blob;
            this.formService.formContentClicked.next(file);
        }), (/**
         * @return {?}
         */
        () => {
            this.logService.error('Unable to send event for file ' + file.name);
        }));
    }
}
UploadWidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'upload-widget',
                template: "<div class=\"adf-upload-widget {{field.className}}\"\r\n     [class.adf-invalid]=\"!field.isValid\"\r\n     [class.adf-readonly]=\"field.readOnly\">\r\n    <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}<span *ngIf=\"isRequired()\">*</span></label>\r\n    <div class=\"adf-upload-widget-container\">\r\n        <div>\r\n            <mat-list *ngIf=\"hasFile\">\r\n                <mat-list-item class=\"adf-upload-files-row\" *ngFor=\"let file of field.value\">\r\n                    <img mat-list-icon class=\"adf-upload-widget__icon\"\r\n                         [id]=\"'file-'+file.id+'-icon'\"\r\n                         [src]=\"getIcon(file.mimeType)\"\r\n                         [alt]=\"mimeTypeIcon\"\r\n                         (click)=\"fileClicked(file)\"\r\n                         (keyup.enter)=\"fileClicked(file)\"\r\n                         role=\"button\"\r\n                         tabindex=\"0\"/>\r\n                    <span matLine id=\"{{'file-'+file.id}}\" (click)=\"fileClicked(file)\" (keyup.enter)=\"fileClicked(file)\"\r\n                          role=\"button\" tabindex=\"0\" class=\"adf-file\">{{file.name}}</span>\r\n                    <button *ngIf=\"!field.readOnly\" mat-icon-button [id]=\"'file-'+file.id+'-remove'\"\r\n                            (click)=\"removeFile(file);\" (keyup.enter)=\"removeFile(file);\">\r\n                        <mat-icon class=\"mat-24\">highlight_off</mat-icon>\r\n                    </button>\r\n                </mat-list-item>\r\n            </mat-list>\r\n        </div>\r\n\r\n        <div *ngIf=\"(!hasFile || multipleOption) && !field.readOnly\">\r\n            <button mat-raised-button color=\"primary\" (click)=\"uploadFiles.click()\">\r\n                {{ 'FORM.FIELD.UPLOAD' | translate }}<mat-icon>file_upload</mat-icon>\r\n                <input #uploadFiles\r\n                       [multiple]=\"multipleOption\"\r\n                       type=\"file\"\r\n                       [id]=\"field.id\"\r\n                       (change)=\"onFileChanged($event)\"/>\r\n            </button>\r\n        </div>\r\n    </div>\r\n    <error-widget [error]=\"field.validationSummary\"></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n</div>\r\n",
                host: baseHost,
                encapsulation: ViewEncapsulation.None,
                styles: [".adf-upload-widget-container{margin-bottom:15px}.adf-upload-widget-container input{display:none}.adf-upload-widget{width:100%;word-break:break-all;padding:.4375em 0;border-top:.84375em solid transparent}.adf-upload-widget__icon{padding:6px;float:left;cursor:pointer}.adf-upload-widget__reset{margin-top:-2px}.adf-upload-files-row .mat-line{margin-bottom:0}"]
            }] }
];
/** @nocollapse */
UploadWidgetComponent.ctorParameters = () => [
    { type: FormService },
    { type: LogService },
    { type: ThumbnailService },
    { type: ProcessContentService }
];
UploadWidgetComponent.propDecorators = {
    fileInput: [{ type: ViewChild, args: ['uploadFiles', { static: true },] }]
};
if (false) {
    /** @type {?} */
    UploadWidgetComponent.prototype.hasFile;
    /** @type {?} */
    UploadWidgetComponent.prototype.displayText;
    /** @type {?} */
    UploadWidgetComponent.prototype.multipleOption;
    /** @type {?} */
    UploadWidgetComponent.prototype.mimeTypeIcon;
    /** @type {?} */
    UploadWidgetComponent.prototype.fileInput;
    /** @type {?} */
    UploadWidgetComponent.prototype.formService;
    /**
     * @type {?}
     * @private
     */
    UploadWidgetComponent.prototype.logService;
    /**
     * @type {?}
     * @private
     */
    UploadWidgetComponent.prototype.thumbnailService;
    /** @type {?} */
    UploadWidgetComponent.prototype.processContentService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLndpZGdldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL3VwbG9hZC91cGxvYWQud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQ0EsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQzlELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFVLFNBQVMsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM1RixPQUFPLEVBQUUsSUFBSSxFQUFjLE1BQU0sTUFBTSxDQUFDO0FBQ3hDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUM3RCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUNsRixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUM5RCxPQUFPLEVBQUUsUUFBUSxFQUFFLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFTL0MsTUFBTSxPQUFPLHFCQUFzQixTQUFRLGVBQWU7Ozs7Ozs7SUFVdEQsWUFBbUIsV0FBd0IsRUFDdkIsVUFBc0IsRUFDdEIsZ0JBQWtDLEVBQ25DLHFCQUE0QztRQUMzRCxLQUFLLENBQUMsV0FBVyxDQUFDLENBQUM7UUFKSixnQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUN2QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbkMsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtRQVQvRCxtQkFBYyxHQUFXLEVBQUUsQ0FBQztJQVc1QixDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksSUFBSSxDQUFDLEtBQUs7WUFDVixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUs7WUFDaEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztTQUN2QjtRQUNELElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0lBQ2hDLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLElBQVM7UUFDaEIsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1osSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3BDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxhQUFhLENBQUMsS0FBVTs7Y0FDZCxLQUFLLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLOztZQUM1QixVQUFVLEdBQUcsRUFBRTtRQUVuQixJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUN2QixVQUFVLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQzNDO1FBRUQsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDM0IsSUFBSSxDQUFDLEtBQUssQ0FBQztpQkFDTixJQUFJLENBQUMsUUFBUTs7OztZQUFDLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEVBQUMsQ0FBQztpQkFDckQsU0FBUzs7OztZQUNOLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQzs7O1lBQzdCLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLDREQUE0RCxDQUFDOzs7WUFDekYsR0FBRyxFQUFFO2dCQUNELElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQztnQkFDOUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLFVBQVUsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7WUFDeEIsQ0FBQyxFQUNKLENBQUM7U0FDVDtJQUNMLENBQUM7Ozs7OztJQUVPLGdCQUFnQixDQUFDLElBQUk7UUFDekIsT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUMsZ0NBQWdDLENBQUMsSUFBSSxDQUFDO2FBQ25FLElBQUksQ0FDRCxHQUFHOzs7O1FBQUMsQ0FBQyxRQUFhLEVBQUUsRUFBRTtZQUNsQixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMvQixRQUFRLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztZQUM1QixPQUFPLFFBQVEsQ0FBQztRQUNwQixDQUFDLEVBQUMsQ0FDTCxDQUFDO0lBQ1YsQ0FBQzs7OztJQUVELG9CQUFvQjtRQUNoQixJQUFJLElBQUksQ0FBQyxLQUFLO1lBQ1YsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNO1lBQ2pCLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTtZQUM1QixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7U0FDdEU7SUFDTCxDQUFDOzs7Ozs7SUFFTyxxQkFBcUIsQ0FBQyxJQUFJOztjQUN4QixLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztRQUU1QyxJQUFJLEtBQUssS0FBSyxDQUFDLENBQUMsRUFBRTtZQUNkLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDbEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUM7U0FDM0I7UUFFRCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFFM0MsSUFBSSxDQUFDLHlCQUF5QixFQUFFLENBQUM7SUFDckMsQ0FBQzs7Ozs7SUFFTyx5QkFBeUI7UUFDN0IsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQy9CLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEVBQUUsQ0FBQztZQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1NBQzlCO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsUUFBUTtRQUNaLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMzRCxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxnQkFBcUI7O2NBQ3ZCLElBQUksR0FBRyxJQUFJLGdCQUFnQixDQUFDLGdCQUFnQixDQUFDOztZQUMvQyxLQUFLLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUM7UUFDakUsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRSxFQUFFO1lBQ3hDLEtBQUssR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ2pFO1FBQ0QsS0FBSyxDQUFDLFNBQVM7Ozs7UUFDWCxDQUFDLElBQVUsRUFBRSxFQUFFO1lBQ1gsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDeEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkQsQ0FBQzs7O1FBQ0QsR0FBRyxFQUFFO1lBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsZ0NBQWdDLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3hFLENBQUMsRUFDSixDQUFDO0lBQ04sQ0FBQzs7O1lBekhKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsZUFBZTtnQkFDekIsNHpFQUFtQztnQkFFbkMsSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2FBQ3hDOzs7O1lBWlEsV0FBVztZQUpYLFVBQVU7WUFDVixnQkFBZ0I7WUFJaEIscUJBQXFCOzs7d0JBbUJ6QixTQUFTLFNBQUMsYUFBYSxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQzs7OztJQUx4Qyx3Q0FBaUI7O0lBQ2pCLDRDQUFvQjs7SUFDcEIsK0NBQTRCOztJQUM1Qiw2Q0FBcUI7O0lBRXJCLDBDQUNzQjs7SUFFViw0Q0FBK0I7Ozs7O0lBQy9CLDJDQUE4Qjs7Ozs7SUFDOUIsaURBQTBDOztJQUMxQyxzREFBbUQiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cblxuLyohXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cbiAqXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4gKlxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuICpcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXG4gKi9cblxuLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xuXG5pbXBvcnQgeyBMb2dTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vc2VydmljZXMvbG9nLnNlcnZpY2UnO1xuaW1wb3J0IHsgVGh1bWJuYWlsU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL3NlcnZpY2VzL3RodW1ibmFpbC5zZXJ2aWNlJztcbmltcG9ydCB7IENvbXBvbmVudCwgRWxlbWVudFJlZiwgT25Jbml0LCBWaWV3Q2hpbGQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBmcm9tLCBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XG5pbXBvcnQgeyBQcm9jZXNzQ29udGVudFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9wcm9jZXNzLWNvbnRlbnQuc2VydmljZSc7XG5pbXBvcnQgeyBDb250ZW50TGlua01vZGVsIH0gZnJvbSAnLi4vY29yZS9jb250ZW50LWxpbmsubW9kZWwnO1xuaW1wb3J0IHsgYmFzZUhvc3QsIFdpZGdldENvbXBvbmVudCB9IGZyb20gJy4vLi4vd2lkZ2V0LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBtYXAsIG1lcmdlTWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ3VwbG9hZC13aWRnZXQnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi91cGxvYWQud2lkZ2V0Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL3VwbG9hZC53aWRnZXQuc2NzcyddLFxuICAgIGhvc3Q6IGJhc2VIb3N0LFxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcbn0pXG5leHBvcnQgY2xhc3MgVXBsb2FkV2lkZ2V0Q29tcG9uZW50IGV4dGVuZHMgV2lkZ2V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAgIGhhc0ZpbGU6IGJvb2xlYW47XG4gICAgZGlzcGxheVRleHQ6IHN0cmluZztcbiAgICBtdWx0aXBsZU9wdGlvbjogc3RyaW5nID0gJyc7XG4gICAgbWltZVR5cGVJY29uOiBzdHJpbmc7XG5cbiAgICBAVmlld0NoaWxkKCd1cGxvYWRGaWxlcycsIHtzdGF0aWM6IHRydWV9KVxuICAgIGZpbGVJbnB1dDogRWxlbWVudFJlZjtcblxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBmb3JtU2VydmljZTogRm9ybVNlcnZpY2UsXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlLFxuICAgICAgICAgICAgICAgIHByaXZhdGUgdGh1bWJuYWlsU2VydmljZTogVGh1bWJuYWlsU2VydmljZSxcbiAgICAgICAgICAgICAgICBwdWJsaWMgcHJvY2Vzc0NvbnRlbnRTZXJ2aWNlOiBQcm9jZXNzQ29udGVudFNlcnZpY2UpIHtcbiAgICAgICAgc3VwZXIoZm9ybVNlcnZpY2UpO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICBpZiAodGhpcy5maWVsZCAmJlxuICAgICAgICAgICAgdGhpcy5maWVsZC52YWx1ZSAmJlxuICAgICAgICAgICAgdGhpcy5maWVsZC52YWx1ZS5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICB0aGlzLmhhc0ZpbGUgPSB0cnVlO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuZ2V0TXVsdGlwbGVGaWxlUGFyYW0oKTtcbiAgICB9XG5cbiAgICByZW1vdmVGaWxlKGZpbGU6IGFueSkge1xuICAgICAgICBpZiAodGhpcy5maWVsZCkge1xuICAgICAgICAgICAgdGhpcy5yZW1vdmVFbGVtZW50RnJvbUxpc3QoZmlsZSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBvbkZpbGVDaGFuZ2VkKGV2ZW50OiBhbnkpIHtcbiAgICAgICAgY29uc3QgZmlsZXMgPSBldmVudC50YXJnZXQuZmlsZXM7XG4gICAgICAgIGxldCBmaWxlc1NhdmVkID0gW107XG5cbiAgICAgICAgaWYgKHRoaXMuZmllbGQuanNvbi52YWx1ZSkge1xuICAgICAgICAgICAgZmlsZXNTYXZlZCA9IFsuLi50aGlzLmZpZWxkLmpzb24udmFsdWVdO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGZpbGVzICYmIGZpbGVzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIGZyb20oZmlsZXMpXG4gICAgICAgICAgICAgICAgLnBpcGUobWVyZ2VNYXAoKGZpbGUpID0+IHRoaXMudXBsb2FkUmF3Q29udGVudChmaWxlKSkpXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShcbiAgICAgICAgICAgICAgICAgICAgKHJlcykgPT4gZmlsZXNTYXZlZC5wdXNoKHJlcyksXG4gICAgICAgICAgICAgICAgICAgICgpID0+IHRoaXMubG9nU2VydmljZS5lcnJvcignRXJyb3IgdXBsb2FkaW5nIGZpbGUuIFNlZSBjb25zb2xlIG91dHB1dCBmb3IgbW9yZSBkZXRhaWxzLicpLFxuICAgICAgICAgICAgICAgICAgICAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZpZWxkLnZhbHVlID0gZmlsZXNTYXZlZDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZmllbGQuanNvbi52YWx1ZSA9IGZpbGVzU2F2ZWQ7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmhhc0ZpbGUgPSB0cnVlO1xuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgdXBsb2FkUmF3Q29udGVudChmaWxlKTogT2JzZXJ2YWJsZTxhbnk+IHtcbiAgICAgICAgcmV0dXJuIHRoaXMucHJvY2Vzc0NvbnRlbnRTZXJ2aWNlLmNyZWF0ZVRlbXBvcmFyeVJhd1JlbGF0ZWRDb250ZW50KGZpbGUpXG4gICAgICAgICAgICAucGlwZShcbiAgICAgICAgICAgICAgICBtYXAoKHJlc3BvbnNlOiBhbnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmluZm8ocmVzcG9uc2UpO1xuICAgICAgICAgICAgICAgICAgICByZXNwb25zZS5jb250ZW50QmxvYiA9IGZpbGU7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcbiAgICAgICAgICAgICAgICB9KVxuICAgICAgICAgICAgKTtcbiAgICB9XG5cbiAgICBnZXRNdWx0aXBsZUZpbGVQYXJhbSgpIHtcbiAgICAgICAgaWYgKHRoaXMuZmllbGQgJiZcbiAgICAgICAgICAgIHRoaXMuZmllbGQucGFyYW1zICYmXG4gICAgICAgICAgICB0aGlzLmZpZWxkLnBhcmFtcy5tdWx0aXBsZSkge1xuICAgICAgICAgICAgdGhpcy5tdWx0aXBsZU9wdGlvbiA9IHRoaXMuZmllbGQucGFyYW1zLm11bHRpcGxlID8gJ211bHRpcGxlJyA6ICcnO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSByZW1vdmVFbGVtZW50RnJvbUxpc3QoZmlsZSkge1xuICAgICAgICBjb25zdCBpbmRleCA9IHRoaXMuZmllbGQudmFsdWUuaW5kZXhPZihmaWxlKTtcblxuICAgICAgICBpZiAoaW5kZXggIT09IC0xKSB7XG4gICAgICAgICAgICB0aGlzLmZpZWxkLnZhbHVlLnNwbGljZShpbmRleCwgMSk7XG4gICAgICAgICAgICB0aGlzLmZpZWxkLmpzb24udmFsdWUgPSB0aGlzLmZpZWxkLnZhbHVlO1xuICAgICAgICAgICAgdGhpcy5maWVsZC51cGRhdGVGb3JtKCk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmhhc0ZpbGUgPSB0aGlzLmZpZWxkLnZhbHVlLmxlbmd0aCA+IDA7XG5cbiAgICAgICAgdGhpcy5yZXNldEZvcm1WYWx1ZVdpdGhOb0ZpbGVzKCk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSByZXNldEZvcm1WYWx1ZVdpdGhOb0ZpbGVzKCkge1xuICAgICAgICBpZiAodGhpcy5maWVsZC52YWx1ZS5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgIHRoaXMuZmllbGQudmFsdWUgPSBbXTtcbiAgICAgICAgICAgIHRoaXMuZmllbGQuanNvbi52YWx1ZSA9IFtdO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgZ2V0SWNvbihtaW1lVHlwZSkge1xuICAgICAgICByZXR1cm4gdGhpcy50aHVtYm5haWxTZXJ2aWNlLmdldE1pbWVUeXBlSWNvbihtaW1lVHlwZSk7XG4gICAgfVxuXG4gICAgZmlsZUNsaWNrZWQoY29udGVudExpbmtNb2RlbDogYW55KTogdm9pZCB7XG4gICAgICAgIGNvbnN0IGZpbGUgPSBuZXcgQ29udGVudExpbmtNb2RlbChjb250ZW50TGlua01vZGVsKTtcbiAgICAgICAgbGV0IGZldGNoID0gdGhpcy5wcm9jZXNzQ29udGVudFNlcnZpY2UuZ2V0Q29udGVudFByZXZpZXcoZmlsZS5pZCk7XG4gICAgICAgIGlmIChmaWxlLmlzVHlwZUltYWdlKCkgfHwgZmlsZS5pc1R5cGVQZGYoKSkge1xuICAgICAgICAgICAgZmV0Y2ggPSB0aGlzLnByb2Nlc3NDb250ZW50U2VydmljZS5nZXRGaWxlUmF3Q29udGVudChmaWxlLmlkKTtcbiAgICAgICAgfVxuICAgICAgICBmZXRjaC5zdWJzY3JpYmUoXG4gICAgICAgICAgICAoYmxvYjogQmxvYikgPT4ge1xuICAgICAgICAgICAgICAgIGZpbGUuY29udGVudEJsb2IgPSBibG9iO1xuICAgICAgICAgICAgICAgIHRoaXMuZm9ybVNlcnZpY2UuZm9ybUNvbnRlbnRDbGlja2VkLm5leHQoZmlsZSk7XG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcignVW5hYmxlIHRvIHNlbmQgZXZlbnQgZm9yIGZpbGUgJyArIGZpbGUubmFtZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgICk7XG4gICAgfVxufVxuIl19