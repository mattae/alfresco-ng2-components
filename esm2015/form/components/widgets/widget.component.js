/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { FormService } from './../../services/form.service';
import { FormFieldModel } from './core/index';
/** @type {?} */
export const baseHost = {
    '(click)': 'event($event)',
    '(blur)': 'event($event)',
    '(change)': 'event($event)',
    '(focus)': 'event($event)',
    '(focusin)': 'event($event)',
    '(focusout)': 'event($event)',
    '(input)': 'event($event)',
    '(invalid)': 'event($event)',
    '(select)': 'event($event)'
};
/**
 * Base widget component.
 */
export class WidgetComponent {
    /**
     * @param {?=} formService
     */
    constructor(formService) {
        this.formService = formService;
        /**
         * Does the widget show a read-only value? (ie, can't be edited)
         */
        this.readOnly = false;
        /**
         * Emitted when a field value changes.
         */
        this.fieldChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    hasField() {
        return this.field ? true : false;
    }
    // Note for developers:
    // returns <any> object to be able binding it to the <element required="required"> attribute
    /**
     * @return {?}
     */
    isRequired() {
        if (this.field && this.field.required) {
            return true;
        }
        return null;
    }
    /**
     * @return {?}
     */
    isValid() {
        return this.field.validationSummary ? true : false;
    }
    /**
     * @return {?}
     */
    hasValue() {
        return this.field &&
            this.field.value !== null &&
            this.field.value !== undefined;
    }
    /**
     * @return {?}
     */
    isInvalidFieldRequired() {
        return !this.field.isValid && !this.field.validationSummary && this.isRequired();
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.fieldChanged.emit(this.field);
    }
    /**
     * @param {?} field
     * @return {?}
     */
    checkVisibility(field) {
        this.fieldChanged.emit(field);
    }
    /**
     * @param {?} field
     * @return {?}
     */
    onFieldChanged(field) {
        this.fieldChanged.emit(field);
    }
    /**
     * @protected
     * @param {?} field
     * @return {?}
     */
    getHyperlinkUrl(field) {
        /** @type {?} */
        let url = WidgetComponent.DEFAULT_HYPERLINK_URL;
        if (field && field.hyperlinkUrl) {
            url = field.hyperlinkUrl;
            if (!/^https?:\/\//i.test(url)) {
                url = `${WidgetComponent.DEFAULT_HYPERLINK_SCHEME}${url}`;
            }
        }
        return url;
    }
    /**
     * @protected
     * @param {?} field
     * @return {?}
     */
    getHyperlinkText(field) {
        if (field) {
            return field.displayText || field.hyperlinkUrl;
        }
        return null;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    event(event) {
        this.formService.formEvents.next(event);
    }
}
WidgetComponent.DEFAULT_HYPERLINK_URL = '#';
WidgetComponent.DEFAULT_HYPERLINK_SCHEME = 'http://';
WidgetComponent.decorators = [
    { type: Component, args: [{
                selector: 'base-widget',
                template: '',
                host: baseHost,
                encapsulation: ViewEncapsulation.None
            }] }
];
/** @nocollapse */
WidgetComponent.ctorParameters = () => [
    { type: FormService }
];
WidgetComponent.propDecorators = {
    readOnly: [{ type: Input }],
    field: [{ type: Input }],
    fieldChanged: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    WidgetComponent.DEFAULT_HYPERLINK_URL;
    /** @type {?} */
    WidgetComponent.DEFAULT_HYPERLINK_SCHEME;
    /**
     * Does the widget show a read-only value? (ie, can't be edited)
     * @type {?}
     */
    WidgetComponent.prototype.readOnly;
    /**
     * Data to be displayed in the field
     * @type {?}
     */
    WidgetComponent.prototype.field;
    /**
     * Emitted when a field value changes.
     * @type {?}
     */
    WidgetComponent.prototype.fieldChanged;
    /** @type {?} */
    WidgetComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL3dpZGdldC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLE9BQU8sRUFBaUIsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pHLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sY0FBYyxDQUFDOztBQUU5QyxNQUFNLE9BQU8sUUFBUSxHQUFHO0lBQ3BCLFNBQVMsRUFBRSxlQUFlO0lBQzFCLFFBQVEsRUFBRSxlQUFlO0lBQ3pCLFVBQVUsRUFBRSxlQUFlO0lBQzNCLFNBQVMsRUFBRSxlQUFlO0lBQzFCLFdBQVcsRUFBRSxlQUFlO0lBQzVCLFlBQVksRUFBRSxlQUFlO0lBQzdCLFNBQVMsRUFBRSxlQUFlO0lBQzFCLFdBQVcsRUFBRSxlQUFlO0lBQzVCLFVBQVUsRUFBRSxlQUFlO0NBQzlCOzs7O0FBV0QsTUFBTSxPQUFPLGVBQWU7Ozs7SUFtQnhCLFlBQW1CLFdBQXlCO1FBQXpCLGdCQUFXLEdBQVgsV0FBVyxDQUFjOzs7O1FBWjVDLGFBQVEsR0FBWSxLQUFLLENBQUM7Ozs7UUFVMUIsaUJBQVksR0FBaUMsSUFBSSxZQUFZLEVBQWtCLENBQUM7SUFHaEYsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ3JDLENBQUM7Ozs7OztJQUlELFVBQVU7UUFDTixJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUU7WUFDbkMsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7SUFFRCxPQUFPO1FBQ0gsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUN2RCxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLE9BQU8sSUFBSSxDQUFDLEtBQUs7WUFDYixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssS0FBSyxJQUFJO1lBQ3pCLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxLQUFLLFNBQVMsQ0FBQztJQUN2QyxDQUFDOzs7O0lBRUQsc0JBQXNCO1FBQ2xCLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWlCLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQ3JGLENBQUM7Ozs7SUFFRCxlQUFlO1FBQ1gsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7Ozs7O0lBRUQsZUFBZSxDQUFDLEtBQXFCO1FBQ2pDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2xDLENBQUM7Ozs7O0lBRUQsY0FBYyxDQUFDLEtBQXFCO1FBQ2hDLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2xDLENBQUM7Ozs7OztJQUVTLGVBQWUsQ0FBQyxLQUFxQjs7WUFDdkMsR0FBRyxHQUFHLGVBQWUsQ0FBQyxxQkFBcUI7UUFDL0MsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLFlBQVksRUFBRTtZQUM3QixHQUFHLEdBQUcsS0FBSyxDQUFDLFlBQVksQ0FBQztZQUN6QixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDNUIsR0FBRyxHQUFHLEdBQUcsZUFBZSxDQUFDLHdCQUF3QixHQUFHLEdBQUcsRUFBRSxDQUFDO2FBQzdEO1NBQ0o7UUFDRCxPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7Ozs7OztJQUVTLGdCQUFnQixDQUFDLEtBQXFCO1FBQzVDLElBQUksS0FBSyxFQUFFO1lBQ1AsT0FBTyxLQUFLLENBQUMsV0FBVyxJQUFJLEtBQUssQ0FBQyxZQUFZLENBQUM7U0FDbEQ7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOzs7OztJQUVELEtBQUssQ0FBQyxLQUFZO1FBQ2QsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQzVDLENBQUM7O0FBL0VNLHFDQUFxQixHQUFXLEdBQUcsQ0FBQztBQUNwQyx3Q0FBd0IsR0FBVyxTQUFTLENBQUM7O1lBVHZELFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsYUFBYTtnQkFDdkIsUUFBUSxFQUFFLEVBQUU7Z0JBQ1osSUFBSSxFQUFFLFFBQVE7Z0JBQ2QsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7YUFDeEM7Ozs7WUF2QlEsV0FBVzs7O3VCQThCZixLQUFLO29CQUlMLEtBQUs7MkJBTUwsTUFBTTs7OztJQWRQLHNDQUEyQzs7SUFDM0MseUNBQW9EOzs7OztJQUdwRCxtQ0FDMEI7Ozs7O0lBRzFCLGdDQUNzQjs7Ozs7SUFLdEIsdUNBQ2dGOztJQUVwRSxzQ0FBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgQWZ0ZXJWaWV3SW5pdCwgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRNb2RlbCB9IGZyb20gJy4vY29yZS9pbmRleCc7XHJcblxyXG5leHBvcnQgY29uc3QgYmFzZUhvc3QgPSB7XHJcbiAgICAnKGNsaWNrKSc6ICdldmVudCgkZXZlbnQpJyxcclxuICAgICcoYmx1ciknOiAnZXZlbnQoJGV2ZW50KScsXHJcbiAgICAnKGNoYW5nZSknOiAnZXZlbnQoJGV2ZW50KScsXHJcbiAgICAnKGZvY3VzKSc6ICdldmVudCgkZXZlbnQpJyxcclxuICAgICcoZm9jdXNpbiknOiAnZXZlbnQoJGV2ZW50KScsXHJcbiAgICAnKGZvY3Vzb3V0KSc6ICdldmVudCgkZXZlbnQpJyxcclxuICAgICcoaW5wdXQpJzogJ2V2ZW50KCRldmVudCknLFxyXG4gICAgJyhpbnZhbGlkKSc6ICdldmVudCgkZXZlbnQpJyxcclxuICAgICcoc2VsZWN0KSc6ICdldmVudCgkZXZlbnQpJ1xyXG59O1xyXG5cclxuLyoqXHJcbiAqIEJhc2Ugd2lkZ2V0IGNvbXBvbmVudC5cclxuICovXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdiYXNlLXdpZGdldCcsXHJcbiAgICB0ZW1wbGF0ZTogJycsXHJcbiAgICBob3N0OiBiYXNlSG9zdCxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIFdpZGdldENvbXBvbmVudCBpbXBsZW1lbnRzIEFmdGVyVmlld0luaXQge1xyXG5cclxuICAgIHN0YXRpYyBERUZBVUxUX0hZUEVSTElOS19VUkw6IHN0cmluZyA9ICcjJztcclxuICAgIHN0YXRpYyBERUZBVUxUX0hZUEVSTElOS19TQ0hFTUU6IHN0cmluZyA9ICdodHRwOi8vJztcclxuXHJcbiAgICAvKiogRG9lcyB0aGUgd2lkZ2V0IHNob3cgYSByZWFkLW9ubHkgdmFsdWU/IChpZSwgY2FuJ3QgYmUgZWRpdGVkKSAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHJlYWRPbmx5OiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgLyoqIERhdGEgdG8gYmUgZGlzcGxheWVkIGluIHRoZSBmaWVsZCAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGZpZWxkOiBGb3JtRmllbGRNb2RlbDtcclxuXHJcbiAgICAvKipcclxuICAgICAqIEVtaXR0ZWQgd2hlbiBhIGZpZWxkIHZhbHVlIGNoYW5nZXMuXHJcbiAgICAgKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgZmllbGRDaGFuZ2VkOiBFdmVudEVtaXR0ZXI8Rm9ybUZpZWxkTW9kZWw+ID0gbmV3IEV2ZW50RW1pdHRlcjxGb3JtRmllbGRNb2RlbD4oKTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZm9ybVNlcnZpY2U/OiBGb3JtU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIGhhc0ZpZWxkKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZpZWxkID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIE5vdGUgZm9yIGRldmVsb3BlcnM6XHJcbiAgICAvLyByZXR1cm5zIDxhbnk+IG9iamVjdCB0byBiZSBhYmxlIGJpbmRpbmcgaXQgdG8gdGhlIDxlbGVtZW50IHJlcXVpcmVkPVwicmVxdWlyZWRcIj4gYXR0cmlidXRlXHJcbiAgICBpc1JlcXVpcmVkKCk6IGFueSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZmllbGQgJiYgdGhpcy5maWVsZC5yZXF1aXJlZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgaXNWYWxpZCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5maWVsZC52YWxpZGF0aW9uU3VtbWFyeSA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBoYXNWYWx1ZSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5maWVsZCAmJlxyXG4gICAgICAgICAgICB0aGlzLmZpZWxkLnZhbHVlICE9PSBudWxsICYmXHJcbiAgICAgICAgICAgIHRoaXMuZmllbGQudmFsdWUgIT09IHVuZGVmaW5lZDtcclxuICAgIH1cclxuXHJcbiAgICBpc0ludmFsaWRGaWVsZFJlcXVpcmVkKCkge1xyXG4gICAgICAgIHJldHVybiAhdGhpcy5maWVsZC5pc1ZhbGlkICYmICF0aGlzLmZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5ICYmIHRoaXMuaXNSZXF1aXJlZCgpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcclxuICAgICAgICB0aGlzLmZpZWxkQ2hhbmdlZC5lbWl0KHRoaXMuZmllbGQpO1xyXG4gICAgfVxyXG5cclxuICAgIGNoZWNrVmlzaWJpbGl0eShmaWVsZDogRm9ybUZpZWxkTW9kZWwpIHtcclxuICAgICAgICB0aGlzLmZpZWxkQ2hhbmdlZC5lbWl0KGZpZWxkKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkZpZWxkQ2hhbmdlZChmaWVsZDogRm9ybUZpZWxkTW9kZWwpIHtcclxuICAgICAgICB0aGlzLmZpZWxkQ2hhbmdlZC5lbWl0KGZpZWxkKTtcclxuICAgIH1cclxuXHJcbiAgICBwcm90ZWN0ZWQgZ2V0SHlwZXJsaW5rVXJsKGZpZWxkOiBGb3JtRmllbGRNb2RlbCkge1xyXG4gICAgICAgIGxldCB1cmwgPSBXaWRnZXRDb21wb25lbnQuREVGQVVMVF9IWVBFUkxJTktfVVJMO1xyXG4gICAgICAgIGlmIChmaWVsZCAmJiBmaWVsZC5oeXBlcmxpbmtVcmwpIHtcclxuICAgICAgICAgICAgdXJsID0gZmllbGQuaHlwZXJsaW5rVXJsO1xyXG4gICAgICAgICAgICBpZiAoIS9eaHR0cHM/OlxcL1xcLy9pLnRlc3QodXJsKSkge1xyXG4gICAgICAgICAgICAgICAgdXJsID0gYCR7V2lkZ2V0Q29tcG9uZW50LkRFRkFVTFRfSFlQRVJMSU5LX1NDSEVNRX0ke3VybH1gO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB1cmw7XHJcbiAgICB9XHJcblxyXG4gICAgcHJvdGVjdGVkIGdldEh5cGVybGlua1RleHQoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKSB7XHJcbiAgICAgICAgaWYgKGZpZWxkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmaWVsZC5kaXNwbGF5VGV4dCB8fCBmaWVsZC5oeXBlcmxpbmtVcmw7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIGV2ZW50KGV2ZW50OiBFdmVudCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZm9ybVNlcnZpY2UuZm9ybUV2ZW50cy5uZXh0KGV2ZW50KTtcclxuICAgIH1cclxufVxyXG4iXX0=