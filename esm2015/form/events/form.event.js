/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class FormEvent {
    /**
     * @param {?} form
     */
    constructor(form) {
        this.isDefaultPrevented = false;
        this.form = form;
    }
    /**
     * @return {?}
     */
    get defaultPrevented() {
        return this.isDefaultPrevented;
    }
    /**
     * @return {?}
     */
    preventDefault() {
        this.isDefaultPrevented = true;
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    FormEvent.prototype.isDefaultPrevented;
    /** @type {?} */
    FormEvent.prototype.form;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5ldmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vZXZlbnRzL2Zvcm0uZXZlbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsTUFBTSxPQUFPLFNBQVM7Ozs7SUFNbEIsWUFBWSxJQUFlO1FBSm5CLHVCQUFrQixHQUFZLEtBQUssQ0FBQztRQUt4QyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztJQUNyQixDQUFDOzs7O0lBRUQsSUFBSSxnQkFBZ0I7UUFDaEIsT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUM7SUFDbkMsQ0FBQzs7OztJQUVELGNBQWM7UUFDVixJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO0lBQ25DLENBQUM7Q0FDSjs7Ozs7O0lBZkcsdUNBQTRDOztJQUU1Qyx5QkFBeUIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgRm9ybU1vZGVsIH0gZnJvbSAnLi8uLi9jb21wb25lbnRzL3dpZGdldHMvY29yZS9pbmRleCc7XHJcblxyXG5leHBvcnQgY2xhc3MgRm9ybUV2ZW50IHtcclxuXHJcbiAgICBwcml2YXRlIGlzRGVmYXVsdFByZXZlbnRlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIHJlYWRvbmx5IGZvcm06IEZvcm1Nb2RlbDtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihmb3JtOiBGb3JtTW9kZWwpIHtcclxuICAgICAgICB0aGlzLmZvcm0gPSBmb3JtO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBkZWZhdWx0UHJldmVudGVkKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmlzRGVmYXVsdFByZXZlbnRlZDtcclxuICAgIH1cclxuXHJcbiAgICBwcmV2ZW50RGVmYXVsdCgpIHtcclxuICAgICAgICB0aGlzLmlzRGVmYXVsdFByZXZlbnRlZCA9IHRydWU7XHJcbiAgICB9XHJcbn1cclxuIl19