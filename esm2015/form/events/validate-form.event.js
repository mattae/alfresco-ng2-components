/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FormEvent } from './form.event';
export class ValidateFormEvent extends FormEvent {
    /**
     * @param {?} form
     */
    constructor(form) {
        super(form);
        this.isValid = true;
        this.errorsField = [];
    }
}
if (false) {
    /** @type {?} */
    ValidateFormEvent.prototype.isValid;
    /** @type {?} */
    ValidateFormEvent.prototype.errorsField;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmFsaWRhdGUtZm9ybS5ldmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vZXZlbnRzL3ZhbGlkYXRlLWZvcm0uZXZlbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUd6QyxNQUFNLE9BQU8saUJBQWtCLFNBQVEsU0FBUzs7OztJQUs1QyxZQUFZLElBQWU7UUFDdkIsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBSmhCLFlBQU8sR0FBRyxJQUFJLENBQUM7UUFDZixnQkFBVyxHQUFxQixFQUFFLENBQUM7SUFJbkMsQ0FBQztDQUNKOzs7SUFORyxvQ0FBZTs7SUFDZix3Q0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgRm9ybU1vZGVsIH0gZnJvbSAnLi8uLi9jb21wb25lbnRzL3dpZGdldHMvY29yZS9pbmRleCc7XHJcbmltcG9ydCB7IEZvcm1FdmVudCB9IGZyb20gJy4vZm9ybS5ldmVudCc7XHJcbmltcG9ydCB7IEZvcm1GaWVsZE1vZGVsIH0gZnJvbSAnLi4vY29tcG9uZW50cy93aWRnZXRzL2NvcmUvZm9ybS1maWVsZC5tb2RlbCc7XHJcblxyXG5leHBvcnQgY2xhc3MgVmFsaWRhdGVGb3JtRXZlbnQgZXh0ZW5kcyBGb3JtRXZlbnQge1xyXG5cclxuICAgIGlzVmFsaWQgPSB0cnVlO1xyXG4gICAgZXJyb3JzRmllbGQ6IEZvcm1GaWVsZE1vZGVsW10gPSBbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihmb3JtOiBGb3JtTW9kZWwpIHtcclxuICAgICAgICBzdXBlcihmb3JtKTtcclxuICAgIH1cclxufVxyXG4iXX0=