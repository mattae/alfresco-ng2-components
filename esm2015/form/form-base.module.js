/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { DataTableModule } from '../datatable/datatable.module';
import { DataColumnModule } from '../data-column/data-column.module';
import { PipeModule } from '../pipes/pipe.module';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../material.module';
import { MASK_DIRECTIVE, WIDGET_DIRECTIVES } from './components/widgets/index';
import { StartFormCustomButtonDirective } from './components/form-custom-button.directive';
import { FormFieldComponent } from './components/form-field/form-field.component';
import { FormListComponent } from './components/form-list.component';
import { ContentWidgetComponent } from './components/widgets/content/content.widget';
import { WidgetComponent } from './components/widgets/widget.component';
import { MatDatetimepickerModule, MatNativeDatetimeModule } from '@mat-datetimepicker/core';
import { FormRendererComponent } from './components/form-renderer.component';
export class FormBaseModule {
}
FormBaseModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    DataTableModule,
                    HttpClientModule,
                    MaterialModule,
                    TranslateModule.forChild(),
                    FormsModule,
                    ReactiveFormsModule,
                    DataColumnModule,
                    PipeModule,
                    MatDatetimepickerModule,
                    MatNativeDatetimeModule
                ],
                declarations: [
                    ContentWidgetComponent,
                    FormFieldComponent,
                    FormListComponent,
                    FormRendererComponent,
                    StartFormCustomButtonDirective,
                    ...WIDGET_DIRECTIVES,
                    ...MASK_DIRECTIVE,
                    WidgetComponent
                ],
                entryComponents: [
                    ...WIDGET_DIRECTIVES
                ],
                exports: [
                    ContentWidgetComponent,
                    FormFieldComponent,
                    FormListComponent,
                    FormRendererComponent,
                    StartFormCustomButtonDirective,
                    ...WIDGET_DIRECTIVES
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1iYXNlLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vZm9ybS1iYXNlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFdBQVcsRUFBRSxtQkFBbUIsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDekMsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUNoRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUNyRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFeEQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXBELE9BQU8sRUFBRSxjQUFjLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUUvRSxPQUFPLEVBQUUsOEJBQThCLEVBQUUsTUFBTSwyQ0FBMkMsQ0FBQztBQUUzRixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSw4Q0FBOEMsQ0FBQztBQUNsRixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUNyRSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUNyRixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDeEUsT0FBTyxFQUFFLHVCQUF1QixFQUFFLHVCQUF1QixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDNUYsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFzQzdFLE1BQU0sT0FBTyxjQUFjOzs7WUFwQzFCLFFBQVEsU0FBQztnQkFDTixPQUFPLEVBQUU7b0JBQ0wsWUFBWTtvQkFDWixlQUFlO29CQUNmLGdCQUFnQjtvQkFDaEIsY0FBYztvQkFDZCxlQUFlLENBQUMsUUFBUSxFQUFFO29CQUMxQixXQUFXO29CQUNYLG1CQUFtQjtvQkFDbkIsZ0JBQWdCO29CQUNoQixVQUFVO29CQUNWLHVCQUF1QjtvQkFDdkIsdUJBQXVCO2lCQUMxQjtnQkFDRCxZQUFZLEVBQUU7b0JBQ1Ysc0JBQXNCO29CQUN0QixrQkFBa0I7b0JBQ2xCLGlCQUFpQjtvQkFDakIscUJBQXFCO29CQUNyQiw4QkFBOEI7b0JBQzlCLEdBQUcsaUJBQWlCO29CQUNwQixHQUFHLGNBQWM7b0JBQ2pCLGVBQWU7aUJBQ2xCO2dCQUNELGVBQWUsRUFBRTtvQkFDYixHQUFHLGlCQUFpQjtpQkFDdkI7Z0JBQ0QsT0FBTyxFQUFFO29CQUNMLHNCQUFzQjtvQkFDdEIsa0JBQWtCO29CQUNsQixpQkFBaUI7b0JBQ2pCLHFCQUFxQjtvQkFDckIsOEJBQThCO29CQUM5QixHQUFHLGlCQUFpQjtpQkFDdkI7YUFDSiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XHJcbmltcG9ydCB7IERhdGFUYWJsZU1vZHVsZSB9IGZyb20gJy4uL2RhdGF0YWJsZS9kYXRhdGFibGUubW9kdWxlJztcclxuaW1wb3J0IHsgRGF0YUNvbHVtbk1vZHVsZSB9IGZyb20gJy4uL2RhdGEtY29sdW1uL2RhdGEtY29sdW1uLm1vZHVsZSc7XHJcbmltcG9ydCB7IFBpcGVNb2R1bGUgfSBmcm9tICcuLi9waXBlcy9waXBlLm1vZHVsZSc7XHJcbmltcG9ydCB7IEh0dHBDbGllbnRNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcblxyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4uL21hdGVyaWFsLm1vZHVsZSc7XHJcblxyXG5pbXBvcnQgeyBNQVNLX0RJUkVDVElWRSwgV0lER0VUX0RJUkVDVElWRVMgfSBmcm9tICcuL2NvbXBvbmVudHMvd2lkZ2V0cy9pbmRleCc7XHJcblxyXG5pbXBvcnQgeyBTdGFydEZvcm1DdXN0b21CdXR0b25EaXJlY3RpdmUgfSBmcm9tICcuL2NvbXBvbmVudHMvZm9ybS1jdXN0b20tYnV0dG9uLmRpcmVjdGl2ZSc7XHJcblxyXG5pbXBvcnQgeyBGb3JtRmllbGRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZm9ybS1maWVsZC9mb3JtLWZpZWxkLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEZvcm1MaXN0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2Zvcm0tbGlzdC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb250ZW50V2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3dpZGdldHMvY29udGVudC9jb250ZW50LndpZGdldCc7XHJcbmltcG9ydCB7IFdpZGdldENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy93aWRnZXRzL3dpZGdldC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBNYXREYXRldGltZXBpY2tlck1vZHVsZSwgTWF0TmF0aXZlRGF0ZXRpbWVNb2R1bGUgfSBmcm9tICdAbWF0LWRhdGV0aW1lcGlja2VyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtUmVuZGVyZXJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZm9ybS1yZW5kZXJlci5jb21wb25lbnQnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgRGF0YVRhYmxlTW9kdWxlLFxyXG4gICAgICAgIEh0dHBDbGllbnRNb2R1bGUsXHJcbiAgICAgICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICAgICAgVHJhbnNsYXRlTW9kdWxlLmZvckNoaWxkKCksXHJcbiAgICAgICAgRm9ybXNNb2R1bGUsXHJcbiAgICAgICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcclxuICAgICAgICBEYXRhQ29sdW1uTW9kdWxlLFxyXG4gICAgICAgIFBpcGVNb2R1bGUsXHJcbiAgICAgICAgTWF0RGF0ZXRpbWVwaWNrZXJNb2R1bGUsXHJcbiAgICAgICAgTWF0TmF0aXZlRGF0ZXRpbWVNb2R1bGVcclxuICAgIF0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtcclxuICAgICAgICBDb250ZW50V2lkZ2V0Q29tcG9uZW50LFxyXG4gICAgICAgIEZvcm1GaWVsZENvbXBvbmVudCxcclxuICAgICAgICBGb3JtTGlzdENvbXBvbmVudCxcclxuICAgICAgICBGb3JtUmVuZGVyZXJDb21wb25lbnQsXHJcbiAgICAgICAgU3RhcnRGb3JtQ3VzdG9tQnV0dG9uRGlyZWN0aXZlLFxyXG4gICAgICAgIC4uLldJREdFVF9ESVJFQ1RJVkVTLFxyXG4gICAgICAgIC4uLk1BU0tfRElSRUNUSVZFLFxyXG4gICAgICAgIFdpZGdldENvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIGVudHJ5Q29tcG9uZW50czogW1xyXG4gICAgICAgIC4uLldJREdFVF9ESVJFQ1RJVkVTXHJcbiAgICBdLFxyXG4gICAgZXhwb3J0czogW1xyXG4gICAgICAgIENvbnRlbnRXaWRnZXRDb21wb25lbnQsXHJcbiAgICAgICAgRm9ybUZpZWxkQ29tcG9uZW50LFxyXG4gICAgICAgIEZvcm1MaXN0Q29tcG9uZW50LFxyXG4gICAgICAgIEZvcm1SZW5kZXJlckNvbXBvbmVudCxcclxuICAgICAgICBTdGFydEZvcm1DdXN0b21CdXR0b25EaXJlY3RpdmUsXHJcbiAgICAgICAgLi4uV0lER0VUX0RJUkVDVElWRVNcclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIEZvcm1CYXNlTW9kdWxlIHtcclxufVxyXG4iXX0=