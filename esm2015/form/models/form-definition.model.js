/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FormSaveRepresentation } from '@alfresco/js-api';
export class FormDefinitionModel extends FormSaveRepresentation {
    /**
     * @param {?} id
     * @param {?} name
     * @param {?} lastUpdatedByFullName
     * @param {?} lastUpdated
     * @param {?} metadata
     */
    constructor(id, name, lastUpdatedByFullName, lastUpdated, metadata) {
        super();
        this.reusable = false;
        this.newVersion = false;
        this.formImageBase64 = '';
        this.formRepresentation = {
            id: id,
            name: name,
            description: '',
            version: 1,
            lastUpdatedBy: 1,
            lastUpdatedByFullName: lastUpdatedByFullName,
            lastUpdated: lastUpdated,
            stencilSetId: 0,
            referenceId: null,
            formDefinition: {
                fields: [{
                        name: 'Label',
                        type: 'container',
                        fieldType: 'ContainerRepresentation',
                        numberOfColumns: 2,
                        required: false,
                        readOnly: false,
                        sizeX: 2,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        fields: { '1': this.metadataToFields(metadata) }
                    }],
                gridsterForm: false,
                javascriptEvents: [],
                metadata: {},
                outcomes: [],
                className: '',
                style: '',
                tabs: [],
                variables: []
            }
        };
    }
    /**
     * @private
     * @param {?} metadata
     * @return {?}
     */
    metadataToFields(metadata) {
        /** @type {?} */
        const fields = [];
        if (metadata) {
            metadata.forEach((/**
             * @param {?} property
             * @return {?}
             */
            function (property) {
                if (property) {
                    /** @type {?} */
                    const field = {
                        type: 'text',
                        id: property.name,
                        name: property.name,
                        required: false,
                        readOnly: false,
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        colspan: 1,
                        params: {
                            existingColspan: 1,
                            maxColspan: 2
                        },
                        layout: {
                            colspan: 1,
                            row: -1,
                            column: -1
                        }
                    };
                    fields.push(field);
                }
            }));
        }
        return fields;
    }
}
if (false) {
    /** @type {?} */
    FormDefinitionModel.prototype.reusable;
    /** @type {?} */
    FormDefinitionModel.prototype.newVersion;
    /** @type {?} */
    FormDefinitionModel.prototype.formRepresentation;
    /** @type {?} */
    FormDefinitionModel.prototype.formImageBase64;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1kZWZpbml0aW9uLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9tb2RlbHMvZm9ybS1kZWZpbml0aW9uLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRTFELE1BQU0sT0FBTyxtQkFBb0IsU0FBUSxzQkFBc0I7Ozs7Ozs7O0lBTTNELFlBQVksRUFBVSxFQUFFLElBQVMsRUFBRSxxQkFBNkIsRUFBRSxXQUFtQixFQUFFLFFBQWE7UUFDaEcsS0FBSyxFQUFFLENBQUM7UUFOWixhQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLGVBQVUsR0FBWSxLQUFLLENBQUM7UUFFNUIsb0JBQWUsR0FBVyxFQUFFLENBQUM7UUFJekIsSUFBSSxDQUFDLGtCQUFrQixHQUFHO1lBQ3RCLEVBQUUsRUFBRSxFQUFFO1lBQ04sSUFBSSxFQUFFLElBQUk7WUFDVixXQUFXLEVBQUUsRUFBRTtZQUNmLE9BQU8sRUFBRSxDQUFDO1lBQ1YsYUFBYSxFQUFFLENBQUM7WUFDaEIscUJBQXFCLEVBQUUscUJBQXFCO1lBQzVDLFdBQVcsRUFBRSxXQUFXO1lBQ3hCLFlBQVksRUFBRSxDQUFDO1lBQ2YsV0FBVyxFQUFFLElBQUk7WUFDakIsY0FBYyxFQUFFO2dCQUNaLE1BQU0sRUFBRSxDQUFDO3dCQUNMLElBQUksRUFBRSxPQUFPO3dCQUNiLElBQUksRUFBRSxXQUFXO3dCQUNqQixTQUFTLEVBQUUseUJBQXlCO3dCQUNwQyxlQUFlLEVBQUUsQ0FBQzt3QkFDbEIsUUFBUSxFQUFFLEtBQUs7d0JBQ2YsUUFBUSxFQUFFLEtBQUs7d0JBQ2YsS0FBSyxFQUFFLENBQUM7d0JBQ1IsS0FBSyxFQUFFLENBQUM7d0JBQ1IsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLE1BQU0sRUFBRSxFQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLEVBQUM7cUJBQ2pELENBQUM7Z0JBQ0YsWUFBWSxFQUFFLEtBQUs7Z0JBQ25CLGdCQUFnQixFQUFFLEVBQUU7Z0JBQ3BCLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFFBQVEsRUFBRSxFQUFFO2dCQUNaLFNBQVMsRUFBRSxFQUFFO2dCQUNiLEtBQUssRUFBRSxFQUFFO2dCQUNULElBQUksRUFBRSxFQUFFO2dCQUNSLFNBQVMsRUFBRSxFQUFFO2FBQ2hCO1NBQ0osQ0FBQztJQUNOLENBQUM7Ozs7OztJQUVPLGdCQUFnQixDQUFDLFFBQWE7O2NBQzVCLE1BQU0sR0FBRyxFQUFFO1FBQ2pCLElBQUksUUFBUSxFQUFFO1lBQ1YsUUFBUSxDQUFDLE9BQU87Ozs7WUFBQyxVQUFTLFFBQVE7Z0JBQzlCLElBQUksUUFBUSxFQUFFOzswQkFDSixLQUFLLEdBQUc7d0JBQ1YsSUFBSSxFQUFFLE1BQU07d0JBQ1osRUFBRSxFQUFFLFFBQVEsQ0FBQyxJQUFJO3dCQUNqQixJQUFJLEVBQUUsUUFBUSxDQUFDLElBQUk7d0JBQ25CLFFBQVEsRUFBRSxLQUFLO3dCQUNmLFFBQVEsRUFBRSxLQUFLO3dCQUNmLEtBQUssRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxDQUFDO3dCQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxPQUFPLEVBQUUsQ0FBQzt3QkFDVixNQUFNLEVBQUU7NEJBQ0osZUFBZSxFQUFFLENBQUM7NEJBQ2xCLFVBQVUsRUFBRSxDQUFDO3lCQUNoQjt3QkFDRCxNQUFNLEVBQUU7NEJBQ0osT0FBTyxFQUFFLENBQUM7NEJBQ1YsR0FBRyxFQUFFLENBQUMsQ0FBQzs0QkFDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDO3lCQUNiO3FCQUNKO29CQUNELE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ3RCO1lBQ0wsQ0FBQyxFQUFDLENBQUM7U0FDTjtRQUVELE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7Q0FDSjs7O0lBNUVHLHVDQUEwQjs7SUFDMUIseUNBQTRCOztJQUM1QixpREFBd0I7O0lBQ3hCLDhDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBGb3JtU2F2ZVJlcHJlc2VudGF0aW9uIH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcblxyXG5leHBvcnQgY2xhc3MgRm9ybURlZmluaXRpb25Nb2RlbCBleHRlbmRzIEZvcm1TYXZlUmVwcmVzZW50YXRpb24ge1xyXG4gICAgcmV1c2FibGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIG5ld1ZlcnNpb246IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGZvcm1SZXByZXNlbnRhdGlvbjogYW55O1xyXG4gICAgZm9ybUltYWdlQmFzZTY0OiBzdHJpbmcgPSAnJztcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihpZDogc3RyaW5nLCBuYW1lOiBhbnksIGxhc3RVcGRhdGVkQnlGdWxsTmFtZTogc3RyaW5nLCBsYXN0VXBkYXRlZDogc3RyaW5nLCBtZXRhZGF0YTogYW55KSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICB0aGlzLmZvcm1SZXByZXNlbnRhdGlvbiA9IHtcclxuICAgICAgICAgICAgaWQ6IGlkLFxyXG4gICAgICAgICAgICBuYW1lOiBuYW1lLFxyXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjogJycsXHJcbiAgICAgICAgICAgIHZlcnNpb246IDEsXHJcbiAgICAgICAgICAgIGxhc3RVcGRhdGVkQnk6IDEsXHJcbiAgICAgICAgICAgIGxhc3RVcGRhdGVkQnlGdWxsTmFtZTogbGFzdFVwZGF0ZWRCeUZ1bGxOYW1lLFxyXG4gICAgICAgICAgICBsYXN0VXBkYXRlZDogbGFzdFVwZGF0ZWQsXHJcbiAgICAgICAgICAgIHN0ZW5jaWxTZXRJZDogMCxcclxuICAgICAgICAgICAgcmVmZXJlbmNlSWQ6IG51bGwsXHJcbiAgICAgICAgICAgIGZvcm1EZWZpbml0aW9uOiB7XHJcbiAgICAgICAgICAgICAgICBmaWVsZHM6IFt7XHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogJ0xhYmVsJyxcclxuICAgICAgICAgICAgICAgICAgICB0eXBlOiAnY29udGFpbmVyJyxcclxuICAgICAgICAgICAgICAgICAgICBmaWVsZFR5cGU6ICdDb250YWluZXJSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgICAgICAgICAgbnVtYmVyT2ZDb2x1bW5zOiAyLFxyXG4gICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgc2l6ZVg6IDIsXHJcbiAgICAgICAgICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgICAgICBjb2w6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgIGZpZWxkczogeycxJzogdGhpcy5tZXRhZGF0YVRvRmllbGRzKG1ldGFkYXRhKX1cclxuICAgICAgICAgICAgICAgIH1dLFxyXG4gICAgICAgICAgICAgICAgZ3JpZHN0ZXJGb3JtOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGphdmFzY3JpcHRFdmVudHM6IFtdLFxyXG4gICAgICAgICAgICAgICAgbWV0YWRhdGE6IHt9LFxyXG4gICAgICAgICAgICAgICAgb3V0Y29tZXM6IFtdLFxyXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnJyxcclxuICAgICAgICAgICAgICAgIHN0eWxlOiAnJyxcclxuICAgICAgICAgICAgICAgIHRhYnM6IFtdLFxyXG4gICAgICAgICAgICAgICAgdmFyaWFibGVzOiBbXVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG1ldGFkYXRhVG9GaWVsZHMobWV0YWRhdGE6IGFueSk6IGFueVtdIHtcclxuICAgICAgICBjb25zdCBmaWVsZHMgPSBbXTtcclxuICAgICAgICBpZiAobWV0YWRhdGEpIHtcclxuICAgICAgICAgICAgbWV0YWRhdGEuZm9yRWFjaChmdW5jdGlvbihwcm9wZXJ0eSkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHByb3BlcnR5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZmllbGQgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICd0ZXh0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IHByb3BlcnR5Lm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IHByb3BlcnR5Lm5hbWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmFtczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhpc3RpbmdDb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF4Q29sc3BhbjogMlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYXlvdXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sdW1uOiAtMVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgICAgICBmaWVsZHMucHVzaChmaWVsZCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZpZWxkcztcclxuICAgIH1cclxufVxyXG4iXX0=