/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class WidgetVisibilityModel {
    /**
     * @param {?=} json
     */
    constructor(json) {
        this.json = json;
        if (json) {
            this.operator = json.operator;
            this.nextCondition = new WidgetVisibilityModel(json.nextCondition);
            this.nextConditionOperator = json.nextConditionOperator;
            this.rightRestResponseId = json.rightRestResponseId;
            this.rightFormFieldId = json.rightFormFieldId;
            this.leftFormFieldId = json.leftFormFieldId;
            this.leftRestResponseId = json.leftRestResponseId;
        }
        else {
            this.json = {};
        }
    }
    /**
     * @param {?} leftType
     * @return {?}
     */
    set leftType(leftType) {
        this.json.leftType = leftType;
    }
    /**
     * @param {?} rightType
     * @return {?}
     */
    set rightType(rightType) {
        this.json.rightType = rightType;
    }
    /**
     * @param {?} leftValue
     * @return {?}
     */
    set leftValue(leftValue) {
        this.json.leftValue = leftValue;
    }
    /**
     * @param {?} rightValue
     * @return {?}
     */
    set rightValue(rightValue) {
        this.json.rightValue = rightValue;
    }
    /**
     * @return {?}
     */
    get leftType() {
        if (this.leftFormFieldId) {
            return WidgetTypeEnum.field;
        }
        else if (this.leftRestResponseId) {
            return WidgetTypeEnum.variable;
        }
        else if (!!this.json.leftType) {
            return this.json.leftType;
        }
    }
    /**
     * @return {?}
     */
    get leftValue() {
        if (this.json.leftValue) {
            return this.json.leftValue;
        }
        else if (this.leftFormFieldId) {
            return this.leftFormFieldId;
        }
        else {
            return this.leftRestResponseId;
        }
    }
    /**
     * @return {?}
     */
    get rightType() {
        if (!!this.json.rightType) {
            return this.json.rightType;
        }
        else if (this.json.rightValue) {
            return WidgetTypeEnum.value;
        }
        else if (this.rightRestResponseId) {
            return WidgetTypeEnum.variable;
        }
        else if (this.rightFormFieldId) {
            return WidgetTypeEnum.field;
        }
    }
    /**
     * @return {?}
     */
    get rightValue() {
        if (this.json.rightValue) {
            return this.json.rightValue;
        }
        else if (this.rightFormFieldId) {
            return this.rightFormFieldId;
        }
        else {
            return this.rightRestResponseId;
        }
    }
}
if (false) {
    /** @type {?} */
    WidgetVisibilityModel.prototype.rightRestResponseId;
    /** @type {?} */
    WidgetVisibilityModel.prototype.rightFormFieldId;
    /** @type {?} */
    WidgetVisibilityModel.prototype.leftRestResponseId;
    /** @type {?} */
    WidgetVisibilityModel.prototype.leftFormFieldId;
    /** @type {?} */
    WidgetVisibilityModel.prototype.operator;
    /** @type {?} */
    WidgetVisibilityModel.prototype.nextCondition;
    /** @type {?} */
    WidgetVisibilityModel.prototype.nextConditionOperator;
    /**
     * @type {?}
     * @private
     */
    WidgetVisibilityModel.prototype.json;
}
/** @enum {string} */
const WidgetTypeEnum = {
    field: 'field',
    variable: 'variable',
    value: 'value',
};
export { WidgetTypeEnum };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0LXZpc2liaWxpdHkubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL21vZGVscy93aWRnZXQtdmlzaWJpbGl0eS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxNQUFNLE9BQU8scUJBQXFCOzs7O0lBUzFCLFlBQW9CLElBQVU7UUFBVixTQUFJLEdBQUosSUFBSSxDQUFNO1FBQ3RCLElBQUksSUFBSSxFQUFFO1lBQ0YsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQzlCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7WUFDbkUsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztZQUN4RCxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDO1lBQ3BELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7WUFDOUMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDO1lBQzVDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUM7U0FDekQ7YUFBTTtZQUNDLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDO1NBQ3RCO0lBQ1QsQ0FBQzs7Ozs7SUFFRCxJQUFJLFFBQVEsQ0FBQyxRQUFnQjtRQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7SUFDdEMsQ0FBQzs7Ozs7SUFFRCxJQUFJLFNBQVMsQ0FBQyxTQUFpQjtRQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7SUFDeEMsQ0FBQzs7Ozs7SUFFRCxJQUFJLFNBQVMsQ0FBQyxTQUFpQjtRQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7SUFDeEMsQ0FBQzs7Ozs7SUFFRCxJQUFJLFVBQVUsQ0FBQyxVQUFrQjtRQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7SUFDMUMsQ0FBQzs7OztJQUVELElBQUksUUFBUTtRQUNKLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUNsQixPQUFPLGNBQWMsQ0FBQyxLQUFLLENBQUM7U0FDbkM7YUFBTSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtZQUM1QixPQUFPLGNBQWMsQ0FBQyxRQUFRLENBQUM7U0FDdEM7YUFBTSxJQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUMxQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1NBQ2pDO0lBQ1QsQ0FBQzs7OztJQUVELElBQUksU0FBUztRQUNMLElBQUssSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUc7WUFDbkIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztTQUNsQzthQUFNLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN6QixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUM7U0FDbkM7YUFBTTtZQUNDLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDO1NBQ3RDO0lBQ1QsQ0FBQzs7OztJQUVELElBQUksU0FBUztRQUNMLElBQUssQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFHO1lBQ3JCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7U0FDbEM7YUFBTSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ3pCLE9BQU8sY0FBYyxDQUFDLEtBQUssQ0FBQztTQUNuQzthQUFNLElBQUksSUFBSSxDQUFDLG1CQUFtQixFQUFFO1lBQzdCLE9BQU8sY0FBYyxDQUFDLFFBQVEsQ0FBQztTQUN0QzthQUFNLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBQzFCLE9BQU8sY0FBYyxDQUFDLEtBQUssQ0FBQztTQUNuQztJQUNULENBQUM7Ozs7SUFFRCxJQUFJLFVBQVU7UUFDTixJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2xCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7U0FDbkM7YUFBTSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUMxQixPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztTQUNwQzthQUFNO1lBQ0MsT0FBTyxJQUFJLENBQUMsbUJBQW1CLENBQUM7U0FDdkM7SUFDVCxDQUFDO0NBRVI7OztJQWhGTyxvREFBNkI7O0lBQzdCLGlEQUEwQjs7SUFDMUIsbURBQTRCOztJQUM1QixnREFBeUI7O0lBQ3pCLHlDQUFpQjs7SUFDakIsOENBQXFDOztJQUNyQyxzREFBOEI7Ozs7O0lBRWxCLHFDQUFrQjs7OztJQTJFOUIsT0FBUSxPQUFPO0lBQ2YsVUFBVyxVQUFVO0lBQ3JCLE9BQVEsT0FBTyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5leHBvcnQgY2xhc3MgV2lkZ2V0VmlzaWJpbGl0eU1vZGVsIHtcclxuICAgICAgICByaWdodFJlc3RSZXNwb25zZUlkPzogc3RyaW5nO1xyXG4gICAgICAgIHJpZ2h0Rm9ybUZpZWxkSWQ/OiBzdHJpbmc7XHJcbiAgICAgICAgbGVmdFJlc3RSZXNwb25zZUlkPzogc3RyaW5nO1xyXG4gICAgICAgIGxlZnRGb3JtRmllbGRJZD86IHN0cmluZztcclxuICAgICAgICBvcGVyYXRvcjogc3RyaW5nO1xyXG4gICAgICAgIG5leHRDb25kaXRpb246IFdpZGdldFZpc2liaWxpdHlNb2RlbDtcclxuICAgICAgICBuZXh0Q29uZGl0aW9uT3BlcmF0b3I6IHN0cmluZztcclxuXHJcbiAgICAgICAgY29uc3RydWN0b3IocHJpdmF0ZSBqc29uPzogYW55KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoanNvbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm9wZXJhdG9yID0ganNvbi5vcGVyYXRvcjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5uZXh0Q29uZGl0aW9uID0gbmV3IFdpZGdldFZpc2liaWxpdHlNb2RlbChqc29uLm5leHRDb25kaXRpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5leHRDb25kaXRpb25PcGVyYXRvciA9IGpzb24ubmV4dENvbmRpdGlvbk9wZXJhdG9yO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJpZ2h0UmVzdFJlc3BvbnNlSWQgPSBqc29uLnJpZ2h0UmVzdFJlc3BvbnNlSWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmlnaHRGb3JtRmllbGRJZCA9IGpzb24ucmlnaHRGb3JtRmllbGRJZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sZWZ0Rm9ybUZpZWxkSWQgPSBqc29uLmxlZnRGb3JtRmllbGRJZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sZWZ0UmVzdFJlc3BvbnNlSWQgPSBqc29uLmxlZnRSZXN0UmVzcG9uc2VJZDtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuanNvbiA9IHt9O1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgc2V0IGxlZnRUeXBlKGxlZnRUeXBlOiBzdHJpbmcpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuanNvbi5sZWZ0VHlwZSA9IGxlZnRUeXBlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgc2V0IHJpZ2h0VHlwZShyaWdodFR5cGU6IHN0cmluZykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5qc29uLnJpZ2h0VHlwZSA9IHJpZ2h0VHlwZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHNldCBsZWZ0VmFsdWUobGVmdFZhbHVlOiBzdHJpbmcpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuanNvbi5sZWZ0VmFsdWUgPSBsZWZ0VmFsdWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBzZXQgcmlnaHRWYWx1ZShyaWdodFZhbHVlOiBzdHJpbmcpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuanNvbi5yaWdodFZhbHVlID0gcmlnaHRWYWx1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGdldCBsZWZ0VHlwZSgpIHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmxlZnRGb3JtRmllbGRJZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gV2lkZ2V0VHlwZUVudW0uZmllbGQ7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMubGVmdFJlc3RSZXNwb25zZUlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBXaWRnZXRUeXBlRW51bS52YXJpYWJsZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoICEhdGhpcy5qc29uLmxlZnRUeXBlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmpzb24ubGVmdFR5cGU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBnZXQgbGVmdFZhbHVlKCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCB0aGlzLmpzb24ubGVmdFZhbHVlICkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5qc29uLmxlZnRWYWx1ZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5sZWZ0Rm9ybUZpZWxkSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubGVmdEZvcm1GaWVsZElkO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubGVmdFJlc3RSZXNwb25zZUlkO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZ2V0IHJpZ2h0VHlwZSgpIHtcclxuICAgICAgICAgICAgICAgIGlmICggISF0aGlzLmpzb24ucmlnaHRUeXBlICkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5qc29uLnJpZ2h0VHlwZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5qc29uLnJpZ2h0VmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFdpZGdldFR5cGVFbnVtLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnJpZ2h0UmVzdFJlc3BvbnNlSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFdpZGdldFR5cGVFbnVtLnZhcmlhYmxlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnJpZ2h0Rm9ybUZpZWxkSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFdpZGdldFR5cGVFbnVtLmZpZWxkO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZ2V0IHJpZ2h0VmFsdWUoKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5qc29uLnJpZ2h0VmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuanNvbi5yaWdodFZhbHVlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnJpZ2h0Rm9ybUZpZWxkSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMucmlnaHRGb3JtRmllbGRJZDtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnJpZ2h0UmVzdFJlc3BvbnNlSWQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGVudW0gV2lkZ2V0VHlwZUVudW0ge1xyXG4gICAgICAgIGZpZWxkID0gJ2ZpZWxkJyxcclxuICAgICAgICB2YXJpYWJsZSA9ICd2YXJpYWJsZScsXHJcbiAgICAgICAgdmFsdWUgPSAndmFsdWUnXHJcbn1cclxuIl19