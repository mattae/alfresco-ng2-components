/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "../../services/alfresco-api.service";
import * as i2 from "../../services/log.service";
export class ActivitiContentService {
    /**
     * @param {?} apiService
     * @param {?} logService
     */
    constructor(apiService, logService) {
        this.apiService = apiService;
        this.logService = logService;
    }
    /**
     * Returns a list of child nodes below the specified folder
     *
     * @param {?} accountId
     * @param {?} folderId
     * @return {?}
     */
    getAlfrescoNodes(accountId, folderId) {
        /** @type {?} */
        const apiService = this.apiService.getInstance();
        /** @type {?} */
        const accountShortId = accountId.replace('alfresco-', '');
        return from(apiService.activiti.alfrescoApi.getContentInFolder(accountShortId, folderId))
            .pipe(map(this.toJsonArray), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Returns a list of all the repositories configured
     *
     * @param {?} tenantId
     * @param {?} includeAccount
     * @return {?}
     */
    getAlfrescoRepositories(tenantId, includeAccount) {
        /** @type {?} */
        const apiService = this.apiService.getInstance();
        /** @type {?} */
        const opts = {
            tenantId: tenantId,
            includeAccounts: includeAccount
        };
        return from(apiService.activiti.alfrescoApi.getRepositories(opts))
            .pipe(map(this.toJsonArray), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Returns a list of child nodes below the specified folder
     *
     * @param {?} accountId
     * @param {?} node
     * @param {?} siteId
     * @return {?}
     */
    linkAlfrescoNode(accountId, node, siteId) {
        /** @type {?} */
        const apiService = this.apiService.getInstance();
        return from(apiService.activiti.contentApi.createTemporaryRelatedContent({
            link: true,
            name: node.title,
            simpleType: node.simpleType,
            source: accountId,
            sourceId: node.id + '@' + siteId
        }))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * @param {?} node
     * @param {?} siteId
     * @param {?} accountId
     * @return {?}
     */
    applyAlfrescoNode(node, siteId, accountId) {
        /** @type {?} */
        const apiService = this.apiService.getInstance();
        /** @type {?} */
        const currentSideId = siteId ? siteId : this.getSiteNameFromNodePath(node);
        /** @type {?} */
        const params = {
            source: accountId,
            mimeType: node.content.mimeType,
            sourceId: node.id + ';' + node.properties['cm:versionLabel'] + '@' + currentSideId,
            name: node.name,
            link: false
        };
        return from(apiService.activiti.contentApi.createTemporaryRelatedContent(params))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    getSiteNameFromNodePath(node) {
        /** @type {?} */
        let siteName = '';
        if (node.path) {
            /** @type {?} */
            const foundNode = node.path
                .elements.find((/**
             * @param {?} pathNode
             * @return {?}
             */
            (pathNode) => pathNode.nodeType === 'st:site' &&
                pathNode.name !== 'Sites'));
            siteName = foundNode ? foundNode.name : '';
        }
        return siteName.toLocaleLowerCase();
    }
    /**
     * @param {?} res
     * @return {?}
     */
    toJson(res) {
        if (res) {
            return res || {};
        }
        return {};
    }
    /**
     * @param {?} res
     * @return {?}
     */
    toJsonArray(res) {
        if (res) {
            return res.data || [];
        }
        return [];
    }
    /**
     * @param {?} error
     * @return {?}
     */
    handleError(error) {
        /** @type {?} */
        let errMsg = ActivitiContentService.UNKNOWN_ERROR_MESSAGE;
        if (error) {
            errMsg = (error.message) ? error.message :
                error.status ? `${error.status} - ${error.statusText}` : ActivitiContentService.GENERIC_ERROR_MESSAGE;
        }
        this.logService.error(errMsg);
        return throwError(errMsg);
    }
}
ActivitiContentService.UNKNOWN_ERROR_MESSAGE = 'Unknown error';
ActivitiContentService.GENERIC_ERROR_MESSAGE = 'Server error';
ActivitiContentService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
ActivitiContentService.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: LogService }
];
/** @nocollapse */ ActivitiContentService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ActivitiContentService_Factory() { return new ActivitiContentService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: ActivitiContentService, providedIn: "root" });
if (false) {
    /** @type {?} */
    ActivitiContentService.UNKNOWN_ERROR_MESSAGE;
    /** @type {?} */
    ActivitiContentService.GENERIC_ERROR_MESSAGE;
    /**
     * @type {?}
     * @private
     */
    ActivitiContentService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    ActivitiContentService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWN0aXZpdGktYWxmcmVzY28uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vc2VydmljZXMvYWN0aXZpdGktYWxmcmVzY28uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUN6RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDeEQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUUzQyxPQUFPLEVBQWMsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUdwRCxPQUFPLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBS2pELE1BQU0sT0FBTyxzQkFBc0I7Ozs7O0lBSy9CLFlBQW9CLFVBQThCLEVBQzlCLFVBQXNCO1FBRHRCLGVBQVUsR0FBVixVQUFVLENBQW9CO1FBQzlCLGVBQVUsR0FBVixVQUFVLENBQVk7SUFDMUMsQ0FBQzs7Ozs7Ozs7SUFRRCxnQkFBZ0IsQ0FBQyxTQUFpQixFQUFFLFFBQWdCOztjQUMxQyxVQUFVLEdBQTZCLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFOztjQUNwRSxjQUFjLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDO1FBQ3pELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLGNBQWMsRUFBRSxRQUFRLENBQUMsQ0FBQzthQUNwRixJQUFJLENBQ0QsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsRUFDckIsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7Ozs7OztJQVFELHVCQUF1QixDQUFDLFFBQWdCLEVBQUUsY0FBdUI7O2NBQ3ZELFVBQVUsR0FBNkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUU7O2NBQ3BFLElBQUksR0FBRztZQUNULFFBQVEsRUFBRSxRQUFRO1lBQ2xCLGVBQWUsRUFBRSxjQUFjO1NBQ2xDO1FBQ0QsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQzdELElBQUksQ0FDRCxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUNyQixVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7Ozs7Ozs7OztJQVNELGdCQUFnQixDQUFDLFNBQWlCLEVBQUUsSUFBcUIsRUFBRSxNQUFjOztjQUMvRCxVQUFVLEdBQTZCLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFO1FBQzFFLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLDZCQUE2QixDQUFDO1lBQ3JFLElBQUksRUFBRSxJQUFJO1lBQ1YsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2hCLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVTtZQUMzQixNQUFNLEVBQUUsU0FBUztZQUNqQixRQUFRLEVBQUUsSUFBSSxDQUFDLEVBQUUsR0FBRyxHQUFHLEdBQUcsTUFBTTtTQUNuQyxDQUFDLENBQUM7YUFDRSxJQUFJLENBQ0QsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFDaEIsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7Ozs7O0lBRUQsaUJBQWlCLENBQUMsSUFBaUIsRUFBRSxNQUFjLEVBQUUsU0FBaUI7O2NBQzVELFVBQVUsR0FBNkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUU7O2NBQ3BFLGFBQWEsR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQzs7Y0FDcEUsTUFBTSxHQUFpQztZQUN6QyxNQUFNLEVBQUUsU0FBUztZQUNqQixRQUFRLEVBQUUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRO1lBQy9CLFFBQVEsRUFBRSxJQUFJLENBQUMsRUFBRSxHQUFHLEdBQUcsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLEdBQUcsR0FBRyxHQUFHLGFBQWE7WUFDbEYsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJO1lBQ2YsSUFBSSxFQUFFLEtBQUs7U0FDZDtRQUNELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLDZCQUE2QixDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQzVFLElBQUksQ0FDRCxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUNoQixVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7Ozs7OztJQUVPLHVCQUF1QixDQUFDLElBQWlCOztZQUN6QyxRQUFRLEdBQUcsRUFBRTtRQUNqQixJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7O2tCQUNMLFNBQVMsR0FBRyxJQUFJLENBQUMsSUFBSTtpQkFDdEIsUUFBUSxDQUFDLElBQUk7Ozs7WUFBQyxDQUFDLFFBQXFCLEVBQUUsRUFBRSxDQUNyQyxRQUFRLENBQUMsUUFBUSxLQUFLLFNBQVM7Z0JBQy9CLFFBQVEsQ0FBQyxJQUFJLEtBQUssT0FBTyxFQUFDO1lBQ2xDLFFBQVEsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztTQUM5QztRQUNELE9BQU8sUUFBUSxDQUFDLGlCQUFpQixFQUFFLENBQUM7SUFDeEMsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsR0FBUTtRQUNYLElBQUksR0FBRyxFQUFFO1lBQ0wsT0FBTyxHQUFHLElBQUksRUFBRSxDQUFDO1NBQ3BCO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxHQUFRO1FBQ2hCLElBQUksR0FBRyxFQUFFO1lBQ0wsT0FBTyxHQUFHLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQztTQUN6QjtRQUNELE9BQU8sRUFBRSxDQUFDO0lBQ2QsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsS0FBVTs7WUFDZCxNQUFNLEdBQUcsc0JBQXNCLENBQUMscUJBQXFCO1FBQ3pELElBQUksS0FBSyxFQUFFO1lBQ1AsTUFBTSxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3RDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sTUFBTSxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLHFCQUFxQixDQUFDO1NBQzdHO1FBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDOUIsT0FBTyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDOUIsQ0FBQzs7QUFuSE0sNENBQXFCLEdBQVcsZUFBZSxDQUFDO0FBQ2hELDRDQUFxQixHQUFXLGNBQWMsQ0FBQzs7WUFOekQsVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBWFEsa0JBQWtCO1lBQ2xCLFVBQVU7Ozs7O0lBYWYsNkNBQXVEOztJQUN2RCw2Q0FBc0Q7Ozs7O0lBRTFDLDRDQUFzQzs7Ozs7SUFDdEMsNENBQThCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaUNvbXBhdGliaWxpdHksIE1pbmltYWxOb2RlLCBSZWxhdGVkQ29udGVudFJlcHJlc2VudGF0aW9uIH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZyb20sIHRocm93RXJyb3IgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgRXh0ZXJuYWxDb250ZW50IH0gZnJvbSAnLi4vY29tcG9uZW50cy93aWRnZXRzL2NvcmUvZXh0ZXJuYWwtY29udGVudCc7XHJcbmltcG9ydCB7IEV4dGVybmFsQ29udGVudExpbmsgfSBmcm9tICcuLi9jb21wb25lbnRzL3dpZGdldHMvY29yZS9leHRlcm5hbC1jb250ZW50LWxpbmsnO1xyXG5pbXBvcnQgeyBtYXAsIGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEFjdGl2aXRpQ29udGVudFNlcnZpY2Uge1xyXG5cclxuICAgIHN0YXRpYyBVTktOT1dOX0VSUk9SX01FU1NBR0U6IHN0cmluZyA9ICdVbmtub3duIGVycm9yJztcclxuICAgIHN0YXRpYyBHRU5FUklDX0VSUk9SX01FU1NBR0U6IHN0cmluZyA9ICdTZXJ2ZXIgZXJyb3InO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm5zIGEgbGlzdCBvZiBjaGlsZCBub2RlcyBiZWxvdyB0aGUgc3BlY2lmaWVkIGZvbGRlclxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBhY2NvdW50SWRcclxuICAgICAqIEBwYXJhbSBmb2xkZXJJZFxyXG4gICAgICovXHJcbiAgICBnZXRBbGZyZXNjb05vZGVzKGFjY291bnRJZDogc3RyaW5nLCBmb2xkZXJJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxbRXh0ZXJuYWxDb250ZW50XT4ge1xyXG4gICAgICAgIGNvbnN0IGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpQ29tcGF0aWJpbGl0eSA9IHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpO1xyXG4gICAgICAgIGNvbnN0IGFjY291bnRTaG9ydElkID0gYWNjb3VudElkLnJlcGxhY2UoJ2FsZnJlc2NvLScsICcnKTtcclxuICAgICAgICByZXR1cm4gZnJvbShhcGlTZXJ2aWNlLmFjdGl2aXRpLmFsZnJlc2NvQXBpLmdldENvbnRlbnRJbkZvbGRlcihhY2NvdW50U2hvcnRJZCwgZm9sZGVySWQpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCh0aGlzLnRvSnNvbkFycmF5KSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJucyBhIGxpc3Qgb2YgYWxsIHRoZSByZXBvc2l0b3JpZXMgY29uZmlndXJlZFxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBhY2NvdW50SWRcclxuICAgICAqIEBwYXJhbSBmb2xkZXJJZFxyXG4gICAgICovXHJcbiAgICBnZXRBbGZyZXNjb1JlcG9zaXRvcmllcyh0ZW5hbnRJZDogbnVtYmVyLCBpbmNsdWRlQWNjb3VudDogYm9vbGVhbik6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgYXBpU2VydmljZTogQWxmcmVzY29BcGlDb21wYXRpYmlsaXR5ID0gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCk7XHJcbiAgICAgICAgY29uc3Qgb3B0cyA9IHtcclxuICAgICAgICAgICAgdGVuYW50SWQ6IHRlbmFudElkLFxyXG4gICAgICAgICAgICBpbmNsdWRlQWNjb3VudHM6IGluY2x1ZGVBY2NvdW50XHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gZnJvbShhcGlTZXJ2aWNlLmFjdGl2aXRpLmFsZnJlc2NvQXBpLmdldFJlcG9zaXRvcmllcyhvcHRzKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy50b0pzb25BcnJheSksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybnMgYSBsaXN0IG9mIGNoaWxkIG5vZGVzIGJlbG93IHRoZSBzcGVjaWZpZWQgZm9sZGVyXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIGFjY291bnRJZFxyXG4gICAgICogQHBhcmFtIG5vZGVcclxuICAgICAqIEBwYXJhbSBzaXRlSWRcclxuICAgICAqL1xyXG4gICAgbGlua0FsZnJlc2NvTm9kZShhY2NvdW50SWQ6IHN0cmluZywgbm9kZTogRXh0ZXJuYWxDb250ZW50LCBzaXRlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8RXh0ZXJuYWxDb250ZW50TGluaz4ge1xyXG4gICAgICAgIGNvbnN0IGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpQ29tcGF0aWJpbGl0eSA9IHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpO1xyXG4gICAgICAgIHJldHVybiBmcm9tKGFwaVNlcnZpY2UuYWN0aXZpdGkuY29udGVudEFwaS5jcmVhdGVUZW1wb3JhcnlSZWxhdGVkQ29udGVudCh7XHJcbiAgICAgICAgICAgIGxpbms6IHRydWUsXHJcbiAgICAgICAgICAgIG5hbWU6IG5vZGUudGl0bGUsXHJcbiAgICAgICAgICAgIHNpbXBsZVR5cGU6IG5vZGUuc2ltcGxlVHlwZSxcclxuICAgICAgICAgICAgc291cmNlOiBhY2NvdW50SWQsXHJcbiAgICAgICAgICAgIHNvdXJjZUlkOiBub2RlLmlkICsgJ0AnICsgc2l0ZUlkXHJcbiAgICAgICAgfSkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKHRoaXMudG9Kc29uKSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIGFwcGx5QWxmcmVzY29Ob2RlKG5vZGU6IE1pbmltYWxOb2RlLCBzaXRlSWQ6IHN0cmluZywgYWNjb3VudElkOiBzdHJpbmcpIHtcclxuICAgICAgICBjb25zdCBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaUNvbXBhdGliaWxpdHkgPSB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKTtcclxuICAgICAgICBjb25zdCBjdXJyZW50U2lkZUlkID0gc2l0ZUlkID8gc2l0ZUlkIDogdGhpcy5nZXRTaXRlTmFtZUZyb21Ob2RlUGF0aChub2RlKTtcclxuICAgICAgICBjb25zdCBwYXJhbXM6IFJlbGF0ZWRDb250ZW50UmVwcmVzZW50YXRpb24gPSB7XHJcbiAgICAgICAgICAgIHNvdXJjZTogYWNjb3VudElkLFxyXG4gICAgICAgICAgICBtaW1lVHlwZTogbm9kZS5jb250ZW50Lm1pbWVUeXBlLFxyXG4gICAgICAgICAgICBzb3VyY2VJZDogbm9kZS5pZCArICc7JyArIG5vZGUucHJvcGVydGllc1snY206dmVyc2lvbkxhYmVsJ10gKyAnQCcgKyBjdXJyZW50U2lkZUlkLFxyXG4gICAgICAgICAgICBuYW1lOiBub2RlLm5hbWUsXHJcbiAgICAgICAgICAgIGxpbms6IGZhbHNlXHJcbiAgICAgICAgfTtcclxuICAgICAgICByZXR1cm4gZnJvbShhcGlTZXJ2aWNlLmFjdGl2aXRpLmNvbnRlbnRBcGkuY3JlYXRlVGVtcG9yYXJ5UmVsYXRlZENvbnRlbnQocGFyYW1zKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy50b0pzb24pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRTaXRlTmFtZUZyb21Ob2RlUGF0aChub2RlOiBNaW5pbWFsTm9kZSk6IHN0cmluZyB7XHJcbiAgICAgICAgbGV0IHNpdGVOYW1lID0gJyc7XHJcbiAgICAgICAgaWYgKG5vZGUucGF0aCkge1xyXG4gICAgICAgICAgICBjb25zdCBmb3VuZE5vZGUgPSBub2RlLnBhdGhcclxuICAgICAgICAgICAgICAgIC5lbGVtZW50cy5maW5kKChwYXRoTm9kZTogTWluaW1hbE5vZGUpID0+XHJcbiAgICAgICAgICAgICAgICAgICAgcGF0aE5vZGUubm9kZVR5cGUgPT09ICdzdDpzaXRlJyAmJlxyXG4gICAgICAgICAgICAgICAgICAgIHBhdGhOb2RlLm5hbWUgIT09ICdTaXRlcycpO1xyXG4gICAgICAgICAgICBzaXRlTmFtZSA9IGZvdW5kTm9kZSA/IGZvdW5kTm9kZS5uYW1lIDogJyc7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBzaXRlTmFtZS50b0xvY2FsZUxvd2VyQ2FzZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHRvSnNvbihyZXM6IGFueSkge1xyXG4gICAgICAgIGlmIChyZXMpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHJlcyB8fCB7fTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHt9O1xyXG4gICAgfVxyXG5cclxuICAgIHRvSnNvbkFycmF5KHJlczogYW55KSB7XHJcbiAgICAgICAgaWYgKHJlcykge1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzLmRhdGEgfHwgW107XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBbXTtcclxuICAgIH1cclxuXHJcbiAgICBoYW5kbGVFcnJvcihlcnJvcjogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBsZXQgZXJyTXNnID0gQWN0aXZpdGlDb250ZW50U2VydmljZS5VTktOT1dOX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgaWYgKGVycm9yKSB7XHJcbiAgICAgICAgICAgIGVyck1zZyA9IChlcnJvci5tZXNzYWdlKSA/IGVycm9yLm1lc3NhZ2UgOlxyXG4gICAgICAgICAgICAgICAgZXJyb3Iuc3RhdHVzID8gYCR7ZXJyb3Iuc3RhdHVzfSAtICR7ZXJyb3Iuc3RhdHVzVGV4dH1gIDogQWN0aXZpdGlDb250ZW50U2VydmljZS5HRU5FUklDX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJNc2cpO1xyXG4gICAgICAgIHJldHVybiB0aHJvd0Vycm9yKGVyck1zZyk7XHJcbiAgICB9XHJcbn1cclxuIl19