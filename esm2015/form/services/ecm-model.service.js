/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { LogService } from '../../services/log.service';
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "../../services/alfresco-api.service";
import * as i2 from "../../services/log.service";
export class EcmModelService {
    /**
     * @param {?} apiService
     * @param {?} logService
     */
    constructor(apiService, logService) {
        this.apiService = apiService;
        this.logService = logService;
    }
    /**
     * @param {?} formName
     * @param {?} form
     * @return {?}
     */
    createEcmTypeForActivitiForm(formName, form) {
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        (observer) => {
            this.searchActivitiEcmModel().subscribe((/**
             * @param {?} model
             * @return {?}
             */
            (model) => {
                if (!model) {
                    this.createActivitiEcmModel(formName, form).subscribe((/**
                     * @param {?} typeForm
                     * @return {?}
                     */
                    (typeForm) => {
                        observer.next(typeForm);
                        observer.complete();
                    }));
                }
                else {
                    this.saveFomType(formName, form).subscribe((/**
                     * @param {?} typeForm
                     * @return {?}
                     */
                    (typeForm) => {
                        observer.next(typeForm);
                        observer.complete();
                    }));
                }
            }), (/**
             * @param {?} err
             * @return {?}
             */
            (err) => this.handleError(err)));
        }));
    }
    /**
     * @return {?}
     */
    searchActivitiEcmModel() {
        return this.getEcmModels().pipe(map((/**
         * @param {?} ecmModels
         * @return {?}
         */
        function (ecmModels) {
            return ecmModels.list.entries.find((/**
             * @param {?} model
             * @return {?}
             */
            (model) => model.entry.name === EcmModelService.MODEL_NAME));
        })));
    }
    /**
     * @param {?} formName
     * @param {?} form
     * @return {?}
     */
    createActivitiEcmModel(formName, form) {
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        (observer) => {
            this.createEcmModel(EcmModelService.MODEL_NAME, EcmModelService.MODEL_NAMESPACE).subscribe((/**
             * @param {?} model
             * @return {?}
             */
            (model) => {
                this.logService.info('model created', model);
                this.activeEcmModel(EcmModelService.MODEL_NAME).subscribe((/**
                 * @param {?} modelActive
                 * @return {?}
                 */
                (modelActive) => {
                    this.logService.info('model active', modelActive);
                    this.createEcmTypeWithProperties(formName, form).subscribe((/**
                     * @param {?} typeCreated
                     * @return {?}
                     */
                    (typeCreated) => {
                        observer.next(typeCreated);
                        observer.complete();
                    }));
                }), (/**
                 * @param {?} err
                 * @return {?}
                 */
                (err) => this.handleError(err)));
            }), (/**
             * @param {?} err
             * @return {?}
             */
            (err) => this.handleError(err)));
        }));
    }
    /**
     * @param {?} formName
     * @param {?} form
     * @return {?}
     */
    saveFomType(formName, form) {
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        (observer) => {
            this.searchEcmType(formName, EcmModelService.MODEL_NAME).subscribe((/**
             * @param {?} ecmType
             * @return {?}
             */
            (ecmType) => {
                this.logService.info('custom types', ecmType);
                if (!ecmType) {
                    this.createEcmTypeWithProperties(formName, form).subscribe((/**
                     * @param {?} typeCreated
                     * @return {?}
                     */
                    (typeCreated) => {
                        observer.next(typeCreated);
                        observer.complete();
                    }));
                }
                else {
                    observer.next(ecmType);
                    observer.complete();
                }
            }), (/**
             * @param {?} err
             * @return {?}
             */
            (err) => this.handleError(err)));
        }));
    }
    /**
     * @param {?} formName
     * @param {?} form
     * @return {?}
     */
    createEcmTypeWithProperties(formName, form) {
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        (observer) => {
            this.createEcmType(formName, EcmModelService.MODEL_NAME, EcmModelService.TYPE_MODEL).subscribe((/**
             * @param {?} typeCreated
             * @return {?}
             */
            (typeCreated) => {
                this.logService.info('type Created', typeCreated);
                this.addPropertyToAType(EcmModelService.MODEL_NAME, formName, form).subscribe((/**
                 * @param {?} propertyAdded
                 * @return {?}
                 */
                (propertyAdded) => {
                    this.logService.info('property Added', propertyAdded);
                    observer.next(typeCreated);
                    observer.complete();
                }), (/**
                 * @param {?} err
                 * @return {?}
                 */
                (err) => this.handleError(err)));
            }), (/**
             * @param {?} err
             * @return {?}
             */
            (err) => this.handleError(err)));
        }));
    }
    /**
     * @param {?} typeName
     * @param {?} modelName
     * @return {?}
     */
    searchEcmType(typeName, modelName) {
        return this.getEcmType(modelName).pipe(map((/**
         * @param {?} customTypes
         * @return {?}
         */
        function (customTypes) {
            return customTypes.list.entries.find((/**
             * @param {?} type
             * @return {?}
             */
            (type) => type.entry.prefixedName === typeName || type.entry.title === typeName));
        })));
    }
    /**
     * @param {?} modelName
     * @return {?}
     */
    activeEcmModel(modelName) {
        return from(this.apiService.getInstance().core.customModelApi.activateCustomModel(modelName))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * @param {?} modelName
     * @param {?} nameSpace
     * @return {?}
     */
    createEcmModel(modelName, nameSpace) {
        return from(this.apiService.getInstance().core.customModelApi.createCustomModel('DRAFT', '', modelName, modelName, nameSpace))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * @return {?}
     */
    getEcmModels() {
        return from(this.apiService.getInstance().core.customModelApi.getAllCustomModel())
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * @param {?} modelName
     * @return {?}
     */
    getEcmType(modelName) {
        return from(this.apiService.getInstance().core.customModelApi.getAllCustomType(modelName))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * @param {?} typeName
     * @param {?} modelName
     * @param {?} parentType
     * @return {?}
     */
    createEcmType(typeName, modelName, parentType) {
        /** @type {?} */
        const name = this.cleanNameType(typeName);
        return from(this.apiService.getInstance().core.customModelApi.createCustomType(modelName, name, parentType, typeName, ''))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * @param {?} modelName
     * @param {?} typeName
     * @param {?} formFields
     * @return {?}
     */
    addPropertyToAType(modelName, typeName, formFields) {
        /** @type {?} */
        const name = this.cleanNameType(typeName);
        /** @type {?} */
        const properties = [];
        if (formFields && formFields.values) {
            for (const key in formFields.values) {
                if (key) {
                    properties.push({
                        name: key,
                        title: key,
                        description: key,
                        dataType: 'd:text',
                        multiValued: false,
                        mandatory: false,
                        mandatoryEnforced: false
                    });
                }
            }
        }
        return from(this.apiService.getInstance().core.customModelApi.addPropertyToType(modelName, name, properties))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * @param {?} name
     * @return {?}
     */
    cleanNameType(name) {
        /** @type {?} */
        let cleanName = name;
        if (name.indexOf(':') !== -1) {
            cleanName = name.split(':')[1];
        }
        return cleanName.replace(/[^a-zA-Z ]/g, '');
    }
    /**
     * @param {?} res
     * @return {?}
     */
    toJson(res) {
        if (res) {
            return res || {};
        }
        return {};
    }
    /**
     * @param {?} err
     * @return {?}
     */
    handleError(err) {
        this.logService.error(err);
    }
}
EcmModelService.MODEL_NAMESPACE = 'activitiForms';
EcmModelService.MODEL_NAME = 'activitiFormsModel';
EcmModelService.TYPE_MODEL = 'cm:folder';
EcmModelService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
EcmModelService.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: LogService }
];
/** @nocollapse */ EcmModelService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function EcmModelService_Factory() { return new EcmModelService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: EcmModelService, providedIn: "root" });
if (false) {
    /** @type {?} */
    EcmModelService.MODEL_NAMESPACE;
    /** @type {?} */
    EcmModelService.MODEL_NAME;
    /** @type {?} */
    EcmModelService.TYPE_MODEL;
    /**
     * @type {?}
     * @private
     */
    EcmModelService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    EcmModelService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWNtLW1vZGVsLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL3NlcnZpY2VzL2VjbS1tb2RlbC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUN6RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsSUFBSSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRXhDLE9BQU8sRUFBRSxHQUFHLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7QUFLakQsTUFBTSxPQUFPLGVBQWU7Ozs7O0lBTXhCLFlBQW9CLFVBQThCLEVBQzlCLFVBQXNCO1FBRHRCLGVBQVUsR0FBVixVQUFVLENBQW9CO1FBQzlCLGVBQVUsR0FBVixVQUFVLENBQVk7SUFDMUMsQ0FBQzs7Ozs7O0lBRU0sNEJBQTRCLENBQUMsUUFBZ0IsRUFBRSxJQUFlO1FBQ2pFLE9BQU8sSUFBSSxVQUFVOzs7O1FBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtZQUMvQixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQyxTQUFTOzs7O1lBQ25DLENBQUMsS0FBSyxFQUFFLEVBQUU7Z0JBQ04sSUFBSSxDQUFDLEtBQUssRUFBRTtvQkFDUixJQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVM7Ozs7b0JBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTt3QkFDL0QsUUFBUSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzt3QkFDeEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUN4QixDQUFDLEVBQUMsQ0FBQztpQkFDTjtxQkFBTTtvQkFDSCxJQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTOzs7O29CQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7d0JBQ3BELFFBQVEsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7d0JBQ3hCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDeEIsQ0FBQyxFQUFDLENBQUM7aUJBQ047WUFDTCxDQUFDOzs7O1lBQ0QsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQ2pDLENBQUM7UUFDTixDQUFDLEVBQUMsQ0FBQztJQUVQLENBQUM7Ozs7SUFFRCxzQkFBc0I7UUFDbEIsT0FBTyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUc7Ozs7UUFBQyxVQUFVLFNBQWM7WUFDeEQsT0FBTyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJOzs7O1lBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLGVBQWUsQ0FBQyxVQUFVLEVBQUMsQ0FBQztRQUNuRyxDQUFDLEVBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7O0lBRUQsc0JBQXNCLENBQUMsUUFBZ0IsRUFBRSxJQUFlO1FBQ3BELE9BQU8sSUFBSSxVQUFVOzs7O1FBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtZQUMvQixJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsZUFBZSxDQUFDLGVBQWUsQ0FBQyxDQUFDLFNBQVM7Ozs7WUFDdEYsQ0FBQyxLQUFLLEVBQUUsRUFBRTtnQkFDTixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsS0FBSyxDQUFDLENBQUM7Z0JBQzdDLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVM7Ozs7Z0JBQ3JELENBQUMsV0FBVyxFQUFFLEVBQUU7b0JBQ1osSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUNsRCxJQUFJLENBQUMsMkJBQTJCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVM7Ozs7b0JBQUMsQ0FBQyxXQUFXLEVBQUUsRUFBRTt3QkFDdkUsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFDM0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUN4QixDQUFDLEVBQUMsQ0FBQztnQkFDUCxDQUFDOzs7O2dCQUNELENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUNqQyxDQUFDO1lBQ04sQ0FBQzs7OztZQUNELENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUNqQyxDQUFDO1FBQ04sQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFFRCxXQUFXLENBQUMsUUFBZ0IsRUFBRSxJQUFlO1FBQ3pDLE9BQU8sSUFBSSxVQUFVOzs7O1FBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtZQUMvQixJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUzs7OztZQUM5RCxDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUNSLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDOUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtvQkFDVixJQUFJLENBQUMsMkJBQTJCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVM7Ozs7b0JBQUMsQ0FBQyxXQUFXLEVBQUUsRUFBRTt3QkFDdkUsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFDM0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUN4QixDQUFDLEVBQUMsQ0FBQztpQkFDTjtxQkFBTTtvQkFDSCxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO29CQUN2QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO1lBQ0wsQ0FBQzs7OztZQUNELENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUNqQyxDQUFDO1FBQ04sQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFFTSwyQkFBMkIsQ0FBQyxRQUFnQixFQUFFLElBQWU7UUFDaEUsT0FBTyxJQUFJLFVBQVU7Ozs7UUFBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO1lBQy9CLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLGVBQWUsQ0FBQyxVQUFVLEVBQUUsZUFBZSxDQUFDLFVBQVUsQ0FBQyxDQUFDLFNBQVM7Ozs7WUFDMUYsQ0FBQyxXQUFXLEVBQUUsRUFBRTtnQkFDWixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsV0FBVyxDQUFDLENBQUM7Z0JBQ2xELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxlQUFlLENBQUMsVUFBVSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTOzs7O2dCQUN6RSxDQUFDLGFBQWEsRUFBRSxFQUFFO29CQUNkLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLGFBQWEsQ0FBQyxDQUFDO29CQUN0RCxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUMzQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUM7Ozs7Z0JBQ0QsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FBQztZQUN4QyxDQUFDOzs7O1lBQ0QsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FBQztRQUN4QyxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQUVNLGFBQWEsQ0FBQyxRQUFnQixFQUFFLFNBQWlCO1FBQ3BELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRzs7OztRQUFDLFVBQVUsV0FBZ0I7WUFDakUsT0FBTyxXQUFXLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJOzs7O1lBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxLQUFLLFFBQVEsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssS0FBSyxRQUFRLEVBQUMsQ0FBQztRQUMxSCxDQUFDLEVBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7SUFFTSxjQUFjLENBQUMsU0FBaUI7UUFDbkMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ3hGLElBQUksQ0FDRCxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUNoQixVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7Ozs7OztJQUVNLGNBQWMsQ0FBQyxTQUFpQixFQUFFLFNBQWlCO1FBQ3RELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEVBQUUsRUFBRSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsU0FBUyxDQUFDLENBQUM7YUFDekgsSUFBSSxDQUNELEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQ2hCLFVBQVU7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQzs7OztJQUVNLFlBQVk7UUFDZixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLEVBQUUsQ0FBQzthQUM3RSxJQUFJLENBQ0QsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFDaEIsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7OztJQUVNLFVBQVUsQ0FBQyxTQUFpQjtRQUMvQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDckYsSUFBSSxDQUNELEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQ2hCLFVBQVU7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQzs7Ozs7OztJQUVNLGFBQWEsQ0FBQyxRQUFnQixFQUFFLFNBQWlCLEVBQUUsVUFBa0I7O2NBQ2xFLElBQUksR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQztRQUV6QyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2FBQ3JILElBQUksQ0FDRCxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUNoQixVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7Ozs7Ozs7SUFFTSxrQkFBa0IsQ0FBQyxTQUFpQixFQUFFLFFBQWdCLEVBQUUsVUFBZTs7Y0FDcEUsSUFBSSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDOztjQUVuQyxVQUFVLEdBQUcsRUFBRTtRQUNyQixJQUFJLFVBQVUsSUFBSSxVQUFVLENBQUMsTUFBTSxFQUFFO1lBQ2pDLEtBQUssTUFBTSxHQUFHLElBQUksVUFBVSxDQUFDLE1BQU0sRUFBRTtnQkFDakMsSUFBSSxHQUFHLEVBQUU7b0JBQ0wsVUFBVSxDQUFDLElBQUksQ0FBQzt3QkFDWixJQUFJLEVBQUUsR0FBRzt3QkFDVCxLQUFLLEVBQUUsR0FBRzt3QkFDVixXQUFXLEVBQUUsR0FBRzt3QkFDaEIsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLFdBQVcsRUFBRSxLQUFLO3dCQUNsQixTQUFTLEVBQUUsS0FBSzt3QkFDaEIsaUJBQWlCLEVBQUUsS0FBSztxQkFDM0IsQ0FBQyxDQUFDO2lCQUNOO2FBQ0o7U0FDSjtRQUVELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO2FBQ3hHLElBQUksQ0FDRCxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUNoQixVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDN0MsQ0FBQztJQUVWLENBQUM7Ozs7O0lBRUQsYUFBYSxDQUFDLElBQVk7O1lBQ2xCLFNBQVMsR0FBRyxJQUFJO1FBQ3BCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTtZQUMxQixTQUFTLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNsQztRQUNELE9BQU8sU0FBUyxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDaEQsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsR0FBUTtRQUNYLElBQUksR0FBRyxFQUFFO1lBQ0wsT0FBTyxHQUFHLElBQUksRUFBRSxDQUFDO1NBQ3BCO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxHQUFRO1FBQ2hCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQy9CLENBQUM7O0FBM0xhLCtCQUFlLEdBQVcsZUFBZSxDQUFDO0FBQzFDLDBCQUFVLEdBQVcsb0JBQW9CLENBQUM7QUFDMUMsMEJBQVUsR0FBVyxXQUFXLENBQUM7O1lBUGxELFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQjs7OztZQVJRLGtCQUFrQjtZQURsQixVQUFVOzs7OztJQVlmLGdDQUF3RDs7SUFDeEQsMkJBQXdEOztJQUN4RCwyQkFBK0M7Ozs7O0lBRW5DLHFDQUFzQzs7Ozs7SUFDdEMscUNBQThCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9sb2cuc2VydmljZSc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmcm9tIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEZvcm1Nb2RlbCB9IGZyb20gJy4uL2NvbXBvbmVudHMvd2lkZ2V0cy9jb3JlL2Zvcm0ubW9kZWwnO1xyXG5pbXBvcnQgeyBtYXAsIGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEVjbU1vZGVsU2VydmljZSB7XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBNT0RFTF9OQU1FU1BBQ0U6IHN0cmluZyA9ICdhY3Rpdml0aUZvcm1zJztcclxuICAgIHB1YmxpYyBzdGF0aWMgTU9ERUxfTkFNRTogc3RyaW5nID0gJ2FjdGl2aXRpRm9ybXNNb2RlbCc7XHJcbiAgICBwdWJsaWMgc3RhdGljIFRZUEVfTU9ERUw6IHN0cmluZyA9ICdjbTpmb2xkZXInO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGNyZWF0ZUVjbVR5cGVGb3JBY3Rpdml0aUZvcm0oZm9ybU5hbWU6IHN0cmluZywgZm9ybTogRm9ybU1vZGVsKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUoKG9ic2VydmVyKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VhcmNoQWN0aXZpdGlFY21Nb2RlbCgpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIChtb2RlbCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghbW9kZWwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVBY3Rpdml0aUVjbU1vZGVsKGZvcm1OYW1lLCBmb3JtKS5zdWJzY3JpYmUoKHR5cGVGb3JtKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHR5cGVGb3JtKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2F2ZUZvbVR5cGUoZm9ybU5hbWUsIGZvcm0pLnN1YnNjcmliZSgodHlwZUZvcm0pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHlwZUZvcm0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBzZWFyY2hBY3Rpdml0aUVjbU1vZGVsKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldEVjbU1vZGVscygpLnBpcGUobWFwKGZ1bmN0aW9uIChlY21Nb2RlbHM6IGFueSkge1xyXG4gICAgICAgICAgICByZXR1cm4gZWNtTW9kZWxzLmxpc3QuZW50cmllcy5maW5kKChtb2RlbCkgPT4gbW9kZWwuZW50cnkubmFtZSA9PT0gRWNtTW9kZWxTZXJ2aWNlLk1PREVMX05BTUUpO1xyXG4gICAgICAgIH0pKTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVBY3Rpdml0aUVjbU1vZGVsKGZvcm1OYW1lOiBzdHJpbmcsIGZvcm06IEZvcm1Nb2RlbCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKChvYnNlcnZlcikgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmNyZWF0ZUVjbU1vZGVsKEVjbU1vZGVsU2VydmljZS5NT0RFTF9OQU1FLCBFY21Nb2RlbFNlcnZpY2UuTU9ERUxfTkFNRVNQQUNFKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAobW9kZWwpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuaW5mbygnbW9kZWwgY3JlYXRlZCcsIG1vZGVsKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFjdGl2ZUVjbU1vZGVsKEVjbU1vZGVsU2VydmljZS5NT0RFTF9OQU1FKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChtb2RlbEFjdGl2ZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmluZm8oJ21vZGVsIGFjdGl2ZScsIG1vZGVsQWN0aXZlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3JlYXRlRWNtVHlwZVdpdGhQcm9wZXJ0aWVzKGZvcm1OYW1lLCBmb3JtKS5zdWJzY3JpYmUoKHR5cGVDcmVhdGVkKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0eXBlQ3JlYXRlZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycilcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHNhdmVGb21UeXBlKGZvcm1OYW1lOiBzdHJpbmcsIGZvcm06IEZvcm1Nb2RlbCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKChvYnNlcnZlcikgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNlYXJjaEVjbVR5cGUoZm9ybU5hbWUsIEVjbU1vZGVsU2VydmljZS5NT0RFTF9OQU1FKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAoZWNtVHlwZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9nU2VydmljZS5pbmZvKCdjdXN0b20gdHlwZXMnLCBlY21UeXBlKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIWVjbVR5cGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVFY21UeXBlV2l0aFByb3BlcnRpZXMoZm9ybU5hbWUsIGZvcm0pLnN1YnNjcmliZSgodHlwZUNyZWF0ZWQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHlwZUNyZWF0ZWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChlY21UeXBlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGNyZWF0ZUVjbVR5cGVXaXRoUHJvcGVydGllcyhmb3JtTmFtZTogc3RyaW5nLCBmb3JtOiBGb3JtTW9kZWwpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZSgob2JzZXJ2ZXIpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5jcmVhdGVFY21UeXBlKGZvcm1OYW1lLCBFY21Nb2RlbFNlcnZpY2UuTU9ERUxfTkFNRSwgRWNtTW9kZWxTZXJ2aWNlLlRZUEVfTU9ERUwpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgICh0eXBlQ3JlYXRlZCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9nU2VydmljZS5pbmZvKCd0eXBlIENyZWF0ZWQnLCB0eXBlQ3JlYXRlZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRQcm9wZXJ0eVRvQVR5cGUoRWNtTW9kZWxTZXJ2aWNlLk1PREVMX05BTUUsIGZvcm1OYW1lLCBmb3JtKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChwcm9wZXJ0eUFkZGVkKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuaW5mbygncHJvcGVydHkgQWRkZWQnLCBwcm9wZXJ0eUFkZGVkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHlwZUNyZWF0ZWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZWFyY2hFY21UeXBlKHR5cGVOYW1lOiBzdHJpbmcsIG1vZGVsTmFtZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRFY21UeXBlKG1vZGVsTmFtZSkucGlwZShtYXAoZnVuY3Rpb24gKGN1c3RvbVR5cGVzOiBhbnkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGN1c3RvbVR5cGVzLmxpc3QuZW50cmllcy5maW5kKCh0eXBlKSA9PiB0eXBlLmVudHJ5LnByZWZpeGVkTmFtZSA9PT0gdHlwZU5hbWUgfHwgdHlwZS5lbnRyeS50aXRsZSA9PT0gdHlwZU5hbWUpO1xyXG4gICAgICAgIH0pKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYWN0aXZlRWNtTW9kZWwobW9kZWxOYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmNvcmUuY3VzdG9tTW9kZWxBcGkuYWN0aXZhdGVDdXN0b21Nb2RlbChtb2RlbE5hbWUpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCh0aGlzLnRvSnNvbiksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgY3JlYXRlRWNtTW9kZWwobW9kZWxOYW1lOiBzdHJpbmcsIG5hbWVTcGFjZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5jb3JlLmN1c3RvbU1vZGVsQXBpLmNyZWF0ZUN1c3RvbU1vZGVsKCdEUkFGVCcsICcnLCBtb2RlbE5hbWUsIG1vZGVsTmFtZSwgbmFtZVNwYWNlKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy50b0pzb24pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldEVjbU1vZGVscygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmNvcmUuY3VzdG9tTW9kZWxBcGkuZ2V0QWxsQ3VzdG9tTW9kZWwoKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy50b0pzb24pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldEVjbVR5cGUobW9kZWxOYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmNvcmUuY3VzdG9tTW9kZWxBcGkuZ2V0QWxsQ3VzdG9tVHlwZShtb2RlbE5hbWUpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCh0aGlzLnRvSnNvbiksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgY3JlYXRlRWNtVHlwZSh0eXBlTmFtZTogc3RyaW5nLCBtb2RlbE5hbWU6IHN0cmluZywgcGFyZW50VHlwZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBuYW1lID0gdGhpcy5jbGVhbk5hbWVUeXBlKHR5cGVOYW1lKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuY29yZS5jdXN0b21Nb2RlbEFwaS5jcmVhdGVDdXN0b21UeXBlKG1vZGVsTmFtZSwgbmFtZSwgcGFyZW50VHlwZSwgdHlwZU5hbWUsICcnKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy50b0pzb24pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFkZFByb3BlcnR5VG9BVHlwZShtb2RlbE5hbWU6IHN0cmluZywgdHlwZU5hbWU6IHN0cmluZywgZm9ybUZpZWxkczogYW55KSB7XHJcbiAgICAgICAgY29uc3QgbmFtZSA9IHRoaXMuY2xlYW5OYW1lVHlwZSh0eXBlTmFtZSk7XHJcblxyXG4gICAgICAgIGNvbnN0IHByb3BlcnRpZXMgPSBbXTtcclxuICAgICAgICBpZiAoZm9ybUZpZWxkcyAmJiBmb3JtRmllbGRzLnZhbHVlcykge1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBmb3JtRmllbGRzLnZhbHVlcykge1xyXG4gICAgICAgICAgICAgICAgaWYgKGtleSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHByb3BlcnRpZXMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IGtleSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IGtleSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IGtleSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YVR5cGU6ICdkOnRleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtdWx0aVZhbHVlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hbmRhdG9yeTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hbmRhdG9yeUVuZm9yY2VkOiBmYWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5jb3JlLmN1c3RvbU1vZGVsQXBpLmFkZFByb3BlcnR5VG9UeXBlKG1vZGVsTmFtZSwgbmFtZSwgcHJvcGVydGllcykpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKHRoaXMudG9Kc29uKSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBjbGVhbk5hbWVUeXBlKG5hbWU6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgbGV0IGNsZWFuTmFtZSA9IG5hbWU7XHJcbiAgICAgICAgaWYgKG5hbWUuaW5kZXhPZignOicpICE9PSAtMSkge1xyXG4gICAgICAgICAgICBjbGVhbk5hbWUgPSBuYW1lLnNwbGl0KCc6JylbMV07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBjbGVhbk5hbWUucmVwbGFjZSgvW15hLXpBLVogXS9nLCAnJyk7XHJcbiAgICB9XHJcblxyXG4gICAgdG9Kc29uKHJlczogYW55KSB7XHJcbiAgICAgICAgaWYgKHJlcykge1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzIHx8IHt9O1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4ge307XHJcbiAgICB9XHJcblxyXG4gICAgaGFuZGxlRXJyb3IoZXJyOiBhbnkpOiBhbnkge1xyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnIpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==