/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import { Injectable } from '@angular/core';
import { Observable, Subject, from, of, throwError } from 'rxjs';
import { FormDefinitionModel } from '../models/form-definition.model';
import { FormModel, FormOutcomeModel } from './../components/widgets/core/index';
import { EcmModelService } from './ecm-model.service';
import { map, catchError, switchMap, combineAll, defaultIfEmpty } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./ecm-model.service";
import * as i2 from "../../services/alfresco-api.service";
import * as i3 from "../../services/log.service";
export class FormService {
    /**
     * @param {?} ecmModelService
     * @param {?} apiService
     * @param {?} logService
     */
    constructor(ecmModelService, apiService, logService) {
        this.ecmModelService = ecmModelService;
        this.apiService = apiService;
        this.logService = logService;
        this.formLoaded = new Subject();
        this.formDataRefreshed = new Subject();
        this.formFieldValueChanged = new Subject();
        this.formEvents = new Subject();
        this.taskCompleted = new Subject();
        this.taskCompletedError = new Subject();
        this.taskSaved = new Subject();
        this.taskSavedError = new Subject();
        this.formContentClicked = new Subject();
        this.validateForm = new Subject();
        this.validateFormField = new Subject();
        this.validateDynamicTableRow = new Subject();
        this.executeOutcome = new Subject();
    }
    /**
     * @private
     * @return {?}
     */
    get taskApi() {
        return this.apiService.getInstance().activiti.taskApi;
    }
    /**
     * @private
     * @return {?}
     */
    get modelsApi() {
        return this.apiService.getInstance().activiti.modelsApi;
    }
    /**
     * @private
     * @return {?}
     */
    get editorApi() {
        return this.apiService.getInstance().activiti.editorApi;
    }
    /**
     * @private
     * @return {?}
     */
    get processApi() {
        return this.apiService.getInstance().activiti.processApi;
    }
    /**
     * @private
     * @return {?}
     */
    get processInstanceVariablesApi() {
        return this.apiService.getInstance().activiti.processInstanceVariablesApi;
    }
    /**
     * @private
     * @return {?}
     */
    get usersWorkflowApi() {
        return this.apiService.getInstance().activiti.usersWorkflowApi;
    }
    /**
     * @private
     * @return {?}
     */
    get groupsApi() {
        return this.apiService.getInstance().activiti.groupsApi;
    }
    /**
     * Parses JSON data to create a corresponding Form model.
     * @param {?} json JSON to create the form
     * @param {?=} data Values for the form fields
     * @param {?=} readOnly Should the form fields be read-only?
     * @return {?} Form model created from input data
     */
    parseForm(json, data, readOnly = false) {
        if (json) {
            /** @type {?} */
            const form = new FormModel(json, data, readOnly, this);
            if (!json.fields) {
                form.outcomes = [
                    new FormOutcomeModel(form, {
                        id: '$save',
                        name: FormOutcomeModel.SAVE_ACTION,
                        isSystem: true
                    })
                ];
            }
            return form;
        }
        return null;
    }
    /**
     * Creates a Form with a field for each metadata property.
     * @param {?} formName Name of the new form
     * @return {?} The new form
     */
    createFormFromANode(formName) {
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        (observer) => {
            this.createForm(formName).subscribe((/**
             * @param {?} form
             * @return {?}
             */
            (form) => {
                this.ecmModelService.searchEcmType(formName, EcmModelService.MODEL_NAME).subscribe((/**
                 * @param {?} customType
                 * @return {?}
                 */
                (customType) => {
                    /** @type {?} */
                    const formDefinitionModel = new FormDefinitionModel(form.id, form.name, form.lastUpdatedByFullName, form.lastUpdated, customType.entry.properties);
                    from(this.editorApi.saveForm(form.id, formDefinitionModel)).subscribe((/**
                     * @param {?} formData
                     * @return {?}
                     */
                    (formData) => {
                        observer.next(formData);
                        observer.complete();
                    }), (/**
                     * @param {?} err
                     * @return {?}
                     */
                    (err) => this.handleError(err)));
                }), (/**
                 * @param {?} err
                 * @return {?}
                 */
                (err) => this.handleError(err)));
            }), (/**
             * @param {?} err
             * @return {?}
             */
            (err) => this.handleError(err)));
        }));
    }
    /**
     * Create a Form.
     * @param {?} formName Name of the new form
     * @return {?} The new form
     */
    createForm(formName) {
        /** @type {?} */
        const dataModel = {
            name: formName,
            description: '',
            modelType: 2,
            stencilSet: 0
        };
        return from(this.modelsApi.createModel(dataModel));
    }
    /**
     * Saves a form.
     * @param {?} formId ID of the form to save
     * @param {?} formModel Model data for the form
     * @return {?} Data for the saved form
     */
    saveForm(formId, formModel) {
        return from(this.editorApi.saveForm(formId, formModel));
    }
    /**
     * Searches for a form by name.
     * @param {?} name The form name to search for
     * @return {?} Form model(s) matching the search name
     */
    searchFrom(name) {
        /** @type {?} */
        const opts = {
            'modelType': 2
        };
        return from(this.modelsApi.getModels(opts))
            .pipe(map((/**
         * @param {?} forms
         * @return {?}
         */
        function (forms) {
            return forms.data.find((/**
             * @param {?} formData
             * @return {?}
             */
            (formData) => formData.name === name));
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets all the forms.
     * @return {?} List of form models
     */
    getForms() {
        /** @type {?} */
        const opts = {
            'modelType': 2
        };
        return from(this.modelsApi.getModels(opts))
            .pipe(map(this.toJsonArray), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets process definitions.
     * @return {?} List of process definitions
     */
    getProcessDefinitions() {
        return from(this.processApi.getProcessDefinitions({}))
            .pipe(map(this.toJsonArray), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets instance variables for a process.
     * @param {?} processInstanceId ID of the target process
     * @return {?} List of instance variable information
     */
    getProcessVariablesById(processInstanceId) {
        return from(this.processInstanceVariablesApi.getProcessInstanceVariables(processInstanceId))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets all the tasks.
     * @return {?} List of tasks
     */
    getTasks() {
        return from(this.taskApi.listTasks({}))
            .pipe(map(this.toJsonArray), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets a task.
     * @param {?} taskId Task Id
     * @return {?} Task info
     */
    getTask(taskId) {
        return from(this.taskApi.getTask(taskId))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Saves a task form.
     * @param {?} taskId Task Id
     * @param {?} formValues Form Values
     * @return {?} Null response when the operation is complete
     */
    saveTaskForm(taskId, formValues) {
        /** @type {?} */
        const saveFormRepresentation = (/** @type {?} */ ({ values: formValues }));
        return from(this.taskApi.saveTaskForm(taskId, saveFormRepresentation))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Completes a Task Form.
     * @param {?} taskId Task Id
     * @param {?} formValues Form Values
     * @param {?=} outcome Form Outcome
     * @return {?} Null response when the operation is complete
     */
    completeTaskForm(taskId, formValues, outcome) {
        /** @type {?} */
        const completeFormRepresentation = (/** @type {?} */ ({ values: formValues }));
        if (outcome) {
            completeFormRepresentation.outcome = outcome;
        }
        return from(this.taskApi.completeTaskForm(taskId, completeFormRepresentation))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets a form related to a task.
     * @param {?} taskId ID of the target task
     * @return {?} Form definition
     */
    getTaskForm(taskId) {
        return from(this.taskApi.getTaskForm(taskId))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets a form definition.
     * @param {?} formId ID of the target form
     * @return {?} Form definition
     */
    getFormDefinitionById(formId) {
        return from(this.editorApi.getForm(formId))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets the form definition with a given name.
     * @param {?} name The form name
     * @return {?} Form definition
     */
    getFormDefinitionByName(name) {
        /** @type {?} */
        const opts = {
            'filter': 'myReusableForms',
            'filterText': name,
            'modelType': 2
        };
        return from(this.modelsApi.getModels(opts))
            .pipe(map(this.getFormId), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets the start form instance for a given process.
     * @param {?} processId Process definition ID
     * @return {?} Form definition
     */
    getStartFormInstance(processId) {
        return from(this.processApi.getProcessInstanceStartForm(processId))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets a process instance.
     * @param {?} processId ID of the process to get
     * @return {?} Process instance
     */
    getProcessInstance(processId) {
        return from(this.processApi.getProcessInstance(processId))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets the start form definition for a given process.
     * @param {?} processId Process definition ID
     * @return {?} Form definition
     */
    getStartFormDefinition(processId) {
        return from(this.processApi.getProcessDefinitionStartForm(processId))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets values of fields populated by a REST backend.
     * @param {?} taskId Task identifier
     * @param {?} field Field identifier
     * @return {?} Field values
     */
    getRestFieldValues(taskId, field) {
        return from(this.taskApi.getRestFieldValues(taskId, field))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets values of fields populated by a REST backend using a process ID.
     * @param {?} processDefinitionId Process identifier
     * @param {?} field Field identifier
     * @return {?} Field values
     */
    getRestFieldValuesByProcessId(processDefinitionId, field) {
        return from(this.processApi.getRestFieldValues(processDefinitionId, field))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets column values of fields populated by a REST backend using a process ID.
     * @param {?} processDefinitionId Process identifier
     * @param {?} field Field identifier
     * @param {?=} column Column identifier
     * @return {?} Field values
     */
    getRestFieldValuesColumnByProcessId(processDefinitionId, field, column) {
        return from(this.processApi.getRestTableFieldValues(processDefinitionId, field, column))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets column values of fields populated by a REST backend.
     * @param {?} taskId Task identifier
     * @param {?} field Field identifier
     * @param {?=} column Column identifier
     * @return {?} Field values
     */
    getRestFieldValuesColumn(taskId, field, column) {
        return from(this.taskApi.getRestFieldValuesColumn(taskId, field, column))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Returns a URL for the profile picture of a user.
     * @param {?} userId ID of the target user
     * @return {?} URL string
     */
    getUserProfileImageApi(userId) {
        return this.apiService.getInstance().activiti.userApi.getUserProfilePictureUrl(userId);
    }
    /**
     * Gets a list of workflow users.
     * @param {?} filter Filter to select specific users
     * @param {?=} groupId Group ID for the search
     * @return {?} Array of users
     */
    getWorkflowUsers(filter, groupId) {
        /** @type {?} */
        const option = { filter: filter };
        if (groupId) {
            option.groupId = groupId;
        }
        return from(this.usersWorkflowApi.getUsers(option))
            .pipe(switchMap((/**
         * @param {?} response
         * @return {?}
         */
        (response) => (/** @type {?} */ (response.data)) || [])), map((/**
         * @param {?} user
         * @return {?}
         */
        (user) => {
            user.userImage = this.getUserProfileImageApi(user.id);
            return of(user);
        })), combineAll(), defaultIfEmpty([]), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets a list of groups in a workflow.
     * @param {?} filter Filter to select specific groups
     * @param {?=} groupId Group ID for the search
     * @return {?} Array of groups
     */
    getWorkflowGroups(filter, groupId) {
        /** @type {?} */
        const option = { filter: filter };
        if (groupId) {
            option.groupId = groupId;
        }
        return from(this.groupsApi.getGroups(option))
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => (/** @type {?} */ (response.data)) || [])), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets the ID of a form.
     * @param {?} form Object representing a form
     * @return {?} ID string
     */
    getFormId(form) {
        /** @type {?} */
        let result = null;
        if (form && form.data && form.data.length > 0) {
            result = form.data[0].id;
        }
        return result;
    }
    /**
     * Creates a JSON representation of form data.
     * @param {?} res Object representing form data
     * @return {?} JSON data
     */
    toJson(res) {
        if (res) {
            return res || {};
        }
        return {};
    }
    /**
     * Creates a JSON array representation of form data.
     * @param {?} res Object representing form data
     * @return {?} JSON data
     */
    toJsonArray(res) {
        if (res) {
            return res.data || [];
        }
        return [];
    }
    /**
     * Reports an error message.
     * @param {?} error Data object with optional `message` and `status` fields for the error
     * @return {?} Error message
     */
    handleError(error) {
        /** @type {?} */
        let errMsg = FormService.UNKNOWN_ERROR_MESSAGE;
        if (error) {
            errMsg = (error.message) ? error.message :
                error.status ? `${error.status} - ${error.statusText}` : FormService.GENERIC_ERROR_MESSAGE;
        }
        this.logService.error(errMsg);
        return throwError(errMsg);
    }
}
FormService.UNKNOWN_ERROR_MESSAGE = 'Unknown error';
FormService.GENERIC_ERROR_MESSAGE = 'Server error';
FormService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
FormService.ctorParameters = () => [
    { type: EcmModelService },
    { type: AlfrescoApiService },
    { type: LogService }
];
/** @nocollapse */ FormService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function FormService_Factory() { return new FormService(i0.ɵɵinject(i1.EcmModelService), i0.ɵɵinject(i2.AlfrescoApiService), i0.ɵɵinject(i3.LogService)); }, token: FormService, providedIn: "root" });
if (false) {
    /** @type {?} */
    FormService.UNKNOWN_ERROR_MESSAGE;
    /** @type {?} */
    FormService.GENERIC_ERROR_MESSAGE;
    /** @type {?} */
    FormService.prototype.formLoaded;
    /** @type {?} */
    FormService.prototype.formDataRefreshed;
    /** @type {?} */
    FormService.prototype.formFieldValueChanged;
    /** @type {?} */
    FormService.prototype.formEvents;
    /** @type {?} */
    FormService.prototype.taskCompleted;
    /** @type {?} */
    FormService.prototype.taskCompletedError;
    /** @type {?} */
    FormService.prototype.taskSaved;
    /** @type {?} */
    FormService.prototype.taskSavedError;
    /** @type {?} */
    FormService.prototype.formContentClicked;
    /** @type {?} */
    FormService.prototype.validateForm;
    /** @type {?} */
    FormService.prototype.validateFormField;
    /** @type {?} */
    FormService.prototype.validateDynamicTableRow;
    /** @type {?} */
    FormService.prototype.executeOutcome;
    /**
     * @type {?}
     * @private
     */
    FormService.prototype.ecmModelService;
    /**
     * @type {?}
     * @private
     */
    FormService.prototype.apiService;
    /**
     * @type {?}
     * @protected
     */
    FormService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDekUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBRXhELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDakUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFHdEUsT0FBTyxFQUFFLFNBQVMsRUFBb0IsZ0JBQWdCLEVBQWMsTUFBTSxvQ0FBb0MsQ0FBQztBQUsvRyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdEQsT0FBTyxFQUFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxjQUFjLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7Ozs7QUFVeEYsTUFBTSxPQUFPLFdBQVc7Ozs7OztJQXFCcEIsWUFBb0IsZUFBZ0MsRUFDaEMsVUFBOEIsRUFDNUIsVUFBc0I7UUFGeEIsb0JBQWUsR0FBZixlQUFlLENBQWlCO1FBQ2hDLGVBQVUsR0FBVixVQUFVLENBQW9CO1FBQzVCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFsQjVDLGVBQVUsR0FBRyxJQUFJLE9BQU8sRUFBYSxDQUFDO1FBQ3RDLHNCQUFpQixHQUFHLElBQUksT0FBTyxFQUFhLENBQUM7UUFDN0MsMEJBQXFCLEdBQUcsSUFBSSxPQUFPLEVBQWtCLENBQUM7UUFDdEQsZUFBVSxHQUFHLElBQUksT0FBTyxFQUFTLENBQUM7UUFDbEMsa0JBQWEsR0FBRyxJQUFJLE9BQU8sRUFBYSxDQUFDO1FBQ3pDLHVCQUFrQixHQUFHLElBQUksT0FBTyxFQUFrQixDQUFDO1FBQ25ELGNBQVMsR0FBRyxJQUFJLE9BQU8sRUFBYSxDQUFDO1FBQ3JDLG1CQUFjLEdBQUcsSUFBSSxPQUFPLEVBQWtCLENBQUM7UUFDL0MsdUJBQWtCLEdBQUcsSUFBSSxPQUFPLEVBQW9CLENBQUM7UUFFckQsaUJBQVksR0FBRyxJQUFJLE9BQU8sRUFBcUIsQ0FBQztRQUNoRCxzQkFBaUIsR0FBRyxJQUFJLE9BQU8sRUFBMEIsQ0FBQztRQUMxRCw0QkFBdUIsR0FBRyxJQUFJLE9BQU8sRUFBZ0MsQ0FBQztRQUV0RSxtQkFBYyxHQUFHLElBQUksT0FBTyxFQUFvQixDQUFDO0lBS2pELENBQUM7Ozs7O0lBRUQsSUFBWSxPQUFPO1FBQ2YsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUM7SUFDMUQsQ0FBQzs7Ozs7SUFFRCxJQUFZLFNBQVM7UUFDakIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7SUFDNUQsQ0FBQzs7Ozs7SUFFRCxJQUFZLFNBQVM7UUFDakIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7SUFDNUQsQ0FBQzs7Ozs7SUFFRCxJQUFZLFVBQVU7UUFDbEIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUM7SUFDN0QsQ0FBQzs7Ozs7SUFFRCxJQUFZLDJCQUEyQjtRQUNuQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLDJCQUEyQixDQUFDO0lBQzlFLENBQUM7Ozs7O0lBRUQsSUFBWSxnQkFBZ0I7UUFDeEIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQztJQUNuRSxDQUFDOzs7OztJQUVELElBQVksU0FBUztRQUNqQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQztJQUM1RCxDQUFDOzs7Ozs7OztJQVNELFNBQVMsQ0FBQyxJQUFTLEVBQUUsSUFBaUIsRUFBRSxXQUFvQixLQUFLO1FBQzdELElBQUksSUFBSSxFQUFFOztrQkFDQSxJQUFJLEdBQUcsSUFBSSxTQUFTLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDO1lBQ3RELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUNkLElBQUksQ0FBQyxRQUFRLEdBQUc7b0JBQ1osSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUU7d0JBQ3ZCLEVBQUUsRUFBRSxPQUFPO3dCQUNYLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxXQUFXO3dCQUNsQyxRQUFRLEVBQUUsSUFBSTtxQkFDakIsQ0FBQztpQkFDTCxDQUFDO2FBQ0w7WUFDRCxPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQzs7Ozs7O0lBT0QsbUJBQW1CLENBQUMsUUFBZ0I7UUFDaEMsT0FBTyxJQUFJLFVBQVU7Ozs7UUFBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO1lBQy9CLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsU0FBUzs7OztZQUMvQixDQUFDLElBQUksRUFBRSxFQUFFO2dCQUNMLElBQUksQ0FBQyxlQUFlLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUzs7OztnQkFDOUUsQ0FBQyxVQUFVLEVBQUUsRUFBRTs7MEJBQ0wsbUJBQW1CLEdBQUcsSUFBSSxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLHFCQUFxQixFQUFFLElBQUksQ0FBQyxXQUFXLEVBQUUsVUFBVSxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUM7b0JBQ2xKLElBQUksQ0FDQSxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxFQUFFLG1CQUFtQixDQUFDLENBQ3hELENBQUMsU0FBUzs7OztvQkFBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO3dCQUNyQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3hCLENBQUM7Ozs7b0JBQUUsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FBQztnQkFDdkMsQ0FBQzs7OztnQkFDRCxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUFDO1lBQ3hDLENBQUM7Ozs7WUFDRCxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUFDO1FBQ3hDLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7O0lBT0QsVUFBVSxDQUFDLFFBQWdCOztjQUNqQixTQUFTLEdBQUc7WUFDZCxJQUFJLEVBQUUsUUFBUTtZQUNkLFdBQVcsRUFBRSxFQUFFO1lBQ2YsU0FBUyxFQUFFLENBQUM7WUFDWixVQUFVLEVBQUUsQ0FBQztTQUNoQjtRQUVELE9BQU8sSUFBSSxDQUNQLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxDQUN4QyxDQUFDO0lBQ04sQ0FBQzs7Ozs7OztJQVFELFFBQVEsQ0FBQyxNQUFjLEVBQUUsU0FBOEI7UUFDbkQsT0FBTyxJQUFJLENBQ1AsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUM3QyxDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBT0QsVUFBVSxDQUFDLElBQVk7O2NBQ2IsSUFBSSxHQUFHO1lBQ1QsV0FBVyxFQUFFLENBQUM7U0FDakI7UUFFRCxPQUFPLElBQUksQ0FDUCxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FDakM7YUFDSSxJQUFJLENBQ0QsR0FBRzs7OztRQUFDLFVBQVUsS0FBVTtZQUNwQixPQUFPLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSTs7OztZQUFDLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxLQUFLLElBQUksRUFBQyxDQUFDO1FBQ2pFLENBQUMsRUFBQyxFQUNGLFVBQVU7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQzs7Ozs7SUFNRCxRQUFROztjQUNFLElBQUksR0FBRztZQUNULFdBQVcsRUFBRSxDQUFDO1NBQ2pCO1FBRUQsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDdEMsSUFBSSxDQUNELEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQ3JCLFVBQVU7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQzs7Ozs7SUFNRCxxQkFBcUI7UUFDakIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUNqRCxJQUFJLENBQ0QsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsRUFDckIsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7Ozs7SUFPRCx1QkFBdUIsQ0FBQyxpQkFBeUI7UUFDN0MsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLDJCQUEyQixDQUFDLDJCQUEyQixDQUFDLGlCQUFpQixDQUFDLENBQUM7YUFDdkYsSUFBSSxDQUNELEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQ2hCLFVBQVU7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQzs7Ozs7SUFNRCxRQUFRO1FBQ0osT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDbEMsSUFBSSxDQUNELEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQ3JCLFVBQVU7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQzs7Ozs7O0lBT0QsT0FBTyxDQUFDLE1BQWM7UUFDbEIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDcEMsSUFBSSxDQUNELEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQ2hCLFVBQVU7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQzs7Ozs7OztJQVFELFlBQVksQ0FBQyxNQUFjLEVBQUUsVUFBc0I7O2NBQ3pDLHNCQUFzQixHQUFHLG1CQUF5QixFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsRUFBQTtRQUU5RSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsc0JBQXNCLENBQUMsQ0FBQzthQUNqRSxJQUFJLENBQ0QsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7Ozs7OztJQVNELGdCQUFnQixDQUFDLE1BQWMsRUFBRSxVQUFzQixFQUFFLE9BQWdCOztjQUMvRCwwQkFBMEIsR0FBUSxtQkFBNkIsRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLEVBQUE7UUFDM0YsSUFBSSxPQUFPLEVBQUU7WUFDVCwwQkFBMEIsQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1NBQ2hEO1FBRUQsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsMEJBQTBCLENBQUMsQ0FBQzthQUN6RSxJQUFJLENBQ0QsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7Ozs7SUFPRCxXQUFXLENBQUMsTUFBYztRQUN0QixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUN4QyxJQUFJLENBQ0QsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFDaEIsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7Ozs7SUFPRCxxQkFBcUIsQ0FBQyxNQUFjO1FBQ2hDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ3RDLElBQUksQ0FDRCxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUNoQixVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7Ozs7OztJQU9ELHVCQUF1QixDQUFDLElBQVk7O2NBQzFCLElBQUksR0FBRztZQUNULFFBQVEsRUFBRSxpQkFBaUI7WUFDM0IsWUFBWSxFQUFFLElBQUk7WUFDbEIsV0FBVyxFQUFFLENBQUM7U0FDakI7UUFFRCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN0QyxJQUFJLENBQ0QsR0FBRyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsRUFDbkIsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7Ozs7SUFPRCxvQkFBb0IsQ0FBQyxTQUFpQjtRQUNsQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLDJCQUEyQixDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQzlELElBQUksQ0FDRCxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUNoQixVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7Ozs7OztJQU9ELGtCQUFrQixDQUFDLFNBQWlCO1FBQ2hDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDckQsSUFBSSxDQUNELEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQ2hCLFVBQVU7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQzs7Ozs7O0lBT0Qsc0JBQXNCLENBQUMsU0FBaUI7UUFDcEMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyw2QkFBNkIsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNoRSxJQUFJLENBQ0QsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFDaEIsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7Ozs7O0lBUUQsa0JBQWtCLENBQUMsTUFBYyxFQUFFLEtBQWE7UUFDNUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7YUFDdEQsSUFBSSxDQUNELFVBQVU7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQzs7Ozs7OztJQVFELDZCQUE2QixDQUFDLG1CQUEyQixFQUFFLEtBQWE7UUFDcEUsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxtQkFBbUIsRUFBRSxLQUFLLENBQUMsQ0FBQzthQUN0RSxJQUFJLENBQ0QsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7Ozs7OztJQVNELG1DQUFtQyxDQUFDLG1CQUEyQixFQUFFLEtBQWEsRUFBRSxNQUFlO1FBQzNGLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsdUJBQXVCLENBQUMsbUJBQW1CLEVBQUUsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQ25GLElBQUksQ0FDRCxVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7Ozs7Ozs7O0lBU0Qsd0JBQXdCLENBQUMsTUFBYyxFQUFFLEtBQWEsRUFBRSxNQUFlO1FBQ25FLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsd0JBQXdCLENBQUMsTUFBTSxFQUFFLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQzthQUNwRSxJQUFJLENBQ0QsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7Ozs7SUFPRCxzQkFBc0IsQ0FBQyxNQUFjO1FBQ2pDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLHdCQUF3QixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzNGLENBQUM7Ozs7Ozs7SUFRRCxnQkFBZ0IsQ0FBQyxNQUFjLEVBQUUsT0FBZ0I7O2NBQ3ZDLE1BQU0sR0FBUSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUU7UUFDdEMsSUFBSSxPQUFPLEVBQUU7WUFDVCxNQUFNLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztTQUM1QjtRQUNELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDOUMsSUFBSSxDQUNELFNBQVM7Ozs7UUFBQyxDQUFDLFFBQWEsRUFBRSxFQUFFLENBQUMsbUJBQXFCLFFBQVEsQ0FBQyxJQUFJLEVBQUEsSUFBSSxFQUFFLEVBQUMsRUFDdEUsR0FBRzs7OztRQUFDLENBQUMsSUFBUyxFQUFFLEVBQUU7WUFDZCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDdEQsT0FBTyxFQUFFLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEIsQ0FBQyxFQUFDLEVBQ0YsVUFBVSxFQUFFLEVBQ1osY0FBYyxDQUFDLEVBQUUsQ0FBQyxFQUNsQixVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7Ozs7Ozs7SUFRRCxpQkFBaUIsQ0FBQyxNQUFjLEVBQUUsT0FBZ0I7O2NBQ3hDLE1BQU0sR0FBUSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUU7UUFDdEMsSUFBSSxPQUFPLEVBQUU7WUFDVCxNQUFNLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztTQUM1QjtRQUNELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ3hDLElBQUksQ0FDRCxHQUFHOzs7O1FBQUMsQ0FBQyxRQUFhLEVBQUUsRUFBRSxDQUFDLG1CQUFlLFFBQVEsQ0FBQyxJQUFJLEVBQUEsSUFBSSxFQUFFLEVBQUMsRUFDMUQsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7Ozs7SUFPRCxTQUFTLENBQUMsSUFBUzs7WUFDWCxNQUFNLEdBQUcsSUFBSTtRQUVqQixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUMzQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7U0FDNUI7UUFFRCxPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDOzs7Ozs7SUFPRCxNQUFNLENBQUMsR0FBUTtRQUNYLElBQUksR0FBRyxFQUFFO1lBQ0wsT0FBTyxHQUFHLElBQUksRUFBRSxDQUFDO1NBQ3BCO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDOzs7Ozs7SUFPRCxXQUFXLENBQUMsR0FBUTtRQUNoQixJQUFJLEdBQUcsRUFBRTtZQUNMLE9BQU8sR0FBRyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUM7U0FDekI7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7Ozs7OztJQU9ELFdBQVcsQ0FBQyxLQUFVOztZQUNkLE1BQU0sR0FBRyxXQUFXLENBQUMscUJBQXFCO1FBQzlDLElBQUksS0FBSyxFQUFFO1lBQ1AsTUFBTSxHQUFHLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7Z0JBQ3RDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sTUFBTSxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxxQkFBcUIsQ0FBQztTQUNsRztRQUNELElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBQzlCLE9BQU8sVUFBVSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzlCLENBQUM7O0FBN2VNLGlDQUFxQixHQUFXLGVBQWUsQ0FBQztBQUNoRCxpQ0FBcUIsR0FBVyxjQUFjLENBQUM7O1lBTnpELFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQjs7OztZQVZRLGVBQWU7WUFiZixrQkFBa0I7WUFDbEIsVUFBVTs7Ozs7SUF5QmYsa0NBQXVEOztJQUN2RCxrQ0FBc0Q7O0lBRXRELGlDQUFzQzs7SUFDdEMsd0NBQTZDOztJQUM3Qyw0Q0FBc0Q7O0lBQ3RELGlDQUFrQzs7SUFDbEMsb0NBQXlDOztJQUN6Qyx5Q0FBbUQ7O0lBQ25ELGdDQUFxQzs7SUFDckMscUNBQStDOztJQUMvQyx5Q0FBcUQ7O0lBRXJELG1DQUFnRDs7SUFDaEQsd0NBQTBEOztJQUMxRCw4Q0FBc0U7O0lBRXRFLHFDQUFpRDs7Ozs7SUFFckMsc0NBQXdDOzs7OztJQUN4QyxpQ0FBc0M7Ozs7O0lBQ3RDLGlDQUFnQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9sb2cuc2VydmljZSc7XHJcbmltcG9ydCB7IFVzZXJQcm9jZXNzTW9kZWwgfSBmcm9tICcuLi8uLi9tb2RlbHMnO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIFN1YmplY3QsIGZyb20sIG9mLCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEZvcm1EZWZpbml0aW9uTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvZm9ybS1kZWZpbml0aW9uLm1vZGVsJztcclxuaW1wb3J0IHsgQ29udGVudExpbmtNb2RlbCB9IGZyb20gJy4vLi4vY29tcG9uZW50cy93aWRnZXRzL2NvcmUvY29udGVudC1saW5rLm1vZGVsJztcclxuaW1wb3J0IHsgR3JvdXBNb2RlbCB9IGZyb20gJy4vLi4vY29tcG9uZW50cy93aWRnZXRzL2NvcmUvZ3JvdXAubW9kZWwnO1xyXG5pbXBvcnQgeyBGb3JtTW9kZWwsIEZvcm1PdXRjb21lRXZlbnQsIEZvcm1PdXRjb21lTW9kZWwsIEZvcm1WYWx1ZXMgfSBmcm9tICcuLy4uL2NvbXBvbmVudHMvd2lkZ2V0cy9jb3JlL2luZGV4JztcclxuaW1wb3J0IHtcclxuICAgIEZvcm1FcnJvckV2ZW50LCBGb3JtRXZlbnQsIEZvcm1GaWVsZEV2ZW50LFxyXG4gICAgVmFsaWRhdGVEeW5hbWljVGFibGVSb3dFdmVudCwgVmFsaWRhdGVGb3JtRXZlbnQsIFZhbGlkYXRlRm9ybUZpZWxkRXZlbnRcclxufSBmcm9tICcuLy4uL2V2ZW50cy9pbmRleCc7XHJcbmltcG9ydCB7IEVjbU1vZGVsU2VydmljZSB9IGZyb20gJy4vZWNtLW1vZGVsLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBtYXAsIGNhdGNoRXJyb3IsIHN3aXRjaE1hcCwgY29tYmluZUFsbCwgZGVmYXVsdElmRW1wdHkgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7XHJcbiAgICBBY3Rpdml0aSxcclxuICAgIENvbXBsZXRlRm9ybVJlcHJlc2VudGF0aW9uLFxyXG4gICAgU2F2ZUZvcm1SZXByZXNlbnRhdGlvblxyXG59IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb3JtU2VydmljZSB7XHJcblxyXG4gICAgc3RhdGljIFVOS05PV05fRVJST1JfTUVTU0FHRTogc3RyaW5nID0gJ1Vua25vd24gZXJyb3InO1xyXG4gICAgc3RhdGljIEdFTkVSSUNfRVJST1JfTUVTU0FHRTogc3RyaW5nID0gJ1NlcnZlciBlcnJvcic7XHJcblxyXG4gICAgZm9ybUxvYWRlZCA9IG5ldyBTdWJqZWN0PEZvcm1FdmVudD4oKTtcclxuICAgIGZvcm1EYXRhUmVmcmVzaGVkID0gbmV3IFN1YmplY3Q8Rm9ybUV2ZW50PigpO1xyXG4gICAgZm9ybUZpZWxkVmFsdWVDaGFuZ2VkID0gbmV3IFN1YmplY3Q8Rm9ybUZpZWxkRXZlbnQ+KCk7XHJcbiAgICBmb3JtRXZlbnRzID0gbmV3IFN1YmplY3Q8RXZlbnQ+KCk7XHJcbiAgICB0YXNrQ29tcGxldGVkID0gbmV3IFN1YmplY3Q8Rm9ybUV2ZW50PigpO1xyXG4gICAgdGFza0NvbXBsZXRlZEVycm9yID0gbmV3IFN1YmplY3Q8Rm9ybUVycm9yRXZlbnQ+KCk7XHJcbiAgICB0YXNrU2F2ZWQgPSBuZXcgU3ViamVjdDxGb3JtRXZlbnQ+KCk7XHJcbiAgICB0YXNrU2F2ZWRFcnJvciA9IG5ldyBTdWJqZWN0PEZvcm1FcnJvckV2ZW50PigpO1xyXG4gICAgZm9ybUNvbnRlbnRDbGlja2VkID0gbmV3IFN1YmplY3Q8Q29udGVudExpbmtNb2RlbD4oKTtcclxuXHJcbiAgICB2YWxpZGF0ZUZvcm0gPSBuZXcgU3ViamVjdDxWYWxpZGF0ZUZvcm1FdmVudD4oKTtcclxuICAgIHZhbGlkYXRlRm9ybUZpZWxkID0gbmV3IFN1YmplY3Q8VmFsaWRhdGVGb3JtRmllbGRFdmVudD4oKTtcclxuICAgIHZhbGlkYXRlRHluYW1pY1RhYmxlUm93ID0gbmV3IFN1YmplY3Q8VmFsaWRhdGVEeW5hbWljVGFibGVSb3dFdmVudD4oKTtcclxuXHJcbiAgICBleGVjdXRlT3V0Y29tZSA9IG5ldyBTdWJqZWN0PEZvcm1PdXRjb21lRXZlbnQ+KCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBlY21Nb2RlbFNlcnZpY2U6IEVjbU1vZGVsU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgYXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJvdGVjdGVkIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldCB0YXNrQXBpKCk6IEFjdGl2aXRpLlRhc2tBcGkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5hY3Rpdml0aS50YXNrQXBpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0IG1vZGVsc0FwaSgpOiBBY3Rpdml0aS5Nb2RlbHNBcGkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5hY3Rpdml0aS5tb2RlbHNBcGk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXQgZWRpdG9yQXBpKCk6IEFjdGl2aXRpLkVkaXRvckFwaSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmFjdGl2aXRpLmVkaXRvckFwaTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldCBwcm9jZXNzQXBpKCk6IEFjdGl2aXRpLlByb2Nlc3NBcGkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5hY3Rpdml0aS5wcm9jZXNzQXBpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0IHByb2Nlc3NJbnN0YW5jZVZhcmlhYmxlc0FwaSgpOiBBY3Rpdml0aS5Qcm9jZXNzSW5zdGFuY2VWYXJpYWJsZXNBcGkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5hY3Rpdml0aS5wcm9jZXNzSW5zdGFuY2VWYXJpYWJsZXNBcGk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXQgdXNlcnNXb3JrZmxvd0FwaSgpOiBBY3Rpdml0aS5Vc2Vyc1dvcmtmbG93QXBpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuYWN0aXZpdGkudXNlcnNXb3JrZmxvd0FwaTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldCBncm91cHNBcGkoKTogQWN0aXZpdGkuR3JvdXBzQXBpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuYWN0aXZpdGkuZ3JvdXBzQXBpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUGFyc2VzIEpTT04gZGF0YSB0byBjcmVhdGUgYSBjb3JyZXNwb25kaW5nIEZvcm0gbW9kZWwuXHJcbiAgICAgKiBAcGFyYW0ganNvbiBKU09OIHRvIGNyZWF0ZSB0aGUgZm9ybVxyXG4gICAgICogQHBhcmFtIGRhdGEgVmFsdWVzIGZvciB0aGUgZm9ybSBmaWVsZHNcclxuICAgICAqIEBwYXJhbSByZWFkT25seSBTaG91bGQgdGhlIGZvcm0gZmllbGRzIGJlIHJlYWQtb25seT9cclxuICAgICAqIEByZXR1cm5zIEZvcm0gbW9kZWwgY3JlYXRlZCBmcm9tIGlucHV0IGRhdGFcclxuICAgICAqL1xyXG4gICAgcGFyc2VGb3JtKGpzb246IGFueSwgZGF0YT86IEZvcm1WYWx1ZXMsIHJlYWRPbmx5OiBib29sZWFuID0gZmFsc2UpOiBGb3JtTW9kZWwge1xyXG4gICAgICAgIGlmIChqc29uKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGZvcm0gPSBuZXcgRm9ybU1vZGVsKGpzb24sIGRhdGEsIHJlYWRPbmx5LCB0aGlzKTtcclxuICAgICAgICAgICAgaWYgKCFqc29uLmZpZWxkcykge1xyXG4gICAgICAgICAgICAgICAgZm9ybS5vdXRjb21lcyA9IFtcclxuICAgICAgICAgICAgICAgICAgICBuZXcgRm9ybU91dGNvbWVNb2RlbChmb3JtLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnJHNhdmUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBGb3JtT3V0Y29tZU1vZGVsLlNBVkVfQUNUSU9OLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc1N5c3RlbTogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICBdO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBmb3JtO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZXMgYSBGb3JtIHdpdGggYSBmaWVsZCBmb3IgZWFjaCBtZXRhZGF0YSBwcm9wZXJ0eS5cclxuICAgICAqIEBwYXJhbSBmb3JtTmFtZSBOYW1lIG9mIHRoZSBuZXcgZm9ybVxyXG4gICAgICogQHJldHVybnMgVGhlIG5ldyBmb3JtXHJcbiAgICAgKi9cclxuICAgIGNyZWF0ZUZvcm1Gcm9tQU5vZGUoZm9ybU5hbWU6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKChvYnNlcnZlcikgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmNyZWF0ZUZvcm0oZm9ybU5hbWUpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIChmb3JtKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lY21Nb2RlbFNlcnZpY2Uuc2VhcmNoRWNtVHlwZShmb3JtTmFtZSwgRWNtTW9kZWxTZXJ2aWNlLk1PREVMX05BTUUpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgICAgICAgICAgKGN1c3RvbVR5cGUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGZvcm1EZWZpbml0aW9uTW9kZWwgPSBuZXcgRm9ybURlZmluaXRpb25Nb2RlbChmb3JtLmlkLCBmb3JtLm5hbWUsIGZvcm0ubGFzdFVwZGF0ZWRCeUZ1bGxOYW1lLCBmb3JtLmxhc3RVcGRhdGVkLCBjdXN0b21UeXBlLmVudHJ5LnByb3BlcnRpZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZnJvbShcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmVkaXRvckFwaS5zYXZlRm9ybShmb3JtLmlkLCBmb3JtRGVmaW5pdGlvbk1vZGVsKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgKS5zdWJzY3JpYmUoKGZvcm1EYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmb3JtRGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZSBhIEZvcm0uXHJcbiAgICAgKiBAcGFyYW0gZm9ybU5hbWUgTmFtZSBvZiB0aGUgbmV3IGZvcm1cclxuICAgICAqIEByZXR1cm5zIFRoZSBuZXcgZm9ybVxyXG4gICAgICovXHJcbiAgICBjcmVhdGVGb3JtKGZvcm1OYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IGRhdGFNb2RlbCA9IHtcclxuICAgICAgICAgICAgbmFtZTogZm9ybU5hbWUsXHJcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnJyxcclxuICAgICAgICAgICAgbW9kZWxUeXBlOiAyLFxyXG4gICAgICAgICAgICBzdGVuY2lsU2V0OiAwXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20oXHJcbiAgICAgICAgICAgIHRoaXMubW9kZWxzQXBpLmNyZWF0ZU1vZGVsKGRhdGFNb2RlbClcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2F2ZXMgYSBmb3JtLlxyXG4gICAgICogQHBhcmFtIGZvcm1JZCBJRCBvZiB0aGUgZm9ybSB0byBzYXZlXHJcbiAgICAgKiBAcGFyYW0gZm9ybU1vZGVsIE1vZGVsIGRhdGEgZm9yIHRoZSBmb3JtXHJcbiAgICAgKiBAcmV0dXJucyBEYXRhIGZvciB0aGUgc2F2ZWQgZm9ybVxyXG4gICAgICovXHJcbiAgICBzYXZlRm9ybShmb3JtSWQ6IG51bWJlciwgZm9ybU1vZGVsOiBGb3JtRGVmaW5pdGlvbk1vZGVsKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbShcclxuICAgICAgICAgICAgdGhpcy5lZGl0b3JBcGkuc2F2ZUZvcm0oZm9ybUlkLCBmb3JtTW9kZWwpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlYXJjaGVzIGZvciBhIGZvcm0gYnkgbmFtZS5cclxuICAgICAqIEBwYXJhbSBuYW1lIFRoZSBmb3JtIG5hbWUgdG8gc2VhcmNoIGZvclxyXG4gICAgICogQHJldHVybnMgRm9ybSBtb2RlbChzKSBtYXRjaGluZyB0aGUgc2VhcmNoIG5hbWVcclxuICAgICAqL1xyXG4gICAgc2VhcmNoRnJvbShuYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IG9wdHMgPSB7XHJcbiAgICAgICAgICAgICdtb2RlbFR5cGUnOiAyXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20oXHJcbiAgICAgICAgICAgIHRoaXMubW9kZWxzQXBpLmdldE1vZGVscyhvcHRzKVxyXG4gICAgICAgIClcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAoZnVuY3Rpb24gKGZvcm1zOiBhbnkpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZm9ybXMuZGF0YS5maW5kKChmb3JtRGF0YSkgPT4gZm9ybURhdGEubmFtZSA9PT0gbmFtZSk7XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhbGwgdGhlIGZvcm1zLlxyXG4gICAgICogQHJldHVybnMgTGlzdCBvZiBmb3JtIG1vZGVsc1xyXG4gICAgICovXHJcbiAgICBnZXRGb3JtcygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IG9wdHMgPSB7XHJcbiAgICAgICAgICAgICdtb2RlbFR5cGUnOiAyXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5tb2RlbHNBcGkuZ2V0TW9kZWxzKG9wdHMpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCh0aGlzLnRvSnNvbkFycmF5KSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBwcm9jZXNzIGRlZmluaXRpb25zLlxyXG4gICAgICogQHJldHVybnMgTGlzdCBvZiBwcm9jZXNzIGRlZmluaXRpb25zXHJcbiAgICAgKi9cclxuICAgIGdldFByb2Nlc3NEZWZpbml0aW9ucygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMucHJvY2Vzc0FwaS5nZXRQcm9jZXNzRGVmaW5pdGlvbnMoe30pKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCh0aGlzLnRvSnNvbkFycmF5KSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBpbnN0YW5jZSB2YXJpYWJsZXMgZm9yIGEgcHJvY2Vzcy5cclxuICAgICAqIEBwYXJhbSBwcm9jZXNzSW5zdGFuY2VJZCBJRCBvZiB0aGUgdGFyZ2V0IHByb2Nlc3NcclxuICAgICAqIEByZXR1cm5zIExpc3Qgb2YgaW5zdGFuY2UgdmFyaWFibGUgaW5mb3JtYXRpb25cclxuICAgICAqL1xyXG4gICAgZ2V0UHJvY2Vzc1ZhcmlhYmxlc0J5SWQocHJvY2Vzc0luc3RhbmNlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55W10+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLnByb2Nlc3NJbnN0YW5jZVZhcmlhYmxlc0FwaS5nZXRQcm9jZXNzSW5zdGFuY2VWYXJpYWJsZXMocHJvY2Vzc0luc3RhbmNlSWQpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCh0aGlzLnRvSnNvbiksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYWxsIHRoZSB0YXNrcy5cclxuICAgICAqIEByZXR1cm5zIExpc3Qgb2YgdGFza3NcclxuICAgICAqL1xyXG4gICAgZ2V0VGFza3MoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLnRhc2tBcGkubGlzdFRhc2tzKHt9KSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy50b0pzb25BcnJheSksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSB0YXNrLlxyXG4gICAgICogQHBhcmFtIHRhc2tJZCBUYXNrIElkXHJcbiAgICAgKiBAcmV0dXJucyBUYXNrIGluZm9cclxuICAgICAqL1xyXG4gICAgZ2V0VGFzayh0YXNrSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy50YXNrQXBpLmdldFRhc2sodGFza0lkKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy50b0pzb24pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTYXZlcyBhIHRhc2sgZm9ybS5cclxuICAgICAqIEBwYXJhbSB0YXNrSWQgVGFzayBJZFxyXG4gICAgICogQHBhcmFtIGZvcm1WYWx1ZXMgRm9ybSBWYWx1ZXNcclxuICAgICAqIEByZXR1cm5zIE51bGwgcmVzcG9uc2Ugd2hlbiB0aGUgb3BlcmF0aW9uIGlzIGNvbXBsZXRlXHJcbiAgICAgKi9cclxuICAgIHNhdmVUYXNrRm9ybSh0YXNrSWQ6IHN0cmluZywgZm9ybVZhbHVlczogRm9ybVZhbHVlcyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3Qgc2F2ZUZvcm1SZXByZXNlbnRhdGlvbiA9IDxTYXZlRm9ybVJlcHJlc2VudGF0aW9uPiB7IHZhbHVlczogZm9ybVZhbHVlcyB9O1xyXG5cclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLnRhc2tBcGkuc2F2ZVRhc2tGb3JtKHRhc2tJZCwgc2F2ZUZvcm1SZXByZXNlbnRhdGlvbikpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb21wbGV0ZXMgYSBUYXNrIEZvcm0uXHJcbiAgICAgKiBAcGFyYW0gdGFza0lkIFRhc2sgSWRcclxuICAgICAqIEBwYXJhbSBmb3JtVmFsdWVzIEZvcm0gVmFsdWVzXHJcbiAgICAgKiBAcGFyYW0gb3V0Y29tZSBGb3JtIE91dGNvbWVcclxuICAgICAqIEByZXR1cm5zIE51bGwgcmVzcG9uc2Ugd2hlbiB0aGUgb3BlcmF0aW9uIGlzIGNvbXBsZXRlXHJcbiAgICAgKi9cclxuICAgIGNvbXBsZXRlVGFza0Zvcm0odGFza0lkOiBzdHJpbmcsIGZvcm1WYWx1ZXM6IEZvcm1WYWx1ZXMsIG91dGNvbWU/OiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IGNvbXBsZXRlRm9ybVJlcHJlc2VudGF0aW9uOiBhbnkgPSA8Q29tcGxldGVGb3JtUmVwcmVzZW50YXRpb24+IHsgdmFsdWVzOiBmb3JtVmFsdWVzIH07XHJcbiAgICAgICAgaWYgKG91dGNvbWUpIHtcclxuICAgICAgICAgICAgY29tcGxldGVGb3JtUmVwcmVzZW50YXRpb24ub3V0Y29tZSA9IG91dGNvbWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLnRhc2tBcGkuY29tcGxldGVUYXNrRm9ybSh0YXNrSWQsIGNvbXBsZXRlRm9ybVJlcHJlc2VudGF0aW9uKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBmb3JtIHJlbGF0ZWQgdG8gYSB0YXNrLlxyXG4gICAgICogQHBhcmFtIHRhc2tJZCBJRCBvZiB0aGUgdGFyZ2V0IHRhc2tcclxuICAgICAqIEByZXR1cm5zIEZvcm0gZGVmaW5pdGlvblxyXG4gICAgICovXHJcbiAgICBnZXRUYXNrRm9ybSh0YXNrSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy50YXNrQXBpLmdldFRhc2tGb3JtKHRhc2tJZCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKHRoaXMudG9Kc29uKSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhIGZvcm0gZGVmaW5pdGlvbi5cclxuICAgICAqIEBwYXJhbSBmb3JtSWQgSUQgb2YgdGhlIHRhcmdldCBmb3JtXHJcbiAgICAgKiBAcmV0dXJucyBGb3JtIGRlZmluaXRpb25cclxuICAgICAqL1xyXG4gICAgZ2V0Rm9ybURlZmluaXRpb25CeUlkKGZvcm1JZDogbnVtYmVyKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmVkaXRvckFwaS5nZXRGb3JtKGZvcm1JZCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKHRoaXMudG9Kc29uKSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgZm9ybSBkZWZpbml0aW9uIHdpdGggYSBnaXZlbiBuYW1lLlxyXG4gICAgICogQHBhcmFtIG5hbWUgVGhlIGZvcm0gbmFtZVxyXG4gICAgICogQHJldHVybnMgRm9ybSBkZWZpbml0aW9uXHJcbiAgICAgKi9cclxuICAgIGdldEZvcm1EZWZpbml0aW9uQnlOYW1lKG5hbWU6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3Qgb3B0cyA9IHtcclxuICAgICAgICAgICAgJ2ZpbHRlcic6ICdteVJldXNhYmxlRm9ybXMnLFxyXG4gICAgICAgICAgICAnZmlsdGVyVGV4dCc6IG5hbWUsXHJcbiAgICAgICAgICAgICdtb2RlbFR5cGUnOiAyXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5tb2RlbHNBcGkuZ2V0TW9kZWxzKG9wdHMpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCh0aGlzLmdldEZvcm1JZCksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIHN0YXJ0IGZvcm0gaW5zdGFuY2UgZm9yIGEgZ2l2ZW4gcHJvY2Vzcy5cclxuICAgICAqIEBwYXJhbSBwcm9jZXNzSWQgUHJvY2VzcyBkZWZpbml0aW9uIElEXHJcbiAgICAgKiBAcmV0dXJucyBGb3JtIGRlZmluaXRpb25cclxuICAgICAqL1xyXG4gICAgZ2V0U3RhcnRGb3JtSW5zdGFuY2UocHJvY2Vzc0lkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMucHJvY2Vzc0FwaS5nZXRQcm9jZXNzSW5zdGFuY2VTdGFydEZvcm0ocHJvY2Vzc0lkKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy50b0pzb24pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgcHJvY2VzcyBpbnN0YW5jZS5cclxuICAgICAqIEBwYXJhbSBwcm9jZXNzSWQgSUQgb2YgdGhlIHByb2Nlc3MgdG8gZ2V0XHJcbiAgICAgKiBAcmV0dXJucyBQcm9jZXNzIGluc3RhbmNlXHJcbiAgICAgKi9cclxuICAgIGdldFByb2Nlc3NJbnN0YW5jZShwcm9jZXNzSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5wcm9jZXNzQXBpLmdldFByb2Nlc3NJbnN0YW5jZShwcm9jZXNzSWQpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCh0aGlzLnRvSnNvbiksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIHN0YXJ0IGZvcm0gZGVmaW5pdGlvbiBmb3IgYSBnaXZlbiBwcm9jZXNzLlxyXG4gICAgICogQHBhcmFtIHByb2Nlc3NJZCBQcm9jZXNzIGRlZmluaXRpb24gSURcclxuICAgICAqIEByZXR1cm5zIEZvcm0gZGVmaW5pdGlvblxyXG4gICAgICovXHJcbiAgICBnZXRTdGFydEZvcm1EZWZpbml0aW9uKHByb2Nlc3NJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLnByb2Nlc3NBcGkuZ2V0UHJvY2Vzc0RlZmluaXRpb25TdGFydEZvcm0ocHJvY2Vzc0lkKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy50b0pzb24pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHZhbHVlcyBvZiBmaWVsZHMgcG9wdWxhdGVkIGJ5IGEgUkVTVCBiYWNrZW5kLlxyXG4gICAgICogQHBhcmFtIHRhc2tJZCBUYXNrIGlkZW50aWZpZXJcclxuICAgICAqIEBwYXJhbSBmaWVsZCBGaWVsZCBpZGVudGlmaWVyXHJcbiAgICAgKiBAcmV0dXJucyBGaWVsZCB2YWx1ZXNcclxuICAgICAqL1xyXG4gICAgZ2V0UmVzdEZpZWxkVmFsdWVzKHRhc2tJZDogc3RyaW5nLCBmaWVsZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLnRhc2tBcGkuZ2V0UmVzdEZpZWxkVmFsdWVzKHRhc2tJZCwgZmllbGQpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB2YWx1ZXMgb2YgZmllbGRzIHBvcHVsYXRlZCBieSBhIFJFU1QgYmFja2VuZCB1c2luZyBhIHByb2Nlc3MgSUQuXHJcbiAgICAgKiBAcGFyYW0gcHJvY2Vzc0RlZmluaXRpb25JZCBQcm9jZXNzIGlkZW50aWZpZXJcclxuICAgICAqIEBwYXJhbSBmaWVsZCBGaWVsZCBpZGVudGlmaWVyXHJcbiAgICAgKiBAcmV0dXJucyBGaWVsZCB2YWx1ZXNcclxuICAgICAqL1xyXG4gICAgZ2V0UmVzdEZpZWxkVmFsdWVzQnlQcm9jZXNzSWQocHJvY2Vzc0RlZmluaXRpb25JZDogc3RyaW5nLCBmaWVsZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLnByb2Nlc3NBcGkuZ2V0UmVzdEZpZWxkVmFsdWVzKHByb2Nlc3NEZWZpbml0aW9uSWQsIGZpZWxkKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgY29sdW1uIHZhbHVlcyBvZiBmaWVsZHMgcG9wdWxhdGVkIGJ5IGEgUkVTVCBiYWNrZW5kIHVzaW5nIGEgcHJvY2VzcyBJRC5cclxuICAgICAqIEBwYXJhbSBwcm9jZXNzRGVmaW5pdGlvbklkIFByb2Nlc3MgaWRlbnRpZmllclxyXG4gICAgICogQHBhcmFtIGZpZWxkIEZpZWxkIGlkZW50aWZpZXJcclxuICAgICAqIEBwYXJhbSBjb2x1bW4gQ29sdW1uIGlkZW50aWZpZXJcclxuICAgICAqIEByZXR1cm5zIEZpZWxkIHZhbHVlc1xyXG4gICAgICovXHJcbiAgICBnZXRSZXN0RmllbGRWYWx1ZXNDb2x1bW5CeVByb2Nlc3NJZChwcm9jZXNzRGVmaW5pdGlvbklkOiBzdHJpbmcsIGZpZWxkOiBzdHJpbmcsIGNvbHVtbj86IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5wcm9jZXNzQXBpLmdldFJlc3RUYWJsZUZpZWxkVmFsdWVzKHByb2Nlc3NEZWZpbml0aW9uSWQsIGZpZWxkLCBjb2x1bW4pKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBjb2x1bW4gdmFsdWVzIG9mIGZpZWxkcyBwb3B1bGF0ZWQgYnkgYSBSRVNUIGJhY2tlbmQuXHJcbiAgICAgKiBAcGFyYW0gdGFza0lkIFRhc2sgaWRlbnRpZmllclxyXG4gICAgICogQHBhcmFtIGZpZWxkIEZpZWxkIGlkZW50aWZpZXJcclxuICAgICAqIEBwYXJhbSBjb2x1bW4gQ29sdW1uIGlkZW50aWZpZXJcclxuICAgICAqIEByZXR1cm5zIEZpZWxkIHZhbHVlc1xyXG4gICAgICovXHJcbiAgICBnZXRSZXN0RmllbGRWYWx1ZXNDb2x1bW4odGFza0lkOiBzdHJpbmcsIGZpZWxkOiBzdHJpbmcsIGNvbHVtbj86IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy50YXNrQXBpLmdldFJlc3RGaWVsZFZhbHVlc0NvbHVtbih0YXNrSWQsIGZpZWxkLCBjb2x1bW4pKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJucyBhIFVSTCBmb3IgdGhlIHByb2ZpbGUgcGljdHVyZSBvZiBhIHVzZXIuXHJcbiAgICAgKiBAcGFyYW0gdXNlcklkIElEIG9mIHRoZSB0YXJnZXQgdXNlclxyXG4gICAgICogQHJldHVybnMgVVJMIHN0cmluZ1xyXG4gICAgICovXHJcbiAgICBnZXRVc2VyUHJvZmlsZUltYWdlQXBpKHVzZXJJZDogbnVtYmVyKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuYWN0aXZpdGkudXNlckFwaS5nZXRVc2VyUHJvZmlsZVBpY3R1cmVVcmwodXNlcklkKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBsaXN0IG9mIHdvcmtmbG93IHVzZXJzLlxyXG4gICAgICogQHBhcmFtIGZpbHRlciBGaWx0ZXIgdG8gc2VsZWN0IHNwZWNpZmljIHVzZXJzXHJcbiAgICAgKiBAcGFyYW0gZ3JvdXBJZCBHcm91cCBJRCBmb3IgdGhlIHNlYXJjaFxyXG4gICAgICogQHJldHVybnMgQXJyYXkgb2YgdXNlcnNcclxuICAgICAqL1xyXG4gICAgZ2V0V29ya2Zsb3dVc2VycyhmaWx0ZXI6IHN0cmluZywgZ3JvdXBJZD86IHN0cmluZyk6IE9ic2VydmFibGU8VXNlclByb2Nlc3NNb2RlbFtdPiB7XHJcbiAgICAgICAgY29uc3Qgb3B0aW9uOiBhbnkgPSB7IGZpbHRlcjogZmlsdGVyIH07XHJcbiAgICAgICAgaWYgKGdyb3VwSWQpIHtcclxuICAgICAgICAgICAgb3B0aW9uLmdyb3VwSWQgPSBncm91cElkO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLnVzZXJzV29ya2Zsb3dBcGkuZ2V0VXNlcnMob3B0aW9uKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBzd2l0Y2hNYXAoKHJlc3BvbnNlOiBhbnkpID0+IDxVc2VyUHJvY2Vzc01vZGVsW10+IHJlc3BvbnNlLmRhdGEgfHwgW10pLFxyXG4gICAgICAgICAgICAgICAgbWFwKCh1c2VyOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB1c2VyLnVzZXJJbWFnZSA9IHRoaXMuZ2V0VXNlclByb2ZpbGVJbWFnZUFwaSh1c2VyLmlkKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gb2YodXNlcik7XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIGNvbWJpbmVBbGwoKSxcclxuICAgICAgICAgICAgICAgIGRlZmF1bHRJZkVtcHR5KFtdKSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhIGxpc3Qgb2YgZ3JvdXBzIGluIGEgd29ya2Zsb3cuXHJcbiAgICAgKiBAcGFyYW0gZmlsdGVyIEZpbHRlciB0byBzZWxlY3Qgc3BlY2lmaWMgZ3JvdXBzXHJcbiAgICAgKiBAcGFyYW0gZ3JvdXBJZCBHcm91cCBJRCBmb3IgdGhlIHNlYXJjaFxyXG4gICAgICogQHJldHVybnMgQXJyYXkgb2YgZ3JvdXBzXHJcbiAgICAgKi9cclxuICAgIGdldFdvcmtmbG93R3JvdXBzKGZpbHRlcjogc3RyaW5nLCBncm91cElkPzogc3RyaW5nKTogT2JzZXJ2YWJsZTxHcm91cE1vZGVsW10+IHtcclxuICAgICAgICBjb25zdCBvcHRpb246IGFueSA9IHsgZmlsdGVyOiBmaWx0ZXIgfTtcclxuICAgICAgICBpZiAoZ3JvdXBJZCkge1xyXG4gICAgICAgICAgICBvcHRpb24uZ3JvdXBJZCA9IGdyb3VwSWQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuZ3JvdXBzQXBpLmdldEdyb3VwcyhvcHRpb24pKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCgocmVzcG9uc2U6IGFueSkgPT4gPEdyb3VwTW9kZWxbXT4gcmVzcG9uc2UuZGF0YSB8fCBbXSksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIElEIG9mIGEgZm9ybS5cclxuICAgICAqIEBwYXJhbSBmb3JtIE9iamVjdCByZXByZXNlbnRpbmcgYSBmb3JtXHJcbiAgICAgKiBAcmV0dXJucyBJRCBzdHJpbmdcclxuICAgICAqL1xyXG4gICAgZ2V0Rm9ybUlkKGZvcm06IGFueSk6IHN0cmluZyB7XHJcbiAgICAgICAgbGV0IHJlc3VsdCA9IG51bGw7XHJcblxyXG4gICAgICAgIGlmIChmb3JtICYmIGZvcm0uZGF0YSAmJiBmb3JtLmRhdGEubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICByZXN1bHQgPSBmb3JtLmRhdGFbMF0uaWQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBhIEpTT04gcmVwcmVzZW50YXRpb24gb2YgZm9ybSBkYXRhLlxyXG4gICAgICogQHBhcmFtIHJlcyBPYmplY3QgcmVwcmVzZW50aW5nIGZvcm0gZGF0YVxyXG4gICAgICogQHJldHVybnMgSlNPTiBkYXRhXHJcbiAgICAgKi9cclxuICAgIHRvSnNvbihyZXM6IGFueSkge1xyXG4gICAgICAgIGlmIChyZXMpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHJlcyB8fCB7fTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHt9O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBhIEpTT04gYXJyYXkgcmVwcmVzZW50YXRpb24gb2YgZm9ybSBkYXRhLlxyXG4gICAgICogQHBhcmFtIHJlcyBPYmplY3QgcmVwcmVzZW50aW5nIGZvcm0gZGF0YVxyXG4gICAgICogQHJldHVybnMgSlNPTiBkYXRhXHJcbiAgICAgKi9cclxuICAgIHRvSnNvbkFycmF5KHJlczogYW55KSB7XHJcbiAgICAgICAgaWYgKHJlcykge1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzLmRhdGEgfHwgW107XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBbXTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlcG9ydHMgYW4gZXJyb3IgbWVzc2FnZS5cclxuICAgICAqIEBwYXJhbSBlcnJvciBEYXRhIG9iamVjdCB3aXRoIG9wdGlvbmFsIGBtZXNzYWdlYCBhbmQgYHN0YXR1c2AgZmllbGRzIGZvciB0aGUgZXJyb3JcclxuICAgICAqIEByZXR1cm5zIEVycm9yIG1lc3NhZ2VcclxuICAgICAqL1xyXG4gICAgaGFuZGxlRXJyb3IoZXJyb3I6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgbGV0IGVyck1zZyA9IEZvcm1TZXJ2aWNlLlVOS05PV05fRVJST1JfTUVTU0FHRTtcclxuICAgICAgICBpZiAoZXJyb3IpIHtcclxuICAgICAgICAgICAgZXJyTXNnID0gKGVycm9yLm1lc3NhZ2UpID8gZXJyb3IubWVzc2FnZSA6XHJcbiAgICAgICAgICAgICAgICBlcnJvci5zdGF0dXMgPyBgJHtlcnJvci5zdGF0dXN9IC0gJHtlcnJvci5zdGF0dXNUZXh0fWAgOiBGb3JtU2VydmljZS5HRU5FUklDX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJNc2cpO1xyXG4gICAgICAgIHJldHVybiB0aHJvd0Vycm9yKGVyck1zZyk7XHJcbiAgICB9XHJcbn1cclxuIl19