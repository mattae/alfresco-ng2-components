/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import { Injectable } from '@angular/core';
import { Observable, from, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "../../services/alfresco-api.service";
import * as i2 from "../../services/log.service";
export class ProcessContentService {
    /**
     * @param {?} apiService
     * @param {?} logService
     */
    constructor(apiService, logService) {
        this.apiService = apiService;
        this.logService = logService;
    }
    /**
     * @private
     * @return {?}
     */
    get contentApi() {
        return this.apiService.getInstance().activiti.contentApi;
    }
    /**
     * Create temporary related content from an uploaded file.
     * @param {?} file File to use for content
     * @return {?} The created content data
     */
    createTemporaryRawRelatedContent(file) {
        return from(this.contentApi.createTemporaryRawRelatedContent(file))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets the metadata for a related content item.
     * @param {?} contentId ID of the content item
     * @return {?} Metadata for the content
     */
    getFileContent(contentId) {
        return from(this.contentApi.getContent(contentId))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets raw binary content data for a related content file.
     * @param {?} contentId ID of the related content
     * @return {?} Binary data of the related content
     */
    getFileRawContent(contentId) {
        return from(this.contentApi.getRawContent(contentId))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets the preview for a related content file.
     * @param {?} contentId ID of the related content
     * @return {?} Binary data of the content preview
     */
    getContentPreview(contentId) {
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        (observer) => {
            this.contentApi.getContentPreview(contentId).then((/**
             * @param {?} result
             * @return {?}
             */
            (result) => {
                observer.next(result);
                observer.complete();
            }), (/**
             * @return {?}
             */
            () => {
                this.contentApi.getRawContent(contentId).then((/**
                 * @param {?} data
                 * @return {?}
                 */
                (data) => {
                    observer.next(data);
                    observer.complete();
                }), (/**
                 * @param {?} err
                 * @return {?}
                 */
                (err) => {
                    observer.error(err);
                    observer.complete();
                }));
            }));
        }));
    }
    /**
     * Gets a URL for direct access to a related content file.
     * @param {?} contentId ID of the related content
     * @return {?} URL to access the content
     */
    getFileRawContentUrl(contentId) {
        return this.contentApi.getRawContentUrl(contentId);
    }
    /**
     * Gets the thumbnail for a related content file.
     * @param {?} contentId ID of the related content
     * @return {?} Binary data of the thumbnail image
     */
    getContentThumbnail(contentId) {
        return from(this.contentApi.getContentThumbnail(contentId))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets related content items for a task instance.
     * @param {?} taskId ID of the target task
     * @param {?=} opts Options supported by JS-API
     * @return {?} Metadata for the content
     */
    getTaskRelatedContent(taskId, opts) {
        return from(this.contentApi.getRelatedContentForTask(taskId, opts))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets related content items for a process instance.
     * @param {?} processId ID of the target process
     * @param {?=} opts Options supported by JS-API
     * @return {?} Metadata for the content
     */
    getProcessRelatedContent(processId, opts) {
        return from(this.contentApi.getRelatedContentForProcessInstance(processId, opts))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Deletes related content.
     * @param {?} contentId Identifier of the content to delete
     * @return {?} Null response that notifies when the deletion is complete
     */
    deleteRelatedContent(contentId) {
        return from(this.contentApi.deleteContent(contentId))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Associates an uploaded file with a process instance.
     * @param {?} processInstanceId ID of the target process instance
     * @param {?} content File to associate
     * @param {?=} opts Options supported by JS-API
     * @return {?} Details of created content
     */
    createProcessRelatedContent(processInstanceId, content, opts) {
        return from(this.contentApi.createRelatedContentOnProcessInstance(processInstanceId, content, opts))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Associates an uploaded file with a task instance.
     * @param {?} taskId ID of the target task
     * @param {?} file File to associate
     * @param {?=} opts Options supported by JS-API
     * @return {?} Details of created content
     */
    createTaskRelatedContent(taskId, file, opts) {
        return from(this.contentApi.createRelatedContentOnTask(taskId, file, opts))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Creates a JSON representation of data.
     * @param {?} res Object representing data
     * @return {?} JSON object
     */
    toJson(res) {
        if (res) {
            return res || {};
        }
        return {};
    }
    /**
     * Creates a JSON array representation of data.
     * @param {?} res Object representing data
     * @return {?} JSON array object
     */
    toJsonArray(res) {
        if (res) {
            return res.data || [];
        }
        return [];
    }
    /**
     * Reports an error message.
     * @param {?} error Data object with optional `message` and `status` fields for the error
     * @return {?} Callback when an error occurs
     */
    handleError(error) {
        /** @type {?} */
        let errMsg = ProcessContentService.UNKNOWN_ERROR_MESSAGE;
        if (error) {
            errMsg = (error.message) ? error.message :
                error.status ? `${error.status} - ${error.statusText}` : ProcessContentService.GENERIC_ERROR_MESSAGE;
        }
        this.logService.error(errMsg);
        return throwError(errMsg);
    }
}
ProcessContentService.UNKNOWN_ERROR_MESSAGE = 'Unknown error';
ProcessContentService.GENERIC_ERROR_MESSAGE = 'Server error';
ProcessContentService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
ProcessContentService.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: LogService }
];
/** @nocollapse */ ProcessContentService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ProcessContentService_Factory() { return new ProcessContentService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: ProcessContentService, providedIn: "root" });
if (false) {
    /** @type {?} */
    ProcessContentService.UNKNOWN_ERROR_MESSAGE;
    /** @type {?} */
    ProcessContentService.GENERIC_ERROR_MESSAGE;
    /**
     * @type {?}
     * @private
     */
    ProcessContentService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    ProcessContentService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvY2Vzcy1jb250ZW50LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL3NlcnZpY2VzL3Byb2Nlc3MtY29udGVudC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ3pFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNwRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7QUFLNUMsTUFBTSxPQUFPLHFCQUFxQjs7Ozs7SUFLOUIsWUFBb0IsVUFBOEIsRUFDOUIsVUFBc0I7UUFEdEIsZUFBVSxHQUFWLFVBQVUsQ0FBb0I7UUFDOUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUMxQyxDQUFDOzs7OztJQUVELElBQVksVUFBVTtRQUNsQixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQztJQUM3RCxDQUFDOzs7Ozs7SUFPRCxnQ0FBZ0MsQ0FBQyxJQUFTO1FBQ3RDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsZ0NBQWdDLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDOUQsSUFBSSxDQUFDLFVBQVU7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUFDLENBQUM7SUFDMUQsQ0FBQzs7Ozs7O0lBT0QsY0FBYyxDQUFDLFNBQWlCO1FBQzVCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQzdDLElBQUksQ0FBQyxVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FBQyxDQUFDO0lBQzFELENBQUM7Ozs7OztJQU9ELGlCQUFpQixDQUFDLFNBQWlCO1FBQy9CLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ2hELElBQUksQ0FBQyxVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FBQyxDQUFDO0lBQzFELENBQUM7Ozs7OztJQU9ELGlCQUFpQixDQUFDLFNBQWlCO1FBQy9CLE9BQU8sSUFBSSxVQUFVOzs7O1FBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtZQUMvQixJQUFJLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUk7Ozs7WUFDN0MsQ0FBQyxNQUFNLEVBQUUsRUFBRTtnQkFDUCxRQUFRLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dCQUN0QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQzs7O1lBQ0QsR0FBRyxFQUFFO2dCQUNELElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUk7Ozs7Z0JBQ3pDLENBQUMsSUFBSSxFQUFFLEVBQUU7b0JBQ0wsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDOzs7O2dCQUNELENBQUMsR0FBRyxFQUFFLEVBQUU7b0JBQ0osUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztvQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2dCQUN4QixDQUFDLEVBQ0osQ0FBQztZQUNOLENBQUMsRUFDSixDQUFDO1FBQ04sQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFPRCxvQkFBb0IsQ0FBQyxTQUFpQjtRQUNsQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDdkQsQ0FBQzs7Ozs7O0lBT0QsbUJBQW1CLENBQUMsU0FBaUI7UUFDakMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUN0RCxJQUFJLENBQUMsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQUMsQ0FBQztJQUMxRCxDQUFDOzs7Ozs7O0lBUUQscUJBQXFCLENBQUMsTUFBYyxFQUFFLElBQVU7UUFDNUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDOUQsSUFBSSxDQUFDLFVBQVU7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUFDLENBQUM7SUFDMUQsQ0FBQzs7Ozs7OztJQVFELHdCQUF3QixDQUFDLFNBQWlCLEVBQUUsSUFBVTtRQUNsRCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLG1DQUFtQyxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsQ0FBQzthQUM1RSxJQUFJLENBQUMsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQUMsQ0FBQztJQUMxRCxDQUFDOzs7Ozs7SUFPRCxvQkFBb0IsQ0FBQyxTQUFpQjtRQUNsQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNoRCxJQUFJLENBQUMsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQUMsQ0FBQztJQUMxRCxDQUFDOzs7Ozs7OztJQVNELDJCQUEyQixDQUFDLGlCQUF5QixFQUFFLE9BQVksRUFBRSxJQUFVO1FBQzNFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMscUNBQXFDLENBQUMsaUJBQWlCLEVBQUUsT0FBTyxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQy9GLElBQUksQ0FBQyxVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FBQyxDQUFDO0lBQzFELENBQUM7Ozs7Ozs7O0lBU0Qsd0JBQXdCLENBQUMsTUFBYyxFQUFFLElBQVMsRUFBRSxJQUFVO1FBQzFELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsMEJBQTBCLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQzthQUN0RSxJQUFJLENBQUMsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQUMsQ0FBQztJQUMxRCxDQUFDOzs7Ozs7SUFPRCxNQUFNLENBQUMsR0FBUTtRQUNYLElBQUksR0FBRyxFQUFFO1lBQ0wsT0FBTyxHQUFHLElBQUksRUFBRSxDQUFDO1NBQ3BCO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDOzs7Ozs7SUFPRCxXQUFXLENBQUMsR0FBUTtRQUNoQixJQUFJLEdBQUcsRUFBRTtZQUNMLE9BQU8sR0FBRyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUM7U0FDekI7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7Ozs7OztJQU9ELFdBQVcsQ0FBQyxLQUFVOztZQUNkLE1BQU0sR0FBRyxxQkFBcUIsQ0FBQyxxQkFBcUI7UUFDeEQsSUFBSSxLQUFLLEVBQUU7WUFDUCxNQUFNLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdEMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxNQUFNLEtBQUssQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUMscUJBQXFCLENBQUM7U0FDNUc7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5QixPQUFPLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM5QixDQUFDOztBQXJMTSwyQ0FBcUIsR0FBVyxlQUFlLENBQUM7QUFDaEQsMkNBQXFCLEdBQVcsY0FBYyxDQUFDOztZQU56RCxVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFUUSxrQkFBa0I7WUFDbEIsVUFBVTs7Ozs7SUFXZiw0Q0FBdUQ7O0lBQ3ZELDRDQUFzRDs7Ozs7SUFFMUMsMkNBQXNDOzs7OztJQUN0QywyQ0FBOEIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMb2dTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbG9nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJlbGF0ZWRDb250ZW50UmVwcmVzZW50YXRpb24gfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgZnJvbSwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBQcm9jZXNzQ29udGVudFNlcnZpY2Uge1xyXG5cclxuICAgIHN0YXRpYyBVTktOT1dOX0VSUk9SX01FU1NBR0U6IHN0cmluZyA9ICdVbmtub3duIGVycm9yJztcclxuICAgIHN0YXRpYyBHRU5FUklDX0VSUk9SX01FU1NBR0U6IHN0cmluZyA9ICdTZXJ2ZXIgZXJyb3InO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXQgY29udGVudEFwaSgpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5hY3Rpdml0aS5jb250ZW50QXBpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlIHRlbXBvcmFyeSByZWxhdGVkIGNvbnRlbnQgZnJvbSBhbiB1cGxvYWRlZCBmaWxlLlxyXG4gICAgICogQHBhcmFtIGZpbGUgRmlsZSB0byB1c2UgZm9yIGNvbnRlbnRcclxuICAgICAqIEByZXR1cm5zIFRoZSBjcmVhdGVkIGNvbnRlbnQgZGF0YVxyXG4gICAgICovXHJcbiAgICBjcmVhdGVUZW1wb3JhcnlSYXdSZWxhdGVkQ29udGVudChmaWxlOiBhbnkpOiBPYnNlcnZhYmxlPFJlbGF0ZWRDb250ZW50UmVwcmVzZW50YXRpb24+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmNvbnRlbnRBcGkuY3JlYXRlVGVtcG9yYXJ5UmF3UmVsYXRlZENvbnRlbnQoZmlsZSkpXHJcbiAgICAgICAgICAgIC5waXBlKGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBtZXRhZGF0YSBmb3IgYSByZWxhdGVkIGNvbnRlbnQgaXRlbS5cclxuICAgICAqIEBwYXJhbSBjb250ZW50SWQgSUQgb2YgdGhlIGNvbnRlbnQgaXRlbVxyXG4gICAgICogQHJldHVybnMgTWV0YWRhdGEgZm9yIHRoZSBjb250ZW50XHJcbiAgICAgKi9cclxuICAgIGdldEZpbGVDb250ZW50KGNvbnRlbnRJZDogbnVtYmVyKTogT2JzZXJ2YWJsZTxSZWxhdGVkQ29udGVudFJlcHJlc2VudGF0aW9uPiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5jb250ZW50QXBpLmdldENvbnRlbnQoY29udGVudElkKSlcclxuICAgICAgICAgICAgLnBpcGUoY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgcmF3IGJpbmFyeSBjb250ZW50IGRhdGEgZm9yIGEgcmVsYXRlZCBjb250ZW50IGZpbGUuXHJcbiAgICAgKiBAcGFyYW0gY29udGVudElkIElEIG9mIHRoZSByZWxhdGVkIGNvbnRlbnRcclxuICAgICAqIEByZXR1cm5zIEJpbmFyeSBkYXRhIG9mIHRoZSByZWxhdGVkIGNvbnRlbnRcclxuICAgICAqL1xyXG4gICAgZ2V0RmlsZVJhd0NvbnRlbnQoY29udGVudElkOiBudW1iZXIpOiBPYnNlcnZhYmxlPEJsb2I+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmNvbnRlbnRBcGkuZ2V0UmF3Q29udGVudChjb250ZW50SWQpKVxyXG4gICAgICAgICAgICAucGlwZShjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgcHJldmlldyBmb3IgYSByZWxhdGVkIGNvbnRlbnQgZmlsZS5cclxuICAgICAqIEBwYXJhbSBjb250ZW50SWQgSUQgb2YgdGhlIHJlbGF0ZWQgY29udGVudFxyXG4gICAgICogQHJldHVybnMgQmluYXJ5IGRhdGEgb2YgdGhlIGNvbnRlbnQgcHJldmlld1xyXG4gICAgICovXHJcbiAgICBnZXRDb250ZW50UHJldmlldyhjb250ZW50SWQ6IG51bWJlcik6IE9ic2VydmFibGU8QmxvYj4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZSgob2JzZXJ2ZXIpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5jb250ZW50QXBpLmdldENvbnRlbnRQcmV2aWV3KGNvbnRlbnRJZCkudGhlbihcclxuICAgICAgICAgICAgICAgIChyZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHJlc3VsdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb250ZW50QXBpLmdldFJhd0NvbnRlbnQoY29udGVudElkKS50aGVuKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChkYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChlcnIpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKGVycik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBVUkwgZm9yIGRpcmVjdCBhY2Nlc3MgdG8gYSByZWxhdGVkIGNvbnRlbnQgZmlsZS5cclxuICAgICAqIEBwYXJhbSBjb250ZW50SWQgSUQgb2YgdGhlIHJlbGF0ZWQgY29udGVudFxyXG4gICAgICogQHJldHVybnMgVVJMIHRvIGFjY2VzcyB0aGUgY29udGVudFxyXG4gICAgICovXHJcbiAgICBnZXRGaWxlUmF3Q29udGVudFVybChjb250ZW50SWQ6IG51bWJlcik6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY29udGVudEFwaS5nZXRSYXdDb250ZW50VXJsKGNvbnRlbnRJZCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSB0aHVtYm5haWwgZm9yIGEgcmVsYXRlZCBjb250ZW50IGZpbGUuXHJcbiAgICAgKiBAcGFyYW0gY29udGVudElkIElEIG9mIHRoZSByZWxhdGVkIGNvbnRlbnRcclxuICAgICAqIEByZXR1cm5zIEJpbmFyeSBkYXRhIG9mIHRoZSB0aHVtYm5haWwgaW1hZ2VcclxuICAgICAqL1xyXG4gICAgZ2V0Q29udGVudFRodW1ibmFpbChjb250ZW50SWQ6IG51bWJlcik6IE9ic2VydmFibGU8QmxvYj4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuY29udGVudEFwaS5nZXRDb250ZW50VGh1bWJuYWlsKGNvbnRlbnRJZCkpXHJcbiAgICAgICAgICAgIC5waXBlKGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHJlbGF0ZWQgY29udGVudCBpdGVtcyBmb3IgYSB0YXNrIGluc3RhbmNlLlxyXG4gICAgICogQHBhcmFtIHRhc2tJZCBJRCBvZiB0aGUgdGFyZ2V0IHRhc2tcclxuICAgICAqIEBwYXJhbSBvcHRzIE9wdGlvbnMgc3VwcG9ydGVkIGJ5IEpTLUFQSVxyXG4gICAgICogQHJldHVybnMgTWV0YWRhdGEgZm9yIHRoZSBjb250ZW50XHJcbiAgICAgKi9cclxuICAgIGdldFRhc2tSZWxhdGVkQ29udGVudCh0YXNrSWQ6IHN0cmluZywgb3B0cz86IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5jb250ZW50QXBpLmdldFJlbGF0ZWRDb250ZW50Rm9yVGFzayh0YXNrSWQsIG9wdHMpKVxyXG4gICAgICAgICAgICAucGlwZShjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyByZWxhdGVkIGNvbnRlbnQgaXRlbXMgZm9yIGEgcHJvY2VzcyBpbnN0YW5jZS5cclxuICAgICAqIEBwYXJhbSBwcm9jZXNzSWQgSUQgb2YgdGhlIHRhcmdldCBwcm9jZXNzXHJcbiAgICAgKiBAcGFyYW0gb3B0cyBPcHRpb25zIHN1cHBvcnRlZCBieSBKUy1BUElcclxuICAgICAqIEByZXR1cm5zIE1ldGFkYXRhIGZvciB0aGUgY29udGVudFxyXG4gICAgICovXHJcbiAgICBnZXRQcm9jZXNzUmVsYXRlZENvbnRlbnQocHJvY2Vzc0lkOiBzdHJpbmcsIG9wdHM/OiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuY29udGVudEFwaS5nZXRSZWxhdGVkQ29udGVudEZvclByb2Nlc3NJbnN0YW5jZShwcm9jZXNzSWQsIG9wdHMpKVxyXG4gICAgICAgICAgICAucGlwZShjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGVsZXRlcyByZWxhdGVkIGNvbnRlbnQuXHJcbiAgICAgKiBAcGFyYW0gY29udGVudElkIElkZW50aWZpZXIgb2YgdGhlIGNvbnRlbnQgdG8gZGVsZXRlXHJcbiAgICAgKiBAcmV0dXJucyBOdWxsIHJlc3BvbnNlIHRoYXQgbm90aWZpZXMgd2hlbiB0aGUgZGVsZXRpb24gaXMgY29tcGxldGVcclxuICAgICAqL1xyXG4gICAgZGVsZXRlUmVsYXRlZENvbnRlbnQoY29udGVudElkOiBudW1iZXIpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuY29udGVudEFwaS5kZWxldGVDb250ZW50KGNvbnRlbnRJZCkpXHJcbiAgICAgICAgICAgIC5waXBlKGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBc3NvY2lhdGVzIGFuIHVwbG9hZGVkIGZpbGUgd2l0aCBhIHByb2Nlc3MgaW5zdGFuY2UuXHJcbiAgICAgKiBAcGFyYW0gcHJvY2Vzc0luc3RhbmNlSWQgSUQgb2YgdGhlIHRhcmdldCBwcm9jZXNzIGluc3RhbmNlXHJcbiAgICAgKiBAcGFyYW0gY29udGVudCBGaWxlIHRvIGFzc29jaWF0ZVxyXG4gICAgICogQHBhcmFtIG9wdHMgT3B0aW9ucyBzdXBwb3J0ZWQgYnkgSlMtQVBJXHJcbiAgICAgKiBAcmV0dXJucyBEZXRhaWxzIG9mIGNyZWF0ZWQgY29udGVudFxyXG4gICAgICovXHJcbiAgICBjcmVhdGVQcm9jZXNzUmVsYXRlZENvbnRlbnQocHJvY2Vzc0luc3RhbmNlSWQ6IHN0cmluZywgY29udGVudDogYW55LCBvcHRzPzogYW55KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmNvbnRlbnRBcGkuY3JlYXRlUmVsYXRlZENvbnRlbnRPblByb2Nlc3NJbnN0YW5jZShwcm9jZXNzSW5zdGFuY2VJZCwgY29udGVudCwgb3B0cykpXHJcbiAgICAgICAgICAgIC5waXBlKGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBc3NvY2lhdGVzIGFuIHVwbG9hZGVkIGZpbGUgd2l0aCBhIHRhc2sgaW5zdGFuY2UuXHJcbiAgICAgKiBAcGFyYW0gdGFza0lkIElEIG9mIHRoZSB0YXJnZXQgdGFza1xyXG4gICAgICogQHBhcmFtIGZpbGUgRmlsZSB0byBhc3NvY2lhdGVcclxuICAgICAqIEBwYXJhbSBvcHRzIE9wdGlvbnMgc3VwcG9ydGVkIGJ5IEpTLUFQSVxyXG4gICAgICogQHJldHVybnMgRGV0YWlscyBvZiBjcmVhdGVkIGNvbnRlbnRcclxuICAgICAqL1xyXG4gICAgY3JlYXRlVGFza1JlbGF0ZWRDb250ZW50KHRhc2tJZDogc3RyaW5nLCBmaWxlOiBhbnksIG9wdHM/OiBhbnkpIHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmNvbnRlbnRBcGkuY3JlYXRlUmVsYXRlZENvbnRlbnRPblRhc2sodGFza0lkLCBmaWxlLCBvcHRzKSlcclxuICAgICAgICAgICAgLnBpcGUoY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZXMgYSBKU09OIHJlcHJlc2VudGF0aW9uIG9mIGRhdGEuXHJcbiAgICAgKiBAcGFyYW0gcmVzIE9iamVjdCByZXByZXNlbnRpbmcgZGF0YVxyXG4gICAgICogQHJldHVybnMgSlNPTiBvYmplY3RcclxuICAgICAqL1xyXG4gICAgdG9Kc29uKHJlczogYW55KSB7XHJcbiAgICAgICAgaWYgKHJlcykge1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzIHx8IHt9O1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4ge307XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDcmVhdGVzIGEgSlNPTiBhcnJheSByZXByZXNlbnRhdGlvbiBvZiBkYXRhLlxyXG4gICAgICogQHBhcmFtIHJlcyBPYmplY3QgcmVwcmVzZW50aW5nIGRhdGFcclxuICAgICAqIEByZXR1cm5zIEpTT04gYXJyYXkgb2JqZWN0XHJcbiAgICAgKi9cclxuICAgIHRvSnNvbkFycmF5KHJlczogYW55KSB7XHJcbiAgICAgICAgaWYgKHJlcykge1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzLmRhdGEgfHwgW107XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBbXTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlcG9ydHMgYW4gZXJyb3IgbWVzc2FnZS5cclxuICAgICAqIEBwYXJhbSBlcnJvciBEYXRhIG9iamVjdCB3aXRoIG9wdGlvbmFsIGBtZXNzYWdlYCBhbmQgYHN0YXR1c2AgZmllbGRzIGZvciB0aGUgZXJyb3JcclxuICAgICAqIEByZXR1cm5zIENhbGxiYWNrIHdoZW4gYW4gZXJyb3Igb2NjdXJzXHJcbiAgICAgKi9cclxuICAgIGhhbmRsZUVycm9yKGVycm9yOiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBlcnJNc2cgPSBQcm9jZXNzQ29udGVudFNlcnZpY2UuVU5LTk9XTl9FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgIGlmIChlcnJvcikge1xyXG4gICAgICAgICAgICBlcnJNc2cgPSAoZXJyb3IubWVzc2FnZSkgPyBlcnJvci5tZXNzYWdlIDpcclxuICAgICAgICAgICAgICAgIGVycm9yLnN0YXR1cyA/IGAke2Vycm9yLnN0YXR1c30gLSAke2Vycm9yLnN0YXR1c1RleHR9YCA6IFByb2Nlc3NDb250ZW50U2VydmljZS5HRU5FUklDX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJNc2cpO1xyXG4gICAgICAgIHJldHVybiB0aHJvd0Vycm9yKGVyck1zZyk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==