/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import { Injectable } from '@angular/core';
import moment from 'moment-es6';
import { from, throwError } from 'rxjs';
import { WidgetTypeEnum } from '../models/widget-visibility.model';
import { map, catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "../../services/alfresco-api.service";
import * as i2 from "../../services/log.service";
export class WidgetVisibilityService {
    /**
     * @param {?} apiService
     * @param {?} logService
     */
    constructor(apiService, logService) {
        this.apiService = apiService;
        this.logService = logService;
    }
    /**
     * @param {?} form
     * @return {?}
     */
    refreshVisibility(form) {
        if (form && form.tabs && form.tabs.length > 0) {
            form.tabs.map((/**
             * @param {?} tabModel
             * @return {?}
             */
            (tabModel) => this.refreshEntityVisibility(tabModel)));
        }
        if (form) {
            form.getFormFields().map((/**
             * @param {?} field
             * @return {?}
             */
            (field) => this.refreshEntityVisibility(field)));
        }
    }
    /**
     * @param {?} element
     * @return {?}
     */
    refreshEntityVisibility(element) {
        /** @type {?} */
        const visible = this.evaluateVisibility(element.form, element.visibilityCondition);
        element.isVisible = visible;
    }
    /**
     * @param {?} form
     * @param {?} visibilityObj
     * @return {?}
     */
    evaluateVisibility(form, visibilityObj) {
        /** @type {?} */
        const isLeftFieldPresent = visibilityObj && (visibilityObj.leftType || visibilityObj.leftValue);
        if (!isLeftFieldPresent || isLeftFieldPresent === 'null') {
            return true;
        }
        else {
            return this.isFieldVisible(form, visibilityObj);
        }
    }
    /**
     * @param {?} form
     * @param {?} visibilityObj
     * @param {?=} accumulator
     * @param {?=} result
     * @return {?}
     */
    isFieldVisible(form, visibilityObj, accumulator = [], result = false) {
        /** @type {?} */
        const leftValue = this.getLeftValue(form, visibilityObj);
        /** @type {?} */
        const rightValue = this.getRightValue(form, visibilityObj);
        /** @type {?} */
        const actualResult = this.evaluateCondition(leftValue, rightValue, visibilityObj.operator);
        accumulator.push({ value: actualResult, operator: visibilityObj.nextConditionOperator });
        if (visibilityObj.nextCondition) {
            result = this.isFieldVisible(form, visibilityObj.nextCondition, accumulator);
        }
        else {
            result = accumulator[0].value ? accumulator[0].value : result;
            for (let i = 1; i < accumulator.length; i++) {
                if (accumulator[i - 1].operator && accumulator[i].value) {
                    result = this.evaluateLogicalOperation(accumulator[i - 1].operator, result, accumulator[i].value);
                }
            }
        }
        return !!result;
    }
    /**
     * @param {?} form
     * @param {?} visibilityObj
     * @return {?}
     */
    getLeftValue(form, visibilityObj) {
        /** @type {?} */
        let leftValue = '';
        if (visibilityObj.leftType && visibilityObj.leftType === WidgetTypeEnum.variable) {
            leftValue = this.getVariableValue(form, visibilityObj.leftValue, this.processVarList);
        }
        else if (visibilityObj.leftType && visibilityObj.leftType === WidgetTypeEnum.field) {
            leftValue = this.getFormValue(form, visibilityObj.leftValue);
            leftValue = leftValue ? leftValue : this.getVariableValue(form, visibilityObj.leftValue, this.processVarList);
        }
        return leftValue;
    }
    /**
     * @param {?} form
     * @param {?} visibilityObj
     * @return {?}
     */
    getRightValue(form, visibilityObj) {
        /** @type {?} */
        let valueFound = '';
        if (visibilityObj.rightType === WidgetTypeEnum.variable) {
            valueFound = this.getVariableValue(form, visibilityObj.rightValue, this.processVarList);
        }
        else if (visibilityObj.rightType === WidgetTypeEnum.field) {
            valueFound = this.getFormValue(form, visibilityObj.rightValue);
        }
        else {
            if (moment(visibilityObj.rightValue, 'YYYY-MM-DD', true).isValid()) {
                valueFound = visibilityObj.rightValue + 'T00:00:00.000Z';
            }
            else {
                valueFound = visibilityObj.rightValue;
            }
        }
        return valueFound;
    }
    /**
     * @param {?} form
     * @param {?} fieldId
     * @return {?}
     */
    getFormValue(form, fieldId) {
        /** @type {?} */
        let value = this.getFieldValue(form.values, fieldId);
        if (!value) {
            value = this.searchValueInForm(form, fieldId);
        }
        return value;
    }
    /**
     * @param {?} valueList
     * @param {?} fieldId
     * @return {?}
     */
    getFieldValue(valueList, fieldId) {
        /** @type {?} */
        let dropDownFilterByName;
        /** @type {?} */
        let valueFound;
        if (fieldId && fieldId.indexOf('_LABEL') > 0) {
            dropDownFilterByName = fieldId.substring(0, fieldId.length - 6);
            if (valueList[dropDownFilterByName]) {
                valueFound = valueList[dropDownFilterByName].name;
            }
        }
        else if (valueList[fieldId] && valueList[fieldId].id) {
            valueFound = valueList[fieldId].id;
        }
        else {
            valueFound = valueList[fieldId];
        }
        return valueFound;
    }
    /**
     * @param {?} form
     * @param {?} fieldId
     * @return {?}
     */
    searchValueInForm(form, fieldId) {
        /** @type {?} */
        let fieldValue = '';
        form.getFormFields().forEach((/**
         * @param {?} formField
         * @return {?}
         */
        (formField) => {
            if (this.isSearchedField(formField, fieldId)) {
                fieldValue = this.getObjectValue(formField, fieldId);
                if (!fieldValue) {
                    if (formField.value && formField.value.id) {
                        fieldValue = formField.value.id;
                    }
                    else {
                        fieldValue = formField.value;
                    }
                }
            }
        }));
        return fieldValue;
    }
    /**
     * @private
     * @param {?} field
     * @param {?} fieldId
     * @return {?}
     */
    getObjectValue(field, fieldId) {
        /** @type {?} */
        let value = '';
        if (field.value && field.value.name) {
            value = field.value.name;
        }
        else if (field.options) {
            /** @type {?} */
            const option = field.options.find((/**
             * @param {?} opt
             * @return {?}
             */
            (opt) => opt.id === field.value));
            if (option) {
                value = this.getValueFromOption(fieldId, option);
            }
        }
        return value;
    }
    /**
     * @private
     * @param {?} fieldId
     * @param {?} option
     * @return {?}
     */
    getValueFromOption(fieldId, option) {
        /** @type {?} */
        let optionValue = '';
        if (fieldId && fieldId.indexOf('_LABEL') > 0) {
            optionValue = option.name;
        }
        else {
            optionValue = option.id;
        }
        return optionValue;
    }
    /**
     * @private
     * @param {?} field
     * @param {?} fieldToFind
     * @return {?}
     */
    isSearchedField(field, fieldToFind) {
        return (field.id && fieldToFind) ? field.id.toUpperCase() === fieldToFind.toUpperCase() : false;
    }
    /**
     * @param {?} form
     * @param {?} name
     * @param {?} processVarList
     * @return {?}
     */
    getVariableValue(form, name, processVarList) {
        return this.getFormVariableValue(form, name) ||
            this.getProcessVariableValue(name, processVarList);
    }
    /**
     * @private
     * @param {?} form
     * @param {?} identifier
     * @return {?}
     */
    getFormVariableValue(form, identifier) {
        /** @type {?} */
        const variables = this.getFormVariables(form);
        if (variables) {
            /** @type {?} */
            const formVariable = variables.find((/**
             * @param {?} formVar
             * @return {?}
             */
            (formVar) => {
                return formVar.name === identifier || formVar.id === identifier;
            }));
            /** @type {?} */
            let value;
            if (formVariable) {
                value = formVariable.value;
                if (formVariable.type === 'date') {
                    value += 'T00:00:00.000Z';
                }
            }
            return value;
        }
    }
    /**
     * @private
     * @param {?} form
     * @return {?}
     */
    getFormVariables(form) {
        return form.json.variables;
    }
    /**
     * @private
     * @param {?} name
     * @param {?} processVarList
     * @return {?}
     */
    getProcessVariableValue(name, processVarList) {
        if (processVarList) {
            /** @type {?} */
            const processVariable = processVarList.find((/**
             * @param {?} variable
             * @return {?}
             */
            (variable) => variable.id === name));
            if (processVariable) {
                return processVariable.value;
            }
        }
    }
    /**
     * @param {?} logicOp
     * @param {?} previousValue
     * @param {?} newValue
     * @return {?}
     */
    evaluateLogicalOperation(logicOp, previousValue, newValue) {
        switch (logicOp) {
            case 'and':
                return previousValue && newValue;
            case 'or':
                return previousValue || newValue;
            case 'and-not':
                return previousValue && !newValue;
            case 'or-not':
                return previousValue || !newValue;
            default:
                this.logService.error('NO valid operation! wrong op request : ' + logicOp);
                break;
        }
    }
    /**
     * @param {?} leftValue
     * @param {?} rightValue
     * @param {?} operator
     * @return {?}
     */
    evaluateCondition(leftValue, rightValue, operator) {
        switch (operator) {
            case '==':
                return leftValue + '' === rightValue + '';
            case '<':
                return leftValue < rightValue;
            case '!=':
                return leftValue + '' !== rightValue + '';
            case '>':
                return leftValue > rightValue;
            case '>=':
                return leftValue >= rightValue;
            case '<=':
                return leftValue <= rightValue;
            case 'empty':
                return leftValue ? leftValue === '' : true;
            case '!empty':
                return leftValue ? leftValue !== '' : false;
            default:
                this.logService.error('NO valid operation!');
                break;
        }
        return;
    }
    /**
     * @return {?}
     */
    cleanProcessVariable() {
        this.processVarList = [];
    }
    /**
     * @param {?} taskId
     * @return {?}
     */
    getTaskProcessVariable(taskId) {
        return from(this.apiService.getInstance().activiti.taskFormsApi.getTaskFormVariables(taskId))
            .pipe(map((/**
         * @param {?} res
         * @return {?}
         */
        (res) => {
            /** @type {?} */
            const jsonRes = this.toJson(res);
            this.processVarList = (/** @type {?} */ (jsonRes));
            return jsonRes;
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * @param {?} res
     * @return {?}
     */
    toJson(res) {
        return res || {};
    }
    /**
     * @private
     * @param {?} err
     * @return {?}
     */
    handleError(err) {
        this.logService.error('Error while performing a call');
        return throwError('Error while performing a call - Server error');
    }
}
WidgetVisibilityService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
WidgetVisibilityService.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: LogService }
];
/** @nocollapse */ WidgetVisibilityService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function WidgetVisibilityService_Factory() { return new WidgetVisibilityService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: WidgetVisibilityService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    WidgetVisibilityService.prototype.processVarList;
    /**
     * @type {?}
     * @private
     */
    WidgetVisibilityService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    WidgetVisibilityService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0LXZpc2liaWxpdHkuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vc2VydmljZXMvd2lkZ2V0LXZpc2liaWxpdHkuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUN6RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDeEQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLE1BQU0sTUFBTSxZQUFZLENBQUM7QUFDaEMsT0FBTyxFQUFjLElBQUksRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFHcEQsT0FBTyxFQUF5QixjQUFjLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUMxRixPQUFPLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBS2pELE1BQU0sT0FBTyx1QkFBdUI7Ozs7O0lBSWhDLFlBQW9CLFVBQThCLEVBQzlCLFVBQXNCO1FBRHRCLGVBQVUsR0FBVixVQUFVLENBQW9CO1FBQzlCLGVBQVUsR0FBVixVQUFVLENBQVk7SUFDMUMsQ0FBQzs7Ozs7SUFFTSxpQkFBaUIsQ0FBQyxJQUFlO1FBQ3BDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzNDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRzs7OztZQUFDLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDLEVBQUMsQ0FBQztTQUN2RTtRQUVELElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDLEdBQUc7Ozs7WUFBQyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLHVCQUF1QixDQUFDLEtBQUssQ0FBQyxFQUFDLENBQUM7U0FDNUU7SUFDTCxDQUFDOzs7OztJQUVELHVCQUF1QixDQUFDLE9BQWtDOztjQUNoRCxPQUFPLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLG1CQUFtQixDQUFDO1FBQ2xGLE9BQU8sQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDO0lBQ2hDLENBQUM7Ozs7OztJQUVELGtCQUFrQixDQUFDLElBQWUsRUFBRSxhQUFvQzs7Y0FDOUQsa0JBQWtCLEdBQUcsYUFBYSxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsSUFBSSxhQUFhLENBQUMsU0FBUyxDQUFDO1FBQy9GLElBQUksQ0FBQyxrQkFBa0IsSUFBSSxrQkFBa0IsS0FBSyxNQUFNLEVBQUU7WUFDdEQsT0FBTyxJQUFJLENBQUM7U0FDZjthQUFNO1lBQ0gsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxhQUFhLENBQUMsQ0FBQztTQUNuRDtJQUNMLENBQUM7Ozs7Ozs7O0lBRUQsY0FBYyxDQUFDLElBQWUsRUFBRSxhQUFvQyxFQUFFLGNBQXFCLEVBQUUsRUFBRSxTQUFrQixLQUFLOztjQUM1RyxTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDOztjQUNsRCxVQUFVLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDOztjQUNwRCxZQUFZLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLFNBQVMsRUFBRSxVQUFVLEVBQUUsYUFBYSxDQUFDLFFBQVEsQ0FBQztRQUMxRixXQUFXLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxRQUFRLEVBQUUsYUFBYSxDQUFDLHFCQUFxQixFQUFFLENBQUMsQ0FBQztRQUN6RixJQUFJLGFBQWEsQ0FBQyxhQUFhLEVBQUU7WUFDN0IsTUFBTSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxhQUFhLEVBQUUsV0FBVyxDQUFDLENBQUM7U0FDaEY7YUFBTTtZQUNILE1BQU0sR0FBRyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUM7WUFFOUQsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFdBQVcsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7Z0JBQ3pDLElBQUksV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxRQUFRLElBQUksV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssRUFBRTtvQkFDckQsTUFBTSxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FDbEMsV0FBVyxDQUFDLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxRQUFRLEVBQzNCLE1BQU0sRUFDTixXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUN2QixDQUFDO2lCQUNMO2FBQ0o7U0FDSjtRQUNELE9BQU8sQ0FBQyxDQUFDLE1BQU0sQ0FBQztJQUVwQixDQUFDOzs7Ozs7SUFFRCxZQUFZLENBQUMsSUFBZSxFQUFFLGFBQW9DOztZQUMxRCxTQUFTLEdBQUcsRUFBRTtRQUNsQixJQUFJLGFBQWEsQ0FBQyxRQUFRLElBQUksYUFBYSxDQUFDLFFBQVEsS0FBSyxjQUFjLENBQUMsUUFBUSxFQUFFO1lBQzlFLFNBQVMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1NBQ3pGO2FBQU0sSUFBSSxhQUFhLENBQUMsUUFBUSxJQUFJLGFBQWEsQ0FBQyxRQUFRLEtBQUssY0FBYyxDQUFDLEtBQUssRUFBRTtZQUNsRixTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzdELFNBQVMsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxhQUFhLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztTQUNqSDtRQUNELE9BQU8sU0FBUyxDQUFDO0lBQ3JCLENBQUM7Ozs7OztJQUVELGFBQWEsQ0FBQyxJQUFlLEVBQUUsYUFBb0M7O1lBQzNELFVBQVUsR0FBRyxFQUFFO1FBQ25CLElBQUksYUFBYSxDQUFDLFNBQVMsS0FBSyxjQUFjLENBQUMsUUFBUSxFQUFFO1lBQ3JELFVBQVUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1NBQzNGO2FBQU0sSUFBSSxhQUFhLENBQUMsU0FBUyxLQUFLLGNBQWMsQ0FBQyxLQUFLLEVBQUU7WUFDekQsVUFBVSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUNsRTthQUFNO1lBQ0gsSUFBSSxNQUFNLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7Z0JBQ2hFLFVBQVUsR0FBRyxhQUFhLENBQUMsVUFBVSxHQUFHLGdCQUFnQixDQUFDO2FBQzVEO2lCQUFNO2dCQUNILFVBQVUsR0FBRyxhQUFhLENBQUMsVUFBVSxDQUFDO2FBQ3pDO1NBQ0o7UUFDRCxPQUFPLFVBQVUsQ0FBQztJQUN0QixDQUFDOzs7Ozs7SUFFRCxZQUFZLENBQUMsSUFBZSxFQUFFLE9BQWU7O1lBQ3JDLEtBQUssR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDO1FBRXBELElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDUixLQUFLLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztTQUNqRDtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7OztJQUVELGFBQWEsQ0FBQyxTQUFjLEVBQUUsT0FBZTs7WUFDckMsb0JBQW9COztZQUFFLFVBQVU7UUFDcEMsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDMUMsb0JBQW9CLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNoRSxJQUFJLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxFQUFFO2dCQUNqQyxVQUFVLEdBQUcsU0FBUyxDQUFDLG9CQUFvQixDQUFDLENBQUMsSUFBSSxDQUFDO2FBQ3JEO1NBQ0o7YUFBTSxJQUFJLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ3BELFVBQVUsR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDO1NBQ3RDO2FBQU07WUFDSCxVQUFVLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ25DO1FBQ0QsT0FBTyxVQUFVLENBQUM7SUFDdEIsQ0FBQzs7Ozs7O0lBRUQsaUJBQWlCLENBQUMsSUFBZSxFQUFFLE9BQWU7O1lBQzFDLFVBQVUsR0FBRyxFQUFFO1FBQ25CLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxPQUFPOzs7O1FBQUMsQ0FBQyxTQUF5QixFQUFFLEVBQUU7WUFDdkQsSUFBSSxJQUFJLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsRUFBRTtnQkFDMUMsVUFBVSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUNyRCxJQUFJLENBQUMsVUFBVSxFQUFFO29CQUNiLElBQUksU0FBUyxDQUFDLEtBQUssSUFBSSxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBRTt3QkFDdkMsVUFBVSxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO3FCQUNuQzt5QkFBTTt3QkFDSCxVQUFVLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQztxQkFDaEM7aUJBQ0o7YUFDSjtRQUNMLENBQUMsRUFBQyxDQUFDO1FBRUgsT0FBTyxVQUFVLENBQUM7SUFDdEIsQ0FBQzs7Ozs7OztJQUVPLGNBQWMsQ0FBQyxLQUFxQixFQUFFLE9BQWU7O1lBQ3JELEtBQUssR0FBRyxFQUFFO1FBQ2QsSUFBSSxLQUFLLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFO1lBQ2pDLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztTQUM1QjthQUFNLElBQUksS0FBSyxDQUFDLE9BQU8sRUFBRTs7a0JBQ2hCLE1BQU0sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUk7Ozs7WUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUUsS0FBSyxLQUFLLENBQUMsS0FBSyxFQUFDO1lBQ2xFLElBQUksTUFBTSxFQUFFO2dCQUNSLEtBQUssR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQ3BEO1NBQ0o7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDOzs7Ozs7O0lBRU8sa0JBQWtCLENBQUMsT0FBZSxFQUFFLE1BQU07O1lBQzFDLFdBQVcsR0FBRyxFQUFFO1FBQ3BCLElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQzFDLFdBQVcsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO1NBQzdCO2FBQU07WUFDSCxXQUFXLEdBQUcsTUFBTSxDQUFDLEVBQUUsQ0FBQztTQUMzQjtRQUNELE9BQU8sV0FBVyxDQUFDO0lBQ3ZCLENBQUM7Ozs7Ozs7SUFFTyxlQUFlLENBQUMsS0FBcUIsRUFBRSxXQUFtQjtRQUM5RCxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUUsSUFBSSxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxXQUFXLEVBQUUsS0FBSyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUNwRyxDQUFDOzs7Ozs7O0lBRUQsZ0JBQWdCLENBQUMsSUFBZSxFQUFFLElBQVksRUFBRSxjQUEwQztRQUN0RixPQUFPLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDO1lBQ3hDLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLEVBQUUsY0FBYyxDQUFDLENBQUM7SUFDM0QsQ0FBQzs7Ozs7OztJQUVPLG9CQUFvQixDQUFDLElBQWUsRUFBRSxVQUFrQjs7Y0FDdEQsU0FBUyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7UUFDN0MsSUFBSSxTQUFTLEVBQUU7O2tCQUNMLFlBQVksR0FBRyxTQUFTLENBQUMsSUFBSTs7OztZQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7Z0JBQzVDLE9BQU8sT0FBTyxDQUFDLElBQUksS0FBSyxVQUFVLElBQUksT0FBTyxDQUFDLEVBQUUsS0FBSyxVQUFVLENBQUM7WUFDcEUsQ0FBQyxFQUFDOztnQkFFRSxLQUFLO1lBQ1QsSUFBSSxZQUFZLEVBQUU7Z0JBQ2QsS0FBSyxHQUFHLFlBQVksQ0FBQyxLQUFLLENBQUM7Z0JBQzNCLElBQUksWUFBWSxDQUFDLElBQUksS0FBSyxNQUFNLEVBQUU7b0JBQzlCLEtBQUssSUFBSSxnQkFBZ0IsQ0FBQztpQkFDN0I7YUFDSjtZQUVELE9BQU8sS0FBSyxDQUFDO1NBQ2hCO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sZ0JBQWdCLENBQUMsSUFBZTtRQUNwQyxPQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO0lBQ2hDLENBQUM7Ozs7Ozs7SUFFTyx1QkFBdUIsQ0FBQyxJQUFZLEVBQUUsY0FBMEM7UUFDcEYsSUFBSSxjQUFjLEVBQUU7O2tCQUNWLGVBQWUsR0FBRyxjQUFjLENBQUMsSUFBSTs7OztZQUFDLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUMsRUFBRSxLQUFLLElBQUksRUFBQztZQUMvRSxJQUFJLGVBQWUsRUFBRTtnQkFDakIsT0FBTyxlQUFlLENBQUMsS0FBSyxDQUFDO2FBQ2hDO1NBQ0o7SUFDTCxDQUFDOzs7Ozs7O0lBRUQsd0JBQXdCLENBQUMsT0FBTyxFQUFFLGFBQWEsRUFBRSxRQUFRO1FBQ3JELFFBQVEsT0FBTyxFQUFFO1lBQ2IsS0FBSyxLQUFLO2dCQUNOLE9BQU8sYUFBYSxJQUFJLFFBQVEsQ0FBQztZQUNyQyxLQUFLLElBQUk7Z0JBQ0wsT0FBTyxhQUFhLElBQUksUUFBUSxDQUFDO1lBQ3JDLEtBQUssU0FBUztnQkFDVixPQUFPLGFBQWEsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUN0QyxLQUFLLFFBQVE7Z0JBQ1QsT0FBTyxhQUFhLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDdEM7Z0JBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMseUNBQXlDLEdBQUcsT0FBTyxDQUFDLENBQUM7Z0JBQzNFLE1BQU07U0FDYjtJQUNMLENBQUM7Ozs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsVUFBVSxFQUFFLFFBQVE7UUFDN0MsUUFBUSxRQUFRLEVBQUU7WUFDZCxLQUFLLElBQUk7Z0JBQ0wsT0FBTyxTQUFTLEdBQUcsRUFBRSxLQUFLLFVBQVUsR0FBRyxFQUFFLENBQUM7WUFDOUMsS0FBSyxHQUFHO2dCQUNKLE9BQU8sU0FBUyxHQUFHLFVBQVUsQ0FBQztZQUNsQyxLQUFLLElBQUk7Z0JBQ0wsT0FBTyxTQUFTLEdBQUcsRUFBRSxLQUFLLFVBQVUsR0FBRyxFQUFFLENBQUM7WUFDOUMsS0FBSyxHQUFHO2dCQUNKLE9BQU8sU0FBUyxHQUFHLFVBQVUsQ0FBQztZQUNsQyxLQUFLLElBQUk7Z0JBQ0wsT0FBTyxTQUFTLElBQUksVUFBVSxDQUFDO1lBQ25DLEtBQUssSUFBSTtnQkFDTCxPQUFPLFNBQVMsSUFBSSxVQUFVLENBQUM7WUFDbkMsS0FBSyxPQUFPO2dCQUNSLE9BQU8sU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7WUFDL0MsS0FBSyxRQUFRO2dCQUNULE9BQU8sU0FBUyxDQUFDLENBQUMsQ0FBQyxTQUFTLEtBQUssRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDaEQ7Z0JBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMscUJBQXFCLENBQUMsQ0FBQztnQkFDN0MsTUFBTTtTQUNiO1FBQ0QsT0FBTztJQUNYLENBQUM7Ozs7SUFFRCxvQkFBb0I7UUFDaEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7SUFDN0IsQ0FBQzs7Ozs7SUFFRCxzQkFBc0IsQ0FBQyxNQUFjO1FBQ2pDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUN4RixJQUFJLENBQ0QsR0FBRzs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7O2tCQUNGLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQztZQUNoQyxJQUFJLENBQUMsY0FBYyxHQUFHLG1CQUE2QixPQUFPLEVBQUEsQ0FBQztZQUMzRCxPQUFPLE9BQU8sQ0FBQztRQUNuQixDQUFDLEVBQUMsRUFDRixVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7Ozs7O0lBRUQsTUFBTSxDQUFDLEdBQVE7UUFDWCxPQUFPLEdBQUcsSUFBSSxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7O0lBRU8sV0FBVyxDQUFDLEdBQUc7UUFDbkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQztRQUN2RCxPQUFPLFVBQVUsQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO0lBQ3RFLENBQUM7OztZQWhRSixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFaUSxrQkFBa0I7WUFDbEIsVUFBVTs7Ozs7Ozs7SUFjZixpREFBbUQ7Ozs7O0lBRXZDLDZDQUFzQzs7Ozs7SUFDdEMsNkNBQThCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgbW9tZW50IGZyb20gJ21vbWVudC1lczYnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmcm9tLCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEZvcm1GaWVsZE1vZGVsLCBGb3JtTW9kZWwsIFRhYk1vZGVsIH0gZnJvbSAnLi4vY29tcG9uZW50cy93aWRnZXRzL2NvcmUvaW5kZXgnO1xyXG5pbXBvcnQgeyBUYXNrUHJvY2Vzc1ZhcmlhYmxlTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvdGFzay1wcm9jZXNzLXZhcmlhYmxlLm1vZGVsJztcclxuaW1wb3J0IHsgV2lkZ2V0VmlzaWJpbGl0eU1vZGVsLCBXaWRnZXRUeXBlRW51bSB9IGZyb20gJy4uL21vZGVscy93aWRnZXQtdmlzaWJpbGl0eS5tb2RlbCc7XHJcbmltcG9ydCB7IG1hcCwgY2F0Y2hFcnJvciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgV2lkZ2V0VmlzaWJpbGl0eVNlcnZpY2Uge1xyXG5cclxuICAgIHByaXZhdGUgcHJvY2Vzc1Zhckxpc3Q6IFRhc2tQcm9jZXNzVmFyaWFibGVNb2RlbFtdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIHJlZnJlc2hWaXNpYmlsaXR5KGZvcm06IEZvcm1Nb2RlbCkge1xyXG4gICAgICAgIGlmIChmb3JtICYmIGZvcm0udGFicyAmJiBmb3JtLnRhYnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBmb3JtLnRhYnMubWFwKCh0YWJNb2RlbCkgPT4gdGhpcy5yZWZyZXNoRW50aXR5VmlzaWJpbGl0eSh0YWJNb2RlbCkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGZvcm0pIHtcclxuICAgICAgICAgICAgZm9ybS5nZXRGb3JtRmllbGRzKCkubWFwKChmaWVsZCkgPT4gdGhpcy5yZWZyZXNoRW50aXR5VmlzaWJpbGl0eShmaWVsZCkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICByZWZyZXNoRW50aXR5VmlzaWJpbGl0eShlbGVtZW50OiBGb3JtRmllbGRNb2RlbCB8IFRhYk1vZGVsKSB7XHJcbiAgICAgICAgY29uc3QgdmlzaWJsZSA9IHRoaXMuZXZhbHVhdGVWaXNpYmlsaXR5KGVsZW1lbnQuZm9ybSwgZWxlbWVudC52aXNpYmlsaXR5Q29uZGl0aW9uKTtcclxuICAgICAgICBlbGVtZW50LmlzVmlzaWJsZSA9IHZpc2libGU7XHJcbiAgICB9XHJcblxyXG4gICAgZXZhbHVhdGVWaXNpYmlsaXR5KGZvcm06IEZvcm1Nb2RlbCwgdmlzaWJpbGl0eU9iajogV2lkZ2V0VmlzaWJpbGl0eU1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgY29uc3QgaXNMZWZ0RmllbGRQcmVzZW50ID0gdmlzaWJpbGl0eU9iaiAmJiAodmlzaWJpbGl0eU9iai5sZWZ0VHlwZSB8fCB2aXNpYmlsaXR5T2JqLmxlZnRWYWx1ZSk7XHJcbiAgICAgICAgaWYgKCFpc0xlZnRGaWVsZFByZXNlbnQgfHwgaXNMZWZ0RmllbGRQcmVzZW50ID09PSAnbnVsbCcpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuaXNGaWVsZFZpc2libGUoZm9ybSwgdmlzaWJpbGl0eU9iaik7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlzRmllbGRWaXNpYmxlKGZvcm06IEZvcm1Nb2RlbCwgdmlzaWJpbGl0eU9iajogV2lkZ2V0VmlzaWJpbGl0eU1vZGVsLCBhY2N1bXVsYXRvcjogYW55W10gPSBbXSwgcmVzdWx0OiBib29sZWFuID0gZmFsc2UpOiBib29sZWFuIHtcclxuICAgICAgICBjb25zdCBsZWZ0VmFsdWUgPSB0aGlzLmdldExlZnRWYWx1ZShmb3JtLCB2aXNpYmlsaXR5T2JqKTtcclxuICAgICAgICBjb25zdCByaWdodFZhbHVlID0gdGhpcy5nZXRSaWdodFZhbHVlKGZvcm0sIHZpc2liaWxpdHlPYmopO1xyXG4gICAgICAgIGNvbnN0IGFjdHVhbFJlc3VsdCA9IHRoaXMuZXZhbHVhdGVDb25kaXRpb24obGVmdFZhbHVlLCByaWdodFZhbHVlLCB2aXNpYmlsaXR5T2JqLm9wZXJhdG9yKTtcclxuICAgICAgICBhY2N1bXVsYXRvci5wdXNoKHsgdmFsdWU6IGFjdHVhbFJlc3VsdCwgb3BlcmF0b3I6IHZpc2liaWxpdHlPYmoubmV4dENvbmRpdGlvbk9wZXJhdG9yIH0pO1xyXG4gICAgICAgIGlmICh2aXNpYmlsaXR5T2JqLm5leHRDb25kaXRpb24pIHtcclxuICAgICAgICAgICAgcmVzdWx0ID0gdGhpcy5pc0ZpZWxkVmlzaWJsZShmb3JtLCB2aXNpYmlsaXR5T2JqLm5leHRDb25kaXRpb24sIGFjY3VtdWxhdG9yKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXN1bHQgPSBhY2N1bXVsYXRvclswXS52YWx1ZSA/IGFjY3VtdWxhdG9yWzBdLnZhbHVlIDogcmVzdWx0O1xyXG5cclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDE7IGkgPCBhY2N1bXVsYXRvci5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgaWYgKGFjY3VtdWxhdG9yW2kgLSAxXS5vcGVyYXRvciAmJiBhY2N1bXVsYXRvcltpXS52YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdCA9IHRoaXMuZXZhbHVhdGVMb2dpY2FsT3BlcmF0aW9uKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBhY2N1bXVsYXRvcltpIC0gMV0ub3BlcmF0b3IsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3VsdCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWNjdW11bGF0b3JbaV0udmFsdWVcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiAhIXJlc3VsdDtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TGVmdFZhbHVlKGZvcm06IEZvcm1Nb2RlbCwgdmlzaWJpbGl0eU9iajogV2lkZ2V0VmlzaWJpbGl0eU1vZGVsKTogc3RyaW5nIHtcclxuICAgICAgICBsZXQgbGVmdFZhbHVlID0gJyc7XHJcbiAgICAgICAgaWYgKHZpc2liaWxpdHlPYmoubGVmdFR5cGUgJiYgdmlzaWJpbGl0eU9iai5sZWZ0VHlwZSA9PT0gV2lkZ2V0VHlwZUVudW0udmFyaWFibGUpIHtcclxuICAgICAgICAgICAgbGVmdFZhbHVlID0gdGhpcy5nZXRWYXJpYWJsZVZhbHVlKGZvcm0sIHZpc2liaWxpdHlPYmoubGVmdFZhbHVlLCB0aGlzLnByb2Nlc3NWYXJMaXN0KTtcclxuICAgICAgICB9IGVsc2UgaWYgKHZpc2liaWxpdHlPYmoubGVmdFR5cGUgJiYgdmlzaWJpbGl0eU9iai5sZWZ0VHlwZSA9PT0gV2lkZ2V0VHlwZUVudW0uZmllbGQpIHtcclxuICAgICAgICAgICAgbGVmdFZhbHVlID0gdGhpcy5nZXRGb3JtVmFsdWUoZm9ybSwgdmlzaWJpbGl0eU9iai5sZWZ0VmFsdWUpO1xyXG4gICAgICAgICAgICBsZWZ0VmFsdWUgPSBsZWZ0VmFsdWUgPyBsZWZ0VmFsdWUgOiB0aGlzLmdldFZhcmlhYmxlVmFsdWUoZm9ybSwgdmlzaWJpbGl0eU9iai5sZWZ0VmFsdWUsIHRoaXMucHJvY2Vzc1Zhckxpc3QpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbGVmdFZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFJpZ2h0VmFsdWUoZm9ybTogRm9ybU1vZGVsLCB2aXNpYmlsaXR5T2JqOiBXaWRnZXRWaXNpYmlsaXR5TW9kZWwpOiBzdHJpbmcge1xyXG4gICAgICAgIGxldCB2YWx1ZUZvdW5kID0gJyc7XHJcbiAgICAgICAgaWYgKHZpc2liaWxpdHlPYmoucmlnaHRUeXBlID09PSBXaWRnZXRUeXBlRW51bS52YXJpYWJsZSkge1xyXG4gICAgICAgICAgICB2YWx1ZUZvdW5kID0gdGhpcy5nZXRWYXJpYWJsZVZhbHVlKGZvcm0sIHZpc2liaWxpdHlPYmoucmlnaHRWYWx1ZSwgdGhpcy5wcm9jZXNzVmFyTGlzdCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh2aXNpYmlsaXR5T2JqLnJpZ2h0VHlwZSA9PT0gV2lkZ2V0VHlwZUVudW0uZmllbGQpIHtcclxuICAgICAgICAgICAgdmFsdWVGb3VuZCA9IHRoaXMuZ2V0Rm9ybVZhbHVlKGZvcm0sIHZpc2liaWxpdHlPYmoucmlnaHRWYWx1ZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKG1vbWVudCh2aXNpYmlsaXR5T2JqLnJpZ2h0VmFsdWUsICdZWVlZLU1NLUREJywgdHJ1ZSkuaXNWYWxpZCgpKSB7XHJcbiAgICAgICAgICAgICAgICB2YWx1ZUZvdW5kID0gdmlzaWJpbGl0eU9iai5yaWdodFZhbHVlICsgJ1QwMDowMDowMC4wMDBaJztcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHZhbHVlRm91bmQgPSB2aXNpYmlsaXR5T2JqLnJpZ2h0VmFsdWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlRm91bmQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Rm9ybVZhbHVlKGZvcm06IEZvcm1Nb2RlbCwgZmllbGRJZDogc3RyaW5nKTogYW55IHtcclxuICAgICAgICBsZXQgdmFsdWUgPSB0aGlzLmdldEZpZWxkVmFsdWUoZm9ybS52YWx1ZXMsIGZpZWxkSWQpO1xyXG5cclxuICAgICAgICBpZiAoIXZhbHVlKSB7XHJcbiAgICAgICAgICAgIHZhbHVlID0gdGhpcy5zZWFyY2hWYWx1ZUluRm9ybShmb3JtLCBmaWVsZElkKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEZpZWxkVmFsdWUodmFsdWVMaXN0OiBhbnksIGZpZWxkSWQ6IHN0cmluZyk6IGFueSB7XHJcbiAgICAgICAgbGV0IGRyb3BEb3duRmlsdGVyQnlOYW1lLCB2YWx1ZUZvdW5kO1xyXG4gICAgICAgIGlmIChmaWVsZElkICYmIGZpZWxkSWQuaW5kZXhPZignX0xBQkVMJykgPiAwKSB7XHJcbiAgICAgICAgICAgIGRyb3BEb3duRmlsdGVyQnlOYW1lID0gZmllbGRJZC5zdWJzdHJpbmcoMCwgZmllbGRJZC5sZW5ndGggLSA2KTtcclxuICAgICAgICAgICAgaWYgKHZhbHVlTGlzdFtkcm9wRG93bkZpbHRlckJ5TmFtZV0pIHtcclxuICAgICAgICAgICAgICAgIHZhbHVlRm91bmQgPSB2YWx1ZUxpc3RbZHJvcERvd25GaWx0ZXJCeU5hbWVdLm5hbWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2UgaWYgKHZhbHVlTGlzdFtmaWVsZElkXSAmJiB2YWx1ZUxpc3RbZmllbGRJZF0uaWQpIHtcclxuICAgICAgICAgICAgdmFsdWVGb3VuZCA9IHZhbHVlTGlzdFtmaWVsZElkXS5pZDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB2YWx1ZUZvdW5kID0gdmFsdWVMaXN0W2ZpZWxkSWRdO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdmFsdWVGb3VuZDtcclxuICAgIH1cclxuXHJcbiAgICBzZWFyY2hWYWx1ZUluRm9ybShmb3JtOiBGb3JtTW9kZWwsIGZpZWxkSWQ6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgbGV0IGZpZWxkVmFsdWUgPSAnJztcclxuICAgICAgICBmb3JtLmdldEZvcm1GaWVsZHMoKS5mb3JFYWNoKChmb3JtRmllbGQ6IEZvcm1GaWVsZE1vZGVsKSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmlzU2VhcmNoZWRGaWVsZChmb3JtRmllbGQsIGZpZWxkSWQpKSB7XHJcbiAgICAgICAgICAgICAgICBmaWVsZFZhbHVlID0gdGhpcy5nZXRPYmplY3RWYWx1ZShmb3JtRmllbGQsIGZpZWxkSWQpO1xyXG4gICAgICAgICAgICAgICAgaWYgKCFmaWVsZFZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGZvcm1GaWVsZC52YWx1ZSAmJiBmb3JtRmllbGQudmFsdWUuaWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRWYWx1ZSA9IGZvcm1GaWVsZC52YWx1ZS5pZDtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWVsZFZhbHVlID0gZm9ybUZpZWxkLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gZmllbGRWYWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldE9iamVjdFZhbHVlKGZpZWxkOiBGb3JtRmllbGRNb2RlbCwgZmllbGRJZDogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgICAgICBsZXQgdmFsdWUgPSAnJztcclxuICAgICAgICBpZiAoZmllbGQudmFsdWUgJiYgZmllbGQudmFsdWUubmFtZSkge1xyXG4gICAgICAgICAgICB2YWx1ZSA9IGZpZWxkLnZhbHVlLm5hbWU7XHJcbiAgICAgICAgfSBlbHNlIGlmIChmaWVsZC5vcHRpb25zKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG9wdGlvbiA9IGZpZWxkLm9wdGlvbnMuZmluZCgob3B0KSA9PiBvcHQuaWQgPT09IGZpZWxkLnZhbHVlKTtcclxuICAgICAgICAgICAgaWYgKG9wdGlvbikge1xyXG4gICAgICAgICAgICAgICAgdmFsdWUgPSB0aGlzLmdldFZhbHVlRnJvbU9wdGlvbihmaWVsZElkLCBvcHRpb24pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldFZhbHVlRnJvbU9wdGlvbihmaWVsZElkOiBzdHJpbmcsIG9wdGlvbik6IHN0cmluZyB7XHJcbiAgICAgICAgbGV0IG9wdGlvblZhbHVlID0gJyc7XHJcbiAgICAgICAgaWYgKGZpZWxkSWQgJiYgZmllbGRJZC5pbmRleE9mKCdfTEFCRUwnKSA+IDApIHtcclxuICAgICAgICAgICAgb3B0aW9uVmFsdWUgPSBvcHRpb24ubmFtZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBvcHRpb25WYWx1ZSA9IG9wdGlvbi5pZDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG9wdGlvblZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaXNTZWFyY2hlZEZpZWxkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCwgZmllbGRUb0ZpbmQ6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiAoZmllbGQuaWQgJiYgZmllbGRUb0ZpbmQpID8gZmllbGQuaWQudG9VcHBlckNhc2UoKSA9PT0gZmllbGRUb0ZpbmQudG9VcHBlckNhc2UoKSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFZhcmlhYmxlVmFsdWUoZm9ybTogRm9ybU1vZGVsLCBuYW1lOiBzdHJpbmcsIHByb2Nlc3NWYXJMaXN0OiBUYXNrUHJvY2Vzc1ZhcmlhYmxlTW9kZWxbXSk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0Rm9ybVZhcmlhYmxlVmFsdWUoZm9ybSwgbmFtZSkgfHxcclxuICAgICAgICAgICAgdGhpcy5nZXRQcm9jZXNzVmFyaWFibGVWYWx1ZShuYW1lLCBwcm9jZXNzVmFyTGlzdCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRGb3JtVmFyaWFibGVWYWx1ZShmb3JtOiBGb3JtTW9kZWwsIGlkZW50aWZpZXI6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgY29uc3QgdmFyaWFibGVzID0gdGhpcy5nZXRGb3JtVmFyaWFibGVzKGZvcm0pO1xyXG4gICAgICAgIGlmICh2YXJpYWJsZXMpIHtcclxuICAgICAgICAgICAgY29uc3QgZm9ybVZhcmlhYmxlID0gdmFyaWFibGVzLmZpbmQoKGZvcm1WYXIpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmb3JtVmFyLm5hbWUgPT09IGlkZW50aWZpZXIgfHwgZm9ybVZhci5pZCA9PT0gaWRlbnRpZmllcjtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICBsZXQgdmFsdWU7XHJcbiAgICAgICAgICAgIGlmIChmb3JtVmFyaWFibGUpIHtcclxuICAgICAgICAgICAgICAgIHZhbHVlID0gZm9ybVZhcmlhYmxlLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgaWYgKGZvcm1WYXJpYWJsZS50eXBlID09PSAnZGF0ZScpIHtcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZSArPSAnVDAwOjAwOjAwLjAwMFonO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0Rm9ybVZhcmlhYmxlcyhmb3JtOiBGb3JtTW9kZWwpOiBhbnlbXSB7XHJcbiAgICAgICAgcmV0dXJuICBmb3JtLmpzb24udmFyaWFibGVzO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0UHJvY2Vzc1ZhcmlhYmxlVmFsdWUobmFtZTogc3RyaW5nLCBwcm9jZXNzVmFyTGlzdDogVGFza1Byb2Nlc3NWYXJpYWJsZU1vZGVsW10pOiBzdHJpbmcge1xyXG4gICAgICAgIGlmIChwcm9jZXNzVmFyTGlzdCkge1xyXG4gICAgICAgICAgICBjb25zdCBwcm9jZXNzVmFyaWFibGUgPSBwcm9jZXNzVmFyTGlzdC5maW5kKCh2YXJpYWJsZSkgPT4gdmFyaWFibGUuaWQgPT09IG5hbWUpO1xyXG4gICAgICAgICAgICBpZiAocHJvY2Vzc1ZhcmlhYmxlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcHJvY2Vzc1ZhcmlhYmxlLnZhbHVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGV2YWx1YXRlTG9naWNhbE9wZXJhdGlvbihsb2dpY09wLCBwcmV2aW91c1ZhbHVlLCBuZXdWYWx1ZSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHN3aXRjaCAobG9naWNPcCkge1xyXG4gICAgICAgICAgICBjYXNlICdhbmQnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHByZXZpb3VzVmFsdWUgJiYgbmV3VmFsdWU7XHJcbiAgICAgICAgICAgIGNhc2UgJ29yJyA6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcHJldmlvdXNWYWx1ZSB8fCBuZXdWYWx1ZTtcclxuICAgICAgICAgICAgY2FzZSAnYW5kLW5vdCc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcHJldmlvdXNWYWx1ZSAmJiAhbmV3VmFsdWU7XHJcbiAgICAgICAgICAgIGNhc2UgJ29yLW5vdCc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcHJldmlvdXNWYWx1ZSB8fCAhbmV3VmFsdWU7XHJcbiAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZXJyb3IoJ05PIHZhbGlkIG9wZXJhdGlvbiEgd3Jvbmcgb3AgcmVxdWVzdCA6ICcgKyBsb2dpY09wKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBldmFsdWF0ZUNvbmRpdGlvbihsZWZ0VmFsdWUsIHJpZ2h0VmFsdWUsIG9wZXJhdG9yKTogYm9vbGVhbiB7XHJcbiAgICAgICAgc3dpdGNoIChvcGVyYXRvcikge1xyXG4gICAgICAgICAgICBjYXNlICc9PSc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbGVmdFZhbHVlICsgJycgPT09IHJpZ2h0VmFsdWUgKyAnJztcclxuICAgICAgICAgICAgY2FzZSAnPCc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbGVmdFZhbHVlIDwgcmlnaHRWYWx1ZTtcclxuICAgICAgICAgICAgY2FzZSAnIT0nOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGxlZnRWYWx1ZSArICcnICE9PSByaWdodFZhbHVlICsgJyc7XHJcbiAgICAgICAgICAgIGNhc2UgJz4nOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGxlZnRWYWx1ZSA+IHJpZ2h0VmFsdWU7XHJcbiAgICAgICAgICAgIGNhc2UgJz49JzpcclxuICAgICAgICAgICAgICAgIHJldHVybiBsZWZ0VmFsdWUgPj0gcmlnaHRWYWx1ZTtcclxuICAgICAgICAgICAgY2FzZSAnPD0nOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGxlZnRWYWx1ZSA8PSByaWdodFZhbHVlO1xyXG4gICAgICAgICAgICBjYXNlICdlbXB0eSc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbGVmdFZhbHVlID8gbGVmdFZhbHVlID09PSAnJyA6IHRydWU7XHJcbiAgICAgICAgICAgIGNhc2UgJyFlbXB0eSc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbGVmdFZhbHVlID8gbGVmdFZhbHVlICE9PSAnJyA6IGZhbHNlO1xyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKCdOTyB2YWxpZCBvcGVyYXRpb24hJyk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuO1xyXG4gICAgfVxyXG5cclxuICAgIGNsZWFuUHJvY2Vzc1ZhcmlhYmxlKCkge1xyXG4gICAgICAgIHRoaXMucHJvY2Vzc1Zhckxpc3QgPSBbXTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUYXNrUHJvY2Vzc1ZhcmlhYmxlKHRhc2tJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxUYXNrUHJvY2Vzc1ZhcmlhYmxlTW9kZWxbXT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmFjdGl2aXRpLnRhc2tGb3Jtc0FwaS5nZXRUYXNrRm9ybVZhcmlhYmxlcyh0YXNrSWQpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCgocmVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QganNvblJlcyA9IHRoaXMudG9Kc29uKHJlcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5wcm9jZXNzVmFyTGlzdCA9IDxUYXNrUHJvY2Vzc1ZhcmlhYmxlTW9kZWxbXT4ganNvblJlcztcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4ganNvblJlcztcclxuICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgdG9Kc29uKHJlczogYW55KTogYW55IHtcclxuICAgICAgICByZXR1cm4gcmVzIHx8IHt9O1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaGFuZGxlRXJyb3IoZXJyKSB7XHJcbiAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKCdFcnJvciB3aGlsZSBwZXJmb3JtaW5nIGEgY2FsbCcpO1xyXG4gICAgICAgIHJldHVybiB0aHJvd0Vycm9yKCdFcnJvciB3aGlsZSBwZXJmb3JtaW5nIGEgY2FsbCAtIFNlcnZlciBlcnJvcicpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==