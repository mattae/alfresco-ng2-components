/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
export class IconComponent {
    constructor() {
        this._value = '';
        this._isCustom = false;
    }
    /**
     * @return {?}
     */
    get value() {
        return this._value;
    }
    /**
     * Icon value, which can be either a ligature name or a custom icon in the format `[namespace]:[name]`.
     * @param {?} value
     * @return {?}
     */
    set value(value) {
        this._value = value || 'settings';
        this._isCustom = this._value.includes(':');
    }
    /**
     * @return {?}
     */
    get isCustom() {
        return this._isCustom;
    }
}
IconComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-icon',
                template: "<ng-container *ngIf=\"isCustom; else: default\">\r\n  <mat-icon [color]=\"color\" [svgIcon]=\"value\"></mat-icon>\r\n</ng-container>\r\n\r\n<ng-template #default>\r\n  <mat-icon [color]=\"color\">{{ value }}</mat-icon>\r\n</ng-template>\r\n",
                encapsulation: ViewEncapsulation.None,
                changeDetection: ChangeDetectionStrategy.OnPush,
                host: { class: 'adf-icon' },
                styles: [".adf-icon{display:inline-flex;vertical-align:middle}"]
            }] }
];
IconComponent.propDecorators = {
    color: [{ type: Input }],
    value: [{ type: Input }]
};
if (false) {
    /**
     * @type {?}
     * @private
     */
    IconComponent.prototype._value;
    /**
     * @type {?}
     * @private
     */
    IconComponent.prototype._isCustom;
    /**
     * Theme color palette for the component.
     * @type {?}
     */
    IconComponent.prototype.color;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWNvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJpY29uL2ljb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFDSCxTQUFTLEVBQ1QsS0FBSyxFQUNMLGlCQUFpQixFQUNqQix1QkFBdUIsRUFDMUIsTUFBTSxlQUFlLENBQUM7QUFXdkIsTUFBTSxPQUFPLGFBQWE7SUFSMUI7UUFTWSxXQUFNLEdBQUcsRUFBRSxDQUFDO1FBQ1osY0FBUyxHQUFHLEtBQUssQ0FBQztJQW9COUIsQ0FBQzs7OztJQWRHLElBQUksS0FBSztRQUNMLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUN2QixDQUFDOzs7Ozs7SUFHRCxJQUNJLEtBQUssQ0FBQyxLQUFhO1FBQ25CLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxJQUFJLFVBQVUsQ0FBQztRQUNsQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQy9DLENBQUM7Ozs7SUFFRCxJQUFJLFFBQVE7UUFDUixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDMUIsQ0FBQzs7O1lBN0JKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsVUFBVTtnQkFDcEIsNFBBQW9DO2dCQUVwQyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtnQkFDckMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07Z0JBQy9DLElBQUksRUFBRSxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUU7O2FBQzlCOzs7b0JBTUksS0FBSztvQkFRTCxLQUFLOzs7Ozs7O0lBWk4sK0JBQW9COzs7OztJQUNwQixrQ0FBMEI7Ozs7O0lBRzFCLDhCQUNvQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQge1xyXG4gICAgQ29tcG9uZW50LFxyXG4gICAgSW5wdXQsXHJcbiAgICBWaWV3RW5jYXBzdWxhdGlvbixcclxuICAgIENoYW5nZURldGVjdGlvblN0cmF0ZWd5XHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFRoZW1lUGFsZXR0ZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtaWNvbicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vaWNvbi5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9pY29uLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gICAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXHJcbiAgICBob3N0OiB7IGNsYXNzOiAnYWRmLWljb24nIH1cclxufSlcclxuZXhwb3J0IGNsYXNzIEljb25Db21wb25lbnQge1xyXG4gICAgcHJpdmF0ZSBfdmFsdWUgPSAnJztcclxuICAgIHByaXZhdGUgX2lzQ3VzdG9tID0gZmFsc2U7XHJcblxyXG4gICAgLyoqIFRoZW1lIGNvbG9yIHBhbGV0dGUgZm9yIHRoZSBjb21wb25lbnQuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgY29sb3I6IFRoZW1lUGFsZXR0ZTtcclxuXHJcbiAgICBnZXQgdmFsdWUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqIEljb24gdmFsdWUsIHdoaWNoIGNhbiBiZSBlaXRoZXIgYSBsaWdhdHVyZSBuYW1lIG9yIGEgY3VzdG9tIGljb24gaW4gdGhlIGZvcm1hdCBgW25hbWVzcGFjZV06W25hbWVdYC4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzZXQgdmFsdWUodmFsdWU6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX3ZhbHVlID0gdmFsdWUgfHwgJ3NldHRpbmdzJztcclxuICAgICAgICB0aGlzLl9pc0N1c3RvbSA9IHRoaXMuX3ZhbHVlLmluY2x1ZGVzKCc6Jyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGlzQ3VzdG9tKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9pc0N1c3RvbTtcclxuICAgIH1cclxufVxyXG4iXX0=