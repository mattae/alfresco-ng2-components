/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ContentChildren, EventEmitter, Input, Output, QueryList, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
export class InfoDrawerTabComponent {
    constructor() {
        /**
         * The title of the tab (string or translation key).
         */
        this.label = '';
        /**
         * Icon to render for the tab.
         */
        this.icon = null;
    }
}
InfoDrawerTabComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-info-drawer-tab',
                template: '<ng-template><ng-content></ng-content></ng-template>'
            }] }
];
InfoDrawerTabComponent.propDecorators = {
    label: [{ type: Input }],
    icon: [{ type: Input }],
    content: [{ type: ViewChild, args: [TemplateRef, { static: true },] }]
};
if (false) {
    /**
     * The title of the tab (string or translation key).
     * @type {?}
     */
    InfoDrawerTabComponent.prototype.label;
    /**
     * Icon to render for the tab.
     * @type {?}
     */
    InfoDrawerTabComponent.prototype.icon;
    /** @type {?} */
    InfoDrawerTabComponent.prototype.content;
}
export class InfoDrawerComponent {
    constructor() {
        /**
         * The title of the info drawer (string or translation key).
         */
        this.title = null;
        /**
         * The selected index tab.
         */
        this.selectedIndex = 0;
        /**
         * Emitted when the currently active tab changes.
         */
        this.currentTab = new EventEmitter();
    }
    /**
     * @return {?}
     */
    showTabLayout() {
        return this.contentBlocks.length > 0;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onTabChange(event) {
        this.currentTab.emit(event.index);
    }
}
InfoDrawerComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-info-drawer',
                template: "<adf-info-drawer-layout>\r\n    <div *ngIf=\"title\" info-drawer-title>{{ title | translate }}</div>\r\n    <ng-content *ngIf=\"!title\" info-drawer-title select=\"[info-drawer-title]\"></ng-content>\r\n\r\n    <ng-content info-drawer-buttons select=\"[info-drawer-buttons]\"></ng-content>\r\n\r\n    <ng-container info-drawer-content *ngIf=\"showTabLayout(); then tabLayout else singleLayout\"></ng-container>\r\n\r\n    <ng-template #tabLayout>\r\n        <mat-tab-group [(selectedIndex)]=\"selectedIndex\" class=\"adf-info-drawer-tabs\" (selectedTabChange)=\"onTabChange($event)\">\r\n            <mat-tab *ngFor=\"let contentBlock of contentBlocks\"\r\n                [label]=\"contentBlock.label | translate\"\r\n                class=\"adf-info-drawer-tab\">\r\n\r\n                <ng-template mat-tab-label>\r\n                    <mat-icon *ngIf=\"contentBlock.icon\">{{ contentBlock.icon }}</mat-icon>\r\n                    <span *ngIf=\"contentBlock.label\">{{ contentBlock.label | translate }}</span>\r\n                </ng-template>\r\n\r\n                <ng-container *ngTemplateOutlet=\"contentBlock.content\"></ng-container>\r\n            </mat-tab>\r\n        </mat-tab-group>\r\n    </ng-template>\r\n\r\n    <ng-template #singleLayout>\r\n        <ng-content select=\"[info-drawer-content]\"></ng-content>\r\n    </ng-template>\r\n</adf-info-drawer-layout>\r\n",
                encapsulation: ViewEncapsulation.None,
                host: { 'class': 'adf-info-drawer' },
                styles: [".adf-info-drawer{display:block}.adf-info-drawer .mat-tab-label{min-width:0}.adf-info-drawer .adf-info-drawer-layout-content{padding:0}.adf-info-drawer .adf-info-drawer-layout-content>:not(.adf-info-drawer-tabs){padding:10px}.adf-info-drawer .adf-info-drawer-layout-content .adf-info-drawer-tabs .mat-tab-body-content>*,.adf-info-drawer .adf-info-drawer-layout-content>:not(.adf-info-drawer-tabs)>*{margin-bottom:20px;display:block}.adf-info-drawer .adf-info-drawer-layout-content .adf-info-drawer-tabs .mat-tab-body-content>:last-child{margin-bottom:0}.adf-info-drawer .adf-info-drawer-layout-content .adf-info-drawer-tabs .mat-tab-label{flex-grow:1}.adf-info-drawer .adf-info-drawer-layout-content .adf-info-drawer-tabs .mat-tab-label .mat-icon+span{padding-left:5px}.adf-info-drawer .adf-info-drawer-layout-content .adf-info-drawer-tabs .mat-ink-bar{height:4px}.adf-info-drawer .adf-info-drawer-layout-content .adf-info-drawer-tabs .mat-tab-body{padding:10px}.adf-info-drawer .adf-info-drawer-layout-content .adf-info-drawer-tabs .mat-tab-body-content{overflow:initial}"]
            }] }
];
InfoDrawerComponent.propDecorators = {
    title: [{ type: Input }],
    selectedIndex: [{ type: Input }],
    currentTab: [{ type: Output }],
    contentBlocks: [{ type: ContentChildren, args: [InfoDrawerTabComponent,] }]
};
if (false) {
    /**
     * The title of the info drawer (string or translation key).
     * @type {?}
     */
    InfoDrawerComponent.prototype.title;
    /**
     * The selected index tab.
     * @type {?}
     */
    InfoDrawerComponent.prototype.selectedIndex;
    /**
     * Emitted when the currently active tab changes.
     * @type {?}
     */
    InfoDrawerComponent.prototype.currentTab;
    /** @type {?} */
    InfoDrawerComponent.prototype.contentBlocks;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5mby1kcmF3ZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiaW5mby1kcmF3ZXIvaW5mby1kcmF3ZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxPQUFPLEVBQUUsU0FBUyxFQUFFLGVBQWUsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQU05SSxNQUFNLE9BQU8sc0JBQXNCO0lBSm5DOzs7O1FBT0ksVUFBSyxHQUFXLEVBQUUsQ0FBQzs7OztRQUluQixTQUFJLEdBQVcsSUFBSSxDQUFDO0lBSXhCLENBQUM7OztZQWZBLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUscUJBQXFCO2dCQUMvQixRQUFRLEVBQUUsc0RBQXNEO2FBQ25FOzs7b0JBR0ksS0FBSzttQkFJTCxLQUFLO3NCQUdMLFNBQVMsU0FBQyxXQUFXLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDOzs7Ozs7O0lBUHRDLHVDQUNtQjs7Ozs7SUFHbkIsc0NBQ29COztJQUVwQix5Q0FDMEI7O0FBVTlCLE1BQU0sT0FBTyxtQkFBbUI7SUFQaEM7Ozs7UUFVSSxVQUFLLEdBQWdCLElBQUksQ0FBQzs7OztRQUkxQixrQkFBYSxHQUFXLENBQUMsQ0FBQzs7OztRQUkxQixlQUFVLEdBQXlCLElBQUksWUFBWSxFQUFVLENBQUM7SUFZbEUsQ0FBQzs7OztJQVBHLGFBQWE7UUFDVCxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxLQUF3QjtRQUNoQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7O1lBN0JKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsaUJBQWlCO2dCQUMzQixvM0NBQTJDO2dCQUUzQyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtnQkFDckMsSUFBSSxFQUFFLEVBQUUsT0FBTyxFQUFFLGlCQUFpQixFQUFFOzthQUN2Qzs7O29CQUdJLEtBQUs7NEJBSUwsS0FBSzt5QkFJTCxNQUFNOzRCQUdOLGVBQWUsU0FBQyxzQkFBc0I7Ozs7Ozs7SUFYdkMsb0NBQzBCOzs7OztJQUcxQiw0Q0FDMEI7Ozs7O0lBRzFCLHlDQUM4RDs7SUFFOUQsNENBQ2lEIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXG5cbi8qIVxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXG4gKlxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuICpcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbiAqXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxuICovXG5cbmltcG9ydCB7IENvbXBvbmVudCwgQ29udGVudENoaWxkcmVuLCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQsIFF1ZXJ5TGlzdCwgVGVtcGxhdGVSZWYsIFZpZXdDaGlsZCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1hdFRhYkNoYW5nZUV2ZW50IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdhZGYtaW5mby1kcmF3ZXItdGFiJyxcbiAgICB0ZW1wbGF0ZTogJzxuZy10ZW1wbGF0ZT48bmctY29udGVudD48L25nLWNvbnRlbnQ+PC9uZy10ZW1wbGF0ZT4nXG59KVxuZXhwb3J0IGNsYXNzIEluZm9EcmF3ZXJUYWJDb21wb25lbnQge1xuICAgIC8qKiBUaGUgdGl0bGUgb2YgdGhlIHRhYiAoc3RyaW5nIG9yIHRyYW5zbGF0aW9uIGtleSkuICovXG4gICAgQElucHV0KClcbiAgICBsYWJlbDogc3RyaW5nID0gJyc7XG5cbiAgICAvKiogSWNvbiB0byByZW5kZXIgZm9yIHRoZSB0YWIuICovXG4gICAgQElucHV0KClcbiAgICBpY29uOiBzdHJpbmcgPSBudWxsO1xuXG4gICAgQFZpZXdDaGlsZChUZW1wbGF0ZVJlZiwge3N0YXRpYzogdHJ1ZX0pXG4gICAgY29udGVudDogVGVtcGxhdGVSZWY8YW55Pjtcbn1cblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdhZGYtaW5mby1kcmF3ZXInLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9pbmZvLWRyYXdlci5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vaW5mby1kcmF3ZXIuY29tcG9uZW50LnNjc3MnXSxcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxuICAgIGhvc3Q6IHsgJ2NsYXNzJzogJ2FkZi1pbmZvLWRyYXdlcicgfVxufSlcbmV4cG9ydCBjbGFzcyBJbmZvRHJhd2VyQ29tcG9uZW50IHtcbiAgICAvKiogVGhlIHRpdGxlIG9mIHRoZSBpbmZvIGRyYXdlciAoc3RyaW5nIG9yIHRyYW5zbGF0aW9uIGtleSkuICovXG4gICAgQElucHV0KClcbiAgICB0aXRsZTogc3RyaW5nfG51bGwgPSBudWxsO1xuXG4gICAgLyoqIFRoZSBzZWxlY3RlZCBpbmRleCB0YWIuICovXG4gICAgQElucHV0KClcbiAgICBzZWxlY3RlZEluZGV4OiBudW1iZXIgPSAwO1xuXG4gICAgLyoqIEVtaXR0ZWQgd2hlbiB0aGUgY3VycmVudGx5IGFjdGl2ZSB0YWIgY2hhbmdlcy4gKi9cbiAgICBAT3V0cHV0KClcbiAgICBjdXJyZW50VGFiOiBFdmVudEVtaXR0ZXI8bnVtYmVyPiA9IG5ldyBFdmVudEVtaXR0ZXI8bnVtYmVyPigpO1xuXG4gICAgQENvbnRlbnRDaGlsZHJlbihJbmZvRHJhd2VyVGFiQ29tcG9uZW50KVxuICAgIGNvbnRlbnRCbG9ja3M6IFF1ZXJ5TGlzdDxJbmZvRHJhd2VyVGFiQ29tcG9uZW50PjtcblxuICAgIHNob3dUYWJMYXlvdXQoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbnRlbnRCbG9ja3MubGVuZ3RoID4gMDtcbiAgICB9XG5cbiAgICBvblRhYkNoYW5nZShldmVudDogTWF0VGFiQ2hhbmdlRXZlbnQpIHtcbiAgICAgICAgdGhpcy5jdXJyZW50VGFiLmVtaXQoZXZlbnQuaW5kZXgpO1xuICAgIH1cbn1cbiJdfQ==