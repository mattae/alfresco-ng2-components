/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../material.module';
import { InfoDrawerLayoutComponent, InfoDrawerTitleDirective, InfoDrawerButtonsDirective, InfoDrawerContentDirective } from './info-drawer-layout.component';
import { InfoDrawerComponent, InfoDrawerTabComponent } from './info-drawer.component';
import { TranslateModule } from '@ngx-translate/core';
/**
 * @return {?}
 */
export function declarations() {
    return [
        InfoDrawerLayoutComponent,
        InfoDrawerTabComponent,
        InfoDrawerComponent,
        InfoDrawerTitleDirective,
        InfoDrawerButtonsDirective,
        InfoDrawerContentDirective
    ];
}
export class InfoDrawerModule {
}
InfoDrawerModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    MaterialModule,
                    TranslateModule.forChild()
                ],
                declarations: declarations(),
                exports: declarations()
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5mby1kcmF3ZXIubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiaW5mby1kcmF3ZXIvaW5mby1kcmF3ZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUVwRCxPQUFPLEVBQUUseUJBQXlCLEVBQUUsd0JBQXdCLEVBQUUsMEJBQTBCLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUM3SixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN0RixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7Ozs7QUFFdEQsTUFBTSxVQUFVLFlBQVk7SUFDeEIsT0FBTztRQUNILHlCQUF5QjtRQUN6QixzQkFBc0I7UUFDdEIsbUJBQW1CO1FBQ25CLHdCQUF3QjtRQUN4QiwwQkFBMEI7UUFDMUIsMEJBQTBCO0tBQzdCLENBQUM7QUFDTixDQUFDO0FBV0QsTUFBTSxPQUFPLGdCQUFnQjs7O1lBVDVCLFFBQVEsU0FBQztnQkFDTixPQUFPLEVBQUU7b0JBQ0wsWUFBWTtvQkFDWixjQUFjO29CQUNkLGVBQWUsQ0FBQyxRQUFRLEVBQUU7aUJBQzdCO2dCQUNELFlBQVksRUFBRSxZQUFZLEVBQUU7Z0JBQzVCLE9BQU8sRUFBRSxZQUFZLEVBQUU7YUFDMUIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuLi9tYXRlcmlhbC5tb2R1bGUnO1xyXG5cclxuaW1wb3J0IHsgSW5mb0RyYXdlckxheW91dENvbXBvbmVudCwgSW5mb0RyYXdlclRpdGxlRGlyZWN0aXZlLCBJbmZvRHJhd2VyQnV0dG9uc0RpcmVjdGl2ZSwgSW5mb0RyYXdlckNvbnRlbnREaXJlY3RpdmUgfSBmcm9tICcuL2luZm8tZHJhd2VyLWxheW91dC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBJbmZvRHJhd2VyQ29tcG9uZW50LCBJbmZvRHJhd2VyVGFiQ29tcG9uZW50IH0gZnJvbSAnLi9pbmZvLWRyYXdlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVNb2R1bGUgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuXHJcbmV4cG9ydCBmdW5jdGlvbiBkZWNsYXJhdGlvbnMoKSB7XHJcbiAgICByZXR1cm4gW1xyXG4gICAgICAgIEluZm9EcmF3ZXJMYXlvdXRDb21wb25lbnQsXHJcbiAgICAgICAgSW5mb0RyYXdlclRhYkNvbXBvbmVudCxcclxuICAgICAgICBJbmZvRHJhd2VyQ29tcG9uZW50LFxyXG4gICAgICAgIEluZm9EcmF3ZXJUaXRsZURpcmVjdGl2ZSxcclxuICAgICAgICBJbmZvRHJhd2VyQnV0dG9uc0RpcmVjdGl2ZSxcclxuICAgICAgICBJbmZvRHJhd2VyQ29udGVudERpcmVjdGl2ZVxyXG4gICAgXTtcclxufVxyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICAgICAgVHJhbnNsYXRlTW9kdWxlLmZvckNoaWxkKClcclxuICAgIF0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IGRlY2xhcmF0aW9ucygpLFxyXG4gICAgZXhwb3J0czogZGVjbGFyYXRpb25zKClcclxufSlcclxuZXhwb3J0IGNsYXNzIEluZm9EcmF3ZXJNb2R1bGUge31cclxuIl19