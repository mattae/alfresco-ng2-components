/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @record
 */
export function AbstractAuthentication() { }
if (false) {
    /** @type {?} */
    AbstractAuthentication.prototype.TYPE;
    /** @type {?} */
    AbstractAuthentication.prototype.alfrescoApi;
    /**
     * @param {?} username
     * @param {?} password
     * @return {?}
     */
    AbstractAuthentication.prototype.login = function (username, password) { };
    /**
     * @return {?}
     */
    AbstractAuthentication.prototype.logout = function () { };
    /**
     * @return {?}
     */
    AbstractAuthentication.prototype.isLoggedIn = function () { };
    /**
     * @return {?}
     */
    AbstractAuthentication.prototype.getTicket = function () { };
    /**
     * @param {?} ticket
     * @return {?}
     */
    AbstractAuthentication.prototype.saveTicket = function (ticket) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24uaW50ZXJmYWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiaW50ZXJmYWNlL2F1dGhlbnRpY2F0aW9uLmludGVyZmFjZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSw0Q0FhQzs7O0lBWkcsc0NBQWE7O0lBQ2IsNkNBQWlCOzs7Ozs7SUFFakIsMkVBQTJEOzs7O0lBRTNELDBEQUEwQjs7OztJQUUxQiw4REFBdUI7Ozs7SUFFdkIsNkRBQW9COzs7OztJQUVwQixvRUFBOEIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBBYnN0cmFjdEF1dGhlbnRpY2F0aW9uIHtcclxuICAgIFRZUEU6IHN0cmluZztcclxuICAgIGFsZnJlc2NvQXBpOiBhbnk7XHJcblxyXG4gICAgbG9naW4odXNlcm5hbWU6IHN0cmluZywgcGFzc3dvcmQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PjtcclxuXHJcbiAgICBsb2dvdXQoKTogT2JzZXJ2YWJsZTxhbnk+O1xyXG5cclxuICAgIGlzTG9nZ2VkSW4oKTogYm9vbGVhbiA7XHJcblxyXG4gICAgZ2V0VGlja2V0KCk6IHN0cmluZztcclxuXHJcbiAgICBzYXZlVGlja2V0KHRpY2tldDogYW55KTogdm9pZDtcclxufVxyXG4iXX0=