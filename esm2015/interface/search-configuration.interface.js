/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @record
 */
export function SearchConfigurationInterface() { }
if (false) {
    /**
     * Generates a QueryBody object with custom search parameters.
     * @param {?} searchTerm Term text to search for
     * @param {?} maxResults Maximum number of search results to show in a page
     * @param {?} skipCount The offset of the start of the page within the results list
     * @return {?} Query body defined by the parameters
     */
    SearchConfigurationInterface.prototype.generateQueryBody = function (searchTerm, maxResults, skipCount) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWNvbmZpZ3VyYXRpb24uaW50ZXJmYWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiaW50ZXJmYWNlL3NlYXJjaC1jb25maWd1cmF0aW9uLmludGVyZmFjZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxrREFXQzs7Ozs7Ozs7O0lBRkcsNEdBQXdGIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IFF1ZXJ5Qm9keSB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBTZWFyY2hDb25maWd1cmF0aW9uSW50ZXJmYWNlIHtcclxuXHJcbiAgICAvKipcclxuICAgICAqIEdlbmVyYXRlcyBhIFF1ZXJ5Qm9keSBvYmplY3Qgd2l0aCBjdXN0b20gc2VhcmNoIHBhcmFtZXRlcnMuXHJcbiAgICAgKiBAcGFyYW0gc2VhcmNoVGVybSBUZXJtIHRleHQgdG8gc2VhcmNoIGZvclxyXG4gICAgICogQHBhcmFtIG1heFJlc3VsdHMgTWF4aW11bSBudW1iZXIgb2Ygc2VhcmNoIHJlc3VsdHMgdG8gc2hvdyBpbiBhIHBhZ2VcclxuICAgICAqIEBwYXJhbSBza2lwQ291bnQgVGhlIG9mZnNldCBvZiB0aGUgc3RhcnQgb2YgdGhlIHBhZ2Ugd2l0aGluIHRoZSByZXN1bHRzIGxpc3RcclxuICAgICAqIEByZXR1cm5zIFF1ZXJ5IGJvZHkgZGVmaW5lZCBieSB0aGUgcGFyYW1ldGVyc1xyXG4gICAgICovXHJcbiAgICBnZW5lcmF0ZVF1ZXJ5Qm9keShzZWFyY2hUZXJtOiBzdHJpbmcsIG1heFJlc3VsdHM6IG51bWJlciwgc2tpcENvdW50OiBudW1iZXIpOiBRdWVyeUJvZHk7XHJcblxyXG59XHJcbiJdfQ==