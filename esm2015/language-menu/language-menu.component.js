/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component } from '@angular/core';
import { AppConfigService, AppConfigValues } from '../app-config/app-config.service';
import { UserPreferencesService } from '../services/user-preferences.service';
export class LanguageMenuComponent {
    /**
     * @param {?} appConfig
     * @param {?} userPreference
     */
    constructor(appConfig, userPreference) {
        this.appConfig = appConfig;
        this.userPreference = userPreference;
        this.languages = [
            { key: 'en', label: 'English' }
        ];
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        /** @type {?} */
        const languagesConfigApp = this.appConfig.get(AppConfigValues.APP_CONFIG_LANGUAGES_KEY);
        if (languagesConfigApp) {
            this.languages = languagesConfigApp;
        }
    }
    /**
     * @param {?} language
     * @return {?}
     */
    changeLanguage(language) {
        this.userPreference.locale = language.key;
        this.userPreference.set('textOrientation', language.direction || 'ltr');
    }
}
LanguageMenuComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-language-menu',
                template: "<button mat-menu-item *ngFor=\"let language of languages\" (click)=\"changeLanguage(language)\">{{language.label}}\r\n</button>\r\n"
            }] }
];
/** @nocollapse */
LanguageMenuComponent.ctorParameters = () => [
    { type: AppConfigService },
    { type: UserPreferencesService }
];
if (false) {
    /** @type {?} */
    LanguageMenuComponent.prototype.languages;
    /**
     * @type {?}
     * @private
     */
    LanguageMenuComponent.prototype.appConfig;
    /**
     * @type {?}
     * @private
     */
    LanguageMenuComponent.prototype.userPreference;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGFuZ3VhZ2UtbWVudS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJsYW5ndWFnZS1tZW51L2xhbmd1YWdlLW1lbnUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDbEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLGVBQWUsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JGLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBTzlFLE1BQU0sT0FBTyxxQkFBcUI7Ozs7O0lBTTlCLFlBQ1ksU0FBMkIsRUFDM0IsY0FBc0M7UUFEdEMsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFDM0IsbUJBQWMsR0FBZCxjQUFjLENBQXdCO1FBTmxELGNBQVMsR0FBd0I7WUFDN0IsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUM7U0FDakMsQ0FBQztJQUtGLENBQUM7Ozs7SUFFRCxRQUFROztjQUNFLGtCQUFrQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFzQixlQUFlLENBQUMsd0JBQXdCLENBQUM7UUFDNUcsSUFBSSxrQkFBa0IsRUFBRTtZQUNwQixJQUFJLENBQUMsU0FBUyxHQUFHLGtCQUFrQixDQUFDO1NBQ3ZDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxjQUFjLENBQUMsUUFBc0I7UUFDakMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQztRQUMxQyxJQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxRQUFRLENBQUMsU0FBUyxJQUFJLEtBQUssQ0FBQyxDQUFDO0lBQzVFLENBQUM7OztZQXpCSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLG1CQUFtQjtnQkFDN0IsK0lBQTJDO2FBQzlDOzs7O1lBUFEsZ0JBQWdCO1lBQ2hCLHNCQUFzQjs7OztJQVMzQiwwQ0FFRTs7Ozs7SUFHRSwwQ0FBbUM7Ozs7O0lBQ25DLCtDQUE4QyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBcHBDb25maWdTZXJ2aWNlLCBBcHBDb25maWdWYWx1ZXMgfSBmcm9tICcuLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy91c2VyLXByZWZlcmVuY2VzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMYW5ndWFnZUl0ZW0gfSBmcm9tICcuL2xhbmd1YWdlLmludGVyZmFjZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLWxhbmd1YWdlLW1lbnUnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICdsYW5ndWFnZS1tZW51LmNvbXBvbmVudC5odG1sJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTGFuZ3VhZ2VNZW51Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBsYW5ndWFnZXM6IEFycmF5PExhbmd1YWdlSXRlbT4gPSBbXHJcbiAgICAgICAgeyBrZXk6ICdlbicsIGxhYmVsOiAnRW5nbGlzaCd9XHJcbiAgICBdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgYXBwQ29uZmlnOiBBcHBDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgdXNlclByZWZlcmVuY2U6IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBjb25zdCBsYW5ndWFnZXNDb25maWdBcHAgPSB0aGlzLmFwcENvbmZpZy5nZXQ8QXJyYXk8TGFuZ3VhZ2VJdGVtPj4oQXBwQ29uZmlnVmFsdWVzLkFQUF9DT05GSUdfTEFOR1VBR0VTX0tFWSk7XHJcbiAgICAgICAgaWYgKGxhbmd1YWdlc0NvbmZpZ0FwcCkge1xyXG4gICAgICAgICAgICB0aGlzLmxhbmd1YWdlcyA9IGxhbmd1YWdlc0NvbmZpZ0FwcDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY2hhbmdlTGFuZ3VhZ2UobGFuZ3VhZ2U6IExhbmd1YWdlSXRlbSkge1xyXG4gICAgICAgIHRoaXMudXNlclByZWZlcmVuY2UubG9jYWxlID0gbGFuZ3VhZ2Uua2V5O1xyXG4gICAgICAgIHRoaXMudXNlclByZWZlcmVuY2Uuc2V0KCd0ZXh0T3JpZW50YXRpb24nLCBsYW5ndWFnZS5kaXJlY3Rpb24gfHwgJ2x0cicpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==