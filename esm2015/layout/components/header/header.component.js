/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
export class HeaderLayoutComponent {
    constructor() {
        /**
         * The router link for the application logo, when clicked.
         */
        this.redirectUrl = '/';
        /**
         * Toggles whether the sidenav button will be displayed in the header
         * or not.
         */
        this.showSidenavToggle = true;
        /**
         * Emitted when the sidenav button is clicked.
         */
        this.clicked = new EventEmitter();
        /**
         * The side of the page that the drawer is attached to (can be 'start' or 'end')
         */
        this.position = 'start';
    }
    /**
     * @return {?}
     */
    toggleMenu() {
        this.clicked.emit(true);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (!this.logo) {
            this.logo = './assets/images/logo.png';
        }
    }
}
HeaderLayoutComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-layout-header',
                template: "<mat-toolbar\r\n    [color]=\"color\"\r\n    [style.background-color]=\"color\"\r\n    role=\"heading\"\r\n    aria-level=\"1\">\r\n    <button\r\n        *ngIf=\"showSidenavToggle && position === 'start'\"\r\n        id=\"adf-sidebar-toggle-start\"\r\n        data-automation-id=\"adf-menu-icon\"\r\n        class=\"mat-icon-button adf-menu-icon\"\r\n        mat-icon-button\r\n        (click)=\"toggleMenu()\"\r\n        aria-label=\"Toggle Menu\">\r\n        <mat-icon\r\n            class=\"mat-icon material-icon\"\r\n            role=\"img\"\r\n            aria-hidden=\"true\">menu</mat-icon>\r\n    </button>\r\n\r\n    <a [routerLink]=\"redirectUrl\" title=\"{{ tooltip }}\">\r\n        <img\r\n            src=\"{{ logo }}\"\r\n            class=\"adf-app-logo\"\r\n            alt=\"{{ 'CORE.HEADER.LOGO_ARIA' | translate }}\"\r\n        />\r\n    </a>\r\n\r\n    <span\r\n        [routerLink]=\"redirectUrl\"\r\n        fxFlex=\"1 1 auto\"\r\n        fxShow\r\n        fxHide.lt-sm=\"true\"\r\n        class=\"adf-app-title\"\r\n        >{{ title }}</span>\r\n    <ng-content></ng-content>\r\n\r\n    <button\r\n        *ngIf=\"showSidenavToggle && position === 'end'\"\r\n        id=\"adf-sidebar-toggle-end\"\r\n        data-automation-id=\"adf-menu-icon\"\r\n        class=\"mat-icon-button adf-menu-icon\"\r\n        mat-icon-button\r\n        (click)=\"toggleMenu()\"\r\n        aria-label=\"Toggle Menu\">\r\n        <mat-icon\r\n            class=\"mat-icon material-icon\"\r\n            role=\"img\"\r\n            aria-hidden=\"true\">menu</mat-icon>\r\n    </button>\r\n</mat-toolbar>\r\n",
                encapsulation: ViewEncapsulation.None,
                host: { class: 'adf-layout-header' }
            }] }
];
HeaderLayoutComponent.propDecorators = {
    title: [{ type: Input }],
    logo: [{ type: Input }],
    redirectUrl: [{ type: Input }],
    tooltip: [{ type: Input }],
    color: [{ type: Input }],
    showSidenavToggle: [{ type: Input }],
    clicked: [{ type: Output }],
    position: [{ type: Input }]
};
if (false) {
    /**
     * Title of the application.
     * @type {?}
     */
    HeaderLayoutComponent.prototype.title;
    /**
     * Path to an image file for the application logo.
     * @type {?}
     */
    HeaderLayoutComponent.prototype.logo;
    /**
     * The router link for the application logo, when clicked.
     * @type {?}
     */
    HeaderLayoutComponent.prototype.redirectUrl;
    /**
     * The tooltip text for the application logo.
     * @type {?}
     */
    HeaderLayoutComponent.prototype.tooltip;
    /**
     * Background color for the header. It can be any hex color code or one
     * of the Material theme colors: 'primary', 'accent' or 'warn'.
     * @type {?}
     */
    HeaderLayoutComponent.prototype.color;
    /**
     * Toggles whether the sidenav button will be displayed in the header
     * or not.
     * @type {?}
     */
    HeaderLayoutComponent.prototype.showSidenavToggle;
    /**
     * Emitted when the sidenav button is clicked.
     * @type {?}
     */
    HeaderLayoutComponent.prototype.clicked;
    /**
     * The side of the page that the drawer is attached to (can be 'start' or 'end')
     * @type {?}
     */
    HeaderLayoutComponent.prototype.position;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImxheW91dC9jb21wb25lbnRzL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsaUJBQWlCLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFRbEcsTUFBTSxPQUFPLHFCQUFxQjtJQU5sQzs7OztRQWNhLGdCQUFXLEdBQW1CLEdBQUcsQ0FBQzs7Ozs7UUFlbEMsc0JBQWlCLEdBQVksSUFBSSxDQUFDOzs7O1FBR2pDLFlBQU8sR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDOzs7O1FBR25DLGFBQVEsR0FBRyxPQUFPLENBQUM7SUFXaEMsQ0FBQzs7OztJQVRHLFVBQVU7UUFDTixJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFO1lBQ1osSUFBSSxDQUFDLElBQUksR0FBRywwQkFBMEIsQ0FBQztTQUMxQztJQUNMLENBQUM7OztZQTdDSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLG1CQUFtQjtnQkFDN0IsdWxEQUFzQztnQkFDdEMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7Z0JBQ3JDLElBQUksRUFBRSxFQUFFLEtBQUssRUFBRSxtQkFBbUIsRUFBRTthQUN2Qzs7O29CQUdJLEtBQUs7bUJBR0wsS0FBSzswQkFHTCxLQUFLO3NCQUdMLEtBQUs7b0JBTUwsS0FBSztnQ0FNTCxLQUFLO3NCQUdMLE1BQU07dUJBR04sS0FBSzs7Ozs7OztJQTNCTixzQ0FBdUI7Ozs7O0lBR3ZCLHFDQUFzQjs7Ozs7SUFHdEIsNENBQTJDOzs7OztJQUczQyx3Q0FBeUI7Ozs7OztJQU16QixzQ0FBdUI7Ozs7OztJQU12QixrREFBMkM7Ozs7O0lBRzNDLHdDQUE0Qzs7Ozs7SUFHNUMseUNBQTRCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE91dHB1dCwgRXZlbnRFbWl0dGVyLCBWaWV3RW5jYXBzdWxhdGlvbiwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLWxheW91dC1oZWFkZXInLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2hlYWRlci5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gICAgaG9zdDogeyBjbGFzczogJ2FkZi1sYXlvdXQtaGVhZGVyJyB9XHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBIZWFkZXJMYXlvdXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgLyoqIFRpdGxlIG9mIHRoZSBhcHBsaWNhdGlvbi4gKi9cclxuICAgIEBJbnB1dCgpIHRpdGxlOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqIFBhdGggdG8gYW4gaW1hZ2UgZmlsZSBmb3IgdGhlIGFwcGxpY2F0aW9uIGxvZ28uICovXHJcbiAgICBASW5wdXQoKSBsb2dvOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqIFRoZSByb3V0ZXIgbGluayBmb3IgdGhlIGFwcGxpY2F0aW9uIGxvZ28sIHdoZW4gY2xpY2tlZC4gKi9cclxuICAgIEBJbnB1dCgpIHJlZGlyZWN0VXJsOiBzdHJpbmcgfCBhbnlbXSA9ICcvJztcclxuXHJcbiAgICAvKiogVGhlIHRvb2x0aXAgdGV4dCBmb3IgdGhlIGFwcGxpY2F0aW9uIGxvZ28uICovXHJcbiAgICBASW5wdXQoKSB0b29sdGlwOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBCYWNrZ3JvdW5kIGNvbG9yIGZvciB0aGUgaGVhZGVyLiBJdCBjYW4gYmUgYW55IGhleCBjb2xvciBjb2RlIG9yIG9uZVxyXG4gICAgICogb2YgdGhlIE1hdGVyaWFsIHRoZW1lIGNvbG9yczogJ3ByaW1hcnknLCAnYWNjZW50JyBvciAnd2FybicuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIGNvbG9yOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUb2dnbGVzIHdoZXRoZXIgdGhlIHNpZGVuYXYgYnV0dG9uIHdpbGwgYmUgZGlzcGxheWVkIGluIHRoZSBoZWFkZXJcclxuICAgICAqIG9yIG5vdC5cclxuICAgICAqL1xyXG4gICAgQElucHV0KCkgc2hvd1NpZGVuYXZUb2dnbGU6IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIHNpZGVuYXYgYnV0dG9uIGlzIGNsaWNrZWQuICovXHJcbiAgICBAT3V0cHV0KCkgY2xpY2tlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIC8qKiBUaGUgc2lkZSBvZiB0aGUgcGFnZSB0aGF0IHRoZSBkcmF3ZXIgaXMgYXR0YWNoZWQgdG8gKGNhbiBiZSAnc3RhcnQnIG9yICdlbmQnKSAqL1xyXG4gICAgQElucHV0KCkgcG9zaXRpb24gPSAnc3RhcnQnO1xyXG5cclxuICAgIHRvZ2dsZU1lbnUoKSB7XHJcbiAgICAgICAgdGhpcy5jbGlja2VkLmVtaXQodHJ1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLmxvZ28pIHtcclxuICAgICAgICAgICAgdGhpcy5sb2dvID0gJy4vYXNzZXRzL2ltYWdlcy9sb2dvLnBuZyc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==