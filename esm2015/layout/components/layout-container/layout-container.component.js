/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { sidenavAnimation, contentAnimation } from '../../helpers/animations';
export class LayoutContainerComponent {
    constructor() {
        this.hideSidenav = false;
        this.expandedSidenav = true;
        /**
         * The side that the drawer is attached to 'start' | 'end' page
         */
        this.position = 'start';
        /**
         * Layout text orientation 'ltr' | 'rtl'
         */
        this.direction = 'ltr';
        this.SIDENAV_STATES = { MOBILE: {}, EXPANDED: {}, COMPACT: {} };
        this.CONTENT_STATES = { MOBILE: {}, EXPANDED: {}, COMPACT: {} };
        this.onMediaQueryChange = this.onMediaQueryChange.bind(this);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.SIDENAV_STATES.MOBILE = { value: 'expanded', params: { width: this.sidenavMax } };
        this.SIDENAV_STATES.EXPANDED = { value: 'expanded', params: { width: this.sidenavMax } };
        this.SIDENAV_STATES.COMPACT = { value: 'compact', params: { width: this.sidenavMin } };
        this.CONTENT_STATES.MOBILE = { value: 'expanded' };
        this.mediaQueryList.addListener(this.onMediaQueryChange);
        if (this.isMobileScreenSize) {
            this.sidenavAnimationState = this.SIDENAV_STATES.MOBILE;
            this.contentAnimationState = this.CONTENT_STATES.MOBILE;
        }
        else if (this.expandedSidenav) {
            this.sidenavAnimationState = this.SIDENAV_STATES.EXPANDED;
            this.contentAnimationState = this.toggledContentAnimation;
        }
        else {
            this.sidenavAnimationState = this.SIDENAV_STATES.COMPACT;
            this.contentAnimationState = this.toggledContentAnimation;
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.mediaQueryList.removeListener(this.onMediaQueryChange);
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (changes && changes.direction) {
            this.contentAnimationState = this.toggledContentAnimation;
        }
    }
    /**
     * @return {?}
     */
    toggleMenu() {
        if (this.isMobileScreenSize) {
            this.sidenav.toggle();
        }
        else {
            this.sidenavAnimationState = this.toggledSidenavAnimation;
            this.contentAnimationState = this.toggledContentAnimation;
        }
    }
    /**
     * @return {?}
     */
    get isMobileScreenSize() {
        return this.mediaQueryList.matches;
    }
    /**
     * @return {?}
     */
    getContentAnimationState() {
        return this.contentAnimationState;
    }
    /**
     * @private
     * @return {?}
     */
    get toggledSidenavAnimation() {
        return this.sidenavAnimationState === this.SIDENAV_STATES.EXPANDED
            ? this.SIDENAV_STATES.COMPACT
            : this.SIDENAV_STATES.EXPANDED;
    }
    /**
     * @private
     * @return {?}
     */
    get toggledContentAnimation() {
        if (this.isMobileScreenSize) {
            return this.CONTENT_STATES.MOBILE;
        }
        if (this.sidenavAnimationState === this.SIDENAV_STATES.EXPANDED) {
            if (this.position === 'start' && this.direction === 'ltr') {
                return { value: 'compact', params: { 'margin-left': this.sidenavMax } };
            }
            if (this.position === 'start' && this.direction === 'rtl') {
                return { value: 'compact', params: { 'margin-right': this.sidenavMax } };
            }
            if (this.position === 'end' && this.direction === 'ltr') {
                return { value: 'compact', params: { 'margin-right': this.sidenavMax } };
            }
            if (this.position === 'end' && this.direction === 'rtl') {
                return { value: 'compact', params: { 'margin-left': this.sidenavMax } };
            }
        }
        else {
            if (this.position === 'start' && this.direction === 'ltr') {
                return { value: 'expanded', params: { 'margin-left': this.sidenavMin } };
            }
            if (this.position === 'start' && this.direction === 'rtl') {
                return { value: 'expanded', params: { 'margin-right': this.sidenavMin } };
            }
            if (this.position === 'end' && this.direction === 'ltr') {
                return { value: 'expanded', params: { 'margin-right': this.sidenavMin } };
            }
            if (this.position === 'end' && this.direction === 'rtl') {
                return { value: 'expanded', params: { 'margin-left': this.sidenavMin } };
            }
        }
    }
    /**
     * @private
     * @return {?}
     */
    onMediaQueryChange() {
        this.sidenavAnimationState = this.SIDENAV_STATES.EXPANDED;
        this.contentAnimationState = this.toggledContentAnimation;
    }
}
LayoutContainerComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-layout-container',
                template: "<mat-sidenav-container>\r\n    <mat-sidenav\r\n        [position]=\"position\"\r\n        [disableClose]=\"!isMobileScreenSize\"\r\n        [ngClass]=\"{ 'adf-sidenav--hidden': hideSidenav }\"\r\n        [@sidenavAnimation]=\"sidenavAnimationState\"\r\n        [opened]=\"!isMobileScreenSize\"\r\n        [mode]=\"isMobileScreenSize ? 'over' : 'side'\">\r\n        <ng-content sidenav select=\"[app-layout-navigation]\"></ng-content>\r\n    </mat-sidenav>\r\n\r\n    <div>\r\n        <div class=\"adf-container-full-width\" [@contentAnimationLeft]=\"getContentAnimationState()\">\r\n            <ng-content select=\"[app-layout-content]\"></ng-content>\r\n        </div>\r\n    </div>\r\n</mat-sidenav-container>\r\n",
                encapsulation: ViewEncapsulation.None,
                animations: [sidenavAnimation, contentAnimation],
                styles: [""]
            }] }
];
/** @nocollapse */
LayoutContainerComponent.ctorParameters = () => [];
LayoutContainerComponent.propDecorators = {
    sidenavMin: [{ type: Input }],
    sidenavMax: [{ type: Input }],
    mediaQueryList: [{ type: Input }],
    hideSidenav: [{ type: Input }],
    expandedSidenav: [{ type: Input }],
    position: [{ type: Input }],
    direction: [{ type: Input }],
    sidenav: [{ type: ViewChild, args: [MatSidenav, { static: true },] }]
};
if (false) {
    /** @type {?} */
    LayoutContainerComponent.prototype.sidenavMin;
    /** @type {?} */
    LayoutContainerComponent.prototype.sidenavMax;
    /** @type {?} */
    LayoutContainerComponent.prototype.mediaQueryList;
    /** @type {?} */
    LayoutContainerComponent.prototype.hideSidenav;
    /** @type {?} */
    LayoutContainerComponent.prototype.expandedSidenav;
    /**
     * The side that the drawer is attached to 'start' | 'end' page
     * @type {?}
     */
    LayoutContainerComponent.prototype.position;
    /**
     * Layout text orientation 'ltr' | 'rtl'
     * @type {?}
     */
    LayoutContainerComponent.prototype.direction;
    /** @type {?} */
    LayoutContainerComponent.prototype.sidenav;
    /** @type {?} */
    LayoutContainerComponent.prototype.sidenavAnimationState;
    /** @type {?} */
    LayoutContainerComponent.prototype.contentAnimationState;
    /** @type {?} */
    LayoutContainerComponent.prototype.SIDENAV_STATES;
    /** @type {?} */
    LayoutContainerComponent.prototype.CONTENT_STATES;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGF5b3V0LWNvbnRhaW5lci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJsYXlvdXQvY29tcG9uZW50cy9sYXlvdXQtY29udGFpbmVyL2xheW91dC1jb250YWluZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQXFCLGlCQUFpQixFQUFhLE1BQU0sZUFBZSxDQUFDO0FBQzdHLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQVU5RSxNQUFNLE9BQU8sd0JBQXdCO0lBd0JqQztRQWpCUyxnQkFBVyxHQUFHLEtBQUssQ0FBQztRQUNwQixvQkFBZSxHQUFHLElBQUksQ0FBQzs7OztRQUd2QixhQUFRLEdBQUcsT0FBTyxDQUFDOzs7O1FBR25CLGNBQVMsR0FBYyxLQUFLLENBQUM7UUFPdEMsbUJBQWMsR0FBRyxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUM7UUFDM0QsbUJBQWMsR0FBRyxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUM7UUFHdkQsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDakUsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDO1FBQ3ZGLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxHQUFHLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUM7UUFDekYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEdBQUcsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQztRQUV2RixJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sR0FBRyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsQ0FBQztRQUVuRCxJQUFJLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztRQUV6RCxJQUFJLElBQUksQ0FBQyxrQkFBa0IsRUFBRTtZQUN6QixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7WUFDeEQsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDO1NBQzNEO2FBQU0sSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQzdCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQztZQUMxRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDO1NBQzdEO2FBQU07WUFDSCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUM7WUFDekQsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQztTQUM3RDtJQUNMLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLGNBQWMsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7SUFDaEUsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsT0FBTztRQUNmLElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxTQUFTLEVBQUU7WUFDOUIsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQztTQUM3RDtJQUNMLENBQUM7Ozs7SUFFRCxVQUFVO1FBQ04sSUFBSSxJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDekIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztTQUN6QjthQUFNO1lBQ0gsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQztZQUMxRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDO1NBQzdEO0lBQ0wsQ0FBQzs7OztJQUVELElBQUksa0JBQWtCO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUM7SUFDdkMsQ0FBQzs7OztJQUVELHdCQUF3QjtRQUNwQixPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQztJQUN0QyxDQUFDOzs7OztJQUVELElBQVksdUJBQXVCO1FBQy9CLE9BQU8sSUFBSSxDQUFDLHFCQUFxQixLQUFLLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUTtZQUM5RCxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPO1lBQzdCLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQztJQUN2QyxDQUFDOzs7OztJQUVELElBQVksdUJBQXVCO1FBQy9CLElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQ3pCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7U0FDckM7UUFFRCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsS0FBSyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRTtZQUM3RCxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssT0FBTyxJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssS0FBSyxFQUFFO2dCQUN2RCxPQUFPLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsRUFBRSxhQUFhLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUM7YUFDM0U7WUFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssT0FBTyxJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssS0FBSyxFQUFFO2dCQUN2RCxPQUFPLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsRUFBRSxjQUFjLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUM7YUFDNUU7WUFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssS0FBSyxJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssS0FBSyxFQUFFO2dCQUNyRCxPQUFPLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsRUFBRSxjQUFjLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUM7YUFDNUU7WUFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssS0FBSyxJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssS0FBSyxFQUFFO2dCQUNyRCxPQUFPLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsRUFBRSxhQUFhLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUM7YUFDM0U7U0FFSjthQUFNO1lBQ0gsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLE9BQU8sSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLEtBQUssRUFBRTtnQkFDdkQsT0FBTyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLEVBQUUsYUFBYSxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDO2FBQzVFO1lBRUQsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLE9BQU8sSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLEtBQUssRUFBRTtnQkFDdkQsT0FBTyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLEVBQUUsY0FBYyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDO2FBQzdFO1lBRUQsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLEtBQUssRUFBRTtnQkFDckQsT0FBTyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLEVBQUUsY0FBYyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDO2FBQzdFO1lBRUQsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLEtBQUssRUFBRTtnQkFDckQsT0FBTyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLEVBQUUsYUFBYSxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDO2FBQzVFO1NBQ0o7SUFDTCxDQUFDOzs7OztJQUVPLGtCQUFrQjtRQUN0QixJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7UUFDMUQsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQztJQUM5RCxDQUFDOzs7WUFySUosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxzQkFBc0I7Z0JBQ2hDLHd0QkFBZ0Q7Z0JBRWhELGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO2dCQUNyQyxVQUFVLEVBQUUsQ0FBQyxnQkFBZ0IsRUFBRSxnQkFBZ0IsQ0FBQzs7YUFDbkQ7Ozs7O3lCQUVJLEtBQUs7eUJBQ0wsS0FBSzs2QkFHTCxLQUFLOzBCQUVMLEtBQUs7OEJBQ0wsS0FBSzt1QkFHTCxLQUFLO3dCQUdMLEtBQUs7c0JBRUwsU0FBUyxTQUFDLFVBQVUsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7Ozs7SUFmckMsOENBQTRCOztJQUM1Qiw4Q0FBNEI7O0lBRzVCLGtEQUE4Qzs7SUFFOUMsK0NBQTZCOztJQUM3QixtREFBZ0M7Ozs7O0lBR2hDLDRDQUE0Qjs7Ozs7SUFHNUIsNkNBQXNDOztJQUV0QywyQ0FBMkQ7O0lBRTNELHlEQUEyQjs7SUFDM0IseURBQTJCOztJQUUzQixrREFBMkQ7O0lBQzNELGtEQUEyRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxuXG4vKiFcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxuICpcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cbiAqL1xuXG5pbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBWaWV3Q2hpbGQsIE9uSW5pdCwgT25EZXN0cm95LCBWaWV3RW5jYXBzdWxhdGlvbiwgT25DaGFuZ2VzIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNYXRTaWRlbmF2IH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHsgc2lkZW5hdkFuaW1hdGlvbiwgY29udGVudEFuaW1hdGlvbiB9IGZyb20gJy4uLy4uL2hlbHBlcnMvYW5pbWF0aW9ucyc7XG5pbXBvcnQgeyBEaXJlY3Rpb24gfSBmcm9tICdAYW5ndWxhci9jZGsvYmlkaSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYWRmLWxheW91dC1jb250YWluZXInLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9sYXlvdXQtY29udGFpbmVyLmNvbXBvbmVudC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9sYXlvdXQtY29udGFpbmVyLmNvbXBvbmVudC5zY3NzJ10sXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcbiAgICBhbmltYXRpb25zOiBbc2lkZW5hdkFuaW1hdGlvbiwgY29udGVudEFuaW1hdGlvbl1cbn0pXG5leHBvcnQgY2xhc3MgTGF5b3V0Q29udGFpbmVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3ksIE9uQ2hhbmdlcyB7XG4gICAgQElucHV0KCkgc2lkZW5hdk1pbjogbnVtYmVyO1xuICAgIEBJbnB1dCgpIHNpZGVuYXZNYXg6IG51bWJlcjtcblxuICAgIC8vIFwiIHwgYW55XCIsIGJlY2F1c2UgU2FmYXJpIHRocm93cyBhbiBlcnJvciBvdGhlcndpc2UuLi5cbiAgICBASW5wdXQoKSBtZWRpYVF1ZXJ5TGlzdDogTWVkaWFRdWVyeUxpc3QgfCBhbnk7XG5cbiAgICBASW5wdXQoKSBoaWRlU2lkZW5hdiA9IGZhbHNlO1xuICAgIEBJbnB1dCgpIGV4cGFuZGVkU2lkZW5hdiA9IHRydWU7XG5cbiAgICAvKiogVGhlIHNpZGUgdGhhdCB0aGUgZHJhd2VyIGlzIGF0dGFjaGVkIHRvICdzdGFydCcgfCAnZW5kJyBwYWdlICovXG4gICAgQElucHV0KCkgcG9zaXRpb24gPSAnc3RhcnQnO1xuXG4gICAgLyoqIExheW91dCB0ZXh0IG9yaWVudGF0aW9uICdsdHInIHwgJ3J0bCcgKi9cbiAgICBASW5wdXQoKSBkaXJlY3Rpb246IERpcmVjdGlvbiA9ICdsdHInO1xuXG4gICAgQFZpZXdDaGlsZChNYXRTaWRlbmF2LCB7c3RhdGljOiB0cnVlfSkgc2lkZW5hdjogTWF0U2lkZW5hdjtcblxuICAgIHNpZGVuYXZBbmltYXRpb25TdGF0ZTogYW55O1xuICAgIGNvbnRlbnRBbmltYXRpb25TdGF0ZTogYW55O1xuXG4gICAgU0lERU5BVl9TVEFURVMgPSB7IE1PQklMRToge30sIEVYUEFOREVEOiB7fSwgQ09NUEFDVDoge30gfTtcbiAgICBDT05URU5UX1NUQVRFUyA9IHsgTU9CSUxFOiB7fSwgRVhQQU5ERUQ6IHt9LCBDT01QQUNUOiB7fSB9O1xuXG4gICAgY29uc3RydWN0b3IoKSB7XG4gICAgICAgIHRoaXMub25NZWRpYVF1ZXJ5Q2hhbmdlID0gdGhpcy5vbk1lZGlhUXVlcnlDaGFuZ2UuYmluZCh0aGlzKTtcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy5TSURFTkFWX1NUQVRFUy5NT0JJTEUgPSB7IHZhbHVlOiAnZXhwYW5kZWQnLCBwYXJhbXM6IHsgd2lkdGg6IHRoaXMuc2lkZW5hdk1heCB9IH07XG4gICAgICAgIHRoaXMuU0lERU5BVl9TVEFURVMuRVhQQU5ERUQgPSB7IHZhbHVlOiAnZXhwYW5kZWQnLCBwYXJhbXM6IHsgd2lkdGg6IHRoaXMuc2lkZW5hdk1heCB9IH07XG4gICAgICAgIHRoaXMuU0lERU5BVl9TVEFURVMuQ09NUEFDVCA9IHsgdmFsdWU6ICdjb21wYWN0JywgcGFyYW1zOiB7IHdpZHRoOiB0aGlzLnNpZGVuYXZNaW4gfSB9O1xuXG4gICAgICAgIHRoaXMuQ09OVEVOVF9TVEFURVMuTU9CSUxFID0geyB2YWx1ZTogJ2V4cGFuZGVkJyB9O1xuXG4gICAgICAgIHRoaXMubWVkaWFRdWVyeUxpc3QuYWRkTGlzdGVuZXIodGhpcy5vbk1lZGlhUXVlcnlDaGFuZ2UpO1xuXG4gICAgICAgIGlmICh0aGlzLmlzTW9iaWxlU2NyZWVuU2l6ZSkge1xuICAgICAgICAgICAgdGhpcy5zaWRlbmF2QW5pbWF0aW9uU3RhdGUgPSB0aGlzLlNJREVOQVZfU1RBVEVTLk1PQklMRTtcbiAgICAgICAgICAgIHRoaXMuY29udGVudEFuaW1hdGlvblN0YXRlID0gdGhpcy5DT05URU5UX1NUQVRFUy5NT0JJTEU7XG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5leHBhbmRlZFNpZGVuYXYpIHtcbiAgICAgICAgICAgIHRoaXMuc2lkZW5hdkFuaW1hdGlvblN0YXRlID0gdGhpcy5TSURFTkFWX1NUQVRFUy5FWFBBTkRFRDtcbiAgICAgICAgICAgIHRoaXMuY29udGVudEFuaW1hdGlvblN0YXRlID0gdGhpcy50b2dnbGVkQ29udGVudEFuaW1hdGlvbjtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuc2lkZW5hdkFuaW1hdGlvblN0YXRlID0gdGhpcy5TSURFTkFWX1NUQVRFUy5DT01QQUNUO1xuICAgICAgICAgICAgdGhpcy5jb250ZW50QW5pbWF0aW9uU3RhdGUgPSB0aGlzLnRvZ2dsZWRDb250ZW50QW5pbWF0aW9uO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgICAgIHRoaXMubWVkaWFRdWVyeUxpc3QucmVtb3ZlTGlzdGVuZXIodGhpcy5vbk1lZGlhUXVlcnlDaGFuZ2UpO1xuICAgIH1cblxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXMpIHtcbiAgICAgICAgaWYgKGNoYW5nZXMgJiYgY2hhbmdlcy5kaXJlY3Rpb24pIHtcbiAgICAgICAgICAgIHRoaXMuY29udGVudEFuaW1hdGlvblN0YXRlID0gdGhpcy50b2dnbGVkQ29udGVudEFuaW1hdGlvbjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHRvZ2dsZU1lbnUoKTogdm9pZCB7XG4gICAgICAgIGlmICh0aGlzLmlzTW9iaWxlU2NyZWVuU2l6ZSkge1xuICAgICAgICAgICAgdGhpcy5zaWRlbmF2LnRvZ2dsZSgpO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5zaWRlbmF2QW5pbWF0aW9uU3RhdGUgPSB0aGlzLnRvZ2dsZWRTaWRlbmF2QW5pbWF0aW9uO1xuICAgICAgICAgICAgdGhpcy5jb250ZW50QW5pbWF0aW9uU3RhdGUgPSB0aGlzLnRvZ2dsZWRDb250ZW50QW5pbWF0aW9uO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgZ2V0IGlzTW9iaWxlU2NyZWVuU2l6ZSgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubWVkaWFRdWVyeUxpc3QubWF0Y2hlcztcbiAgICB9XG5cbiAgICBnZXRDb250ZW50QW5pbWF0aW9uU3RhdGUoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmNvbnRlbnRBbmltYXRpb25TdGF0ZTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldCB0b2dnbGVkU2lkZW5hdkFuaW1hdGlvbigpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2lkZW5hdkFuaW1hdGlvblN0YXRlID09PSB0aGlzLlNJREVOQVZfU1RBVEVTLkVYUEFOREVEXG4gICAgICAgICAgICA/IHRoaXMuU0lERU5BVl9TVEFURVMuQ09NUEFDVFxuICAgICAgICAgICAgOiB0aGlzLlNJREVOQVZfU1RBVEVTLkVYUEFOREVEO1xuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0IHRvZ2dsZWRDb250ZW50QW5pbWF0aW9uKCkge1xuICAgICAgICBpZiAodGhpcy5pc01vYmlsZVNjcmVlblNpemUpIHtcbiAgICAgICAgICAgIHJldHVybiB0aGlzLkNPTlRFTlRfU1RBVEVTLk1PQklMRTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0aGlzLnNpZGVuYXZBbmltYXRpb25TdGF0ZSA9PT0gdGhpcy5TSURFTkFWX1NUQVRFUy5FWFBBTkRFRCkge1xuICAgICAgICAgICAgaWYgKHRoaXMucG9zaXRpb24gPT09ICdzdGFydCcgJiYgdGhpcy5kaXJlY3Rpb24gPT09ICdsdHInKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHsgdmFsdWU6ICdjb21wYWN0JywgcGFyYW1zOiB7ICdtYXJnaW4tbGVmdCc6IHRoaXMuc2lkZW5hdk1heCB9IH07XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLnBvc2l0aW9uID09PSAnc3RhcnQnICYmIHRoaXMuZGlyZWN0aW9uID09PSAncnRsJykge1xuICAgICAgICAgICAgICAgIHJldHVybiB7IHZhbHVlOiAnY29tcGFjdCcsIHBhcmFtczogeyAnbWFyZ2luLXJpZ2h0JzogdGhpcy5zaWRlbmF2TWF4IH0gfTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMucG9zaXRpb24gPT09ICdlbmQnICYmIHRoaXMuZGlyZWN0aW9uID09PSAnbHRyJykge1xuICAgICAgICAgICAgICAgIHJldHVybiB7IHZhbHVlOiAnY29tcGFjdCcsIHBhcmFtczogeyAnbWFyZ2luLXJpZ2h0JzogdGhpcy5zaWRlbmF2TWF4IH0gfTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMucG9zaXRpb24gPT09ICdlbmQnICYmIHRoaXMuZGlyZWN0aW9uID09PSAncnRsJykge1xuICAgICAgICAgICAgICAgIHJldHVybiB7IHZhbHVlOiAnY29tcGFjdCcsIHBhcmFtczogeyAnbWFyZ2luLWxlZnQnOiB0aGlzLnNpZGVuYXZNYXggfSB9O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAodGhpcy5wb3NpdGlvbiA9PT0gJ3N0YXJ0JyAmJiB0aGlzLmRpcmVjdGlvbiA9PT0gJ2x0cicpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4geyB2YWx1ZTogJ2V4cGFuZGVkJywgcGFyYW1zOiB7ICdtYXJnaW4tbGVmdCc6IHRoaXMuc2lkZW5hdk1pbiB9IH07XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLnBvc2l0aW9uID09PSAnc3RhcnQnICYmIHRoaXMuZGlyZWN0aW9uID09PSAncnRsJykge1xuICAgICAgICAgICAgICAgIHJldHVybiB7IHZhbHVlOiAnZXhwYW5kZWQnLCBwYXJhbXM6IHsgJ21hcmdpbi1yaWdodCc6IHRoaXMuc2lkZW5hdk1pbiB9IH07XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLnBvc2l0aW9uID09PSAnZW5kJyAmJiB0aGlzLmRpcmVjdGlvbiA9PT0gJ2x0cicpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4geyB2YWx1ZTogJ2V4cGFuZGVkJywgcGFyYW1zOiB7ICdtYXJnaW4tcmlnaHQnOiB0aGlzLnNpZGVuYXZNaW4gfSB9O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5wb3NpdGlvbiA9PT0gJ2VuZCcgJiYgdGhpcy5kaXJlY3Rpb24gPT09ICdydGwnKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHsgdmFsdWU6ICdleHBhbmRlZCcsIHBhcmFtczogeyAnbWFyZ2luLWxlZnQnOiB0aGlzLnNpZGVuYXZNaW4gfSB9O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBvbk1lZGlhUXVlcnlDaGFuZ2UoKSB7XG4gICAgICAgIHRoaXMuc2lkZW5hdkFuaW1hdGlvblN0YXRlID0gdGhpcy5TSURFTkFWX1NUQVRFUy5FWFBBTkRFRDtcbiAgICAgICAgdGhpcy5jb250ZW50QW5pbWF0aW9uU3RhdGUgPSB0aGlzLnRvZ2dsZWRDb250ZW50QW5pbWF0aW9uO1xuICAgIH1cbn1cbiJdfQ==