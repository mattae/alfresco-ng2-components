/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, Component, Directive, Input, ViewEncapsulation } from '@angular/core';
export class SidebarActionMenuComponent {
    constructor() {
        /**
         * Width in pixels for sidebar action menu options.
         */
        this.width = 272;
    }
    /**
     * @return {?}
     */
    isExpanded() {
        return this.expanded;
    }
}
SidebarActionMenuComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-sidebar-action-menu',
                template: "<div class=\"adf-sidebar-action-menu\">\r\n    <button *ngIf=\"isExpanded()\" mat-raised-button class=\"adf-sidebar-action-menu-button\" data-automation-id=\"create-button\" [matMenuTriggerFor]=\"adfSidebarMenu\">\r\n        <span *ngIf=\"title\" class=\"adf-sidebar-action-menu-text\">{{ title }}</span>\r\n        <ng-content select=\"[adf-sidebar-menu-title-icon], [sidebar-menu-title-icon]\"></ng-content>\r\n    </button>\r\n\r\n    <div *ngIf=\"!isExpanded()\" class=\"adf-sidebar-action-menu-icon\" [matMenuTriggerFor]=\"adfSidebarMenu\">\r\n        <ng-content select=\"[adf-sidebar-menu-expand-icon], [sidebar-menu-expand-icon]\"></ng-content>\r\n    </div>\r\n\r\n    <mat-menu #adfSidebarMenu=\"matMenu\" class=\"adf-sidebar-action-menu-panel\" [overlapTrigger]=\"false\" yPosition=\"below\">\r\n        <div class=\"adf-sidebar-action-menu-options\" [style.width.px]=\"width\">\r\n            <ng-content select=\"[adf-sidebar-menu-options], [sidebar-menu-options]\"></ng-content>\r\n        </div>\r\n    </mat-menu>\r\n</div>\r\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                encapsulation: ViewEncapsulation.None,
                host: { 'class': 'adf-sidebar-action-menu' },
                styles: [""]
            }] }
];
SidebarActionMenuComponent.propDecorators = {
    title: [{ type: Input }],
    expanded: [{ type: Input }],
    width: [{ type: Input }]
};
if (false) {
    /**
     * The title of the sidebar action.
     * @type {?}
     */
    SidebarActionMenuComponent.prototype.title;
    /**
     * Toggle the sidebar action menu on expand.
     * @type {?}
     */
    SidebarActionMenuComponent.prototype.expanded;
    /**
     * Width in pixels for sidebar action menu options.
     * @type {?}
     */
    SidebarActionMenuComponent.prototype.width;
}
/**
 * Directive selectors without adf- prefix will be deprecated on 3.0.0
 */
export class SidebarMenuDirective {
}
SidebarMenuDirective.decorators = [
    { type: Directive, args: [{ selector: '[adf-sidebar-menu-options], [sidebar-menu-options]' },] }
];
export class SidebarMenuTitleIconDirective {
}
SidebarMenuTitleIconDirective.decorators = [
    { type: Directive, args: [{ selector: '[adf-sidebar-menu-title-icon], [sidebar-menu-title-icon]' },] }
];
export class SidebarMenuExpandIconDirective {
}
SidebarMenuExpandIconDirective.decorators = [
    { type: Directive, args: [{ selector: '[adf-sidebar-menu-expand-icon], [sidebar-menu-expand-icon]' },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lkZWJhci1hY3Rpb24tbWVudS5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJsYXlvdXQvY29tcG9uZW50cy9zaWRlYmFyLWFjdGlvbi9zaWRlYmFyLWFjdGlvbi1tZW51LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFXeEcsTUFBTSxPQUFPLDBCQUEwQjtJQVR2Qzs7OztRQXFCSSxVQUFLLEdBQVcsR0FBRyxDQUFDO0lBS3hCLENBQUM7Ozs7SUFIRyxVQUFVO1FBQ04sT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3pCLENBQUM7OztZQXpCSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLHlCQUF5QjtnQkFDbkMsK2hDQUFtRDtnQkFFbkQsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07Z0JBQy9DLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO2dCQUNyQyxJQUFJLEVBQUUsRUFBRSxPQUFPLEVBQUUseUJBQXlCLEVBQUU7O2FBQy9DOzs7b0JBS0ksS0FBSzt1QkFJTCxLQUFLO29CQUlMLEtBQUs7Ozs7Ozs7SUFSTiwyQ0FDYzs7Ozs7SUFHZCw4Q0FDa0I7Ozs7O0lBR2xCLDJDQUNvQjs7Ozs7QUFVdUQsTUFBTSxPQUFPLG9CQUFvQjs7O1lBQS9HLFNBQVMsU0FBQyxFQUFFLFFBQVEsRUFBRSxvREFBb0QsRUFBRTs7QUFDUSxNQUFNLE9BQU8sNkJBQTZCOzs7WUFBOUgsU0FBUyxTQUFDLEVBQUUsUUFBUSxFQUFFLDBEQUEwRCxFQUFFOztBQUNJLE1BQU0sT0FBTyw4QkFBOEI7OztZQUFqSSxTQUFTLFNBQUMsRUFBRSxRQUFRLEVBQUUsNERBQTRELEVBQUUiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksIENvbXBvbmVudCwgRGlyZWN0aXZlLCBJbnB1dCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtc2lkZWJhci1hY3Rpb24tbWVudScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vc2lkZWJhci1hY3Rpb24tbWVudS5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9zaWRlYmFyLWFjdGlvbi1tZW51LmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXHJcbiAgICBob3N0OiB7ICdjbGFzcyc6ICdhZGYtc2lkZWJhci1hY3Rpb24tbWVudScgfVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIFNpZGViYXJBY3Rpb25NZW51Q29tcG9uZW50IHtcclxuXHJcbiAgICAvKiogVGhlIHRpdGxlIG9mIHRoZSBzaWRlYmFyIGFjdGlvbi4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICB0aXRsZTogc3RyaW5nO1xyXG5cclxuICAgIC8qKiBUb2dnbGUgdGhlIHNpZGViYXIgYWN0aW9uIG1lbnUgb24gZXhwYW5kLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGV4cGFuZGVkOiBib29sZWFuO1xyXG5cclxuICAgIC8qKiBXaWR0aCBpbiBwaXhlbHMgZm9yIHNpZGViYXIgYWN0aW9uIG1lbnUgb3B0aW9ucy4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICB3aWR0aDogbnVtYmVyID0gMjcyO1xyXG5cclxuICAgIGlzRXhwYW5kZWQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZXhwYW5kZWQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbi8qKlxyXG4gKiBEaXJlY3RpdmUgc2VsZWN0b3JzIHdpdGhvdXQgYWRmLSBwcmVmaXggd2lsbCBiZSBkZXByZWNhdGVkIG9uIDMuMC4wXHJcbiAqL1xyXG5ARGlyZWN0aXZlKHsgc2VsZWN0b3I6ICdbYWRmLXNpZGViYXItbWVudS1vcHRpb25zXSwgW3NpZGViYXItbWVudS1vcHRpb25zXScgfSkgZXhwb3J0IGNsYXNzIFNpZGViYXJNZW51RGlyZWN0aXZlIHt9XHJcbkBEaXJlY3RpdmUoeyBzZWxlY3RvcjogJ1thZGYtc2lkZWJhci1tZW51LXRpdGxlLWljb25dLCBbc2lkZWJhci1tZW51LXRpdGxlLWljb25dJyB9KSBleHBvcnQgY2xhc3MgU2lkZWJhck1lbnVUaXRsZUljb25EaXJlY3RpdmUgeyB9XHJcbkBEaXJlY3RpdmUoeyBzZWxlY3RvcjogJ1thZGYtc2lkZWJhci1tZW51LWV4cGFuZC1pY29uXSwgW3NpZGViYXItbWVudS1leHBhbmQtaWNvbl0nIH0pIGV4cG9ydCBjbGFzcyBTaWRlYmFyTWVudUV4cGFuZEljb25EaXJlY3RpdmUgeyB9XHJcbiJdfQ==