/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ContentChild, Input, Output, ViewChild, EventEmitter, ViewEncapsulation } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { UserPreferencesService } from '../../../services/user-preferences.service';
import { SidenavLayoutContentDirective } from '../../directives/sidenav-layout-content.directive';
import { SidenavLayoutHeaderDirective } from '../../directives/sidenav-layout-header.directive';
import { SidenavLayoutNavigationDirective } from '../../directives/sidenav-layout-navigation.directive';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
export class SidenavLayoutComponent {
    /**
     * @param {?} mediaMatcher
     * @param {?} userPreferencesService
     */
    constructor(mediaMatcher, userPreferencesService) {
        this.mediaMatcher = mediaMatcher;
        this.userPreferencesService = userPreferencesService;
        /**
         * The direction of the layout. 'ltr' or 'rtl'
         */
        this.dir = 'ltr';
        /**
         * The side that the drawer is attached to. Possible values are 'start' and 'end'.
         */
        this.position = 'start';
        /**
         * Toggles showing/hiding the navigation region.
         */
        this.hideSidenav = false;
        /**
         * Should the navigation region be expanded initially?
         */
        this.expandedSidenav = true;
        /**
         * Emitted when the menu toggle and the collapsed/expanded state of the sideNav changes.
         */
        this.expanded = new EventEmitter();
        this.templateContext = {
            toggleMenu: (/**
             * @return {?}
             */
            () => { }),
            isMenuMinimized: (/**
             * @return {?}
             */
            () => this.isMenuMinimized)
        };
        this.onDestroy$ = new Subject();
        this.onMediaQueryChange = this.onMediaQueryChange.bind(this);
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        /** @type {?} */
        const initialMenuState = !this.expandedSidenav;
        this.menuOpenStateSubject = new BehaviorSubject(initialMenuState);
        this.menuOpenState$ = this.menuOpenStateSubject.asObservable();
        /** @type {?} */
        const stepOver = this.stepOver || SidenavLayoutComponent.STEP_OVER;
        this.isMenuMinimized = initialMenuState;
        this.mediaQueryList = this.mediaMatcher.matchMedia(`(max-width: ${stepOver}px)`);
        this.mediaQueryList.addListener(this.onMediaQueryChange);
        this.userPreferencesService
            .select('textOrientation')
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} direction
         * @return {?}
         */
        (direction) => {
            this.dir = direction;
        }));
    }
    /**
     * @return {?}
     */
    ngAfterViewInit() {
        this.templateContext.toggleMenu = this.toggleMenu.bind(this);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.mediaQueryList.removeListener(this.onMediaQueryChange);
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    }
    /**
     * @return {?}
     */
    toggleMenu() {
        if (!this.mediaQueryList.matches) {
            this.isMenuMinimized = !this.isMenuMinimized;
        }
        else {
            this.isMenuMinimized = false;
        }
        this.container.toggleMenu();
        this.expanded.emit(!this.isMenuMinimized);
    }
    /**
     * @return {?}
     */
    get isMenuMinimized() {
        return this._isMenuMinimized;
    }
    /**
     * @param {?} menuState
     * @return {?}
     */
    set isMenuMinimized(menuState) {
        this._isMenuMinimized = menuState;
        this.menuOpenStateSubject.next(!menuState);
    }
    /**
     * @return {?}
     */
    get isHeaderInside() {
        return this.mediaQueryList.matches;
    }
    /**
     * @return {?}
     */
    get headerTemplate() {
        return this.headerDirective && this.headerDirective.template || this.emptyTemplate;
    }
    /**
     * @return {?}
     */
    get navigationTemplate() {
        return this.navigationDirective && this.navigationDirective.template || this.emptyTemplate;
    }
    /**
     * @return {?}
     */
    get contentTemplate() {
        return this.contentDirective && this.contentDirective.template || this.emptyTemplate;
    }
    /**
     * @return {?}
     */
    onMediaQueryChange() {
        this.isMenuMinimized = false;
        this.expanded.emit(!this.isMenuMinimized);
    }
}
SidenavLayoutComponent.STEP_OVER = 600;
SidenavLayoutComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-sidenav-layout',
                template: "<div class=\"adf-sidenav-layout-full-space\">\r\n    <ng-container *ngIf=\"!isHeaderInside\">\r\n        <ng-container class=\"adf-sidenav-layout-outer-header\"\r\n                      *ngTemplateOutlet=\"headerTemplate; context:templateContext\"></ng-container>\r\n    </ng-container>\r\n\r\n    <adf-layout-container #container\r\n                          [direction]=\"dir\"\r\n                          [position]=\"position\"\r\n                          [sidenavMin]=\"sidenavMin\"\r\n                          [sidenavMax]=\"sidenavMax\"\r\n                          [mediaQueryList]=\"mediaQueryList\"\r\n                          [hideSidenav]=\"hideSidenav\"\r\n                          [expandedSidenav]=\"expandedSidenav\"\r\n                          data-automation-id=\"adf-layout-container\"\r\n                          class=\"adf-layout__content\">\r\n\r\n        <ng-container app-layout-navigation\r\n                      *ngTemplateOutlet=\"navigationTemplate; context:templateContext\"></ng-container>\r\n\r\n        <ng-container app-layout-content>\r\n            <ng-container *ngIf=\"isHeaderInside\">\r\n                <ng-container *ngTemplateOutlet=\"headerTemplate; context:templateContext\"></ng-container>\r\n            </ng-container>\r\n            <ng-container *ngTemplateOutlet=\"contentTemplate; context:templateContext\"></ng-container>\r\n        </ng-container>\r\n    </adf-layout-container>\r\n\r\n    <ng-template #emptyTemplate></ng-template>\r\n</div>\r\n",
                encapsulation: ViewEncapsulation.None,
                host: { class: 'adf-sidenav-layout' },
                styles: [""]
            }] }
];
/** @nocollapse */
SidenavLayoutComponent.ctorParameters = () => [
    { type: MediaMatcher },
    { type: UserPreferencesService }
];
SidenavLayoutComponent.propDecorators = {
    position: [{ type: Input }],
    sidenavMin: [{ type: Input }],
    sidenavMax: [{ type: Input }],
    stepOver: [{ type: Input }],
    hideSidenav: [{ type: Input }],
    expandedSidenav: [{ type: Input }],
    expanded: [{ type: Output }],
    headerDirective: [{ type: ContentChild, args: [SidenavLayoutHeaderDirective, { static: true },] }],
    navigationDirective: [{ type: ContentChild, args: [SidenavLayoutNavigationDirective, { static: true },] }],
    contentDirective: [{ type: ContentChild, args: [SidenavLayoutContentDirective, { static: true },] }],
    container: [{ type: ViewChild, args: ['container', { static: true },] }],
    emptyTemplate: [{ type: ViewChild, args: ['emptyTemplate', { static: true },] }]
};
if (false) {
    /** @type {?} */
    SidenavLayoutComponent.STEP_OVER;
    /**
     * The direction of the layout. 'ltr' or 'rtl'
     * @type {?}
     */
    SidenavLayoutComponent.prototype.dir;
    /**
     * The side that the drawer is attached to. Possible values are 'start' and 'end'.
     * @type {?}
     */
    SidenavLayoutComponent.prototype.position;
    /**
     * Minimum size of the navigation region.
     * @type {?}
     */
    SidenavLayoutComponent.prototype.sidenavMin;
    /**
     * Maximum size of the navigation region.
     * @type {?}
     */
    SidenavLayoutComponent.prototype.sidenavMax;
    /**
     * Screen size at which display switches from small screen to large screen configuration.
     * @type {?}
     */
    SidenavLayoutComponent.prototype.stepOver;
    /**
     * Toggles showing/hiding the navigation region.
     * @type {?}
     */
    SidenavLayoutComponent.prototype.hideSidenav;
    /**
     * Should the navigation region be expanded initially?
     * @type {?}
     */
    SidenavLayoutComponent.prototype.expandedSidenav;
    /**
     * Emitted when the menu toggle and the collapsed/expanded state of the sideNav changes.
     * @type {?}
     */
    SidenavLayoutComponent.prototype.expanded;
    /** @type {?} */
    SidenavLayoutComponent.prototype.headerDirective;
    /** @type {?} */
    SidenavLayoutComponent.prototype.navigationDirective;
    /** @type {?} */
    SidenavLayoutComponent.prototype.contentDirective;
    /**
     * @type {?}
     * @private
     */
    SidenavLayoutComponent.prototype.menuOpenStateSubject;
    /** @type {?} */
    SidenavLayoutComponent.prototype.menuOpenState$;
    /** @type {?} */
    SidenavLayoutComponent.prototype.container;
    /** @type {?} */
    SidenavLayoutComponent.prototype.emptyTemplate;
    /** @type {?} */
    SidenavLayoutComponent.prototype.mediaQueryList;
    /** @type {?} */
    SidenavLayoutComponent.prototype._isMenuMinimized;
    /** @type {?} */
    SidenavLayoutComponent.prototype.templateContext;
    /**
     * @type {?}
     * @private
     */
    SidenavLayoutComponent.prototype.onDestroy$;
    /**
     * @type {?}
     * @private
     */
    SidenavLayoutComponent.prototype.mediaMatcher;
    /**
     * @type {?}
     * @private
     */
    SidenavLayoutComponent.prototype.userPreferencesService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2lkZW5hdi1sYXlvdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsibGF5b3V0L2NvbXBvbmVudHMvc2lkZW5hdi1sYXlvdXQvc2lkZW5hdi1sYXlvdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxPQUFPLEVBQ0gsU0FBUyxFQUNULFlBQVksRUFDWixLQUFLLEVBQ0wsTUFBTSxFQUdOLFNBQVMsRUFHVCxZQUFZLEVBQ1osaUJBQWlCLEVBQ3BCLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUNuRCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUNwRixPQUFPLEVBQUUsNkJBQTZCLEVBQUUsTUFBTSxtREFBbUQsQ0FBQztBQUNsRyxPQUFPLEVBQUUsNEJBQTRCLEVBQUUsTUFBTSxrREFBa0QsQ0FBQztBQUNoRyxPQUFPLEVBQUUsZ0NBQWdDLEVBQUUsTUFBTSxzREFBc0QsQ0FBQztBQUN4RyxPQUFPLEVBQUUsZUFBZSxFQUFjLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUU1RCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFTM0MsTUFBTSxPQUFPLHNCQUFzQjs7Ozs7SUErQy9CLFlBQW9CLFlBQTBCLEVBQVUsc0JBQThDO1FBQWxGLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQVUsMkJBQXNCLEdBQXRCLHNCQUFzQixDQUF3Qjs7OztRQTNDdEcsUUFBRyxHQUFHLEtBQUssQ0FBQzs7OztRQUdILGFBQVEsR0FBRyxPQUFPLENBQUM7Ozs7UUFZbkIsZ0JBQVcsR0FBRyxLQUFLLENBQUM7Ozs7UUFHcEIsb0JBQWUsR0FBRyxJQUFJLENBQUM7Ozs7UUFHdEIsYUFBUSxHQUFHLElBQUksWUFBWSxFQUFXLENBQUM7UUFlakQsb0JBQWUsR0FBRztZQUNkLFVBQVU7OztZQUFFLEdBQUcsRUFBRSxHQUFFLENBQUMsQ0FBQTtZQUNwQixlQUFlOzs7WUFBRSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFBO1NBQzlDLENBQUM7UUFFTSxlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztRQUd4QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNqRSxDQUFDOzs7O0lBRUQsUUFBUTs7Y0FDRSxnQkFBZ0IsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlO1FBRTlDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLGVBQWUsQ0FBVSxnQkFBZ0IsQ0FBQyxDQUFDO1FBQzNFLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFlBQVksRUFBRSxDQUFDOztjQUV6RCxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsSUFBSSxzQkFBc0IsQ0FBQyxTQUFTO1FBQ2xFLElBQUksQ0FBQyxlQUFlLEdBQUcsZ0JBQWdCLENBQUM7UUFFeEMsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLFVBQVUsQ0FBQyxlQUFlLFFBQVEsS0FBSyxDQUFDLENBQUM7UUFDakYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFFekQsSUFBSSxDQUFDLHNCQUFzQjthQUN0QixNQUFNLENBQUMsaUJBQWlCLENBQUM7YUFDekIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDaEMsU0FBUzs7OztRQUFDLENBQUMsU0FBb0IsRUFBRSxFQUFFO1lBQ2hDLElBQUksQ0FBQyxHQUFHLEdBQUcsU0FBUyxDQUFDO1FBQ3pCLENBQUMsRUFBQyxDQUFDO0lBQ1gsQ0FBQzs7OztJQUVELGVBQWU7UUFDWCxJQUFJLENBQUMsZUFBZSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNqRSxDQUFDOzs7O0lBRUQsV0FBVztRQUNQLElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBQzVELElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDL0IsQ0FBQzs7OztJQUVELFVBQVU7UUFDTixJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLEVBQUU7WUFDOUIsSUFBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7U0FDaEQ7YUFBTTtZQUNILElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO1NBQ2hDO1FBRUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsQ0FBQztRQUM1QixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUM5QyxDQUFDOzs7O0lBRUQsSUFBSSxlQUFlO1FBQ2YsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFRCxJQUFJLGVBQWUsQ0FBQyxTQUFrQjtRQUNsQyxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsU0FBUyxDQUFDO1FBQ2xDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUMvQyxDQUFDOzs7O0lBRUQsSUFBSSxjQUFjO1FBQ2QsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQztJQUN2QyxDQUFDOzs7O0lBRUQsSUFBSSxjQUFjO1FBQ2QsT0FBTyxJQUFJLENBQUMsZUFBZSxJQUFJLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDdkYsQ0FBQzs7OztJQUVELElBQUksa0JBQWtCO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixJQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQztJQUMvRixDQUFDOzs7O0lBRUQsSUFBSSxlQUFlO1FBQ2YsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDO0lBQ3pGLENBQUM7Ozs7SUFFRCxrQkFBa0I7UUFDZCxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztRQUM3QixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztJQUM5QyxDQUFDOztBQXZITSxnQ0FBUyxHQUFHLEdBQUcsQ0FBQzs7WUFSMUIsU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxvQkFBb0I7Z0JBQzlCLGcvQ0FBOEM7Z0JBRTlDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO2dCQUNyQyxJQUFJLEVBQUUsRUFBRSxLQUFLLEVBQUUsb0JBQW9CLEVBQUU7O2FBQ3hDOzs7O1lBZlEsWUFBWTtZQUNaLHNCQUFzQjs7O3VCQXNCMUIsS0FBSzt5QkFHTCxLQUFLO3lCQUdMLEtBQUs7dUJBR0wsS0FBSzswQkFHTCxLQUFLOzhCQUdMLEtBQUs7dUJBR0wsTUFBTTs4QkFFTixZQUFZLFNBQUMsNEJBQTRCLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDO2tDQUN6RCxZQUFZLFNBQUMsZ0NBQWdDLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDOytCQUM3RCxZQUFZLFNBQUMsNkJBQTZCLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDO3dCQUsxRCxTQUFTLFNBQUMsV0FBVyxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQzs0QkFDckMsU0FBUyxTQUFDLGVBQWUsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7Ozs7SUFsQzFDLGlDQUF1Qjs7Ozs7SUFHdkIscUNBQVk7Ozs7O0lBR1osMENBQTRCOzs7OztJQUc1Qiw0Q0FBNEI7Ozs7O0lBRzVCLDRDQUE0Qjs7Ozs7SUFHNUIsMENBQTBCOzs7OztJQUcxQiw2Q0FBNkI7Ozs7O0lBRzdCLGlEQUFnQzs7Ozs7SUFHaEMsMENBQWlEOztJQUVqRCxpREFBMEc7O0lBQzFHLHFEQUFzSDs7SUFDdEgsa0RBQTZHOzs7OztJQUU3RyxzREFBdUQ7O0lBQ3ZELGdEQUEyQzs7SUFFM0MsMkNBQXVEOztJQUN2RCwrQ0FBK0Q7O0lBRS9ELGdEQUErQjs7SUFDL0Isa0RBQWlCOztJQUVqQixpREFHRTs7Ozs7SUFFRiw0Q0FBNEM7Ozs7O0lBRWhDLDhDQUFrQzs7Ozs7SUFBRSx3REFBc0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cblxuLyohXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cbiAqXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4gKlxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuICpcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXG4gKi9cblxuaW1wb3J0IHtcbiAgICBDb21wb25lbnQsXG4gICAgQ29udGVudENoaWxkLFxuICAgIElucHV0LFxuICAgIE91dHB1dCxcbiAgICBPbkluaXQsXG4gICAgQWZ0ZXJWaWV3SW5pdCxcbiAgICBWaWV3Q2hpbGQsXG4gICAgT25EZXN0cm95LFxuICAgIFRlbXBsYXRlUmVmLFxuICAgIEV2ZW50RW1pdHRlcixcbiAgICBWaWV3RW5jYXBzdWxhdGlvblxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1lZGlhTWF0Y2hlciB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9sYXlvdXQnO1xuaW1wb3J0IHsgVXNlclByZWZlcmVuY2VzU2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NlcnZpY2VzL3VzZXItcHJlZmVyZW5jZXMuc2VydmljZSc7XG5pbXBvcnQgeyBTaWRlbmF2TGF5b3V0Q29udGVudERpcmVjdGl2ZSB9IGZyb20gJy4uLy4uL2RpcmVjdGl2ZXMvc2lkZW5hdi1sYXlvdXQtY29udGVudC5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgU2lkZW5hdkxheW91dEhlYWRlckRpcmVjdGl2ZSB9IGZyb20gJy4uLy4uL2RpcmVjdGl2ZXMvc2lkZW5hdi1sYXlvdXQtaGVhZGVyLmRpcmVjdGl2ZSc7XG5pbXBvcnQgeyBTaWRlbmF2TGF5b3V0TmF2aWdhdGlvbkRpcmVjdGl2ZSB9IGZyb20gJy4uLy4uL2RpcmVjdGl2ZXMvc2lkZW5hdi1sYXlvdXQtbmF2aWdhdGlvbi5kaXJlY3RpdmUnO1xuaW1wb3J0IHsgQmVoYXZpb3JTdWJqZWN0LCBPYnNlcnZhYmxlLCBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBEaXJlY3Rpb24gfSBmcm9tICdAYW5ndWxhci9jZGsvYmlkaSc7XG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYWRmLXNpZGVuYXYtbGF5b3V0JyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vc2lkZW5hdi1sYXlvdXQuY29tcG9uZW50Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL3NpZGVuYXYtbGF5b3V0LmNvbXBvbmVudC5zY3NzJ10sXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcbiAgICBob3N0OiB7IGNsYXNzOiAnYWRmLXNpZGVuYXYtbGF5b3V0JyB9XG59KVxuZXhwb3J0IGNsYXNzIFNpZGVuYXZMYXlvdXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQsIE9uRGVzdHJveSB7XG4gICAgc3RhdGljIFNURVBfT1ZFUiA9IDYwMDtcblxuICAgIC8qKiBUaGUgZGlyZWN0aW9uIG9mIHRoZSBsYXlvdXQuICdsdHInIG9yICdydGwnICovXG4gICAgZGlyID0gJ2x0cic7XG5cbiAgICAvKiogVGhlIHNpZGUgdGhhdCB0aGUgZHJhd2VyIGlzIGF0dGFjaGVkIHRvLiBQb3NzaWJsZSB2YWx1ZXMgYXJlICdzdGFydCcgYW5kICdlbmQnLiAqL1xuICAgIEBJbnB1dCgpIHBvc2l0aW9uID0gJ3N0YXJ0JztcblxuICAgIC8qKiBNaW5pbXVtIHNpemUgb2YgdGhlIG5hdmlnYXRpb24gcmVnaW9uLiAqL1xuICAgIEBJbnB1dCgpIHNpZGVuYXZNaW46IG51bWJlcjtcblxuICAgIC8qKiBNYXhpbXVtIHNpemUgb2YgdGhlIG5hdmlnYXRpb24gcmVnaW9uLiAqL1xuICAgIEBJbnB1dCgpIHNpZGVuYXZNYXg6IG51bWJlcjtcblxuICAgIC8qKiBTY3JlZW4gc2l6ZSBhdCB3aGljaCBkaXNwbGF5IHN3aXRjaGVzIGZyb20gc21hbGwgc2NyZWVuIHRvIGxhcmdlIHNjcmVlbiBjb25maWd1cmF0aW9uLiAqL1xuICAgIEBJbnB1dCgpIHN0ZXBPdmVyOiBudW1iZXI7XG5cbiAgICAvKiogVG9nZ2xlcyBzaG93aW5nL2hpZGluZyB0aGUgbmF2aWdhdGlvbiByZWdpb24uICovXG4gICAgQElucHV0KCkgaGlkZVNpZGVuYXYgPSBmYWxzZTtcblxuICAgIC8qKiBTaG91bGQgdGhlIG5hdmlnYXRpb24gcmVnaW9uIGJlIGV4cGFuZGVkIGluaXRpYWxseT8gKi9cbiAgICBASW5wdXQoKSBleHBhbmRlZFNpZGVuYXYgPSB0cnVlO1xuXG4gICAgLyoqIEVtaXR0ZWQgd2hlbiB0aGUgbWVudSB0b2dnbGUgYW5kIHRoZSBjb2xsYXBzZWQvZXhwYW5kZWQgc3RhdGUgb2YgdGhlIHNpZGVOYXYgY2hhbmdlcy4gKi9cbiAgICBAT3V0cHV0KCkgZXhwYW5kZWQgPSBuZXcgRXZlbnRFbWl0dGVyPGJvb2xlYW4+KCk7XG5cbiAgICBAQ29udGVudENoaWxkKFNpZGVuYXZMYXlvdXRIZWFkZXJEaXJlY3RpdmUsIHtzdGF0aWM6IHRydWV9KSBoZWFkZXJEaXJlY3RpdmU6IFNpZGVuYXZMYXlvdXRIZWFkZXJEaXJlY3RpdmU7XG4gICAgQENvbnRlbnRDaGlsZChTaWRlbmF2TGF5b3V0TmF2aWdhdGlvbkRpcmVjdGl2ZSwge3N0YXRpYzogdHJ1ZX0pIG5hdmlnYXRpb25EaXJlY3RpdmU6IFNpZGVuYXZMYXlvdXROYXZpZ2F0aW9uRGlyZWN0aXZlO1xuICAgIEBDb250ZW50Q2hpbGQoU2lkZW5hdkxheW91dENvbnRlbnREaXJlY3RpdmUsIHtzdGF0aWM6IHRydWV9KSBjb250ZW50RGlyZWN0aXZlOiBTaWRlbmF2TGF5b3V0Q29udGVudERpcmVjdGl2ZTtcblxuICAgIHByaXZhdGUgbWVudU9wZW5TdGF0ZVN1YmplY3Q6IEJlaGF2aW9yU3ViamVjdDxib29sZWFuPjtcbiAgICBwdWJsaWMgbWVudU9wZW5TdGF0ZSQ6IE9ic2VydmFibGU8Ym9vbGVhbj47XG5cbiAgICBAVmlld0NoaWxkKCdjb250YWluZXInLCB7c3RhdGljOiB0cnVlfSkgY29udGFpbmVyOiBhbnk7XG4gICAgQFZpZXdDaGlsZCgnZW1wdHlUZW1wbGF0ZScsIHtzdGF0aWM6IHRydWV9KSBlbXB0eVRlbXBsYXRlOiBhbnk7XG5cbiAgICBtZWRpYVF1ZXJ5TGlzdDogTWVkaWFRdWVyeUxpc3Q7XG4gICAgX2lzTWVudU1pbmltaXplZDtcblxuICAgIHRlbXBsYXRlQ29udGV4dCA9IHtcbiAgICAgICAgdG9nZ2xlTWVudTogKCkgPT4ge30sXG4gICAgICAgIGlzTWVudU1pbmltaXplZDogKCkgPT4gdGhpcy5pc01lbnVNaW5pbWl6ZWRcbiAgICB9O1xuXG4gICAgcHJpdmF0ZSBvbkRlc3Ryb3kkID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgbWVkaWFNYXRjaGVyOiBNZWRpYU1hdGNoZXIsIHByaXZhdGUgdXNlclByZWZlcmVuY2VzU2VydmljZTogVXNlclByZWZlcmVuY2VzU2VydmljZSApIHtcbiAgICAgICAgdGhpcy5vbk1lZGlhUXVlcnlDaGFuZ2UgPSB0aGlzLm9uTWVkaWFRdWVyeUNoYW5nZS5iaW5kKHRoaXMpO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICBjb25zdCBpbml0aWFsTWVudVN0YXRlID0gIXRoaXMuZXhwYW5kZWRTaWRlbmF2O1xuXG4gICAgICAgIHRoaXMubWVudU9wZW5TdGF0ZVN1YmplY3QgPSBuZXcgQmVoYXZpb3JTdWJqZWN0PGJvb2xlYW4+KGluaXRpYWxNZW51U3RhdGUpO1xuICAgICAgICB0aGlzLm1lbnVPcGVuU3RhdGUkID0gdGhpcy5tZW51T3BlblN0YXRlU3ViamVjdC5hc09ic2VydmFibGUoKTtcblxuICAgICAgICBjb25zdCBzdGVwT3ZlciA9IHRoaXMuc3RlcE92ZXIgfHwgU2lkZW5hdkxheW91dENvbXBvbmVudC5TVEVQX09WRVI7XG4gICAgICAgIHRoaXMuaXNNZW51TWluaW1pemVkID0gaW5pdGlhbE1lbnVTdGF0ZTtcblxuICAgICAgICB0aGlzLm1lZGlhUXVlcnlMaXN0ID0gdGhpcy5tZWRpYU1hdGNoZXIubWF0Y2hNZWRpYShgKG1heC13aWR0aDogJHtzdGVwT3Zlcn1weClgKTtcbiAgICAgICAgdGhpcy5tZWRpYVF1ZXJ5TGlzdC5hZGRMaXN0ZW5lcih0aGlzLm9uTWVkaWFRdWVyeUNoYW5nZSk7XG5cbiAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZXNTZXJ2aWNlXG4gICAgICAgICAgICAuc2VsZWN0KCd0ZXh0T3JpZW50YXRpb24nKVxuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMub25EZXN0cm95JCkpXG4gICAgICAgICAgICAuc3Vic2NyaWJlKChkaXJlY3Rpb246IERpcmVjdGlvbikgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuZGlyID0gZGlyZWN0aW9uO1xuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgICAgICB0aGlzLnRlbXBsYXRlQ29udGV4dC50b2dnbGVNZW51ID0gdGhpcy50b2dnbGVNZW51LmJpbmQodGhpcyk7XG4gICAgfVxuXG4gICAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XG4gICAgICAgIHRoaXMubWVkaWFRdWVyeUxpc3QucmVtb3ZlTGlzdGVuZXIodGhpcy5vbk1lZGlhUXVlcnlDaGFuZ2UpO1xuICAgICAgICB0aGlzLm9uRGVzdHJveSQubmV4dCh0cnVlKTtcbiAgICAgICAgdGhpcy5vbkRlc3Ryb3kkLmNvbXBsZXRlKCk7XG4gICAgfVxuXG4gICAgdG9nZ2xlTWVudSgpIHtcbiAgICAgICAgaWYgKCF0aGlzLm1lZGlhUXVlcnlMaXN0Lm1hdGNoZXMpIHtcbiAgICAgICAgICAgIHRoaXMuaXNNZW51TWluaW1pemVkID0gIXRoaXMuaXNNZW51TWluaW1pemVkO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgdGhpcy5pc01lbnVNaW5pbWl6ZWQgPSBmYWxzZTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuY29udGFpbmVyLnRvZ2dsZU1lbnUoKTtcbiAgICAgICAgdGhpcy5leHBhbmRlZC5lbWl0KCF0aGlzLmlzTWVudU1pbmltaXplZCk7XG4gICAgfVxuXG4gICAgZ2V0IGlzTWVudU1pbmltaXplZCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzTWVudU1pbmltaXplZDtcbiAgICB9XG5cbiAgICBzZXQgaXNNZW51TWluaW1pemVkKG1lbnVTdGF0ZTogYm9vbGVhbikge1xuICAgICAgICB0aGlzLl9pc01lbnVNaW5pbWl6ZWQgPSBtZW51U3RhdGU7XG4gICAgICAgIHRoaXMubWVudU9wZW5TdGF0ZVN1YmplY3QubmV4dCghbWVudVN0YXRlKTtcbiAgICB9XG5cbiAgICBnZXQgaXNIZWFkZXJJbnNpZGUoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLm1lZGlhUXVlcnlMaXN0Lm1hdGNoZXM7XG4gICAgfVxuXG4gICAgZ2V0IGhlYWRlclRlbXBsYXRlKCk6IFRlbXBsYXRlUmVmPGFueT4ge1xuICAgICAgICByZXR1cm4gdGhpcy5oZWFkZXJEaXJlY3RpdmUgJiYgdGhpcy5oZWFkZXJEaXJlY3RpdmUudGVtcGxhdGUgfHwgdGhpcy5lbXB0eVRlbXBsYXRlO1xuICAgIH1cblxuICAgIGdldCBuYXZpZ2F0aW9uVGVtcGxhdGUoKTogVGVtcGxhdGVSZWY8YW55PiB7XG4gICAgICAgIHJldHVybiB0aGlzLm5hdmlnYXRpb25EaXJlY3RpdmUgJiYgdGhpcy5uYXZpZ2F0aW9uRGlyZWN0aXZlLnRlbXBsYXRlIHx8IHRoaXMuZW1wdHlUZW1wbGF0ZTtcbiAgICB9XG5cbiAgICBnZXQgY29udGVudFRlbXBsYXRlKCk6IFRlbXBsYXRlUmVmPGFueT4ge1xuICAgICAgICByZXR1cm4gdGhpcy5jb250ZW50RGlyZWN0aXZlICYmIHRoaXMuY29udGVudERpcmVjdGl2ZS50ZW1wbGF0ZSB8fCB0aGlzLmVtcHR5VGVtcGxhdGU7XG4gICAgfVxuXG4gICAgb25NZWRpYVF1ZXJ5Q2hhbmdlKCkge1xuICAgICAgICB0aGlzLmlzTWVudU1pbmltaXplZCA9IGZhbHNlO1xuICAgICAgICB0aGlzLmV4cGFuZGVkLmVtaXQoIXRoaXMuaXNNZW51TWluaW1pemVkKTtcbiAgICB9XG59XG4iXX0=