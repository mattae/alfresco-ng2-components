/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material.module';
import { SidenavLayoutContentDirective } from './directives/sidenav-layout-content.directive';
import { SidenavLayoutHeaderDirective } from './directives/sidenav-layout-header.directive';
import { SidenavLayoutNavigationDirective } from './directives/sidenav-layout-navigation.directive';
import { SidenavLayoutComponent } from './components/sidenav-layout/sidenav-layout.component';
import { LayoutContainerComponent } from './components/layout-container/layout-container.component';
import { SidebarActionMenuComponent, SidebarMenuDirective, SidebarMenuExpandIconDirective, SidebarMenuTitleIconDirective } from './components/sidebar-action/sidebar-action-menu.component';
import { HeaderLayoutComponent } from './components/header/header.component';
import { TranslateModule } from '@ngx-translate/core';
export class LayoutModule {
}
LayoutModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule,
                    MaterialModule,
                    RouterModule,
                    TranslateModule.forChild()
                ],
                exports: [
                    SidenavLayoutHeaderDirective,
                    SidenavLayoutContentDirective,
                    SidenavLayoutNavigationDirective,
                    SidenavLayoutComponent,
                    LayoutContainerComponent,
                    SidebarActionMenuComponent,
                    SidebarMenuDirective,
                    SidebarMenuExpandIconDirective,
                    SidebarMenuTitleIconDirective,
                    HeaderLayoutComponent
                ],
                declarations: [
                    SidenavLayoutHeaderDirective,
                    SidenavLayoutContentDirective,
                    SidenavLayoutNavigationDirective,
                    SidenavLayoutComponent,
                    LayoutContainerComponent,
                    SidebarActionMenuComponent,
                    SidebarMenuDirective,
                    SidebarMenuExpandIconDirective,
                    SidebarMenuTitleIconDirective,
                    HeaderLayoutComponent
                ]
            },] }
];
export { LayoutModule as SidenavLayoutModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGF5b3V0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImxheW91dC9sYXlvdXQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDOUYsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDNUYsT0FBTyxFQUFFLGdDQUFnQyxFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDcEcsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDOUYsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sMERBQTBELENBQUM7QUFDcEcsT0FBTyxFQUFFLDBCQUEwQixFQUFFLG9CQUFvQixFQUNyRCw4QkFBOEIsRUFBRSw2QkFBNkIsRUFBRSxNQUFNLDJEQUEyRCxDQUFDO0FBQ3JJLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQWlDdEQsTUFBTSxPQUFPLFlBQVk7OztZQWhDeEIsUUFBUSxTQUFDO2dCQUNOLE9BQU8sRUFBRTtvQkFDTCxZQUFZO29CQUNaLGNBQWM7b0JBQ2QsWUFBWTtvQkFDWixlQUFlLENBQUMsUUFBUSxFQUFFO2lCQUM3QjtnQkFDRCxPQUFPLEVBQUU7b0JBQ0wsNEJBQTRCO29CQUM1Qiw2QkFBNkI7b0JBQzdCLGdDQUFnQztvQkFDaEMsc0JBQXNCO29CQUN0Qix3QkFBd0I7b0JBQ3hCLDBCQUEwQjtvQkFDMUIsb0JBQW9CO29CQUNwQiw4QkFBOEI7b0JBQzlCLDZCQUE2QjtvQkFDN0IscUJBQXFCO2lCQUN4QjtnQkFDRCxZQUFZLEVBQUU7b0JBQ1YsNEJBQTRCO29CQUM1Qiw2QkFBNkI7b0JBQzdCLGdDQUFnQztvQkFDaEMsc0JBQXNCO29CQUN0Qix3QkFBd0I7b0JBQ3hCLDBCQUEwQjtvQkFDMUIsb0JBQW9CO29CQUNwQiw4QkFBOEI7b0JBQzlCLDZCQUE2QjtvQkFDN0IscUJBQXFCO2lCQUN4QjthQUNKOztBQUVELE9BQU8sRUFBRSxZQUFZLElBQUksbUJBQW1CLEVBQUUsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSb3V0ZXJNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBNYXRlcmlhbE1vZHVsZSB9IGZyb20gJy4uL21hdGVyaWFsLm1vZHVsZSc7XHJcbmltcG9ydCB7IFNpZGVuYXZMYXlvdXRDb250ZW50RGlyZWN0aXZlIH0gZnJvbSAnLi9kaXJlY3RpdmVzL3NpZGVuYXYtbGF5b3V0LWNvbnRlbnQuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgU2lkZW5hdkxheW91dEhlYWRlckRpcmVjdGl2ZSB9IGZyb20gJy4vZGlyZWN0aXZlcy9zaWRlbmF2LWxheW91dC1oZWFkZXIuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgU2lkZW5hdkxheW91dE5hdmlnYXRpb25EaXJlY3RpdmUgfSBmcm9tICcuL2RpcmVjdGl2ZXMvc2lkZW5hdi1sYXlvdXQtbmF2aWdhdGlvbi5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBTaWRlbmF2TGF5b3V0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3NpZGVuYXYtbGF5b3V0L3NpZGVuYXYtbGF5b3V0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IExheW91dENvbnRhaW5lckNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9sYXlvdXQtY29udGFpbmVyL2xheW91dC1jb250YWluZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU2lkZWJhckFjdGlvbk1lbnVDb21wb25lbnQsIFNpZGViYXJNZW51RGlyZWN0aXZlLFxyXG4gICAgU2lkZWJhck1lbnVFeHBhbmRJY29uRGlyZWN0aXZlLCBTaWRlYmFyTWVudVRpdGxlSWNvbkRpcmVjdGl2ZSB9IGZyb20gJy4vY29tcG9uZW50cy9zaWRlYmFyLWFjdGlvbi9zaWRlYmFyLWFjdGlvbi1tZW51LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IEhlYWRlckxheW91dENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9oZWFkZXIvaGVhZGVyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZU1vZHVsZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICBNYXRlcmlhbE1vZHVsZSxcclxuICAgICAgICBSb3V0ZXJNb2R1bGUsXHJcbiAgICAgICAgVHJhbnNsYXRlTW9kdWxlLmZvckNoaWxkKClcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgU2lkZW5hdkxheW91dEhlYWRlckRpcmVjdGl2ZSxcclxuICAgICAgICBTaWRlbmF2TGF5b3V0Q29udGVudERpcmVjdGl2ZSxcclxuICAgICAgICBTaWRlbmF2TGF5b3V0TmF2aWdhdGlvbkRpcmVjdGl2ZSxcclxuICAgICAgICBTaWRlbmF2TGF5b3V0Q29tcG9uZW50LFxyXG4gICAgICAgIExheW91dENvbnRhaW5lckNvbXBvbmVudCxcclxuICAgICAgICBTaWRlYmFyQWN0aW9uTWVudUNvbXBvbmVudCxcclxuICAgICAgICBTaWRlYmFyTWVudURpcmVjdGl2ZSxcclxuICAgICAgICBTaWRlYmFyTWVudUV4cGFuZEljb25EaXJlY3RpdmUsXHJcbiAgICAgICAgU2lkZWJhck1lbnVUaXRsZUljb25EaXJlY3RpdmUsXHJcbiAgICAgICAgSGVhZGVyTGF5b3V0Q29tcG9uZW50XHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBbXHJcbiAgICAgICAgU2lkZW5hdkxheW91dEhlYWRlckRpcmVjdGl2ZSxcclxuICAgICAgICBTaWRlbmF2TGF5b3V0Q29udGVudERpcmVjdGl2ZSxcclxuICAgICAgICBTaWRlbmF2TGF5b3V0TmF2aWdhdGlvbkRpcmVjdGl2ZSxcclxuICAgICAgICBTaWRlbmF2TGF5b3V0Q29tcG9uZW50LFxyXG4gICAgICAgIExheW91dENvbnRhaW5lckNvbXBvbmVudCxcclxuICAgICAgICBTaWRlYmFyQWN0aW9uTWVudUNvbXBvbmVudCxcclxuICAgICAgICBTaWRlYmFyTWVudURpcmVjdGl2ZSxcclxuICAgICAgICBTaWRlYmFyTWVudUV4cGFuZEljb25EaXJlY3RpdmUsXHJcbiAgICAgICAgU2lkZWJhck1lbnVUaXRsZUljb25EaXJlY3RpdmUsXHJcbiAgICAgICAgSGVhZGVyTGF5b3V0Q29tcG9uZW50XHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMYXlvdXRNb2R1bGUge31cclxuZXhwb3J0IHsgTGF5b3V0TW9kdWxlIGFzIFNpZGVuYXZMYXlvdXRNb2R1bGUgfTtcclxuIl19