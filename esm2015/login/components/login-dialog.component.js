/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Inject, ViewEncapsulation, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { LoginDialogPanelComponent } from './login-dialog-panel.component';
export class LoginDialogComponent {
    /**
     * @param {?} data
     */
    constructor(data) {
        this.data = data;
        this.buttonActionName = '';
        this.buttonActionName = data.actionName ? `LOGIN.DIALOG.${data.actionName.toUpperCase()}` : 'LOGIN.DIALOG.CHOOSE';
    }
    /**
     * @return {?}
     */
    close() {
        this.data.logged.complete();
    }
    /**
     * @return {?}
     */
    submitForm() {
        this.loginPanel.submitForm();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onLoginSuccess(event) {
        this.data.logged.next(event);
        this.close();
    }
    /**
     * @return {?}
     */
    isFormValid() {
        return this.loginPanel ? this.loginPanel.isValid() : false;
    }
}
LoginDialogComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-login-dialog',
                template: "<header\r\n    mat-dialog-title\r\n    data-automation-id=\"login-dialog-title\">{{data?.title}}\r\n</header>\r\n\r\n<mat-dialog-content class=\"adf-login-dialog-content\">\r\n    <adf-login-dialog-panel #adfLoginPanel\r\n                            (success)=\"onLoginSuccess($event)\">\r\n    </adf-login-dialog-panel>\r\n</mat-dialog-content>\r\n\r\n<mat-dialog-actions align=\"end\">\r\n    <button\r\n        mat-button\r\n        (click)=\"close()\"\r\n        data-automation-id=\"login-dialog-actions-cancel\">{{ 'LOGIN.DIALOG.CANCEL' | translate }}\r\n    </button>\r\n\r\n    <button mat-button\r\n        class=\"choose-action\"\r\n        data-automation-id=\"login-dialog-actions-perform\"\r\n        [disabled]=\"!isFormValid()\"\r\n        (click)=\"submitForm()\">{{ buttonActionName | translate}}\r\n    </button>\r\n</mat-dialog-actions>\r\n",
                encapsulation: ViewEncapsulation.None,
                styles: [""]
            }] }
];
/** @nocollapse */
LoginDialogComponent.ctorParameters = () => [
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
];
LoginDialogComponent.propDecorators = {
    loginPanel: [{ type: ViewChild, args: ['adfLoginPanel', { static: true },] }]
};
if (false) {
    /** @type {?} */
    LoginDialogComponent.prototype.loginPanel;
    /** @type {?} */
    LoginDialogComponent.prototype.buttonActionName;
    /** @type {?} */
    LoginDialogComponent.prototype.data;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tZGlhbG9nLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImxvZ2luL2NvbXBvbmVudHMvbG9naW4tZGlhbG9nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQ0EsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUVwRCxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQU8zRSxNQUFNLE9BQU8sb0JBQW9COzs7O0lBTzdCLFlBQTRDLElBQThCO1FBQTlCLFNBQUksR0FBSixJQUFJLENBQTBCO1FBRjFFLHFCQUFnQixHQUFHLEVBQUUsQ0FBQztRQUdsQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMscUJBQXFCLENBQUM7SUFDdEgsQ0FBQzs7OztJQUVELEtBQUs7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUNoQyxDQUFDOzs7O0lBRUQsVUFBVTtRQUNOLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFRCxjQUFjLENBQUMsS0FBVTtRQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2pCLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDL0QsQ0FBQzs7O1lBaENKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1Qix1MkJBQTRDO2dCQUU1QyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7YUFDeEM7Ozs7NENBUWdCLE1BQU0sU0FBQyxlQUFlOzs7eUJBTGxDLFNBQVMsU0FBQyxlQUFlLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDOzs7O0lBQTFDLDBDQUNzQzs7SUFFdEMsZ0RBQXNCOztJQUVWLG9DQUE4RCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxuXG4vKiFcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxuICpcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cbiAqL1xuXG5pbXBvcnQgeyBDb21wb25lbnQsIEluamVjdCwgVmlld0VuY2Fwc3VsYXRpb24sIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTUFUX0RJQUxPR19EQVRBIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHsgTG9naW5EaWFsb2dDb21wb25lbnREYXRhIH0gZnJvbSAnLi9sb2dpbi1kaWFsb2ctY29tcG9uZW50LWRhdGEuaW50ZXJmYWNlJztcbmltcG9ydCB7IExvZ2luRGlhbG9nUGFuZWxDb21wb25lbnQgfSBmcm9tICcuL2xvZ2luLWRpYWxvZy1wYW5lbC5jb21wb25lbnQnO1xuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdhZGYtbG9naW4tZGlhbG9nJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vbG9naW4tZGlhbG9nLmNvbXBvbmVudC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9sb2dpbi1kaWFsb2cuY29tcG9uZW50LnNjc3MnXSxcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXG59KVxuZXhwb3J0IGNsYXNzIExvZ2luRGlhbG9nQ29tcG9uZW50IHtcblxuICAgIEBWaWV3Q2hpbGQoJ2FkZkxvZ2luUGFuZWwnLCB7c3RhdGljOiB0cnVlfSlcbiAgICBsb2dpblBhbmVsOiBMb2dpbkRpYWxvZ1BhbmVsQ29tcG9uZW50O1xuXG4gICAgYnV0dG9uQWN0aW9uTmFtZSA9ICcnO1xuXG4gICAgY29uc3RydWN0b3IoQEluamVjdChNQVRfRElBTE9HX0RBVEEpIHB1YmxpYyBkYXRhOiBMb2dpbkRpYWxvZ0NvbXBvbmVudERhdGEpIHtcbiAgICAgICAgdGhpcy5idXR0b25BY3Rpb25OYW1lID0gZGF0YS5hY3Rpb25OYW1lID8gYExPR0lOLkRJQUxPRy4ke2RhdGEuYWN0aW9uTmFtZS50b1VwcGVyQ2FzZSgpfWAgOiAnTE9HSU4uRElBTE9HLkNIT09TRSc7XG4gICAgfVxuXG4gICAgY2xvc2UoKSB7XG4gICAgICAgIHRoaXMuZGF0YS5sb2dnZWQuY29tcGxldGUoKTtcbiAgICB9XG5cbiAgICBzdWJtaXRGb3JtKCk6IHZvaWQge1xuICAgICAgICB0aGlzLmxvZ2luUGFuZWwuc3VibWl0Rm9ybSgpO1xuICAgIH1cblxuICAgIG9uTG9naW5TdWNjZXNzKGV2ZW50OiBhbnkpIHtcbiAgICAgICAgdGhpcy5kYXRhLmxvZ2dlZC5uZXh0KGV2ZW50KTtcbiAgICAgICAgdGhpcy5jbG9zZSgpO1xuICAgIH1cblxuICAgIGlzRm9ybVZhbGlkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5sb2dpblBhbmVsID8gdGhpcy5sb2dpblBhbmVsLmlzVmFsaWQoKSA6IGZhbHNlO1xuICAgIH1cbn1cbiJdfQ==