/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { AuthenticationService } from '../../services/authentication.service';
import { LogService } from '../../services/log.service';
import { TranslationService } from '../../services/translation.service';
import { UserPreferencesService } from '../../services/user-preferences.service';
import { LoginErrorEvent } from '../models/login-error.event';
import { LoginSubmitEvent } from '../models/login-submit.event';
import { LoginSuccessEvent } from '../models/login-success.event';
import { AppConfigService, AppConfigValues } from '../../app-config/app-config.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
/** @enum {number} */
const LoginSteps = {
    Landing: 0,
    Checking: 1,
    Welcome: 2,
};
LoginSteps[LoginSteps.Landing] = 'Landing';
LoginSteps[LoginSteps.Checking] = 'Checking';
LoginSteps[LoginSteps.Welcome] = 'Welcome';
/**
 * @record
 */
function ValidationMessage() { }
if (false) {
    /** @type {?} */
    ValidationMessage.prototype.value;
    /** @type {?|undefined} */
    ValidationMessage.prototype.params;
}
export class LoginComponent {
    /**
     * Constructor
     * @param {?} _fb
     * @param {?} authService
     * @param {?} translateService
     * @param {?} logService
     * @param {?} router
     * @param {?} appConfig
     * @param {?} userPreferences
     * @param {?} location
     * @param {?} route
     * @param {?} sanitizer
     */
    constructor(_fb, authService, translateService, logService, router, appConfig, userPreferences, location, route, sanitizer) {
        this._fb = _fb;
        this.authService = authService;
        this.translateService = translateService;
        this.logService = logService;
        this.router = router;
        this.appConfig = appConfig;
        this.userPreferences = userPreferences;
        this.location = location;
        this.route = route;
        this.sanitizer = sanitizer;
        this.isPasswordShow = false;
        /**
         * Should the `Remember me` checkbox be shown? When selected, this
         * option will remember the logged-in user after the browser is closed
         * to avoid logging in repeatedly.
         */
        this.showRememberMe = true;
        /**
         * Should the extra actions (`Need Help`, `Register`, etc) be shown?
         */
        this.showLoginActions = true;
        /**
         * Sets the URL of the NEED HELP link in the footer.
         */
        this.needHelpLink = '';
        /**
         * Sets the URL of the REGISTER link in the footer.
         */
        this.registerLink = '';
        /**
         * Path to a custom logo image.
         */
        this.logoImageUrl = './assets/images/alfresco-logo.svg';
        /**
         * Path to a custom background image.
         */
        this.backgroundImageUrl = './assets/images/background.svg';
        /**
         * The copyright text below the login box.
         */
        this.copyrightText = '\u00A9 2016 Alfresco Software, Inc. All Rights Reserved.';
        /**
         * Route to redirect to on successful login.
         */
        this.successRoute = null;
        /**
         * Emitted when the login is successful.
         */
        this.success = new EventEmitter();
        /**
         * Emitted when the login fails.
         */
        this.error = new EventEmitter();
        /**
         * Emitted when the login form is submitted.
         */
        this.executeSubmit = new EventEmitter();
        this.implicitFlow = false;
        this.isError = false;
        this.actualLoginStep = LoginSteps.Landing;
        this.LoginSteps = LoginSteps;
        this.rememberMe = true;
        this.minLength = 2;
        this.onDestroy$ = new Subject();
        this.initFormError();
        this.initFormFieldsMessages();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.authService.isOauth()) {
            /** @type {?} */
            const oauth = this.appConfig.get(AppConfigValues.OAUTHCONFIG, null);
            if (oauth && oauth.implicitFlow) {
                this.implicitFlow = true;
            }
        }
        if (this.authService.isEcmLoggedIn() || this.authService.isBpmLoggedIn()) {
            this.location.forward();
        }
        else {
            this.route.queryParams.subscribe((/**
             * @param {?} params
             * @return {?}
             */
            (params) => {
                /** @type {?} */
                const url = params['redirectUrl'];
                /** @type {?} */
                const provider = this.appConfig.get(AppConfigValues.PROVIDERS);
                this.authService.setRedirect({ provider, url });
            }));
        }
        if (this.hasCustomFieldsValidation()) {
            this.form = this._fb.group(this.fieldsValidation);
        }
        else {
            this.initFormFieldsDefault();
            this.initFormFieldsMessagesDefault();
        }
        this.form.valueChanges
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} data
         * @return {?}
         */
        data => this.onValueChanged(data)));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    }
    /**
     * @return {?}
     */
    submit() {
        this.onSubmit(this.form.value);
    }
    /**
     * Method called on submit form
     * @param {?} values
     * @return {?}
     */
    onSubmit(values) {
        this.disableError();
        if (this.authService.isOauth() && !this.authService.isSSODiscoveryConfigured()) {
            this.errorMsg = 'LOGIN.MESSAGES.SSO-WRONG-CONFIGURATION';
            this.isError = true;
        }
        else {
            /** @type {?} */
            const args = new LoginSubmitEvent({
                controls: { username: this.form.controls.username }
            });
            this.executeSubmit.emit(args);
            if (args.defaultPrevented) {
                return false;
            }
            else {
                this.performLogin(values);
            }
        }
    }
    /**
     * @return {?}
     */
    implicitLogin() {
        if (this.authService.isOauth() && !this.authService.isSSODiscoveryConfigured()) {
            this.errorMsg = 'LOGIN.MESSAGES.SSO-WRONG-CONFIGURATION';
            this.isError = true;
        }
        else {
            this.authService.ssoImplicitLogin();
        }
    }
    /**
     * The method check the error in the form and push the error in the formError object
     * @param {?} data
     * @return {?}
     */
    onValueChanged(data) {
        this.disableError();
        for (const field in this.formError) {
            if (field) {
                this.formError[field] = '';
                /** @type {?} */
                const hasError = (this.form.controls[field].errors && data[field] !== '') ||
                    (this.form.controls[field].dirty &&
                        !this.form.controls[field].valid);
                if (hasError) {
                    for (const key in this.form.controls[field].errors) {
                        if (key) {
                            /** @type {?} */
                            const message = this._message[field][key];
                            if (message && message.value) {
                                /** @type {?} */
                                const translated = this.translateService.instant(message.value, message.params);
                                this.formError[field] += translated;
                            }
                        }
                    }
                }
            }
        }
    }
    /**
     * @private
     * @param {?} values
     * @return {?}
     */
    performLogin(values) {
        this.actualLoginStep = LoginSteps.Checking;
        this.authService
            .login(values.username, values.password, this.rememberMe)
            .subscribe((/**
         * @param {?} token
         * @return {?}
         */
        (token) => {
            /** @type {?} */
            const redirectUrl = this.authService.getRedirect();
            this.actualLoginStep = LoginSteps.Welcome;
            this.userPreferences.setStoragePrefix(values.username);
            values.password = null;
            this.success.emit(new LoginSuccessEvent(token, values.username, null));
            if (redirectUrl) {
                this.authService.setRedirect(null);
                this.router.navigateByUrl(redirectUrl);
            }
            else if (this.successRoute) {
                this.router.navigate([this.successRoute]);
            }
        }), (/**
         * @param {?} err
         * @return {?}
         */
        (err) => {
            this.actualLoginStep = LoginSteps.Landing;
            this.displayErrorMessage(err);
            this.isError = true;
            this.error.emit(new LoginErrorEvent(err));
        }), (/**
         * @return {?}
         */
        () => this.logService.info('Login done')));
    }
    /**
     * Check and display the right error message in the UI
     * @private
     * @param {?} err
     * @return {?}
     */
    displayErrorMessage(err) {
        if (err.error &&
            err.error.crossDomain &&
            err.error.message.indexOf('Access-Control-Allow-Origin') !== -1) {
            this.errorMsg = err.error.message;
        }
        else if (err.status === 403 &&
            err.message.indexOf('Invalid CSRF-token') !== -1) {
            this.errorMsg = 'LOGIN.MESSAGES.LOGIN-ERROR-CSRF';
        }
        else if (err.status === 403 &&
            err.message.indexOf('The system is currently in read-only mode') !==
                -1) {
            this.errorMsg = 'LOGIN.MESSAGES.LOGIN-ECM-LICENSE';
        }
        else {
            this.errorMsg = 'LOGIN.MESSAGES.LOGIN-ERROR-CREDENTIALS';
        }
    }
    /**
     * Add a custom form error for a field
     * @param {?} field
     * @param {?} msg
     * @return {?}
     */
    addCustomFormError(field, msg) {
        this.formError[field] += msg;
    }
    /**
     * Add a custom validation rule error for a field
     * @param {?} field
     * @param {?} ruleId - i.e. required | minlength | maxlength
     * @param {?} msg
     * @param {?=} params
     * @return {?}
     */
    addCustomValidationError(field, ruleId, msg, params) {
        this._message[field][ruleId] = {
            value: msg,
            params
        };
    }
    /**
     * Display and hide the password value.
     * @return {?}
     */
    toggleShowPassword() {
        this.isPasswordShow = !this.isPasswordShow;
    }
    /**
     * The method return if a field is valid or not
     * @param {?} field
     * @return {?}
     */
    isErrorStyle(field) {
        return !field.valid && field.dirty && !field.pristine;
    }
    /**
     * Trim username
     * @param {?} event
     * @return {?}
     */
    trimUsername(event) {
        event.target.value = event.target.value.trim();
    }
    /**
     * @return {?}
     */
    getBackgroundUrlImageUrl() {
        return this.sanitizer.bypassSecurityTrustStyle(`url(${this.backgroundImageUrl})`);
    }
    /**
     * Default formError values
     * @private
     * @return {?}
     */
    initFormError() {
        this.formError = {
            username: '',
            password: ''
        };
    }
    /**
     * Init form fields messages
     * @private
     * @return {?}
     */
    initFormFieldsMessages() {
        this._message = {
            username: {},
            password: {}
        };
    }
    /**
     * Default form fields messages
     * @private
     * @return {?}
     */
    initFormFieldsMessagesDefault() {
        this._message = {
            username: {
                required: {
                    value: 'LOGIN.MESSAGES.USERNAME-REQUIRED'
                },
                minLength: {
                    value: 'LOGIN.MESSAGES.USERNAME-MIN',
                    params: {
                        /**
                         * @return {?}
                         */
                        get minLength() {
                            return this.minLength;
                        }
                    }
                }
            },
            password: {
                required: {
                    value: 'LOGIN.MESSAGES.PASSWORD-REQUIRED'
                }
            }
        };
    }
    /**
     * @private
     * @return {?}
     */
    initFormFieldsDefault() {
        this.form = this._fb.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    }
    /**
     * Disable the error flag
     * @private
     * @return {?}
     */
    disableError() {
        this.isError = false;
        this.initFormError();
    }
    /**
     * @private
     * @return {?}
     */
    hasCustomFieldsValidation() {
        return this.fieldsValidation !== undefined;
    }
}
LoginComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-login',
                template: "<div class=\"adf-login-content\" [style.background-image]=\"getBackgroundUrlImageUrl()\">\r\n    <div class=\"adf-ie11FixerParent\">\r\n        <div class=\"adf-ie11FixerChild\">\r\n\r\n            <mat-card class=\"adf-login-card-wide\">\r\n                <form id=\"adf-login-form\" [formGroup]=\"form\" (submit)=\"onSubmit(form.value)\" autocomplete=\"off\">\r\n                    <mat-card-header>\r\n                        <mat-card-title>\r\n                            <div class=\"adf-alfresco-logo\">\r\n                                <!--HEADER TEMPLATE-->\r\n                                <ng-template *ngIf=\"headerTemplate\"\r\n                                             ngFor [ngForOf]=\"[data]\"\r\n                                             [ngForTemplate]=\"headerTemplate\">\r\n                                </ng-template>\r\n                                <img *ngIf=\"!headerTemplate\" id=\"adf-login-img-logo\" class=\"adf-img-logo\" [src]=\"logoImageUrl\"\r\n                                     alt=\"{{'LOGIN.LOGO' | translate }}\">\r\n                            </div>\r\n                        </mat-card-title>\r\n                    </mat-card-header>\r\n\r\n                    <mat-card-content class=\"adf-login-controls\">\r\n\r\n                        <!--ERRORS AREA-->\r\n                        <div class=\"adf-error-container\">\r\n                            <div *ngIf=\"isError\" id=\"login-error\" data-automation-id=\"login-error\"\r\n                                 class=\"adf-error  adf-error-message\">\r\n                                <mat-icon class=\"adf-error-icon\">warning</mat-icon>\r\n                                <span class=\"adf-login-error-message\">{{errorMsg | translate }}</span>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div *ngIf=\"!implicitFlow\">\r\n\r\n                            <!--USERNAME FIELD-->\r\n                            <div class=\"adf-login__field\"\r\n                                 [ngClass]=\"{'adf-is-invalid': isErrorStyle(form.controls.username)}\">\r\n                                <mat-form-field class=\"adf-full-width\" floatPlaceholder=\"never\" color=\"primary\">\r\n                                    <input matInput placeholder=\"{{'LOGIN.LABEL.USERNAME' | translate }}\"\r\n                                           type=\"text\"\r\n                                           class=\"adf-full-width\"\r\n                                           [formControl]=\"form.controls['username']\"\r\n                                           autocapitalize=\"none\"\r\n                                           id=\"username\"\r\n                                           data-automation-id=\"username\"\r\n                                           (blur)=\"trimUsername($event)\">\r\n                                </mat-form-field>\r\n\r\n                                <span class=\"adf-login-validation\" for=\"username\" *ngIf=\"formError['username']\">\r\n                                <span id=\"username-error\" class=\"adf-login-error\" data-automation-id=\"username-error\">{{formError['username'] | translate }}</span>\r\n                            </span>\r\n                            </div>\r\n\r\n                            <!--PASSWORD FIELD-->\r\n                            <div class=\"adf-login__field\">\r\n                                <mat-form-field class=\"adf-full-width\" floatPlaceholder=\"never\" color=\"primary\">\r\n                                    <input matInput placeholder=\"{{'LOGIN.LABEL.PASSWORD' | translate }}\"\r\n                                           [type]=\"isPasswordShow ? 'text' : 'password'\"\r\n                                           [formControl]=\"form.controls['password']\"\r\n                                           id=\"password\"\r\n                                           data-automation-id=\"password\">\r\n                                    <mat-icon *ngIf=\"isPasswordShow\" matSuffix class=\"adf-login-password-icon\"\r\n                                              data-automation-id=\"hide_password\" (click)=\"toggleShowPassword()\" (keyup.enter)=\"toggleShowPassword()\">\r\n                                        visibility\r\n                                    </mat-icon>\r\n                                    <mat-icon *ngIf=\"!isPasswordShow\" matSuffix class=\"adf-login-password-icon\"\r\n                                              data-automation-id=\"show_password\" (click)=\"toggleShowPassword()\" (keyup.enter)=\"toggleShowPassword()\">\r\n                                        visibility_off\r\n                                    </mat-icon>\r\n                                </mat-form-field>\r\n                                <span class=\"adf-login-validation\" for=\"password\" *ngIf=\"formError['password']\">\r\n                                <span id=\"password-required\" class=\"adf-login-error\"\r\n                                      data-automation-id=\"password-required\">{{formError['password'] | translate }}</span>\r\n                            </span>\r\n                            </div>\r\n\r\n                            <!--CUSTOM CONTENT-->\r\n                            <ng-content></ng-content>\r\n\r\n                            <br>\r\n                            <button type=\"submit\" id=\"login-button\"\r\n                                    class=\"adf-login-button\"\r\n                                    mat-raised-button color=\"primary\"\r\n                                    [class.adf-isChecking]=\"actualLoginStep === LoginSteps.Checking\"\r\n                                    [class.adf-isWelcome]=\"actualLoginStep === LoginSteps.Welcome\"\r\n                                    data-automation-id=\"login-button\" [disabled]=\"!form.valid\">\r\n\r\n                                <span *ngIf=\"actualLoginStep === LoginSteps.Landing\" class=\"adf-login-button-label\">{{ 'LOGIN.BUTTON.LOGIN' | translate }}</span>\r\n\r\n                                <div *ngIf=\"actualLoginStep === LoginSteps.Checking\"\r\n                                     class=\"adf-interactive-login-label\">\r\n                                    <span\r\n                                        class=\"adf-login-button-label\">{{ 'LOGIN.BUTTON.CHECKING' | translate }}</span>\r\n                                    <div class=\"adf-login-spinner-container\">\r\n                                        <mat-spinner id=\"checking-spinner\" class=\"adf-login-checking-spinner\"\r\n                                                     [diameter]=\"25\"></mat-spinner>\r\n                                    </div>\r\n                                </div>\r\n\r\n\r\n                                <div *ngIf=\"actualLoginStep === LoginSteps.Welcome\" class=\"adf-interactive-login-label\">\r\n                                    <span class=\"adf-login-button-label\">{{ 'LOGIN.BUTTON.WELCOME' | translate }}</span>\r\n                                    <mat-icon class=\"adf-welcome-icon\">done</mat-icon>\r\n                                </div>\r\n\r\n                            </button>\r\n                            <div *ngIf=\"showRememberMe\" class=\"adf-login__remember-me\">\r\n                                <mat-checkbox id=\"adf-login-remember\" color=\"primary\" class=\"adf-login-remember-me\"\r\n                                              [checked]=\"rememberMe\"\r\n                                              (change)=\"rememberMe = !rememberMe\">{{ 'LOGIN.LABEL.REMEMBER' | translate }}\r\n                                </mat-checkbox>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div *ngIf=\"implicitFlow\">\r\n                            <button type=\"button\" (click)=\"implicitLogin()\" id=\"login-button-sso\"\r\n                                    class=\"adf-login-button\"\r\n                                    mat-raised-button color=\"primary\"\r\n                                    data-automation-id=\"login-button-sso\">\r\n                                <span  class=\"adf-login-button-label\">{{ 'LOGIN.BUTTON.SSO' | translate }}</span>\r\n                            </button>\r\n                        </div>\r\n\r\n                    </mat-card-content>\r\n\r\n                    <mat-card-actions *ngIf=\"footerTemplate || showLoginActions\">\r\n\r\n                        <div class=\"adf-login-action-container\">\r\n                            <!--FOOTER TEMPLATE-->\r\n                            <ng-template *ngIf=\"footerTemplate\"\r\n                                         ngFor [ngForOf]=\"[data]\"\r\n                                         [ngForTemplate]=\"footerTemplate\">\r\n                            </ng-template>\r\n                            <div class=\"adf-login-action\" *ngIf=\"!footerTemplate && showLoginActions\">\r\n                                <div id=\"adf-login-action-left\" class=\"adf-login-action-left\">\r\n                                    <a href=\"{{needHelpLink}}\">{{'LOGIN.ACTION.HELP' | translate }}</a>\r\n                                </div>\r\n                                <div id=\"adf-login-action-right\" class=\"adf-login-action-right\">\r\n                                    <a href=\"{{registerLink}}\">{{'LOGIN.ACTION.REGISTER' | translate }}</a>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </mat-card-actions>\r\n\r\n                </form>\r\n            </mat-card>\r\n\r\n            <div class=\"adf-copyright\" data-automation-id=\"login-copyright\">\r\n                {{ copyrightText }}\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n",
                encapsulation: ViewEncapsulation.None,
                host: {
                    class: 'adf-login'
                },
                styles: [""]
            }] }
];
/** @nocollapse */
LoginComponent.ctorParameters = () => [
    { type: FormBuilder },
    { type: AuthenticationService },
    { type: TranslationService },
    { type: LogService },
    { type: Router },
    { type: AppConfigService },
    { type: UserPreferencesService },
    { type: Location },
    { type: ActivatedRoute },
    { type: DomSanitizer }
];
LoginComponent.propDecorators = {
    showRememberMe: [{ type: Input }],
    showLoginActions: [{ type: Input }],
    needHelpLink: [{ type: Input }],
    registerLink: [{ type: Input }],
    logoImageUrl: [{ type: Input }],
    backgroundImageUrl: [{ type: Input }],
    copyrightText: [{ type: Input }],
    fieldsValidation: [{ type: Input }],
    successRoute: [{ type: Input }],
    success: [{ type: Output }],
    error: [{ type: Output }],
    executeSubmit: [{ type: Output }]
};
if (false) {
    /** @type {?} */
    LoginComponent.prototype.isPasswordShow;
    /**
     * Should the `Remember me` checkbox be shown? When selected, this
     * option will remember the logged-in user after the browser is closed
     * to avoid logging in repeatedly.
     * @type {?}
     */
    LoginComponent.prototype.showRememberMe;
    /**
     * Should the extra actions (`Need Help`, `Register`, etc) be shown?
     * @type {?}
     */
    LoginComponent.prototype.showLoginActions;
    /**
     * Sets the URL of the NEED HELP link in the footer.
     * @type {?}
     */
    LoginComponent.prototype.needHelpLink;
    /**
     * Sets the URL of the REGISTER link in the footer.
     * @type {?}
     */
    LoginComponent.prototype.registerLink;
    /**
     * Path to a custom logo image.
     * @type {?}
     */
    LoginComponent.prototype.logoImageUrl;
    /**
     * Path to a custom background image.
     * @type {?}
     */
    LoginComponent.prototype.backgroundImageUrl;
    /**
     * The copyright text below the login box.
     * @type {?}
     */
    LoginComponent.prototype.copyrightText;
    /**
     * Custom validation rules for the login form.
     * @type {?}
     */
    LoginComponent.prototype.fieldsValidation;
    /**
     * Route to redirect to on successful login.
     * @type {?}
     */
    LoginComponent.prototype.successRoute;
    /**
     * Emitted when the login is successful.
     * @type {?}
     */
    LoginComponent.prototype.success;
    /**
     * Emitted when the login fails.
     * @type {?}
     */
    LoginComponent.prototype.error;
    /**
     * Emitted when the login form is submitted.
     * @type {?}
     */
    LoginComponent.prototype.executeSubmit;
    /** @type {?} */
    LoginComponent.prototype.implicitFlow;
    /** @type {?} */
    LoginComponent.prototype.form;
    /** @type {?} */
    LoginComponent.prototype.isError;
    /** @type {?} */
    LoginComponent.prototype.errorMsg;
    /** @type {?} */
    LoginComponent.prototype.actualLoginStep;
    /** @type {?} */
    LoginComponent.prototype.LoginSteps;
    /** @type {?} */
    LoginComponent.prototype.rememberMe;
    /** @type {?} */
    LoginComponent.prototype.formError;
    /** @type {?} */
    LoginComponent.prototype.minLength;
    /** @type {?} */
    LoginComponent.prototype.footerTemplate;
    /** @type {?} */
    LoginComponent.prototype.headerTemplate;
    /** @type {?} */
    LoginComponent.prototype.data;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype._message;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.onDestroy$;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype._fb;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.authService;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.translateService;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.logService;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.appConfig;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.userPreferences;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.location;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.route;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.sanitizer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsibG9naW4vY29tcG9uZW50cy9sb2dpbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUNILFNBQVMsRUFBRSxZQUFZLEVBQ3ZCLEtBQUssRUFBVSxNQUFNLEVBQWUsaUJBQWlCLEVBQ3hELE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBbUIsV0FBVyxFQUFhLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3JGLE9BQU8sRUFBRSxNQUFNLEVBQUUsY0FBYyxFQUFVLE1BQU0saUJBQWlCLENBQUM7QUFDakUsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUN4RSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUVqRixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDOUQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDaEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDbEUsT0FBTyxFQUNILGdCQUFnQixFQUNoQixlQUFlLEVBQ2xCLE1BQU0scUNBQXFDLENBQUM7QUFFN0MsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7SUFHdkMsVUFBVztJQUNYLFdBQVk7SUFDWixVQUFXOzs7Ozs7OztBQUdmLGdDQUdDOzs7SUFGRyxrQ0FBYzs7SUFDZCxtQ0FBYTs7QUFZakIsTUFBTSxPQUFPLGNBQWM7Ozs7Ozs7Ozs7Ozs7O0lBOEV2QixZQUNZLEdBQWdCLEVBQ2hCLFdBQWtDLEVBQ2xDLGdCQUFvQyxFQUNwQyxVQUFzQixFQUN0QixNQUFjLEVBQ2QsU0FBMkIsRUFDM0IsZUFBdUMsRUFDdkMsUUFBa0IsRUFDbEIsS0FBcUIsRUFDckIsU0FBdUI7UUFUdkIsUUFBRyxHQUFILEdBQUcsQ0FBYTtRQUNoQixnQkFBVyxHQUFYLFdBQVcsQ0FBdUI7UUFDbEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFvQjtRQUNwQyxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQUMzQixvQkFBZSxHQUFmLGVBQWUsQ0FBd0I7UUFDdkMsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUNsQixVQUFLLEdBQUwsS0FBSyxDQUFnQjtRQUNyQixjQUFTLEdBQVQsU0FBUyxDQUFjO1FBdkZuQyxtQkFBYyxHQUFZLEtBQUssQ0FBQzs7Ozs7O1FBUWhDLG1CQUFjLEdBQVksSUFBSSxDQUFDOzs7O1FBSS9CLHFCQUFnQixHQUFZLElBQUksQ0FBQzs7OztRQUlqQyxpQkFBWSxHQUFXLEVBQUUsQ0FBQzs7OztRQUkxQixpQkFBWSxHQUFXLEVBQUUsQ0FBQzs7OztRQUkxQixpQkFBWSxHQUFXLG1DQUFtQyxDQUFDOzs7O1FBSTNELHVCQUFrQixHQUFXLGdDQUFnQyxDQUFDOzs7O1FBSTlELGtCQUFhLEdBQVcsMERBQTBELENBQUM7Ozs7UUFRbkYsaUJBQVksR0FBVyxJQUFJLENBQUM7Ozs7UUFJNUIsWUFBTyxHQUFHLElBQUksWUFBWSxFQUFxQixDQUFDOzs7O1FBSWhELFVBQUssR0FBRyxJQUFJLFlBQVksRUFBbUIsQ0FBQzs7OztRQUk1QyxrQkFBYSxHQUFHLElBQUksWUFBWSxFQUFvQixDQUFDO1FBRXJELGlCQUFZLEdBQVksS0FBSyxDQUFDO1FBRzlCLFlBQU8sR0FBWSxLQUFLLENBQUM7UUFFekIsb0JBQWUsR0FBUSxVQUFVLENBQUMsT0FBTyxDQUFDO1FBQzFDLGVBQVUsR0FBUSxVQUFVLENBQUM7UUFDN0IsZUFBVSxHQUFZLElBQUksQ0FBQztRQUUzQixjQUFTLEdBQVcsQ0FBQyxDQUFDO1FBTWQsZUFBVSxHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7UUFvQnhDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztRQUNyQixJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQztJQUNsQyxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsRUFBRTs7a0JBQ3RCLEtBQUssR0FBcUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQW1CLGVBQWUsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDO1lBQ3ZHLElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxZQUFZLEVBQUU7Z0JBQzdCLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDO2FBQzVCO1NBQ0o7UUFFRCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsRUFBRTtZQUN0RSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQzNCO2FBQU07WUFDSCxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxTQUFTOzs7O1lBQUMsQ0FBQyxNQUFjLEVBQUUsRUFBRTs7c0JBQzFDLEdBQUcsR0FBRyxNQUFNLENBQUMsYUFBYSxDQUFDOztzQkFDM0IsUUFBUSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFTLGVBQWUsQ0FBQyxTQUFTLENBQUM7Z0JBRXRFLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEVBQUUsUUFBUSxFQUFFLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDbEQsQ0FBQyxFQUFDLENBQUM7U0FDUjtRQUVELElBQUksSUFBSSxDQUFDLHlCQUF5QixFQUFFLEVBQUU7WUFDbEMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztTQUNyRDthQUFNO1lBQ0gsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7WUFDN0IsSUFBSSxDQUFDLDZCQUE2QixFQUFFLENBQUM7U0FDeEM7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVk7YUFDakIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDaEMsU0FBUzs7OztRQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBQyxDQUFDO0lBQ3RELENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDOzs7O0lBRUQsTUFBTTtRQUNGLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNuQyxDQUFDOzs7Ozs7SUFPRCxRQUFRLENBQUMsTUFBVztRQUNoQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFFcEIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyx3QkFBd0IsRUFBRSxFQUFFO1lBQzVFLElBQUksQ0FBQyxRQUFRLEdBQUcsd0NBQXdDLENBQUM7WUFDekQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUM7U0FDdkI7YUFBTTs7a0JBQ0csSUFBSSxHQUFHLElBQUksZ0JBQWdCLENBQUM7Z0JBQzlCLFFBQVEsRUFBRSxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLEVBQUU7YUFDdEQsQ0FBQztZQUNGLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRTlCLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO2dCQUN2QixPQUFPLEtBQUssQ0FBQzthQUNoQjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQzdCO1NBQ0o7SUFDTCxDQUFDOzs7O0lBRUQsYUFBYTtRQUNULElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsd0JBQXdCLEVBQUUsRUFBRTtZQUM1RSxJQUFJLENBQUMsUUFBUSxHQUFHLHdDQUF3QyxDQUFDO1lBQ3pELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1NBQ3ZCO2FBQU07WUFDSCxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixFQUFFLENBQUM7U0FDdkM7SUFDTCxDQUFDOzs7Ozs7SUFNRCxjQUFjLENBQUMsSUFBUztRQUNwQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDcEIsS0FBSyxNQUFNLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hDLElBQUksS0FBSyxFQUFFO2dCQUNQLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEdBQUcsRUFBRSxDQUFDOztzQkFDckIsUUFBUSxHQUNWLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ3hELENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSzt3QkFDNUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxLQUFLLENBQUM7Z0JBQ3pDLElBQUksUUFBUSxFQUFFO29CQUNWLEtBQUssTUFBTSxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsTUFBTSxFQUFFO3dCQUNoRCxJQUFJLEdBQUcsRUFBRTs7a0NBQ0MsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxDQUFDOzRCQUN6QyxJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMsS0FBSyxFQUFFOztzQ0FDcEIsVUFBVSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxPQUFPLENBQUMsTUFBTSxDQUFDO2dDQUMvRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLFVBQVUsQ0FBQzs2QkFDdkM7eUJBQ0o7cUJBQ0o7aUJBQ0o7YUFDSjtTQUNKO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sWUFBWSxDQUFDLE1BQVc7UUFDNUIsSUFBSSxDQUFDLGVBQWUsR0FBRyxVQUFVLENBQUMsUUFBUSxDQUFDO1FBQzNDLElBQUksQ0FBQyxXQUFXO2FBQ1gsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDO2FBQ3hELFNBQVM7Ozs7UUFDTixDQUFDLEtBQVUsRUFBRSxFQUFFOztrQkFDTCxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUU7WUFFbEQsSUFBSSxDQUFDLGVBQWUsR0FBRyxVQUFVLENBQUMsT0FBTyxDQUFDO1lBQzFDLElBQUksQ0FBQyxlQUFlLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3ZELE1BQU0sQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUNiLElBQUksaUJBQWlCLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQ3RELENBQUM7WUFFRixJQUFJLFdBQVcsRUFBRTtnQkFDYixJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDbkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDMUM7aUJBQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFO2dCQUMxQixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDO2FBQzdDO1FBQ0wsQ0FBQzs7OztRQUNELENBQUMsR0FBUSxFQUFFLEVBQUU7WUFDVCxJQUFJLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUM7WUFDMUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzlCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1lBQ3BCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksZUFBZSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDOUMsQ0FBQzs7O1FBQ0QsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQzNDLENBQUM7SUFDVixDQUFDOzs7Ozs7O0lBS08sbUJBQW1CLENBQUMsR0FBUTtRQUNoQyxJQUNJLEdBQUcsQ0FBQyxLQUFLO1lBQ1QsR0FBRyxDQUFDLEtBQUssQ0FBQyxXQUFXO1lBQ3JCLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyw2QkFBNkIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUNqRTtZQUNFLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7U0FDckM7YUFBTSxJQUNILEdBQUcsQ0FBQyxNQUFNLEtBQUssR0FBRztZQUNsQixHQUFHLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUNsRDtZQUNFLElBQUksQ0FBQyxRQUFRLEdBQUcsaUNBQWlDLENBQUM7U0FDckQ7YUFBTSxJQUNILEdBQUcsQ0FBQyxNQUFNLEtBQUssR0FBRztZQUNsQixHQUFHLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQywyQ0FBMkMsQ0FBQztnQkFDaEUsQ0FBQyxDQUFDLEVBQ0o7WUFDRSxJQUFJLENBQUMsUUFBUSxHQUFHLGtDQUFrQyxDQUFDO1NBQ3REO2FBQU07WUFDSCxJQUFJLENBQUMsUUFBUSxHQUFHLHdDQUF3QyxDQUFDO1NBQzVEO0lBQ0wsQ0FBQzs7Ozs7OztJQU9NLGtCQUFrQixDQUFDLEtBQWEsRUFBRSxHQUFXO1FBQ2hELElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxDQUFDO0lBQ2pDLENBQUM7Ozs7Ozs7OztJQVFELHdCQUF3QixDQUNwQixLQUFhLEVBQ2IsTUFBYyxFQUNkLEdBQVcsRUFDWCxNQUFZO1FBRVosSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRztZQUMzQixLQUFLLEVBQUUsR0FBRztZQUNWLE1BQU07U0FDVCxDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFLRCxrQkFBa0I7UUFDZCxJQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUMvQyxDQUFDOzs7Ozs7SUFNRCxZQUFZLENBQUMsS0FBc0I7UUFDL0IsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7SUFDMUQsQ0FBQzs7Ozs7O0lBS0QsWUFBWSxDQUFDLEtBQVU7UUFDbkIsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDbkQsQ0FBQzs7OztJQUVELHdCQUF3QjtRQUNwQixPQUFRLElBQUksQ0FBQyxTQUFTLENBQUMsd0JBQXdCLENBQUMsT0FBTyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDO0lBQ3ZGLENBQUM7Ozs7OztJQUtPLGFBQWE7UUFDakIsSUFBSSxDQUFDLFNBQVMsR0FBRztZQUNiLFFBQVEsRUFBRSxFQUFFO1lBQ1osUUFBUSxFQUFFLEVBQUU7U0FDZixDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBS08sc0JBQXNCO1FBQzFCLElBQUksQ0FBQyxRQUFRLEdBQUc7WUFDWixRQUFRLEVBQUUsRUFBRTtZQUNaLFFBQVEsRUFBRSxFQUFFO1NBQ2YsQ0FBQztJQUNOLENBQUM7Ozs7OztJQUtPLDZCQUE2QjtRQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHO1lBQ1osUUFBUSxFQUFFO2dCQUNOLFFBQVEsRUFBRTtvQkFDTixLQUFLLEVBQUUsa0NBQWtDO2lCQUM1QztnQkFDRCxTQUFTLEVBQUU7b0JBQ1AsS0FBSyxFQUFFLDZCQUE2QjtvQkFDcEMsTUFBTSxFQUFFOzs7O3dCQUNKLElBQUksU0FBUzs0QkFDVCxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7d0JBQzFCLENBQUM7cUJBQ0o7aUJBQ0o7YUFFSjtZQUNELFFBQVEsRUFBRTtnQkFDTixRQUFRLEVBQUU7b0JBQ04sS0FBSyxFQUFFLGtDQUFrQztpQkFDNUM7YUFDSjtTQUNKLENBQUM7SUFDTixDQUFDOzs7OztJQUVPLHFCQUFxQjtRQUN6QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDO1lBQ3ZCLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQ25DLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1NBQ3RDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQUtPLFlBQVk7UUFDaEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDckIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3pCLENBQUM7Ozs7O0lBRU8seUJBQXlCO1FBQzdCLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixLQUFLLFNBQVMsQ0FBQztJQUMvQyxDQUFDOzs7WUEzWEosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxXQUFXO2dCQUNyQiwwcVRBQXFDO2dCQUVyQyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtnQkFDckMsSUFBSSxFQUFFO29CQUNGLEtBQUssRUFBRSxXQUFXO2lCQUNyQjs7YUFDSjs7OztZQXZDeUIsV0FBVztZQUc1QixxQkFBcUI7WUFFckIsa0JBQWtCO1lBRGxCLFVBQVU7WUFIVixNQUFNO1lBV1gsZ0JBQWdCO1lBTlgsc0JBQXNCO1lBSnRCLFFBQVE7WUFEQSxjQUFjO1lBZXRCLFlBQVk7Ozs2QkFnQ2hCLEtBQUs7K0JBSUwsS0FBSzsyQkFJTCxLQUFLOzJCQUlMLEtBQUs7MkJBSUwsS0FBSztpQ0FJTCxLQUFLOzRCQUlMLEtBQUs7K0JBSUwsS0FBSzsyQkFJTCxLQUFLO3NCQUlMLE1BQU07b0JBSU4sTUFBTTs0QkFJTixNQUFNOzs7O0lBbkRQLHdDQUFnQzs7Ozs7OztJQU9oQyx3Q0FDK0I7Ozs7O0lBRy9CLDBDQUNpQzs7Ozs7SUFHakMsc0NBQzBCOzs7OztJQUcxQixzQ0FDMEI7Ozs7O0lBRzFCLHNDQUMyRDs7Ozs7SUFHM0QsNENBQzhEOzs7OztJQUc5RCx1Q0FDbUY7Ozs7O0lBR25GLDBDQUNzQjs7Ozs7SUFHdEIsc0NBQzRCOzs7OztJQUc1QixpQ0FDZ0Q7Ozs7O0lBR2hELCtCQUM0Qzs7Ozs7SUFHNUMsdUNBQ3FEOztJQUVyRCxzQ0FBOEI7O0lBRTlCLDhCQUFnQjs7SUFDaEIsaUNBQXlCOztJQUN6QixrQ0FBaUI7O0lBQ2pCLHlDQUEwQzs7SUFDMUMsb0NBQTZCOztJQUM3QixvQ0FBMkI7O0lBQzNCLG1DQUFvQzs7SUFDcEMsbUNBQXNCOztJQUN0Qix3Q0FBaUM7O0lBQ2pDLHdDQUFpQzs7SUFDakMsOEJBQVU7Ozs7O0lBRVYsa0NBQXdFOzs7OztJQUN4RSxvQ0FBNEM7Ozs7O0lBU3hDLDZCQUF3Qjs7Ozs7SUFDeEIscUNBQTBDOzs7OztJQUMxQywwQ0FBNEM7Ozs7O0lBQzVDLG9DQUE4Qjs7Ozs7SUFDOUIsZ0NBQXNCOzs7OztJQUN0QixtQ0FBbUM7Ozs7O0lBQ25DLHlDQUErQzs7Ozs7SUFDL0Msa0NBQTBCOzs7OztJQUMxQiwrQkFBNkI7Ozs7O0lBQzdCLG1DQUErQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQge1xyXG4gICAgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsXHJcbiAgICBJbnB1dCwgT25Jbml0LCBPdXRwdXQsIFRlbXBsYXRlUmVmLCBWaWV3RW5jYXBzdWxhdGlvbiwgT25EZXN0cm95XHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFic3RyYWN0Q29udHJvbCwgRm9ybUJ1aWxkZXIsIEZvcm1Hcm91cCwgVmFsaWRhdG9ycyB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuaW1wb3J0IHsgUm91dGVyLCBBY3RpdmF0ZWRSb3V0ZSwgUGFyYW1zIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgTG9jYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBBdXRoZW50aWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVHJhbnNsYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvdHJhbnNsYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy91c2VyLXByZWZlcmVuY2VzLnNlcnZpY2UnO1xyXG5cclxuaW1wb3J0IHsgTG9naW5FcnJvckV2ZW50IH0gZnJvbSAnLi4vbW9kZWxzL2xvZ2luLWVycm9yLmV2ZW50JztcclxuaW1wb3J0IHsgTG9naW5TdWJtaXRFdmVudCB9IGZyb20gJy4uL21vZGVscy9sb2dpbi1zdWJtaXQuZXZlbnQnO1xyXG5pbXBvcnQgeyBMb2dpblN1Y2Nlc3NFdmVudCB9IGZyb20gJy4uL21vZGVscy9sb2dpbi1zdWNjZXNzLmV2ZW50JztcclxuaW1wb3J0IHtcclxuICAgIEFwcENvbmZpZ1NlcnZpY2UsXHJcbiAgICBBcHBDb25maWdWYWx1ZXNcclxufSBmcm9tICcuLi8uLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IE9hdXRoQ29uZmlnTW9kZWwgfSBmcm9tICcuLi8uLi9tb2RlbHMvb2F1dGgtY29uZmlnLm1vZGVsJztcclxuaW1wb3J0IHsgRG9tU2FuaXRpemVyIH0gZnJvbSAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlcic7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuZW51bSBMb2dpblN0ZXBzIHtcclxuICAgIExhbmRpbmcgPSAwLFxyXG4gICAgQ2hlY2tpbmcgPSAxLFxyXG4gICAgV2VsY29tZSA9IDJcclxufVxyXG5cclxuaW50ZXJmYWNlIFZhbGlkYXRpb25NZXNzYWdlIHtcclxuICAgIHZhbHVlOiBzdHJpbmc7XHJcbiAgICBwYXJhbXM/OiBhbnk7XHJcbn1cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtbG9naW4nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2xvZ2luLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2xvZ2luLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gICAgaG9zdDoge1xyXG4gICAgICAgIGNsYXNzOiAnYWRmLWxvZ2luJ1xyXG4gICAgfVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTG9naW5Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcbiAgICBpc1Bhc3N3b3JkU2hvdzogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2hvdWxkIHRoZSBgUmVtZW1iZXIgbWVgIGNoZWNrYm94IGJlIHNob3duPyBXaGVuIHNlbGVjdGVkLCB0aGlzXHJcbiAgICAgKiBvcHRpb24gd2lsbCByZW1lbWJlciB0aGUgbG9nZ2VkLWluIHVzZXIgYWZ0ZXIgdGhlIGJyb3dzZXIgaXMgY2xvc2VkXHJcbiAgICAgKiB0byBhdm9pZCBsb2dnaW5nIGluIHJlcGVhdGVkbHkuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzaG93UmVtZW1iZXJNZTogYm9vbGVhbiA9IHRydWU7XHJcblxyXG4gICAgLyoqIFNob3VsZCB0aGUgZXh0cmEgYWN0aW9ucyAoYE5lZWQgSGVscGAsIGBSZWdpc3RlcmAsIGV0YykgYmUgc2hvd24/ICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgc2hvd0xvZ2luQWN0aW9uczogYm9vbGVhbiA9IHRydWU7XHJcblxyXG4gICAgLyoqIFNldHMgdGhlIFVSTCBvZiB0aGUgTkVFRCBIRUxQIGxpbmsgaW4gdGhlIGZvb3Rlci4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBuZWVkSGVscExpbms6IHN0cmluZyA9ICcnO1xyXG5cclxuICAgIC8qKiBTZXRzIHRoZSBVUkwgb2YgdGhlIFJFR0lTVEVSIGxpbmsgaW4gdGhlIGZvb3Rlci4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICByZWdpc3Rlckxpbms6IHN0cmluZyA9ICcnO1xyXG5cclxuICAgIC8qKiBQYXRoIHRvIGEgY3VzdG9tIGxvZ28gaW1hZ2UuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgbG9nb0ltYWdlVXJsOiBzdHJpbmcgPSAnLi9hc3NldHMvaW1hZ2VzL2FsZnJlc2NvLWxvZ28uc3ZnJztcclxuXHJcbiAgICAvKiogUGF0aCB0byBhIGN1c3RvbSBiYWNrZ3JvdW5kIGltYWdlLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGJhY2tncm91bmRJbWFnZVVybDogc3RyaW5nID0gJy4vYXNzZXRzL2ltYWdlcy9iYWNrZ3JvdW5kLnN2Zyc7XHJcblxyXG4gICAgLyoqIFRoZSBjb3B5cmlnaHQgdGV4dCBiZWxvdyB0aGUgbG9naW4gYm94LiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGNvcHlyaWdodFRleHQ6IHN0cmluZyA9ICdcXHUwMEE5IDIwMTYgQWxmcmVzY28gU29mdHdhcmUsIEluYy4gQWxsIFJpZ2h0cyBSZXNlcnZlZC4nO1xyXG5cclxuICAgIC8qKiBDdXN0b20gdmFsaWRhdGlvbiBydWxlcyBmb3IgdGhlIGxvZ2luIGZvcm0uICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgZmllbGRzVmFsaWRhdGlvbjogYW55O1xyXG5cclxuICAgIC8qKiBSb3V0ZSB0byByZWRpcmVjdCB0byBvbiBzdWNjZXNzZnVsIGxvZ2luLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHN1Y2Nlc3NSb3V0ZTogc3RyaW5nID0gbnVsbDtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSBsb2dpbiBpcyBzdWNjZXNzZnVsLiAqL1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICBzdWNjZXNzID0gbmV3IEV2ZW50RW1pdHRlcjxMb2dpblN1Y2Nlc3NFdmVudD4oKTtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSBsb2dpbiBmYWlscy4gKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgZXJyb3IgPSBuZXcgRXZlbnRFbWl0dGVyPExvZ2luRXJyb3JFdmVudD4oKTtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSBsb2dpbiBmb3JtIGlzIHN1Ym1pdHRlZC4gKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgZXhlY3V0ZVN1Ym1pdCA9IG5ldyBFdmVudEVtaXR0ZXI8TG9naW5TdWJtaXRFdmVudD4oKTtcclxuXHJcbiAgICBpbXBsaWNpdEZsb3c6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBmb3JtOiBGb3JtR3JvdXA7XHJcbiAgICBpc0Vycm9yOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBlcnJvck1zZzogc3RyaW5nO1xyXG4gICAgYWN0dWFsTG9naW5TdGVwOiBhbnkgPSBMb2dpblN0ZXBzLkxhbmRpbmc7XHJcbiAgICBMb2dpblN0ZXBzOiBhbnkgPSBMb2dpblN0ZXBzO1xyXG4gICAgcmVtZW1iZXJNZTogYm9vbGVhbiA9IHRydWU7XHJcbiAgICBmb3JtRXJyb3I6IHsgW2lkOiBzdHJpbmddOiBzdHJpbmcgfTtcclxuICAgIG1pbkxlbmd0aDogbnVtYmVyID0gMjtcclxuICAgIGZvb3RlclRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xyXG4gICAgaGVhZGVyVGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XHJcbiAgICBkYXRhOiBhbnk7XHJcblxyXG4gICAgcHJpdmF0ZSBfbWVzc2FnZTogeyBbaWQ6IHN0cmluZ106IHsgW2lkOiBzdHJpbmddOiBWYWxpZGF0aW9uTWVzc2FnZSB9IH07XHJcbiAgICBwcml2YXRlIG9uRGVzdHJveSQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29uc3RydWN0b3JcclxuICAgICAqIEBwYXJhbSBfZmJcclxuICAgICAqIEBwYXJhbSBhdXRoU2VydmljZVxyXG4gICAgICogQHBhcmFtIHRyYW5zbGF0ZVxyXG4gICAgICovXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIF9mYjogRm9ybUJ1aWxkZXIsXHJcbiAgICAgICAgcHJpdmF0ZSBhdXRoU2VydmljZTogQXV0aGVudGljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgdHJhbnNsYXRlU2VydmljZTogVHJhbnNsYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgbG9nU2VydmljZTogTG9nU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxyXG4gICAgICAgIHByaXZhdGUgYXBwQ29uZmlnOiBBcHBDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgdXNlclByZWZlcmVuY2VzOiBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgbG9jYXRpb246IExvY2F0aW9uLFxyXG4gICAgICAgIHByaXZhdGUgcm91dGU6IEFjdGl2YXRlZFJvdXRlLFxyXG4gICAgICAgIHByaXZhdGUgc2FuaXRpemVyOiBEb21TYW5pdGl6ZXJcclxuICAgICkge1xyXG4gICAgICAgIHRoaXMuaW5pdEZvcm1FcnJvcigpO1xyXG4gICAgICAgIHRoaXMuaW5pdEZvcm1GaWVsZHNNZXNzYWdlcygpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmF1dGhTZXJ2aWNlLmlzT2F1dGgoKSkge1xyXG4gICAgICAgICAgICBjb25zdCBvYXV0aDogT2F1dGhDb25maWdNb2RlbCA9IHRoaXMuYXBwQ29uZmlnLmdldDxPYXV0aENvbmZpZ01vZGVsPihBcHBDb25maWdWYWx1ZXMuT0FVVEhDT05GSUcsIG51bGwpO1xyXG4gICAgICAgICAgICBpZiAob2F1dGggJiYgb2F1dGguaW1wbGljaXRGbG93KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmltcGxpY2l0RmxvdyA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmF1dGhTZXJ2aWNlLmlzRWNtTG9nZ2VkSW4oKSB8fCB0aGlzLmF1dGhTZXJ2aWNlLmlzQnBtTG9nZ2VkSW4oKSkge1xyXG4gICAgICAgICAgICB0aGlzLmxvY2F0aW9uLmZvcndhcmQoKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnJvdXRlLnF1ZXJ5UGFyYW1zLnN1YnNjcmliZSgocGFyYW1zOiBQYXJhbXMpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHVybCA9IHBhcmFtc1sncmVkaXJlY3RVcmwnXTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHByb3ZpZGVyID0gdGhpcy5hcHBDb25maWcuZ2V0PHN0cmluZz4oQXBwQ29uZmlnVmFsdWVzLlBST1ZJREVSUyk7XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5hdXRoU2VydmljZS5zZXRSZWRpcmVjdCh7IHByb3ZpZGVyLCB1cmwgfSk7XHJcbiAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5oYXNDdXN0b21GaWVsZHNWYWxpZGF0aW9uKCkpIHtcclxuICAgICAgICAgICAgdGhpcy5mb3JtID0gdGhpcy5fZmIuZ3JvdXAodGhpcy5maWVsZHNWYWxpZGF0aW9uKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmluaXRGb3JtRmllbGRzRGVmYXVsdCgpO1xyXG4gICAgICAgICAgICB0aGlzLmluaXRGb3JtRmllbGRzTWVzc2FnZXNEZWZhdWx0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuZm9ybS52YWx1ZUNoYW5nZXNcclxuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMub25EZXN0cm95JCkpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoZGF0YSA9PiB0aGlzLm9uVmFsdWVDaGFuZ2VkKGRhdGEpKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQubmV4dCh0cnVlKTtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQuY29tcGxldGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBzdWJtaXQoKSB7XHJcbiAgICAgICAgdGhpcy5vblN1Ym1pdCh0aGlzLmZvcm0udmFsdWUpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTWV0aG9kIGNhbGxlZCBvbiBzdWJtaXQgZm9ybVxyXG4gICAgICogQHBhcmFtIHZhbHVlc1xyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKi9cclxuICAgIG9uU3VibWl0KHZhbHVlczogYW55KSB7XHJcbiAgICAgICAgdGhpcy5kaXNhYmxlRXJyb3IoKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuYXV0aFNlcnZpY2UuaXNPYXV0aCgpICYmICF0aGlzLmF1dGhTZXJ2aWNlLmlzU1NPRGlzY292ZXJ5Q29uZmlndXJlZCgpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JNc2cgPSAnTE9HSU4uTUVTU0FHRVMuU1NPLVdST05HLUNPTkZJR1VSQVRJT04nO1xyXG4gICAgICAgICAgICB0aGlzLmlzRXJyb3IgPSB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGFyZ3MgPSBuZXcgTG9naW5TdWJtaXRFdmVudCh7XHJcbiAgICAgICAgICAgICAgICBjb250cm9sczogeyB1c2VybmFtZTogdGhpcy5mb3JtLmNvbnRyb2xzLnVzZXJuYW1lIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIHRoaXMuZXhlY3V0ZVN1Ym1pdC5lbWl0KGFyZ3MpO1xyXG5cclxuICAgICAgICAgICAgaWYgKGFyZ3MuZGVmYXVsdFByZXZlbnRlZCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wZXJmb3JtTG9naW4odmFsdWVzKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpbXBsaWNpdExvZ2luKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmF1dGhTZXJ2aWNlLmlzT2F1dGgoKSAmJiAhdGhpcy5hdXRoU2VydmljZS5pc1NTT0Rpc2NvdmVyeUNvbmZpZ3VyZWQoKSkge1xyXG4gICAgICAgICAgICB0aGlzLmVycm9yTXNnID0gJ0xPR0lOLk1FU1NBR0VTLlNTTy1XUk9ORy1DT05GSUdVUkFUSU9OJztcclxuICAgICAgICAgICAgdGhpcy5pc0Vycm9yID0gdHJ1ZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmF1dGhTZXJ2aWNlLnNzb0ltcGxpY2l0TG9naW4oKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUaGUgbWV0aG9kIGNoZWNrIHRoZSBlcnJvciBpbiB0aGUgZm9ybSBhbmQgcHVzaCB0aGUgZXJyb3IgaW4gdGhlIGZvcm1FcnJvciBvYmplY3RcclxuICAgICAqIEBwYXJhbSBkYXRhXHJcbiAgICAgKi9cclxuICAgIG9uVmFsdWVDaGFuZ2VkKGRhdGE6IGFueSkge1xyXG4gICAgICAgIHRoaXMuZGlzYWJsZUVycm9yKCk7XHJcbiAgICAgICAgZm9yIChjb25zdCBmaWVsZCBpbiB0aGlzLmZvcm1FcnJvcikge1xyXG4gICAgICAgICAgICBpZiAoZmllbGQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZm9ybUVycm9yW2ZpZWxkXSA9ICcnO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgaGFzRXJyb3IgPVxyXG4gICAgICAgICAgICAgICAgICAgICh0aGlzLmZvcm0uY29udHJvbHNbZmllbGRdLmVycm9ycyAmJiBkYXRhW2ZpZWxkXSAhPT0gJycpIHx8XHJcbiAgICAgICAgICAgICAgICAgICAgKHRoaXMuZm9ybS5jb250cm9sc1tmaWVsZF0uZGlydHkgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgIXRoaXMuZm9ybS5jb250cm9sc1tmaWVsZF0udmFsaWQpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGhhc0Vycm9yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZm9yIChjb25zdCBrZXkgaW4gdGhpcy5mb3JtLmNvbnRyb2xzW2ZpZWxkXS5lcnJvcnMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGtleSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgbWVzc2FnZSA9IHRoaXMuX21lc3NhZ2VbZmllbGRdW2tleV07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAobWVzc2FnZSAmJiBtZXNzYWdlLnZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdHJhbnNsYXRlZCA9IHRoaXMudHJhbnNsYXRlU2VydmljZS5pbnN0YW50KG1lc3NhZ2UudmFsdWUsIG1lc3NhZ2UucGFyYW1zKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm1FcnJvcltmaWVsZF0gKz0gdHJhbnNsYXRlZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHBlcmZvcm1Mb2dpbih2YWx1ZXM6IGFueSkge1xyXG4gICAgICAgIHRoaXMuYWN0dWFsTG9naW5TdGVwID0gTG9naW5TdGVwcy5DaGVja2luZztcclxuICAgICAgICB0aGlzLmF1dGhTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5sb2dpbih2YWx1ZXMudXNlcm5hbWUsIHZhbHVlcy5wYXNzd29yZCwgdGhpcy5yZW1lbWJlck1lKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgKHRva2VuOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCByZWRpcmVjdFVybCA9IHRoaXMuYXV0aFNlcnZpY2UuZ2V0UmVkaXJlY3QoKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hY3R1YWxMb2dpblN0ZXAgPSBMb2dpblN0ZXBzLldlbGNvbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZXMuc2V0U3RvcmFnZVByZWZpeCh2YWx1ZXMudXNlcm5hbWUpO1xyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlcy5wYXNzd29yZCA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdWNjZXNzLmVtaXQoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5ldyBMb2dpblN1Y2Nlc3NFdmVudCh0b2tlbiwgdmFsdWVzLnVzZXJuYW1lLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZWRpcmVjdFVybCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmF1dGhTZXJ2aWNlLnNldFJlZGlyZWN0KG51bGwpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKHJlZGlyZWN0VXJsKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3VjY2Vzc1JvdXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFt0aGlzLnN1Y2Nlc3NSb3V0ZV0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAoZXJyOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFjdHVhbExvZ2luU3RlcCA9IExvZ2luU3RlcHMuTGFuZGluZztcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRpc3BsYXlFcnJvck1lc3NhZ2UoZXJyKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzRXJyb3IgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZXJyb3IuZW1pdChuZXcgTG9naW5FcnJvckV2ZW50KGVycikpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICgpID0+IHRoaXMubG9nU2VydmljZS5pbmZvKCdMb2dpbiBkb25lJylcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrIGFuZCBkaXNwbGF5IHRoZSByaWdodCBlcnJvciBtZXNzYWdlIGluIHRoZSBVSVxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIGRpc3BsYXlFcnJvck1lc3NhZ2UoZXJyOiBhbnkpOiB2b2lkIHtcclxuICAgICAgICBpZiAoXHJcbiAgICAgICAgICAgIGVyci5lcnJvciAmJlxyXG4gICAgICAgICAgICBlcnIuZXJyb3IuY3Jvc3NEb21haW4gJiZcclxuICAgICAgICAgICAgZXJyLmVycm9yLm1lc3NhZ2UuaW5kZXhPZignQWNjZXNzLUNvbnRyb2wtQWxsb3ctT3JpZ2luJykgIT09IC0xXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JNc2cgPSBlcnIuZXJyb3IubWVzc2FnZTtcclxuICAgICAgICB9IGVsc2UgaWYgKFxyXG4gICAgICAgICAgICBlcnIuc3RhdHVzID09PSA0MDMgJiZcclxuICAgICAgICAgICAgZXJyLm1lc3NhZ2UuaW5kZXhPZignSW52YWxpZCBDU1JGLXRva2VuJykgIT09IC0xXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JNc2cgPSAnTE9HSU4uTUVTU0FHRVMuTE9HSU4tRVJST1ItQ1NSRic7XHJcbiAgICAgICAgfSBlbHNlIGlmIChcclxuICAgICAgICAgICAgZXJyLnN0YXR1cyA9PT0gNDAzICYmXHJcbiAgICAgICAgICAgIGVyci5tZXNzYWdlLmluZGV4T2YoJ1RoZSBzeXN0ZW0gaXMgY3VycmVudGx5IGluIHJlYWQtb25seSBtb2RlJykgIT09XHJcbiAgICAgICAgICAgIC0xXHJcbiAgICAgICAgKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JNc2cgPSAnTE9HSU4uTUVTU0FHRVMuTE9HSU4tRUNNLUxJQ0VOU0UnO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JNc2cgPSAnTE9HSU4uTUVTU0FHRVMuTE9HSU4tRVJST1ItQ1JFREVOVElBTFMnO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEFkZCBhIGN1c3RvbSBmb3JtIGVycm9yIGZvciBhIGZpZWxkXHJcbiAgICAgKiBAcGFyYW0gZmllbGRcclxuICAgICAqIEBwYXJhbSBtc2dcclxuICAgICAqL1xyXG4gICAgcHVibGljIGFkZEN1c3RvbUZvcm1FcnJvcihmaWVsZDogc3RyaW5nLCBtc2c6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuZm9ybUVycm9yW2ZpZWxkXSArPSBtc2c7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBZGQgYSBjdXN0b20gdmFsaWRhdGlvbiBydWxlIGVycm9yIGZvciBhIGZpZWxkXHJcbiAgICAgKiBAcGFyYW0gZmllbGRcclxuICAgICAqIEBwYXJhbSBydWxlSWQgLSBpLmUuIHJlcXVpcmVkIHwgbWlubGVuZ3RoIHwgbWF4bGVuZ3RoXHJcbiAgICAgKiBAcGFyYW0gbXNnXHJcbiAgICAgKi9cclxuICAgIGFkZEN1c3RvbVZhbGlkYXRpb25FcnJvcihcclxuICAgICAgICBmaWVsZDogc3RyaW5nLFxyXG4gICAgICAgIHJ1bGVJZDogc3RyaW5nLFxyXG4gICAgICAgIG1zZzogc3RyaW5nLFxyXG4gICAgICAgIHBhcmFtcz86IGFueVxyXG4gICAgKSB7XHJcbiAgICAgICAgdGhpcy5fbWVzc2FnZVtmaWVsZF1bcnVsZUlkXSA9IHtcclxuICAgICAgICAgICAgdmFsdWU6IG1zZyxcclxuICAgICAgICAgICAgcGFyYW1zXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERpc3BsYXkgYW5kIGhpZGUgdGhlIHBhc3N3b3JkIHZhbHVlLlxyXG4gICAgICovXHJcbiAgICB0b2dnbGVTaG93UGFzc3dvcmQoKSB7XHJcbiAgICAgICAgdGhpcy5pc1Bhc3N3b3JkU2hvdyA9ICF0aGlzLmlzUGFzc3dvcmRTaG93O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGhlIG1ldGhvZCByZXR1cm4gaWYgYSBmaWVsZCBpcyB2YWxpZCBvciBub3RcclxuICAgICAqIEBwYXJhbSBmaWVsZFxyXG4gICAgICovXHJcbiAgICBpc0Vycm9yU3R5bGUoZmllbGQ6IEFic3RyYWN0Q29udHJvbCkge1xyXG4gICAgICAgIHJldHVybiAhZmllbGQudmFsaWQgJiYgZmllbGQuZGlydHkgJiYgIWZpZWxkLnByaXN0aW5lO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVHJpbSB1c2VybmFtZVxyXG4gICAgICovXHJcbiAgICB0cmltVXNlcm5hbWUoZXZlbnQ6IGFueSkge1xyXG4gICAgICAgIGV2ZW50LnRhcmdldC52YWx1ZSA9IGV2ZW50LnRhcmdldC52YWx1ZS50cmltKCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0QmFja2dyb3VuZFVybEltYWdlVXJsKCkge1xyXG4gICAgICAgIHJldHVybiAgdGhpcy5zYW5pdGl6ZXIuYnlwYXNzU2VjdXJpdHlUcnVzdFN0eWxlKGB1cmwoJHt0aGlzLmJhY2tncm91bmRJbWFnZVVybH0pYCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZWZhdWx0IGZvcm1FcnJvciB2YWx1ZXNcclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBpbml0Rm9ybUVycm9yKCkge1xyXG4gICAgICAgIHRoaXMuZm9ybUVycm9yID0ge1xyXG4gICAgICAgICAgICB1c2VybmFtZTogJycsXHJcbiAgICAgICAgICAgIHBhc3N3b3JkOiAnJ1xyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBJbml0IGZvcm0gZmllbGRzIG1lc3NhZ2VzXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgaW5pdEZvcm1GaWVsZHNNZXNzYWdlcygpIHtcclxuICAgICAgICB0aGlzLl9tZXNzYWdlID0ge1xyXG4gICAgICAgICAgICB1c2VybmFtZToge30sXHJcbiAgICAgICAgICAgIHBhc3N3b3JkOiB7fVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZWZhdWx0IGZvcm0gZmllbGRzIG1lc3NhZ2VzXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgaW5pdEZvcm1GaWVsZHNNZXNzYWdlc0RlZmF1bHQoKSB7XHJcbiAgICAgICAgdGhpcy5fbWVzc2FnZSA9IHtcclxuICAgICAgICAgICAgdXNlcm5hbWU6IHtcclxuICAgICAgICAgICAgICAgIHJlcXVpcmVkOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6ICdMT0dJTi5NRVNTQUdFUy5VU0VSTkFNRS1SRVFVSVJFRCdcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBtaW5MZW5ndGg6IHtcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZTogJ0xPR0lOLk1FU1NBR0VTLlVTRVJOQU1FLU1JTicsXHJcbiAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGdldCBtaW5MZW5ndGgoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5taW5MZW5ndGg7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBwYXNzd29yZDoge1xyXG4gICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHtcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZTogJ0xPR0lOLk1FU1NBR0VTLlBBU1NXT1JELVJFUVVJUkVEJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGluaXRGb3JtRmllbGRzRGVmYXVsdCgpIHtcclxuICAgICAgICB0aGlzLmZvcm0gPSB0aGlzLl9mYi5ncm91cCh7XHJcbiAgICAgICAgICAgIHVzZXJuYW1lOiBbJycsIFZhbGlkYXRvcnMucmVxdWlyZWRdLFxyXG4gICAgICAgICAgICBwYXNzd29yZDogWycnLCBWYWxpZGF0b3JzLnJlcXVpcmVkXVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGlzYWJsZSB0aGUgZXJyb3IgZmxhZ1xyXG4gICAgICovXHJcbiAgICBwcml2YXRlIGRpc2FibGVFcnJvcigpIHtcclxuICAgICAgICB0aGlzLmlzRXJyb3IgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmluaXRGb3JtRXJyb3IoKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGhhc0N1c3RvbUZpZWxkc1ZhbGlkYXRpb24oKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZmllbGRzVmFsaWRhdGlvbiAhPT0gdW5kZWZpbmVkO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==