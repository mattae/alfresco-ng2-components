/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { of, throwError } from 'rxjs';
// TODO: should be extending AuthenticationService
export class AuthenticationMock /*extends AuthenticationService*/ {
    constructor() {
        this.redirectUrl = null;
    }
    /**
     * @param {?} url
     * @return {?}
     */
    setRedirectUrl(url) {
        this.redirectUrl = url;
    }
    /**
     * @return {?}
     */
    getRedirectUrl() {
        return this.redirectUrl ? this.redirectUrl.url : null;
    }
    // TODO: real auth service returns Observable<string>
    /**
     * @param {?} username
     * @param {?} password
     * @return {?}
     */
    login(username, password) {
        if (username === 'fake-username' && password === 'fake-password') {
            return of({ type: 'type', ticket: 'ticket' });
        }
        if (username === 'fake-username-CORS-error' && password === 'fake-password') {
            return throwError({
                error: {
                    crossDomain: true,
                    message: 'ERROR: the network is offline, Origin is not allowed by Access-Control-Allow-Origin'
                }
            });
        }
        if (username === 'fake-username-CSRF-error' && password === 'fake-password') {
            return throwError({ message: 'ERROR: Invalid CSRF-token', status: 403 });
        }
        if (username === 'fake-username-ECM-access-error' && password === 'fake-password') {
            return throwError({ message: 'ERROR: 00170728 Access Denied.  The system is currently in read-only mode', status: 403 });
        }
        return throwError('Fake server error');
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    AuthenticationMock.prototype.redirectUrl;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24uc2VydmljZS5tb2NrLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsibW9jay9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlLm1vY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFjLEVBQUUsRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7O0FBSWxELE1BQU0sT0FBTyxrQkFBa0IsQ0FBQyxpQ0FBaUM7SUFBakU7UUFDWSxnQkFBVyxHQUFxQixJQUFJLENBQUM7SUFtQ2pELENBQUM7Ozs7O0lBakNHLGNBQWMsQ0FBQyxHQUFxQjtRQUNoQyxJQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQztJQUMzQixDQUFDOzs7O0lBRUQsY0FBYztRQUNWLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUMxRCxDQUFDOzs7Ozs7O0lBR0QsS0FBSyxDQUFDLFFBQWdCLEVBQUUsUUFBZ0I7UUFDcEMsSUFBSSxRQUFRLEtBQUssZUFBZSxJQUFJLFFBQVEsS0FBSyxlQUFlLEVBQUU7WUFDOUQsT0FBTyxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUMsQ0FBQyxDQUFDO1NBQ2hEO1FBRUQsSUFBSSxRQUFRLEtBQUssMEJBQTBCLElBQUksUUFBUSxLQUFLLGVBQWUsRUFBRTtZQUN6RSxPQUFPLFVBQVUsQ0FBQztnQkFDZCxLQUFLLEVBQUU7b0JBQ0gsV0FBVyxFQUFFLElBQUk7b0JBQ2pCLE9BQU8sRUFBRSxxRkFBcUY7aUJBQ2pHO2FBQ0osQ0FBQyxDQUFDO1NBQ047UUFFRCxJQUFJLFFBQVEsS0FBSywwQkFBMEIsSUFBSSxRQUFRLEtBQUssZUFBZSxFQUFFO1lBQ3pFLE9BQU8sVUFBVSxDQUFDLEVBQUMsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUMsQ0FBQyxDQUFDO1NBQzFFO1FBRUQsSUFBSSxRQUFRLEtBQUssZ0NBQWdDLElBQUksUUFBUSxLQUFLLGVBQWUsRUFBRTtZQUMvRSxPQUFPLFVBQVUsQ0FBQyxFQUFDLE9BQU8sRUFBRSwyRUFBMkUsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFDLENBQUMsQ0FBQztTQUMxSDtRQUVELE9BQU8sVUFBVSxDQUFDLG1CQUFtQixDQUFDLENBQUM7SUFDM0MsQ0FBQztDQUNKOzs7Ozs7SUFuQ0cseUNBQTZDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IE9ic2VydmFibGUsIG9mLCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFJlZGlyZWN0aW9uTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvcmVkaXJlY3Rpb24ubW9kZWwnO1xyXG5cclxuLy8gVE9ETzogc2hvdWxkIGJlIGV4dGVuZGluZyBBdXRoZW50aWNhdGlvblNlcnZpY2VcclxuZXhwb3J0IGNsYXNzIEF1dGhlbnRpY2F0aW9uTW9jayAvKmV4dGVuZHMgQXV0aGVudGljYXRpb25TZXJ2aWNlKi8ge1xyXG4gICAgcHJpdmF0ZSByZWRpcmVjdFVybDogUmVkaXJlY3Rpb25Nb2RlbCA9IG51bGw7XHJcblxyXG4gICAgc2V0UmVkaXJlY3RVcmwodXJsOiBSZWRpcmVjdGlvbk1vZGVsKSB7XHJcbiAgICAgICAgdGhpcy5yZWRpcmVjdFVybCA9IHVybDtcclxuICAgIH1cclxuXHJcbiAgICBnZXRSZWRpcmVjdFVybCgpOiBzdHJpbmd8bnVsbCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucmVkaXJlY3RVcmwgPyB0aGlzLnJlZGlyZWN0VXJsLnVybCA6IG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gVE9ETzogcmVhbCBhdXRoIHNlcnZpY2UgcmV0dXJucyBPYnNlcnZhYmxlPHN0cmluZz5cclxuICAgIGxvZ2luKHVzZXJuYW1lOiBzdHJpbmcsIHBhc3N3b3JkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPHsgdHlwZTogc3RyaW5nLCB0aWNrZXQ6IGFueSB9PiB7XHJcbiAgICAgICAgaWYgKHVzZXJuYW1lID09PSAnZmFrZS11c2VybmFtZScgJiYgcGFzc3dvcmQgPT09ICdmYWtlLXBhc3N3b3JkJykge1xyXG4gICAgICAgICAgICByZXR1cm4gb2YoeyB0eXBlOiAndHlwZScsIHRpY2tldDogJ3RpY2tldCd9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh1c2VybmFtZSA9PT0gJ2Zha2UtdXNlcm5hbWUtQ09SUy1lcnJvcicgJiYgcGFzc3dvcmQgPT09ICdmYWtlLXBhc3N3b3JkJykge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhyb3dFcnJvcih7XHJcbiAgICAgICAgICAgICAgICBlcnJvcjoge1xyXG4gICAgICAgICAgICAgICAgICAgIGNyb3NzRG9tYWluOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6ICdFUlJPUjogdGhlIG5ldHdvcmsgaXMgb2ZmbGluZSwgT3JpZ2luIGlzIG5vdCBhbGxvd2VkIGJ5IEFjY2Vzcy1Db250cm9sLUFsbG93LU9yaWdpbidcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodXNlcm5hbWUgPT09ICdmYWtlLXVzZXJuYW1lLUNTUkYtZXJyb3InICYmIHBhc3N3b3JkID09PSAnZmFrZS1wYXNzd29yZCcpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRocm93RXJyb3Ioe21lc3NhZ2U6ICdFUlJPUjogSW52YWxpZCBDU1JGLXRva2VuJywgc3RhdHVzOiA0MDN9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh1c2VybmFtZSA9PT0gJ2Zha2UtdXNlcm5hbWUtRUNNLWFjY2Vzcy1lcnJvcicgJiYgcGFzc3dvcmQgPT09ICdmYWtlLXBhc3N3b3JkJykge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhyb3dFcnJvcih7bWVzc2FnZTogJ0VSUk9SOiAwMDE3MDcyOCBBY2Nlc3MgRGVuaWVkLiAgVGhlIHN5c3RlbSBpcyBjdXJyZW50bHkgaW4gcmVhZC1vbmx5IG1vZGUnLCBzdGF0dXM6IDQwM30pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoJ0Zha2Ugc2VydmVyIGVycm9yJyk7XHJcbiAgICB9XHJcbn1cclxuIl19