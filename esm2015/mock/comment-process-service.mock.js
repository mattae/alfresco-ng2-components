/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommentModel } from '../models/comment.model';
import { UserProcessModel } from '../models/user-process.model';
/** @type {?} */
export let fakeUser1 = { id: 1, email: 'fake-email@dom.com', firstName: 'firstName', lastName: 'lastName' };
/** @type {?} */
export let fakeUser2 = { id: 1001, email: 'some-one@somegroup.com', firstName: 'some', lastName: 'one' };
const ɵ0 = [
    {
        id: 1, message: 'fake-message-1', created: '', createdBy: fakeUser1
    },
    {
        id: 2, message: 'fake-message-2', created: '', createdBy: fakeUser1
    }
];
/** @type {?} */
export let fakeTasksComment = {
    size: 2, total: 2, start: 0,
    data: ɵ0
};
/** @type {?} */
export let fakeProcessComment = new CommentModel({ id: 1, message: 'Test', created: new Date('2016-11-10T03:37:30.010+0000'), createdBy: new UserProcessModel({
        id: 13,
        firstName: 'Wilbur',
        lastName: 'Adams',
        email: 'wilbur@app.com'
    }) });
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudC1wcm9jZXNzLXNlcnZpY2UubW9jay5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vY2svY29tbWVudC1wcm9jZXNzLXNlcnZpY2UubW9jay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDdkQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sOEJBQThCLENBQUM7O0FBRWhFLE1BQU0sS0FBSyxTQUFTLEdBQUcsRUFBRSxFQUFFLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxvQkFBb0IsRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUU7O0FBRTNHLE1BQU0sS0FBSyxTQUFTLEdBQUcsRUFBRSxFQUFFLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSx3QkFBd0IsRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUU7V0FJOUY7SUFDRjtRQUNJLEVBQUUsRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLFNBQVM7S0FDdEU7SUFDRDtRQUNJLEVBQUUsRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE9BQU8sRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLFNBQVM7S0FDdEU7Q0FDSjs7QUFUTCxNQUFNLEtBQUssZ0JBQWdCLEdBQUc7SUFDMUIsSUFBSSxFQUFFLENBQUMsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLEtBQUssRUFBRSxDQUFDO0lBQzNCLElBQUksSUFPSDtDQUNKOztBQUVELE1BQU0sS0FBSyxrQkFBa0IsR0FBRyxJQUFJLFlBQVksQ0FBQyxFQUFDLEVBQUUsRUFBRSxDQUFDLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMsOEJBQThCLENBQUMsRUFBRSxTQUFTLEVBQUUsSUFBSSxnQkFBZ0IsQ0FBQztRQUN6SixFQUFFLEVBQUUsRUFBRTtRQUNOLFNBQVMsRUFBRSxRQUFRO1FBQ25CLFFBQVEsRUFBRSxPQUFPO1FBQ2pCLEtBQUssRUFBRSxnQkFBZ0I7S0FDMUIsQ0FBQyxFQUFDLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ29tbWVudE1vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL2NvbW1lbnQubW9kZWwnO1xyXG5pbXBvcnQgeyBVc2VyUHJvY2Vzc01vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL3VzZXItcHJvY2Vzcy5tb2RlbCc7XHJcblxyXG5leHBvcnQgbGV0IGZha2VVc2VyMSA9IHsgaWQ6IDEsIGVtYWlsOiAnZmFrZS1lbWFpbEBkb20uY29tJywgZmlyc3ROYW1lOiAnZmlyc3ROYW1lJywgbGFzdE5hbWU6ICdsYXN0TmFtZScgfTtcclxuXHJcbmV4cG9ydCBsZXQgZmFrZVVzZXIyID0geyBpZDogMTAwMSwgZW1haWw6ICdzb21lLW9uZUBzb21lZ3JvdXAuY29tJywgZmlyc3ROYW1lOiAnc29tZScsIGxhc3ROYW1lOiAnb25lJyB9O1xyXG5cclxuZXhwb3J0IGxldCBmYWtlVGFza3NDb21tZW50ID0ge1xyXG4gICAgc2l6ZTogMiwgdG90YWw6IDIsIHN0YXJ0OiAwLFxyXG4gICAgZGF0YTogW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgaWQ6IDEsIG1lc3NhZ2U6ICdmYWtlLW1lc3NhZ2UtMScsIGNyZWF0ZWQ6ICcnLCBjcmVhdGVkQnk6IGZha2VVc2VyMVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBpZDogMiwgbWVzc2FnZTogJ2Zha2UtbWVzc2FnZS0yJywgY3JlYXRlZDogJycsIGNyZWF0ZWRCeTogZmFrZVVzZXIxXHJcbiAgICAgICAgfVxyXG4gICAgXVxyXG59O1xyXG5cclxuZXhwb3J0IGxldCBmYWtlUHJvY2Vzc0NvbW1lbnQgPSBuZXcgQ29tbWVudE1vZGVsKHtpZDogMSwgbWVzc2FnZTogJ1Rlc3QnLCBjcmVhdGVkOiBuZXcgRGF0ZSgnMjAxNi0xMS0xMFQwMzozNzozMC4wMTArMDAwMCcpLCBjcmVhdGVkQnk6IG5ldyBVc2VyUHJvY2Vzc01vZGVsKHtcclxuICAgIGlkOiAxMyxcclxuICAgIGZpcnN0TmFtZTogJ1dpbGJ1cicsXHJcbiAgICBsYXN0TmFtZTogJ0FkYW1zJyxcclxuICAgIGVtYWlsOiAnd2lsYnVyQGFwcC5jb20nXHJcbn0pfSk7XHJcbiJdfQ==