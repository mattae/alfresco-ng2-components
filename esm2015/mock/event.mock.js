/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class EventMock {
    /**
     * @param {?} key
     * @return {?}
     */
    static keyDown(key) {
        /** @type {?} */
        const event = document.createEvent('Event');
        event.keyCode = key;
        event.initEvent('keydown');
        document.dispatchEvent(event);
    }
    /**
     * @param {?} key
     * @return {?}
     */
    static keyUp(key) {
        /** @type {?} */
        const event = document.createEvent('Event');
        event.keyCode = key;
        event.initEvent('keyup');
        document.dispatchEvent(event);
    }
    /**
     * @return {?}
     */
    static resizeMobileView() {
        // todo: no longer compiles with TS 2.0.2 as innerWidth/innerHeight are readonly fields
        /*
        window.innerWidth = 320;
        window.innerHeight = 568;
        */
        window.dispatchEvent(new Event('resize'));
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnQubW9jay5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vY2svZXZlbnQubW9jay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxNQUFNLE9BQU8sU0FBUzs7Ozs7SUFFbEIsTUFBTSxDQUFDLE9BQU8sQ0FBQyxHQUFROztjQUNiLEtBQUssR0FBUSxRQUFRLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQztRQUNoRCxLQUFLLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUNwQixLQUFLLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzNCLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbEMsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsS0FBSyxDQUFDLEdBQVE7O2NBQ1gsS0FBSyxHQUFRLFFBQVEsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDO1FBQ2hELEtBQUssQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDO1FBQ3BCLEtBQUssQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDekIsUUFBUSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNsQyxDQUFDOzs7O0lBRUQsTUFBTSxDQUFDLGdCQUFnQjtRQUNuQix1RkFBdUY7UUFDdkY7OztVQUdFO1FBQ0YsTUFBTSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQzlDLENBQUM7Q0FDSiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5leHBvcnQgY2xhc3MgRXZlbnRNb2NrIHtcclxuXHJcbiAgICBzdGF0aWMga2V5RG93bihrZXk6IGFueSkge1xyXG4gICAgICAgIGNvbnN0IGV2ZW50OiBhbnkgPSBkb2N1bWVudC5jcmVhdGVFdmVudCgnRXZlbnQnKTtcclxuICAgICAgICBldmVudC5rZXlDb2RlID0ga2V5O1xyXG4gICAgICAgIGV2ZW50LmluaXRFdmVudCgna2V5ZG93bicpO1xyXG4gICAgICAgIGRvY3VtZW50LmRpc3BhdGNoRXZlbnQoZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyBrZXlVcChrZXk6IGFueSkge1xyXG4gICAgICAgIGNvbnN0IGV2ZW50OiBhbnkgPSBkb2N1bWVudC5jcmVhdGVFdmVudCgnRXZlbnQnKTtcclxuICAgICAgICBldmVudC5rZXlDb2RlID0ga2V5O1xyXG4gICAgICAgIGV2ZW50LmluaXRFdmVudCgna2V5dXAnKTtcclxuICAgICAgICBkb2N1bWVudC5kaXNwYXRjaEV2ZW50KGV2ZW50KTtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgcmVzaXplTW9iaWxlVmlldygpIHtcclxuICAgICAgICAvLyB0b2RvOiBubyBsb25nZXIgY29tcGlsZXMgd2l0aCBUUyAyLjAuMiBhcyBpbm5lcldpZHRoL2lubmVySGVpZ2h0IGFyZSByZWFkb25seSBmaWVsZHNcclxuICAgICAgICAvKlxyXG4gICAgICAgIHdpbmRvdy5pbm5lcldpZHRoID0gMzIwO1xyXG4gICAgICAgIHdpbmRvdy5pbm5lckhlaWdodCA9IDU2ODtcclxuICAgICAgICAqL1xyXG4gICAgICAgIHdpbmRvdy5kaXNwYXRjaEV2ZW50KG5ldyBFdmVudCgncmVzaXplJykpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==