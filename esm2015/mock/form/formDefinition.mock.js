/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/** @type {?} */
export let formDefinitionTwoTextFields = {
    id: 20,
    name: 'formTextDefinition',
    processDefinitionId: 'textDefinition:1:153',
    processDefinitionName: 'textDefinition',
    processDefinitionKey: 'textDefinition',
    taskId: '159',
    taskDefinitionKey: 'sid-D941F49F-2B04-4FBB-9B49-9E95991993E8',
    tabs: [],
    fields: [
        {
            fieldType: 'ContainerRepresentation',
            id: '1507044399260',
            name: 'Label',
            type: 'container',
            value: null,
            required: false,
            readOnly: false,
            overrideId: false,
            colspan: 1,
            placeholder: null,
            minLength: 0,
            maxLength: 0,
            minValue: null,
            maxValue: null,
            regexPattern: null,
            optionType: null,
            hasEmptyValue: null,
            options: null,
            restUrl: null,
            restResponsePath: null,
            restIdProperty: null,
            restLabelProperty: null,
            tab: null,
            className: null,
            dateDisplayFormat: null,
            layout: null,
            sizeX: 2,
            sizeY: 1,
            row: -1,
            col: -1,
            visibilityCondition: null,
            numberOfColumns: 2,
            fields: {
                '1': [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'firstname',
                        name: 'firstName',
                        type: 'text',
                        value: null,
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 2
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ],
                '2': [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'lastname',
                        name: 'lastName',
                        type: 'text',
                        value: null,
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 1
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ]
            }
        }
    ],
    outcomes: [],
    javascriptEvents: [],
    className: '',
    style: '',
    customFieldTemplates: {},
    metadata: {},
    variables: [],
    customFieldsValueInfo: {},
    gridsterForm: false,
    globalDateFormat: 'D-M-YYYY'
};
/** @type {?} */
export let formDefinitionDropdownField = {
    id: 21,
    name: 'dropdownDefinition',
    processDefinitionId: 'textDefinition:2:163',
    processDefinitionName: 'textDefinition',
    processDefinitionKey: 'textDefinition',
    taskId: '169',
    taskDefinitionKey: 'sid-D941F49F-2B04-4FBB-9B49-9E95991993E8',
    tabs: [],
    fields: [
        {
            fieldType: 'ContainerRepresentation',
            id: '1507046026940',
            name: 'Label',
            type: 'container',
            value: null,
            required: false,
            readOnly: false,
            overrideId: false,
            colspan: 1,
            placeholder: null,
            minLength: 0,
            maxLength: 0,
            minValue: null,
            maxValue: null,
            regexPattern: null,
            optionType: null,
            hasEmptyValue: null,
            options: null,
            restUrl: null,
            restResponsePath: null,
            restIdProperty: null,
            restLabelProperty: null,
            tab: null,
            className: null,
            dateDisplayFormat: null,
            layout: null,
            sizeX: 2,
            sizeY: 1,
            row: -1,
            col: -1,
            visibilityCondition: null,
            numberOfColumns: 2,
            fields: {
                '1': [
                    {
                        fieldType: 'RestFieldRepresentation',
                        id: 'country',
                        name: 'country',
                        type: 'dropdown',
                        value: 'Choose one...',
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: true,
                        options: [
                            {
                                id: 'empty',
                                name: 'Choose one...'
                            },
                            {
                                id: 'option_1',
                                name: 'united kingdom'
                            },
                            {
                                id: 'option_2',
                                name: 'italy'
                            },
                            {
                                id: 'option_3',
                                name: 'france'
                            }
                        ],
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 2
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null,
                        endpoint: null,
                        requestHeaders: null
                    }
                ],
                '2': []
            }
        }
    ],
    outcomes: [],
    javascriptEvents: [],
    className: '',
    style: '',
    customFieldTemplates: {},
    metadata: {},
    variables: [],
    customFieldsValueInfo: {},
    gridsterForm: false,
    globalDateFormat: 'D-M-YYYY'
};
/** @type {?} */
export let formDefinitionRequiredField = {
    id: 21,
    name: 'dropdownDefinition',
    processDefinitionId: 'textDefinition:2:163',
    processDefinitionName: 'textDefinition',
    processDefinitionKey: 'textDefinition',
    taskId: '169',
    taskDefinitionKey: 'sid-D941F49F-2B04-4FBB-9B49-9E95991993E8',
    tabs: [],
    fields: [
        {
            fieldType: 'ContainerRepresentation',
            id: '1507046026940',
            name: 'Label',
            type: 'container',
            value: null,
            required: false,
            readOnly: false,
            overrideId: false,
            colspan: 1,
            placeholder: null,
            minLength: 0,
            maxLength: 0,
            minValue: null,
            maxValue: null,
            regexPattern: null,
            optionType: null,
            hasEmptyValue: null,
            options: null,
            restUrl: null,
            restResponsePath: null,
            restIdProperty: null,
            restLabelProperty: null,
            tab: null,
            className: null,
            dateDisplayFormat: null,
            layout: null,
            sizeX: 2,
            sizeY: 1,
            row: -1,
            col: -1,
            visibilityCondition: null,
            numberOfColumns: 2,
            fields: {
                '1': [
                    {
                        fieldType: 'RestFieldRepresentation',
                        id: 'country',
                        name: 'country',
                        type: 'dropdown',
                        value: 'Choose one...',
                        required: true,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: true,
                        options: [
                            {
                                id: 'empty',
                                name: 'Choose one...'
                            },
                            {
                                id: 'option_1',
                                name: 'united kingdom'
                            },
                            {
                                id: 'option_2',
                                name: 'italy'
                            },
                            {
                                id: 'option_3',
                                name: 'france'
                            }
                        ],
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 2
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null,
                        endpoint: null,
                        requestHeaders: null
                    }
                ],
                '2': []
            }
        }
    ],
    outcomes: [],
    javascriptEvents: [],
    className: '',
    style: '',
    customFieldTemplates: {},
    metadata: {},
    variables: [],
    customFieldsValueInfo: {},
    gridsterForm: false,
    globalDateFormat: 'D-M-YYYY'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybURlZmluaXRpb24ubW9jay5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vY2svZm9ybS9mb3JtRGVmaW5pdGlvbi5tb2NrLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxNQUFNLEtBQUssMkJBQTJCLEdBQUc7SUFDckMsRUFBRSxFQUFFLEVBQUU7SUFDTixJQUFJLEVBQUUsb0JBQW9CO0lBQzFCLG1CQUFtQixFQUFFLHNCQUFzQjtJQUMzQyxxQkFBcUIsRUFBRSxnQkFBZ0I7SUFDdkMsb0JBQW9CLEVBQUUsZ0JBQWdCO0lBQ3RDLE1BQU0sRUFBRSxLQUFLO0lBQ2IsaUJBQWlCLEVBQUUsMENBQTBDO0lBQzdELElBQUksRUFBRSxFQUFFO0lBQ1IsTUFBTSxFQUFFO1FBQ0o7WUFDSSxTQUFTLEVBQUUseUJBQXlCO1lBQ3BDLEVBQUUsRUFBRSxlQUFlO1lBQ25CLElBQUksRUFBRSxPQUFPO1lBQ2IsSUFBSSxFQUFFLFdBQVc7WUFDakIsS0FBSyxFQUFFLElBQUk7WUFDWCxRQUFRLEVBQUUsS0FBSztZQUNmLFFBQVEsRUFBRSxLQUFLO1lBQ2YsVUFBVSxFQUFFLEtBQUs7WUFDakIsT0FBTyxFQUFFLENBQUM7WUFDVixXQUFXLEVBQUUsSUFBSTtZQUNqQixTQUFTLEVBQUUsQ0FBQztZQUNaLFNBQVMsRUFBRSxDQUFDO1lBQ1osUUFBUSxFQUFFLElBQUk7WUFDZCxRQUFRLEVBQUUsSUFBSTtZQUNkLFlBQVksRUFBRSxJQUFJO1lBQ2xCLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLGFBQWEsRUFBRSxJQUFJO1lBQ25CLE9BQU8sRUFBRSxJQUFJO1lBQ2IsT0FBTyxFQUFFLElBQUk7WUFDYixnQkFBZ0IsRUFBRSxJQUFJO1lBQ3RCLGNBQWMsRUFBRSxJQUFJO1lBQ3BCLGlCQUFpQixFQUFFLElBQUk7WUFDdkIsR0FBRyxFQUFFLElBQUk7WUFDVCxTQUFTLEVBQUUsSUFBSTtZQUNmLGlCQUFpQixFQUFFLElBQUk7WUFDdkIsTUFBTSxFQUFFLElBQUk7WUFDWixLQUFLLEVBQUUsQ0FBQztZQUNSLEtBQUssRUFBRSxDQUFDO1lBQ1IsR0FBRyxFQUFFLENBQUMsQ0FBQztZQUNQLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDUCxtQkFBbUIsRUFBRSxJQUFJO1lBQ3pCLGVBQWUsRUFBRSxDQUFDO1lBQ2xCLE1BQU0sRUFBRTtnQkFDSixHQUFHLEVBQUU7b0JBQ0Q7d0JBQ0ksU0FBUyxFQUFFLHlCQUF5Qjt3QkFDcEMsRUFBRSxFQUFFLFdBQVc7d0JBQ2YsSUFBSSxFQUFFLFdBQVc7d0JBQ2pCLElBQUksRUFBRSxNQUFNO3dCQUNaLEtBQUssRUFBRSxJQUFJO3dCQUNYLFFBQVEsRUFBRSxLQUFLO3dCQUNmLFFBQVEsRUFBRSxLQUFLO3dCQUNmLFVBQVUsRUFBRSxLQUFLO3dCQUNqQixPQUFPLEVBQUUsQ0FBQzt3QkFDVixXQUFXLEVBQUUsSUFBSTt3QkFDakIsU0FBUyxFQUFFLENBQUM7d0JBQ1osU0FBUyxFQUFFLENBQUM7d0JBQ1osUUFBUSxFQUFFLElBQUk7d0JBQ2QsUUFBUSxFQUFFLElBQUk7d0JBQ2QsWUFBWSxFQUFFLElBQUk7d0JBQ2xCLFVBQVUsRUFBRSxJQUFJO3dCQUNoQixhQUFhLEVBQUUsSUFBSTt3QkFDbkIsT0FBTyxFQUFFLElBQUk7d0JBQ2IsT0FBTyxFQUFFLElBQUk7d0JBQ2IsZ0JBQWdCLEVBQUUsSUFBSTt3QkFDdEIsY0FBYyxFQUFFLElBQUk7d0JBQ3BCLGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLEdBQUcsRUFBRSxJQUFJO3dCQUNULFNBQVMsRUFBRSxJQUFJO3dCQUNmLE1BQU0sRUFBRTs0QkFDSixlQUFlLEVBQUUsQ0FBQzs0QkFDbEIsVUFBVSxFQUFFLENBQUM7eUJBQ2hCO3dCQUNELGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLE1BQU0sRUFBRTs0QkFDSixHQUFHLEVBQUUsQ0FBQyxDQUFDOzRCQUNQLE1BQU0sRUFBRSxDQUFDLENBQUM7NEJBQ1YsT0FBTyxFQUFFLENBQUM7eUJBQ2I7d0JBQ0QsS0FBSyxFQUFFLENBQUM7d0JBQ1IsS0FBSyxFQUFFLENBQUM7d0JBQ1IsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLG1CQUFtQixFQUFFLElBQUk7cUJBQzVCO2lCQUNKO2dCQUNELEdBQUcsRUFBRTtvQkFDRDt3QkFDSSxTQUFTLEVBQUUseUJBQXlCO3dCQUNwQyxFQUFFLEVBQUUsVUFBVTt3QkFDZCxJQUFJLEVBQUUsVUFBVTt3QkFDaEIsSUFBSSxFQUFFLE1BQU07d0JBQ1osS0FBSyxFQUFFLElBQUk7d0JBQ1gsUUFBUSxFQUFFLEtBQUs7d0JBQ2YsUUFBUSxFQUFFLEtBQUs7d0JBQ2YsVUFBVSxFQUFFLEtBQUs7d0JBQ2pCLE9BQU8sRUFBRSxDQUFDO3dCQUNWLFdBQVcsRUFBRSxJQUFJO3dCQUNqQixTQUFTLEVBQUUsQ0FBQzt3QkFDWixTQUFTLEVBQUUsQ0FBQzt3QkFDWixRQUFRLEVBQUUsSUFBSTt3QkFDZCxRQUFRLEVBQUUsSUFBSTt3QkFDZCxZQUFZLEVBQUUsSUFBSTt3QkFDbEIsVUFBVSxFQUFFLElBQUk7d0JBQ2hCLGFBQWEsRUFBRSxJQUFJO3dCQUNuQixPQUFPLEVBQUUsSUFBSTt3QkFDYixPQUFPLEVBQUUsSUFBSTt3QkFDYixnQkFBZ0IsRUFBRSxJQUFJO3dCQUN0QixjQUFjLEVBQUUsSUFBSTt3QkFDcEIsaUJBQWlCLEVBQUUsSUFBSTt3QkFDdkIsR0FBRyxFQUFFLElBQUk7d0JBQ1QsU0FBUyxFQUFFLElBQUk7d0JBQ2YsTUFBTSxFQUFFOzRCQUNKLGVBQWUsRUFBRSxDQUFDOzRCQUNsQixVQUFVLEVBQUUsQ0FBQzt5QkFDaEI7d0JBQ0QsaUJBQWlCLEVBQUUsSUFBSTt3QkFDdkIsTUFBTSxFQUFFOzRCQUNKLEdBQUcsRUFBRSxDQUFDLENBQUM7NEJBQ1AsTUFBTSxFQUFFLENBQUMsQ0FBQzs0QkFDVixPQUFPLEVBQUUsQ0FBQzt5QkFDYjt3QkFDRCxLQUFLLEVBQUUsQ0FBQzt3QkFDUixLQUFLLEVBQUUsQ0FBQzt3QkFDUixHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsbUJBQW1CLEVBQUUsSUFBSTtxQkFDNUI7aUJBQ0o7YUFDSjtTQUNKO0tBQ0o7SUFDRCxRQUFRLEVBQUUsRUFBRTtJQUNaLGdCQUFnQixFQUFFLEVBQUU7SUFDcEIsU0FBUyxFQUFFLEVBQUU7SUFDYixLQUFLLEVBQUUsRUFBRTtJQUNULG9CQUFvQixFQUFFLEVBQUU7SUFDeEIsUUFBUSxFQUFFLEVBQUU7SUFDWixTQUFTLEVBQUUsRUFBRTtJQUNiLHFCQUFxQixFQUFFLEVBQUU7SUFDekIsWUFBWSxFQUFFLEtBQUs7SUFDbkIsZ0JBQWdCLEVBQUUsVUFBVTtDQUMvQjs7QUFFRCxNQUFNLEtBQUssMkJBQTJCLEdBQUc7SUFDckMsRUFBRSxFQUFFLEVBQUU7SUFDTixJQUFJLEVBQUUsb0JBQW9CO0lBQzFCLG1CQUFtQixFQUFFLHNCQUFzQjtJQUMzQyxxQkFBcUIsRUFBRSxnQkFBZ0I7SUFDdkMsb0JBQW9CLEVBQUUsZ0JBQWdCO0lBQ3RDLE1BQU0sRUFBRSxLQUFLO0lBQ2IsaUJBQWlCLEVBQUUsMENBQTBDO0lBQzdELElBQUksRUFBRSxFQUFFO0lBQ1IsTUFBTSxFQUFFO1FBQ0o7WUFDSSxTQUFTLEVBQUUseUJBQXlCO1lBQ3BDLEVBQUUsRUFBRSxlQUFlO1lBQ25CLElBQUksRUFBRSxPQUFPO1lBQ2IsSUFBSSxFQUFFLFdBQVc7WUFDakIsS0FBSyxFQUFFLElBQUk7WUFDWCxRQUFRLEVBQUUsS0FBSztZQUNmLFFBQVEsRUFBRSxLQUFLO1lBQ2YsVUFBVSxFQUFFLEtBQUs7WUFDakIsT0FBTyxFQUFFLENBQUM7WUFDVixXQUFXLEVBQUUsSUFBSTtZQUNqQixTQUFTLEVBQUUsQ0FBQztZQUNaLFNBQVMsRUFBRSxDQUFDO1lBQ1osUUFBUSxFQUFFLElBQUk7WUFDZCxRQUFRLEVBQUUsSUFBSTtZQUNkLFlBQVksRUFBRSxJQUFJO1lBQ2xCLFVBQVUsRUFBRSxJQUFJO1lBQ2hCLGFBQWEsRUFBRSxJQUFJO1lBQ25CLE9BQU8sRUFBRSxJQUFJO1lBQ2IsT0FBTyxFQUFFLElBQUk7WUFDYixnQkFBZ0IsRUFBRSxJQUFJO1lBQ3RCLGNBQWMsRUFBRSxJQUFJO1lBQ3BCLGlCQUFpQixFQUFFLElBQUk7WUFDdkIsR0FBRyxFQUFFLElBQUk7WUFDVCxTQUFTLEVBQUUsSUFBSTtZQUNmLGlCQUFpQixFQUFFLElBQUk7WUFDdkIsTUFBTSxFQUFFLElBQUk7WUFDWixLQUFLLEVBQUUsQ0FBQztZQUNSLEtBQUssRUFBRSxDQUFDO1lBQ1IsR0FBRyxFQUFFLENBQUMsQ0FBQztZQUNQLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDUCxtQkFBbUIsRUFBRSxJQUFJO1lBQ3pCLGVBQWUsRUFBRSxDQUFDO1lBQ2xCLE1BQU0sRUFBRTtnQkFDSixHQUFHLEVBQUU7b0JBQ0Q7d0JBQ0ksU0FBUyxFQUFFLHlCQUF5Qjt3QkFDcEMsRUFBRSxFQUFFLFNBQVM7d0JBQ2IsSUFBSSxFQUFFLFNBQVM7d0JBQ2YsSUFBSSxFQUFFLFVBQVU7d0JBQ2hCLEtBQUssRUFBRSxlQUFlO3dCQUN0QixRQUFRLEVBQUUsS0FBSzt3QkFDZixRQUFRLEVBQUUsS0FBSzt3QkFDZixVQUFVLEVBQUUsS0FBSzt3QkFDakIsT0FBTyxFQUFFLENBQUM7d0JBQ1YsV0FBVyxFQUFFLElBQUk7d0JBQ2pCLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFlBQVksRUFBRSxJQUFJO3dCQUNsQixVQUFVLEVBQUUsSUFBSTt3QkFDaEIsYUFBYSxFQUFFLElBQUk7d0JBQ25CLE9BQU8sRUFBRTs0QkFDTDtnQ0FDSSxFQUFFLEVBQUUsT0FBTztnQ0FDWCxJQUFJLEVBQUUsZUFBZTs2QkFDeEI7NEJBQ0Q7Z0NBQ0ksRUFBRSxFQUFFLFVBQVU7Z0NBQ2QsSUFBSSxFQUFFLGdCQUFnQjs2QkFDekI7NEJBQ0Q7Z0NBQ0ksRUFBRSxFQUFFLFVBQVU7Z0NBQ2QsSUFBSSxFQUFFLE9BQU87NkJBQ2hCOzRCQUNEO2dDQUNJLEVBQUUsRUFBRSxVQUFVO2dDQUNkLElBQUksRUFBRSxRQUFROzZCQUNqQjt5QkFDSjt3QkFDRCxPQUFPLEVBQUUsSUFBSTt3QkFDYixnQkFBZ0IsRUFBRSxJQUFJO3dCQUN0QixjQUFjLEVBQUUsSUFBSTt3QkFDcEIsaUJBQWlCLEVBQUUsSUFBSTt3QkFDdkIsR0FBRyxFQUFFLElBQUk7d0JBQ1QsU0FBUyxFQUFFLElBQUk7d0JBQ2YsTUFBTSxFQUFFOzRCQUNKLGVBQWUsRUFBRSxDQUFDOzRCQUNsQixVQUFVLEVBQUUsQ0FBQzt5QkFDaEI7d0JBQ0QsaUJBQWlCLEVBQUUsSUFBSTt3QkFDdkIsTUFBTSxFQUFFOzRCQUNKLEdBQUcsRUFBRSxDQUFDLENBQUM7NEJBQ1AsTUFBTSxFQUFFLENBQUMsQ0FBQzs0QkFDVixPQUFPLEVBQUUsQ0FBQzt5QkFDYjt3QkFDRCxLQUFLLEVBQUUsQ0FBQzt3QkFDUixLQUFLLEVBQUUsQ0FBQzt3QkFDUixHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsbUJBQW1CLEVBQUUsSUFBSTt3QkFDekIsUUFBUSxFQUFFLElBQUk7d0JBQ2QsY0FBYyxFQUFFLElBQUk7cUJBQ3ZCO2lCQUNKO2dCQUNELEdBQUcsRUFBRSxFQUFFO2FBQ1Y7U0FDSjtLQUNKO0lBQ0QsUUFBUSxFQUFFLEVBQUU7SUFDWixnQkFBZ0IsRUFBRSxFQUFFO0lBQ3BCLFNBQVMsRUFBRSxFQUFFO0lBQ2IsS0FBSyxFQUFFLEVBQUU7SUFDVCxvQkFBb0IsRUFBRSxFQUFFO0lBQ3hCLFFBQVEsRUFBRSxFQUFFO0lBQ1osU0FBUyxFQUFFLEVBQUU7SUFDYixxQkFBcUIsRUFBRSxFQUFFO0lBQ3pCLFlBQVksRUFBRSxLQUFLO0lBQ25CLGdCQUFnQixFQUFFLFVBQVU7Q0FDL0I7O0FBRUQsTUFBTSxLQUFLLDJCQUEyQixHQUFHO0lBQ3JDLEVBQUUsRUFBRSxFQUFFO0lBQ04sSUFBSSxFQUFFLG9CQUFvQjtJQUMxQixtQkFBbUIsRUFBRSxzQkFBc0I7SUFDM0MscUJBQXFCLEVBQUUsZ0JBQWdCO0lBQ3ZDLG9CQUFvQixFQUFFLGdCQUFnQjtJQUN0QyxNQUFNLEVBQUUsS0FBSztJQUNiLGlCQUFpQixFQUFFLDBDQUEwQztJQUM3RCxJQUFJLEVBQUUsRUFBRTtJQUNSLE1BQU0sRUFBRTtRQUNKO1lBQ0ksU0FBUyxFQUFFLHlCQUF5QjtZQUNwQyxFQUFFLEVBQUUsZUFBZTtZQUNuQixJQUFJLEVBQUUsT0FBTztZQUNiLElBQUksRUFBRSxXQUFXO1lBQ2pCLEtBQUssRUFBRSxJQUFJO1lBQ1gsUUFBUSxFQUFFLEtBQUs7WUFDZixRQUFRLEVBQUUsS0FBSztZQUNmLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLE9BQU8sRUFBRSxDQUFDO1lBQ1YsV0FBVyxFQUFFLElBQUk7WUFDakIsU0FBUyxFQUFFLENBQUM7WUFDWixTQUFTLEVBQUUsQ0FBQztZQUNaLFFBQVEsRUFBRSxJQUFJO1lBQ2QsUUFBUSxFQUFFLElBQUk7WUFDZCxZQUFZLEVBQUUsSUFBSTtZQUNsQixVQUFVLEVBQUUsSUFBSTtZQUNoQixhQUFhLEVBQUUsSUFBSTtZQUNuQixPQUFPLEVBQUUsSUFBSTtZQUNiLE9BQU8sRUFBRSxJQUFJO1lBQ2IsZ0JBQWdCLEVBQUUsSUFBSTtZQUN0QixjQUFjLEVBQUUsSUFBSTtZQUNwQixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLEdBQUcsRUFBRSxJQUFJO1lBQ1QsU0FBUyxFQUFFLElBQUk7WUFDZixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLE1BQU0sRUFBRSxJQUFJO1lBQ1osS0FBSyxFQUFFLENBQUM7WUFDUixLQUFLLEVBQUUsQ0FBQztZQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBQ1AsbUJBQW1CLEVBQUUsSUFBSTtZQUN6QixlQUFlLEVBQUUsQ0FBQztZQUNsQixNQUFNLEVBQUU7Z0JBQ0osR0FBRyxFQUFFO29CQUNEO3dCQUNJLFNBQVMsRUFBRSx5QkFBeUI7d0JBQ3BDLEVBQUUsRUFBRSxTQUFTO3dCQUNiLElBQUksRUFBRSxTQUFTO3dCQUNmLElBQUksRUFBRSxVQUFVO3dCQUNoQixLQUFLLEVBQUUsZUFBZTt3QkFDdEIsUUFBUSxFQUFFLElBQUk7d0JBQ2QsUUFBUSxFQUFFLEtBQUs7d0JBQ2YsVUFBVSxFQUFFLEtBQUs7d0JBQ2pCLE9BQU8sRUFBRSxDQUFDO3dCQUNWLFdBQVcsRUFBRSxJQUFJO3dCQUNqQixTQUFTLEVBQUUsQ0FBQzt3QkFDWixTQUFTLEVBQUUsQ0FBQzt3QkFDWixRQUFRLEVBQUUsSUFBSTt3QkFDZCxRQUFRLEVBQUUsSUFBSTt3QkFDZCxZQUFZLEVBQUUsSUFBSTt3QkFDbEIsVUFBVSxFQUFFLElBQUk7d0JBQ2hCLGFBQWEsRUFBRSxJQUFJO3dCQUNuQixPQUFPLEVBQUU7NEJBQ0w7Z0NBQ0ksRUFBRSxFQUFFLE9BQU87Z0NBQ1gsSUFBSSxFQUFFLGVBQWU7NkJBQ3hCOzRCQUNEO2dDQUNJLEVBQUUsRUFBRSxVQUFVO2dDQUNkLElBQUksRUFBRSxnQkFBZ0I7NkJBQ3pCOzRCQUNEO2dDQUNJLEVBQUUsRUFBRSxVQUFVO2dDQUNkLElBQUksRUFBRSxPQUFPOzZCQUNoQjs0QkFDRDtnQ0FDSSxFQUFFLEVBQUUsVUFBVTtnQ0FDZCxJQUFJLEVBQUUsUUFBUTs2QkFDakI7eUJBQ0o7d0JBQ0QsT0FBTyxFQUFFLElBQUk7d0JBQ2IsZ0JBQWdCLEVBQUUsSUFBSTt3QkFDdEIsY0FBYyxFQUFFLElBQUk7d0JBQ3BCLGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLEdBQUcsRUFBRSxJQUFJO3dCQUNULFNBQVMsRUFBRSxJQUFJO3dCQUNmLE1BQU0sRUFBRTs0QkFDSixlQUFlLEVBQUUsQ0FBQzs0QkFDbEIsVUFBVSxFQUFFLENBQUM7eUJBQ2hCO3dCQUNELGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLE1BQU0sRUFBRTs0QkFDSixHQUFHLEVBQUUsQ0FBQyxDQUFDOzRCQUNQLE1BQU0sRUFBRSxDQUFDLENBQUM7NEJBQ1YsT0FBTyxFQUFFLENBQUM7eUJBQ2I7d0JBQ0QsS0FBSyxFQUFFLENBQUM7d0JBQ1IsS0FBSyxFQUFFLENBQUM7d0JBQ1IsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLG1CQUFtQixFQUFFLElBQUk7d0JBQ3pCLFFBQVEsRUFBRSxJQUFJO3dCQUNkLGNBQWMsRUFBRSxJQUFJO3FCQUN2QjtpQkFDSjtnQkFDRCxHQUFHLEVBQUUsRUFBRTthQUNWO1NBQ0o7S0FDSjtJQUNELFFBQVEsRUFBRSxFQUFFO0lBQ1osZ0JBQWdCLEVBQUUsRUFBRTtJQUNwQixTQUFTLEVBQUUsRUFBRTtJQUNiLEtBQUssRUFBRSxFQUFFO0lBQ1Qsb0JBQW9CLEVBQUUsRUFBRTtJQUN4QixRQUFRLEVBQUUsRUFBRTtJQUNaLFNBQVMsRUFBRSxFQUFFO0lBQ2IscUJBQXFCLEVBQUUsRUFBRTtJQUN6QixZQUFZLEVBQUUsS0FBSztJQUNuQixnQkFBZ0IsRUFBRSxVQUFVO0NBQy9CIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmV4cG9ydCBsZXQgZm9ybURlZmluaXRpb25Ud29UZXh0RmllbGRzID0ge1xyXG4gICAgaWQ6IDIwLFxyXG4gICAgbmFtZTogJ2Zvcm1UZXh0RGVmaW5pdGlvbicsXHJcbiAgICBwcm9jZXNzRGVmaW5pdGlvbklkOiAndGV4dERlZmluaXRpb246MToxNTMnLFxyXG4gICAgcHJvY2Vzc0RlZmluaXRpb25OYW1lOiAndGV4dERlZmluaXRpb24nLFxyXG4gICAgcHJvY2Vzc0RlZmluaXRpb25LZXk6ICd0ZXh0RGVmaW5pdGlvbicsXHJcbiAgICB0YXNrSWQ6ICcxNTknLFxyXG4gICAgdGFza0RlZmluaXRpb25LZXk6ICdzaWQtRDk0MUY0OUYtMkIwNC00RkJCLTlCNDktOUU5NTk5MTk5M0U4JyxcclxuICAgIHRhYnM6IFtdLFxyXG4gICAgZmllbGRzOiBbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBmaWVsZFR5cGU6ICdDb250YWluZXJSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgIGlkOiAnMTUwNzA0NDM5OTI2MCcsXHJcbiAgICAgICAgICAgIG5hbWU6ICdMYWJlbCcsXHJcbiAgICAgICAgICAgIHR5cGU6ICdjb250YWluZXInLFxyXG4gICAgICAgICAgICB2YWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgICAgIG92ZXJyaWRlSWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBjb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICBwbGFjZWhvbGRlcjogbnVsbCxcclxuICAgICAgICAgICAgbWluTGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICBtYXhMZW5ndGg6IDAsXHJcbiAgICAgICAgICAgIG1pblZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICBtYXhWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgcmVnZXhQYXR0ZXJuOiBudWxsLFxyXG4gICAgICAgICAgICBvcHRpb25UeXBlOiBudWxsLFxyXG4gICAgICAgICAgICBoYXNFbXB0eVZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICBvcHRpb25zOiBudWxsLFxyXG4gICAgICAgICAgICByZXN0VXJsOiBudWxsLFxyXG4gICAgICAgICAgICByZXN0UmVzcG9uc2VQYXRoOiBudWxsLFxyXG4gICAgICAgICAgICByZXN0SWRQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgcmVzdExhYmVsUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgIHRhYjogbnVsbCxcclxuICAgICAgICAgICAgY2xhc3NOYW1lOiBudWxsLFxyXG4gICAgICAgICAgICBkYXRlRGlzcGxheUZvcm1hdDogbnVsbCxcclxuICAgICAgICAgICAgbGF5b3V0OiBudWxsLFxyXG4gICAgICAgICAgICBzaXplWDogMixcclxuICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGwsXHJcbiAgICAgICAgICAgIG51bWJlck9mQ29sdW1uczogMixcclxuICAgICAgICAgICAgZmllbGRzOiB7XHJcbiAgICAgICAgICAgICAgICAnMSc6IFtcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkVHlwZTogJ0Zvcm1GaWVsZFJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdmaXJzdG5hbWUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnZmlyc3ROYW1lJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3RleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG92ZXJyaWRlSWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWluTGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXhMZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pblZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXhWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVnZXhQYXR0ZXJuOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25UeXBlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBoYXNFbXB0eVZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0VXJsOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0UmVzcG9uc2VQYXRoOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0SWRQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdExhYmVsUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhYjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4aXN0aW5nQ29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heENvbHNwYW46IDJcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0ZURpc3BsYXlGb3JtYXQ6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxheW91dDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbHVtbjogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xzcGFuOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpemVYOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWTogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sOiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmlzaWJpbGl0eUNvbmRpdGlvbjogbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICAnMic6IFtcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkVHlwZTogJ0Zvcm1GaWVsZFJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdsYXN0bmFtZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdsYXN0TmFtZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICd0ZXh0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvdmVycmlkZUlkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbkxlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4TGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5WYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlZ2V4UGF0dGVybjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uVHlwZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGFzRW1wdHlWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uczogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGlzdGluZ0NvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhDb2xzcGFuOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYXlvdXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2x1bW46IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGxcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICBdLFxyXG4gICAgb3V0Y29tZXM6IFtdLFxyXG4gICAgamF2YXNjcmlwdEV2ZW50czogW10sXHJcbiAgICBjbGFzc05hbWU6ICcnLFxyXG4gICAgc3R5bGU6ICcnLFxyXG4gICAgY3VzdG9tRmllbGRUZW1wbGF0ZXM6IHt9LFxyXG4gICAgbWV0YWRhdGE6IHt9LFxyXG4gICAgdmFyaWFibGVzOiBbXSxcclxuICAgIGN1c3RvbUZpZWxkc1ZhbHVlSW5mbzoge30sXHJcbiAgICBncmlkc3RlckZvcm06IGZhbHNlLFxyXG4gICAgZ2xvYmFsRGF0ZUZvcm1hdDogJ0QtTS1ZWVlZJ1xyXG59O1xyXG5cclxuZXhwb3J0IGxldCBmb3JtRGVmaW5pdGlvbkRyb3Bkb3duRmllbGQgPSB7XHJcbiAgICBpZDogMjEsXHJcbiAgICBuYW1lOiAnZHJvcGRvd25EZWZpbml0aW9uJyxcclxuICAgIHByb2Nlc3NEZWZpbml0aW9uSWQ6ICd0ZXh0RGVmaW5pdGlvbjoyOjE2MycsXHJcbiAgICBwcm9jZXNzRGVmaW5pdGlvbk5hbWU6ICd0ZXh0RGVmaW5pdGlvbicsXHJcbiAgICBwcm9jZXNzRGVmaW5pdGlvbktleTogJ3RleHREZWZpbml0aW9uJyxcclxuICAgIHRhc2tJZDogJzE2OScsXHJcbiAgICB0YXNrRGVmaW5pdGlvbktleTogJ3NpZC1EOTQxRjQ5Ri0yQjA0LTRGQkItOUI0OS05RTk1OTkxOTkzRTgnLFxyXG4gICAgdGFiczogW10sXHJcbiAgICBmaWVsZHM6IFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGZpZWxkVHlwZTogJ0NvbnRhaW5lclJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgaWQ6ICcxNTA3MDQ2MDI2OTQwJyxcclxuICAgICAgICAgICAgbmFtZTogJ0xhYmVsJyxcclxuICAgICAgICAgICAgdHlwZTogJ2NvbnRhaW5lcicsXHJcbiAgICAgICAgICAgIHZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBudWxsLFxyXG4gICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIG9wdGlvbnM6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RVcmw6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RSZXNwb25zZVBhdGg6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RJZFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICByZXN0TGFiZWxQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgdGFiOiBudWxsLFxyXG4gICAgICAgICAgICBjbGFzc05hbWU6IG51bGwsXHJcbiAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICBsYXlvdXQ6IG51bGwsXHJcbiAgICAgICAgICAgIHNpemVYOiAyLFxyXG4gICAgICAgICAgICBzaXplWTogMSxcclxuICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgY29sOiAtMSxcclxuICAgICAgICAgICAgdmlzaWJpbGl0eUNvbmRpdGlvbjogbnVsbCxcclxuICAgICAgICAgICAgbnVtYmVyT2ZDb2x1bW5zOiAyLFxyXG4gICAgICAgICAgICBmaWVsZHM6IHtcclxuICAgICAgICAgICAgICAgICcxJzogW1xyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRUeXBlOiAnUmVzdEZpZWxkUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogJ2NvdW50cnknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnY291bnRyeScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdkcm9wZG93bicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiAnQ2hvb3NlIG9uZS4uLicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvdmVycmlkZUlkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbkxlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4TGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5WYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlZ2V4UGF0dGVybjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uVHlwZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGFzRW1wdHlWYWx1ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uczogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnZW1wdHknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdDaG9vc2Ugb25lLi4uJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogJ29wdGlvbl8xJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAndW5pdGVkIGtpbmdkb20nXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnb3B0aW9uXzInLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdpdGFseSdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdvcHRpb25fMycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ2ZyYW5jZSdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGlzdGluZ0NvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhDb2xzcGFuOiAyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYXlvdXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2x1bW46IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVuZHBvaW50OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1ZXN0SGVhZGVyczogbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICAnMic6IFtdXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICBdLFxyXG4gICAgb3V0Y29tZXM6IFtdLFxyXG4gICAgamF2YXNjcmlwdEV2ZW50czogW10sXHJcbiAgICBjbGFzc05hbWU6ICcnLFxyXG4gICAgc3R5bGU6ICcnLFxyXG4gICAgY3VzdG9tRmllbGRUZW1wbGF0ZXM6IHt9LFxyXG4gICAgbWV0YWRhdGE6IHt9LFxyXG4gICAgdmFyaWFibGVzOiBbXSxcclxuICAgIGN1c3RvbUZpZWxkc1ZhbHVlSW5mbzoge30sXHJcbiAgICBncmlkc3RlckZvcm06IGZhbHNlLFxyXG4gICAgZ2xvYmFsRGF0ZUZvcm1hdDogJ0QtTS1ZWVlZJ1xyXG59O1xyXG5cclxuZXhwb3J0IGxldCBmb3JtRGVmaW5pdGlvblJlcXVpcmVkRmllbGQgPSB7XHJcbiAgICBpZDogMjEsXHJcbiAgICBuYW1lOiAnZHJvcGRvd25EZWZpbml0aW9uJyxcclxuICAgIHByb2Nlc3NEZWZpbml0aW9uSWQ6ICd0ZXh0RGVmaW5pdGlvbjoyOjE2MycsXHJcbiAgICBwcm9jZXNzRGVmaW5pdGlvbk5hbWU6ICd0ZXh0RGVmaW5pdGlvbicsXHJcbiAgICBwcm9jZXNzRGVmaW5pdGlvbktleTogJ3RleHREZWZpbml0aW9uJyxcclxuICAgIHRhc2tJZDogJzE2OScsXHJcbiAgICB0YXNrRGVmaW5pdGlvbktleTogJ3NpZC1EOTQxRjQ5Ri0yQjA0LTRGQkItOUI0OS05RTk1OTkxOTkzRTgnLFxyXG4gICAgdGFiczogW10sXHJcbiAgICBmaWVsZHM6IFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGZpZWxkVHlwZTogJ0NvbnRhaW5lclJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgaWQ6ICcxNTA3MDQ2MDI2OTQwJyxcclxuICAgICAgICAgICAgbmFtZTogJ0xhYmVsJyxcclxuICAgICAgICAgICAgdHlwZTogJ2NvbnRhaW5lcicsXHJcbiAgICAgICAgICAgIHZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBudWxsLFxyXG4gICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIG9wdGlvbnM6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RVcmw6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RSZXNwb25zZVBhdGg6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RJZFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICByZXN0TGFiZWxQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgdGFiOiBudWxsLFxyXG4gICAgICAgICAgICBjbGFzc05hbWU6IG51bGwsXHJcbiAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICBsYXlvdXQ6IG51bGwsXHJcbiAgICAgICAgICAgIHNpemVYOiAyLFxyXG4gICAgICAgICAgICBzaXplWTogMSxcclxuICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgY29sOiAtMSxcclxuICAgICAgICAgICAgdmlzaWJpbGl0eUNvbmRpdGlvbjogbnVsbCxcclxuICAgICAgICAgICAgbnVtYmVyT2ZDb2x1bW5zOiAyLFxyXG4gICAgICAgICAgICBmaWVsZHM6IHtcclxuICAgICAgICAgICAgICAgICcxJzogW1xyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRUeXBlOiAnUmVzdEZpZWxkUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogJ2NvdW50cnknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnY291bnRyeScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdkcm9wZG93bicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiAnQ2hvb3NlIG9uZS4uLicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG92ZXJyaWRlSWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWluTGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXhMZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pblZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXhWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVnZXhQYXR0ZXJuOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25UeXBlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBoYXNFbXB0eVZhbHVlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zOiBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdlbXB0eScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ0Nob29zZSBvbmUuLi4nXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnb3B0aW9uXzEnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICd1bml0ZWQga2luZ2RvbSdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdvcHRpb25fMicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ2l0YWx5J1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogJ29wdGlvbl8zJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnZnJhbmNlJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0VXJsOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0UmVzcG9uc2VQYXRoOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0SWRQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdExhYmVsUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhYjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4aXN0aW5nQ29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heENvbHNwYW46IDJcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0ZURpc3BsYXlGb3JtYXQ6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxheW91dDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbHVtbjogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xzcGFuOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpemVYOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWTogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sOiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmlzaWJpbGl0eUNvbmRpdGlvbjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZW5kcG9pbnQ6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVlc3RIZWFkZXJzOiBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgICcyJzogW11cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIF0sXHJcbiAgICBvdXRjb21lczogW10sXHJcbiAgICBqYXZhc2NyaXB0RXZlbnRzOiBbXSxcclxuICAgIGNsYXNzTmFtZTogJycsXHJcbiAgICBzdHlsZTogJycsXHJcbiAgICBjdXN0b21GaWVsZFRlbXBsYXRlczoge30sXHJcbiAgICBtZXRhZGF0YToge30sXHJcbiAgICB2YXJpYWJsZXM6IFtdLFxyXG4gICAgY3VzdG9tRmllbGRzVmFsdWVJbmZvOiB7fSxcclxuICAgIGdyaWRzdGVyRm9ybTogZmFsc2UsXHJcbiAgICBnbG9iYWxEYXRlRm9ybWF0OiAnRC1NLVlZWVknXHJcbn07XHJcbiJdfQ==