/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/** @type {?} */
export let fakeRendition = {
    entry: {
        id: 'pdf',
        content: {
            mimeType: 'application/pdf',
            mimeTypeName: 'Adobe PDF Document'
        },
        status: 'NOT_CREATED'
    }
};
/** @type {?} */
export let fakeRenditionCreated = {
    entry: {
        id: 'pdf',
        content: {
            mimeType: 'application/pdf',
            mimeTypeName: 'Adobe PDF Document'
        },
        status: 'CREATED'
    }
};
/** @type {?} */
export let fakeRenditionsList = {
    list: {
        pagination: {
            count: 6,
            hasMoreItems: false,
            totalItems: 6,
            skipCount: 0,
            maxItems: 100
        },
        entries: [
            {
                entry: {
                    id: 'avatar',
                    content: {
                        mimeType: 'image/png',
                        mimeTypeName: 'PNG Image'
                    },
                    status: 'NOT_CREATED'
                }
            },
            {
                entry: {
                    id: 'avatar32',
                    content: {
                        mimeType: 'image/png',
                        mimeTypeName: 'PNG Image'
                    },
                    status: 'NOT_CREATED'
                }
            },
            {
                entry: {
                    id: 'doclib',
                    content: {
                        mimeType: 'image/png',
                        mimeTypeName: 'PNG Image'
                    },
                    status: 'NOT_CREATED'
                }
            },
            {
                entry: {
                    id: 'imgpreview',
                    content: {
                        mimeType: 'image/jpeg',
                        mimeTypeName: 'JPEG Image'
                    },
                    status: 'NOT_CREATED'
                }
            },
            {
                entry: {
                    id: 'medium',
                    content: {
                        mimeType: 'image/jpeg',
                        mimeTypeName: 'JPEG Image'
                    },
                    status: 'NOT_CREATED'
                }
            },
            {
                entry: {
                    id: 'pdf',
                    content: {
                        mimeType: 'application/pdf',
                        mimeTypeName: 'Adobe PDF Document'
                    },
                    status: 'NOT_CREATED'
                }
            }
        ]
    }
};
/** @type {?} */
export let fakeRenditionsListWithACreated = {
    list: {
        pagination: {
            count: 6,
            hasMoreItems: false,
            totalItems: 6,
            skipCount: 0,
            maxItems: 100
        },
        entries: [
            {
                entry: {
                    id: 'avatar',
                    content: {
                        mimeType: 'image/png',
                        mimeTypeName: 'PNG Image'
                    },
                    status: 'NOT_CREATED'
                }
            },
            {
                entry: {
                    id: 'avatar32',
                    content: {
                        mimeType: 'image/png',
                        mimeTypeName: 'PNG Image'
                    },
                    status: 'NOT_CREATED'
                }
            },
            {
                entry: {
                    id: 'doclib',
                    content: {
                        mimeType: 'image/png',
                        mimeTypeName: 'PNG Image'
                    },
                    status: 'NOT_CREATED'
                }
            },
            {
                entry: {
                    id: 'imgpreview',
                    content: {
                        mimeType: 'image/jpeg',
                        mimeTypeName: 'JPEG Image'
                    },
                    status: 'NOT_CREATED'
                }
            },
            {
                entry: {
                    id: 'medium',
                    content: {
                        mimeType: 'image/jpeg',
                        mimeTypeName: 'JPEG Image'
                    },
                    status: 'NOT_CREATED'
                }
            },
            {
                entry: {
                    id: 'pdf',
                    content: {
                        mimeType: 'application/pdf',
                        mimeTypeName: 'Adobe PDF Document'
                    },
                    status: 'CREATED'
                }
            }
        ]
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVuZGl0aW9uc1NlcnZpY2UubW9jay5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vY2svcmVuZGl0aW9uc1NlcnZpY2UubW9jay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsTUFBTSxLQUFLLGFBQWEsR0FBRztJQUN2QixLQUFLLEVBQUU7UUFDSCxFQUFFLEVBQUUsS0FBSztRQUNULE9BQU8sRUFBRTtZQUNMLFFBQVEsRUFBRSxpQkFBaUI7WUFDM0IsWUFBWSxFQUFFLG9CQUFvQjtTQUNyQztRQUNELE1BQU0sRUFBRSxhQUFhO0tBQ3hCO0NBQ0o7O0FBRUQsTUFBTSxLQUFLLG9CQUFvQixHQUFHO0lBQzlCLEtBQUssRUFBRTtRQUNILEVBQUUsRUFBRSxLQUFLO1FBQ1QsT0FBTyxFQUFFO1lBQ0wsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixZQUFZLEVBQUUsb0JBQW9CO1NBQ3JDO1FBQ0QsTUFBTSxFQUFFLFNBQVM7S0FDcEI7Q0FDSjs7QUFFRCxNQUFNLEtBQUssa0JBQWtCLEdBQUc7SUFDNUIsSUFBSSxFQUFFO1FBQ0YsVUFBVSxFQUFFO1lBQ1IsS0FBSyxFQUFFLENBQUM7WUFDUixZQUFZLEVBQUUsS0FBSztZQUNuQixVQUFVLEVBQUUsQ0FBQztZQUNiLFNBQVMsRUFBRSxDQUFDO1lBQ1osUUFBUSxFQUFFLEdBQUc7U0FDaEI7UUFDRCxPQUFPLEVBQUU7WUFDTDtnQkFDSSxLQUFLLEVBQUU7b0JBQ0gsRUFBRSxFQUFFLFFBQVE7b0JBQ1osT0FBTyxFQUFFO3dCQUNMLFFBQVEsRUFBRSxXQUFXO3dCQUNyQixZQUFZLEVBQUUsV0FBVztxQkFDNUI7b0JBQ0QsTUFBTSxFQUFFLGFBQWE7aUJBQ3hCO2FBQ0o7WUFDRDtnQkFDSSxLQUFLLEVBQUU7b0JBQ0gsRUFBRSxFQUFFLFVBQVU7b0JBQ2QsT0FBTyxFQUFFO3dCQUNMLFFBQVEsRUFBRSxXQUFXO3dCQUNyQixZQUFZLEVBQUUsV0FBVztxQkFDNUI7b0JBQ0QsTUFBTSxFQUFFLGFBQWE7aUJBQ3hCO2FBQ0o7WUFDRDtnQkFDSSxLQUFLLEVBQUU7b0JBQ0gsRUFBRSxFQUFFLFFBQVE7b0JBQ1osT0FBTyxFQUFFO3dCQUNMLFFBQVEsRUFBRSxXQUFXO3dCQUNyQixZQUFZLEVBQUUsV0FBVztxQkFDNUI7b0JBQ0QsTUFBTSxFQUFFLGFBQWE7aUJBQ3hCO2FBQ0o7WUFDRDtnQkFDSSxLQUFLLEVBQUU7b0JBQ0gsRUFBRSxFQUFFLFlBQVk7b0JBQ2hCLE9BQU8sRUFBRTt3QkFDTCxRQUFRLEVBQUUsWUFBWTt3QkFDdEIsWUFBWSxFQUFFLFlBQVk7cUJBQzdCO29CQUNELE1BQU0sRUFBRSxhQUFhO2lCQUN4QjthQUNKO1lBQ0Q7Z0JBQ0ksS0FBSyxFQUFFO29CQUNILEVBQUUsRUFBRSxRQUFRO29CQUNaLE9BQU8sRUFBRTt3QkFDTCxRQUFRLEVBQUUsWUFBWTt3QkFDdEIsWUFBWSxFQUFFLFlBQVk7cUJBQzdCO29CQUNELE1BQU0sRUFBRSxhQUFhO2lCQUN4QjthQUNKO1lBQ0Q7Z0JBQ0ksS0FBSyxFQUFFO29CQUNILEVBQUUsRUFBRSxLQUFLO29CQUNULE9BQU8sRUFBRTt3QkFDTCxRQUFRLEVBQUUsaUJBQWlCO3dCQUMzQixZQUFZLEVBQUUsb0JBQW9CO3FCQUNyQztvQkFDRCxNQUFNLEVBQUUsYUFBYTtpQkFDeEI7YUFDSjtTQUNKO0tBQ0o7Q0FDSjs7QUFFRCxNQUFNLEtBQUssOEJBQThCLEdBQUc7SUFDeEMsSUFBSSxFQUFFO1FBQ0YsVUFBVSxFQUFFO1lBQ1IsS0FBSyxFQUFFLENBQUM7WUFDUixZQUFZLEVBQUUsS0FBSztZQUNuQixVQUFVLEVBQUUsQ0FBQztZQUNiLFNBQVMsRUFBRSxDQUFDO1lBQ1osUUFBUSxFQUFFLEdBQUc7U0FDaEI7UUFDRCxPQUFPLEVBQUU7WUFDTDtnQkFDSSxLQUFLLEVBQUU7b0JBQ0gsRUFBRSxFQUFFLFFBQVE7b0JBQ1osT0FBTyxFQUFFO3dCQUNMLFFBQVEsRUFBRSxXQUFXO3dCQUNyQixZQUFZLEVBQUUsV0FBVztxQkFDNUI7b0JBQ0QsTUFBTSxFQUFFLGFBQWE7aUJBQ3hCO2FBQ0o7WUFDRDtnQkFDSSxLQUFLLEVBQUU7b0JBQ0gsRUFBRSxFQUFFLFVBQVU7b0JBQ2QsT0FBTyxFQUFFO3dCQUNMLFFBQVEsRUFBRSxXQUFXO3dCQUNyQixZQUFZLEVBQUUsV0FBVztxQkFDNUI7b0JBQ0QsTUFBTSxFQUFFLGFBQWE7aUJBQ3hCO2FBQ0o7WUFDRDtnQkFDSSxLQUFLLEVBQUU7b0JBQ0gsRUFBRSxFQUFFLFFBQVE7b0JBQ1osT0FBTyxFQUFFO3dCQUNMLFFBQVEsRUFBRSxXQUFXO3dCQUNyQixZQUFZLEVBQUUsV0FBVztxQkFDNUI7b0JBQ0QsTUFBTSxFQUFFLGFBQWE7aUJBQ3hCO2FBQ0o7WUFDRDtnQkFDSSxLQUFLLEVBQUU7b0JBQ0gsRUFBRSxFQUFFLFlBQVk7b0JBQ2hCLE9BQU8sRUFBRTt3QkFDTCxRQUFRLEVBQUUsWUFBWTt3QkFDdEIsWUFBWSxFQUFFLFlBQVk7cUJBQzdCO29CQUNELE1BQU0sRUFBRSxhQUFhO2lCQUN4QjthQUNKO1lBQ0Q7Z0JBQ0ksS0FBSyxFQUFFO29CQUNILEVBQUUsRUFBRSxRQUFRO29CQUNaLE9BQU8sRUFBRTt3QkFDTCxRQUFRLEVBQUUsWUFBWTt3QkFDdEIsWUFBWSxFQUFFLFlBQVk7cUJBQzdCO29CQUNELE1BQU0sRUFBRSxhQUFhO2lCQUN4QjthQUNKO1lBQ0Q7Z0JBQ0ksS0FBSyxFQUFFO29CQUNILEVBQUUsRUFBRSxLQUFLO29CQUNULE9BQU8sRUFBRTt3QkFDTCxRQUFRLEVBQUUsaUJBQWlCO3dCQUMzQixZQUFZLEVBQUUsb0JBQW9CO3FCQUNyQztvQkFDRCxNQUFNLEVBQUUsU0FBUztpQkFDcEI7YUFDSjtTQUNKO0tBQ0o7Q0FDSiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5leHBvcnQgbGV0IGZha2VSZW5kaXRpb24gPSB7XHJcbiAgICBlbnRyeToge1xyXG4gICAgICAgIGlkOiAncGRmJyxcclxuICAgICAgICBjb250ZW50OiB7XHJcbiAgICAgICAgICAgIG1pbWVUeXBlOiAnYXBwbGljYXRpb24vcGRmJyxcclxuICAgICAgICAgICAgbWltZVR5cGVOYW1lOiAnQWRvYmUgUERGIERvY3VtZW50J1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgc3RhdHVzOiAnTk9UX0NSRUFURUQnXHJcbiAgICB9XHJcbn07XHJcblxyXG5leHBvcnQgbGV0IGZha2VSZW5kaXRpb25DcmVhdGVkID0ge1xyXG4gICAgZW50cnk6IHtcclxuICAgICAgICBpZDogJ3BkZicsXHJcbiAgICAgICAgY29udGVudDoge1xyXG4gICAgICAgICAgICBtaW1lVHlwZTogJ2FwcGxpY2F0aW9uL3BkZicsXHJcbiAgICAgICAgICAgIG1pbWVUeXBlTmFtZTogJ0Fkb2JlIFBERiBEb2N1bWVudCdcclxuICAgICAgICB9LFxyXG4gICAgICAgIHN0YXR1czogJ0NSRUFURUQnXHJcbiAgICB9XHJcbn07XHJcblxyXG5leHBvcnQgbGV0IGZha2VSZW5kaXRpb25zTGlzdCA9IHtcclxuICAgIGxpc3Q6IHtcclxuICAgICAgICBwYWdpbmF0aW9uOiB7XHJcbiAgICAgICAgICAgIGNvdW50OiA2LFxyXG4gICAgICAgICAgICBoYXNNb3JlSXRlbXM6IGZhbHNlLFxyXG4gICAgICAgICAgICB0b3RhbEl0ZW1zOiA2LFxyXG4gICAgICAgICAgICBza2lwQ291bnQ6IDAsXHJcbiAgICAgICAgICAgIG1heEl0ZW1zOiAxMDBcclxuICAgICAgICB9LFxyXG4gICAgICAgIGVudHJpZXM6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgZW50cnk6IHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogJ2F2YXRhcicsXHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW1lVHlwZTogJ2ltYWdlL3BuZycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbWVUeXBlTmFtZTogJ1BORyBJbWFnZSdcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1czogJ05PVF9DUkVBVEVEJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBlbnRyeToge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiAnYXZhdGFyMzInLFxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWltZVR5cGU6ICdpbWFnZS9wbmcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW1lVHlwZU5hbWU6ICdQTkcgSW1hZ2UnXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6ICdOT1RfQ1JFQVRFRCdcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgZW50cnk6IHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogJ2RvY2xpYicsXHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW1lVHlwZTogJ2ltYWdlL3BuZycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbWVUeXBlTmFtZTogJ1BORyBJbWFnZSdcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1czogJ05PVF9DUkVBVEVEJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBlbnRyeToge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiAnaW1ncHJldmlldycsXHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW1lVHlwZTogJ2ltYWdlL2pwZWcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW1lVHlwZU5hbWU6ICdKUEVHIEltYWdlJ1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiAnTk9UX0NSRUFURUQnXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGVudHJ5OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWQ6ICdtZWRpdW0nLFxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWltZVR5cGU6ICdpbWFnZS9qcGVnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWltZVR5cGVOYW1lOiAnSlBFRyBJbWFnZSdcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1czogJ05PVF9DUkVBVEVEJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBlbnRyeToge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiAncGRmJyxcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbWVUeXBlOiAnYXBwbGljYXRpb24vcGRmJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWltZVR5cGVOYW1lOiAnQWRvYmUgUERGIERvY3VtZW50J1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiAnTk9UX0NSRUFURUQnXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICBdXHJcbiAgICB9XHJcbn07XHJcblxyXG5leHBvcnQgbGV0IGZha2VSZW5kaXRpb25zTGlzdFdpdGhBQ3JlYXRlZCA9IHtcclxuICAgIGxpc3Q6IHtcclxuICAgICAgICBwYWdpbmF0aW9uOiB7XHJcbiAgICAgICAgICAgIGNvdW50OiA2LFxyXG4gICAgICAgICAgICBoYXNNb3JlSXRlbXM6IGZhbHNlLFxyXG4gICAgICAgICAgICB0b3RhbEl0ZW1zOiA2LFxyXG4gICAgICAgICAgICBza2lwQ291bnQ6IDAsXHJcbiAgICAgICAgICAgIG1heEl0ZW1zOiAxMDBcclxuICAgICAgICB9LFxyXG4gICAgICAgIGVudHJpZXM6IFtcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgZW50cnk6IHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogJ2F2YXRhcicsXHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW1lVHlwZTogJ2ltYWdlL3BuZycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbWVUeXBlTmFtZTogJ1BORyBJbWFnZSdcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1czogJ05PVF9DUkVBVEVEJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBlbnRyeToge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiAnYXZhdGFyMzInLFxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWltZVR5cGU6ICdpbWFnZS9wbmcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW1lVHlwZU5hbWU6ICdQTkcgSW1hZ2UnXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6ICdOT1RfQ1JFQVRFRCdcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgZW50cnk6IHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogJ2RvY2xpYicsXHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW1lVHlwZTogJ2ltYWdlL3BuZycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbWVUeXBlTmFtZTogJ1BORyBJbWFnZSdcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1czogJ05PVF9DUkVBVEVEJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBlbnRyeToge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiAnaW1ncHJldmlldycsXHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW1lVHlwZTogJ2ltYWdlL2pwZWcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW1lVHlwZU5hbWU6ICdKUEVHIEltYWdlJ1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiAnTk9UX0NSRUFURUQnXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGVudHJ5OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWQ6ICdtZWRpdW0nLFxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWltZVR5cGU6ICdpbWFnZS9qcGVnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWltZVR5cGVOYW1lOiAnSlBFRyBJbWFnZSdcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHN0YXR1czogJ05PVF9DUkVBVEVEJ1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBlbnRyeToge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiAncGRmJyxcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbWVUeXBlOiAnYXBwbGljYXRpb24vcGRmJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWltZVR5cGVOYW1lOiAnQWRvYmUgUERGIERvY3VtZW50J1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzOiAnQ1JFQVRFRCdcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH1cclxufTtcclxuIl19