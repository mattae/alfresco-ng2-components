/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/** @type {?} */
export let fakeSearch = {
    list: {
        pagination: {
            count: 1,
            hasMoreItems: false,
            totalItems: 1,
            skipCount: 0,
            maxItems: 100
        },
        entries: [
            {
                entry: {
                    id: '123',
                    name: 'MyDoc',
                    content: {
                        mimetype: 'text/plain'
                    },
                    createdByUser: {
                        displayName: 'John Doe'
                    },
                    modifiedByUser: {
                        displayName: 'John Doe'
                    }
                }
            }
        ]
    }
};
/** @type {?} */
export let mockError = {
    error: {
        errorKey: 'Search failed',
        statusCode: 400,
        briefSummary: '08220082 search failed',
        stackTrace: 'For security reasons the stack trace is no longer displayed, but the property is kept for previous versions.',
        descriptionURL: 'https://api-explorer.alfresco.com'
    }
};
const ɵ0 = /**
 * @param {?} term
 * @param {?} opts
 * @return {?}
 */
(term, opts) => Promise.resolve(fakeSearch);
/** @type {?} */
export let searchMockApi = {
    core: {
        queriesApi: {
            findNodes: (ɵ0)
        }
    }
};
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnNlcnZpY2UubW9jay5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vY2svc2VhcmNoLnNlcnZpY2UubW9jay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsTUFBTSxLQUFLLFVBQVUsR0FBRztJQUNwQixJQUFJLEVBQUU7UUFDRixVQUFVLEVBQUU7WUFDUixLQUFLLEVBQUUsQ0FBQztZQUNSLFlBQVksRUFBRSxLQUFLO1lBQ25CLFVBQVUsRUFBRSxDQUFDO1lBQ2IsU0FBUyxFQUFFLENBQUM7WUFDWixRQUFRLEVBQUUsR0FBRztTQUNoQjtRQUNELE9BQU8sRUFBRTtZQUNMO2dCQUNJLEtBQUssRUFBRTtvQkFDSCxFQUFFLEVBQUUsS0FBSztvQkFDVCxJQUFJLEVBQUUsT0FBTztvQkFDYixPQUFPLEVBQUU7d0JBQ0wsUUFBUSxFQUFFLFlBQVk7cUJBQ3pCO29CQUNELGFBQWEsRUFBRTt3QkFDWCxXQUFXLEVBQUUsVUFBVTtxQkFDMUI7b0JBQ0QsY0FBYyxFQUFFO3dCQUNaLFdBQVcsRUFBRSxVQUFVO3FCQUMxQjtpQkFDSjthQUNKO1NBQ0o7S0FDSjtDQUNKOztBQUVELE1BQU0sS0FBSyxTQUFTLEdBQUc7SUFDbkIsS0FBSyxFQUFFO1FBQ0gsUUFBUSxFQUFFLGVBQWU7UUFDekIsVUFBVSxFQUFFLEdBQUc7UUFDZixZQUFZLEVBQUUsd0JBQXdCO1FBQ3RDLFVBQVUsRUFBRSw4R0FBOEc7UUFDMUgsY0FBYyxFQUFFLG1DQUFtQztLQUN0RDtDQUNKOzs7Ozs7QUFLc0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLEVBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFVBQVUsQ0FBQzs7QUFIbEUsTUFBTSxLQUFLLGFBQWEsR0FBRztJQUN2QixJQUFJLEVBQUU7UUFDRixVQUFVLEVBQUU7WUFDUixTQUFTLE1BQTZDO1NBQ3pEO0tBQ0o7Q0FDSiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5leHBvcnQgbGV0IGZha2VTZWFyY2ggPSB7XHJcbiAgICBsaXN0OiB7XHJcbiAgICAgICAgcGFnaW5hdGlvbjoge1xyXG4gICAgICAgICAgICBjb3VudDogMSxcclxuICAgICAgICAgICAgaGFzTW9yZUl0ZW1zOiBmYWxzZSxcclxuICAgICAgICAgICAgdG90YWxJdGVtczogMSxcclxuICAgICAgICAgICAgc2tpcENvdW50OiAwLFxyXG4gICAgICAgICAgICBtYXhJdGVtczogMTAwXHJcbiAgICAgICAgfSxcclxuICAgICAgICBlbnRyaWVzOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgIGVudHJ5OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWQ6ICcxMjMnLFxyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6ICdNeURvYycsXHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW1ldHlwZTogJ3RleHQvcGxhaW4nXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBjcmVhdGVkQnlVc2VyOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRpc3BsYXlOYW1lOiAnSm9obiBEb2UnXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICBtb2RpZmllZEJ5VXNlcjoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogJ0pvaG4gRG9lJ1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIF1cclxuICAgIH1cclxufTtcclxuXHJcbmV4cG9ydCBsZXQgbW9ja0Vycm9yID0ge1xyXG4gICAgZXJyb3I6IHtcclxuICAgICAgICBlcnJvcktleTogJ1NlYXJjaCBmYWlsZWQnLFxyXG4gICAgICAgIHN0YXR1c0NvZGU6IDQwMCxcclxuICAgICAgICBicmllZlN1bW1hcnk6ICcwODIyMDA4MiBzZWFyY2ggZmFpbGVkJyxcclxuICAgICAgICBzdGFja1RyYWNlOiAnRm9yIHNlY3VyaXR5IHJlYXNvbnMgdGhlIHN0YWNrIHRyYWNlIGlzIG5vIGxvbmdlciBkaXNwbGF5ZWQsIGJ1dCB0aGUgcHJvcGVydHkgaXMga2VwdCBmb3IgcHJldmlvdXMgdmVyc2lvbnMuJyxcclxuICAgICAgICBkZXNjcmlwdGlvblVSTDogJ2h0dHBzOi8vYXBpLWV4cGxvcmVyLmFsZnJlc2NvLmNvbSdcclxuICAgIH1cclxufTtcclxuXHJcbmV4cG9ydCBsZXQgc2VhcmNoTW9ja0FwaSA9IHtcclxuICAgIGNvcmU6IHtcclxuICAgICAgICBxdWVyaWVzQXBpOiB7XHJcbiAgICAgICAgICAgIGZpbmROb2RlczogKHRlcm0sIG9wdHMpID0+IFByb21pc2UucmVzb2x2ZShmYWtlU2VhcmNoKVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufTtcclxuIl19