/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { EventEmitter } from '@angular/core';
import { of } from 'rxjs';
/**
 * @record
 */
export function LangChangeEvent() { }
if (false) {
    /** @type {?} */
    LangChangeEvent.prototype.lang;
    /** @type {?} */
    LangChangeEvent.prototype.translations;
}
export class TranslationMock {
    constructor() {
        this.defaultLang = 'en';
        this.translate = {
            onLangChange: new EventEmitter()
        };
    }
    /**
     * @return {?}
     */
    addTranslationFolder() { }
    /**
     * @return {?}
     */
    onTranslationChanged() { }
    /**
     * @return {?}
     */
    use() { }
    /**
     * @return {?}
     */
    loadTranslation() { }
    /**
     * @param {?} key
     * @param {?=} interpolateParams
     * @return {?}
     */
    get(key, interpolateParams) {
        return of(key);
    }
    /**
     * @param {?} key
     * @param {?=} interpolateParams
     * @return {?}
     */
    instant(key, interpolateParams) {
        return key;
    }
}
if (false) {
    /** @type {?} */
    TranslationMock.prototype.defaultLang;
    /** @type {?} */
    TranslationMock.prototype.userLang;
    /** @type {?} */
    TranslationMock.prototype.customLoader;
    /** @type {?} */
    TranslationMock.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhbnNsYXRpb24uc2VydmljZS5tb2NrLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsibW9jay90cmFuc2xhdGlvbi5zZXJ2aWNlLm1vY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3QyxPQUFPLEVBQWMsRUFBRSxFQUFFLE1BQU0sTUFBTSxDQUFDOzs7O0FBR3RDLHFDQUdDOzs7SUFGRywrQkFBYTs7SUFDYix1Q0FBa0I7O0FBR3RCLE1BQU0sT0FBTyxlQUFlO0lBQTVCO1FBRUksZ0JBQVcsR0FBVyxJQUFJLENBQUM7UUFJM0IsY0FBUyxHQUFRO1lBQ2IsWUFBWSxFQUFFLElBQUksWUFBWSxFQUFtQjtTQUNwRCxDQUFDO0lBa0JOLENBQUM7Ozs7SUFoQkcsb0JBQW9CLEtBQUksQ0FBQzs7OztJQUV6QixvQkFBb0IsS0FBSSxDQUFDOzs7O0lBRXpCLEdBQUcsS0FBUyxDQUFDOzs7O0lBRWIsZUFBZSxLQUFJLENBQUM7Ozs7OztJQUVwQixHQUFHLENBQUMsR0FBMkIsRUFBRSxpQkFBMEI7UUFDdkQsT0FBTyxFQUFFLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDbkIsQ0FBQzs7Ozs7O0lBRUQsT0FBTyxDQUFDLEdBQTJCLEVBQUUsaUJBQTBCO1FBQzNELE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQztDQUVKOzs7SUF4Qkcsc0NBQTJCOztJQUMzQixtQ0FBaUI7O0lBQ2pCLHVDQUFrQjs7SUFFbEIsb0NBRUUiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgRXZlbnRFbWl0dGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIG9mIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFRyYW5zbGF0aW9uU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3RyYW5zbGF0aW9uLnNlcnZpY2UnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBMYW5nQ2hhbmdlRXZlbnQge1xyXG4gICAgbGFuZzogc3RyaW5nO1xyXG4gICAgdHJhbnNsYXRpb25zOiBhbnk7XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBUcmFuc2xhdGlvbk1vY2sgaW1wbGVtZW50cyBUcmFuc2xhdGlvblNlcnZpY2Uge1xyXG5cclxuICAgIGRlZmF1bHRMYW5nOiBzdHJpbmcgPSAnZW4nO1xyXG4gICAgdXNlckxhbmc6IHN0cmluZztcclxuICAgIGN1c3RvbUxvYWRlcjogYW55O1xyXG5cclxuICAgIHRyYW5zbGF0ZTogYW55ID0ge1xyXG4gICAgICAgIG9uTGFuZ0NoYW5nZTogbmV3IEV2ZW50RW1pdHRlcjxMYW5nQ2hhbmdlRXZlbnQ+KClcclxuICAgIH07XHJcblxyXG4gICAgYWRkVHJhbnNsYXRpb25Gb2xkZXIoKSB7fVxyXG5cclxuICAgIG9uVHJhbnNsYXRpb25DaGFuZ2VkKCkge31cclxuXHJcbiAgICB1c2UoKTogYW55IHt9XHJcblxyXG4gICAgbG9hZFRyYW5zbGF0aW9uKCkge31cclxuXHJcbiAgICBnZXQoa2V5OiBzdHJpbmcgfCBBcnJheTxzdHJpbmc+LCBpbnRlcnBvbGF0ZVBhcmFtcz86IE9iamVjdCk6IE9ic2VydmFibGU8c3RyaW5nIHwgYW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG9mKGtleSk7XHJcbiAgICB9XHJcblxyXG4gICAgaW5zdGFudChrZXk6IHN0cmluZyB8IEFycmF5PHN0cmluZz4sIGludGVycG9sYXRlUGFyYW1zPzogT2JqZWN0KTogc3RyaW5nIHwgYW55IHtcclxuICAgICAgICByZXR1cm4ga2V5O1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=