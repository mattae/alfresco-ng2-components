/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* spellchecker: disable */
export class AllowableOperationsEnum extends String {
}
AllowableOperationsEnum.DELETE = 'delete';
AllowableOperationsEnum.UPDATE = 'update';
AllowableOperationsEnum.CREATE = 'create';
AllowableOperationsEnum.COPY = 'copy';
AllowableOperationsEnum.LOCK = 'lock';
AllowableOperationsEnum.UPDATEPERMISSIONS = 'updatePermissions';
AllowableOperationsEnum.NOT_DELETE = '!delete';
AllowableOperationsEnum.NOT_UPDATE = '!update';
AllowableOperationsEnum.NOT_CREATE = '!create';
AllowableOperationsEnum.NOT_UPDATEPERMISSIONS = '!updatePermissions';
if (false) {
    /** @type {?} */
    AllowableOperationsEnum.DELETE;
    /** @type {?} */
    AllowableOperationsEnum.UPDATE;
    /** @type {?} */
    AllowableOperationsEnum.CREATE;
    /** @type {?} */
    AllowableOperationsEnum.COPY;
    /** @type {?} */
    AllowableOperationsEnum.LOCK;
    /** @type {?} */
    AllowableOperationsEnum.UPDATEPERMISSIONS;
    /** @type {?} */
    AllowableOperationsEnum.NOT_DELETE;
    /** @type {?} */
    AllowableOperationsEnum.NOT_UPDATE;
    /** @type {?} */
    AllowableOperationsEnum.NOT_CREATE;
    /** @type {?} */
    AllowableOperationsEnum.NOT_UPDATEPERMISSIONS;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxsb3dhYmxlLW9wZXJhdGlvbnMuZW51bS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vZGVscy9hbGxvd2FibGUtb3BlcmF0aW9ucy5lbnVtLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQSxNQUFNLE9BQU8sdUJBQXdCLFNBQVEsTUFBTTs7QUFDeEMsOEJBQU0sR0FBVyxRQUFRLENBQUM7QUFDMUIsOEJBQU0sR0FBVyxRQUFRLENBQUM7QUFDMUIsOEJBQU0sR0FBVyxRQUFRLENBQUM7QUFDMUIsNEJBQUksR0FBVyxNQUFNLENBQUM7QUFDdEIsNEJBQUksR0FBVyxNQUFNLENBQUM7QUFDdEIseUNBQWlCLEdBQVcsbUJBQW1CLENBQUM7QUFDaEQsa0NBQVUsR0FBVyxTQUFTLENBQUM7QUFDL0Isa0NBQVUsR0FBVyxTQUFTLENBQUM7QUFDL0Isa0NBQVUsR0FBVyxTQUFTLENBQUM7QUFDL0IsNkNBQXFCLEdBQVcsb0JBQW9CLENBQUM7OztJQVQ1RCwrQkFBaUM7O0lBQ2pDLCtCQUFpQzs7SUFDakMsK0JBQWlDOztJQUNqQyw2QkFBNkI7O0lBQzdCLDZCQUE2Qjs7SUFDN0IsMENBQXVEOztJQUN2RCxtQ0FBc0M7O0lBQ3RDLG1DQUFzQzs7SUFDdEMsbUNBQXNDOztJQUN0Qyw4Q0FBNEQiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuIC8qIHNwZWxsY2hlY2tlcjogZGlzYWJsZSAqL1xyXG5leHBvcnQgY2xhc3MgQWxsb3dhYmxlT3BlcmF0aW9uc0VudW0gZXh0ZW5kcyBTdHJpbmcge1xyXG4gICAgc3RhdGljIERFTEVURTogc3RyaW5nID0gJ2RlbGV0ZSc7XHJcbiAgICBzdGF0aWMgVVBEQVRFOiBzdHJpbmcgPSAndXBkYXRlJztcclxuICAgIHN0YXRpYyBDUkVBVEU6IHN0cmluZyA9ICdjcmVhdGUnO1xyXG4gICAgc3RhdGljIENPUFk6IHN0cmluZyA9ICdjb3B5JztcclxuICAgIHN0YXRpYyBMT0NLOiBzdHJpbmcgPSAnbG9jayc7XHJcbiAgICBzdGF0aWMgVVBEQVRFUEVSTUlTU0lPTlM6IHN0cmluZyA9ICd1cGRhdGVQZXJtaXNzaW9ucyc7XHJcbiAgICBzdGF0aWMgTk9UX0RFTEVURTogc3RyaW5nID0gJyFkZWxldGUnO1xyXG4gICAgc3RhdGljIE5PVF9VUERBVEU6IHN0cmluZyA9ICchdXBkYXRlJztcclxuICAgIHN0YXRpYyBOT1RfQ1JFQVRFOiBzdHJpbmcgPSAnIWNyZWF0ZSc7XHJcbiAgICBzdGF0aWMgTk9UX1VQREFURVBFUk1JU1NJT05TOiBzdHJpbmcgPSAnIXVwZGF0ZVBlcm1pc3Npb25zJztcclxufVxyXG4iXX0=