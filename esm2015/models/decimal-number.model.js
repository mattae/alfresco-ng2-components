/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class DecimalNumberModel {
    /**
     * @param {?=} obj
     */
    constructor(obj) {
        if (obj) {
            this.minIntegerDigits = obj.minIntegerDigits;
            this.minFractionDigits = obj.minFractionDigits;
            this.maxFractionDigits = obj.maxFractionDigits;
        }
    }
}
if (false) {
    /** @type {?} */
    DecimalNumberModel.prototype.minIntegerDigits;
    /** @type {?} */
    DecimalNumberModel.prototype.minFractionDigits;
    /** @type {?} */
    DecimalNumberModel.prototype.maxFractionDigits;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVjaW1hbC1udW1iZXIubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJtb2RlbHMvZGVjaW1hbC1udW1iZXIubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsTUFBTSxPQUFPLGtCQUFrQjs7OztJQUszQixZQUFZLEdBQVM7UUFDakIsSUFBSSxHQUFHLEVBQUU7WUFDTCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsR0FBRyxDQUFDLGdCQUFnQixDQUFDO1lBQzdDLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxHQUFHLENBQUMsaUJBQWlCLENBQUM7WUFDL0MsSUFBSSxDQUFDLGlCQUFpQixHQUFHLEdBQUcsQ0FBQyxpQkFBaUIsQ0FBQztTQUNsRDtJQUNMLENBQUM7Q0FDSjs7O0lBWEcsOENBQXlCOztJQUN6QiwrQ0FBMEI7O0lBQzFCLCtDQUEwQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5leHBvcnQgY2xhc3MgRGVjaW1hbE51bWJlck1vZGVsIHtcclxuICAgIG1pbkludGVnZXJEaWdpdHM6IG51bWJlcjtcclxuICAgIG1pbkZyYWN0aW9uRGlnaXRzOiBudW1iZXI7XHJcbiAgICBtYXhGcmFjdGlvbkRpZ2l0czogbnVtYmVyO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKG9iaj86IGFueSkge1xyXG4gICAgICAgIGlmIChvYmopIHtcclxuICAgICAgICAgICAgdGhpcy5taW5JbnRlZ2VyRGlnaXRzID0gb2JqLm1pbkludGVnZXJEaWdpdHM7XHJcbiAgICAgICAgICAgIHRoaXMubWluRnJhY3Rpb25EaWdpdHMgPSBvYmoubWluRnJhY3Rpb25EaWdpdHM7XHJcbiAgICAgICAgICAgIHRoaXMubWF4RnJhY3Rpb25EaWdpdHMgPSBvYmoubWF4RnJhY3Rpb25EaWdpdHM7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==