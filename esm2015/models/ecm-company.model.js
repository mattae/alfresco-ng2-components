/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class EcmCompanyModel {
}
if (false) {
    /** @type {?} */
    EcmCompanyModel.prototype.organization;
    /** @type {?} */
    EcmCompanyModel.prototype.address1;
    /** @type {?} */
    EcmCompanyModel.prototype.address2;
    /** @type {?} */
    EcmCompanyModel.prototype.address3;
    /** @type {?} */
    EcmCompanyModel.prototype.postcode;
    /** @type {?} */
    EcmCompanyModel.prototype.telephone;
    /** @type {?} */
    EcmCompanyModel.prototype.fax;
    /** @type {?} */
    EcmCompanyModel.prototype.email;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWNtLWNvbXBhbnkubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJtb2RlbHMvZWNtLWNvbXBhbnkubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsTUFBTSxPQUFPLGVBQWU7Q0FTM0I7OztJQVJLLHVDQUFxQjs7SUFDckIsbUNBQWlCOztJQUNqQixtQ0FBaUI7O0lBQ2pCLG1DQUFpQjs7SUFDakIsbUNBQWlCOztJQUNqQixvQ0FBa0I7O0lBQ2xCLDhCQUFZOztJQUNaLGdDQUFjIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmV4cG9ydCBjbGFzcyBFY21Db21wYW55TW9kZWwge1xyXG4gICAgICBvcmdhbml6YXRpb246IHN0cmluZztcclxuICAgICAgYWRkcmVzczE6IHN0cmluZztcclxuICAgICAgYWRkcmVzczI6IHN0cmluZztcclxuICAgICAgYWRkcmVzczM6IHN0cmluZztcclxuICAgICAgcG9zdGNvZGU6IHN0cmluZztcclxuICAgICAgdGVsZXBob25lOiBzdHJpbmc7XHJcbiAgICAgIGZheDogc3RyaW5nO1xyXG4gICAgICBlbWFpbDogc3RyaW5nO1xyXG59XHJcbiJdfQ==