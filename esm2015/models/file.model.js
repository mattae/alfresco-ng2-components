/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @record
 */
export function FileUploadProgress() { }
if (false) {
    /** @type {?} */
    FileUploadProgress.prototype.loaded;
    /** @type {?} */
    FileUploadProgress.prototype.total;
    /** @type {?} */
    FileUploadProgress.prototype.percent;
}
export class FileUploadOptions {
}
if (false) {
    /** @type {?} */
    FileUploadOptions.prototype.comment;
    /** @type {?} */
    FileUploadOptions.prototype.newVersion;
    /** @type {?} */
    FileUploadOptions.prototype.majorVersion;
    /** @type {?} */
    FileUploadOptions.prototype.parentId;
    /** @type {?} */
    FileUploadOptions.prototype.path;
    /** @type {?} */
    FileUploadOptions.prototype.nodeType;
    /** @type {?} */
    FileUploadOptions.prototype.properties;
    /** @type {?} */
    FileUploadOptions.prototype.association;
    /** @type {?} */
    FileUploadOptions.prototype.secondaryChildren;
    /** @type {?} */
    FileUploadOptions.prototype.targets;
}
/** @enum {number} */
const FileUploadStatus = {
    Pending: 0,
    Complete: 1,
    Starting: 2,
    Progress: 3,
    Cancelled: 4,
    Aborted: 5,
    Error: 6,
    Deleted: 7,
};
export { FileUploadStatus };
FileUploadStatus[FileUploadStatus.Pending] = 'Pending';
FileUploadStatus[FileUploadStatus.Complete] = 'Complete';
FileUploadStatus[FileUploadStatus.Starting] = 'Starting';
FileUploadStatus[FileUploadStatus.Progress] = 'Progress';
FileUploadStatus[FileUploadStatus.Cancelled] = 'Cancelled';
FileUploadStatus[FileUploadStatus.Aborted] = 'Aborted';
FileUploadStatus[FileUploadStatus.Error] = 'Error';
FileUploadStatus[FileUploadStatus.Deleted] = 'Deleted';
export class FileModel {
    /**
     * @param {?} file
     * @param {?=} options
     * @param {?=} id
     */
    constructor(file, options, id) {
        this.status = FileUploadStatus.Pending;
        this.file = file;
        this.id = id;
        this.name = file.name;
        this.size = file.size;
        this.data = null;
        this.errorCode = null;
        this.progress = {
            loaded: 0,
            total: 0,
            percent: 0
        };
        this.options = Object.assign({}, {
            newVersion: false
        }, options);
    }
    /**
     * @return {?}
     */
    get extension() {
        return this.name.slice((Math.max(0, this.name.lastIndexOf('.')) || Infinity) + 1);
    }
}
if (false) {
    /** @type {?} */
    FileModel.prototype.name;
    /** @type {?} */
    FileModel.prototype.size;
    /** @type {?} */
    FileModel.prototype.file;
    /** @type {?} */
    FileModel.prototype.id;
    /** @type {?} */
    FileModel.prototype.status;
    /** @type {?} */
    FileModel.prototype.errorCode;
    /** @type {?} */
    FileModel.prototype.progress;
    /** @type {?} */
    FileModel.prototype.options;
    /** @type {?} */
    FileModel.prototype.data;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vZGVscy9maWxlLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLHdDQUlDOzs7SUFIRyxvQ0FBZTs7SUFDZixtQ0FBYzs7SUFDZCxxQ0FBZ0I7O0FBR3BCLE1BQU0sT0FBTyxpQkFBaUI7Q0FXN0I7OztJQVZHLG9DQUFpQjs7SUFDakIsdUNBQXFCOztJQUNyQix5Q0FBdUI7O0lBQ3ZCLHFDQUFrQjs7SUFDbEIsaUNBQWM7O0lBQ2QscUNBQWtCOztJQUNsQix1Q0FBaUI7O0lBQ2pCLHdDQUFrQjs7SUFDbEIsOENBQXFDOztJQUNyQyxvQ0FBNEI7Ozs7SUFJNUIsVUFBVztJQUNYLFdBQVk7SUFDWixXQUFZO0lBQ1osV0FBWTtJQUNaLFlBQWE7SUFDYixVQUFXO0lBQ1gsUUFBUztJQUNULFVBQVc7Ozs7Ozs7Ozs7O0FBR2YsTUFBTSxPQUFPLFNBQVM7Ozs7OztJQVlsQixZQUFZLElBQVUsRUFBRSxPQUEyQixFQUFFLEVBQVc7UUFOaEUsV0FBTSxHQUFxQixnQkFBZ0IsQ0FBQyxPQUFPLENBQUM7UUFPaEQsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7UUFDakIsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLENBQUM7UUFDYixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDdEIsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBRXRCLElBQUksQ0FBQyxRQUFRLEdBQUc7WUFDWixNQUFNLEVBQUUsQ0FBQztZQUNULEtBQUssRUFBRSxDQUFDO1lBQ1IsT0FBTyxFQUFFLENBQUM7U0FDYixDQUFDO1FBRUYsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRTtZQUM3QixVQUFVLEVBQUUsS0FBSztTQUNwQixFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ2hCLENBQUM7Ozs7SUFFRCxJQUFJLFNBQVM7UUFDVCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxRQUFRLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUN0RixDQUFDO0NBQ0o7OztJQWpDRyx5QkFBc0I7O0lBQ3RCLHlCQUFzQjs7SUFDdEIseUJBQW9COztJQUVwQix1QkFBVzs7SUFDWCwyQkFBb0Q7O0lBQ3BELDhCQUFrQjs7SUFDbEIsNkJBQTZCOztJQUM3Qiw0QkFBMkI7O0lBQzNCLHlCQUFVIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEFzc29jQ2hpbGRCb2R5LCBBc3NvY2lhdGlvbkJvZHkgfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgRmlsZVVwbG9hZFByb2dyZXNzIHtcclxuICAgIGxvYWRlZDogbnVtYmVyO1xyXG4gICAgdG90YWw6IG51bWJlcjtcclxuICAgIHBlcmNlbnQ6IG51bWJlcjtcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIEZpbGVVcGxvYWRPcHRpb25zIHtcclxuICAgIGNvbW1lbnQ/OiBzdHJpbmc7XHJcbiAgICBuZXdWZXJzaW9uPzogYm9vbGVhbjtcclxuICAgIG1ham9yVmVyc2lvbj86IGJvb2xlYW47XHJcbiAgICBwYXJlbnRJZD86IHN0cmluZztcclxuICAgIHBhdGg/OiBzdHJpbmc7XHJcbiAgICBub2RlVHlwZT86IHN0cmluZztcclxuICAgIHByb3BlcnRpZXM/OiBhbnk7XHJcbiAgICBhc3NvY2lhdGlvbj86IGFueTtcclxuICAgIHNlY29uZGFyeUNoaWxkcmVuPzogQXNzb2NDaGlsZEJvZHlbXTtcclxuICAgIHRhcmdldHM/OiBBc3NvY2lhdGlvbkJvZHlbXTtcclxufVxyXG5cclxuZXhwb3J0IGVudW0gRmlsZVVwbG9hZFN0YXR1cyB7XHJcbiAgICBQZW5kaW5nID0gMCxcclxuICAgIENvbXBsZXRlID0gMSxcclxuICAgIFN0YXJ0aW5nID0gMixcclxuICAgIFByb2dyZXNzID0gMyxcclxuICAgIENhbmNlbGxlZCA9IDQsXHJcbiAgICBBYm9ydGVkID0gNSxcclxuICAgIEVycm9yID0gNixcclxuICAgIERlbGV0ZWQgPSA3XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBGaWxlTW9kZWwge1xyXG4gICAgcmVhZG9ubHkgbmFtZTogc3RyaW5nO1xyXG4gICAgcmVhZG9ubHkgc2l6ZTogbnVtYmVyO1xyXG4gICAgcmVhZG9ubHkgZmlsZTogRmlsZTtcclxuXHJcbiAgICBpZDogc3RyaW5nO1xyXG4gICAgc3RhdHVzOiBGaWxlVXBsb2FkU3RhdHVzID0gRmlsZVVwbG9hZFN0YXR1cy5QZW5kaW5nO1xyXG4gICAgZXJyb3JDb2RlOiBudW1iZXI7XHJcbiAgICBwcm9ncmVzczogRmlsZVVwbG9hZFByb2dyZXNzO1xyXG4gICAgb3B0aW9uczogRmlsZVVwbG9hZE9wdGlvbnM7XHJcbiAgICBkYXRhOiBhbnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoZmlsZTogRmlsZSwgb3B0aW9ucz86IEZpbGVVcGxvYWRPcHRpb25zLCBpZD86IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuZmlsZSA9IGZpbGU7XHJcbiAgICAgICAgdGhpcy5pZCA9IGlkO1xyXG4gICAgICAgIHRoaXMubmFtZSA9IGZpbGUubmFtZTtcclxuICAgICAgICB0aGlzLnNpemUgPSBmaWxlLnNpemU7XHJcbiAgICAgICAgdGhpcy5kYXRhID0gbnVsbDtcclxuICAgICAgICB0aGlzLmVycm9yQ29kZSA9IG51bGw7XHJcblxyXG4gICAgICAgIHRoaXMucHJvZ3Jlc3MgPSB7XHJcbiAgICAgICAgICAgIGxvYWRlZDogMCxcclxuICAgICAgICAgICAgdG90YWw6IDAsXHJcbiAgICAgICAgICAgIHBlcmNlbnQ6IDBcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICB0aGlzLm9wdGlvbnMgPSBPYmplY3QuYXNzaWduKHt9LCB7XHJcbiAgICAgICAgICAgIG5ld1ZlcnNpb246IGZhbHNlXHJcbiAgICAgICAgfSwgb3B0aW9ucyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGV4dGVuc2lvbigpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm5hbWUuc2xpY2UoKE1hdGgubWF4KDAsIHRoaXMubmFtZS5sYXN0SW5kZXhPZignLicpKSB8fCBJbmZpbml0eSkgKyAxKTtcclxuICAgIH1cclxufVxyXG4iXX0=