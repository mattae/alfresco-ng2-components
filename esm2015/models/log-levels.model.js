/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class LogLevelsEnum extends Number {
}
LogLevelsEnum.TRACE = 5;
LogLevelsEnum.DEBUG = 4;
LogLevelsEnum.INFO = 3;
LogLevelsEnum.WARN = 2;
LogLevelsEnum.ERROR = 1;
LogLevelsEnum.SILENT = 0;
if (false) {
    /** @type {?} */
    LogLevelsEnum.TRACE;
    /** @type {?} */
    LogLevelsEnum.DEBUG;
    /** @type {?} */
    LogLevelsEnum.INFO;
    /** @type {?} */
    LogLevelsEnum.WARN;
    /** @type {?} */
    LogLevelsEnum.ERROR;
    /** @type {?} */
    LogLevelsEnum.SILENT;
}
/** @type {?} */
export let logLevels = [
    { level: LogLevelsEnum.TRACE, name: 'TRACE' },
    { level: LogLevelsEnum.DEBUG, name: 'DEBUG' },
    { level: LogLevelsEnum.INFO, name: 'INFO' },
    { level: LogLevelsEnum.WARN, name: 'WARN' },
    { level: LogLevelsEnum.ERROR, name: 'ERROR' },
    { level: LogLevelsEnum.SILENT, name: 'SILENT' }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLWxldmVscy5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vZGVscy9sb2ctbGV2ZWxzLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE1BQU0sT0FBTyxhQUFjLFNBQVEsTUFBTTs7QUFDOUIsbUJBQUssR0FBVyxDQUFDLENBQUM7QUFDbEIsbUJBQUssR0FBVyxDQUFDLENBQUM7QUFDbEIsa0JBQUksR0FBVyxDQUFDLENBQUM7QUFDakIsa0JBQUksR0FBVyxDQUFDLENBQUM7QUFDakIsbUJBQUssR0FBVyxDQUFDLENBQUM7QUFDbEIsb0JBQU0sR0FBVyxDQUFDLENBQUM7OztJQUwxQixvQkFBeUI7O0lBQ3pCLG9CQUF5Qjs7SUFDekIsbUJBQXdCOztJQUN4QixtQkFBd0I7O0lBQ3hCLG9CQUF5Qjs7SUFDekIscUJBQTBCOzs7QUFHOUIsTUFBTSxLQUFLLFNBQVMsR0FBVTtJQUMxQixFQUFDLEtBQUssRUFBRSxhQUFhLENBQUMsS0FBSyxFQUFFLElBQUksRUFBRSxPQUFPLEVBQUM7SUFDM0MsRUFBQyxLQUFLLEVBQUUsYUFBYSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFDO0lBQzNDLEVBQUMsS0FBSyxFQUFFLGFBQWEsQ0FBQyxJQUFJLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBQztJQUN6QyxFQUFDLEtBQUssRUFBRSxhQUFhLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUM7SUFDekMsRUFBQyxLQUFLLEVBQUUsYUFBYSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFDO0lBQzNDLEVBQUMsS0FBSyxFQUFFLGFBQWEsQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLFFBQVEsRUFBQztDQUNoRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5leHBvcnQgY2xhc3MgTG9nTGV2ZWxzRW51bSBleHRlbmRzIE51bWJlciB7XHJcbiAgICBzdGF0aWMgVFJBQ0U6IG51bWJlciA9IDU7XHJcbiAgICBzdGF0aWMgREVCVUc6IG51bWJlciA9IDQ7XHJcbiAgICBzdGF0aWMgSU5GTzogbnVtYmVyID0gMztcclxuICAgIHN0YXRpYyBXQVJOOiBudW1iZXIgPSAyO1xyXG4gICAgc3RhdGljIEVSUk9SOiBudW1iZXIgPSAxO1xyXG4gICAgc3RhdGljIFNJTEVOVDogbnVtYmVyID0gMDtcclxufVxyXG5cclxuZXhwb3J0IGxldCBsb2dMZXZlbHM6IGFueVtdID0gW1xyXG4gICAge2xldmVsOiBMb2dMZXZlbHNFbnVtLlRSQUNFLCBuYW1lOiAnVFJBQ0UnfSxcclxuICAgIHtsZXZlbDogTG9nTGV2ZWxzRW51bS5ERUJVRywgbmFtZTogJ0RFQlVHJ30sXHJcbiAgICB7bGV2ZWw6IExvZ0xldmVsc0VudW0uSU5GTywgbmFtZTogJ0lORk8nfSxcclxuICAgIHtsZXZlbDogTG9nTGV2ZWxzRW51bS5XQVJOLCBuYW1lOiAnV0FSTid9LFxyXG4gICAge2xldmVsOiBMb2dMZXZlbHNFbnVtLkVSUk9SLCBuYW1lOiAnRVJST1InfSxcclxuICAgIHtsZXZlbDogTG9nTGV2ZWxzRW51bS5TSUxFTlQsIG5hbWU6ICdTSUxFTlQnfVxyXG5dO1xyXG4iXX0=