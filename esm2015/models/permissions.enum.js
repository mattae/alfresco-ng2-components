/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* spellchecker: disable */
export class PermissionsEnum extends String {
}
PermissionsEnum.CONTRIBUTOR = 'Contributor';
PermissionsEnum.CONSUMER = 'Consumer';
PermissionsEnum.COLLABORATOR = 'Collaborator';
PermissionsEnum.MANAGER = 'Manager';
PermissionsEnum.EDITOR = 'Editor';
PermissionsEnum.COORDINATOR = 'Coordinator';
PermissionsEnum.NOT_CONTRIBUTOR = '!Contributor';
PermissionsEnum.NOT_CONSUMER = '!Consumer';
PermissionsEnum.NOT_COLLABORATOR = '!Collaborator';
PermissionsEnum.NOT_MANAGER = '!Manager';
PermissionsEnum.NOT_EDITOR = '!Editor';
PermissionsEnum.NOT_COORDINATOR = '!Coordinator';
if (false) {
    /** @type {?} */
    PermissionsEnum.CONTRIBUTOR;
    /** @type {?} */
    PermissionsEnum.CONSUMER;
    /** @type {?} */
    PermissionsEnum.COLLABORATOR;
    /** @type {?} */
    PermissionsEnum.MANAGER;
    /** @type {?} */
    PermissionsEnum.EDITOR;
    /** @type {?} */
    PermissionsEnum.COORDINATOR;
    /** @type {?} */
    PermissionsEnum.NOT_CONTRIBUTOR;
    /** @type {?} */
    PermissionsEnum.NOT_CONSUMER;
    /** @type {?} */
    PermissionsEnum.NOT_COLLABORATOR;
    /** @type {?} */
    PermissionsEnum.NOT_MANAGER;
    /** @type {?} */
    PermissionsEnum.NOT_EDITOR;
    /** @type {?} */
    PermissionsEnum.NOT_COORDINATOR;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVybWlzc2lvbnMuZW51bS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vZGVscy9wZXJtaXNzaW9ucy5lbnVtLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQSxNQUFNLE9BQU8sZUFBZ0IsU0FBUSxNQUFNOztBQUNoQywyQkFBVyxHQUFXLGFBQWEsQ0FBQztBQUNwQyx3QkFBUSxHQUFXLFVBQVUsQ0FBQztBQUM5Qiw0QkFBWSxHQUFXLGNBQWMsQ0FBQztBQUN0Qyx1QkFBTyxHQUFXLFNBQVMsQ0FBQztBQUM1QixzQkFBTSxHQUFXLFFBQVEsQ0FBQztBQUMxQiwyQkFBVyxHQUFXLGFBQWEsQ0FBQztBQUNwQywrQkFBZSxHQUFXLGNBQWMsQ0FBQztBQUN6Qyw0QkFBWSxHQUFXLFdBQVcsQ0FBQztBQUNuQyxnQ0FBZ0IsR0FBVyxlQUFlLENBQUM7QUFDM0MsMkJBQVcsR0FBVyxVQUFVLENBQUM7QUFDakMsMEJBQVUsR0FBVyxTQUFTLENBQUM7QUFDL0IsK0JBQWUsR0FBVyxjQUFjLENBQUM7OztJQVhoRCw0QkFBMkM7O0lBQzNDLHlCQUFxQzs7SUFDckMsNkJBQTZDOztJQUM3Qyx3QkFBbUM7O0lBQ25DLHVCQUFpQzs7SUFDakMsNEJBQTJDOztJQUMzQyxnQ0FBZ0Q7O0lBQ2hELDZCQUEwQzs7SUFDMUMsaUNBQWtEOztJQUNsRCw0QkFBd0M7O0lBQ3hDLDJCQUFzQzs7SUFDdEMsZ0NBQWdEIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbiAvKiBzcGVsbGNoZWNrZXI6IGRpc2FibGUgKi9cclxuZXhwb3J0IGNsYXNzIFBlcm1pc3Npb25zRW51bSBleHRlbmRzIFN0cmluZyB7XHJcbiAgICBzdGF0aWMgQ09OVFJJQlVUT1I6IHN0cmluZyA9ICdDb250cmlidXRvcic7XHJcbiAgICBzdGF0aWMgQ09OU1VNRVI6IHN0cmluZyA9ICdDb25zdW1lcic7XHJcbiAgICBzdGF0aWMgQ09MTEFCT1JBVE9SOiBzdHJpbmcgPSAnQ29sbGFib3JhdG9yJztcclxuICAgIHN0YXRpYyBNQU5BR0VSOiBzdHJpbmcgPSAnTWFuYWdlcic7XHJcbiAgICBzdGF0aWMgRURJVE9SOiBzdHJpbmcgPSAnRWRpdG9yJztcclxuICAgIHN0YXRpYyBDT09SRElOQVRPUjogc3RyaW5nID0gJ0Nvb3JkaW5hdG9yJztcclxuICAgIHN0YXRpYyBOT1RfQ09OVFJJQlVUT1I6IHN0cmluZyA9ICchQ29udHJpYnV0b3InO1xyXG4gICAgc3RhdGljIE5PVF9DT05TVU1FUjogc3RyaW5nID0gJyFDb25zdW1lcic7XHJcbiAgICBzdGF0aWMgTk9UX0NPTExBQk9SQVRPUjogc3RyaW5nID0gJyFDb2xsYWJvcmF0b3InO1xyXG4gICAgc3RhdGljIE5PVF9NQU5BR0VSOiBzdHJpbmcgPSAnIU1hbmFnZXInO1xyXG4gICAgc3RhdGljIE5PVF9FRElUT1I6IHN0cmluZyA9ICchRWRpdG9yJztcclxuICAgIHN0YXRpYyBOT1RfQ09PUkRJTkFUT1I6IHN0cmluZyA9ICchQ29vcmRpbmF0b3InO1xyXG59XHJcbiJdfQ==