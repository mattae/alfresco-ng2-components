/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class BpmProductVersionModel {
    /**
     * @param {?=} obj
     */
    constructor(obj) {
        if (obj) {
            this.edition = obj.edition || null;
            this.majorVersion = obj.majorVersion || null;
            this.revisionVersion = obj.revisionVersion || null;
            this.minorVersion = obj.minorVersion || null;
            this.type = obj.type || null;
        }
    }
}
if (false) {
    /** @type {?} */
    BpmProductVersionModel.prototype.edition;
    /** @type {?} */
    BpmProductVersionModel.prototype.majorVersion;
    /** @type {?} */
    BpmProductVersionModel.prototype.revisionVersion;
    /** @type {?} */
    BpmProductVersionModel.prototype.minorVersion;
    /** @type {?} */
    BpmProductVersionModel.prototype.type;
}
export class VersionModel {
    /**
     * @param {?=} obj
     */
    constructor(obj) {
        if (obj) {
            this.major = obj.major || null;
            this.minor = obj.minor || null;
            this.patch = obj.patch || null;
            this.hotfix = obj.hotfix || null;
            this.schema = obj.schema || null;
            this.label = obj.label || null;
            this.display = obj.display || null;
        }
    }
}
if (false) {
    /** @type {?} */
    VersionModel.prototype.major;
    /** @type {?} */
    VersionModel.prototype.minor;
    /** @type {?} */
    VersionModel.prototype.patch;
    /** @type {?} */
    VersionModel.prototype.hotfix;
    /** @type {?} */
    VersionModel.prototype.schema;
    /** @type {?} */
    VersionModel.prototype.label;
    /** @type {?} */
    VersionModel.prototype.display;
}
export class LicenseModel {
    /**
     * @param {?=} obj
     */
    constructor(obj) {
        if (obj) {
            this.issuedAt = obj.issuedAt || null;
            this.expiresAt = obj.expiresAt || null;
            this.remainingDays = obj.remainingDays || null;
            this.holder = obj.holder || null;
            this.mode = obj.mode || null;
            this.isClusterEnabled = obj.isClusterEnabled ? true : false;
            this.isCryptodocEnabled = obj.isCryptodocEnabled ? true : false;
        }
    }
}
if (false) {
    /** @type {?} */
    LicenseModel.prototype.issuedAt;
    /** @type {?} */
    LicenseModel.prototype.expiresAt;
    /** @type {?} */
    LicenseModel.prototype.remainingDays;
    /** @type {?} */
    LicenseModel.prototype.holder;
    /** @type {?} */
    LicenseModel.prototype.mode;
    /** @type {?} */
    LicenseModel.prototype.isClusterEnabled;
    /** @type {?} */
    LicenseModel.prototype.isCryptodocEnabled;
}
export class VersionStatusModel {
    /**
     * @param {?=} obj
     */
    constructor(obj) {
        if (obj) {
            this.isReadOnly = obj.isReadOnly ? true : false;
            this.isAuditEnabled = obj.isAuditEnabled ? true : false;
            this.isQuickShareEnabled = obj.isQuickShareEnabled ? true : false;
            this.isThumbnailGenerationEnabled = obj.isThumbnailGenerationEnabled ? true : false;
        }
    }
}
if (false) {
    /** @type {?} */
    VersionStatusModel.prototype.isReadOnly;
    /** @type {?} */
    VersionStatusModel.prototype.isAuditEnabled;
    /** @type {?} */
    VersionStatusModel.prototype.isQuickShareEnabled;
    /** @type {?} */
    VersionStatusModel.prototype.isThumbnailGenerationEnabled;
}
export class VersionModuleModel {
    /**
     * @param {?=} obj
     */
    constructor(obj) {
        if (obj) {
            this.id = obj.id || null;
            this.title = obj.title || null;
            this.description = obj.description || null;
            this.version = obj.version || null;
            this.installDate = obj.installDate || null;
            this.installState = obj.installState || null;
            this.versionMin = obj.versionMin || null;
            this.versionMax = obj.versionMax || null;
        }
    }
}
if (false) {
    /** @type {?} */
    VersionModuleModel.prototype.id;
    /** @type {?} */
    VersionModuleModel.prototype.title;
    /** @type {?} */
    VersionModuleModel.prototype.description;
    /** @type {?} */
    VersionModuleModel.prototype.version;
    /** @type {?} */
    VersionModuleModel.prototype.installDate;
    /** @type {?} */
    VersionModuleModel.prototype.installState;
    /** @type {?} */
    VersionModuleModel.prototype.versionMin;
    /** @type {?} */
    VersionModuleModel.prototype.versionMax;
}
export class EcmProductVersionModel {
    /**
     * @param {?=} obj
     */
    constructor(obj) {
        this.modules = [];
        if (obj && obj.entry && obj.entry.repository) {
            this.edition = obj.entry.repository.edition || null;
            this.version = new VersionModel(obj.entry.repository.version);
            this.license = new LicenseModel(obj.entry.repository.license);
            this.status = new VersionStatusModel(obj.entry.repository.status);
            if (obj.entry.repository.modules) {
                obj.entry.repository.modules.forEach((/**
                 * @param {?} module
                 * @return {?}
                 */
                (module) => {
                    this.modules.push(new VersionModuleModel(module));
                }));
            }
        }
    }
}
if (false) {
    /** @type {?} */
    EcmProductVersionModel.prototype.edition;
    /** @type {?} */
    EcmProductVersionModel.prototype.version;
    /** @type {?} */
    EcmProductVersionModel.prototype.license;
    /** @type {?} */
    EcmProductVersionModel.prototype.status;
    /** @type {?} */
    EcmProductVersionModel.prototype.modules;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHJvZHVjdC12ZXJzaW9uLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsibW9kZWxzL3Byb2R1Y3QtdmVyc2lvbi5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxNQUFNLE9BQU8sc0JBQXNCOzs7O0lBTy9CLFlBQVksR0FBUztRQUNqQixJQUFJLEdBQUcsRUFBRTtZQUNMLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUM7WUFDbkMsSUFBSSxDQUFDLFlBQVksR0FBRyxHQUFHLENBQUMsWUFBWSxJQUFJLElBQUksQ0FBQztZQUM3QyxJQUFJLENBQUMsZUFBZSxHQUFHLEdBQUcsQ0FBQyxlQUFlLElBQUksSUFBSSxDQUFDO1lBQ25ELElBQUksQ0FBQyxZQUFZLEdBQUcsR0FBRyxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUM7WUFDN0MsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQztTQUNoQztJQUNMLENBQUM7Q0FDSjs7O0lBZkcseUNBQWdCOztJQUNoQiw4Q0FBcUI7O0lBQ3JCLGlEQUF3Qjs7SUFDeEIsOENBQXFCOztJQUNyQixzQ0FBYTs7QUFhakIsTUFBTSxPQUFPLFlBQVk7Ozs7SUFTckIsWUFBWSxHQUFTO1FBQ2pCLElBQUksR0FBRyxFQUFFO1lBQ0wsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQztZQUMvQixJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDO1lBQy9CLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUM7WUFDL0IsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQztZQUNqQyxJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDO1lBQ2pDLElBQUksQ0FBQyxLQUFLLEdBQUcsR0FBRyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUM7WUFDL0IsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQztTQUN0QztJQUNMLENBQUM7Q0FDSjs7O0lBbkJHLDZCQUFjOztJQUNkLDZCQUFjOztJQUNkLDZCQUFjOztJQUNkLDhCQUFlOztJQUNmLDhCQUFlOztJQUNmLDZCQUFjOztJQUNkLCtCQUFnQjs7QUFlcEIsTUFBTSxPQUFPLFlBQVk7Ozs7SUFTckIsWUFBWSxHQUFTO1FBQ2pCLElBQUksR0FBRyxFQUFFO1lBQ0wsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQztZQUNyQyxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxhQUFhLEdBQUcsR0FBRyxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUM7WUFDL0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQztZQUNqQyxJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDO1lBQzdCLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQzVELElBQUksQ0FBQyxrQkFBa0IsR0FBRyxHQUFHLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1NBQ25FO0lBQ0wsQ0FBQztDQUNKOzs7SUFuQkcsZ0NBQWlCOztJQUNqQixpQ0FBa0I7O0lBQ2xCLHFDQUFzQjs7SUFDdEIsOEJBQWU7O0lBQ2YsNEJBQWE7O0lBQ2Isd0NBQTBCOztJQUMxQiwwQ0FBNEI7O0FBZWhDLE1BQU0sT0FBTyxrQkFBa0I7Ozs7SUFNM0IsWUFBWSxHQUFTO1FBQ2pCLElBQUksR0FBRyxFQUFFO1lBQ0wsSUFBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUNoRCxJQUFJLENBQUMsY0FBYyxHQUFHLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQ3hELElBQUksQ0FBQyxtQkFBbUIsR0FBRyxHQUFHLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQ2xFLElBQUksQ0FBQyw0QkFBNEIsR0FBRyxHQUFHLENBQUMsNEJBQTRCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1NBQ3ZGO0lBQ0wsQ0FBQztDQUNKOzs7SUFiRyx3Q0FBb0I7O0lBQ3BCLDRDQUF3Qjs7SUFDeEIsaURBQTZCOztJQUM3QiwwREFBc0M7O0FBWTFDLE1BQU0sT0FBTyxrQkFBa0I7Ozs7SUFVM0IsWUFBWSxHQUFTO1FBQ2pCLElBQUksR0FBRyxFQUFFO1lBQ0wsSUFBSSxDQUFDLEVBQUUsR0FBRyxHQUFHLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQztZQUN6QixJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDO1lBQy9CLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUM7WUFDM0MsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQztZQUNuQyxJQUFJLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDO1lBQzNDLElBQUksQ0FBQyxZQUFZLEdBQUcsR0FBRyxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUM7WUFDN0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQztZQUN6QyxJQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDO1NBQzVDO0lBQ0wsQ0FBQztDQUNKOzs7SUFyQkcsZ0NBQVc7O0lBQ1gsbUNBQWM7O0lBQ2QseUNBQW9COztJQUNwQixxQ0FBZ0I7O0lBQ2hCLHlDQUFvQjs7SUFDcEIsMENBQXFCOztJQUNyQix3Q0FBbUI7O0lBQ25CLHdDQUFtQjs7QUFnQnZCLE1BQU0sT0FBTyxzQkFBc0I7Ozs7SUFPL0IsWUFBWSxHQUFTO1FBRnJCLFlBQU8sR0FBeUIsRUFBRSxDQUFDO1FBRy9CLElBQUksR0FBRyxJQUFJLEdBQUcsQ0FBQyxLQUFLLElBQUksR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQUU7WUFDMUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDO1lBQ3BELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxZQUFZLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDOUQsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLFlBQVksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztZQUM5RCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksa0JBQWtCLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDbEUsSUFBSSxHQUFHLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUU7Z0JBQzlCLEdBQUcsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxPQUFPOzs7O2dCQUFDLENBQUMsTUFBTSxFQUFFLEVBQUU7b0JBQzVDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksa0JBQWtCLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDdEQsQ0FBQyxFQUFDLENBQUM7YUFDTjtTQUNKO0lBQ0wsQ0FBQztDQUVKOzs7SUFwQkcseUNBQWdCOztJQUNoQix5Q0FBc0I7O0lBQ3RCLHlDQUFzQjs7SUFDdEIsd0NBQTJCOztJQUMzQix5Q0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuZXhwb3J0IGNsYXNzIEJwbVByb2R1Y3RWZXJzaW9uTW9kZWwge1xyXG4gICAgZWRpdGlvbjogc3RyaW5nO1xyXG4gICAgbWFqb3JWZXJzaW9uOiBzdHJpbmc7XHJcbiAgICByZXZpc2lvblZlcnNpb246IHN0cmluZztcclxuICAgIG1pbm9yVmVyc2lvbjogc3RyaW5nO1xyXG4gICAgdHlwZTogc3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKG9iaj86IGFueSkge1xyXG4gICAgICAgIGlmIChvYmopIHtcclxuICAgICAgICAgICAgdGhpcy5lZGl0aW9uID0gb2JqLmVkaXRpb24gfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5tYWpvclZlcnNpb24gPSBvYmoubWFqb3JWZXJzaW9uIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMucmV2aXNpb25WZXJzaW9uID0gb2JqLnJldmlzaW9uVmVyc2lvbiB8fCBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLm1pbm9yVmVyc2lvbiA9IG9iai5taW5vclZlcnNpb24gfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy50eXBlID0gb2JqLnR5cGUgfHwgbnVsbDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBWZXJzaW9uTW9kZWwge1xyXG4gICAgbWFqb3I6IHN0cmluZztcclxuICAgIG1pbm9yOiBzdHJpbmc7XHJcbiAgICBwYXRjaDogc3RyaW5nO1xyXG4gICAgaG90Zml4OiBzdHJpbmc7XHJcbiAgICBzY2hlbWE6IG51bWJlcjtcclxuICAgIGxhYmVsOiBzdHJpbmc7XHJcbiAgICBkaXNwbGF5OiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3Iob2JqPzogYW55KSB7XHJcbiAgICAgICAgaWYgKG9iaikge1xyXG4gICAgICAgICAgICB0aGlzLm1ham9yID0gb2JqLm1ham9yIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMubWlub3IgPSBvYmoubWlub3IgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5wYXRjaCA9IG9iai5wYXRjaCB8fCBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLmhvdGZpeCA9IG9iai5ob3RmaXggfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5zY2hlbWEgPSBvYmouc2NoZW1hIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMubGFiZWwgPSBvYmoubGFiZWwgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5kaXNwbGF5ID0gb2JqLmRpc3BsYXkgfHwgbnVsbDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBMaWNlbnNlTW9kZWwge1xyXG4gICAgaXNzdWVkQXQ6IHN0cmluZztcclxuICAgIGV4cGlyZXNBdDogc3RyaW5nO1xyXG4gICAgcmVtYWluaW5nRGF5czogbnVtYmVyO1xyXG4gICAgaG9sZGVyOiBzdHJpbmc7XHJcbiAgICBtb2RlOiBzdHJpbmc7XHJcbiAgICBpc0NsdXN0ZXJFbmFibGVkOiBib29sZWFuO1xyXG4gICAgaXNDcnlwdG9kb2NFbmFibGVkOiBib29sZWFuO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKG9iaj86IGFueSkge1xyXG4gICAgICAgIGlmIChvYmopIHtcclxuICAgICAgICAgICAgdGhpcy5pc3N1ZWRBdCA9IG9iai5pc3N1ZWRBdCB8fCBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLmV4cGlyZXNBdCA9IG9iai5leHBpcmVzQXQgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5yZW1haW5pbmdEYXlzID0gb2JqLnJlbWFpbmluZ0RheXMgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5ob2xkZXIgPSBvYmouaG9sZGVyIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMubW9kZSA9IG9iai5tb2RlIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuaXNDbHVzdGVyRW5hYmxlZCA9IG9iai5pc0NsdXN0ZXJFbmFibGVkID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmlzQ3J5cHRvZG9jRW5hYmxlZCA9IG9iai5pc0NyeXB0b2RvY0VuYWJsZWQgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgVmVyc2lvblN0YXR1c01vZGVsIHtcclxuICAgIGlzUmVhZE9ubHk6IGJvb2xlYW47XHJcbiAgICBpc0F1ZGl0RW5hYmxlZDogYm9vbGVhbjtcclxuICAgIGlzUXVpY2tTaGFyZUVuYWJsZWQ6IGJvb2xlYW47XHJcbiAgICBpc1RodW1ibmFpbEdlbmVyYXRpb25FbmFibGVkOiBib29sZWFuO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKG9iaj86IGFueSkge1xyXG4gICAgICAgIGlmIChvYmopIHtcclxuICAgICAgICAgICAgdGhpcy5pc1JlYWRPbmx5ID0gb2JqLmlzUmVhZE9ubHkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuaXNBdWRpdEVuYWJsZWQgPSBvYmouaXNBdWRpdEVuYWJsZWQgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMuaXNRdWlja1NoYXJlRW5hYmxlZCA9IG9iai5pc1F1aWNrU2hhcmVFbmFibGVkID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmlzVGh1bWJuYWlsR2VuZXJhdGlvbkVuYWJsZWQgPSBvYmouaXNUaHVtYm5haWxHZW5lcmF0aW9uRW5hYmxlZCA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBWZXJzaW9uTW9kdWxlTW9kZWwge1xyXG4gICAgaWQ6IHN0cmluZztcclxuICAgIHRpdGxlOiBzdHJpbmc7XHJcbiAgICBkZXNjcmlwdGlvbjogc3RyaW5nO1xyXG4gICAgdmVyc2lvbjogc3RyaW5nO1xyXG4gICAgaW5zdGFsbERhdGU6IHN0cmluZztcclxuICAgIGluc3RhbGxTdGF0ZTogc3RyaW5nO1xyXG4gICAgdmVyc2lvbk1pbjogc3RyaW5nO1xyXG4gICAgdmVyc2lvbk1heDogc3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKG9iaj86IGFueSkge1xyXG4gICAgICAgIGlmIChvYmopIHtcclxuICAgICAgICAgICAgdGhpcy5pZCA9IG9iai5pZCB8fCBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLnRpdGxlID0gb2JqLnRpdGxlIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSBvYmouZGVzY3JpcHRpb24gfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy52ZXJzaW9uID0gb2JqLnZlcnNpb24gfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5pbnN0YWxsRGF0ZSA9IG9iai5pbnN0YWxsRGF0ZSB8fCBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLmluc3RhbGxTdGF0ZSA9IG9iai5pbnN0YWxsU3RhdGUgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy52ZXJzaW9uTWluID0gb2JqLnZlcnNpb25NaW4gfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy52ZXJzaW9uTWF4ID0gb2JqLnZlcnNpb25NYXggfHwgbnVsbDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBFY21Qcm9kdWN0VmVyc2lvbk1vZGVsIHtcclxuICAgIGVkaXRpb246IHN0cmluZztcclxuICAgIHZlcnNpb246IFZlcnNpb25Nb2RlbDtcclxuICAgIGxpY2Vuc2U6IExpY2Vuc2VNb2RlbDtcclxuICAgIHN0YXR1czogVmVyc2lvblN0YXR1c01vZGVsO1xyXG4gICAgbW9kdWxlczogVmVyc2lvbk1vZHVsZU1vZGVsW10gPSBbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihvYmo/OiBhbnkpIHtcclxuICAgICAgICBpZiAob2JqICYmIG9iai5lbnRyeSAmJiBvYmouZW50cnkucmVwb3NpdG9yeSkge1xyXG4gICAgICAgICAgICB0aGlzLmVkaXRpb24gPSBvYmouZW50cnkucmVwb3NpdG9yeS5lZGl0aW9uIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMudmVyc2lvbiA9IG5ldyBWZXJzaW9uTW9kZWwob2JqLmVudHJ5LnJlcG9zaXRvcnkudmVyc2lvbik7XHJcbiAgICAgICAgICAgIHRoaXMubGljZW5zZSA9IG5ldyBMaWNlbnNlTW9kZWwob2JqLmVudHJ5LnJlcG9zaXRvcnkubGljZW5zZSk7XHJcbiAgICAgICAgICAgIHRoaXMuc3RhdHVzID0gbmV3IFZlcnNpb25TdGF0dXNNb2RlbChvYmouZW50cnkucmVwb3NpdG9yeS5zdGF0dXMpO1xyXG4gICAgICAgICAgICBpZiAob2JqLmVudHJ5LnJlcG9zaXRvcnkubW9kdWxlcykge1xyXG4gICAgICAgICAgICAgICAgb2JqLmVudHJ5LnJlcG9zaXRvcnkubW9kdWxlcy5mb3JFYWNoKChtb2R1bGUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm1vZHVsZXMucHVzaChuZXcgVmVyc2lvbk1vZHVsZU1vZGVsKG1vZHVsZSkpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==