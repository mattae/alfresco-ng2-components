/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export { FileUploadOptions, FileUploadStatus, FileModel } from './file.model';
export { AllowableOperationsEnum } from './allowable-operations.enum';
export { PermissionsEnum } from './permissions.enum';
export { BpmProductVersionModel, VersionModel, LicenseModel, VersionStatusModel, VersionModuleModel, EcmProductVersionModel } from './product-version.model';
export { UserProcessModel } from './user-process.model';
export { CommentModel } from './comment.model';
export { EcmCompanyModel } from './ecm-company.model';
export { RedirectionModel } from './redirection.model';
export { PaginationModel } from './pagination.model';
export {} from './oauth-config.model';
export { RequestPaginationModel } from './request-pagination.model';
export {} from './notification.model';
export { DecimalNumberModel } from './decimal-number.model';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vZGVscy9wdWJsaWMtYXBpLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLCtEQUFjLGNBQWMsQ0FBQztBQUM3Qix3Q0FBYyw2QkFBNkIsQ0FBQztBQUM1QyxnQ0FBYyxvQkFBb0IsQ0FBQztBQUNuQyxtSUFBYyx5QkFBeUIsQ0FBQztBQUN4QyxpQ0FBYyxzQkFBc0IsQ0FBQztBQUNyQyw2QkFBYyxpQkFBaUIsQ0FBQztBQUNoQyxnQ0FBYyxxQkFBcUIsQ0FBQztBQUNwQyxpQ0FBYyxxQkFBcUIsQ0FBQztBQUNwQyxnQ0FBYyxvQkFBb0IsQ0FBQztBQUNuQyxlQUFjLHNCQUFzQixDQUFDO0FBQ3JDLHVDQUFjLDRCQUE0QixDQUFDO0FBQzNDLGVBQWMsc0JBQXNCLENBQUM7QUFDckMsbUNBQWMsd0JBQXdCLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuZXhwb3J0ICogZnJvbSAnLi9maWxlLm1vZGVsJztcclxuZXhwb3J0ICogZnJvbSAnLi9hbGxvd2FibGUtb3BlcmF0aW9ucy5lbnVtJztcclxuZXhwb3J0ICogZnJvbSAnLi9wZXJtaXNzaW9ucy5lbnVtJztcclxuZXhwb3J0ICogZnJvbSAnLi9wcm9kdWN0LXZlcnNpb24ubW9kZWwnO1xyXG5leHBvcnQgKiBmcm9tICcuL3VzZXItcHJvY2Vzcy5tb2RlbCc7XHJcbmV4cG9ydCAqIGZyb20gJy4vY29tbWVudC5tb2RlbCc7XHJcbmV4cG9ydCAqIGZyb20gJy4vZWNtLWNvbXBhbnkubW9kZWwnO1xyXG5leHBvcnQgKiBmcm9tICcuL3JlZGlyZWN0aW9uLm1vZGVsJztcclxuZXhwb3J0ICogZnJvbSAnLi9wYWdpbmF0aW9uLm1vZGVsJztcclxuZXhwb3J0ICogZnJvbSAnLi9vYXV0aC1jb25maWcubW9kZWwnO1xyXG5leHBvcnQgKiBmcm9tICcuL3JlcXVlc3QtcGFnaW5hdGlvbi5tb2RlbCc7XHJcbmV4cG9ydCAqIGZyb20gJy4vbm90aWZpY2F0aW9uLm1vZGVsJztcclxuZXhwb3J0ICogZnJvbSAnLi9kZWNpbWFsLW51bWJlci5tb2RlbCc7XHJcbiJdfQ==