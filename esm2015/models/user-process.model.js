/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * This object represent the process service user.*
 */
export class UserProcessModel {
    /**
     * @param {?=} input
     */
    constructor(input) {
        if (input) {
            this.id = input.id;
            this.email = input.email || null;
            this.firstName = input.firstName || null;
            this.lastName = input.lastName || null;
            this.pictureId = input.pictureId || null;
            this.externalId = input.externalId || null;
        }
    }
}
if (false) {
    /** @type {?} */
    UserProcessModel.prototype.id;
    /** @type {?} */
    UserProcessModel.prototype.email;
    /** @type {?} */
    UserProcessModel.prototype.firstName;
    /** @type {?} */
    UserProcessModel.prototype.lastName;
    /** @type {?} */
    UserProcessModel.prototype.pictureId;
    /** @type {?} */
    UserProcessModel.prototype.externalId;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1wcm9jZXNzLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsibW9kZWxzL3VzZXItcHJvY2Vzcy5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXVCQSxNQUFNLE9BQU8sZ0JBQWdCOzs7O0lBUXpCLFlBQVksS0FBVztRQUNuQixJQUFJLEtBQUssRUFBRTtZQUNQLElBQUksQ0FBQyxFQUFFLEdBQUcsS0FBSyxDQUFDLEVBQUUsQ0FBQztZQUNuQixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDO1lBQ2pDLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUM7WUFDekMsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQztZQUN2QyxJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDO1lBQ3pDLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUM7U0FDOUM7SUFDTCxDQUFDO0NBRUo7OztJQWxCRyw4QkFBWTs7SUFDWixpQ0FBZTs7SUFDZixxQ0FBbUI7O0lBQ25CLG9DQUFrQjs7SUFDbEIscUNBQW1COztJQUNuQixzQ0FBb0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyoqXHJcbiAqIFRoaXMgb2JqZWN0IHJlcHJlc2VudCB0aGUgcHJvY2VzcyBzZXJ2aWNlIHVzZXIuKlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IExpZ2h0VXNlclJlcHJlc2VudGF0aW9uIH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcblxyXG5leHBvcnQgY2xhc3MgVXNlclByb2Nlc3NNb2RlbCBpbXBsZW1lbnRzIExpZ2h0VXNlclJlcHJlc2VudGF0aW9uIHtcclxuICAgIGlkPzogbnVtYmVyO1xyXG4gICAgZW1haWw/OiBzdHJpbmc7XHJcbiAgICBmaXJzdE5hbWU/OiBzdHJpbmc7XHJcbiAgICBsYXN0TmFtZT86IHN0cmluZztcclxuICAgIHBpY3R1cmVJZD86IG51bWJlcjtcclxuICAgIGV4dGVybmFsSWQ/OiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IoaW5wdXQ/OiBhbnkpIHtcclxuICAgICAgICBpZiAoaW5wdXQpIHtcclxuICAgICAgICAgICAgdGhpcy5pZCA9IGlucHV0LmlkO1xyXG4gICAgICAgICAgICB0aGlzLmVtYWlsID0gaW5wdXQuZW1haWwgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5maXJzdE5hbWUgPSBpbnB1dC5maXJzdE5hbWUgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5sYXN0TmFtZSA9IGlucHV0Lmxhc3ROYW1lIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMucGljdHVyZUlkID0gaW5wdXQucGljdHVyZUlkIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuZXh0ZXJuYWxJZCA9IGlucHV0LmV4dGVybmFsSWQgfHwgbnVsbDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==