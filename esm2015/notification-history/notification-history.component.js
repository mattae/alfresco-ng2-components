/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewChild } from '@angular/core';
import { NotificationService } from '../services/notification.service';
import { MatMenuTrigger } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
export class NotificationHistoryComponent {
    /**
     * @param {?} notificationService
     */
    constructor(notificationService) {
        this.notificationService = notificationService;
        this.onDestroy$ = new Subject();
        this.notifications = [];
        /**
         * Custom choice for opening the menu at the bottom. Can be `before` or `after`.
         */
        this.menuPositionX = 'after';
        /**
         * Custom choice for opening the menu at the bottom. Can be `above` or `below`.
         */
        this.menuPositionY = 'below';
        this.notificationService.messages
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} message
         * @return {?}
         */
        (message) => {
            this.notifications.push(message);
        }));
    }
    /**
     * @return {?}
     */
    isEmptyNotification() {
        return (!this.notifications || this.notifications.length === 0);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeyPress(event) {
        this.closeUserModal(event);
    }
    /**
     * @return {?}
     */
    markAsRead() {
        this.notifications = [];
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    }
    /**
     * @private
     * @param {?} $event
     * @return {?}
     */
    closeUserModal($event) {
        if ($event.keyCode === 27) {
            this.trigger.closeMenu();
        }
    }
}
NotificationHistoryComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-notification-history',
                template: "<div (keyup)=\"onKeyPress($event)\">\r\n    <button mat-button [matMenuTriggerFor]=\"menu\" class=\"adf-notification-history-menu_button\"\r\n            id=\"adf-notification-history-open-button\">\r\n        <mat-icon>mail_outline</mat-icon>\r\n    </button>\r\n    <mat-menu #menu=\"matMenu\" [xPosition]=\"menuPositionX\" [yPosition]=\"menuPositionY\"\r\n              [overlapTrigger]=\"false\" id=\"adf-notification-history-menu\" class=\"adf-notification-history-menu\">\r\n\r\n        <div id=\"adf-notification-history-list\">\r\n            <mat-list>\r\n                <mat-list-item>\r\n                    <h6 mat-line>{{ 'NOTIFICATION_HISTORY.NOTIFICATIONS' | translate }}</h6>\r\n                </mat-list-item>\r\n            </mat-list>\r\n            <mat-divider></mat-divider>\r\n\r\n            <mat-list>\r\n                <mat-list-item *ngFor=\"let notification of notifications\">\r\n                    <mat-icon mat-list-icon>{{notification.info? notification.info: 'info'}}</mat-icon>\r\n                    <h4 mat-line>{{notification.message}}</h4>\r\n                    <p mat-line> {{notification.dateTime | date}} </p>\r\n                </mat-list-item>\r\n                <mat-list-item *ngIf=\"isEmptyNotification()\" id=\"adf-notification-history-component-no-message\">\r\n                    <h4 mat-line>{{ 'NOTIFICATION_HISTORY.NO_MESSAGE' | translate }}</h4>\r\n                </mat-list-item>\r\n                <mat-action-list *ngIf=\"!isEmptyNotification()\" id=\"adf-notification-history-mark-as-read\">\r\n                    <button mat-list-item (click)=\"markAsRead()\">{{ 'NOTIFICATION_HISTORY.MARK_AS_READ' | translate }}\r\n                    </button>\r\n                </mat-action-list>\r\n            </mat-list>\r\n        </div>\r\n    </mat-menu>\r\n</div>\r\n",
                styles: [".adf-notification-history-menu_button.mat-button{margin-right:0;border-radius:90%;padding:0;min-width:40px;height:40px}@media only screen and (min-device-width:480px){.mat-menu-panel.adf-notification-history-menu{max-height:450px;min-width:450px;overflow:auto;padding:0}}.mat-menu-panel.adf-notification-history-menu .mat-menu-content{padding:0}"]
            }] }
];
/** @nocollapse */
NotificationHistoryComponent.ctorParameters = () => [
    { type: NotificationService }
];
NotificationHistoryComponent.propDecorators = {
    trigger: [{ type: ViewChild, args: [MatMenuTrigger, { static: true },] }],
    menuPositionX: [{ type: Input }],
    menuPositionY: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    NotificationHistoryComponent.prototype.onDestroy$;
    /** @type {?} */
    NotificationHistoryComponent.prototype.notifications;
    /** @type {?} */
    NotificationHistoryComponent.prototype.trigger;
    /**
     * Custom choice for opening the menu at the bottom. Can be `before` or `after`.
     * @type {?}
     */
    NotificationHistoryComponent.prototype.menuPositionX;
    /**
     * Custom choice for opening the menu at the bottom. Can be `above` or `below`.
     * @type {?}
     */
    NotificationHistoryComponent.prototype.menuPositionY;
    /**
     * @type {?}
     * @private
     */
    NotificationHistoryComponent.prototype.notificationService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLWhpc3RvcnkuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsibm90aWZpY2F0aW9uLWhpc3Rvcnkvbm90aWZpY2F0aW9uLWhpc3RvcnkuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFFdkUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBTy9CLE1BQU0sT0FBTyw0QkFBNEI7Ozs7SUFpQnJDLFlBQ1ksbUJBQXdDO1FBQXhDLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFoQnBELGVBQVUsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDO1FBRXBDLGtCQUFhLEdBQXdCLEVBQUUsQ0FBQzs7OztRQU94QyxrQkFBYSxHQUFXLE9BQU8sQ0FBQzs7OztRQUloQyxrQkFBYSxHQUFXLE9BQU8sQ0FBQztRQUk1QixJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUTthQUM1QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNoQyxTQUFTOzs7O1FBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUN2QixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNyQyxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFRCxtQkFBbUI7UUFDZixPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDO0lBQ3BFLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLEtBQW9CO1FBQzNCLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDL0IsQ0FBQzs7OztJQUVELFVBQVU7UUFDTixJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQsV0FBVztRQUNQLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDL0IsQ0FBQzs7Ozs7O0lBRU8sY0FBYyxDQUFDLE1BQXFCO1FBQ3hDLElBQUksTUFBTSxDQUFDLE9BQU8sS0FBSyxFQUFFLEVBQUU7WUFDdkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUUsQ0FBQztTQUM1QjtJQUNMLENBQUM7OztZQXBESixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLDBCQUEwQjtnQkFFcEMsZ3pEQUFrRDs7YUFDckQ7Ozs7WUFWUSxtQkFBbUI7OztzQkFpQnZCLFNBQVMsU0FBQyxjQUFjLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDOzRCQUl4QyxLQUFLOzRCQUlMLEtBQUs7Ozs7SUFaTixrREFBb0M7O0lBRXBDLHFEQUF3Qzs7SUFFeEMsK0NBQ3dCOzs7OztJQUd4QixxREFDZ0M7Ozs7O0lBR2hDLHFEQUNnQzs7Ozs7SUFHNUIsMkRBQWdEIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXG5cbi8qIVxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXG4gKlxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuICpcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbiAqXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxuICovXG5cbmltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIFZpZXdDaGlsZCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOb3RpZmljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvbm90aWZpY2F0aW9uLnNlcnZpY2UnO1xuaW1wb3J0IHsgTm90aWZpY2F0aW9uTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvbm90aWZpY2F0aW9uLm1vZGVsJztcbmltcG9ydCB7IE1hdE1lbnVUcmlnZ2VyIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2FkZi1ub3RpZmljYXRpb24taGlzdG9yeScsXG4gICAgc3R5bGVVcmxzOiBbJ25vdGlmaWNhdGlvbi1oaXN0b3J5LmNvbXBvbmVudC5zY3NzJ10sXG4gICAgdGVtcGxhdGVVcmw6ICdub3RpZmljYXRpb24taGlzdG9yeS5jb21wb25lbnQuaHRtbCdcbn0pXG5leHBvcnQgY2xhc3MgTm90aWZpY2F0aW9uSGlzdG9yeUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uRGVzdHJveSB7XG5cbiAgICBvbkRlc3Ryb3kkID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcblxuICAgIG5vdGlmaWNhdGlvbnM6IE5vdGlmaWNhdGlvbk1vZGVsW10gPSBbXTtcblxuICAgIEBWaWV3Q2hpbGQoTWF0TWVudVRyaWdnZXIsIHtzdGF0aWM6IHRydWV9KVxuICAgIHRyaWdnZXI6IE1hdE1lbnVUcmlnZ2VyO1xuXG4gICAgLyoqIEN1c3RvbSBjaG9pY2UgZm9yIG9wZW5pbmcgdGhlIG1lbnUgYXQgdGhlIGJvdHRvbS4gQ2FuIGJlIGBiZWZvcmVgIG9yIGBhZnRlcmAuICovXG4gICAgQElucHV0KClcbiAgICBtZW51UG9zaXRpb25YOiBzdHJpbmcgPSAnYWZ0ZXInO1xuXG4gICAgLyoqIEN1c3RvbSBjaG9pY2UgZm9yIG9wZW5pbmcgdGhlIG1lbnUgYXQgdGhlIGJvdHRvbS4gQ2FuIGJlIGBhYm92ZWAgb3IgYGJlbG93YC4gKi9cbiAgICBASW5wdXQoKVxuICAgIG1lbnVQb3NpdGlvblk6IHN0cmluZyA9ICdiZWxvdyc7XG5cbiAgICBjb25zdHJ1Y3RvcihcbiAgICAgICAgcHJpdmF0ZSBub3RpZmljYXRpb25TZXJ2aWNlOiBOb3RpZmljYXRpb25TZXJ2aWNlKSB7XG4gICAgICAgIHRoaXMubm90aWZpY2F0aW9uU2VydmljZS5tZXNzYWdlc1xuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMub25EZXN0cm95JCkpXG4gICAgICAgICAgICAuc3Vic2NyaWJlKChtZXNzYWdlKSA9PiB7XG4gICAgICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbnMucHVzaChtZXNzYWdlKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgaXNFbXB0eU5vdGlmaWNhdGlvbigpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuICghdGhpcy5ub3RpZmljYXRpb25zIHx8IHRoaXMubm90aWZpY2F0aW9ucy5sZW5ndGggPT09IDApO1xuICAgIH1cblxuICAgIG9uS2V5UHJlc3MoZXZlbnQ6IEtleWJvYXJkRXZlbnQpIHtcbiAgICAgICAgdGhpcy5jbG9zZVVzZXJNb2RhbChldmVudCk7XG4gICAgfVxuXG4gICAgbWFya0FzUmVhZCgpIHtcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25zID0gW107XG4gICAgfVxuXG4gICAgbmdPbkRlc3Ryb3koKSB7XG4gICAgICAgIHRoaXMub25EZXN0cm95JC5uZXh0KHRydWUpO1xuICAgICAgICB0aGlzLm9uRGVzdHJveSQuY29tcGxldGUoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGNsb3NlVXNlck1vZGFsKCRldmVudDogS2V5Ym9hcmRFdmVudCkge1xuICAgICAgICBpZiAoJGV2ZW50LmtleUNvZGUgPT09IDI3KSB7XG4gICAgICAgICAgICB0aGlzLnRyaWdnZXIuY2xvc2VNZW51KCk7XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=