/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation, ChangeDetectorRef, HostBinding } from '@angular/core';
import { Pagination } from '@alfresco/js-api';
import { Subject } from 'rxjs';
import { PaginationModel } from '../models/pagination.model';
import { UserPreferencesService, UserPreferenceValues } from '../services/user-preferences.service';
import { takeUntil } from 'rxjs/operators';
export class PaginationComponent {
    /**
     * @param {?} cdr
     * @param {?} userPreferencesService
     */
    constructor(cdr, userPreferencesService) {
        this.cdr = cdr;
        this.userPreferencesService = userPreferencesService;
        /**
         * Pagination object.
         */
        this.pagination = PaginationComponent.DEFAULT_PAGINATION;
        /**
         * Emitted when pagination changes in any way.
         */
        this.change = new EventEmitter();
        /**
         * Emitted when the page number changes.
         */
        this.changePageNumber = new EventEmitter();
        /**
         * Emitted when the page size changes.
         */
        this.changePageSize = new EventEmitter();
        /**
         * Emitted when the next page is requested.
         */
        this.nextPage = new EventEmitter();
        /**
         * Emitted when the previous page is requested.
         */
        this.prevPage = new EventEmitter();
        this.onDestroy$ = new Subject();
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.userPreferencesService
            .select(UserPreferenceValues.PaginationSize)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} pagSize
         * @return {?}
         */
        pagSize => this.pagination.maxItems = pagSize));
        if (!this.supportedPageSizes) {
            this.supportedPageSizes = this.userPreferencesService.supportedPageSizes;
        }
        if (this.target) {
            this.target.pagination
                .pipe(takeUntil(this.onDestroy$))
                .subscribe((/**
             * @param {?} pagination
             * @return {?}
             */
            pagination => {
                if (pagination.count === 0 && !this.isFirstPage) {
                    this.goPrevious();
                }
                this.pagination = pagination;
                this.cdr.detectChanges();
            }));
        }
        if (!this.pagination) {
            this.pagination = PaginationComponent.DEFAULT_PAGINATION;
        }
    }
    /**
     * @return {?}
     */
    get lastPage() {
        const { maxItems, totalItems } = this.pagination;
        return (totalItems && maxItems)
            ? Math.ceil(totalItems / maxItems)
            : 1;
    }
    /**
     * @return {?}
     */
    get current() {
        const { maxItems, skipCount } = this.pagination;
        return (skipCount && maxItems)
            ? Math.floor(skipCount / maxItems) + 1
            : 1;
    }
    /**
     * @return {?}
     */
    get isLastPage() {
        return this.current === this.lastPage;
    }
    /**
     * @return {?}
     */
    get isFirstPage() {
        return this.current === 1;
    }
    /**
     * @return {?}
     */
    get next() {
        return this.isLastPage ? this.current : this.current + 1;
    }
    /**
     * @return {?}
     */
    get previous() {
        return this.isFirstPage ? 1 : this.current - 1;
    }
    /**
     * @return {?}
     */
    get hasItems() {
        return this.pagination && this.pagination.count > 0;
    }
    /**
     * @return {?}
     */
    get isEmpty() {
        return !this.hasItems;
    }
    /**
     * @return {?}
     */
    get range() {
        const { skipCount, maxItems, totalItems } = this.pagination;
        const { isLastPage } = this;
        /** @type {?} */
        const start = totalItems ? skipCount + 1 : 0;
        /** @type {?} */
        const end = isLastPage ? totalItems : skipCount + maxItems;
        return [start, end];
    }
    /**
     * @return {?}
     */
    get pages() {
        return Array(this.lastPage)
            .fill('n')
            .map((/**
         * @param {?} item
         * @param {?} index
         * @return {?}
         */
        (item, index) => (index + 1)));
    }
    /**
     * @return {?}
     */
    goNext() {
        if (this.hasItems) {
            /** @type {?} */
            const maxItems = this.pagination.maxItems;
            /** @type {?} */
            const skipCount = (this.next - 1) * maxItems;
            this.pagination.skipCount = skipCount;
            this.handlePaginationEvent(PaginationComponent.ACTIONS.NEXT_PAGE, {
                skipCount,
                maxItems
            });
        }
    }
    /**
     * @return {?}
     */
    goPrevious() {
        if (this.hasItems) {
            /** @type {?} */
            const maxItems = this.pagination.maxItems;
            /** @type {?} */
            const skipCount = (this.previous - 1) * maxItems;
            this.pagination.skipCount = skipCount;
            this.handlePaginationEvent(PaginationComponent.ACTIONS.PREV_PAGE, {
                skipCount,
                maxItems
            });
        }
    }
    /**
     * @param {?} pageNumber
     * @return {?}
     */
    onChangePageNumber(pageNumber) {
        if (this.hasItems) {
            /** @type {?} */
            const maxItems = this.pagination.maxItems;
            /** @type {?} */
            const skipCount = (pageNumber - 1) * maxItems;
            this.pagination.skipCount = skipCount;
            this.handlePaginationEvent(PaginationComponent.ACTIONS.CHANGE_PAGE_NUMBER, {
                skipCount,
                maxItems
            });
        }
    }
    /**
     * @param {?} maxItems
     * @return {?}
     */
    onChangePageSize(maxItems) {
        this.pagination.skipCount = 0;
        this.userPreferencesService.paginationSize = maxItems;
        this.handlePaginationEvent(PaginationComponent.ACTIONS.CHANGE_PAGE_SIZE, {
            skipCount: 0,
            maxItems
        });
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    }
    /**
     * @param {?} action
     * @param {?} params
     * @return {?}
     */
    handlePaginationEvent(action, params) {
        const { NEXT_PAGE, PREV_PAGE, CHANGE_PAGE_NUMBER, CHANGE_PAGE_SIZE } = PaginationComponent.ACTIONS;
        const { change, changePageNumber, changePageSize, nextPage, prevPage, pagination } = this;
        /** @type {?} */
        const paginationModel = Object.assign({}, pagination, params);
        if (action === NEXT_PAGE) {
            nextPage.emit(paginationModel);
        }
        if (action === PREV_PAGE) {
            prevPage.emit(paginationModel);
        }
        if (action === CHANGE_PAGE_NUMBER) {
            changePageNumber.emit(paginationModel);
        }
        if (action === CHANGE_PAGE_SIZE) {
            changePageSize.emit(paginationModel);
        }
        change.emit(params);
        if (this.target) {
            this.target.updatePagination(params);
        }
    }
}
PaginationComponent.DEFAULT_PAGINATION = new Pagination({
    skipCount: 0,
    maxItems: 25,
    totalItems: 0
});
PaginationComponent.ACTIONS = {
    NEXT_PAGE: 'NEXT_PAGE',
    PREV_PAGE: 'PREV_PAGE',
    CHANGE_PAGE_SIZE: 'CHANGE_PAGE_SIZE',
    CHANGE_PAGE_NUMBER: 'CHANGE_PAGE_NUMBER'
};
PaginationComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-pagination',
                host: { 'class': 'adf-pagination' },
                template: "<ng-container *ngIf=\"hasItems\">\r\n    <div class=\"adf-pagination__block adf-pagination__range-block\">\r\n        <span class=\"adf-pagination__range\">\r\n            {{\r\n                'CORE.PAGINATION.ITEMS_RANGE' | translate: {\r\n                    range: range.join('-'),\r\n                    total: pagination.totalItems\r\n                }\r\n            }}\r\n        </span>\r\n    </div>\r\n\r\n    <div class=\"adf-pagination__block adf-pagination__perpage-block\">\r\n        <span>\r\n            {{ 'CORE.PAGINATION.ITEMS_PER_PAGE' | translate }}\r\n        </span>\r\n\r\n        <span class=\"adf-pagination__max-items\">\r\n            {{ pagination.maxItems }}\r\n        </span>\r\n\r\n        <button\r\n            mat-icon-button\r\n            [attr.aria-label]=\"'CORE.PAGINATION.ARIA.ITEMS_PER_PAGE' | translate\"\r\n            [matMenuTriggerFor]=\"pageSizeMenu\">\r\n            <mat-icon>arrow_drop_down</mat-icon>\r\n        </button>\r\n\r\n        <mat-menu #pageSizeMenu=\"matMenu\" class=\"adf-pagination__page-selector\">\r\n            <button\r\n                mat-menu-item\r\n                *ngFor=\"let pageSize of supportedPageSizes\"\r\n                (click)=\"onChangePageSize(pageSize)\">\r\n                {{ pageSize }}\r\n            </button>\r\n        </mat-menu>\r\n    </div>\r\n\r\n    <div class=\"adf-pagination__block adf-pagination__actualinfo-block\">\r\n        <span class=\"adf-pagination__current-page\">\r\n            {{ 'CORE.PAGINATION.CURRENT_PAGE' | translate: { number: current } }}\r\n        </span>\r\n\r\n        <button\r\n            mat-icon-button\r\n            data-automation-id=\"page-selector\"\r\n            [attr.aria-label]=\"'CORE.PAGINATION.ARIA.CURRENT_PAGE' | translate\"\r\n            [matMenuTriggerFor]=\"pagesMenu\"\r\n            *ngIf=\"pages.length > 1\">\r\n            <mat-icon>arrow_drop_down</mat-icon>\r\n        </button>\r\n\r\n        <span class=\"adf-pagination__total-pages\">\r\n            {{ 'CORE.PAGINATION.TOTAL_PAGES' | translate: { total: pages.length } }}\r\n        </span>\r\n\r\n        <mat-menu #pagesMenu=\"matMenu\" class=\"adf-pagination__page-selector\">\r\n            <button\r\n                mat-menu-item\r\n                *ngFor=\"let pageNumber of pages\"\r\n                (click)=\"onChangePageNumber(pageNumber)\">\r\n                {{ pageNumber }}\r\n            </button>\r\n        </mat-menu>\r\n    </div>\r\n\r\n    <div class=\"adf-pagination__block adf-pagination__controls-block\">\r\n        <button\r\n            class=\"adf-pagination__previous-button\"\r\n            mat-icon-button\r\n            [attr.aria-label]=\"'CORE.PAGINATION.ARIA.PREVIOUS_PAGE' | translate\"\r\n            [disabled]=\"isFirstPage\"\r\n            (click)=\"goPrevious()\">\r\n            <mat-icon>keyboard_arrow_left</mat-icon>\r\n        </button>\r\n\r\n        <button\r\n            class=\"adf-pagination__next-button\"\r\n            mat-icon-button\r\n            [attr.aria-label]=\"'CORE.PAGINATION.ARIA.NEXT_PAGE' | translate\"\r\n            [disabled]=\"isLastPage\"\r\n            (click)=\"goNext()\">\r\n            <mat-icon>keyboard_arrow_right</mat-icon>\r\n        </button>\r\n    </div>\r\n</ng-container>\r\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                encapsulation: ViewEncapsulation.None,
                styles: [""]
            }] }
];
/** @nocollapse */
PaginationComponent.ctorParameters = () => [
    { type: ChangeDetectorRef },
    { type: UserPreferencesService }
];
PaginationComponent.propDecorators = {
    target: [{ type: Input }],
    supportedPageSizes: [{ type: Input }],
    pagination: [{ type: Input }],
    change: [{ type: Output }],
    changePageNumber: [{ type: Output }],
    changePageSize: [{ type: Output }],
    nextPage: [{ type: Output }],
    prevPage: [{ type: Output }],
    isEmpty: [{ type: HostBinding, args: ['class.adf-pagination__empty',] }]
};
if (false) {
    /** @type {?} */
    PaginationComponent.DEFAULT_PAGINATION;
    /** @type {?} */
    PaginationComponent.ACTIONS;
    /**
     * Component that provides custom pagination support.
     * @type {?}
     */
    PaginationComponent.prototype.target;
    /**
     * An array of page sizes.
     * @type {?}
     */
    PaginationComponent.prototype.supportedPageSizes;
    /**
     * Pagination object.
     * @type {?}
     */
    PaginationComponent.prototype.pagination;
    /**
     * Emitted when pagination changes in any way.
     * @type {?}
     */
    PaginationComponent.prototype.change;
    /**
     * Emitted when the page number changes.
     * @type {?}
     */
    PaginationComponent.prototype.changePageNumber;
    /**
     * Emitted when the page size changes.
     * @type {?}
     */
    PaginationComponent.prototype.changePageSize;
    /**
     * Emitted when the next page is requested.
     * @type {?}
     */
    PaginationComponent.prototype.nextPage;
    /**
     * Emitted when the previous page is requested.
     * @type {?}
     */
    PaginationComponent.prototype.prevPage;
    /**
     * @type {?}
     * @private
     */
    PaginationComponent.prototype.onDestroy$;
    /**
     * @type {?}
     * @private
     */
    PaginationComponent.prototype.cdr;
    /**
     * @type {?}
     * @private
     */
    PaginationComponent.prototype.userPreferencesService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnaW5hdGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJwYWdpbmF0aW9uL3BhZ2luYXRpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFDSCx1QkFBdUIsRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBVSxNQUFNLEVBQUUsaUJBQWlCLEVBQzFGLGlCQUFpQixFQUFhLFdBQVcsRUFDNUMsTUFBTSxlQUFlLENBQUM7QUFFdkIsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRzlDLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzdELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ3BHLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQVUzQyxNQUFNLE9BQU8sbUJBQW1COzs7OztJQWlENUIsWUFBb0IsR0FBc0IsRUFBVSxzQkFBOEM7UUFBOUUsUUFBRyxHQUFILEdBQUcsQ0FBbUI7UUFBVSwyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCOzs7O1FBeEJsRyxlQUFVLEdBQW9CLG1CQUFtQixDQUFDLGtCQUFrQixDQUFDOzs7O1FBSXJFLFdBQU0sR0FBa0MsSUFBSSxZQUFZLEVBQW1CLENBQUM7Ozs7UUFJNUUscUJBQWdCLEdBQWtDLElBQUksWUFBWSxFQUFtQixDQUFDOzs7O1FBSXRGLG1CQUFjLEdBQWtDLElBQUksWUFBWSxFQUFtQixDQUFDOzs7O1FBSXBGLGFBQVEsR0FBa0MsSUFBSSxZQUFZLEVBQW1CLENBQUM7Ozs7UUFJOUUsYUFBUSxHQUFrQyxJQUFJLFlBQVksRUFBbUIsQ0FBQztRQUV0RSxlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztJQUc1QyxDQUFDOzs7O0lBRUQsUUFBUTtRQUNKLElBQUksQ0FBQyxzQkFBc0I7YUFDdEIsTUFBTSxDQUFDLG9CQUFvQixDQUFDLGNBQWMsQ0FBQzthQUMzQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNoQyxTQUFTOzs7O1FBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsR0FBRyxPQUFPLEVBQUMsQ0FBQztRQUU5RCxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQzFCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsa0JBQWtCLENBQUM7U0FDNUU7UUFFRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVU7aUJBQ2pCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2lCQUNoQyxTQUFTOzs7O1lBQUMsVUFBVSxDQUFDLEVBQUU7Z0JBQ3BCLElBQUksVUFBVSxDQUFDLEtBQUssS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxFQUFFO29CQUM3QyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7aUJBQ3JCO2dCQUVELElBQUksQ0FBQyxVQUFVLEdBQUcsVUFBVSxDQUFDO2dCQUM3QixJQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQzdCLENBQUMsRUFBQyxDQUFDO1NBQ1Y7UUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUNsQixJQUFJLENBQUMsVUFBVSxHQUFHLG1CQUFtQixDQUFDLGtCQUFrQixDQUFDO1NBQzVEO0lBQ0wsQ0FBQzs7OztJQUVELElBQUksUUFBUTtjQUNGLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxHQUFHLElBQUksQ0FBQyxVQUFVO1FBRWhELE9BQU8sQ0FBQyxVQUFVLElBQUksUUFBUSxDQUFDO1lBQzNCLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7WUFDbEMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNaLENBQUM7Ozs7SUFFRCxJQUFJLE9BQU87Y0FDRCxFQUFFLFFBQVEsRUFBRSxTQUFTLEVBQUUsR0FBRyxJQUFJLENBQUMsVUFBVTtRQUUvQyxPQUFPLENBQUMsU0FBUyxJQUFJLFFBQVEsQ0FBQztZQUMxQixDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLEdBQUcsUUFBUSxDQUFDLEdBQUcsQ0FBQztZQUN0QyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ1osQ0FBQzs7OztJQUVELElBQUksVUFBVTtRQUNWLE9BQU8sSUFBSSxDQUFDLE9BQU8sS0FBSyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQzFDLENBQUM7Ozs7SUFFRCxJQUFJLFdBQVc7UUFDWCxPQUFPLElBQUksQ0FBQyxPQUFPLEtBQUssQ0FBQyxDQUFDO0lBQzlCLENBQUM7Ozs7SUFFRCxJQUFJLElBQUk7UUFDSixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO0lBQzdELENBQUM7Ozs7SUFFRCxJQUFJLFFBQVE7UUFDUixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7SUFDbkQsQ0FBQzs7OztJQUVELElBQUksUUFBUTtRQUNSLE9BQU8sSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7SUFDeEQsQ0FBQzs7OztJQUVELElBQ0ksT0FBTztRQUNQLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQzFCLENBQUM7Ozs7SUFFRCxJQUFJLEtBQUs7Y0FDQyxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLEdBQUcsSUFBSSxDQUFDLFVBQVU7Y0FDckQsRUFBRSxVQUFVLEVBQUUsR0FBRyxJQUFJOztjQUVyQixLQUFLLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOztjQUN0QyxHQUFHLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLFNBQVMsR0FBRyxRQUFRO1FBRTFELE9BQU8sQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDeEIsQ0FBQzs7OztJQUVELElBQUksS0FBSztRQUNMLE9BQU8sS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7YUFDdEIsSUFBSSxDQUFDLEdBQUcsQ0FBQzthQUNULEdBQUc7Ozs7O1FBQUMsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLEVBQUUsQ0FBQyxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsRUFBQyxDQUFDO0lBQzNDLENBQUM7Ozs7SUFFRCxNQUFNO1FBQ0YsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFOztrQkFDVCxRQUFRLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFROztrQkFDbkMsU0FBUyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksR0FBRyxDQUFDLENBQUMsR0FBRyxRQUFRO1lBQzVDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLFNBQVMsQ0FBQztZQUV0QyxJQUFJLENBQUMscUJBQXFCLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRTtnQkFDOUQsU0FBUztnQkFDVCxRQUFRO2FBQ1gsQ0FBQyxDQUFDO1NBQ047SUFDTCxDQUFDOzs7O0lBRUQsVUFBVTtRQUNOLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTs7a0JBQ1QsUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUTs7a0JBQ25DLFNBQVMsR0FBRyxDQUFDLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLEdBQUcsUUFBUTtZQUNoRCxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7WUFFdEMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUU7Z0JBQzlELFNBQVM7Z0JBQ1QsUUFBUTthQUNYLENBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxrQkFBa0IsQ0FBQyxVQUFrQjtRQUNqQyxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7O2tCQUNULFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVE7O2tCQUNuQyxTQUFTLEdBQUcsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLEdBQUcsUUFBUTtZQUM3QyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7WUFFdEMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRTtnQkFDdkUsU0FBUztnQkFDVCxRQUFRO2FBQ1gsQ0FBQyxDQUFDO1NBQ047SUFDTCxDQUFDOzs7OztJQUVELGdCQUFnQixDQUFDLFFBQWdCO1FBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQztRQUM5QixJQUFJLENBQUMsc0JBQXNCLENBQUMsY0FBYyxHQUFHLFFBQVEsQ0FBQztRQUN0RCxJQUFJLENBQUMscUJBQXFCLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLGdCQUFnQixFQUFFO1lBQ3JFLFNBQVMsRUFBRSxDQUFDO1lBQ1osUUFBUTtTQUNYLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDOzs7Ozs7SUFFRCxxQkFBcUIsQ0FBQyxNQUFjLEVBQUUsTUFBdUI7Y0FDbkQsRUFDRixTQUFTLEVBQ1QsU0FBUyxFQUNULGtCQUFrQixFQUNsQixnQkFBZ0IsRUFDbkIsR0FBRyxtQkFBbUIsQ0FBQyxPQUFPO2NBRXpCLEVBQ0YsTUFBTSxFQUNOLGdCQUFnQixFQUNoQixjQUFjLEVBQ2QsUUFBUSxFQUNSLFFBQVEsRUFDUixVQUFVLEVBQ2IsR0FBRyxJQUFJOztjQUVGLGVBQWUsR0FBb0IsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQztRQUU5RSxJQUFJLE1BQU0sS0FBSyxTQUFTLEVBQUU7WUFDdEIsUUFBUSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUNsQztRQUVELElBQUksTUFBTSxLQUFLLFNBQVMsRUFBRTtZQUN0QixRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1NBQ2xDO1FBRUQsSUFBSSxNQUFNLEtBQUssa0JBQWtCLEVBQUU7WUFDL0IsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1NBQzFDO1FBRUQsSUFBSSxNQUFNLEtBQUssZ0JBQWdCLEVBQUU7WUFDN0IsY0FBYyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUN4QztRQUVELE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFFcEIsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN4QztJQUNMLENBQUM7O0FBcE9NLHNDQUFrQixHQUFlLElBQUksVUFBVSxDQUFDO0lBQ25ELFNBQVMsRUFBRSxDQUFDO0lBQ1osUUFBUSxFQUFFLEVBQUU7SUFDWixVQUFVLEVBQUUsQ0FBQztDQUNoQixDQUFDLENBQUM7QUFFSSwyQkFBTyxHQUFHO0lBQ2IsU0FBUyxFQUFFLFdBQVc7SUFDdEIsU0FBUyxFQUFFLFdBQVc7SUFDdEIsZ0JBQWdCLEVBQUUsa0JBQWtCO0lBQ3BDLGtCQUFrQixFQUFFLG9CQUFvQjtDQUMzQyxDQUFDOztZQXJCTCxTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGdCQUFnQjtnQkFDMUIsSUFBSSxFQUFFLEVBQUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFO2dCQUNuQyxpdUdBQTBDO2dCQUUxQyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtnQkFDL0MsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2FBQ3hDOzs7O1lBbEJHLGlCQUFpQjtZQVFaLHNCQUFzQjs7O3FCQTJCMUIsS0FBSztpQ0FJTCxLQUFLO3lCQUlMLEtBQUs7cUJBSUwsTUFBTTsrQkFJTixNQUFNOzZCQUlOLE1BQU07dUJBSU4sTUFBTTt1QkFJTixNQUFNO3NCQXdFTixXQUFXLFNBQUMsNkJBQTZCOzs7O0lBbEgxQyx1Q0FJRzs7SUFFSCw0QkFLRTs7Ozs7SUFHRixxQ0FDMkI7Ozs7O0lBRzNCLGlEQUM2Qjs7Ozs7SUFHN0IseUNBQ3FFOzs7OztJQUdyRSxxQ0FDNEU7Ozs7O0lBRzVFLCtDQUNzRjs7Ozs7SUFHdEYsNkNBQ29GOzs7OztJQUdwRix1Q0FDOEU7Ozs7O0lBRzlFLHVDQUM4RTs7Ozs7SUFFOUUseUNBQTRDOzs7OztJQUVoQyxrQ0FBOEI7Ozs7O0lBQUUscURBQXNEIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7XHJcbiAgICBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSwgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkluaXQsIE91dHB1dCwgVmlld0VuY2Fwc3VsYXRpb24sXHJcbiAgICBDaGFuZ2VEZXRlY3RvclJlZiwgT25EZXN0cm95LCBIb3N0QmluZGluZ1xyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgUGFnaW5hdGlvbiB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5pbXBvcnQgeyBQYWdpbmF0ZWRDb21wb25lbnQgfSBmcm9tICcuL3BhZ2luYXRlZC1jb21wb25lbnQuaW50ZXJmYWNlJztcclxuaW1wb3J0IHsgUGFnaW5hdGlvbkNvbXBvbmVudEludGVyZmFjZSB9IGZyb20gJy4vcGFnaW5hdGlvbi1jb21wb25lbnQuaW50ZXJmYWNlJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBQYWdpbmF0aW9uTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvcGFnaW5hdGlvbi5tb2RlbCc7XHJcbmltcG9ydCB7IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UsIFVzZXJQcmVmZXJlbmNlVmFsdWVzIH0gZnJvbSAnLi4vc2VydmljZXMvdXNlci1wcmVmZXJlbmNlcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1wYWdpbmF0aW9uJyxcclxuICAgIGhvc3Q6IHsgJ2NsYXNzJzogJ2FkZi1wYWdpbmF0aW9uJyB9LFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL3BhZ2luYXRpb24uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vcGFnaW5hdGlvbi5jb21wb25lbnQuc2NzcyddLFxyXG4gICAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBQYWdpbmF0aW9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3ksIFBhZ2luYXRpb25Db21wb25lbnRJbnRlcmZhY2Uge1xyXG5cclxuICAgIHN0YXRpYyBERUZBVUxUX1BBR0lOQVRJT046IFBhZ2luYXRpb24gPSBuZXcgUGFnaW5hdGlvbih7XHJcbiAgICAgICAgc2tpcENvdW50OiAwLFxyXG4gICAgICAgIG1heEl0ZW1zOiAyNSxcclxuICAgICAgICB0b3RhbEl0ZW1zOiAwXHJcbiAgICB9KTtcclxuXHJcbiAgICBzdGF0aWMgQUNUSU9OUyA9IHtcclxuICAgICAgICBORVhUX1BBR0U6ICdORVhUX1BBR0UnLFxyXG4gICAgICAgIFBSRVZfUEFHRTogJ1BSRVZfUEFHRScsXHJcbiAgICAgICAgQ0hBTkdFX1BBR0VfU0laRTogJ0NIQU5HRV9QQUdFX1NJWkUnLFxyXG4gICAgICAgIENIQU5HRV9QQUdFX05VTUJFUjogJ0NIQU5HRV9QQUdFX05VTUJFUidcclxuICAgIH07XHJcblxyXG4gICAgLyoqIENvbXBvbmVudCB0aGF0IHByb3ZpZGVzIGN1c3RvbSBwYWdpbmF0aW9uIHN1cHBvcnQuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgdGFyZ2V0OiBQYWdpbmF0ZWRDb21wb25lbnQ7XHJcblxyXG4gICAgLyoqIEFuIGFycmF5IG9mIHBhZ2Ugc2l6ZXMuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgc3VwcG9ydGVkUGFnZVNpemVzOiBudW1iZXJbXTtcclxuXHJcbiAgICAvKiogUGFnaW5hdGlvbiBvYmplY3QuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgcGFnaW5hdGlvbjogUGFnaW5hdGlvbk1vZGVsID0gUGFnaW5hdGlvbkNvbXBvbmVudC5ERUZBVUxUX1BBR0lOQVRJT047XHJcblxyXG4gICAgLyoqIEVtaXR0ZWQgd2hlbiBwYWdpbmF0aW9uIGNoYW5nZXMgaW4gYW55IHdheS4gKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgY2hhbmdlOiBFdmVudEVtaXR0ZXI8UGFnaW5hdGlvbk1vZGVsPiA9IG5ldyBFdmVudEVtaXR0ZXI8UGFnaW5hdGlvbk1vZGVsPigpO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIHBhZ2UgbnVtYmVyIGNoYW5nZXMuICovXHJcbiAgICBAT3V0cHV0KClcclxuICAgIGNoYW5nZVBhZ2VOdW1iZXI6IEV2ZW50RW1pdHRlcjxQYWdpbmF0aW9uTW9kZWw+ID0gbmV3IEV2ZW50RW1pdHRlcjxQYWdpbmF0aW9uTW9kZWw+KCk7XHJcblxyXG4gICAgLyoqIEVtaXR0ZWQgd2hlbiB0aGUgcGFnZSBzaXplIGNoYW5nZXMuICovXHJcbiAgICBAT3V0cHV0KClcclxuICAgIGNoYW5nZVBhZ2VTaXplOiBFdmVudEVtaXR0ZXI8UGFnaW5hdGlvbk1vZGVsPiA9IG5ldyBFdmVudEVtaXR0ZXI8UGFnaW5hdGlvbk1vZGVsPigpO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIG5leHQgcGFnZSBpcyByZXF1ZXN0ZWQuICovXHJcbiAgICBAT3V0cHV0KClcclxuICAgIG5leHRQYWdlOiBFdmVudEVtaXR0ZXI8UGFnaW5hdGlvbk1vZGVsPiA9IG5ldyBFdmVudEVtaXR0ZXI8UGFnaW5hdGlvbk1vZGVsPigpO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIHByZXZpb3VzIHBhZ2UgaXMgcmVxdWVzdGVkLiAqL1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICBwcmV2UGFnZTogRXZlbnRFbWl0dGVyPFBhZ2luYXRpb25Nb2RlbD4gPSBuZXcgRXZlbnRFbWl0dGVyPFBhZ2luYXRpb25Nb2RlbD4oKTtcclxuXHJcbiAgICBwcml2YXRlIG9uRGVzdHJveSQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgY2RyOiBDaGFuZ2VEZXRlY3RvclJlZiwgcHJpdmF0ZSB1c2VyUHJlZmVyZW5jZXNTZXJ2aWNlOiBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZXNTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5zZWxlY3QoVXNlclByZWZlcmVuY2VWYWx1ZXMuUGFnaW5hdGlvblNpemUpXHJcbiAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLm9uRGVzdHJveSQpKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKHBhZ1NpemUgPT4gdGhpcy5wYWdpbmF0aW9uLm1heEl0ZW1zID0gcGFnU2l6ZSk7XHJcblxyXG4gICAgICAgIGlmICghdGhpcy5zdXBwb3J0ZWRQYWdlU2l6ZXMpIHtcclxuICAgICAgICAgICAgdGhpcy5zdXBwb3J0ZWRQYWdlU2l6ZXMgPSB0aGlzLnVzZXJQcmVmZXJlbmNlc1NlcnZpY2Uuc3VwcG9ydGVkUGFnZVNpemVzO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMudGFyZ2V0KSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFyZ2V0LnBhZ2luYXRpb25cclxuICAgICAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLm9uRGVzdHJveSQpKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShwYWdpbmF0aW9uID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocGFnaW5hdGlvbi5jb3VudCA9PT0gMCAmJiAhdGhpcy5pc0ZpcnN0UGFnZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmdvUHJldmlvdXMoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucGFnaW5hdGlvbiA9IHBhZ2luYXRpb247XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jZHIuZGV0ZWN0Q2hhbmdlcygpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXRoaXMucGFnaW5hdGlvbikge1xyXG4gICAgICAgICAgICB0aGlzLnBhZ2luYXRpb24gPSBQYWdpbmF0aW9uQ29tcG9uZW50LkRFRkFVTFRfUEFHSU5BVElPTjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGxhc3RQYWdlKCk6IG51bWJlciB7XHJcbiAgICAgICAgY29uc3QgeyBtYXhJdGVtcywgdG90YWxJdGVtcyB9ID0gdGhpcy5wYWdpbmF0aW9uO1xyXG5cclxuICAgICAgICByZXR1cm4gKHRvdGFsSXRlbXMgJiYgbWF4SXRlbXMpXHJcbiAgICAgICAgICAgID8gTWF0aC5jZWlsKHRvdGFsSXRlbXMgLyBtYXhJdGVtcylcclxuICAgICAgICAgICAgOiAxO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBjdXJyZW50KCk6IG51bWJlciB7XHJcbiAgICAgICAgY29uc3QgeyBtYXhJdGVtcywgc2tpcENvdW50IH0gPSB0aGlzLnBhZ2luYXRpb247XHJcblxyXG4gICAgICAgIHJldHVybiAoc2tpcENvdW50ICYmIG1heEl0ZW1zKVxyXG4gICAgICAgICAgICA/IE1hdGguZmxvb3Ioc2tpcENvdW50IC8gbWF4SXRlbXMpICsgMVxyXG4gICAgICAgICAgICA6IDE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGlzTGFzdFBhZ2UoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudCA9PT0gdGhpcy5sYXN0UGFnZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgaXNGaXJzdFBhZ2UoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3VycmVudCA9PT0gMTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgbmV4dCgpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmlzTGFzdFBhZ2UgPyB0aGlzLmN1cnJlbnQgOiB0aGlzLmN1cnJlbnQgKyAxO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBwcmV2aW91cygpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmlzRmlyc3RQYWdlID8gMSA6IHRoaXMuY3VycmVudCAtIDE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGhhc0l0ZW1zKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnBhZ2luYXRpb24gJiYgdGhpcy5wYWdpbmF0aW9uLmNvdW50ID4gMDtcclxuICAgIH1cclxuXHJcbiAgICBASG9zdEJpbmRpbmcoJ2NsYXNzLmFkZi1wYWdpbmF0aW9uX19lbXB0eScpXHJcbiAgICBnZXQgaXNFbXB0eSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gIXRoaXMuaGFzSXRlbXM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHJhbmdlKCk6IG51bWJlcltdIHtcclxuICAgICAgICBjb25zdCB7IHNraXBDb3VudCwgbWF4SXRlbXMsIHRvdGFsSXRlbXMgfSA9IHRoaXMucGFnaW5hdGlvbjtcclxuICAgICAgICBjb25zdCB7IGlzTGFzdFBhZ2UgfSA9IHRoaXM7XHJcblxyXG4gICAgICAgIGNvbnN0IHN0YXJ0ID0gdG90YWxJdGVtcyA/IHNraXBDb3VudCArIDEgOiAwO1xyXG4gICAgICAgIGNvbnN0IGVuZCA9IGlzTGFzdFBhZ2UgPyB0b3RhbEl0ZW1zIDogc2tpcENvdW50ICsgbWF4SXRlbXM7XHJcblxyXG4gICAgICAgIHJldHVybiBbc3RhcnQsIGVuZF07XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHBhZ2VzKCk6IG51bWJlcltdIHtcclxuICAgICAgICByZXR1cm4gQXJyYXkodGhpcy5sYXN0UGFnZSlcclxuICAgICAgICAgICAgLmZpbGwoJ24nKVxyXG4gICAgICAgICAgICAubWFwKChpdGVtLCBpbmRleCkgPT4gKGluZGV4ICsgMSkpO1xyXG4gICAgfVxyXG5cclxuICAgIGdvTmV4dCgpIHtcclxuICAgICAgICBpZiAodGhpcy5oYXNJdGVtcykge1xyXG4gICAgICAgICAgICBjb25zdCBtYXhJdGVtcyA9IHRoaXMucGFnaW5hdGlvbi5tYXhJdGVtcztcclxuICAgICAgICAgICAgY29uc3Qgc2tpcENvdW50ID0gKHRoaXMubmV4dCAtIDEpICogbWF4SXRlbXM7XHJcbiAgICAgICAgICAgIHRoaXMucGFnaW5hdGlvbi5za2lwQ291bnQgPSBza2lwQ291bnQ7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmhhbmRsZVBhZ2luYXRpb25FdmVudChQYWdpbmF0aW9uQ29tcG9uZW50LkFDVElPTlMuTkVYVF9QQUdFLCB7XHJcbiAgICAgICAgICAgICAgICBza2lwQ291bnQsXHJcbiAgICAgICAgICAgICAgICBtYXhJdGVtc1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ29QcmV2aW91cygpIHtcclxuICAgICAgICBpZiAodGhpcy5oYXNJdGVtcykge1xyXG4gICAgICAgICAgICBjb25zdCBtYXhJdGVtcyA9IHRoaXMucGFnaW5hdGlvbi5tYXhJdGVtcztcclxuICAgICAgICAgICAgY29uc3Qgc2tpcENvdW50ID0gKHRoaXMucHJldmlvdXMgLSAxKSAqIG1heEl0ZW1zO1xyXG4gICAgICAgICAgICB0aGlzLnBhZ2luYXRpb24uc2tpcENvdW50ID0gc2tpcENvdW50O1xyXG5cclxuICAgICAgICAgICAgdGhpcy5oYW5kbGVQYWdpbmF0aW9uRXZlbnQoUGFnaW5hdGlvbkNvbXBvbmVudC5BQ1RJT05TLlBSRVZfUEFHRSwge1xyXG4gICAgICAgICAgICAgICAgc2tpcENvdW50LFxyXG4gICAgICAgICAgICAgICAgbWF4SXRlbXNcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uQ2hhbmdlUGFnZU51bWJlcihwYWdlTnVtYmVyOiBudW1iZXIpIHtcclxuICAgICAgICBpZiAodGhpcy5oYXNJdGVtcykge1xyXG4gICAgICAgICAgICBjb25zdCBtYXhJdGVtcyA9IHRoaXMucGFnaW5hdGlvbi5tYXhJdGVtcztcclxuICAgICAgICAgICAgY29uc3Qgc2tpcENvdW50ID0gKHBhZ2VOdW1iZXIgLSAxKSAqIG1heEl0ZW1zO1xyXG4gICAgICAgICAgICB0aGlzLnBhZ2luYXRpb24uc2tpcENvdW50ID0gc2tpcENvdW50O1xyXG5cclxuICAgICAgICAgICAgdGhpcy5oYW5kbGVQYWdpbmF0aW9uRXZlbnQoUGFnaW5hdGlvbkNvbXBvbmVudC5BQ1RJT05TLkNIQU5HRV9QQUdFX05VTUJFUiwge1xyXG4gICAgICAgICAgICAgICAgc2tpcENvdW50LFxyXG4gICAgICAgICAgICAgICAgbWF4SXRlbXNcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uQ2hhbmdlUGFnZVNpemUobWF4SXRlbXM6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMucGFnaW5hdGlvbi5za2lwQ291bnQgPSAwO1xyXG4gICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VzU2VydmljZS5wYWdpbmF0aW9uU2l6ZSA9IG1heEl0ZW1zO1xyXG4gICAgICAgIHRoaXMuaGFuZGxlUGFnaW5hdGlvbkV2ZW50KFBhZ2luYXRpb25Db21wb25lbnQuQUNUSU9OUy5DSEFOR0VfUEFHRV9TSVpFLCB7XHJcbiAgICAgICAgICAgIHNraXBDb3VudDogMCxcclxuICAgICAgICAgICAgbWF4SXRlbXNcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQubmV4dCh0cnVlKTtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQuY29tcGxldGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBoYW5kbGVQYWdpbmF0aW9uRXZlbnQoYWN0aW9uOiBzdHJpbmcsIHBhcmFtczogUGFnaW5hdGlvbk1vZGVsKSB7XHJcbiAgICAgICAgY29uc3Qge1xyXG4gICAgICAgICAgICBORVhUX1BBR0UsXHJcbiAgICAgICAgICAgIFBSRVZfUEFHRSxcclxuICAgICAgICAgICAgQ0hBTkdFX1BBR0VfTlVNQkVSLFxyXG4gICAgICAgICAgICBDSEFOR0VfUEFHRV9TSVpFXHJcbiAgICAgICAgfSA9IFBhZ2luYXRpb25Db21wb25lbnQuQUNUSU9OUztcclxuXHJcbiAgICAgICAgY29uc3Qge1xyXG4gICAgICAgICAgICBjaGFuZ2UsXHJcbiAgICAgICAgICAgIGNoYW5nZVBhZ2VOdW1iZXIsXHJcbiAgICAgICAgICAgIGNoYW5nZVBhZ2VTaXplLFxyXG4gICAgICAgICAgICBuZXh0UGFnZSxcclxuICAgICAgICAgICAgcHJldlBhZ2UsXHJcbiAgICAgICAgICAgIHBhZ2luYXRpb25cclxuICAgICAgICB9ID0gdGhpcztcclxuXHJcbiAgICAgICAgY29uc3QgcGFnaW5hdGlvbk1vZGVsOiBQYWdpbmF0aW9uTW9kZWwgPSBPYmplY3QuYXNzaWduKHt9LCBwYWdpbmF0aW9uLCBwYXJhbXMpO1xyXG5cclxuICAgICAgICBpZiAoYWN0aW9uID09PSBORVhUX1BBR0UpIHtcclxuICAgICAgICAgICAgbmV4dFBhZ2UuZW1pdChwYWdpbmF0aW9uTW9kZWwpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGFjdGlvbiA9PT0gUFJFVl9QQUdFKSB7XHJcbiAgICAgICAgICAgIHByZXZQYWdlLmVtaXQocGFnaW5hdGlvbk1vZGVsKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChhY3Rpb24gPT09IENIQU5HRV9QQUdFX05VTUJFUikge1xyXG4gICAgICAgICAgICBjaGFuZ2VQYWdlTnVtYmVyLmVtaXQocGFnaW5hdGlvbk1vZGVsKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChhY3Rpb24gPT09IENIQU5HRV9QQUdFX1NJWkUpIHtcclxuICAgICAgICAgICAgY2hhbmdlUGFnZVNpemUuZW1pdChwYWdpbmF0aW9uTW9kZWwpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY2hhbmdlLmVtaXQocGFyYW1zKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMudGFyZ2V0KSB7XHJcbiAgICAgICAgICAgIHRoaXMudGFyZ2V0LnVwZGF0ZVBhZ2luYXRpb24ocGFyYW1zKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19