/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DecimalPipe } from '@angular/common';
import { Pipe } from '@angular/core';
import { AppConfigService } from '../app-config/app-config.service';
import { UserPreferencesService, UserPreferenceValues } from '../services/user-preferences.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
export class DecimalNumberPipe {
    /**
     * @param {?=} userPreferenceService
     * @param {?=} appConfig
     */
    constructor(userPreferenceService, appConfig) {
        this.userPreferenceService = userPreferenceService;
        this.appConfig = appConfig;
        this.defaultLocale = DecimalNumberPipe.DEFAULT_LOCALE;
        this.defaultMinIntegerDigits = DecimalNumberPipe.DEFAULT_MIN_INTEGER_DIGITS;
        this.defaultMinFractionDigits = DecimalNumberPipe.DEFAULT_MIN_FRACTION_DIGITS;
        this.defaultMaxFractionDigits = DecimalNumberPipe.DEFAULT_MAX_FRACTION_DIGITS;
        this.onDestroy$ = new Subject();
        if (this.userPreferenceService) {
            this.userPreferenceService.select(UserPreferenceValues.Locale)
                .pipe(takeUntil(this.onDestroy$))
                .subscribe((/**
             * @param {?} locale
             * @return {?}
             */
            (locale) => {
                if (locale) {
                    this.defaultLocale = locale;
                }
            }));
        }
        if (this.appConfig) {
            this.defaultMinIntegerDigits = this.appConfig.get('decimalValues.minIntegerDigits', DecimalNumberPipe.DEFAULT_MIN_INTEGER_DIGITS);
            this.defaultMinFractionDigits = this.appConfig.get('decimalValues.minFractionDigits', DecimalNumberPipe.DEFAULT_MIN_FRACTION_DIGITS);
            this.defaultMaxFractionDigits = this.appConfig.get('decimalValues.maxFractionDigits', DecimalNumberPipe.DEFAULT_MAX_FRACTION_DIGITS);
        }
    }
    /**
     * @param {?} value
     * @param {?=} digitsInfo
     * @param {?=} locale
     * @return {?}
     */
    transform(value, digitsInfo, locale) {
        /** @type {?} */
        const actualMinIntegerDigits = digitsInfo && digitsInfo.minIntegerDigits ? digitsInfo.minIntegerDigits : this.defaultMinIntegerDigits;
        /** @type {?} */
        const actualMinFractionDigits = digitsInfo && digitsInfo.minFractionDigits ? digitsInfo.minFractionDigits : this.defaultMinFractionDigits;
        /** @type {?} */
        const actualMaxFractionDigits = digitsInfo && digitsInfo.maxFractionDigits ? digitsInfo.maxFractionDigits : this.defaultMaxFractionDigits;
        /** @type {?} */
        const actualDigitsInfo = `${actualMinIntegerDigits}.${actualMinFractionDigits}-${actualMaxFractionDigits}`;
        /** @type {?} */
        const actualLocale = locale || this.defaultLocale;
        /** @type {?} */
        const datePipe = new DecimalPipe(actualLocale);
        return datePipe.transform(value, actualDigitsInfo);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    }
}
DecimalNumberPipe.DEFAULT_LOCALE = 'en-US';
DecimalNumberPipe.DEFAULT_MIN_INTEGER_DIGITS = 1;
DecimalNumberPipe.DEFAULT_MIN_FRACTION_DIGITS = 0;
DecimalNumberPipe.DEFAULT_MAX_FRACTION_DIGITS = 2;
DecimalNumberPipe.decorators = [
    { type: Pipe, args: [{
                name: 'adfDecimalNumber',
                pure: false
            },] }
];
/** @nocollapse */
DecimalNumberPipe.ctorParameters = () => [
    { type: UserPreferencesService },
    { type: AppConfigService }
];
if (false) {
    /** @type {?} */
    DecimalNumberPipe.DEFAULT_LOCALE;
    /** @type {?} */
    DecimalNumberPipe.DEFAULT_MIN_INTEGER_DIGITS;
    /** @type {?} */
    DecimalNumberPipe.DEFAULT_MIN_FRACTION_DIGITS;
    /** @type {?} */
    DecimalNumberPipe.DEFAULT_MAX_FRACTION_DIGITS;
    /** @type {?} */
    DecimalNumberPipe.prototype.defaultLocale;
    /** @type {?} */
    DecimalNumberPipe.prototype.defaultMinIntegerDigits;
    /** @type {?} */
    DecimalNumberPipe.prototype.defaultMinFractionDigits;
    /** @type {?} */
    DecimalNumberPipe.prototype.defaultMaxFractionDigits;
    /** @type {?} */
    DecimalNumberPipe.prototype.onDestroy$;
    /** @type {?} */
    DecimalNumberPipe.prototype.userPreferenceService;
    /** @type {?} */
    DecimalNumberPipe.prototype.appConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVjaW1hbC1udW1iZXIucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInBpcGVzL2RlY2ltYWwtbnVtYmVyLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxJQUFJLEVBQTRCLE1BQU0sZUFBZSxDQUFDO0FBQy9ELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBRXBHLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBTTNDLE1BQU0sT0FBTyxpQkFBaUI7Ozs7O0lBYzFCLFlBQW1CLHFCQUE4QyxFQUM5QyxTQUE0QjtRQUQ1QiwwQkFBcUIsR0FBckIscUJBQXFCLENBQXlCO1FBQzlDLGNBQVMsR0FBVCxTQUFTLENBQW1CO1FBUi9DLGtCQUFhLEdBQVcsaUJBQWlCLENBQUMsY0FBYyxDQUFDO1FBQ3pELDRCQUF1QixHQUFXLGlCQUFpQixDQUFDLDBCQUEwQixDQUFDO1FBQy9FLDZCQUF3QixHQUFXLGlCQUFpQixDQUFDLDJCQUEyQixDQUFDO1FBQ2pGLDZCQUF3QixHQUFXLGlCQUFpQixDQUFDLDJCQUEyQixDQUFDO1FBRWpGLGVBQVUsR0FBcUIsSUFBSSxPQUFPLEVBQVcsQ0FBQztRQUtsRCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUM1QixJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQztpQkFDekQsSUFBSSxDQUNELFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQzdCO2lCQUNBLFNBQVM7Ozs7WUFBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO2dCQUNsQixJQUFJLE1BQU0sRUFBRTtvQkFDUixJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQztpQkFDL0I7WUFDTCxDQUFDLEVBQUMsQ0FBQztTQUNWO1FBRUQsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLElBQUksQ0FBQyx1QkFBdUIsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBUyxnQ0FBZ0MsRUFBRSxpQkFBaUIsQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO1lBQzFJLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBUyxpQ0FBaUMsRUFBRSxpQkFBaUIsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1lBQzdJLElBQUksQ0FBQyx3QkFBd0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBUyxpQ0FBaUMsRUFBRSxpQkFBaUIsQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO1NBQ2hKO0lBQ0wsQ0FBQzs7Ozs7OztJQUVELFNBQVMsQ0FBQyxLQUFVLEVBQUUsVUFBK0IsRUFBRSxNQUFlOztjQUM1RCxzQkFBc0IsR0FBVyxVQUFVLElBQUksVUFBVSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyx1QkFBdUI7O2NBQ3ZJLHVCQUF1QixHQUFXLFVBQVUsSUFBSSxVQUFVLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLHdCQUF3Qjs7Y0FDM0ksdUJBQXVCLEdBQVcsVUFBVSxJQUFJLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsd0JBQXdCOztjQUUzSSxnQkFBZ0IsR0FBRyxHQUFHLHNCQUFzQixJQUFJLHVCQUF1QixJQUFJLHVCQUF1QixFQUFFOztjQUNwRyxZQUFZLEdBQUcsTUFBTSxJQUFJLElBQUksQ0FBQyxhQUFhOztjQUUzQyxRQUFRLEdBQWdCLElBQUksV0FBVyxDQUFDLFlBQVksQ0FBQztRQUMzRCxPQUFPLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLGdCQUFnQixDQUFDLENBQUM7SUFDdkQsQ0FBQzs7OztJQUVELFdBQVc7UUFDUCxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQy9CLENBQUM7O0FBakRNLGdDQUFjLEdBQUcsT0FBTyxDQUFDO0FBQ3pCLDRDQUEwQixHQUFHLENBQUMsQ0FBQztBQUMvQiw2Q0FBMkIsR0FBRyxDQUFDLENBQUM7QUFDaEMsNkNBQTJCLEdBQUcsQ0FBQyxDQUFDOztZQVQxQyxJQUFJLFNBQUM7Z0JBQ0YsSUFBSSxFQUFFLGtCQUFrQjtnQkFDeEIsSUFBSSxFQUFFLEtBQUs7YUFDZDs7OztZQVJRLHNCQUFzQjtZQUR0QixnQkFBZ0I7Ozs7SUFZckIsaUNBQWdDOztJQUNoQyw2Q0FBc0M7O0lBQ3RDLDhDQUF1Qzs7SUFDdkMsOENBQXVDOztJQUV2QywwQ0FBeUQ7O0lBQ3pELG9EQUErRTs7SUFDL0UscURBQWlGOztJQUNqRixxREFBaUY7O0lBRWpGLHVDQUFzRDs7SUFFMUMsa0RBQXFEOztJQUNyRCxzQ0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgRGVjaW1hbFBpcGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtLCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQXBwQ29uZmlnU2VydmljZSB9IGZyb20gJy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXNlclByZWZlcmVuY2VzU2VydmljZSwgVXNlclByZWZlcmVuY2VWYWx1ZXMgfSBmcm9tICcuLi9zZXJ2aWNlcy91c2VyLXByZWZlcmVuY2VzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEZWNpbWFsTnVtYmVyTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvZGVjaW1hbC1udW1iZXIubW9kZWwnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBQaXBlKHtcclxuICAgIG5hbWU6ICdhZGZEZWNpbWFsTnVtYmVyJyxcclxuICAgIHB1cmU6IGZhbHNlXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEZWNpbWFsTnVtYmVyUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0sIE9uRGVzdHJveSB7XHJcblxyXG4gICAgc3RhdGljIERFRkFVTFRfTE9DQUxFID0gJ2VuLVVTJztcclxuICAgIHN0YXRpYyBERUZBVUxUX01JTl9JTlRFR0VSX0RJR0lUUyA9IDE7XHJcbiAgICBzdGF0aWMgREVGQVVMVF9NSU5fRlJBQ1RJT05fRElHSVRTID0gMDtcclxuICAgIHN0YXRpYyBERUZBVUxUX01BWF9GUkFDVElPTl9ESUdJVFMgPSAyO1xyXG5cclxuICAgIGRlZmF1bHRMb2NhbGU6IHN0cmluZyA9IERlY2ltYWxOdW1iZXJQaXBlLkRFRkFVTFRfTE9DQUxFO1xyXG4gICAgZGVmYXVsdE1pbkludGVnZXJEaWdpdHM6IG51bWJlciA9IERlY2ltYWxOdW1iZXJQaXBlLkRFRkFVTFRfTUlOX0lOVEVHRVJfRElHSVRTO1xyXG4gICAgZGVmYXVsdE1pbkZyYWN0aW9uRGlnaXRzOiBudW1iZXIgPSBEZWNpbWFsTnVtYmVyUGlwZS5ERUZBVUxUX01JTl9GUkFDVElPTl9ESUdJVFM7XHJcbiAgICBkZWZhdWx0TWF4RnJhY3Rpb25EaWdpdHM6IG51bWJlciA9IERlY2ltYWxOdW1iZXJQaXBlLkRFRkFVTFRfTUFYX0ZSQUNUSU9OX0RJR0lUUztcclxuXHJcbiAgICBvbkRlc3Ryb3kkOiBTdWJqZWN0PGJvb2xlYW4+ID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgdXNlclByZWZlcmVuY2VTZXJ2aWNlPzogVXNlclByZWZlcmVuY2VzU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBhcHBDb25maWc/OiBBcHBDb25maWdTZXJ2aWNlKSB7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnVzZXJQcmVmZXJlbmNlU2VydmljZSkge1xyXG4gICAgICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlU2VydmljZS5zZWxlY3QoVXNlclByZWZlcmVuY2VWYWx1ZXMuTG9jYWxlKVxyXG4gICAgICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICAgICAgdGFrZVVudGlsKHRoaXMub25EZXN0cm95JClcclxuICAgICAgICAgICAgICAgIClcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoKGxvY2FsZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChsb2NhbGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWZhdWx0TG9jYWxlID0gbG9jYWxlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuYXBwQ29uZmlnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVmYXVsdE1pbkludGVnZXJEaWdpdHMgPSB0aGlzLmFwcENvbmZpZy5nZXQ8bnVtYmVyPignZGVjaW1hbFZhbHVlcy5taW5JbnRlZ2VyRGlnaXRzJywgRGVjaW1hbE51bWJlclBpcGUuREVGQVVMVF9NSU5fSU5URUdFUl9ESUdJVFMpO1xyXG4gICAgICAgICAgICB0aGlzLmRlZmF1bHRNaW5GcmFjdGlvbkRpZ2l0cyA9IHRoaXMuYXBwQ29uZmlnLmdldDxudW1iZXI+KCdkZWNpbWFsVmFsdWVzLm1pbkZyYWN0aW9uRGlnaXRzJywgRGVjaW1hbE51bWJlclBpcGUuREVGQVVMVF9NSU5fRlJBQ1RJT05fRElHSVRTKTtcclxuICAgICAgICAgICAgdGhpcy5kZWZhdWx0TWF4RnJhY3Rpb25EaWdpdHMgPSB0aGlzLmFwcENvbmZpZy5nZXQ8bnVtYmVyPignZGVjaW1hbFZhbHVlcy5tYXhGcmFjdGlvbkRpZ2l0cycsIERlY2ltYWxOdW1iZXJQaXBlLkRFRkFVTFRfTUFYX0ZSQUNUSU9OX0RJR0lUUyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHRyYW5zZm9ybSh2YWx1ZTogYW55LCBkaWdpdHNJbmZvPzogRGVjaW1hbE51bWJlck1vZGVsLCBsb2NhbGU/OiBzdHJpbmcpOiBhbnkge1xyXG4gICAgICAgIGNvbnN0IGFjdHVhbE1pbkludGVnZXJEaWdpdHM6IG51bWJlciA9IGRpZ2l0c0luZm8gJiYgZGlnaXRzSW5mby5taW5JbnRlZ2VyRGlnaXRzID8gZGlnaXRzSW5mby5taW5JbnRlZ2VyRGlnaXRzIDogdGhpcy5kZWZhdWx0TWluSW50ZWdlckRpZ2l0cztcclxuICAgICAgICBjb25zdCBhY3R1YWxNaW5GcmFjdGlvbkRpZ2l0czogbnVtYmVyID0gZGlnaXRzSW5mbyAmJiBkaWdpdHNJbmZvLm1pbkZyYWN0aW9uRGlnaXRzID8gZGlnaXRzSW5mby5taW5GcmFjdGlvbkRpZ2l0cyA6IHRoaXMuZGVmYXVsdE1pbkZyYWN0aW9uRGlnaXRzO1xyXG4gICAgICAgIGNvbnN0IGFjdHVhbE1heEZyYWN0aW9uRGlnaXRzOiBudW1iZXIgPSBkaWdpdHNJbmZvICYmIGRpZ2l0c0luZm8ubWF4RnJhY3Rpb25EaWdpdHMgPyBkaWdpdHNJbmZvLm1heEZyYWN0aW9uRGlnaXRzIDogdGhpcy5kZWZhdWx0TWF4RnJhY3Rpb25EaWdpdHM7XHJcblxyXG4gICAgICAgIGNvbnN0IGFjdHVhbERpZ2l0c0luZm8gPSBgJHthY3R1YWxNaW5JbnRlZ2VyRGlnaXRzfS4ke2FjdHVhbE1pbkZyYWN0aW9uRGlnaXRzfS0ke2FjdHVhbE1heEZyYWN0aW9uRGlnaXRzfWA7XHJcbiAgICAgICAgY29uc3QgYWN0dWFsTG9jYWxlID0gbG9jYWxlIHx8IHRoaXMuZGVmYXVsdExvY2FsZTtcclxuXHJcbiAgICAgICAgY29uc3QgZGF0ZVBpcGU6IERlY2ltYWxQaXBlID0gbmV3IERlY2ltYWxQaXBlKGFjdHVhbExvY2FsZSk7XHJcbiAgICAgICAgcmV0dXJuIGRhdGVQaXBlLnRyYW5zZm9ybSh2YWx1ZSwgYWN0dWFsRGlnaXRzSW5mbyk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkRlc3Ryb3koKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5vbkRlc3Ryb3kkLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5vbkRlc3Ryb3kkLmNvbXBsZXRlKCk7XHJcbiAgICB9XHJcbn1cclxuIl19