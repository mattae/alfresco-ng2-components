/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Pipe } from '@angular/core';
export class FormatSpacePipe {
    /**
     * @param {?} inputValue
     * @param {?=} replaceChar
     * @param {?=} lowerCase
     * @return {?}
     */
    transform(inputValue, replaceChar = '_', lowerCase = true) {
        /** @type {?} */
        let transformedString = '';
        if (inputValue) {
            transformedString = lowerCase ? inputValue.trim().split(' ').join(replaceChar).toLocaleLowerCase() :
                inputValue.trim().split(' ').join(replaceChar);
        }
        return transformedString;
    }
}
FormatSpacePipe.decorators = [
    { type: Pipe, args: [{
                name: 'formatSpace'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWF0LXNwYWNlLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJwaXBlcy9mb3JtYXQtc3BhY2UucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUtwRCxNQUFNLE9BQU8sZUFBZTs7Ozs7OztJQUV4QixTQUFTLENBQUMsVUFBa0IsRUFBRSxjQUFzQixHQUFHLEVBQUUsWUFBcUIsSUFBSTs7WUFDMUUsaUJBQWlCLEdBQUcsRUFBRTtRQUMxQixJQUFJLFVBQVUsRUFBRTtZQUNaLGlCQUFpQixHQUFHLFNBQVMsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxDQUFDO2dCQUNoRyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztTQUN0RDtRQUNELE9BQU8saUJBQWlCLENBQUM7SUFDN0IsQ0FBQzs7O1lBWkosSUFBSSxTQUFDO2dCQUNGLElBQUksRUFBRSxhQUFhO2FBQ3RCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBQaXBlKHtcclxuICAgIG5hbWU6ICdmb3JtYXRTcGFjZSdcclxufSlcclxuZXhwb3J0IGNsYXNzIEZvcm1hdFNwYWNlUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xyXG5cclxuICAgIHRyYW5zZm9ybShpbnB1dFZhbHVlOiBzdHJpbmcsIHJlcGxhY2VDaGFyOiBzdHJpbmcgPSAnXycsIGxvd2VyQ2FzZTogYm9vbGVhbiA9IHRydWUpOiBzdHJpbmcge1xyXG4gICAgICAgIGxldCB0cmFuc2Zvcm1lZFN0cmluZyA9ICcnO1xyXG4gICAgICAgIGlmIChpbnB1dFZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRyYW5zZm9ybWVkU3RyaW5nID0gbG93ZXJDYXNlID8gaW5wdXRWYWx1ZS50cmltKCkuc3BsaXQoJyAnKS5qb2luKHJlcGxhY2VDaGFyKS50b0xvY2FsZUxvd2VyQ2FzZSgpIDpcclxuICAgICAgICAgICAgICAgIGlucHV0VmFsdWUudHJpbSgpLnNwbGl0KCcgJykuam9pbihyZXBsYWNlQ2hhcik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cmFuc2Zvcm1lZFN0cmluZztcclxuICAgIH1cclxuXHJcbn1cclxuIl19