/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DatePipe } from '@angular/common';
import { Pipe } from '@angular/core';
import { AppConfigService } from '../app-config/app-config.service';
import { UserPreferencesService, UserPreferenceValues } from '../services/user-preferences.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
export class LocalizedDatePipe {
    /**
     * @param {?=} userPreferenceService
     * @param {?=} appConfig
     */
    constructor(userPreferenceService, appConfig) {
        this.userPreferenceService = userPreferenceService;
        this.appConfig = appConfig;
        this.defaultLocale = LocalizedDatePipe.DEFAULT_LOCALE;
        this.defaultFormat = LocalizedDatePipe.DEFAULT_DATE_FORMAT;
        this.onDestroy$ = new Subject();
        if (this.userPreferenceService) {
            this.userPreferenceService
                .select(UserPreferenceValues.Locale)
                .pipe(takeUntil(this.onDestroy$))
                .subscribe((/**
             * @param {?} locale
             * @return {?}
             */
            locale => {
                if (locale) {
                    this.defaultLocale = locale;
                }
            }));
        }
        if (this.appConfig) {
            this.defaultFormat = this.appConfig.get('dateValues.defaultDateFormat', LocalizedDatePipe.DEFAULT_DATE_FORMAT);
        }
    }
    /**
     * @param {?} value
     * @param {?=} format
     * @param {?=} locale
     * @return {?}
     */
    transform(value, format, locale) {
        /** @type {?} */
        const actualFormat = format || this.defaultFormat;
        /** @type {?} */
        const actualLocale = locale || this.defaultLocale;
        /** @type {?} */
        const datePipe = new DatePipe(actualLocale);
        return datePipe.transform(value, actualFormat);
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    }
}
LocalizedDatePipe.DEFAULT_LOCALE = 'en-US';
LocalizedDatePipe.DEFAULT_DATE_FORMAT = 'mediumDate';
LocalizedDatePipe.decorators = [
    { type: Pipe, args: [{
                name: 'adfLocalizedDate',
                pure: false
            },] }
];
/** @nocollapse */
LocalizedDatePipe.ctorParameters = () => [
    { type: UserPreferencesService },
    { type: AppConfigService }
];
if (false) {
    /** @type {?} */
    LocalizedDatePipe.DEFAULT_LOCALE;
    /** @type {?} */
    LocalizedDatePipe.DEFAULT_DATE_FORMAT;
    /** @type {?} */
    LocalizedDatePipe.prototype.defaultLocale;
    /** @type {?} */
    LocalizedDatePipe.prototype.defaultFormat;
    /**
     * @type {?}
     * @private
     */
    LocalizedDatePipe.prototype.onDestroy$;
    /** @type {?} */
    LocalizedDatePipe.prototype.userPreferenceService;
    /** @type {?} */
    LocalizedDatePipe.prototype.appConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWxpemVkLWRhdGUucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInBpcGVzL2xvY2FsaXplZC1kYXRlLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxJQUFJLEVBQTRCLE1BQU0sZUFBZSxDQUFDO0FBQy9ELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ3BHLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBTTNDLE1BQU0sT0FBTyxpQkFBaUI7Ozs7O0lBVTFCLFlBQW1CLHFCQUE4QyxFQUM5QyxTQUE0QjtRQUQ1QiwwQkFBcUIsR0FBckIscUJBQXFCLENBQXlCO1FBQzlDLGNBQVMsR0FBVCxTQUFTLENBQW1CO1FBTi9DLGtCQUFhLEdBQVcsaUJBQWlCLENBQUMsY0FBYyxDQUFDO1FBQ3pELGtCQUFhLEdBQVcsaUJBQWlCLENBQUMsbUJBQW1CLENBQUM7UUFFdEQsZUFBVSxHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7UUFLeEMsSUFBSSxJQUFJLENBQUMscUJBQXFCLEVBQUU7WUFDNUIsSUFBSSxDQUFDLHFCQUFxQjtpQkFDckIsTUFBTSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQztpQkFDbkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7aUJBQ2hDLFNBQVM7Ozs7WUFBQyxNQUFNLENBQUMsRUFBRTtnQkFDaEIsSUFBSSxNQUFNLEVBQUU7b0JBQ1IsSUFBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUM7aUJBQy9CO1lBQ0wsQ0FBQyxFQUFDLENBQUM7U0FDVjtRQUVELElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFTLDhCQUE4QixFQUFFLGlCQUFpQixDQUFDLG1CQUFtQixDQUFDLENBQUM7U0FDMUg7SUFDTCxDQUFDOzs7Ozs7O0lBRUQsU0FBUyxDQUFDLEtBQVUsRUFBRSxNQUFlLEVBQUUsTUFBZTs7Y0FDNUMsWUFBWSxHQUFHLE1BQU0sSUFBSSxJQUFJLENBQUMsYUFBYTs7Y0FDM0MsWUFBWSxHQUFHLE1BQU0sSUFBSSxJQUFJLENBQUMsYUFBYTs7Y0FDM0MsUUFBUSxHQUFhLElBQUksUUFBUSxDQUFDLFlBQVksQ0FBQztRQUNyRCxPQUFPLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLFlBQVksQ0FBQyxDQUFDO0lBQ25ELENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDOztBQXJDTSxnQ0FBYyxHQUFHLE9BQU8sQ0FBQztBQUN6QixxQ0FBbUIsR0FBRyxZQUFZLENBQUM7O1lBUDdDLElBQUksU0FBQztnQkFDRixJQUFJLEVBQUUsa0JBQWtCO2dCQUN4QixJQUFJLEVBQUUsS0FBSzthQUNkOzs7O1lBUFEsc0JBQXNCO1lBRHRCLGdCQUFnQjs7OztJQVdyQixpQ0FBZ0M7O0lBQ2hDLHNDQUEwQzs7SUFFMUMsMENBQXlEOztJQUN6RCwwQ0FBOEQ7Ozs7O0lBRTlELHVDQUE0Qzs7SUFFaEMsa0RBQXFEOztJQUNyRCxzQ0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgRGF0ZVBpcGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtLCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQXBwQ29uZmlnU2VydmljZSB9IGZyb20gJy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXNlclByZWZlcmVuY2VzU2VydmljZSwgVXNlclByZWZlcmVuY2VWYWx1ZXMgfSBmcm9tICcuLi9zZXJ2aWNlcy91c2VyLXByZWZlcmVuY2VzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBQaXBlKHtcclxuICAgIG5hbWU6ICdhZGZMb2NhbGl6ZWREYXRlJyxcclxuICAgIHB1cmU6IGZhbHNlXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMb2NhbGl6ZWREYXRlUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0sIE9uRGVzdHJveSB7XHJcblxyXG4gICAgc3RhdGljIERFRkFVTFRfTE9DQUxFID0gJ2VuLVVTJztcclxuICAgIHN0YXRpYyBERUZBVUxUX0RBVEVfRk9STUFUID0gJ21lZGl1bURhdGUnO1xyXG5cclxuICAgIGRlZmF1bHRMb2NhbGU6IHN0cmluZyA9IExvY2FsaXplZERhdGVQaXBlLkRFRkFVTFRfTE9DQUxFO1xyXG4gICAgZGVmYXVsdEZvcm1hdDogc3RyaW5nID0gTG9jYWxpemVkRGF0ZVBpcGUuREVGQVVMVF9EQVRFX0ZPUk1BVDtcclxuXHJcbiAgICBwcml2YXRlIG9uRGVzdHJveSQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyB1c2VyUHJlZmVyZW5jZVNlcnZpY2U/OiBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIGFwcENvbmZpZz86IEFwcENvbmZpZ1NlcnZpY2UpIHtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMudXNlclByZWZlcmVuY2VTZXJ2aWNlKSB7XHJcbiAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VTZXJ2aWNlXHJcbiAgICAgICAgICAgICAgICAuc2VsZWN0KFVzZXJQcmVmZXJlbmNlVmFsdWVzLkxvY2FsZSlcclxuICAgICAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLm9uRGVzdHJveSQpKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShsb2NhbGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChsb2NhbGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5kZWZhdWx0TG9jYWxlID0gbG9jYWxlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuYXBwQ29uZmlnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGVmYXVsdEZvcm1hdCA9IHRoaXMuYXBwQ29uZmlnLmdldDxzdHJpbmc+KCdkYXRlVmFsdWVzLmRlZmF1bHREYXRlRm9ybWF0JywgTG9jYWxpemVkRGF0ZVBpcGUuREVGQVVMVF9EQVRFX0ZPUk1BVCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHRyYW5zZm9ybSh2YWx1ZTogYW55LCBmb3JtYXQ/OiBzdHJpbmcsIGxvY2FsZT86IHN0cmluZyk6IGFueSB7XHJcbiAgICAgICAgY29uc3QgYWN0dWFsRm9ybWF0ID0gZm9ybWF0IHx8IHRoaXMuZGVmYXVsdEZvcm1hdDtcclxuICAgICAgICBjb25zdCBhY3R1YWxMb2NhbGUgPSBsb2NhbGUgfHwgdGhpcy5kZWZhdWx0TG9jYWxlO1xyXG4gICAgICAgIGNvbnN0IGRhdGVQaXBlOiBEYXRlUGlwZSA9IG5ldyBEYXRlUGlwZShhY3R1YWxMb2NhbGUpO1xyXG4gICAgICAgIHJldHVybiBkYXRlUGlwZS50cmFuc2Zvcm0odmFsdWUsIGFjdHVhbEZvcm1hdCk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAgICAgdGhpcy5vbkRlc3Ryb3kkLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5vbkRlc3Ryb3kkLmNvbXBsZXRlKCk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==