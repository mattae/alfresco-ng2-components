/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Pipe } from '@angular/core';
export class MultiValuePipe {
    /**
     * @param {?} values
     * @param {?=} valueSeparator
     * @return {?}
     */
    transform(values, valueSeparator = MultiValuePipe.DEFAULT_SEPARATOR) {
        if (values && values instanceof Array) {
            /** @type {?} */
            const valueList = values.map((/**
             * @param {?} value
             * @return {?}
             */
            (value) => value.trim()));
            return valueList.join(valueSeparator);
        }
        return (/** @type {?} */ (values));
    }
}
MultiValuePipe.DEFAULT_SEPARATOR = ', ';
MultiValuePipe.decorators = [
    { type: Pipe, args: [{ name: 'multiValue' },] }
];
if (false) {
    /** @type {?} */
    MultiValuePipe.DEFAULT_SEPARATOR;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXVsdGktdmFsdWUucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInBpcGVzL211bHRpLXZhbHVlLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFHcEQsTUFBTSxPQUFPLGNBQWM7Ozs7OztJQUl2QixTQUFTLENBQUMsTUFBMEIsRUFBRSxpQkFBeUIsY0FBYyxDQUFDLGlCQUFpQjtRQUUzRixJQUFJLE1BQU0sSUFBSSxNQUFNLFlBQVksS0FBSyxFQUFFOztrQkFDN0IsU0FBUyxHQUFHLE1BQU0sQ0FBQyxHQUFHOzs7O1lBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsRUFBQztZQUNyRCxPQUFPLFNBQVMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7U0FDekM7UUFFRCxPQUFPLG1CQUFTLE1BQU0sRUFBQSxDQUFDO0lBQzNCLENBQUM7O0FBVk0sZ0NBQWlCLEdBQUcsSUFBSSxDQUFDOztZQUhuQyxJQUFJLFNBQUMsRUFBRSxJQUFJLEVBQUUsWUFBWSxFQUFFOzs7O0lBR3hCLGlDQUFnQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AUGlwZSh7IG5hbWU6ICdtdWx0aVZhbHVlJyB9KVxyXG5leHBvcnQgY2xhc3MgTXVsdGlWYWx1ZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuXHJcbiAgICBzdGF0aWMgREVGQVVMVF9TRVBBUkFUT1IgPSAnLCAnO1xyXG5cclxuICAgIHRyYW5zZm9ybSh2YWx1ZXM6IHN0cmluZyB8IHN0cmluZyBbXSwgdmFsdWVTZXBhcmF0b3I6IHN0cmluZyA9IE11bHRpVmFsdWVQaXBlLkRFRkFVTFRfU0VQQVJBVE9SKTogc3RyaW5nIHtcclxuXHJcbiAgICAgICAgaWYgKHZhbHVlcyAmJiB2YWx1ZXMgaW5zdGFuY2VvZiBBcnJheSkge1xyXG4gICAgICAgICAgICBjb25zdCB2YWx1ZUxpc3QgPSB2YWx1ZXMubWFwKCh2YWx1ZSkgPT4gdmFsdWUudHJpbSgpKTtcclxuICAgICAgICAgICAgcmV0dXJuIHZhbHVlTGlzdC5qb2luKHZhbHVlU2VwYXJhdG9yKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiA8c3RyaW5nPiB2YWx1ZXM7XHJcbiAgICB9XHJcbn1cclxuIl19