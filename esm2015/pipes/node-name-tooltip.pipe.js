/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Pipe } from '@angular/core';
export class NodeNameTooltipPipe {
    /**
     * @param {?} node
     * @return {?}
     */
    transform(node) {
        if (node) {
            return this.getNodeTooltip(node);
        }
        return null;
    }
    /**
     * @private
     * @param {?} lines
     * @param {?} line
     * @return {?}
     */
    containsLine(lines, line) {
        return lines.some((/**
         * @param {?} item
         * @return {?}
         */
        (item) => {
            return item.toLowerCase() === line.toLowerCase();
        }));
    }
    /**
     * @private
     * @param {?} lines
     * @return {?}
     */
    removeDuplicateLines(lines) {
        /** @type {?} */
        const reducer = (/**
         * @param {?} acc
         * @param {?} line
         * @return {?}
         */
        (acc, line) => {
            if (!this.containsLine(acc, line)) {
                acc.push(line);
            }
            return acc;
        });
        return lines.reduce(reducer, []);
    }
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    getNodeTooltip(node) {
        if (!node || !node.entry) {
            return null;
        }
        const { entry: { properties, name } } = node;
        /** @type {?} */
        const lines = [name];
        if (properties) {
            const { 'cm:title': title, 'cm:description': description } = properties;
            if (title && description) {
                lines[0] = title;
                lines[1] = description;
            }
            if (title) {
                lines[1] = title;
            }
            if (description) {
                lines[1] = description;
            }
        }
        return this.removeDuplicateLines(lines).join(`\n`);
    }
}
NodeNameTooltipPipe.decorators = [
    { type: Pipe, args: [{
                name: 'adfNodeNameTooltip'
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9kZS1uYW1lLXRvb2x0aXAucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInBpcGVzL25vZGUtbmFtZS10b29sdGlwLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFNcEQsTUFBTSxPQUFPLG1CQUFtQjs7Ozs7SUFFNUIsU0FBUyxDQUFDLElBQWU7UUFDckIsSUFBSSxJQUFJLEVBQUU7WUFDTixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDcEM7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOzs7Ozs7O0lBRU8sWUFBWSxDQUFDLEtBQWUsRUFBRSxJQUFZO1FBQzlDLE9BQU8sS0FBSyxDQUFDLElBQUk7Ozs7UUFBQyxDQUFDLElBQVksRUFBRSxFQUFFO1lBQy9CLE9BQU8sSUFBSSxDQUFDLFdBQVcsRUFBRSxLQUFLLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNyRCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQUVPLG9CQUFvQixDQUFDLEtBQWU7O2NBQ2xDLE9BQU87Ozs7O1FBQUcsQ0FBQyxHQUFhLEVBQUUsSUFBWSxFQUFZLEVBQUU7WUFDdEQsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxFQUFFO2dCQUFFLEdBQUcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFBRTtZQUN0RCxPQUFPLEdBQUcsQ0FBQztRQUNmLENBQUMsQ0FBQTtRQUVELE9BQU8sS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDckMsQ0FBQzs7Ozs7O0lBRU8sY0FBYyxDQUFDLElBQWU7UUFDbEMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUU7WUFDdEIsT0FBTyxJQUFJLENBQUM7U0FDZjtjQUVLLEVBQUUsS0FBSyxFQUFFLEVBQUUsVUFBVSxFQUFFLElBQUksRUFBRSxFQUFFLEdBQUcsSUFBSTs7Y0FDdEMsS0FBSyxHQUFHLENBQUUsSUFBSSxDQUFFO1FBRXRCLElBQUksVUFBVSxFQUFFO2tCQUNOLEVBQ0YsVUFBVSxFQUFFLEtBQUssRUFDakIsZ0JBQWdCLEVBQUUsV0FBVyxFQUNoQyxHQUFHLFVBQVU7WUFFZCxJQUFJLEtBQUssSUFBSSxXQUFXLEVBQUU7Z0JBQ3RCLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUM7Z0JBQ2pCLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxXQUFXLENBQUM7YUFDMUI7WUFFRCxJQUFJLEtBQUssRUFBRTtnQkFDUCxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDO2FBQ3BCO1lBRUQsSUFBSSxXQUFXLEVBQUU7Z0JBQ2IsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLFdBQVcsQ0FBQzthQUMxQjtTQUNKO1FBRUQsT0FBTyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3ZELENBQUM7OztZQXhESixJQUFJLFNBQUM7Z0JBQ0YsSUFBSSxFQUFFLG9CQUFvQjthQUM3QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5vZGVFbnRyeSB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5cclxuQFBpcGUoe1xyXG4gICAgbmFtZTogJ2FkZk5vZGVOYW1lVG9vbHRpcCdcclxufSlcclxuZXhwb3J0IGNsYXNzIE5vZGVOYW1lVG9vbHRpcFBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuXHJcbiAgICB0cmFuc2Zvcm0obm9kZTogTm9kZUVudHJ5KTogc3RyaW5nIHtcclxuICAgICAgICBpZiAobm9kZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5nZXROb2RlVG9vbHRpcChub2RlKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBjb250YWluc0xpbmUobGluZXM6IHN0cmluZ1tdLCBsaW5lOiBzdHJpbmcpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gbGluZXMuc29tZSgoaXRlbTogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiBpdGVtLnRvTG93ZXJDYXNlKCkgPT09IGxpbmUudG9Mb3dlckNhc2UoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHJlbW92ZUR1cGxpY2F0ZUxpbmVzKGxpbmVzOiBzdHJpbmdbXSk6IHN0cmluZ1tdIHtcclxuICAgICAgICBjb25zdCByZWR1Y2VyID0gKGFjYzogc3RyaW5nW10sIGxpbmU6IHN0cmluZyk6IHN0cmluZ1tdID0+IHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLmNvbnRhaW5zTGluZShhY2MsIGxpbmUpKSB7IGFjYy5wdXNoKGxpbmUpOyB9XHJcbiAgICAgICAgICAgIHJldHVybiBhY2M7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGxpbmVzLnJlZHVjZShyZWR1Y2VyLCBbXSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXROb2RlVG9vbHRpcChub2RlOiBOb2RlRW50cnkpOiBzdHJpbmcge1xyXG4gICAgICAgIGlmICghbm9kZSB8fCAhbm9kZS5lbnRyeSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHsgZW50cnk6IHsgcHJvcGVydGllcywgbmFtZSB9IH0gPSBub2RlO1xyXG4gICAgICAgIGNvbnN0IGxpbmVzID0gWyBuYW1lIF07XHJcblxyXG4gICAgICAgIGlmIChwcm9wZXJ0aWVzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHtcclxuICAgICAgICAgICAgICAgICdjbTp0aXRsZSc6IHRpdGxlLFxyXG4gICAgICAgICAgICAgICAgJ2NtOmRlc2NyaXB0aW9uJzogZGVzY3JpcHRpb25cclxuICAgICAgICAgICAgfSA9IHByb3BlcnRpZXM7XHJcblxyXG4gICAgICAgICAgICBpZiAodGl0bGUgJiYgZGVzY3JpcHRpb24pIHtcclxuICAgICAgICAgICAgICAgIGxpbmVzWzBdID0gdGl0bGU7XHJcbiAgICAgICAgICAgICAgICBsaW5lc1sxXSA9IGRlc2NyaXB0aW9uO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAodGl0bGUpIHtcclxuICAgICAgICAgICAgICAgIGxpbmVzWzFdID0gdGl0bGU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChkZXNjcmlwdGlvbikge1xyXG4gICAgICAgICAgICAgICAgbGluZXNbMV0gPSBkZXNjcmlwdGlvbjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMucmVtb3ZlRHVwbGljYXRlTGluZXMobGluZXMpLmpvaW4oYFxcbmApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==