/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FileSizePipe } from './file-size.pipe';
import { MimeTypeIconPipe } from './mime-type-icon.pipe';
import { NodeNameTooltipPipe } from './node-name-tooltip.pipe';
import { HighlightPipe } from './text-highlight.pipe';
import { TimeAgoPipe } from './time-ago.pipe';
import { InitialUsernamePipe } from './user-initial.pipe';
import { FullNamePipe } from './full-name.pipe';
import { FormatSpacePipe } from './format-space.pipe';
import { FileTypePipe } from './file-type.pipe';
import { MultiValuePipe } from './multi-value.pipe';
import { LocalizedDatePipe } from './localized-date.pipe';
import { DecimalNumberPipe } from './decimal-number.pipe';
export class PipeModule {
}
PipeModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [
                    FileSizePipe,
                    HighlightPipe,
                    TimeAgoPipe,
                    MimeTypeIconPipe,
                    InitialUsernamePipe,
                    FullNamePipe,
                    NodeNameTooltipPipe,
                    FormatSpacePipe,
                    FileTypePipe,
                    MultiValuePipe,
                    LocalizedDatePipe,
                    DecimalNumberPipe
                ],
                providers: [
                    FileSizePipe,
                    HighlightPipe,
                    TimeAgoPipe,
                    MimeTypeIconPipe,
                    InitialUsernamePipe,
                    NodeNameTooltipPipe,
                    FormatSpacePipe,
                    FileTypePipe,
                    MultiValuePipe,
                    LocalizedDatePipe,
                    DecimalNumberPipe
                ],
                exports: [
                    FileSizePipe,
                    HighlightPipe,
                    TimeAgoPipe,
                    MimeTypeIconPipe,
                    InitialUsernamePipe,
                    FullNamePipe,
                    NodeNameTooltipPipe,
                    FormatSpacePipe,
                    FileTypePipe,
                    MultiValuePipe,
                    LocalizedDatePipe,
                    DecimalNumberPipe
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGlwZS5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJwaXBlcy9waXBlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUV6QyxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDaEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDekQsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDL0QsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3RELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUM5QyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFDaEQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDO0FBQ3RELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNoRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDMUQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFnRDFELE1BQU0sT0FBTyxVQUFVOzs7WUE5Q3RCLFFBQVEsU0FBQztnQkFDTixPQUFPLEVBQUU7b0JBQ0wsWUFBWTtpQkFDZjtnQkFDRCxZQUFZLEVBQUU7b0JBQ1YsWUFBWTtvQkFDWixhQUFhO29CQUNiLFdBQVc7b0JBQ1gsZ0JBQWdCO29CQUNoQixtQkFBbUI7b0JBQ25CLFlBQVk7b0JBQ1osbUJBQW1CO29CQUNuQixlQUFlO29CQUNmLFlBQVk7b0JBQ1osY0FBYztvQkFDZCxpQkFBaUI7b0JBQ2pCLGlCQUFpQjtpQkFDcEI7Z0JBQ0QsU0FBUyxFQUFFO29CQUNQLFlBQVk7b0JBQ1osYUFBYTtvQkFDYixXQUFXO29CQUNYLGdCQUFnQjtvQkFDaEIsbUJBQW1CO29CQUNuQixtQkFBbUI7b0JBQ25CLGVBQWU7b0JBQ2YsWUFBWTtvQkFDWixjQUFjO29CQUNkLGlCQUFpQjtvQkFDakIsaUJBQWlCO2lCQUNwQjtnQkFDRCxPQUFPLEVBQUU7b0JBQ0wsWUFBWTtvQkFDWixhQUFhO29CQUNiLFdBQVc7b0JBQ1gsZ0JBQWdCO29CQUNoQixtQkFBbUI7b0JBQ25CLFlBQVk7b0JBQ1osbUJBQW1CO29CQUNuQixlQUFlO29CQUNmLFlBQVk7b0JBQ1osY0FBYztvQkFDZCxpQkFBaUI7b0JBQ2pCLGlCQUFpQjtpQkFDcEI7YUFDSiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDb21tb25Nb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuaW1wb3J0IHsgRmlsZVNpemVQaXBlIH0gZnJvbSAnLi9maWxlLXNpemUucGlwZSc7XHJcbmltcG9ydCB7IE1pbWVUeXBlSWNvblBpcGUgfSBmcm9tICcuL21pbWUtdHlwZS1pY29uLnBpcGUnO1xyXG5pbXBvcnQgeyBOb2RlTmFtZVRvb2x0aXBQaXBlIH0gZnJvbSAnLi9ub2RlLW5hbWUtdG9vbHRpcC5waXBlJztcclxuaW1wb3J0IHsgSGlnaGxpZ2h0UGlwZSB9IGZyb20gJy4vdGV4dC1oaWdobGlnaHQucGlwZSc7XHJcbmltcG9ydCB7IFRpbWVBZ29QaXBlIH0gZnJvbSAnLi90aW1lLWFnby5waXBlJztcclxuaW1wb3J0IHsgSW5pdGlhbFVzZXJuYW1lUGlwZSB9IGZyb20gJy4vdXNlci1pbml0aWFsLnBpcGUnO1xyXG5pbXBvcnQgeyBGdWxsTmFtZVBpcGUgfSBmcm9tICcuL2Z1bGwtbmFtZS5waXBlJztcclxuaW1wb3J0IHsgRm9ybWF0U3BhY2VQaXBlIH0gZnJvbSAnLi9mb3JtYXQtc3BhY2UucGlwZSc7XHJcbmltcG9ydCB7IEZpbGVUeXBlUGlwZSB9IGZyb20gJy4vZmlsZS10eXBlLnBpcGUnO1xyXG5pbXBvcnQgeyBNdWx0aVZhbHVlUGlwZSB9IGZyb20gJy4vbXVsdGktdmFsdWUucGlwZSc7XHJcbmltcG9ydCB7IExvY2FsaXplZERhdGVQaXBlIH0gZnJvbSAnLi9sb2NhbGl6ZWQtZGF0ZS5waXBlJztcclxuaW1wb3J0IHsgRGVjaW1hbE51bWJlclBpcGUgfSBmcm9tICcuL2RlY2ltYWwtbnVtYmVyLnBpcGUnO1xyXG5cclxuQE5nTW9kdWxlKHtcclxuICAgIGltcG9ydHM6IFtcclxuICAgICAgICBDb21tb25Nb2R1bGVcclxuICAgIF0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtcclxuICAgICAgICBGaWxlU2l6ZVBpcGUsXHJcbiAgICAgICAgSGlnaGxpZ2h0UGlwZSxcclxuICAgICAgICBUaW1lQWdvUGlwZSxcclxuICAgICAgICBNaW1lVHlwZUljb25QaXBlLFxyXG4gICAgICAgIEluaXRpYWxVc2VybmFtZVBpcGUsXHJcbiAgICAgICAgRnVsbE5hbWVQaXBlLFxyXG4gICAgICAgIE5vZGVOYW1lVG9vbHRpcFBpcGUsXHJcbiAgICAgICAgRm9ybWF0U3BhY2VQaXBlLFxyXG4gICAgICAgIEZpbGVUeXBlUGlwZSxcclxuICAgICAgICBNdWx0aVZhbHVlUGlwZSxcclxuICAgICAgICBMb2NhbGl6ZWREYXRlUGlwZSxcclxuICAgICAgICBEZWNpbWFsTnVtYmVyUGlwZVxyXG4gICAgXSxcclxuICAgIHByb3ZpZGVyczogW1xyXG4gICAgICAgIEZpbGVTaXplUGlwZSxcclxuICAgICAgICBIaWdobGlnaHRQaXBlLFxyXG4gICAgICAgIFRpbWVBZ29QaXBlLFxyXG4gICAgICAgIE1pbWVUeXBlSWNvblBpcGUsXHJcbiAgICAgICAgSW5pdGlhbFVzZXJuYW1lUGlwZSxcclxuICAgICAgICBOb2RlTmFtZVRvb2x0aXBQaXBlLFxyXG4gICAgICAgIEZvcm1hdFNwYWNlUGlwZSxcclxuICAgICAgICBGaWxlVHlwZVBpcGUsXHJcbiAgICAgICAgTXVsdGlWYWx1ZVBpcGUsXHJcbiAgICAgICAgTG9jYWxpemVkRGF0ZVBpcGUsXHJcbiAgICAgICAgRGVjaW1hbE51bWJlclBpcGVcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgRmlsZVNpemVQaXBlLFxyXG4gICAgICAgIEhpZ2hsaWdodFBpcGUsXHJcbiAgICAgICAgVGltZUFnb1BpcGUsXHJcbiAgICAgICAgTWltZVR5cGVJY29uUGlwZSxcclxuICAgICAgICBJbml0aWFsVXNlcm5hbWVQaXBlLFxyXG4gICAgICAgIEZ1bGxOYW1lUGlwZSxcclxuICAgICAgICBOb2RlTmFtZVRvb2x0aXBQaXBlLFxyXG4gICAgICAgIEZvcm1hdFNwYWNlUGlwZSxcclxuICAgICAgICBGaWxlVHlwZVBpcGUsXHJcbiAgICAgICAgTXVsdGlWYWx1ZVBpcGUsXHJcbiAgICAgICAgTG9jYWxpemVkRGF0ZVBpcGUsXHJcbiAgICAgICAgRGVjaW1hbE51bWJlclBpcGVcclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFBpcGVNb2R1bGUge1xyXG59XHJcbiJdfQ==