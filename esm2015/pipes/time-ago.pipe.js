/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import moment from 'moment-es6';
import { Pipe } from '@angular/core';
import { AppConfigService } from '../app-config/app-config.service';
import { UserPreferenceValues, UserPreferencesService } from '../services/user-preferences.service';
import { DatePipe } from '@angular/common';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
export class TimeAgoPipe {
    /**
     * @param {?} userPreferenceService
     * @param {?} appConfig
     */
    constructor(userPreferenceService, appConfig) {
        this.userPreferenceService = userPreferenceService;
        this.appConfig = appConfig;
        this.onDestroy$ = new Subject();
        this.userPreferenceService
            .select(UserPreferenceValues.Locale)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} locale
         * @return {?}
         */
        locale => {
            this.defaultLocale = locale || TimeAgoPipe.DEFAULT_LOCALE;
        }));
        this.defaultDateTimeFormat = this.appConfig.get('dateValues.defaultDateTimeFormat', TimeAgoPipe.DEFAULT_DATE_TIME_FORMAT);
    }
    /**
     * @param {?} value
     * @param {?=} locale
     * @return {?}
     */
    transform(value, locale) {
        if (value !== null && value !== undefined) {
            /** @type {?} */
            const actualLocale = locale || this.defaultLocale;
            /** @type {?} */
            const then = moment(value);
            /** @type {?} */
            const diff = moment().locale(actualLocale).diff(then, 'days');
            if (diff > 7) {
                /** @type {?} */
                const datePipe = new DatePipe(actualLocale);
                return datePipe.transform(value, this.defaultDateTimeFormat);
            }
            else {
                return then.locale(actualLocale).fromNow();
            }
        }
        return '';
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    }
}
TimeAgoPipe.DEFAULT_LOCALE = 'en-US';
TimeAgoPipe.DEFAULT_DATE_TIME_FORMAT = 'dd/MM/yyyy HH:mm';
TimeAgoPipe.decorators = [
    { type: Pipe, args: [{
                name: 'adfTimeAgo'
            },] }
];
/** @nocollapse */
TimeAgoPipe.ctorParameters = () => [
    { type: UserPreferencesService },
    { type: AppConfigService }
];
if (false) {
    /** @type {?} */
    TimeAgoPipe.DEFAULT_LOCALE;
    /** @type {?} */
    TimeAgoPipe.DEFAULT_DATE_TIME_FORMAT;
    /** @type {?} */
    TimeAgoPipe.prototype.defaultLocale;
    /** @type {?} */
    TimeAgoPipe.prototype.defaultDateTimeFormat;
    /**
     * @type {?}
     * @private
     */
    TimeAgoPipe.prototype.onDestroy$;
    /** @type {?} */
    TimeAgoPipe.prototype.userPreferenceService;
    /** @type {?} */
    TimeAgoPipe.prototype.appConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZS1hZ28ucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInBpcGVzL3RpbWUtYWdvLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxNQUFNLE1BQU0sWUFBWSxDQUFDO0FBQ2hDLE9BQU8sRUFBRSxJQUFJLEVBQTRCLE1BQU0sZUFBZSxDQUFDO0FBQy9ELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ3BHLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMzQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQy9CLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUszQyxNQUFNLE9BQU8sV0FBVzs7Ozs7SUFVcEIsWUFBbUIscUJBQTZDLEVBQzdDLFNBQTJCO1FBRDNCLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBd0I7UUFDN0MsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFIdEMsZUFBVSxHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7UUFJeEMsSUFBSSxDQUFDLHFCQUFxQjthQUNyQixNQUFNLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDO2FBQ25DLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2hDLFNBQVM7Ozs7UUFBQyxNQUFNLENBQUMsRUFBRTtZQUNoQixJQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sSUFBSSxXQUFXLENBQUMsY0FBYyxDQUFDO1FBQzlELENBQUMsRUFBQyxDQUFDO1FBQ1AsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFTLGtDQUFrQyxFQUFFLFdBQVcsQ0FBQyx3QkFBd0IsQ0FBQyxDQUFDO0lBQ3RJLENBQUM7Ozs7OztJQUVELFNBQVMsQ0FBQyxLQUFXLEVBQUUsTUFBZTtRQUNsQyxJQUFJLEtBQUssS0FBSyxJQUFJLElBQUksS0FBSyxLQUFLLFNBQVMsRUFBRzs7a0JBQ2xDLFlBQVksR0FBRyxNQUFNLElBQUksSUFBSSxDQUFDLGFBQWE7O2tCQUMzQyxJQUFJLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQzs7a0JBQ3BCLElBQUksR0FBRyxNQUFNLEVBQUUsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUM7WUFDN0QsSUFBSyxJQUFJLEdBQUcsQ0FBQyxFQUFFOztzQkFDTCxRQUFRLEdBQWEsSUFBSSxRQUFRLENBQUMsWUFBWSxDQUFDO2dCQUNyRCxPQUFPLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO2FBQ2hFO2lCQUFNO2dCQUNILE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUM5QztTQUNKO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDOzs7O0lBRUQsV0FBVztRQUNQLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDL0IsQ0FBQzs7QUFyQ00sMEJBQWMsR0FBRyxPQUFPLENBQUM7QUFDekIsb0NBQXdCLEdBQUcsa0JBQWtCLENBQUM7O1lBTnhELElBQUksU0FBQztnQkFDRixJQUFJLEVBQUUsWUFBWTthQUNyQjs7OztZQVA4QixzQkFBc0I7WUFENUMsZ0JBQWdCOzs7O0lBV3JCLDJCQUFnQzs7SUFDaEMscUNBQXFEOztJQUVyRCxvQ0FBc0I7O0lBQ3RCLDRDQUE4Qjs7Ozs7SUFFOUIsaUNBQTRDOztJQUVoQyw0Q0FBb0Q7O0lBQ3BELGdDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgbW9tZW50IGZyb20gJ21vbWVudC1lczYnO1xyXG5pbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtLCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQXBwQ29uZmlnU2VydmljZSB9IGZyb20gJy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXNlclByZWZlcmVuY2VWYWx1ZXMsIFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy91c2VyLXByZWZlcmVuY2VzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEYXRlUGlwZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQFBpcGUoe1xyXG4gICAgbmFtZTogJ2FkZlRpbWVBZ28nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUaW1lQWdvUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0sIE9uRGVzdHJveSB7XHJcblxyXG4gICAgc3RhdGljIERFRkFVTFRfTE9DQUxFID0gJ2VuLVVTJztcclxuICAgIHN0YXRpYyBERUZBVUxUX0RBVEVfVElNRV9GT1JNQVQgPSAnZGQvTU0veXl5eSBISDptbSc7XHJcblxyXG4gICAgZGVmYXVsdExvY2FsZTogc3RyaW5nO1xyXG4gICAgZGVmYXVsdERhdGVUaW1lRm9ybWF0OiBzdHJpbmc7XHJcblxyXG4gICAgcHJpdmF0ZSBvbkRlc3Ryb3kkID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgdXNlclByZWZlcmVuY2VTZXJ2aWNlOiBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIGFwcENvbmZpZzogQXBwQ29uZmlnU2VydmljZSkge1xyXG4gICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5zZWxlY3QoVXNlclByZWZlcmVuY2VWYWx1ZXMuTG9jYWxlKVxyXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5vbkRlc3Ryb3kkKSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZShsb2NhbGUgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kZWZhdWx0TG9jYWxlID0gbG9jYWxlIHx8IFRpbWVBZ29QaXBlLkRFRkFVTFRfTE9DQUxFO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB0aGlzLmRlZmF1bHREYXRlVGltZUZvcm1hdCA9IHRoaXMuYXBwQ29uZmlnLmdldDxzdHJpbmc+KCdkYXRlVmFsdWVzLmRlZmF1bHREYXRlVGltZUZvcm1hdCcsIFRpbWVBZ29QaXBlLkRFRkFVTFRfREFURV9USU1FX0ZPUk1BVCk7XHJcbiAgICB9XHJcblxyXG4gICAgdHJhbnNmb3JtKHZhbHVlOiBEYXRlLCBsb2NhbGU/OiBzdHJpbmcpIHtcclxuICAgICAgICBpZiAodmFsdWUgIT09IG51bGwgJiYgdmFsdWUgIT09IHVuZGVmaW5lZCApIHtcclxuICAgICAgICAgICAgY29uc3QgYWN0dWFsTG9jYWxlID0gbG9jYWxlIHx8IHRoaXMuZGVmYXVsdExvY2FsZTtcclxuICAgICAgICAgICAgY29uc3QgdGhlbiA9IG1vbWVudCh2YWx1ZSk7XHJcbiAgICAgICAgICAgIGNvbnN0IGRpZmYgPSBtb21lbnQoKS5sb2NhbGUoYWN0dWFsTG9jYWxlKS5kaWZmKHRoZW4sICdkYXlzJyk7XHJcbiAgICAgICAgICAgIGlmICggZGlmZiA+IDcpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGRhdGVQaXBlOiBEYXRlUGlwZSA9IG5ldyBEYXRlUGlwZShhY3R1YWxMb2NhbGUpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGRhdGVQaXBlLnRyYW5zZm9ybSh2YWx1ZSwgdGhpcy5kZWZhdWx0RGF0ZVRpbWVGb3JtYXQpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoZW4ubG9jYWxlKGFjdHVhbExvY2FsZSkuZnJvbU5vdygpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiAnJztcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQubmV4dCh0cnVlKTtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQuY29tcGxldGUoKTtcclxuICAgIH1cclxufVxyXG4iXX0=