/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Pipe } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
export class InitialUsernamePipe {
    /**
     * @param {?} sanitized
     */
    constructor(sanitized) {
        this.sanitized = sanitized;
    }
    /**
     * @param {?} user
     * @param {?=} className
     * @param {?=} delimiter
     * @return {?}
     */
    transform(user, className = '', delimiter = '') {
        /** @type {?} */
        let safeHtml = '';
        if (user) {
            /** @type {?} */
            const initialResult = this.getInitialUserName(user.firstName, user.lastName, delimiter);
            safeHtml = this.sanitized.bypassSecurityTrustHtml(`<div id="user-initials-image" class="${className}">${initialResult}</div>`);
        }
        return safeHtml;
    }
    /**
     * @param {?} firstName
     * @param {?} lastName
     * @param {?} delimiter
     * @return {?}
     */
    getInitialUserName(firstName, lastName, delimiter) {
        firstName = (firstName ? firstName[0] : '');
        lastName = (lastName ? lastName[0] : '');
        return firstName + delimiter + lastName;
    }
}
InitialUsernamePipe.decorators = [
    { type: Pipe, args: [{
                name: 'usernameInitials'
            },] }
];
/** @nocollapse */
InitialUsernamePipe.ctorParameters = () => [
    { type: DomSanitizer }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    InitialUsernamePipe.prototype.sanitized;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1pbml0aWFsLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJwaXBlcy91c2VyLWluaXRpYWwucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUNwRCxPQUFPLEVBQUUsWUFBWSxFQUFZLE1BQU0sMkJBQTJCLENBQUM7QUFPbkUsTUFBTSxPQUFPLG1CQUFtQjs7OztJQUU1QixZQUFvQixTQUF1QjtRQUF2QixjQUFTLEdBQVQsU0FBUyxDQUFjO0lBQzNDLENBQUM7Ozs7Ozs7SUFFRCxTQUFTLENBQUMsSUFBcUMsRUFBRSxZQUFvQixFQUFFLEVBQUUsWUFBb0IsRUFBRTs7WUFDdkYsUUFBUSxHQUFhLEVBQUU7UUFDM0IsSUFBSSxJQUFJLEVBQUU7O2tCQUNBLGFBQWEsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLFNBQVMsQ0FBQztZQUN2RixRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyx1QkFBdUIsQ0FBQyx3Q0FBd0MsU0FBUyxLQUFLLGFBQWEsUUFBUSxDQUFDLENBQUM7U0FDbEk7UUFDRCxPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDOzs7Ozs7O0lBRUQsa0JBQWtCLENBQUMsU0FBaUIsRUFBRSxRQUFnQixFQUFFLFNBQWlCO1FBQ3JFLFNBQVMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztRQUM1QyxRQUFRLEdBQUcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDekMsT0FBTyxTQUFTLEdBQUcsU0FBUyxHQUFHLFFBQVEsQ0FBQztJQUM1QyxDQUFDOzs7WUFyQkosSUFBSSxTQUFDO2dCQUNGLElBQUksRUFBRSxrQkFBa0I7YUFDM0I7Ozs7WUFOUSxZQUFZOzs7Ozs7O0lBU0wsd0NBQStCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRG9tU2FuaXRpemVyLCBTYWZlSHRtbCB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xyXG5pbXBvcnQgeyBVc2VyUHJvY2Vzc01vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL3VzZXItcHJvY2Vzcy5tb2RlbCc7XHJcbmltcG9ydCB7IEVjbVVzZXJNb2RlbCB9IGZyb20gJy4uL3VzZXJpbmZvL21vZGVscy9lY20tdXNlci5tb2RlbCc7XHJcblxyXG5AUGlwZSh7XHJcbiAgICBuYW1lOiAndXNlcm5hbWVJbml0aWFscydcclxufSlcclxuZXhwb3J0IGNsYXNzIEluaXRpYWxVc2VybmFtZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNhbml0aXplZDogRG9tU2FuaXRpemVyKSB7XHJcbiAgICB9XHJcblxyXG4gICAgdHJhbnNmb3JtKHVzZXI6IFVzZXJQcm9jZXNzTW9kZWwgfCBFY21Vc2VyTW9kZWwsIGNsYXNzTmFtZTogc3RyaW5nID0gJycsIGRlbGltaXRlcjogc3RyaW5nID0gJycpOiBTYWZlSHRtbCB7XHJcbiAgICAgICAgbGV0IHNhZmVIdG1sOiBTYWZlSHRtbCA9ICcnO1xyXG4gICAgICAgIGlmICh1c2VyKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGluaXRpYWxSZXN1bHQgPSB0aGlzLmdldEluaXRpYWxVc2VyTmFtZSh1c2VyLmZpcnN0TmFtZSwgdXNlci5sYXN0TmFtZSwgZGVsaW1pdGVyKTtcclxuICAgICAgICAgICAgc2FmZUh0bWwgPSB0aGlzLnNhbml0aXplZC5ieXBhc3NTZWN1cml0eVRydXN0SHRtbChgPGRpdiBpZD1cInVzZXItaW5pdGlhbHMtaW1hZ2VcIiBjbGFzcz1cIiR7Y2xhc3NOYW1lfVwiPiR7aW5pdGlhbFJlc3VsdH08L2Rpdj5gKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHNhZmVIdG1sO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEluaXRpYWxVc2VyTmFtZShmaXJzdE5hbWU6IHN0cmluZywgbGFzdE5hbWU6IHN0cmluZywgZGVsaW1pdGVyOiBzdHJpbmcpIHtcclxuICAgICAgICBmaXJzdE5hbWUgPSAoZmlyc3ROYW1lID8gZmlyc3ROYW1lWzBdIDogJycpO1xyXG4gICAgICAgIGxhc3ROYW1lID0gKGxhc3ROYW1lID8gbGFzdE5hbWVbMF0gOiAnJyk7XHJcbiAgICAgICAgcmV0dXJuIGZpcnN0TmFtZSArIGRlbGltaXRlciArIGxhc3ROYW1lO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==