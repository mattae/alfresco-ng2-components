/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { AlfrescoApiCompatibility, AlfrescoApiConfig } from '@alfresco/js-api';
import { AppConfigService, AppConfigValues } from '../app-config/app-config.service';
import { Subject } from 'rxjs';
import { StorageService } from './storage.service';
import * as i0 from "@angular/core";
import * as i1 from "../app-config/app-config.service";
import * as i2 from "./storage.service";
/* tslint:disable:adf-file-name */
export class AlfrescoApiService {
    /**
     * @param {?} appConfig
     * @param {?} storageService
     */
    constructor(appConfig, storageService) {
        this.appConfig = appConfig;
        this.storageService = storageService;
        /**
         * Publish/subscribe to events related to node updates.
         */
        this.nodeUpdated = new Subject();
        this.alfrescoApiInitializedSubject = new Subject();
        this.alfrescoApiInitialized = this.alfrescoApiInitializedSubject.asObservable();
    }
    /**
     * @return {?}
     */
    getInstance() {
        return this.alfrescoApi;
    }
    /**
     * @return {?}
     */
    get taskApi() {
        return this.getInstance().activiti.taskApi;
    }
    /**
     * @return {?}
     */
    get contentApi() {
        return this.getInstance().content;
    }
    /**
     * @return {?}
     */
    get nodesApi() {
        return this.getInstance().nodes;
    }
    /**
     * @return {?}
     */
    get renditionsApi() {
        return this.getInstance().core.renditionsApi;
    }
    /**
     * @return {?}
     */
    get sharedLinksApi() {
        return this.getInstance().core.sharedlinksApi;
    }
    /**
     * @return {?}
     */
    get sitesApi() {
        return this.getInstance().core.sitesApi;
    }
    /**
     * @return {?}
     */
    get favoritesApi() {
        return this.getInstance().core.favoritesApi;
    }
    /**
     * @return {?}
     */
    get peopleApi() {
        return this.getInstance().core.peopleApi;
    }
    /**
     * @return {?}
     */
    get searchApi() {
        return this.getInstance().search.searchApi;
    }
    /**
     * @return {?}
     */
    get versionsApi() {
        return this.getInstance().core.versionsApi;
    }
    /**
     * @return {?}
     */
    get classesApi() {
        return this.getInstance().core.classesApi;
    }
    /**
     * @return {?}
     */
    get groupsApi() {
        return this.getInstance().core.groupsApi;
    }
    /**
     * @return {?}
     */
    load() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            yield this.appConfig.load().then((/**
             * @return {?}
             */
            () => {
                this.storageService.prefix = this.appConfig.get(AppConfigValues.STORAGE_PREFIX, '');
                this.initAlfrescoApi();
                this.alfrescoApiInitializedSubject.next();
            }));
        });
    }
    /**
     * @return {?}
     */
    reset() {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            this.initAlfrescoApi();
        });
    }
    /**
     * @protected
     * @return {?}
     */
    initAlfrescoApi() {
        /** @type {?} */
        const oauth = Object.assign({}, this.appConfig.get(AppConfigValues.OAUTHCONFIG, null));
        if (oauth) {
            oauth.redirectUri = window.location.origin + (oauth.redirectUri || '/');
            oauth.redirectUriLogout = window.location.origin + (oauth.redirectUriLogout || '/');
        }
        /** @type {?} */
        const config = new AlfrescoApiConfig({
            provider: this.appConfig.get(AppConfigValues.PROVIDERS),
            hostEcm: this.appConfig.get(AppConfigValues.ECMHOST),
            hostBpm: this.appConfig.get(AppConfigValues.BPMHOST),
            authType: this.appConfig.get(AppConfigValues.AUTHTYPE, 'BASIC'),
            contextRootBpm: this.appConfig.get(AppConfigValues.CONTEXTROOTBPM),
            contextRoot: this.appConfig.get(AppConfigValues.CONTEXTROOTECM),
            disableCsrf: this.appConfig.get(AppConfigValues.DISABLECSRF),
            withCredentials: this.appConfig.get(AppConfigValues.AUTH_WITH_CREDENTIALS, false),
            oauth2: oauth
        });
        if (this.alfrescoApi && this.isDifferentConfig(this.lastConfig, config)) {
            this.lastConfig = config;
            this.alfrescoApi.configureJsApi(config);
        }
        else {
            this.lastConfig = config;
            this.alfrescoApi = new AlfrescoApiCompatibility(config);
        }
    }
    /**
     * @param {?} lastConfig
     * @param {?} newConfig
     * @return {?}
     */
    isDifferentConfig(lastConfig, newConfig) {
        return JSON.stringify(lastConfig) !== JSON.stringify(newConfig);
    }
}
AlfrescoApiService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
AlfrescoApiService.ctorParameters = () => [
    { type: AppConfigService },
    { type: StorageService }
];
/** @nocollapse */ AlfrescoApiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AlfrescoApiService_Factory() { return new AlfrescoApiService(i0.ɵɵinject(i1.AppConfigService), i0.ɵɵinject(i2.StorageService)); }, token: AlfrescoApiService, providedIn: "root" });
if (false) {
    /**
     * Publish/subscribe to events related to node updates.
     * @type {?}
     */
    AlfrescoApiService.prototype.nodeUpdated;
    /**
     * @type {?}
     * @protected
     */
    AlfrescoApiService.prototype.alfrescoApiInitializedSubject;
    /** @type {?} */
    AlfrescoApiService.prototype.alfrescoApiInitialized;
    /**
     * @type {?}
     * @protected
     */
    AlfrescoApiService.prototype.alfrescoApi;
    /** @type {?} */
    AlfrescoApiService.prototype.lastConfig;
    /**
     * @type {?}
     * @protected
     */
    AlfrescoApiService.prototype.appConfig;
    /**
     * @type {?}
     * @protected
     */
    AlfrescoApiService.prototype.storageService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxmcmVzY28tYXBpLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9hbGZyZXNjby1hcGkuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQVEzQyxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUMvRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsZUFBZSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDckYsT0FBTyxFQUFFLE9BQU8sRUFBYyxNQUFNLE1BQU0sQ0FBQztBQUUzQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7Ozs7O0FBT25ELE1BQU0sT0FBTyxrQkFBa0I7Ozs7O0lBaUUzQixZQUNjLFNBQTJCLEVBQzNCLGNBQThCO1FBRDlCLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBQzNCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjs7OztRQS9ENUMsZ0JBQVcsR0FBRyxJQUFJLE9BQU8sRUFBUSxDQUFDO1FBZ0U5QixJQUFJLENBQUMsNkJBQTZCLEdBQUcsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUNuRCxJQUFJLENBQUMsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLDZCQUE2QixDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3BGLENBQUM7Ozs7SUF6REQsV0FBVztRQUNQLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQsSUFBSSxPQUFPO1FBQ1AsT0FBTyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQztJQUMvQyxDQUFDOzs7O0lBRUQsSUFBSSxVQUFVO1FBQ1YsT0FBTyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDO0lBQ3RDLENBQUM7Ozs7SUFFRCxJQUFJLFFBQVE7UUFDUixPQUFPLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQyxLQUFLLENBQUM7SUFDcEMsQ0FBQzs7OztJQUVELElBQUksYUFBYTtRQUNiLE9BQU8sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUM7SUFDakQsQ0FBQzs7OztJQUVELElBQUksY0FBYztRQUNkLE9BQU8sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDbEQsQ0FBQzs7OztJQUVELElBQUksUUFBUTtRQUNSLE9BQU8sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDNUMsQ0FBQzs7OztJQUVELElBQUksWUFBWTtRQUNaLE9BQU8sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDaEQsQ0FBQzs7OztJQUVELElBQUksU0FBUztRQUNULE9BQU8sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDN0MsQ0FBQzs7OztJQUVELElBQUksU0FBUztRQUNULE9BQU8sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUM7SUFDL0MsQ0FBQzs7OztJQUVELElBQUksV0FBVztRQUNYLE9BQU8sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDL0MsQ0FBQzs7OztJQUVELElBQUksVUFBVTtRQUNWLE9BQU8sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7SUFDOUMsQ0FBQzs7OztJQUVELElBQUksU0FBUztRQUNULE9BQU8sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7SUFDN0MsQ0FBQzs7OztJQVNLLElBQUk7O1lBQ04sTUFBTSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxDQUFDLElBQUk7OztZQUFDLEdBQUcsRUFBRTtnQkFDbEMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQVMsZUFBZSxDQUFDLGNBQWMsRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDNUYsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO2dCQUN2QixJQUFJLENBQUMsNkJBQTZCLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDOUMsQ0FBQyxFQUFDLENBQUM7UUFDUCxDQUFDO0tBQUE7Ozs7SUFFSyxLQUFLOztZQUNQLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztRQUMzQixDQUFDO0tBQUE7Ozs7O0lBRVMsZUFBZTs7Y0FDZixLQUFLLEdBQXFCLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFtQixlQUFlLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQzFILElBQUksS0FBSyxFQUFFO1lBQ1AsS0FBSyxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEtBQUssQ0FBQyxXQUFXLElBQUksR0FBRyxDQUFDLENBQUM7WUFDeEUsS0FBSyxDQUFDLGlCQUFpQixHQUFHLE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsS0FBSyxDQUFDLGlCQUFpQixJQUFJLEdBQUcsQ0FBQyxDQUFDO1NBQ3ZGOztjQUVLLE1BQU0sR0FBRyxJQUFJLGlCQUFpQixDQUFDO1lBQ2pDLFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBUyxlQUFlLENBQUMsU0FBUyxDQUFDO1lBQy9ELE9BQU8sRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBUyxlQUFlLENBQUMsT0FBTyxDQUFDO1lBQzVELE9BQU8sRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBUyxlQUFlLENBQUMsT0FBTyxDQUFDO1lBQzVELFFBQVEsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBUyxlQUFlLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQztZQUN2RSxjQUFjLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQVMsZUFBZSxDQUFDLGNBQWMsQ0FBQztZQUMxRSxXQUFXLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQVMsZUFBZSxDQUFDLGNBQWMsQ0FBQztZQUN2RSxXQUFXLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQVUsZUFBZSxDQUFDLFdBQVcsQ0FBQztZQUNyRSxlQUFlLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQVUsZUFBZSxDQUFDLHFCQUFxQixFQUFFLEtBQUssQ0FBQztZQUMxRixNQUFNLEVBQUUsS0FBSztTQUNoQixDQUFDO1FBRUYsSUFBSSxJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxFQUFFO1lBQ3JFLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQzNDO2FBQU07WUFDSCxJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQztZQUN6QixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksd0JBQXdCLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDM0Q7SUFFTCxDQUFDOzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxVQUE2QixFQUFFLFNBQTRCO1FBQ3pFLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsS0FBSyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3BFLENBQUM7OztZQXRISixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFUUSxnQkFBZ0I7WUFHaEIsY0FBYzs7Ozs7Ozs7SUFXbkIseUNBQWtDOzs7OztJQUVsQywyREFBc0Q7O0lBQ3RELG9EQUF3Qzs7Ozs7SUFFeEMseUNBQWdEOztJQUVoRCx3Q0FBOEI7Ozs7O0lBdUQxQix1Q0FBcUM7Ozs7O0lBQ3JDLDRDQUF3QyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7XHJcbiAgICBDb250ZW50QXBpLFxyXG4gICAgQ29yZSxcclxuICAgIEFjdGl2aXRpLFxyXG4gICAgU2VhcmNoQXBpLFxyXG4gICAgTm9kZVxyXG59IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaUNvbXBhdGliaWxpdHksIEFsZnJlc2NvQXBpQ29uZmlnIH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UsIEFwcENvbmZpZ1ZhbHVlcyB9IGZyb20gJy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3ViamVjdCwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBPYXV0aENvbmZpZ01vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL29hdXRoLWNvbmZpZy5tb2RlbCc7XHJcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnLi9zdG9yYWdlLnNlcnZpY2UnO1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6YWRmLWZpbGUtbmFtZSAqL1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBbGZyZXNjb0FwaVNlcnZpY2Uge1xyXG4gICAgLyoqXHJcbiAgICAgKiBQdWJsaXNoL3N1YnNjcmliZSB0byBldmVudHMgcmVsYXRlZCB0byBub2RlIHVwZGF0ZXMuXHJcbiAgICAgKi9cclxuICAgIG5vZGVVcGRhdGVkID0gbmV3IFN1YmplY3Q8Tm9kZT4oKTtcclxuXHJcbiAgICBwcm90ZWN0ZWQgYWxmcmVzY29BcGlJbml0aWFsaXplZFN1YmplY3Q6IFN1YmplY3Q8YW55PjtcclxuICAgIGFsZnJlc2NvQXBpSW5pdGlhbGl6ZWQ6IE9ic2VydmFibGU8YW55PjtcclxuXHJcbiAgICBwcm90ZWN0ZWQgYWxmcmVzY29BcGk6IEFsZnJlc2NvQXBpQ29tcGF0aWJpbGl0eTtcclxuXHJcbiAgICBsYXN0Q29uZmlnOiBBbGZyZXNjb0FwaUNvbmZpZztcclxuXHJcbiAgICBnZXRJbnN0YW5jZSgpOiBBbGZyZXNjb0FwaUNvbXBhdGliaWxpdHkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFsZnJlc2NvQXBpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCB0YXNrQXBpKCk6IEFjdGl2aXRpLlRhc2tBcGkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldEluc3RhbmNlKCkuYWN0aXZpdGkudGFza0FwaTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgY29udGVudEFwaSgpOiBDb250ZW50QXBpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRJbnN0YW5jZSgpLmNvbnRlbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IG5vZGVzQXBpKCk6IENvcmUuTm9kZXNBcGkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldEluc3RhbmNlKCkubm9kZXM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHJlbmRpdGlvbnNBcGkoKTogQ29yZS5SZW5kaXRpb25zQXBpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRJbnN0YW5jZSgpLmNvcmUucmVuZGl0aW9uc0FwaTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgc2hhcmVkTGlua3NBcGkoKTogQ29yZS5TaGFyZWRsaW5rc0FwaSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0SW5zdGFuY2UoKS5jb3JlLnNoYXJlZGxpbmtzQXBpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBzaXRlc0FwaSgpOiBDb3JlLlNpdGVzQXBpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRJbnN0YW5jZSgpLmNvcmUuc2l0ZXNBcGk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGZhdm9yaXRlc0FwaSgpOiBDb3JlLkZhdm9yaXRlc0FwaSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0SW5zdGFuY2UoKS5jb3JlLmZhdm9yaXRlc0FwaTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgcGVvcGxlQXBpKCk6IENvcmUuUGVvcGxlQXBpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRJbnN0YW5jZSgpLmNvcmUucGVvcGxlQXBpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBzZWFyY2hBcGkoKTogU2VhcmNoQXBpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRJbnN0YW5jZSgpLnNlYXJjaC5zZWFyY2hBcGk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHZlcnNpb25zQXBpKCk6IENvcmUuVmVyc2lvbnNBcGkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldEluc3RhbmNlKCkuY29yZS52ZXJzaW9uc0FwaTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgY2xhc3Nlc0FwaSgpOiBDb3JlLkNsYXNzZXNBcGkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldEluc3RhbmNlKCkuY29yZS5jbGFzc2VzQXBpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBncm91cHNBcGkoKTogQ29yZS5Hcm91cHNBcGkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldEluc3RhbmNlKCkuY29yZS5ncm91cHNBcGk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJvdGVjdGVkIGFwcENvbmZpZzogQXBwQ29uZmlnU2VydmljZSxcclxuICAgICAgICBwcm90ZWN0ZWQgc3RvcmFnZVNlcnZpY2U6IFN0b3JhZ2VTZXJ2aWNlKSB7XHJcbiAgICAgICAgdGhpcy5hbGZyZXNjb0FwaUluaXRpYWxpemVkU3ViamVjdCA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICAgICAgdGhpcy5hbGZyZXNjb0FwaUluaXRpYWxpemVkID0gdGhpcy5hbGZyZXNjb0FwaUluaXRpYWxpemVkU3ViamVjdC5hc09ic2VydmFibGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBsb2FkKCkge1xyXG4gICAgICAgIGF3YWl0IHRoaXMuYXBwQ29uZmlnLmxvYWQoKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zdG9yYWdlU2VydmljZS5wcmVmaXggPSB0aGlzLmFwcENvbmZpZy5nZXQ8c3RyaW5nPihBcHBDb25maWdWYWx1ZXMuU1RPUkFHRV9QUkVGSVgsICcnKTtcclxuICAgICAgICAgICAgdGhpcy5pbml0QWxmcmVzY29BcGkoKTtcclxuICAgICAgICAgICAgdGhpcy5hbGZyZXNjb0FwaUluaXRpYWxpemVkU3ViamVjdC5uZXh0KCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgcmVzZXQoKSB7XHJcbiAgICAgICAgdGhpcy5pbml0QWxmcmVzY29BcGkoKTtcclxuICAgIH1cclxuXHJcbiAgICBwcm90ZWN0ZWQgaW5pdEFsZnJlc2NvQXBpKCkge1xyXG4gICAgICAgIGNvbnN0IG9hdXRoOiBPYXV0aENvbmZpZ01vZGVsID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5hcHBDb25maWcuZ2V0PE9hdXRoQ29uZmlnTW9kZWw+KEFwcENvbmZpZ1ZhbHVlcy5PQVVUSENPTkZJRywgbnVsbCkpO1xyXG4gICAgICAgIGlmIChvYXV0aCkge1xyXG4gICAgICAgICAgICBvYXV0aC5yZWRpcmVjdFVyaSA9IHdpbmRvdy5sb2NhdGlvbi5vcmlnaW4gKyAob2F1dGgucmVkaXJlY3RVcmkgfHwgJy8nKTtcclxuICAgICAgICAgICAgb2F1dGgucmVkaXJlY3RVcmlMb2dvdXQgPSB3aW5kb3cubG9jYXRpb24ub3JpZ2luICsgKG9hdXRoLnJlZGlyZWN0VXJpTG9nb3V0IHx8ICcvJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBjb25maWcgPSBuZXcgQWxmcmVzY29BcGlDb25maWcoe1xyXG4gICAgICAgICAgICBwcm92aWRlcjogdGhpcy5hcHBDb25maWcuZ2V0PHN0cmluZz4oQXBwQ29uZmlnVmFsdWVzLlBST1ZJREVSUyksXHJcbiAgICAgICAgICAgIGhvc3RFY206IHRoaXMuYXBwQ29uZmlnLmdldDxzdHJpbmc+KEFwcENvbmZpZ1ZhbHVlcy5FQ01IT1NUKSxcclxuICAgICAgICAgICAgaG9zdEJwbTogdGhpcy5hcHBDb25maWcuZ2V0PHN0cmluZz4oQXBwQ29uZmlnVmFsdWVzLkJQTUhPU1QpLFxyXG4gICAgICAgICAgICBhdXRoVHlwZTogdGhpcy5hcHBDb25maWcuZ2V0PHN0cmluZz4oQXBwQ29uZmlnVmFsdWVzLkFVVEhUWVBFLCAnQkFTSUMnKSxcclxuICAgICAgICAgICAgY29udGV4dFJvb3RCcG06IHRoaXMuYXBwQ29uZmlnLmdldDxzdHJpbmc+KEFwcENvbmZpZ1ZhbHVlcy5DT05URVhUUk9PVEJQTSksXHJcbiAgICAgICAgICAgIGNvbnRleHRSb290OiB0aGlzLmFwcENvbmZpZy5nZXQ8c3RyaW5nPihBcHBDb25maWdWYWx1ZXMuQ09OVEVYVFJPT1RFQ00pLFxyXG4gICAgICAgICAgICBkaXNhYmxlQ3NyZjogdGhpcy5hcHBDb25maWcuZ2V0PGJvb2xlYW4+KEFwcENvbmZpZ1ZhbHVlcy5ESVNBQkxFQ1NSRiksXHJcbiAgICAgICAgICAgIHdpdGhDcmVkZW50aWFsczogdGhpcy5hcHBDb25maWcuZ2V0PGJvb2xlYW4+KEFwcENvbmZpZ1ZhbHVlcy5BVVRIX1dJVEhfQ1JFREVOVElBTFMsIGZhbHNlKSxcclxuICAgICAgICAgICAgb2F1dGgyOiBvYXV0aFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5hbGZyZXNjb0FwaSAmJiB0aGlzLmlzRGlmZmVyZW50Q29uZmlnKHRoaXMubGFzdENvbmZpZywgY29uZmlnKSkge1xyXG4gICAgICAgICAgICB0aGlzLmxhc3RDb25maWcgPSBjb25maWc7XHJcbiAgICAgICAgICAgIHRoaXMuYWxmcmVzY29BcGkuY29uZmlndXJlSnNBcGkoY29uZmlnKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmxhc3RDb25maWcgPSBjb25maWc7XHJcbiAgICAgICAgICAgIHRoaXMuYWxmcmVzY29BcGkgPSBuZXcgQWxmcmVzY29BcGlDb21wYXRpYmlsaXR5KGNvbmZpZyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBpc0RpZmZlcmVudENvbmZpZyhsYXN0Q29uZmlnOiBBbGZyZXNjb0FwaUNvbmZpZywgbmV3Q29uZmlnOiBBbGZyZXNjb0FwaUNvbmZpZykge1xyXG4gICAgICAgIHJldHVybiBKU09OLnN0cmluZ2lmeShsYXN0Q29uZmlnKSAhPT0gSlNPTi5zdHJpbmdpZnkobmV3Q29uZmlnKTtcclxuICAgIH1cclxufVxyXG4iXX0=