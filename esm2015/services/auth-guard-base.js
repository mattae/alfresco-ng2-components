/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AppConfigValues } from '../app-config/app-config.service';
/**
 * @abstract
 */
export class AuthGuardBase {
    /**
     * @param {?} authenticationService
     * @param {?} router
     * @param {?} appConfigService
     */
    constructor(authenticationService, router, appConfigService) {
        this.authenticationService = authenticationService;
        this.router = router;
        this.appConfigService = appConfigService;
    }
    /**
     * @protected
     * @return {?}
     */
    get withCredentials() {
        return this.appConfigService.get('auth.withCredentials', false);
    }
    /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    canActivate(route, state) {
        return this.checkLogin(route, state.url);
    }
    /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    canActivateChild(route, state) {
        return this.canActivate(route, state);
    }
    /**
     * @protected
     * @param {?} provider
     * @param {?} url
     * @return {?}
     */
    redirectToUrl(provider, url) {
        this.authenticationService.setRedirect({ provider, url });
        /** @type {?} */
        const pathToLogin = this.getLoginRoute();
        /** @type {?} */
        const urlToRedirect = `/${pathToLogin}?redirectUrl=${url}`;
        this.router.navigateByUrl(urlToRedirect);
    }
    /**
     * @protected
     * @return {?}
     */
    getLoginRoute() {
        return (this.appConfigService &&
            this.appConfigService.get(AppConfigValues.LOGIN_ROUTE, 'login'));
    }
    /**
     * @protected
     * @return {?}
     */
    isOAuthWithoutSilentLogin() {
        /** @type {?} */
        const oauth = this.appConfigService.get(AppConfigValues.OAUTHCONFIG, null);
        return (this.authenticationService.isOauth() && oauth.silentLogin === false);
    }
}
if (false) {
    /**
     * @type {?}
     * @protected
     */
    AuthGuardBase.prototype.authenticationService;
    /**
     * @type {?}
     * @protected
     */
    AuthGuardBase.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    AuthGuardBase.prototype.appConfigService;
    /**
     * @abstract
     * @param {?} activeRoute
     * @param {?} redirectUrl
     * @return {?}
     */
    AuthGuardBase.prototype.checkLogin = function (activeRoute, redirectUrl) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1ndWFyZC1iYXNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvYXV0aC1ndWFyZC1iYXNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMEJBLE9BQU8sRUFFSCxlQUFlLEVBQ2xCLE1BQU0sa0NBQWtDLENBQUM7Ozs7QUFHMUMsTUFBTSxPQUFnQixhQUFhOzs7Ozs7SUFhL0IsWUFDYyxxQkFBNEMsRUFDNUMsTUFBYyxFQUNkLGdCQUFrQztRQUZsQywwQkFBcUIsR0FBckIscUJBQXFCLENBQXVCO1FBQzVDLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO0lBQzdDLENBQUM7Ozs7O0lBWEosSUFBYyxlQUFlO1FBQ3pCLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FDNUIsc0JBQXNCLEVBQ3RCLEtBQUssQ0FDUixDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBUUQsV0FBVyxDQUNQLEtBQTZCLEVBQzdCLEtBQTBCO1FBRTFCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzdDLENBQUM7Ozs7OztJQUVELGdCQUFnQixDQUNaLEtBQTZCLEVBQzdCLEtBQTBCO1FBRTFCLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7Ozs7OztJQUVTLGFBQWEsQ0FBQyxRQUFnQixFQUFFLEdBQVc7UUFDakQsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFdBQVcsQ0FBQyxFQUFFLFFBQVEsRUFBRSxHQUFHLEVBQUUsQ0FBQyxDQUFDOztjQUVwRCxXQUFXLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRTs7Y0FDbEMsYUFBYSxHQUFHLElBQUksV0FBVyxnQkFBZ0IsR0FBRyxFQUFFO1FBRTFELElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxDQUFDO0lBQzdDLENBQUM7Ozs7O0lBRVMsYUFBYTtRQUNuQixPQUFPLENBQ0gsSUFBSSxDQUFDLGdCQUFnQjtZQUNyQixJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUNyQixlQUFlLENBQUMsV0FBVyxFQUMzQixPQUFPLENBQ1YsQ0FDSixDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFFUyx5QkFBeUI7O2NBQ3pCLEtBQUssR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUNuQyxlQUFlLENBQUMsV0FBVyxFQUMzQixJQUFJLENBQ1A7UUFDRCxPQUFPLENBQ0gsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sRUFBRSxJQUFJLEtBQUssQ0FBQyxXQUFXLEtBQUssS0FBSyxDQUN0RSxDQUFDO0lBQ04sQ0FBQztDQUNKOzs7Ozs7SUEvQ08sOENBQXNEOzs7OztJQUN0RCwrQkFBd0I7Ozs7O0lBQ3hCLHlDQUE0Qzs7Ozs7OztJQWZoRCw2RUFHb0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHtcclxuICAgIFJvdXRlcixcclxuICAgIENhbkFjdGl2YXRlLFxyXG4gICAgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcclxuICAgIFJvdXRlclN0YXRlU25hcHNob3QsXHJcbiAgICBDYW5BY3RpdmF0ZUNoaWxkXHJcbn0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBdXRoZW50aWNhdGlvblNlcnZpY2UgfSBmcm9tICcuL2F1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQge1xyXG4gICAgQXBwQ29uZmlnU2VydmljZSxcclxuICAgIEFwcENvbmZpZ1ZhbHVlc1xyXG59IGZyb20gJy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2F1dGhDb25maWdNb2RlbCB9IGZyb20gJy4uL21vZGVscy9vYXV0aC1jb25maWcubW9kZWwnO1xyXG5cclxuZXhwb3J0IGFic3RyYWN0IGNsYXNzIEF1dGhHdWFyZEJhc2UgaW1wbGVtZW50cyBDYW5BY3RpdmF0ZSwgQ2FuQWN0aXZhdGVDaGlsZCB7XHJcbiAgICBhYnN0cmFjdCBjaGVja0xvZ2luKFxyXG4gICAgICAgIGFjdGl2ZVJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LFxyXG4gICAgICAgIHJlZGlyZWN0VXJsOiBzdHJpbmdcclxuICAgICk6IE9ic2VydmFibGU8Ym9vbGVhbj4gfCBQcm9taXNlPGJvb2xlYW4+IHwgYm9vbGVhbjtcclxuXHJcbiAgICBwcm90ZWN0ZWQgZ2V0IHdpdGhDcmVkZW50aWFscygpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcHBDb25maWdTZXJ2aWNlLmdldDxib29sZWFuPihcclxuICAgICAgICAgICAgJ2F1dGgud2l0aENyZWRlbnRpYWxzJyxcclxuICAgICAgICAgICAgZmFsc2VcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByb3RlY3RlZCBhdXRoZW50aWNhdGlvblNlcnZpY2U6IEF1dGhlbnRpY2F0aW9uU2VydmljZSxcclxuICAgICAgICBwcm90ZWN0ZWQgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgcHJvdGVjdGVkIGFwcENvbmZpZ1NlcnZpY2U6IEFwcENvbmZpZ1NlcnZpY2VcclxuICAgICkge31cclxuXHJcbiAgICBjYW5BY3RpdmF0ZShcclxuICAgICAgICByb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcclxuICAgICAgICBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdFxyXG4gICAgKTogT2JzZXJ2YWJsZTxib29sZWFuPiB8IFByb21pc2U8Ym9vbGVhbj4gfCBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jaGVja0xvZ2luKHJvdXRlLCBzdGF0ZS51cmwpO1xyXG4gICAgfVxyXG5cclxuICAgIGNhbkFjdGl2YXRlQ2hpbGQoXHJcbiAgICAgICAgcm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsXHJcbiAgICAgICAgc3RhdGU6IFJvdXRlclN0YXRlU25hcHNob3RcclxuICAgICk6IE9ic2VydmFibGU8Ym9vbGVhbj4gfCBQcm9taXNlPGJvb2xlYW4+IHwgYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY2FuQWN0aXZhdGUocm91dGUsIHN0YXRlKTtcclxuICAgIH1cclxuXHJcbiAgICBwcm90ZWN0ZWQgcmVkaXJlY3RUb1VybChwcm92aWRlcjogc3RyaW5nLCB1cmw6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuYXV0aGVudGljYXRpb25TZXJ2aWNlLnNldFJlZGlyZWN0KHsgcHJvdmlkZXIsIHVybCB9KTtcclxuXHJcbiAgICAgICAgY29uc3QgcGF0aFRvTG9naW4gPSB0aGlzLmdldExvZ2luUm91dGUoKTtcclxuICAgICAgICBjb25zdCB1cmxUb1JlZGlyZWN0ID0gYC8ke3BhdGhUb0xvZ2lufT9yZWRpcmVjdFVybD0ke3VybH1gO1xyXG5cclxuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZUJ5VXJsKHVybFRvUmVkaXJlY3QpO1xyXG4gICAgfVxyXG5cclxuICAgIHByb3RlY3RlZCBnZXRMb2dpblJvdXRlKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgdGhpcy5hcHBDb25maWdTZXJ2aWNlICYmXHJcbiAgICAgICAgICAgIHRoaXMuYXBwQ29uZmlnU2VydmljZS5nZXQ8c3RyaW5nPihcclxuICAgICAgICAgICAgICAgIEFwcENvbmZpZ1ZhbHVlcy5MT0dJTl9ST1VURSxcclxuICAgICAgICAgICAgICAgICdsb2dpbidcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJvdGVjdGVkIGlzT0F1dGhXaXRob3V0U2lsZW50TG9naW4oKSB7XHJcbiAgICAgICAgY29uc3Qgb2F1dGggPSB0aGlzLmFwcENvbmZpZ1NlcnZpY2UuZ2V0PE9hdXRoQ29uZmlnTW9kZWw+KFxyXG4gICAgICAgICAgICBBcHBDb25maWdWYWx1ZXMuT0FVVEhDT05GSUcsXHJcbiAgICAgICAgICAgIG51bGxcclxuICAgICAgICApO1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIHRoaXMuYXV0aGVudGljYXRpb25TZXJ2aWNlLmlzT2F1dGgoKSAmJiBvYXV0aC5zaWxlbnRMb2dpbiA9PT0gZmFsc2VcclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==