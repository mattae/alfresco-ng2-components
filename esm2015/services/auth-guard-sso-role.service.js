/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { JwtHelperService } from './jwt-helper.service';
import { Router } from '@angular/router';
import * as i0 from "@angular/core";
import * as i1 from "./jwt-helper.service";
import * as i2 from "@angular/router";
export class AuthGuardSsoRoleService {
    /**
     * @param {?} jwtHelperService
     * @param {?} router
     */
    constructor(jwtHelperService, router) {
        this.jwtHelperService = jwtHelperService;
        this.router = router;
    }
    /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    canActivate(route, state) {
        /** @type {?} */
        let hasRole;
        /** @type {?} */
        let hasRealmRole = false;
        /** @type {?} */
        let hasClientRole = true;
        if (route.data) {
            if (route.data['roles']) {
                /** @type {?} */
                const rolesToCheck = route.data['roles'];
                hasRealmRole = this.hasRealmRoles(rolesToCheck);
            }
            if (route.data['clientRoles']) {
                /** @type {?} */
                const clientRoleName = route.params[route.data['clientRoles']];
                /** @type {?} */
                const rolesToCheck = route.data['roles'];
                hasClientRole = this.hasRealmRolesForClientRole(clientRoleName, rolesToCheck);
            }
        }
        hasRole = hasRealmRole && hasClientRole;
        if (!hasRole && route.data && route.data['redirectUrl']) {
            this.router.navigate(['/' + route.data['redirectUrl']]);
        }
        return hasRole;
    }
    /**
     * @return {?}
     */
    getRealmRoles() {
        /** @type {?} */
        const access = this.jwtHelperService.getValueFromLocalAccessToken('realm_access');
        return access ? access['roles'] : [];
    }
    /**
     * @param {?} client
     * @return {?}
     */
    getClientRoles(client) {
        /** @type {?} */
        const clientRole = this.jwtHelperService.getValueFromLocalAccessToken('resource_access')[client];
        return clientRole ? clientRole['roles'] : [];
    }
    /**
     * @param {?} role
     * @return {?}
     */
    hasRealmRole(role) {
        /** @type {?} */
        let hasRole = false;
        if (this.jwtHelperService.getAccessToken()) {
            /** @type {?} */
            const realmRoles = this.getRealmRoles();
            hasRole = realmRoles.some((/**
             * @param {?} currentRole
             * @return {?}
             */
            (currentRole) => {
                return currentRole === role;
            }));
        }
        return hasRole;
    }
    /**
     * @param {?} rolesToCheck
     * @return {?}
     */
    hasRealmRoles(rolesToCheck) {
        return rolesToCheck.some((/**
         * @param {?} currentRole
         * @return {?}
         */
        (currentRole) => {
            return this.hasRealmRole(currentRole);
        }));
    }
    /**
     * @param {?} clientRole
     * @param {?} rolesToCheck
     * @return {?}
     */
    hasRealmRolesForClientRole(clientRole, rolesToCheck) {
        return rolesToCheck.some((/**
         * @param {?} currentRole
         * @return {?}
         */
        (currentRole) => {
            return this.hasClientRole(clientRole, currentRole);
        }));
    }
    /**
     * @param {?} clientRole
     * @param {?} role
     * @return {?}
     */
    hasClientRole(clientRole, role) {
        /** @type {?} */
        let hasRole = false;
        if (this.jwtHelperService.getAccessToken()) {
            /** @type {?} */
            const clientRoles = this.getClientRoles(clientRole);
            hasRole = clientRoles.some((/**
             * @param {?} currentRole
             * @return {?}
             */
            (currentRole) => {
                return currentRole === role;
            }));
        }
        return hasRole;
    }
}
AuthGuardSsoRoleService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
AuthGuardSsoRoleService.ctorParameters = () => [
    { type: JwtHelperService },
    { type: Router }
];
/** @nocollapse */ AuthGuardSsoRoleService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AuthGuardSsoRoleService_Factory() { return new AuthGuardSsoRoleService(i0.ɵɵinject(i1.JwtHelperService), i0.ɵɵinject(i2.Router)); }, token: AuthGuardSsoRoleService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    AuthGuardSsoRoleService.prototype.jwtHelperService;
    /**
     * @type {?}
     * @private
     */
    AuthGuardSsoRoleService.prototype.router;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1ndWFyZC1zc28tcm9sZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvYXV0aC1ndWFyZC1zc28tcm9sZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDeEQsT0FBTyxFQUF1QyxNQUFNLEVBQXVCLE1BQU0saUJBQWlCLENBQUM7Ozs7QUFLbkcsTUFBTSxPQUFPLHVCQUF1Qjs7Ozs7SUE2QmhDLFlBQW9CLGdCQUFrQyxFQUFVLE1BQWM7UUFBMUQscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUFVLFdBQU0sR0FBTixNQUFNLENBQVE7SUFDOUUsQ0FBQzs7Ozs7O0lBNUJELFdBQVcsQ0FBQyxLQUE2QixFQUFFLEtBQTBCOztZQUM3RCxPQUFPOztZQUNQLFlBQVksR0FBRyxLQUFLOztZQUNwQixhQUFhLEdBQUcsSUFBSTtRQUV4QixJQUFJLEtBQUssQ0FBQyxJQUFJLEVBQUU7WUFDWixJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUU7O3NCQUNmLFlBQVksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztnQkFDeEMsWUFBWSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDbkQ7WUFFRCxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUU7O3NCQUNyQixjQUFjLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDOztzQkFDeEQsWUFBWSxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO2dCQUN4QyxhQUFhLEdBQUcsSUFBSSxDQUFDLDBCQUEwQixDQUFDLGNBQWMsRUFBRSxZQUFZLENBQUMsQ0FBQzthQUNqRjtTQUNKO1FBRUQsT0FBTyxHQUFHLFlBQVksSUFBSSxhQUFhLENBQUM7UUFFeEMsSUFBSSxDQUFDLE9BQU8sSUFBSSxLQUFLLENBQUMsSUFBSSxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLEVBQUU7WUFDckQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDM0Q7UUFFRCxPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDOzs7O0lBS0QsYUFBYTs7Y0FDSCxNQUFNLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLDRCQUE0QixDQUFNLGNBQWMsQ0FBQztRQUN0RixPQUFPLE1BQU0sQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDekMsQ0FBQzs7Ozs7SUFFRCxjQUFjLENBQUMsTUFBYzs7Y0FDbkIsVUFBVSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyw0QkFBNEIsQ0FBTSxpQkFBaUIsQ0FBQyxDQUFDLE1BQU0sQ0FBQztRQUNyRyxPQUFPLFVBQVUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDakQsQ0FBQzs7Ozs7SUFFRCxZQUFZLENBQUMsSUFBWTs7WUFDakIsT0FBTyxHQUFHLEtBQUs7UUFDbkIsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxFQUFFLEVBQUU7O2tCQUNsQyxVQUFVLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRTtZQUN2QyxPQUFPLEdBQUcsVUFBVSxDQUFDLElBQUk7Ozs7WUFBQyxDQUFDLFdBQVcsRUFBRSxFQUFFO2dCQUN0QyxPQUFPLFdBQVcsS0FBSyxJQUFJLENBQUM7WUFDaEMsQ0FBQyxFQUFDLENBQUM7U0FDTjtRQUNELE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7Ozs7O0lBRUQsYUFBYSxDQUFDLFlBQXVCO1FBQ2pDLE9BQU8sWUFBWSxDQUFDLElBQUk7Ozs7UUFBQyxDQUFDLFdBQVcsRUFBRSxFQUFFO1lBQ3JDLE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUMxQyxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQUVELDBCQUEwQixDQUFDLFVBQWtCLEVBQUUsWUFBdUI7UUFDbEUsT0FBTyxZQUFZLENBQUMsSUFBSTs7OztRQUFDLENBQUMsV0FBVyxFQUFFLEVBQUU7WUFDckMsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLFVBQVUsRUFBRSxXQUFXLENBQUMsQ0FBQztRQUN2RCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQUVELGFBQWEsQ0FBQyxVQUFVLEVBQUUsSUFBWTs7WUFDOUIsT0FBTyxHQUFHLEtBQUs7UUFDbkIsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxFQUFFLEVBQUU7O2tCQUNsQyxXQUFXLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxVQUFVLENBQUM7WUFDbkQsT0FBTyxHQUFHLFdBQVcsQ0FBQyxJQUFJOzs7O1lBQUMsQ0FBQyxXQUFXLEVBQUUsRUFBRTtnQkFDdkMsT0FBTyxXQUFXLEtBQUssSUFBSSxDQUFDO1lBQ2hDLENBQUMsRUFBQyxDQUFDO1NBQ047UUFDRCxPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDOzs7WUE3RUosVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBTFEsZ0JBQWdCO1lBQ3FCLE1BQU07Ozs7Ozs7O0lBa0NwQyxtREFBMEM7Ozs7O0lBQUUseUNBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSnd0SGVscGVyU2VydmljZSB9IGZyb20gJy4vand0LWhlbHBlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgQ2FuQWN0aXZhdGUsIFJvdXRlciwgUm91dGVyU3RhdGVTbmFwc2hvdCB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEF1dGhHdWFyZFNzb1JvbGVTZXJ2aWNlIGltcGxlbWVudHMgQ2FuQWN0aXZhdGUge1xyXG5cclxuICAgIGNhbkFjdGl2YXRlKHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGxldCBoYXNSb2xlO1xyXG4gICAgICAgIGxldCBoYXNSZWFsbVJvbGUgPSBmYWxzZTtcclxuICAgICAgICBsZXQgaGFzQ2xpZW50Um9sZSA9IHRydWU7XHJcblxyXG4gICAgICAgIGlmIChyb3V0ZS5kYXRhKSB7XHJcbiAgICAgICAgICAgIGlmIChyb3V0ZS5kYXRhWydyb2xlcyddKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByb2xlc1RvQ2hlY2sgPSByb3V0ZS5kYXRhWydyb2xlcyddO1xyXG4gICAgICAgICAgICAgICAgaGFzUmVhbG1Sb2xlID0gdGhpcy5oYXNSZWFsbVJvbGVzKHJvbGVzVG9DaGVjayk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChyb3V0ZS5kYXRhWydjbGllbnRSb2xlcyddKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBjbGllbnRSb2xlTmFtZSA9IHJvdXRlLnBhcmFtc1tyb3V0ZS5kYXRhWydjbGllbnRSb2xlcyddXTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJvbGVzVG9DaGVjayA9IHJvdXRlLmRhdGFbJ3JvbGVzJ107XHJcbiAgICAgICAgICAgICAgICBoYXNDbGllbnRSb2xlID0gdGhpcy5oYXNSZWFsbVJvbGVzRm9yQ2xpZW50Um9sZShjbGllbnRSb2xlTmFtZSwgcm9sZXNUb0NoZWNrKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaGFzUm9sZSA9IGhhc1JlYWxtUm9sZSAmJiBoYXNDbGllbnRSb2xlO1xyXG5cclxuICAgICAgICBpZiAoIWhhc1JvbGUgJiYgcm91dGUuZGF0YSAmJiByb3V0ZS5kYXRhWydyZWRpcmVjdFVybCddKSB7XHJcbiAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlKFsnLycgKyByb3V0ZS5kYXRhWydyZWRpcmVjdFVybCddXSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gaGFzUm9sZTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGp3dEhlbHBlclNlcnZpY2U6IEp3dEhlbHBlclNlcnZpY2UsIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIpIHtcclxuICAgIH1cclxuXHJcbiAgICBnZXRSZWFsbVJvbGVzKCk6IHN0cmluZ1tdIHtcclxuICAgICAgICBjb25zdCBhY2Nlc3MgPSB0aGlzLmp3dEhlbHBlclNlcnZpY2UuZ2V0VmFsdWVGcm9tTG9jYWxBY2Nlc3NUb2tlbjxhbnk+KCdyZWFsbV9hY2Nlc3MnKTtcclxuICAgICAgICByZXR1cm4gYWNjZXNzID8gYWNjZXNzWydyb2xlcyddIDogW107XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q2xpZW50Um9sZXMoY2xpZW50OiBzdHJpbmcpOiBzdHJpbmdbXSB7XHJcbiAgICAgICAgY29uc3QgY2xpZW50Um9sZSA9IHRoaXMuand0SGVscGVyU2VydmljZS5nZXRWYWx1ZUZyb21Mb2NhbEFjY2Vzc1Rva2VuPGFueT4oJ3Jlc291cmNlX2FjY2VzcycpW2NsaWVudF07XHJcbiAgICAgICAgcmV0dXJuIGNsaWVudFJvbGUgPyBjbGllbnRSb2xlWydyb2xlcyddIDogW107XHJcbiAgICB9XHJcblxyXG4gICAgaGFzUmVhbG1Sb2xlKHJvbGU6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGxldCBoYXNSb2xlID0gZmFsc2U7XHJcbiAgICAgICAgaWYgKHRoaXMuand0SGVscGVyU2VydmljZS5nZXRBY2Nlc3NUb2tlbigpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHJlYWxtUm9sZXMgPSB0aGlzLmdldFJlYWxtUm9sZXMoKTtcclxuICAgICAgICAgICAgaGFzUm9sZSA9IHJlYWxtUm9sZXMuc29tZSgoY3VycmVudFJvbGUpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBjdXJyZW50Um9sZSA9PT0gcm9sZTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBoYXNSb2xlO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc1JlYWxtUm9sZXMocm9sZXNUb0NoZWNrOiBzdHJpbmcgW10pOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gcm9sZXNUb0NoZWNrLnNvbWUoKGN1cnJlbnRSb2xlKSA9PiB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmhhc1JlYWxtUm9sZShjdXJyZW50Um9sZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgaGFzUmVhbG1Sb2xlc0ZvckNsaWVudFJvbGUoY2xpZW50Um9sZTogc3RyaW5nLCByb2xlc1RvQ2hlY2s6IHN0cmluZyBbXSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiByb2xlc1RvQ2hlY2suc29tZSgoY3VycmVudFJvbGUpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuaGFzQ2xpZW50Um9sZShjbGllbnRSb2xlLCBjdXJyZW50Um9sZSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgaGFzQ2xpZW50Um9sZShjbGllbnRSb2xlLCByb2xlOiBzdHJpbmcpOiBib29sZWFuIHtcclxuICAgICAgICBsZXQgaGFzUm9sZSA9IGZhbHNlO1xyXG4gICAgICAgIGlmICh0aGlzLmp3dEhlbHBlclNlcnZpY2UuZ2V0QWNjZXNzVG9rZW4oKSkge1xyXG4gICAgICAgICAgICBjb25zdCBjbGllbnRSb2xlcyA9IHRoaXMuZ2V0Q2xpZW50Um9sZXMoY2xpZW50Um9sZSk7XHJcbiAgICAgICAgICAgIGhhc1JvbGUgPSBjbGllbnRSb2xlcy5zb21lKChjdXJyZW50Um9sZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGN1cnJlbnRSb2xlID09PSByb2xlO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGhhc1JvbGU7XHJcbiAgICB9XHJcbn1cclxuIl19