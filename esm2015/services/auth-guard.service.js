/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { AppConfigService } from '../app-config/app-config.service';
import { AuthGuardBase } from './auth-guard-base';
import { JwtHelperService } from './jwt-helper.service';
import * as i0 from "@angular/core";
import * as i1 from "./jwt-helper.service";
import * as i2 from "./authentication.service";
import * as i3 from "@angular/router";
import * as i4 from "../app-config/app-config.service";
export class AuthGuard extends AuthGuardBase {
    /**
     * @param {?} jwtHelperService
     * @param {?} authenticationService
     * @param {?} router
     * @param {?} appConfigService
     */
    constructor(jwtHelperService, authenticationService, router, appConfigService) {
        super(authenticationService, router, appConfigService);
        this.jwtHelperService = jwtHelperService;
        this.ticketChangeBind = this.ticketChange.bind(this);
        window.addEventListener('storage', this.ticketChangeBind);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    ticketChange(event) {
        if (event.key === 'ticket-ECM' && event.newValue !== event.oldValue) {
            this.ticketChangeRedirect(event, 'ECM');
        }
        if (event.key === 'ticket-BPM' && event.newValue !== event.oldValue) {
            this.ticketChangeRedirect(event, 'BPM');
        }
        if (event.key === JwtHelperService.USER_ACCESS_TOKEN &&
            this.jwtHelperService.getValueFromToken(event.newValue, JwtHelperService.USER_PREFERRED_USERNAME) !==
                this.jwtHelperService.getValueFromToken(event.oldValue, JwtHelperService.USER_PREFERRED_USERNAME)) {
            this.ticketChangeRedirect(event, 'ALL');
        }
    }
    /**
     * @private
     * @param {?} event
     * @param {?} provider
     * @return {?}
     */
    ticketChangeRedirect(event, provider) {
        if (!event.newValue) {
            this.redirectToUrl(provider, this.router.url);
        }
        else {
            window.location.reload();
        }
        window.removeEventListener('storage', this.ticketChangeBind);
    }
    /**
     * @param {?} activeRoute
     * @param {?} redirectUrl
     * @return {?}
     */
    checkLogin(activeRoute, redirectUrl) {
        if (this.authenticationService.isLoggedIn() || this.withCredentials) {
            return true;
        }
        if (!this.authenticationService.isOauth() || this.isOAuthWithoutSilentLogin()) {
            this.redirectToUrl('ALL', redirectUrl);
        }
        return false;
    }
}
AuthGuard.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
AuthGuard.ctorParameters = () => [
    { type: JwtHelperService },
    { type: AuthenticationService },
    { type: Router },
    { type: AppConfigService }
];
/** @nocollapse */ AuthGuard.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AuthGuard_Factory() { return new AuthGuard(i0.ɵɵinject(i1.JwtHelperService), i0.ɵɵinject(i2.AuthenticationService), i0.ɵɵinject(i3.Router), i0.ɵɵinject(i4.AppConfigService)); }, token: AuthGuard, providedIn: "root" });
if (false) {
    /** @type {?} */
    AuthGuard.prototype.ticketChangeBind;
    /**
     * @type {?}
     * @private
     */
    AuthGuard.prototype.jwtHelperService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1ndWFyZC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvYXV0aC1ndWFyZC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUEwQixNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUNqRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUVqRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUNwRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbEQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7Ozs7OztBQUt4RCxNQUFNLE9BQU8sU0FBVSxTQUFRLGFBQWE7Ozs7Ozs7SUFJeEMsWUFBb0IsZ0JBQWtDLEVBQzFDLHFCQUE0QyxFQUM1QyxNQUFjLEVBQ2QsZ0JBQWtDO1FBQzFDLEtBQUssQ0FBQyxxQkFBcUIsRUFBRSxNQUFNLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztRQUp2QyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBS2xELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUVyRCxNQUFNLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQzlELENBQUM7Ozs7O0lBRUQsWUFBWSxDQUFDLEtBQW1CO1FBQzVCLElBQUksS0FBSyxDQUFDLEdBQUcsS0FBSyxZQUFZLElBQUksS0FBSyxDQUFDLFFBQVEsS0FBSyxLQUFLLENBQUMsUUFBUSxFQUFFO1lBQ2pFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7U0FDM0M7UUFFRCxJQUFJLEtBQUssQ0FBQyxHQUFHLEtBQUssWUFBWSxJQUFJLEtBQUssQ0FBQyxRQUFRLEtBQUssS0FBSyxDQUFDLFFBQVEsRUFBRTtZQUNqRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQzNDO1FBRUQsSUFBSSxLQUFLLENBQUMsR0FBRyxLQUFLLGdCQUFnQixDQUFDLGlCQUFpQjtZQUNoRCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsaUJBQWlCLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxnQkFBZ0IsQ0FBQyx1QkFBdUIsQ0FBQztnQkFDakcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsZ0JBQWdCLENBQUMsdUJBQXVCLENBQUMsRUFBRTtZQUNuRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQzNDO0lBQ0wsQ0FBQzs7Ozs7OztJQUVPLG9CQUFvQixDQUFDLEtBQW1CLEVBQUUsUUFBZ0I7UUFDOUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUU7WUFDakIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUNqRDthQUFNO1lBQ0gsTUFBTSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEVBQUUsQ0FBQztTQUM1QjtRQUVELE1BQU0sQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDakUsQ0FBQzs7Ozs7O0lBRUQsVUFBVSxDQUFDLFdBQW1DLEVBQUUsV0FBbUI7UUFDL0QsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsVUFBVSxFQUFFLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUNqRSxPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMseUJBQXlCLEVBQUUsRUFBRTtZQUMzRSxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxXQUFXLENBQUMsQ0FBQztTQUMxQztRQUVELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7OztZQXBESixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFKUSxnQkFBZ0I7WUFKaEIscUJBQXFCO1lBREcsTUFBTTtZQUc5QixnQkFBZ0I7Ozs7O0lBU3JCLHFDQUFzQjs7Ozs7SUFFVixxQ0FBMEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBBdXRoZW50aWNhdGlvblNlcnZpY2UgfSBmcm9tICcuL2F1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IEF1dGhHdWFyZEJhc2UgfSBmcm9tICcuL2F1dGgtZ3VhcmQtYmFzZSc7XHJcbmltcG9ydCB7IEp3dEhlbHBlclNlcnZpY2UgfSBmcm9tICcuL2p3dC1oZWxwZXIuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEF1dGhHdWFyZCBleHRlbmRzIEF1dGhHdWFyZEJhc2Uge1xyXG5cclxuICAgIHRpY2tldENoYW5nZUJpbmQ6IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGp3dEhlbHBlclNlcnZpY2U6IEp3dEhlbHBlclNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBhdXRoZW50aWNhdGlvblNlcnZpY2U6IEF1dGhlbnRpY2F0aW9uU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHJvdXRlcjogUm91dGVyLFxyXG4gICAgICAgICAgICAgICAgYXBwQ29uZmlnU2VydmljZTogQXBwQ29uZmlnU2VydmljZSkge1xyXG4gICAgICAgIHN1cGVyKGF1dGhlbnRpY2F0aW9uU2VydmljZSwgcm91dGVyLCBhcHBDb25maWdTZXJ2aWNlKTtcclxuICAgICAgICB0aGlzLnRpY2tldENoYW5nZUJpbmQgPSB0aGlzLnRpY2tldENoYW5nZS5iaW5kKHRoaXMpO1xyXG5cclxuICAgICAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc3RvcmFnZScsIHRoaXMudGlja2V0Q2hhbmdlQmluZCk7XHJcbiAgICB9XHJcblxyXG4gICAgdGlja2V0Q2hhbmdlKGV2ZW50OiBTdG9yYWdlRXZlbnQpIHtcclxuICAgICAgICBpZiAoZXZlbnQua2V5ID09PSAndGlja2V0LUVDTScgJiYgZXZlbnQubmV3VmFsdWUgIT09IGV2ZW50Lm9sZFZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGlja2V0Q2hhbmdlUmVkaXJlY3QoZXZlbnQsICdFQ00nKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChldmVudC5rZXkgPT09ICd0aWNrZXQtQlBNJyAmJiBldmVudC5uZXdWYWx1ZSAhPT0gZXZlbnQub2xkVmFsdWUpIHtcclxuICAgICAgICAgICAgdGhpcy50aWNrZXRDaGFuZ2VSZWRpcmVjdChldmVudCwgJ0JQTScpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGV2ZW50LmtleSA9PT0gSnd0SGVscGVyU2VydmljZS5VU0VSX0FDQ0VTU19UT0tFTiAmJlxyXG4gICAgICAgICAgICB0aGlzLmp3dEhlbHBlclNlcnZpY2UuZ2V0VmFsdWVGcm9tVG9rZW4oZXZlbnQubmV3VmFsdWUsIEp3dEhlbHBlclNlcnZpY2UuVVNFUl9QUkVGRVJSRURfVVNFUk5BTUUpICE9PVxyXG4gICAgICAgICAgICB0aGlzLmp3dEhlbHBlclNlcnZpY2UuZ2V0VmFsdWVGcm9tVG9rZW4oZXZlbnQub2xkVmFsdWUsIEp3dEhlbHBlclNlcnZpY2UuVVNFUl9QUkVGRVJSRURfVVNFUk5BTUUpKSB7XHJcbiAgICAgICAgICAgIHRoaXMudGlja2V0Q2hhbmdlUmVkaXJlY3QoZXZlbnQsICdBTEwnKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSB0aWNrZXRDaGFuZ2VSZWRpcmVjdChldmVudDogU3RvcmFnZUV2ZW50LCBwcm92aWRlcjogc3RyaW5nKSB7XHJcbiAgICAgICAgaWYgKCFldmVudC5uZXdWYWx1ZSkge1xyXG4gICAgICAgICAgICB0aGlzLnJlZGlyZWN0VG9VcmwocHJvdmlkZXIsIHRoaXMucm91dGVyLnVybCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLnJlbG9hZCgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgd2luZG93LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3N0b3JhZ2UnLCB0aGlzLnRpY2tldENoYW5nZUJpbmQpO1xyXG4gICAgfVxyXG5cclxuICAgIGNoZWNrTG9naW4oYWN0aXZlUm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIHJlZGlyZWN0VXJsOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHwgUHJvbWlzZTxib29sZWFuPiB8IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLmF1dGhlbnRpY2F0aW9uU2VydmljZS5pc0xvZ2dlZEluKCkgfHwgdGhpcy53aXRoQ3JlZGVudGlhbHMpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICghdGhpcy5hdXRoZW50aWNhdGlvblNlcnZpY2UuaXNPYXV0aCgpIHx8IHRoaXMuaXNPQXV0aFdpdGhvdXRTaWxlbnRMb2dpbigpKSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVkaXJlY3RUb1VybCgnQUxMJywgcmVkaXJlY3RVcmwpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==