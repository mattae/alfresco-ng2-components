/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { AppConfigService } from '../app-config/app-config.service';
import { AlfrescoApiService } from '../services/alfresco-api.service';
import { StorageService } from './storage.service';
import { UserPreferencesService } from './user-preferences.service';
import { DemoForm } from '../mock/form/demo-form.mock';
import * as i0 from "@angular/core";
import * as i1 from "../app-config/app-config.service";
import * as i2 from "./alfresco-api.service";
import * as i3 from "./user-preferences.service";
import * as i4 from "./storage.service";
export class CoreAutomationService {
    /**
     * @param {?} appConfigService
     * @param {?} alfrescoApiService
     * @param {?} userPreferencesService
     * @param {?} storageService
     */
    constructor(appConfigService, alfrescoApiService, userPreferencesService, storageService) {
        this.appConfigService = appConfigService;
        this.alfrescoApiService = alfrescoApiService;
        this.userPreferencesService = userPreferencesService;
        this.storageService = storageService;
        this.forms = new DemoForm();
    }
    /**
     * @return {?}
     */
    setup() {
        /** @type {?} */
        const adfProxy = window['adf'] || {};
        adfProxy.setConfigField = (/**
         * @param {?} field
         * @param {?} value
         * @return {?}
         */
        (field, value) => {
            this.appConfigService.config[field] = JSON.parse(value);
        });
        adfProxy.setStorageItem = (/**
         * @param {?} key
         * @param {?} data
         * @return {?}
         */
        (key, data) => {
            this.storageService.setItem(key, data);
        });
        adfProxy.setUserPreference = (/**
         * @param {?} key
         * @param {?} data
         * @return {?}
         */
        (key, data) => {
            this.userPreferencesService.set(key, data);
        });
        adfProxy.setFormInEditor = (/**
         * @param {?} json
         * @return {?}
         */
        (json) => {
            this.forms.formDefinition = JSON.parse(json);
        });
        adfProxy.setCloudFormInEditor = (/**
         * @param {?} json
         * @return {?}
         */
        (json) => {
            this.forms.cloudFormDefinition = JSON.parse(json);
        });
        adfProxy.clearStorage = (/**
         * @return {?}
         */
        () => {
            this.storageService.clear();
        });
        adfProxy.apiReset = (/**
         * @return {?}
         */
        () => {
            this.alfrescoApiService.reset();
        });
        window['adf'] = adfProxy;
    }
}
CoreAutomationService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
CoreAutomationService.ctorParameters = () => [
    { type: AppConfigService },
    { type: AlfrescoApiService },
    { type: UserPreferencesService },
    { type: StorageService }
];
/** @nocollapse */ CoreAutomationService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function CoreAutomationService_Factory() { return new CoreAutomationService(i0.ɵɵinject(i1.AppConfigService), i0.ɵɵinject(i2.AlfrescoApiService), i0.ɵɵinject(i3.UserPreferencesService), i0.ɵɵinject(i4.StorageService)); }, token: CoreAutomationService, providedIn: "root" });
if (false) {
    /** @type {?} */
    CoreAutomationService.prototype.forms;
    /**
     * @type {?}
     * @private
     */
    CoreAutomationService.prototype.appConfigService;
    /**
     * @type {?}
     * @private
     */
    CoreAutomationService.prototype.alfrescoApiService;
    /**
     * @type {?}
     * @private
     */
    CoreAutomationService.prototype.userPreferencesService;
    /**
     * @type {?}
     * @private
     */
    CoreAutomationService.prototype.storageService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b21hdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvYXV0b21hdGlvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDcEUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDdEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQzs7Ozs7O0FBS3ZELE1BQU0sT0FBTyxxQkFBcUI7Ozs7Ozs7SUFJOUIsWUFBb0IsZ0JBQWtDLEVBQ2xDLGtCQUFzQyxFQUN0QyxzQkFBOEMsRUFDOUMsY0FBOEI7UUFIOUIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFDOUMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBTDNDLFVBQUssR0FBRyxJQUFJLFFBQVEsRUFBRSxDQUFDO0lBTTlCLENBQUM7Ozs7SUFFRCxLQUFLOztjQUNLLFFBQVEsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRTtRQUVwQyxRQUFRLENBQUMsY0FBYzs7Ozs7UUFBRyxDQUFDLEtBQWEsRUFBRSxLQUFhLEVBQUUsRUFBRTtZQUN2RCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDNUQsQ0FBQyxDQUFBLENBQUM7UUFFRixRQUFRLENBQUMsY0FBYzs7Ozs7UUFBRyxDQUFDLEdBQVcsRUFBRSxJQUFZLEVBQUUsRUFBRTtZQUNwRCxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDM0MsQ0FBQyxDQUFBLENBQUM7UUFFRixRQUFRLENBQUMsaUJBQWlCOzs7OztRQUFHLENBQUMsR0FBVyxFQUFFLElBQVMsRUFBRSxFQUFFO1lBQ3BELElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxHQUFHLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO1FBQy9DLENBQUMsQ0FBQSxDQUFDO1FBRUYsUUFBUSxDQUFDLGVBQWU7Ozs7UUFBRyxDQUFDLElBQVksRUFBRSxFQUFFO1lBQ3hDLElBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakQsQ0FBQyxDQUFBLENBQUM7UUFFRixRQUFRLENBQUMsb0JBQW9COzs7O1FBQUcsQ0FBQyxJQUFZLEVBQUUsRUFBRTtZQUM3QyxJQUFJLENBQUMsS0FBSyxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDdEQsQ0FBQyxDQUFBLENBQUM7UUFFRixRQUFRLENBQUMsWUFBWTs7O1FBQUcsR0FBRyxFQUFFO1lBQ3pCLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxFQUFFLENBQUM7UUFDaEMsQ0FBQyxDQUFBLENBQUM7UUFFRixRQUFRLENBQUMsUUFBUTs7O1FBQUcsR0FBRyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQyxDQUFDLENBQUEsQ0FBQztRQUVGLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxRQUFRLENBQUM7SUFDN0IsQ0FBQzs7O1lBN0NKLFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQjs7OztZQVJRLGdCQUFnQjtZQUNoQixrQkFBa0I7WUFFbEIsc0JBQXNCO1lBRHRCLGNBQWM7Ozs7O0lBU25CLHNDQUE4Qjs7Ozs7SUFFbEIsaURBQTBDOzs7OztJQUMxQyxtREFBOEM7Ozs7O0lBQzlDLHVEQUFzRDs7Ozs7SUFDdEQsK0NBQXNDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQXBwQ29uZmlnU2VydmljZSB9IGZyb20gJy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTdG9yYWdlU2VydmljZSB9IGZyb20gJy4vc3RvcmFnZS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXNlclByZWZlcmVuY2VzU2VydmljZSB9IGZyb20gJy4vdXNlci1wcmVmZXJlbmNlcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRGVtb0Zvcm0gfSBmcm9tICcuLi9tb2NrL2Zvcm0vZGVtby1mb3JtLm1vY2snO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb3JlQXV0b21hdGlvblNlcnZpY2Uge1xyXG5cclxuICAgIHB1YmxpYyBmb3JtcyA9IG5ldyBEZW1vRm9ybSgpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBwQ29uZmlnU2VydmljZTogQXBwQ29uZmlnU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgYWxmcmVzY29BcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHVzZXJQcmVmZXJlbmNlc1NlcnZpY2U6IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHN0b3JhZ2VTZXJ2aWNlOiBTdG9yYWdlU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIHNldHVwKCkge1xyXG4gICAgICAgIGNvbnN0IGFkZlByb3h5ID0gd2luZG93WydhZGYnXSB8fCB7fTtcclxuXHJcbiAgICAgICAgYWRmUHJveHkuc2V0Q29uZmlnRmllbGQgPSAoZmllbGQ6IHN0cmluZywgdmFsdWU6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFwcENvbmZpZ1NlcnZpY2UuY29uZmlnW2ZpZWxkXSA9IEpTT04ucGFyc2UodmFsdWUpO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGFkZlByb3h5LnNldFN0b3JhZ2VJdGVtID0gKGtleTogc3RyaW5nLCBkYXRhOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zdG9yYWdlU2VydmljZS5zZXRJdGVtKGtleSwgZGF0YSk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgYWRmUHJveHkuc2V0VXNlclByZWZlcmVuY2UgPSAoa2V5OiBzdHJpbmcsIGRhdGE6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlc1NlcnZpY2Uuc2V0KGtleSwgZGF0YSk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgYWRmUHJveHkuc2V0Rm9ybUluRWRpdG9yID0gKGpzb246IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZvcm1zLmZvcm1EZWZpbml0aW9uID0gSlNPTi5wYXJzZShqc29uKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBhZGZQcm94eS5zZXRDbG91ZEZvcm1JbkVkaXRvciA9IChqc29uOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5mb3Jtcy5jbG91ZEZvcm1EZWZpbml0aW9uID0gSlNPTi5wYXJzZShqc29uKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBhZGZQcm94eS5jbGVhclN0b3JhZ2UgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc3RvcmFnZVNlcnZpY2UuY2xlYXIoKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBhZGZQcm94eS5hcGlSZXNldCA9ICgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UucmVzZXQoKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICB3aW5kb3dbJ2FkZiddID0gYWRmUHJveHk7XHJcbiAgICB9XHJcbn1cclxuIl19