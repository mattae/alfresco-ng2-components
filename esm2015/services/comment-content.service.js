/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { CommentModel } from '../models/comment.model';
import { AlfrescoApiService } from '../services/alfresco-api.service';
import { LogService } from '../services/log.service';
import { map, catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./log.service";
export class CommentContentService {
    /**
     * @param {?} apiService
     * @param {?} logService
     */
    constructor(apiService, logService) {
        this.apiService = apiService;
        this.logService = logService;
    }
    /**
     * Adds a comment to a node.
     * @param {?} nodeId ID of the target node
     * @param {?} message Text for the comment
     * @return {?} Details of the comment added
     */
    addNodeComment(nodeId, message) {
        return from(this.apiService.getInstance().core.commentsApi.addComment(nodeId, { content: message }))
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            return new CommentModel({
                id: response.entry.id,
                message: response.entry.content,
                created: response.entry.createdAt,
                createdBy: response.entry.createdBy
            });
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets all comments that have been added to a node.
     * @param {?} nodeId ID of the target node
     * @return {?} Details for each comment
     */
    getNodeComments(nodeId) {
        return from(this.apiService.getInstance().core.commentsApi.getComments(nodeId))
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            /** @type {?} */
            const comments = [];
            response.list.entries.forEach((/**
             * @param {?} comment
             * @return {?}
             */
            (comment) => {
                comments.push(new CommentModel({
                    id: comment.entry.id,
                    message: comment.entry.content,
                    created: comment.entry.createdAt,
                    createdBy: comment.entry.createdBy
                }));
            }));
            return comments;
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * @private
     * @param {?} error
     * @return {?}
     */
    handleError(error) {
        this.logService.error(error);
        return throwError(error || 'Server error');
    }
}
CommentContentService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
CommentContentService.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: LogService }
];
/** @nocollapse */ CommentContentService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function CommentContentService_Factory() { return new CommentContentService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: CommentContentService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    CommentContentService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    CommentContentService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudC1jb250ZW50LnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9jb21tZW50LWNvbnRlbnQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBYyxJQUFJLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3BELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUN0RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0seUJBQXlCLENBQUM7QUFDckQsT0FBTyxFQUFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7OztBQUtqRCxNQUFNLE9BQU8scUJBQXFCOzs7OztJQUU5QixZQUFvQixVQUE4QixFQUM5QixVQUFzQjtRQUR0QixlQUFVLEdBQVYsVUFBVSxDQUFvQjtRQUM5QixlQUFVLEdBQVYsVUFBVSxDQUFZO0lBQzFDLENBQUM7Ozs7Ozs7SUFRRCxjQUFjLENBQUMsTUFBYyxFQUFFLE9BQWU7UUFDMUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsRUFBQyxPQUFPLEVBQUUsT0FBTyxFQUFDLENBQUMsQ0FBQzthQUM3RixJQUFJLENBQ0QsR0FBRzs7OztRQUFDLENBQUMsUUFBYSxFQUFFLEVBQUU7WUFDbEIsT0FBTyxJQUFJLFlBQVksQ0FBQztnQkFDcEIsRUFBRSxFQUFFLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRTtnQkFDckIsT0FBTyxFQUFFLFFBQVEsQ0FBQyxLQUFLLENBQUMsT0FBTztnQkFDL0IsT0FBTyxFQUFFLFFBQVEsQ0FBQyxLQUFLLENBQUMsU0FBUztnQkFDakMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxLQUFLLENBQUMsU0FBUzthQUN0QyxDQUFDLENBQUM7UUFDUCxDQUFDLEVBQUMsRUFDRixVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7Ozs7OztJQU9ELGVBQWUsQ0FBQyxNQUFjO1FBQzFCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDMUUsSUFBSSxDQUNELEdBQUc7Ozs7UUFBQyxDQUFDLFFBQWEsRUFBRSxFQUFFOztrQkFDWixRQUFRLEdBQW1CLEVBQUU7WUFDbkMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTzs7OztZQUFDLENBQUMsT0FBWSxFQUFFLEVBQUU7Z0JBQzNDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxZQUFZLENBQUM7b0JBQzNCLEVBQUUsRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQUU7b0JBQ3BCLE9BQU8sRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU87b0JBQzlCLE9BQU8sRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLFNBQVM7b0JBQ2hDLFNBQVMsRUFBRSxPQUFPLENBQUMsS0FBSyxDQUFDLFNBQVM7aUJBQ3JDLENBQUMsQ0FBQyxDQUFDO1lBQ1IsQ0FBQyxFQUFDLENBQUM7WUFDSCxPQUFPLFFBQVEsQ0FBQztRQUNwQixDQUFDLEVBQUMsRUFDRixVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7Ozs7OztJQUVPLFdBQVcsQ0FBQyxLQUFVO1FBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLE9BQU8sVUFBVSxDQUFDLEtBQUssSUFBSSxjQUFjLENBQUMsQ0FBQztJQUMvQyxDQUFDOzs7WUF6REosVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBTlEsa0JBQWtCO1lBQ2xCLFVBQVU7Ozs7Ozs7O0lBUUgsMkNBQXNDOzs7OztJQUN0QywyQ0FBOEIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmcm9tLCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IENvbW1lbnRNb2RlbCB9IGZyb20gJy4uL21vZGVscy9jb21tZW50Lm1vZGVsJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMb2dTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvbG9nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBtYXAsIGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIENvbW1lbnRDb250ZW50U2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEFkZHMgYSBjb21tZW50IHRvIGEgbm9kZS5cclxuICAgICAqIEBwYXJhbSBub2RlSWQgSUQgb2YgdGhlIHRhcmdldCBub2RlXHJcbiAgICAgKiBAcGFyYW0gbWVzc2FnZSBUZXh0IGZvciB0aGUgY29tbWVudFxyXG4gICAgICogQHJldHVybnMgRGV0YWlscyBvZiB0aGUgY29tbWVudCBhZGRlZFxyXG4gICAgICovXHJcbiAgICBhZGROb2RlQ29tbWVudChub2RlSWQ6IHN0cmluZywgbWVzc2FnZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxDb21tZW50TW9kZWw+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5jb3JlLmNvbW1lbnRzQXBpLmFkZENvbW1lbnQobm9kZUlkLCB7Y29udGVudDogbWVzc2FnZX0pKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBuZXcgQ29tbWVudE1vZGVsKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6IHJlc3BvbnNlLmVudHJ5LmlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiByZXNwb25zZS5lbnRyeS5jb250ZW50LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjcmVhdGVkOiByZXNwb25zZS5lbnRyeS5jcmVhdGVkQXQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0ZWRCeTogcmVzcG9uc2UuZW50cnkuY3JlYXRlZEJ5XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhbGwgY29tbWVudHMgdGhhdCBoYXZlIGJlZW4gYWRkZWQgdG8gYSBub2RlLlxyXG4gICAgICogQHBhcmFtIG5vZGVJZCBJRCBvZiB0aGUgdGFyZ2V0IG5vZGVcclxuICAgICAqIEByZXR1cm5zIERldGFpbHMgZm9yIGVhY2ggY29tbWVudFxyXG4gICAgICovXHJcbiAgICBnZXROb2RlQ29tbWVudHMobm9kZUlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPENvbW1lbnRNb2RlbFtdPiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuY29yZS5jb21tZW50c0FwaS5nZXRDb21tZW50cyhub2RlSWQpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGNvbW1lbnRzOiBDb21tZW50TW9kZWxbXSA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLmxpc3QuZW50cmllcy5mb3JFYWNoKChjb21tZW50OiBhbnkpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29tbWVudHMucHVzaChuZXcgQ29tbWVudE1vZGVsKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBjb21tZW50LmVudHJ5LmlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogY29tbWVudC5lbnRyeS5jb250ZW50LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY3JlYXRlZDogY29tbWVudC5lbnRyeS5jcmVhdGVkQXQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjcmVhdGVkQnk6IGNvbW1lbnQuZW50cnkuY3JlYXRlZEJ5XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gY29tbWVudHM7XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaGFuZGxlRXJyb3IoZXJyb3I6IGFueSkge1xyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IgfHwgJ1NlcnZlciBlcnJvcicpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=