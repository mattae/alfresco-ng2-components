/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { CommentModel } from '../models/comment.model';
import { UserProcessModel } from '../models/user-process.model';
import { AlfrescoApiService } from './alfresco-api.service';
import { LogService } from './log.service';
import { map, catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./log.service";
export class CommentProcessService {
    /**
     * @param {?} apiService
     * @param {?} logService
     */
    constructor(apiService, logService) {
        this.apiService = apiService;
        this.logService = logService;
    }
    /**
     * Adds a comment to a task.
     * @param {?} taskId ID of the target task
     * @param {?} message Text for the comment
     * @return {?} Details about the comment
     */
    addTaskComment(taskId, message) {
        return from(this.apiService.getInstance().activiti.taskApi.addTaskComment({ message: message }, taskId))
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            return new CommentModel({
                id: response.id,
                message: response.message,
                created: response.created,
                createdBy: response.createdBy
            });
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets all comments that have been added to a task.
     * @param {?} taskId ID of the target task
     * @return {?} Details for each comment
     */
    getTaskComments(taskId) {
        return from(this.apiService.getInstance().activiti.taskApi.getTaskComments(taskId))
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            /** @type {?} */
            const comments = [];
            response.data.forEach((/**
             * @param {?} comment
             * @return {?}
             */
            (comment) => {
                /** @type {?} */
                const user = new UserProcessModel(comment.createdBy);
                comments.push(new CommentModel({
                    id: comment.id,
                    message: comment.message,
                    created: comment.created,
                    createdBy: user
                }));
            }));
            return comments;
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets all comments that have been added to a process instance.
     * @param {?} processInstanceId ID of the target process instance
     * @return {?} Details for each comment
     */
    getProcessInstanceComments(processInstanceId) {
        return from(this.apiService.getInstance().activiti.commentsApi.getProcessInstanceComments(processInstanceId))
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            /** @type {?} */
            const comments = [];
            response.data.forEach((/**
             * @param {?} comment
             * @return {?}
             */
            (comment) => {
                /** @type {?} */
                const user = new UserProcessModel(comment.createdBy);
                comments.push(new CommentModel({
                    id: comment.id,
                    message: comment.message,
                    created: comment.created,
                    createdBy: user
                }));
            }));
            return comments;
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Adds a comment to a process instance.
     * @param {?} processInstanceId ID of the target process instance
     * @param {?} message Text for the comment
     * @return {?} Details of the comment added
     */
    addProcessInstanceComment(processInstanceId, message) {
        return from(this.apiService.getInstance().activiti.commentsApi.addProcessInstanceComment({ message: message }, processInstanceId)).pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            return new CommentModel({
                id: response.id,
                message: response.message,
                created: response.created,
                createdBy: response.createdBy
            });
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * @private
     * @param {?} error
     * @return {?}
     */
    handleError(error) {
        this.logService.error(error);
        return throwError(error || 'Server error');
    }
}
CommentProcessService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
CommentProcessService.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: LogService }
];
/** @nocollapse */ CommentProcessService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function CommentProcessService_Factory() { return new CommentProcessService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: CommentProcessService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    CommentProcessService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    CommentProcessService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudC1wcm9jZXNzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9jb21tZW50LXByb2Nlc3Muc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBYyxJQUFJLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3BELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUNoRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxHQUFHLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7QUFLakQsTUFBTSxPQUFPLHFCQUFxQjs7Ozs7SUFFOUIsWUFBb0IsVUFBOEIsRUFDOUIsVUFBc0I7UUFEdEIsZUFBVSxHQUFWLFVBQVUsQ0FBb0I7UUFDOUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUMxQyxDQUFDOzs7Ozs7O0lBUUQsY0FBYyxDQUFDLE1BQWMsRUFBRSxPQUFlO1FBQzFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLEVBQUUsTUFBTSxDQUFDLENBQUM7YUFDbkcsSUFBSSxDQUNELEdBQUc7Ozs7UUFBQyxDQUFDLFFBQXNCLEVBQUUsRUFBRTtZQUMzQixPQUFPLElBQUksWUFBWSxDQUFDO2dCQUNwQixFQUFFLEVBQUUsUUFBUSxDQUFDLEVBQUU7Z0JBQ2YsT0FBTyxFQUFFLFFBQVEsQ0FBQyxPQUFPO2dCQUN6QixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQ3pCLFNBQVMsRUFBRSxRQUFRLENBQUMsU0FBUzthQUNoQyxDQUFDLENBQUM7UUFDUCxDQUFDLEVBQUMsRUFDRixVQUFVOzs7O1FBQUMsQ0FBQyxHQUFRLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDbEQsQ0FBQztJQUNWLENBQUM7Ozs7OztJQU9ELGVBQWUsQ0FBQyxNQUFjO1FBQzFCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDOUUsSUFBSSxDQUNELEdBQUc7Ozs7UUFBQyxDQUFDLFFBQWEsRUFBRSxFQUFFOztrQkFDWixRQUFRLEdBQW1CLEVBQUU7WUFDbkMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPOzs7O1lBQUMsQ0FBQyxPQUFxQixFQUFFLEVBQUU7O3NCQUN0QyxJQUFJLEdBQUcsSUFBSSxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDO2dCQUNwRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksWUFBWSxDQUFDO29CQUMzQixFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7b0JBQ2QsT0FBTyxFQUFFLE9BQU8sQ0FBQyxPQUFPO29CQUN4QixPQUFPLEVBQUUsT0FBTyxDQUFDLE9BQU87b0JBQ3hCLFNBQVMsRUFBRSxJQUFJO2lCQUNsQixDQUFDLENBQUMsQ0FBQztZQUNSLENBQUMsRUFBQyxDQUFDO1lBQ0gsT0FBTyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxFQUFDLEVBQ0YsVUFBVTs7OztRQUFDLENBQUMsR0FBUSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQ2xELENBQUM7SUFDVixDQUFDOzs7Ozs7SUFPRCwwQkFBMEIsQ0FBQyxpQkFBeUI7UUFDaEQsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLDBCQUEwQixDQUFDLGlCQUFpQixDQUFDLENBQUM7YUFDeEcsSUFBSSxDQUNELEdBQUc7Ozs7UUFBQyxDQUFDLFFBQWEsRUFBRSxFQUFFOztrQkFDWixRQUFRLEdBQW1CLEVBQUU7WUFDbkMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPOzs7O1lBQUMsQ0FBQyxPQUFxQixFQUFFLEVBQUU7O3NCQUN0QyxJQUFJLEdBQUcsSUFBSSxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDO2dCQUNwRCxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksWUFBWSxDQUFDO29CQUMzQixFQUFFLEVBQUUsT0FBTyxDQUFDLEVBQUU7b0JBQ2QsT0FBTyxFQUFFLE9BQU8sQ0FBQyxPQUFPO29CQUN4QixPQUFPLEVBQUUsT0FBTyxDQUFDLE9BQU87b0JBQ3hCLFNBQVMsRUFBRSxJQUFJO2lCQUNsQixDQUFDLENBQUMsQ0FBQztZQUNSLENBQUMsRUFBQyxDQUFDO1lBQ0gsT0FBTyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxFQUFDLEVBQ0YsVUFBVTs7OztRQUFDLENBQUMsR0FBUSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQ2xELENBQUM7SUFDVixDQUFDOzs7Ozs7O0lBUUQseUJBQXlCLENBQUMsaUJBQXlCLEVBQUUsT0FBZTtRQUNoRSxPQUFPLElBQUksQ0FDUCxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxXQUFXLENBQUMseUJBQXlCLENBQUMsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLEVBQUUsaUJBQWlCLENBQUMsQ0FDeEgsQ0FBQyxJQUFJLENBQ0YsR0FBRzs7OztRQUFDLENBQUMsUUFBc0IsRUFBRSxFQUFFO1lBQzNCLE9BQU8sSUFBSSxZQUFZLENBQUM7Z0JBQ3BCLEVBQUUsRUFBRSxRQUFRLENBQUMsRUFBRTtnQkFDZixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQ3pCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTztnQkFDekIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxTQUFTO2FBQ2hDLENBQUMsQ0FBQztRQUNQLENBQUMsRUFBQyxFQUNGLFVBQVU7Ozs7UUFBQyxDQUFDLEdBQVEsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUNsRCxDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBRU8sV0FBVyxDQUFDLEtBQVU7UUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsT0FBTyxVQUFVLENBQUMsS0FBSyxJQUFJLGNBQWMsQ0FBQyxDQUFDO0lBQy9DLENBQUM7OztZQXpHSixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFOUSxrQkFBa0I7WUFDbEIsVUFBVTs7Ozs7Ozs7SUFRSCwyQ0FBc0M7Ozs7O0lBQ3RDLDJDQUE4QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZyb20sIHRocm93RXJyb3IgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQ29tbWVudE1vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL2NvbW1lbnQubW9kZWwnO1xyXG5pbXBvcnQgeyBVc2VyUHJvY2Vzc01vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL3VzZXItcHJvY2Vzcy5tb2RlbCc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4vYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMb2dTZXJ2aWNlIH0gZnJvbSAnLi9sb2cuc2VydmljZSc7XHJcbmltcG9ydCB7IG1hcCwgY2F0Y2hFcnJvciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29tbWVudFByb2Nlc3NTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgbG9nU2VydmljZTogTG9nU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQWRkcyBhIGNvbW1lbnQgdG8gYSB0YXNrLlxyXG4gICAgICogQHBhcmFtIHRhc2tJZCBJRCBvZiB0aGUgdGFyZ2V0IHRhc2tcclxuICAgICAqIEBwYXJhbSBtZXNzYWdlIFRleHQgZm9yIHRoZSBjb21tZW50XHJcbiAgICAgKiBAcmV0dXJucyBEZXRhaWxzIGFib3V0IHRoZSBjb21tZW50XHJcbiAgICAgKi9cclxuICAgIGFkZFRhc2tDb21tZW50KHRhc2tJZDogc3RyaW5nLCBtZXNzYWdlOiBzdHJpbmcpOiBPYnNlcnZhYmxlPENvbW1lbnRNb2RlbD4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmFjdGl2aXRpLnRhc2tBcGkuYWRkVGFza0NvbW1lbnQoeyBtZXNzYWdlOiBtZXNzYWdlIH0sIHRhc2tJZCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKChyZXNwb25zZTogQ29tbWVudE1vZGVsKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBDb21tZW50TW9kZWwoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogcmVzcG9uc2UuaWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IHJlc3BvbnNlLm1lc3NhZ2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0ZWQ6IHJlc3BvbnNlLmNyZWF0ZWQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0ZWRCeTogcmVzcG9uc2UuY3JlYXRlZEJ5XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycjogYW55KSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGFsbCBjb21tZW50cyB0aGF0IGhhdmUgYmVlbiBhZGRlZCB0byBhIHRhc2suXHJcbiAgICAgKiBAcGFyYW0gdGFza0lkIElEIG9mIHRoZSB0YXJnZXQgdGFza1xyXG4gICAgICogQHJldHVybnMgRGV0YWlscyBmb3IgZWFjaCBjb21tZW50XHJcbiAgICAgKi9cclxuICAgIGdldFRhc2tDb21tZW50cyh0YXNrSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8Q29tbWVudE1vZGVsW10+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5hY3Rpdml0aS50YXNrQXBpLmdldFRhc2tDb21tZW50cyh0YXNrSWQpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGNvbW1lbnRzOiBDb21tZW50TW9kZWxbXSA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLmRhdGEuZm9yRWFjaCgoY29tbWVudDogQ29tbWVudE1vZGVsKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXIgPSBuZXcgVXNlclByb2Nlc3NNb2RlbChjb21tZW50LmNyZWF0ZWRCeSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbW1lbnRzLnB1c2gobmV3IENvbW1lbnRNb2RlbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogY29tbWVudC5pZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IGNvbW1lbnQubWVzc2FnZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0ZWQ6IGNvbW1lbnQuY3JlYXRlZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0ZWRCeTogdXNlclxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGNvbW1lbnRzO1xyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnI6IGFueSkgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhbGwgY29tbWVudHMgdGhhdCBoYXZlIGJlZW4gYWRkZWQgdG8gYSBwcm9jZXNzIGluc3RhbmNlLlxyXG4gICAgICogQHBhcmFtIHByb2Nlc3NJbnN0YW5jZUlkIElEIG9mIHRoZSB0YXJnZXQgcHJvY2VzcyBpbnN0YW5jZVxyXG4gICAgICogQHJldHVybnMgRGV0YWlscyBmb3IgZWFjaCBjb21tZW50XHJcbiAgICAgKi9cclxuICAgIGdldFByb2Nlc3NJbnN0YW5jZUNvbW1lbnRzKHByb2Nlc3NJbnN0YW5jZUlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPENvbW1lbnRNb2RlbFtdPiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuYWN0aXZpdGkuY29tbWVudHNBcGkuZ2V0UHJvY2Vzc0luc3RhbmNlQ29tbWVudHMocHJvY2Vzc0luc3RhbmNlSWQpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGNvbW1lbnRzOiBDb21tZW50TW9kZWxbXSA9IFtdO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLmRhdGEuZm9yRWFjaCgoY29tbWVudDogQ29tbWVudE1vZGVsKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHVzZXIgPSBuZXcgVXNlclByb2Nlc3NNb2RlbChjb21tZW50LmNyZWF0ZWRCeSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbW1lbnRzLnB1c2gobmV3IENvbW1lbnRNb2RlbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogY29tbWVudC5pZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IGNvbW1lbnQubWVzc2FnZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0ZWQ6IGNvbW1lbnQuY3JlYXRlZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNyZWF0ZWRCeTogdXNlclxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGNvbW1lbnRzO1xyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnI6IGFueSkgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQWRkcyBhIGNvbW1lbnQgdG8gYSBwcm9jZXNzIGluc3RhbmNlLlxyXG4gICAgICogQHBhcmFtIHByb2Nlc3NJbnN0YW5jZUlkIElEIG9mIHRoZSB0YXJnZXQgcHJvY2VzcyBpbnN0YW5jZVxyXG4gICAgICogQHBhcmFtIG1lc3NhZ2UgVGV4dCBmb3IgdGhlIGNvbW1lbnRcclxuICAgICAqIEByZXR1cm5zIERldGFpbHMgb2YgdGhlIGNvbW1lbnQgYWRkZWRcclxuICAgICAqL1xyXG4gICAgYWRkUHJvY2Vzc0luc3RhbmNlQ29tbWVudChwcm9jZXNzSW5zdGFuY2VJZDogc3RyaW5nLCBtZXNzYWdlOiBzdHJpbmcpOiBPYnNlcnZhYmxlPENvbW1lbnRNb2RlbD4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKFxyXG4gICAgICAgICAgICB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5hY3Rpdml0aS5jb21tZW50c0FwaS5hZGRQcm9jZXNzSW5zdGFuY2VDb21tZW50KHsgbWVzc2FnZTogbWVzc2FnZSB9LCBwcm9jZXNzSW5zdGFuY2VJZClcclxuICAgICAgICApLnBpcGUoXHJcbiAgICAgICAgICAgIG1hcCgocmVzcG9uc2U6IENvbW1lbnRNb2RlbCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBDb21tZW50TW9kZWwoe1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiByZXNwb25zZS5pZCxcclxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiByZXNwb25zZS5tZXNzYWdlLFxyXG4gICAgICAgICAgICAgICAgICAgIGNyZWF0ZWQ6IHJlc3BvbnNlLmNyZWF0ZWQsXHJcbiAgICAgICAgICAgICAgICAgICAgY3JlYXRlZEJ5OiByZXNwb25zZS5jcmVhdGVkQnlcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyOiBhbnkpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaGFuZGxlRXJyb3IoZXJyb3I6IGFueSkge1xyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IgfHwgJ1NlcnZlciBlcnJvcicpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=