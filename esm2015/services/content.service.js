/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Subject, from, throwError } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { AuthenticationService } from './authentication.service';
import { LogService } from './log.service';
import { catchError } from 'rxjs/operators';
import { PermissionsEnum } from '../models/permissions.enum';
import { AllowableOperationsEnum } from '../models/allowable-operations.enum';
import { DownloadService } from './download.service';
import { ThumbnailService } from './thumbnail.service';
import * as i0 from "@angular/core";
import * as i1 from "./authentication.service";
import * as i2 from "./alfresco-api.service";
import * as i3 from "./log.service";
import * as i4 from "@angular/platform-browser";
import * as i5 from "./download.service";
import * as i6 from "./thumbnail.service";
export class ContentService {
    /**
     * @param {?} authService
     * @param {?} apiService
     * @param {?} logService
     * @param {?} sanitizer
     * @param {?} downloadService
     * @param {?} thumbnailService
     */
    constructor(authService, apiService, logService, sanitizer, downloadService, thumbnailService) {
        this.authService = authService;
        this.apiService = apiService;
        this.logService = logService;
        this.sanitizer = sanitizer;
        this.downloadService = downloadService;
        this.thumbnailService = thumbnailService;
        this.folderCreated = new Subject();
        this.folderCreate = new Subject();
        this.folderEdit = new Subject();
    }
    /**
     * @deprecated in 3.2.0, use DownloadService instead.
     * Invokes content download for a Blob with a file name.
     * @param {?} blob Content to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    downloadBlob(blob, fileName) {
        this.downloadService.downloadBlob(blob, fileName);
    }
    /**
     * @deprecated in 3.2.0, use DownloadService instead.
     * Invokes content download for a data array with a file name.
     * @param {?} data Data to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    downloadData(data, fileName) {
        this.downloadService.downloadData(data, fileName);
    }
    /**
     * @deprecated in 3.2.0, use DownloadService instead.
     * Invokes content download for a JSON object with a file name.
     * @param {?} json JSON object to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    downloadJSON(json, fileName) {
        this.downloadService.downloadJSON(json, fileName);
    }
    /**
     * Creates a trusted object URL from the Blob.
     * WARNING: calling this method with untrusted user data exposes your application to XSS security risks!
     * @param {?} blob Data to wrap into object URL
     * @return {?} URL string
     */
    createTrustedUrl(blob) {
        /** @type {?} */
        const url = window.URL.createObjectURL(blob);
        return (/** @type {?} */ (this.sanitizer.bypassSecurityTrustUrl(url)));
    }
    /**
     * @private
     * @return {?}
     */
    get contentApi() {
        return this.apiService.getInstance().content;
    }
    /**
     * @deprecated in 3.2.0, use ThumbnailService instead.
     * Gets a thumbnail URL for the given document node.
     * @param {?} node Node or Node ID to get URL for.
     * @param {?=} attachment Toggles whether to retrieve content as an attachment for download
     * @param {?=} ticket Custom ticket to use for authentication
     * @return {?} URL string
     */
    getDocumentThumbnailUrl(node, attachment, ticket) {
        return this.thumbnailService.getDocumentThumbnailUrl(node, attachment, ticket);
    }
    /**
     * Gets a content URL for the given node.
     * @param {?} node Node or Node ID to get URL for.
     * @param {?=} attachment Toggles whether to retrieve content as an attachment for download
     * @param {?=} ticket Custom ticket to use for authentication
     * @return {?} URL string or `null`
     */
    getContentUrl(node, attachment, ticket) {
        if (node) {
            /** @type {?} */
            let nodeId;
            if (typeof node === 'string') {
                nodeId = node;
            }
            else if (node.entry) {
                nodeId = node.entry.id;
            }
            return this.contentApi.getContentUrl(nodeId, attachment, ticket);
        }
        return null;
    }
    /**
     * Gets content for the given node.
     * @param {?} nodeId ID of the target node
     * @return {?} Content data
     */
    getNodeContent(nodeId) {
        return from(this.apiService.getInstance().core.nodesApi.getFileContent(nodeId))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets a Node via its node ID.
     * @param {?} nodeId ID of the target node
     * @param {?=} opts Options supported by JS-API
     * @return {?} Details of the folder
     */
    getNode(nodeId, opts) {
        return from(this.apiService.getInstance().nodes.getNode(nodeId, opts));
    }
    /**
     * Checks if the user has permission on that node
     * @param {?} node Node to check permissions
     * @param {?} permission Required permission type
     * @return {?} True if the user has the required permissions, false otherwise
     */
    hasPermissions(node, permission) {
        /** @type {?} */
        let hasPermissions = false;
        if (node && node.permissions && node.permissions.locallySet) {
            if (permission && permission.startsWith('!')) {
                hasPermissions = node.permissions.locallySet.find((/**
                 * @param {?} currentPermission
                 * @return {?}
                 */
                (currentPermission) => currentPermission.name === permission.replace('!', ''))) ? false : true;
            }
            else {
                hasPermissions = node.permissions.locallySet.find((/**
                 * @param {?} currentPermission
                 * @return {?}
                 */
                (currentPermission) => currentPermission.name === permission)) ? true : false;
            }
        }
        else {
            if (permission === PermissionsEnum.CONSUMER) {
                hasPermissions = true;
            }
            else if (permission === PermissionsEnum.NOT_CONSUMER) {
                hasPermissions = false;
            }
            else if (permission && permission.startsWith('!')) {
                hasPermissions = true;
            }
        }
        return hasPermissions;
    }
    /**
     * Checks if the user has permissions on that node
     * @param {?} node Node to check allowableOperations
     * @param {?} allowableOperation Create, delete, update, updatePermissions, !create, !delete, !update, !updatePermissions
     * @return {?} True if the user has the required permissions, false otherwise
     */
    hasAllowableOperations(node, allowableOperation) {
        /** @type {?} */
        let hasAllowableOperations = false;
        if (node && node.allowableOperations) {
            if (allowableOperation && allowableOperation.startsWith('!')) {
                hasAllowableOperations = node.allowableOperations.find((/**
                 * @param {?} currentOperation
                 * @return {?}
                 */
                (currentOperation) => currentOperation === allowableOperation.replace('!', ''))) ? false : true;
            }
            else {
                hasAllowableOperations = node.allowableOperations.find((/**
                 * @param {?} currentOperation
                 * @return {?}
                 */
                (currentOperation) => currentOperation === allowableOperation)) ? true : false;
            }
        }
        else {
            if (allowableOperation && allowableOperation.startsWith('!')) {
                hasAllowableOperations = true;
            }
        }
        if (allowableOperation === AllowableOperationsEnum.COPY) {
            hasAllowableOperations = true;
        }
        if (allowableOperation === AllowableOperationsEnum.LOCK) {
            hasAllowableOperations = node.isFile;
            if (node.isLocked && node.allowableOperations) {
                hasAllowableOperations = !!~node.allowableOperations.indexOf('updatePermissions');
            }
        }
        return hasAllowableOperations;
    }
    /**
     * @private
     * @param {?} error
     * @return {?}
     */
    handleError(error) {
        this.logService.error(error);
        return throwError(error || 'Server error');
    }
}
ContentService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
ContentService.ctorParameters = () => [
    { type: AuthenticationService },
    { type: AlfrescoApiService },
    { type: LogService },
    { type: DomSanitizer },
    { type: DownloadService },
    { type: ThumbnailService }
];
/** @nocollapse */ ContentService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ContentService_Factory() { return new ContentService(i0.ɵɵinject(i1.AuthenticationService), i0.ɵɵinject(i2.AlfrescoApiService), i0.ɵɵinject(i3.LogService), i0.ɵɵinject(i4.DomSanitizer), i0.ɵɵinject(i5.DownloadService), i0.ɵɵinject(i6.ThumbnailService)); }, token: ContentService, providedIn: "root" });
if (false) {
    /** @type {?} */
    ContentService.prototype.folderCreated;
    /** @type {?} */
    ContentService.prototype.folderCreate;
    /** @type {?} */
    ContentService.prototype.folderEdit;
    /** @type {?} */
    ContentService.prototype.authService;
    /** @type {?} */
    ContentService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    ContentService.prototype.logService;
    /**
     * @type {?}
     * @private
     */
    ContentService.prototype.sanitizer;
    /**
     * @type {?}
     * @private
     */
    ContentService.prototype.downloadService;
    /**
     * @type {?}
     * @private
     */
    ContentService.prototype.thumbnailService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvY29udGVudC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRXpELE9BQU8sRUFBYyxPQUFPLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUU3RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNqRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM1QyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDN0QsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDOzs7Ozs7OztBQUt2RCxNQUFNLE9BQU8sY0FBYzs7Ozs7Ozs7O0lBTXZCLFlBQW1CLFdBQWtDLEVBQ2xDLFVBQThCLEVBQzdCLFVBQXNCLEVBQ3RCLFNBQXVCLEVBQ3ZCLGVBQWdDLEVBQ2hDLGdCQUFrQztRQUxuQyxnQkFBVyxHQUFYLFdBQVcsQ0FBdUI7UUFDbEMsZUFBVSxHQUFWLFVBQVUsQ0FBb0I7UUFDN0IsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixjQUFTLEdBQVQsU0FBUyxDQUFjO1FBQ3ZCLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUNoQyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBVHRELGtCQUFhLEdBQWdDLElBQUksT0FBTyxFQUFzQixDQUFDO1FBQy9FLGlCQUFZLEdBQXlCLElBQUksT0FBTyxFQUFlLENBQUM7UUFDaEUsZUFBVSxHQUF5QixJQUFJLE9BQU8sRUFBZSxDQUFDO0lBUTlELENBQUM7Ozs7Ozs7O0lBUUQsWUFBWSxDQUFDLElBQVUsRUFBRSxRQUFnQjtRQUNyQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDdEQsQ0FBQzs7Ozs7Ozs7SUFRRCxZQUFZLENBQUMsSUFBUyxFQUFFLFFBQWdCO1FBQ3BDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztJQUN0RCxDQUFDOzs7Ozs7OztJQVFELFlBQVksQ0FBQyxJQUFTLEVBQUUsUUFBZ0I7UUFDcEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ3RELENBQUM7Ozs7Ozs7SUFRRCxnQkFBZ0IsQ0FBQyxJQUFVOztjQUNqQixHQUFHLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDO1FBQzVDLE9BQU8sbUJBQVMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxHQUFHLENBQUMsRUFBQSxDQUFDO0lBQy9ELENBQUM7Ozs7O0lBRUQsSUFBWSxVQUFVO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUM7SUFDakQsQ0FBQzs7Ozs7Ozs7O0lBVUQsdUJBQXVCLENBQUMsSUFBd0IsRUFBRSxVQUFvQixFQUFFLE1BQWU7UUFDbkYsT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsdUJBQXVCLENBQUMsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNuRixDQUFDOzs7Ozs7OztJQVNELGFBQWEsQ0FBQyxJQUF3QixFQUFFLFVBQW9CLEVBQUUsTUFBZTtRQUN6RSxJQUFJLElBQUksRUFBRTs7Z0JBQ0YsTUFBYztZQUVsQixJQUFJLE9BQU8sSUFBSSxLQUFLLFFBQVEsRUFBRTtnQkFDMUIsTUFBTSxHQUFHLElBQUksQ0FBQzthQUNqQjtpQkFBTSxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7Z0JBQ25CLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQzthQUMxQjtZQUVELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLFVBQVUsRUFBRSxNQUFNLENBQUMsQ0FBQztTQUNwRTtRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7OztJQU9ELGNBQWMsQ0FBQyxNQUFjO1FBQ3pCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDMUUsSUFBSSxDQUNELFVBQVU7Ozs7UUFBQyxDQUFDLEdBQVEsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUNsRCxDQUFDO0lBQ1YsQ0FBQzs7Ozs7OztJQVFELE9BQU8sQ0FBQyxNQUFjLEVBQUUsSUFBVTtRQUM5QixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDM0UsQ0FBQzs7Ozs7OztJQVFELGNBQWMsQ0FBQyxJQUFVLEVBQUUsVUFBb0M7O1lBQ3ZELGNBQWMsR0FBRyxLQUFLO1FBRTFCLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLEVBQUU7WUFDekQsSUFBSSxVQUFVLElBQUksVUFBVSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDMUMsY0FBYyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLElBQUk7Ozs7Z0JBQUMsQ0FBQyxpQkFBaUIsRUFBRSxFQUFFLENBQUMsaUJBQWlCLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2FBQ25KO2lCQUFNO2dCQUNILGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJOzs7O2dCQUFDLENBQUMsaUJBQWlCLEVBQUUsRUFBRSxDQUFDLGlCQUFpQixDQUFDLElBQUksS0FBSyxVQUFVLEVBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7YUFDbEk7U0FFSjthQUFNO1lBRUgsSUFBSSxVQUFVLEtBQUssZUFBZSxDQUFDLFFBQVEsRUFBRTtnQkFDekMsY0FBYyxHQUFHLElBQUksQ0FBQzthQUN6QjtpQkFBTSxJQUFJLFVBQVUsS0FBSyxlQUFlLENBQUMsWUFBWSxFQUFFO2dCQUNwRCxjQUFjLEdBQUcsS0FBSyxDQUFDO2FBQzFCO2lCQUFNLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ2pELGNBQWMsR0FBRyxJQUFJLENBQUM7YUFDekI7U0FDSjtRQUVELE9BQU8sY0FBYyxDQUFDO0lBQzFCLENBQUM7Ozs7Ozs7SUFRRCxzQkFBc0IsQ0FBQyxJQUFVLEVBQUUsa0JBQW9EOztZQUMvRSxzQkFBc0IsR0FBRyxLQUFLO1FBRWxDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUNsQyxJQUFJLGtCQUFrQixJQUFJLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDMUQsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUk7Ozs7Z0JBQUMsQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFLENBQUMsZ0JBQWdCLEtBQUssa0JBQWtCLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQzthQUN6SjtpQkFBTTtnQkFDSCxzQkFBc0IsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSTs7OztnQkFBQyxDQUFDLGdCQUFnQixFQUFFLEVBQUUsQ0FBQyxnQkFBZ0IsS0FBSyxrQkFBa0IsRUFBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQzthQUN4STtTQUVKO2FBQU07WUFDSCxJQUFJLGtCQUFrQixJQUFJLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDMUQsc0JBQXNCLEdBQUcsSUFBSSxDQUFDO2FBQ2pDO1NBQ0o7UUFFRCxJQUFJLGtCQUFrQixLQUFLLHVCQUF1QixDQUFDLElBQUksRUFBRTtZQUNyRCxzQkFBc0IsR0FBRyxJQUFJLENBQUM7U0FDakM7UUFFRCxJQUFJLGtCQUFrQixLQUFLLHVCQUF1QixDQUFDLElBQUksRUFBRTtZQUNyRCxzQkFBc0IsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1lBRXJDLElBQUksSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsbUJBQW1CLEVBQUU7Z0JBQzNDLHNCQUFzQixHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsQ0FBQzthQUNyRjtTQUNKO1FBRUQsT0FBTyxzQkFBc0IsQ0FBQztJQUNsQyxDQUFDOzs7Ozs7SUFFTyxXQUFXLENBQUMsS0FBVTtRQUMxQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixPQUFPLFVBQVUsQ0FBQyxLQUFLLElBQUksY0FBYyxDQUFDLENBQUM7SUFDL0MsQ0FBQzs7O1lBN0xKLFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQjs7OztZQVZRLHFCQUFxQjtZQURyQixrQkFBa0I7WUFFbEIsVUFBVTtZQU5WLFlBQVk7WUFVWixlQUFlO1lBQ2YsZ0JBQWdCOzs7OztJQU9yQix1Q0FBK0U7O0lBQy9FLHNDQUFnRTs7SUFDaEUsb0NBQThEOztJQUVsRCxxQ0FBeUM7O0lBQ3pDLG9DQUFxQzs7Ozs7SUFDckMsb0NBQThCOzs7OztJQUM5QixtQ0FBK0I7Ozs7O0lBQy9CLHlDQUF3Qzs7Ozs7SUFDeEMsMENBQTBDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRG9tU2FuaXRpemVyIH0gZnJvbSAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlcic7XHJcbmltcG9ydCB7IENvbnRlbnRBcGksIE1pbmltYWxOb2RlLCBOb2RlLCBOb2RlRW50cnkgfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3ViamVjdCwgZnJvbSwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBGb2xkZXJDcmVhdGVkRXZlbnQgfSBmcm9tICcuLi9ldmVudHMvZm9sZGVyLWNyZWF0ZWQuZXZlbnQnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXV0aGVudGljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4vbG9nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBQZXJtaXNzaW9uc0VudW0gfSBmcm9tICcuLi9tb2RlbHMvcGVybWlzc2lvbnMuZW51bSc7XHJcbmltcG9ydCB7IEFsbG93YWJsZU9wZXJhdGlvbnNFbnVtIH0gZnJvbSAnLi4vbW9kZWxzL2FsbG93YWJsZS1vcGVyYXRpb25zLmVudW0nO1xyXG5pbXBvcnQgeyBEb3dubG9hZFNlcnZpY2UgfSBmcm9tICcuL2Rvd25sb2FkLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUaHVtYm5haWxTZXJ2aWNlIH0gZnJvbSAnLi90aHVtYm5haWwuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIENvbnRlbnRTZXJ2aWNlIHtcclxuXHJcbiAgICBmb2xkZXJDcmVhdGVkOiBTdWJqZWN0PEZvbGRlckNyZWF0ZWRFdmVudD4gPSBuZXcgU3ViamVjdDxGb2xkZXJDcmVhdGVkRXZlbnQ+KCk7XHJcbiAgICBmb2xkZXJDcmVhdGU6IFN1YmplY3Q8TWluaW1hbE5vZGU+ID0gbmV3IFN1YmplY3Q8TWluaW1hbE5vZGU+KCk7XHJcbiAgICBmb2xkZXJFZGl0OiBTdWJqZWN0PE1pbmltYWxOb2RlPiA9IG5ldyBTdWJqZWN0PE1pbmltYWxOb2RlPigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBhdXRoU2VydmljZTogQXV0aGVudGljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgbG9nU2VydmljZTogTG9nU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgc2FuaXRpemVyOiBEb21TYW5pdGl6ZXIsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGRvd25sb2FkU2VydmljZTogRG93bmxvYWRTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSB0aHVtYm5haWxTZXJ2aWNlOiBUaHVtYm5haWxTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAZGVwcmVjYXRlZCBpbiAzLjIuMCwgdXNlIERvd25sb2FkU2VydmljZSBpbnN0ZWFkLlxyXG4gICAgICogSW52b2tlcyBjb250ZW50IGRvd25sb2FkIGZvciBhIEJsb2Igd2l0aCBhIGZpbGUgbmFtZS5cclxuICAgICAqIEBwYXJhbSBibG9iIENvbnRlbnQgdG8gZG93bmxvYWQuXHJcbiAgICAgKiBAcGFyYW0gZmlsZU5hbWUgTmFtZSBvZiB0aGUgcmVzdWx0aW5nIGZpbGUuXHJcbiAgICAgKi9cclxuICAgIGRvd25sb2FkQmxvYihibG9iOiBCbG9iLCBmaWxlTmFtZTogc3RyaW5nKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5kb3dubG9hZFNlcnZpY2UuZG93bmxvYWRCbG9iKGJsb2IsIGZpbGVOYW1lKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEBkZXByZWNhdGVkIGluIDMuMi4wLCB1c2UgRG93bmxvYWRTZXJ2aWNlIGluc3RlYWQuXHJcbiAgICAgKiBJbnZva2VzIGNvbnRlbnQgZG93bmxvYWQgZm9yIGEgZGF0YSBhcnJheSB3aXRoIGEgZmlsZSBuYW1lLlxyXG4gICAgICogQHBhcmFtIGRhdGEgRGF0YSB0byBkb3dubG9hZC5cclxuICAgICAqIEBwYXJhbSBmaWxlTmFtZSBOYW1lIG9mIHRoZSByZXN1bHRpbmcgZmlsZS5cclxuICAgICAqL1xyXG4gICAgZG93bmxvYWREYXRhKGRhdGE6IGFueSwgZmlsZU5hbWU6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZG93bmxvYWRTZXJ2aWNlLmRvd25sb2FkRGF0YShkYXRhLCBmaWxlTmFtZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAZGVwcmVjYXRlZCBpbiAzLjIuMCwgdXNlIERvd25sb2FkU2VydmljZSBpbnN0ZWFkLlxyXG4gICAgICogSW52b2tlcyBjb250ZW50IGRvd25sb2FkIGZvciBhIEpTT04gb2JqZWN0IHdpdGggYSBmaWxlIG5hbWUuXHJcbiAgICAgKiBAcGFyYW0ganNvbiBKU09OIG9iamVjdCB0byBkb3dubG9hZC5cclxuICAgICAqIEBwYXJhbSBmaWxlTmFtZSBOYW1lIG9mIHRoZSByZXN1bHRpbmcgZmlsZS5cclxuICAgICAqL1xyXG4gICAgZG93bmxvYWRKU09OKGpzb246IGFueSwgZmlsZU5hbWU6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZG93bmxvYWRTZXJ2aWNlLmRvd25sb2FkSlNPTihqc29uLCBmaWxlTmFtZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDcmVhdGVzIGEgdHJ1c3RlZCBvYmplY3QgVVJMIGZyb20gdGhlIEJsb2IuXHJcbiAgICAgKiBXQVJOSU5HOiBjYWxsaW5nIHRoaXMgbWV0aG9kIHdpdGggdW50cnVzdGVkIHVzZXIgZGF0YSBleHBvc2VzIHlvdXIgYXBwbGljYXRpb24gdG8gWFNTIHNlY3VyaXR5IHJpc2tzIVxyXG4gICAgICogQHBhcmFtICBibG9iIERhdGEgdG8gd3JhcCBpbnRvIG9iamVjdCBVUkxcclxuICAgICAqIEByZXR1cm5zIFVSTCBzdHJpbmdcclxuICAgICAqL1xyXG4gICAgY3JlYXRlVHJ1c3RlZFVybChibG9iOiBCbG9iKTogc3RyaW5nIHtcclxuICAgICAgICBjb25zdCB1cmwgPSB3aW5kb3cuVVJMLmNyZWF0ZU9iamVjdFVSTChibG9iKTtcclxuICAgICAgICByZXR1cm4gPHN0cmluZz4gdGhpcy5zYW5pdGl6ZXIuYnlwYXNzU2VjdXJpdHlUcnVzdFVybCh1cmwpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0IGNvbnRlbnRBcGkoKTogQ29udGVudEFwaSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmNvbnRlbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAZGVwcmVjYXRlZCBpbiAzLjIuMCwgdXNlIFRodW1ibmFpbFNlcnZpY2UgaW5zdGVhZC5cclxuICAgICAqIEdldHMgYSB0aHVtYm5haWwgVVJMIGZvciB0aGUgZ2l2ZW4gZG9jdW1lbnQgbm9kZS5cclxuICAgICAqIEBwYXJhbSBub2RlIE5vZGUgb3IgTm9kZSBJRCB0byBnZXQgVVJMIGZvci5cclxuICAgICAqIEBwYXJhbSBhdHRhY2htZW50IFRvZ2dsZXMgd2hldGhlciB0byByZXRyaWV2ZSBjb250ZW50IGFzIGFuIGF0dGFjaG1lbnQgZm9yIGRvd25sb2FkXHJcbiAgICAgKiBAcGFyYW0gdGlja2V0IEN1c3RvbSB0aWNrZXQgdG8gdXNlIGZvciBhdXRoZW50aWNhdGlvblxyXG4gICAgICogQHJldHVybnMgVVJMIHN0cmluZ1xyXG4gICAgICovXHJcbiAgICBnZXREb2N1bWVudFRodW1ibmFpbFVybChub2RlOiBOb2RlRW50cnkgfCBzdHJpbmcsIGF0dGFjaG1lbnQ/OiBib29sZWFuLCB0aWNrZXQ/OiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnRodW1ibmFpbFNlcnZpY2UuZ2V0RG9jdW1lbnRUaHVtYm5haWxVcmwobm9kZSwgYXR0YWNobWVudCwgdGlja2V0KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBjb250ZW50IFVSTCBmb3IgdGhlIGdpdmVuIG5vZGUuXHJcbiAgICAgKiBAcGFyYW0gbm9kZSBOb2RlIG9yIE5vZGUgSUQgdG8gZ2V0IFVSTCBmb3IuXHJcbiAgICAgKiBAcGFyYW0gYXR0YWNobWVudCBUb2dnbGVzIHdoZXRoZXIgdG8gcmV0cmlldmUgY29udGVudCBhcyBhbiBhdHRhY2htZW50IGZvciBkb3dubG9hZFxyXG4gICAgICogQHBhcmFtIHRpY2tldCBDdXN0b20gdGlja2V0IHRvIHVzZSBmb3IgYXV0aGVudGljYXRpb25cclxuICAgICAqIEByZXR1cm5zIFVSTCBzdHJpbmcgb3IgYG51bGxgXHJcbiAgICAgKi9cclxuICAgIGdldENvbnRlbnRVcmwobm9kZTogTm9kZUVudHJ5IHwgc3RyaW5nLCBhdHRhY2htZW50PzogYm9vbGVhbiwgdGlja2V0Pzogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgICAgICBpZiAobm9kZSkge1xyXG4gICAgICAgICAgICBsZXQgbm9kZUlkOiBzdHJpbmc7XHJcblxyXG4gICAgICAgICAgICBpZiAodHlwZW9mIG5vZGUgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgICAgICBub2RlSWQgPSBub2RlO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKG5vZGUuZW50cnkpIHtcclxuICAgICAgICAgICAgICAgIG5vZGVJZCA9IG5vZGUuZW50cnkuaWQ7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbnRlbnRBcGkuZ2V0Q29udGVudFVybChub2RlSWQsIGF0dGFjaG1lbnQsIHRpY2tldCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgY29udGVudCBmb3IgdGhlIGdpdmVuIG5vZGUuXHJcbiAgICAgKiBAcGFyYW0gbm9kZUlkIElEIG9mIHRoZSB0YXJnZXQgbm9kZVxyXG4gICAgICogQHJldHVybnMgQ29udGVudCBkYXRhXHJcbiAgICAgKi9cclxuICAgIGdldE5vZGVDb250ZW50KG5vZGVJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5jb3JlLm5vZGVzQXBpLmdldEZpbGVDb250ZW50KG5vZGVJZCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyOiBhbnkpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBOb2RlIHZpYSBpdHMgbm9kZSBJRC5cclxuICAgICAqIEBwYXJhbSBub2RlSWQgSUQgb2YgdGhlIHRhcmdldCBub2RlXHJcbiAgICAgKiBAcGFyYW0gb3B0cyBPcHRpb25zIHN1cHBvcnRlZCBieSBKUy1BUElcclxuICAgICAqIEByZXR1cm5zIERldGFpbHMgb2YgdGhlIGZvbGRlclxyXG4gICAgICovXHJcbiAgICBnZXROb2RlKG5vZGVJZDogc3RyaW5nLCBvcHRzPzogYW55KTogT2JzZXJ2YWJsZTxOb2RlRW50cnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5ub2Rlcy5nZXROb2RlKG5vZGVJZCwgb3B0cykpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2tzIGlmIHRoZSB1c2VyIGhhcyBwZXJtaXNzaW9uIG9uIHRoYXQgbm9kZVxyXG4gICAgICogQHBhcmFtIG5vZGUgTm9kZSB0byBjaGVjayBwZXJtaXNzaW9uc1xyXG4gICAgICogQHBhcmFtIHBlcm1pc3Npb24gUmVxdWlyZWQgcGVybWlzc2lvbiB0eXBlXHJcbiAgICAgKiBAcmV0dXJucyBUcnVlIGlmIHRoZSB1c2VyIGhhcyB0aGUgcmVxdWlyZWQgcGVybWlzc2lvbnMsIGZhbHNlIG90aGVyd2lzZVxyXG4gICAgICovXHJcbiAgICBoYXNQZXJtaXNzaW9ucyhub2RlOiBOb2RlLCBwZXJtaXNzaW9uOiBQZXJtaXNzaW9uc0VudW0gfCBzdHJpbmcpOiBib29sZWFuIHtcclxuICAgICAgICBsZXQgaGFzUGVybWlzc2lvbnMgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgaWYgKG5vZGUgJiYgbm9kZS5wZXJtaXNzaW9ucyAmJiBub2RlLnBlcm1pc3Npb25zLmxvY2FsbHlTZXQpIHtcclxuICAgICAgICAgICAgaWYgKHBlcm1pc3Npb24gJiYgcGVybWlzc2lvbi5zdGFydHNXaXRoKCchJykpIHtcclxuICAgICAgICAgICAgICAgIGhhc1Blcm1pc3Npb25zID0gbm9kZS5wZXJtaXNzaW9ucy5sb2NhbGx5U2V0LmZpbmQoKGN1cnJlbnRQZXJtaXNzaW9uKSA9PiBjdXJyZW50UGVybWlzc2lvbi5uYW1lID09PSBwZXJtaXNzaW9uLnJlcGxhY2UoJyEnLCAnJykpID8gZmFsc2UgOiB0cnVlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaGFzUGVybWlzc2lvbnMgPSBub2RlLnBlcm1pc3Npb25zLmxvY2FsbHlTZXQuZmluZCgoY3VycmVudFBlcm1pc3Npb24pID0+IGN1cnJlbnRQZXJtaXNzaW9uLm5hbWUgPT09IHBlcm1pc3Npb24pID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0gZWxzZSB7XHJcblxyXG4gICAgICAgICAgICBpZiAocGVybWlzc2lvbiA9PT0gUGVybWlzc2lvbnNFbnVtLkNPTlNVTUVSKSB7XHJcbiAgICAgICAgICAgICAgICBoYXNQZXJtaXNzaW9ucyA9IHRydWU7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAocGVybWlzc2lvbiA9PT0gUGVybWlzc2lvbnNFbnVtLk5PVF9DT05TVU1FUikge1xyXG4gICAgICAgICAgICAgICAgaGFzUGVybWlzc2lvbnMgPSBmYWxzZTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChwZXJtaXNzaW9uICYmIHBlcm1pc3Npb24uc3RhcnRzV2l0aCgnIScpKSB7XHJcbiAgICAgICAgICAgICAgICBoYXNQZXJtaXNzaW9ucyA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBoYXNQZXJtaXNzaW9ucztcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrcyBpZiB0aGUgdXNlciBoYXMgcGVybWlzc2lvbnMgb24gdGhhdCBub2RlXHJcbiAgICAgKiBAcGFyYW0gbm9kZSBOb2RlIHRvIGNoZWNrIGFsbG93YWJsZU9wZXJhdGlvbnNcclxuICAgICAqIEBwYXJhbSBhbGxvd2FibGVPcGVyYXRpb24gQ3JlYXRlLCBkZWxldGUsIHVwZGF0ZSwgdXBkYXRlUGVybWlzc2lvbnMsICFjcmVhdGUsICFkZWxldGUsICF1cGRhdGUsICF1cGRhdGVQZXJtaXNzaW9uc1xyXG4gICAgICogQHJldHVybnMgVHJ1ZSBpZiB0aGUgdXNlciBoYXMgdGhlIHJlcXVpcmVkIHBlcm1pc3Npb25zLCBmYWxzZSBvdGhlcndpc2VcclxuICAgICAqL1xyXG4gICAgaGFzQWxsb3dhYmxlT3BlcmF0aW9ucyhub2RlOiBOb2RlLCBhbGxvd2FibGVPcGVyYXRpb246IEFsbG93YWJsZU9wZXJhdGlvbnNFbnVtIHwgc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgbGV0IGhhc0FsbG93YWJsZU9wZXJhdGlvbnMgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgaWYgKG5vZGUgJiYgbm9kZS5hbGxvd2FibGVPcGVyYXRpb25zKSB7XHJcbiAgICAgICAgICAgIGlmIChhbGxvd2FibGVPcGVyYXRpb24gJiYgYWxsb3dhYmxlT3BlcmF0aW9uLnN0YXJ0c1dpdGgoJyEnKSkge1xyXG4gICAgICAgICAgICAgICAgaGFzQWxsb3dhYmxlT3BlcmF0aW9ucyA9IG5vZGUuYWxsb3dhYmxlT3BlcmF0aW9ucy5maW5kKChjdXJyZW50T3BlcmF0aW9uKSA9PiBjdXJyZW50T3BlcmF0aW9uID09PSBhbGxvd2FibGVPcGVyYXRpb24ucmVwbGFjZSgnIScsICcnKSkgPyBmYWxzZSA6IHRydWU7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBoYXNBbGxvd2FibGVPcGVyYXRpb25zID0gbm9kZS5hbGxvd2FibGVPcGVyYXRpb25zLmZpbmQoKGN1cnJlbnRPcGVyYXRpb24pID0+IGN1cnJlbnRPcGVyYXRpb24gPT09IGFsbG93YWJsZU9wZXJhdGlvbikgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKGFsbG93YWJsZU9wZXJhdGlvbiAmJiBhbGxvd2FibGVPcGVyYXRpb24uc3RhcnRzV2l0aCgnIScpKSB7XHJcbiAgICAgICAgICAgICAgICBoYXNBbGxvd2FibGVPcGVyYXRpb25zID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGFsbG93YWJsZU9wZXJhdGlvbiA9PT0gQWxsb3dhYmxlT3BlcmF0aW9uc0VudW0uQ09QWSkge1xyXG4gICAgICAgICAgICBoYXNBbGxvd2FibGVPcGVyYXRpb25zID0gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChhbGxvd2FibGVPcGVyYXRpb24gPT09IEFsbG93YWJsZU9wZXJhdGlvbnNFbnVtLkxPQ0spIHtcclxuICAgICAgICAgICAgaGFzQWxsb3dhYmxlT3BlcmF0aW9ucyA9IG5vZGUuaXNGaWxlO1xyXG5cclxuICAgICAgICAgICAgaWYgKG5vZGUuaXNMb2NrZWQgJiYgbm9kZS5hbGxvd2FibGVPcGVyYXRpb25zKSB7XHJcbiAgICAgICAgICAgICAgICBoYXNBbGxvd2FibGVPcGVyYXRpb25zID0gISF+bm9kZS5hbGxvd2FibGVPcGVyYXRpb25zLmluZGV4T2YoJ3VwZGF0ZVBlcm1pc3Npb25zJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBoYXNBbGxvd2FibGVPcGVyYXRpb25zO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaGFuZGxlRXJyb3IoZXJyb3I6IGFueSkge1xyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IgfHwgJ1NlcnZlciBlcnJvcicpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==