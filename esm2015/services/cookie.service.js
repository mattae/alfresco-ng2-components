/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class CookieService {
    /**
     * Checks if cookies are enabled.
     * @return {?} True if enabled, false otherwise
     */
    isEnabled() {
        // for certain scenarios Chrome may say 'true' but have cookies still disabled
        if (navigator.cookieEnabled === false) {
            return false;
        }
        document.cookie = 'test-cookie';
        return document.cookie.indexOf('test-cookie') >= 0;
    }
    /**
     * Retrieves a cookie by its key.
     * @param {?} key Key to identify the cookie
     * @return {?} The cookie data or null if it is not found
     */
    getItem(key) {
        /** @type {?} */
        const regexp = new RegExp('(?:' + key + '|;\s*' + key + ')=(.*?)(?:;|$)', 'g');
        /** @type {?} */
        const result = regexp.exec(document.cookie);
        return (result === null) ? null : result[1];
    }
    /**
     * Sets a cookie.
     * @param {?} key Key to identify the cookie
     * @param {?} data Data value to set for the cookie
     * @param {?} expiration Expiration date of the data
     * @param {?} path "Pathname" to store the cookie
     * @return {?}
     */
    setItem(key, data, expiration, path) {
        document.cookie = `${key}=${data}` +
            (expiration ? ';expires=' + expiration.toUTCString() : '') +
            (path ? `;path=${path}` : ';path=/');
    }
    /**
     * Placeholder for testing purposes - do not use.
     * @return {?}
     */
    clear() {
        /* placeholder for testing purposes */
    }
}
CookieService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */ CookieService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function CookieService_Factory() { return new CookieService(); }, token: CookieService, providedIn: "root" });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29va2llLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9jb29raWUuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDOztBQUszQyxNQUFNLE9BQU8sYUFBYTs7Ozs7SUFNdEIsU0FBUztRQUNMLDhFQUE4RTtRQUM5RSxJQUFJLFNBQVMsQ0FBQyxhQUFhLEtBQUssS0FBSyxFQUFFO1lBQ25DLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBRUQsUUFBUSxDQUFDLE1BQU0sR0FBRyxhQUFhLENBQUM7UUFDaEMsT0FBTyxRQUFRLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdkQsQ0FBQzs7Ozs7O0lBT0QsT0FBTyxDQUFDLEdBQVc7O2NBQ1QsTUFBTSxHQUFHLElBQUksTUFBTSxDQUFDLEtBQUssR0FBRyxHQUFHLEdBQUcsT0FBTyxHQUFHLEdBQUcsR0FBRyxnQkFBZ0IsRUFBRSxHQUFHLENBQUM7O2NBQ3hFLE1BQU0sR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUM7UUFDM0MsT0FBTyxDQUFDLE1BQU0sS0FBSyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDaEQsQ0FBQzs7Ozs7Ozs7O0lBU0QsT0FBTyxDQUFDLEdBQVcsRUFBRSxJQUFZLEVBQUUsVUFBdUIsRUFBRSxJQUFtQjtRQUMzRSxRQUFRLENBQUMsTUFBTSxHQUFHLEdBQUcsR0FBRyxJQUFJLElBQUksRUFBRTtZQUM5QixDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsV0FBVyxHQUFHLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1lBQzFELENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxTQUFTLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUM3QyxDQUFDOzs7OztJQUdELEtBQUs7UUFDRCxzQ0FBc0M7SUFDMUMsQ0FBQzs7O1lBOUNKLFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIENvb2tpZVNlcnZpY2Uge1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2tzIGlmIGNvb2tpZXMgYXJlIGVuYWJsZWQuXHJcbiAgICAgKiBAcmV0dXJucyBUcnVlIGlmIGVuYWJsZWQsIGZhbHNlIG90aGVyd2lzZVxyXG4gICAgICovXHJcbiAgICBpc0VuYWJsZWQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgLy8gZm9yIGNlcnRhaW4gc2NlbmFyaW9zIENocm9tZSBtYXkgc2F5ICd0cnVlJyBidXQgaGF2ZSBjb29raWVzIHN0aWxsIGRpc2FibGVkXHJcbiAgICAgICAgaWYgKG5hdmlnYXRvci5jb29raWVFbmFibGVkID09PSBmYWxzZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBkb2N1bWVudC5jb29raWUgPSAndGVzdC1jb29raWUnO1xyXG4gICAgICAgIHJldHVybiBkb2N1bWVudC5jb29raWUuaW5kZXhPZigndGVzdC1jb29raWUnKSA+PSAwO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0cmlldmVzIGEgY29va2llIGJ5IGl0cyBrZXkuXHJcbiAgICAgKiBAcGFyYW0ga2V5IEtleSB0byBpZGVudGlmeSB0aGUgY29va2llXHJcbiAgICAgKiBAcmV0dXJucyBUaGUgY29va2llIGRhdGEgb3IgbnVsbCBpZiBpdCBpcyBub3QgZm91bmRcclxuICAgICAqL1xyXG4gICAgZ2V0SXRlbShrZXk6IHN0cmluZyk6IHN0cmluZyB8IG51bGwge1xyXG4gICAgICAgIGNvbnN0IHJlZ2V4cCA9IG5ldyBSZWdFeHAoJyg/OicgKyBrZXkgKyAnfDtcXHMqJyArIGtleSArICcpPSguKj8pKD86O3wkKScsICdnJyk7XHJcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gcmVnZXhwLmV4ZWMoZG9jdW1lbnQuY29va2llKTtcclxuICAgICAgICByZXR1cm4gKHJlc3VsdCA9PT0gbnVsbCkgPyBudWxsIDogcmVzdWx0WzFdO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0cyBhIGNvb2tpZS5cclxuICAgICAqIEBwYXJhbSBrZXkgS2V5IHRvIGlkZW50aWZ5IHRoZSBjb29raWVcclxuICAgICAqIEBwYXJhbSBkYXRhIERhdGEgdmFsdWUgdG8gc2V0IGZvciB0aGUgY29va2llXHJcbiAgICAgKiBAcGFyYW0gZXhwaXJhdGlvbiBFeHBpcmF0aW9uIGRhdGUgb2YgdGhlIGRhdGFcclxuICAgICAqIEBwYXJhbSBwYXRoIFwiUGF0aG5hbWVcIiB0byBzdG9yZSB0aGUgY29va2llXHJcbiAgICAgKi9cclxuICAgIHNldEl0ZW0oa2V5OiBzdHJpbmcsIGRhdGE6IHN0cmluZywgZXhwaXJhdGlvbjogRGF0ZSB8IG51bGwsIHBhdGg6IHN0cmluZyB8IG51bGwpOiB2b2lkIHtcclxuICAgICAgICBkb2N1bWVudC5jb29raWUgPSBgJHtrZXl9PSR7ZGF0YX1gICtcclxuICAgICAgICAgICAgKGV4cGlyYXRpb24gPyAnO2V4cGlyZXM9JyArIGV4cGlyYXRpb24udG9VVENTdHJpbmcoKSA6ICcnKSArXHJcbiAgICAgICAgICAgIChwYXRoID8gYDtwYXRoPSR7cGF0aH1gIDogJztwYXRoPS8nKTtcclxuICAgIH1cclxuXHJcbiAgICAvKiogUGxhY2Vob2xkZXIgZm9yIHRlc3RpbmcgcHVycG9zZXMgLSBkbyBub3QgdXNlLiAqL1xyXG4gICAgY2xlYXIoKSB7XHJcbiAgICAgICAgLyogcGxhY2Vob2xkZXIgZm9yIHRlc3RpbmcgcHVycG9zZXMgKi9cclxuICAgIH1cclxufVxyXG4iXX0=