/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, of } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { UserPreferencesService } from './user-preferences.service';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./user-preferences.service";
export class DeletedNodesApiService {
    /**
     * @param {?} apiService
     * @param {?} preferences
     */
    constructor(apiService, preferences) {
        this.apiService = apiService;
        this.preferences = preferences;
    }
    /**
     * @private
     * @return {?}
     */
    get nodesApi() {
        return this.apiService.getInstance().core.nodesApi;
    }
    /**
     * Gets a list of nodes in the trash.
     * @param {?=} options Options for JS-API call
     * @return {?} List of nodes in the trash
     */
    getDeletedNodes(options) {
        /** @type {?} */
        const defaultOptions = {
            include: ['path', 'properties'],
            maxItems: this.preferences.paginationSize,
            skipCount: 0
        };
        /** @type {?} */
        const queryOptions = Object.assign(defaultOptions, options);
        /** @type {?} */
        const promise = this.nodesApi.getDeletedNodes(queryOptions);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => of(err))));
    }
}
DeletedNodesApiService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
DeletedNodesApiService.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: UserPreferencesService }
];
/** @nocollapse */ DeletedNodesApiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function DeletedNodesApiService_Factory() { return new DeletedNodesApiService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.UserPreferencesService)); }, token: DeletedNodesApiService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    DeletedNodesApiService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    DeletedNodesApiService.prototype.preferences;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsZXRlZC1ub2Rlcy1hcGkuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2RlbGV0ZWQtbm9kZXMtYXBpLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQWMsSUFBSSxFQUFFLEVBQUUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUc1QyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUNwRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7QUFLNUMsTUFBTSxPQUFPLHNCQUFzQjs7Ozs7SUFDL0IsWUFDWSxVQUE4QixFQUM5QixXQUFtQztRQURuQyxlQUFVLEdBQVYsVUFBVSxDQUFvQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBd0I7SUFDNUMsQ0FBQzs7Ozs7SUFFSixJQUFZLFFBQVE7UUFDakIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDdEQsQ0FBQzs7Ozs7O0lBT0QsZUFBZSxDQUFDLE9BQWdCOztjQUN0QixjQUFjLEdBQUc7WUFDbkIsT0FBTyxFQUFFLENBQUUsTUFBTSxFQUFFLFlBQVksQ0FBRTtZQUNqQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjO1lBQ3pDLFNBQVMsRUFBRSxDQUFDO1NBQ2Y7O2NBQ0ssWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQzs7Y0FDckQsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQztRQUUzRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQ3JCLFVBQVU7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQy9CLENBQUM7SUFDTixDQUFDOzs7WUE5QkosVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBTlEsa0JBQWtCO1lBQ2xCLHNCQUFzQjs7Ozs7Ozs7SUFRdkIsNENBQXNDOzs7OztJQUN0Qyw2Q0FBMkMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmcm9tLCBvZiB9IGZyb20gJ3J4anMnO1xyXG5cclxuaW1wb3J0IHsgTm9kZVBhZ2luZyB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXNlclByZWZlcmVuY2VzU2VydmljZSB9IGZyb20gJy4vdXNlci1wcmVmZXJlbmNlcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgY2F0Y2hFcnJvciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRGVsZXRlZE5vZGVzQXBpU2VydmljZSB7XHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIHByZWZlcmVuY2VzOiBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlXHJcbiAgICApIHt9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXQgbm9kZXNBcGkoKSB7XHJcbiAgICAgICByZXR1cm4gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuY29yZS5ub2Rlc0FwaTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBsaXN0IG9mIG5vZGVzIGluIHRoZSB0cmFzaC5cclxuICAgICAqIEBwYXJhbSBvcHRpb25zIE9wdGlvbnMgZm9yIEpTLUFQSSBjYWxsXHJcbiAgICAgKiBAcmV0dXJucyBMaXN0IG9mIG5vZGVzIGluIHRoZSB0cmFzaFxyXG4gICAgICovXHJcbiAgICBnZXREZWxldGVkTm9kZXMob3B0aW9ucz86IE9iamVjdCk6IE9ic2VydmFibGU8Tm9kZVBhZ2luZz4ge1xyXG4gICAgICAgIGNvbnN0IGRlZmF1bHRPcHRpb25zID0ge1xyXG4gICAgICAgICAgICBpbmNsdWRlOiBbICdwYXRoJywgJ3Byb3BlcnRpZXMnIF0sXHJcbiAgICAgICAgICAgIG1heEl0ZW1zOiB0aGlzLnByZWZlcmVuY2VzLnBhZ2luYXRpb25TaXplLFxyXG4gICAgICAgICAgICBza2lwQ291bnQ6IDBcclxuICAgICAgICB9O1xyXG4gICAgICAgIGNvbnN0IHF1ZXJ5T3B0aW9ucyA9IE9iamVjdC5hc3NpZ24oZGVmYXVsdE9wdGlvbnMsIG9wdGlvbnMpO1xyXG4gICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLm5vZGVzQXBpLmdldERlbGV0ZWROb2RlcyhxdWVyeU9wdGlvbnMpO1xyXG5cclxuICAgICAgICByZXR1cm4gZnJvbShwcm9taXNlKS5waXBlKFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IG9mKGVycikpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=