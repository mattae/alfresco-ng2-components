/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { BpmProductVersionModel, EcmProductVersionModel } from '../models/product-version.model';
import { AlfrescoApiService } from './alfresco-api.service';
import { map, catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
export class DiscoveryApiService {
    /**
     * @param {?} apiService
     */
    constructor(apiService) {
        this.apiService = apiService;
    }
    /**
     * Gets product information for Content Services.
     * @return {?} ProductVersionModel containing product details
     */
    getEcmProductInfo() {
        return from(this.apiService.getInstance().discovery.discoveryApi.getRepositoryInformation())
            .pipe(map((/**
         * @param {?} res
         * @return {?}
         */
        (res) => new EcmProductVersionModel(res))), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => throwError(err))));
    }
    /**
     * Gets product information for Process Services.
     * @return {?} ProductVersionModel containing product details
     */
    getBpmProductInfo() {
        return from(this.apiService.getInstance().activiti.aboutApi.getAppVersion())
            .pipe(map((/**
         * @param {?} res
         * @return {?}
         */
        (res) => new BpmProductVersionModel(res))), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => throwError(err))));
    }
}
DiscoveryApiService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
DiscoveryApiService.ctorParameters = () => [
    { type: AlfrescoApiService }
];
/** @nocollapse */ DiscoveryApiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function DiscoveryApiService_Factory() { return new DiscoveryApiService(i0.ɵɵinject(i1.AlfrescoApiService)); }, token: DiscoveryApiService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    DiscoveryApiService.prototype.apiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlzY292ZXJ5LWFwaS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvZGlzY292ZXJ5LWFwaS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDeEMsT0FBTyxFQUFFLHNCQUFzQixFQUFFLHNCQUFzQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDakcsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDNUQsT0FBTyxFQUFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7O0FBTWpELE1BQU0sT0FBTyxtQkFBbUI7Ozs7SUFFNUIsWUFBb0IsVUFBOEI7UUFBOUIsZUFBVSxHQUFWLFVBQVUsQ0FBb0I7SUFBSSxDQUFDOzs7OztJQU1oRCxpQkFBaUI7UUFDcEIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLHdCQUF3QixFQUFFLENBQUM7YUFDdkYsSUFBSSxDQUNELEdBQUc7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxzQkFBc0IsQ0FBQyxHQUFHLENBQUMsRUFBQyxFQUM3QyxVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUN2QyxDQUFDO0lBQ1YsQ0FBQzs7Ozs7SUFNTSxpQkFBaUI7UUFDcEIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLGFBQWEsRUFBRSxDQUFDO2FBQ3ZFLElBQUksQ0FDRCxHQUFHOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksc0JBQXNCLENBQUMsR0FBRyxDQUFDLEVBQUMsRUFDN0MsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDdkMsQ0FBQztJQUNWLENBQUM7OztZQTdCSixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFOUSxrQkFBa0I7Ozs7Ozs7O0lBU1gseUNBQXNDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgZnJvbSwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBCcG1Qcm9kdWN0VmVyc2lvbk1vZGVsLCBFY21Qcm9kdWN0VmVyc2lvbk1vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL3Byb2R1Y3QtdmVyc2lvbi5tb2RlbCc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4vYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBtYXAsIGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRGlzY292ZXJ5QXBpU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UpIHsgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBwcm9kdWN0IGluZm9ybWF0aW9uIGZvciBDb250ZW50IFNlcnZpY2VzLlxyXG4gICAgICogQHJldHVybnMgUHJvZHVjdFZlcnNpb25Nb2RlbCBjb250YWluaW5nIHByb2R1Y3QgZGV0YWlsc1xyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0RWNtUHJvZHVjdEluZm8oKTogT2JzZXJ2YWJsZTxFY21Qcm9kdWN0VmVyc2lvbk1vZGVsPiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuZGlzY292ZXJ5LmRpc2NvdmVyeUFwaS5nZXRSZXBvc2l0b3J5SW5mb3JtYXRpb24oKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAoKHJlcykgPT4gbmV3IEVjbVByb2R1Y3RWZXJzaW9uTW9kZWwocmVzKSksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRocm93RXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgcHJvZHVjdCBpbmZvcm1hdGlvbiBmb3IgUHJvY2VzcyBTZXJ2aWNlcy5cclxuICAgICAqIEByZXR1cm5zIFByb2R1Y3RWZXJzaW9uTW9kZWwgY29udGFpbmluZyBwcm9kdWN0IGRldGFpbHNcclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldEJwbVByb2R1Y3RJbmZvKCk6IE9ic2VydmFibGU8QnBtUHJvZHVjdFZlcnNpb25Nb2RlbD4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmFjdGl2aXRpLmFib3V0QXBpLmdldEFwcFZlcnNpb24oKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAoKHJlcykgPT4gbmV3IEJwbVByb2R1Y3RWZXJzaW9uTW9kZWwocmVzKSksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRocm93RXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=