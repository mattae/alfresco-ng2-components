/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { LogService } from './log.service';
import { AlfrescoApiService } from './alfresco-api.service';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./log.service";
export class DownloadZipService {
    /**
     * @param {?} apiService
     * @param {?} logService
     */
    constructor(apiService, logService) {
        this.apiService = apiService;
        this.logService = logService;
    }
    /**
     * Creates a new download.
     * @param {?} payload Object containing the node IDs of the items to add to the ZIP file
     * @return {?} Status object for the download
     */
    createDownload(payload) {
        return from(this.apiService.getInstance().core.downloadsApi.createDownload(payload)).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets a content URL for the given node.
     * @param {?} nodeId Node to get URL for.
     * @param {?=} attachment Toggles whether to retrieve content as an attachment for download
     * @return {?} URL string
     */
    getContentUrl(nodeId, attachment) {
        return this.apiService.getInstance().content.getContentUrl(nodeId, attachment);
    }
    /**
     * Gets a Node via its node ID.
     * @param {?} nodeId ID of the target node
     * @return {?} Details of the node
     */
    getNode(nodeId) {
        return from(this.apiService.getInstance().core.nodesApi.getNode(nodeId));
    }
    /**
     * Gets status information for a download node.
     * @param {?} downloadId ID of the download node
     * @return {?} Status object for the download
     */
    getDownload(downloadId) {
        return from(this.apiService.getInstance().core.downloadsApi.getDownload(downloadId));
    }
    /**
     * Cancels a download.
     * @param {?} downloadId ID of the target download node
     * @return {?}
     */
    cancelDownload(downloadId) {
        this.apiService.getInstance().core.downloadsApi.cancelDownload(downloadId);
    }
    /**
     * @private
     * @param {?} error
     * @return {?}
     */
    handleError(error) {
        this.logService.error(error);
        return throwError(error || 'Server error');
    }
}
DownloadZipService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
DownloadZipService.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: LogService }
];
/** @nocollapse */ DownloadZipService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function DownloadZipService_Factory() { return new DownloadZipService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: DownloadZipService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    DownloadZipService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    DownloadZipService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG93bmxvYWQtemlwLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9kb3dubG9hZC16aXAuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBYyxJQUFJLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3BELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDNUQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBSzVDLE1BQU0sT0FBTyxrQkFBa0I7Ozs7O0lBRTNCLFlBQW9CLFVBQThCLEVBQzlCLFVBQXNCO1FBRHRCLGVBQVUsR0FBVixVQUFVLENBQW9CO1FBQzlCLGVBQVUsR0FBVixVQUFVLENBQVk7SUFDMUMsQ0FBQzs7Ozs7O0lBT0QsY0FBYyxDQUFDLE9BQTJCO1FBQ3RDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQ3JGLFVBQVU7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUM3QyxDQUFDO0lBQ04sQ0FBQzs7Ozs7OztJQVFELGFBQWEsQ0FBQyxNQUFjLEVBQUUsVUFBb0I7UUFDOUMsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ25GLENBQUM7Ozs7OztJQU9ELE9BQU8sQ0FBQyxNQUFjO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUM3RSxDQUFDOzs7Ozs7SUFPRCxXQUFXLENBQUMsVUFBa0I7UUFDMUIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDO0lBQ3pGLENBQUM7Ozs7OztJQU1ELGNBQWMsQ0FBQyxVQUFrQjtRQUM3QixJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQy9FLENBQUM7Ozs7OztJQUVPLFdBQVcsQ0FBQyxLQUFVO1FBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLE9BQU8sVUFBVSxDQUFDLEtBQUssSUFBSSxjQUFjLENBQUMsQ0FBQztJQUMvQyxDQUFDOzs7WUEzREosVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBTFEsa0JBQWtCO1lBRGxCLFVBQVU7Ozs7Ozs7O0lBU0gsd0NBQXNDOzs7OztJQUN0Qyx3Q0FBOEIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgTm9kZUVudHJ5LCBEb3dubG9hZEVudHJ5LCBEb3dubG9hZEJvZHlDcmVhdGUgfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmcm9tLCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIERvd25sb2FkWmlwU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZXMgYSBuZXcgZG93bmxvYWQuXHJcbiAgICAgKiBAcGFyYW0gcGF5bG9hZCBPYmplY3QgY29udGFpbmluZyB0aGUgbm9kZSBJRHMgb2YgdGhlIGl0ZW1zIHRvIGFkZCB0byB0aGUgWklQIGZpbGVcclxuICAgICAqIEByZXR1cm5zIFN0YXR1cyBvYmplY3QgZm9yIHRoZSBkb3dubG9hZFxyXG4gICAgICovXHJcbiAgICBjcmVhdGVEb3dubG9hZChwYXlsb2FkOiBEb3dubG9hZEJvZHlDcmVhdGUpOiBPYnNlcnZhYmxlPERvd25sb2FkRW50cnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5jb3JlLmRvd25sb2Fkc0FwaS5jcmVhdGVEb3dubG9hZChwYXlsb2FkKSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBjb250ZW50IFVSTCBmb3IgdGhlIGdpdmVuIG5vZGUuXHJcbiAgICAgKiBAcGFyYW0gbm9kZUlkIE5vZGUgdG8gZ2V0IFVSTCBmb3IuXHJcbiAgICAgKiBAcGFyYW0gYXR0YWNobWVudCBUb2dnbGVzIHdoZXRoZXIgdG8gcmV0cmlldmUgY29udGVudCBhcyBhbiBhdHRhY2htZW50IGZvciBkb3dubG9hZFxyXG4gICAgICogQHJldHVybnMgVVJMIHN0cmluZ1xyXG4gICAgICovXHJcbiAgICBnZXRDb250ZW50VXJsKG5vZGVJZDogc3RyaW5nLCBhdHRhY2htZW50PzogYm9vbGVhbik6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmNvbnRlbnQuZ2V0Q29udGVudFVybChub2RlSWQsIGF0dGFjaG1lbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhIE5vZGUgdmlhIGl0cyBub2RlIElELlxyXG4gICAgICogQHBhcmFtIG5vZGVJZCBJRCBvZiB0aGUgdGFyZ2V0IG5vZGVcclxuICAgICAqIEByZXR1cm5zIERldGFpbHMgb2YgdGhlIG5vZGVcclxuICAgICAqL1xyXG4gICAgZ2V0Tm9kZShub2RlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8Tm9kZUVudHJ5PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuY29yZS5ub2Rlc0FwaS5nZXROb2RlKG5vZGVJZCkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBzdGF0dXMgaW5mb3JtYXRpb24gZm9yIGEgZG93bmxvYWQgbm9kZS5cclxuICAgICAqIEBwYXJhbSBkb3dubG9hZElkIElEIG9mIHRoZSBkb3dubG9hZCBub2RlXHJcbiAgICAgKiBAcmV0dXJucyBTdGF0dXMgb2JqZWN0IGZvciB0aGUgZG93bmxvYWRcclxuICAgICAqL1xyXG4gICAgZ2V0RG93bmxvYWQoZG93bmxvYWRJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxEb3dubG9hZEVudHJ5PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuY29yZS5kb3dubG9hZHNBcGkuZ2V0RG93bmxvYWQoZG93bmxvYWRJZCkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2FuY2VscyBhIGRvd25sb2FkLlxyXG4gICAgICogQHBhcmFtIGRvd25sb2FkSWQgSUQgb2YgdGhlIHRhcmdldCBkb3dubG9hZCBub2RlXHJcbiAgICAgKi9cclxuICAgIGNhbmNlbERvd25sb2FkKGRvd25sb2FkSWQ6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmNvcmUuZG93bmxvYWRzQXBpLmNhbmNlbERvd25sb2FkKGRvd25sb2FkSWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaGFuZGxlRXJyb3IoZXJyb3I6IGFueSkge1xyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IgfHwgJ1NlcnZlciBlcnJvcicpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==