/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class DownloadService {
    constructor() {
        this.saveData = ((/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            const a = document.createElement('a');
            document.body.appendChild(a);
            a.style.display = 'none';
            return (/**
             * @param {?} fileData
             * @param {?} format
             * @param {?} fileName
             * @return {?}
             */
            function (fileData, format, fileName) {
                /** @type {?} */
                let blob = null;
                if (format === 'blob' || format === 'data') {
                    blob = new Blob([fileData], { type: 'octet/stream' });
                }
                if (format === 'object' || format === 'json') {
                    /** @type {?} */
                    const json = JSON.stringify(fileData);
                    blob = new Blob([json], { type: 'octet/stream' });
                }
                if (blob) {
                    if (typeof window.navigator !== 'undefined' &&
                        window.navigator.msSaveOrOpenBlob) {
                        navigator.msSaveOrOpenBlob(blob, fileName);
                    }
                    else {
                        /** @type {?} */
                        const url = window.URL.createObjectURL(blob);
                        a.href = url;
                        a.download = fileName;
                        a.click();
                        window.URL.revokeObjectURL(url);
                    }
                }
            });
        }))();
    }
    /**
     * Invokes content download for a Blob with a file name.
     * @param {?} blob Content to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    downloadBlob(blob, fileName) {
        this.saveData(blob, 'blob', fileName);
    }
    /**
     * Invokes content download for a data array with a file name.
     * @param {?} data Data to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    downloadData(data, fileName) {
        this.saveData(data, 'data', fileName);
    }
    /**
     * Invokes content download for a JSON object with a file name.
     * @param {?} json JSON object to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    downloadJSON(json, fileName) {
        this.saveData(json, 'json', fileName);
    }
}
DownloadService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
DownloadService.ctorParameters = () => [];
/** @nocollapse */ DownloadService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function DownloadService_Factory() { return new DownloadService(); }, token: DownloadService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    DownloadService.prototype.saveData;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG93bmxvYWQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2Rvd25sb2FkLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFLM0MsTUFBTSxPQUFPLGVBQWU7SUFHeEI7UUFDSSxJQUFJLENBQUMsUUFBUSxHQUFHOzs7UUFBQzs7a0JBQ1AsQ0FBQyxHQUFHLFFBQVEsQ0FBQyxhQUFhLENBQUMsR0FBRyxDQUFDO1lBQ3JDLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDO1lBQzdCLENBQUMsQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztZQUV6Qjs7Ozs7O1lBQU8sVUFBUyxRQUFRLEVBQUUsTUFBTSxFQUFFLFFBQVE7O29CQUNsQyxJQUFJLEdBQUcsSUFBSTtnQkFFZixJQUFJLE1BQU0sS0FBSyxNQUFNLElBQUksTUFBTSxLQUFLLE1BQU0sRUFBRTtvQkFDeEMsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsUUFBUSxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsY0FBYyxFQUFFLENBQUMsQ0FBQztpQkFDekQ7Z0JBRUQsSUFBSSxNQUFNLEtBQUssUUFBUSxJQUFJLE1BQU0sS0FBSyxNQUFNLEVBQUU7OzBCQUNwQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUM7b0JBQ3JDLElBQUksR0FBRyxJQUFJLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLEVBQUUsSUFBSSxFQUFFLGNBQWMsRUFBRSxDQUFDLENBQUM7aUJBQ3JEO2dCQUVELElBQUksSUFBSSxFQUFFO29CQUNOLElBQ0ksT0FBTyxNQUFNLENBQUMsU0FBUyxLQUFLLFdBQVc7d0JBQ3ZDLE1BQU0sQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLEVBQ25DO3dCQUNFLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7cUJBQzlDO3lCQUFNOzs4QkFDRyxHQUFHLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDO3dCQUM1QyxDQUFDLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQzt3QkFDYixDQUFDLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQzt3QkFDdEIsQ0FBQyxDQUFDLEtBQUssRUFBRSxDQUFDO3dCQUVWLE1BQU0sQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxDQUFDO3FCQUNuQztpQkFDSjtZQUNMLENBQUMsRUFBQztRQUNOLENBQUMsRUFBQyxFQUFFLENBQUM7SUFDVCxDQUFDOzs7Ozs7O0lBT0QsWUFBWSxDQUFDLElBQVUsRUFBRSxRQUFnQjtRQUNyQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDMUMsQ0FBQzs7Ozs7OztJQU9ELFlBQVksQ0FBQyxJQUFTLEVBQUUsUUFBZ0I7UUFDcEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQzFDLENBQUM7Ozs7Ozs7SUFPRCxZQUFZLENBQUMsSUFBUyxFQUFFLFFBQWdCO1FBQ3BDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQztJQUMxQyxDQUFDOzs7WUFwRUosVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7Ozs7Ozs7O0lBRUcsbUNBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRG93bmxvYWRTZXJ2aWNlIHtcclxuICAgIHByaXZhdGUgc2F2ZURhdGE6IEZ1bmN0aW9uO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMuc2F2ZURhdGEgPSAoZnVuY3Rpb24oKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGEgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdhJyk7XHJcbiAgICAgICAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQoYSk7XHJcbiAgICAgICAgICAgIGEuc3R5bGUuZGlzcGxheSA9ICdub25lJztcclxuXHJcbiAgICAgICAgICAgIHJldHVybiBmdW5jdGlvbihmaWxlRGF0YSwgZm9ybWF0LCBmaWxlTmFtZSkge1xyXG4gICAgICAgICAgICAgICAgbGV0IGJsb2IgPSBudWxsO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmIChmb3JtYXQgPT09ICdibG9iJyB8fCBmb3JtYXQgPT09ICdkYXRhJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGJsb2IgPSBuZXcgQmxvYihbZmlsZURhdGFdLCB7IHR5cGU6ICdvY3RldC9zdHJlYW0nIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmIChmb3JtYXQgPT09ICdvYmplY3QnIHx8IGZvcm1hdCA9PT0gJ2pzb24nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QganNvbiA9IEpTT04uc3RyaW5naWZ5KGZpbGVEYXRhKTtcclxuICAgICAgICAgICAgICAgICAgICBibG9iID0gbmV3IEJsb2IoW2pzb25dLCB7IHR5cGU6ICdvY3RldC9zdHJlYW0nIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmIChibG9iKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlb2Ygd2luZG93Lm5hdmlnYXRvciAhPT0gJ3VuZGVmaW5lZCcgJiZcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2luZG93Lm5hdmlnYXRvci5tc1NhdmVPck9wZW5CbG9iXHJcbiAgICAgICAgICAgICAgICAgICAgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hdmlnYXRvci5tc1NhdmVPck9wZW5CbG9iKGJsb2IsIGZpbGVOYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB1cmwgPSB3aW5kb3cuVVJMLmNyZWF0ZU9iamVjdFVSTChibG9iKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYS5ocmVmID0gdXJsO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhLmRvd25sb2FkID0gZmlsZU5hbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGEuY2xpY2soKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5VUkwucmV2b2tlT2JqZWN0VVJMKHVybCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH0pKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBJbnZva2VzIGNvbnRlbnQgZG93bmxvYWQgZm9yIGEgQmxvYiB3aXRoIGEgZmlsZSBuYW1lLlxyXG4gICAgICogQHBhcmFtIGJsb2IgQ29udGVudCB0byBkb3dubG9hZC5cclxuICAgICAqIEBwYXJhbSBmaWxlTmFtZSBOYW1lIG9mIHRoZSByZXN1bHRpbmcgZmlsZS5cclxuICAgICAqL1xyXG4gICAgZG93bmxvYWRCbG9iKGJsb2I6IEJsb2IsIGZpbGVOYW1lOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnNhdmVEYXRhKGJsb2IsICdibG9iJywgZmlsZU5hbWUpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogSW52b2tlcyBjb250ZW50IGRvd25sb2FkIGZvciBhIGRhdGEgYXJyYXkgd2l0aCBhIGZpbGUgbmFtZS5cclxuICAgICAqIEBwYXJhbSBkYXRhIERhdGEgdG8gZG93bmxvYWQuXHJcbiAgICAgKiBAcGFyYW0gZmlsZU5hbWUgTmFtZSBvZiB0aGUgcmVzdWx0aW5nIGZpbGUuXHJcbiAgICAgKi9cclxuICAgIGRvd25sb2FkRGF0YShkYXRhOiBhbnksIGZpbGVOYW1lOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnNhdmVEYXRhKGRhdGEsICdkYXRhJywgZmlsZU5hbWUpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogSW52b2tlcyBjb250ZW50IGRvd25sb2FkIGZvciBhIEpTT04gb2JqZWN0IHdpdGggYSBmaWxlIG5hbWUuXHJcbiAgICAgKiBAcGFyYW0ganNvbiBKU09OIG9iamVjdCB0byBkb3dubG9hZC5cclxuICAgICAqIEBwYXJhbSBmaWxlTmFtZSBOYW1lIG9mIHRoZSByZXN1bHRpbmcgZmlsZS5cclxuICAgICAqL1xyXG4gICAgZG93bmxvYWRKU09OKGpzb246IGFueSwgZmlsZU5hbWU6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuc2F2ZURhdGEoanNvbiwgJ2pzb24nLCBmaWxlTmFtZSk7XHJcbiAgICB9XHJcbn1cclxuIl19