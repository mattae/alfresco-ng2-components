/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, of } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { UserPreferencesService } from './user-preferences.service';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./user-preferences.service";
export class FavoritesApiService {
    /**
     * @param {?} apiService
     * @param {?} preferences
     */
    constructor(apiService, preferences) {
        this.apiService = apiService;
        this.preferences = preferences;
    }
    /**
     * @param {?} __0
     * @return {?}
     */
    static remapEntry({ entry }) {
        entry.properties = {
            'cm:title': entry.title,
            'cm:description': entry.description
        };
        return { entry };
    }
    /**
     * @param {?=} data
     * @return {?}
     */
    remapFavoritesData(data = {}) {
        /** @type {?} */
        const list = (data.list || {});
        /** @type {?} */
        const pagination = (list.pagination || {});
        /** @type {?} */
        const entries = this
            .remapFavoriteEntries(list.entries || []);
        return (/** @type {?} */ ({
            list: { entries, pagination }
        }));
    }
    /**
     * @param {?} entries
     * @return {?}
     */
    remapFavoriteEntries(entries) {
        return entries
            .map((/**
         * @param {?} __0
         * @return {?}
         */
        ({ entry: { target } }) => ({
            entry: target.file || target.folder
        })))
            .filter((/**
         * @param {?} __0
         * @return {?}
         */
        ({ entry }) => (!!entry)))
            .map(FavoritesApiService.remapEntry);
    }
    /**
     * @private
     * @return {?}
     */
    get favoritesApi() {
        return this.apiService.getInstance().core.favoritesApi;
    }
    /**
     * Gets the favorites for a user.
     * @param {?} personId ID of the user
     * @param {?=} options Options supported by JS-API
     * @return {?} List of favorites
     */
    getFavorites(personId, options) {
        /** @type {?} */
        const defaultOptions = {
            maxItems: this.preferences.paginationSize,
            skipCount: 0,
            where: '(EXISTS(target/file) OR EXISTS(target/folder))',
            include: ['properties', 'allowableOperations']
        };
        /** @type {?} */
        const queryOptions = Object.assign(defaultOptions, options);
        /** @type {?} */
        const promise = this.favoritesApi
            .getFavorites(personId, queryOptions)
            .then(this.remapFavoritesData);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => of(err))));
    }
}
FavoritesApiService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
FavoritesApiService.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: UserPreferencesService }
];
/** @nocollapse */ FavoritesApiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function FavoritesApiService_Factory() { return new FavoritesApiService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.UserPreferencesService)); }, token: FavoritesApiService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    FavoritesApiService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    FavoritesApiService.prototype.preferences;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2b3JpdGVzLWFwaS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvZmF2b3JpdGVzLWFwaS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFjLElBQUksRUFBRSxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDNUMsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDNUQsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDcEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBSzVDLE1BQU0sT0FBTyxtQkFBbUI7Ozs7O0lBK0I1QixZQUNZLFVBQThCLEVBQzlCLFdBQW1DO1FBRG5DLGVBQVUsR0FBVixVQUFVLENBQW9CO1FBQzlCLGdCQUFXLEdBQVgsV0FBVyxDQUF3QjtJQUM1QyxDQUFDOzs7OztJQWhDSixNQUFNLENBQUMsVUFBVSxDQUFDLEVBQUUsS0FBSyxFQUFPO1FBQzVCLEtBQUssQ0FBQyxVQUFVLEdBQUc7WUFDZixVQUFVLEVBQUUsS0FBSyxDQUFDLEtBQUs7WUFDdkIsZ0JBQWdCLEVBQUUsS0FBSyxDQUFDLFdBQVc7U0FDdEMsQ0FBQztRQUVGLE9BQU8sRUFBRSxLQUFLLEVBQUUsQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELGtCQUFrQixDQUFDLE9BQVksRUFBRTs7Y0FDdkIsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUM7O2NBQ3hCLFVBQVUsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRSxDQUFDOztjQUNwQyxPQUFPLEdBQVUsSUFBSTthQUN0QixvQkFBb0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQztRQUU3QyxPQUFPLG1CQUFhO1lBQ2hCLElBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUU7U0FDaEMsRUFBQSxDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFFRCxvQkFBb0IsQ0FBQyxPQUFjO1FBQy9CLE9BQU8sT0FBTzthQUNULEdBQUc7Ozs7UUFBQyxDQUFDLEVBQUUsS0FBSyxFQUFFLEVBQUUsTUFBTSxFQUFFLEVBQU0sRUFBRSxFQUFFLENBQUMsQ0FBQztZQUNqQyxLQUFLLEVBQUUsTUFBTSxDQUFDLElBQUksSUFBSSxNQUFNLENBQUMsTUFBTTtTQUN0QyxDQUFDLEVBQUM7YUFDRixNQUFNOzs7O1FBQUMsQ0FBQyxFQUFFLEtBQUssRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsRUFBQzthQUNoQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDN0MsQ0FBQzs7Ozs7SUFPRCxJQUFZLFlBQVk7UUFDckIsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDMUQsQ0FBQzs7Ozs7OztJQVFELFlBQVksQ0FBQyxRQUFnQixFQUFFLE9BQWE7O2NBQ2xDLGNBQWMsR0FBRztZQUNuQixRQUFRLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjO1lBQ3pDLFNBQVMsRUFBRSxDQUFDO1lBQ1osS0FBSyxFQUFFLGdEQUFnRDtZQUN2RCxPQUFPLEVBQUUsQ0FBRSxZQUFZLEVBQUUscUJBQXFCLENBQUU7U0FDbkQ7O2NBQ0ssWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQzs7Y0FDckQsT0FBTyxHQUFHLElBQUksQ0FBQyxZQUFZO2FBQzVCLFlBQVksQ0FBQyxRQUFRLEVBQUUsWUFBWSxDQUFDO2FBQ3BDLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUM7UUFFbEMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUNyQixVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUMvQixDQUFDO0lBQ04sQ0FBQzs7O1lBaEVKLFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQjs7OztZQU5RLGtCQUFrQjtZQUNsQixzQkFBc0I7Ozs7Ozs7O0lBc0N2Qix5Q0FBc0M7Ozs7O0lBQ3RDLDBDQUEyQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5vZGVQYWdpbmcgfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgZnJvbSwgb2YgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UgfSBmcm9tICcuL3VzZXItcHJlZmVyZW5jZXMuc2VydmljZSc7XHJcbmltcG9ydCB7IGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEZhdm9yaXRlc0FwaVNlcnZpY2Uge1xyXG5cclxuICAgIHN0YXRpYyByZW1hcEVudHJ5KHsgZW50cnkgfTogYW55KTogYW55IHtcclxuICAgICAgICBlbnRyeS5wcm9wZXJ0aWVzID0ge1xyXG4gICAgICAgICAgICAnY206dGl0bGUnOiBlbnRyeS50aXRsZSxcclxuICAgICAgICAgICAgJ2NtOmRlc2NyaXB0aW9uJzogZW50cnkuZGVzY3JpcHRpb25cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4geyBlbnRyeSB9O1xyXG4gICAgfVxyXG5cclxuICAgIHJlbWFwRmF2b3JpdGVzRGF0YShkYXRhOiBhbnkgPSB7fSk6IE5vZGVQYWdpbmcge1xyXG4gICAgICAgIGNvbnN0IGxpc3QgPSAoZGF0YS5saXN0IHx8IHt9KTtcclxuICAgICAgICBjb25zdCBwYWdpbmF0aW9uID0gKGxpc3QucGFnaW5hdGlvbiB8fCB7fSk7XHJcbiAgICAgICAgY29uc3QgZW50cmllczogYW55W10gPSB0aGlzXHJcbiAgICAgICAgICAgIC5yZW1hcEZhdm9yaXRlRW50cmllcyhsaXN0LmVudHJpZXMgfHwgW10pO1xyXG5cclxuICAgICAgICByZXR1cm4gPE5vZGVQYWdpbmc+IHtcclxuICAgICAgICAgICAgbGlzdDogeyBlbnRyaWVzLCBwYWdpbmF0aW9uIH1cclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHJlbWFwRmF2b3JpdGVFbnRyaWVzKGVudHJpZXM6IGFueVtdKSB7XHJcbiAgICAgICAgcmV0dXJuIGVudHJpZXNcclxuICAgICAgICAgICAgLm1hcCgoeyBlbnRyeTogeyB0YXJnZXQgfX06IGFueSkgPT4gKHtcclxuICAgICAgICAgICAgICAgIGVudHJ5OiB0YXJnZXQuZmlsZSB8fCB0YXJnZXQuZm9sZGVyXHJcbiAgICAgICAgICAgIH0pKVxyXG4gICAgICAgICAgICAuZmlsdGVyKCh7IGVudHJ5IH0pID0+ICghIWVudHJ5KSlcclxuICAgICAgICAgICAgLm1hcChGYXZvcml0ZXNBcGlTZXJ2aWNlLnJlbWFwRW50cnkpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgYXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgcHJlZmVyZW5jZXM6IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2VcclxuICAgICkge31cclxuXHJcbiAgICBwcml2YXRlIGdldCBmYXZvcml0ZXNBcGkoKSB7XHJcbiAgICAgICByZXR1cm4gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuY29yZS5mYXZvcml0ZXNBcGk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBmYXZvcml0ZXMgZm9yIGEgdXNlci5cclxuICAgICAqIEBwYXJhbSBwZXJzb25JZCBJRCBvZiB0aGUgdXNlclxyXG4gICAgICogQHBhcmFtIG9wdGlvbnMgT3B0aW9ucyBzdXBwb3J0ZWQgYnkgSlMtQVBJXHJcbiAgICAgKiBAcmV0dXJucyBMaXN0IG9mIGZhdm9yaXRlc1xyXG4gICAgICovXHJcbiAgICBnZXRGYXZvcml0ZXMocGVyc29uSWQ6IHN0cmluZywgb3B0aW9ucz86IGFueSk6IE9ic2VydmFibGU8Tm9kZVBhZ2luZz4ge1xyXG4gICAgICAgIGNvbnN0IGRlZmF1bHRPcHRpb25zID0ge1xyXG4gICAgICAgICAgICBtYXhJdGVtczogdGhpcy5wcmVmZXJlbmNlcy5wYWdpbmF0aW9uU2l6ZSxcclxuICAgICAgICAgICAgc2tpcENvdW50OiAwLFxyXG4gICAgICAgICAgICB3aGVyZTogJyhFWElTVFModGFyZ2V0L2ZpbGUpIE9SIEVYSVNUUyh0YXJnZXQvZm9sZGVyKSknLFxyXG4gICAgICAgICAgICBpbmNsdWRlOiBbICdwcm9wZXJ0aWVzJywgJ2FsbG93YWJsZU9wZXJhdGlvbnMnIF1cclxuICAgICAgICB9O1xyXG4gICAgICAgIGNvbnN0IHF1ZXJ5T3B0aW9ucyA9IE9iamVjdC5hc3NpZ24oZGVmYXVsdE9wdGlvbnMsIG9wdGlvbnMpO1xyXG4gICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLmZhdm9yaXRlc0FwaVxyXG4gICAgICAgICAgICAuZ2V0RmF2b3JpdGVzKHBlcnNvbklkLCBxdWVyeU9wdGlvbnMpXHJcbiAgICAgICAgICAgIC50aGVuKHRoaXMucmVtYXBGYXZvcml0ZXNEYXRhKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20ocHJvbWlzZSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiBvZihlcnIpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19