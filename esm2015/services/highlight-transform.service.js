/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
/**
 * @record
 */
export function HighlightTransformResult() { }
if (false) {
    /** @type {?} */
    HighlightTransformResult.prototype.text;
    /** @type {?} */
    HighlightTransformResult.prototype.changed;
}
export class HighlightTransformService {
    /**
     * Searches for `search` string(s) within `text` and highlights all occurrences.
     * @param {?} text Text to search within
     * @param {?} search Text pattern to search for
     * @param {?=} wrapperClass CSS class used to provide highlighting style
     * @return {?} New text along with boolean value to indicate whether anything was highlighted
     */
    highlight(text, search, wrapperClass = 'adf-highlight') {
        /** @type {?} */
        let isMatching = false;
        /** @type {?} */
        let result = text;
        if (search && text) {
            /** @type {?} */
            let pattern = search.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&');
            pattern = pattern.split(' ').filter((/**
             * @param {?} t
             * @return {?}
             */
            (t) => {
                return t.length > 0;
            })).join('|');
            /** @type {?} */
            const regex = new RegExp(pattern, 'gi');
            result = text.replace(/<[^>]+>/g, '').replace(regex, (/**
             * @param {?} match
             * @return {?}
             */
            (match) => {
                isMatching = true;
                return `<span class="${wrapperClass}">${match}</span>`;
            }));
            return { text: result, changed: isMatching };
        }
        else {
            return { text: result, changed: isMatching };
        }
    }
}
HighlightTransformService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */ HighlightTransformService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function HighlightTransformService_Factory() { return new HighlightTransformService(); }, token: HighlightTransformService, providedIn: "root" });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGlnaGxpZ2h0LXRyYW5zZm9ybS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvaGlnaGxpZ2h0LXRyYW5zZm9ybS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7O0FBRTNDLDhDQUdDOzs7SUFGRyx3Q0FBYTs7SUFDYiwyQ0FBaUI7O0FBTXJCLE1BQU0sT0FBTyx5QkFBeUI7Ozs7Ozs7O0lBUzNCLFNBQVMsQ0FBQyxJQUFZLEVBQUUsTUFBYyxFQUFFLGVBQXVCLGVBQWU7O1lBQzdFLFVBQVUsR0FBRyxLQUFLOztZQUNsQixNQUFNLEdBQUcsSUFBSTtRQUVqQixJQUFJLE1BQU0sSUFBSSxJQUFJLEVBQUU7O2dCQUNaLE9BQU8sR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDLHFDQUFxQyxFQUFFLE1BQU0sQ0FBQztZQUMzRSxPQUFPLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxNQUFNOzs7O1lBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRTtnQkFDdEMsT0FBTyxDQUFDLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztZQUN4QixDQUFDLEVBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7O2tCQUVQLEtBQUssR0FBRyxJQUFJLE1BQU0sQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDO1lBQ3ZDLE1BQU0sR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsS0FBSzs7OztZQUFFLENBQUMsS0FBSyxFQUFFLEVBQUU7Z0JBQzNELFVBQVUsR0FBRyxJQUFJLENBQUM7Z0JBQ2xCLE9BQU8sZ0JBQWdCLFlBQVksS0FBSyxLQUFLLFNBQVMsQ0FBQztZQUMzRCxDQUFDLEVBQUMsQ0FBQztZQUVILE9BQU8sRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsQ0FBQztTQUNoRDthQUFNO1lBQ0gsT0FBTyxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxDQUFDO1NBQ2hEO0lBQ0wsQ0FBQzs7O1lBaENKLFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEhpZ2hsaWdodFRyYW5zZm9ybVJlc3VsdCB7XHJcbiAgICB0ZXh0OiBzdHJpbmc7XHJcbiAgICBjaGFuZ2VkOiBib29sZWFuO1xyXG59XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEhpZ2hsaWdodFRyYW5zZm9ybVNlcnZpY2Uge1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2VhcmNoZXMgZm9yIGBzZWFyY2hgIHN0cmluZyhzKSB3aXRoaW4gYHRleHRgIGFuZCBoaWdobGlnaHRzIGFsbCBvY2N1cnJlbmNlcy5cclxuICAgICAqIEBwYXJhbSB0ZXh0IFRleHQgdG8gc2VhcmNoIHdpdGhpblxyXG4gICAgICogQHBhcmFtIHNlYXJjaCBUZXh0IHBhdHRlcm4gdG8gc2VhcmNoIGZvclxyXG4gICAgICogQHBhcmFtIHdyYXBwZXJDbGFzcyBDU1MgY2xhc3MgdXNlZCB0byBwcm92aWRlIGhpZ2hsaWdodGluZyBzdHlsZVxyXG4gICAgICogQHJldHVybnMgTmV3IHRleHQgYWxvbmcgd2l0aCBib29sZWFuIHZhbHVlIHRvIGluZGljYXRlIHdoZXRoZXIgYW55dGhpbmcgd2FzIGhpZ2hsaWdodGVkXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBoaWdobGlnaHQodGV4dDogc3RyaW5nLCBzZWFyY2g6IHN0cmluZywgd3JhcHBlckNsYXNzOiBzdHJpbmcgPSAnYWRmLWhpZ2hsaWdodCcpOiBIaWdobGlnaHRUcmFuc2Zvcm1SZXN1bHQge1xyXG4gICAgICAgIGxldCBpc01hdGNoaW5nID0gZmFsc2UsXHJcbiAgICAgICAgICAgIHJlc3VsdCA9IHRleHQ7XHJcblxyXG4gICAgICAgIGlmIChzZWFyY2ggJiYgdGV4dCkge1xyXG4gICAgICAgICAgICBsZXQgcGF0dGVybiA9IHNlYXJjaC5yZXBsYWNlKC9bXFwtXFxbXFxdXFwvXFx7XFx9XFwoXFwpXFwqXFwrXFw/XFwuXFxcXFxcXlxcJFxcfF0vZywgJ1xcXFwkJicpO1xyXG4gICAgICAgICAgICBwYXR0ZXJuID0gcGF0dGVybi5zcGxpdCgnICcpLmZpbHRlcigodCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHQubGVuZ3RoID4gMDtcclxuICAgICAgICAgICAgfSkuam9pbignfCcpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgcmVnZXggPSBuZXcgUmVnRXhwKHBhdHRlcm4sICdnaScpO1xyXG4gICAgICAgICAgICByZXN1bHQgPSB0ZXh0LnJlcGxhY2UoLzxbXj5dKz4vZywgJycpLnJlcGxhY2UocmVnZXgsIChtYXRjaCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaXNNYXRjaGluZyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gYDxzcGFuIGNsYXNzPVwiJHt3cmFwcGVyQ2xhc3N9XCI+JHttYXRjaH08L3NwYW4+YDtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4geyB0ZXh0OiByZXN1bHQsIGNoYW5nZWQ6IGlzTWF0Y2hpbmcgfTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4geyB0ZXh0OiByZXN1bHQsIGNoYW5nZWQ6IGlzTWF0Y2hpbmcgfTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19