/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class JwtHelperService {
    constructor() {
    }
    /**
     * Decodes a JSON web token into a JS object.
     * @param {?} token Token in encoded form
     * @return {?} Decoded token data object
     */
    decodeToken(token) {
        /** @type {?} */
        const parts = token.split('.');
        if (parts.length !== 3) {
            throw new Error('JWT must have 3 parts');
        }
        /** @type {?} */
        const decoded = this.urlBase64Decode(parts[1]);
        if (!decoded) {
            throw new Error('Cannot decode the token');
        }
        return JSON.parse(decoded);
    }
    /**
     * @private
     * @param {?} token
     * @return {?}
     */
    urlBase64Decode(token) {
        /** @type {?} */
        let output = token.replace(/-/g, '+').replace(/_/g, '/');
        switch (output.length % 4) {
            case 0: {
                break;
            }
            case 2: {
                output += '==';
                break;
            }
            case 3: {
                output += '=';
                break;
            }
            default: {
                throw new Error('Illegal base64url string!');
            }
        }
        return decodeURIComponent(escape(window.atob(output)));
    }
    /**
     * Gets a named value from the user access token.
     * @template T
     * @param {?} key Key name of the field to retrieve
     * @return {?} Value from the token
     */
    getValueFromLocalAccessToken(key) {
        return this.getValueFromToken(this.getAccessToken(), key);
    }
    /**
     * Gets access token
     * @return {?} access token
     */
    getAccessToken() {
        return localStorage.getItem(JwtHelperService.USER_ACCESS_TOKEN);
    }
    /**
     * Gets a named value from the user access token.
     * @template T
     * @param {?} accessToken
     * @param {?} key accessToken
     * @return {?} Value from the token
     */
    getValueFromToken(accessToken, key) {
        /** @type {?} */
        let value;
        if (accessToken) {
            /** @type {?} */
            const tokenPayload = this.decodeToken(accessToken);
            value = tokenPayload[key];
        }
        return (/** @type {?} */ (value));
    }
}
JwtHelperService.USER_NAME = 'name';
JwtHelperService.FAMILY_NAME = 'family_name';
JwtHelperService.GIVEN_NAME = 'given_name';
JwtHelperService.USER_EMAIL = 'email';
JwtHelperService.USER_ACCESS_TOKEN = 'access_token';
JwtHelperService.USER_PREFERRED_USERNAME = 'preferred_username';
JwtHelperService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
JwtHelperService.ctorParameters = () => [];
/** @nocollapse */ JwtHelperService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function JwtHelperService_Factory() { return new JwtHelperService(); }, token: JwtHelperService, providedIn: "root" });
if (false) {
    /** @type {?} */
    JwtHelperService.USER_NAME;
    /** @type {?} */
    JwtHelperService.FAMILY_NAME;
    /** @type {?} */
    JwtHelperService.GIVEN_NAME;
    /** @type {?} */
    JwtHelperService.USER_EMAIL;
    /** @type {?} */
    JwtHelperService.USER_ACCESS_TOKEN;
    /** @type {?} */
    JwtHelperService.USER_PREFERRED_USERNAME;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiand0LWhlbHBlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvand0LWhlbHBlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBSzNDLE1BQU0sT0FBTyxnQkFBZ0I7SUFTekI7SUFDQSxDQUFDOzs7Ozs7SUFPRCxXQUFXLENBQUMsS0FBSzs7Y0FDUCxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7UUFFOUIsSUFBSSxLQUFLLENBQUMsTUFBTSxLQUFLLENBQUMsRUFBRTtZQUNwQixNQUFNLElBQUksS0FBSyxDQUFDLHVCQUF1QixDQUFDLENBQUM7U0FDNUM7O2NBRUssT0FBTyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzlDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDVixNQUFNLElBQUksS0FBSyxDQUFDLHlCQUF5QixDQUFDLENBQUM7U0FDOUM7UUFFRCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDL0IsQ0FBQzs7Ozs7O0lBRU8sZUFBZSxDQUFDLEtBQUs7O1lBQ3JCLE1BQU0sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQztRQUN4RCxRQUFRLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3ZCLEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ0osTUFBTTthQUNUO1lBQ0QsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDSixNQUFNLElBQUksSUFBSSxDQUFDO2dCQUNmLE1BQU07YUFDVDtZQUNELEtBQUssQ0FBQyxDQUFDLENBQUM7Z0JBQ0osTUFBTSxJQUFJLEdBQUcsQ0FBQztnQkFDZCxNQUFNO2FBQ1Q7WUFDRCxPQUFPLENBQUMsQ0FBQztnQkFDTCxNQUFNLElBQUksS0FBSyxDQUFDLDJCQUEyQixDQUFDLENBQUM7YUFDaEQ7U0FDSjtRQUNELE9BQU8sa0JBQWtCLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzNELENBQUM7Ozs7Ozs7SUFPRCw0QkFBNEIsQ0FBSSxHQUFXO1FBQ3ZDLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUM5RCxDQUFDOzs7OztJQU1ELGNBQWM7UUFDVixPQUFPLFlBQVksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUNwRSxDQUFDOzs7Ozs7OztJQVFELGlCQUFpQixDQUFJLFdBQW1CLEVBQUUsR0FBVzs7WUFDN0MsS0FBSztRQUNULElBQUksV0FBVyxFQUFFOztrQkFDUCxZQUFZLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUM7WUFDbEQsS0FBSyxHQUFHLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUM3QjtRQUNELE9BQU8sbUJBQUksS0FBSyxFQUFBLENBQUM7SUFDckIsQ0FBQzs7QUFqRk0sMEJBQVMsR0FBRyxNQUFNLENBQUM7QUFDbkIsNEJBQVcsR0FBRyxhQUFhLENBQUM7QUFDNUIsMkJBQVUsR0FBRyxZQUFZLENBQUM7QUFDMUIsMkJBQVUsR0FBRyxPQUFPLENBQUM7QUFDckIsa0NBQWlCLEdBQUcsY0FBYyxDQUFDO0FBQ25DLHdDQUF1QixHQUFHLG9CQUFvQixDQUFDOztZQVZ6RCxVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7Ozs7SUFHRywyQkFBMEI7O0lBQzFCLDZCQUFtQzs7SUFDbkMsNEJBQWlDOztJQUNqQyw0QkFBNEI7O0lBQzVCLG1DQUEwQzs7SUFDMUMseUNBQXNEIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgSnd0SGVscGVyU2VydmljZSB7XHJcblxyXG4gICAgc3RhdGljIFVTRVJfTkFNRSA9ICduYW1lJztcclxuICAgIHN0YXRpYyBGQU1JTFlfTkFNRSA9ICdmYW1pbHlfbmFtZSc7XHJcbiAgICBzdGF0aWMgR0lWRU5fTkFNRSA9ICdnaXZlbl9uYW1lJztcclxuICAgIHN0YXRpYyBVU0VSX0VNQUlMID0gJ2VtYWlsJztcclxuICAgIHN0YXRpYyBVU0VSX0FDQ0VTU19UT0tFTiA9ICdhY2Nlc3NfdG9rZW4nO1xyXG4gICAgc3RhdGljIFVTRVJfUFJFRkVSUkVEX1VTRVJOQU1FID0gJ3ByZWZlcnJlZF91c2VybmFtZSc7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZWNvZGVzIGEgSlNPTiB3ZWIgdG9rZW4gaW50byBhIEpTIG9iamVjdC5cclxuICAgICAqIEBwYXJhbSB0b2tlbiBUb2tlbiBpbiBlbmNvZGVkIGZvcm1cclxuICAgICAqIEByZXR1cm5zIERlY29kZWQgdG9rZW4gZGF0YSBvYmplY3RcclxuICAgICAqL1xyXG4gICAgZGVjb2RlVG9rZW4odG9rZW4pOiBPYmplY3Qge1xyXG4gICAgICAgIGNvbnN0IHBhcnRzID0gdG9rZW4uc3BsaXQoJy4nKTtcclxuXHJcbiAgICAgICAgaWYgKHBhcnRzLmxlbmd0aCAhPT0gMykge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0pXVCBtdXN0IGhhdmUgMyBwYXJ0cycpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgZGVjb2RlZCA9IHRoaXMudXJsQmFzZTY0RGVjb2RlKHBhcnRzWzFdKTtcclxuICAgICAgICBpZiAoIWRlY29kZWQpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdDYW5ub3QgZGVjb2RlIHRoZSB0b2tlbicpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIEpTT04ucGFyc2UoZGVjb2RlZCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSB1cmxCYXNlNjREZWNvZGUodG9rZW4pOiBzdHJpbmcge1xyXG4gICAgICAgIGxldCBvdXRwdXQgPSB0b2tlbi5yZXBsYWNlKC8tL2csICcrJykucmVwbGFjZSgvXy9nLCAnLycpO1xyXG4gICAgICAgIHN3aXRjaCAob3V0cHV0Lmxlbmd0aCAlIDQpIHtcclxuICAgICAgICAgICAgY2FzZSAwOiB7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjYXNlIDI6IHtcclxuICAgICAgICAgICAgICAgIG91dHB1dCArPSAnPT0nO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2FzZSAzOiB7XHJcbiAgICAgICAgICAgICAgICBvdXRwdXQgKz0gJz0nO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZGVmYXVsdDoge1xyXG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdJbGxlZ2FsIGJhc2U2NHVybCBzdHJpbmchJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGRlY29kZVVSSUNvbXBvbmVudChlc2NhcGUod2luZG93LmF0b2Iob3V0cHV0KSkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhIG5hbWVkIHZhbHVlIGZyb20gdGhlIHVzZXIgYWNjZXNzIHRva2VuLlxyXG4gICAgICogQHBhcmFtIGtleSBLZXkgbmFtZSBvZiB0aGUgZmllbGQgdG8gcmV0cmlldmVcclxuICAgICAqIEByZXR1cm5zIFZhbHVlIGZyb20gdGhlIHRva2VuXHJcbiAgICAgKi9cclxuICAgIGdldFZhbHVlRnJvbUxvY2FsQWNjZXNzVG9rZW48VD4oa2V5OiBzdHJpbmcpOiBUIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRWYWx1ZUZyb21Ub2tlbih0aGlzLmdldEFjY2Vzc1Rva2VuKCksIGtleSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGFjY2VzcyB0b2tlblxyXG4gICAgICogQHJldHVybnMgYWNjZXNzIHRva2VuXHJcbiAgICAgKi9cclxuICAgIGdldEFjY2Vzc1Rva2VuKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIGxvY2FsU3RvcmFnZS5nZXRJdGVtKEp3dEhlbHBlclNlcnZpY2UuVVNFUl9BQ0NFU1NfVE9LRU4pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhIG5hbWVkIHZhbHVlIGZyb20gdGhlIHVzZXIgYWNjZXNzIHRva2VuLlxyXG4gICAgICogQHBhcmFtIGtleSBhY2Nlc3NUb2tlblxyXG4gICAgICogQHBhcmFtIGtleSBLZXkgbmFtZSBvZiB0aGUgZmllbGQgdG8gcmV0cmlldmVcclxuICAgICAqIEByZXR1cm5zIFZhbHVlIGZyb20gdGhlIHRva2VuXHJcbiAgICAgKi9cclxuICAgIGdldFZhbHVlRnJvbVRva2VuPFQ+KGFjY2Vzc1Rva2VuOiBzdHJpbmcsIGtleTogc3RyaW5nKTogVCB7XHJcbiAgICAgICAgbGV0IHZhbHVlO1xyXG4gICAgICAgIGlmIChhY2Nlc3NUb2tlbikge1xyXG4gICAgICAgICAgICBjb25zdCB0b2tlblBheWxvYWQgPSB0aGlzLmRlY29kZVRva2VuKGFjY2Vzc1Rva2VuKTtcclxuICAgICAgICAgICAgdmFsdWUgPSB0b2tlblBheWxvYWRba2V5XTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIDxUPiB2YWx1ZTtcclxuICAgIH1cclxufVxyXG4iXX0=