/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { AlfrescoApiService } from './alfresco-api.service';
import moment from 'moment-es6';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
export class LockService {
    /**
     * @param {?} alfrescoApiService
     */
    constructor(alfrescoApiService) {
        this.alfrescoApiService = alfrescoApiService;
    }
    /**
     * @param {?} node
     * @return {?}
     */
    isLocked(node) {
        /** @type {?} */
        let isLocked = false;
        if (this.hasLockConfigured(node)) {
            if (this.isReadOnlyLock(node)) {
                isLocked = true;
                if (this.isLockExpired(node)) {
                    isLocked = false;
                }
            }
            else if (this.isLockOwnerAllowed(node)) {
                isLocked = this.alfrescoApiService.getInstance().getEcmUsername() !== node.properties['cm:lockOwner'].id;
                if (this.isLockExpired(node)) {
                    isLocked = false;
                }
            }
        }
        return isLocked;
    }
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    hasLockConfigured(node) {
        return node.isFile && node.isLocked && node.properties['cm:lockType'];
    }
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    isReadOnlyLock(node) {
        return node.properties['cm:lockType'] === 'READ_ONLY_LOCK' && node.properties['cm:lockLifetime'] === 'PERSISTENT';
    }
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    isLockOwnerAllowed(node) {
        return node.properties['cm:lockType'] === 'WRITE_LOCK' && node.properties['cm:lockLifetime'] === 'PERSISTENT';
    }
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    getLockExpiryTime(node) {
        if (node.properties['cm:expiryDate']) {
            return moment(node.properties['cm:expiryDate'], 'yyyy-MM-ddThh:mm:ssZ');
        }
    }
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    isLockExpired(node) {
        /** @type {?} */
        const expiryLockTime = this.getLockExpiryTime(node);
        return moment().isAfter(expiryLockTime);
    }
}
LockService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
LockService.ctorParameters = () => [
    { type: AlfrescoApiService }
];
/** @nocollapse */ LockService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function LockService_Factory() { return new LockService(i0.ɵɵinject(i1.AlfrescoApiService)); }, token: LockService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    LockService.prototype.alfrescoApiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jay5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvbG9jay5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDNUQsT0FBTyxNQUFNLE1BQU0sWUFBWSxDQUFDOzs7QUFNaEMsTUFBTSxPQUFPLFdBQVc7Ozs7SUFFcEIsWUFBb0Isa0JBQXNDO1FBQXRDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7SUFDMUQsQ0FBQzs7Ozs7SUFFRCxRQUFRLENBQUMsSUFBVTs7WUFDWCxRQUFRLEdBQUcsS0FBSztRQUNwQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUM5QixJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzNCLFFBQVEsR0FBRyxJQUFJLENBQUM7Z0JBQ2hCLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBRTtvQkFDMUIsUUFBUSxHQUFHLEtBQUssQ0FBQztpQkFDcEI7YUFDSjtpQkFBTSxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDdEMsUUFBUSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxjQUFjLEVBQUUsS0FBSyxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztnQkFDekcsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUMxQixRQUFRLEdBQUcsS0FBSyxDQUFDO2lCQUNwQjthQUNKO1NBQ0o7UUFDRCxPQUFPLFFBQVEsQ0FBQztJQUNwQixDQUFDOzs7Ozs7SUFFTyxpQkFBaUIsQ0FBQyxJQUFVO1FBQ2hDLE9BQU8sSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLENBQUM7SUFDMUUsQ0FBQzs7Ozs7O0lBRU8sY0FBYyxDQUFDLElBQVU7UUFDN0IsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxLQUFLLGdCQUFnQixJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsS0FBSyxZQUFZLENBQUM7SUFDdEgsQ0FBQzs7Ozs7O0lBRU8sa0JBQWtCLENBQUMsSUFBVTtRQUNqQyxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLEtBQUssWUFBWSxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsaUJBQWlCLENBQUMsS0FBSyxZQUFZLENBQUM7SUFDbEgsQ0FBQzs7Ozs7O0lBRU8saUJBQWlCLENBQUMsSUFBVTtRQUNoQyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLEVBQUU7WUFDbEMsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO1NBQzNFO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sYUFBYSxDQUFDLElBQVU7O2NBQ3RCLGNBQWMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDO1FBQ25ELE9BQU8sTUFBTSxFQUFFLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxDQUFDO0lBQzVDLENBQUM7OztZQS9DSixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFOUSxrQkFBa0I7Ozs7Ozs7O0lBU1gseUNBQThDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTm9kZSB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IG1vbWVudCBmcm9tICdtb21lbnQtZXM2JztcclxuaW1wb3J0IHsgTW9tZW50IH0gZnJvbSAnbW9tZW50JztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTG9ja1NlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYWxmcmVzY29BcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBpc0xvY2tlZChub2RlOiBOb2RlKTogYm9vbGVhbiB7XHJcbiAgICAgICAgbGV0IGlzTG9ja2VkID0gZmFsc2U7XHJcbiAgICAgICAgaWYgKHRoaXMuaGFzTG9ja0NvbmZpZ3VyZWQobm9kZSkpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuaXNSZWFkT25seUxvY2sobm9kZSkpIHtcclxuICAgICAgICAgICAgICAgIGlzTG9ja2VkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmlzTG9ja0V4cGlyZWQobm9kZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICBpc0xvY2tlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMuaXNMb2NrT3duZXJBbGxvd2VkKG5vZGUpKSB7XHJcbiAgICAgICAgICAgICAgICBpc0xvY2tlZCA9IHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuZ2V0RWNtVXNlcm5hbWUoKSAhPT0gbm9kZS5wcm9wZXJ0aWVzWydjbTpsb2NrT3duZXInXS5pZDtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmlzTG9ja0V4cGlyZWQobm9kZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICBpc0xvY2tlZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBpc0xvY2tlZDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGhhc0xvY2tDb25maWd1cmVkKG5vZGU6IE5vZGUpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gbm9kZS5pc0ZpbGUgJiYgbm9kZS5pc0xvY2tlZCAmJiBub2RlLnByb3BlcnRpZXNbJ2NtOmxvY2tUeXBlJ107XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBpc1JlYWRPbmx5TG9jayhub2RlOiBOb2RlKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIG5vZGUucHJvcGVydGllc1snY206bG9ja1R5cGUnXSA9PT0gJ1JFQURfT05MWV9MT0NLJyAmJiBub2RlLnByb3BlcnRpZXNbJ2NtOmxvY2tMaWZldGltZSddID09PSAnUEVSU0lTVEVOVCc7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBpc0xvY2tPd25lckFsbG93ZWQobm9kZTogTm9kZSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBub2RlLnByb3BlcnRpZXNbJ2NtOmxvY2tUeXBlJ10gPT09ICdXUklURV9MT0NLJyAmJiBub2RlLnByb3BlcnRpZXNbJ2NtOmxvY2tMaWZldGltZSddID09PSAnUEVSU0lTVEVOVCc7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRMb2NrRXhwaXJ5VGltZShub2RlOiBOb2RlKTogTW9tZW50IHtcclxuICAgICAgICBpZiAobm9kZS5wcm9wZXJ0aWVzWydjbTpleHBpcnlEYXRlJ10pIHtcclxuICAgICAgICAgICAgcmV0dXJuIG1vbWVudChub2RlLnByb3BlcnRpZXNbJ2NtOmV4cGlyeURhdGUnXSwgJ3l5eXktTU0tZGRUaGg6bW06c3NaJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaXNMb2NrRXhwaXJlZChub2RlOiBOb2RlKTogYm9vbGVhbiB7XHJcbiAgICAgICAgY29uc3QgZXhwaXJ5TG9ja1RpbWUgPSB0aGlzLmdldExvY2tFeHBpcnlUaW1lKG5vZGUpO1xyXG4gICAgICAgIHJldHVybiBtb21lbnQoKS5pc0FmdGVyKGV4cGlyeUxvY2tUaW1lKTtcclxuICAgIH1cclxufVxyXG4iXX0=