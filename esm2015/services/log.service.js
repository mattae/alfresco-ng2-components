/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:no-console  */
import { Injectable } from '@angular/core';
import { AppConfigService, AppConfigValues } from '../app-config/app-config.service';
import { logLevels, LogLevelsEnum } from '../models/log-levels.model';
import { Subject } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "../app-config/app-config.service";
export class LogService {
    /**
     * @param {?} appConfig
     */
    constructor(appConfig) {
        this.appConfig = appConfig;
        this.onMessage = new Subject();
    }
    /**
     * @return {?}
     */
    get currentLogLevel() {
        /** @type {?} */
        const configLevel = this.appConfig.get(AppConfigValues.LOG_LEVEL);
        if (configLevel) {
            return this.getLogLevel(configLevel);
        }
        return LogLevelsEnum.TRACE;
    }
    /**
     * Logs a message at the "ERROR" level.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    error(message, ...optionalParams) {
        if (this.currentLogLevel >= LogLevelsEnum.ERROR) {
            this.messageBus(message, 'ERROR');
            console.error(message, ...optionalParams);
        }
    }
    /**
     * Logs a message at the "DEBUG" level.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    debug(message, ...optionalParams) {
        if (this.currentLogLevel >= LogLevelsEnum.DEBUG) {
            this.messageBus(message, 'DEBUG');
            console.debug(message, ...optionalParams);
        }
    }
    /**
     * Logs a message at the "INFO" level.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    info(message, ...optionalParams) {
        if (this.currentLogLevel >= LogLevelsEnum.INFO) {
            this.messageBus(message, 'INFO');
            console.info(message, ...optionalParams);
        }
    }
    /**
     * Logs a message at any level from "TRACE" upwards.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    log(message, ...optionalParams) {
        if (this.currentLogLevel >= LogLevelsEnum.TRACE) {
            this.messageBus(message, 'LOG');
            console.log(message, ...optionalParams);
        }
    }
    /**
     * Logs a message at the "TRACE" level.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    trace(message, ...optionalParams) {
        if (this.currentLogLevel >= LogLevelsEnum.TRACE) {
            this.messageBus(message, 'TRACE');
            console.trace(message, ...optionalParams);
        }
    }
    /**
     * Logs a message at the "WARN" level.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    warn(message, ...optionalParams) {
        if (this.currentLogLevel >= LogLevelsEnum.WARN) {
            this.messageBus(message, 'WARN');
            console.warn(message, ...optionalParams);
        }
    }
    /**
     * Logs a message if a boolean test fails.
     * @param {?=} test Test value (typically a boolean expression)
     * @param {?=} message Message to show if test is false
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    assert(test, message, ...optionalParams) {
        if (this.currentLogLevel !== LogLevelsEnum.SILENT) {
            this.messageBus(message, 'ASSERT');
            console.assert(test, message, ...optionalParams);
        }
    }
    /**
     * Starts an indented group of log messages.
     * @param {?=} groupTitle Title shown at the start of the group
     * @param {...?} optionalParams Interpolation values for the title in "printf" format
     * @return {?}
     */
    group(groupTitle, ...optionalParams) {
        if (this.currentLogLevel !== LogLevelsEnum.SILENT) {
            console.group(groupTitle, ...optionalParams);
        }
    }
    /**
     * Ends a indented group of log messages.
     * @return {?}
     */
    groupEnd() {
        if (this.currentLogLevel !== LogLevelsEnum.SILENT) {
            console.groupEnd();
        }
    }
    /**
     * Converts a log level name string into its numeric equivalent.
     * @param {?} level Level name
     * @return {?} Numeric log level
     */
    getLogLevel(level) {
        /** @type {?} */
        const referencedLevel = logLevels.find((/**
         * @param {?} currentLevel
         * @return {?}
         */
        (currentLevel) => {
            return currentLevel.name.toLocaleLowerCase() === level.toLocaleLowerCase();
        }));
        return referencedLevel ? referencedLevel.level : 5;
    }
    /**
     * Triggers notification callback for log messages.
     * @param {?} text Message text
     * @param {?} logLevel Log level for the message
     * @return {?}
     */
    messageBus(text, logLevel) {
        this.onMessage.next({ text: text, type: logLevel });
    }
}
LogService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
LogService.ctorParameters = () => [
    { type: AppConfigService }
];
/** @nocollapse */ LogService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function LogService_Factory() { return new LogService(i0.ɵɵinject(i1.AppConfigService)); }, token: LogService, providedIn: "root" });
if (false) {
    /** @type {?} */
    LogService.prototype.onMessage;
    /**
     * @type {?}
     * @private
     */
    LogService.prototype.appConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9sb2cuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsZUFBZSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDckYsT0FBTyxFQUFFLFNBQVMsRUFBRSxhQUFhLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUN0RSxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDOzs7QUFLL0IsTUFBTSxPQUFPLFVBQVU7Ozs7SUFjbkIsWUFBb0IsU0FBMkI7UUFBM0IsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFDM0MsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO0lBQ25DLENBQUM7Ozs7SUFkRCxJQUFJLGVBQWU7O2NBQ1QsV0FBVyxHQUFXLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFTLGVBQWUsQ0FBQyxTQUFTLENBQUM7UUFFakYsSUFBSSxXQUFXLEVBQUU7WUFDYixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7U0FDeEM7UUFFRCxPQUFPLGFBQWEsQ0FBQyxLQUFLLENBQUM7SUFDL0IsQ0FBQzs7Ozs7OztJQWFELEtBQUssQ0FBQyxPQUFhLEVBQUUsR0FBRyxjQUFxQjtRQUN6QyxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksYUFBYSxDQUFDLEtBQUssRUFBRTtZQUU3QyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQztZQUVsQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxHQUFHLGNBQWMsQ0FBQyxDQUFDO1NBQzdDO0lBQ0wsQ0FBQzs7Ozs7OztJQU9ELEtBQUssQ0FBQyxPQUFhLEVBQUUsR0FBRyxjQUFxQjtRQUN6QyxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksYUFBYSxDQUFDLEtBQUssRUFBRTtZQUU3QyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQztZQUVsQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxHQUFHLGNBQWMsQ0FBQyxDQUFDO1NBQzdDO0lBQ0wsQ0FBQzs7Ozs7OztJQU9ELElBQUksQ0FBQyxPQUFhLEVBQUUsR0FBRyxjQUFxQjtRQUN4QyxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksYUFBYSxDQUFDLElBQUksRUFBRTtZQUU1QyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztZQUVqQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxHQUFHLGNBQWMsQ0FBQyxDQUFDO1NBQzVDO0lBQ0wsQ0FBQzs7Ozs7OztJQU9ELEdBQUcsQ0FBQyxPQUFhLEVBQUUsR0FBRyxjQUFxQjtRQUN2QyxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksYUFBYSxDQUFDLEtBQUssRUFBRTtZQUU3QyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztZQUVoQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxHQUFHLGNBQWMsQ0FBQyxDQUFDO1NBQzNDO0lBQ0wsQ0FBQzs7Ozs7OztJQU9ELEtBQUssQ0FBQyxPQUFhLEVBQUUsR0FBRyxjQUFxQjtRQUN6QyxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksYUFBYSxDQUFDLEtBQUssRUFBRTtZQUU3QyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQztZQUVsQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRSxHQUFHLGNBQWMsQ0FBQyxDQUFDO1NBQzdDO0lBQ0wsQ0FBQzs7Ozs7OztJQU9ELElBQUksQ0FBQyxPQUFhLEVBQUUsR0FBRyxjQUFxQjtRQUN4QyxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksYUFBYSxDQUFDLElBQUksRUFBRTtZQUU1QyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztZQUVqQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxHQUFHLGNBQWMsQ0FBQyxDQUFDO1NBQzVDO0lBQ0wsQ0FBQzs7Ozs7Ozs7SUFRRCxNQUFNLENBQUMsSUFBYyxFQUFFLE9BQWdCLEVBQUUsR0FBRyxjQUFxQjtRQUM3RCxJQUFJLElBQUksQ0FBQyxlQUFlLEtBQUssYUFBYSxDQUFDLE1BQU0sRUFBRTtZQUUvQyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxRQUFRLENBQUMsQ0FBQztZQUVuQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxPQUFPLEVBQUUsR0FBRyxjQUFjLENBQUMsQ0FBQztTQUNwRDtJQUNMLENBQUM7Ozs7Ozs7SUFPRCxLQUFLLENBQUMsVUFBbUIsRUFBRSxHQUFHLGNBQXFCO1FBQy9DLElBQUksSUFBSSxDQUFDLGVBQWUsS0FBSyxhQUFhLENBQUMsTUFBTSxFQUFFO1lBQy9DLE9BQU8sQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLEdBQUcsY0FBYyxDQUFDLENBQUM7U0FDaEQ7SUFDTCxDQUFDOzs7OztJQUtELFFBQVE7UUFDSixJQUFJLElBQUksQ0FBQyxlQUFlLEtBQUssYUFBYSxDQUFDLE1BQU0sRUFBRTtZQUMvQyxPQUFPLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDdEI7SUFDTCxDQUFDOzs7Ozs7SUFPRCxXQUFXLENBQUMsS0FBYTs7Y0FDZixlQUFlLEdBQUcsU0FBUyxDQUFDLElBQUk7Ozs7UUFBQyxDQUFDLFlBQWlCLEVBQUUsRUFBRTtZQUN6RCxPQUFPLFlBQVksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsS0FBSyxLQUFLLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUMvRSxDQUFDLEVBQUM7UUFFRixPQUFPLGVBQWUsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7Ozs7Ozs7SUFPRCxVQUFVLENBQUMsSUFBWSxFQUFFLFFBQWdCO1FBQ3JDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQztJQUN4RCxDQUFDOzs7WUFoS0osVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBTlEsZ0JBQWdCOzs7OztJQW1CckIsK0JBQXdCOzs7OztJQUVaLCtCQUFtQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4vKiB0c2xpbnQ6ZGlzYWJsZTpuby1jb25zb2xlICAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBcHBDb25maWdTZXJ2aWNlLCBBcHBDb25maWdWYWx1ZXMgfSBmcm9tICcuLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IGxvZ0xldmVscywgTG9nTGV2ZWxzRW51bSB9IGZyb20gJy4uL21vZGVscy9sb2ctbGV2ZWxzLm1vZGVsJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMb2dTZXJ2aWNlIHtcclxuXHJcbiAgICBnZXQgY3VycmVudExvZ0xldmVsKCkge1xyXG4gICAgICAgIGNvbnN0IGNvbmZpZ0xldmVsOiBzdHJpbmcgPSB0aGlzLmFwcENvbmZpZy5nZXQ8c3RyaW5nPihBcHBDb25maWdWYWx1ZXMuTE9HX0xFVkVMKTtcclxuXHJcbiAgICAgICAgaWYgKGNvbmZpZ0xldmVsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmdldExvZ0xldmVsKGNvbmZpZ0xldmVsKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBMb2dMZXZlbHNFbnVtLlRSQUNFO1xyXG4gICAgfVxyXG5cclxuICAgIG9uTWVzc2FnZTogU3ViamVjdDxhbnk+O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBwQ29uZmlnOiBBcHBDb25maWdTZXJ2aWNlKSB7XHJcbiAgICAgICAgdGhpcy5vbk1lc3NhZ2UgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTG9ncyBhIG1lc3NhZ2UgYXQgdGhlIFwiRVJST1JcIiBsZXZlbC5cclxuICAgICAqIEBwYXJhbSBtZXNzYWdlIE1lc3NhZ2UgdG8gbG9nXHJcbiAgICAgKiBAcGFyYW0gb3B0aW9uYWxQYXJhbXMgSW50ZXJwb2xhdGlvbiB2YWx1ZXMgZm9yIHRoZSBtZXNzYWdlIGluIFwicHJpbnRmXCIgZm9ybWF0XHJcbiAgICAgKi9cclxuICAgIGVycm9yKG1lc3NhZ2U/OiBhbnksIC4uLm9wdGlvbmFsUGFyYW1zOiBhbnlbXSkge1xyXG4gICAgICAgIGlmICh0aGlzLmN1cnJlbnRMb2dMZXZlbCA+PSBMb2dMZXZlbHNFbnVtLkVSUk9SKSB7XHJcblxyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2VCdXMobWVzc2FnZSwgJ0VSUk9SJyk7XHJcblxyXG4gICAgICAgICAgICBjb25zb2xlLmVycm9yKG1lc3NhZ2UsIC4uLm9wdGlvbmFsUGFyYW1zKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBMb2dzIGEgbWVzc2FnZSBhdCB0aGUgXCJERUJVR1wiIGxldmVsLlxyXG4gICAgICogQHBhcmFtIG1lc3NhZ2UgTWVzc2FnZSB0byBsb2dcclxuICAgICAqIEBwYXJhbSBvcHRpb25hbFBhcmFtcyBJbnRlcnBvbGF0aW9uIHZhbHVlcyBmb3IgdGhlIG1lc3NhZ2UgaW4gXCJwcmludGZcIiBmb3JtYXRcclxuICAgICAqL1xyXG4gICAgZGVidWcobWVzc2FnZT86IGFueSwgLi4ub3B0aW9uYWxQYXJhbXM6IGFueVtdKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY3VycmVudExvZ0xldmVsID49IExvZ0xldmVsc0VudW0uREVCVUcpIHtcclxuXHJcbiAgICAgICAgICAgIHRoaXMubWVzc2FnZUJ1cyhtZXNzYWdlLCAnREVCVUcnKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnNvbGUuZGVidWcobWVzc2FnZSwgLi4ub3B0aW9uYWxQYXJhbXMpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExvZ3MgYSBtZXNzYWdlIGF0IHRoZSBcIklORk9cIiBsZXZlbC5cclxuICAgICAqIEBwYXJhbSBtZXNzYWdlIE1lc3NhZ2UgdG8gbG9nXHJcbiAgICAgKiBAcGFyYW0gb3B0aW9uYWxQYXJhbXMgSW50ZXJwb2xhdGlvbiB2YWx1ZXMgZm9yIHRoZSBtZXNzYWdlIGluIFwicHJpbnRmXCIgZm9ybWF0XHJcbiAgICAgKi9cclxuICAgIGluZm8obWVzc2FnZT86IGFueSwgLi4ub3B0aW9uYWxQYXJhbXM6IGFueVtdKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY3VycmVudExvZ0xldmVsID49IExvZ0xldmVsc0VudW0uSU5GTykge1xyXG5cclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlQnVzKG1lc3NhZ2UsICdJTkZPJyk7XHJcblxyXG4gICAgICAgICAgICBjb25zb2xlLmluZm8obWVzc2FnZSwgLi4ub3B0aW9uYWxQYXJhbXMpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExvZ3MgYSBtZXNzYWdlIGF0IGFueSBsZXZlbCBmcm9tIFwiVFJBQ0VcIiB1cHdhcmRzLlxyXG4gICAgICogQHBhcmFtIG1lc3NhZ2UgTWVzc2FnZSB0byBsb2dcclxuICAgICAqIEBwYXJhbSBvcHRpb25hbFBhcmFtcyBJbnRlcnBvbGF0aW9uIHZhbHVlcyBmb3IgdGhlIG1lc3NhZ2UgaW4gXCJwcmludGZcIiBmb3JtYXRcclxuICAgICAqL1xyXG4gICAgbG9nKG1lc3NhZ2U/OiBhbnksIC4uLm9wdGlvbmFsUGFyYW1zOiBhbnlbXSkge1xyXG4gICAgICAgIGlmICh0aGlzLmN1cnJlbnRMb2dMZXZlbCA+PSBMb2dMZXZlbHNFbnVtLlRSQUNFKSB7XHJcblxyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2VCdXMobWVzc2FnZSwgJ0xPRycpO1xyXG5cclxuICAgICAgICAgICAgY29uc29sZS5sb2cobWVzc2FnZSwgLi4ub3B0aW9uYWxQYXJhbXMpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExvZ3MgYSBtZXNzYWdlIGF0IHRoZSBcIlRSQUNFXCIgbGV2ZWwuXHJcbiAgICAgKiBAcGFyYW0gbWVzc2FnZSBNZXNzYWdlIHRvIGxvZ1xyXG4gICAgICogQHBhcmFtIG9wdGlvbmFsUGFyYW1zIEludGVycG9sYXRpb24gdmFsdWVzIGZvciB0aGUgbWVzc2FnZSBpbiBcInByaW50ZlwiIGZvcm1hdFxyXG4gICAgICovXHJcbiAgICB0cmFjZShtZXNzYWdlPzogYW55LCAuLi5vcHRpb25hbFBhcmFtczogYW55W10pIHtcclxuICAgICAgICBpZiAodGhpcy5jdXJyZW50TG9nTGV2ZWwgPj0gTG9nTGV2ZWxzRW51bS5UUkFDRSkge1xyXG5cclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlQnVzKG1lc3NhZ2UsICdUUkFDRScpO1xyXG5cclxuICAgICAgICAgICAgY29uc29sZS50cmFjZShtZXNzYWdlLCAuLi5vcHRpb25hbFBhcmFtcyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTG9ncyBhIG1lc3NhZ2UgYXQgdGhlIFwiV0FSTlwiIGxldmVsLlxyXG4gICAgICogQHBhcmFtIG1lc3NhZ2UgTWVzc2FnZSB0byBsb2dcclxuICAgICAqIEBwYXJhbSBvcHRpb25hbFBhcmFtcyBJbnRlcnBvbGF0aW9uIHZhbHVlcyBmb3IgdGhlIG1lc3NhZ2UgaW4gXCJwcmludGZcIiBmb3JtYXRcclxuICAgICAqL1xyXG4gICAgd2FybihtZXNzYWdlPzogYW55LCAuLi5vcHRpb25hbFBhcmFtczogYW55W10pIHtcclxuICAgICAgICBpZiAodGhpcy5jdXJyZW50TG9nTGV2ZWwgPj0gTG9nTGV2ZWxzRW51bS5XQVJOKSB7XHJcblxyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2VCdXMobWVzc2FnZSwgJ1dBUk4nKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnNvbGUud2FybihtZXNzYWdlLCAuLi5vcHRpb25hbFBhcmFtcyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTG9ncyBhIG1lc3NhZ2UgaWYgYSBib29sZWFuIHRlc3QgZmFpbHMuXHJcbiAgICAgKiBAcGFyYW0gdGVzdCBUZXN0IHZhbHVlICh0eXBpY2FsbHkgYSBib29sZWFuIGV4cHJlc3Npb24pXHJcbiAgICAgKiBAcGFyYW0gbWVzc2FnZSBNZXNzYWdlIHRvIHNob3cgaWYgdGVzdCBpcyBmYWxzZVxyXG4gICAgICogQHBhcmFtIG9wdGlvbmFsUGFyYW1zIEludGVycG9sYXRpb24gdmFsdWVzIGZvciB0aGUgbWVzc2FnZSBpbiBcInByaW50ZlwiIGZvcm1hdFxyXG4gICAgICovXHJcbiAgICBhc3NlcnQodGVzdD86IGJvb2xlYW4sIG1lc3NhZ2U/OiBzdHJpbmcsIC4uLm9wdGlvbmFsUGFyYW1zOiBhbnlbXSkge1xyXG4gICAgICAgIGlmICh0aGlzLmN1cnJlbnRMb2dMZXZlbCAhPT0gTG9nTGV2ZWxzRW51bS5TSUxFTlQpIHtcclxuXHJcbiAgICAgICAgICAgIHRoaXMubWVzc2FnZUJ1cyhtZXNzYWdlLCAnQVNTRVJUJyk7XHJcblxyXG4gICAgICAgICAgICBjb25zb2xlLmFzc2VydCh0ZXN0LCBtZXNzYWdlLCAuLi5vcHRpb25hbFBhcmFtcyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU3RhcnRzIGFuIGluZGVudGVkIGdyb3VwIG9mIGxvZyBtZXNzYWdlcy5cclxuICAgICAqIEBwYXJhbSBncm91cFRpdGxlIFRpdGxlIHNob3duIGF0IHRoZSBzdGFydCBvZiB0aGUgZ3JvdXBcclxuICAgICAqIEBwYXJhbSBvcHRpb25hbFBhcmFtcyBJbnRlcnBvbGF0aW9uIHZhbHVlcyBmb3IgdGhlIHRpdGxlIGluIFwicHJpbnRmXCIgZm9ybWF0XHJcbiAgICAgKi9cclxuICAgIGdyb3VwKGdyb3VwVGl0bGU/OiBzdHJpbmcsIC4uLm9wdGlvbmFsUGFyYW1zOiBhbnlbXSkge1xyXG4gICAgICAgIGlmICh0aGlzLmN1cnJlbnRMb2dMZXZlbCAhPT0gTG9nTGV2ZWxzRW51bS5TSUxFTlQpIHtcclxuICAgICAgICAgICAgY29uc29sZS5ncm91cChncm91cFRpdGxlLCAuLi5vcHRpb25hbFBhcmFtcyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRW5kcyBhIGluZGVudGVkIGdyb3VwIG9mIGxvZyBtZXNzYWdlcy5cclxuICAgICAqL1xyXG4gICAgZ3JvdXBFbmQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY3VycmVudExvZ0xldmVsICE9PSBMb2dMZXZlbHNFbnVtLlNJTEVOVCkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmdyb3VwRW5kKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29udmVydHMgYSBsb2cgbGV2ZWwgbmFtZSBzdHJpbmcgaW50byBpdHMgbnVtZXJpYyBlcXVpdmFsZW50LlxyXG4gICAgICogQHBhcmFtIGxldmVsIExldmVsIG5hbWVcclxuICAgICAqIEByZXR1cm5zIE51bWVyaWMgbG9nIGxldmVsXHJcbiAgICAgKi9cclxuICAgIGdldExvZ0xldmVsKGxldmVsOiBzdHJpbmcpOiBMb2dMZXZlbHNFbnVtIHtcclxuICAgICAgICBjb25zdCByZWZlcmVuY2VkTGV2ZWwgPSBsb2dMZXZlbHMuZmluZCgoY3VycmVudExldmVsOiBhbnkpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGN1cnJlbnRMZXZlbC5uYW1lLnRvTG9jYWxlTG93ZXJDYXNlKCkgPT09IGxldmVsLnRvTG9jYWxlTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiByZWZlcmVuY2VkTGV2ZWwgPyByZWZlcmVuY2VkTGV2ZWwubGV2ZWwgOiA1O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVHJpZ2dlcnMgbm90aWZpY2F0aW9uIGNhbGxiYWNrIGZvciBsb2cgbWVzc2FnZXMuXHJcbiAgICAgKiBAcGFyYW0gdGV4dCBNZXNzYWdlIHRleHRcclxuICAgICAqIEBwYXJhbSBsb2dMZXZlbCBMb2cgbGV2ZWwgZm9yIHRoZSBtZXNzYWdlXHJcbiAgICAgKi9cclxuICAgIG1lc3NhZ2VCdXModGV4dDogc3RyaW5nLCBsb2dMZXZlbDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5vbk1lc3NhZ2UubmV4dCh7IHRleHQ6IHRleHQsIHR5cGU6IGxvZ0xldmVsIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==