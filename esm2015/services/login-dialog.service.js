/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { MatDialog } from '@angular/material';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { LoginDialogComponent } from '../login/components/login-dialog.component';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/dialog";
export class LoginDialogService {
    /**
     * @param {?} dialog
     */
    constructor(dialog) {
        this.dialog = dialog;
    }
    /**
     * Opens a dialog to choose a file to upload.
     * @param {?} actionName Name of the action to show in the title
     * @param {?} title Title for the dialog
     * @return {?} Information about the chosen file(s)
     */
    openLogin(actionName, title) {
        /** @type {?} */
        const logged = new Subject();
        logged.subscribe({
            complete: this.close.bind(this)
        });
        /** @type {?} */
        const data = {
            title,
            actionName,
            logged
        };
        this.openLoginDialog(data, 'adf-login-dialog', '630px');
        return logged;
    }
    /**
     * @private
     * @param {?} data
     * @param {?} currentPanelClass
     * @param {?} chosenWidth
     * @return {?}
     */
    openLoginDialog(data, currentPanelClass, chosenWidth) {
        this.dialog.open(LoginDialogComponent, { data, panelClass: currentPanelClass, width: chosenWidth });
    }
    /**
     * Closes the currently open dialog.
     * @return {?}
     */
    close() {
        this.dialog.closeAll();
    }
}
LoginDialogService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
LoginDialogService.ctorParameters = () => [
    { type: MatDialog }
];
/** @nocollapse */ LoginDialogService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function LoginDialogService_Factory() { return new LoginDialogService(i0.ɵɵinject(i1.MatDialog)); }, token: LoginDialogService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    LoginDialogService.prototype.dialog;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tZGlhbG9nLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9sb2dpbi1kaWFsb2cuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDOUMsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsT0FBTyxFQUFjLE1BQU0sTUFBTSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDOzs7QUFNbEYsTUFBTSxPQUFPLGtCQUFrQjs7OztJQUUzQixZQUFvQixNQUFpQjtRQUFqQixXQUFNLEdBQU4sTUFBTSxDQUFXO0lBQ3JDLENBQUM7Ozs7Ozs7SUFRRCxTQUFTLENBQUMsVUFBa0IsRUFBRSxLQUFhOztjQUNqQyxNQUFNLEdBQUcsSUFBSSxPQUFPLEVBQVU7UUFDcEMsTUFBTSxDQUFDLFNBQVMsQ0FBQztZQUNiLFFBQVEsRUFBRSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDbEMsQ0FBQyxDQUFDOztjQUVHLElBQUksR0FBNkI7WUFDbkMsS0FBSztZQUNMLFVBQVU7WUFDVixNQUFNO1NBQ1Q7UUFFRCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksRUFBRSxrQkFBa0IsRUFBRSxPQUFPLENBQUMsQ0FBQztRQUN4RCxPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDOzs7Ozs7OztJQUVPLGVBQWUsQ0FBQyxJQUE4QixFQUFFLGlCQUF5QixFQUFFLFdBQW1CO1FBQ2xHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLG9CQUFvQixFQUFFLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxpQkFBaUIsRUFBRSxLQUFLLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQztJQUN4RyxDQUFDOzs7OztJQUdELEtBQUs7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQzNCLENBQUM7OztZQXJDSixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFSUSxTQUFTOzs7Ozs7OztJQVdGLG9DQUF5QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgU3ViamVjdCwgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBMb2dpbkRpYWxvZ0NvbXBvbmVudCB9IGZyb20gJy4uL2xvZ2luL2NvbXBvbmVudHMvbG9naW4tZGlhbG9nLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IExvZ2luRGlhbG9nQ29tcG9uZW50RGF0YSB9IGZyb20gJy4uL2xvZ2luL2NvbXBvbmVudHMvbG9naW4tZGlhbG9nLWNvbXBvbmVudC1kYXRhLmludGVyZmFjZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIExvZ2luRGlhbG9nU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBkaWFsb2c6IE1hdERpYWxvZykge1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogT3BlbnMgYSBkaWFsb2cgdG8gY2hvb3NlIGEgZmlsZSB0byB1cGxvYWQuXHJcbiAgICAgKiBAcGFyYW0gYWN0aW9uTmFtZSBOYW1lIG9mIHRoZSBhY3Rpb24gdG8gc2hvdyBpbiB0aGUgdGl0bGVcclxuICAgICAqIEBwYXJhbSB0aXRsZSBUaXRsZSBmb3IgdGhlIGRpYWxvZ1xyXG4gICAgICogQHJldHVybnMgSW5mb3JtYXRpb24gYWJvdXQgdGhlIGNob3NlbiBmaWxlKHMpXHJcbiAgICAgKi9cclxuICAgIG9wZW5Mb2dpbihhY3Rpb25OYW1lOiBzdHJpbmcsIHRpdGxlOiBzdHJpbmcpOiBPYnNlcnZhYmxlPHN0cmluZz4ge1xyXG4gICAgICAgIGNvbnN0IGxvZ2dlZCA9IG5ldyBTdWJqZWN0PHN0cmluZz4oKTtcclxuICAgICAgICBsb2dnZWQuc3Vic2NyaWJlKHtcclxuICAgICAgICAgICAgY29tcGxldGU6IHRoaXMuY2xvc2UuYmluZCh0aGlzKVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb25zdCBkYXRhOiBMb2dpbkRpYWxvZ0NvbXBvbmVudERhdGEgPSB7XHJcbiAgICAgICAgICAgIHRpdGxlLFxyXG4gICAgICAgICAgICBhY3Rpb25OYW1lLFxyXG4gICAgICAgICAgICBsb2dnZWRcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICB0aGlzLm9wZW5Mb2dpbkRpYWxvZyhkYXRhLCAnYWRmLWxvZ2luLWRpYWxvZycsICc2MzBweCcpO1xyXG4gICAgICAgIHJldHVybiBsb2dnZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvcGVuTG9naW5EaWFsb2coZGF0YTogTG9naW5EaWFsb2dDb21wb25lbnREYXRhLCBjdXJyZW50UGFuZWxDbGFzczogc3RyaW5nLCBjaG9zZW5XaWR0aDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5kaWFsb2cub3BlbihMb2dpbkRpYWxvZ0NvbXBvbmVudCwgeyBkYXRhLCBwYW5lbENsYXNzOiBjdXJyZW50UGFuZWxDbGFzcywgd2lkdGg6IGNob3NlbldpZHRoIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKiBDbG9zZXMgdGhlIGN1cnJlbnRseSBvcGVuIGRpYWxvZy4gKi9cclxuICAgIGNsb3NlKCkge1xyXG4gICAgICAgIHRoaXMuZGlhbG9nLmNsb3NlQWxsKCk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==