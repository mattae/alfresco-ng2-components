/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { UserPreferencesService } from './user-preferences.service';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./user-preferences.service";
export class NodesApiService {
    /**
     * @param {?} api
     * @param {?} preferences
     */
    constructor(api, preferences) {
        this.api = api;
        this.preferences = preferences;
    }
    /**
     * @private
     * @return {?}
     */
    get nodesApi() {
        return this.api.getInstance().core.nodesApi;
    }
    /**
     * @private
     * @param {?} entity
     * @return {?}
     */
    getEntryFromEntity(entity) {
        return entity.entry;
    }
    /**
     * Gets the stored information about a node.
     * @param {?} nodeId ID of the target node
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} Node information
     */
    getNode(nodeId, options = {}) {
        /** @type {?} */
        const defaults = {
            include: ['path', 'properties', 'allowableOperations', 'permissions']
        };
        /** @type {?} */
        const queryOptions = Object.assign(defaults, options);
        /** @type {?} */
        const promise = this.nodesApi
            .getNode(nodeId, queryOptions)
            .then(this.getEntryFromEntity);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => throwError(err))));
    }
    /**
     * Gets the items contained in a folder node.
     * @param {?} nodeId ID of the target node
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} List of child items from the folder
     */
    getNodeChildren(nodeId, options = {}) {
        /** @type {?} */
        const defaults = {
            maxItems: this.preferences.paginationSize,
            skipCount: 0,
            include: ['path', 'properties', 'allowableOperations', 'permissions']
        };
        /** @type {?} */
        const queryOptions = Object.assign(defaults, options);
        /** @type {?} */
        const promise = this.nodesApi
            .getNodeChildren(nodeId, queryOptions);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => throwError(err))));
    }
    /**
     * Creates a new document node inside a folder.
     * @param {?} parentNodeId ID of the parent folder node
     * @param {?} nodeBody Data for the new node
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} Details of the new node
     */
    createNode(parentNodeId, nodeBody, options = {}) {
        /** @type {?} */
        const promise = this.nodesApi
            .addNode(parentNodeId, nodeBody, options)
            .then(this.getEntryFromEntity);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => throwError(err))));
    }
    /**
     * Creates a new folder node inside a parent folder.
     * @param {?} parentNodeId ID of the parent folder node
     * @param {?} nodeBody Data for the new folder
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} Details of the new folder
     */
    createFolder(parentNodeId, nodeBody, options = {}) {
        /** @type {?} */
        const body = Object.assign({ nodeType: 'cm:folder' }, nodeBody);
        return this.createNode(parentNodeId, body, options);
    }
    /**
     * Updates the information about a node.
     * @param {?} nodeId ID of the target node
     * @param {?} nodeBody New data for the node
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} Updated node information
     */
    updateNode(nodeId, nodeBody, options = {}) {
        /** @type {?} */
        const defaults = {
            include: ['path', 'properties', 'allowableOperations', 'permissions']
        };
        /** @type {?} */
        const queryOptions = Object.assign(defaults, options);
        /** @type {?} */
        const promise = this.nodesApi
            .updateNode(nodeId, nodeBody, queryOptions)
            .then(this.getEntryFromEntity);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => throwError(err))));
    }
    /**
     * Moves a node to the trashcan.
     * @param {?} nodeId ID of the target node
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} Empty result that notifies when the deletion is complete
     */
    deleteNode(nodeId, options = {}) {
        /** @type {?} */
        const promise = this.nodesApi.deleteNode(nodeId, options);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => throwError(err))));
    }
    /**
     * Restores a node previously moved to the trashcan.
     * @param {?} nodeId ID of the node to restore
     * @return {?} Details of the restored node
     */
    restoreNode(nodeId) {
        /** @type {?} */
        const promise = this.nodesApi
            .restoreNode(nodeId)
            .then(this.getEntryFromEntity);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => throwError(err))));
    }
}
NodesApiService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
NodesApiService.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: UserPreferencesService }
];
/** @nocollapse */ NodesApiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function NodesApiService_Factory() { return new NodesApiService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.UserPreferencesService)); }, token: NodesApiService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    NodesApiService.prototype.api;
    /**
     * @type {?}
     * @private
     */
    NodesApiService.prototype.preferences;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9kZXMtYXBpLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9ub2Rlcy1hcGkuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTNDLE9BQU8sRUFBYyxJQUFJLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3BELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7OztBQUs1QyxNQUFNLE9BQU8sZUFBZTs7Ozs7SUFFeEIsWUFDWSxHQUF1QixFQUN2QixXQUFtQztRQURuQyxRQUFHLEdBQUgsR0FBRyxDQUFvQjtRQUN2QixnQkFBVyxHQUFYLFdBQVcsQ0FBd0I7SUFBRyxDQUFDOzs7OztJQUVuRCxJQUFZLFFBQVE7UUFDaEIsT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDaEQsQ0FBQzs7Ozs7O0lBRU8sa0JBQWtCLENBQUMsTUFBaUI7UUFDeEMsT0FBTyxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ3hCLENBQUM7Ozs7Ozs7SUFRRCxPQUFPLENBQUMsTUFBYyxFQUFFLFVBQWUsRUFBRTs7Y0FDL0IsUUFBUSxHQUFHO1lBQ2IsT0FBTyxFQUFFLENBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxxQkFBcUIsRUFBRSxhQUFhLENBQUU7U0FDMUU7O2NBQ0ssWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQzs7Y0FDL0MsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRO2FBQ3hCLE9BQU8sQ0FBQyxNQUFNLEVBQUUsWUFBWSxDQUFDO2FBQzdCLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUM7UUFFbEMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUNyQixVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUN2QyxDQUFDO0lBQ04sQ0FBQzs7Ozs7OztJQVFELGVBQWUsQ0FBQyxNQUFjLEVBQUUsVUFBZSxFQUFFOztjQUN2QyxRQUFRLEdBQUc7WUFDYixRQUFRLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjO1lBQ3pDLFNBQVMsRUFBRSxDQUFDO1lBQ1osT0FBTyxFQUFFLENBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxxQkFBcUIsRUFBRSxhQUFhLENBQUU7U0FDMUU7O2NBQ0ssWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLE9BQU8sQ0FBQzs7Y0FDL0MsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRO2FBQ3hCLGVBQWUsQ0FBQyxNQUFNLEVBQUUsWUFBWSxDQUFDO1FBRTFDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FDckIsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDdkMsQ0FBQztJQUNOLENBQUM7Ozs7Ozs7O0lBU0QsVUFBVSxDQUFDLFlBQW9CLEVBQUUsUUFBYSxFQUFFLFVBQWUsRUFBRTs7Y0FDdkQsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRO2FBQ3hCLE9BQU8sQ0FBQyxZQUFZLEVBQUUsUUFBUSxFQUFFLE9BQU8sQ0FBQzthQUN4QyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDO1FBRWxDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FDckIsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDdkMsQ0FBQztJQUNOLENBQUM7Ozs7Ozs7O0lBU0QsWUFBWSxDQUFDLFlBQW9CLEVBQUUsUUFBYSxFQUFFLFVBQWUsRUFBRTs7Y0FDekQsSUFBSSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxRQUFRLEVBQUUsV0FBVyxFQUFFLEVBQUUsUUFBUSxDQUFDO1FBQy9ELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEVBQUUsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO0lBQ3hELENBQUM7Ozs7Ozs7O0lBU0QsVUFBVSxDQUFDLE1BQWMsRUFBRSxRQUFhLEVBQUUsVUFBZSxFQUFFOztjQUNqRCxRQUFRLEdBQUc7WUFDYixPQUFPLEVBQUUsQ0FBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLHFCQUFxQixFQUFFLGFBQWEsQ0FBRTtTQUMxRTs7Y0FDSyxZQUFZLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDOztjQUUvQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVE7YUFDeEIsVUFBVSxDQUFDLE1BQU0sRUFBRSxRQUFRLEVBQUUsWUFBWSxDQUFDO2FBQzFDLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUM7UUFFbEMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUNyQixVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUN2QyxDQUFDO0lBQ04sQ0FBQzs7Ozs7OztJQVFELFVBQVUsQ0FBQyxNQUFjLEVBQUUsVUFBZSxFQUFFOztjQUNsQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQztRQUV6RCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQ3JCLFVBQVU7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQ3ZDLENBQUM7SUFDTixDQUFDOzs7Ozs7SUFPRCxXQUFXLENBQUMsTUFBYzs7Y0FDaEIsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRO2FBQ3hCLFdBQVcsQ0FBQyxNQUFNLENBQUM7YUFDbkIsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztRQUVsQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQ3JCLFVBQVU7Ozs7UUFBQyxDQUFDLEdBQUcsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQ3ZDLENBQUM7SUFDTixDQUFDOzs7WUF4SUosVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBTlEsa0JBQWtCO1lBQ2xCLHNCQUFzQjs7Ozs7Ozs7SUFTdkIsOEJBQStCOzs7OztJQUMvQixzQ0FBMkMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOb2RlRW50cnksIE1pbmltYWxOb2RlLCBOb2RlUGFnaW5nIH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZyb20sIHRocm93RXJyb3IgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UgfSBmcm9tICcuL3VzZXItcHJlZmVyZW5jZXMuc2VydmljZSc7XHJcbmltcG9ydCB7IGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIE5vZGVzQXBpU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBhcGk6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIHByZWZlcmVuY2VzOiBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlKSB7fVxyXG5cclxuICAgIHByaXZhdGUgZ2V0IG5vZGVzQXBpKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwaS5nZXRJbnN0YW5jZSgpLmNvcmUubm9kZXNBcGk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRFbnRyeUZyb21FbnRpdHkoZW50aXR5OiBOb2RlRW50cnkpIHtcclxuICAgICAgICByZXR1cm4gZW50aXR5LmVudHJ5O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgc3RvcmVkIGluZm9ybWF0aW9uIGFib3V0IGEgbm9kZS5cclxuICAgICAqIEBwYXJhbSBub2RlSWQgSUQgb2YgdGhlIHRhcmdldCBub2RlXHJcbiAgICAgKiBAcGFyYW0gb3B0aW9ucyBPcHRpb25hbCBwYXJhbWV0ZXJzIHN1cHBvcnRlZCBieSBKUy1BUElcclxuICAgICAqIEByZXR1cm5zIE5vZGUgaW5mb3JtYXRpb25cclxuICAgICAqL1xyXG4gICAgZ2V0Tm9kZShub2RlSWQ6IHN0cmluZywgb3B0aW9uczogYW55ID0ge30pOiBPYnNlcnZhYmxlPE1pbmltYWxOb2RlPiB7XHJcbiAgICAgICAgY29uc3QgZGVmYXVsdHMgPSB7XHJcbiAgICAgICAgICAgIGluY2x1ZGU6IFsgJ3BhdGgnLCAncHJvcGVydGllcycsICdhbGxvd2FibGVPcGVyYXRpb25zJywgJ3Blcm1pc3Npb25zJyBdXHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zdCBxdWVyeU9wdGlvbnMgPSBPYmplY3QuYXNzaWduKGRlZmF1bHRzLCBvcHRpb25zKTtcclxuICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5ub2Rlc0FwaVxyXG4gICAgICAgICAgICAuZ2V0Tm9kZShub2RlSWQsIHF1ZXJ5T3B0aW9ucylcclxuICAgICAgICAgICAgLnRoZW4odGhpcy5nZXRFbnRyeUZyb21FbnRpdHkpO1xyXG5cclxuICAgICAgICByZXR1cm4gZnJvbShwcm9taXNlKS5waXBlKFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRocm93RXJyb3IoZXJyKSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgaXRlbXMgY29udGFpbmVkIGluIGEgZm9sZGVyIG5vZGUuXHJcbiAgICAgKiBAcGFyYW0gbm9kZUlkIElEIG9mIHRoZSB0YXJnZXQgbm9kZVxyXG4gICAgICogQHBhcmFtIG9wdGlvbnMgT3B0aW9uYWwgcGFyYW1ldGVycyBzdXBwb3J0ZWQgYnkgSlMtQVBJXHJcbiAgICAgKiBAcmV0dXJucyBMaXN0IG9mIGNoaWxkIGl0ZW1zIGZyb20gdGhlIGZvbGRlclxyXG4gICAgICovXHJcbiAgICBnZXROb2RlQ2hpbGRyZW4obm9kZUlkOiBzdHJpbmcsIG9wdGlvbnM6IGFueSA9IHt9KTogT2JzZXJ2YWJsZTxOb2RlUGFnaW5nPiB7XHJcbiAgICAgICAgY29uc3QgZGVmYXVsdHMgPSB7XHJcbiAgICAgICAgICAgIG1heEl0ZW1zOiB0aGlzLnByZWZlcmVuY2VzLnBhZ2luYXRpb25TaXplLFxyXG4gICAgICAgICAgICBza2lwQ291bnQ6IDAsXHJcbiAgICAgICAgICAgIGluY2x1ZGU6IFsgJ3BhdGgnLCAncHJvcGVydGllcycsICdhbGxvd2FibGVPcGVyYXRpb25zJywgJ3Blcm1pc3Npb25zJyBdXHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zdCBxdWVyeU9wdGlvbnMgPSBPYmplY3QuYXNzaWduKGRlZmF1bHRzLCBvcHRpb25zKTtcclxuICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5ub2Rlc0FwaVxyXG4gICAgICAgICAgICAuZ2V0Tm9kZUNoaWxkcmVuKG5vZGVJZCwgcXVlcnlPcHRpb25zKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20ocHJvbWlzZSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aHJvd0Vycm9yKGVycikpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZXMgYSBuZXcgZG9jdW1lbnQgbm9kZSBpbnNpZGUgYSBmb2xkZXIuXHJcbiAgICAgKiBAcGFyYW0gcGFyZW50Tm9kZUlkIElEIG9mIHRoZSBwYXJlbnQgZm9sZGVyIG5vZGVcclxuICAgICAqIEBwYXJhbSBub2RlQm9keSBEYXRhIGZvciB0aGUgbmV3IG5vZGVcclxuICAgICAqIEBwYXJhbSBvcHRpb25zIE9wdGlvbmFsIHBhcmFtZXRlcnMgc3VwcG9ydGVkIGJ5IEpTLUFQSVxyXG4gICAgICogQHJldHVybnMgRGV0YWlscyBvZiB0aGUgbmV3IG5vZGVcclxuICAgICAqL1xyXG4gICAgY3JlYXRlTm9kZShwYXJlbnROb2RlSWQ6IHN0cmluZywgbm9kZUJvZHk6IGFueSwgb3B0aW9uczogYW55ID0ge30pOiBPYnNlcnZhYmxlPE1pbmltYWxOb2RlPiB7XHJcbiAgICAgICAgY29uc3QgcHJvbWlzZSA9IHRoaXMubm9kZXNBcGlcclxuICAgICAgICAgICAgLmFkZE5vZGUocGFyZW50Tm9kZUlkLCBub2RlQm9keSwgb3B0aW9ucylcclxuICAgICAgICAgICAgLnRoZW4odGhpcy5nZXRFbnRyeUZyb21FbnRpdHkpO1xyXG5cclxuICAgICAgICByZXR1cm4gZnJvbShwcm9taXNlKS5waXBlKFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRocm93RXJyb3IoZXJyKSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBhIG5ldyBmb2xkZXIgbm9kZSBpbnNpZGUgYSBwYXJlbnQgZm9sZGVyLlxyXG4gICAgICogQHBhcmFtIHBhcmVudE5vZGVJZCBJRCBvZiB0aGUgcGFyZW50IGZvbGRlciBub2RlXHJcbiAgICAgKiBAcGFyYW0gbm9kZUJvZHkgRGF0YSBmb3IgdGhlIG5ldyBmb2xkZXJcclxuICAgICAqIEBwYXJhbSBvcHRpb25zIE9wdGlvbmFsIHBhcmFtZXRlcnMgc3VwcG9ydGVkIGJ5IEpTLUFQSVxyXG4gICAgICogQHJldHVybnMgRGV0YWlscyBvZiB0aGUgbmV3IGZvbGRlclxyXG4gICAgICovXHJcbiAgICBjcmVhdGVGb2xkZXIocGFyZW50Tm9kZUlkOiBzdHJpbmcsIG5vZGVCb2R5OiBhbnksIG9wdGlvbnM6IGFueSA9IHt9KTogT2JzZXJ2YWJsZTxNaW5pbWFsTm9kZT4ge1xyXG4gICAgICAgIGNvbnN0IGJvZHkgPSBPYmplY3QuYXNzaWduKHsgbm9kZVR5cGU6ICdjbTpmb2xkZXInIH0sIG5vZGVCb2R5KTtcclxuICAgICAgICByZXR1cm4gdGhpcy5jcmVhdGVOb2RlKHBhcmVudE5vZGVJZCwgYm9keSwgb3B0aW9ucyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBVcGRhdGVzIHRoZSBpbmZvcm1hdGlvbiBhYm91dCBhIG5vZGUuXHJcbiAgICAgKiBAcGFyYW0gbm9kZUlkIElEIG9mIHRoZSB0YXJnZXQgbm9kZVxyXG4gICAgICogQHBhcmFtIG5vZGVCb2R5IE5ldyBkYXRhIGZvciB0aGUgbm9kZVxyXG4gICAgICogQHBhcmFtIG9wdGlvbnMgT3B0aW9uYWwgcGFyYW1ldGVycyBzdXBwb3J0ZWQgYnkgSlMtQVBJXHJcbiAgICAgKiBAcmV0dXJucyBVcGRhdGVkIG5vZGUgaW5mb3JtYXRpb25cclxuICAgICAqL1xyXG4gICAgdXBkYXRlTm9kZShub2RlSWQ6IHN0cmluZywgbm9kZUJvZHk6IGFueSwgb3B0aW9uczogYW55ID0ge30pOiBPYnNlcnZhYmxlPE1pbmltYWxOb2RlPiB7XHJcbiAgICAgICAgY29uc3QgZGVmYXVsdHMgPSB7XHJcbiAgICAgICAgICAgIGluY2x1ZGU6IFsgJ3BhdGgnLCAncHJvcGVydGllcycsICdhbGxvd2FibGVPcGVyYXRpb25zJywgJ3Blcm1pc3Npb25zJyBdXHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zdCBxdWVyeU9wdGlvbnMgPSBPYmplY3QuYXNzaWduKGRlZmF1bHRzLCBvcHRpb25zKTtcclxuXHJcbiAgICAgICAgY29uc3QgcHJvbWlzZSA9IHRoaXMubm9kZXNBcGlcclxuICAgICAgICAgICAgLnVwZGF0ZU5vZGUobm9kZUlkLCBub2RlQm9keSwgcXVlcnlPcHRpb25zKVxyXG4gICAgICAgICAgICAudGhlbih0aGlzLmdldEVudHJ5RnJvbUVudGl0eSk7XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHByb21pc2UpLnBpcGUoXHJcbiAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhyb3dFcnJvcihlcnIpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBNb3ZlcyBhIG5vZGUgdG8gdGhlIHRyYXNoY2FuLlxyXG4gICAgICogQHBhcmFtIG5vZGVJZCBJRCBvZiB0aGUgdGFyZ2V0IG5vZGVcclxuICAgICAqIEBwYXJhbSBvcHRpb25zIE9wdGlvbmFsIHBhcmFtZXRlcnMgc3VwcG9ydGVkIGJ5IEpTLUFQSVxyXG4gICAgICogQHJldHVybnMgRW1wdHkgcmVzdWx0IHRoYXQgbm90aWZpZXMgd2hlbiB0aGUgZGVsZXRpb24gaXMgY29tcGxldGVcclxuICAgICAqL1xyXG4gICAgZGVsZXRlTm9kZShub2RlSWQ6IHN0cmluZywgb3B0aW9uczogYW55ID0ge30pOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLm5vZGVzQXBpLmRlbGV0ZU5vZGUobm9kZUlkLCBvcHRpb25zKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20ocHJvbWlzZSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aHJvd0Vycm9yKGVycikpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlc3RvcmVzIGEgbm9kZSBwcmV2aW91c2x5IG1vdmVkIHRvIHRoZSB0cmFzaGNhbi5cclxuICAgICAqIEBwYXJhbSBub2RlSWQgSUQgb2YgdGhlIG5vZGUgdG8gcmVzdG9yZVxyXG4gICAgICogQHJldHVybnMgRGV0YWlscyBvZiB0aGUgcmVzdG9yZWQgbm9kZVxyXG4gICAgICovXHJcbiAgICByZXN0b3JlTm9kZShub2RlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8TWluaW1hbE5vZGU+IHtcclxuICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5ub2Rlc0FwaVxyXG4gICAgICAgICAgICAucmVzdG9yZU5vZGUobm9kZUlkKVxyXG4gICAgICAgICAgICAudGhlbih0aGlzLmdldEVudHJ5RnJvbUVudGl0eSk7XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHByb21pc2UpLnBpcGUoXHJcbiAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhyb3dFcnJvcihlcnIpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19