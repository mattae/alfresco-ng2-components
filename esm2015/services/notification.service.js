/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { TranslationService } from './translation.service';
import { AppConfigService, AppConfigValues } from '../app-config/app-config.service';
import { Subject } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/snack-bar";
import * as i2 from "./translation.service";
import * as i3 from "../app-config/app-config.service";
export class NotificationService {
    /**
     * @param {?} snackBar
     * @param {?} translationService
     * @param {?} appConfigService
     */
    constructor(snackBar, translationService, appConfigService) {
        this.snackBar = snackBar;
        this.translationService = translationService;
        this.appConfigService = appConfigService;
        this.DEFAULT_DURATION_MESSAGE = 5000;
        this.messages = new Subject();
        this.DEFAULT_DURATION_MESSAGE = this.appConfigService.get(AppConfigValues.NOTIFY_DURATION) || this.DEFAULT_DURATION_MESSAGE;
    }
    /**
     * Opens a SnackBar notification to show a message.
     * @param {?} message The message (or resource key) to show.
     * @param {?=} config Time before notification disappears after being shown or MatSnackBarConfig object
     * @return {?} Information/control object for the SnackBar
     */
    openSnackMessage(message, config) {
        if (!config) {
            config = this.DEFAULT_DURATION_MESSAGE;
        }
        /** @type {?} */
        const translatedMessage = this.translationService.instant(message);
        if (typeof config === 'number') {
            config = {
                duration: config
            };
        }
        this.messages.next({ message: translatedMessage, dateTime: new Date });
        return this.snackBar.open(translatedMessage, null, config);
    }
    /**
     * Opens a SnackBar notification with a message and a response button.
     * @param {?} message The message (or resource key) to show.
     * @param {?} action Caption for the response button
     * @param {?=} config Time before notification disappears after being shown or MatSnackBarConfig object
     * @return {?} Information/control object for the SnackBar
     */
    openSnackMessageAction(message, action, config) {
        if (!config) {
            config = this.DEFAULT_DURATION_MESSAGE;
        }
        /** @type {?} */
        const translatedMessage = this.translationService.instant(message);
        if (typeof config === 'number') {
            config = {
                duration: config
            };
        }
        this.messages.next({ message: translatedMessage, dateTime: new Date });
        return this.snackBar.open(translatedMessage, action, config);
    }
    /**
     *  dismiss the notification snackbar
     * @return {?}
     */
    dismissSnackMessageAction() {
        return this.snackBar.dismiss();
    }
    /**
     * @protected
     * @param {?} message
     * @param {?} panelClass
     * @param {?=} action
     * @return {?}
     */
    showMessage(message, panelClass, action) {
        message = this.translationService.instant(message);
        this.messages.next({ message: message, dateTime: new Date });
        return this.snackBar.open(message, action, {
            duration: this.DEFAULT_DURATION_MESSAGE,
            panelClass
        });
    }
    /**
     * Rase error message
     * @param {?} message Text message or translation key for the message.
     * @param {?=} action Action name
     * @return {?}
     */
    showError(message, action) {
        return this.showMessage(message, 'adf-error-snackbar', action);
    }
    /**
     * Rase info message
     * @param {?} message Text message or translation key for the message.
     * @param {?=} action Action name
     * @return {?}
     */
    showInfo(message, action) {
        return this.showMessage(message, 'adf-info-snackbar', action);
    }
    /**
     * Rase warning message
     * @param {?} message Text message or translation key for the message.
     * @param {?=} action Action name
     * @return {?}
     */
    showWarning(message, action) {
        return this.showMessage(message, 'adf-warning-snackbar', action);
    }
}
NotificationService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
NotificationService.ctorParameters = () => [
    { type: MatSnackBar },
    { type: TranslationService },
    { type: AppConfigService }
];
/** @nocollapse */ NotificationService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function NotificationService_Factory() { return new NotificationService(i0.ɵɵinject(i1.MatSnackBar), i0.ɵɵinject(i2.TranslationService), i0.ɵɵinject(i3.AppConfigService)); }, token: NotificationService, providedIn: "root" });
if (false) {
    /** @type {?} */
    NotificationService.prototype.DEFAULT_DURATION_MESSAGE;
    /** @type {?} */
    NotificationService.prototype.messages;
    /**
     * @type {?}
     * @private
     */
    NotificationService.prototype.snackBar;
    /**
     * @type {?}
     * @private
     */
    NotificationService.prototype.translationService;
    /**
     * @type {?}
     * @private
     */
    NotificationService.prototype.appConfigService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9ub3RpZmljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxXQUFXLEVBQXFDLE1BQU0sbUJBQW1CLENBQUM7QUFDbkYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDM0QsT0FBTyxFQUFFLGdCQUFnQixFQUFFLGVBQWUsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JGLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7Ozs7O0FBTS9CLE1BQU0sT0FBTyxtQkFBbUI7Ozs7OztJQU01QixZQUFvQixRQUFxQixFQUNyQixrQkFBc0MsRUFDdEMsZ0JBQWtDO1FBRmxDLGFBQVEsR0FBUixRQUFRLENBQWE7UUFDckIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBTnRELDZCQUF3QixHQUFXLElBQUksQ0FBQztRQUV4QyxhQUFRLEdBQStCLElBQUksT0FBTyxFQUFFLENBQUM7UUFLakQsSUFBSSxDQUFDLHdCQUF3QixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQVMsZUFBZSxDQUFDLGVBQWUsQ0FBQyxJQUFJLElBQUksQ0FBQyx3QkFBd0IsQ0FBQztJQUV4SSxDQUFDOzs7Ozs7O0lBUUQsZ0JBQWdCLENBQUMsT0FBZSxFQUFFLE1BQW1DO1FBQ2pFLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDVCxNQUFNLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDO1NBQzFDOztjQUVLLGlCQUFpQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDO1FBRWxFLElBQUksT0FBTyxNQUFNLEtBQUssUUFBUSxFQUFFO1lBQzVCLE1BQU0sR0FBRztnQkFDTCxRQUFRLEVBQUUsTUFBTTthQUNuQixDQUFDO1NBQ0w7UUFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxRQUFRLEVBQUUsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBRXZFLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsSUFBSSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQy9ELENBQUM7Ozs7Ozs7O0lBU0Qsc0JBQXNCLENBQUMsT0FBZSxFQUFFLE1BQWMsRUFBRSxNQUFtQztRQUN2RixJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ1QsTUFBTSxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQztTQUMxQzs7Y0FFSyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQztRQUVsRSxJQUFJLE9BQU8sTUFBTSxLQUFLLFFBQVEsRUFBRTtZQUM1QixNQUFNLEdBQUc7Z0JBQ0wsUUFBUSxFQUFFLE1BQU07YUFDbkIsQ0FBQztTQUNMO1FBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsUUFBUSxFQUFFLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQztRQUV2RSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNqRSxDQUFDOzs7OztJQUtELHlCQUF5QjtRQUNyQixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDbkMsQ0FBQzs7Ozs7Ozs7SUFFUyxXQUFXLENBQUMsT0FBZSxFQUFFLFVBQWtCLEVBQUUsTUFBZTtRQUN0RSxPQUFPLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUVuRCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsUUFBUSxFQUFFLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQztRQUU3RCxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxNQUFNLEVBQUU7WUFDdkMsUUFBUSxFQUFFLElBQUksQ0FBQyx3QkFBd0I7WUFDdkMsVUFBVTtTQUNiLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7SUFPRCxTQUFTLENBQUMsT0FBZSxFQUFFLE1BQWU7UUFDdEMsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNuRSxDQUFDOzs7Ozs7O0lBT0QsUUFBUSxDQUFDLE9BQWUsRUFBRSxNQUFlO1FBQ3JDLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDbEUsQ0FBQzs7Ozs7OztJQU9ELFdBQVcsQ0FBQyxPQUFlLEVBQUUsTUFBZTtRQUN4QyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ3JFLENBQUM7OztZQTVHSixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFSUSxXQUFXO1lBQ1gsa0JBQWtCO1lBQ2xCLGdCQUFnQjs7Ozs7SUFTckIsdURBQXdDOztJQUV4Qyx1Q0FBcUQ7Ozs7O0lBRXpDLHVDQUE2Qjs7Ozs7SUFDN0IsaURBQThDOzs7OztJQUM5QywrQ0FBMEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXRTbmFja0JhciwgTWF0U25hY2tCYXJSZWYsIE1hdFNuYWNrQmFyQ29uZmlnIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGlvblNlcnZpY2UgfSBmcm9tICcuL3RyYW5zbGF0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBDb25maWdTZXJ2aWNlLCBBcHBDb25maWdWYWx1ZXMgfSBmcm9tICcuLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgTm90aWZpY2F0aW9uTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvbm90aWZpY2F0aW9uLm1vZGVsJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTm90aWZpY2F0aW9uU2VydmljZSB7XHJcblxyXG4gICAgREVGQVVMVF9EVVJBVElPTl9NRVNTQUdFOiBudW1iZXIgPSA1MDAwO1xyXG5cclxuICAgIG1lc3NhZ2VzOiBTdWJqZWN0PE5vdGlmaWNhdGlvbk1vZGVsPiA9IG5ldyBTdWJqZWN0KCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBzbmFja0JhcjogTWF0U25hY2tCYXIsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHRyYW5zbGF0aW9uU2VydmljZTogVHJhbnNsYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBhcHBDb25maWdTZXJ2aWNlOiBBcHBDb25maWdTZXJ2aWNlKSB7XHJcbiAgICAgICAgdGhpcy5ERUZBVUxUX0RVUkFUSU9OX01FU1NBR0UgPSB0aGlzLmFwcENvbmZpZ1NlcnZpY2UuZ2V0PG51bWJlcj4oQXBwQ29uZmlnVmFsdWVzLk5PVElGWV9EVVJBVElPTikgfHwgdGhpcy5ERUZBVUxUX0RVUkFUSU9OX01FU1NBR0U7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogT3BlbnMgYSBTbmFja0JhciBub3RpZmljYXRpb24gdG8gc2hvdyBhIG1lc3NhZ2UuXHJcbiAgICAgKiBAcGFyYW0gbWVzc2FnZSBUaGUgbWVzc2FnZSAob3IgcmVzb3VyY2Uga2V5KSB0byBzaG93LlxyXG4gICAgICogQHBhcmFtIGNvbmZpZyBUaW1lIGJlZm9yZSBub3RpZmljYXRpb24gZGlzYXBwZWFycyBhZnRlciBiZWluZyBzaG93biBvciBNYXRTbmFja0JhckNvbmZpZyBvYmplY3RcclxuICAgICAqIEByZXR1cm5zIEluZm9ybWF0aW9uL2NvbnRyb2wgb2JqZWN0IGZvciB0aGUgU25hY2tCYXJcclxuICAgICAqL1xyXG4gICAgb3BlblNuYWNrTWVzc2FnZShtZXNzYWdlOiBzdHJpbmcsIGNvbmZpZz86IG51bWJlciB8IE1hdFNuYWNrQmFyQ29uZmlnKTogTWF0U25hY2tCYXJSZWY8YW55PiB7XHJcbiAgICAgICAgaWYgKCFjb25maWcpIHtcclxuICAgICAgICAgICAgY29uZmlnID0gdGhpcy5ERUZBVUxUX0RVUkFUSU9OX01FU1NBR0U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB0cmFuc2xhdGVkTWVzc2FnZSA9IHRoaXMudHJhbnNsYXRpb25TZXJ2aWNlLmluc3RhbnQobWVzc2FnZSk7XHJcblxyXG4gICAgICAgIGlmICh0eXBlb2YgY29uZmlnID09PSAnbnVtYmVyJykge1xyXG4gICAgICAgICAgICBjb25maWcgPSB7XHJcbiAgICAgICAgICAgICAgICBkdXJhdGlvbjogY29uZmlnXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLm1lc3NhZ2VzLm5leHQoeyBtZXNzYWdlOiB0cmFuc2xhdGVkTWVzc2FnZSwgZGF0ZVRpbWU6IG5ldyBEYXRlIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5zbmFja0Jhci5vcGVuKHRyYW5zbGF0ZWRNZXNzYWdlLCBudWxsLCBjb25maWcpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogT3BlbnMgYSBTbmFja0JhciBub3RpZmljYXRpb24gd2l0aCBhIG1lc3NhZ2UgYW5kIGEgcmVzcG9uc2UgYnV0dG9uLlxyXG4gICAgICogQHBhcmFtIG1lc3NhZ2UgVGhlIG1lc3NhZ2UgKG9yIHJlc291cmNlIGtleSkgdG8gc2hvdy5cclxuICAgICAqIEBwYXJhbSBhY3Rpb24gQ2FwdGlvbiBmb3IgdGhlIHJlc3BvbnNlIGJ1dHRvblxyXG4gICAgICogQHBhcmFtIGNvbmZpZyBUaW1lIGJlZm9yZSBub3RpZmljYXRpb24gZGlzYXBwZWFycyBhZnRlciBiZWluZyBzaG93biBvciBNYXRTbmFja0JhckNvbmZpZyBvYmplY3RcclxuICAgICAqIEByZXR1cm5zIEluZm9ybWF0aW9uL2NvbnRyb2wgb2JqZWN0IGZvciB0aGUgU25hY2tCYXJcclxuICAgICAqL1xyXG4gICAgb3BlblNuYWNrTWVzc2FnZUFjdGlvbihtZXNzYWdlOiBzdHJpbmcsIGFjdGlvbjogc3RyaW5nLCBjb25maWc/OiBudW1iZXIgfCBNYXRTbmFja0JhckNvbmZpZyk6IE1hdFNuYWNrQmFyUmVmPGFueT4ge1xyXG4gICAgICAgIGlmICghY29uZmlnKSB7XHJcbiAgICAgICAgICAgIGNvbmZpZyA9IHRoaXMuREVGQVVMVF9EVVJBVElPTl9NRVNTQUdFO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgdHJhbnNsYXRlZE1lc3NhZ2UgPSB0aGlzLnRyYW5zbGF0aW9uU2VydmljZS5pbnN0YW50KG1lc3NhZ2UpO1xyXG5cclxuICAgICAgICBpZiAodHlwZW9mIGNvbmZpZyA9PT0gJ251bWJlcicpIHtcclxuICAgICAgICAgICAgY29uZmlnID0ge1xyXG4gICAgICAgICAgICAgICAgZHVyYXRpb246IGNvbmZpZ1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5tZXNzYWdlcy5uZXh0KHsgbWVzc2FnZTogdHJhbnNsYXRlZE1lc3NhZ2UsIGRhdGVUaW1lOiBuZXcgRGF0ZSB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc25hY2tCYXIub3Blbih0cmFuc2xhdGVkTWVzc2FnZSwgYWN0aW9uLCBjb25maWcpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogIGRpc21pc3MgdGhlIG5vdGlmaWNhdGlvbiBzbmFja2JhclxyXG4gICAgICovXHJcbiAgICBkaXNtaXNzU25hY2tNZXNzYWdlQWN0aW9uKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNuYWNrQmFyLmRpc21pc3MoKTtcclxuICAgIH1cclxuXHJcbiAgICBwcm90ZWN0ZWQgc2hvd01lc3NhZ2UobWVzc2FnZTogc3RyaW5nLCBwYW5lbENsYXNzOiBzdHJpbmcsIGFjdGlvbj86IHN0cmluZyk6IE1hdFNuYWNrQmFyUmVmPGFueT4ge1xyXG4gICAgICAgIG1lc3NhZ2UgPSB0aGlzLnRyYW5zbGF0aW9uU2VydmljZS5pbnN0YW50KG1lc3NhZ2UpO1xyXG5cclxuICAgICAgICB0aGlzLm1lc3NhZ2VzLm5leHQoeyBtZXNzYWdlOiBtZXNzYWdlLCBkYXRlVGltZTogbmV3IERhdGUgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLnNuYWNrQmFyLm9wZW4obWVzc2FnZSwgYWN0aW9uLCB7XHJcbiAgICAgICAgICAgIGR1cmF0aW9uOiB0aGlzLkRFRkFVTFRfRFVSQVRJT05fTUVTU0FHRSxcclxuICAgICAgICAgICAgcGFuZWxDbGFzc1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmFzZSBlcnJvciBtZXNzYWdlXHJcbiAgICAgKiBAcGFyYW0gbWVzc2FnZSBUZXh0IG1lc3NhZ2Ugb3IgdHJhbnNsYXRpb24ga2V5IGZvciB0aGUgbWVzc2FnZS5cclxuICAgICAqIEBwYXJhbSBhY3Rpb24gQWN0aW9uIG5hbWVcclxuICAgICAqL1xyXG4gICAgc2hvd0Vycm9yKG1lc3NhZ2U6IHN0cmluZywgYWN0aW9uPzogc3RyaW5nKTogTWF0U25hY2tCYXJSZWY8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2hvd01lc3NhZ2UobWVzc2FnZSwgJ2FkZi1lcnJvci1zbmFja2JhcicsIGFjdGlvbik7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSYXNlIGluZm8gbWVzc2FnZVxyXG4gICAgICogQHBhcmFtIG1lc3NhZ2UgVGV4dCBtZXNzYWdlIG9yIHRyYW5zbGF0aW9uIGtleSBmb3IgdGhlIG1lc3NhZ2UuXHJcbiAgICAgKiBAcGFyYW0gYWN0aW9uIEFjdGlvbiBuYW1lXHJcbiAgICAgKi9cclxuICAgIHNob3dJbmZvKG1lc3NhZ2U6IHN0cmluZywgYWN0aW9uPzogc3RyaW5nKTogTWF0U25hY2tCYXJSZWY8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2hvd01lc3NhZ2UobWVzc2FnZSwgJ2FkZi1pbmZvLXNuYWNrYmFyJywgYWN0aW9uKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJhc2Ugd2FybmluZyBtZXNzYWdlXHJcbiAgICAgKiBAcGFyYW0gbWVzc2FnZSBUZXh0IG1lc3NhZ2Ugb3IgdHJhbnNsYXRpb24ga2V5IGZvciB0aGUgbWVzc2FnZS5cclxuICAgICAqIEBwYXJhbSBhY3Rpb24gQWN0aW9uIG5hbWVcclxuICAgICAqL1xyXG4gICAgc2hvd1dhcm5pbmcobWVzc2FnZTogc3RyaW5nLCBhY3Rpb24/OiBzdHJpbmcpOiBNYXRTbmFja0JhclJlZjxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zaG93TWVzc2FnZShtZXNzYWdlLCAnYWRmLXdhcm5pbmctc25hY2tiYXInLCBhY3Rpb24pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==