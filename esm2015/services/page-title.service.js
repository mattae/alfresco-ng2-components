/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AppConfigService } from '../app-config/app-config.service';
import { TranslationService } from './translation.service';
import * as i0 from "@angular/core";
import * as i1 from "@angular/platform-browser";
import * as i2 from "../app-config/app-config.service";
import * as i3 from "./translation.service";
export class PageTitleService {
    /**
     * @param {?} titleService
     * @param {?} appConfig
     * @param {?} translationService
     */
    constructor(titleService, appConfig, translationService) {
        this.titleService = titleService;
        this.appConfig = appConfig;
        this.translationService = translationService;
        this.originalTitle = '';
        this.translatedTitle = '';
        translationService.translate.onLangChange.subscribe((/**
         * @return {?}
         */
        () => this.onLanguageChanged()));
        translationService.translate.onTranslationChange.subscribe((/**
         * @return {?}
         */
        () => this.onLanguageChanged()));
    }
    /**
     * Sets the page title.
     * @param {?=} value The new title
     * @return {?}
     */
    setTitle(value = '') {
        this.originalTitle = value;
        this.translatedTitle = this.translationService.instant(value);
        this.updateTitle();
    }
    /**
     * @private
     * @return {?}
     */
    onLanguageChanged() {
        this.translatedTitle = this.translationService.instant(this.originalTitle);
        this.updateTitle();
    }
    /**
     * @private
     * @return {?}
     */
    updateTitle() {
        /** @type {?} */
        const name = this.appConfig.get('application.name') || 'Alfresco ADF Application';
        /** @type {?} */
        const title = this.translatedTitle ? `${this.translatedTitle} - ${name}` : `${name}`;
        this.titleService.setTitle(title);
    }
}
PageTitleService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
PageTitleService.ctorParameters = () => [
    { type: Title },
    { type: AppConfigService },
    { type: TranslationService }
];
/** @nocollapse */ PageTitleService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function PageTitleService_Factory() { return new PageTitleService(i0.ɵɵinject(i1.Title), i0.ɵɵinject(i2.AppConfigService), i0.ɵɵinject(i3.TranslationService)); }, token: PageTitleService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    PageTitleService.prototype.originalTitle;
    /**
     * @type {?}
     * @private
     */
    PageTitleService.prototype.translatedTitle;
    /**
     * @type {?}
     * @private
     */
    PageTitleService.prototype.titleService;
    /**
     * @type {?}
     * @private
     */
    PageTitleService.prototype.appConfig;
    /**
     * @type {?}
     * @private
     */
    PageTitleService.prototype.translationService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZS10aXRsZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvcGFnZS10aXRsZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHVCQUF1QixDQUFDOzs7OztBQUszRCxNQUFNLE9BQU8sZ0JBQWdCOzs7Ozs7SUFLekIsWUFDWSxZQUFtQixFQUNuQixTQUEyQixFQUMzQixrQkFBc0M7UUFGdEMsaUJBQVksR0FBWixZQUFZLENBQU87UUFDbkIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFDM0IsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQU4xQyxrQkFBYSxHQUFXLEVBQUUsQ0FBQztRQUMzQixvQkFBZSxHQUFXLEVBQUUsQ0FBQztRQU1qQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLFNBQVM7OztRQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxFQUFDLENBQUM7UUFDcEYsa0JBQWtCLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLFNBQVM7OztRQUFDLEdBQUcsRUFBRSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxFQUFDLENBQUM7SUFDL0YsQ0FBQzs7Ozs7O0lBTUQsUUFBUSxDQUFDLFFBQWdCLEVBQUU7UUFDdkIsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFDM0IsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRTlELElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUN2QixDQUFDOzs7OztJQUVPLGlCQUFpQjtRQUNyQixJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzNFLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUN2QixDQUFDOzs7OztJQUVPLFdBQVc7O2NBQ1QsSUFBSSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLElBQUksMEJBQTBCOztjQUUzRSxLQUFLLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsR0FBRyxJQUFJLENBQUMsZUFBZSxNQUFNLElBQUksRUFBRSxDQUFDLENBQUMsQ0FBQyxHQUFHLElBQUksRUFBRTtRQUNwRixJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN0QyxDQUFDOzs7WUFyQ0osVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBTlEsS0FBSztZQUNMLGdCQUFnQjtZQUNoQixrQkFBa0I7Ozs7Ozs7O0lBT3ZCLHlDQUFtQzs7Ozs7SUFDbkMsMkNBQXFDOzs7OztJQUdqQyx3Q0FBMkI7Ozs7O0lBQzNCLHFDQUFtQzs7Ozs7SUFDbkMsOENBQThDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVGl0bGUgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcclxuaW1wb3J0IHsgQXBwQ29uZmlnU2VydmljZSB9IGZyb20gJy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVHJhbnNsYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi90cmFuc2xhdGlvbi5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgUGFnZVRpdGxlU2VydmljZSB7XHJcblxyXG4gICAgcHJpdmF0ZSBvcmlnaW5hbFRpdGxlOiBzdHJpbmcgPSAnJztcclxuICAgIHByaXZhdGUgdHJhbnNsYXRlZFRpdGxlOiBzdHJpbmcgPSAnJztcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIHRpdGxlU2VydmljZTogVGl0bGUsXHJcbiAgICAgICAgcHJpdmF0ZSBhcHBDb25maWc6IEFwcENvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSB0cmFuc2xhdGlvblNlcnZpY2U6IFRyYW5zbGF0aW9uU2VydmljZSkge1xyXG4gICAgICAgIHRyYW5zbGF0aW9uU2VydmljZS50cmFuc2xhdGUub25MYW5nQ2hhbmdlLnN1YnNjcmliZSgoKSA9PiB0aGlzLm9uTGFuZ3VhZ2VDaGFuZ2VkKCkpO1xyXG4gICAgICAgIHRyYW5zbGF0aW9uU2VydmljZS50cmFuc2xhdGUub25UcmFuc2xhdGlvbkNoYW5nZS5zdWJzY3JpYmUoKCkgPT4gdGhpcy5vbkxhbmd1YWdlQ2hhbmdlZCgpKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldHMgdGhlIHBhZ2UgdGl0bGUuXHJcbiAgICAgKiBAcGFyYW0gdmFsdWUgVGhlIG5ldyB0aXRsZVxyXG4gICAgICovXHJcbiAgICBzZXRUaXRsZSh2YWx1ZTogc3RyaW5nID0gJycpIHtcclxuICAgICAgICB0aGlzLm9yaWdpbmFsVGl0bGUgPSB2YWx1ZTtcclxuICAgICAgICB0aGlzLnRyYW5zbGF0ZWRUaXRsZSA9IHRoaXMudHJhbnNsYXRpb25TZXJ2aWNlLmluc3RhbnQodmFsdWUpO1xyXG5cclxuICAgICAgICB0aGlzLnVwZGF0ZVRpdGxlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvbkxhbmd1YWdlQ2hhbmdlZCgpIHtcclxuICAgICAgICB0aGlzLnRyYW5zbGF0ZWRUaXRsZSA9IHRoaXMudHJhbnNsYXRpb25TZXJ2aWNlLmluc3RhbnQodGhpcy5vcmlnaW5hbFRpdGxlKTtcclxuICAgICAgICB0aGlzLnVwZGF0ZVRpdGxlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSB1cGRhdGVUaXRsZSgpIHtcclxuICAgICAgICBjb25zdCBuYW1lID0gdGhpcy5hcHBDb25maWcuZ2V0KCdhcHBsaWNhdGlvbi5uYW1lJykgfHwgJ0FsZnJlc2NvIEFERiBBcHBsaWNhdGlvbic7XHJcblxyXG4gICAgICAgIGNvbnN0IHRpdGxlID0gdGhpcy50cmFuc2xhdGVkVGl0bGUgPyBgJHt0aGlzLnRyYW5zbGF0ZWRUaXRsZX0gLSAke25hbWV9YCA6IGAke25hbWV9YDtcclxuICAgICAgICB0aGlzLnRpdGxlU2VydmljZS5zZXRUaXRsZSh0aXRsZSk7XHJcbiAgICB9XHJcbn1cclxuIl19