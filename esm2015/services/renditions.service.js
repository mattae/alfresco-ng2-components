/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { Observable, from, interval, empty } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { concatMap, switchMap, takeWhile, map } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
export class RenditionsService {
    /**
     * @param {?} apiService
     */
    constructor(apiService) {
        this.apiService = apiService;
    }
    /**
     * Gets the first available rendition found for a node.
     * @param {?} nodeId ID of the target node
     * @return {?} Information object for the rendition
     */
    getAvailableRenditionForNode(nodeId) {
        return from(this.apiService.renditionsApi.getRenditions(nodeId)).pipe(map((/**
         * @param {?} availableRenditions
         * @return {?}
         */
        (availableRenditions) => {
            /** @type {?} */
            const renditionsAvailable = availableRenditions.list.entries.filter((/**
             * @param {?} rendition
             * @return {?}
             */
            (rendition) => (rendition.entry.id === 'pdf' || rendition.entry.id === 'imgpreview')));
            /** @type {?} */
            const existingRendition = renditionsAvailable.find((/**
             * @param {?} rend
             * @return {?}
             */
            (rend) => rend.entry.status === 'CREATED'));
            return existingRendition ? existingRendition : renditionsAvailable[0];
        })));
    }
    /**
     * Generates a rendition for a node using the first available encoding.
     * @param {?} nodeId ID of the target node
     * @return {?} Null response to indicate completion
     */
    generateRenditionForNode(nodeId) {
        return this.getAvailableRenditionForNode(nodeId).pipe(map((/**
         * @param {?} rendition
         * @return {?}
         */
        (rendition) => {
            if (rendition.entry.status !== 'CREATED') {
                return from(this.apiService.renditionsApi.createRendition(nodeId, { id: rendition.entry.id }));
            }
            else {
                return empty();
            }
        })));
    }
    /**
     * Checks if the specified rendition is available for a node.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @return {?} True if the rendition is available, false otherwise
     */
    isRenditionAvailable(nodeId, encoding) {
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        (observer) => {
            this.getRendition(nodeId, encoding).subscribe((/**
             * @param {?} res
             * @return {?}
             */
            (res) => {
                /** @type {?} */
                let isAvailable = true;
                if (res.entry.status.toString() === 'NOT_CREATED') {
                    isAvailable = false;
                }
                observer.next(isAvailable);
                observer.complete();
            }), (/**
             * @return {?}
             */
            () => {
                observer.next(false);
                observer.complete();
            }));
        }));
    }
    /**
     * Checks if the node can be converted using the specified rendition.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @return {?} True if the node can be converted, false otherwise
     */
    isConversionPossible(nodeId, encoding) {
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        (observer) => {
            this.getRendition(nodeId, encoding).subscribe((/**
             * @return {?}
             */
            () => {
                observer.next(true);
                observer.complete();
            }), (/**
             * @return {?}
             */
            () => {
                observer.next(false);
                observer.complete();
            }));
        }));
    }
    /**
     * Gets a URL linking to the specified rendition of a node.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @return {?} URL string
     */
    getRenditionUrl(nodeId, encoding) {
        return this.apiService.contentApi.getRenditionUrl(nodeId, encoding);
    }
    /**
     * Gets information about a rendition of a node.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @return {?} Information object about the rendition
     */
    getRendition(nodeId, encoding) {
        return from(this.apiService.renditionsApi.getRendition(nodeId, encoding));
    }
    /**
     * Gets a list of all renditions for a node.
     * @param {?} nodeId ID of the target node
     * @return {?} Paged list of rendition details
     */
    getRenditionsListByNodeId(nodeId) {
        return from(this.apiService.renditionsApi.getRenditions(nodeId));
    }
    /**
     * Creates a rendition for a node.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @return {?} Null response to indicate completion
     */
    createRendition(nodeId, encoding) {
        return from(this.apiService.renditionsApi.createRendition(nodeId, { id: encoding }));
    }
    /**
     * Repeatedly attempts to create a rendition, through to success or failure.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @param {?=} pollingInterval Time interval (in milliseconds) between checks for completion
     * @param {?=} retries Number of attempts to make before declaring failure
     * @return {?} True if the rendition was created, false otherwise
     */
    convert(nodeId, encoding, pollingInterval = 1000, retries = 5) {
        return this.createRendition(nodeId, encoding)
            .pipe(concatMap((/**
         * @return {?}
         */
        () => this.pollRendition(nodeId, encoding, pollingInterval, retries))));
    }
    /**
     * @private
     * @param {?} nodeId
     * @param {?} encoding
     * @param {?=} intervalSize
     * @param {?=} retries
     * @return {?}
     */
    pollRendition(nodeId, encoding, intervalSize = 1000, retries = 5) {
        /** @type {?} */
        let attempts = 0;
        return interval(intervalSize)
            .pipe(switchMap((/**
         * @return {?}
         */
        () => this.getRendition(nodeId, encoding))), takeWhile((/**
         * @param {?} renditionEntry
         * @return {?}
         */
        (renditionEntry) => {
            attempts += 1;
            if (attempts > retries) {
                return false;
            }
            return (renditionEntry.entry.status.toString() !== 'CREATED');
        })));
    }
}
RenditionsService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
RenditionsService.ctorParameters = () => [
    { type: AlfrescoApiService }
];
/** @nocollapse */ RenditionsService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function RenditionsService_Factory() { return new RenditionsService(i0.ɵɵinject(i1.AlfrescoApiService)); }, token: RenditionsService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    RenditionsService.prototype.apiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVuZGl0aW9ucy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvcmVuZGl0aW9ucy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN6RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7OztBQUt0RSxNQUFNLE9BQU8saUJBQWlCOzs7O0lBRTFCLFlBQW9CLFVBQThCO1FBQTlCLGVBQVUsR0FBVixVQUFVLENBQW9CO0lBQ2xELENBQUM7Ozs7OztJQU9ELDRCQUE0QixDQUFDLE1BQWM7UUFDdkMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUNqRSxHQUFHOzs7O1FBQUMsQ0FBQyxtQkFBb0MsRUFBRSxFQUFFOztrQkFDbkMsbUJBQW1CLEdBQXFCLG1CQUFtQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTTs7OztZQUNqRixDQUFDLFNBQVMsRUFBRSxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUUsS0FBSyxLQUFLLElBQUksU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFLEtBQUssWUFBWSxDQUFDLEVBQUM7O2tCQUNuRixpQkFBaUIsR0FBRyxtQkFBbUIsQ0FBQyxJQUFJOzs7O1lBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxLQUFLLFNBQVMsRUFBQztZQUM3RixPQUFPLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDMUUsQ0FBQyxFQUFDLENBQUMsQ0FBQztJQUNaLENBQUM7Ozs7OztJQU9ELHdCQUF3QixDQUFDLE1BQWM7UUFDbkMsT0FBTyxJQUFJLENBQUMsNEJBQTRCLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUNqRCxHQUFHOzs7O1FBQUMsQ0FBQyxTQUF5QixFQUFFLEVBQUU7WUFDOUIsSUFBSSxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxTQUFTLEVBQUU7Z0JBQ3RDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsRUFBRSxFQUFFLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDbEc7aUJBQU07Z0JBQ0gsT0FBTyxLQUFLLEVBQUUsQ0FBQzthQUNsQjtRQUNMLENBQUMsRUFBQyxDQUNMLENBQUM7SUFDTixDQUFDOzs7Ozs7O0lBUUQsb0JBQW9CLENBQUMsTUFBYyxFQUFFLFFBQWdCO1FBQ2pELE9BQU8sSUFBSSxVQUFVOzs7O1FBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtZQUMvQixJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQyxTQUFTOzs7O1lBQ3pDLENBQUMsR0FBRyxFQUFFLEVBQUU7O29CQUNBLFdBQVcsR0FBRyxJQUFJO2dCQUN0QixJQUFJLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxLQUFLLGFBQWEsRUFBRTtvQkFDL0MsV0FBVyxHQUFHLEtBQUssQ0FBQztpQkFDdkI7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztnQkFDM0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUM7OztZQUNELEdBQUcsRUFBRTtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUNKLENBQUM7UUFDTixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7SUFRRCxvQkFBb0IsQ0FBQyxNQUFjLEVBQUUsUUFBZ0I7UUFDakQsT0FBTyxJQUFJLFVBQVU7Ozs7UUFBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO1lBQy9CLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDLFNBQVM7OztZQUN6QyxHQUFHLEVBQUU7Z0JBQ0QsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFDcEIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUM7OztZQUNELEdBQUcsRUFBRTtnQkFDRCxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUNKLENBQUM7UUFDTixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7SUFRRCxlQUFlLENBQUMsTUFBYyxFQUFFLFFBQWdCO1FBQzVDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQztJQUN4RSxDQUFDOzs7Ozs7O0lBUUQsWUFBWSxDQUFDLE1BQWMsRUFBRSxRQUFnQjtRQUN6QyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDOUUsQ0FBQzs7Ozs7O0lBT0QseUJBQXlCLENBQUMsTUFBYztRQUNwQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztJQUNyRSxDQUFDOzs7Ozs7O0lBUUQsZUFBZSxDQUFDLE1BQWMsRUFBRSxRQUFnQjtRQUM1QyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUN6RixDQUFDOzs7Ozs7Ozs7SUFVRCxPQUFPLENBQUMsTUFBYyxFQUFFLFFBQWdCLEVBQUUsa0JBQTBCLElBQUksRUFBRSxVQUFrQixDQUFDO1FBQ3pGLE9BQU8sSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDO2FBQ3hDLElBQUksQ0FDRCxTQUFTOzs7UUFBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxRQUFRLEVBQUUsZUFBZSxFQUFFLE9BQU8sQ0FBQyxFQUFDLENBQ2xGLENBQUM7SUFDVixDQUFDOzs7Ozs7Ozs7SUFFTyxhQUFhLENBQUMsTUFBYyxFQUFFLFFBQWdCLEVBQUUsZUFBdUIsSUFBSSxFQUFFLFVBQWtCLENBQUM7O1lBQ2hHLFFBQVEsR0FBRyxDQUFDO1FBQ2hCLE9BQU8sUUFBUSxDQUFDLFlBQVksQ0FBQzthQUN4QixJQUFJLENBQ0QsU0FBUzs7O1FBQUMsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLEVBQUMsRUFDcEQsU0FBUzs7OztRQUFDLENBQUMsY0FBOEIsRUFBRSxFQUFFO1lBQ3pDLFFBQVEsSUFBSSxDQUFDLENBQUM7WUFDZCxJQUFJLFFBQVEsR0FBRyxPQUFPLEVBQUU7Z0JBQ3BCLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1lBQ0QsT0FBTyxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxLQUFLLFNBQVMsQ0FBQyxDQUFDO1FBQ2xFLENBQUMsRUFBQyxDQUNMLENBQUM7SUFDVixDQUFDOzs7WUF6SkosVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBTFEsa0JBQWtCOzs7Ozs7OztJQVFYLHVDQUFzQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJlbmRpdGlvbkVudHJ5LCBSZW5kaXRpb25QYWdpbmcgfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgZnJvbSwgaW50ZXJ2YWwsIGVtcHR5IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4vYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBjb25jYXRNYXAsIHN3aXRjaE1hcCwgdGFrZVdoaWxlLCBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFJlbmRpdGlvbnNTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgZmlyc3QgYXZhaWxhYmxlIHJlbmRpdGlvbiBmb3VuZCBmb3IgYSBub2RlLlxyXG4gICAgICogQHBhcmFtIG5vZGVJZCBJRCBvZiB0aGUgdGFyZ2V0IG5vZGVcclxuICAgICAqIEByZXR1cm5zIEluZm9ybWF0aW9uIG9iamVjdCBmb3IgdGhlIHJlbmRpdGlvblxyXG4gICAgICovXHJcbiAgICBnZXRBdmFpbGFibGVSZW5kaXRpb25Gb3JOb2RlKG5vZGVJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxSZW5kaXRpb25FbnRyeT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5yZW5kaXRpb25zQXBpLmdldFJlbmRpdGlvbnMobm9kZUlkKSkucGlwZShcclxuICAgICAgICAgICAgbWFwKChhdmFpbGFibGVSZW5kaXRpb25zOiBSZW5kaXRpb25QYWdpbmcpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlbmRpdGlvbnNBdmFpbGFibGU6IFJlbmRpdGlvbkVudHJ5W10gPSBhdmFpbGFibGVSZW5kaXRpb25zLmxpc3QuZW50cmllcy5maWx0ZXIoXHJcbiAgICAgICAgICAgICAgICAgICAgKHJlbmRpdGlvbikgPT4gKHJlbmRpdGlvbi5lbnRyeS5pZCA9PT0gJ3BkZicgfHwgcmVuZGl0aW9uLmVudHJ5LmlkID09PSAnaW1ncHJldmlldycpKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGV4aXN0aW5nUmVuZGl0aW9uID0gcmVuZGl0aW9uc0F2YWlsYWJsZS5maW5kKChyZW5kKSA9PiByZW5kLmVudHJ5LnN0YXR1cyA9PT0gJ0NSRUFURUQnKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBleGlzdGluZ1JlbmRpdGlvbiA/IGV4aXN0aW5nUmVuZGl0aW9uIDogcmVuZGl0aW9uc0F2YWlsYWJsZVswXTtcclxuICAgICAgICAgICAgfSkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2VuZXJhdGVzIGEgcmVuZGl0aW9uIGZvciBhIG5vZGUgdXNpbmcgdGhlIGZpcnN0IGF2YWlsYWJsZSBlbmNvZGluZy5cclxuICAgICAqIEBwYXJhbSBub2RlSWQgSUQgb2YgdGhlIHRhcmdldCBub2RlXHJcbiAgICAgKiBAcmV0dXJucyBOdWxsIHJlc3BvbnNlIHRvIGluZGljYXRlIGNvbXBsZXRpb25cclxuICAgICAqL1xyXG4gICAgZ2VuZXJhdGVSZW5kaXRpb25Gb3JOb2RlKG5vZGVJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRBdmFpbGFibGVSZW5kaXRpb25Gb3JOb2RlKG5vZGVJZCkucGlwZShcclxuICAgICAgICAgICAgbWFwKChyZW5kaXRpb246IFJlbmRpdGlvbkVudHJ5KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAocmVuZGl0aW9uLmVudHJ5LnN0YXR1cyAhPT0gJ0NSRUFURUQnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLnJlbmRpdGlvbnNBcGkuY3JlYXRlUmVuZGl0aW9uKG5vZGVJZCwgeyBpZDogcmVuZGl0aW9uLmVudHJ5LmlkIH0pKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGVtcHR5KCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrcyBpZiB0aGUgc3BlY2lmaWVkIHJlbmRpdGlvbiBpcyBhdmFpbGFibGUgZm9yIGEgbm9kZS5cclxuICAgICAqIEBwYXJhbSBub2RlSWQgSUQgb2YgdGhlIHRhcmdldCBub2RlXHJcbiAgICAgKiBAcGFyYW0gZW5jb2RpbmcgTmFtZSBvZiB0aGUgcmVuZGl0aW9uIGVuY29kaW5nXHJcbiAgICAgKiBAcmV0dXJucyBUcnVlIGlmIHRoZSByZW5kaXRpb24gaXMgYXZhaWxhYmxlLCBmYWxzZSBvdGhlcndpc2VcclxuICAgICAqL1xyXG4gICAgaXNSZW5kaXRpb25BdmFpbGFibGUobm9kZUlkOiBzdHJpbmcsIGVuY29kaW5nOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUoKG9ic2VydmVyKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0UmVuZGl0aW9uKG5vZGVJZCwgZW5jb2RpbmcpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIChyZXMpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBsZXQgaXNBdmFpbGFibGUgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyZXMuZW50cnkuc3RhdHVzLnRvU3RyaW5nKCkgPT09ICdOT1RfQ1JFQVRFRCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNBdmFpbGFibGUgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChpc0F2YWlsYWJsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrcyBpZiB0aGUgbm9kZSBjYW4gYmUgY29udmVydGVkIHVzaW5nIHRoZSBzcGVjaWZpZWQgcmVuZGl0aW9uLlxyXG4gICAgICogQHBhcmFtIG5vZGVJZCBJRCBvZiB0aGUgdGFyZ2V0IG5vZGVcclxuICAgICAqIEBwYXJhbSBlbmNvZGluZyBOYW1lIG9mIHRoZSByZW5kaXRpb24gZW5jb2RpbmdcclxuICAgICAqIEByZXR1cm5zIFRydWUgaWYgdGhlIG5vZGUgY2FuIGJlIGNvbnZlcnRlZCwgZmFsc2Ugb3RoZXJ3aXNlXHJcbiAgICAgKi9cclxuICAgIGlzQ29udmVyc2lvblBvc3NpYmxlKG5vZGVJZDogc3RyaW5nLCBlbmNvZGluZzogc3RyaW5nKTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKChvYnNlcnZlcikgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmdldFJlbmRpdGlvbihub2RlSWQsIGVuY29kaW5nKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0cnVlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZhbHNlKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhIFVSTCBsaW5raW5nIHRvIHRoZSBzcGVjaWZpZWQgcmVuZGl0aW9uIG9mIGEgbm9kZS5cclxuICAgICAqIEBwYXJhbSBub2RlSWQgSUQgb2YgdGhlIHRhcmdldCBub2RlXHJcbiAgICAgKiBAcGFyYW0gZW5jb2RpbmcgTmFtZSBvZiB0aGUgcmVuZGl0aW9uIGVuY29kaW5nXHJcbiAgICAgKiBAcmV0dXJucyBVUkwgc3RyaW5nXHJcbiAgICAgKi9cclxuICAgIGdldFJlbmRpdGlvblVybChub2RlSWQ6IHN0cmluZywgZW5jb2Rpbmc6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBpU2VydmljZS5jb250ZW50QXBpLmdldFJlbmRpdGlvblVybChub2RlSWQsIGVuY29kaW5nKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgaW5mb3JtYXRpb24gYWJvdXQgYSByZW5kaXRpb24gb2YgYSBub2RlLlxyXG4gICAgICogQHBhcmFtIG5vZGVJZCBJRCBvZiB0aGUgdGFyZ2V0IG5vZGVcclxuICAgICAqIEBwYXJhbSBlbmNvZGluZyBOYW1lIG9mIHRoZSByZW5kaXRpb24gZW5jb2RpbmdcclxuICAgICAqIEByZXR1cm5zIEluZm9ybWF0aW9uIG9iamVjdCBhYm91dCB0aGUgcmVuZGl0aW9uXHJcbiAgICAgKi9cclxuICAgIGdldFJlbmRpdGlvbihub2RlSWQ6IHN0cmluZywgZW5jb2Rpbmc6IHN0cmluZyk6IE9ic2VydmFibGU8UmVuZGl0aW9uRW50cnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UucmVuZGl0aW9uc0FwaS5nZXRSZW5kaXRpb24obm9kZUlkLCBlbmNvZGluZykpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhIGxpc3Qgb2YgYWxsIHJlbmRpdGlvbnMgZm9yIGEgbm9kZS5cclxuICAgICAqIEBwYXJhbSBub2RlSWQgSUQgb2YgdGhlIHRhcmdldCBub2RlXHJcbiAgICAgKiBAcmV0dXJucyBQYWdlZCBsaXN0IG9mIHJlbmRpdGlvbiBkZXRhaWxzXHJcbiAgICAgKi9cclxuICAgIGdldFJlbmRpdGlvbnNMaXN0QnlOb2RlSWQobm9kZUlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPFJlbmRpdGlvblBhZ2luZz4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5yZW5kaXRpb25zQXBpLmdldFJlbmRpdGlvbnMobm9kZUlkKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDcmVhdGVzIGEgcmVuZGl0aW9uIGZvciBhIG5vZGUuXHJcbiAgICAgKiBAcGFyYW0gbm9kZUlkIElEIG9mIHRoZSB0YXJnZXQgbm9kZVxyXG4gICAgICogQHBhcmFtIGVuY29kaW5nIE5hbWUgb2YgdGhlIHJlbmRpdGlvbiBlbmNvZGluZ1xyXG4gICAgICogQHJldHVybnMgTnVsbCByZXNwb25zZSB0byBpbmRpY2F0ZSBjb21wbGV0aW9uXHJcbiAgICAgKi9cclxuICAgIGNyZWF0ZVJlbmRpdGlvbihub2RlSWQ6IHN0cmluZywgZW5jb2Rpbmc6IHN0cmluZyk6IE9ic2VydmFibGU8e30+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UucmVuZGl0aW9uc0FwaS5jcmVhdGVSZW5kaXRpb24obm9kZUlkLCB7IGlkOiBlbmNvZGluZyB9KSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXBlYXRlZGx5IGF0dGVtcHRzIHRvIGNyZWF0ZSBhIHJlbmRpdGlvbiwgdGhyb3VnaCB0byBzdWNjZXNzIG9yIGZhaWx1cmUuXHJcbiAgICAgKiBAcGFyYW0gbm9kZUlkIElEIG9mIHRoZSB0YXJnZXQgbm9kZVxyXG4gICAgICogQHBhcmFtIGVuY29kaW5nIE5hbWUgb2YgdGhlIHJlbmRpdGlvbiBlbmNvZGluZ1xyXG4gICAgICogQHBhcmFtIHBvbGxpbmdJbnRlcnZhbCBUaW1lIGludGVydmFsIChpbiBtaWxsaXNlY29uZHMpIGJldHdlZW4gY2hlY2tzIGZvciBjb21wbGV0aW9uXHJcbiAgICAgKiBAcGFyYW0gcmV0cmllcyBOdW1iZXIgb2YgYXR0ZW1wdHMgdG8gbWFrZSBiZWZvcmUgZGVjbGFyaW5nIGZhaWx1cmVcclxuICAgICAqIEByZXR1cm5zIFRydWUgaWYgdGhlIHJlbmRpdGlvbiB3YXMgY3JlYXRlZCwgZmFsc2Ugb3RoZXJ3aXNlXHJcbiAgICAgKi9cclxuICAgIGNvbnZlcnQobm9kZUlkOiBzdHJpbmcsIGVuY29kaW5nOiBzdHJpbmcsIHBvbGxpbmdJbnRlcnZhbDogbnVtYmVyID0gMTAwMCwgcmV0cmllczogbnVtYmVyID0gNSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNyZWF0ZVJlbmRpdGlvbihub2RlSWQsIGVuY29kaW5nKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIGNvbmNhdE1hcCgoKSA9PiB0aGlzLnBvbGxSZW5kaXRpb24obm9kZUlkLCBlbmNvZGluZywgcG9sbGluZ0ludGVydmFsLCByZXRyaWVzKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHBvbGxSZW5kaXRpb24obm9kZUlkOiBzdHJpbmcsIGVuY29kaW5nOiBzdHJpbmcsIGludGVydmFsU2l6ZTogbnVtYmVyID0gMTAwMCwgcmV0cmllczogbnVtYmVyID0gNSkge1xyXG4gICAgICAgIGxldCBhdHRlbXB0cyA9IDA7XHJcbiAgICAgICAgcmV0dXJuIGludGVydmFsKGludGVydmFsU2l6ZSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBzd2l0Y2hNYXAoKCkgPT4gdGhpcy5nZXRSZW5kaXRpb24obm9kZUlkLCBlbmNvZGluZykpLFxyXG4gICAgICAgICAgICAgICAgdGFrZVdoaWxlKChyZW5kaXRpb25FbnRyeTogUmVuZGl0aW9uRW50cnkpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBhdHRlbXB0cyArPSAxO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChhdHRlbXB0cyA+IHJldHJpZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gKHJlbmRpdGlvbkVudHJ5LmVudHJ5LnN0YXR1cy50b1N0cmluZygpICE9PSAnQ1JFQVRFRCcpO1xyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=