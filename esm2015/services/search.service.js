/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { Subject, from, throwError } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { SearchConfigurationService } from './search-configuration.service';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./search-configuration.service";
export class SearchService {
    /**
     * @param {?} apiService
     * @param {?} searchConfigurationService
     */
    constructor(apiService, searchConfigurationService) {
        this.apiService = apiService;
        this.searchConfigurationService = searchConfigurationService;
        this.dataLoaded = new Subject();
    }
    /**
     * Gets a list of nodes that match the given search criteria.
     * @param {?} term Term to search for
     * @param {?=} options Options for delivery of the search results
     * @return {?} List of nodes resulting from the search
     */
    getNodeQueryResults(term, options) {
        /** @type {?} */
        const promise = this.apiService.getInstance().core.queriesApi.findNodes(term, options);
        promise.then((/**
         * @param {?} nodePaging
         * @return {?}
         */
        (nodePaging) => {
            this.dataLoaded.next(nodePaging);
        })).catch((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err)));
        return from(promise);
    }
    /**
     * Performs a search.
     * @param {?} searchTerm Term to search for
     * @param {?} maxResults Maximum number of items in the list of results
     * @param {?} skipCount Number of higher-ranked items to skip over in the list
     * @return {?} List of search results
     */
    search(searchTerm, maxResults, skipCount) {
        /** @type {?} */
        const searchQuery = Object.assign(this.searchConfigurationService.generateQueryBody(searchTerm, maxResults, skipCount));
        /** @type {?} */
        const promise = this.apiService.getInstance().search.searchApi.search(searchQuery);
        promise.then((/**
         * @param {?} nodePaging
         * @return {?}
         */
        (nodePaging) => {
            this.dataLoaded.next(nodePaging);
        })).catch((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err)));
        return from(promise);
    }
    /**
     * Performs a search with its parameters supplied by a QueryBody object.
     * @param {?} queryBody Object containing the search parameters
     * @return {?} List of search results
     */
    searchByQueryBody(queryBody) {
        /** @type {?} */
        const promise = this.apiService.getInstance().search.searchApi.search(queryBody);
        promise.then((/**
         * @param {?} nodePaging
         * @return {?}
         */
        (nodePaging) => {
            this.dataLoaded.next(nodePaging);
        })).catch((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err)));
        return from(promise);
    }
    /**
     * @private
     * @param {?} error
     * @return {?}
     */
    handleError(error) {
        return throwError(error || 'Server error');
    }
}
SearchService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SearchService.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: SearchConfigurationService }
];
/** @nocollapse */ SearchService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function SearchService_Factory() { return new SearchService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.SearchConfigurationService)); }, token: SearchService, providedIn: "root" });
if (false) {
    /** @type {?} */
    SearchService.prototype.dataLoaded;
    /**
     * @type {?}
     * @private
     */
    SearchService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    SearchService.prototype.searchConfigurationService;
}
/**
 * @record
 */
export function SearchOptions() { }
if (false) {
    /**
     * The number of entities that exist in the collection before those included in this list.
     * @type {?|undefined}
     */
    SearchOptions.prototype.skipCount;
    /**
     * The maximum number of items to return in the list.
     * @type {?|undefined}
     */
    SearchOptions.prototype.maxItems;
    /**
     * The id of the node to start the search from. Supports the aliases -my-, -root- and -shared-.
     * @type {?|undefined}
     */
    SearchOptions.prototype.rootNodeId;
    /**
     * Restrict the returned results to only those of the given node type and its sub-types.
     * @type {?|undefined}
     */
    SearchOptions.prototype.nodeType;
    /**
     * Return additional information about the node. The available optional fields are:
     * `allowableOperations`, `aspectNames`, `isLink`, `isLocked`, `path` and `properties`.
     * @type {?|undefined}
     */
    SearchOptions.prototype.include;
    /**
     * String to control the order of the entities returned in a list. You can use this
     * parameter to sort the list by one or more fields. Each field has a default sort order,
     * which is normally ascending order (but see the JS-API docs to check if any fields used
     * in a method have a descending default search order). To sort the entities in a specific
     * order, you can use the "ASC" and "DESC" keywords for any field.
     * @type {?|undefined}
     */
    SearchOptions.prototype.orderBy;
    /**
     * List of field names. You can use this parameter to restrict the fields returned within
     * a response if, for example, you want to save on overall bandwidth. The list applies to a
     * returned individual entity or entries within a collection. If the API method also supports
     * the `include` parameter, then the fields specified in the include parameter are returned in
     * addition to those specified in the fields parameter.
     * @type {?|undefined}
     */
    SearchOptions.prototype.fields;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9zZWFyY2guc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTNDLE9BQU8sRUFBYyxPQUFPLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUM3RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQzs7OztBQUs1RSxNQUFNLE9BQU8sYUFBYTs7Ozs7SUFJdEIsWUFBb0IsVUFBOEIsRUFDOUIsMEJBQXNEO1FBRHRELGVBQVUsR0FBVixVQUFVLENBQW9CO1FBQzlCLCtCQUEwQixHQUExQiwwQkFBMEIsQ0FBNEI7UUFIMUUsZUFBVSxHQUF3QixJQUFJLE9BQU8sRUFBRSxDQUFDO0lBSWhELENBQUM7Ozs7Ozs7SUFRRCxtQkFBbUIsQ0FBQyxJQUFZLEVBQUUsT0FBdUI7O2NBQy9DLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUM7UUFFdEYsT0FBTyxDQUFDLElBQUk7Ozs7UUFBQyxDQUFDLFVBQXNCLEVBQUUsRUFBRTtZQUNwQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNyQyxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FBQztRQUV6QyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN6QixDQUFDOzs7Ozs7OztJQVNELE1BQU0sQ0FBQyxVQUFrQixFQUFFLFVBQWtCLEVBQUUsU0FBaUI7O2NBQ3RELFdBQVcsR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLEVBQUUsVUFBVSxFQUFFLFNBQVMsQ0FBQyxDQUFDOztjQUNqSCxPQUFPLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxXQUFXLENBQUM7UUFFbEYsT0FBTyxDQUFDLElBQUk7Ozs7UUFBQyxDQUFDLFVBQXNCLEVBQUUsRUFBRTtZQUNwQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNyQyxDQUFDLEVBQUMsQ0FBQyxLQUFLOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FBQztRQUV6QyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN6QixDQUFDOzs7Ozs7SUFPRCxpQkFBaUIsQ0FBQyxTQUFvQjs7Y0FDNUIsT0FBTyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDO1FBRWhGLE9BQU8sQ0FBQyxJQUFJOzs7O1FBQUMsQ0FBQyxVQUFzQixFQUFFLEVBQUU7WUFDcEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDckMsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQUM7UUFFekMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDekIsQ0FBQzs7Ozs7O0lBRU8sV0FBVyxDQUFDLEtBQVU7UUFDMUIsT0FBTyxVQUFVLENBQUMsS0FBSyxJQUFJLGNBQWMsQ0FBQyxDQUFDO0lBQy9DLENBQUM7OztZQTlESixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFMUSxrQkFBa0I7WUFDbEIsMEJBQTBCOzs7OztJQU8vQixtQ0FBZ0Q7Ozs7O0lBRXBDLG1DQUFzQzs7Ozs7SUFDdEMsbURBQThEOzs7OztBQXlEOUUsbUNBb0NDOzs7Ozs7SUFsQ0csa0NBQW1COzs7OztJQUduQixpQ0FBa0I7Ozs7O0lBR2xCLG1DQUFvQjs7Ozs7SUFHcEIsaUNBQWtCOzs7Ozs7SUFNbEIsZ0NBQW1COzs7Ozs7Ozs7SUFTbkIsZ0NBQWlCOzs7Ozs7Ozs7SUFTakIsK0JBQWtCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTm9kZVBhZ2luZywgUXVlcnlCb2R5IH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIFN1YmplY3QsIGZyb20sIHRocm93RXJyb3IgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IFNlYXJjaENvbmZpZ3VyYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi9zZWFyY2gtY29uZmlndXJhdGlvbi5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgU2VhcmNoU2VydmljZSB7XHJcblxyXG4gICAgZGF0YUxvYWRlZDogU3ViamVjdDxOb2RlUGFnaW5nPiA9IG5ldyBTdWJqZWN0KCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHNlYXJjaENvbmZpZ3VyYXRpb25TZXJ2aWNlOiBTZWFyY2hDb25maWd1cmF0aW9uU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhIGxpc3Qgb2Ygbm9kZXMgdGhhdCBtYXRjaCB0aGUgZ2l2ZW4gc2VhcmNoIGNyaXRlcmlhLlxyXG4gICAgICogQHBhcmFtIHRlcm0gVGVybSB0byBzZWFyY2ggZm9yXHJcbiAgICAgKiBAcGFyYW0gb3B0aW9ucyBPcHRpb25zIGZvciBkZWxpdmVyeSBvZiB0aGUgc2VhcmNoIHJlc3VsdHNcclxuICAgICAqIEByZXR1cm5zIExpc3Qgb2Ygbm9kZXMgcmVzdWx0aW5nIGZyb20gdGhlIHNlYXJjaFxyXG4gICAgICovXHJcbiAgICBnZXROb2RlUXVlcnlSZXN1bHRzKHRlcm06IHN0cmluZywgb3B0aW9ucz86IFNlYXJjaE9wdGlvbnMpOiBPYnNlcnZhYmxlPE5vZGVQYWdpbmc+IHtcclxuICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuY29yZS5xdWVyaWVzQXBpLmZpbmROb2Rlcyh0ZXJtLCBvcHRpb25zKTtcclxuXHJcbiAgICAgICAgcHJvbWlzZS50aGVuKChub2RlUGFnaW5nOiBOb2RlUGFnaW5nKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZGF0YUxvYWRlZC5uZXh0KG5vZGVQYWdpbmcpO1xyXG4gICAgICAgIH0pLmNhdGNoKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSk7XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHByb21pc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUGVyZm9ybXMgYSBzZWFyY2guXHJcbiAgICAgKiBAcGFyYW0gc2VhcmNoVGVybSBUZXJtIHRvIHNlYXJjaCBmb3JcclxuICAgICAqIEBwYXJhbSBtYXhSZXN1bHRzIE1heGltdW0gbnVtYmVyIG9mIGl0ZW1zIGluIHRoZSBsaXN0IG9mIHJlc3VsdHNcclxuICAgICAqIEBwYXJhbSBza2lwQ291bnQgTnVtYmVyIG9mIGhpZ2hlci1yYW5rZWQgaXRlbXMgdG8gc2tpcCBvdmVyIGluIHRoZSBsaXN0XHJcbiAgICAgKiBAcmV0dXJucyBMaXN0IG9mIHNlYXJjaCByZXN1bHRzXHJcbiAgICAgKi9cclxuICAgIHNlYXJjaChzZWFyY2hUZXJtOiBzdHJpbmcsIG1heFJlc3VsdHM6IG51bWJlciwgc2tpcENvdW50OiBudW1iZXIpOiBPYnNlcnZhYmxlPE5vZGVQYWdpbmc+IHtcclxuICAgICAgICBjb25zdCBzZWFyY2hRdWVyeSA9IE9iamVjdC5hc3NpZ24odGhpcy5zZWFyY2hDb25maWd1cmF0aW9uU2VydmljZS5nZW5lcmF0ZVF1ZXJ5Qm9keShzZWFyY2hUZXJtLCBtYXhSZXN1bHRzLCBza2lwQ291bnQpKTtcclxuICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuc2VhcmNoLnNlYXJjaEFwaS5zZWFyY2goc2VhcmNoUXVlcnkpO1xyXG5cclxuICAgICAgICBwcm9taXNlLnRoZW4oKG5vZGVQYWdpbmc6IE5vZGVQYWdpbmcpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5kYXRhTG9hZGVkLm5leHQobm9kZVBhZ2luZyk7XHJcbiAgICAgICAgfSkuY2F0Y2goKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20ocHJvbWlzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBQZXJmb3JtcyBhIHNlYXJjaCB3aXRoIGl0cyBwYXJhbWV0ZXJzIHN1cHBsaWVkIGJ5IGEgUXVlcnlCb2R5IG9iamVjdC5cclxuICAgICAqIEBwYXJhbSBxdWVyeUJvZHkgT2JqZWN0IGNvbnRhaW5pbmcgdGhlIHNlYXJjaCBwYXJhbWV0ZXJzXHJcbiAgICAgKiBAcmV0dXJucyBMaXN0IG9mIHNlYXJjaCByZXN1bHRzXHJcbiAgICAgKi9cclxuICAgIHNlYXJjaEJ5UXVlcnlCb2R5KHF1ZXJ5Qm9keTogUXVlcnlCb2R5KTogT2JzZXJ2YWJsZTxOb2RlUGFnaW5nPiB7XHJcbiAgICAgICAgY29uc3QgcHJvbWlzZSA9IHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLnNlYXJjaC5zZWFyY2hBcGkuc2VhcmNoKHF1ZXJ5Qm9keSk7XHJcblxyXG4gICAgICAgIHByb21pc2UudGhlbigobm9kZVBhZ2luZzogTm9kZVBhZ2luZykgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmRhdGFMb2FkZWQubmV4dChub2RlUGFnaW5nKTtcclxuICAgICAgICB9KS5jYXRjaCgoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpO1xyXG5cclxuICAgICAgICByZXR1cm4gZnJvbShwcm9taXNlKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGhhbmRsZUVycm9yKGVycm9yOiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiB0aHJvd0Vycm9yKGVycm9yIHx8ICdTZXJ2ZXIgZXJyb3InKTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBTZWFyY2hPcHRpb25zIHtcclxuICAgIC8qKiBUaGUgbnVtYmVyIG9mIGVudGl0aWVzIHRoYXQgZXhpc3QgaW4gdGhlIGNvbGxlY3Rpb24gYmVmb3JlIHRob3NlIGluY2x1ZGVkIGluIHRoaXMgbGlzdC4gKi9cclxuICAgIHNraXBDb3VudD86IG51bWJlcjtcclxuXHJcbiAgICAvKiogVGhlIG1heGltdW0gbnVtYmVyIG9mIGl0ZW1zIHRvIHJldHVybiBpbiB0aGUgbGlzdC4gKi9cclxuICAgIG1heEl0ZW1zPzogbnVtYmVyO1xyXG5cclxuICAgIC8qKiBUaGUgaWQgb2YgdGhlIG5vZGUgdG8gc3RhcnQgdGhlIHNlYXJjaCBmcm9tLiBTdXBwb3J0cyB0aGUgYWxpYXNlcyAtbXktLCAtcm9vdC0gYW5kIC1zaGFyZWQtLiAqL1xyXG4gICAgcm9vdE5vZGVJZD86IHN0cmluZztcclxuXHJcbiAgICAvKiogUmVzdHJpY3QgdGhlIHJldHVybmVkIHJlc3VsdHMgdG8gb25seSB0aG9zZSBvZiB0aGUgZ2l2ZW4gbm9kZSB0eXBlIGFuZCBpdHMgc3ViLXR5cGVzLiAqL1xyXG4gICAgbm9kZVR5cGU/OiBzdHJpbmc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm4gYWRkaXRpb25hbCBpbmZvcm1hdGlvbiBhYm91dCB0aGUgbm9kZS4gVGhlIGF2YWlsYWJsZSBvcHRpb25hbCBmaWVsZHMgYXJlOlxyXG4gICAgICogYGFsbG93YWJsZU9wZXJhdGlvbnNgLCBgYXNwZWN0TmFtZXNgLCBgaXNMaW5rYCwgYGlzTG9ja2VkYCwgYHBhdGhgIGFuZCBgcHJvcGVydGllc2AuXHJcbiAgICAgKi9cclxuICAgIGluY2x1ZGU/OiBzdHJpbmdbXTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIFN0cmluZyB0byBjb250cm9sIHRoZSBvcmRlciBvZiB0aGUgZW50aXRpZXMgcmV0dXJuZWQgaW4gYSBsaXN0LiBZb3UgY2FuIHVzZSB0aGlzXHJcbiAgICAgKiBwYXJhbWV0ZXIgdG8gc29ydCB0aGUgbGlzdCBieSBvbmUgb3IgbW9yZSBmaWVsZHMuIEVhY2ggZmllbGQgaGFzIGEgZGVmYXVsdCBzb3J0IG9yZGVyLFxyXG4gICAgICogd2hpY2ggaXMgbm9ybWFsbHkgYXNjZW5kaW5nIG9yZGVyIChidXQgc2VlIHRoZSBKUy1BUEkgZG9jcyB0byBjaGVjayBpZiBhbnkgZmllbGRzIHVzZWRcclxuICAgICAqIGluIGEgbWV0aG9kIGhhdmUgYSBkZXNjZW5kaW5nIGRlZmF1bHQgc2VhcmNoIG9yZGVyKS4gVG8gc29ydCB0aGUgZW50aXRpZXMgaW4gYSBzcGVjaWZpY1xyXG4gICAgICogb3JkZXIsIHlvdSBjYW4gdXNlIHRoZSBcIkFTQ1wiIGFuZCBcIkRFU0NcIiBrZXl3b3JkcyBmb3IgYW55IGZpZWxkLlxyXG4gICAgICovXHJcbiAgICBvcmRlckJ5Pzogc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogTGlzdCBvZiBmaWVsZCBuYW1lcy4gWW91IGNhbiB1c2UgdGhpcyBwYXJhbWV0ZXIgdG8gcmVzdHJpY3QgdGhlIGZpZWxkcyByZXR1cm5lZCB3aXRoaW5cclxuICAgICAqIGEgcmVzcG9uc2UgaWYsIGZvciBleGFtcGxlLCB5b3Ugd2FudCB0byBzYXZlIG9uIG92ZXJhbGwgYmFuZHdpZHRoLiBUaGUgbGlzdCBhcHBsaWVzIHRvIGFcclxuICAgICAqIHJldHVybmVkIGluZGl2aWR1YWwgZW50aXR5IG9yIGVudHJpZXMgd2l0aGluIGEgY29sbGVjdGlvbi4gSWYgdGhlIEFQSSBtZXRob2QgYWxzbyBzdXBwb3J0c1xyXG4gICAgICogdGhlIGBpbmNsdWRlYCBwYXJhbWV0ZXIsIHRoZW4gdGhlIGZpZWxkcyBzcGVjaWZpZWQgaW4gdGhlIGluY2x1ZGUgcGFyYW1ldGVyIGFyZSByZXR1cm5lZCBpblxyXG4gICAgICogYWRkaXRpb24gdG8gdGhvc2Ugc3BlY2lmaWVkIGluIHRoZSBmaWVsZHMgcGFyYW1ldGVyLlxyXG4gICAgICovXHJcbiAgICBmaWVsZHM/OiBzdHJpbmdbXTtcclxufVxyXG4iXX0=