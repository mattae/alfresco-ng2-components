/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, of, Subject } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { UserPreferencesService } from './user-preferences.service';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./user-preferences.service";
export class SharedLinksApiService {
    /**
     * @param {?} apiService
     * @param {?} preferences
     */
    constructor(apiService, preferences) {
        this.apiService = apiService;
        this.preferences = preferences;
        this.error = new Subject();
    }
    /**
     * @private
     * @return {?}
     */
    get sharedLinksApi() {
        return this.apiService.getInstance().core.sharedlinksApi;
    }
    /**
     * Gets shared links available to the current user.
     * @param {?=} options Options supported by JS-API
     * @return {?} List of shared links
     */
    getSharedLinks(options = {}) {
        /** @type {?} */
        const defaultOptions = {
            maxItems: this.preferences.paginationSize,
            skipCount: 0,
            include: ['properties', 'allowableOperations']
        };
        /** @type {?} */
        const queryOptions = Object.assign({}, defaultOptions, options);
        /** @type {?} */
        const promise = this.sharedLinksApi.findSharedLinks(queryOptions);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => of(err))));
    }
    /**
     * Creates a shared link available to the current user.
     * @param {?} nodeId ID of the node to link to
     * @param {?=} options Options supported by JS-API
     * @return {?} The shared link just created
     */
    createSharedLinks(nodeId, options = {}) {
        /** @type {?} */
        const promise = this.sharedLinksApi.addSharedLink({ nodeId: nodeId });
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => of(err))));
    }
    /**
     * Deletes a shared link.
     * @param {?} sharedId ID of the link to delete
     * @return {?} Null response notifying when the operation is complete
     */
    deleteSharedLink(sharedId) {
        /** @type {?} */
        const promise = this.sharedLinksApi.deleteSharedLink(sharedId);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => of(err))));
    }
}
SharedLinksApiService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SharedLinksApiService.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: UserPreferencesService }
];
/** @nocollapse */ SharedLinksApiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function SharedLinksApiService_Factory() { return new SharedLinksApiService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.UserPreferencesService)); }, token: SharedLinksApiService, providedIn: "root" });
if (false) {
    /** @type {?} */
    SharedLinksApiService.prototype.error;
    /**
     * @type {?}
     * @private
     */
    SharedLinksApiService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    SharedLinksApiService.prototype.preferences;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLWxpbmtzLWFwaS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvc2hhcmVkLWxpbmtzLWFwaS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFjLElBQUksRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3JELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7OztBQUs1QyxNQUFNLE9BQU8scUJBQXFCOzs7OztJQUk5QixZQUFvQixVQUE4QixFQUM5QixXQUFtQztRQURuQyxlQUFVLEdBQVYsVUFBVSxDQUFvQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBd0I7UUFIdkQsVUFBSyxHQUFHLElBQUksT0FBTyxFQUEyQyxDQUFDO0lBSS9ELENBQUM7Ozs7O0lBRUQsSUFBWSxjQUFjO1FBQ3RCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDO0lBQzdELENBQUM7Ozs7OztJQU9ELGNBQWMsQ0FBQyxVQUFlLEVBQUU7O2NBQ3RCLGNBQWMsR0FBRztZQUNuQixRQUFRLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjO1lBQ3pDLFNBQVMsRUFBRSxDQUFDO1lBQ1osT0FBTyxFQUFFLENBQUMsWUFBWSxFQUFFLHFCQUFxQixDQUFDO1NBQ2pEOztjQUNLLFlBQVksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxjQUFjLEVBQUUsT0FBTyxDQUFDOztjQUN6RCxPQUFPLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDO1FBRWpFLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FDckIsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQUMsQ0FDL0IsQ0FBQztJQUNOLENBQUM7Ozs7Ozs7SUFRRCxpQkFBaUIsQ0FBQyxNQUFjLEVBQUUsVUFBZSxFQUFFOztjQUN6QyxPQUFPLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLENBQUM7UUFFckUsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUNyQixVQUFVOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUMvQixDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBT0QsZ0JBQWdCLENBQUMsUUFBZ0I7O2NBQ3ZCLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQztRQUU5RCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQ3JCLFVBQVU7Ozs7UUFBQyxDQUFDLEdBQVUsRUFBRSxFQUFFLENBQUMsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQ3RDLENBQUM7SUFDTixDQUFDOzs7WUEzREosVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBTlEsa0JBQWtCO1lBQ2xCLHNCQUFzQjs7Ozs7SUFRM0Isc0NBQStEOzs7OztJQUVuRCwyQ0FBc0M7Ozs7O0lBQ3RDLDRDQUEyQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE5vZGVQYWdpbmcsIFNoYXJlZExpbmtFbnRyeSB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmcm9tLCBvZiwgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXNlclByZWZlcmVuY2VzU2VydmljZSB9IGZyb20gJy4vdXNlci1wcmVmZXJlbmNlcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgY2F0Y2hFcnJvciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgU2hhcmVkTGlua3NBcGlTZXJ2aWNlIHtcclxuXHJcbiAgICBlcnJvciA9IG5ldyBTdWJqZWN0PHsgc3RhdHVzQ29kZTogbnVtYmVyLCBtZXNzYWdlOiBzdHJpbmcgfT4oKTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgcHJlZmVyZW5jZXM6IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldCBzaGFyZWRMaW5rc0FwaSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuY29yZS5zaGFyZWRsaW5rc0FwaTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgc2hhcmVkIGxpbmtzIGF2YWlsYWJsZSB0byB0aGUgY3VycmVudCB1c2VyLlxyXG4gICAgICogQHBhcmFtIG9wdGlvbnMgT3B0aW9ucyBzdXBwb3J0ZWQgYnkgSlMtQVBJXHJcbiAgICAgKiBAcmV0dXJucyBMaXN0IG9mIHNoYXJlZCBsaW5rc1xyXG4gICAgICovXHJcbiAgICBnZXRTaGFyZWRMaW5rcyhvcHRpb25zOiBhbnkgPSB7fSk6IE9ic2VydmFibGU8Tm9kZVBhZ2luZz4ge1xyXG4gICAgICAgIGNvbnN0IGRlZmF1bHRPcHRpb25zID0ge1xyXG4gICAgICAgICAgICBtYXhJdGVtczogdGhpcy5wcmVmZXJlbmNlcy5wYWdpbmF0aW9uU2l6ZSxcclxuICAgICAgICAgICAgc2tpcENvdW50OiAwLFxyXG4gICAgICAgICAgICBpbmNsdWRlOiBbJ3Byb3BlcnRpZXMnLCAnYWxsb3dhYmxlT3BlcmF0aW9ucyddXHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zdCBxdWVyeU9wdGlvbnMgPSBPYmplY3QuYXNzaWduKHt9LCBkZWZhdWx0T3B0aW9ucywgb3B0aW9ucyk7XHJcbiAgICAgICAgY29uc3QgcHJvbWlzZSA9IHRoaXMuc2hhcmVkTGlua3NBcGkuZmluZFNoYXJlZExpbmtzKHF1ZXJ5T3B0aW9ucyk7XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHByb21pc2UpLnBpcGUoXHJcbiAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gb2YoZXJyKSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBhIHNoYXJlZCBsaW5rIGF2YWlsYWJsZSB0byB0aGUgY3VycmVudCB1c2VyLlxyXG4gICAgICogQHBhcmFtIG5vZGVJZCBJRCBvZiB0aGUgbm9kZSB0byBsaW5rIHRvXHJcbiAgICAgKiBAcGFyYW0gb3B0aW9ucyBPcHRpb25zIHN1cHBvcnRlZCBieSBKUy1BUElcclxuICAgICAqIEByZXR1cm5zIFRoZSBzaGFyZWQgbGluayBqdXN0IGNyZWF0ZWRcclxuICAgICAqL1xyXG4gICAgY3JlYXRlU2hhcmVkTGlua3Mobm9kZUlkOiBzdHJpbmcsIG9wdGlvbnM6IGFueSA9IHt9KTogT2JzZXJ2YWJsZTxTaGFyZWRMaW5rRW50cnk+IHtcclxuICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5zaGFyZWRMaW5rc0FwaS5hZGRTaGFyZWRMaW5rKHsgbm9kZUlkOiBub2RlSWQgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHByb21pc2UpLnBpcGUoXHJcbiAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gb2YoZXJyKSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGVsZXRlcyBhIHNoYXJlZCBsaW5rLlxyXG4gICAgICogQHBhcmFtIHNoYXJlZElkIElEIG9mIHRoZSBsaW5rIHRvIGRlbGV0ZVxyXG4gICAgICogQHJldHVybnMgTnVsbCByZXNwb25zZSBub3RpZnlpbmcgd2hlbiB0aGUgb3BlcmF0aW9uIGlzIGNvbXBsZXRlXHJcbiAgICAgKi9cclxuICAgIGRlbGV0ZVNoYXJlZExpbmsoc2hhcmVkSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55IHwgRXJyb3I+IHtcclxuICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5zaGFyZWRMaW5rc0FwaS5kZWxldGVTaGFyZWRMaW5rKHNoYXJlZElkKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20ocHJvbWlzZSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyOiBFcnJvcikgPT4gb2YoZXJyKSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==