/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
export class SitesService {
    /**
     * @param {?} apiService
     */
    constructor(apiService) {
        this.apiService = apiService;
    }
    /**
     * Gets a list of all sites in the repository.
     * @param {?=} opts Options supported by JS-API
     * @return {?} List of sites
     */
    getSites(opts = {}) {
        /** @type {?} */
        const defaultOptions = {
            skipCount: 0,
            include: ['properties']
        };
        /** @type {?} */
        const queryOptions = Object.assign({}, defaultOptions, opts);
        return from(this.apiService.getInstance().core.sitesApi.getSites(queryOptions))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets the details for a site.
     * @param {?} siteId ID of the target site
     * @param {?=} opts Options supported by JS-API
     * @return {?} Information about the site
     */
    getSite(siteId, opts) {
        return from(this.apiService.getInstance().core.sitesApi.getSite(siteId, opts))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Deletes a site.
     * @param {?} siteId Site to delete
     * @param {?=} permanentFlag True: deletion is permanent; False: site is moved to the trash
     * @return {?} Null response notifying when the operation is complete
     */
    deleteSite(siteId, permanentFlag = true) {
        /** @type {?} */
        const options = {};
        options.permanent = permanentFlag;
        return from(this.apiService.getInstance().core.sitesApi.deleteSite(siteId, options))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets a site's content.
     * @param {?} siteId ID of the target site
     * @return {?} Site content
     */
    getSiteContent(siteId) {
        return this.getSite(siteId, { relations: ['containers'] });
    }
    /**
     * Gets a list of all a site's members.
     * @param {?} siteId ID of the target site
     * @return {?} Site members
     */
    getSiteMembers(siteId) {
        return this.getSite(siteId, { relations: ['members'] });
    }
    /**
     * Gets the username of the user currently logged into ACS.
     * @return {?} Username string
     */
    getEcmCurrentLoggedUserName() {
        return this.apiService.getInstance().getEcmUsername();
    }
    /**
     * @private
     * @param {?} error
     * @return {?}
     */
    handleError(error) {
        console.error(error);
        return throwError(error || 'Server error');
    }
}
SitesService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
SitesService.ctorParameters = () => [
    { type: AlfrescoApiService }
];
/** @nocollapse */ SitesService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function SitesService_Factory() { return new SitesService(i0.ɵɵinject(i1.AlfrescoApiService)); }, token: SitesService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    SitesService.prototype.apiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2l0ZXMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL3NpdGVzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQWMsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNwRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUU1RCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7OztBQUs1QyxNQUFNLE9BQU8sWUFBWTs7OztJQUVyQixZQUNZLFVBQThCO1FBQTlCLGVBQVUsR0FBVixVQUFVLENBQW9CO0lBQzFDLENBQUM7Ozs7OztJQU9ELFFBQVEsQ0FBQyxPQUFZLEVBQUU7O2NBQ2IsY0FBYyxHQUFHO1lBQ25CLFNBQVMsRUFBRSxDQUFDO1lBQ1osT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO1NBQzFCOztjQUNLLFlBQVksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxjQUFjLEVBQUUsSUFBSSxDQUFDO1FBQzVELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDMUUsSUFBSSxDQUNELFVBQVU7Ozs7UUFBQyxDQUFDLEdBQVEsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUNsRCxDQUFDO0lBQ1YsQ0FBQzs7Ozs7OztJQVFELE9BQU8sQ0FBQyxNQUFjLEVBQUUsSUFBVTtRQUM5QixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQzthQUN6RSxJQUFJLENBQ0QsVUFBVTs7OztRQUFDLENBQUMsR0FBUSxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQ2xELENBQUM7SUFDVixDQUFDOzs7Ozs7O0lBUUQsVUFBVSxDQUFDLE1BQWMsRUFBRSxnQkFBeUIsSUFBSTs7Y0FDOUMsT0FBTyxHQUFRLEVBQUU7UUFDdkIsT0FBTyxDQUFDLFNBQVMsR0FBRyxhQUFhLENBQUM7UUFDbEMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7YUFDL0UsSUFBSSxDQUNELFVBQVU7Ozs7UUFBQyxDQUFDLEdBQVEsRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBQyxDQUNsRCxDQUFDO0lBQ1YsQ0FBQzs7Ozs7O0lBT0QsY0FBYyxDQUFDLE1BQWM7UUFDekIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLFNBQVMsRUFBRSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUMvRCxDQUFDOzs7Ozs7SUFPRCxjQUFjLENBQUMsTUFBYztRQUN6QixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxFQUFFLEVBQUUsU0FBUyxFQUFFLENBQUMsU0FBUyxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQzVELENBQUM7Ozs7O0lBTUQsMkJBQTJCO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUMxRCxDQUFDOzs7Ozs7SUFFTyxXQUFXLENBQUMsS0FBVTtRQUMxQixPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3JCLE9BQU8sVUFBVSxDQUFDLEtBQUssSUFBSSxjQUFjLENBQUMsQ0FBQztJQUMvQyxDQUFDOzs7WUFuRkosVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBTlEsa0JBQWtCOzs7Ozs7OztJQVVuQixrQ0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmcm9tLCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4vYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTaXRlUGFnaW5nLCBTaXRlRW50cnkgfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuaW1wb3J0IHsgY2F0Y2hFcnJvciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgU2l0ZXNTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhIGxpc3Qgb2YgYWxsIHNpdGVzIGluIHRoZSByZXBvc2l0b3J5LlxyXG4gICAgICogQHBhcmFtIG9wdHMgT3B0aW9ucyBzdXBwb3J0ZWQgYnkgSlMtQVBJXHJcbiAgICAgKiBAcmV0dXJucyBMaXN0IG9mIHNpdGVzXHJcbiAgICAgKi9cclxuICAgIGdldFNpdGVzKG9wdHM6IGFueSA9IHt9KTogT2JzZXJ2YWJsZTxTaXRlUGFnaW5nPiB7XHJcbiAgICAgICAgY29uc3QgZGVmYXVsdE9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIHNraXBDb3VudDogMCxcclxuICAgICAgICAgICAgaW5jbHVkZTogWydwcm9wZXJ0aWVzJ11cclxuICAgICAgICB9O1xyXG4gICAgICAgIGNvbnN0IHF1ZXJ5T3B0aW9ucyA9IE9iamVjdC5hc3NpZ24oe30sIGRlZmF1bHRPcHRpb25zLCBvcHRzKTtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5jb3JlLnNpdGVzQXBpLmdldFNpdGVzKHF1ZXJ5T3B0aW9ucykpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyOiBhbnkpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIGRldGFpbHMgZm9yIGEgc2l0ZS5cclxuICAgICAqIEBwYXJhbSBzaXRlSWQgSUQgb2YgdGhlIHRhcmdldCBzaXRlXHJcbiAgICAgKiBAcGFyYW0gb3B0cyBPcHRpb25zIHN1cHBvcnRlZCBieSBKUy1BUElcclxuICAgICAqIEByZXR1cm5zIEluZm9ybWF0aW9uIGFib3V0IHRoZSBzaXRlXHJcbiAgICAgKi9cclxuICAgIGdldFNpdGUoc2l0ZUlkOiBzdHJpbmcsIG9wdHM/OiBhbnkpOiBPYnNlcnZhYmxlPFNpdGVFbnRyeSB8IHt9PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuY29yZS5zaXRlc0FwaS5nZXRTaXRlKHNpdGVJZCwgb3B0cykpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyOiBhbnkpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERlbGV0ZXMgYSBzaXRlLlxyXG4gICAgICogQHBhcmFtIHNpdGVJZCBTaXRlIHRvIGRlbGV0ZVxyXG4gICAgICogQHBhcmFtIHBlcm1hbmVudEZsYWcgVHJ1ZTogZGVsZXRpb24gaXMgcGVybWFuZW50OyBGYWxzZTogc2l0ZSBpcyBtb3ZlZCB0byB0aGUgdHJhc2hcclxuICAgICAqIEByZXR1cm5zIE51bGwgcmVzcG9uc2Ugbm90aWZ5aW5nIHdoZW4gdGhlIG9wZXJhdGlvbiBpcyBjb21wbGV0ZVxyXG4gICAgICovXHJcbiAgICBkZWxldGVTaXRlKHNpdGVJZDogc3RyaW5nLCBwZXJtYW5lbnRGbGFnOiBib29sZWFuID0gdHJ1ZSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3Qgb3B0aW9uczogYW55ID0ge307XHJcbiAgICAgICAgb3B0aW9ucy5wZXJtYW5lbnQgPSBwZXJtYW5lbnRGbGFnO1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmNvcmUuc2l0ZXNBcGkuZGVsZXRlU2l0ZShzaXRlSWQsIG9wdGlvbnMpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycjogYW55KSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgc2l0ZSdzIGNvbnRlbnQuXHJcbiAgICAgKiBAcGFyYW0gc2l0ZUlkIElEIG9mIHRoZSB0YXJnZXQgc2l0ZVxyXG4gICAgICogQHJldHVybnMgU2l0ZSBjb250ZW50XHJcbiAgICAgKi9cclxuICAgIGdldFNpdGVDb250ZW50KHNpdGVJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxTaXRlRW50cnkgfCB7fT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldFNpdGUoc2l0ZUlkLCB7IHJlbGF0aW9uczogWydjb250YWluZXJzJ10gfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgbGlzdCBvZiBhbGwgYSBzaXRlJ3MgbWVtYmVycy5cclxuICAgICAqIEBwYXJhbSBzaXRlSWQgSUQgb2YgdGhlIHRhcmdldCBzaXRlXHJcbiAgICAgKiBAcmV0dXJucyBTaXRlIG1lbWJlcnNcclxuICAgICAqL1xyXG4gICAgZ2V0U2l0ZU1lbWJlcnMoc2l0ZUlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPFNpdGVFbnRyeSB8IHt9PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0U2l0ZShzaXRlSWQsIHsgcmVsYXRpb25zOiBbJ21lbWJlcnMnXSB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIHVzZXJuYW1lIG9mIHRoZSB1c2VyIGN1cnJlbnRseSBsb2dnZWQgaW50byBBQ1MuXHJcbiAgICAgKiBAcmV0dXJucyBVc2VybmFtZSBzdHJpbmdcclxuICAgICAqL1xyXG4gICAgZ2V0RWNtQ3VycmVudExvZ2dlZFVzZXJOYW1lKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmdldEVjbVVzZXJuYW1lKCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBoYW5kbGVFcnJvcihlcnJvcjogYW55KTogYW55IHtcclxuICAgICAgICBjb25zb2xlLmVycm9yKGVycm9yKTtcclxuICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihlcnJvciB8fCAnU2VydmVyIGVycm9yJyk7XHJcbiAgICB9XHJcbn1cclxuIl19