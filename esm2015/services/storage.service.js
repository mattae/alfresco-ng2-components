/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
export class StorageService {
    constructor() {
        this.memoryStore = {};
        this.useLocalStorage = false;
        this._prefix = '';
        this.useLocalStorage = this.storageAvailable('localStorage');
    }
    /**
     * @return {?}
     */
    get prefix() {
        return this._prefix;
    }
    /**
     * @param {?} prefix
     * @return {?}
     */
    set prefix(prefix) {
        this._prefix = prefix ? prefix + '_' : '';
    }
    /**
     * Gets an item.
     * @param {?} key Key to identify the item
     * @return {?} The item (if any) retrieved by the key
     */
    getItem(key) {
        if (this.useLocalStorage) {
            return localStorage.getItem(this.prefix + key);
        }
        else {
            return this.memoryStore.hasOwnProperty(this.prefix + key) ? this.memoryStore[this.prefix + key] : null;
        }
    }
    /**
     * Stores an item
     * @param {?} key Key to identify the item
     * @param {?} data Data to store
     * @return {?}
     */
    setItem(key, data) {
        if (this.useLocalStorage) {
            localStorage.setItem(this.prefix + key, data);
        }
        else {
            this.memoryStore[this.prefix + key] = data.toString();
        }
    }
    /**
     * Removes all currently stored items.
     * @return {?}
     */
    clear() {
        if (this.useLocalStorage) {
            localStorage.clear();
        }
        else {
            this.memoryStore = {};
        }
    }
    /**
     * Removes a single item.
     * @param {?} key Key to identify the item
     * @return {?}
     */
    removeItem(key) {
        if (this.useLocalStorage) {
            localStorage.removeItem(this.prefix + key);
        }
        else {
            delete this.memoryStore[this.prefix + key];
        }
    }
    /**
     * Is any item currently stored under `key`?
     * @param {?} key Key identifying item to check
     * @return {?} True if key retrieves an item, false otherwise
     */
    hasItem(key) {
        if (this.useLocalStorage) {
            return localStorage.getItem(this.prefix + key) ? true : false;
        }
        else {
            return this.memoryStore.hasOwnProperty(key);
        }
    }
    /**
     * @private
     * @param {?} type
     * @return {?}
     */
    storageAvailable(type) {
        try {
            /** @type {?} */
            const storage = window[type];
            /** @type {?} */
            const key = '__storage_test__';
            storage.setItem(key, key);
            storage.removeItem(key, key);
            return true;
        }
        catch (e) {
            return false;
        }
    }
}
StorageService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
StorageService.ctorParameters = () => [];
/** @nocollapse */ StorageService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function StorageService_Factory() { return new StorageService(); }, token: StorageService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    StorageService.prototype.memoryStore;
    /**
     * @type {?}
     * @private
     */
    StorageService.prototype.useLocalStorage;
    /**
     * @type {?}
     * @private
     */
    StorageService.prototype._prefix;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmFnZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvc3RvcmFnZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBSzNDLE1BQU0sT0FBTyxjQUFjO0lBY3ZCO1FBWlEsZ0JBQVcsR0FBMkIsRUFBRSxDQUFDO1FBQ3pDLG9CQUFlLEdBQVksS0FBSyxDQUFDO1FBQ2pDLFlBQU8sR0FBVyxFQUFFLENBQUM7UUFXekIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDakUsQ0FBQzs7OztJQVZELElBQUksTUFBTTtRQUNOLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQztJQUN4QixDQUFDOzs7OztJQUVELElBQUksTUFBTSxDQUFDLE1BQWM7UUFDckIsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUM5QyxDQUFDOzs7Ozs7SUFXRCxPQUFPLENBQUMsR0FBVztRQUNmLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN0QixPQUFPLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsQ0FBQztTQUNsRDthQUFNO1lBQ0gsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztTQUMxRztJQUNMLENBQUM7Ozs7Ozs7SUFPRCxPQUFPLENBQUMsR0FBVyxFQUFFLElBQVk7UUFDN0IsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3RCLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDakQ7YUFBTTtZQUNILElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDekQ7SUFDTCxDQUFDOzs7OztJQUdELEtBQUs7UUFDRCxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDdEIsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ3hCO2FBQU07WUFDSCxJQUFJLENBQUMsV0FBVyxHQUFHLEVBQUUsQ0FBQztTQUN6QjtJQUNMLENBQUM7Ozs7OztJQU1ELFVBQVUsQ0FBQyxHQUFXO1FBQ2xCLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN0QixZQUFZLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUM7U0FDOUM7YUFBTTtZQUNILE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFDO1NBQzlDO0lBQ0wsQ0FBQzs7Ozs7O0lBT0QsT0FBTyxDQUFDLEdBQVc7UUFDZixJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDdEIsT0FBTyxZQUFZLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1NBQ2pFO2FBQU07WUFDSCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1NBQy9DO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sZ0JBQWdCLENBQUMsSUFBWTtRQUNqQyxJQUFJOztrQkFDTSxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQzs7a0JBQ3RCLEdBQUcsR0FBRyxrQkFBa0I7WUFDOUIsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDMUIsT0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDN0IsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsT0FBTyxLQUFLLENBQUM7U0FDaEI7SUFDTCxDQUFDOzs7WUEzRkosVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7Ozs7Ozs7O0lBR0cscUNBQWlEOzs7OztJQUNqRCx5Q0FBeUM7Ozs7O0lBQ3pDLGlDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFN0b3JhZ2VTZXJ2aWNlIHtcclxuXHJcbiAgICBwcml2YXRlIG1lbW9yeVN0b3JlOiB7IFtrZXk6IHN0cmluZ106IGFueSB9ID0ge307XHJcbiAgICBwcml2YXRlIHVzZUxvY2FsU3RvcmFnZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgcHJpdmF0ZSBfcHJlZml4OiBzdHJpbmcgPSAnJztcclxuXHJcbiAgICBnZXQgcHJlZml4KCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9wcmVmaXg7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IHByZWZpeChwcmVmaXg6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuX3ByZWZpeCA9IHByZWZpeCA/IHByZWZpeCArICdfJyA6ICcnO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMudXNlTG9jYWxTdG9yYWdlID0gdGhpcy5zdG9yYWdlQXZhaWxhYmxlKCdsb2NhbFN0b3JhZ2UnKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYW4gaXRlbS5cclxuICAgICAqIEBwYXJhbSBrZXkgS2V5IHRvIGlkZW50aWZ5IHRoZSBpdGVtXHJcbiAgICAgKiBAcmV0dXJucyBUaGUgaXRlbSAoaWYgYW55KSByZXRyaWV2ZWQgYnkgdGhlIGtleVxyXG4gICAgICovXHJcbiAgICBnZXRJdGVtKGtleTogc3RyaW5nKTogc3RyaW5nIHwgbnVsbCB7XHJcbiAgICAgICAgaWYgKHRoaXMudXNlTG9jYWxTdG9yYWdlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbSh0aGlzLnByZWZpeCArIGtleSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMubWVtb3J5U3RvcmUuaGFzT3duUHJvcGVydHkodGhpcy5wcmVmaXggKyBrZXkpID8gdGhpcy5tZW1vcnlTdG9yZVt0aGlzLnByZWZpeCArIGtleV0gOiBudWxsO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFN0b3JlcyBhbiBpdGVtXHJcbiAgICAgKiBAcGFyYW0ga2V5IEtleSB0byBpZGVudGlmeSB0aGUgaXRlbVxyXG4gICAgICogQHBhcmFtIGRhdGEgRGF0YSB0byBzdG9yZVxyXG4gICAgICovXHJcbiAgICBzZXRJdGVtKGtleTogc3RyaW5nLCBkYXRhOiBzdHJpbmcpIHtcclxuICAgICAgICBpZiAodGhpcy51c2VMb2NhbFN0b3JhZ2UpIHtcclxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLnNldEl0ZW0odGhpcy5wcmVmaXggKyBrZXksIGRhdGEpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMubWVtb3J5U3RvcmVbdGhpcy5wcmVmaXggKyBrZXldID0gZGF0YS50b1N0cmluZygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKiogUmVtb3ZlcyBhbGwgY3VycmVudGx5IHN0b3JlZCBpdGVtcy4gKi9cclxuICAgIGNsZWFyKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnVzZUxvY2FsU3RvcmFnZSkge1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2UuY2xlYXIoKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLm1lbW9yeVN0b3JlID0ge307XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVtb3ZlcyBhIHNpbmdsZSBpdGVtLlxyXG4gICAgICogQHBhcmFtIGtleSBLZXkgdG8gaWRlbnRpZnkgdGhlIGl0ZW1cclxuICAgICAqL1xyXG4gICAgcmVtb3ZlSXRlbShrZXk6IHN0cmluZykge1xyXG4gICAgICAgIGlmICh0aGlzLnVzZUxvY2FsU3RvcmFnZSkge1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2UucmVtb3ZlSXRlbSh0aGlzLnByZWZpeCArIGtleSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgZGVsZXRlIHRoaXMubWVtb3J5U3RvcmVbdGhpcy5wcmVmaXggKyBrZXldO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIElzIGFueSBpdGVtIGN1cnJlbnRseSBzdG9yZWQgdW5kZXIgYGtleWA/XHJcbiAgICAgKiBAcGFyYW0ga2V5IEtleSBpZGVudGlmeWluZyBpdGVtIHRvIGNoZWNrXHJcbiAgICAgKiBAcmV0dXJucyBUcnVlIGlmIGtleSByZXRyaWV2ZXMgYW4gaXRlbSwgZmFsc2Ugb3RoZXJ3aXNlXHJcbiAgICAgKi9cclxuICAgIGhhc0l0ZW0oa2V5OiBzdHJpbmcpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodGhpcy51c2VMb2NhbFN0b3JhZ2UpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMucHJlZml4ICsga2V5KSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5tZW1vcnlTdG9yZS5oYXNPd25Qcm9wZXJ0eShrZXkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHN0b3JhZ2VBdmFpbGFibGUodHlwZTogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgdHJ5IHtcclxuICAgICAgICAgICAgY29uc3Qgc3RvcmFnZSA9IHdpbmRvd1t0eXBlXTtcclxuICAgICAgICAgICAgY29uc3Qga2V5ID0gJ19fc3RvcmFnZV90ZXN0X18nO1xyXG4gICAgICAgICAgICBzdG9yYWdlLnNldEl0ZW0oa2V5LCBrZXkpO1xyXG4gICAgICAgICAgICBzdG9yYWdlLnJlbW92ZUl0ZW0oa2V5LCBrZXkpO1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9IGNhdGNoIChlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19