/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, forkJoin, throwError, of } from 'rxjs';
import { ComponentTranslationModel } from '../models/component.model';
import { ObjectUtils } from '../utils/object-utils';
import { map, catchError, retry } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
export class TranslateLoaderService {
    /**
     * @param {?} http
     */
    constructor(http) {
        this.http = http;
        this.prefix = 'i18n';
        this.suffix = '.json';
        this.providers = [];
        this.queue = [];
        this.defaultLang = 'en';
    }
    /**
     * @param {?} value
     * @return {?}
     */
    setDefaultLang(value) {
        this.defaultLang = value || 'en';
    }
    /**
     * @param {?} name
     * @param {?} path
     * @return {?}
     */
    registerProvider(name, path) {
        /** @type {?} */
        const registered = this.providers.find((/**
         * @param {?} provider
         * @return {?}
         */
        (provider) => provider.name === name));
        if (registered) {
            registered.path = path;
        }
        else {
            this.providers.push(new ComponentTranslationModel({ name: name, path: path }));
        }
    }
    /**
     * @param {?} name
     * @return {?}
     */
    providerRegistered(name) {
        return this.providers.find((/**
         * @param {?} x
         * @return {?}
         */
        (x) => x.name === name)) ? true : false;
    }
    /**
     * @param {?} lang
     * @param {?} component
     * @param {?=} fallbackUrl
     * @return {?}
     */
    fetchLanguageFile(lang, component, fallbackUrl) {
        /** @type {?} */
        const translationUrl = fallbackUrl || `${component.path}/${this.prefix}/${lang}${this.suffix}?v=${Date.now()}`;
        return this.http.get(translationUrl).pipe(map((/**
         * @param {?} res
         * @return {?}
         */
        (res) => {
            component.json[lang] = res;
        })), retry(3), catchError((/**
         * @return {?}
         */
        () => {
            if (!fallbackUrl && lang.includes('-')) {
                const [langId] = lang.split('-');
                if (langId && langId !== this.defaultLang) {
                    /** @type {?} */
                    const url = `${component.path}/${this.prefix}/${langId}${this.suffix}?v=${Date.now()}`;
                    return this.fetchLanguageFile(lang, component, url);
                }
            }
            return throwError(`Failed to load ${translationUrl}`);
        })));
    }
    /**
     * @param {?} lang
     * @return {?}
     */
    getComponentToFetch(lang) {
        /** @type {?} */
        const observableBatch = [];
        if (!this.queue[lang]) {
            this.queue[lang] = [];
        }
        this.providers.forEach((/**
         * @param {?} component
         * @return {?}
         */
        (component) => {
            if (!this.isComponentInQueue(lang, component.name)) {
                this.queue[lang].push(component.name);
                observableBatch.push(this.fetchLanguageFile(lang, component));
            }
        }));
        return observableBatch;
    }
    /**
     * @param {?} lang
     * @return {?}
     */
    init(lang) {
        if (this.queue[lang] === undefined) {
            this.queue[lang] = [];
        }
    }
    /**
     * @param {?} lang
     * @param {?} name
     * @return {?}
     */
    isComponentInQueue(lang, name) {
        return (this.queue[lang] || []).find((/**
         * @param {?} x
         * @return {?}
         */
        (x) => x === name)) ? true : false;
    }
    /**
     * @param {?} lang
     * @return {?}
     */
    getFullTranslationJSON(lang) {
        /** @type {?} */
        let result = {};
        this.providers
            .slice(0)
            .sort((/**
         * @param {?} a
         * @param {?} b
         * @return {?}
         */
        (a, b) => {
            if (a.name === 'app') {
                return 1;
            }
            if (b.name === 'app') {
                return -1;
            }
            return a.name.localeCompare(b.name);
        }))
            .forEach((/**
         * @param {?} model
         * @return {?}
         */
        (model) => {
            if (model.json && model.json[lang]) {
                result = ObjectUtils.merge(result, model.json[lang]);
            }
        }));
        return result;
    }
    /**
     * @param {?} lang
     * @return {?}
     */
    getTranslation(lang) {
        /** @type {?} */
        let hasFailures = false;
        /** @type {?} */
        const batch = [
            ...this.getComponentToFetch(lang).map((/**
             * @param {?} observable
             * @return {?}
             */
            (observable) => {
                return observable.pipe(catchError((/**
                 * @param {?} error
                 * @return {?}
                 */
                (error) => {
                    console.warn(error);
                    hasFailures = true;
                    return of(error);
                })));
            }))
        ];
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        (observer) => {
            if (batch.length > 0) {
                forkJoin(batch).subscribe((/**
                 * @return {?}
                 */
                () => {
                    /** @type {?} */
                    const fullTranslation = this.getFullTranslationJSON(lang);
                    if (fullTranslation) {
                        observer.next(fullTranslation);
                    }
                    if (hasFailures) {
                        observer.error('Failed to load some resources');
                    }
                    else {
                        observer.complete();
                    }
                }), (/**
                 * @param {?} err
                 * @return {?}
                 */
                (err) => {
                    observer.error('Failed to load some resources');
                }));
            }
            else {
                /** @type {?} */
                const fullTranslation = this.getFullTranslationJSON(lang);
                if (fullTranslation) {
                    observer.next(fullTranslation);
                    observer.complete();
                }
            }
        }));
    }
}
TranslateLoaderService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
TranslateLoaderService.ctorParameters = () => [
    { type: HttpClient }
];
/** @nocollapse */ TranslateLoaderService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function TranslateLoaderService_Factory() { return new TranslateLoaderService(i0.ɵɵinject(i1.HttpClient)); }, token: TranslateLoaderService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    TranslateLoaderService.prototype.prefix;
    /**
     * @type {?}
     * @private
     */
    TranslateLoaderService.prototype.suffix;
    /**
     * @type {?}
     * @private
     */
    TranslateLoaderService.prototype.providers;
    /**
     * @type {?}
     * @private
     */
    TranslateLoaderService.prototype.queue;
    /**
     * @type {?}
     * @private
     */
    TranslateLoaderService.prototype.defaultLang;
    /**
     * @type {?}
     * @private
     */
    TranslateLoaderService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhbnNsYXRlLWxvYWRlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvdHJhbnNsYXRlLWxvYWRlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNsRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDNUQsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sMkJBQTJCLENBQUM7QUFDdEUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3BELE9BQU8sRUFBRSxHQUFHLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7QUFLeEQsTUFBTSxPQUFPLHNCQUFzQjs7OztJQVEvQixZQUFvQixJQUFnQjtRQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZO1FBTjVCLFdBQU0sR0FBVyxNQUFNLENBQUM7UUFDeEIsV0FBTSxHQUFXLE9BQU8sQ0FBQztRQUN6QixjQUFTLEdBQWdDLEVBQUUsQ0FBQztRQUM1QyxVQUFLLEdBQWdCLEVBQUUsQ0FBQztRQUN4QixnQkFBVyxHQUFXLElBQUksQ0FBQztJQUduQyxDQUFDOzs7OztJQUVELGNBQWMsQ0FBQyxLQUFhO1FBQ3hCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxJQUFJLElBQUksQ0FBQztJQUNyQyxDQUFDOzs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxJQUFZLEVBQUUsSUFBWTs7Y0FDakMsVUFBVSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSTs7OztRQUFDLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxRQUFRLENBQUMsSUFBSSxLQUFLLElBQUksRUFBQztRQUM1RSxJQUFJLFVBQVUsRUFBRTtZQUNaLFVBQVUsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1NBQzFCO2FBQU07WUFDSCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxJQUFJLHlCQUF5QixDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ2xGO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxrQkFBa0IsQ0FBQyxJQUFZO1FBQzNCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJOzs7O1FBQUMsQ0FBQyxDQUFDLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQyxJQUFJLEtBQUssSUFBSSxFQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ3RFLENBQUM7Ozs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxJQUFZLEVBQUUsU0FBb0MsRUFBRSxXQUFvQjs7Y0FDaEYsY0FBYyxHQUFHLFdBQVcsSUFBSSxHQUFHLFNBQVMsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLEdBQUcsSUFBSSxDQUFDLE1BQU0sTUFBTSxJQUFJLENBQUMsR0FBRyxFQUFFLEVBQUU7UUFFOUcsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsQ0FBQyxJQUFJLENBQ3JDLEdBQUc7Ozs7UUFBQyxDQUFDLEdBQWEsRUFBRSxFQUFFO1lBQ2xCLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDO1FBQy9CLENBQUMsRUFBQyxFQUNGLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFDUixVQUFVOzs7UUFBQyxHQUFHLEVBQUU7WUFDWixJQUFJLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUU7c0JBQzlCLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7Z0JBRWhDLElBQUksTUFBTSxJQUFJLE1BQU0sS0FBSyxJQUFJLENBQUMsV0FBVyxFQUFFOzswQkFDakMsR0FBRyxHQUFHLEdBQUcsU0FBUyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxNQUFNLElBQUksQ0FBQyxHQUFHLEVBQUUsRUFBRTtvQkFFdEYsT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLFNBQVMsRUFBRSxHQUFHLENBQUMsQ0FBQztpQkFDdkQ7YUFDSjtZQUNELE9BQU8sVUFBVSxDQUFDLGtCQUFrQixjQUFjLEVBQUUsQ0FBQyxDQUFDO1FBQzFELENBQUMsRUFBQyxDQUNMLENBQUM7SUFDTixDQUFDOzs7OztJQUVELG1CQUFtQixDQUFDLElBQVk7O2NBQ3RCLGVBQWUsR0FBRyxFQUFFO1FBQzFCLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ25CLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO1NBQ3pCO1FBQ0QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPOzs7O1FBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBRTtZQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFdEMsZUFBZSxDQUFDLElBQUksQ0FDaEIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksRUFBRSxTQUFTLENBQUMsQ0FDMUMsQ0FBQzthQUNMO1FBQ0wsQ0FBQyxFQUFDLENBQUM7UUFFSCxPQUFPLGVBQWUsQ0FBQztJQUMzQixDQUFDOzs7OztJQUVELElBQUksQ0FBQyxJQUFZO1FBQ2IsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLFNBQVMsRUFBRTtZQUNoQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsQ0FBQztTQUN6QjtJQUNMLENBQUM7Ozs7OztJQUVELGtCQUFrQixDQUFDLElBQVksRUFBRSxJQUFZO1FBQ3pDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxDQUFDLENBQUMsRUFBRSxFQUFFLENBQUMsQ0FBQyxLQUFLLElBQUksRUFBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUMzRSxDQUFDOzs7OztJQUVELHNCQUFzQixDQUFDLElBQVk7O1lBQzNCLE1BQU0sR0FBRyxFQUFFO1FBRWYsSUFBSSxDQUFDLFNBQVM7YUFDVCxLQUFLLENBQUMsQ0FBQyxDQUFDO2FBQ1IsSUFBSTs7Ozs7UUFBQyxDQUFDLENBQUMsRUFBRSxDQUFDLEVBQUUsRUFBRTtZQUNYLElBQUksQ0FBQyxDQUFDLElBQUksS0FBSyxLQUFLLEVBQUU7Z0JBQ2xCLE9BQU8sQ0FBQyxDQUFDO2FBQ1o7WUFDRCxJQUFJLENBQUMsQ0FBQyxJQUFJLEtBQUssS0FBSyxFQUFFO2dCQUNsQixPQUFPLENBQUMsQ0FBQyxDQUFDO2FBQ2I7WUFDRCxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4QyxDQUFDLEVBQUM7YUFDRCxPQUFPOzs7O1FBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTtZQUNmLElBQUksS0FBSyxDQUFDLElBQUksSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNoQyxNQUFNLEdBQUcsV0FBVyxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO2FBQ3hEO1FBQ0wsQ0FBQyxFQUFDLENBQUM7UUFFUCxPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDOzs7OztJQUVELGNBQWMsQ0FBQyxJQUFZOztZQUNuQixXQUFXLEdBQUcsS0FBSzs7Y0FDakIsS0FBSyxHQUFHO1lBQ1YsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRzs7OztZQUFDLENBQUMsVUFBVSxFQUFFLEVBQUU7Z0JBQ2pELE9BQU8sVUFBVSxDQUFDLElBQUksQ0FDbEIsVUFBVTs7OztnQkFBQyxDQUFDLEtBQUssRUFBRSxFQUFFO29CQUNqQixPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO29CQUNwQixXQUFXLEdBQUcsSUFBSSxDQUFDO29CQUNuQixPQUFPLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsQ0FBQyxFQUFDLENBQ0wsQ0FBQztZQUNOLENBQUMsRUFBQztTQUNMO1FBRUQsT0FBTyxJQUFJLFVBQVU7Ozs7UUFBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO1lBRS9CLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ2xCLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTOzs7Z0JBQ3JCLEdBQUcsRUFBRTs7MEJBQ0ssZUFBZSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUM7b0JBQ3pELElBQUksZUFBZSxFQUFFO3dCQUNqQixRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO3FCQUNsQztvQkFDRCxJQUFJLFdBQVcsRUFBRTt3QkFDYixRQUFRLENBQUMsS0FBSyxDQUFDLCtCQUErQixDQUFDLENBQUM7cUJBQ25EO3lCQUFNO3dCQUNILFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztxQkFDdkI7Z0JBQ0wsQ0FBQzs7OztnQkFDRCxDQUFDLEdBQUcsRUFBRSxFQUFFO29CQUNKLFFBQVEsQ0FBQyxLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQztnQkFDcEQsQ0FBQyxFQUFDLENBQUM7YUFDVjtpQkFBTTs7c0JBQ0csZUFBZSxHQUFHLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUM7Z0JBQ3pELElBQUksZUFBZSxFQUFFO29CQUNqQixRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO29CQUMvQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7aUJBQ3ZCO2FBQ0o7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7OztZQWpKSixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFYUSxVQUFVOzs7Ozs7OztJQWNmLHdDQUFnQzs7Ozs7SUFDaEMsd0NBQWlDOzs7OztJQUNqQywyQ0FBb0Q7Ozs7O0lBQ3BELHVDQUFnQzs7Ozs7SUFDaEMsNkNBQW1DOzs7OztJQUV2QixzQ0FBd0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSZXNwb25zZSB9IGZyb20gJ0Bhbmd1bGFyL2h0dHAnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVMb2FkZXIgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgZm9ya0pvaW4sIHRocm93RXJyb3IsIG9mIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IENvbXBvbmVudFRyYW5zbGF0aW9uTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvY29tcG9uZW50Lm1vZGVsJztcclxuaW1wb3J0IHsgT2JqZWN0VXRpbHMgfSBmcm9tICcuLi91dGlscy9vYmplY3QtdXRpbHMnO1xyXG5pbXBvcnQgeyBtYXAsIGNhdGNoRXJyb3IsIHJldHJ5IH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUcmFuc2xhdGVMb2FkZXJTZXJ2aWNlIGltcGxlbWVudHMgVHJhbnNsYXRlTG9hZGVyIHtcclxuXHJcbiAgICBwcml2YXRlIHByZWZpeDogc3RyaW5nID0gJ2kxOG4nO1xyXG4gICAgcHJpdmF0ZSBzdWZmaXg6IHN0cmluZyA9ICcuanNvbic7XHJcbiAgICBwcml2YXRlIHByb3ZpZGVyczogQ29tcG9uZW50VHJhbnNsYXRpb25Nb2RlbFtdID0gW107XHJcbiAgICBwcml2YXRlIHF1ZXVlOiBzdHJpbmcgW11bXSA9IFtdO1xyXG4gICAgcHJpdmF0ZSBkZWZhdWx0TGFuZzogc3RyaW5nID0gJ2VuJztcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQpIHtcclxuICAgIH1cclxuXHJcbiAgICBzZXREZWZhdWx0TGFuZyh2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0TGFuZyA9IHZhbHVlIHx8ICdlbic7XHJcbiAgICB9XHJcblxyXG4gICAgcmVnaXN0ZXJQcm92aWRlcihuYW1lOiBzdHJpbmcsIHBhdGg6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IHJlZ2lzdGVyZWQgPSB0aGlzLnByb3ZpZGVycy5maW5kKChwcm92aWRlcikgPT4gcHJvdmlkZXIubmFtZSA9PT0gbmFtZSk7XHJcbiAgICAgICAgaWYgKHJlZ2lzdGVyZWQpIHtcclxuICAgICAgICAgICAgcmVnaXN0ZXJlZC5wYXRoID0gcGF0aDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLnByb3ZpZGVycy5wdXNoKG5ldyBDb21wb25lbnRUcmFuc2xhdGlvbk1vZGVsKHsgbmFtZTogbmFtZSwgcGF0aDogcGF0aCB9KSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByb3ZpZGVyUmVnaXN0ZXJlZChuYW1lOiBzdHJpbmcpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm92aWRlcnMuZmluZCgoeCkgPT4geC5uYW1lID09PSBuYW1lKSA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBmZXRjaExhbmd1YWdlRmlsZShsYW5nOiBzdHJpbmcsIGNvbXBvbmVudDogQ29tcG9uZW50VHJhbnNsYXRpb25Nb2RlbCwgZmFsbGJhY2tVcmw/OiBzdHJpbmcpOiBPYnNlcnZhYmxlPHZvaWQ+IHtcclxuICAgICAgICBjb25zdCB0cmFuc2xhdGlvblVybCA9IGZhbGxiYWNrVXJsIHx8IGAke2NvbXBvbmVudC5wYXRofS8ke3RoaXMucHJlZml4fS8ke2xhbmd9JHt0aGlzLnN1ZmZpeH0/dj0ke0RhdGUubm93KCl9YDtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaHR0cC5nZXQodHJhbnNsYXRpb25VcmwpLnBpcGUoXHJcbiAgICAgICAgICAgIG1hcCgocmVzOiBSZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29tcG9uZW50Lmpzb25bbGFuZ10gPSByZXM7XHJcbiAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICByZXRyeSgzKSxcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIWZhbGxiYWNrVXJsICYmIGxhbmcuaW5jbHVkZXMoJy0nKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IFtsYW5nSWRdID0gbGFuZy5zcGxpdCgnLScpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAobGFuZ0lkICYmIGxhbmdJZCAhPT0gdGhpcy5kZWZhdWx0TGFuZykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB1cmwgPSBgJHtjb21wb25lbnQucGF0aH0vJHt0aGlzLnByZWZpeH0vJHtsYW5nSWR9JHt0aGlzLnN1ZmZpeH0/dj0ke0RhdGUubm93KCl9YDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmZldGNoTGFuZ3VhZ2VGaWxlKGxhbmcsIGNvbXBvbmVudCwgdXJsKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihgRmFpbGVkIHRvIGxvYWQgJHt0cmFuc2xhdGlvblVybH1gKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENvbXBvbmVudFRvRmV0Y2gobGFuZzogc3RyaW5nKTogQXJyYXk8T2JzZXJ2YWJsZTxhbnk+PiB7XHJcbiAgICAgICAgY29uc3Qgb2JzZXJ2YWJsZUJhdGNoID0gW107XHJcbiAgICAgICAgaWYgKCF0aGlzLnF1ZXVlW2xhbmddKSB7XHJcbiAgICAgICAgICAgIHRoaXMucXVldWVbbGFuZ10gPSBbXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5wcm92aWRlcnMuZm9yRWFjaCgoY29tcG9uZW50KSA9PiB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5pc0NvbXBvbmVudEluUXVldWUobGFuZywgY29tcG9uZW50Lm5hbWUpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnF1ZXVlW2xhbmddLnB1c2goY29tcG9uZW50Lm5hbWUpO1xyXG5cclxuICAgICAgICAgICAgICAgIG9ic2VydmFibGVCYXRjaC5wdXNoKFxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmV0Y2hMYW5ndWFnZUZpbGUobGFuZywgY29tcG9uZW50KVxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gb2JzZXJ2YWJsZUJhdGNoO1xyXG4gICAgfVxyXG5cclxuICAgIGluaXQobGFuZzogc3RyaW5nKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucXVldWVbbGFuZ10gPT09IHVuZGVmaW5lZCkge1xyXG4gICAgICAgICAgICB0aGlzLnF1ZXVlW2xhbmddID0gW107XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGlzQ29tcG9uZW50SW5RdWV1ZShsYW5nOiBzdHJpbmcsIG5hbWU6IHN0cmluZykge1xyXG4gICAgICAgIHJldHVybiAodGhpcy5xdWV1ZVtsYW5nXSB8fCBbXSkuZmluZCgoeCkgPT4geCA9PT0gbmFtZSkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RnVsbFRyYW5zbGF0aW9uSlNPTihsYW5nOiBzdHJpbmcpOiBhbnkge1xyXG4gICAgICAgIGxldCByZXN1bHQgPSB7fTtcclxuXHJcbiAgICAgICAgdGhpcy5wcm92aWRlcnNcclxuICAgICAgICAgICAgLnNsaWNlKDApXHJcbiAgICAgICAgICAgIC5zb3J0KChhLCBiKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoYS5uYW1lID09PSAnYXBwJykge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAxO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKGIubmFtZSA9PT0gJ2FwcCcpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gLTE7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gYS5uYW1lLmxvY2FsZUNvbXBhcmUoYi5uYW1lKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLmZvckVhY2goKG1vZGVsKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAobW9kZWwuanNvbiAmJiBtb2RlbC5qc29uW2xhbmddKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0ID0gT2JqZWN0VXRpbHMubWVyZ2UocmVzdWx0LCBtb2RlbC5qc29uW2xhbmddKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VHJhbnNsYXRpb24obGFuZzogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBsZXQgaGFzRmFpbHVyZXMgPSBmYWxzZTtcclxuICAgICAgICBjb25zdCBiYXRjaCA9IFtcclxuICAgICAgICAgICAgLi4udGhpcy5nZXRDb21wb25lbnRUb0ZldGNoKGxhbmcpLm1hcCgob2JzZXJ2YWJsZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG9ic2VydmFibGUucGlwZShcclxuICAgICAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zb2xlLndhcm4oZXJyb3IpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoYXNGYWlsdXJlcyA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBvZihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKChvYnNlcnZlcikgPT4ge1xyXG5cclxuICAgICAgICAgICAgaWYgKGJhdGNoLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIGZvcmtKb2luKGJhdGNoKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBmdWxsVHJhbnNsYXRpb24gPSB0aGlzLmdldEZ1bGxUcmFuc2xhdGlvbkpTT04obGFuZyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChmdWxsVHJhbnNsYXRpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZnVsbFRyYW5zbGF0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoaGFzRmFpbHVyZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdGYWlsZWQgdG8gbG9hZCBzb21lIHJlc291cmNlcycpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgKGVycikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5lcnJvcignRmFpbGVkIHRvIGxvYWQgc29tZSByZXNvdXJjZXMnKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGZ1bGxUcmFuc2xhdGlvbiA9IHRoaXMuZ2V0RnVsbFRyYW5zbGF0aW9uSlNPTihsYW5nKTtcclxuICAgICAgICAgICAgICAgIGlmIChmdWxsVHJhbnNsYXRpb24pIHtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KGZ1bGxUcmFuc2xhdGlvbik7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==