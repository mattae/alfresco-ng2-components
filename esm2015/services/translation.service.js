/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UserPreferencesService, UserPreferenceValues } from './user-preferences.service';
import * as i0 from "@angular/core";
import * as i1 from "@ngx-translate/core";
import * as i2 from "./user-preferences.service";
/** @type {?} */
export const TRANSLATION_PROVIDER = new InjectionToken('Injection token for translation providers.');
/**
 * @record
 */
export function TranslationProvider() { }
if (false) {
    /** @type {?} */
    TranslationProvider.prototype.name;
    /** @type {?} */
    TranslationProvider.prototype.source;
}
export class TranslationService {
    /**
     * @param {?} translate
     * @param {?} userPreferencesService
     * @param {?} providers
     */
    constructor(translate, userPreferencesService, providers) {
        this.translate = translate;
        this.customLoader = (/** @type {?} */ (this.translate.currentLoader));
        this.defaultLang = 'en';
        translate.setDefaultLang(this.defaultLang);
        this.customLoader.setDefaultLang(this.defaultLang);
        if (providers && providers.length > 0) {
            for (const provider of providers) {
                this.addTranslationFolder(provider.name, provider.source);
            }
        }
        userPreferencesService.select(UserPreferenceValues.Locale).subscribe((/**
         * @param {?} locale
         * @return {?}
         */
        (locale) => {
            if (locale) {
                this.userLang = locale;
                this.use(this.userLang);
            }
        }));
    }
    /**
     * Adds a new folder of translation source files.
     * @param {?=} name Name for the translation provider
     * @param {?=} path Path to the folder
     * @return {?}
     */
    addTranslationFolder(name = '', path = '') {
        if (!this.customLoader.providerRegistered(name)) {
            this.customLoader.registerProvider(name, path);
            if (this.userLang) {
                this.loadTranslation(this.userLang, this.defaultLang);
            }
            else {
                this.loadTranslation(this.defaultLang);
            }
        }
    }
    /**
     * Loads a translation file.
     * @param {?} lang Language code for the language to load
     * @param {?=} fallback Language code to fall back to if the first one was unavailable
     * @return {?}
     */
    loadTranslation(lang, fallback) {
        this.translate.getTranslation(lang).subscribe((/**
         * @return {?}
         */
        () => {
            this.translate.use(lang);
            this.onTranslationChanged(lang);
        }), (/**
         * @return {?}
         */
        () => {
            if (fallback && fallback !== lang) {
                this.loadTranslation(fallback);
            }
        }));
    }
    /**
     * Triggers a notification callback when the translation language changes.
     * @param {?} lang The new language code
     * @return {?}
     */
    onTranslationChanged(lang) {
        this.translate.onTranslationChange.next({
            lang: lang,
            translations: this.customLoader.getFullTranslationJSON(lang)
        });
    }
    /**
     * Sets the target language for translations.
     * @param {?} lang Code name for the language
     * @return {?} Translations available for the language
     */
    use(lang) {
        this.customLoader.init(lang);
        return this.translate.use(lang);
    }
    /**
     * Gets the translation for the supplied key.
     * @param {?} key Key to translate
     * @param {?=} interpolateParams String(s) to be interpolated into the main message
     * @return {?} Translated text
     */
    get(key, interpolateParams) {
        return this.translate.get(key, interpolateParams);
    }
    /**
     * Directly returns the translation for the supplied key.
     * @param {?} key Key to translate
     * @param {?=} interpolateParams String(s) to be interpolated into the main message
     * @return {?} Translated text
     */
    instant(key, interpolateParams) {
        return key ? this.translate.instant(key, interpolateParams) : '';
    }
}
TranslationService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
TranslationService.ctorParameters = () => [
    { type: TranslateService },
    { type: UserPreferencesService },
    { type: Array, decorators: [{ type: Optional }, { type: Inject, args: [TRANSLATION_PROVIDER,] }] }
];
/** @nocollapse */ TranslationService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function TranslationService_Factory() { return new TranslationService(i0.ɵɵinject(i1.TranslateService), i0.ɵɵinject(i2.UserPreferencesService), i0.ɵɵinject(TRANSLATION_PROVIDER, 8)); }, token: TranslationService, providedIn: "root" });
if (false) {
    /** @type {?} */
    TranslationService.prototype.defaultLang;
    /** @type {?} */
    TranslationService.prototype.userLang;
    /** @type {?} */
    TranslationService.prototype.customLoader;
    /** @type {?} */
    TranslationService.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhbnNsYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL3RyYW5zbGF0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLE1BQU0sRUFBRSxVQUFVLEVBQUUsY0FBYyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3RSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUd2RCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQzs7Ozs7QUFFMUYsTUFBTSxPQUFPLG9CQUFvQixHQUFHLElBQUksY0FBYyxDQUFDLDRDQUE0QyxDQUFDOzs7O0FBRXBHLHlDQUdDOzs7SUFGRyxtQ0FBYTs7SUFDYixxQ0FBZTs7QUFNbkIsTUFBTSxPQUFPLGtCQUFrQjs7Ozs7O0lBSzNCLFlBQW1CLFNBQTJCLEVBQ2xDLHNCQUE4QyxFQUNKLFNBQWdDO1FBRm5FLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBRzFDLElBQUksQ0FBQyxZQUFZLEdBQUcsbUJBQXlCLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFBLENBQUM7UUFFMUUsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDeEIsU0FBUyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRW5ELElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ25DLEtBQUssTUFBTSxRQUFRLElBQUksU0FBUyxFQUFFO2dCQUM5QixJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDN0Q7U0FDSjtRQUVELHNCQUFzQixDQUFDLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTOzs7O1FBQUMsQ0FBQyxNQUFNLEVBQUUsRUFBRTtZQUM1RSxJQUFJLE1BQU0sRUFBRTtnQkFDUixJQUFJLENBQUMsUUFBUSxHQUFHLE1BQU0sQ0FBQztnQkFDdkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDM0I7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7SUFPRCxvQkFBb0IsQ0FBQyxPQUFlLEVBQUUsRUFBRSxPQUFlLEVBQUU7UUFDckQsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDN0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFL0MsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNmLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDekQ7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDMUM7U0FDSjtJQUNMLENBQUM7Ozs7Ozs7SUFPRCxlQUFlLENBQUMsSUFBWSxFQUFFLFFBQWlCO1FBQzNDLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVM7OztRQUN6QyxHQUFHLEVBQUU7WUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN6QixJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDcEMsQ0FBQzs7O1FBQ0QsR0FBRyxFQUFFO1lBQ0QsSUFBSSxRQUFRLElBQUksUUFBUSxLQUFLLElBQUksRUFBRTtnQkFDL0IsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUNsQztRQUNMLENBQUMsRUFDSixDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBTUQsb0JBQW9CLENBQUMsSUFBWTtRQUM3QixJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQztZQUNwQyxJQUFJLEVBQUUsSUFBSTtZQUNWLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQztTQUMvRCxDQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFPRCxHQUFHLENBQUMsSUFBWTtRQUNaLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzdCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDcEMsQ0FBQzs7Ozs7OztJQVFELEdBQUcsQ0FBQyxHQUEyQixFQUFFLGlCQUEwQjtRQUN2RCxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO0lBQ3RELENBQUM7Ozs7Ozs7SUFRRCxPQUFPLENBQUMsR0FBMkIsRUFBRSxpQkFBMEI7UUFDM0QsT0FBTyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDckUsQ0FBQzs7O1lBMUdKLFVBQVUsU0FBQztnQkFDUixVQUFVLEVBQUUsTUFBTTthQUNyQjs7OztZQWRRLGdCQUFnQjtZQUdoQixzQkFBc0I7d0NBbUJkLFFBQVEsWUFBSSxNQUFNLFNBQUMsb0JBQW9COzs7OztJQU5wRCx5Q0FBb0I7O0lBQ3BCLHNDQUFpQjs7SUFDakIsMENBQXFDOztJQUV6Qix1Q0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0LCBJbmplY3RhYmxlLCBJbmplY3Rpb25Ub2tlbiwgT3B0aW9uYWwgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZUxvYWRlclNlcnZpY2UgfSBmcm9tICcuL3RyYW5zbGF0ZS1sb2FkZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UsIFVzZXJQcmVmZXJlbmNlVmFsdWVzIH0gZnJvbSAnLi91c2VyLXByZWZlcmVuY2VzLnNlcnZpY2UnO1xyXG5cclxuZXhwb3J0IGNvbnN0IFRSQU5TTEFUSU9OX1BST1ZJREVSID0gbmV3IEluamVjdGlvblRva2VuKCdJbmplY3Rpb24gdG9rZW4gZm9yIHRyYW5zbGF0aW9uIHByb3ZpZGVycy4nKTtcclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgVHJhbnNsYXRpb25Qcm92aWRlciB7XHJcbiAgICBuYW1lOiBzdHJpbmc7XHJcbiAgICBzb3VyY2U6IHN0cmluZztcclxufVxyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUcmFuc2xhdGlvblNlcnZpY2Uge1xyXG4gICAgZGVmYXVsdExhbmc6IHN0cmluZztcclxuICAgIHVzZXJMYW5nOiBzdHJpbmc7XHJcbiAgICBjdXN0b21Mb2FkZXI6IFRyYW5zbGF0ZUxvYWRlclNlcnZpY2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHVzZXJQcmVmZXJlbmNlc1NlcnZpY2U6IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBAT3B0aW9uYWwoKSBASW5qZWN0KFRSQU5TTEFUSU9OX1BST1ZJREVSKSBwcm92aWRlcnM6IFRyYW5zbGF0aW9uUHJvdmlkZXJbXSkge1xyXG4gICAgICAgIHRoaXMuY3VzdG9tTG9hZGVyID0gPFRyYW5zbGF0ZUxvYWRlclNlcnZpY2U+IHRoaXMudHJhbnNsYXRlLmN1cnJlbnRMb2FkZXI7XHJcblxyXG4gICAgICAgIHRoaXMuZGVmYXVsdExhbmcgPSAnZW4nO1xyXG4gICAgICAgIHRyYW5zbGF0ZS5zZXREZWZhdWx0TGFuZyh0aGlzLmRlZmF1bHRMYW5nKTtcclxuICAgICAgICB0aGlzLmN1c3RvbUxvYWRlci5zZXREZWZhdWx0TGFuZyh0aGlzLmRlZmF1bHRMYW5nKTtcclxuXHJcbiAgICAgICAgaWYgKHByb3ZpZGVycyAmJiBwcm92aWRlcnMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IHByb3ZpZGVyIG9mIHByb3ZpZGVycykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hZGRUcmFuc2xhdGlvbkZvbGRlcihwcm92aWRlci5uYW1lLCBwcm92aWRlci5zb3VyY2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB1c2VyUHJlZmVyZW5jZXNTZXJ2aWNlLnNlbGVjdChVc2VyUHJlZmVyZW5jZVZhbHVlcy5Mb2NhbGUpLnN1YnNjcmliZSgobG9jYWxlKSA9PiB7XHJcbiAgICAgICAgICAgIGlmIChsb2NhbGUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMudXNlckxhbmcgPSBsb2NhbGU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZSh0aGlzLnVzZXJMYW5nKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQWRkcyBhIG5ldyBmb2xkZXIgb2YgdHJhbnNsYXRpb24gc291cmNlIGZpbGVzLlxyXG4gICAgICogQHBhcmFtIG5hbWUgTmFtZSBmb3IgdGhlIHRyYW5zbGF0aW9uIHByb3ZpZGVyXHJcbiAgICAgKiBAcGFyYW0gcGF0aCBQYXRoIHRvIHRoZSBmb2xkZXJcclxuICAgICAqL1xyXG4gICAgYWRkVHJhbnNsYXRpb25Gb2xkZXIobmFtZTogc3RyaW5nID0gJycsIHBhdGg6IHN0cmluZyA9ICcnKSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLmN1c3RvbUxvYWRlci5wcm92aWRlclJlZ2lzdGVyZWQobmFtZSkpIHtcclxuICAgICAgICAgICAgdGhpcy5jdXN0b21Mb2FkZXIucmVnaXN0ZXJQcm92aWRlcihuYW1lLCBwYXRoKTtcclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLnVzZXJMYW5nKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRUcmFuc2xhdGlvbih0aGlzLnVzZXJMYW5nLCB0aGlzLmRlZmF1bHRMYW5nKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZFRyYW5zbGF0aW9uKHRoaXMuZGVmYXVsdExhbmcpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTG9hZHMgYSB0cmFuc2xhdGlvbiBmaWxlLlxyXG4gICAgICogQHBhcmFtIGxhbmcgTGFuZ3VhZ2UgY29kZSBmb3IgdGhlIGxhbmd1YWdlIHRvIGxvYWRcclxuICAgICAqIEBwYXJhbSBmYWxsYmFjayBMYW5ndWFnZSBjb2RlIHRvIGZhbGwgYmFjayB0byBpZiB0aGUgZmlyc3Qgb25lIHdhcyB1bmF2YWlsYWJsZVxyXG4gICAgICovXHJcbiAgICBsb2FkVHJhbnNsYXRpb24obGFuZzogc3RyaW5nLCBmYWxsYmFjaz86IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMudHJhbnNsYXRlLmdldFRyYW5zbGF0aW9uKGxhbmcpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy50cmFuc2xhdGUudXNlKGxhbmcpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vblRyYW5zbGF0aW9uQ2hhbmdlZChsYW5nKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKGZhbGxiYWNrICYmIGZhbGxiYWNrICE9PSBsYW5nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2FkVHJhbnNsYXRpb24oZmFsbGJhY2spO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFRyaWdnZXJzIGEgbm90aWZpY2F0aW9uIGNhbGxiYWNrIHdoZW4gdGhlIHRyYW5zbGF0aW9uIGxhbmd1YWdlIGNoYW5nZXMuXHJcbiAgICAgKiBAcGFyYW0gbGFuZyBUaGUgbmV3IGxhbmd1YWdlIGNvZGVcclxuICAgICAqL1xyXG4gICAgb25UcmFuc2xhdGlvbkNoYW5nZWQobGFuZzogc3RyaW5nKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy50cmFuc2xhdGUub25UcmFuc2xhdGlvbkNoYW5nZS5uZXh0KHtcclxuICAgICAgICAgICAgbGFuZzogbGFuZyxcclxuICAgICAgICAgICAgdHJhbnNsYXRpb25zOiB0aGlzLmN1c3RvbUxvYWRlci5nZXRGdWxsVHJhbnNsYXRpb25KU09OKGxhbmcpXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXRzIHRoZSB0YXJnZXQgbGFuZ3VhZ2UgZm9yIHRyYW5zbGF0aW9ucy5cclxuICAgICAqIEBwYXJhbSBsYW5nIENvZGUgbmFtZSBmb3IgdGhlIGxhbmd1YWdlXHJcbiAgICAgKiBAcmV0dXJucyBUcmFuc2xhdGlvbnMgYXZhaWxhYmxlIGZvciB0aGUgbGFuZ3VhZ2VcclxuICAgICAqL1xyXG4gICAgdXNlKGxhbmc6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgdGhpcy5jdXN0b21Mb2FkZXIuaW5pdChsYW5nKTtcclxuICAgICAgICByZXR1cm4gdGhpcy50cmFuc2xhdGUudXNlKGxhbmcpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgdHJhbnNsYXRpb24gZm9yIHRoZSBzdXBwbGllZCBrZXkuXHJcbiAgICAgKiBAcGFyYW0ga2V5IEtleSB0byB0cmFuc2xhdGVcclxuICAgICAqIEBwYXJhbSBpbnRlcnBvbGF0ZVBhcmFtcyBTdHJpbmcocykgdG8gYmUgaW50ZXJwb2xhdGVkIGludG8gdGhlIG1haW4gbWVzc2FnZVxyXG4gICAgICogQHJldHVybnMgVHJhbnNsYXRlZCB0ZXh0XHJcbiAgICAgKi9cclxuICAgIGdldChrZXk6IHN0cmluZyB8IEFycmF5PHN0cmluZz4sIGludGVycG9sYXRlUGFyYW1zPzogT2JqZWN0KTogT2JzZXJ2YWJsZTxzdHJpbmcgfCBhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy50cmFuc2xhdGUuZ2V0KGtleSwgaW50ZXJwb2xhdGVQYXJhbXMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGlyZWN0bHkgcmV0dXJucyB0aGUgdHJhbnNsYXRpb24gZm9yIHRoZSBzdXBwbGllZCBrZXkuXHJcbiAgICAgKiBAcGFyYW0ga2V5IEtleSB0byB0cmFuc2xhdGVcclxuICAgICAqIEBwYXJhbSBpbnRlcnBvbGF0ZVBhcmFtcyBTdHJpbmcocykgdG8gYmUgaW50ZXJwb2xhdGVkIGludG8gdGhlIG1haW4gbWVzc2FnZVxyXG4gICAgICogQHJldHVybnMgVHJhbnNsYXRlZCB0ZXh0XHJcbiAgICAgKi9cclxuICAgIGluc3RhbnQoa2V5OiBzdHJpbmcgfCBBcnJheTxzdHJpbmc+LCBpbnRlcnBvbGF0ZVBhcmFtcz86IE9iamVjdCk6IHN0cmluZyB8IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIGtleSA/IHRoaXMudHJhbnNsYXRlLmluc3RhbnQoa2V5LCBpbnRlcnBvbGF0ZVBhcmFtcykgOiAnJztcclxuICAgIH1cclxufVxyXG4iXX0=