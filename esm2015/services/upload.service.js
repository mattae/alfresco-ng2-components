/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { Minimatch } from 'minimatch-browser';
import { Subject } from 'rxjs';
import { AppConfigService } from '../app-config/app-config.service';
import { FileUploadCompleteEvent, FileUploadDeleteEvent, FileUploadErrorEvent, FileUploadEvent } from '../events/file.event';
import { FileUploadStatus } from '../models/file.model';
import { AlfrescoApiService } from './alfresco-api.service';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "../app-config/app-config.service";
export class UploadService {
    /**
     * @param {?} apiService
     * @param {?} appConfigService
     */
    constructor(apiService, appConfigService) {
        this.apiService = apiService;
        this.appConfigService = appConfigService;
        this.cache = {};
        this.totalComplete = 0;
        this.totalAborted = 0;
        this.totalError = 0;
        this.excludedFileList = [];
        this.matchingOptions = null;
        this.activeTask = null;
        this.queue = [];
        this.queueChanged = new Subject();
        this.fileUpload = new Subject();
        this.fileUploadStarting = new Subject();
        this.fileUploadCancelled = new Subject();
        this.fileUploadProgress = new Subject();
        this.fileUploadAborted = new Subject();
        this.fileUploadError = new Subject();
        this.fileUploadComplete = new Subject();
        this.fileUploadDeleted = new Subject();
        this.fileDeleted = new Subject();
    }
    /**
     * Checks whether the service is uploading a file.
     * @return {?} True if a file is uploading, false otherwise
     */
    isUploading() {
        return this.activeTask ? true : false;
    }
    /**
     * Gets the file Queue
     * @return {?} Array of files that form the queue
     */
    getQueue() {
        return this.queue;
    }
    /**
     * Adds files to the uploading queue to be uploaded
     * @param {...?} files One or more separate parameters or an array of files to queue
     * @return {?} Array of files that were not blocked from upload by the ignore list
     */
    addToQueue(...files) {
        /** @type {?} */
        const allowedFiles = files.filter((/**
         * @param {?} currentFile
         * @return {?}
         */
        (currentFile) => this.filterElement(currentFile)));
        this.queue = this.queue.concat(allowedFiles);
        this.queueChanged.next(this.queue);
        return allowedFiles;
    }
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    filterElement(file) {
        /** @type {?} */
        let isAllowed = true;
        this.excludedFileList = (/** @type {?} */ (this.appConfigService.get('files.excluded')));
        if (this.excludedFileList) {
            this.matchingOptions = this.appConfigService.get('files.match-options');
            isAllowed = this.excludedFileList.filter((/**
             * @param {?} pattern
             * @return {?}
             */
            (pattern) => {
                /** @type {?} */
                const minimatch = new Minimatch(pattern, this.matchingOptions);
                return minimatch.match(file.name);
            })).length === 0;
        }
        return isAllowed;
    }
    /**
     * Finds all the files in the queue that are not yet uploaded and uploads them into the directory folder.
     * @param {?=} emitter Emitter to invoke on file status change
     * @return {?}
     */
    uploadFilesInTheQueue(emitter) {
        if (!this.activeTask) {
            /** @type {?} */
            const file = this.queue.find((/**
             * @param {?} currentFile
             * @return {?}
             */
            (currentFile) => currentFile.status === FileUploadStatus.Pending));
            if (file) {
                this.onUploadStarting(file);
                /** @type {?} */
                const promise = this.beginUpload(file, emitter);
                this.activeTask = promise;
                this.cache[file.id] = promise;
                /** @type {?} */
                const next = (/**
                 * @return {?}
                 */
                () => {
                    this.activeTask = null;
                    setTimeout((/**
                     * @return {?}
                     */
                    () => this.uploadFilesInTheQueue(emitter)), 100);
                });
                promise.next = next;
                promise.then((/**
                 * @return {?}
                 */
                () => next()), (/**
                 * @return {?}
                 */
                () => next()));
            }
        }
    }
    /**
     * Cancels uploading of files.
     * @param {...?} files One or more separate parameters or an array of files specifying uploads to cancel
     * @return {?}
     */
    cancelUpload(...files) {
        files.forEach((/**
         * @param {?} file
         * @return {?}
         */
        (file) => {
            /** @type {?} */
            const promise = this.cache[file.id];
            if (promise) {
                promise.abort();
                delete this.cache[file.id];
            }
            else {
                /** @type {?} */
                const performAction = this.getAction(file);
                performAction();
            }
        }));
    }
    /**
     * Clears the upload queue
     * @return {?}
     */
    clearQueue() {
        this.queue = [];
        this.totalComplete = 0;
        this.totalAborted = 0;
        this.totalError = 0;
    }
    /**
     * Gets an upload promise for a file.
     * @param {?} file The target file
     * @return {?} Promise that is resolved if the upload is successful or error otherwise
     */
    getUploadPromise(file) {
        /** @type {?} */
        const opts = {
            renditions: 'doclib',
            include: ['allowableOperations']
        };
        if (file.options.newVersion === true) {
            opts.overwrite = true;
            opts.majorVersion = file.options.majorVersion;
            opts.comment = file.options.comment;
            opts.name = file.name;
        }
        else {
            opts.autoRename = true;
        }
        if (file.options.nodeType) {
            opts.nodeType = file.options.nodeType;
        }
        if (file.id) {
            return this.apiService.getInstance().node.updateNodeContent(file.id, file.file, opts);
        }
        else {
            return this.apiService.getInstance().upload.uploadFile(file.file, file.options.path, file.options.parentId, file.options, opts);
        }
    }
    /**
     * @private
     * @param {?} file
     * @param {?} emitter
     * @return {?}
     */
    beginUpload(file, emitter) {
        /** @type {?} */
        const promise = this.getUploadPromise(file);
        promise.on('progress', (/**
         * @param {?} progress
         * @return {?}
         */
        (progress) => {
            this.onUploadProgress(file, progress);
        }))
            .on('abort', (/**
         * @return {?}
         */
        () => {
            this.onUploadAborted(file);
            if (emitter) {
                emitter.emit({ value: 'File aborted' });
            }
        }))
            .on('error', (/**
         * @param {?} err
         * @return {?}
         */
        (err) => {
            this.onUploadError(file, err);
            if (emitter) {
                emitter.emit({ value: 'Error file uploaded' });
            }
        }))
            .on('success', (/**
         * @param {?} data
         * @return {?}
         */
        (data) => {
            this.onUploadComplete(file, data);
            if (emitter) {
                emitter.emit({ value: data });
            }
        }))
            .catch((/**
         * @param {?} err
         * @return {?}
         */
        (err) => {
        }));
        return promise;
    }
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    onUploadStarting(file) {
        if (file) {
            file.status = FileUploadStatus.Starting;
            /** @type {?} */
            const event = new FileUploadEvent(file, FileUploadStatus.Starting);
            this.fileUpload.next(event);
            this.fileUploadStarting.next(event);
        }
    }
    /**
     * @private
     * @param {?} file
     * @param {?} progress
     * @return {?}
     */
    onUploadProgress(file, progress) {
        if (file) {
            file.progress = progress;
            file.status = FileUploadStatus.Progress;
            /** @type {?} */
            const event = new FileUploadEvent(file, FileUploadStatus.Progress);
            this.fileUpload.next(event);
            this.fileUploadProgress.next(event);
        }
    }
    /**
     * @private
     * @param {?} file
     * @param {?} error
     * @return {?}
     */
    onUploadError(file, error) {
        if (file) {
            file.errorCode = (error || {}).status;
            file.status = FileUploadStatus.Error;
            this.totalError++;
            /** @type {?} */
            const promise = this.cache[file.id];
            if (promise) {
                delete this.cache[file.id];
            }
            /** @type {?} */
            const event = new FileUploadErrorEvent(file, error, this.totalError);
            this.fileUpload.next(event);
            this.fileUploadError.next(event);
        }
    }
    /**
     * @private
     * @param {?} file
     * @param {?} data
     * @return {?}
     */
    onUploadComplete(file, data) {
        if (file) {
            file.status = FileUploadStatus.Complete;
            file.data = data;
            this.totalComplete++;
            /** @type {?} */
            const promise = this.cache[file.id];
            if (promise) {
                delete this.cache[file.id];
            }
            /** @type {?} */
            const event = new FileUploadCompleteEvent(file, this.totalComplete, data, this.totalAborted);
            this.fileUpload.next(event);
            this.fileUploadComplete.next(event);
        }
    }
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    onUploadAborted(file) {
        if (file) {
            file.status = FileUploadStatus.Aborted;
            this.totalAborted++;
            /** @type {?} */
            const promise = this.cache[file.id];
            if (promise) {
                delete this.cache[file.id];
            }
            /** @type {?} */
            const event = new FileUploadEvent(file, FileUploadStatus.Aborted);
            this.fileUpload.next(event);
            this.fileUploadAborted.next(event);
            promise.next();
        }
    }
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    onUploadCancelled(file) {
        if (file) {
            file.status = FileUploadStatus.Cancelled;
            /** @type {?} */
            const event = new FileUploadEvent(file, FileUploadStatus.Cancelled);
            this.fileUpload.next(event);
            this.fileUploadCancelled.next(event);
        }
    }
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    onUploadDeleted(file) {
        if (file) {
            file.status = FileUploadStatus.Deleted;
            this.totalComplete--;
            /** @type {?} */
            const event = new FileUploadDeleteEvent(file, this.totalComplete);
            this.fileUpload.next(event);
            this.fileUploadDeleted.next(event);
        }
    }
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    getAction(file) {
        /** @type {?} */
        const actions = {
            [FileUploadStatus.Pending]: (/**
             * @return {?}
             */
            () => this.onUploadCancelled(file)),
            [FileUploadStatus.Deleted]: (/**
             * @return {?}
             */
            () => this.onUploadDeleted(file)),
            [FileUploadStatus.Error]: (/**
             * @return {?}
             */
            () => this.onUploadError(file, null))
        };
        return actions[file.status];
    }
}
UploadService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
UploadService.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: AppConfigService }
];
/** @nocollapse */ UploadService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function UploadService_Factory() { return new UploadService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.AppConfigService)); }, token: UploadService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    UploadService.prototype.cache;
    /**
     * @type {?}
     * @private
     */
    UploadService.prototype.totalComplete;
    /**
     * @type {?}
     * @private
     */
    UploadService.prototype.totalAborted;
    /**
     * @type {?}
     * @private
     */
    UploadService.prototype.totalError;
    /**
     * @type {?}
     * @private
     */
    UploadService.prototype.excludedFileList;
    /**
     * @type {?}
     * @private
     */
    UploadService.prototype.matchingOptions;
    /** @type {?} */
    UploadService.prototype.activeTask;
    /** @type {?} */
    UploadService.prototype.queue;
    /** @type {?} */
    UploadService.prototype.queueChanged;
    /** @type {?} */
    UploadService.prototype.fileUpload;
    /** @type {?} */
    UploadService.prototype.fileUploadStarting;
    /** @type {?} */
    UploadService.prototype.fileUploadCancelled;
    /** @type {?} */
    UploadService.prototype.fileUploadProgress;
    /** @type {?} */
    UploadService.prototype.fileUploadAborted;
    /** @type {?} */
    UploadService.prototype.fileUploadError;
    /** @type {?} */
    UploadService.prototype.fileUploadComplete;
    /** @type {?} */
    UploadService.prototype.fileUploadDeleted;
    /** @type {?} */
    UploadService.prototype.fileDeleted;
    /**
     * @type {?}
     * @protected
     */
    UploadService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    UploadService.prototype.appConfigService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy91cGxvYWQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQWdCLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6RCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDOUMsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUMvQixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUNwRSxPQUFPLEVBQ0gsdUJBQXVCLEVBQ3ZCLHFCQUFxQixFQUNyQixvQkFBb0IsRUFDcEIsZUFBZSxFQUNsQixNQUFNLHNCQUFzQixDQUFDO0FBQzlCLE9BQU8sRUFBaUMsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN2RixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQzs7OztBQUs1RCxNQUFNLE9BQU8sYUFBYTs7Ozs7SUF1QnRCLFlBQXNCLFVBQThCLEVBQVUsZ0JBQWtDO1FBQTFFLGVBQVUsR0FBVixVQUFVLENBQW9CO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQXJCeEYsVUFBSyxHQUEyQixFQUFFLENBQUM7UUFDbkMsa0JBQWEsR0FBVyxDQUFDLENBQUM7UUFDMUIsaUJBQVksR0FBVyxDQUFDLENBQUM7UUFDekIsZUFBVSxHQUFXLENBQUMsQ0FBQztRQUN2QixxQkFBZ0IsR0FBYSxFQUFFLENBQUM7UUFDaEMsb0JBQWUsR0FBUSxJQUFJLENBQUM7UUFFcEMsZUFBVSxHQUFpQixJQUFJLENBQUM7UUFDaEMsVUFBSyxHQUFnQixFQUFFLENBQUM7UUFFeEIsaUJBQVksR0FBeUIsSUFBSSxPQUFPLEVBQWUsQ0FBQztRQUNoRSxlQUFVLEdBQTZCLElBQUksT0FBTyxFQUFtQixDQUFDO1FBQ3RFLHVCQUFrQixHQUE2QixJQUFJLE9BQU8sRUFBbUIsQ0FBQztRQUM5RSx3QkFBbUIsR0FBNkIsSUFBSSxPQUFPLEVBQW1CLENBQUM7UUFDL0UsdUJBQWtCLEdBQTZCLElBQUksT0FBTyxFQUFtQixDQUFDO1FBQzlFLHNCQUFpQixHQUE2QixJQUFJLE9BQU8sRUFBbUIsQ0FBQztRQUM3RSxvQkFBZSxHQUFrQyxJQUFJLE9BQU8sRUFBd0IsQ0FBQztRQUNyRix1QkFBa0IsR0FBcUMsSUFBSSxPQUFPLEVBQTJCLENBQUM7UUFDOUYsc0JBQWlCLEdBQW1DLElBQUksT0FBTyxFQUF5QixDQUFDO1FBQ3pGLGdCQUFXLEdBQW9CLElBQUksT0FBTyxFQUFVLENBQUM7SUFHckQsQ0FBQzs7Ozs7SUFNRCxXQUFXO1FBQ1AsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUMxQyxDQUFDOzs7OztJQU1ELFFBQVE7UUFDSixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7SUFDdEIsQ0FBQzs7Ozs7O0lBT0QsVUFBVSxDQUFDLEdBQUcsS0FBa0I7O2NBQ3RCLFlBQVksR0FBRyxLQUFLLENBQUMsTUFBTTs7OztRQUFDLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxFQUFDO1FBQ25GLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDN0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ25DLE9BQU8sWUFBWSxDQUFDO0lBQ3hCLENBQUM7Ozs7OztJQUVPLGFBQWEsQ0FBQyxJQUFlOztZQUM3QixTQUFTLEdBQUcsSUFBSTtRQUVwQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsbUJBQVcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxnQkFBZ0IsQ0FBQyxFQUFBLENBQUM7UUFDL0UsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFFdkIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLHFCQUFxQixDQUFDLENBQUM7WUFFeEUsU0FBUyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNOzs7O1lBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTs7c0JBQzNDLFNBQVMsR0FBRyxJQUFJLFNBQVMsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQztnQkFDOUQsT0FBTyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0QyxDQUFDLEVBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO1NBQ25CO1FBQ0QsT0FBTyxTQUFTLENBQUM7SUFDckIsQ0FBQzs7Ozs7O0lBTUQscUJBQXFCLENBQUMsT0FBMkI7UUFDN0MsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7O2tCQUNaLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUk7Ozs7WUFBQyxDQUFDLFdBQVcsRUFBRSxFQUFFLENBQUMsV0FBVyxDQUFDLE1BQU0sS0FBSyxnQkFBZ0IsQ0FBQyxPQUFPLEVBQUM7WUFDOUYsSUFBSSxJQUFJLEVBQUU7Z0JBQ04sSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxDQUFDOztzQkFFdEIsT0FBTyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQztnQkFDL0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUM7Z0JBQzFCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQzs7c0JBRXhCLElBQUk7OztnQkFBRyxHQUFHLEVBQUU7b0JBQ2QsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7b0JBQ3ZCLFVBQVU7OztvQkFBQyxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxDQUFDLEdBQUUsR0FBRyxDQUFDLENBQUM7Z0JBQy9ELENBQUMsQ0FBQTtnQkFFRCxPQUFPLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztnQkFFcEIsT0FBTyxDQUFDLElBQUk7OztnQkFDUixHQUFHLEVBQUUsQ0FBQyxJQUFJLEVBQUU7OztnQkFDWixHQUFHLEVBQUUsQ0FBQyxJQUFJLEVBQUUsRUFDZixDQUFDO2FBQ0w7U0FDSjtJQUNMLENBQUM7Ozs7OztJQU1ELFlBQVksQ0FBQyxHQUFHLEtBQWtCO1FBQzlCLEtBQUssQ0FBQyxPQUFPOzs7O1FBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRTs7a0JBQ2IsT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztZQUVuQyxJQUFJLE9BQU8sRUFBRTtnQkFDVCxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUM7Z0JBQ2hCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDOUI7aUJBQU07O3NCQUNHLGFBQWEsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQztnQkFDMUMsYUFBYSxFQUFFLENBQUM7YUFDbkI7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBR0QsVUFBVTtRQUNOLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO0lBQ3hCLENBQUM7Ozs7OztJQU9ELGdCQUFnQixDQUFDLElBQWU7O2NBQ3RCLElBQUksR0FBUTtZQUNkLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLE9BQU8sRUFBRSxDQUFDLHFCQUFxQixDQUFDO1NBQ25DO1FBRUQsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsS0FBSyxJQUFJLEVBQUU7WUFDbEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDdEIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQztZQUM5QyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztTQUN6QjthQUFNO1lBQ0gsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7U0FDMUI7UUFFRCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7U0FDekM7UUFFRCxJQUFJLElBQUksQ0FBQyxFQUFFLEVBQUU7WUFDVCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUN2RCxJQUFJLENBQUMsRUFBRSxFQUNQLElBQUksQ0FBQyxJQUFJLEVBQ1QsSUFBSSxDQUNQLENBQUM7U0FDTDthQUFNO1lBQ0gsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQ2xELElBQUksQ0FBQyxJQUFJLEVBQ1QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQ2pCLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUNyQixJQUFJLENBQUMsT0FBTyxFQUNaLElBQUksQ0FDUCxDQUFDO1NBQ0w7SUFDTCxDQUFDOzs7Ozs7O0lBRU8sV0FBVyxDQUFDLElBQWUsRUFBRSxPQUEwQjs7Y0FFckQsT0FBTyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUM7UUFFM0MsT0FBTyxDQUFDLEVBQUUsQ0FBQyxVQUFVOzs7O1FBQUUsQ0FBQyxRQUE0QixFQUFFLEVBQUU7WUFDcEQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztRQUMxQyxDQUFDLEVBQUM7YUFDRyxFQUFFLENBQUMsT0FBTzs7O1FBQUUsR0FBRyxFQUFFO1lBQ2QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMzQixJQUFJLE9BQU8sRUFBRTtnQkFDVCxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLGNBQWMsRUFBRSxDQUFDLENBQUM7YUFDM0M7UUFDTCxDQUFDLEVBQUM7YUFDRCxFQUFFLENBQUMsT0FBTzs7OztRQUFFLENBQUMsR0FBRyxFQUFFLEVBQUU7WUFDakIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDOUIsSUFBSSxPQUFPLEVBQUU7Z0JBQ1QsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLEtBQUssRUFBRSxxQkFBcUIsRUFBRSxDQUFDLENBQUM7YUFDbEQ7UUFDTCxDQUFDLEVBQUM7YUFDRCxFQUFFLENBQUMsU0FBUzs7OztRQUFFLENBQUMsSUFBSSxFQUFFLEVBQUU7WUFDcEIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNsQyxJQUFJLE9BQU8sRUFBRTtnQkFDVCxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7YUFDakM7UUFDTCxDQUFDLEVBQUM7YUFDRCxLQUFLOzs7O1FBQUMsQ0FBQyxHQUFHLEVBQUUsRUFBRTtRQUNmLENBQUMsRUFBQyxDQUFDO1FBRVAsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQzs7Ozs7O0lBRU8sZ0JBQWdCLENBQUMsSUFBZTtRQUNwQyxJQUFJLElBQUksRUFBRTtZQUNOLElBQUksQ0FBQyxNQUFNLEdBQUcsZ0JBQWdCLENBQUMsUUFBUSxDQUFDOztrQkFDbEMsS0FBSyxHQUFHLElBQUksZUFBZSxDQUFDLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7WUFDbEUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUN2QztJQUNMLENBQUM7Ozs7Ozs7SUFFTyxnQkFBZ0IsQ0FBQyxJQUFlLEVBQUUsUUFBNEI7UUFDbEUsSUFBSSxJQUFJLEVBQUU7WUFDTixJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztZQUN6QixJQUFJLENBQUMsTUFBTSxHQUFHLGdCQUFnQixDQUFDLFFBQVEsQ0FBQzs7a0JBRWxDLEtBQUssR0FBRyxJQUFJLGVBQWUsQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsUUFBUSxDQUFDO1lBQ2xFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDdkM7SUFDTCxDQUFDOzs7Ozs7O0lBRU8sYUFBYSxDQUFDLElBQWUsRUFBRSxLQUFVO1FBQzdDLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxDQUFDLFNBQVMsR0FBRyxDQUFFLEtBQUssSUFBSSxFQUFFLENBQUUsQ0FBQyxNQUFNLENBQUM7WUFDeEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUM7WUFDckMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDOztrQkFFWixPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO1lBQ25DLElBQUksT0FBTyxFQUFFO2dCQUNULE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDOUI7O2tCQUVLLEtBQUssR0FBRyxJQUFJLG9CQUFvQixDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUNwRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNwQztJQUNMLENBQUM7Ozs7Ozs7SUFFTyxnQkFBZ0IsQ0FBQyxJQUFlLEVBQUUsSUFBUztRQUMvQyxJQUFJLElBQUksRUFBRTtZQUNOLElBQUksQ0FBQyxNQUFNLEdBQUcsZ0JBQWdCLENBQUMsUUFBUSxDQUFDO1lBQ3hDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1lBQ2pCLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQzs7a0JBRWYsT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztZQUNuQyxJQUFJLE9BQU8sRUFBRTtnQkFDVCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQzlCOztrQkFFSyxLQUFLLEdBQUcsSUFBSSx1QkFBdUIsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUM1RixJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3ZDO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sZUFBZSxDQUFDLElBQWU7UUFDbkMsSUFBSSxJQUFJLEVBQUU7WUFDTixJQUFJLENBQUMsTUFBTSxHQUFHLGdCQUFnQixDQUFDLE9BQU8sQ0FBQztZQUN2QyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7O2tCQUVkLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUM7WUFDbkMsSUFBSSxPQUFPLEVBQUU7Z0JBQ1QsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUM5Qjs7a0JBRUssS0FBSyxHQUFHLElBQUksZUFBZSxDQUFDLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxPQUFPLENBQUM7WUFDakUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNuQyxPQUFPLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDbEI7SUFDTCxDQUFDOzs7Ozs7SUFFTyxpQkFBaUIsQ0FBQyxJQUFlO1FBQ3JDLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxDQUFDLE1BQU0sR0FBRyxnQkFBZ0IsQ0FBQyxTQUFTLENBQUM7O2tCQUVuQyxLQUFLLEdBQUcsSUFBSSxlQUFlLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLFNBQVMsQ0FBQztZQUNuRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQ3hDO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sZUFBZSxDQUFDLElBQWU7UUFDbkMsSUFBSSxJQUFJLEVBQUU7WUFDTixJQUFJLENBQUMsTUFBTSxHQUFHLGdCQUFnQixDQUFDLE9BQU8sQ0FBQztZQUN2QyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7O2tCQUVmLEtBQUssR0FBRyxJQUFJLHFCQUFxQixDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDO1lBQ2pFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDdEM7SUFDTCxDQUFDOzs7Ozs7SUFFTyxTQUFTLENBQUMsSUFBSTs7Y0FDWixPQUFPLEdBQUc7WUFDWixDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQzs7O1lBQUUsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFBO1lBQzlELENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDOzs7WUFBRSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxDQUFBO1lBQzVELENBQUMsZ0JBQWdCLENBQUMsS0FBSyxDQUFDOzs7WUFBRSxHQUFHLEVBQUUsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQTtTQUNqRTtRQUVELE9BQU8sT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNoQyxDQUFDOzs7WUE1U0osVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBSlEsa0JBQWtCO1lBUmxCLGdCQUFnQjs7Ozs7Ozs7SUFlckIsOEJBQTJDOzs7OztJQUMzQyxzQ0FBa0M7Ozs7O0lBQ2xDLHFDQUFpQzs7Ozs7SUFDakMsbUNBQStCOzs7OztJQUMvQix5Q0FBd0M7Ozs7O0lBQ3hDLHdDQUFvQzs7SUFFcEMsbUNBQWdDOztJQUNoQyw4QkFBd0I7O0lBRXhCLHFDQUFnRTs7SUFDaEUsbUNBQXNFOztJQUN0RSwyQ0FBOEU7O0lBQzlFLDRDQUErRTs7SUFDL0UsMkNBQThFOztJQUM5RSwwQ0FBNkU7O0lBQzdFLHdDQUFxRjs7SUFDckYsMkNBQThGOztJQUM5RiwwQ0FBeUY7O0lBQ3pGLG9DQUFxRDs7Ozs7SUFFekMsbUNBQXdDOzs7OztJQUFFLHlDQUEwQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBFdmVudEVtaXR0ZXIsIEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWluaW1hdGNoIH0gZnJvbSAnbWluaW1hdGNoLWJyb3dzZXInO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7XHJcbiAgICBGaWxlVXBsb2FkQ29tcGxldGVFdmVudCxcclxuICAgIEZpbGVVcGxvYWREZWxldGVFdmVudCxcclxuICAgIEZpbGVVcGxvYWRFcnJvckV2ZW50LFxyXG4gICAgRmlsZVVwbG9hZEV2ZW50XHJcbn0gZnJvbSAnLi4vZXZlbnRzL2ZpbGUuZXZlbnQnO1xyXG5pbXBvcnQgeyBGaWxlTW9kZWwsIEZpbGVVcGxvYWRQcm9ncmVzcywgRmlsZVVwbG9hZFN0YXR1cyB9IGZyb20gJy4uL21vZGVscy9maWxlLm1vZGVsJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFVwbG9hZFNlcnZpY2Uge1xyXG5cclxuICAgIHByaXZhdGUgY2FjaGU6IHsgW2tleTogc3RyaW5nXTogYW55IH0gPSB7fTtcclxuICAgIHByaXZhdGUgdG90YWxDb21wbGV0ZTogbnVtYmVyID0gMDtcclxuICAgIHByaXZhdGUgdG90YWxBYm9ydGVkOiBudW1iZXIgPSAwO1xyXG4gICAgcHJpdmF0ZSB0b3RhbEVycm9yOiBudW1iZXIgPSAwO1xyXG4gICAgcHJpdmF0ZSBleGNsdWRlZEZpbGVMaXN0OiBzdHJpbmdbXSA9IFtdO1xyXG4gICAgcHJpdmF0ZSBtYXRjaGluZ09wdGlvbnM6IGFueSA9IG51bGw7XHJcblxyXG4gICAgYWN0aXZlVGFzazogUHJvbWlzZTxhbnk+ID0gbnVsbDtcclxuICAgIHF1ZXVlOiBGaWxlTW9kZWxbXSA9IFtdO1xyXG5cclxuICAgIHF1ZXVlQ2hhbmdlZDogU3ViamVjdDxGaWxlTW9kZWxbXT4gPSBuZXcgU3ViamVjdDxGaWxlTW9kZWxbXT4oKTtcclxuICAgIGZpbGVVcGxvYWQ6IFN1YmplY3Q8RmlsZVVwbG9hZEV2ZW50PiA9IG5ldyBTdWJqZWN0PEZpbGVVcGxvYWRFdmVudD4oKTtcclxuICAgIGZpbGVVcGxvYWRTdGFydGluZzogU3ViamVjdDxGaWxlVXBsb2FkRXZlbnQ+ID0gbmV3IFN1YmplY3Q8RmlsZVVwbG9hZEV2ZW50PigpO1xyXG4gICAgZmlsZVVwbG9hZENhbmNlbGxlZDogU3ViamVjdDxGaWxlVXBsb2FkRXZlbnQ+ID0gbmV3IFN1YmplY3Q8RmlsZVVwbG9hZEV2ZW50PigpO1xyXG4gICAgZmlsZVVwbG9hZFByb2dyZXNzOiBTdWJqZWN0PEZpbGVVcGxvYWRFdmVudD4gPSBuZXcgU3ViamVjdDxGaWxlVXBsb2FkRXZlbnQ+KCk7XHJcbiAgICBmaWxlVXBsb2FkQWJvcnRlZDogU3ViamVjdDxGaWxlVXBsb2FkRXZlbnQ+ID0gbmV3IFN1YmplY3Q8RmlsZVVwbG9hZEV2ZW50PigpO1xyXG4gICAgZmlsZVVwbG9hZEVycm9yOiBTdWJqZWN0PEZpbGVVcGxvYWRFcnJvckV2ZW50PiA9IG5ldyBTdWJqZWN0PEZpbGVVcGxvYWRFcnJvckV2ZW50PigpO1xyXG4gICAgZmlsZVVwbG9hZENvbXBsZXRlOiBTdWJqZWN0PEZpbGVVcGxvYWRDb21wbGV0ZUV2ZW50PiA9IG5ldyBTdWJqZWN0PEZpbGVVcGxvYWRDb21wbGV0ZUV2ZW50PigpO1xyXG4gICAgZmlsZVVwbG9hZERlbGV0ZWQ6IFN1YmplY3Q8RmlsZVVwbG9hZERlbGV0ZUV2ZW50PiA9IG5ldyBTdWJqZWN0PEZpbGVVcGxvYWREZWxldGVFdmVudD4oKTtcclxuICAgIGZpbGVEZWxldGVkOiBTdWJqZWN0PHN0cmluZz4gPSBuZXcgU3ViamVjdDxzdHJpbmc+KCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvdGVjdGVkIGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSwgcHJpdmF0ZSBhcHBDb25maWdTZXJ2aWNlOiBBcHBDb25maWdTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVja3Mgd2hldGhlciB0aGUgc2VydmljZSBpcyB1cGxvYWRpbmcgYSBmaWxlLlxyXG4gICAgICogQHJldHVybnMgVHJ1ZSBpZiBhIGZpbGUgaXMgdXBsb2FkaW5nLCBmYWxzZSBvdGhlcndpc2VcclxuICAgICAqL1xyXG4gICAgaXNVcGxvYWRpbmcoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYWN0aXZlVGFzayA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIGZpbGUgUXVldWVcclxuICAgICAqIEByZXR1cm5zIEFycmF5IG9mIGZpbGVzIHRoYXQgZm9ybSB0aGUgcXVldWVcclxuICAgICAqL1xyXG4gICAgZ2V0UXVldWUoKTogRmlsZU1vZGVsW10ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnF1ZXVlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQWRkcyBmaWxlcyB0byB0aGUgdXBsb2FkaW5nIHF1ZXVlIHRvIGJlIHVwbG9hZGVkXHJcbiAgICAgKiBAcGFyYW0gZmlsZXMgT25lIG9yIG1vcmUgc2VwYXJhdGUgcGFyYW1ldGVycyBvciBhbiBhcnJheSBvZiBmaWxlcyB0byBxdWV1ZVxyXG4gICAgICogQHJldHVybnMgQXJyYXkgb2YgZmlsZXMgdGhhdCB3ZXJlIG5vdCBibG9ja2VkIGZyb20gdXBsb2FkIGJ5IHRoZSBpZ25vcmUgbGlzdFxyXG4gICAgICovXHJcbiAgICBhZGRUb1F1ZXVlKC4uLmZpbGVzOiBGaWxlTW9kZWxbXSk6IEZpbGVNb2RlbFtdIHtcclxuICAgICAgICBjb25zdCBhbGxvd2VkRmlsZXMgPSBmaWxlcy5maWx0ZXIoKGN1cnJlbnRGaWxlKSA9PiB0aGlzLmZpbHRlckVsZW1lbnQoY3VycmVudEZpbGUpKTtcclxuICAgICAgICB0aGlzLnF1ZXVlID0gdGhpcy5xdWV1ZS5jb25jYXQoYWxsb3dlZEZpbGVzKTtcclxuICAgICAgICB0aGlzLnF1ZXVlQ2hhbmdlZC5uZXh0KHRoaXMucXVldWUpO1xyXG4gICAgICAgIHJldHVybiBhbGxvd2VkRmlsZXM7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBmaWx0ZXJFbGVtZW50KGZpbGU6IEZpbGVNb2RlbCkge1xyXG4gICAgICAgIGxldCBpc0FsbG93ZWQgPSB0cnVlO1xyXG5cclxuICAgICAgICB0aGlzLmV4Y2x1ZGVkRmlsZUxpc3QgPSA8c3RyaW5nW10+IHRoaXMuYXBwQ29uZmlnU2VydmljZS5nZXQoJ2ZpbGVzLmV4Y2x1ZGVkJyk7XHJcbiAgICAgICAgaWYgKHRoaXMuZXhjbHVkZWRGaWxlTGlzdCkge1xyXG5cclxuICAgICAgICAgICAgdGhpcy5tYXRjaGluZ09wdGlvbnMgPSB0aGlzLmFwcENvbmZpZ1NlcnZpY2UuZ2V0KCdmaWxlcy5tYXRjaC1vcHRpb25zJyk7XHJcblxyXG4gICAgICAgICAgICBpc0FsbG93ZWQgPSB0aGlzLmV4Y2x1ZGVkRmlsZUxpc3QuZmlsdGVyKChwYXR0ZXJuKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBtaW5pbWF0Y2ggPSBuZXcgTWluaW1hdGNoKHBhdHRlcm4sIHRoaXMubWF0Y2hpbmdPcHRpb25zKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBtaW5pbWF0Y2gubWF0Y2goZmlsZS5uYW1lKTtcclxuICAgICAgICAgICAgfSkubGVuZ3RoID09PSAwO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gaXNBbGxvd2VkO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRmluZHMgYWxsIHRoZSBmaWxlcyBpbiB0aGUgcXVldWUgdGhhdCBhcmUgbm90IHlldCB1cGxvYWRlZCBhbmQgdXBsb2FkcyB0aGVtIGludG8gdGhlIGRpcmVjdG9yeSBmb2xkZXIuXHJcbiAgICAgKiBAcGFyYW0gZW1pdHRlciBFbWl0dGVyIHRvIGludm9rZSBvbiBmaWxlIHN0YXR1cyBjaGFuZ2VcclxuICAgICAqL1xyXG4gICAgdXBsb2FkRmlsZXNJblRoZVF1ZXVlKGVtaXR0ZXI/OiBFdmVudEVtaXR0ZXI8YW55Pik6IHZvaWQge1xyXG4gICAgICAgIGlmICghdGhpcy5hY3RpdmVUYXNrKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGZpbGUgPSB0aGlzLnF1ZXVlLmZpbmQoKGN1cnJlbnRGaWxlKSA9PiBjdXJyZW50RmlsZS5zdGF0dXMgPT09IEZpbGVVcGxvYWRTdGF0dXMuUGVuZGluZyk7XHJcbiAgICAgICAgICAgIGlmIChmaWxlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9uVXBsb2FkU3RhcnRpbmcoZmlsZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgcHJvbWlzZSA9IHRoaXMuYmVnaW5VcGxvYWQoZmlsZSwgZW1pdHRlcik7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmFjdGl2ZVRhc2sgPSBwcm9taXNlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jYWNoZVtmaWxlLmlkXSA9IHByb21pc2U7XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgbmV4dCA9ICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFjdGl2ZVRhc2sgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4gdGhpcy51cGxvYWRGaWxlc0luVGhlUXVldWUoZW1pdHRlciksIDEwMCk7XHJcbiAgICAgICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgICAgIHByb21pc2UubmV4dCA9IG5leHQ7XHJcblxyXG4gICAgICAgICAgICAgICAgcHJvbWlzZS50aGVuKFxyXG4gICAgICAgICAgICAgICAgICAgICgpID0+IG5leHQoKSxcclxuICAgICAgICAgICAgICAgICAgICAoKSA9PiBuZXh0KClcclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDYW5jZWxzIHVwbG9hZGluZyBvZiBmaWxlcy5cclxuICAgICAqIEBwYXJhbSBmaWxlcyBPbmUgb3IgbW9yZSBzZXBhcmF0ZSBwYXJhbWV0ZXJzIG9yIGFuIGFycmF5IG9mIGZpbGVzIHNwZWNpZnlpbmcgdXBsb2FkcyB0byBjYW5jZWxcclxuICAgICAqL1xyXG4gICAgY2FuY2VsVXBsb2FkKC4uLmZpbGVzOiBGaWxlTW9kZWxbXSkge1xyXG4gICAgICAgIGZpbGVzLmZvckVhY2goKGZpbGUpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgcHJvbWlzZSA9IHRoaXMuY2FjaGVbZmlsZS5pZF07XHJcblxyXG4gICAgICAgICAgICBpZiAocHJvbWlzZSkge1xyXG4gICAgICAgICAgICAgICAgcHJvbWlzZS5hYm9ydCgpO1xyXG4gICAgICAgICAgICAgICAgZGVsZXRlIHRoaXMuY2FjaGVbZmlsZS5pZF07XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBwZXJmb3JtQWN0aW9uID0gdGhpcy5nZXRBY3Rpb24oZmlsZSk7XHJcbiAgICAgICAgICAgICAgICBwZXJmb3JtQWN0aW9uKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKiogQ2xlYXJzIHRoZSB1cGxvYWQgcXVldWUgKi9cclxuICAgIGNsZWFyUXVldWUoKSB7XHJcbiAgICAgICAgdGhpcy5xdWV1ZSA9IFtdO1xyXG4gICAgICAgIHRoaXMudG90YWxDb21wbGV0ZSA9IDA7XHJcbiAgICAgICAgdGhpcy50b3RhbEFib3J0ZWQgPSAwO1xyXG4gICAgICAgIHRoaXMudG90YWxFcnJvciA9IDA7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGFuIHVwbG9hZCBwcm9taXNlIGZvciBhIGZpbGUuXHJcbiAgICAgKiBAcGFyYW0gZmlsZSBUaGUgdGFyZ2V0IGZpbGVcclxuICAgICAqIEByZXR1cm5zIFByb21pc2UgdGhhdCBpcyByZXNvbHZlZCBpZiB0aGUgdXBsb2FkIGlzIHN1Y2Nlc3NmdWwgb3IgZXJyb3Igb3RoZXJ3aXNlXHJcbiAgICAgKi9cclxuICAgIGdldFVwbG9hZFByb21pc2UoZmlsZTogRmlsZU1vZGVsKTogYW55IHtcclxuICAgICAgICBjb25zdCBvcHRzOiBhbnkgPSB7XHJcbiAgICAgICAgICAgIHJlbmRpdGlvbnM6ICdkb2NsaWInLFxyXG4gICAgICAgICAgICBpbmNsdWRlOiBbJ2FsbG93YWJsZU9wZXJhdGlvbnMnXVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGlmIChmaWxlLm9wdGlvbnMubmV3VmVyc2lvbiA9PT0gdHJ1ZSkge1xyXG4gICAgICAgICAgICBvcHRzLm92ZXJ3cml0ZSA9IHRydWU7XHJcbiAgICAgICAgICAgIG9wdHMubWFqb3JWZXJzaW9uID0gZmlsZS5vcHRpb25zLm1ham9yVmVyc2lvbjtcclxuICAgICAgICAgICAgb3B0cy5jb21tZW50ID0gZmlsZS5vcHRpb25zLmNvbW1lbnQ7XHJcbiAgICAgICAgICAgIG9wdHMubmFtZSA9IGZpbGUubmFtZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBvcHRzLmF1dG9SZW5hbWUgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGZpbGUub3B0aW9ucy5ub2RlVHlwZSkge1xyXG4gICAgICAgICAgICBvcHRzLm5vZGVUeXBlID0gZmlsZS5vcHRpb25zLm5vZGVUeXBlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGZpbGUuaWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLm5vZGUudXBkYXRlTm9kZUNvbnRlbnQoXHJcbiAgICAgICAgICAgICAgICBmaWxlLmlkLFxyXG4gICAgICAgICAgICAgICAgZmlsZS5maWxlLFxyXG4gICAgICAgICAgICAgICAgb3B0c1xyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS51cGxvYWQudXBsb2FkRmlsZShcclxuICAgICAgICAgICAgICAgIGZpbGUuZmlsZSxcclxuICAgICAgICAgICAgICAgIGZpbGUub3B0aW9ucy5wYXRoLFxyXG4gICAgICAgICAgICAgICAgZmlsZS5vcHRpb25zLnBhcmVudElkLFxyXG4gICAgICAgICAgICAgICAgZmlsZS5vcHRpb25zLFxyXG4gICAgICAgICAgICAgICAgb3B0c1xyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGJlZ2luVXBsb2FkKGZpbGU6IEZpbGVNb2RlbCwgZW1pdHRlcjogRXZlbnRFbWl0dGVyPGFueT4pOiBhbnkge1xyXG5cclxuICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5nZXRVcGxvYWRQcm9taXNlKGZpbGUpO1xyXG5cclxuICAgICAgICBwcm9taXNlLm9uKCdwcm9ncmVzcycsIChwcm9ncmVzczogRmlsZVVwbG9hZFByb2dyZXNzKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMub25VcGxvYWRQcm9ncmVzcyhmaWxlLCBwcm9ncmVzcyk7XHJcbiAgICAgICAgfSlcclxuICAgICAgICAgICAgLm9uKCdhYm9ydCcsICgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMub25VcGxvYWRBYm9ydGVkKGZpbGUpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGVtaXR0ZXIpIHtcclxuICAgICAgICAgICAgICAgICAgICBlbWl0dGVyLmVtaXQoeyB2YWx1ZTogJ0ZpbGUgYWJvcnRlZCcgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5vbignZXJyb3InLCAoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9uVXBsb2FkRXJyb3IoZmlsZSwgZXJyKTtcclxuICAgICAgICAgICAgICAgIGlmIChlbWl0dGVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZW1pdHRlci5lbWl0KHsgdmFsdWU6ICdFcnJvciBmaWxlIHVwbG9hZGVkJyB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLm9uKCdzdWNjZXNzJywgKGRhdGEpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMub25VcGxvYWRDb21wbGV0ZShmaWxlLCBkYXRhKTtcclxuICAgICAgICAgICAgICAgIGlmIChlbWl0dGVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZW1pdHRlci5lbWl0KHsgdmFsdWU6IGRhdGEgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5jYXRjaCgoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gcHJvbWlzZTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG9uVXBsb2FkU3RhcnRpbmcoZmlsZTogRmlsZU1vZGVsKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKGZpbGUpIHtcclxuICAgICAgICAgICAgZmlsZS5zdGF0dXMgPSBGaWxlVXBsb2FkU3RhdHVzLlN0YXJ0aW5nO1xyXG4gICAgICAgICAgICBjb25zdCBldmVudCA9IG5ldyBGaWxlVXBsb2FkRXZlbnQoZmlsZSwgRmlsZVVwbG9hZFN0YXR1cy5TdGFydGluZyk7XHJcbiAgICAgICAgICAgIHRoaXMuZmlsZVVwbG9hZC5uZXh0KGV2ZW50KTtcclxuICAgICAgICAgICAgdGhpcy5maWxlVXBsb2FkU3RhcnRpbmcubmV4dChldmVudCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25VcGxvYWRQcm9ncmVzcyhmaWxlOiBGaWxlTW9kZWwsIHByb2dyZXNzOiBGaWxlVXBsb2FkUHJvZ3Jlc3MpOiB2b2lkIHtcclxuICAgICAgICBpZiAoZmlsZSkge1xyXG4gICAgICAgICAgICBmaWxlLnByb2dyZXNzID0gcHJvZ3Jlc3M7XHJcbiAgICAgICAgICAgIGZpbGUuc3RhdHVzID0gRmlsZVVwbG9hZFN0YXR1cy5Qcm9ncmVzcztcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50ID0gbmV3IEZpbGVVcGxvYWRFdmVudChmaWxlLCBGaWxlVXBsb2FkU3RhdHVzLlByb2dyZXNzKTtcclxuICAgICAgICAgICAgdGhpcy5maWxlVXBsb2FkLm5leHQoZXZlbnQpO1xyXG4gICAgICAgICAgICB0aGlzLmZpbGVVcGxvYWRQcm9ncmVzcy5uZXh0KGV2ZW50KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvblVwbG9hZEVycm9yKGZpbGU6IEZpbGVNb2RlbCwgZXJyb3I6IGFueSk6IHZvaWQge1xyXG4gICAgICAgIGlmIChmaWxlKSB7XHJcbiAgICAgICAgICAgIGZpbGUuZXJyb3JDb2RlID0gKCBlcnJvciB8fCB7fSApLnN0YXR1cztcclxuICAgICAgICAgICAgZmlsZS5zdGF0dXMgPSBGaWxlVXBsb2FkU3RhdHVzLkVycm9yO1xyXG4gICAgICAgICAgICB0aGlzLnRvdGFsRXJyb3IrKztcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLmNhY2hlW2ZpbGUuaWRdO1xyXG4gICAgICAgICAgICBpZiAocHJvbWlzZSkge1xyXG4gICAgICAgICAgICAgICAgZGVsZXRlIHRoaXMuY2FjaGVbZmlsZS5pZF07XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50ID0gbmV3IEZpbGVVcGxvYWRFcnJvckV2ZW50KGZpbGUsIGVycm9yLCB0aGlzLnRvdGFsRXJyb3IpO1xyXG4gICAgICAgICAgICB0aGlzLmZpbGVVcGxvYWQubmV4dChldmVudCk7XHJcbiAgICAgICAgICAgIHRoaXMuZmlsZVVwbG9hZEVycm9yLm5leHQoZXZlbnQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG9uVXBsb2FkQ29tcGxldGUoZmlsZTogRmlsZU1vZGVsLCBkYXRhOiBhbnkpOiB2b2lkIHtcclxuICAgICAgICBpZiAoZmlsZSkge1xyXG4gICAgICAgICAgICBmaWxlLnN0YXR1cyA9IEZpbGVVcGxvYWRTdGF0dXMuQ29tcGxldGU7XHJcbiAgICAgICAgICAgIGZpbGUuZGF0YSA9IGRhdGE7XHJcbiAgICAgICAgICAgIHRoaXMudG90YWxDb21wbGV0ZSsrO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgcHJvbWlzZSA9IHRoaXMuY2FjaGVbZmlsZS5pZF07XHJcbiAgICAgICAgICAgIGlmIChwcm9taXNlKSB7XHJcbiAgICAgICAgICAgICAgICBkZWxldGUgdGhpcy5jYWNoZVtmaWxlLmlkXTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgZXZlbnQgPSBuZXcgRmlsZVVwbG9hZENvbXBsZXRlRXZlbnQoZmlsZSwgdGhpcy50b3RhbENvbXBsZXRlLCBkYXRhLCB0aGlzLnRvdGFsQWJvcnRlZCk7XHJcbiAgICAgICAgICAgIHRoaXMuZmlsZVVwbG9hZC5uZXh0KGV2ZW50KTtcclxuICAgICAgICAgICAgdGhpcy5maWxlVXBsb2FkQ29tcGxldGUubmV4dChldmVudCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25VcGxvYWRBYm9ydGVkKGZpbGU6IEZpbGVNb2RlbCk6IHZvaWQge1xyXG4gICAgICAgIGlmIChmaWxlKSB7XHJcbiAgICAgICAgICAgIGZpbGUuc3RhdHVzID0gRmlsZVVwbG9hZFN0YXR1cy5BYm9ydGVkO1xyXG4gICAgICAgICAgICB0aGlzLnRvdGFsQWJvcnRlZCsrO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgcHJvbWlzZSA9IHRoaXMuY2FjaGVbZmlsZS5pZF07XHJcbiAgICAgICAgICAgIGlmIChwcm9taXNlKSB7XHJcbiAgICAgICAgICAgICAgICBkZWxldGUgdGhpcy5jYWNoZVtmaWxlLmlkXTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgZXZlbnQgPSBuZXcgRmlsZVVwbG9hZEV2ZW50KGZpbGUsIEZpbGVVcGxvYWRTdGF0dXMuQWJvcnRlZCk7XHJcbiAgICAgICAgICAgIHRoaXMuZmlsZVVwbG9hZC5uZXh0KGV2ZW50KTtcclxuICAgICAgICAgICAgdGhpcy5maWxlVXBsb2FkQWJvcnRlZC5uZXh0KGV2ZW50KTtcclxuICAgICAgICAgICAgcHJvbWlzZS5uZXh0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25VcGxvYWRDYW5jZWxsZWQoZmlsZTogRmlsZU1vZGVsKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKGZpbGUpIHtcclxuICAgICAgICAgICAgZmlsZS5zdGF0dXMgPSBGaWxlVXBsb2FkU3RhdHVzLkNhbmNlbGxlZDtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50ID0gbmV3IEZpbGVVcGxvYWRFdmVudChmaWxlLCBGaWxlVXBsb2FkU3RhdHVzLkNhbmNlbGxlZCk7XHJcbiAgICAgICAgICAgIHRoaXMuZmlsZVVwbG9hZC5uZXh0KGV2ZW50KTtcclxuICAgICAgICAgICAgdGhpcy5maWxlVXBsb2FkQ2FuY2VsbGVkLm5leHQoZXZlbnQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG9uVXBsb2FkRGVsZXRlZChmaWxlOiBGaWxlTW9kZWwpOiB2b2lkIHtcclxuICAgICAgICBpZiAoZmlsZSkge1xyXG4gICAgICAgICAgICBmaWxlLnN0YXR1cyA9IEZpbGVVcGxvYWRTdGF0dXMuRGVsZXRlZDtcclxuICAgICAgICAgICAgdGhpcy50b3RhbENvbXBsZXRlLS07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBldmVudCA9IG5ldyBGaWxlVXBsb2FkRGVsZXRlRXZlbnQoZmlsZSwgdGhpcy50b3RhbENvbXBsZXRlKTtcclxuICAgICAgICAgICAgdGhpcy5maWxlVXBsb2FkLm5leHQoZXZlbnQpO1xyXG4gICAgICAgICAgICB0aGlzLmZpbGVVcGxvYWREZWxldGVkLm5leHQoZXZlbnQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldEFjdGlvbihmaWxlKSB7XHJcbiAgICAgICAgY29uc3QgYWN0aW9ucyA9IHtcclxuICAgICAgICAgICAgW0ZpbGVVcGxvYWRTdGF0dXMuUGVuZGluZ106ICgpID0+IHRoaXMub25VcGxvYWRDYW5jZWxsZWQoZmlsZSksXHJcbiAgICAgICAgICAgIFtGaWxlVXBsb2FkU3RhdHVzLkRlbGV0ZWRdOiAoKSA9PiB0aGlzLm9uVXBsb2FkRGVsZXRlZChmaWxlKSxcclxuICAgICAgICAgICAgW0ZpbGVVcGxvYWRTdGF0dXMuRXJyb3JdOiAoKSA9PiB0aGlzLm9uVXBsb2FkRXJyb3IoZmlsZSwgbnVsbClcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gYWN0aW9uc1tmaWxlLnN0YXR1c107XHJcbiAgICB9XHJcbn1cclxuIl19