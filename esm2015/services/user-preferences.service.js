/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from 'rxjs';
import { AppConfigService, AppConfigValues } from '../app-config/app-config.service';
import { StorageService } from './storage.service';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { AlfrescoApiService } from './alfresco-api.service';
import * as i0 from "@angular/core";
import * as i1 from "@ngx-translate/core";
import * as i2 from "../app-config/app-config.service";
import * as i3 from "./storage.service";
import * as i4 from "./alfresco-api.service";
/** @enum {string} */
const UserPreferenceValues = {
    PaginationSize: 'paginationSize',
    Locale: 'locale',
    SupportedPageSizes: 'supportedPageSizes',
    ExpandedSideNavStatus: 'expandedSidenav',
};
export { UserPreferenceValues };
export class UserPreferencesService {
    /**
     * @param {?} translate
     * @param {?} appConfig
     * @param {?} storage
     * @param {?} alfrescoApiService
     */
    constructor(translate, appConfig, storage, alfrescoApiService) {
        this.translate = translate;
        this.appConfig = appConfig;
        this.storage = storage;
        this.alfrescoApiService = alfrescoApiService;
        this.defaults = {
            paginationSize: 25,
            supportedPageSizes: [5, 10, 15, 20],
            locale: 'en',
            expandedSidenav: true
        };
        this.userPreferenceStatus = this.defaults;
        this.alfrescoApiService.alfrescoApiInitialized.subscribe(this.initUserPreferenceStatus.bind(this));
        this.onChangeSubject = new BehaviorSubject(this.userPreferenceStatus);
        this.onChange = this.onChangeSubject.asObservable();
    }
    /**
     * @private
     * @return {?}
     */
    initUserPreferenceStatus() {
        this.initUserLanguage();
        this.set(UserPreferenceValues.PaginationSize, this.paginationSize);
        this.set(UserPreferenceValues.SupportedPageSizes, JSON.stringify(this.supportedPageSizes));
    }
    /**
     * @private
     * @return {?}
     */
    initUserLanguage() {
        if (this.locale || this.appConfig.get(UserPreferenceValues.Locale)) {
            /** @type {?} */
            const locale = this.locale || this.getDefaultLocale();
            this.set(UserPreferenceValues.Locale, locale);
            this.set('textOrientation', this.getLanguageByKey(locale).direction || 'ltr');
        }
        else {
            /** @type {?} */
            const locale = this.locale || this.getDefaultLocale();
            this.setWithoutStore(UserPreferenceValues.Locale, locale);
            this.setWithoutStore('textOrientation', this.getLanguageByKey(locale).direction || 'ltr');
        }
    }
    /**
     * Sets up a callback to notify when a property has changed.
     * @param {?} property The property to watch
     * @return {?} Notification callback
     */
    select(property) {
        return this.onChange
            .pipe(map((/**
         * @param {?} userPreferenceStatus
         * @return {?}
         */
        (userPreferenceStatus) => userPreferenceStatus[property])), distinctUntilChanged());
    }
    /**
     * Gets a preference property.
     * @param {?} property Name of the property
     * @param {?=} defaultValue Default to return if the property is not found
     * @return {?} Preference property
     */
    get(property, defaultValue) {
        /** @type {?} */
        const key = this.getPropertyKey(property);
        /** @type {?} */
        const value = this.storage.getItem(key);
        if (value === undefined || value === null) {
            return defaultValue;
        }
        return value;
    }
    /**
     * Sets a preference property.
     * @param {?} property Name of the property
     * @param {?} value New value for the property
     * @return {?}
     */
    set(property, value) {
        if (!property) {
            return;
        }
        this.storage.setItem(this.getPropertyKey(property), value);
        this.userPreferenceStatus[property] = value;
        this.onChangeSubject.next(this.userPreferenceStatus);
    }
    /**
     * Sets a preference property.
     * @param {?} property Name of the property
     * @param {?} value New value for the property
     * @return {?}
     */
    setWithoutStore(property, value) {
        if (!property) {
            return;
        }
        this.userPreferenceStatus[property] = value;
        this.onChangeSubject.next(this.userPreferenceStatus);
    }
    /**
     * Check if an item is present in the storage
     * @param {?} property Name of the property
     * @return {?} True if the item is present, false otherwise
     */
    hasItem(property) {
        if (!property) {
            return;
        }
        return this.storage.hasItem(this.getPropertyKey(property));
    }
    /**
     * Gets the active storage prefix for preferences.
     * @return {?} Storage prefix
     */
    getStoragePrefix() {
        return this.storage.getItem('USER_PROFILE') || 'GUEST';
    }
    /**
     * Sets the active storage prefix for preferences.
     * @param {?} value Name of the prefix
     * @return {?}
     */
    setStoragePrefix(value) {
        this.storage.setItem('USER_PROFILE', value || 'GUEST');
        this.initUserPreferenceStatus();
    }
    /**
     * Gets the full property key with prefix.
     * @param {?} property The property name
     * @return {?} Property key
     */
    getPropertyKey(property) {
        return `${this.getStoragePrefix()}__${property}`;
    }
    /**
     * Gets an array containing the available page sizes.
     * @return {?} Array of page size values
     */
    get supportedPageSizes() {
        /** @type {?} */
        const supportedPageSizes = this.get(UserPreferenceValues.SupportedPageSizes);
        if (supportedPageSizes) {
            return JSON.parse(supportedPageSizes);
        }
        else {
            return this.appConfig.get('pagination.supportedPageSizes', this.defaults.supportedPageSizes);
        }
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set supportedPageSizes(value) {
        this.set(UserPreferenceValues.SupportedPageSizes, JSON.stringify(value));
    }
    /**
     * Pagination size.
     * @param {?} value
     * @return {?}
     */
    set paginationSize(value) {
        this.set(UserPreferenceValues.PaginationSize, value);
    }
    /**
     * @return {?}
     */
    get paginationSize() {
        /** @type {?} */
        const paginationSize = this.get(UserPreferenceValues.PaginationSize);
        if (paginationSize) {
            return Number(paginationSize);
        }
        else {
            return Number(this.appConfig.get('pagination.size', this.defaults.paginationSize));
        }
    }
    /**
     * Current locale setting.
     * @return {?}
     */
    get locale() {
        return this.get(UserPreferenceValues.Locale);
    }
    /**
     * @param {?} value
     * @return {?}
     */
    set locale(value) {
        this.set(UserPreferenceValues.Locale, value);
    }
    /**
     * Gets the default locale.
     * @return {?} Default locale language code
     */
    getDefaultLocale() {
        return this.appConfig.get(UserPreferenceValues.Locale) || this.translate.getBrowserCultureLang() || 'en';
    }
    /**
     * @private
     * @param {?} key
     * @return {?}
     */
    getLanguageByKey(key) {
        return (this.appConfig
            .get(AppConfigValues.APP_CONFIG_LANGUAGES_KEY, [(/** @type {?} */ ({ key: 'en' }))])
            .find((/**
         * @param {?} language
         * @return {?}
         */
        (language) => key.includes(language.key))) || (/** @type {?} */ ({ key: 'en' })));
    }
}
UserPreferencesService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
UserPreferencesService.ctorParameters = () => [
    { type: TranslateService },
    { type: AppConfigService },
    { type: StorageService },
    { type: AlfrescoApiService }
];
/** @nocollapse */ UserPreferencesService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function UserPreferencesService_Factory() { return new UserPreferencesService(i0.ɵɵinject(i1.TranslateService), i0.ɵɵinject(i2.AppConfigService), i0.ɵɵinject(i3.StorageService), i0.ɵɵinject(i4.AlfrescoApiService)); }, token: UserPreferencesService, providedIn: "root" });
if (false) {
    /** @type {?} */
    UserPreferencesService.prototype.defaults;
    /**
     * @type {?}
     * @private
     */
    UserPreferencesService.prototype.userPreferenceStatus;
    /**
     * @type {?}
     * @private
     */
    UserPreferencesService.prototype.onChangeSubject;
    /** @type {?} */
    UserPreferencesService.prototype.onChange;
    /** @type {?} */
    UserPreferencesService.prototype.translate;
    /**
     * @type {?}
     * @private
     */
    UserPreferencesService.prototype.appConfig;
    /**
     * @type {?}
     * @private
     */
    UserPreferencesService.prototype.storage;
    /**
     * @type {?}
     * @private
     */
    UserPreferencesService.prototype.alfrescoApiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1wcmVmZXJlbmNlcy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvdXNlci1wcmVmZXJlbmNlcy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdkQsT0FBTyxFQUFjLGVBQWUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNuRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsZUFBZSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFFckYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQzs7Ozs7Ozs7SUFHeEQsZ0JBQWlCLGdCQUFnQjtJQUNqQyxRQUFTLFFBQVE7SUFDakIsb0JBQXFCLG9CQUFvQjtJQUN6Qyx1QkFBd0IsaUJBQWlCOzs7QUFNN0MsTUFBTSxPQUFPLHNCQUFzQjs7Ozs7OztJQWEvQixZQUFtQixTQUEyQixFQUMxQixTQUEyQixFQUMzQixPQUF1QixFQUN2QixrQkFBc0M7UUFIdkMsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFDMUIsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFDM0IsWUFBTyxHQUFQLE9BQU8sQ0FBZ0I7UUFDdkIsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQWQxRCxhQUFRLEdBQUc7WUFDUCxjQUFjLEVBQUUsRUFBRTtZQUNsQixrQkFBa0IsRUFBRSxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQztZQUNuQyxNQUFNLEVBQUUsSUFBSTtZQUNaLGVBQWUsRUFBRSxJQUFJO1NBQ3hCLENBQUM7UUFFTSx5QkFBb0IsR0FBUSxJQUFJLENBQUMsUUFBUSxDQUFDO1FBUTlDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1FBQ25HLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxlQUFlLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7UUFDdEUsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3hELENBQUM7Ozs7O0lBRU8sd0JBQXdCO1FBQzVCLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLGtCQUFrQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUMsQ0FBQztJQUMvRixDQUFDOzs7OztJQUVPLGdCQUFnQjtRQUNwQixJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQVMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLEVBQUU7O2tCQUNsRSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFFckQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDOUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxJQUFJLEtBQUssQ0FBQyxDQUFDO1NBQ2pGO2FBQU07O2tCQUNHLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUVyRCxJQUFJLENBQUMsZUFBZSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztZQUMxRCxJQUFJLENBQUMsZUFBZSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLElBQUksS0FBSyxDQUFDLENBQUM7U0FDN0Y7SUFDTCxDQUFDOzs7Ozs7SUFPRCxNQUFNLENBQUMsUUFBZ0I7UUFDbkIsT0FBTyxJQUFJLENBQUMsUUFBUTthQUNmLElBQUksQ0FDRCxHQUFHOzs7O1FBQUMsQ0FBQyxvQkFBb0IsRUFBRSxFQUFFLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLEVBQUMsRUFDN0Qsb0JBQW9CLEVBQUUsQ0FDekIsQ0FBQztJQUNWLENBQUM7Ozs7Ozs7SUFRRCxHQUFHLENBQUMsUUFBZ0IsRUFBRSxZQUFxQjs7Y0FDakMsR0FBRyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDOztjQUNuQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDO1FBQ3ZDLElBQUksS0FBSyxLQUFLLFNBQVMsSUFBSSxLQUFLLEtBQUssSUFBSSxFQUFFO1lBQ3ZDLE9BQU8sWUFBWSxDQUFDO1NBQ3ZCO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7Ozs7OztJQU9ELEdBQUcsQ0FBQyxRQUFnQixFQUFFLEtBQVU7UUFDNUIsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNYLE9BQU87U0FDVjtRQUNELElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUNoQixJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxFQUM3QixLQUFLLENBQ1IsQ0FBQztRQUNGLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsR0FBRyxLQUFLLENBQUM7UUFDNUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7SUFDekQsQ0FBQzs7Ozs7OztJQU9ELGVBQWUsQ0FBQyxRQUFnQixFQUFFLEtBQVU7UUFDeEMsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNYLE9BQU87U0FDVjtRQUNELElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsR0FBRyxLQUFLLENBQUM7UUFDNUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLG9CQUFvQixDQUFDLENBQUM7SUFDekQsQ0FBQzs7Ozs7O0lBT0QsT0FBTyxDQUFDLFFBQWdCO1FBQ3BCLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDWCxPQUFPO1NBQ1Y7UUFDRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUN2QixJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQyxDQUNoQyxDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFNRCxnQkFBZ0I7UUFDWixPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLGNBQWMsQ0FBQyxJQUFJLE9BQU8sQ0FBQztJQUMzRCxDQUFDOzs7Ozs7SUFNRCxnQkFBZ0IsQ0FBQyxLQUFhO1FBQzFCLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxLQUFLLElBQUksT0FBTyxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLHdCQUF3QixFQUFFLENBQUM7SUFDcEMsQ0FBQzs7Ozs7O0lBT0QsY0FBYyxDQUFDLFFBQWdCO1FBQzNCLE9BQU8sR0FBRyxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsS0FBSyxRQUFRLEVBQUUsQ0FBQztJQUNyRCxDQUFDOzs7OztJQU1ELElBQUksa0JBQWtCOztjQUNaLGtCQUFrQixHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsa0JBQWtCLENBQUM7UUFFNUUsSUFBSSxrQkFBa0IsRUFBRTtZQUNwQixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsa0JBQWtCLENBQUMsQ0FBQztTQUN6QzthQUFNO1lBQ0gsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQywrQkFBK0IsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUM7U0FDaEc7SUFDTCxDQUFDOzs7OztJQUVELElBQUksa0JBQWtCLENBQUMsS0FBZTtRQUNsQyxJQUFJLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLGtCQUFrQixFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUM3RSxDQUFDOzs7Ozs7SUFHRCxJQUFJLGNBQWMsQ0FBQyxLQUFhO1FBQzVCLElBQUksQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsY0FBYyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ3pELENBQUM7Ozs7SUFFRCxJQUFJLGNBQWM7O2NBQ1IsY0FBYyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsY0FBYyxDQUFDO1FBRXBFLElBQUksY0FBYyxFQUFFO1lBQ2hCLE9BQU8sTUFBTSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1NBQ2pDO2FBQU07WUFDSCxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7U0FDdEY7SUFDTCxDQUFDOzs7OztJQUdELElBQUksTUFBTTtRQUNOLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNqRCxDQUFDOzs7OztJQUVELElBQUksTUFBTSxDQUFDLEtBQWE7UUFDcEIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDLENBQUM7SUFDakQsQ0FBQzs7Ozs7SUFNTSxnQkFBZ0I7UUFDbkIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBUyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLHFCQUFxQixFQUFFLElBQUksSUFBSSxDQUFDO0lBQ3JILENBQUM7Ozs7OztJQUVPLGdCQUFnQixDQUFDLEdBQVc7UUFDaEMsT0FBTyxDQUNILElBQUksQ0FBQyxTQUFTO2FBQ1QsR0FBRyxDQUFzQixlQUFlLENBQUMsd0JBQXdCLEVBQUUsQ0FBQyxtQkFBZSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsRUFBQSxDQUFDLENBQUM7YUFDbEcsSUFBSTs7OztRQUFDLENBQUMsUUFBUSxFQUFFLEVBQUUsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBQyxJQUFJLG1CQUFlLEVBQUUsR0FBRyxFQUFFLElBQUksRUFBRSxFQUFBLENBQ3RGLENBQUM7SUFDTixDQUFDOzs7WUF2TUosVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBakJRLGdCQUFnQjtZQUVoQixnQkFBZ0I7WUFFaEIsY0FBYztZQUVkLGtCQUFrQjs7Ozs7SUFjdkIsMENBS0U7Ozs7O0lBRUYsc0RBQWtEOzs7OztJQUNsRCxpREFBOEM7O0lBQzlDLDBDQUEwQjs7SUFFZCwyQ0FBa0M7Ozs7O0lBQ2xDLDJDQUFtQzs7Ozs7SUFDbkMseUNBQStCOzs7OztJQUMvQixvREFBOEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVTZXJ2aWNlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIEJlaGF2aW9yU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBcHBDb25maWdTZXJ2aWNlLCBBcHBDb25maWdWYWx1ZXMgfSBmcm9tICcuLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IExhbmd1YWdlSXRlbSB9IGZyb20gJy4uL2xhbmd1YWdlLW1lbnUvbGFuZ3VhZ2UuaW50ZXJmYWNlJztcclxuaW1wb3J0IHsgU3RvcmFnZVNlcnZpY2UgfSBmcm9tICcuL3N0b3JhZ2Uuc2VydmljZSc7XHJcbmltcG9ydCB7IGRpc3RpbmN0VW50aWxDaGFuZ2VkLCBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4vYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5cclxuZXhwb3J0IGVudW0gVXNlclByZWZlcmVuY2VWYWx1ZXMge1xyXG4gICAgUGFnaW5hdGlvblNpemUgPSAncGFnaW5hdGlvblNpemUnLFxyXG4gICAgTG9jYWxlID0gJ2xvY2FsZScsXHJcbiAgICBTdXBwb3J0ZWRQYWdlU2l6ZXMgPSAnc3VwcG9ydGVkUGFnZVNpemVzJyxcclxuICAgIEV4cGFuZGVkU2lkZU5hdlN0YXR1cyA9ICdleHBhbmRlZFNpZGVuYXYnXHJcbn1cclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgVXNlclByZWZlcmVuY2VzU2VydmljZSB7XHJcblxyXG4gICAgZGVmYXVsdHMgPSB7XHJcbiAgICAgICAgcGFnaW5hdGlvblNpemU6IDI1LFxyXG4gICAgICAgIHN1cHBvcnRlZFBhZ2VTaXplczogWzUsIDEwLCAxNSwgMjBdLFxyXG4gICAgICAgIGxvY2FsZTogJ2VuJyxcclxuICAgICAgICBleHBhbmRlZFNpZGVuYXY6IHRydWVcclxuICAgIH07XHJcblxyXG4gICAgcHJpdmF0ZSB1c2VyUHJlZmVyZW5jZVN0YXR1czogYW55ID0gdGhpcy5kZWZhdWx0cztcclxuICAgIHByaXZhdGUgb25DaGFuZ2VTdWJqZWN0OiBCZWhhdmlvclN1YmplY3Q8YW55PjtcclxuICAgIG9uQ2hhbmdlOiBPYnNlcnZhYmxlPGFueT47XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIHRyYW5zbGF0ZTogVHJhbnNsYXRlU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgYXBwQ29uZmlnOiBBcHBDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBzdG9yYWdlOiBTdG9yYWdlU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgYWxmcmVzY29BcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UpIHtcclxuICAgICAgICB0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5hbGZyZXNjb0FwaUluaXRpYWxpemVkLnN1YnNjcmliZSh0aGlzLmluaXRVc2VyUHJlZmVyZW5jZVN0YXR1cy5iaW5kKHRoaXMpKTtcclxuICAgICAgICB0aGlzLm9uQ2hhbmdlU3ViamVjdCA9IG5ldyBCZWhhdmlvclN1YmplY3QodGhpcy51c2VyUHJlZmVyZW5jZVN0YXR1cyk7XHJcbiAgICAgICAgdGhpcy5vbkNoYW5nZSA9IHRoaXMub25DaGFuZ2VTdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaW5pdFVzZXJQcmVmZXJlbmNlU3RhdHVzKCkge1xyXG4gICAgICAgIHRoaXMuaW5pdFVzZXJMYW5ndWFnZSgpO1xyXG4gICAgICAgIHRoaXMuc2V0KFVzZXJQcmVmZXJlbmNlVmFsdWVzLlBhZ2luYXRpb25TaXplLCB0aGlzLnBhZ2luYXRpb25TaXplKTtcclxuICAgICAgICB0aGlzLnNldChVc2VyUHJlZmVyZW5jZVZhbHVlcy5TdXBwb3J0ZWRQYWdlU2l6ZXMsIEpTT04uc3RyaW5naWZ5KHRoaXMuc3VwcG9ydGVkUGFnZVNpemVzKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBpbml0VXNlckxhbmd1YWdlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmxvY2FsZSB8fCB0aGlzLmFwcENvbmZpZy5nZXQ8c3RyaW5nPihVc2VyUHJlZmVyZW5jZVZhbHVlcy5Mb2NhbGUpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGxvY2FsZSA9IHRoaXMubG9jYWxlIHx8IHRoaXMuZ2V0RGVmYXVsdExvY2FsZSgpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5zZXQoVXNlclByZWZlcmVuY2VWYWx1ZXMuTG9jYWxlLCBsb2NhbGUpO1xyXG4gICAgICAgICAgICB0aGlzLnNldCgndGV4dE9yaWVudGF0aW9uJywgdGhpcy5nZXRMYW5ndWFnZUJ5S2V5KGxvY2FsZSkuZGlyZWN0aW9uIHx8ICdsdHInKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBsb2NhbGUgPSB0aGlzLmxvY2FsZSB8fCB0aGlzLmdldERlZmF1bHRMb2NhbGUoKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuc2V0V2l0aG91dFN0b3JlKFVzZXJQcmVmZXJlbmNlVmFsdWVzLkxvY2FsZSwgbG9jYWxlKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRXaXRob3V0U3RvcmUoJ3RleHRPcmllbnRhdGlvbicsIHRoaXMuZ2V0TGFuZ3VhZ2VCeUtleShsb2NhbGUpLmRpcmVjdGlvbiB8fCAnbHRyJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0cyB1cCBhIGNhbGxiYWNrIHRvIG5vdGlmeSB3aGVuIGEgcHJvcGVydHkgaGFzIGNoYW5nZWQuXHJcbiAgICAgKiBAcGFyYW0gcHJvcGVydHkgVGhlIHByb3BlcnR5IHRvIHdhdGNoXHJcbiAgICAgKiBAcmV0dXJucyBOb3RpZmljYXRpb24gY2FsbGJhY2tcclxuICAgICAqL1xyXG4gICAgc2VsZWN0KHByb3BlcnR5OiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm9uQ2hhbmdlXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKCh1c2VyUHJlZmVyZW5jZVN0YXR1cykgPT4gdXNlclByZWZlcmVuY2VTdGF0dXNbcHJvcGVydHldKSxcclxuICAgICAgICAgICAgICAgIGRpc3RpbmN0VW50aWxDaGFuZ2VkKClcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBwcmVmZXJlbmNlIHByb3BlcnR5LlxyXG4gICAgICogQHBhcmFtIHByb3BlcnR5IE5hbWUgb2YgdGhlIHByb3BlcnR5XHJcbiAgICAgKiBAcGFyYW0gZGVmYXVsdFZhbHVlIERlZmF1bHQgdG8gcmV0dXJuIGlmIHRoZSBwcm9wZXJ0eSBpcyBub3QgZm91bmRcclxuICAgICAqIEByZXR1cm5zIFByZWZlcmVuY2UgcHJvcGVydHlcclxuICAgICAqL1xyXG4gICAgZ2V0KHByb3BlcnR5OiBzdHJpbmcsIGRlZmF1bHRWYWx1ZT86IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgY29uc3Qga2V5ID0gdGhpcy5nZXRQcm9wZXJ0eUtleShwcm9wZXJ0eSk7XHJcbiAgICAgICAgY29uc3QgdmFsdWUgPSB0aGlzLnN0b3JhZ2UuZ2V0SXRlbShrZXkpO1xyXG4gICAgICAgIGlmICh2YWx1ZSA9PT0gdW5kZWZpbmVkIHx8IHZhbHVlID09PSBudWxsKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBkZWZhdWx0VmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldHMgYSBwcmVmZXJlbmNlIHByb3BlcnR5LlxyXG4gICAgICogQHBhcmFtIHByb3BlcnR5IE5hbWUgb2YgdGhlIHByb3BlcnR5XHJcbiAgICAgKiBAcGFyYW0gdmFsdWUgTmV3IHZhbHVlIGZvciB0aGUgcHJvcGVydHlcclxuICAgICAqL1xyXG4gICAgc2V0KHByb3BlcnR5OiBzdHJpbmcsIHZhbHVlOiBhbnkpIHtcclxuICAgICAgICBpZiAoIXByb3BlcnR5KSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5zdG9yYWdlLnNldEl0ZW0oXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0UHJvcGVydHlLZXkocHJvcGVydHkpLFxyXG4gICAgICAgICAgICB2YWx1ZVxyXG4gICAgICAgICk7XHJcbiAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZVN0YXR1c1twcm9wZXJ0eV0gPSB2YWx1ZTtcclxuICAgICAgICB0aGlzLm9uQ2hhbmdlU3ViamVjdC5uZXh0KHRoaXMudXNlclByZWZlcmVuY2VTdGF0dXMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0cyBhIHByZWZlcmVuY2UgcHJvcGVydHkuXHJcbiAgICAgKiBAcGFyYW0gcHJvcGVydHkgTmFtZSBvZiB0aGUgcHJvcGVydHlcclxuICAgICAqIEBwYXJhbSB2YWx1ZSBOZXcgdmFsdWUgZm9yIHRoZSBwcm9wZXJ0eVxyXG4gICAgICovXHJcbiAgICBzZXRXaXRob3V0U3RvcmUocHJvcGVydHk6IHN0cmluZywgdmFsdWU6IGFueSkge1xyXG4gICAgICAgIGlmICghcHJvcGVydHkpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlU3RhdHVzW3Byb3BlcnR5XSA9IHZhbHVlO1xyXG4gICAgICAgIHRoaXMub25DaGFuZ2VTdWJqZWN0Lm5leHQodGhpcy51c2VyUHJlZmVyZW5jZVN0YXR1cyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVjayBpZiBhbiBpdGVtIGlzIHByZXNlbnQgaW4gdGhlIHN0b3JhZ2VcclxuICAgICAqIEBwYXJhbSBwcm9wZXJ0eSBOYW1lIG9mIHRoZSBwcm9wZXJ0eVxyXG4gICAgICogQHJldHVybnMgVHJ1ZSBpZiB0aGUgaXRlbSBpcyBwcmVzZW50LCBmYWxzZSBvdGhlcndpc2VcclxuICAgICAqL1xyXG4gICAgaGFzSXRlbShwcm9wZXJ0eTogc3RyaW5nKSB7XHJcbiAgICAgICAgaWYgKCFwcm9wZXJ0eSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLnN0b3JhZ2UuaGFzSXRlbShcclxuICAgICAgICAgICAgdGhpcy5nZXRQcm9wZXJ0eUtleShwcm9wZXJ0eSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgYWN0aXZlIHN0b3JhZ2UgcHJlZml4IGZvciBwcmVmZXJlbmNlcy5cclxuICAgICAqIEByZXR1cm5zIFN0b3JhZ2UgcHJlZml4XHJcbiAgICAgKi9cclxuICAgIGdldFN0b3JhZ2VQcmVmaXgoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zdG9yYWdlLmdldEl0ZW0oJ1VTRVJfUFJPRklMRScpIHx8ICdHVUVTVCc7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXRzIHRoZSBhY3RpdmUgc3RvcmFnZSBwcmVmaXggZm9yIHByZWZlcmVuY2VzLlxyXG4gICAgICogQHBhcmFtIHZhbHVlIE5hbWUgb2YgdGhlIHByZWZpeFxyXG4gICAgICovXHJcbiAgICBzZXRTdG9yYWdlUHJlZml4KHZhbHVlOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLnN0b3JhZ2Uuc2V0SXRlbSgnVVNFUl9QUk9GSUxFJywgdmFsdWUgfHwgJ0dVRVNUJyk7XHJcbiAgICAgICAgdGhpcy5pbml0VXNlclByZWZlcmVuY2VTdGF0dXMoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIGZ1bGwgcHJvcGVydHkga2V5IHdpdGggcHJlZml4LlxyXG4gICAgICogQHBhcmFtIHByb3BlcnR5IFRoZSBwcm9wZXJ0eSBuYW1lXHJcbiAgICAgKiBAcmV0dXJucyBQcm9wZXJ0eSBrZXlcclxuICAgICAqL1xyXG4gICAgZ2V0UHJvcGVydHlLZXkocHJvcGVydHk6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIGAke3RoaXMuZ2V0U3RvcmFnZVByZWZpeCgpfV9fJHtwcm9wZXJ0eX1gO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhbiBhcnJheSBjb250YWluaW5nIHRoZSBhdmFpbGFibGUgcGFnZSBzaXplcy5cclxuICAgICAqIEByZXR1cm5zIEFycmF5IG9mIHBhZ2Ugc2l6ZSB2YWx1ZXNcclxuICAgICAqL1xyXG4gICAgZ2V0IHN1cHBvcnRlZFBhZ2VTaXplcygpOiBudW1iZXJbXSB7XHJcbiAgICAgICAgY29uc3Qgc3VwcG9ydGVkUGFnZVNpemVzID0gdGhpcy5nZXQoVXNlclByZWZlcmVuY2VWYWx1ZXMuU3VwcG9ydGVkUGFnZVNpemVzKTtcclxuXHJcbiAgICAgICAgaWYgKHN1cHBvcnRlZFBhZ2VTaXplcykge1xyXG4gICAgICAgICAgICByZXR1cm4gSlNPTi5wYXJzZShzdXBwb3J0ZWRQYWdlU2l6ZXMpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFwcENvbmZpZy5nZXQoJ3BhZ2luYXRpb24uc3VwcG9ydGVkUGFnZVNpemVzJywgdGhpcy5kZWZhdWx0cy5zdXBwb3J0ZWRQYWdlU2l6ZXMpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBzZXQgc3VwcG9ydGVkUGFnZVNpemVzKHZhbHVlOiBudW1iZXJbXSkge1xyXG4gICAgICAgIHRoaXMuc2V0KFVzZXJQcmVmZXJlbmNlVmFsdWVzLlN1cHBvcnRlZFBhZ2VTaXplcywgSlNPTi5zdHJpbmdpZnkodmFsdWUpKTtcclxuICAgIH1cclxuXHJcbiAgICAvKiogUGFnaW5hdGlvbiBzaXplLiAqL1xyXG4gICAgc2V0IHBhZ2luYXRpb25TaXplKHZhbHVlOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLnNldChVc2VyUHJlZmVyZW5jZVZhbHVlcy5QYWdpbmF0aW9uU2l6ZSwgdmFsdWUpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBwYWdpbmF0aW9uU2l6ZSgpOiBudW1iZXIge1xyXG4gICAgICAgIGNvbnN0IHBhZ2luYXRpb25TaXplID0gdGhpcy5nZXQoVXNlclByZWZlcmVuY2VWYWx1ZXMuUGFnaW5hdGlvblNpemUpO1xyXG5cclxuICAgICAgICBpZiAocGFnaW5hdGlvblNpemUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIE51bWJlcihwYWdpbmF0aW9uU2l6ZSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIE51bWJlcih0aGlzLmFwcENvbmZpZy5nZXQoJ3BhZ2luYXRpb24uc2l6ZScsIHRoaXMuZGVmYXVsdHMucGFnaW5hdGlvblNpemUpKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqIEN1cnJlbnQgbG9jYWxlIHNldHRpbmcuICovXHJcbiAgICBnZXQgbG9jYWxlKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0KFVzZXJQcmVmZXJlbmNlVmFsdWVzLkxvY2FsZSk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IGxvY2FsZSh2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5zZXQoVXNlclByZWZlcmVuY2VWYWx1ZXMuTG9jYWxlLCB2YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBkZWZhdWx0IGxvY2FsZS5cclxuICAgICAqIEByZXR1cm5zIERlZmF1bHQgbG9jYWxlIGxhbmd1YWdlIGNvZGVcclxuICAgICAqL1xyXG4gICAgcHVibGljIGdldERlZmF1bHRMb2NhbGUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcHBDb25maWcuZ2V0PHN0cmluZz4oVXNlclByZWZlcmVuY2VWYWx1ZXMuTG9jYWxlKSB8fCB0aGlzLnRyYW5zbGF0ZS5nZXRCcm93c2VyQ3VsdHVyZUxhbmcoKSB8fCAnZW4nO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0TGFuZ3VhZ2VCeUtleShrZXk6IHN0cmluZyk6IExhbmd1YWdlSXRlbSB7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgdGhpcy5hcHBDb25maWdcclxuICAgICAgICAgICAgICAgIC5nZXQ8QXJyYXk8TGFuZ3VhZ2VJdGVtPj4oQXBwQ29uZmlnVmFsdWVzLkFQUF9DT05GSUdfTEFOR1VBR0VTX0tFWSwgWzxMYW5ndWFnZUl0ZW0+IHsga2V5OiAnZW4nIH1dKVxyXG4gICAgICAgICAgICAgICAgLmZpbmQoKGxhbmd1YWdlKSA9PiBrZXkuaW5jbHVkZXMobGFuZ3VhZ2Uua2V5KSkgfHwgPExhbmd1YWdlSXRlbT4geyBrZXk6ICdlbicgfVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19