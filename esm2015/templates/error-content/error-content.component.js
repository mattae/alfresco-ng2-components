/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ChangeDetectionStrategy, Input, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationService } from '../../services/translation.service';
export class ErrorContentComponent {
    /**
     * @param {?} route
     * @param {?} router
     * @param {?} translateService
     */
    constructor(route, router, translateService) {
        this.route = route;
        this.router = router;
        this.translateService = translateService;
        /**
         * Target URL for the secondary button.
         */
        this.secondaryButtonUrl = 'report-issue';
        /**
         * Target URL for the return button.
         */
        this.returnButtonUrl = '/';
        /**
         * Error code associated with this error.
         */
        this.errorCode = ErrorContentComponent.UNKNOWN_ERROR;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        if (this.route) {
            this.route.params.forEach((/**
             * @param {?} params
             * @return {?}
             */
            (params) => {
                if (params['id']) {
                    this.errorCode = this.checkErrorExists(params['id']) ? params['id'] : ErrorContentComponent.UNKNOWN_ERROR;
                }
            }));
        }
    }
    /**
     * @param {?} errorCode
     * @return {?}
     */
    checkErrorExists(errorCode) {
        /** @type {?} */
        const errorMessage = this.translateService.instant('ERROR_CONTENT.' + errorCode);
        return errorMessage !== ('ERROR_CONTENT.' + errorCode);
    }
    /**
     * @return {?}
     */
    getTranslations() {
        this.hasSecondButton = this.translateService.instant('ERROR_CONTENT.' + this.errorCode + '.SECONDARY_BUTTON.TEXT') ? true : false;
    }
    /**
     * @return {?}
     */
    ngAfterContentChecked() {
        this.getTranslations();
    }
    /**
     * @return {?}
     */
    onSecondButton() {
        this.router.navigate(['/' + this.secondaryButtonUrl]);
    }
    /**
     * @return {?}
     */
    onReturnButton() {
        this.router.navigate(['/' + this.returnButtonUrl]);
    }
}
ErrorContentComponent.UNKNOWN_ERROR = 'UNKNOWN';
ErrorContentComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-error-content',
                template: "<div class=\"adf-error-content\">\r\n    <p class=\"adf-error-content-code\">\r\n        {{ errorCode }}\r\n    </p>\r\n    <div class=\"adf-error-content-shadow\"></div>\r\n    <p class=\"adf-error-content-title\">\r\n        {{ 'ERROR_CONTENT.' + errorCode + '.TITLE' | translate }}\r\n    </p>\r\n    <p class=\"adf-error-content-description\">\r\n        {{ 'ERROR_CONTENT.' + errorCode + '.DESCRIPTION' | translate }}\r\n    </p>\r\n    <div class=\"adf-error-content-buttons\">\r\n        <a a id=\"adf-secondary-button\" mat-raised-button color=\"primary\"\r\n            *ngIf=\"hasSecondButton\" (click)=\"onSecondButton()\"\r\n            class=\"adf-error-content-description-link\">\r\n            {{ 'ERROR_CONTENT.' + errorCode + '.SECONDARY_BUTTON.TEXT' | translate | uppercase }}\r\n        </a>\r\n        <a id=\"adf-return-button\" mat-raised-button color=\"primary\" (click)=\"onReturnButton()\">\r\n            {{ 'ERROR_CONTENT.' + this.errorCode + '.RETURN_BUTTON.TEXT' | translate | uppercase }}\r\n        </a>\r\n    </div>\r\n</div>\r\n",
                changeDetection: ChangeDetectionStrategy.OnPush,
                encapsulation: ViewEncapsulation.None,
                host: { class: 'adf-error-content' },
                styles: [""]
            }] }
];
/** @nocollapse */
ErrorContentComponent.ctorParameters = () => [
    { type: ActivatedRoute },
    { type: Router },
    { type: TranslationService }
];
ErrorContentComponent.propDecorators = {
    secondaryButtonUrl: [{ type: Input }],
    returnButtonUrl: [{ type: Input }],
    errorCode: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    ErrorContentComponent.UNKNOWN_ERROR;
    /**
     * Target URL for the secondary button.
     * @type {?}
     */
    ErrorContentComponent.prototype.secondaryButtonUrl;
    /**
     * Target URL for the return button.
     * @type {?}
     */
    ErrorContentComponent.prototype.returnButtonUrl;
    /**
     * Error code associated with this error.
     * @type {?}
     */
    ErrorContentComponent.prototype.errorCode;
    /** @type {?} */
    ErrorContentComponent.prototype.hasSecondButton;
    /**
     * @type {?}
     * @private
     */
    ErrorContentComponent.prototype.route;
    /**
     * @type {?}
     * @private
     */
    ErrorContentComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    ErrorContentComponent.prototype.translateService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXJyb3ItY29udGVudC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJ0ZW1wbGF0ZXMvZXJyb3ItY29udGVudC9lcnJvci1jb250ZW50LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQ0gsU0FBUyxFQUNULHVCQUF1QixFQUN2QixLQUFLLEVBQ0wsaUJBQWlCLEVBR3BCLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBVSxjQUFjLEVBQUUsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDakUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFVeEUsTUFBTSxPQUFPLHFCQUFxQjs7Ozs7O0lBa0I5QixZQUFvQixLQUFxQixFQUNyQixNQUFjLEVBQ2QsZ0JBQW9DO1FBRnBDLFVBQUssR0FBTCxLQUFLLENBQWdCO1FBQ3JCLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQW9COzs7O1FBZHhELHVCQUFrQixHQUFXLGNBQWMsQ0FBQzs7OztRQUk1QyxvQkFBZSxHQUFXLEdBQUcsQ0FBQzs7OztRQUk5QixjQUFTLEdBQVcscUJBQXFCLENBQUMsYUFBYSxDQUFDO0lBT3hELENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1osSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsT0FBTzs7OztZQUFDLENBQUMsTUFBYyxFQUFFLEVBQUU7Z0JBQ3pDLElBQUksTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUNkLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLHFCQUFxQixDQUFDLGFBQWEsQ0FBQztpQkFDN0c7WUFDTCxDQUFDLEVBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxTQUFpQjs7Y0FDeEIsWUFBWSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEdBQUcsU0FBUyxDQUFDO1FBQ2hGLE9BQU8sWUFBWSxLQUFLLENBQUMsZ0JBQWdCLEdBQUcsU0FBUyxDQUFDLENBQUM7SUFDM0QsQ0FBQzs7OztJQUVELGVBQWU7UUFDWCxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxPQUFPLENBQ2hELGdCQUFnQixHQUFHLElBQUksQ0FBQyxTQUFTLEdBQUcsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDckYsQ0FBQzs7OztJQUVELHFCQUFxQjtRQUNqQixJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDM0IsQ0FBQzs7OztJQUVELGNBQWM7UUFDVixJQUFJLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQyxDQUFDO0lBQzFELENBQUM7Ozs7SUFFRCxjQUFjO1FBQ1YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUM7SUFDdkQsQ0FBQzs7QUFuRE0sbUNBQWEsR0FBRyxTQUFTLENBQUM7O1lBVnBDLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsbUJBQW1CO2dCQUM3QixtakNBQTZDO2dCQUU3QyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtnQkFDL0MsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7Z0JBQ3JDLElBQUksRUFBRSxFQUFFLEtBQUssRUFBRSxtQkFBbUIsRUFBRTs7YUFDdkM7Ozs7WUFWZ0IsY0FBYztZQUFFLE1BQU07WUFDOUIsa0JBQWtCOzs7aUNBZXRCLEtBQUs7OEJBSUwsS0FBSzt3QkFJTCxLQUFLOzs7O0lBWE4sb0NBQWlDOzs7OztJQUdqQyxtREFDNEM7Ozs7O0lBRzVDLGdEQUM4Qjs7Ozs7SUFHOUIsMENBQ3dEOztJQUV4RCxnREFBeUI7Ozs7O0lBRWIsc0NBQTZCOzs7OztJQUM3Qix1Q0FBc0I7Ozs7O0lBQ3RCLGlEQUE0QyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQge1xyXG4gICAgQ29tcG9uZW50LFxyXG4gICAgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksXHJcbiAgICBJbnB1dCxcclxuICAgIFZpZXdFbmNhcHN1bGF0aW9uLFxyXG4gICAgT25Jbml0LFxyXG4gICAgQWZ0ZXJDb250ZW50Q2hlY2tlZFxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBQYXJhbXMsIEFjdGl2YXRlZFJvdXRlLCBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy90cmFuc2xhdGlvbi5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtZXJyb3ItY29udGVudCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vZXJyb3ItY29udGVudC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9lcnJvci1jb250ZW50LmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXHJcbiAgICBob3N0OiB7IGNsYXNzOiAnYWRmLWVycm9yLWNvbnRlbnQnIH1cclxufSlcclxuZXhwb3J0IGNsYXNzIEVycm9yQ29udGVudENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJDb250ZW50Q2hlY2tlZCB7XHJcblxyXG4gICAgc3RhdGljIFVOS05PV05fRVJST1IgPSAnVU5LTk9XTic7XHJcblxyXG4gICAgLyoqIFRhcmdldCBVUkwgZm9yIHRoZSBzZWNvbmRhcnkgYnV0dG9uLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHNlY29uZGFyeUJ1dHRvblVybDogc3RyaW5nID0gJ3JlcG9ydC1pc3N1ZSc7XHJcblxyXG4gICAgLyoqIFRhcmdldCBVUkwgZm9yIHRoZSByZXR1cm4gYnV0dG9uLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHJldHVybkJ1dHRvblVybDogc3RyaW5nID0gJy8nO1xyXG5cclxuICAgIC8qKiBFcnJvciBjb2RlIGFzc29jaWF0ZWQgd2l0aCB0aGlzIGVycm9yLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGVycm9yQ29kZTogc3RyaW5nID0gRXJyb3JDb250ZW50Q29tcG9uZW50LlVOS05PV05fRVJST1I7XHJcblxyXG4gICAgaGFzU2Vjb25kQnV0dG9uOiBib29sZWFuO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgcm91dGU6IEFjdGl2YXRlZFJvdXRlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgdHJhbnNsYXRlU2VydmljZTogVHJhbnNsYXRpb25TZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucm91dGUpIHtcclxuICAgICAgICAgICAgdGhpcy5yb3V0ZS5wYXJhbXMuZm9yRWFjaCgocGFyYW1zOiBQYXJhbXMpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChwYXJhbXNbJ2lkJ10pIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmVycm9yQ29kZSA9IHRoaXMuY2hlY2tFcnJvckV4aXN0cyhwYXJhbXNbJ2lkJ10pID8gcGFyYW1zWydpZCddIDogRXJyb3JDb250ZW50Q29tcG9uZW50LlVOS05PV05fRVJST1I7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjaGVja0Vycm9yRXhpc3RzKGVycm9yQ29kZTogc3RyaW5nICkge1xyXG4gICAgICAgIGNvbnN0IGVycm9yTWVzc2FnZSA9IHRoaXMudHJhbnNsYXRlU2VydmljZS5pbnN0YW50KCdFUlJPUl9DT05URU5ULicgKyBlcnJvckNvZGUpO1xyXG4gICAgICAgIHJldHVybiBlcnJvck1lc3NhZ2UgIT09ICgnRVJST1JfQ09OVEVOVC4nICsgZXJyb3JDb2RlKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUcmFuc2xhdGlvbnMoKSB7XHJcbiAgICAgICAgdGhpcy5oYXNTZWNvbmRCdXR0b24gPSB0aGlzLnRyYW5zbGF0ZVNlcnZpY2UuaW5zdGFudChcclxuICAgICAgICAgICAgJ0VSUk9SX0NPTlRFTlQuJyArIHRoaXMuZXJyb3JDb2RlICsgJy5TRUNPTkRBUllfQlVUVE9OLlRFWFQnKSA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBuZ0FmdGVyQ29udGVudENoZWNrZWQoKSB7XHJcbiAgICAgICAgdGhpcy5nZXRUcmFuc2xhdGlvbnMoKTtcclxuICAgIH1cclxuXHJcbiAgICBvblNlY29uZEJ1dHRvbigpIHtcclxuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy8nICsgdGhpcy5zZWNvbmRhcnlCdXR0b25VcmxdKTtcclxuICAgIH1cclxuXHJcbiAgICBvblJldHVybkJ1dHRvbigpIHtcclxuICAgICAgICB0aGlzLnJvdXRlci5uYXZpZ2F0ZShbJy8nICsgdGhpcy5yZXR1cm5CdXR0b25VcmxdKTtcclxuICAgIH1cclxufVxyXG4iXX0=