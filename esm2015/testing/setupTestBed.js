/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { TestBed } from '@angular/core/testing';
/**
 * @record
 */
function DoneFn() { }
if (false) {
    /** @type {?} */
    DoneFn.prototype.fail;
    /* Skipping unhandled member: (): void;*/
}
/** @type {?} */
const resetTestingModule = TestBed.resetTestingModule;
/** @type {?} */
const preventAngularFromResetting = (/**
 * @return {?}
 */
() => (TestBed.resetTestingModule = (/**
 * @return {?}
 */
() => TestBed)));
const ɵ0 = preventAngularFromResetting;
/** @type {?} */
const allowAngularToReset = (/**
 * @return {?}
 */
() => (TestBed.resetTestingModule = resetTestingModule));
const ɵ1 = allowAngularToReset;
/** @type {?} */
export const setupTestBed = (/**
 * @param {?} moduleDef
 * @return {?}
 */
(moduleDef) => {
    beforeAll((/**
     * @param {?} done
     * @return {?}
     */
    (done) => ((/**
     * @return {?}
     */
    () => tslib_1.__awaiter(this, void 0, void 0, function* () {
        localStorage.clear();
        sessionStorage.clear();
        resetTestingModule();
        preventAngularFromResetting();
        TestBed.configureTestingModule(moduleDef);
        yield TestBed.compileComponents();
        // prevent Angular from resetting testing module
        TestBed.resetTestingModule = (/**
         * @return {?}
         */
        () => TestBed);
    })))()
        .then(done)
        .catch(done.fail)));
    afterAll((/**
     * @return {?}
     */
    () => allowAngularToReset()));
});
export { ɵ0, ɵ1 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dXBUZXN0QmVkLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsidGVzdGluZy9zZXR1cFRlc3RCZWQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxPQUFPLEVBQXNCLE1BQU0sdUJBQXVCLENBQUM7Ozs7QUFFcEUscUJBR0M7OztJQURHLHNCQUF5Qzs7OztNQU12QyxrQkFBa0IsR0FBRyxPQUFPLENBQUMsa0JBQWtCOztNQUMvQywyQkFBMkI7OztBQUFHLEdBQUcsRUFBRSxDQUFDLENBQUMsT0FBTyxDQUFDLGtCQUFrQjs7O0FBQUcsR0FBRyxFQUFFLENBQUMsT0FBTyxDQUFBLENBQUMsQ0FBQTs7O01BQ2hGLG1CQUFtQjs7O0FBQUcsR0FBRyxFQUFFLENBQUMsQ0FBQyxPQUFPLENBQUMsa0JBQWtCLEdBQUcsa0JBQWtCLENBQUMsQ0FBQTs7O0FBRW5GLE1BQU0sT0FBTyxZQUFZOzs7O0FBQUcsQ0FBQyxTQUE2QixFQUFFLEVBQUU7SUFDMUQsU0FBUzs7OztJQUFDLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FDZjs7O0lBQUMsR0FBUyxFQUFFO1FBQ1IsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ3JCLGNBQWMsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUN2QixrQkFBa0IsRUFBRSxDQUFDO1FBQ3JCLDJCQUEyQixFQUFFLENBQUM7UUFDOUIsT0FBTyxDQUFDLHNCQUFzQixDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzFDLE1BQU0sT0FBTyxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFFbEMsZ0RBQWdEO1FBQ2hELE9BQU8sQ0FBQyxrQkFBa0I7OztRQUFHLEdBQUcsRUFBRSxDQUFDLE9BQU8sQ0FBQSxDQUFDO0lBQy9DLENBQUMsQ0FBQSxFQUFDLEVBQUU7U0FDQyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ1YsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFDeEIsQ0FBQztJQUVGLFFBQVE7OztJQUFDLEdBQUcsRUFBRSxDQUFDLG1CQUFtQixFQUFFLEVBQUMsQ0FBQztBQUMxQyxDQUFDLENBQUEiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgVGVzdEJlZCwgVGVzdE1vZHVsZU1ldGFkYXRhIH0gZnJvbSAnQGFuZ3VsYXIvY29yZS90ZXN0aW5nJztcclxuXHJcbmludGVyZmFjZSBEb25lRm4gZXh0ZW5kcyBGdW5jdGlvbiB7XHJcbiAgICAoKTogdm9pZDtcclxuICAgIGZhaWw6IChtZXNzYWdlPzogRXJyb3IgfCBzdHJpbmcpID0+IHZvaWQ7XHJcbn1cclxuXHJcbmRlY2xhcmUgZnVuY3Rpb24gYmVmb3JlQWxsKGFjdGlvbjogKGRvbmU6IERvbmVGbikgPT4gdm9pZCwgdGltZW91dD86IG51bWJlcik6IHZvaWQ7XHJcbmRlY2xhcmUgZnVuY3Rpb24gYWZ0ZXJBbGwoYWN0aW9uOiAoZG9uZTogRG9uZUZuKSA9PiB2b2lkLCB0aW1lb3V0PzogbnVtYmVyKTogdm9pZDtcclxuXHJcbmNvbnN0IHJlc2V0VGVzdGluZ01vZHVsZSA9IFRlc3RCZWQucmVzZXRUZXN0aW5nTW9kdWxlO1xyXG5jb25zdCBwcmV2ZW50QW5ndWxhckZyb21SZXNldHRpbmcgPSAoKSA9PiAoVGVzdEJlZC5yZXNldFRlc3RpbmdNb2R1bGUgPSAoKSA9PiBUZXN0QmVkKTtcclxuY29uc3QgYWxsb3dBbmd1bGFyVG9SZXNldCA9ICgpID0+IChUZXN0QmVkLnJlc2V0VGVzdGluZ01vZHVsZSA9IHJlc2V0VGVzdGluZ01vZHVsZSk7XHJcblxyXG5leHBvcnQgY29uc3Qgc2V0dXBUZXN0QmVkID0gKG1vZHVsZURlZjogVGVzdE1vZHVsZU1ldGFkYXRhKSA9PiB7XHJcbiAgICBiZWZvcmVBbGwoKGRvbmUpID0+XHJcbiAgICAgICAgKGFzeW5jICgpID0+IHtcclxuICAgICAgICAgICAgbG9jYWxTdG9yYWdlLmNsZWFyKCk7XHJcbiAgICAgICAgICAgIHNlc3Npb25TdG9yYWdlLmNsZWFyKCk7XHJcbiAgICAgICAgICAgIHJlc2V0VGVzdGluZ01vZHVsZSgpO1xyXG4gICAgICAgICAgICBwcmV2ZW50QW5ndWxhckZyb21SZXNldHRpbmcoKTtcclxuICAgICAgICAgICAgVGVzdEJlZC5jb25maWd1cmVUZXN0aW5nTW9kdWxlKG1vZHVsZURlZik7XHJcbiAgICAgICAgICAgIGF3YWl0IFRlc3RCZWQuY29tcGlsZUNvbXBvbmVudHMoKTtcclxuXHJcbiAgICAgICAgICAgIC8vIHByZXZlbnQgQW5ndWxhciBmcm9tIHJlc2V0dGluZyB0ZXN0aW5nIG1vZHVsZVxyXG4gICAgICAgICAgICBUZXN0QmVkLnJlc2V0VGVzdGluZ01vZHVsZSA9ICgpID0+IFRlc3RCZWQ7XHJcbiAgICAgICAgfSkoKVxyXG4gICAgICAgICAgICAudGhlbihkb25lKVxyXG4gICAgICAgICAgICAuY2F0Y2goZG9uZS5mYWlsKVxyXG4gICAgKTtcclxuXHJcbiAgICBhZnRlckFsbCgoKSA9PiBhbGxvd0FuZ3VsYXJUb1Jlc2V0KCkpO1xyXG59O1xyXG4iXX0=