/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewEncapsulation, ViewChild } from '@angular/core';
import { AuthenticationService } from '../../services/authentication.service';
import { BpmUserService } from './../services/bpm-user.service';
import { EcmUserService } from './../services/ecm-user.service';
import { IdentityUserService } from '../services/identity-user.service';
import { of } from 'rxjs';
import { MatMenuTrigger } from '@angular/material';
export class UserInfoComponent {
    /**
     * @param {?} ecmUserService
     * @param {?} bpmUserService
     * @param {?} identityUserService
     * @param {?} authService
     */
    constructor(ecmUserService, bpmUserService, identityUserService, authService) {
        this.ecmUserService = ecmUserService;
        this.bpmUserService = bpmUserService;
        this.identityUserService = identityUserService;
        this.authService = authService;
        /**
         * Custom path for the background banner image for ACS users.
         */
        this.ecmBackgroundImage = './assets/images/ecm-background.png';
        /**
         * Custom path for the background banner image for APS users.
         */
        this.bpmBackgroundImage = './assets/images/bpm-background.png';
        /**
         * Custom choice for opening the menu at the bottom. Can be `before` or `after`.
         */
        this.menuPositionX = 'after';
        /**
         * Custom choice for opening the menu at the bottom. Can be `above` or `below`.
         */
        this.menuPositionY = 'below';
        /**
         * Shows/hides the username next to the user info button.
         */
        this.showName = true;
        /**
         * When the username is shown, this defines its position relative to the user info button.
         * Can be `right` or `left`.
         */
        this.namePosition = 'right';
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.getUserInfo();
    }
    /**
     * @return {?}
     */
    getUserInfo() {
        if (this.authService.isOauth()) {
            this.loadIdentityUserInfo();
        }
        else if (this.authService.isEcmLoggedIn() && this.authService.isBpmLoggedIn()) {
            this.loadEcmUserInfo();
            this.loadBpmUserInfo();
        }
        else if (this.authService.isEcmLoggedIn()) {
            this.loadEcmUserInfo();
        }
        else if (this.authService.isBpmLoggedIn()) {
            this.loadBpmUserInfo();
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onKeyPress(event) {
        this.closeUserModal(event);
    }
    /**
     * @private
     * @param {?} $event
     * @return {?}
     */
    closeUserModal($event) {
        if ($event.keyCode === 27) {
            this.trigger.closeMenu();
        }
    }
    /**
     * @return {?}
     */
    isLoggedIn() {
        return this.authService.isLoggedIn();
    }
    /**
     * @return {?}
     */
    loadEcmUserInfo() {
        this.ecmUser$ = this.ecmUserService.getCurrentUserInfo();
    }
    /**
     * @return {?}
     */
    loadBpmUserInfo() {
        this.bpmUser$ = this.bpmUserService.getCurrentUserInfo();
    }
    /**
     * @return {?}
     */
    loadIdentityUserInfo() {
        this.identityUser$ = of(this.identityUserService.getCurrentUserInfo());
    }
    /**
     * @param {?} event
     * @return {?}
     */
    stopClosing(event) {
        event.stopPropagation();
    }
    /**
     * @param {?} avatarId
     * @return {?}
     */
    getEcmAvatar(avatarId) {
        return this.ecmUserService.getUserProfileImage(avatarId);
    }
    /**
     * @return {?}
     */
    getBpmUserImage() {
        return this.bpmUserService.getCurrentUserProfileImage();
    }
    /**
     * @return {?}
     */
    showOnRight() {
        return this.namePosition === 'right';
    }
}
UserInfoComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-userinfo',
                template: "<div id=\"userinfo_container\" [class.adf-userinfo-name-right]=\"showOnRight()\" (keyup)=\"onKeyPress($event)\"\r\n    class=\"adf-userinfo-container\" *ngIf=\"isLoggedIn()\">\r\n\r\n    <ng-container *ngIf=\"showName\">\r\n        <span *ngIf=\"identityUser$ | async as identityUser; else showBpmAndEcmUserFullNames\" id=\"adf-userinfo-identity-name-display\"\r\n            class=\"adf-userinfo-name\">{{identityUser | fullName}}</span>\r\n        <ng-template #showBpmAndEcmUserFullNames>\r\n            <span *ngIf=\"ecmUser$ | async as ecmUser; else showBpmUserFullName\" id=\"adf-userinfo-ecm-name-display\"\r\n                class=\"adf-userinfo-name\">{{ecmUser | fullName}}</span>\r\n            <ng-template #showBpmUserFullName>\r\n                <span *ngIf=\"bpmUser$ | async as bpmUser\" id=\"adf-userinfo-bpm-name-display\"\r\n                    class=\"adf-userinfo-name\">{{bpmUser | fullName}}</span>\r\n            </ng-template>\r\n        </ng-template>\r\n    </ng-container>\r\n\r\n    <button mat-button [matMenuTriggerFor]=\"menu\" class=\"adf-userinfo-menu_button\" data-automation-id=\"adf-user-profile\">\r\n        <div class=\"adf-userinfo-button-profile\" id=\"user-profile\">\r\n            <div *ngIf=\"identityUser$ | async as identityUser; else showBpmAndEcmUserImage\" id=\"identity-user-image\">\r\n                <div [outerHTML]=\"identityUser | usernameInitials:'adf-userinfo-pic'\"></div>\r\n            </div>\r\n            <ng-template #showBpmAndEcmUserImage>\r\n                <div *ngIf=\"ecmUser$ | async as ecmUser; else showBpmUserImage\" id=\"ecm-user-image\">\r\n                    <div *ngIf=\"ecmUser.avatarId; else initialTemplate\" class=\"adf-userinfo-profile-container\">\r\n                        <img id=\"logged-user-img\" [src]=\"getEcmAvatar(ecmUser.avatarId)\" alt=\"user-info-profile-button\"\r\n                            class=\"adf-userinfo-profile-image\"/>\r\n                    </div>\r\n                    <ng-template #initialTemplate>\r\n                        <div [outerHTML]=\"ecmUser | usernameInitials:'adf-userinfo-pic'\"></div>\r\n                    </ng-template>\r\n                </div>\r\n                <ng-template #showBpmUserImage>\r\n                    <div *ngIf=\"bpmUser$ | async as bpmUser\" id=\"bpm-user-image\">\r\n                            <div *ngIf=\"bpmUser.pictureId; else initialTemplate\" class=\"adf-userinfo-profile-container\">\r\n                                <img id=\"logged-user-img\" [src]=\"getBpmUserImage()\" alt=\"user-info-profile-button\"\r\n                                    class=\"adf-userinfo-profile-image\"/>\r\n                            </div>\r\n                            <ng-template #initialTemplate>\r\n                                <div [outerHTML]=\"bpmUser | usernameInitials:'adf-userinfo-pic'\"></div>\r\n                            </ng-template>\r\n                    </div>\r\n                </ng-template>\r\n            </ng-template>\r\n        </div>\r\n    </button>\r\n    <mat-menu #menu=\"matMenu\" id=\"user-profile-lists\" [xPosition]=\"menuPositionX\" [yPosition]=\"menuPositionY\" [overlapTrigger]=\"false\" class=\"adf-userinfo-menu\">\r\n        <mat-tab-group id=\"tab-group-env\" (click)=\"stopClosing($event)\"\r\n            class=\"adf-userinfo-tab\" [class.adf-hide-tab]=\"!(bpmUser$ | async) || !(ecmUser$ | async)\">\r\n            <mat-tab id=\"ecm-panel\" label=\"{{ 'USER_PROFILE.TAB.CS' | translate }}\" *ngIf=\"ecmUser$ | async as ecmUser\">\r\n                <mat-card class=\"adf-userinfo-card\">\r\n                    <mat-card-header class=\"adf-userinfo-card-header\" [style.background-image]=\"'url(' + ecmBackgroundImage + ')'\">\r\n                        <div *ngIf=\"ecmUser.avatarId; else initialTemplate\" class=\"adf-userinfo-profile-container adf-hide-small\">\r\n                            <img class=\"adf-userinfo-profile-picture\" id=\"ecm-user-detail-image\"\r\n                                alt=\"ecm-profile-image\" [src]=\"getEcmAvatar(ecmUser.avatarId)\" />\r\n                        </div>\r\n                        <ng-template #initialTemplate>\r\n                            <div [outerHTML]=\"ecmUser | usernameInitials:'adf-userinfo-profile-initials adf-hide-small'\"></div>\r\n                        </ng-template>\r\n\r\n                       <div class=\"adf-userinfo-title\" id=\"ecm-username\">{{ecmUser | fullName}}</div>\r\n                    </mat-card-header>\r\n                    <mat-card-content>\r\n                        <div class=\"adf-userinfo-supporting-text\">\r\n                                <div class=\"adf-userinfo-detail\">\r\n                                    <span id=\"ecm-full-name\" class=\"adf-userinfo__detail-title\">{{ecmUser | fullName}}</span>\r\n                                    <span class=\"adf-userinfo__detail-profile\" id=\"ecm-email\"> {{ecmUser.email}} </span>\r\n                                </div>\r\n                                <div class=\"adf-userinfo-detail\">\r\n                                    <span class=\"adf-userinfo__secondary-info\" id=\"ecm-job-title-label\">\r\n                                        {{ 'USER_PROFILE.LABELS.ECM.JOB_TITLE' | translate }}\r\n                                        <span id=\"ecm-job-title\" class=\"adf-userinfo__detail-profile\"> {{ ecmUser.jobTitle ? ecmUser.jobTitle : 'N/A' }} </span>\r\n                                    </span>\r\n                                </div>\r\n                        </div>\r\n                    </mat-card-content>\r\n                </mat-card>\r\n            </mat-tab>\r\n            <mat-tab id=\"bpm-panel\" label=\"{{ 'USER_PROFILE.TAB.PS' | translate }}\" *ngIf=\"bpmUser$ | async as bpmUser\">\r\n            <mat-card class=\"adf-userinfo-card\">\r\n                <mat-card-header class=\"adf-userinfo-card-header\" [style.background-image]=\"'url(' + bpmBackgroundImage + ')'\">\r\n                    <img *ngIf=\"bpmUser.pictureId; else initialTemplate\" class=\"adf-userinfo-profile-picture adf-hide-small\" id=\"bpm-user-detail-image\"\r\n                            alt=\"bpm-profile-image\" [src]=\"getBpmUserImage()\"/>\r\n                    <ng-template #initialTemplate>\r\n                        <div [outerHTML]=\"bpmUser | usernameInitials:'adf-userinfo-profile-initials adf-hide-small'\"></div>\r\n                    </ng-template>\r\n                   <div class=\"adf-userinfo-title\" id=\"bpm-username\">{{bpmUser | fullName}}</div>\r\n                </mat-card-header>\r\n                <mat-card-content>\r\n                    <div class=\"adf-userinfo-supporting-text\">\r\n                            <div class=\"adf-userinfo-detail\">\r\n                                <span id=\"bpm-full-name\" class=\"adf-userinfo__detail-title\">{{ bpmUser | fullName }}</span>\r\n                                <span class=\"adf-userinfo__detail-profile\" id=\"bpm-email\"> {{bpmUser.email}} </span>\r\n                            </div>\r\n                            <div class=\"adf-userinfo-detail\">\r\n                                <span id=\"bpm-tenant\" class=\"adf-userinfo__secondary-info\">\r\n                                    {{ 'USER_PROFILE.LABELS.BPM.TENANT' | translate }}\r\n                                    <span class=\"adf-userinfo__detail-profile\">{{ bpmUser.tenantName ? bpmUser.tenantName : '' }}</span>\r\n                                </span>\r\n                            </div>\r\n                    </div>\r\n                </mat-card-content>\r\n            </mat-card>\r\n            </mat-tab>\r\n            <mat-tab id=\"identity-panel\" *ngIf=\"identityUser$ | async as identityUser\">\r\n                <mat-card class=\"adf-userinfo-card\">\r\n                    <mat-card-header class=\"adf-userinfo-card-header\" [style.background-image]=\"'url(' + bpmBackgroundImage + ')'\">\r\n                        <div\r\n                            [outerHTML]=\"identityUser | usernameInitials:'adf-userinfo-profile-initials adf-hide-small'\">\r\n                        </div>\r\n                       <div class=\"adf-userinfo-title\" id=\"identity-username\">{{identityUser | fullName}}</div>\r\n                    </mat-card-header>\r\n                    <mat-card-content>\r\n                        <div class=\"adf-userinfo-supporting-text\">\r\n                                <div class=\"adf-userinfo-detail\">\r\n                                    <span id=\"identity-full-name\" class=\"adf-userinfo__detail-title\">{{identityUser | fullName}}</span>\r\n                                    <span class=\"adf-userinfo__detail-profile\" id=\"identity-email\"> {{identityUser.email}} </span>\r\n                                </div>\r\n                        </div>\r\n                    </mat-card-content>\r\n                </mat-card>\r\n            </mat-tab>\r\n        </mat-tab-group>\r\n    </mat-menu>\r\n</div>\r\n",
                encapsulation: ViewEncapsulation.None,
                styles: [""]
            }] }
];
/** @nocollapse */
UserInfoComponent.ctorParameters = () => [
    { type: EcmUserService },
    { type: BpmUserService },
    { type: IdentityUserService },
    { type: AuthenticationService }
];
UserInfoComponent.propDecorators = {
    trigger: [{ type: ViewChild, args: [MatMenuTrigger, { static: true },] }],
    ecmBackgroundImage: [{ type: Input }],
    bpmBackgroundImage: [{ type: Input }],
    menuPositionX: [{ type: Input }],
    menuPositionY: [{ type: Input }],
    showName: [{ type: Input }],
    namePosition: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    UserInfoComponent.prototype.trigger;
    /**
     * Custom path for the background banner image for ACS users.
     * @type {?}
     */
    UserInfoComponent.prototype.ecmBackgroundImage;
    /**
     * Custom path for the background banner image for APS users.
     * @type {?}
     */
    UserInfoComponent.prototype.bpmBackgroundImage;
    /**
     * Custom choice for opening the menu at the bottom. Can be `before` or `after`.
     * @type {?}
     */
    UserInfoComponent.prototype.menuPositionX;
    /**
     * Custom choice for opening the menu at the bottom. Can be `above` or `below`.
     * @type {?}
     */
    UserInfoComponent.prototype.menuPositionY;
    /**
     * Shows/hides the username next to the user info button.
     * @type {?}
     */
    UserInfoComponent.prototype.showName;
    /**
     * When the username is shown, this defines its position relative to the user info button.
     * Can be `right` or `left`.
     * @type {?}
     */
    UserInfoComponent.prototype.namePosition;
    /** @type {?} */
    UserInfoComponent.prototype.ecmUser$;
    /** @type {?} */
    UserInfoComponent.prototype.bpmUser$;
    /** @type {?} */
    UserInfoComponent.prototype.identityUser$;
    /** @type {?} */
    UserInfoComponent.prototype.selectedIndex;
    /**
     * @type {?}
     * @private
     */
    UserInfoComponent.prototype.ecmUserService;
    /**
     * @type {?}
     * @private
     */
    UserInfoComponent.prototype.bpmUserService;
    /**
     * @type {?}
     * @private
     */
    UserInfoComponent.prototype.identityUserService;
    /**
     * @type {?}
     * @private
     */
    UserInfoComponent.prototype.authService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1pbmZvLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInVzZXJpbmZvL2NvbXBvbmVudHMvdXNlci1pbmZvLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQ0EsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQVUsaUJBQWlCLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3ZGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBSTlFLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDaEUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDeEUsT0FBTyxFQUFFLEVBQUUsRUFBYyxNQUFNLE1BQU0sQ0FBQztBQUN0QyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFRbkQsTUFBTSxPQUFPLGlCQUFpQjs7Ozs7OztJQW1DMUIsWUFBb0IsY0FBOEIsRUFDOUIsY0FBOEIsRUFDOUIsbUJBQXdDLEVBQ3hDLFdBQWtDO1FBSGxDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsd0JBQW1CLEdBQW5CLG1CQUFtQixDQUFxQjtRQUN4QyxnQkFBVyxHQUFYLFdBQVcsQ0FBdUI7Ozs7UUFoQ3RELHVCQUFrQixHQUFXLG9DQUFvQyxDQUFDOzs7O1FBSWxFLHVCQUFrQixHQUFXLG9DQUFvQyxDQUFDOzs7O1FBSWxFLGtCQUFhLEdBQVcsT0FBTyxDQUFDOzs7O1FBSWhDLGtCQUFhLEdBQVcsT0FBTyxDQUFDOzs7O1FBSWhDLGFBQVEsR0FBWSxJQUFJLENBQUM7Ozs7O1FBTXpCLGlCQUFZLEdBQVcsT0FBTyxDQUFDO0lBVy9CLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxFQUFFO1lBQzVCLElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO1NBQy9CO2FBQU0sSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLEVBQUU7WUFDN0UsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztTQUMxQjthQUFNLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLEVBQUUsRUFBRTtZQUN6QyxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7U0FDMUI7YUFBTSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLEVBQUU7WUFDekMsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1NBQzFCO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxVQUFVLENBQUMsS0FBb0I7UUFDM0IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUMvQixDQUFDOzs7Ozs7SUFFTyxjQUFjLENBQUMsTUFBcUI7UUFDeEMsSUFBSSxNQUFNLENBQUMsT0FBTyxLQUFLLEVBQUUsRUFBRztZQUN4QixJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDO1NBQzVCO0lBQ0wsQ0FBQzs7OztJQUVELFVBQVU7UUFDTixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDekMsQ0FBQzs7OztJQUVELGVBQWU7UUFDWCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUM3RCxDQUFDOzs7O0lBRUQsZUFBZTtRQUNYLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxrQkFBa0IsRUFBRSxDQUFDO0lBQzdELENBQUM7Ozs7SUFFRCxvQkFBb0I7UUFDaEIsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLGtCQUFrQixFQUFFLENBQUMsQ0FBQztJQUMzRSxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxLQUFLO1FBQ2IsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO0lBQzVCLENBQUM7Ozs7O0lBRUQsWUFBWSxDQUFDLFFBQWE7UUFDdEIsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzdELENBQUM7Ozs7SUFFRCxlQUFlO1FBQ1gsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLDBCQUEwQixFQUFFLENBQUM7SUFDNUQsQ0FBQzs7OztJQUVELFdBQVc7UUFDUCxPQUFPLElBQUksQ0FBQyxZQUFZLEtBQUssT0FBTyxDQUFDO0lBQ3pDLENBQUM7OztZQXhHSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLGNBQWM7Z0JBRXhCLDB6UkFBeUM7Z0JBQ3pDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOzthQUN4Qzs7OztZQVZRLGNBQWM7WUFEZCxjQUFjO1lBRWQsbUJBQW1CO1lBTm5CLHFCQUFxQjs7O3NCQWtCekIsU0FBUyxTQUFDLGNBQWMsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7aUNBR3hDLEtBQUs7aUNBSUwsS0FBSzs0QkFJTCxLQUFLOzRCQUlMLEtBQUs7dUJBSUwsS0FBSzsyQkFNTCxLQUFLOzs7O0lBekJOLG9DQUFtRTs7Ozs7SUFHbkUsK0NBQ2tFOzs7OztJQUdsRSwrQ0FDa0U7Ozs7O0lBR2xFLDBDQUNnQzs7Ozs7SUFHaEMsMENBQ2dDOzs7OztJQUdoQyxxQ0FDeUI7Ozs7OztJQUt6Qix5Q0FDK0I7O0lBRS9CLHFDQUFtQzs7SUFDbkMscUNBQW1DOztJQUNuQywwQ0FBNkM7O0lBQzdDLDBDQUFzQjs7Ozs7SUFFViwyQ0FBc0M7Ozs7O0lBQ3RDLDJDQUFzQzs7Ozs7SUFDdEMsZ0RBQWdEOzs7OztJQUNoRCx3Q0FBMEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cblxuLyohXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cbiAqXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4gKlxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuICpcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXG4gKi9cblxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiwgVmlld0NoaWxkIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBdXRoZW50aWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlJztcbmltcG9ydCB7IEJwbVVzZXJNb2RlbCB9IGZyb20gJy4vLi4vbW9kZWxzL2JwbS11c2VyLm1vZGVsJztcbmltcG9ydCB7IEVjbVVzZXJNb2RlbCB9IGZyb20gJy4vLi4vbW9kZWxzL2VjbS11c2VyLm1vZGVsJztcbmltcG9ydCB7IElkZW50aXR5VXNlck1vZGVsIH0gZnJvbSAnLi8uLi9tb2RlbHMvaWRlbnRpdHktdXNlci5tb2RlbCc7XG5pbXBvcnQgeyBCcG1Vc2VyU2VydmljZSB9IGZyb20gJy4vLi4vc2VydmljZXMvYnBtLXVzZXIuc2VydmljZSc7XG5pbXBvcnQgeyBFY21Vc2VyU2VydmljZSB9IGZyb20gJy4vLi4vc2VydmljZXMvZWNtLXVzZXIuc2VydmljZSc7XG5pbXBvcnQgeyBJZGVudGl0eVVzZXJTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvaWRlbnRpdHktdXNlci5zZXJ2aWNlJztcbmltcG9ydCB7IG9mLCBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBNYXRNZW51VHJpZ2dlciB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdhZGYtdXNlcmluZm8nLFxuICAgIHN0eWxlVXJsczogWycuL3VzZXItaW5mby5jb21wb25lbnQuc2NzcyddLFxuICAgIHRlbXBsYXRlVXJsOiAnLi91c2VyLWluZm8uY29tcG9uZW50Lmh0bWwnLFxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcbn0pXG5leHBvcnQgY2xhc3MgVXNlckluZm9Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgQFZpZXdDaGlsZChNYXRNZW51VHJpZ2dlciwge3N0YXRpYzogdHJ1ZX0pIHRyaWdnZXI6IE1hdE1lbnVUcmlnZ2VyO1xuXG4gICAgLyoqIEN1c3RvbSBwYXRoIGZvciB0aGUgYmFja2dyb3VuZCBiYW5uZXIgaW1hZ2UgZm9yIEFDUyB1c2Vycy4gKi9cbiAgICBASW5wdXQoKVxuICAgIGVjbUJhY2tncm91bmRJbWFnZTogc3RyaW5nID0gJy4vYXNzZXRzL2ltYWdlcy9lY20tYmFja2dyb3VuZC5wbmcnO1xuXG4gICAgLyoqIEN1c3RvbSBwYXRoIGZvciB0aGUgYmFja2dyb3VuZCBiYW5uZXIgaW1hZ2UgZm9yIEFQUyB1c2Vycy4gKi9cbiAgICBASW5wdXQoKVxuICAgIGJwbUJhY2tncm91bmRJbWFnZTogc3RyaW5nID0gJy4vYXNzZXRzL2ltYWdlcy9icG0tYmFja2dyb3VuZC5wbmcnO1xuXG4gICAgLyoqIEN1c3RvbSBjaG9pY2UgZm9yIG9wZW5pbmcgdGhlIG1lbnUgYXQgdGhlIGJvdHRvbS4gQ2FuIGJlIGBiZWZvcmVgIG9yIGBhZnRlcmAuICovXG4gICAgQElucHV0KClcbiAgICBtZW51UG9zaXRpb25YOiBzdHJpbmcgPSAnYWZ0ZXInO1xuXG4gICAgLyoqIEN1c3RvbSBjaG9pY2UgZm9yIG9wZW5pbmcgdGhlIG1lbnUgYXQgdGhlIGJvdHRvbS4gQ2FuIGJlIGBhYm92ZWAgb3IgYGJlbG93YC4gKi9cbiAgICBASW5wdXQoKVxuICAgIG1lbnVQb3NpdGlvblk6IHN0cmluZyA9ICdiZWxvdyc7XG5cbiAgICAvKiogU2hvd3MvaGlkZXMgdGhlIHVzZXJuYW1lIG5leHQgdG8gdGhlIHVzZXIgaW5mbyBidXR0b24uICovXG4gICAgQElucHV0KClcbiAgICBzaG93TmFtZTogYm9vbGVhbiA9IHRydWU7XG5cbiAgICAvKiogV2hlbiB0aGUgdXNlcm5hbWUgaXMgc2hvd24sIHRoaXMgZGVmaW5lcyBpdHMgcG9zaXRpb24gcmVsYXRpdmUgdG8gdGhlIHVzZXIgaW5mbyBidXR0b24uXG4gICAgICogQ2FuIGJlIGByaWdodGAgb3IgYGxlZnRgLlxuICAgICAqL1xuICAgIEBJbnB1dCgpXG4gICAgbmFtZVBvc2l0aW9uOiBzdHJpbmcgPSAncmlnaHQnO1xuXG4gICAgZWNtVXNlciQ6IE9ic2VydmFibGU8RWNtVXNlck1vZGVsPjtcbiAgICBicG1Vc2VyJDogT2JzZXJ2YWJsZTxCcG1Vc2VyTW9kZWw+O1xuICAgIGlkZW50aXR5VXNlciQ6IE9ic2VydmFibGU8SWRlbnRpdHlVc2VyTW9kZWw+O1xuICAgIHNlbGVjdGVkSW5kZXg6IG51bWJlcjtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgZWNtVXNlclNlcnZpY2U6IEVjbVVzZXJTZXJ2aWNlLFxuICAgICAgICAgICAgICAgIHByaXZhdGUgYnBtVXNlclNlcnZpY2U6IEJwbVVzZXJTZXJ2aWNlLFxuICAgICAgICAgICAgICAgIHByaXZhdGUgaWRlbnRpdHlVc2VyU2VydmljZTogSWRlbnRpdHlVc2VyU2VydmljZSxcbiAgICAgICAgICAgICAgICBwcml2YXRlIGF1dGhTZXJ2aWNlOiBBdXRoZW50aWNhdGlvblNlcnZpY2UpIHtcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy5nZXRVc2VySW5mbygpO1xuICAgIH1cblxuICAgIGdldFVzZXJJbmZvKCkge1xuICAgICAgICBpZiAodGhpcy5hdXRoU2VydmljZS5pc09hdXRoKCkpIHtcbiAgICAgICAgICAgIHRoaXMubG9hZElkZW50aXR5VXNlckluZm8oKTtcbiAgICAgICAgfSBlbHNlIGlmICh0aGlzLmF1dGhTZXJ2aWNlLmlzRWNtTG9nZ2VkSW4oKSAmJiB0aGlzLmF1dGhTZXJ2aWNlLmlzQnBtTG9nZ2VkSW4oKSkge1xuICAgICAgICAgICAgdGhpcy5sb2FkRWNtVXNlckluZm8oKTtcbiAgICAgICAgICAgIHRoaXMubG9hZEJwbVVzZXJJbmZvKCk7XG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5hdXRoU2VydmljZS5pc0VjbUxvZ2dlZEluKCkpIHtcbiAgICAgICAgICAgIHRoaXMubG9hZEVjbVVzZXJJbmZvKCk7XG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5hdXRoU2VydmljZS5pc0JwbUxvZ2dlZEluKCkpIHtcbiAgICAgICAgICAgIHRoaXMubG9hZEJwbVVzZXJJbmZvKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBvbktleVByZXNzKGV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XG4gICAgICAgIHRoaXMuY2xvc2VVc2VyTW9kYWwoZXZlbnQpO1xuICAgIH1cblxuICAgIHByaXZhdGUgY2xvc2VVc2VyTW9kYWwoJGV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQua2V5Q29kZSA9PT0gMjcgKSB7XG4gICAgICAgICAgICB0aGlzLnRyaWdnZXIuY2xvc2VNZW51KCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBpc0xvZ2dlZEluKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5hdXRoU2VydmljZS5pc0xvZ2dlZEluKCk7XG4gICAgfVxuXG4gICAgbG9hZEVjbVVzZXJJbmZvKCk6IHZvaWQge1xuICAgICAgICB0aGlzLmVjbVVzZXIkID0gdGhpcy5lY21Vc2VyU2VydmljZS5nZXRDdXJyZW50VXNlckluZm8oKTtcbiAgICB9XG5cbiAgICBsb2FkQnBtVXNlckluZm8oKSB7XG4gICAgICAgIHRoaXMuYnBtVXNlciQgPSB0aGlzLmJwbVVzZXJTZXJ2aWNlLmdldEN1cnJlbnRVc2VySW5mbygpO1xuICAgIH1cblxuICAgIGxvYWRJZGVudGl0eVVzZXJJbmZvKCkge1xuICAgICAgICB0aGlzLmlkZW50aXR5VXNlciQgPSBvZih0aGlzLmlkZW50aXR5VXNlclNlcnZpY2UuZ2V0Q3VycmVudFVzZXJJbmZvKCkpO1xuICAgIH1cblxuICAgIHN0b3BDbG9zaW5nKGV2ZW50KSB7XG4gICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xuICAgIH1cblxuICAgIGdldEVjbUF2YXRhcihhdmF0YXJJZDogYW55ICk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiB0aGlzLmVjbVVzZXJTZXJ2aWNlLmdldFVzZXJQcm9maWxlSW1hZ2UoYXZhdGFySWQpO1xuICAgIH1cblxuICAgIGdldEJwbVVzZXJJbWFnZSgpOiBzdHJpbmcge1xuICAgICAgICByZXR1cm4gdGhpcy5icG1Vc2VyU2VydmljZS5nZXRDdXJyZW50VXNlclByb2ZpbGVJbWFnZSgpO1xuICAgIH1cblxuICAgIHNob3dPblJpZ2h0KCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5uYW1lUG9zaXRpb24gPT09ICdyaWdodCc7XG4gICAgfVxufVxuIl19