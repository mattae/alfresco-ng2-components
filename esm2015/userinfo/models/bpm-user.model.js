/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class BpmUserModel {
    /**
     * @param {?=} input
     */
    constructor(input) {
        if (input) {
            this.apps = input.apps;
            this.capabilities = input.capabilities;
            this.company = input.company;
            this.created = input.created;
            this.email = input.email;
            this.externalId = input.externalId;
            this.firstName = input.firstName;
            this.lastName = input.lastName;
            this.fullname = input.fullname;
            this.groups = input.groups;
            this.id = input.id;
            this.lastUpdate = input.lastUpdate;
            this.latestSyncTimeStamp = input.latestSyncTimeStamp;
            this.password = input.password;
            this.pictureId = input.pictureId;
            this.status = input.status;
            this.tenantId = input.tenantId;
            this.tenantName = input.tenantName;
            this.tenantPictureId = input.tenantPictureId;
            this.type = input.type;
        }
    }
}
if (false) {
    /** @type {?} */
    BpmUserModel.prototype.apps;
    /** @type {?} */
    BpmUserModel.prototype.capabilities;
    /** @type {?} */
    BpmUserModel.prototype.company;
    /** @type {?} */
    BpmUserModel.prototype.created;
    /** @type {?} */
    BpmUserModel.prototype.email;
    /** @type {?} */
    BpmUserModel.prototype.externalId;
    /** @type {?} */
    BpmUserModel.prototype.firstName;
    /** @type {?} */
    BpmUserModel.prototype.lastName;
    /** @type {?} */
    BpmUserModel.prototype.fullname;
    /** @type {?} */
    BpmUserModel.prototype.groups;
    /** @type {?} */
    BpmUserModel.prototype.id;
    /** @type {?} */
    BpmUserModel.prototype.lastUpdate;
    /** @type {?} */
    BpmUserModel.prototype.latestSyncTimeStamp;
    /** @type {?} */
    BpmUserModel.prototype.password;
    /** @type {?} */
    BpmUserModel.prototype.pictureId;
    /** @type {?} */
    BpmUserModel.prototype.status;
    /** @type {?} */
    BpmUserModel.prototype.tenantId;
    /** @type {?} */
    BpmUserModel.prototype.tenantName;
    /** @type {?} */
    BpmUserModel.prototype.tenantPictureId;
    /** @type {?} */
    BpmUserModel.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnBtLXVzZXIubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJ1c2VyaW5mby9tb2RlbHMvYnBtLXVzZXIubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsTUFBTSxPQUFPLFlBQVk7Ozs7SUFzQnJCLFlBQVksS0FBVztRQUNuQixJQUFJLEtBQUssRUFBRTtZQUNQLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztZQUN2QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQyxZQUFZLENBQUM7WUFDdkMsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO1lBQzdCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQztZQUM3QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7WUFDekIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDO1lBQ25DLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQztZQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDL0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQy9CLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztZQUMzQixJQUFJLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQyxFQUFFLENBQUM7WUFDbkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDO1lBQ25DLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUMsbUJBQW1CLENBQUM7WUFDckQsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQy9CLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQztZQUNqQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7WUFDM0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQy9CLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQztZQUNuQyxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQyxlQUFlLENBQUM7WUFDN0MsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO1NBQzFCO0lBQ0wsQ0FBQztDQUNKOzs7SUE3Q0csNEJBQVU7O0lBQ1Ysb0NBQXVCOztJQUN2QiwrQkFBZ0I7O0lBQ2hCLCtCQUFjOztJQUNkLDZCQUFjOztJQUNkLGtDQUFtQjs7SUFDbkIsaUNBQWtCOztJQUNsQixnQ0FBaUI7O0lBQ2pCLGdDQUFpQjs7SUFDakIsOEJBQVk7O0lBQ1osMEJBQVc7O0lBQ1gsa0NBQWlCOztJQUNqQiwyQ0FBMEI7O0lBQzFCLGdDQUFpQjs7SUFDakIsaUNBQWtCOztJQUNsQiw4QkFBZTs7SUFDZixnQ0FBaUI7O0lBQ2pCLGtDQUFtQjs7SUFDbkIsdUNBQXdCOztJQUN4Qiw0QkFBYSIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBVc2VyUmVwcmVzZW50YXRpb24gfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuXHJcbmV4cG9ydCBjbGFzcyBCcG1Vc2VyTW9kZWwgaW1wbGVtZW50cyBVc2VyUmVwcmVzZW50YXRpb24ge1xyXG4gICAgYXBwczogYW55O1xyXG4gICAgY2FwYWJpbGl0aWVzOiBzdHJpbmdbXTtcclxuICAgIGNvbXBhbnk6IHN0cmluZztcclxuICAgIGNyZWF0ZWQ6IERhdGU7XHJcbiAgICBlbWFpbDogc3RyaW5nO1xyXG4gICAgZXh0ZXJuYWxJZDogc3RyaW5nO1xyXG4gICAgZmlyc3ROYW1lOiBzdHJpbmc7XHJcbiAgICBsYXN0TmFtZTogc3RyaW5nO1xyXG4gICAgZnVsbG5hbWU6IHN0cmluZztcclxuICAgIGdyb3VwczogYW55O1xyXG4gICAgaWQ6IG51bWJlcjtcclxuICAgIGxhc3RVcGRhdGU6IERhdGU7XHJcbiAgICBsYXRlc3RTeW5jVGltZVN0YW1wOiBEYXRlO1xyXG4gICAgcGFzc3dvcmQ6IHN0cmluZztcclxuICAgIHBpY3R1cmVJZDogbnVtYmVyO1xyXG4gICAgc3RhdHVzOiBzdHJpbmc7XHJcbiAgICB0ZW5hbnRJZDogbnVtYmVyO1xyXG4gICAgdGVuYW50TmFtZTogc3RyaW5nO1xyXG4gICAgdGVuYW50UGljdHVyZUlkOiBudW1iZXI7XHJcbiAgICB0eXBlOiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IoaW5wdXQ/OiBhbnkpIHtcclxuICAgICAgICBpZiAoaW5wdXQpIHtcclxuICAgICAgICAgICAgdGhpcy5hcHBzID0gaW5wdXQuYXBwcztcclxuICAgICAgICAgICAgdGhpcy5jYXBhYmlsaXRpZXMgPSBpbnB1dC5jYXBhYmlsaXRpZXM7XHJcbiAgICAgICAgICAgIHRoaXMuY29tcGFueSA9IGlucHV0LmNvbXBhbnk7XHJcbiAgICAgICAgICAgIHRoaXMuY3JlYXRlZCA9IGlucHV0LmNyZWF0ZWQ7XHJcbiAgICAgICAgICAgIHRoaXMuZW1haWwgPSBpbnB1dC5lbWFpbDtcclxuICAgICAgICAgICAgdGhpcy5leHRlcm5hbElkID0gaW5wdXQuZXh0ZXJuYWxJZDtcclxuICAgICAgICAgICAgdGhpcy5maXJzdE5hbWUgPSBpbnB1dC5maXJzdE5hbWU7XHJcbiAgICAgICAgICAgIHRoaXMubGFzdE5hbWUgPSBpbnB1dC5sYXN0TmFtZTtcclxuICAgICAgICAgICAgdGhpcy5mdWxsbmFtZSA9IGlucHV0LmZ1bGxuYW1lO1xyXG4gICAgICAgICAgICB0aGlzLmdyb3VwcyA9IGlucHV0Lmdyb3VwcztcclxuICAgICAgICAgICAgdGhpcy5pZCA9IGlucHV0LmlkO1xyXG4gICAgICAgICAgICB0aGlzLmxhc3RVcGRhdGUgPSBpbnB1dC5sYXN0VXBkYXRlO1xyXG4gICAgICAgICAgICB0aGlzLmxhdGVzdFN5bmNUaW1lU3RhbXAgPSBpbnB1dC5sYXRlc3RTeW5jVGltZVN0YW1wO1xyXG4gICAgICAgICAgICB0aGlzLnBhc3N3b3JkID0gaW5wdXQucGFzc3dvcmQ7XHJcbiAgICAgICAgICAgIHRoaXMucGljdHVyZUlkID0gaW5wdXQucGljdHVyZUlkO1xyXG4gICAgICAgICAgICB0aGlzLnN0YXR1cyA9IGlucHV0LnN0YXR1cztcclxuICAgICAgICAgICAgdGhpcy50ZW5hbnRJZCA9IGlucHV0LnRlbmFudElkO1xyXG4gICAgICAgICAgICB0aGlzLnRlbmFudE5hbWUgPSBpbnB1dC50ZW5hbnROYW1lO1xyXG4gICAgICAgICAgICB0aGlzLnRlbmFudFBpY3R1cmVJZCA9IGlucHV0LnRlbmFudFBpY3R1cmVJZDtcclxuICAgICAgICAgICAgdGhpcy50eXBlID0gaW5wdXQudHlwZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19