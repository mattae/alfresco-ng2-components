/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class EcmUserModel {
    /**
     * @param {?=} obj
     */
    constructor(obj) {
        this.id = obj && obj.id || null;
        this.firstName = obj && obj.firstName;
        this.lastName = obj && obj.lastName;
        this.description = obj && obj.description || null;
        this.avatarId = obj && obj.avatarId || null;
        this.email = obj && obj.email || null;
        this.skypeId = obj && obj.skypeId;
        this.googleId = obj && obj.googleId;
        this.instantMessageId = obj && obj.instantMessageId;
        this.jobTitle = obj && obj.jobTitle || null;
        this.location = obj && obj.location || null;
        this.company = obj && obj.company;
        this.mobile = obj && obj.mobile;
        this.telephone = obj && obj.telephone;
        this.statusUpdatedAt = obj && obj.statusUpdatedAt;
        this.userStatus = obj && obj.userStatus;
        this.enabled = obj && obj.enabled;
        this.emailNotificationsEnabled = obj && obj.emailNotificationsEnabled;
    }
}
if (false) {
    /** @type {?} */
    EcmUserModel.prototype.id;
    /** @type {?} */
    EcmUserModel.prototype.firstName;
    /** @type {?} */
    EcmUserModel.prototype.lastName;
    /** @type {?} */
    EcmUserModel.prototype.description;
    /** @type {?} */
    EcmUserModel.prototype.avatarId;
    /** @type {?} */
    EcmUserModel.prototype.email;
    /** @type {?} */
    EcmUserModel.prototype.skypeId;
    /** @type {?} */
    EcmUserModel.prototype.googleId;
    /** @type {?} */
    EcmUserModel.prototype.instantMessageId;
    /** @type {?} */
    EcmUserModel.prototype.jobTitle;
    /** @type {?} */
    EcmUserModel.prototype.location;
    /** @type {?} */
    EcmUserModel.prototype.company;
    /** @type {?} */
    EcmUserModel.prototype.mobile;
    /** @type {?} */
    EcmUserModel.prototype.telephone;
    /** @type {?} */
    EcmUserModel.prototype.statusUpdatedAt;
    /** @type {?} */
    EcmUserModel.prototype.userStatus;
    /** @type {?} */
    EcmUserModel.prototype.enabled;
    /** @type {?} */
    EcmUserModel.prototype.emailNotificationsEnabled;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWNtLXVzZXIubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJ1c2VyaW5mby9tb2RlbHMvZWNtLXVzZXIubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQkEsTUFBTSxPQUFPLFlBQVk7Ozs7SUFvQnJCLFlBQVksR0FBUztRQUNqQixJQUFJLENBQUMsRUFBRSxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQztRQUNoQyxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsU0FBUyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxRQUFRLENBQUM7UUFDcEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUM7UUFDbEQsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUM7UUFDNUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUM7UUFDdEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQztRQUNsQyxJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLGdCQUFnQixDQUFDO1FBQ3BELElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDO1FBQzVDLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDO1FBQzVDLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUM7UUFDbEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLE1BQU0sQ0FBQztRQUNoQyxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsU0FBUyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxlQUFlLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxlQUFlLENBQUM7UUFDbEQsSUFBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLFVBQVUsQ0FBQztRQUN4QyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDO1FBQ2xDLElBQUksQ0FBQyx5QkFBeUIsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLHlCQUF5QixDQUFDO0lBQzFFLENBQUM7Q0FDSjs7O0lBdkNHLDBCQUFXOztJQUNYLGlDQUFrQjs7SUFDbEIsZ0NBQWlCOztJQUNqQixtQ0FBb0I7O0lBQ3BCLGdDQUFpQjs7SUFDakIsNkJBQWM7O0lBQ2QsK0JBQWdCOztJQUNoQixnQ0FBaUI7O0lBQ2pCLHdDQUF5Qjs7SUFDekIsZ0NBQWlCOztJQUNqQixnQ0FBaUI7O0lBQ2pCLCtCQUF5Qjs7SUFDekIsOEJBQWU7O0lBQ2YsaUNBQWtCOztJQUNsQix1Q0FBc0I7O0lBQ3RCLGtDQUFtQjs7SUFDbkIsK0JBQWlCOztJQUNqQixpREFBbUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgUGVyc29uIH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IEVjbUNvbXBhbnlNb2RlbCB9IGZyb20gJy4uLy4uL21vZGVscy9lY20tY29tcGFueS5tb2RlbCc7XHJcblxyXG5leHBvcnQgY2xhc3MgRWNtVXNlck1vZGVsIGltcGxlbWVudHMgUGVyc29uIHtcclxuICAgIGlkOiBzdHJpbmc7XHJcbiAgICBmaXJzdE5hbWU6IHN0cmluZztcclxuICAgIGxhc3ROYW1lOiBzdHJpbmc7XHJcbiAgICBkZXNjcmlwdGlvbjogc3RyaW5nO1xyXG4gICAgYXZhdGFySWQ6IHN0cmluZztcclxuICAgIGVtYWlsOiBzdHJpbmc7XHJcbiAgICBza3lwZUlkOiBzdHJpbmc7XHJcbiAgICBnb29nbGVJZDogc3RyaW5nO1xyXG4gICAgaW5zdGFudE1lc3NhZ2VJZDogc3RyaW5nO1xyXG4gICAgam9iVGl0bGU6IHN0cmluZztcclxuICAgIGxvY2F0aW9uOiBzdHJpbmc7XHJcbiAgICBjb21wYW55OiBFY21Db21wYW55TW9kZWw7XHJcbiAgICBtb2JpbGU6IHN0cmluZztcclxuICAgIHRlbGVwaG9uZTogc3RyaW5nO1xyXG4gICAgc3RhdHVzVXBkYXRlZEF0OiBEYXRlO1xyXG4gICAgdXNlclN0YXR1czogc3RyaW5nO1xyXG4gICAgZW5hYmxlZDogYm9vbGVhbjtcclxuICAgIGVtYWlsTm90aWZpY2F0aW9uc0VuYWJsZWQ6IGJvb2xlYW47XHJcblxyXG4gICAgY29uc3RydWN0b3Iob2JqPzogYW55KSB7XHJcbiAgICAgICAgdGhpcy5pZCA9IG9iaiAmJiBvYmouaWQgfHwgbnVsbDtcclxuICAgICAgICB0aGlzLmZpcnN0TmFtZSA9IG9iaiAmJiBvYmouZmlyc3ROYW1lO1xyXG4gICAgICAgIHRoaXMubGFzdE5hbWUgPSBvYmogJiYgb2JqLmxhc3ROYW1lO1xyXG4gICAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSBvYmogJiYgb2JqLmRlc2NyaXB0aW9uIHx8IG51bGw7XHJcbiAgICAgICAgdGhpcy5hdmF0YXJJZCA9IG9iaiAmJiBvYmouYXZhdGFySWQgfHwgbnVsbDtcclxuICAgICAgICB0aGlzLmVtYWlsID0gb2JqICYmIG9iai5lbWFpbCB8fCBudWxsO1xyXG4gICAgICAgIHRoaXMuc2t5cGVJZCA9IG9iaiAmJiBvYmouc2t5cGVJZDtcclxuICAgICAgICB0aGlzLmdvb2dsZUlkID0gb2JqICYmIG9iai5nb29nbGVJZDtcclxuICAgICAgICB0aGlzLmluc3RhbnRNZXNzYWdlSWQgPSBvYmogJiYgb2JqLmluc3RhbnRNZXNzYWdlSWQ7XHJcbiAgICAgICAgdGhpcy5qb2JUaXRsZSA9IG9iaiAmJiBvYmouam9iVGl0bGUgfHwgbnVsbDtcclxuICAgICAgICB0aGlzLmxvY2F0aW9uID0gb2JqICYmIG9iai5sb2NhdGlvbiB8fCBudWxsO1xyXG4gICAgICAgIHRoaXMuY29tcGFueSA9IG9iaiAmJiBvYmouY29tcGFueTtcclxuICAgICAgICB0aGlzLm1vYmlsZSA9IG9iaiAmJiBvYmoubW9iaWxlO1xyXG4gICAgICAgIHRoaXMudGVsZXBob25lID0gb2JqICYmIG9iai50ZWxlcGhvbmU7XHJcbiAgICAgICAgdGhpcy5zdGF0dXNVcGRhdGVkQXQgPSBvYmogJiYgb2JqLnN0YXR1c1VwZGF0ZWRBdDtcclxuICAgICAgICB0aGlzLnVzZXJTdGF0dXMgPSBvYmogJiYgb2JqLnVzZXJTdGF0dXM7XHJcbiAgICAgICAgdGhpcy5lbmFibGVkID0gb2JqICYmIG9iai5lbmFibGVkO1xyXG4gICAgICAgIHRoaXMuZW1haWxOb3RpZmljYXRpb25zRW5hYmxlZCA9IG9iaiAmJiBvYmouZW1haWxOb3RpZmljYXRpb25zRW5hYmxlZDtcclxuICAgIH1cclxufVxyXG4iXX0=