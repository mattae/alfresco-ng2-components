/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class IdentityGroupModel {
    /**
     * @param {?=} obj
     */
    constructor(obj) {
        if (obj) {
            this.id = obj.id || null;
            this.name = obj.name || null;
            this.path = obj.path || null;
            this.realmRoles = obj.realmRoles || null;
            this.clientRoles = obj.clientRoles || null;
            this.access = obj.access || null;
            this.attributes = obj.attributes || null;
        }
    }
}
if (false) {
    /** @type {?} */
    IdentityGroupModel.prototype.id;
    /** @type {?} */
    IdentityGroupModel.prototype.name;
    /** @type {?} */
    IdentityGroupModel.prototype.path;
    /** @type {?} */
    IdentityGroupModel.prototype.realmRoles;
    /** @type {?} */
    IdentityGroupModel.prototype.clientRoles;
    /** @type {?} */
    IdentityGroupModel.prototype.access;
    /** @type {?} */
    IdentityGroupModel.prototype.attributes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWRlbnRpdHktZ3JvdXAubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJ1c2VyaW5mby9tb2RlbHMvaWRlbnRpdHktZ3JvdXAubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkEsTUFBTSxPQUFPLGtCQUFrQjs7OztJQVUzQixZQUFZLEdBQVM7UUFDakIsSUFBSSxHQUFHLEVBQUU7WUFDTCxJQUFJLENBQUMsRUFBRSxHQUFHLEdBQUcsQ0FBQyxFQUFFLElBQUksSUFBSSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUM7WUFDN0IsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQztZQUM3QixJQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDO1lBQ3pDLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUM7WUFDM0MsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQztZQUNqQyxJQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDO1NBQzVDO0lBQ0wsQ0FBQztDQUNKOzs7SUFuQkcsZ0NBQVc7O0lBQ1gsa0NBQWE7O0lBQ2Isa0NBQWE7O0lBQ2Isd0NBQXFCOztJQUNyQix5Q0FBaUI7O0lBQ2pCLG9DQUFZOztJQUNaLHdDQUFnQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBJZGVudGl0eUdyb3VwTW9kZWwge1xyXG5cclxuICAgIGlkOiBzdHJpbmc7XHJcbiAgICBuYW1lOiBzdHJpbmc7XHJcbiAgICBwYXRoOiBzdHJpbmc7XHJcbiAgICByZWFsbVJvbGVzOiBzdHJpbmdbXTtcclxuICAgIGNsaWVudFJvbGVzOiBhbnk7XHJcbiAgICBhY2Nlc3M6IGFueTtcclxuICAgIGF0dHJpYnV0ZXM6IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihvYmo/OiBhbnkpIHtcclxuICAgICAgICBpZiAob2JqKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaWQgPSBvYmouaWQgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5uYW1lID0gb2JqLm5hbWUgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5wYXRoID0gb2JqLnBhdGggfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5yZWFsbVJvbGVzID0gb2JqLnJlYWxtUm9sZXMgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5jbGllbnRSb2xlcyA9IG9iai5jbGllbnRSb2xlcyB8fCBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLmFjY2VzcyA9IG9iai5hY2Nlc3MgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5hdHRyaWJ1dGVzID0gb2JqLmF0dHJpYnV0ZXMgfHwgbnVsbDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19