/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class IdentityRoleModel {
    /**
     * @param {?=} obj
     */
    constructor(obj) {
        if (obj) {
            this.id = obj.id || null;
            this.name = obj.name || null;
            this.description = obj.description || null;
            this.clientRole = obj.clientRole || null;
            this.composite = obj.composite || null;
            this.containerId = obj.containerId || null;
            this.scopeParamRequired = obj.scopeParamRequired || null;
        }
    }
}
if (false) {
    /** @type {?} */
    IdentityRoleModel.prototype.id;
    /** @type {?} */
    IdentityRoleModel.prototype.name;
    /** @type {?} */
    IdentityRoleModel.prototype.description;
    /** @type {?} */
    IdentityRoleModel.prototype.clientRole;
    /** @type {?} */
    IdentityRoleModel.prototype.composite;
    /** @type {?} */
    IdentityRoleModel.prototype.containerId;
    /** @type {?} */
    IdentityRoleModel.prototype.scopeParamRequired;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWRlbnRpdHktcm9sZS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInVzZXJpbmZvL21vZGVscy9pZGVudGl0eS1yb2xlLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBLE1BQU0sT0FBTyxpQkFBaUI7Ozs7SUFTMUIsWUFBWSxHQUFTO1FBQ2pCLElBQUksR0FBRyxFQUFFO1lBQ0wsSUFBSSxDQUFDLEVBQUUsR0FBRyxHQUFHLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQztZQUN6QixJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDO1lBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUM7WUFDM0MsSUFBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLENBQUMsVUFBVSxJQUFJLElBQUksQ0FBQztZQUN6QyxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxXQUFXLEdBQUcsR0FBRyxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUM7WUFDM0MsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEdBQUcsQ0FBQyxrQkFBa0IsSUFBSSxJQUFJLENBQUM7U0FDNUQ7SUFDTCxDQUFDO0NBQ0o7OztJQW5CRywrQkFBVzs7SUFDWCxpQ0FBYTs7SUFDYix3Q0FBcUI7O0lBQ3JCLHVDQUFxQjs7SUFDckIsc0NBQW9COztJQUNwQix3Q0FBcUI7O0lBQ3JCLCtDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcbmV4cG9ydCBjbGFzcyBJZGVudGl0eVJvbGVNb2RlbCB7XHJcbiAgICBpZDogc3RyaW5nO1xyXG4gICAgbmFtZTogc3RyaW5nO1xyXG4gICAgZGVzY3JpcHRpb24/OiBzdHJpbmc7XHJcbiAgICBjbGllbnRSb2xlPzogYm9vbGVhbjtcclxuICAgIGNvbXBvc2l0ZT86IGJvb2xlYW47XHJcbiAgICBjb250YWluZXJJZD86IHN0cmluZztcclxuICAgIHNjb3BlUGFyYW1SZXF1aXJlZD86IGJvb2xlYW47XHJcblxyXG4gICAgY29uc3RydWN0b3Iob2JqPzogYW55KSB7XHJcbiAgICAgICAgaWYgKG9iaikge1xyXG4gICAgICAgICAgICB0aGlzLmlkID0gb2JqLmlkIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMubmFtZSA9IG9iai5uYW1lIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuZGVzY3JpcHRpb24gPSBvYmouZGVzY3JpcHRpb24gfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5jbGllbnRSb2xlID0gb2JqLmNsaWVudFJvbGUgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5jb21wb3NpdGUgPSBvYmouY29tcG9zaXRlIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuY29udGFpbmVySWQgPSBvYmouY29udGFpbmVySWQgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5zY29wZVBhcmFtUmVxdWlyZWQgPSBvYmouc2NvcGVQYXJhbVJlcXVpcmVkIHx8IG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==