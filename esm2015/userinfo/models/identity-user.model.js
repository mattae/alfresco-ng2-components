/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export class IdentityUserModel {
    /**
     * @param {?=} obj
     */
    constructor(obj) {
        if (obj) {
            this.id = obj.id || null;
            this.firstName = obj.firstName || null;
            this.lastName = obj.lastName || null;
            this.email = obj.email || null;
            this.username = obj.username || null;
            this.createdTimestamp = obj.createdTimestamp || null;
            this.emailVerified = obj.emailVerified || null;
            this.enabled = obj.enabled || null;
        }
    }
}
if (false) {
    /** @type {?} */
    IdentityUserModel.prototype.id;
    /** @type {?} */
    IdentityUserModel.prototype.firstName;
    /** @type {?} */
    IdentityUserModel.prototype.lastName;
    /** @type {?} */
    IdentityUserModel.prototype.email;
    /** @type {?} */
    IdentityUserModel.prototype.username;
    /** @type {?} */
    IdentityUserModel.prototype.createdTimestamp;
    /** @type {?} */
    IdentityUserModel.prototype.emailVerified;
    /** @type {?} */
    IdentityUserModel.prototype.enabled;
}
export class IdentityUserPasswordModel {
    /**
     * @param {?=} obj
     */
    constructor(obj) {
        if (obj) {
            this.type = obj.type;
            this.value = obj.value;
            this.temporary = obj.temporary;
        }
    }
}
if (false) {
    /** @type {?} */
    IdentityUserPasswordModel.prototype.type;
    /** @type {?} */
    IdentityUserPasswordModel.prototype.value;
    /** @type {?} */
    IdentityUserPasswordModel.prototype.temporary;
}
/**
 * @record
 */
export function IdentityUserQueryResponse() { }
if (false) {
    /** @type {?} */
    IdentityUserQueryResponse.prototype.entries;
    /** @type {?} */
    IdentityUserQueryResponse.prototype.pagination;
}
export class IdentityUserQueryCloudRequestModel {
    /**
     * @param {?=} obj
     */
    constructor(obj) {
        if (obj) {
            this.first = obj.first;
            this.max = obj.max;
        }
    }
}
if (false) {
    /** @type {?} */
    IdentityUserQueryCloudRequestModel.prototype.first;
    /** @type {?} */
    IdentityUserQueryCloudRequestModel.prototype.max;
}
export class IdentityJoinGroupRequestModel {
    /**
     * @param {?=} obj
     */
    constructor(obj) {
        if (obj) {
            this.realm = obj.realm;
            this.userId = obj.userId;
            this.groupId = obj.groupId;
        }
    }
}
if (false) {
    /** @type {?} */
    IdentityJoinGroupRequestModel.prototype.realm;
    /** @type {?} */
    IdentityJoinGroupRequestModel.prototype.userId;
    /** @type {?} */
    IdentityJoinGroupRequestModel.prototype.groupId;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWRlbnRpdHktdXNlci5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInVzZXJpbmZvL21vZGVscy9pZGVudGl0eS11c2VyLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLE1BQU0sT0FBTyxpQkFBaUI7Ozs7SUFVMUIsWUFBWSxHQUFTO1FBQ2pCLElBQUksR0FBRyxFQUFFO1lBQ0wsSUFBSSxDQUFDLEVBQUUsR0FBRyxHQUFHLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQztZQUN6QixJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUM7WUFDckMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQztZQUMvQixJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDO1lBQ3JDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxHQUFHLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDO1lBQ3JELElBQUksQ0FBQyxhQUFhLEdBQUcsR0FBRyxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUM7WUFDL0MsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQztTQUN0QztJQUNMLENBQUM7Q0FDSjs7O0lBckJHLCtCQUFXOztJQUNYLHNDQUFrQjs7SUFDbEIscUNBQWlCOztJQUNqQixrQ0FBYzs7SUFDZCxxQ0FBaUI7O0lBQ2pCLDZDQUF1Qjs7SUFDdkIsMENBQXdCOztJQUN4QixvQ0FBa0I7O0FBZ0J0QixNQUFNLE9BQU8seUJBQXlCOzs7O0lBTWxDLFlBQVksR0FBUztRQUNqQixJQUFJLEdBQUcsRUFBRTtZQUNMLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLElBQUksQ0FBQztZQUNyQixJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUM7WUFDdkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxHQUFHLENBQUMsU0FBUyxDQUFDO1NBQ2xDO0lBQ0wsQ0FBQztDQUNKOzs7SUFYRyx5Q0FBYTs7SUFDYiwwQ0FBYzs7SUFDZCw4Q0FBbUI7Ozs7O0FBV3ZCLCtDQUlDOzs7SUFGRyw0Q0FBNkI7O0lBQzdCLCtDQUF1Qjs7QUFHM0IsTUFBTSxPQUFPLGtDQUFrQzs7OztJQUszQyxZQUFZLEdBQVM7UUFDakIsSUFBSSxHQUFHLEVBQUU7WUFDTCxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUM7WUFDdkIsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsR0FBRyxDQUFDO1NBQ3RCO0lBQ0wsQ0FBQztDQUNKOzs7SUFURyxtREFBYzs7SUFDZCxpREFBWTs7QUFVaEIsTUFBTSxPQUFPLDZCQUE2Qjs7OztJQU10QyxZQUFZLEdBQVM7UUFDakIsSUFBSSxHQUFHLEVBQUU7WUFDTCxJQUFJLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUM7WUFDdkIsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQztTQUM5QjtJQUNMLENBQUM7Q0FDSjs7O0lBWEcsOENBQWM7O0lBQ2QsK0NBQWU7O0lBQ2YsZ0RBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IFBhZ2luYXRpb24gfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuXHJcbmV4cG9ydCBjbGFzcyBJZGVudGl0eVVzZXJNb2RlbCB7XHJcbiAgICBpZDogc3RyaW5nO1xyXG4gICAgZmlyc3ROYW1lOiBzdHJpbmc7XHJcbiAgICBsYXN0TmFtZTogc3RyaW5nO1xyXG4gICAgZW1haWw6IHN0cmluZztcclxuICAgIHVzZXJuYW1lOiBzdHJpbmc7XHJcbiAgICBjcmVhdGVkVGltZXN0YW1wPzogYW55O1xyXG4gICAgZW1haWxWZXJpZmllZD86IGJvb2xlYW47XHJcbiAgICBlbmFibGVkPzogYm9vbGVhbjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihvYmo/OiBhbnkpIHtcclxuICAgICAgICBpZiAob2JqKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaWQgPSBvYmouaWQgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5maXJzdE5hbWUgPSBvYmouZmlyc3ROYW1lIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMubGFzdE5hbWUgPSBvYmoubGFzdE5hbWUgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5lbWFpbCA9IG9iai5lbWFpbCB8fCBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLnVzZXJuYW1lID0gb2JqLnVzZXJuYW1lIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuY3JlYXRlZFRpbWVzdGFtcCA9IG9iai5jcmVhdGVkVGltZXN0YW1wIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuZW1haWxWZXJpZmllZCA9IG9iai5lbWFpbFZlcmlmaWVkIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuZW5hYmxlZCA9IG9iai5lbmFibGVkIHx8IG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgSWRlbnRpdHlVc2VyUGFzc3dvcmRNb2RlbCB7XHJcblxyXG4gICAgdHlwZTogc3RyaW5nO1xyXG4gICAgdmFsdWU6IHN0cmluZztcclxuICAgIHRlbXBvcmFyeTogYm9vbGVhbjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihvYmo/OiBhbnkpIHtcclxuICAgICAgICBpZiAob2JqKSB7XHJcbiAgICAgICAgICAgIHRoaXMudHlwZSA9IG9iai50eXBlO1xyXG4gICAgICAgICAgICB0aGlzLnZhbHVlID0gb2JqLnZhbHVlO1xyXG4gICAgICAgICAgICB0aGlzLnRlbXBvcmFyeSA9IG9iai50ZW1wb3Jhcnk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIElkZW50aXR5VXNlclF1ZXJ5UmVzcG9uc2Uge1xyXG5cclxuICAgIGVudHJpZXM6IElkZW50aXR5VXNlck1vZGVsW107XHJcbiAgICBwYWdpbmF0aW9uOiBQYWdpbmF0aW9uO1xyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgSWRlbnRpdHlVc2VyUXVlcnlDbG91ZFJlcXVlc3RNb2RlbCB7XHJcblxyXG4gICAgZmlyc3Q6IG51bWJlcjtcclxuICAgIG1heDogbnVtYmVyO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKG9iaj86IGFueSkge1xyXG4gICAgICAgIGlmIChvYmopIHtcclxuICAgICAgICAgICAgdGhpcy5maXJzdCA9IG9iai5maXJzdDtcclxuICAgICAgICAgICAgdGhpcy5tYXggPSBvYmoubWF4O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIElkZW50aXR5Sm9pbkdyb3VwUmVxdWVzdE1vZGVsIHtcclxuXHJcbiAgICByZWFsbTogc3RyaW5nO1xyXG4gICAgdXNlcklkOiBzdHJpbmc7XHJcbiAgICBncm91cElkOiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3Iob2JqPzogYW55KSB7XHJcbiAgICAgICAgaWYgKG9iaikge1xyXG4gICAgICAgICAgICB0aGlzLnJlYWxtID0gb2JqLnJlYWxtO1xyXG4gICAgICAgICAgICB0aGlzLnVzZXJJZCA9IG9iai51c2VySWQ7XHJcbiAgICAgICAgICAgIHRoaXMuZ3JvdXBJZCA9IG9iai5ncm91cElkO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=