/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export { UserInfoComponent } from './components/user-info.component';
export { BpmUserService } from './services/bpm-user.service';
export { EcmUserService } from './services/ecm-user.service';
export { IdentityUserService } from './services/identity-user.service';
export { BpmUserModel } from './models/bpm-user.model';
export { EcmUserModel } from './models/ecm-user.model';
export { IdentityUserModel, IdentityUserPasswordModel, IdentityUserQueryCloudRequestModel, IdentityJoinGroupRequestModel } from './models/identity-user.model';
export { IdentityRoleModel } from './models/identity-role.model';
export { IdentityGroupModel } from './models/identity-group.model';
export { UserInfoModule } from './userinfo.module';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInVzZXJpbmZvL3B1YmxpYy1hcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsa0NBQWMsa0NBQWtDLENBQUM7QUFDakQsK0JBQWMsNkJBQTZCLENBQUM7QUFDNUMsK0JBQWMsNkJBQTZCLENBQUM7QUFDNUMsb0NBQWMsa0NBQWtDLENBQUM7QUFDakQsNkJBQWMseUJBQXlCLENBQUM7QUFDeEMsNkJBQWMseUJBQXlCLENBQUM7QUFDeEMsZ0lBQWMsOEJBQThCLENBQUM7QUFDN0Msa0NBQWMsOEJBQThCLENBQUM7QUFDN0MsbUNBQWMsK0JBQStCLENBQUM7QUFFOUMsK0JBQWMsbUJBQW1CLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxuICpcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cbiAqL1xuXG5leHBvcnQgKiBmcm9tICcuL2NvbXBvbmVudHMvdXNlci1pbmZvLmNvbXBvbmVudCc7XG5leHBvcnQgKiBmcm9tICcuL3NlcnZpY2VzL2JwbS11c2VyLnNlcnZpY2UnO1xuZXhwb3J0ICogZnJvbSAnLi9zZXJ2aWNlcy9lY20tdXNlci5zZXJ2aWNlJztcbmV4cG9ydCAqIGZyb20gJy4vc2VydmljZXMvaWRlbnRpdHktdXNlci5zZXJ2aWNlJztcbmV4cG9ydCAqIGZyb20gJy4vbW9kZWxzL2JwbS11c2VyLm1vZGVsJztcbmV4cG9ydCAqIGZyb20gJy4vbW9kZWxzL2VjbS11c2VyLm1vZGVsJztcbmV4cG9ydCAqIGZyb20gJy4vbW9kZWxzL2lkZW50aXR5LXVzZXIubW9kZWwnO1xuZXhwb3J0ICogZnJvbSAnLi9tb2RlbHMvaWRlbnRpdHktcm9sZS5tb2RlbCc7XG5leHBvcnQgKiBmcm9tICcuL21vZGVscy9pZGVudGl0eS1ncm91cC5tb2RlbCc7XG5cbmV4cG9ydCAqIGZyb20gJy4vdXNlcmluZm8ubW9kdWxlJztcbiJdfQ==