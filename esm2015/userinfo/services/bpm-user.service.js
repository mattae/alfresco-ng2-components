/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import { BpmUserModel } from '../models/bpm-user.model';
import { map, catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "../../services/alfresco-api.service";
import * as i2 from "../../services/log.service";
/**
 *
 * BPMUserService retrieve all the information of an Ecm user.
 *
 */
export class BpmUserService {
    /**
     * @param {?} apiService
     * @param {?} logService
     */
    constructor(apiService, logService) {
        this.apiService = apiService;
        this.logService = logService;
    }
    /**
     * Gets information about the current user.
     * @return {?} User information object
     */
    getCurrentUserInfo() {
        return from(this.apiService.getInstance().activiti.profileApi.getProfile())
            .pipe(map((/**
         * @param {?} userRepresentation
         * @return {?}
         */
        (userRepresentation) => {
            return new BpmUserModel(userRepresentation);
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets the current user's profile image as a URL.
     * @return {?} URL string
     */
    getCurrentUserProfileImage() {
        return this.apiService.getInstance().activiti.profileApi.getProfilePictureUrl();
    }
    /**
     * Throw the error
     * @private
     * @param {?} error
     * @return {?}
     */
    handleError(error) {
        // in a real world app, we may send the error to some remote logging infrastructure
        // instead of just logging it to the console
        this.logService.error(error);
        return throwError(error || 'Server error');
    }
}
BpmUserService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
BpmUserService.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: LogService }
];
/** @nocollapse */ BpmUserService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function BpmUserService_Factory() { return new BpmUserService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: BpmUserService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    BpmUserService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    BpmUserService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnBtLXVzZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInVzZXJpbmZvL3NlcnZpY2VzL2JwbS11c2VyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQWMsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNwRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUN6RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDeEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxHQUFHLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7Ozs7OztBQVdqRCxNQUFNLE9BQU8sY0FBYzs7Ozs7SUFFdkIsWUFBb0IsVUFBOEIsRUFDOUIsVUFBc0I7UUFEdEIsZUFBVSxHQUFWLFVBQVUsQ0FBb0I7UUFDOUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUMxQyxDQUFDOzs7OztJQU1ELGtCQUFrQjtRQUNkLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsQ0FBQzthQUN0RSxJQUFJLENBQ0QsR0FBRzs7OztRQUFDLENBQUMsa0JBQXNDLEVBQUUsRUFBRTtZQUMzQyxPQUFPLElBQUksWUFBWSxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDaEQsQ0FBQyxFQUFDLEVBQ0YsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7OztJQU1ELDBCQUEwQjtRQUN0QixPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0lBQ3BGLENBQUM7Ozs7Ozs7SUFNTyxXQUFXLENBQUMsS0FBVTtRQUMxQixtRkFBbUY7UUFDbkYsNENBQTRDO1FBQzVDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLE9BQU8sVUFBVSxDQUFDLEtBQUssSUFBSSxjQUFjLENBQUMsQ0FBQztJQUMvQyxDQUFDOzs7WUF4Q0osVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBYlEsa0JBQWtCO1lBQ2xCLFVBQVU7Ozs7Ozs7O0lBZUgsb0NBQXNDOzs7OztJQUN0QyxvQ0FBOEIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmcm9tLCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQnBtVXNlck1vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL2JwbS11c2VyLm1vZGVsJztcclxuaW1wb3J0IHsgbWFwLCBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBVc2VyUmVwcmVzZW50YXRpb24gfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuXHJcbi8qKlxyXG4gKlxyXG4gKiBCUE1Vc2VyU2VydmljZSByZXRyaWV2ZSBhbGwgdGhlIGluZm9ybWF0aW9uIG9mIGFuIEVjbSB1c2VyLlxyXG4gKlxyXG4gKi9cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBCcG1Vc2VyU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgaW5mb3JtYXRpb24gYWJvdXQgdGhlIGN1cnJlbnQgdXNlci5cclxuICAgICAqIEByZXR1cm5zIFVzZXIgaW5mb3JtYXRpb24gb2JqZWN0XHJcbiAgICAgKi9cclxuICAgIGdldEN1cnJlbnRVc2VySW5mbygpOiBPYnNlcnZhYmxlPEJwbVVzZXJNb2RlbD4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmFjdGl2aXRpLnByb2ZpbGVBcGkuZ2V0UHJvZmlsZSgpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCgodXNlclJlcHJlc2VudGF0aW9uOiBVc2VyUmVwcmVzZW50YXRpb24pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmV3IEJwbVVzZXJNb2RlbCh1c2VyUmVwcmVzZW50YXRpb24pO1xyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIGN1cnJlbnQgdXNlcidzIHByb2ZpbGUgaW1hZ2UgYXMgYSBVUkwuXHJcbiAgICAgKiBAcmV0dXJucyBVUkwgc3RyaW5nXHJcbiAgICAgKi9cclxuICAgIGdldEN1cnJlbnRVc2VyUHJvZmlsZUltYWdlKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmFjdGl2aXRpLnByb2ZpbGVBcGkuZ2V0UHJvZmlsZVBpY3R1cmVVcmwoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFRocm93IHRoZSBlcnJvclxyXG4gICAgICogQHBhcmFtIGVycm9yXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgaGFuZGxlRXJyb3IoZXJyb3I6IGFueSkge1xyXG4gICAgICAgIC8vIGluIGEgcmVhbCB3b3JsZCBhcHAsIHdlIG1heSBzZW5kIHRoZSBlcnJvciB0byBzb21lIHJlbW90ZSBsb2dnaW5nIGluZnJhc3RydWN0dXJlXHJcbiAgICAgICAgLy8gaW5zdGVhZCBvZiBqdXN0IGxvZ2dpbmcgaXQgdG8gdGhlIGNvbnNvbGVcclxuICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgIHJldHVybiB0aHJvd0Vycm9yKGVycm9yIHx8ICdTZXJ2ZXIgZXJyb3InKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19