/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ContentService } from '../../services/content.service';
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import { EcmUserModel } from '../models/ecm-user.model';
import * as i0 from "@angular/core";
import * as i1 from "../../services/alfresco-api.service";
import * as i2 from "../../services/content.service";
import * as i3 from "../../services/log.service";
export class EcmUserService {
    /**
     * @param {?} apiService
     * @param {?} contentService
     * @param {?} logService
     */
    constructor(apiService, contentService, logService) {
        this.apiService = apiService;
        this.contentService = contentService;
        this.logService = logService;
    }
    /**
     * Gets information about a user identified by their username.
     * @param {?} userName Target username
     * @return {?} User information
     */
    getUserInfo(userName) {
        return from(this.apiService.getInstance().core.peopleApi.getPerson(userName))
            .pipe(map((/**
         * @param {?} personEntry
         * @return {?}
         */
        (personEntry) => {
            return new EcmUserModel(personEntry.entry);
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        (err) => this.handleError(err))));
    }
    /**
     * Gets information about the user who is currently logged-in.
     * @return {?} User information as for getUserInfo
     */
    getCurrentUserInfo() {
        return this.getUserInfo('-me-');
    }
    /**
     * Returns a profile image as a URL.
     * @param {?} avatarId Target avatar
     * @return {?} Image URL
     */
    getUserProfileImage(avatarId) {
        if (avatarId) {
            return this.contentService.getContentUrl(avatarId);
        }
        return null;
    }
    /**
     * Throw the error
     * @private
     * @param {?} error
     * @return {?}
     */
    handleError(error) {
        this.logService.error(error);
        return throwError(error || 'Server error');
    }
}
EcmUserService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
EcmUserService.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: ContentService },
    { type: LogService }
];
/** @nocollapse */ EcmUserService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function EcmUserService_Factory() { return new EcmUserService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.ContentService), i0.ɵɵinject(i3.LogService)); }, token: EcmUserService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    EcmUserService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    EcmUserService.prototype.contentService;
    /**
     * @type {?}
     * @private
     */
    EcmUserService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWNtLXVzZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInVzZXJpbmZvL3NlcnZpY2VzL2VjbS11c2VyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQWMsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNwRCxPQUFPLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUN6RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDeEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDBCQUEwQixDQUFDOzs7OztBQU14RCxNQUFNLE9BQU8sY0FBYzs7Ozs7O0lBRXZCLFlBQW9CLFVBQThCLEVBQzlCLGNBQThCLEVBQzlCLFVBQXNCO1FBRnRCLGVBQVUsR0FBVixVQUFVLENBQW9CO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixlQUFVLEdBQVYsVUFBVSxDQUFZO0lBQzFDLENBQUM7Ozs7OztJQU9ELFdBQVcsQ0FBQyxRQUFnQjtRQUN4QixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQ3hFLElBQUksQ0FDRCxHQUFHOzs7O1FBQUMsQ0FBQyxXQUF3QixFQUFFLEVBQUU7WUFDN0IsT0FBTyxJQUFJLFlBQVksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDL0MsQ0FBQyxFQUFDLEVBQ0YsVUFBVTs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7OztJQU1ELGtCQUFrQjtRQUNkLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNwQyxDQUFDOzs7Ozs7SUFPRCxtQkFBbUIsQ0FBQyxRQUFnQjtRQUNoQyxJQUFJLFFBQVEsRUFBRTtZQUNWLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDdEQ7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOzs7Ozs7O0lBTU8sV0FBVyxDQUFDLEtBQVU7UUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsT0FBTyxVQUFVLENBQUMsS0FBSyxJQUFJLGNBQWMsQ0FBQyxDQUFDO0lBQy9DLENBQUM7OztZQXBESixVQUFVLFNBQUM7Z0JBQ1IsVUFBVSxFQUFFLE1BQU07YUFDckI7Ozs7WUFQUSxrQkFBa0I7WUFEbEIsY0FBYztZQUVkLFVBQVU7Ozs7Ozs7O0lBU0gsb0NBQXNDOzs7OztJQUN0Qyx3Q0FBc0M7Ozs7O0lBQ3RDLG9DQUE4QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZyb20sIHRocm93RXJyb3IgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgbWFwLCBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBDb250ZW50U2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2NvbnRlbnQuc2VydmljZSc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRWNtVXNlck1vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL2VjbS11c2VyLm1vZGVsJztcclxuaW1wb3J0IHsgUGVyc29uRW50cnkgfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRWNtVXNlclNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBjb250ZW50U2VydmljZTogQ29udGVudFNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgaW5mb3JtYXRpb24gYWJvdXQgYSB1c2VyIGlkZW50aWZpZWQgYnkgdGhlaXIgdXNlcm5hbWUuXHJcbiAgICAgKiBAcGFyYW0gdXNlck5hbWUgVGFyZ2V0IHVzZXJuYW1lXHJcbiAgICAgKiBAcmV0dXJucyBVc2VyIGluZm9ybWF0aW9uXHJcbiAgICAgKi9cclxuICAgIGdldFVzZXJJbmZvKHVzZXJOYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPEVjbVVzZXJNb2RlbD4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmNvcmUucGVvcGxlQXBpLmdldFBlcnNvbih1c2VyTmFtZSkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKChwZXJzb25FbnRyeTogUGVyc29uRW50cnkpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmV3IEVjbVVzZXJNb2RlbChwZXJzb25FbnRyeS5lbnRyeSk7XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBpbmZvcm1hdGlvbiBhYm91dCB0aGUgdXNlciB3aG8gaXMgY3VycmVudGx5IGxvZ2dlZC1pbi5cclxuICAgICAqIEByZXR1cm5zIFVzZXIgaW5mb3JtYXRpb24gYXMgZm9yIGdldFVzZXJJbmZvXHJcbiAgICAgKi9cclxuICAgIGdldEN1cnJlbnRVc2VySW5mbygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRVc2VySW5mbygnLW1lLScpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmV0dXJucyBhIHByb2ZpbGUgaW1hZ2UgYXMgYSBVUkwuXHJcbiAgICAgKiBAcGFyYW0gYXZhdGFySWQgVGFyZ2V0IGF2YXRhclxyXG4gICAgICogQHJldHVybnMgSW1hZ2UgVVJMXHJcbiAgICAgKi9cclxuICAgIGdldFVzZXJQcm9maWxlSW1hZ2UoYXZhdGFySWQ6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgaWYgKGF2YXRhcklkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmNvbnRlbnRTZXJ2aWNlLmdldENvbnRlbnRVcmwoYXZhdGFySWQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFRocm93IHRoZSBlcnJvclxyXG4gICAgICogQHBhcmFtIGVycm9yXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgaGFuZGxlRXJyb3IoZXJyb3I6IGFueSkge1xyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IgfHwgJ1NlcnZlciBlcnJvcicpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=