/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { of, from, throwError } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { IdentityUserModel } from '../models/identity-user.model';
import { JwtHelperService } from '../../services/jwt-helper.service';
import { LogService } from '../../services/log.service';
import { AppConfigService } from '../../app-config/app-config.service';
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import * as i0 from "@angular/core";
import * as i1 from "../../services/jwt-helper.service";
import * as i2 from "../../services/alfresco-api.service";
import * as i3 from "../../app-config/app-config.service";
import * as i4 from "../../services/log.service";
export class IdentityUserService {
    /**
     * @param {?} jwtHelperService
     * @param {?} alfrescoApiService
     * @param {?} appConfigService
     * @param {?} logService
     */
    constructor(jwtHelperService, alfrescoApiService, appConfigService, logService) {
        this.jwtHelperService = jwtHelperService;
        this.alfrescoApiService = alfrescoApiService;
        this.appConfigService = appConfigService;
        this.logService = logService;
    }
    /**
     * Gets the name and other basic details of the current user.
     * @return {?} The user's details
     */
    getCurrentUserInfo() {
        /** @type {?} */
        const familyName = this.jwtHelperService.getValueFromLocalAccessToken(JwtHelperService.FAMILY_NAME);
        /** @type {?} */
        const givenName = this.jwtHelperService.getValueFromLocalAccessToken(JwtHelperService.GIVEN_NAME);
        /** @type {?} */
        const email = this.jwtHelperService.getValueFromLocalAccessToken(JwtHelperService.USER_EMAIL);
        /** @type {?} */
        const username = this.jwtHelperService.getValueFromLocalAccessToken(JwtHelperService.USER_PREFERRED_USERNAME);
        /** @type {?} */
        const user = { firstName: givenName, lastName: familyName, email: email, username: username };
        return new IdentityUserModel(user);
    }
    /**
     * Find users based on search input.
     * @param {?} search Search query string
     * @return {?} List of users
     */
    findUsersByName(search) {
        if (search === '') {
            return of([]);
        }
        /** @type {?} */
        const url = this.buildUserUrl();
        /** @type {?} */
        const httpMethod = 'GET';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = { search: search };
        /** @type {?} */
        const bodyParam = {};
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return (from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, Object, null, null)));
    }
    /**
     * Find users based on username input.
     * @param {?} username Search query string
     * @return {?} List of users
     */
    findUserByUsername(username) {
        if (username === '') {
            return of([]);
        }
        /** @type {?} */
        const url = this.buildUserUrl();
        /** @type {?} */
        const httpMethod = 'GET';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = { username: username };
        /** @type {?} */
        const bodyParam = {};
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return (from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, Object, null, null)));
    }
    /**
     * Find users based on email input.
     * @param {?} email Search query string
     * @return {?} List of users
     */
    findUserByEmail(email) {
        if (email === '') {
            return of([]);
        }
        /** @type {?} */
        const url = this.buildUserUrl();
        /** @type {?} */
        const httpMethod = 'GET';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = { email: email };
        /** @type {?} */
        const bodyParam = {};
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return (from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, Object, null, null)));
    }
    /**
     * Find users based on id input.
     * @param {?} id Search query string
     * @return {?} users object
     */
    findUserById(id) {
        if (id === '') {
            return of([]);
        }
        /** @type {?} */
        const url = this.buildUserUrl() + '/' + id;
        /** @type {?} */
        const httpMethod = 'GET';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = {};
        /** @type {?} */
        const bodyParam = {};
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return (from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, Object, null, null)));
    }
    /**
     * Get client roles of a user for a particular client.
     * @param {?} userId ID of the target user
     * @param {?} clientId ID of the client app
     * @return {?} List of client roles
     */
    getClientRoles(userId, clientId) {
        /** @type {?} */
        const url = this.buildUserClientRoleMapping(userId, clientId);
        /** @type {?} */
        const httpMethod = 'GET';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = {};
        /** @type {?} */
        const bodyParam = {};
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, Object, null, null));
    }
    /**
     * Checks whether user has access to a client app.
     * @param {?} userId ID of the target user
     * @param {?} clientId ID of the client app
     * @return {?} True if the user has access, false otherwise
     */
    checkUserHasClientApp(userId, clientId) {
        return this.getClientRoles(userId, clientId).pipe(map((/**
         * @param {?} clientRoles
         * @return {?}
         */
        (clientRoles) => {
            if (clientRoles.length > 0) {
                return true;
            }
            return false;
        })));
    }
    /**
     * Checks whether a user has any of the client app roles.
     * @param {?} userId ID of the target user
     * @param {?} clientId ID of the client app
     * @param {?} roleNames List of role names to check for
     * @return {?} True if the user has one or more of the roles, false otherwise
     */
    checkUserHasAnyClientAppRole(userId, clientId, roleNames) {
        return this.getClientRoles(userId, clientId).pipe(map((/**
         * @param {?} clientRoles
         * @return {?}
         */
        (clientRoles) => {
            /** @type {?} */
            let hasRole = false;
            if (clientRoles.length > 0) {
                roleNames.forEach((/**
                 * @param {?} roleName
                 * @return {?}
                 */
                (roleName) => {
                    /** @type {?} */
                    const role = clientRoles.find((/**
                     * @param {?} availableRole
                     * @return {?}
                     */
                    (availableRole) => {
                        return availableRole.name === roleName;
                    }));
                    if (role) {
                        hasRole = true;
                        return;
                    }
                }));
            }
            return hasRole;
        })));
    }
    /**
     * Gets the client ID for an application.
     * @param {?} applicationName Name of the application
     * @return {?} Client ID string
     */
    getClientIdByApplicationName(applicationName) {
        /** @type {?} */
        const url = this.buildGetClientsUrl();
        /** @type {?} */
        const httpMethod = 'GET';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = { clientId: applicationName };
        /** @type {?} */
        const bodyParam = {};
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance()
            .oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, Object, null, null)).pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            /** @type {?} */
            const clientId = response && response.length > 0 ? response[0].id : '';
            return clientId;
        })));
    }
    /**
     * Checks if a user has access to an application.
     * @param {?} userId ID of the user
     * @param {?} applicationName Name of the application
     * @return {?} True if the user has access, false otherwise
     */
    checkUserHasApplicationAccess(userId, applicationName) {
        return this.getClientIdByApplicationName(applicationName).pipe(switchMap((/**
         * @param {?} clientId
         * @return {?}
         */
        (clientId) => {
            return this.checkUserHasClientApp(userId, clientId);
        })));
    }
    /**
     * Checks if a user has any application role.
     * @param {?} userId ID of the target user
     * @param {?} applicationName Name of the application
     * @param {?} roleNames List of role names to check for
     * @return {?} True if the user has one or more of the roles, false otherwise
     */
    checkUserHasAnyApplicationRole(userId, applicationName, roleNames) {
        return this.getClientIdByApplicationName(applicationName).pipe(switchMap((/**
         * @param {?} clientId
         * @return {?}
         */
        (clientId) => {
            return this.checkUserHasAnyClientAppRole(userId, clientId, roleNames);
        })));
    }
    /**
     * Gets details for all users.
     * @return {?} Array of user info objects
     */
    getUsers() {
        /** @type {?} */
        const url = this.buildUserUrl();
        /** @type {?} */
        const httpMethod = 'GET';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = {};
        /** @type {?} */
        const bodyParam = {};
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const authNames = [];
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, authNames, contentTypes, accepts, null, null)).pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            return response;
        })));
    }
    /**
     * Gets a list of roles for a user.
     * @param {?} userId ID of the user
     * @return {?} Array of role info objects
     */
    getUserRoles(userId) {
        /** @type {?} */
        const url = this.buildRolesUrl(userId);
        /** @type {?} */
        const httpMethod = 'GET';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = {};
        /** @type {?} */
        const bodyParam = {};
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, Object, null, null)).pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            return response;
        })));
    }
    /**
     * Gets an array of users (including the current user) who have any of the roles in the supplied list.
     * @param {?} roleNames List of role names to look for
     * @return {?} Array of user info objects
     */
    getUsersByRolesWithCurrentUser(roleNames) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const filteredUsers = [];
            if (roleNames && roleNames.length > 0) {
                /** @type {?} */
                const users = yield this.getUsers().toPromise();
                for (let i = 0; i < users.length; i++) {
                    /** @type {?} */
                    const hasAnyRole = yield this.userHasAnyRole(users[i].id, roleNames);
                    if (hasAnyRole) {
                        filteredUsers.push(users[i]);
                    }
                }
            }
            return filteredUsers;
        });
    }
    /**
     * Gets an array of users (not including the current user) who have any of the roles in the supplied list.
     * @param {?} roleNames List of role names to look for
     * @return {?} Array of user info objects
     */
    getUsersByRolesWithoutCurrentUser(roleNames) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const filteredUsers = [];
            if (roleNames && roleNames.length > 0) {
                /** @type {?} */
                const currentUser = this.getCurrentUserInfo();
                /** @type {?} */
                let users = yield this.getUsers().toPromise();
                users = users.filter((/**
                 * @param {?} user
                 * @return {?}
                 */
                (user) => { return user.username !== currentUser.username; }));
                for (let i = 0; i < users.length; i++) {
                    /** @type {?} */
                    const hasAnyRole = yield this.userHasAnyRole(users[i].id, roleNames);
                    if (hasAnyRole) {
                        filteredUsers.push(users[i]);
                    }
                }
            }
            return filteredUsers;
        });
    }
    /**
     * @private
     * @param {?} userId
     * @param {?} roleNames
     * @return {?}
     */
    userHasAnyRole(userId, roleNames) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const userRoles = yield this.getUserRoles(userId).toPromise();
            /** @type {?} */
            const hasAnyRole = roleNames.some((/**
             * @param {?} roleName
             * @return {?}
             */
            (roleName) => {
                /** @type {?} */
                const filteredRoles = userRoles.filter((/**
                 * @param {?} userRole
                 * @return {?}
                 */
                (userRole) => {
                    return userRole.name.toLocaleLowerCase() === roleName.toLocaleLowerCase();
                }));
                return filteredRoles.length > 0;
            }));
            return hasAnyRole;
        });
    }
    /**
     * Checks if a user has one of the roles from a list.
     * @param {?} userId ID of the target user
     * @param {?} roleNames Array of roles to check for
     * @return {?} True if the user has one of the roles, false otherwise
     */
    checkUserHasRole(userId, roleNames) {
        return this.getUserRoles(userId).pipe(map((/**
         * @param {?} userRoles
         * @return {?}
         */
        (userRoles) => {
            /** @type {?} */
            let hasRole = false;
            if (userRoles && userRoles.length > 0) {
                roleNames.forEach((/**
                 * @param {?} roleName
                 * @return {?}
                 */
                (roleName) => {
                    /** @type {?} */
                    const role = userRoles.find((/**
                     * @param {?} userRole
                     * @return {?}
                     */
                    (userRole) => {
                        return roleName === userRole.name;
                    }));
                    if (role) {
                        hasRole = true;
                        return;
                    }
                }));
            }
            return hasRole;
        })));
    }
    /**
     * Gets details for all users.
     * @param {?} requestQuery
     * @return {?} Array of user information objects.
     */
    queryUsers(requestQuery) {
        /** @type {?} */
        const url = this.buildUserUrl();
        /** @type {?} */
        const httpMethod = 'GET';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = { first: requestQuery.first, max: requestQuery.max };
        /** @type {?} */
        const bodyParam = {};
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const authNames = [];
        /** @type {?} */
        const contentTypes = ['application/json'];
        return this.getTotalUsersCount().pipe(switchMap((/**
         * @param {?} totalCount
         * @return {?}
         */
        (totalCount) => from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, authNames, contentTypes, null, null, null)).pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        (response) => {
            return (/** @type {?} */ ({
                entries: response,
                pagination: {
                    skipCount: requestQuery.first,
                    maxItems: requestQuery.max,
                    count: totalCount,
                    hasMoreItems: false,
                    totalItems: totalCount
                }
            }));
        })), catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => this.handleError(error)))))));
    }
    /**
     * Gets users total count.
     * @return {?} Number of users count.
     */
    getTotalUsersCount() {
        /** @type {?} */
        const url = this.buildUserUrl() + `/count`;
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance()
            .oauth2Auth.callCustomApi(url, 'GET', null, null, null, null, null, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => this.handleError(error))));
    }
    /**
     * Creates new user.
     * @param {?} newUser Object containing the new user details.
     * @return {?} Empty response when the user created.
     */
    createUser(newUser) {
        /** @type {?} */
        const url = this.buildUserUrl();
        /** @type {?} */
        const request = JSON.stringify(newUser);
        /** @type {?} */
        const httpMethod = 'POST';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = {};
        /** @type {?} */
        const bodyParam = request;
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => this.handleError(error))));
    }
    /**
     * Updates user details.
     * @param {?} userId Id of the user.
     * @param {?} updatedUser Object containing the user details.
     * @return {?} Empty response when the user updated.
     */
    updateUser(userId, updatedUser) {
        /** @type {?} */
        const url = this.buildUserUrl() + '/' + userId;
        /** @type {?} */
        const request = JSON.stringify(updatedUser);
        /** @type {?} */
        const httpMethod = 'PUT';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = {};
        /** @type {?} */
        const bodyParam = request;
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => this.handleError(error))));
    }
    /**
     * Deletes User.
     * @param {?} userId Id of the  user.
     * @return {?} Empty response when the user deleted.
     */
    deleteUser(userId) {
        /** @type {?} */
        const url = this.buildUserUrl() + '/' + userId;
        /** @type {?} */
        const httpMethod = 'DELETE';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = {};
        /** @type {?} */
        const bodyParam = {};
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => this.handleError(error))));
    }
    /**
     * Changes user password.
     * @param {?} userId Id of the user.
     * @param {?} newPassword
     * @return {?} Empty response when the password changed.
     */
    changePassword(userId, newPassword) {
        /** @type {?} */
        const url = this.buildUserUrl() + '/' + userId + '/reset-password';
        /** @type {?} */
        const request = JSON.stringify(newPassword);
        /** @type {?} */
        const httpMethod = 'PUT';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = {};
        /** @type {?} */
        const bodyParam = request;
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => this.handleError(error))));
    }
    /**
     * Gets involved groups.
     * @param {?} userId Id of the user.
     * @return {?} Array of involved groups information objects.
     */
    getInvolvedGroups(userId) {
        /** @type {?} */
        const url = this.buildUserUrl() + '/' + userId + '/groups/';
        /** @type {?} */
        const httpMethod = 'GET';
        /** @type {?} */
        const pathParams = { id: userId };
        /** @type {?} */
        const queryParams = {};
        /** @type {?} */
        const bodyParam = {};
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const authNames = [];
        /** @type {?} */
        const contentTypes = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, authNames, contentTypes, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => this.handleError(error))));
    }
    /**
     * Joins group.
     * @param {?} joinGroupRequest Details of join group request (IdentityJoinGroupRequestModel).
     * @return {?} Empty response when the user joined the group.
     */
    joinGroup(joinGroupRequest) {
        /** @type {?} */
        const url = this.buildUserUrl() + '/' + joinGroupRequest.userId + '/groups/' + joinGroupRequest.groupId;
        /** @type {?} */
        const request = JSON.stringify(joinGroupRequest);
        /** @type {?} */
        const httpMethod = 'PUT';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = {};
        /** @type {?} */
        const bodyParam = request;
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => this.handleError(error))));
    }
    /**
     * Leaves group.
     * @param {?} userId Id of the user.
     * @param {?} groupId Id of the  group.
     * @return {?} Empty response when the user left the group.
     */
    leaveGroup(userId, groupId) {
        /** @type {?} */
        const url = this.buildUserUrl() + '/' + userId + '/groups/' + groupId;
        /** @type {?} */
        const httpMethod = 'DELETE';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = {};
        /** @type {?} */
        const bodyParam = {};
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => this.handleError(error))));
    }
    /**
     * Gets available roles
     * @param {?} userId Id of the user.
     * @return {?} Array of available roles information objects
     */
    getAvailableRoles(userId) {
        /** @type {?} */
        const url = this.buildUserUrl() + '/' + userId + '/role-mappings/realm/available';
        /** @type {?} */
        const httpMethod = 'GET';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = {};
        /** @type {?} */
        const bodyParam = {};
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const authNames = [];
        /** @type {?} */
        const contentTypes = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, authNames, contentTypes, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => this.handleError(error))));
    }
    /**
     * Gets assigned roles.
     * @param {?} userId Id of the user.
     * @return {?} Array of assigned roles information objects
     */
    getAssignedRoles(userId) {
        /** @type {?} */
        const url = this.buildUserUrl() + '/' + userId + '/role-mappings/realm';
        /** @type {?} */
        const httpMethod = 'GET';
        /** @type {?} */
        const pathParams = { id: userId };
        /** @type {?} */
        const queryParams = {};
        /** @type {?} */
        const bodyParam = {};
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const authNames = [];
        /** @type {?} */
        const contentTypes = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, authNames, contentTypes, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => this.handleError(error))));
    }
    /**
     * Gets effective roles.
     * @param {?} userId Id of the user.
     * @return {?} Array of composite roles information objects
     */
    getEffectiveRoles(userId) {
        /** @type {?} */
        const url = this.buildUserUrl() + '/' + userId + '/role-mappings/realm/composite';
        /** @type {?} */
        const httpMethod = 'GET';
        /** @type {?} */
        const pathParams = { id: userId };
        /** @type {?} */
        const queryParams = {};
        /** @type {?} */
        const bodyParam = {};
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const authNames = [];
        /** @type {?} */
        const contentTypes = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, authNames, contentTypes, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => this.handleError(error))));
    }
    /**
     * Assigns roles to the user.
     * @param {?} userId Id of the user.
     * @param {?} roles Array of roles.
     * @return {?} Empty response when the role assigned.
     */
    assignRoles(userId, roles) {
        /** @type {?} */
        const url = this.buildUserUrl() + '/' + userId + '/role-mappings/realm';
        /** @type {?} */
        const request = JSON.stringify(roles);
        /** @type {?} */
        const httpMethod = 'POST';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = {};
        /** @type {?} */
        const bodyParam = request;
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => this.handleError(error))));
    }
    /**
     * Removes assigned roles.
     * @param {?} userId Id of the user.
     * @param {?} removedRoles
     * @return {?} Empty response when the role removed.
     */
    removeRoles(userId, removedRoles) {
        /** @type {?} */
        const url = this.buildUserUrl() + '/' + userId + '/role-mappings/realm';
        /** @type {?} */
        const request = JSON.stringify(removedRoles);
        /** @type {?} */
        const httpMethod = 'DELETE';
        /** @type {?} */
        const pathParams = {};
        /** @type {?} */
        const queryParams = {};
        /** @type {?} */
        const bodyParam = request;
        /** @type {?} */
        const headerParams = {};
        /** @type {?} */
        const formParams = {};
        /** @type {?} */
        const contentTypes = ['application/json'];
        /** @type {?} */
        const accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        (error) => this.handleError(error))));
    }
    /**
     * @private
     * @return {?}
     */
    buildUserUrl() {
        return `${this.appConfigService.get('identityHost')}/users`;
    }
    /**
     * @private
     * @param {?} userId
     * @param {?} clientId
     * @return {?}
     */
    buildUserClientRoleMapping(userId, clientId) {
        return `${this.appConfigService.get('identityHost')}/users/${userId}/role-mappings/clients/${clientId}`;
    }
    /**
     * @private
     * @param {?} userId
     * @return {?}
     */
    buildRolesUrl(userId) {
        return `${this.appConfigService.get('identityHost')}/users/${userId}/role-mappings/realm/composite`;
    }
    /**
     * @private
     * @return {?}
     */
    buildGetClientsUrl() {
        return `${this.appConfigService.get('identityHost')}/clients`;
    }
    /**
     * Throw the error
     * @private
     * @param {?} error
     * @return {?}
     */
    handleError(error) {
        this.logService.error(error);
        return throwError(error || 'Server error');
    }
}
IdentityUserService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
IdentityUserService.ctorParameters = () => [
    { type: JwtHelperService },
    { type: AlfrescoApiService },
    { type: AppConfigService },
    { type: LogService }
];
/** @nocollapse */ IdentityUserService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function IdentityUserService_Factory() { return new IdentityUserService(i0.ɵɵinject(i1.JwtHelperService), i0.ɵɵinject(i2.AlfrescoApiService), i0.ɵɵinject(i3.AppConfigService), i0.ɵɵinject(i4.LogService)); }, token: IdentityUserService, providedIn: "root" });
if (false) {
    /**
     * @type {?}
     * @private
     */
    IdentityUserService.prototype.jwtHelperService;
    /**
     * @type {?}
     * @private
     */
    IdentityUserService.prototype.alfrescoApiService;
    /**
     * @type {?}
     * @private
     */
    IdentityUserService.prototype.appConfigService;
    /**
     * @type {?}
     * @private
     */
    IdentityUserService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWRlbnRpdHktdXNlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsidXNlcmluZm8vc2VydmljZXMvaWRlbnRpdHktdXNlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBYyxFQUFFLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN4RCxPQUFPLEVBQUUsVUFBVSxFQUFFLEdBQUcsRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUU1RCxPQUFPLEVBQ0gsaUJBQWlCLEVBS3BCLE1BQU0sK0JBQStCLENBQUM7QUFDdkMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDckUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDOzs7Ozs7QUFPekUsTUFBTSxPQUFPLG1CQUFtQjs7Ozs7OztJQUU1QixZQUNZLGdCQUFrQyxFQUNsQyxrQkFBc0MsRUFDdEMsZ0JBQWtDLEVBQ2xDLFVBQXNCO1FBSHRCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QyxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLGVBQVUsR0FBVixVQUFVLENBQVk7SUFBSSxDQUFDOzs7OztJQU12QyxrQkFBa0I7O2NBQ1IsVUFBVSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyw0QkFBNEIsQ0FBUyxnQkFBZ0IsQ0FBQyxXQUFXLENBQUM7O2NBQ3JHLFNBQVMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsNEJBQTRCLENBQVMsZ0JBQWdCLENBQUMsVUFBVSxDQUFDOztjQUNuRyxLQUFLLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLDRCQUE0QixDQUFTLGdCQUFnQixDQUFDLFVBQVUsQ0FBQzs7Y0FDL0YsUUFBUSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyw0QkFBNEIsQ0FBUyxnQkFBZ0IsQ0FBQyx1QkFBdUIsQ0FBQzs7Y0FDL0csSUFBSSxHQUFHLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxRQUFRLEVBQUUsVUFBVSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRTtRQUM3RixPQUFPLElBQUksaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDdkMsQ0FBQzs7Ozs7O0lBT0QsZUFBZSxDQUFDLE1BQWM7UUFDMUIsSUFBSSxNQUFNLEtBQUssRUFBRSxFQUFFO1lBQ2YsT0FBTyxFQUFFLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDakI7O2NBQ0ssR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUU7O2NBQ3pCLFVBQVUsR0FBRyxLQUFLOztjQUFFLFVBQVUsR0FBRyxFQUFFOztjQUFFLFdBQVcsR0FBRyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUU7O2NBQUUsU0FBUyxHQUFHLEVBQUU7O2NBQUUsWUFBWSxHQUFHLEVBQUU7O2NBQzFHLFVBQVUsR0FBRyxFQUFFOztjQUFFLFlBQVksR0FBRyxDQUFDLGtCQUFrQixDQUFDOztjQUFFLE9BQU8sR0FBRyxDQUFDLGtCQUFrQixDQUFDO1FBRXhGLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQ3ZFLEdBQUcsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFDeEMsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQ25DLFlBQVksRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FDN0MsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7O0lBT0Qsa0JBQWtCLENBQUMsUUFBZ0I7UUFDL0IsSUFBSSxRQUFRLEtBQUssRUFBRSxFQUFFO1lBQ2pCLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ2pCOztjQUNLLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFOztjQUN6QixVQUFVLEdBQUcsS0FBSzs7Y0FBRSxVQUFVLEdBQUcsRUFBRTs7Y0FBRSxXQUFXLEdBQUcsRUFBRSxRQUFRLEVBQUUsUUFBUSxFQUFFOztjQUFFLFNBQVMsR0FBRyxFQUFFOztjQUFFLFlBQVksR0FBRyxFQUFFOztjQUM5RyxVQUFVLEdBQUcsRUFBRTs7Y0FBRSxZQUFZLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQzs7Y0FBRSxPQUFPLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztRQUV4RixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUN2RSxHQUFHLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQ3hDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUNuQyxZQUFZLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQzdDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQU9ELGVBQWUsQ0FBQyxLQUFhO1FBQ3pCLElBQUksS0FBSyxLQUFLLEVBQUUsRUFBRTtZQUNkLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ2pCOztjQUNLLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFOztjQUN6QixVQUFVLEdBQUcsS0FBSzs7Y0FBRSxVQUFVLEdBQUcsRUFBRTs7Y0FBRSxXQUFXLEdBQUcsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFOztjQUFFLFNBQVMsR0FBRyxFQUFFOztjQUFFLFlBQVksR0FBRyxFQUFFOztjQUN4RyxVQUFVLEdBQUcsRUFBRTs7Y0FBRSxZQUFZLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQzs7Y0FBRSxPQUFPLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztRQUV4RixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUN2RSxHQUFHLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQ3hDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUNuQyxZQUFZLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQzdDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQU9ELFlBQVksQ0FBQyxFQUFVO1FBQ25CLElBQUksRUFBRSxLQUFLLEVBQUUsRUFBRTtZQUNYLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ2pCOztjQUNLLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLEdBQUcsR0FBRyxHQUFHLEVBQUU7O2NBQ3BDLFVBQVUsR0FBRyxLQUFLOztjQUFFLFVBQVUsR0FBRyxFQUFFOztjQUFFLFdBQVcsR0FBRyxFQUFFOztjQUFFLFNBQVMsR0FBRyxFQUFFOztjQUFFLFlBQVksR0FBRyxFQUFFOztjQUMxRixVQUFVLEdBQUcsRUFBRTs7Y0FBRSxZQUFZLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQzs7Y0FBRSxPQUFPLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztRQUV4RixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUN2RSxHQUFHLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQ3hDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUNuQyxZQUFZLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQzdDLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7SUFRRCxjQUFjLENBQUMsTUFBYyxFQUFFLFFBQWdCOztjQUNyQyxHQUFHLEdBQUcsSUFBSSxDQUFDLDBCQUEwQixDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUM7O2NBQ3ZELFVBQVUsR0FBRyxLQUFLOztjQUFFLFVBQVUsR0FBRyxFQUFFOztjQUFFLFdBQVcsR0FBRyxFQUFFOztjQUFFLFNBQVMsR0FBRyxFQUFFOztjQUFFLFlBQVksR0FBRyxFQUFFOztjQUMxRixVQUFVLEdBQUcsRUFBRTs7Y0FBRSxZQUFZLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQzs7Y0FBRSxPQUFPLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztRQUV4RixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FDdEUsR0FBRyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUN4QyxZQUFZLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFDbkMsWUFBWSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUM3QyxDQUFDO0lBQ04sQ0FBQzs7Ozs7OztJQVFELHFCQUFxQixDQUFDLE1BQWMsRUFBRSxRQUFnQjtRQUNsRCxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FDN0MsR0FBRzs7OztRQUFDLENBQUMsV0FBa0IsRUFBRSxFQUFFO1lBQ3ZCLElBQUksV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3hCLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7WUFDRCxPQUFPLEtBQUssQ0FBQztRQUNqQixDQUFDLEVBQUMsQ0FDTCxDQUFDO0lBQ04sQ0FBQzs7Ozs7Ozs7SUFTRCw0QkFBNEIsQ0FBQyxNQUFjLEVBQUUsUUFBZ0IsRUFBRSxTQUFtQjtRQUM5RSxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FDN0MsR0FBRzs7OztRQUFDLENBQUMsV0FBa0IsRUFBRSxFQUFFOztnQkFDbkIsT0FBTyxHQUFHLEtBQUs7WUFDbkIsSUFBSSxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDeEIsU0FBUyxDQUFDLE9BQU87Ozs7Z0JBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTs7MEJBQ3JCLElBQUksR0FBRyxXQUFXLENBQUMsSUFBSTs7OztvQkFBQyxDQUFDLGFBQWEsRUFBRSxFQUFFO3dCQUM1QyxPQUFPLGFBQWEsQ0FBQyxJQUFJLEtBQUssUUFBUSxDQUFDO29CQUMzQyxDQUFDLEVBQUM7b0JBRUYsSUFBSSxJQUFJLEVBQUU7d0JBQ04sT0FBTyxHQUFHLElBQUksQ0FBQzt3QkFDZixPQUFPO3FCQUNWO2dCQUNMLENBQUMsRUFBQyxDQUFDO2FBQ047WUFDRCxPQUFPLE9BQU8sQ0FBQztRQUNuQixDQUFDLEVBQUMsQ0FDTCxDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBT0QsNEJBQTRCLENBQUMsZUFBdUI7O2NBQzFDLEdBQUcsR0FBRyxJQUFJLENBQUMsa0JBQWtCLEVBQUU7O2NBQy9CLFVBQVUsR0FBRyxLQUFLOztjQUFFLFVBQVUsR0FBRyxFQUFFOztjQUFFLFdBQVcsR0FBRyxFQUFFLFFBQVEsRUFBRSxlQUFlLEVBQUU7O2NBQUUsU0FBUyxHQUFHLEVBQUU7O2NBQUUsWUFBWSxHQUFHLEVBQUU7O2NBQUUsVUFBVSxHQUFHLEVBQUU7O2NBQ3RJLFlBQVksR0FBRyxDQUFDLGtCQUFrQixDQUFDOztjQUFFLE9BQU8sR0FBRyxDQUFDLGtCQUFrQixDQUFDO1FBQ3ZFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUU7YUFDNUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxHQUFHLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUM1RSxVQUFVLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFDbkMsT0FBTyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQ25DLENBQUMsSUFBSSxDQUNGLEdBQUc7Ozs7UUFBQyxDQUFDLFFBQWUsRUFBRSxFQUFFOztrQkFDZCxRQUFRLEdBQUcsUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQyxFQUFFO1lBQ3RFLE9BQU8sUUFBUSxDQUFDO1FBQ3BCLENBQUMsRUFBQyxDQUNMLENBQUM7SUFDTixDQUFDOzs7Ozs7O0lBUUQsNkJBQTZCLENBQUMsTUFBYyxFQUFFLGVBQXVCO1FBQ2pFLE9BQU8sSUFBSSxDQUFDLDRCQUE0QixDQUFDLGVBQWUsQ0FBQyxDQUFDLElBQUksQ0FDMUQsU0FBUzs7OztRQUFDLENBQUMsUUFBZ0IsRUFBRSxFQUFFO1lBQzNCLE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQztRQUN4RCxDQUFDLEVBQUMsQ0FDTCxDQUFDO0lBQ04sQ0FBQzs7Ozs7Ozs7SUFTRCw4QkFBOEIsQ0FBQyxNQUFjLEVBQUUsZUFBdUIsRUFBRSxTQUFtQjtRQUN2RixPQUFPLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxlQUFlLENBQUMsQ0FBQyxJQUFJLENBQzFELFNBQVM7Ozs7UUFBQyxDQUFDLFFBQWdCLEVBQUUsRUFBRTtZQUMzQixPQUFPLElBQUksQ0FBQyw0QkFBNEIsQ0FBQyxNQUFNLEVBQUUsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQzFFLENBQUMsRUFBQyxDQUNMLENBQUM7SUFDTixDQUFDOzs7OztJQU1ELFFBQVE7O2NBQ0UsR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUU7O2NBQ3pCLFVBQVUsR0FBRyxLQUFLOztjQUFFLFVBQVUsR0FBRyxFQUFFOztjQUFFLFdBQVcsR0FBRyxFQUFFOztjQUFFLFNBQVMsR0FBRyxFQUFFOztjQUFFLFlBQVksR0FBRyxFQUFFOztjQUMxRixVQUFVLEdBQUcsRUFBRTs7Y0FBRSxTQUFTLEdBQUcsRUFBRTs7Y0FBRSxZQUFZLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQzs7Y0FBRSxPQUFPLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztRQUV4RyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FDdEUsR0FBRyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUN4QyxZQUFZLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQzlDLFlBQVksRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUNyQyxDQUFDLElBQUksQ0FDRixHQUFHOzs7O1FBQUMsQ0FBQyxRQUE2QixFQUFFLEVBQUU7WUFDbEMsT0FBTyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxFQUFDLENBQ0wsQ0FBQztJQUNOLENBQUM7Ozs7OztJQU9ELFlBQVksQ0FBQyxNQUFjOztjQUNqQixHQUFHLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7O2NBQ2hDLFVBQVUsR0FBRyxLQUFLOztjQUFFLFVBQVUsR0FBRyxFQUFFOztjQUFFLFdBQVcsR0FBRyxFQUFFOztjQUFFLFNBQVMsR0FBRyxFQUFFOztjQUFFLFlBQVksR0FBRyxFQUFFOztjQUMxRixVQUFVLEdBQUcsRUFBRTs7Y0FBRSxZQUFZLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQzs7Y0FBRSxPQUFPLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztRQUV4RixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FDdEUsR0FBRyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUN4QyxZQUFZLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFDbkMsWUFBWSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUM3QyxDQUFDLElBQUksQ0FDRixHQUFHOzs7O1FBQUMsQ0FBQyxRQUE2QixFQUFFLEVBQUU7WUFDbEMsT0FBTyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxFQUFDLENBQ0wsQ0FBQztJQUNOLENBQUM7Ozs7OztJQU9LLDhCQUE4QixDQUFDLFNBQW1COzs7a0JBQzlDLGFBQWEsR0FBd0IsRUFBRTtZQUM3QyxJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs7c0JBQzdCLEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxTQUFTLEVBQUU7Z0JBRS9DLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFOzswQkFDN0IsVUFBVSxHQUFHLE1BQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLFNBQVMsQ0FBQztvQkFDcEUsSUFBSSxVQUFVLEVBQUU7d0JBQ1osYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztxQkFDaEM7aUJBQ0o7YUFDSjtZQUVELE9BQU8sYUFBYSxDQUFDO1FBQ3pCLENBQUM7S0FBQTs7Ozs7O0lBT0ssaUNBQWlDLENBQUMsU0FBbUI7OztrQkFDakQsYUFBYSxHQUF3QixFQUFFO1lBQzdDLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOztzQkFDN0IsV0FBVyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsRUFBRTs7b0JBQ3pDLEtBQUssR0FBRyxNQUFNLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQyxTQUFTLEVBQUU7Z0JBRTdDLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTTs7OztnQkFBQyxDQUFDLElBQUksRUFBRSxFQUFFLEdBQUcsT0FBTyxJQUFJLENBQUMsUUFBUSxLQUFLLFdBQVcsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLEVBQUMsQ0FBQztnQkFFbkYsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7OzBCQUM3QixVQUFVLEdBQUcsTUFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsU0FBUyxDQUFDO29CQUNwRSxJQUFJLFVBQVUsRUFBRTt3QkFDWixhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUNoQztpQkFDSjthQUNKO1lBRUQsT0FBTyxhQUFhLENBQUM7UUFDekIsQ0FBQztLQUFBOzs7Ozs7O0lBRWEsY0FBYyxDQUFDLE1BQWMsRUFBRSxTQUFtQjs7O2tCQUN0RCxTQUFTLEdBQUcsTUFBTSxJQUFJLENBQUMsWUFBWSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVMsRUFBRTs7a0JBQ3ZELFVBQVUsR0FBRyxTQUFTLENBQUMsSUFBSTs7OztZQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7O3NCQUNyQyxhQUFhLEdBQUcsU0FBUyxDQUFDLE1BQU07Ozs7Z0JBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRTtvQkFDaEQsT0FBTyxRQUFRLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLEtBQUssUUFBUSxDQUFDLGlCQUFpQixFQUFFLENBQUM7Z0JBQzlFLENBQUMsRUFBQztnQkFFRixPQUFPLGFBQWEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO1lBQ3BDLENBQUMsRUFBQztZQUVGLE9BQU8sVUFBVSxDQUFDO1FBQ3RCLENBQUM7S0FBQTs7Ozs7OztJQVFELGdCQUFnQixDQUFDLE1BQWMsRUFBRSxTQUFtQjtRQUNoRCxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLEdBQUc7Ozs7UUFBQyxDQUFDLFNBQThCLEVBQUUsRUFBRTs7Z0JBQ3JFLE9BQU8sR0FBRyxLQUFLO1lBQ25CLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUNuQyxTQUFTLENBQUMsT0FBTzs7OztnQkFBQyxDQUFDLFFBQWdCLEVBQUUsRUFBRTs7MEJBQzdCLElBQUksR0FBRyxTQUFTLENBQUMsSUFBSTs7OztvQkFBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO3dCQUNyQyxPQUFPLFFBQVEsS0FBSyxRQUFRLENBQUMsSUFBSSxDQUFDO29CQUN0QyxDQUFDLEVBQUM7b0JBQ0YsSUFBSSxJQUFJLEVBQUU7d0JBQ04sT0FBTyxHQUFHLElBQUksQ0FBQzt3QkFDZixPQUFPO3FCQUNWO2dCQUNMLENBQUMsRUFBQyxDQUFDO2FBQ047WUFDRCxPQUFPLE9BQU8sQ0FBQztRQUNuQixDQUFDLEVBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7O0lBTUQsVUFBVSxDQUFDLFlBQWdEOztjQUNqRCxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRTs7Y0FDekIsVUFBVSxHQUFHLEtBQUs7O2NBQUUsVUFBVSxHQUFHLEVBQUU7O2NBQ3pDLFdBQVcsR0FBRyxFQUFFLEtBQUssRUFBRSxZQUFZLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxZQUFZLENBQUMsR0FBRyxFQUFFOztjQUFFLFNBQVMsR0FBRyxFQUFFOztjQUFFLFlBQVksR0FBRyxFQUFFOztjQUNyRyxVQUFVLEdBQUcsRUFBRTs7Y0FBRSxTQUFTLEdBQUcsRUFBRTs7Y0FBRSxZQUFZLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztRQUVwRSxPQUFPLElBQUksQ0FBQyxrQkFBa0IsRUFBRSxDQUFDLElBQUksQ0FDN0IsU0FBUzs7OztRQUFDLENBQUMsVUFBZSxFQUFFLEVBQUUsQ0FDOUIsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUMvRCxHQUFHLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQ3hDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFDOUMsWUFBWSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQ2xDLENBQUMsSUFBSSxDQUNGLEdBQUc7Ozs7UUFBQyxDQUFDLFFBQTZCLEVBQUUsRUFBRTtZQUNsQyxPQUFPLG1CQUE0QjtnQkFDL0IsT0FBTyxFQUFFLFFBQVE7Z0JBQ2pCLFVBQVUsRUFBRTtvQkFDVixTQUFTLEVBQUUsWUFBWSxDQUFDLEtBQUs7b0JBQzdCLFFBQVEsRUFBRSxZQUFZLENBQUMsR0FBRztvQkFDMUIsS0FBSyxFQUFFLFVBQVU7b0JBQ2pCLFlBQVksRUFBRSxLQUFLO29CQUNuQixVQUFVLEVBQUUsVUFBVTtpQkFDdkI7YUFDRixFQUFBLENBQUM7UUFDUixDQUFDLEVBQUMsRUFDRixVQUFVOzs7O1FBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQUMsQ0FDN0MsRUFBQyxDQUNiLENBQUM7SUFDTixDQUFDOzs7OztJQU1ELGtCQUFrQjs7Y0FDUixHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxHQUFHLFFBQVE7O2NBQ3BDLFlBQVksR0FBRyxDQUFDLGtCQUFrQixDQUFDOztjQUFFLE9BQU8sR0FBRyxDQUFDLGtCQUFrQixDQUFDO1FBQ3pFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUU7YUFDNUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUNsQyxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFDaEIsSUFBSSxFQUFFLElBQUksRUFBRSxZQUFZLEVBQ3hCLE9BQU8sRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FDeEIsQ0FBQyxDQUFDLElBQUksQ0FDTCxVQUFVOzs7O1FBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQUMsQ0FDakQsQ0FBQztJQUNWLENBQUM7Ozs7OztJQU9ELFVBQVUsQ0FBQyxPQUEwQjs7Y0FDM0IsR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUU7O2NBQ3pCLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQzs7Y0FDakMsVUFBVSxHQUFHLE1BQU07O2NBQUUsVUFBVSxHQUFHLEVBQUU7O2NBQUUsV0FBVyxHQUFHLEVBQUU7O2NBQUUsU0FBUyxHQUFHLE9BQU87O2NBQUUsWUFBWSxHQUFHLEVBQUU7O2NBQ3BHLFVBQVUsR0FBRyxFQUFFOztjQUFFLFlBQVksR0FBRyxDQUFDLGtCQUFrQixDQUFDOztjQUFFLE9BQU8sR0FBRyxDQUFDLGtCQUFrQixDQUFDO1FBRXBGLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUMxRSxHQUFHLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQ3hDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUNuQyxZQUFZLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUN0QyxDQUFDLENBQUMsSUFBSSxDQUNILFVBQVU7Ozs7UUFBQyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBQyxDQUNqRCxDQUFDO0lBQ04sQ0FBQzs7Ozs7OztJQVFELFVBQVUsQ0FBQyxNQUFjLEVBQUUsV0FBOEI7O2NBQy9DLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLEdBQUcsR0FBRyxHQUFHLE1BQU07O2NBQ3hDLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQzs7Y0FDckMsVUFBVSxHQUFHLEtBQUs7O2NBQUUsVUFBVSxHQUFHLEVBQUU7O2NBQUcsV0FBVyxHQUFHLEVBQUU7O2NBQUUsU0FBUyxHQUFHLE9BQU87O2NBQUUsWUFBWSxHQUFHLEVBQUU7O2NBQ3BHLFVBQVUsR0FBRyxFQUFFOztjQUFFLFlBQVksR0FBRyxDQUFDLGtCQUFrQixDQUFDOztjQUFFLE9BQU8sR0FBRyxDQUFDLGtCQUFrQixDQUFDO1FBRXBGLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUMxRSxHQUFHLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQ3hDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUNuQyxZQUFZLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUN0QyxDQUFDLENBQUMsSUFBSSxDQUNILFVBQVU7Ozs7UUFBQyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBQyxDQUNqRCxDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBT0QsVUFBVSxDQUFDLE1BQWM7O2NBQ2YsR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUUsR0FBRyxHQUFHLEdBQUcsTUFBTTs7Y0FDeEMsVUFBVSxHQUFHLFFBQVE7O2NBQUUsVUFBVSxHQUFHLEVBQUU7O2NBQUcsV0FBVyxHQUFHLEVBQUU7O2NBQUUsU0FBUyxHQUFHLEVBQUU7O2NBQUUsWUFBWSxHQUFHLEVBQUU7O2NBQ2xHLFVBQVUsR0FBRyxFQUFFOztjQUFFLFlBQVksR0FBRyxDQUFDLGtCQUFrQixDQUFDOztjQUFFLE9BQU8sR0FBRyxDQUFDLGtCQUFrQixDQUFDO1FBRXBGLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUMxRSxHQUFHLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQ3hDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUNuQyxZQUFZLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUN0QyxDQUFDLENBQUMsSUFBSSxDQUNILFVBQVU7Ozs7UUFBQyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBQyxDQUNqRCxDQUFDO0lBQ04sQ0FBQzs7Ozs7OztJQVFELGNBQWMsQ0FBQyxNQUFjLEVBQUUsV0FBc0M7O2NBQzNELEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLEdBQUcsR0FBRyxHQUFHLE1BQU0sR0FBRyxpQkFBaUI7O2NBQzVELE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQzs7Y0FDckMsVUFBVSxHQUFHLEtBQUs7O2NBQUUsVUFBVSxHQUFHLEVBQUU7O2NBQUcsV0FBVyxHQUFHLEVBQUU7O2NBQUUsU0FBUyxHQUFHLE9BQU87O2NBQUUsWUFBWSxHQUFHLEVBQUU7O2NBQ3BHLFVBQVUsR0FBRyxFQUFFOztjQUFFLFlBQVksR0FBRyxDQUFDLGtCQUFrQixDQUFDOztjQUFFLE9BQU8sR0FBRyxDQUFDLGtCQUFrQixDQUFDO1FBRXBGLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUMxRSxHQUFHLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQ3hDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUNuQyxZQUFZLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUN0QyxDQUFDLENBQUMsSUFBSSxDQUNILFVBQVU7Ozs7UUFBQyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBQyxDQUNqRCxDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBT0QsaUJBQWlCLENBQUMsTUFBYzs7Y0FDdEIsR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUUsR0FBRyxHQUFHLEdBQUcsTUFBTSxHQUFHLFVBQVU7O2NBQ3JELFVBQVUsR0FBRyxLQUFLOztjQUFFLFVBQVUsR0FBRyxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUM7O2NBQ3BELFdBQVcsR0FBRyxFQUFFOztjQUFFLFNBQVMsR0FBRyxFQUFFOztjQUFFLFlBQVksR0FBRyxFQUFFOztjQUNuRCxVQUFVLEdBQUcsRUFBRTs7Y0FBRSxTQUFTLEdBQUcsRUFBRTs7Y0FBRSxZQUFZLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztRQUVwRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FDOUQsR0FBRyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUN4QyxZQUFZLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQzlDLFlBQVksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FDN0IsQ0FBQyxDQUFDLElBQUksQ0FDSCxVQUFVOzs7O1FBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQUMsQ0FDakQsQ0FBQztJQUNsQixDQUFDOzs7Ozs7SUFPRCxTQUFTLENBQUMsZ0JBQStDOztjQUMvQyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxHQUFHLEdBQUcsR0FBRyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsVUFBVSxHQUFHLGdCQUFnQixDQUFDLE9BQU87O2NBQ2pHLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDOztjQUMxQyxVQUFVLEdBQUcsS0FBSzs7Y0FBRSxVQUFVLEdBQUcsRUFBRTs7Y0FBRyxXQUFXLEdBQUcsRUFBRTs7Y0FBRSxTQUFTLEdBQUcsT0FBTzs7Y0FBRSxZQUFZLEdBQUcsRUFBRTs7Y0FDcEcsVUFBVSxHQUFHLEVBQUU7O2NBQUUsWUFBWSxHQUFHLENBQUMsa0JBQWtCLENBQUM7O2NBQUUsT0FBTyxHQUFHLENBQUMsa0JBQWtCLENBQUM7UUFFcEYsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQzFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFDeEMsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQ25DLFlBQVksRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQ3RDLENBQUMsQ0FBQyxJQUFJLENBQ0gsVUFBVTs7OztRQUFDLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQ2pELENBQUM7SUFDTixDQUFDOzs7Ozs7O0lBUUQsVUFBVSxDQUFDLE1BQVcsRUFBRSxPQUFlOztjQUM3QixHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxHQUFHLEdBQUcsR0FBRyxNQUFNLEdBQUcsVUFBVSxHQUFHLE9BQU87O2NBQy9ELFVBQVUsR0FBRyxRQUFROztjQUFFLFVBQVUsR0FBRyxFQUFFOztjQUFHLFdBQVcsR0FBRyxFQUFFOztjQUFFLFNBQVMsR0FBRyxFQUFFOztjQUFFLFlBQVksR0FBRyxFQUFFOztjQUNsRyxVQUFVLEdBQUcsRUFBRTs7Y0FBRSxZQUFZLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQzs7Y0FBRSxPQUFPLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztRQUVwRixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FDMUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUN4QyxZQUFZLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFDbkMsWUFBWSxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FDdEMsQ0FBQyxDQUFDLElBQUksQ0FDSCxVQUFVOzs7O1FBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQUMsQ0FDakQsQ0FBQztJQUNOLENBQUM7Ozs7OztJQU9ELGlCQUFpQixDQUFDLE1BQWM7O2NBQ3RCLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLEdBQUcsR0FBRyxHQUFHLE1BQU0sR0FBRyxnQ0FBZ0M7O2NBQzNFLFVBQVUsR0FBRyxLQUFLOztjQUFFLFVBQVUsR0FBRyxFQUFFOztjQUN6QyxXQUFXLEdBQUcsRUFBRTs7Y0FBRSxTQUFTLEdBQUcsRUFBRTs7Y0FBRSxZQUFZLEdBQUcsRUFBRTs7Y0FDbkQsVUFBVSxHQUFHLEVBQUU7O2NBQUUsU0FBUyxHQUFHLEVBQUU7O2NBQUUsWUFBWSxHQUFHLENBQUMsa0JBQWtCLENBQUM7UUFFcEUsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQzlELEdBQUcsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFDeEMsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUM5QyxZQUFZLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQzdCLENBQUMsQ0FBQyxJQUFJLENBQ0gsVUFBVTs7OztRQUFDLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQ2pELENBQUM7SUFDbEIsQ0FBQzs7Ozs7O0lBT0QsZ0JBQWdCLENBQUMsTUFBYzs7Y0FDckIsR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUUsR0FBRyxHQUFHLEdBQUcsTUFBTSxHQUFHLHNCQUFzQjs7Y0FDakUsVUFBVSxHQUFHLEtBQUs7O2NBQUUsVUFBVSxHQUFHLEVBQUUsRUFBRSxFQUFFLE1BQU0sRUFBQzs7Y0FDcEQsV0FBVyxHQUFHLEVBQUU7O2NBQUUsU0FBUyxHQUFHLEVBQUU7O2NBQUUsWUFBWSxHQUFHLEVBQUU7O2NBQ25ELFVBQVUsR0FBRyxFQUFFOztjQUFFLFNBQVMsR0FBRyxFQUFFOztjQUFFLFlBQVksR0FBRyxDQUFDLGtCQUFrQixDQUFDO1FBRXBFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUM5RCxHQUFHLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQ3hDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFDOUMsWUFBWSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUM3QixDQUFDLENBQUMsSUFBSSxDQUNILFVBQVU7Ozs7UUFBQyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBQyxDQUNqRCxDQUFDO0lBQ2xCLENBQUM7Ozs7OztJQU9ELGlCQUFpQixDQUFDLE1BQWM7O2NBQ3RCLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLEdBQUcsR0FBRyxHQUFHLE1BQU0sR0FBRyxnQ0FBZ0M7O2NBQzNFLFVBQVUsR0FBRyxLQUFLOztjQUFFLFVBQVUsR0FBRyxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUM7O2NBQ3BELFdBQVcsR0FBRyxFQUFFOztjQUFFLFNBQVMsR0FBRyxFQUFFOztjQUFFLFlBQVksR0FBRyxFQUFFOztjQUNuRCxVQUFVLEdBQUcsRUFBRTs7Y0FBRSxTQUFTLEdBQUcsRUFBRTs7Y0FBRSxZQUFZLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztRQUVwRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FDOUQsR0FBRyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUN4QyxZQUFZLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQzlDLFlBQVksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FDN0IsQ0FBQyxDQUFDLElBQUksQ0FDSCxVQUFVOzs7O1FBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQUMsQ0FDakQsQ0FBQztJQUNsQixDQUFDOzs7Ozs7O0lBUUQsV0FBVyxDQUFDLE1BQWMsRUFBRSxLQUEwQjs7Y0FDNUMsR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUUsR0FBRyxHQUFHLEdBQUcsTUFBTSxHQUFHLHNCQUFzQjs7Y0FDakUsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDOztjQUMvQixVQUFVLEdBQUcsTUFBTTs7Y0FBRSxVQUFVLEdBQUcsRUFBRTs7Y0FBRyxXQUFXLEdBQUcsRUFBRTs7Y0FBRSxTQUFTLEdBQUcsT0FBTzs7Y0FBRSxZQUFZLEdBQUcsRUFBRTs7Y0FDckcsVUFBVSxHQUFHLEVBQUU7O2NBQUUsWUFBWSxHQUFHLENBQUMsa0JBQWtCLENBQUM7O2NBQUUsT0FBTyxHQUFHLENBQUMsa0JBQWtCLENBQUM7UUFFcEYsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQzFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFDeEMsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQ25DLFlBQVksRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQ3RDLENBQUMsQ0FBQyxJQUFJLENBQ0gsVUFBVTs7OztRQUFDLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQ2pELENBQUM7SUFDTixDQUFDOzs7Ozs7O0lBUUQsV0FBVyxDQUFDLE1BQWMsRUFBRSxZQUFpQzs7Y0FDbkQsR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUUsR0FBRyxHQUFHLEdBQUcsTUFBTSxHQUFHLHNCQUFzQjs7Y0FDakUsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDOztjQUN0QyxVQUFVLEdBQUcsUUFBUTs7Y0FBRSxVQUFVLEdBQUcsRUFBRTs7Y0FBRyxXQUFXLEdBQUcsRUFBRTs7Y0FBRSxTQUFTLEdBQUcsT0FBTzs7Y0FBRSxZQUFZLEdBQUcsRUFBRTs7Y0FDdkcsVUFBVSxHQUFHLEVBQUU7O2NBQUUsWUFBWSxHQUFHLENBQUMsa0JBQWtCLENBQUM7O2NBQUUsT0FBTyxHQUFHLENBQUMsa0JBQWtCLENBQUM7UUFFcEYsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQzFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFDeEMsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQ25DLFlBQVksRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQ3RDLENBQUMsQ0FBQyxJQUFJLENBQ0gsVUFBVTs7OztRQUFDLENBQUMsS0FBSyxFQUFFLEVBQUUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUFDLENBQ2pELENBQUM7SUFDTixDQUFDOzs7OztJQUVPLFlBQVk7UUFDaEIsT0FBTyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQztJQUNoRSxDQUFDOzs7Ozs7O0lBRU8sMEJBQTBCLENBQUMsTUFBYyxFQUFFLFFBQWdCO1FBQy9ELE9BQU8sR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxVQUFVLE1BQU0sMEJBQTBCLFFBQVEsRUFBRSxDQUFDO0lBQzVHLENBQUM7Ozs7OztJQUVPLGFBQWEsQ0FBQyxNQUFjO1FBQ2hDLE9BQU8sR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxVQUFVLE1BQU0sZ0NBQWdDLENBQUM7SUFDeEcsQ0FBQzs7Ozs7SUFFTyxrQkFBa0I7UUFDdEIsT0FBTyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQztJQUNsRSxDQUFDOzs7Ozs7O0lBTU8sV0FBVyxDQUFDLEtBQWU7UUFDL0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsT0FBTyxVQUFVLENBQUMsS0FBSyxJQUFJLGNBQWMsQ0FBQyxDQUFDO0lBQy9DLENBQUM7OztZQWpwQkosVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBVFEsZ0JBQWdCO1lBR2hCLGtCQUFrQjtZQURsQixnQkFBZ0I7WUFEaEIsVUFBVTs7Ozs7Ozs7SUFZWCwrQ0FBMEM7Ozs7O0lBQzFDLGlEQUE4Qzs7Ozs7SUFDOUMsK0NBQTBDOzs7OztJQUMxQyx5Q0FBOEIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiwgZnJvbSwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBjYXRjaEVycm9yLCBtYXAsIHN3aXRjaE1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbmltcG9ydCB7XHJcbiAgICBJZGVudGl0eVVzZXJNb2RlbCxcclxuICAgIElkZW50aXR5VXNlclF1ZXJ5UmVzcG9uc2UsXHJcbiAgICBJZGVudGl0eVVzZXJRdWVyeUNsb3VkUmVxdWVzdE1vZGVsLFxyXG4gICAgSWRlbnRpdHlVc2VyUGFzc3dvcmRNb2RlbCxcclxuICAgIElkZW50aXR5Sm9pbkdyb3VwUmVxdWVzdE1vZGVsXHJcbn0gZnJvbSAnLi4vbW9kZWxzL2lkZW50aXR5LXVzZXIubW9kZWwnO1xyXG5pbXBvcnQgeyBKd3RIZWxwZXJTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvand0LWhlbHBlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXBwQ29uZmlnU2VydmljZSB9IGZyb20gJy4uLy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBJZGVudGl0eVJvbGVNb2RlbCB9IGZyb20gJy4uL21vZGVscy9pZGVudGl0eS1yb2xlLm1vZGVsJztcclxuaW1wb3J0IHsgSWRlbnRpdHlHcm91cE1vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL2lkZW50aXR5LWdyb3VwLm1vZGVsJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgSWRlbnRpdHlVc2VyU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBqd3RIZWxwZXJTZXJ2aWNlOiBKd3RIZWxwZXJTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgYWxmcmVzY29BcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBhcHBDb25maWdTZXJ2aWNlOiBBcHBDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgbG9nU2VydmljZTogTG9nU2VydmljZSkgeyB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBuYW1lIGFuZCBvdGhlciBiYXNpYyBkZXRhaWxzIG9mIHRoZSBjdXJyZW50IHVzZXIuXHJcbiAgICAgKiBAcmV0dXJucyBUaGUgdXNlcidzIGRldGFpbHNcclxuICAgICAqL1xyXG4gICAgZ2V0Q3VycmVudFVzZXJJbmZvKCk6IElkZW50aXR5VXNlck1vZGVsIHtcclxuICAgICAgICBjb25zdCBmYW1pbHlOYW1lID0gdGhpcy5qd3RIZWxwZXJTZXJ2aWNlLmdldFZhbHVlRnJvbUxvY2FsQWNjZXNzVG9rZW48c3RyaW5nPihKd3RIZWxwZXJTZXJ2aWNlLkZBTUlMWV9OQU1FKTtcclxuICAgICAgICBjb25zdCBnaXZlbk5hbWUgPSB0aGlzLmp3dEhlbHBlclNlcnZpY2UuZ2V0VmFsdWVGcm9tTG9jYWxBY2Nlc3NUb2tlbjxzdHJpbmc+KEp3dEhlbHBlclNlcnZpY2UuR0lWRU5fTkFNRSk7XHJcbiAgICAgICAgY29uc3QgZW1haWwgPSB0aGlzLmp3dEhlbHBlclNlcnZpY2UuZ2V0VmFsdWVGcm9tTG9jYWxBY2Nlc3NUb2tlbjxzdHJpbmc+KEp3dEhlbHBlclNlcnZpY2UuVVNFUl9FTUFJTCk7XHJcbiAgICAgICAgY29uc3QgdXNlcm5hbWUgPSB0aGlzLmp3dEhlbHBlclNlcnZpY2UuZ2V0VmFsdWVGcm9tTG9jYWxBY2Nlc3NUb2tlbjxzdHJpbmc+KEp3dEhlbHBlclNlcnZpY2UuVVNFUl9QUkVGRVJSRURfVVNFUk5BTUUpO1xyXG4gICAgICAgIGNvbnN0IHVzZXIgPSB7IGZpcnN0TmFtZTogZ2l2ZW5OYW1lLCBsYXN0TmFtZTogZmFtaWx5TmFtZSwgZW1haWw6IGVtYWlsLCB1c2VybmFtZTogdXNlcm5hbWUgfTtcclxuICAgICAgICByZXR1cm4gbmV3IElkZW50aXR5VXNlck1vZGVsKHVzZXIpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRmluZCB1c2VycyBiYXNlZCBvbiBzZWFyY2ggaW5wdXQuXHJcbiAgICAgKiBAcGFyYW0gc2VhcmNoIFNlYXJjaCBxdWVyeSBzdHJpbmdcclxuICAgICAqIEByZXR1cm5zIExpc3Qgb2YgdXNlcnNcclxuICAgICAqL1xyXG4gICAgZmluZFVzZXJzQnlOYW1lKHNlYXJjaDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBpZiAoc2VhcmNoID09PSAnJykge1xyXG4gICAgICAgICAgICByZXR1cm4gb2YoW10pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlclVybCgpO1xyXG4gICAgICAgIGNvbnN0IGh0dHBNZXRob2QgPSAnR0VUJywgcGF0aFBhcmFtcyA9IHt9LCBxdWVyeVBhcmFtcyA9IHsgc2VhcmNoOiBzZWFyY2ggfSwgYm9keVBhcmFtID0ge30sIGhlYWRlclBhcmFtcyA9IHt9LFxyXG4gICAgICAgICAgICBmb3JtUGFyYW1zID0ge30sIGNvbnRlbnRUeXBlcyA9IFsnYXBwbGljYXRpb24vanNvbiddLCBhY2NlcHRzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiAoZnJvbSh0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLm9hdXRoMkF1dGguY2FsbEN1c3RvbUFwaShcclxuICAgICAgICAgICAgdXJsLCBodHRwTWV0aG9kLCBwYXRoUGFyYW1zLCBxdWVyeVBhcmFtcyxcclxuICAgICAgICAgICAgaGVhZGVyUGFyYW1zLCBmb3JtUGFyYW1zLCBib2R5UGFyYW0sXHJcbiAgICAgICAgICAgIGNvbnRlbnRUeXBlcywgYWNjZXB0cywgT2JqZWN0LCBudWxsLCBudWxsKVxyXG4gICAgICAgICkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRmluZCB1c2VycyBiYXNlZCBvbiB1c2VybmFtZSBpbnB1dC5cclxuICAgICAqIEBwYXJhbSB1c2VybmFtZSBTZWFyY2ggcXVlcnkgc3RyaW5nXHJcbiAgICAgKiBAcmV0dXJucyBMaXN0IG9mIHVzZXJzXHJcbiAgICAgKi9cclxuICAgIGZpbmRVc2VyQnlVc2VybmFtZSh1c2VybmFtZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBpZiAodXNlcm5hbWUgPT09ICcnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBvZihbXSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuYnVpbGRVc2VyVXJsKCk7XHJcbiAgICAgICAgY29uc3QgaHR0cE1ldGhvZCA9ICdHRVQnLCBwYXRoUGFyYW1zID0ge30sIHF1ZXJ5UGFyYW1zID0geyB1c2VybmFtZTogdXNlcm5hbWUgfSwgYm9keVBhcmFtID0ge30sIGhlYWRlclBhcmFtcyA9IHt9LFxyXG4gICAgICAgICAgICBmb3JtUGFyYW1zID0ge30sIGNvbnRlbnRUeXBlcyA9IFsnYXBwbGljYXRpb24vanNvbiddLCBhY2NlcHRzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiAoZnJvbSh0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLm9hdXRoMkF1dGguY2FsbEN1c3RvbUFwaShcclxuICAgICAgICAgICAgdXJsLCBodHRwTWV0aG9kLCBwYXRoUGFyYW1zLCBxdWVyeVBhcmFtcyxcclxuICAgICAgICAgICAgaGVhZGVyUGFyYW1zLCBmb3JtUGFyYW1zLCBib2R5UGFyYW0sXHJcbiAgICAgICAgICAgIGNvbnRlbnRUeXBlcywgYWNjZXB0cywgT2JqZWN0LCBudWxsLCBudWxsKVxyXG4gICAgICAgICkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRmluZCB1c2VycyBiYXNlZCBvbiBlbWFpbCBpbnB1dC5cclxuICAgICAqIEBwYXJhbSBlbWFpbCBTZWFyY2ggcXVlcnkgc3RyaW5nXHJcbiAgICAgKiBAcmV0dXJucyBMaXN0IG9mIHVzZXJzXHJcbiAgICAgKi9cclxuICAgIGZpbmRVc2VyQnlFbWFpbChlbWFpbDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBpZiAoZW1haWwgPT09ICcnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBvZihbXSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuYnVpbGRVc2VyVXJsKCk7XHJcbiAgICAgICAgY29uc3QgaHR0cE1ldGhvZCA9ICdHRVQnLCBwYXRoUGFyYW1zID0ge30sIHF1ZXJ5UGFyYW1zID0geyBlbWFpbDogZW1haWwgfSwgYm9keVBhcmFtID0ge30sIGhlYWRlclBhcmFtcyA9IHt9LFxyXG4gICAgICAgICAgICBmb3JtUGFyYW1zID0ge30sIGNvbnRlbnRUeXBlcyA9IFsnYXBwbGljYXRpb24vanNvbiddLCBhY2NlcHRzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiAoZnJvbSh0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLm9hdXRoMkF1dGguY2FsbEN1c3RvbUFwaShcclxuICAgICAgICAgICAgdXJsLCBodHRwTWV0aG9kLCBwYXRoUGFyYW1zLCBxdWVyeVBhcmFtcyxcclxuICAgICAgICAgICAgaGVhZGVyUGFyYW1zLCBmb3JtUGFyYW1zLCBib2R5UGFyYW0sXHJcbiAgICAgICAgICAgIGNvbnRlbnRUeXBlcywgYWNjZXB0cywgT2JqZWN0LCBudWxsLCBudWxsKVxyXG4gICAgICAgICkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRmluZCB1c2VycyBiYXNlZCBvbiBpZCBpbnB1dC5cclxuICAgICAqIEBwYXJhbSBpZCBTZWFyY2ggcXVlcnkgc3RyaW5nXHJcbiAgICAgKiBAcmV0dXJucyB1c2VycyBvYmplY3RcclxuICAgICAqL1xyXG4gICAgZmluZFVzZXJCeUlkKGlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGlmIChpZCA9PT0gJycpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG9mKFtdKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5idWlsZFVzZXJVcmwoKSArICcvJyArIGlkO1xyXG4gICAgICAgIGNvbnN0IGh0dHBNZXRob2QgPSAnR0VUJywgcGF0aFBhcmFtcyA9IHt9LCBxdWVyeVBhcmFtcyA9IHt9LCBib2R5UGFyYW0gPSB7fSwgaGVhZGVyUGFyYW1zID0ge30sXHJcbiAgICAgICAgICAgIGZvcm1QYXJhbXMgPSB7fSwgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ10sIGFjY2VwdHMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIChmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkub2F1dGgyQXV0aC5jYWxsQ3VzdG9tQXBpKFxyXG4gICAgICAgICAgICB1cmwsIGh0dHBNZXRob2QsIHBhdGhQYXJhbXMsIHF1ZXJ5UGFyYW1zLFxyXG4gICAgICAgICAgICBoZWFkZXJQYXJhbXMsIGZvcm1QYXJhbXMsIGJvZHlQYXJhbSxcclxuICAgICAgICAgICAgY29udGVudFR5cGVzLCBhY2NlcHRzLCBPYmplY3QsIG51bGwsIG51bGwpXHJcbiAgICAgICAgKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgY2xpZW50IHJvbGVzIG9mIGEgdXNlciBmb3IgYSBwYXJ0aWN1bGFyIGNsaWVudC5cclxuICAgICAqIEBwYXJhbSB1c2VySWQgSUQgb2YgdGhlIHRhcmdldCB1c2VyXHJcbiAgICAgKiBAcGFyYW0gY2xpZW50SWQgSUQgb2YgdGhlIGNsaWVudCBhcHBcclxuICAgICAqIEByZXR1cm5zIExpc3Qgb2YgY2xpZW50IHJvbGVzXHJcbiAgICAgKi9cclxuICAgIGdldENsaWVudFJvbGVzKHVzZXJJZDogc3RyaW5nLCBjbGllbnRJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnlbXT4ge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuYnVpbGRVc2VyQ2xpZW50Um9sZU1hcHBpbmcodXNlcklkLCBjbGllbnRJZCk7XHJcbiAgICAgICAgY29uc3QgaHR0cE1ldGhvZCA9ICdHRVQnLCBwYXRoUGFyYW1zID0ge30sIHF1ZXJ5UGFyYW1zID0ge30sIGJvZHlQYXJhbSA9IHt9LCBoZWFkZXJQYXJhbXMgPSB7fSxcclxuICAgICAgICAgICAgZm9ybVBhcmFtcyA9IHt9LCBjb250ZW50VHlwZXMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXSwgYWNjZXB0cyA9IFsnYXBwbGljYXRpb24vanNvbiddO1xyXG5cclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLm9hdXRoMkF1dGguY2FsbEN1c3RvbUFwaShcclxuICAgICAgICAgICAgdXJsLCBodHRwTWV0aG9kLCBwYXRoUGFyYW1zLCBxdWVyeVBhcmFtcyxcclxuICAgICAgICAgICAgaGVhZGVyUGFyYW1zLCBmb3JtUGFyYW1zLCBib2R5UGFyYW0sXHJcbiAgICAgICAgICAgIGNvbnRlbnRUeXBlcywgYWNjZXB0cywgT2JqZWN0LCBudWxsLCBudWxsKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVja3Mgd2hldGhlciB1c2VyIGhhcyBhY2Nlc3MgdG8gYSBjbGllbnQgYXBwLlxyXG4gICAgICogQHBhcmFtIHVzZXJJZCBJRCBvZiB0aGUgdGFyZ2V0IHVzZXJcclxuICAgICAqIEBwYXJhbSBjbGllbnRJZCBJRCBvZiB0aGUgY2xpZW50IGFwcFxyXG4gICAgICogQHJldHVybnMgVHJ1ZSBpZiB0aGUgdXNlciBoYXMgYWNjZXNzLCBmYWxzZSBvdGhlcndpc2VcclxuICAgICAqL1xyXG4gICAgY2hlY2tVc2VySGFzQ2xpZW50QXBwKHVzZXJJZDogc3RyaW5nLCBjbGllbnRJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0Q2xpZW50Um9sZXModXNlcklkLCBjbGllbnRJZCkucGlwZShcclxuICAgICAgICAgICAgbWFwKChjbGllbnRSb2xlczogYW55W10pID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChjbGllbnRSb2xlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrcyB3aGV0aGVyIGEgdXNlciBoYXMgYW55IG9mIHRoZSBjbGllbnQgYXBwIHJvbGVzLlxyXG4gICAgICogQHBhcmFtIHVzZXJJZCBJRCBvZiB0aGUgdGFyZ2V0IHVzZXJcclxuICAgICAqIEBwYXJhbSBjbGllbnRJZCBJRCBvZiB0aGUgY2xpZW50IGFwcFxyXG4gICAgICogQHBhcmFtIHJvbGVOYW1lcyBMaXN0IG9mIHJvbGUgbmFtZXMgdG8gY2hlY2sgZm9yXHJcbiAgICAgKiBAcmV0dXJucyBUcnVlIGlmIHRoZSB1c2VyIGhhcyBvbmUgb3IgbW9yZSBvZiB0aGUgcm9sZXMsIGZhbHNlIG90aGVyd2lzZVxyXG4gICAgICovXHJcbiAgICBjaGVja1VzZXJIYXNBbnlDbGllbnRBcHBSb2xlKHVzZXJJZDogc3RyaW5nLCBjbGllbnRJZDogc3RyaW5nLCByb2xlTmFtZXM6IHN0cmluZ1tdKTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0Q2xpZW50Um9sZXModXNlcklkLCBjbGllbnRJZCkucGlwZShcclxuICAgICAgICAgICAgbWFwKChjbGllbnRSb2xlczogYW55W10pID0+IHtcclxuICAgICAgICAgICAgICAgIGxldCBoYXNSb2xlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICBpZiAoY2xpZW50Um9sZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJvbGVOYW1lcy5mb3JFYWNoKChyb2xlTmFtZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCByb2xlID0gY2xpZW50Um9sZXMuZmluZCgoYXZhaWxhYmxlUm9sZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGF2YWlsYWJsZVJvbGUubmFtZSA9PT0gcm9sZU5hbWU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJvbGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhc1JvbGUgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gaGFzUm9sZTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgY2xpZW50IElEIGZvciBhbiBhcHBsaWNhdGlvbi5cclxuICAgICAqIEBwYXJhbSBhcHBsaWNhdGlvbk5hbWUgTmFtZSBvZiB0aGUgYXBwbGljYXRpb25cclxuICAgICAqIEByZXR1cm5zIENsaWVudCBJRCBzdHJpbmdcclxuICAgICAqL1xyXG4gICAgZ2V0Q2xpZW50SWRCeUFwcGxpY2F0aW9uTmFtZShhcHBsaWNhdGlvbk5hbWU6IHN0cmluZyk6IE9ic2VydmFibGU8c3RyaW5nPiB7XHJcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5idWlsZEdldENsaWVudHNVcmwoKTtcclxuICAgICAgICBjb25zdCBodHRwTWV0aG9kID0gJ0dFVCcsIHBhdGhQYXJhbXMgPSB7fSwgcXVlcnlQYXJhbXMgPSB7IGNsaWVudElkOiBhcHBsaWNhdGlvbk5hbWUgfSwgYm9keVBhcmFtID0ge30sIGhlYWRlclBhcmFtcyA9IHt9LCBmb3JtUGFyYW1zID0ge30sXHJcbiAgICAgICAgICAgIGNvbnRlbnRUeXBlcyA9IFsnYXBwbGljYXRpb24vanNvbiddLCBhY2NlcHRzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKVxyXG4gICAgICAgICAgICAub2F1dGgyQXV0aC5jYWxsQ3VzdG9tQXBpKHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsIGhlYWRlclBhcmFtcyxcclxuICAgICAgICAgICAgICAgIGZvcm1QYXJhbXMsIGJvZHlQYXJhbSwgY29udGVudFR5cGVzLFxyXG4gICAgICAgICAgICAgICAgYWNjZXB0cywgT2JqZWN0LCBudWxsLCBudWxsKVxyXG4gICAgICAgICkucGlwZShcclxuICAgICAgICAgICAgbWFwKChyZXNwb25zZTogYW55W10pID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNsaWVudElkID0gcmVzcG9uc2UgJiYgcmVzcG9uc2UubGVuZ3RoID4gMCA/IHJlc3BvbnNlWzBdLmlkIDogJyc7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gY2xpZW50SWQ7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrcyBpZiBhIHVzZXIgaGFzIGFjY2VzcyB0byBhbiBhcHBsaWNhdGlvbi5cclxuICAgICAqIEBwYXJhbSB1c2VySWQgSUQgb2YgdGhlIHVzZXJcclxuICAgICAqIEBwYXJhbSBhcHBsaWNhdGlvbk5hbWUgTmFtZSBvZiB0aGUgYXBwbGljYXRpb25cclxuICAgICAqIEByZXR1cm5zIFRydWUgaWYgdGhlIHVzZXIgaGFzIGFjY2VzcywgZmFsc2Ugb3RoZXJ3aXNlXHJcbiAgICAgKi9cclxuICAgIGNoZWNrVXNlckhhc0FwcGxpY2F0aW9uQWNjZXNzKHVzZXJJZDogc3RyaW5nLCBhcHBsaWNhdGlvbk5hbWU6IHN0cmluZyk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldENsaWVudElkQnlBcHBsaWNhdGlvbk5hbWUoYXBwbGljYXRpb25OYW1lKS5waXBlKFxyXG4gICAgICAgICAgICBzd2l0Y2hNYXAoKGNsaWVudElkOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmNoZWNrVXNlckhhc0NsaWVudEFwcCh1c2VySWQsIGNsaWVudElkKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2tzIGlmIGEgdXNlciBoYXMgYW55IGFwcGxpY2F0aW9uIHJvbGUuXHJcbiAgICAgKiBAcGFyYW0gdXNlcklkIElEIG9mIHRoZSB0YXJnZXQgdXNlclxyXG4gICAgICogQHBhcmFtIGFwcGxpY2F0aW9uTmFtZSBOYW1lIG9mIHRoZSBhcHBsaWNhdGlvblxyXG4gICAgICogQHBhcmFtIHJvbGVOYW1lcyBMaXN0IG9mIHJvbGUgbmFtZXMgdG8gY2hlY2sgZm9yXHJcbiAgICAgKiBAcmV0dXJucyBUcnVlIGlmIHRoZSB1c2VyIGhhcyBvbmUgb3IgbW9yZSBvZiB0aGUgcm9sZXMsIGZhbHNlIG90aGVyd2lzZVxyXG4gICAgICovXHJcbiAgICBjaGVja1VzZXJIYXNBbnlBcHBsaWNhdGlvblJvbGUodXNlcklkOiBzdHJpbmcsIGFwcGxpY2F0aW9uTmFtZTogc3RyaW5nLCByb2xlTmFtZXM6IHN0cmluZ1tdKTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0Q2xpZW50SWRCeUFwcGxpY2F0aW9uTmFtZShhcHBsaWNhdGlvbk5hbWUpLnBpcGUoXHJcbiAgICAgICAgICAgIHN3aXRjaE1hcCgoY2xpZW50SWQ6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuY2hlY2tVc2VySGFzQW55Q2xpZW50QXBwUm9sZSh1c2VySWQsIGNsaWVudElkLCByb2xlTmFtZXMpO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGRldGFpbHMgZm9yIGFsbCB1c2Vycy5cclxuICAgICAqIEByZXR1cm5zIEFycmF5IG9mIHVzZXIgaW5mbyBvYmplY3RzXHJcbiAgICAgKi9cclxuICAgIGdldFVzZXJzKCk6IE9ic2VydmFibGU8SWRlbnRpdHlVc2VyTW9kZWxbXT4ge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuYnVpbGRVc2VyVXJsKCk7XHJcbiAgICAgICAgY29uc3QgaHR0cE1ldGhvZCA9ICdHRVQnLCBwYXRoUGFyYW1zID0ge30sIHF1ZXJ5UGFyYW1zID0ge30sIGJvZHlQYXJhbSA9IHt9LCBoZWFkZXJQYXJhbXMgPSB7fSxcclxuICAgICAgICAgICAgZm9ybVBhcmFtcyA9IHt9LCBhdXRoTmFtZXMgPSBbXSwgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ10sIGFjY2VwdHMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgICAgIGhlYWRlclBhcmFtcywgZm9ybVBhcmFtcywgYm9keVBhcmFtLCBhdXRoTmFtZXMsXHJcbiAgICAgICAgICAgIGNvbnRlbnRUeXBlcywgYWNjZXB0cywgbnVsbCwgbnVsbClcclxuICAgICAgICApLnBpcGUoXHJcbiAgICAgICAgICAgIG1hcCgocmVzcG9uc2U6IElkZW50aXR5VXNlck1vZGVsW10pID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhIGxpc3Qgb2Ygcm9sZXMgZm9yIGEgdXNlci5cclxuICAgICAqIEBwYXJhbSB1c2VySWQgSUQgb2YgdGhlIHVzZXJcclxuICAgICAqIEByZXR1cm5zIEFycmF5IG9mIHJvbGUgaW5mbyBvYmplY3RzXHJcbiAgICAgKi9cclxuICAgIGdldFVzZXJSb2xlcyh1c2VySWQ6IHN0cmluZyk6IE9ic2VydmFibGU8SWRlbnRpdHlSb2xlTW9kZWxbXT4ge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuYnVpbGRSb2xlc1VybCh1c2VySWQpO1xyXG4gICAgICAgIGNvbnN0IGh0dHBNZXRob2QgPSAnR0VUJywgcGF0aFBhcmFtcyA9IHt9LCBxdWVyeVBhcmFtcyA9IHt9LCBib2R5UGFyYW0gPSB7fSwgaGVhZGVyUGFyYW1zID0ge30sXHJcbiAgICAgICAgICAgIGZvcm1QYXJhbXMgPSB7fSwgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ10sIGFjY2VwdHMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgICAgIGhlYWRlclBhcmFtcywgZm9ybVBhcmFtcywgYm9keVBhcmFtLFxyXG4gICAgICAgICAgICBjb250ZW50VHlwZXMsIGFjY2VwdHMsIE9iamVjdCwgbnVsbCwgbnVsbClcclxuICAgICAgICApLnBpcGUoXHJcbiAgICAgICAgICAgIG1hcCgocmVzcG9uc2U6IElkZW50aXR5Um9sZU1vZGVsW10pID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiByZXNwb25zZTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhbiBhcnJheSBvZiB1c2VycyAoaW5jbHVkaW5nIHRoZSBjdXJyZW50IHVzZXIpIHdobyBoYXZlIGFueSBvZiB0aGUgcm9sZXMgaW4gdGhlIHN1cHBsaWVkIGxpc3QuXHJcbiAgICAgKiBAcGFyYW0gcm9sZU5hbWVzIExpc3Qgb2Ygcm9sZSBuYW1lcyB0byBsb29rIGZvclxyXG4gICAgICogQHJldHVybnMgQXJyYXkgb2YgdXNlciBpbmZvIG9iamVjdHNcclxuICAgICAqL1xyXG4gICAgYXN5bmMgZ2V0VXNlcnNCeVJvbGVzV2l0aEN1cnJlbnRVc2VyKHJvbGVOYW1lczogc3RyaW5nW10pOiBQcm9taXNlPElkZW50aXR5VXNlck1vZGVsW10+IHtcclxuICAgICAgICBjb25zdCBmaWx0ZXJlZFVzZXJzOiBJZGVudGl0eVVzZXJNb2RlbFtdID0gW107XHJcbiAgICAgICAgaWYgKHJvbGVOYW1lcyAmJiByb2xlTmFtZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBjb25zdCB1c2VycyA9IGF3YWl0IHRoaXMuZ2V0VXNlcnMoKS50b1Byb21pc2UoKTtcclxuXHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdXNlcnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGhhc0FueVJvbGUgPSBhd2FpdCB0aGlzLnVzZXJIYXNBbnlSb2xlKHVzZXJzW2ldLmlkLCByb2xlTmFtZXMpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGhhc0FueVJvbGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJlZFVzZXJzLnB1c2godXNlcnNbaV0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZmlsdGVyZWRVc2VycztcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYW4gYXJyYXkgb2YgdXNlcnMgKG5vdCBpbmNsdWRpbmcgdGhlIGN1cnJlbnQgdXNlcikgd2hvIGhhdmUgYW55IG9mIHRoZSByb2xlcyBpbiB0aGUgc3VwcGxpZWQgbGlzdC5cclxuICAgICAqIEBwYXJhbSByb2xlTmFtZXMgTGlzdCBvZiByb2xlIG5hbWVzIHRvIGxvb2sgZm9yXHJcbiAgICAgKiBAcmV0dXJucyBBcnJheSBvZiB1c2VyIGluZm8gb2JqZWN0c1xyXG4gICAgICovXHJcbiAgICBhc3luYyBnZXRVc2Vyc0J5Um9sZXNXaXRob3V0Q3VycmVudFVzZXIocm9sZU5hbWVzOiBzdHJpbmdbXSk6IFByb21pc2U8SWRlbnRpdHlVc2VyTW9kZWxbXT4ge1xyXG4gICAgICAgIGNvbnN0IGZpbHRlcmVkVXNlcnM6IElkZW50aXR5VXNlck1vZGVsW10gPSBbXTtcclxuICAgICAgICBpZiAocm9sZU5hbWVzICYmIHJvbGVOYW1lcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGN1cnJlbnRVc2VyID0gdGhpcy5nZXRDdXJyZW50VXNlckluZm8oKTtcclxuICAgICAgICAgICAgbGV0IHVzZXJzID0gYXdhaXQgdGhpcy5nZXRVc2VycygpLnRvUHJvbWlzZSgpO1xyXG5cclxuICAgICAgICAgICAgdXNlcnMgPSB1c2Vycy5maWx0ZXIoKHVzZXIpID0+IHsgcmV0dXJuIHVzZXIudXNlcm5hbWUgIT09IGN1cnJlbnRVc2VyLnVzZXJuYW1lOyB9KTtcclxuXHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdXNlcnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGhhc0FueVJvbGUgPSBhd2FpdCB0aGlzLnVzZXJIYXNBbnlSb2xlKHVzZXJzW2ldLmlkLCByb2xlTmFtZXMpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGhhc0FueVJvbGUpIHtcclxuICAgICAgICAgICAgICAgICAgICBmaWx0ZXJlZFVzZXJzLnB1c2godXNlcnNbaV0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZmlsdGVyZWRVc2VycztcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGFzeW5jIHVzZXJIYXNBbnlSb2xlKHVzZXJJZDogc3RyaW5nLCByb2xlTmFtZXM6IHN0cmluZ1tdKTogUHJvbWlzZTxib29sZWFuPiB7XHJcbiAgICAgICAgY29uc3QgdXNlclJvbGVzID0gYXdhaXQgdGhpcy5nZXRVc2VyUm9sZXModXNlcklkKS50b1Byb21pc2UoKTtcclxuICAgICAgICBjb25zdCBoYXNBbnlSb2xlID0gcm9sZU5hbWVzLnNvbWUoKHJvbGVOYW1lKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGZpbHRlcmVkUm9sZXMgPSB1c2VyUm9sZXMuZmlsdGVyKCh1c2VyUm9sZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHVzZXJSb2xlLm5hbWUudG9Mb2NhbGVMb3dlckNhc2UoKSA9PT0gcm9sZU5hbWUudG9Mb2NhbGVMb3dlckNhc2UoKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gZmlsdGVyZWRSb2xlcy5sZW5ndGggPiAwO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gaGFzQW55Um9sZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrcyBpZiBhIHVzZXIgaGFzIG9uZSBvZiB0aGUgcm9sZXMgZnJvbSBhIGxpc3QuXHJcbiAgICAgKiBAcGFyYW0gdXNlcklkIElEIG9mIHRoZSB0YXJnZXQgdXNlclxyXG4gICAgICogQHBhcmFtIHJvbGVOYW1lcyBBcnJheSBvZiByb2xlcyB0byBjaGVjayBmb3JcclxuICAgICAqIEByZXR1cm5zIFRydWUgaWYgdGhlIHVzZXIgaGFzIG9uZSBvZiB0aGUgcm9sZXMsIGZhbHNlIG90aGVyd2lzZVxyXG4gICAgICovXHJcbiAgICBjaGVja1VzZXJIYXNSb2xlKHVzZXJJZDogc3RyaW5nLCByb2xlTmFtZXM6IHN0cmluZ1tdKTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0VXNlclJvbGVzKHVzZXJJZCkucGlwZShtYXAoKHVzZXJSb2xlczogSWRlbnRpdHlSb2xlTW9kZWxbXSkgPT4ge1xyXG4gICAgICAgICAgICBsZXQgaGFzUm9sZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICBpZiAodXNlclJvbGVzICYmIHVzZXJSb2xlcy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICByb2xlTmFtZXMuZm9yRWFjaCgocm9sZU5hbWU6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJvbGUgPSB1c2VyUm9sZXMuZmluZCgodXNlclJvbGUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJvbGVOYW1lID09PSB1c2VyUm9sZS5uYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChyb2xlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc1JvbGUgPSB0cnVlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIGhhc1JvbGU7XHJcbiAgICAgICAgfSkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBkZXRhaWxzIGZvciBhbGwgdXNlcnMuXHJcbiAgICAgKiBAcmV0dXJucyBBcnJheSBvZiB1c2VyIGluZm9ybWF0aW9uIG9iamVjdHMuXHJcbiAgICAgKi9cclxuICAgIHF1ZXJ5VXNlcnMocmVxdWVzdFF1ZXJ5OiBJZGVudGl0eVVzZXJRdWVyeUNsb3VkUmVxdWVzdE1vZGVsKTogT2JzZXJ2YWJsZTxJZGVudGl0eVVzZXJRdWVyeVJlc3BvbnNlPiB7XHJcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5idWlsZFVzZXJVcmwoKTtcclxuICAgICAgICBjb25zdCBodHRwTWV0aG9kID0gJ0dFVCcsIHBhdGhQYXJhbXMgPSB7fSxcclxuICAgICAgICBxdWVyeVBhcmFtcyA9IHsgZmlyc3Q6IHJlcXVlc3RRdWVyeS5maXJzdCwgbWF4OiByZXF1ZXN0UXVlcnkubWF4IH0sIGJvZHlQYXJhbSA9IHt9LCBoZWFkZXJQYXJhbXMgPSB7fSxcclxuICAgICAgICBmb3JtUGFyYW1zID0ge30sIGF1dGhOYW1lcyA9IFtdLCBjb250ZW50VHlwZXMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0VG90YWxVc2Vyc0NvdW50KCkucGlwZShcclxuICAgICAgICAgICAgICAgIHN3aXRjaE1hcCgodG90YWxDb3VudDogYW55KSA9PlxyXG4gICAgICAgICAgICAgICAgZnJvbSh0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLm9hdXRoMkF1dGguY2FsbEN1c3RvbUFwaShcclxuICAgICAgICAgICAgICAgICAgICB1cmwsIGh0dHBNZXRob2QsIHBhdGhQYXJhbXMsIHF1ZXJ5UGFyYW1zLFxyXG4gICAgICAgICAgICAgICAgICAgIGhlYWRlclBhcmFtcywgZm9ybVBhcmFtcywgYm9keVBhcmFtLCBhdXRoTmFtZXMsXHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudFR5cGVzLCBudWxsLCBudWxsLCBudWxsKVxyXG4gICAgICAgICAgICAgICAgKS5waXBlKFxyXG4gICAgICAgICAgICAgICAgICAgIG1hcCgocmVzcG9uc2U6IElkZW50aXR5VXNlck1vZGVsW10pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIDxJZGVudGl0eVVzZXJRdWVyeVJlc3BvbnNlPiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbnRyaWVzOiByZXNwb25zZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHBhZ2luYXRpb246IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgc2tpcENvdW50OiByZXF1ZXN0UXVlcnkuZmlyc3QsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heEl0ZW1zOiByZXF1ZXN0UXVlcnkubWF4LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb3VudDogdG90YWxDb3VudCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaGFzTW9yZUl0ZW1zOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdG90YWxJdGVtczogdG90YWxDb3VudFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyb3IpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyb3IpKVxyXG4gICAgICAgICAgICAgICAgICAgICkpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdXNlcnMgdG90YWwgY291bnQuXHJcbiAgICAgKiBAcmV0dXJucyBOdW1iZXIgb2YgdXNlcnMgY291bnQuXHJcbiAgICAgKi9cclxuICAgIGdldFRvdGFsVXNlcnNDb3VudCgpOiBPYnNlcnZhYmxlPG51bWJlcj4ge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuYnVpbGRVc2VyVXJsKCkgKyBgL2NvdW50YDtcclxuICAgICAgICBjb25zdCBjb250ZW50VHlwZXMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXSwgYWNjZXB0cyA9IFsnYXBwbGljYXRpb24vanNvbiddO1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKClcclxuICAgICAgICAgICAgLm9hdXRoMkF1dGguY2FsbEN1c3RvbUFwaSh1cmwsICdHRVQnLFxyXG4gICAgICAgICAgICAgIG51bGwsIG51bGwsIG51bGwsXHJcbiAgICAgICAgICAgICAgbnVsbCwgbnVsbCwgY29udGVudFR5cGVzLFxyXG4gICAgICAgICAgICAgIGFjY2VwdHMsIG51bGwsIG51bGwsIG51bGxcclxuICAgICAgICAgICAgICApKS5waXBlKFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyb3IpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyb3IpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBuZXcgdXNlci5cclxuICAgICAqIEBwYXJhbSBuZXdVc2VyIE9iamVjdCBjb250YWluaW5nIHRoZSBuZXcgdXNlciBkZXRhaWxzLlxyXG4gICAgICogQHJldHVybnMgRW1wdHkgcmVzcG9uc2Ugd2hlbiB0aGUgdXNlciBjcmVhdGVkLlxyXG4gICAgICovXHJcbiAgICBjcmVhdGVVc2VyKG5ld1VzZXI6IElkZW50aXR5VXNlck1vZGVsKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlclVybCgpO1xyXG4gICAgICAgIGNvbnN0IHJlcXVlc3QgPSBKU09OLnN0cmluZ2lmeShuZXdVc2VyKTtcclxuICAgICAgICBjb25zdCBodHRwTWV0aG9kID0gJ1BPU1QnLCBwYXRoUGFyYW1zID0ge30sIHF1ZXJ5UGFyYW1zID0ge30sIGJvZHlQYXJhbSA9IHJlcXVlc3QsIGhlYWRlclBhcmFtcyA9IHt9LFxyXG4gICAgICAgIGZvcm1QYXJhbXMgPSB7fSwgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ10sIGFjY2VwdHMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgdXJsLCBodHRwTWV0aG9kLCBwYXRoUGFyYW1zLCBxdWVyeVBhcmFtcyxcclxuICAgICAgICBoZWFkZXJQYXJhbXMsIGZvcm1QYXJhbXMsIGJvZHlQYXJhbSxcclxuICAgICAgICBjb250ZW50VHlwZXMsIGFjY2VwdHMsIG51bGwsIG51bGwsIG51bGxcclxuICAgICAgICApKS5waXBlKFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnJvcikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnJvcikpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFVwZGF0ZXMgdXNlciBkZXRhaWxzLlxyXG4gICAgICogQHBhcmFtIHVzZXJJZCBJZCBvZiB0aGUgdXNlci5cclxuICAgICAqIEBwYXJhbSB1cGRhdGVkVXNlciBPYmplY3QgY29udGFpbmluZyB0aGUgdXNlciBkZXRhaWxzLlxyXG4gICAgICogQHJldHVybnMgRW1wdHkgcmVzcG9uc2Ugd2hlbiB0aGUgdXNlciB1cGRhdGVkLlxyXG4gICAgICovXHJcbiAgICB1cGRhdGVVc2VyKHVzZXJJZDogc3RyaW5nLCB1cGRhdGVkVXNlcjogSWRlbnRpdHlVc2VyTW9kZWwpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuYnVpbGRVc2VyVXJsKCkgKyAnLycgKyB1c2VySWQ7XHJcbiAgICAgICAgY29uc3QgcmVxdWVzdCA9IEpTT04uc3RyaW5naWZ5KHVwZGF0ZWRVc2VyKTtcclxuICAgICAgICBjb25zdCBodHRwTWV0aG9kID0gJ1BVVCcsIHBhdGhQYXJhbXMgPSB7fSAsIHF1ZXJ5UGFyYW1zID0ge30sIGJvZHlQYXJhbSA9IHJlcXVlc3QsIGhlYWRlclBhcmFtcyA9IHt9LFxyXG4gICAgICAgIGZvcm1QYXJhbXMgPSB7fSwgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ10sIGFjY2VwdHMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgdXJsLCBodHRwTWV0aG9kLCBwYXRoUGFyYW1zLCBxdWVyeVBhcmFtcyxcclxuICAgICAgICBoZWFkZXJQYXJhbXMsIGZvcm1QYXJhbXMsIGJvZHlQYXJhbSxcclxuICAgICAgICBjb250ZW50VHlwZXMsIGFjY2VwdHMsIG51bGwsIG51bGwsIG51bGxcclxuICAgICAgICApKS5waXBlKFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnJvcikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnJvcikpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERlbGV0ZXMgVXNlci5cclxuICAgICAqIEBwYXJhbSB1c2VySWQgSWQgb2YgdGhlICB1c2VyLlxyXG4gICAgICogQHJldHVybnMgRW1wdHkgcmVzcG9uc2Ugd2hlbiB0aGUgdXNlciBkZWxldGVkLlxyXG4gICAgICovXHJcbiAgICBkZWxldGVVc2VyKHVzZXJJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlclVybCgpICsgJy8nICsgdXNlcklkO1xyXG4gICAgICAgIGNvbnN0IGh0dHBNZXRob2QgPSAnREVMRVRFJywgcGF0aFBhcmFtcyA9IHt9ICwgcXVlcnlQYXJhbXMgPSB7fSwgYm9keVBhcmFtID0ge30sIGhlYWRlclBhcmFtcyA9IHt9LFxyXG4gICAgICAgIGZvcm1QYXJhbXMgPSB7fSwgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ10sIGFjY2VwdHMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgdXJsLCBodHRwTWV0aG9kLCBwYXRoUGFyYW1zLCBxdWVyeVBhcmFtcyxcclxuICAgICAgICBoZWFkZXJQYXJhbXMsIGZvcm1QYXJhbXMsIGJvZHlQYXJhbSxcclxuICAgICAgICBjb250ZW50VHlwZXMsIGFjY2VwdHMsIG51bGwsIG51bGwsIG51bGxcclxuICAgICAgICApKS5waXBlKFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnJvcikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnJvcikpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoYW5nZXMgdXNlciBwYXNzd29yZC5cclxuICAgICAqIEBwYXJhbSB1c2VySWQgSWQgb2YgdGhlIHVzZXIuXHJcbiAgICAgKiBAcGFyYW0gY3JlZGVudGlhbHMgRGV0YWlscyBvZiB1c2VyIENyZWRlbnRpYWxzLlxyXG4gICAgICogQHJldHVybnMgRW1wdHkgcmVzcG9uc2Ugd2hlbiB0aGUgcGFzc3dvcmQgY2hhbmdlZC5cclxuICAgICAqL1xyXG4gICAgY2hhbmdlUGFzc3dvcmQodXNlcklkOiBzdHJpbmcsIG5ld1Bhc3N3b3JkOiBJZGVudGl0eVVzZXJQYXNzd29yZE1vZGVsKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlclVybCgpICsgJy8nICsgdXNlcklkICsgJy9yZXNldC1wYXNzd29yZCc7XHJcbiAgICAgICAgY29uc3QgcmVxdWVzdCA9IEpTT04uc3RyaW5naWZ5KG5ld1Bhc3N3b3JkKTtcclxuICAgICAgICBjb25zdCBodHRwTWV0aG9kID0gJ1BVVCcsIHBhdGhQYXJhbXMgPSB7fSAsIHF1ZXJ5UGFyYW1zID0ge30sIGJvZHlQYXJhbSA9IHJlcXVlc3QsIGhlYWRlclBhcmFtcyA9IHt9LFxyXG4gICAgICAgIGZvcm1QYXJhbXMgPSB7fSwgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ10sIGFjY2VwdHMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgdXJsLCBodHRwTWV0aG9kLCBwYXRoUGFyYW1zLCBxdWVyeVBhcmFtcyxcclxuICAgICAgICBoZWFkZXJQYXJhbXMsIGZvcm1QYXJhbXMsIGJvZHlQYXJhbSxcclxuICAgICAgICBjb250ZW50VHlwZXMsIGFjY2VwdHMsIG51bGwsIG51bGwsIG51bGxcclxuICAgICAgICApKS5waXBlKFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnJvcikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnJvcikpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgaW52b2x2ZWQgZ3JvdXBzLlxyXG4gICAgICogQHBhcmFtIHVzZXJJZCBJZCBvZiB0aGUgdXNlci5cclxuICAgICAqIEByZXR1cm5zIEFycmF5IG9mIGludm9sdmVkIGdyb3VwcyBpbmZvcm1hdGlvbiBvYmplY3RzLlxyXG4gICAgICovXHJcbiAgICBnZXRJbnZvbHZlZEdyb3Vwcyh1c2VySWQ6IHN0cmluZyk6IE9ic2VydmFibGU8SWRlbnRpdHlHcm91cE1vZGVsW10+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlclVybCgpICsgJy8nICsgdXNlcklkICsgJy9ncm91cHMvJztcclxuICAgICAgICBjb25zdCBodHRwTWV0aG9kID0gJ0dFVCcsIHBhdGhQYXJhbXMgPSB7IGlkOiB1c2VySWR9LFxyXG4gICAgICAgIHF1ZXJ5UGFyYW1zID0ge30sIGJvZHlQYXJhbSA9IHt9LCBoZWFkZXJQYXJhbXMgPSB7fSxcclxuICAgICAgICBmb3JtUGFyYW1zID0ge30sIGF1dGhOYW1lcyA9IFtdLCBjb250ZW50VHlwZXMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgICAgICAgICAgICAgdXJsLCBodHRwTWV0aG9kLCBwYXRoUGFyYW1zLCBxdWVyeVBhcmFtcyxcclxuICAgICAgICAgICAgICAgICAgICBoZWFkZXJQYXJhbXMsIGZvcm1QYXJhbXMsIGJvZHlQYXJhbSwgYXV0aE5hbWVzLFxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnRUeXBlcywgbnVsbCwgbnVsbCwgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICkpLnBpcGUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycm9yKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycm9yKSlcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogSm9pbnMgZ3JvdXAuXHJcbiAgICAgKiBAcGFyYW0gam9pbkdyb3VwUmVxdWVzdCBEZXRhaWxzIG9mIGpvaW4gZ3JvdXAgcmVxdWVzdCAoSWRlbnRpdHlKb2luR3JvdXBSZXF1ZXN0TW9kZWwpLlxyXG4gICAgICogQHJldHVybnMgRW1wdHkgcmVzcG9uc2Ugd2hlbiB0aGUgdXNlciBqb2luZWQgdGhlIGdyb3VwLlxyXG4gICAgICovXHJcbiAgICBqb2luR3JvdXAoam9pbkdyb3VwUmVxdWVzdDogSWRlbnRpdHlKb2luR3JvdXBSZXF1ZXN0TW9kZWwpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuYnVpbGRVc2VyVXJsKCkgKyAnLycgKyBqb2luR3JvdXBSZXF1ZXN0LnVzZXJJZCArICcvZ3JvdXBzLycgKyBqb2luR3JvdXBSZXF1ZXN0Lmdyb3VwSWQ7XHJcbiAgICAgICAgY29uc3QgcmVxdWVzdCA9IEpTT04uc3RyaW5naWZ5KGpvaW5Hcm91cFJlcXVlc3QpO1xyXG4gICAgICAgIGNvbnN0IGh0dHBNZXRob2QgPSAnUFVUJywgcGF0aFBhcmFtcyA9IHt9ICwgcXVlcnlQYXJhbXMgPSB7fSwgYm9keVBhcmFtID0gcmVxdWVzdCwgaGVhZGVyUGFyYW1zID0ge30sXHJcbiAgICAgICAgZm9ybVBhcmFtcyA9IHt9LCBjb250ZW50VHlwZXMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXSwgYWNjZXB0cyA9IFsnYXBwbGljYXRpb24vanNvbiddO1xyXG5cclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLm9hdXRoMkF1dGguY2FsbEN1c3RvbUFwaShcclxuICAgICAgICB1cmwsIGh0dHBNZXRob2QsIHBhdGhQYXJhbXMsIHF1ZXJ5UGFyYW1zLFxyXG4gICAgICAgIGhlYWRlclBhcmFtcywgZm9ybVBhcmFtcywgYm9keVBhcmFtLFxyXG4gICAgICAgIGNvbnRlbnRUeXBlcywgYWNjZXB0cywgbnVsbCwgbnVsbCwgbnVsbFxyXG4gICAgICAgICkpLnBpcGUoXHJcbiAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycm9yKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycm9yKSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTGVhdmVzIGdyb3VwLlxyXG4gICAgICogQHBhcmFtIHVzZXJJZCBJZCBvZiB0aGUgdXNlci5cclxuICAgICAqIEBwYXJhbSBncm91cElkIElkIG9mIHRoZSAgZ3JvdXAuXHJcbiAgICAgKiBAcmV0dXJucyBFbXB0eSByZXNwb25zZSB3aGVuIHRoZSB1c2VyIGxlZnQgdGhlIGdyb3VwLlxyXG4gICAgICovXHJcbiAgICBsZWF2ZUdyb3VwKHVzZXJJZDogYW55LCBncm91cElkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuYnVpbGRVc2VyVXJsKCkgKyAnLycgKyB1c2VySWQgKyAnL2dyb3Vwcy8nICsgZ3JvdXBJZDtcclxuICAgICAgICBjb25zdCBodHRwTWV0aG9kID0gJ0RFTEVURScsIHBhdGhQYXJhbXMgPSB7fSAsIHF1ZXJ5UGFyYW1zID0ge30sIGJvZHlQYXJhbSA9IHt9LCBoZWFkZXJQYXJhbXMgPSB7fSxcclxuICAgICAgICBmb3JtUGFyYW1zID0ge30sIGNvbnRlbnRUeXBlcyA9IFsnYXBwbGljYXRpb24vanNvbiddLCBhY2NlcHRzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkub2F1dGgyQXV0aC5jYWxsQ3VzdG9tQXBpKFxyXG4gICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgaGVhZGVyUGFyYW1zLCBmb3JtUGFyYW1zLCBib2R5UGFyYW0sXHJcbiAgICAgICAgY29udGVudFR5cGVzLCBhY2NlcHRzLCBudWxsLCBudWxsLCBudWxsXHJcbiAgICAgICAgKSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyb3IpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyb3IpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGF2YWlsYWJsZSByb2xlc1xyXG4gICAgICogQHBhcmFtIHVzZXJJZCBJZCBvZiB0aGUgdXNlci5cclxuICAgICAqIEByZXR1cm5zIEFycmF5IG9mIGF2YWlsYWJsZSByb2xlcyBpbmZvcm1hdGlvbiBvYmplY3RzXHJcbiAgICAgKi9cclxuICAgIGdldEF2YWlsYWJsZVJvbGVzKHVzZXJJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxJZGVudGl0eVJvbGVNb2RlbFtdPiB7XHJcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5idWlsZFVzZXJVcmwoKSArICcvJyArIHVzZXJJZCArICcvcm9sZS1tYXBwaW5ncy9yZWFsbS9hdmFpbGFibGUnO1xyXG4gICAgICAgIGNvbnN0IGh0dHBNZXRob2QgPSAnR0VUJywgcGF0aFBhcmFtcyA9IHt9LFxyXG4gICAgICAgIHF1ZXJ5UGFyYW1zID0ge30sIGJvZHlQYXJhbSA9IHt9LCBoZWFkZXJQYXJhbXMgPSB7fSxcclxuICAgICAgICBmb3JtUGFyYW1zID0ge30sIGF1dGhOYW1lcyA9IFtdLCBjb250ZW50VHlwZXMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgICAgICAgICAgICAgdXJsLCBodHRwTWV0aG9kLCBwYXRoUGFyYW1zLCBxdWVyeVBhcmFtcyxcclxuICAgICAgICAgICAgICAgICAgICBoZWFkZXJQYXJhbXMsIGZvcm1QYXJhbXMsIGJvZHlQYXJhbSwgYXV0aE5hbWVzLFxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnRUeXBlcywgbnVsbCwgbnVsbCwgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICkpLnBpcGUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycm9yKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycm9yKSlcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhc3NpZ25lZCByb2xlcy5cclxuICAgICAqIEBwYXJhbSB1c2VySWQgSWQgb2YgdGhlIHVzZXIuXHJcbiAgICAgKiBAcmV0dXJucyBBcnJheSBvZiBhc3NpZ25lZCByb2xlcyBpbmZvcm1hdGlvbiBvYmplY3RzXHJcbiAgICAgKi9cclxuICAgIGdldEFzc2lnbmVkUm9sZXModXNlcklkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPElkZW50aXR5Um9sZU1vZGVsW10+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlclVybCgpICsgJy8nICsgdXNlcklkICsgJy9yb2xlLW1hcHBpbmdzL3JlYWxtJztcclxuICAgICAgICBjb25zdCBodHRwTWV0aG9kID0gJ0dFVCcsIHBhdGhQYXJhbXMgPSB7IGlkOiB1c2VySWR9LFxyXG4gICAgICAgIHF1ZXJ5UGFyYW1zID0ge30sIGJvZHlQYXJhbSA9IHt9LCBoZWFkZXJQYXJhbXMgPSB7fSxcclxuICAgICAgICBmb3JtUGFyYW1zID0ge30sIGF1dGhOYW1lcyA9IFtdLCBjb250ZW50VHlwZXMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgICAgICAgICAgICAgdXJsLCBodHRwTWV0aG9kLCBwYXRoUGFyYW1zLCBxdWVyeVBhcmFtcyxcclxuICAgICAgICAgICAgICAgICAgICBoZWFkZXJQYXJhbXMsIGZvcm1QYXJhbXMsIGJvZHlQYXJhbSwgYXV0aE5hbWVzLFxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnRUeXBlcywgbnVsbCwgbnVsbCwgbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICkpLnBpcGUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycm9yKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycm9yKSlcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBlZmZlY3RpdmUgcm9sZXMuXHJcbiAgICAgKiBAcGFyYW0gdXNlcklkIElkIG9mIHRoZSB1c2VyLlxyXG4gICAgICogQHJldHVybnMgQXJyYXkgb2YgY29tcG9zaXRlIHJvbGVzIGluZm9ybWF0aW9uIG9iamVjdHNcclxuICAgICAqL1xyXG4gICAgZ2V0RWZmZWN0aXZlUm9sZXModXNlcklkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPElkZW50aXR5Um9sZU1vZGVsW10+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlclVybCgpICsgJy8nICsgdXNlcklkICsgJy9yb2xlLW1hcHBpbmdzL3JlYWxtL2NvbXBvc2l0ZSc7XHJcbiAgICAgICAgY29uc3QgaHR0cE1ldGhvZCA9ICdHRVQnLCBwYXRoUGFyYW1zID0geyBpZDogdXNlcklkfSxcclxuICAgICAgICBxdWVyeVBhcmFtcyA9IHt9LCBib2R5UGFyYW0gPSB7fSwgaGVhZGVyUGFyYW1zID0ge30sXHJcbiAgICAgICAgZm9ybVBhcmFtcyA9IHt9LCBhdXRoTmFtZXMgPSBbXSwgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkub2F1dGgyQXV0aC5jYWxsQ3VzdG9tQXBpKFxyXG4gICAgICAgICAgICAgICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgICAgICAgICAgICAgaGVhZGVyUGFyYW1zLCBmb3JtUGFyYW1zLCBib2R5UGFyYW0sIGF1dGhOYW1lcyxcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50VHlwZXMsIG51bGwsIG51bGwsIG51bGxcclxuICAgICAgICAgICAgICAgICAgICApKS5waXBlKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnJvcikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnJvcikpXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEFzc2lnbnMgcm9sZXMgdG8gdGhlIHVzZXIuXHJcbiAgICAgKiBAcGFyYW0gdXNlcklkIElkIG9mIHRoZSB1c2VyLlxyXG4gICAgICogQHBhcmFtIHJvbGVzIEFycmF5IG9mIHJvbGVzLlxyXG4gICAgICogQHJldHVybnMgRW1wdHkgcmVzcG9uc2Ugd2hlbiB0aGUgcm9sZSBhc3NpZ25lZC5cclxuICAgICAqL1xyXG4gICAgYXNzaWduUm9sZXModXNlcklkOiBzdHJpbmcsIHJvbGVzOiBJZGVudGl0eVJvbGVNb2RlbFtdKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlclVybCgpICsgJy8nICsgdXNlcklkICsgJy9yb2xlLW1hcHBpbmdzL3JlYWxtJztcclxuICAgICAgICBjb25zdCByZXF1ZXN0ID0gSlNPTi5zdHJpbmdpZnkocm9sZXMpO1xyXG4gICAgICAgIGNvbnN0IGh0dHBNZXRob2QgPSAnUE9TVCcsIHBhdGhQYXJhbXMgPSB7fSAsIHF1ZXJ5UGFyYW1zID0ge30sIGJvZHlQYXJhbSA9IHJlcXVlc3QsIGhlYWRlclBhcmFtcyA9IHt9LFxyXG4gICAgICAgIGZvcm1QYXJhbXMgPSB7fSwgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ10sIGFjY2VwdHMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgdXJsLCBodHRwTWV0aG9kLCBwYXRoUGFyYW1zLCBxdWVyeVBhcmFtcyxcclxuICAgICAgICBoZWFkZXJQYXJhbXMsIGZvcm1QYXJhbXMsIGJvZHlQYXJhbSxcclxuICAgICAgICBjb250ZW50VHlwZXMsIGFjY2VwdHMsIG51bGwsIG51bGwsIG51bGxcclxuICAgICAgICApKS5waXBlKFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnJvcikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnJvcikpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlbW92ZXMgYXNzaWduZWQgcm9sZXMuXHJcbiAgICAgKiBAcGFyYW0gdXNlcklkIElkIG9mIHRoZSB1c2VyLlxyXG4gICAgICogQHBhcmFtIHJvbGVzIEFycmF5IG9mIHJvbGVzLlxyXG4gICAgICogQHJldHVybnMgRW1wdHkgcmVzcG9uc2Ugd2hlbiB0aGUgcm9sZSByZW1vdmVkLlxyXG4gICAgICovXHJcbiAgICByZW1vdmVSb2xlcyh1c2VySWQ6IHN0cmluZywgcmVtb3ZlZFJvbGVzOiBJZGVudGl0eVJvbGVNb2RlbFtdKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlclVybCgpICsgJy8nICsgdXNlcklkICsgJy9yb2xlLW1hcHBpbmdzL3JlYWxtJztcclxuICAgICAgICBjb25zdCByZXF1ZXN0ID0gSlNPTi5zdHJpbmdpZnkocmVtb3ZlZFJvbGVzKTtcclxuICAgICAgICBjb25zdCBodHRwTWV0aG9kID0gJ0RFTEVURScsIHBhdGhQYXJhbXMgPSB7fSAsIHF1ZXJ5UGFyYW1zID0ge30sIGJvZHlQYXJhbSA9IHJlcXVlc3QsIGhlYWRlclBhcmFtcyA9IHt9LFxyXG4gICAgICAgIGZvcm1QYXJhbXMgPSB7fSwgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ10sIGFjY2VwdHMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgdXJsLCBodHRwTWV0aG9kLCBwYXRoUGFyYW1zLCBxdWVyeVBhcmFtcyxcclxuICAgICAgICBoZWFkZXJQYXJhbXMsIGZvcm1QYXJhbXMsIGJvZHlQYXJhbSxcclxuICAgICAgICBjb250ZW50VHlwZXMsIGFjY2VwdHMsIG51bGwsIG51bGwsIG51bGxcclxuICAgICAgICApKS5waXBlKFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnJvcikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnJvcikpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGJ1aWxkVXNlclVybCgpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiBgJHt0aGlzLmFwcENvbmZpZ1NlcnZpY2UuZ2V0KCdpZGVudGl0eUhvc3QnKX0vdXNlcnNgO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYnVpbGRVc2VyQ2xpZW50Um9sZU1hcHBpbmcodXNlcklkOiBzdHJpbmcsIGNsaWVudElkOiBzdHJpbmcpOiBhbnkge1xyXG4gICAgICAgIHJldHVybiBgJHt0aGlzLmFwcENvbmZpZ1NlcnZpY2UuZ2V0KCdpZGVudGl0eUhvc3QnKX0vdXNlcnMvJHt1c2VySWR9L3JvbGUtbWFwcGluZ3MvY2xpZW50cy8ke2NsaWVudElkfWA7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBidWlsZFJvbGVzVXJsKHVzZXJJZDogc3RyaW5nKTogYW55IHtcclxuICAgICAgICByZXR1cm4gYCR7dGhpcy5hcHBDb25maWdTZXJ2aWNlLmdldCgnaWRlbnRpdHlIb3N0Jyl9L3VzZXJzLyR7dXNlcklkfS9yb2xlLW1hcHBpbmdzL3JlYWxtL2NvbXBvc2l0ZWA7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBidWlsZEdldENsaWVudHNVcmwoKSB7XHJcbiAgICAgICAgcmV0dXJuIGAke3RoaXMuYXBwQ29uZmlnU2VydmljZS5nZXQoJ2lkZW50aXR5SG9zdCcpfS9jbGllbnRzYDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFRocm93IHRoZSBlcnJvclxyXG4gICAgICogQHBhcmFtIGVycm9yXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgaGFuZGxlRXJyb3IoZXJyb3I6IFJlc3BvbnNlKSB7XHJcbiAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKGVycm9yKTtcclxuICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihlcnJvciB8fCAnU2VydmVyIGVycm9yJyk7XHJcbiAgICB9XHJcbn1cclxuIl19