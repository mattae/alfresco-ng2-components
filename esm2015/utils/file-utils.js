/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @record
 */
export function FileInfo() { }
if (false) {
    /** @type {?|undefined} */
    FileInfo.prototype.entry;
    /** @type {?|undefined} */
    FileInfo.prototype.file;
    /** @type {?|undefined} */
    FileInfo.prototype.relativeFolder;
}
export class FileUtils {
    /**
     * @param {?} folder
     * @return {?}
     */
    static flatten(folder) {
        /** @type {?} */
        const reader = folder.createReader();
        /** @type {?} */
        const files = [];
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        (resolve) => {
            /** @type {?} */
            const iterations = [];
            ((/**
             * @return {?}
             */
            function traverse() {
                reader.readEntries((/**
                 * @param {?} entries
                 * @return {?}
                 */
                (entries) => {
                    if (!entries.length) {
                        Promise.all(iterations).then((/**
                         * @return {?}
                         */
                        () => resolve(files)));
                    }
                    else {
                        iterations.push(Promise.all(entries.map((/**
                         * @param {?} entry
                         * @return {?}
                         */
                        (entry) => {
                            if (entry.isFile) {
                                return new Promise((/**
                                 * @param {?} resolveFile
                                 * @return {?}
                                 */
                                (resolveFile) => {
                                    entry.file((/**
                                     * @param {?} file
                                     * @return {?}
                                     */
                                    function (file) {
                                        files.push({
                                            entry: entry,
                                            file: file,
                                            relativeFolder: entry.fullPath.replace(/\/[^\/]*$/, '')
                                        });
                                        resolveFile();
                                    }));
                                }));
                            }
                            else {
                                return FileUtils.flatten(entry).then((/**
                                 * @param {?} result
                                 * @return {?}
                                 */
                                (result) => {
                                    files.push(...result);
                                }));
                            }
                        }))));
                        // Try calling traverse() again for the same dir, according to spec
                        traverse();
                    }
                }));
            }))();
        }));
    }
    /**
     * @param {?} fileList
     * @return {?}
     */
    static toFileArray(fileList) {
        /** @type {?} */
        const result = [];
        if (fileList && fileList.length > 0) {
            for (let i = 0; i < fileList.length; i++) {
                result.push(fileList[i]);
            }
        }
        return result;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZS11dGlscy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInV0aWxzL2ZpbGUtdXRpbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsOEJBSUM7OztJQUhHLHlCQUFZOztJQUNaLHdCQUFZOztJQUNaLGtDQUF3Qjs7QUFHNUIsTUFBTSxPQUFPLFNBQVM7Ozs7O0lBRWxCLE1BQU0sQ0FBQyxPQUFPLENBQUMsTUFBVzs7Y0FDaEIsTUFBTSxHQUFHLE1BQU0sQ0FBQyxZQUFZLEVBQUU7O2NBQzlCLEtBQUssR0FBZSxFQUFFO1FBQzVCLE9BQU8sSUFBSSxPQUFPOzs7O1FBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTs7a0JBQ3JCLFVBQVUsR0FBRyxFQUFFO1lBQ3JCOzs7WUFBQyxTQUFTLFFBQVE7Z0JBQ2QsTUFBTSxDQUFDLFdBQVc7Ozs7Z0JBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtvQkFDM0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUU7d0JBQ2pCLE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSTs7O3dCQUFDLEdBQUcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBQyxDQUFDO3FCQUN0RDt5QkFBTTt3QkFDSCxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUc7Ozs7d0JBQUMsQ0FBQyxLQUFLLEVBQUUsRUFBRTs0QkFDOUMsSUFBSSxLQUFLLENBQUMsTUFBTSxFQUFFO2dDQUNkLE9BQU8sSUFBSSxPQUFPOzs7O2dDQUFDLENBQUMsV0FBVyxFQUFFLEVBQUU7b0NBQy9CLEtBQUssQ0FBQyxJQUFJOzs7O29DQUFDLFVBQVUsSUFBVTt3Q0FDM0IsS0FBSyxDQUFDLElBQUksQ0FBQzs0Q0FDUCxLQUFLLEVBQUUsS0FBSzs0Q0FDWixJQUFJLEVBQUUsSUFBSTs0Q0FDVixjQUFjLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLEVBQUUsQ0FBQzt5Q0FDMUQsQ0FBQyxDQUFDO3dDQUNILFdBQVcsRUFBRSxDQUFDO29DQUNsQixDQUFDLEVBQUMsQ0FBQztnQ0FDUCxDQUFDLEVBQUMsQ0FBQzs2QkFDTjtpQ0FBTTtnQ0FDSCxPQUFPLFNBQVMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSTs7OztnQ0FBQyxDQUFDLE1BQU0sRUFBRSxFQUFFO29DQUM1QyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUM7Z0NBQzFCLENBQUMsRUFBQyxDQUFDOzZCQUNOO3dCQUNMLENBQUMsRUFBQyxDQUFDLENBQUMsQ0FBQzt3QkFDTCxtRUFBbUU7d0JBQ25FLFFBQVEsRUFBRSxDQUFDO3FCQUNkO2dCQUNMLENBQUMsRUFBQyxDQUFDO1lBQ1AsQ0FBQyxFQUFDLEVBQUUsQ0FBQztRQUNULENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxNQUFNLENBQUMsV0FBVyxDQUFDLFFBQWtCOztjQUMzQixNQUFNLEdBQUcsRUFBRTtRQUVqQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNqQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDdEMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUM1QjtTQUNKO1FBRUQsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQztDQUNKIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmV4cG9ydCBpbnRlcmZhY2UgRmlsZUluZm8ge1xyXG4gICAgZW50cnk/OiBhbnk7XHJcbiAgICBmaWxlPzogRmlsZTtcclxuICAgIHJlbGF0aXZlRm9sZGVyPzogc3RyaW5nO1xyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgRmlsZVV0aWxzIHtcclxuXHJcbiAgICBzdGF0aWMgZmxhdHRlbihmb2xkZXI6IGFueSk6IFByb21pc2U8RmlsZUluZm9bXT4ge1xyXG4gICAgICAgIGNvbnN0IHJlYWRlciA9IGZvbGRlci5jcmVhdGVSZWFkZXIoKTtcclxuICAgICAgICBjb25zdCBmaWxlczogRmlsZUluZm9bXSA9IFtdO1xyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBpdGVyYXRpb25zID0gW107XHJcbiAgICAgICAgICAgIChmdW5jdGlvbiB0cmF2ZXJzZSgpIHtcclxuICAgICAgICAgICAgICAgIHJlYWRlci5yZWFkRW50cmllcygoZW50cmllcykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghZW50cmllcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgUHJvbWlzZS5hbGwoaXRlcmF0aW9ucykudGhlbigoKSA9PiByZXNvbHZlKGZpbGVzKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXRlcmF0aW9ucy5wdXNoKFByb21pc2UuYWxsKGVudHJpZXMubWFwKChlbnRyeSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGVudHJ5LmlzRmlsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZUZpbGUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZW50cnkuZmlsZShmdW5jdGlvbiAoZmlsZTogRmlsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsZXMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZW50cnk6IGVudHJ5LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGU6IGZpbGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmVsYXRpdmVGb2xkZXI6IGVudHJ5LmZ1bGxQYXRoLnJlcGxhY2UoL1xcL1teXFwvXSokLywgJycpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmVGaWxlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gRmlsZVV0aWxzLmZsYXR0ZW4oZW50cnkpLnRoZW4oKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlcy5wdXNoKC4uLnJlc3VsdCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8vIFRyeSBjYWxsaW5nIHRyYXZlcnNlKCkgYWdhaW4gZm9yIHRoZSBzYW1lIGRpciwgYWNjb3JkaW5nIHRvIHNwZWNcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHJhdmVyc2UoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSkoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgdG9GaWxlQXJyYXkoZmlsZUxpc3Q6IEZpbGVMaXN0KTogRmlsZVtdIHtcclxuICAgICAgICBjb25zdCByZXN1bHQgPSBbXTtcclxuXHJcbiAgICAgICAgaWYgKGZpbGVMaXN0ICYmIGZpbGVMaXN0Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBmaWxlTGlzdC5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgcmVzdWx0LnB1c2goZmlsZUxpc3RbaV0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==