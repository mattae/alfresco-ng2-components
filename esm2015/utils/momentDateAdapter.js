/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DateAdapter } from '@angular/material';
import { isMoment } from 'moment';
import moment from 'moment-es6';
export class MomentDateAdapter extends DateAdapter {
    constructor() {
        super(...arguments);
        this.localeData = moment.localeData();
    }
    /**
     * @param {?} date
     * @return {?}
     */
    getYear(date) {
        return date.year();
    }
    /**
     * @param {?} date
     * @return {?}
     */
    getMonth(date) {
        return date.month();
    }
    /**
     * @param {?} date
     * @return {?}
     */
    getDate(date) {
        return date.date();
    }
    /**
     * @param {?} date
     * @return {?}
     */
    getDayOfWeek(date) {
        return date.day();
    }
    /**
     * @param {?} style
     * @return {?}
     */
    getMonthNames(style) {
        switch (style) {
            case 'long':
                return this.localeData.months();
            case 'short':
                return this.localeData.monthsShort();
            case 'narrow':
                return this.localeData.monthsShort().map((/**
                 * @param {?} month
                 * @return {?}
                 */
                (month) => month[0]));
            default:
                return;
        }
    }
    /**
     * @return {?}
     */
    getDateNames() {
        /** @type {?} */
        const dateNames = [];
        for (let date = 1; date <= 31; date++) {
            dateNames.push(String(date));
        }
        return dateNames;
    }
    /**
     * @param {?} style
     * @return {?}
     */
    getDayOfWeekNames(style) {
        switch (style) {
            case 'long':
                return this.localeData.weekdays();
            case 'short':
                return this.localeData.weekdaysShort();
            case 'narrow':
                return this.localeData.weekdaysShort();
            default:
                return;
        }
    }
    /**
     * @param {?} date
     * @return {?}
     */
    getYearName(date) {
        return String(date.year());
    }
    /**
     * @return {?}
     */
    getFirstDayOfWeek() {
        return this.localeData.firstDayOfWeek();
    }
    /**
     * @param {?} date
     * @return {?}
     */
    getNumDaysInMonth(date) {
        return date.daysInMonth();
    }
    /**
     * @param {?} date
     * @return {?}
     */
    clone(date) {
        /** @type {?} */
        const locale = this.locale || 'en';
        return date.clone().locale(locale);
    }
    /**
     * @param {?} year
     * @param {?} month
     * @param {?} date
     * @return {?}
     */
    createDate(year, month, date) {
        return moment([year, month, date]);
    }
    /**
     * @return {?}
     */
    today() {
        /** @type {?} */
        const locale = this.locale || 'en';
        return moment().locale(locale);
    }
    /**
     * @param {?} value
     * @param {?} parseFormat
     * @return {?}
     */
    parse(value, parseFormat) {
        /** @type {?} */
        const locale = this.locale || 'en';
        if (value && typeof value === 'string') {
            /** @type {?} */
            let m = moment(value, parseFormat, locale, true);
            if (!m.isValid()) {
                // use strict parsing because Moment's parser is very forgiving, and this can lead to undesired behavior.
                m = moment(value, this.overrideDisplayFormat, locale, true);
            }
            if (m.isValid()) {
                // if user omits year, it defaults to 2001, so check for that issue.
                if (m.year() === 2001 && value.indexOf('2001') === -1) {
                    // if 2001 not actually in the value string, change to current year
                    /** @type {?} */
                    const currentYear = new Date().getFullYear();
                    m.set('year', currentYear);
                    // if date is in the future, set previous year
                    if (m.isAfter(moment())) {
                        m.set('year', currentYear - 1);
                    }
                }
            }
            return m;
        }
        return value ? moment(value).locale(locale) : null;
    }
    /**
     * @param {?} date
     * @param {?} displayFormat
     * @return {?}
     */
    format(date, displayFormat) {
        date = this.clone(date);
        displayFormat = this.overrideDisplayFormat ? this.overrideDisplayFormat : displayFormat;
        if (date && date.format) {
            return date.format(displayFormat);
        }
        else {
            return '';
        }
    }
    /**
     * @param {?} date
     * @param {?} years
     * @return {?}
     */
    addCalendarYears(date, years) {
        return date.clone().add(years, 'y');
    }
    /**
     * @param {?} date
     * @param {?} months
     * @return {?}
     */
    addCalendarMonths(date, months) {
        return date.clone().add(months, 'M');
    }
    /**
     * @param {?} date
     * @param {?} days
     * @return {?}
     */
    addCalendarDays(date, days) {
        return date.clone().add(days, 'd');
    }
    /**
     * @param {?} date
     * @return {?}
     */
    getISODateString(date) {
        return date.toISOString();
    }
    /**
     * @param {?} locale
     * @return {?}
     */
    setLocale(locale) {
        super.setLocale(locale);
        this.localeData = moment.localeData(locale);
    }
    /**
     * @param {?} first
     * @param {?} second
     * @return {?}
     */
    compareDate(first, second) {
        return first.diff(second, 'seconds', true);
    }
    /**
     * @param {?} first
     * @param {?} second
     * @return {?}
     */
    sameDate(first, second) {
        if (first == null) {
            // same if both null
            return second == null;
        }
        else if (isMoment(first)) {
            return first.isSame(second);
        }
        else {
            /** @type {?} */
            const isSame = super.sameDate(first, second);
            return isSame;
        }
    }
    /**
     * @param {?} date
     * @param {?=} min
     * @param {?=} max
     * @return {?}
     */
    clampDate(date, min, max) {
        if (min && date.isBefore(min)) {
            return min;
        }
        else if (max && date.isAfter(max)) {
            return max;
        }
        else {
            return date;
        }
    }
    /**
     * @param {?} date
     * @return {?}
     */
    isDateInstance(date) {
        /** @type {?} */
        let isValidDateInstance = false;
        if (date) {
            isValidDateInstance = date._isAMomentObject;
        }
        return isValidDateInstance;
    }
    /**
     * @param {?} date
     * @return {?}
     */
    isValid(date) {
        return date.isValid();
    }
    /**
     * @param {?} date
     * @return {?}
     */
    toIso8601(date) {
        return this.clone(date).format();
    }
    /**
     * @param {?} iso8601String
     * @return {?}
     */
    fromIso8601(iso8601String) {
        /** @type {?} */
        const locale = this.locale || 'en';
        /** @type {?} */
        const d = moment(iso8601String, moment.ISO_8601).locale(locale);
        return this.isValid(d) ? d : null;
    }
    /**
     * @return {?}
     */
    invalid() {
        return moment.invalid();
    }
}
if (false) {
    /**
     * @type {?}
     * @private
     */
    MomentDateAdapter.prototype.localeData;
    /** @type {?} */
    MomentDateAdapter.prototype.overrideDisplayFormat;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9tZW50RGF0ZUFkYXB0ZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJ1dGlscy9tb21lbnREYXRlQWRhcHRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDaEQsT0FBTyxFQUFFLFFBQVEsRUFBVSxNQUFNLFFBQVEsQ0FBQztBQUMxQyxPQUFPLE1BQU0sTUFBTSxZQUFZLENBQUM7QUFFaEMsTUFBTSxPQUFPLGlCQUFrQixTQUFRLFdBQW1CO0lBQTFEOztRQUVZLGVBQVUsR0FBUSxNQUFNLENBQUMsVUFBVSxFQUFFLENBQUM7SUFrTWxELENBQUM7Ozs7O0lBOUxHLE9BQU8sQ0FBQyxJQUFZO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3ZCLENBQUM7Ozs7O0lBRUQsUUFBUSxDQUFDLElBQVk7UUFDakIsT0FBTyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDeEIsQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsSUFBWTtRQUNoQixPQUFPLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN2QixDQUFDOzs7OztJQUVELFlBQVksQ0FBQyxJQUFZO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBRUQsYUFBYSxDQUFDLEtBQWtDO1FBQzVDLFFBQVEsS0FBSyxFQUFFO1lBQ1gsS0FBSyxNQUFNO2dCQUNQLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNwQyxLQUFLLE9BQU87Z0JBQ1IsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3pDLEtBQUssUUFBUTtnQkFDVCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRzs7OztnQkFBQyxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFDLENBQUM7WUFDbEU7Z0JBQ0ksT0FBTztTQUNkO0lBQ0wsQ0FBQzs7OztJQUVELFlBQVk7O2NBQ0YsU0FBUyxHQUFhLEVBQUU7UUFDOUIsS0FBSyxJQUFJLElBQUksR0FBRyxDQUFDLEVBQUUsSUFBSSxJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRTtZQUNuQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1NBQ2hDO1FBRUQsT0FBTyxTQUFTLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCxpQkFBaUIsQ0FBQyxLQUFrQztRQUNoRCxRQUFRLEtBQUssRUFBRTtZQUNYLEtBQUssTUFBTTtnQkFDUCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDdEMsS0FBSyxPQUFPO2dCQUNSLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUMzQyxLQUFLLFFBQVE7Z0JBQ1QsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQzNDO2dCQUNJLE9BQU87U0FDZDtJQUNMLENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLElBQVk7UUFDcEIsT0FBTyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUM7SUFDL0IsQ0FBQzs7OztJQUVELGlCQUFpQjtRQUNiLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUM1QyxDQUFDOzs7OztJQUVELGlCQUFpQixDQUFDLElBQVk7UUFDMUIsT0FBTyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDOUIsQ0FBQzs7Ozs7SUFFRCxLQUFLLENBQUMsSUFBWTs7Y0FDUixNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJO1FBQ2xDLE9BQU8sSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUN2QyxDQUFDOzs7Ozs7O0lBRUQsVUFBVSxDQUFDLElBQVksRUFBRSxLQUFhLEVBQUUsSUFBWTtRQUNoRCxPQUFPLE1BQU0sQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLENBQUMsQ0FBQztJQUN2QyxDQUFDOzs7O0lBRUQsS0FBSzs7Y0FDSyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJO1FBQ2xDLE9BQU8sTUFBTSxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7OztJQUVELEtBQUssQ0FBQyxLQUFVLEVBQUUsV0FBZ0I7O2NBQ3hCLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUk7UUFFbEMsSUFBSSxLQUFLLElBQUksT0FBTyxLQUFLLEtBQUssUUFBUSxFQUFFOztnQkFDaEMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxLQUFLLEVBQUUsV0FBVyxFQUFFLE1BQU0sRUFBRSxJQUFJLENBQUM7WUFDaEQsSUFBSSxDQUFDLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFDZCx5R0FBeUc7Z0JBQ3pHLENBQUMsR0FBRyxNQUFNLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDL0Q7WUFDRCxJQUFJLENBQUMsQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFDYixvRUFBb0U7Z0JBQ3BFLElBQUksQ0FBQyxDQUFDLElBQUksRUFBRSxLQUFLLElBQUksSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFOzs7MEJBRTdDLFdBQVcsR0FBRyxJQUFJLElBQUksRUFBRSxDQUFDLFdBQVcsRUFBRTtvQkFDNUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsV0FBVyxDQUFDLENBQUM7b0JBQzNCLDhDQUE4QztvQkFDOUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUU7d0JBQ3JCLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLFdBQVcsR0FBRyxDQUFDLENBQUMsQ0FBQztxQkFDbEM7aUJBQ0o7YUFDSjtZQUNELE9BQU8sQ0FBQyxDQUFDO1NBQ1o7UUFFRCxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQ3ZELENBQUM7Ozs7OztJQUVELE1BQU0sQ0FBQyxJQUFZLEVBQUUsYUFBa0I7UUFDbkMsSUFBSSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEIsYUFBYSxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxhQUFhLENBQUM7UUFFeEYsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNyQixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLENBQUM7U0FDckM7YUFBTTtZQUNILE9BQU8sRUFBRSxDQUFDO1NBQ2I7SUFDTCxDQUFDOzs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxJQUFZLEVBQUUsS0FBYTtRQUN4QyxPQUFPLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxHQUFHLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQ3hDLENBQUM7Ozs7OztJQUVELGlCQUFpQixDQUFDLElBQVksRUFBRSxNQUFjO1FBQzFDLE9BQU8sSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDekMsQ0FBQzs7Ozs7O0lBRUQsZUFBZSxDQUFDLElBQVksRUFBRSxJQUFZO1FBQ3RDLE9BQU8sSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLEdBQUcsQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDdkMsQ0FBQzs7Ozs7SUFFRCxnQkFBZ0IsQ0FBQyxJQUFZO1FBQ3pCLE9BQU8sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQzlCLENBQUM7Ozs7O0lBRUQsU0FBUyxDQUFDLE1BQVc7UUFDakIsS0FBSyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUV4QixJQUFJLENBQUMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDaEQsQ0FBQzs7Ozs7O0lBRUQsV0FBVyxDQUFDLEtBQWEsRUFBRSxNQUFjO1FBQ3JDLE9BQU8sS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQy9DLENBQUM7Ozs7OztJQUVELFFBQVEsQ0FBQyxLQUFtQixFQUFFLE1BQW9CO1FBQzlDLElBQUksS0FBSyxJQUFJLElBQUksRUFBRTtZQUNmLG9CQUFvQjtZQUNwQixPQUFPLE1BQU0sSUFBSSxJQUFJLENBQUM7U0FDekI7YUFBTSxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsRUFBRTtZQUN4QixPQUFPLEtBQUssQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDL0I7YUFBTTs7a0JBQ0csTUFBTSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQztZQUM1QyxPQUFPLE1BQU0sQ0FBQztTQUNqQjtJQUNMLENBQUM7Ozs7Ozs7SUFFRCxTQUFTLENBQUMsSUFBWSxFQUFFLEdBQWtCLEVBQUUsR0FBa0I7UUFDMUQsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUMzQixPQUFPLEdBQUcsQ0FBQztTQUNkO2FBQU0sSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUNqQyxPQUFPLEdBQUcsQ0FBQztTQUNkO2FBQU07WUFDSCxPQUFPLElBQUksQ0FBQztTQUNmO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxjQUFjLENBQUMsSUFBUzs7WUFDaEIsbUJBQW1CLEdBQUcsS0FBSztRQUUvQixJQUFJLElBQUksRUFBRTtZQUNOLG1CQUFtQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztTQUMvQztRQUVELE9BQU8sbUJBQW1CLENBQUM7SUFDL0IsQ0FBQzs7Ozs7SUFFRCxPQUFPLENBQUMsSUFBWTtRQUNoQixPQUFPLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUMxQixDQUFDOzs7OztJQUVELFNBQVMsQ0FBQyxJQUFZO1FBQ2xCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztJQUNyQyxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxhQUFxQjs7Y0FDdkIsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSTs7Y0FDNUIsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxhQUFhLEVBQUUsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUM7UUFDL0QsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUN0QyxDQUFDOzs7O0lBRUQsT0FBTztRQUNILE9BQU8sTUFBTSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQzVCLENBQUM7Q0FDSjs7Ozs7O0lBbE1HLHVDQUE4Qzs7SUFFOUMsa0RBQThCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IERhdGVBZGFwdGVyIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBpc01vbWVudCwgTW9tZW50IH0gZnJvbSAnbW9tZW50JztcclxuaW1wb3J0IG1vbWVudCBmcm9tICdtb21lbnQtZXM2JztcclxuXHJcbmV4cG9ydCBjbGFzcyBNb21lbnREYXRlQWRhcHRlciBleHRlbmRzIERhdGVBZGFwdGVyPE1vbWVudD4ge1xyXG5cclxuICAgIHByaXZhdGUgbG9jYWxlRGF0YTogYW55ID0gbW9tZW50LmxvY2FsZURhdGEoKTtcclxuXHJcbiAgICBvdmVycmlkZURpc3BsYXlGb3JtYXQ6IHN0cmluZztcclxuXHJcbiAgICBnZXRZZWFyKGRhdGU6IE1vbWVudCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUueWVhcigpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE1vbnRoKGRhdGU6IE1vbWVudCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUubW9udGgoKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREYXRlKGRhdGU6IE1vbWVudCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUuZGF0ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldERheU9mV2VlayhkYXRlOiBNb21lbnQpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiBkYXRlLmRheSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE1vbnRoTmFtZXMoc3R5bGU6ICdsb25nJyB8ICdzaG9ydCcgfCAnbmFycm93Jyk6IHN0cmluZ1tdIHtcclxuICAgICAgICBzd2l0Y2ggKHN0eWxlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ2xvbmcnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxlRGF0YS5tb250aHMoKTtcclxuICAgICAgICAgICAgY2FzZSAnc2hvcnQnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxlRGF0YS5tb250aHNTaG9ydCgpO1xyXG4gICAgICAgICAgICBjYXNlICduYXJyb3cnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxlRGF0YS5tb250aHNTaG9ydCgpLm1hcCgobW9udGgpID0+IG1vbnRoWzBdKTtcclxuICAgICAgICAgICAgZGVmYXVsdCA6XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldERhdGVOYW1lcygpOiBzdHJpbmdbXSB7XHJcbiAgICAgICAgY29uc3QgZGF0ZU5hbWVzOiBzdHJpbmdbXSA9IFtdO1xyXG4gICAgICAgIGZvciAobGV0IGRhdGUgPSAxOyBkYXRlIDw9IDMxOyBkYXRlKyspIHtcclxuICAgICAgICAgICAgZGF0ZU5hbWVzLnB1c2goU3RyaW5nKGRhdGUpKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBkYXRlTmFtZXM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGF5T2ZXZWVrTmFtZXMoc3R5bGU6ICdsb25nJyB8ICdzaG9ydCcgfCAnbmFycm93Jyk6IHN0cmluZ1tdIHtcclxuICAgICAgICBzd2l0Y2ggKHN0eWxlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ2xvbmcnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxlRGF0YS53ZWVrZGF5cygpO1xyXG4gICAgICAgICAgICBjYXNlICdzaG9ydCc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5sb2NhbGVEYXRhLndlZWtkYXlzU2hvcnQoKTtcclxuICAgICAgICAgICAgY2FzZSAnbmFycm93JzpcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmxvY2FsZURhdGEud2Vla2RheXNTaG9ydCgpO1xyXG4gICAgICAgICAgICBkZWZhdWx0IDpcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0WWVhck5hbWUoZGF0ZTogTW9tZW50KTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gU3RyaW5nKGRhdGUueWVhcigpKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRGaXJzdERheU9mV2VlaygpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmxvY2FsZURhdGEuZmlyc3REYXlPZldlZWsoKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXROdW1EYXlzSW5Nb250aChkYXRlOiBNb21lbnQpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiBkYXRlLmRheXNJbk1vbnRoKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2xvbmUoZGF0ZTogTW9tZW50KTogTW9tZW50IHtcclxuICAgICAgICBjb25zdCBsb2NhbGUgPSB0aGlzLmxvY2FsZSB8fCAnZW4nO1xyXG4gICAgICAgIHJldHVybiBkYXRlLmNsb25lKCkubG9jYWxlKGxvY2FsZSk7XHJcbiAgICB9XHJcblxyXG4gICAgY3JlYXRlRGF0ZSh5ZWFyOiBudW1iZXIsIG1vbnRoOiBudW1iZXIsIGRhdGU6IG51bWJlcik6IE1vbWVudCB7XHJcbiAgICAgICAgcmV0dXJuIG1vbWVudChbeWVhciwgbW9udGgsIGRhdGVdKTtcclxuICAgIH1cclxuXHJcbiAgICB0b2RheSgpOiBNb21lbnQge1xyXG4gICAgICAgIGNvbnN0IGxvY2FsZSA9IHRoaXMubG9jYWxlIHx8ICdlbic7XHJcbiAgICAgICAgcmV0dXJuIG1vbWVudCgpLmxvY2FsZShsb2NhbGUpO1xyXG4gICAgfVxyXG5cclxuICAgIHBhcnNlKHZhbHVlOiBhbnksIHBhcnNlRm9ybWF0OiBhbnkpOiBNb21lbnQge1xyXG4gICAgICAgIGNvbnN0IGxvY2FsZSA9IHRoaXMubG9jYWxlIHx8ICdlbic7XHJcblxyXG4gICAgICAgIGlmICh2YWx1ZSAmJiB0eXBlb2YgdmFsdWUgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgIGxldCBtID0gbW9tZW50KHZhbHVlLCBwYXJzZUZvcm1hdCwgbG9jYWxlLCB0cnVlKTtcclxuICAgICAgICAgICAgaWYgKCFtLmlzVmFsaWQoKSkge1xyXG4gICAgICAgICAgICAgICAgLy8gdXNlIHN0cmljdCBwYXJzaW5nIGJlY2F1c2UgTW9tZW50J3MgcGFyc2VyIGlzIHZlcnkgZm9yZ2l2aW5nLCBhbmQgdGhpcyBjYW4gbGVhZCB0byB1bmRlc2lyZWQgYmVoYXZpb3IuXHJcbiAgICAgICAgICAgICAgICBtID0gbW9tZW50KHZhbHVlLCB0aGlzLm92ZXJyaWRlRGlzcGxheUZvcm1hdCwgbG9jYWxlLCB0cnVlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAobS5pc1ZhbGlkKCkpIHtcclxuICAgICAgICAgICAgICAgIC8vIGlmIHVzZXIgb21pdHMgeWVhciwgaXQgZGVmYXVsdHMgdG8gMjAwMSwgc28gY2hlY2sgZm9yIHRoYXQgaXNzdWUuXHJcbiAgICAgICAgICAgICAgICBpZiAobS55ZWFyKCkgPT09IDIwMDEgJiYgdmFsdWUuaW5kZXhPZignMjAwMScpID09PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIGlmIDIwMDEgbm90IGFjdHVhbGx5IGluIHRoZSB2YWx1ZSBzdHJpbmcsIGNoYW5nZSB0byBjdXJyZW50IHllYXJcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBjdXJyZW50WWVhciA9IG5ldyBEYXRlKCkuZ2V0RnVsbFllYXIoKTtcclxuICAgICAgICAgICAgICAgICAgICBtLnNldCgneWVhcicsIGN1cnJlbnRZZWFyKTtcclxuICAgICAgICAgICAgICAgICAgICAvLyBpZiBkYXRlIGlzIGluIHRoZSBmdXR1cmUsIHNldCBwcmV2aW91cyB5ZWFyXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKG0uaXNBZnRlcihtb21lbnQoKSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbS5zZXQoJ3llYXInLCBjdXJyZW50WWVhciAtIDEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gbTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB2YWx1ZSA/IG1vbWVudCh2YWx1ZSkubG9jYWxlKGxvY2FsZSkgOiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIGZvcm1hdChkYXRlOiBNb21lbnQsIGRpc3BsYXlGb3JtYXQ6IGFueSk6IHN0cmluZyB7XHJcbiAgICAgICAgZGF0ZSA9IHRoaXMuY2xvbmUoZGF0ZSk7XHJcbiAgICAgICAgZGlzcGxheUZvcm1hdCA9IHRoaXMub3ZlcnJpZGVEaXNwbGF5Rm9ybWF0ID8gdGhpcy5vdmVycmlkZURpc3BsYXlGb3JtYXQgOiBkaXNwbGF5Rm9ybWF0O1xyXG5cclxuICAgICAgICBpZiAoZGF0ZSAmJiBkYXRlLmZvcm1hdCkge1xyXG4gICAgICAgICAgICByZXR1cm4gZGF0ZS5mb3JtYXQoZGlzcGxheUZvcm1hdCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhZGRDYWxlbmRhclllYXJzKGRhdGU6IE1vbWVudCwgeWVhcnM6IG51bWJlcik6IE1vbWVudCB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUuY2xvbmUoKS5hZGQoeWVhcnMsICd5Jyk7XHJcbiAgICB9XHJcblxyXG4gICAgYWRkQ2FsZW5kYXJNb250aHMoZGF0ZTogTW9tZW50LCBtb250aHM6IG51bWJlcik6IE1vbWVudCB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUuY2xvbmUoKS5hZGQobW9udGhzLCAnTScpO1xyXG4gICAgfVxyXG5cclxuICAgIGFkZENhbGVuZGFyRGF5cyhkYXRlOiBNb21lbnQsIGRheXM6IG51bWJlcik6IE1vbWVudCB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUuY2xvbmUoKS5hZGQoZGF5cywgJ2QnKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRJU09EYXRlU3RyaW5nKGRhdGU6IE1vbWVudCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUudG9JU09TdHJpbmcoKTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRMb2NhbGUobG9jYWxlOiBhbnkpOiB2b2lkIHtcclxuICAgICAgICBzdXBlci5zZXRMb2NhbGUobG9jYWxlKTtcclxuXHJcbiAgICAgICAgdGhpcy5sb2NhbGVEYXRhID0gbW9tZW50LmxvY2FsZURhdGEobG9jYWxlKTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wYXJlRGF0ZShmaXJzdDogTW9tZW50LCBzZWNvbmQ6IE1vbWVudCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIGZpcnN0LmRpZmYoc2Vjb25kLCAnc2Vjb25kcycsIHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHNhbWVEYXRlKGZpcnN0OiBhbnkgfCBNb21lbnQsIHNlY29uZDogYW55IHwgTW9tZW50KTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKGZpcnN0ID09IG51bGwpIHtcclxuICAgICAgICAgICAgLy8gc2FtZSBpZiBib3RoIG51bGxcclxuICAgICAgICAgICAgcmV0dXJuIHNlY29uZCA9PSBudWxsO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaXNNb21lbnQoZmlyc3QpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmaXJzdC5pc1NhbWUoc2Vjb25kKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBpc1NhbWUgPSBzdXBlci5zYW1lRGF0ZShmaXJzdCwgc2Vjb25kKTtcclxuICAgICAgICAgICAgcmV0dXJuIGlzU2FtZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY2xhbXBEYXRlKGRhdGU6IE1vbWVudCwgbWluPzogYW55IHwgTW9tZW50LCBtYXg/OiBhbnkgfCBNb21lbnQpOiBNb21lbnQge1xyXG4gICAgICAgIGlmIChtaW4gJiYgZGF0ZS5pc0JlZm9yZShtaW4pKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBtaW47XHJcbiAgICAgICAgfSBlbHNlIGlmIChtYXggJiYgZGF0ZS5pc0FmdGVyKG1heCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG1heDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gZGF0ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaXNEYXRlSW5zdGFuY2UoZGF0ZTogYW55KSB7XHJcbiAgICAgICAgbGV0IGlzVmFsaWREYXRlSW5zdGFuY2UgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgaWYgKGRhdGUpIHtcclxuICAgICAgICAgICAgaXNWYWxpZERhdGVJbnN0YW5jZSA9IGRhdGUuX2lzQU1vbWVudE9iamVjdDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBpc1ZhbGlkRGF0ZUluc3RhbmNlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzVmFsaWQoZGF0ZTogTW9tZW50KTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUuaXNWYWxpZCgpO1xyXG4gICAgfVxyXG5cclxuICAgIHRvSXNvODYwMShkYXRlOiBNb21lbnQpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNsb25lKGRhdGUpLmZvcm1hdCgpO1xyXG4gICAgfVxyXG5cclxuICAgIGZyb21Jc284NjAxKGlzbzg2MDFTdHJpbmc6IHN0cmluZyk6IE1vbWVudCB8IG51bGwge1xyXG4gICAgICAgIGNvbnN0IGxvY2FsZSA9IHRoaXMubG9jYWxlIHx8ICdlbic7XHJcbiAgICAgICAgY29uc3QgZCA9IG1vbWVudChpc284NjAxU3RyaW5nLCBtb21lbnQuSVNPXzg2MDEpLmxvY2FsZShsb2NhbGUpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQoZCkgPyBkIDogbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBpbnZhbGlkKCk6IE1vbWVudCB7XHJcbiAgICAgICAgcmV0dXJuIG1vbWVudC5pbnZhbGlkKCk7XHJcbiAgICB9XHJcbn1cclxuIl19