/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewEncapsulation, ElementRef } from '@angular/core';
import { ContentService } from '../../services/content.service';
import { AppConfigService } from './../../app-config/app-config.service';
import { DomSanitizer } from '@angular/platform-browser';
export class ImgViewerComponent {
    /**
     * @param {?} sanitizer
     * @param {?} appConfigService
     * @param {?} contentService
     * @param {?} el
     */
    constructor(sanitizer, appConfigService, contentService, el) {
        this.sanitizer = sanitizer;
        this.appConfigService = appConfigService;
        this.contentService = contentService;
        this.el = el;
        this.showToolbar = true;
        this.rotate = 0;
        this.scaleX = 1.0;
        this.scaleY = 1.0;
        this.offsetX = 0;
        this.offsetY = 0;
        this.isDragged = false;
        this.drag = { x: 0, y: 0 };
        this.delta = { x: 0, y: 0 };
        this.initializeScaling();
    }
    /**
     * @return {?}
     */
    get transform() {
        return this.sanitizer.bypassSecurityTrustStyle(`scale(${this.scaleX}, ${this.scaleY}) rotate(${this.rotate}deg) translate(${this.offsetX}px, ${this.offsetY}px)`);
    }
    /**
     * @return {?}
     */
    get currentScaleText() {
        return Math.round(this.scaleX * 100) + '%';
    }
    /**
     * @return {?}
     */
    initializeScaling() {
        /** @type {?} */
        const scaling = this.appConfigService.get('adf-viewer.image-viewer-scaling', undefined) / 100;
        if (scaling) {
            this.scaleX = scaling;
            this.scaleY = scaling;
        }
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.element = (/** @type {?} */ (this.el.nativeElement.querySelector('#viewer-image')));
        if (this.element) {
            this.element.addEventListener('mousedown', this.onMouseDown.bind(this));
            this.element.addEventListener('mouseup', this.onMouseUp.bind(this));
            this.element.addEventListener('mouseleave', this.onMouseLeave.bind(this));
            this.element.addEventListener('mouseout', this.onMouseOut.bind(this));
            this.element.addEventListener('mousemove', this.onMouseMove.bind(this));
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.element) {
            this.element.removeEventListener('mousedown', this.onMouseDown);
            this.element.removeEventListener('mouseup', this.onMouseUp);
            this.element.removeEventListener('mouseleave', this.onMouseLeave);
            this.element.removeEventListener('mouseout', this.onMouseOut);
            this.element.removeEventListener('mousemove', this.onMouseMove);
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onMouseDown(event) {
        event.preventDefault();
        this.isDragged = true;
        this.drag = { x: event.pageX, y: event.pageY };
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onMouseMove(event) {
        if (this.isDragged) {
            event.preventDefault();
            this.delta.x = event.pageX - this.drag.x;
            this.delta.y = event.pageY - this.drag.y;
            this.drag.x = event.pageX;
            this.drag.y = event.pageY;
            /** @type {?} */
            const scaleX = (this.scaleX !== 0 ? this.scaleX : 1.0);
            /** @type {?} */
            const scaleY = (this.scaleY !== 0 ? this.scaleY : 1.0);
            this.offsetX += (this.delta.x / scaleX);
            this.offsetY += (this.delta.y / scaleY);
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onMouseUp(event) {
        if (this.isDragged) {
            event.preventDefault();
            this.isDragged = false;
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onMouseLeave(event) {
        if (this.isDragged) {
            event.preventDefault();
            this.isDragged = false;
        }
    }
    /**
     * @param {?} event
     * @return {?}
     */
    onMouseOut(event) {
        if (this.isDragged) {
            event.preventDefault();
            this.isDragged = false;
        }
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        /** @type {?} */
        const blobFile = changes['blobFile'];
        if (blobFile && blobFile.currentValue) {
            this.urlFile = this.contentService.createTrustedUrl(this.blobFile);
            return;
        }
        if (!this.urlFile && !this.blobFile) {
            throw new Error('Attribute urlFile or blobFile is required');
        }
    }
    /**
     * @return {?}
     */
    zoomIn() {
        /** @type {?} */
        const ratio = +((this.scaleX + 0.2).toFixed(1));
        this.scaleX = this.scaleY = ratio;
    }
    /**
     * @return {?}
     */
    zoomOut() {
        /** @type {?} */
        let ratio = +((this.scaleX - 0.2).toFixed(1));
        if (ratio < 0.2) {
            ratio = 0.2;
        }
        this.scaleX = this.scaleY = ratio;
    }
    /**
     * @return {?}
     */
    rotateLeft() {
        /** @type {?} */
        const angle = this.rotate - 90;
        this.rotate = Math.abs(angle) < 360 ? angle : 0;
    }
    /**
     * @return {?}
     */
    rotateRight() {
        /** @type {?} */
        const angle = this.rotate + 90;
        this.rotate = Math.abs(angle) < 360 ? angle : 0;
    }
    /**
     * @return {?}
     */
    reset() {
        this.rotate = 0;
        this.scaleX = 1.0;
        this.scaleY = 1.0;
        this.offsetX = 0;
        this.offsetY = 0;
    }
}
ImgViewerComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-img-viewer',
                template: "<div id=\"adf-image-container\"  class=\"adf-image-container\" tabindex=\"0\" role=\"img\" [attr.aria-label]=\"nameFile\" [style.transform]=\"transform\" data-automation-id=\"adf-image-container\">\r\n    <img id=\"viewer-image\" [src]=\"urlFile\" [alt]=\"nameFile\" [ngStyle]=\"{ 'cursor' : isDragged ? 'move': 'default' } \" />\r\n</div>\r\n\r\n<div class=\"adf-image-viewer__toolbar\" *ngIf=\"showToolbar\">\r\n    <adf-toolbar>\r\n        <button\r\n            id=\"viewer-zoom-in-button\"\r\n            mat-icon-button\r\n            title=\"{{ 'ADF_VIEWER.ARIA.ZOOM_IN' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.ZOOM_IN' | translate }}\"\r\n            (click)=\"zoomIn()\">\r\n            <mat-icon>zoom_in</mat-icon>\r\n        </button>\r\n\r\n        <button\r\n            id=\"viewer-zoom-out-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.ZOOM_OUT' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.ZOOM_OUT' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"zoomOut()\">\r\n            <mat-icon>zoom_out</mat-icon>\r\n        </button>\r\n\r\n        <div class=\"adf-viewer__toolbar-page-scale\" data-automation-id=\"adf-page-scale\">\r\n            {{ currentScaleText }}\r\n        </div>\r\n\r\n        <button\r\n            id=\"viewer-rotate-left-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.ROTATE_LEFT' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.ROTATE_LEFT' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"rotateLeft()\">\r\n            <mat-icon>rotate_left</mat-icon>\r\n        </button>\r\n\r\n        <button\r\n            id=\"viewer-rotate-right-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.ROTATE_RIGHT' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.ROTATE_RIGHT' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"rotateRight()\">\r\n            <mat-icon>rotate_right</mat-icon>\r\n        </button>\r\n\r\n        <button\r\n            id=\"viewer-reset-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.RESET' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.RESET' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"reset()\">\r\n            <mat-icon>zoom_out_map</mat-icon>\r\n        </button>\r\n    </adf-toolbar>\r\n</div>\r\n",
                host: { 'class': 'adf-image-viewer' },
                encapsulation: ViewEncapsulation.None,
                styles: [""]
            }] }
];
/** @nocollapse */
ImgViewerComponent.ctorParameters = () => [
    { type: DomSanitizer },
    { type: AppConfigService },
    { type: ContentService },
    { type: ElementRef }
];
ImgViewerComponent.propDecorators = {
    showToolbar: [{ type: Input }],
    urlFile: [{ type: Input }],
    blobFile: [{ type: Input }],
    nameFile: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    ImgViewerComponent.prototype.showToolbar;
    /** @type {?} */
    ImgViewerComponent.prototype.urlFile;
    /** @type {?} */
    ImgViewerComponent.prototype.blobFile;
    /** @type {?} */
    ImgViewerComponent.prototype.nameFile;
    /** @type {?} */
    ImgViewerComponent.prototype.rotate;
    /** @type {?} */
    ImgViewerComponent.prototype.scaleX;
    /** @type {?} */
    ImgViewerComponent.prototype.scaleY;
    /** @type {?} */
    ImgViewerComponent.prototype.offsetX;
    /** @type {?} */
    ImgViewerComponent.prototype.offsetY;
    /** @type {?} */
    ImgViewerComponent.prototype.isDragged;
    /**
     * @type {?}
     * @private
     */
    ImgViewerComponent.prototype.drag;
    /**
     * @type {?}
     * @private
     */
    ImgViewerComponent.prototype.delta;
    /**
     * @type {?}
     * @private
     */
    ImgViewerComponent.prototype.element;
    /**
     * @type {?}
     * @private
     */
    ImgViewerComponent.prototype.sanitizer;
    /**
     * @type {?}
     * @private
     */
    ImgViewerComponent.prototype.appConfigService;
    /**
     * @type {?}
     * @private
     */
    ImgViewerComponent.prototype.contentService;
    /**
     * @type {?}
     * @private
     */
    ImgViewerComponent.prototype.el;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1nVmlld2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInZpZXdlci9jb21wb25lbnRzL2ltZ1ZpZXdlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUNILFNBQVMsRUFDVCxLQUFLLEVBR0wsaUJBQWlCLEVBQ2pCLFVBQVUsRUFHYixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDaEUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDekUsT0FBTyxFQUFFLFlBQVksRUFBYSxNQUFNLDJCQUEyQixDQUFDO0FBU3BFLE1BQU0sT0FBTyxrQkFBa0I7Ozs7Ozs7SUFrQzNCLFlBQ1ksU0FBdUIsRUFDdkIsZ0JBQWtDLEVBQ2xDLGNBQThCLEVBQzlCLEVBQWM7UUFIZCxjQUFTLEdBQVQsU0FBUyxDQUFjO1FBQ3ZCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLE9BQUUsR0FBRixFQUFFLENBQVk7UUFuQzFCLGdCQUFXLEdBQUcsSUFBSSxDQUFDO1FBV25CLFdBQU0sR0FBVyxDQUFDLENBQUM7UUFDbkIsV0FBTSxHQUFXLEdBQUcsQ0FBQztRQUNyQixXQUFNLEdBQVcsR0FBRyxDQUFDO1FBQ3JCLFlBQU8sR0FBVyxDQUFDLENBQUM7UUFDcEIsWUFBTyxHQUFXLENBQUMsQ0FBQztRQUNwQixjQUFTLEdBQVksS0FBSyxDQUFDO1FBRW5CLFNBQUksR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDO1FBQ3RCLFVBQUssR0FBRyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDO1FBaUIzQixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUM3QixDQUFDOzs7O0lBaEJELElBQUksU0FBUztRQUNULE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyx3QkFBd0IsQ0FBQyxTQUFTLElBQUksQ0FBQyxNQUFNLEtBQUssSUFBSSxDQUFDLE1BQU0sWUFBWSxJQUFJLENBQUMsTUFBTSxrQkFBa0IsSUFBSSxDQUFDLE9BQU8sT0FBTyxJQUFJLENBQUMsT0FBTyxLQUFLLENBQUMsQ0FBQztJQUN0SyxDQUFDOzs7O0lBRUQsSUFBSSxnQkFBZ0I7UUFDaEIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLEdBQUcsR0FBRyxDQUFDO0lBQy9DLENBQUM7Ozs7SUFZRCxpQkFBaUI7O2NBQ1AsT0FBTyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQVMsaUNBQWlDLEVBQUUsU0FBUyxDQUFDLEdBQUcsR0FBRztRQUNyRyxJQUFJLE9BQU8sRUFBRTtZQUNULElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDO1NBQ3pCO0lBQ0wsQ0FBQzs7OztJQUVELFFBQVE7UUFDSixJQUFJLENBQUMsT0FBTyxHQUFHLG1CQUFjLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsRUFBQSxDQUFDO1FBRWxGLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNkLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDeEUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNwRSxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzFFLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDdEUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUMzRTtJQUNMLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ2hFLElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUM1RCxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7WUFDbEUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQzlELElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztTQUNuRTtJQUNMLENBQUM7Ozs7O0lBRUQsV0FBVyxDQUFDLEtBQWlCO1FBQ3pCLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztRQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQyxFQUFFLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQyxFQUFFLEtBQUssQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNuRCxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxLQUFpQjtRQUN6QixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDaEIsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBRXZCLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDekMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUV6QyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBQzFCLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7O2tCQUVwQixNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDOztrQkFDaEQsTUFBTSxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQztZQUV0RCxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUM7WUFDeEMsSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxDQUFDO1NBQzNDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxTQUFTLENBQUMsS0FBaUI7UUFDdkIsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztTQUMxQjtJQUNMLENBQUM7Ozs7O0lBRUQsWUFBWSxDQUFDLEtBQWlCO1FBQzFCLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7U0FDMUI7SUFDTCxDQUFDOzs7OztJQUVELFVBQVUsQ0FBQyxLQUFpQjtRQUN4QixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDaEIsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1NBQzFCO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsT0FBc0I7O2NBQ3hCLFFBQVEsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDO1FBQ3BDLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxZQUFZLEVBQUU7WUFDbkMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNuRSxPQUFPO1NBQ1Y7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakMsTUFBTSxJQUFJLEtBQUssQ0FBQywyQ0FBMkMsQ0FBQyxDQUFDO1NBQ2hFO0lBQ0wsQ0FBQzs7OztJQUVELE1BQU07O2NBQ0ksS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQy9DLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDdEMsQ0FBQzs7OztJQUVELE9BQU87O1lBQ0MsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdDLElBQUksS0FBSyxHQUFHLEdBQUcsRUFBRTtZQUNiLEtBQUssR0FBRyxHQUFHLENBQUM7U0FDZjtRQUNELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDdEMsQ0FBQzs7OztJQUVELFVBQVU7O2NBQ0EsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRTtRQUM5QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNwRCxDQUFDOzs7O0lBRUQsV0FBVzs7Y0FDRCxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFO1FBQzlCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3BELENBQUM7Ozs7SUFFRCxLQUFLO1FBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7UUFDbEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7UUFDbEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDakIsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7SUFDckIsQ0FBQzs7O1lBcEtKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsZ0JBQWdCO2dCQUMxQixtM0VBQXlDO2dCQUV6QyxJQUFJLEVBQUUsRUFBRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUU7Z0JBQ3JDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOzthQUN4Qzs7OztZQVJRLFlBQVk7WUFEWixnQkFBZ0I7WUFEaEIsY0FBYztZQUpuQixVQUFVOzs7MEJBaUJULEtBQUs7c0JBR0wsS0FBSzt1QkFHTCxLQUFLO3VCQUdMLEtBQUs7Ozs7SUFUTix5Q0FDbUI7O0lBRW5CLHFDQUNnQjs7SUFFaEIsc0NBQ2U7O0lBRWYsc0NBQ2lCOztJQUVqQixvQ0FBbUI7O0lBQ25CLG9DQUFxQjs7SUFDckIsb0NBQXFCOztJQUNyQixxQ0FBb0I7O0lBQ3BCLHFDQUFvQjs7SUFDcEIsdUNBQTJCOzs7OztJQUUzQixrQ0FBOEI7Ozs7O0lBQzlCLG1DQUErQjs7Ozs7SUFVL0IscUNBQTZCOzs7OztJQUd6Qix1Q0FBK0I7Ozs7O0lBQy9CLDhDQUEwQzs7Ozs7SUFDMUMsNENBQXNDOzs7OztJQUN0QyxnQ0FBc0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHtcclxuICAgIENvbXBvbmVudCxcclxuICAgIElucHV0LFxyXG4gICAgT25DaGFuZ2VzLFxyXG4gICAgU2ltcGxlQ2hhbmdlcyxcclxuICAgIFZpZXdFbmNhcHN1bGF0aW9uLFxyXG4gICAgRWxlbWVudFJlZixcclxuICAgIE9uSW5pdCxcclxuICAgIE9uRGVzdHJveVxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb250ZW50U2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2NvbnRlbnQuc2VydmljZSc7XHJcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLy4uLy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRG9tU2FuaXRpemVyLCBTYWZlU3R5bGUgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtaW1nLXZpZXdlcicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vaW1nVmlld2VyLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2ltZ1ZpZXdlci5jb21wb25lbnQuc2NzcyddLFxyXG4gICAgaG9zdDogeyAnY2xhc3MnOiAnYWRmLWltYWdlLXZpZXdlcicgfSxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIEltZ1ZpZXdlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzLCBPbkRlc3Ryb3kge1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzaG93VG9vbGJhciA9IHRydWU7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHVybEZpbGU6IHN0cmluZztcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgYmxvYkZpbGU6IEJsb2I7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIG5hbWVGaWxlOiBzdHJpbmc7XHJcblxyXG4gICAgcm90YXRlOiBudW1iZXIgPSAwO1xyXG4gICAgc2NhbGVYOiBudW1iZXIgPSAxLjA7XHJcbiAgICBzY2FsZVk6IG51bWJlciA9IDEuMDtcclxuICAgIG9mZnNldFg6IG51bWJlciA9IDA7XHJcbiAgICBvZmZzZXRZOiBudW1iZXIgPSAwO1xyXG4gICAgaXNEcmFnZ2VkOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgcHJpdmF0ZSBkcmFnID0geyB4OiAwLCB5OiAwIH07XHJcbiAgICBwcml2YXRlIGRlbHRhID0geyB4OiAwLCB5OiAwIH07XHJcblxyXG4gICAgZ2V0IHRyYW5zZm9ybSgpOiBTYWZlU3R5bGUge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNhbml0aXplci5ieXBhc3NTZWN1cml0eVRydXN0U3R5bGUoYHNjYWxlKCR7dGhpcy5zY2FsZVh9LCAke3RoaXMuc2NhbGVZfSkgcm90YXRlKCR7dGhpcy5yb3RhdGV9ZGVnKSB0cmFuc2xhdGUoJHt0aGlzLm9mZnNldFh9cHgsICR7dGhpcy5vZmZzZXRZfXB4KWApO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBjdXJyZW50U2NhbGVUZXh0KCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIE1hdGgucm91bmQodGhpcy5zY2FsZVggKiAxMDApICsgJyUnO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZWxlbWVudDogSFRNTEVsZW1lbnQ7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBzYW5pdGl6ZXI6IERvbVNhbml0aXplcixcclxuICAgICAgICBwcml2YXRlIGFwcENvbmZpZ1NlcnZpY2U6IEFwcENvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBjb250ZW50U2VydmljZTogQ29udGVudFNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBlbDogRWxlbWVudFJlZikge1xyXG4gICAgICAgIHRoaXMuaW5pdGlhbGl6ZVNjYWxpbmcoKTtcclxuICAgIH1cclxuXHJcbiAgICBpbml0aWFsaXplU2NhbGluZygpIHtcclxuICAgICAgICBjb25zdCBzY2FsaW5nID0gdGhpcy5hcHBDb25maWdTZXJ2aWNlLmdldDxudW1iZXI+KCdhZGYtdmlld2VyLmltYWdlLXZpZXdlci1zY2FsaW5nJywgdW5kZWZpbmVkKSAvIDEwMDtcclxuICAgICAgICBpZiAoc2NhbGluZykge1xyXG4gICAgICAgICAgICB0aGlzLnNjYWxlWCA9IHNjYWxpbmc7XHJcbiAgICAgICAgICAgIHRoaXMuc2NhbGVZID0gc2NhbGluZztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5lbGVtZW50ID0gPEhUTUxFbGVtZW50PiB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignI3ZpZXdlci1pbWFnZScpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5lbGVtZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZWRvd24nLCB0aGlzLm9uTW91c2VEb3duLmJpbmQodGhpcykpO1xyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignbW91c2V1cCcsIHRoaXMub25Nb3VzZVVwLmJpbmQodGhpcykpO1xyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignbW91c2VsZWF2ZScsIHRoaXMub25Nb3VzZUxlYXZlLmJpbmQodGhpcykpO1xyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignbW91c2VvdXQnLCB0aGlzLm9uTW91c2VPdXQuYmluZCh0aGlzKSk7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZW1vdmUnLCB0aGlzLm9uTW91c2VNb3ZlLmJpbmQodGhpcykpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICBpZiAodGhpcy5lbGVtZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdtb3VzZWRvd24nLCB0aGlzLm9uTW91c2VEb3duKTtcclxuICAgICAgICAgICAgdGhpcy5lbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ21vdXNldXAnLCB0aGlzLm9uTW91c2VVcCk7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdtb3VzZWxlYXZlJywgdGhpcy5vbk1vdXNlTGVhdmUpO1xyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcignbW91c2VvdXQnLCB0aGlzLm9uTW91c2VPdXQpO1xyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcignbW91c2Vtb3ZlJywgdGhpcy5vbk1vdXNlTW92ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uTW91c2VEb3duKGV2ZW50OiBNb3VzZUV2ZW50KSB7XHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB0aGlzLmlzRHJhZ2dlZCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5kcmFnID0geyB4OiBldmVudC5wYWdlWCwgeTogZXZlbnQucGFnZVkgfTtcclxuICAgIH1cclxuXHJcbiAgICBvbk1vdXNlTW92ZShldmVudDogTW91c2VFdmVudCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzRHJhZ2dlZCkge1xyXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5kZWx0YS54ID0gZXZlbnQucGFnZVggLSB0aGlzLmRyYWcueDtcclxuICAgICAgICAgICAgdGhpcy5kZWx0YS55ID0gZXZlbnQucGFnZVkgLSB0aGlzLmRyYWcueTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZHJhZy54ID0gZXZlbnQucGFnZVg7XHJcbiAgICAgICAgICAgIHRoaXMuZHJhZy55ID0gZXZlbnQucGFnZVk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBzY2FsZVggPSAodGhpcy5zY2FsZVggIT09IDAgPyB0aGlzLnNjYWxlWCA6IDEuMCk7XHJcbiAgICAgICAgICAgIGNvbnN0IHNjYWxlWSA9ICh0aGlzLnNjYWxlWSAhPT0gMCA/IHRoaXMuc2NhbGVZIDogMS4wKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMub2Zmc2V0WCArPSAodGhpcy5kZWx0YS54IC8gc2NhbGVYKTtcclxuICAgICAgICAgICAgdGhpcy5vZmZzZXRZICs9ICh0aGlzLmRlbHRhLnkgLyBzY2FsZVkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvbk1vdXNlVXAoZXZlbnQ6IE1vdXNlRXZlbnQpIHtcclxuICAgICAgICBpZiAodGhpcy5pc0RyYWdnZWQpIHtcclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgdGhpcy5pc0RyYWdnZWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25Nb3VzZUxlYXZlKGV2ZW50OiBNb3VzZUV2ZW50KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNEcmFnZ2VkKSB7XHJcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIHRoaXMuaXNEcmFnZ2VkID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uTW91c2VPdXQoZXZlbnQ6IE1vdXNlRXZlbnQpIHtcclxuICAgICAgICBpZiAodGhpcy5pc0RyYWdnZWQpIHtcclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgdGhpcy5pc0RyYWdnZWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgICAgIGNvbnN0IGJsb2JGaWxlID0gY2hhbmdlc1snYmxvYkZpbGUnXTtcclxuICAgICAgICBpZiAoYmxvYkZpbGUgJiYgYmxvYkZpbGUuY3VycmVudFZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMudXJsRmlsZSA9IHRoaXMuY29udGVudFNlcnZpY2UuY3JlYXRlVHJ1c3RlZFVybCh0aGlzLmJsb2JGaWxlKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIXRoaXMudXJsRmlsZSAmJiAhdGhpcy5ibG9iRmlsZSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0F0dHJpYnV0ZSB1cmxGaWxlIG9yIGJsb2JGaWxlIGlzIHJlcXVpcmVkJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHpvb21JbigpIHtcclxuICAgICAgICBjb25zdCByYXRpbyA9ICsoKHRoaXMuc2NhbGVYICsgMC4yKS50b0ZpeGVkKDEpKTtcclxuICAgICAgICB0aGlzLnNjYWxlWCA9IHRoaXMuc2NhbGVZID0gcmF0aW87XHJcbiAgICB9XHJcblxyXG4gICAgem9vbU91dCgpIHtcclxuICAgICAgICBsZXQgcmF0aW8gPSArKCh0aGlzLnNjYWxlWCAtIDAuMikudG9GaXhlZCgxKSk7XHJcbiAgICAgICAgaWYgKHJhdGlvIDwgMC4yKSB7XHJcbiAgICAgICAgICAgIHJhdGlvID0gMC4yO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNjYWxlWCA9IHRoaXMuc2NhbGVZID0gcmF0aW87XHJcbiAgICB9XHJcblxyXG4gICAgcm90YXRlTGVmdCgpIHtcclxuICAgICAgICBjb25zdCBhbmdsZSA9IHRoaXMucm90YXRlIC0gOTA7XHJcbiAgICAgICAgdGhpcy5yb3RhdGUgPSBNYXRoLmFicyhhbmdsZSkgPCAzNjAgPyBhbmdsZSA6IDA7XHJcbiAgICB9XHJcblxyXG4gICAgcm90YXRlUmlnaHQoKSB7XHJcbiAgICAgICAgY29uc3QgYW5nbGUgPSB0aGlzLnJvdGF0ZSArIDkwO1xyXG4gICAgICAgIHRoaXMucm90YXRlID0gTWF0aC5hYnMoYW5nbGUpIDwgMzYwID8gYW5nbGUgOiAwO1xyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0KCkge1xyXG4gICAgICAgIHRoaXMucm90YXRlID0gMDtcclxuICAgICAgICB0aGlzLnNjYWxlWCA9IDEuMDtcclxuICAgICAgICB0aGlzLnNjYWxlWSA9IDEuMDtcclxuICAgICAgICB0aGlzLm9mZnNldFggPSAwO1xyXG4gICAgICAgIHRoaXMub2Zmc2V0WSA9IDA7XHJcbiAgICB9XHJcbn1cclxuIl19