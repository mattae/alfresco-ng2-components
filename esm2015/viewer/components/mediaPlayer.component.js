/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { ContentService } from '../../services/content.service';
export class MediaPlayerComponent {
    /**
     * @param {?} contentService
     */
    constructor(contentService) {
        this.contentService = contentService;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        /** @type {?} */
        const blobFile = changes['blobFile'];
        if (blobFile && blobFile.currentValue) {
            this.urlFile = this.contentService.createTrustedUrl(this.blobFile);
            return;
        }
        if (!this.urlFile && !this.blobFile) {
            throw new Error('Attribute urlFile or blobFile is required');
        }
    }
}
MediaPlayerComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-media-player',
                template: "<video controls>\r\n    <source [src]=\"urlFile\" [type]=\"mimeType\" />\r\n</video>\r\n",
                host: { 'class': 'adf-media-player' },
                encapsulation: ViewEncapsulation.None,
                styles: [".adf-media-player{display:flex}.adf-media-player video{display:flex;flex:1;max-height:90vh;max-width:100%}"]
            }] }
];
/** @nocollapse */
MediaPlayerComponent.ctorParameters = () => [
    { type: ContentService }
];
MediaPlayerComponent.propDecorators = {
    urlFile: [{ type: Input }],
    blobFile: [{ type: Input }],
    mimeType: [{ type: Input }],
    nameFile: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    MediaPlayerComponent.prototype.urlFile;
    /** @type {?} */
    MediaPlayerComponent.prototype.blobFile;
    /** @type {?} */
    MediaPlayerComponent.prototype.mimeType;
    /** @type {?} */
    MediaPlayerComponent.prototype.nameFile;
    /**
     * @type {?}
     * @private
     */
    MediaPlayerComponent.prototype.contentService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVkaWFQbGF5ZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsidmlld2VyL2NvbXBvbmVudHMvbWVkaWFQbGF5ZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUE0QixpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5RixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFTaEUsTUFBTSxPQUFPLG9CQUFvQjs7OztJQWM3QixZQUFvQixjQUE4QjtRQUE5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7SUFBSSxDQUFDOzs7OztJQUV2RCxXQUFXLENBQUMsT0FBc0I7O2NBQ3hCLFFBQVEsR0FBRyxPQUFPLENBQUMsVUFBVSxDQUFDO1FBQ3BDLElBQUksUUFBUSxJQUFJLFFBQVEsQ0FBQyxZQUFZLEVBQUU7WUFDbkMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUNuRSxPQUFPO1NBQ1Y7UUFFRCxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDakMsTUFBTSxJQUFJLEtBQUssQ0FBQywyQ0FBMkMsQ0FBQyxDQUFDO1NBQ2hFO0lBQ0wsQ0FBQzs7O1lBakNKLFNBQVMsU0FBQztnQkFDUCxRQUFRLEVBQUUsa0JBQWtCO2dCQUM1QixvR0FBMkM7Z0JBRTNDLElBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRTtnQkFDckMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2FBQ3hDOzs7O1lBUlEsY0FBYzs7O3NCQVdsQixLQUFLO3VCQUdMLEtBQUs7dUJBR0wsS0FBSzt1QkFHTCxLQUFLOzs7O0lBVE4sdUNBQ2dCOztJQUVoQix3Q0FDZTs7SUFFZix3Q0FDaUI7O0lBRWpCLHdDQUNpQjs7Ozs7SUFFTCw4Q0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25DaGFuZ2VzLCBTaW1wbGVDaGFuZ2VzLCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb250ZW50U2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2NvbnRlbnQuc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLW1lZGlhLXBsYXllcicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vbWVkaWFQbGF5ZXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vbWVkaWFQbGF5ZXIuY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGhvc3Q6IHsgJ2NsYXNzJzogJ2FkZi1tZWRpYS1wbGF5ZXInIH0sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNZWRpYVBsYXllckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHVybEZpbGU6IHN0cmluZztcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgYmxvYkZpbGU6IEJsb2I7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIG1pbWVUeXBlOiBzdHJpbmc7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIG5hbWVGaWxlOiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBjb250ZW50U2VydmljZTogQ29udGVudFNlcnZpY2UgKSB7fVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgICAgICBjb25zdCBibG9iRmlsZSA9IGNoYW5nZXNbJ2Jsb2JGaWxlJ107XHJcbiAgICAgICAgaWYgKGJsb2JGaWxlICYmIGJsb2JGaWxlLmN1cnJlbnRWYWx1ZSkge1xyXG4gICAgICAgICAgICB0aGlzLnVybEZpbGUgPSB0aGlzLmNvbnRlbnRTZXJ2aWNlLmNyZWF0ZVRydXN0ZWRVcmwodGhpcy5ibG9iRmlsZSk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghdGhpcy51cmxGaWxlICYmICF0aGlzLmJsb2JGaWxlKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignQXR0cmlidXRlIHVybEZpbGUgb3IgYmxvYkZpbGUgaXMgcmVxdWlyZWQnKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19