/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
export class PdfPasswordDialogComponent {
    /**
     * @param {?} dialogRef
     * @param {?} data
     */
    constructor(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.passwordFormControl = new FormControl('', [Validators.required]);
    }
    /**
     * @return {?}
     */
    isError() {
        return this.data.reason === pdfjsLib.PasswordResponses.INCORRECT_PASSWORD;
    }
    /**
     * @return {?}
     */
    isValid() {
        return !this.passwordFormControl.hasError('required');
    }
    /**
     * @return {?}
     */
    submit() {
        this.dialogRef.close(this.passwordFormControl.value);
    }
}
PdfPasswordDialogComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-pdf-viewer-password-dialog',
                template: "<div mat-dialog-title>\r\n    <mat-icon>lock</mat-icon>\r\n</div>\r\n\r\n<mat-dialog-content>\r\n    <form (submit)=\"submit()\">\r\n        <mat-form-field class=\"adf-full-width\">\r\n            <input matInput\r\n                   data-automation-id='adf-password-dialog-input'\r\n                   type=\"password\"\r\n                   placeholder=\"{{ 'ADF_VIEWER.PDF_DIALOG.PLACEHOLDER' | translate }}\"\r\n                   [formControl]=\"passwordFormControl\" />\r\n        </mat-form-field>\r\n\r\n        <mat-error *ngIf=\"isError()\" data-automation-id='adf-password-dialog-error'>{{ 'ADF_VIEWER.PDF_DIALOG.ERROR' | translate }}</mat-error>\r\n    </form>\r\n</mat-dialog-content>\r\n\r\n<mat-dialog-actions class=\"adf-dialog-buttons\">\r\n    <span class=\"adf-fill-remaining-space\"></span>\r\n\r\n    <button mat-button mat-dialog-close data-automation-id='adf-password-dialog-close'>{{ 'ADF_VIEWER.PDF_DIALOG.CLOSE' | translate }}</button>\r\n\r\n    <button mat-button\r\n            data-automation-id='adf-password-dialog-submit'\r\n            class=\"adf-dialog-action-button\"\r\n            [disabled]=\"!isValid()\"\r\n            (click)=\"submit()\">\r\n        {{ 'ADF_VIEWER.PDF_DIALOG.SUBMIT' | translate }}\r\n    </button>\r\n</mat-dialog-actions>\r\n",
                styles: [".adf-fill-remaining-space{flex:1 1 auto}.adf-full-width{width:100%}"]
            }] }
];
/** @nocollapse */
PdfPasswordDialogComponent.ctorParameters = () => [
    { type: MatDialogRef },
    { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
];
if (false) {
    /** @type {?} */
    PdfPasswordDialogComponent.prototype.passwordFormControl;
    /**
     * @type {?}
     * @private
     */
    PdfPasswordDialogComponent.prototype.dialogRef;
    /** @type {?} */
    PdfPasswordDialogComponent.prototype.data;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGRmVmlld2VyLXBhc3N3b3JkLWRpYWxvZy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInZpZXdlci9jb21wb25lbnRzL3BkZlZpZXdlci1wYXNzd29yZC1kaWFsb2cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDMUQsT0FBTyxFQUFFLFlBQVksRUFBRSxlQUFlLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBU3pELE1BQU0sT0FBTywwQkFBMEI7Ozs7O0lBR25DLFlBQ1ksU0FBbUQsRUFDM0IsSUFBUztRQURqQyxjQUFTLEdBQVQsU0FBUyxDQUEwQztRQUMzQixTQUFJLEdBQUosSUFBSSxDQUFLO0lBQzFDLENBQUM7Ozs7SUFFSixRQUFRO1FBQ0osSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksV0FBVyxDQUFDLEVBQUUsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQzFFLENBQUM7Ozs7SUFFRCxPQUFPO1FBQ0gsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sS0FBSyxRQUFRLENBQUMsaUJBQWlCLENBQUMsa0JBQWtCLENBQUM7SUFDOUUsQ0FBQzs7OztJQUVELE9BQU87UUFDSCxPQUFPLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUMxRCxDQUFDOzs7O0lBRUQsTUFBTTtRQUNGLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN6RCxDQUFDOzs7WUEzQkosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxnQ0FBZ0M7Z0JBQzFDLHN4Q0FBK0M7O2FBRWxEOzs7O1lBVFEsWUFBWTs0Q0FlWixNQUFNLFNBQUMsZUFBZTs7OztJQUozQix5REFBaUM7Ozs7O0lBRzdCLCtDQUEyRDs7SUFDM0QsMENBQXlDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgSW5qZWN0LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nUmVmLCBNQVRfRElBTE9HX0RBVEEgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IEZvcm1Db250cm9sLCBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5cclxuZGVjbGFyZSBjb25zdCBwZGZqc0xpYjogYW55O1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1wZGYtdmlld2VyLXBhc3N3b3JkLWRpYWxvZycsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vcGRmVmlld2VyLXBhc3N3b3JkLWRpYWxvZy5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWyAnLi9wZGZWaWV3ZXItcGFzc3dvcmQtZGlhbG9nLnNjc3MnIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIFBkZlBhc3N3b3JkRGlhbG9nQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIHBhc3N3b3JkRm9ybUNvbnRyb2w6IEZvcm1Db250cm9sO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgZGlhbG9nUmVmOiBNYXREaWFsb2dSZWY8UGRmUGFzc3dvcmREaWFsb2dDb21wb25lbnQ+LFxyXG4gICAgICAgIEBJbmplY3QoTUFUX0RJQUxPR19EQVRBKSBwdWJsaWMgZGF0YTogYW55XHJcbiAgICApIHt9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5wYXNzd29yZEZvcm1Db250cm9sID0gbmV3IEZvcm1Db250cm9sKCcnLCBbVmFsaWRhdG9ycy5yZXF1aXJlZF0pO1xyXG4gICAgfVxyXG5cclxuICAgIGlzRXJyb3IoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZGF0YS5yZWFzb24gPT09IHBkZmpzTGliLlBhc3N3b3JkUmVzcG9uc2VzLklOQ09SUkVDVF9QQVNTV09SRDtcclxuICAgIH1cclxuXHJcbiAgICBpc1ZhbGlkKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiAhdGhpcy5wYXNzd29yZEZvcm1Db250cm9sLmhhc0Vycm9yKCdyZXF1aXJlZCcpO1xyXG4gICAgfVxyXG5cclxuICAgIHN1Ym1pdCgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmRpYWxvZ1JlZi5jbG9zZSh0aGlzLnBhc3N3b3JkRm9ybUNvbnRyb2wudmFsdWUpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==