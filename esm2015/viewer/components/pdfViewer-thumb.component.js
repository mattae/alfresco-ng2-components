/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
export class PdfThumbComponent {
    /**
     * @param {?} sanitizer
     */
    constructor(sanitizer) {
        this.sanitizer = sanitizer;
        this.page = null;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.image$ = this.page.getPage().then((/**
         * @param {?} page
         * @return {?}
         */
        (page) => this.getThumb(page)));
    }
    /**
     * @private
     * @param {?} page
     * @return {?}
     */
    getThumb(page) {
        /** @type {?} */
        const viewport = page.getViewport(1);
        /** @type {?} */
        const pageRatio = viewport.width / viewport.height;
        /** @type {?} */
        const canvas = this.getCanvas(pageRatio);
        /** @type {?} */
        const scale = Math.min((canvas.height / viewport.height), (canvas.width / viewport.width));
        return page.render({
            canvasContext: canvas.getContext('2d'),
            viewport: page.getViewport(scale)
        })
            .then((/**
         * @return {?}
         */
        () => {
            /** @type {?} */
            const imageSource = canvas.toDataURL();
            return this.sanitizer.bypassSecurityTrustUrl(imageSource);
        }));
    }
    /**
     * @private
     * @param {?} pageRatio
     * @return {?}
     */
    getCanvas(pageRatio) {
        /** @type {?} */
        const canvas = document.createElement('canvas');
        canvas.width = this.page.getWidth();
        canvas.height = this.page.getHeight();
        return canvas;
    }
}
PdfThumbComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-pdf-thumb',
                template: "<ng-container *ngIf=\"image$ | async as image\">\r\n    <img [src]=\"image\" [alt]=\"'ADF_VIEWER.SIDEBAR.THUMBNAILS.PAGE' | translate: { pageNum: page.id }\"\r\n        title=\"{{ 'ADF_VIEWER.SIDEBAR.THUMBNAILS.PAGE' | translate: { pageNum: page.id } }}\">\r\n</ng-container>\r\n",
                encapsulation: ViewEncapsulation.None
            }] }
];
/** @nocollapse */
PdfThumbComponent.ctorParameters = () => [
    { type: DomSanitizer }
];
PdfThumbComponent.propDecorators = {
    page: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    PdfThumbComponent.prototype.page;
    /** @type {?} */
    PdfThumbComponent.prototype.image$;
    /**
     * @type {?}
     * @private
     */
    PdfThumbComponent.prototype.sanitizer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGRmVmlld2VyLXRodW1iLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInZpZXdlci9jb21wb25lbnRzL3BkZlZpZXdlci10aHVtYi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQVUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDNUUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBT3pELE1BQU0sT0FBTyxpQkFBaUI7Ozs7SUFPMUIsWUFBb0IsU0FBdUI7UUFBdkIsY0FBUyxHQUFULFNBQVMsQ0FBYztRQUozQyxTQUFJLEdBQVEsSUFBSSxDQUFDO0lBSTZCLENBQUM7Ozs7SUFFL0MsUUFBUTtRQUNKLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJOzs7O1FBQUMsQ0FBQyxJQUFJLEVBQUUsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUMsQ0FBQztJQUMxRSxDQUFDOzs7Ozs7SUFFTyxRQUFRLENBQUMsSUFBSTs7Y0FDWCxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7O2NBRTlCLFNBQVMsR0FBRyxRQUFRLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxNQUFNOztjQUM1QyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUM7O2NBQ2xDLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUUxRixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDZixhQUFhLEVBQUUsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7WUFDdEMsUUFBUSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO1NBQ3BDLENBQUM7YUFDRCxJQUFJOzs7UUFBQyxHQUFHLEVBQUU7O2tCQUNELFdBQVcsR0FBRyxNQUFNLENBQUMsU0FBUyxFQUFFO1lBQ3RDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUM5RCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQUVPLFNBQVMsQ0FBQyxTQUFTOztjQUNqQixNQUFNLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7UUFDL0MsTUFBTSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ3BDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQztRQUN0QyxPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDOzs7WUF4Q0osU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxlQUFlO2dCQUN6QixtU0FBK0M7Z0JBQy9DLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO2FBQ3hDOzs7O1lBTlEsWUFBWTs7O21CQVNoQixLQUFLOzs7O0lBQU4saUNBQ2lCOztJQUVqQixtQ0FBd0I7Ozs7O0lBRVosc0NBQStCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRG9tU2FuaXRpemVyIH0gZnJvbSAnQGFuZ3VsYXIvcGxhdGZvcm0tYnJvd3Nlcic7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLXBkZi10aHVtYicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vcGRmVmlld2VyLXRodW1iLmNvbXBvbmVudC5odG1sJyxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIFBkZlRodW1iQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgcGFnZTogYW55ID0gbnVsbDtcclxuXHJcbiAgICBpbWFnZSQ6IFByb21pc2U8c3RyaW5nPjtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNhbml0aXplcjogRG9tU2FuaXRpemVyKSB7fVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMuaW1hZ2UkID0gdGhpcy5wYWdlLmdldFBhZ2UoKS50aGVuKChwYWdlKSA9PiB0aGlzLmdldFRodW1iKHBhZ2UpKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldFRodW1iKHBhZ2UpOiBQcm9taXNlPHN0cmluZz4ge1xyXG4gICAgICAgIGNvbnN0IHZpZXdwb3J0ID0gcGFnZS5nZXRWaWV3cG9ydCgxKTtcclxuXHJcbiAgICAgICAgY29uc3QgcGFnZVJhdGlvID0gdmlld3BvcnQud2lkdGggLyB2aWV3cG9ydC5oZWlnaHQ7XHJcbiAgICAgICAgY29uc3QgY2FudmFzID0gdGhpcy5nZXRDYW52YXMocGFnZVJhdGlvKTtcclxuICAgICAgICBjb25zdCBzY2FsZSA9IE1hdGgubWluKChjYW52YXMuaGVpZ2h0IC8gdmlld3BvcnQuaGVpZ2h0KSwgKGNhbnZhcy53aWR0aCAvIHZpZXdwb3J0LndpZHRoKSk7XHJcblxyXG4gICAgICAgIHJldHVybiBwYWdlLnJlbmRlcih7XHJcbiAgICAgICAgICAgIGNhbnZhc0NvbnRleHQ6IGNhbnZhcy5nZXRDb250ZXh0KCcyZCcpLFxyXG4gICAgICAgICAgICB2aWV3cG9ydDogcGFnZS5nZXRWaWV3cG9ydChzY2FsZSlcclxuICAgICAgICB9KVxyXG4gICAgICAgIC50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgaW1hZ2VTb3VyY2UgPSBjYW52YXMudG9EYXRhVVJMKCk7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnNhbml0aXplci5ieXBhc3NTZWN1cml0eVRydXN0VXJsKGltYWdlU291cmNlKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldENhbnZhcyhwYWdlUmF0aW8pOiBIVE1MQ2FudmFzRWxlbWVudCB7XHJcbiAgICAgICAgY29uc3QgY2FudmFzID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnY2FudmFzJyk7XHJcbiAgICAgICAgY2FudmFzLndpZHRoID0gdGhpcy5wYWdlLmdldFdpZHRoKCk7XHJcbiAgICAgICAgY2FudmFzLmhlaWdodCA9IHRoaXMucGFnZS5nZXRIZWlnaHQoKTtcclxuICAgICAgICByZXR1cm4gY2FudmFzO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==