/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, TemplateRef, HostListener, Output, Input, ViewEncapsulation, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { LogService } from '../../services/log.service';
import { RenderingQueueServices } from '../services/rendering-queue.services';
import { PdfPasswordDialogComponent } from './pdfViewer-password-dialog';
import { AppConfigService } from './../../app-config/app-config.service';
/**
 * @record
 */
export function PdfDocumentOptions() { }
if (false) {
    /** @type {?|undefined} */
    PdfDocumentOptions.prototype.url;
    /** @type {?|undefined} */
    PdfDocumentOptions.prototype.data;
    /** @type {?|undefined} */
    PdfDocumentOptions.prototype.withCredentials;
}
export class PdfViewerComponent {
    /**
     * @param {?} dialog
     * @param {?} renderingQueueServices
     * @param {?} logService
     * @param {?} appConfigService
     */
    constructor(dialog, renderingQueueServices, logService, appConfigService) {
        this.dialog = dialog;
        this.renderingQueueServices = renderingQueueServices;
        this.logService = logService;
        this.appConfigService = appConfigService;
        this.showToolbar = true;
        this.allowThumbnails = false;
        this.thumbnailsTemplate = null;
        this.rendered = new EventEmitter();
        this.error = new EventEmitter();
        this.close = new EventEmitter();
        this.currentScaleMode = 'auto';
        this.currentScale = 1;
        this.MAX_AUTO_SCALE = 1.25;
        this.DEFAULT_SCALE_DELTA = 1.1;
        this.MIN_SCALE = 0.25;
        this.MAX_SCALE = 10.0;
        this.isPanelDisabled = true;
        this.showThumbnails = false;
        this.pdfThumbnailsContext = { viewer: null };
        // needed to preserve "this" context
        this.onPageChange = this.onPageChange.bind(this);
        this.onPagesLoaded = this.onPagesLoaded.bind(this);
        this.onPageRendered = this.onPageRendered.bind(this);
        this.randomPdfId = this.generateUuid();
        this.currentScale = this.getUserScaling();
    }
    /**
     * @return {?}
     */
    get currentScaleText() {
        return Math.round(this.currentScale * 100) + '%';
    }
    /**
     * @return {?}
     */
    getUserScaling() {
        /** @type {?} */
        const scaleConfig = this.appConfigService.get('adf-viewer.pdf-viewer-scaling', undefined) / 100;
        if (scaleConfig) {
            return this.checkLimits(scaleConfig);
        }
        else {
            return 1;
        }
    }
    /**
     * @param {?} scaleConfig
     * @return {?}
     */
    checkLimits(scaleConfig) {
        if (scaleConfig > this.MAX_SCALE) {
            return this.MAX_SCALE;
        }
        else if (scaleConfig < this.MIN_SCALE) {
            return this.MIN_SCALE;
        }
        else {
            return scaleConfig;
        }
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        /** @type {?} */
        const blobFile = changes['blobFile'];
        if (blobFile && blobFile.currentValue) {
            /** @type {?} */
            const reader = new FileReader();
            reader.onload = (/**
             * @return {?}
             */
            () => {
                /** @type {?} */
                const options = {
                    data: reader.result,
                    withCredentials: this.appConfigService.get('auth.withCredentials', undefined)
                };
                this.executePdf(options);
            });
            reader.readAsArrayBuffer(blobFile.currentValue);
        }
        /** @type {?} */
        const urlFile = changes['urlFile'];
        if (urlFile && urlFile.currentValue) {
            /** @type {?} */
            const options = {
                url: urlFile.currentValue,
                withCredentials: this.appConfigService.get('auth.withCredentials', undefined)
            };
            this.executePdf(options);
        }
        if (!this.urlFile && !this.blobFile) {
            throw new Error('Attribute urlFile or blobFile is required');
        }
    }
    /**
     * @param {?} pdfOptions
     * @return {?}
     */
    executePdf(pdfOptions) {
        pdfjsLib.GlobalWorkerOptions.workerSrc = 'pdf.worker.min.js';
        this.loadingTask = pdfjsLib.getDocument(pdfOptions);
        this.loadingTask.onPassword = (/**
         * @param {?} callback
         * @param {?} reason
         * @return {?}
         */
        (callback, reason) => {
            this.onPdfPassword(callback, reason);
        });
        this.loadingTask.onProgress = (/**
         * @param {?} progressData
         * @return {?}
         */
        (progressData) => {
            /** @type {?} */
            const level = progressData.loaded / progressData.total;
            this.loadingPercent = Math.round(level * 100);
        });
        this.loadingTask.then((/**
         * @param {?} pdfDocument
         * @return {?}
         */
        (pdfDocument) => {
            this.currentPdfDocument = pdfDocument;
            this.totalPages = pdfDocument.numPages;
            this.page = 1;
            this.displayPage = 1;
            this.initPDFViewer(this.currentPdfDocument);
            this.currentPdfDocument.getPage(1).then((/**
             * @return {?}
             */
            () => {
                this.scalePage('auto');
            }), (/**
             * @return {?}
             */
            () => {
                this.error.emit();
            }));
        }), (/**
         * @return {?}
         */
        () => {
            this.error.emit();
        }));
    }
    /**
     * @param {?} pdfDocument
     * @return {?}
     */
    initPDFViewer(pdfDocument) {
        /** @type {?} */
        const viewer = document.getElementById(`${this.randomPdfId}-viewer-viewerPdf`);
        /** @type {?} */
        const container = document.getElementById(`${this.randomPdfId}-viewer-pdf-viewer`);
        if (viewer && container) {
            this.documentContainer = container;
            // cspell: disable-next
            this.documentContainer.addEventListener('pagechange', this.onPageChange, true);
            // cspell: disable-next
            this.documentContainer.addEventListener('pagesloaded', this.onPagesLoaded, true);
            // cspell: disable-next
            this.documentContainer.addEventListener('textlayerrendered', this.onPageRendered, true);
            this.pdfViewer = new pdfjsViewer.PDFViewer({
                container: this.documentContainer,
                viewer: viewer,
                renderingQueue: this.renderingQueueServices
            });
            this.renderingQueueServices.setViewer(this.pdfViewer);
            this.pdfViewer.setDocument(pdfDocument);
            this.pdfThumbnailsContext.viewer = this.pdfViewer;
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        if (this.documentContainer) {
            // cspell: disable-next
            this.documentContainer.removeEventListener('pagechange', this.onPageChange, true);
            // cspell: disable-next
            this.documentContainer.removeEventListener('pagesloaded', this.onPagesLoaded, true);
            // cspell: disable-next
            this.documentContainer.removeEventListener('textlayerrendered', this.onPageRendered, true);
        }
        if (this.loadingTask) {
            try {
                this.loadingTask.destroy();
            }
            catch (_a) {
            }
            this.loadingTask = null;
        }
    }
    /**
     * @return {?}
     */
    toggleThumbnails() {
        this.showThumbnails = !this.showThumbnails;
    }
    /**
     * Method to scale the page current support implementation
     *
     * @param {?} scaleMode - new scale mode
     * @return {?}
     */
    scalePage(scaleMode) {
        this.currentScaleMode = scaleMode;
        /** @type {?} */
        const viewerContainer = document.getElementById(`${this.randomPdfId}-viewer-main-container`);
        /** @type {?} */
        const documentContainer = document.getElementById(`${this.randomPdfId}-viewer-pdf-viewer`);
        if (this.pdfViewer && documentContainer) {
            /** @type {?} */
            let widthContainer;
            /** @type {?} */
            let heightContainer;
            if (viewerContainer && viewerContainer.clientWidth <= documentContainer.clientWidth) {
                widthContainer = viewerContainer.clientWidth;
                heightContainer = viewerContainer.clientHeight;
            }
            else {
                widthContainer = documentContainer.clientWidth;
                heightContainer = documentContainer.clientHeight;
            }
            /** @type {?} */
            const currentPage = this.pdfViewer._pages[this.pdfViewer._currentPageNumber - 1];
            /** @type {?} */
            const padding = 20;
            /** @type {?} */
            const pageWidthScale = (widthContainer - padding) / currentPage.width * currentPage.scale;
            /** @type {?} */
            const pageHeightScale = (heightContainer - padding) / currentPage.width * currentPage.scale;
            /** @type {?} */
            let scale = this.getUserScaling();
            if (!scale) {
                switch (this.currentScaleMode) {
                    case 'page-actual':
                        scale = 1;
                        break;
                    case 'page-width':
                        scale = pageWidthScale;
                        break;
                    case 'page-height':
                        scale = pageHeightScale;
                        break;
                    case 'page-fit':
                        scale = Math.min(pageWidthScale, pageHeightScale);
                        break;
                    case 'auto':
                        /** @type {?} */
                        let horizontalScale;
                        if (this.isLandscape) {
                            horizontalScale = Math.min(pageHeightScale, pageWidthScale);
                        }
                        else {
                            horizontalScale = pageWidthScale;
                        }
                        horizontalScale = Math.round(horizontalScale);
                        scale = Math.min(this.MAX_AUTO_SCALE, horizontalScale);
                        break;
                    default:
                        this.logService.error('pdfViewSetScale: \'' + scaleMode + '\' is an unknown zoom value.');
                        return;
                }
                this.setScaleUpdatePages(scale);
            }
            else {
                this.currentScale = 0;
                this.setScaleUpdatePages(scale);
            }
        }
    }
    /**
     * Update all the pages with the newScale scale
     *
     * @param {?} newScale - new scale page
     * @return {?}
     */
    setScaleUpdatePages(newScale) {
        if (this.pdfViewer) {
            if (!this.isSameScale(this.currentScale, newScale)) {
                this.currentScale = newScale;
                this.pdfViewer._pages.forEach((/**
                 * @param {?} currentPage
                 * @return {?}
                 */
                function (currentPage) {
                    currentPage.update(newScale);
                }));
            }
            this.pdfViewer.update();
        }
    }
    /**
     * Check if the request scale of the page is the same for avoid useless re-rendering
     *
     * @param {?} oldScale - old scale page
     * @param {?} newScale - new scale page
     *
     * @return {?}
     */
    isSameScale(oldScale, newScale) {
        return (newScale === oldScale);
    }
    /**
     * Check if is a land scape view
     *
     * @param {?} width
     * @param {?} height
     * @return {?}
     */
    isLandscape(width, height) {
        return (width > height);
    }
    /**
     * Method triggered when the page is resized
     * @return {?}
     */
    onResize() {
        this.scalePage(this.currentScaleMode);
    }
    /**
     * toggle the fit page pdf
     * @return {?}
     */
    pageFit() {
        if (this.currentScaleMode !== 'page-fit') {
            this.scalePage('page-fit');
        }
        else {
            this.scalePage('auto');
        }
    }
    /**
     * zoom in page pdf
     *
     * @param {?=} ticks
     * @return {?}
     */
    zoomIn(ticks) {
        /** @type {?} */
        let newScale = this.currentScale;
        do {
            newScale = (newScale * this.DEFAULT_SCALE_DELTA).toFixed(2);
            newScale = Math.ceil(newScale * 10) / 10;
            newScale = Math.min(this.MAX_SCALE, newScale);
        } while (--ticks > 0 && newScale < this.MAX_SCALE);
        this.currentScaleMode = 'auto';
        this.setScaleUpdatePages(newScale);
    }
    /**
     * zoom out page pdf
     *
     * @param {?=} ticks
     * @return {?}
     */
    zoomOut(ticks) {
        /** @type {?} */
        let newScale = this.currentScale;
        do {
            newScale = (newScale / this.DEFAULT_SCALE_DELTA).toFixed(2);
            newScale = Math.floor(newScale * 10) / 10;
            newScale = Math.max(this.MIN_SCALE, newScale);
        } while (--ticks > 0 && newScale > this.MIN_SCALE);
        this.currentScaleMode = 'auto';
        this.setScaleUpdatePages(newScale);
    }
    /**
     * load the previous page
     * @return {?}
     */
    previousPage() {
        if (this.pdfViewer && this.page > 1) {
            this.page--;
            this.displayPage = this.page;
            this.pdfViewer.currentPageNumber = this.page;
        }
    }
    /**
     * load the next page
     * @return {?}
     */
    nextPage() {
        if (this.pdfViewer && this.page < this.totalPages) {
            this.page++;
            this.displayPage = this.page;
            this.pdfViewer.currentPageNumber = this.page;
        }
    }
    /**
     * load the page in input
     *
     * @param {?} page to load
     * @return {?}
     */
    inputPage(page) {
        /** @type {?} */
        const pageInput = parseInt(page, 10);
        if (!isNaN(pageInput) && pageInput > 0 && pageInput <= this.totalPages) {
            this.page = pageInput;
            this.displayPage = this.page;
            this.pdfViewer.currentPageNumber = this.page;
        }
        else {
            this.displayPage = this.page;
        }
    }
    /**
     * Page Change Event
     *
     * @param {?} event
     * @return {?}
     */
    onPageChange(event) {
        this.page = event.pageNumber;
        this.displayPage = event.pageNumber;
    }
    /**
     * @param {?} callback
     * @param {?} reason
     * @return {?}
     */
    onPdfPassword(callback, reason) {
        this.dialog
            .open(PdfPasswordDialogComponent, {
            width: '400px',
            data: { reason }
        })
            .afterClosed().subscribe((/**
         * @param {?} password
         * @return {?}
         */
        (password) => {
            if (password) {
                callback(password);
            }
            else {
                this.close.emit();
            }
        }));
    }
    /**
     * Page Rendered Event
     * @return {?}
     */
    onPageRendered() {
        this.rendered.emit();
    }
    /**
     * Pages Loaded Event
     *
     * @param {?} event
     * @return {?}
     */
    onPagesLoaded(event) {
        this.isPanelDisabled = false;
    }
    /**
     * Keyboard Event Listener
     * @param {?} event
     * @return {?}
     */
    handleKeyboardEvent(event) {
        /** @type {?} */
        const key = event.keyCode;
        if (key === 39) { // right arrow
            this.nextPage();
        }
        else if (key === 37) { // left arrow
            this.previousPage();
        }
    }
    /**
     * @private
     * @return {?}
     */
    generateUuid() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (/**
         * @param {?} c
         * @return {?}
         */
        function (c) {
            /** @type {?} */
            const r = Math.random() * 16 | 0;
            /** @type {?} */
            const v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        }));
    }
}
PdfViewerComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-pdf-viewer',
                template: "<div class=\"adf-pdf-viewer__container\">\r\n    <ng-container *ngIf=\"showThumbnails\">\r\n        <div class=\"adf-pdf-viewer__thumbnails\">\r\n            <div class=\"adf-thumbnails-template__container\">\r\n                <div class=\"adf-thumbnails-template__buttons\">\r\n                    <button mat-icon-button data-automation-id='adf-thumbnails-close' (click)=\"toggleThumbnails()\">\r\n                        <mat-icon>close</mat-icon>\r\n                    </button>\r\n                </div>\r\n\r\n                <ng-container *ngIf=\"thumbnailsTemplate\">\r\n                    <ng-container *ngTemplateOutlet=\"thumbnailsTemplate;context:pdfThumbnailsContext\"></ng-container>\r\n                </ng-container>\r\n\r\n                <ng-container *ngIf=\"!thumbnailsTemplate\">\r\n                    <adf-pdf-thumbnails [pdfViewer]=\"pdfViewer\"></adf-pdf-thumbnails>\r\n                </ng-container>\r\n            </div>\r\n        </div>\r\n    </ng-container>\r\n\r\n    <div class=\"adf-pdf-viewer__content\">\r\n        <div [id]=\"randomPdfId+'-viewer-pdf-viewer'\" class=\"adf-viewer-pdf-viewer\" (window:resize)=\"onResize()\">\r\n            <div [id]=\"randomPdfId+'-viewer-viewerPdf'\" class=\"adf-pdfViewer\" role=\"document\" tabindex=\"0\" aria-expanded=\"true\">\r\n                <div id=\"loader-container\" class=\"adf-loader-container\">\r\n                    <div class=\"adf-loader-item\">\r\n                        <mat-progress-bar mode=\"indeterminate\"></mat-progress-bar>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"adf-pdf-viewer__toolbar\" *ngIf=\"showToolbar\">\r\n    <adf-toolbar>\r\n\r\n        <ng-container *ngIf=\"allowThumbnails\">\r\n            <button mat-icon-button\r\n                    data-automation-id=\"adf-thumbnails-button\"\r\n                    [disabled]=\"isPanelDisabled\"\r\n                    (click)=\"toggleThumbnails()\">\r\n                <mat-icon>dashboard</mat-icon>\r\n            </button>\r\n            <adf-toolbar-divider></adf-toolbar-divider>\r\n        </ng-container>\r\n\r\n        <button\r\n            id=\"viewer-previous-page-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.PREVIOUS_PAGE' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.PREVIOUS_PAGE' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"previousPage()\">\r\n            <mat-icon>keyboard_arrow_up</mat-icon>\r\n        </button>\r\n\r\n        <button\r\n            id=\"viewer-next-page-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.NEXT_PAGE' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.NEXT_PAGE' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"nextPage()\">\r\n            <mat-icon>keyboard_arrow_down</mat-icon>\r\n        </button>\r\n\r\n        <div class=\"adf-pdf-viewer__toolbar-page-selector\">\r\n            <span>{{ 'ADF_VIEWER.PAGE_LABEL.SHOWING' | translate }}</span>\r\n            <input #page\r\n                   type=\"text\"\r\n                   data-automation-id=\"adf-page-selector\"\r\n                   pattern=\"-?[0-9]*(\\.[0-9]+)?\"\r\n                   value=\"{{ displayPage }}\"\r\n                   (keyup.enter)=\"inputPage(page.value)\">\r\n            <span>{{ 'ADF_VIEWER.PAGE_LABEL.OF' | translate }} {{ totalPages }}</span>\r\n        </div>\r\n\r\n        <div class=\"adf-viewer__toolbar-page-scale\" data-automation-id=\"adf-page-scale\">\r\n            {{ currentScaleText }}\r\n        </div>\r\n\r\n        <button\r\n            id=\"viewer-zoom-in-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.ZOOM_IN' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.ZOOM_IN' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"zoomIn()\">\r\n            <mat-icon>zoom_in</mat-icon>\r\n        </button>\r\n\r\n        <button\r\n            id=\"viewer-zoom-out-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.ZOOM_OUT' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.ZOOM_OUT' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"zoomOut()\">\r\n            <mat-icon>zoom_out</mat-icon>\r\n        </button>\r\n\r\n        <button\r\n            id=\"viewer-scale-page-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.FIT_PAGE' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.FIT_PAGE' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"pageFit()\">\r\n            <mat-icon>zoom_out_map</mat-icon>\r\n        </button>\r\n\r\n    </adf-toolbar>\r\n</div>\r\n",
                providers: [RenderingQueueServices],
                host: { 'class': 'adf-pdf-viewer' },
                encapsulation: ViewEncapsulation.None,
                styles: ["", ".adf-pdf-viewer .textLayer{position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;opacity:.2;line-height:1;border:1px solid gray}.adf-pdf-viewer .textLayer>div{color:transparent;position:absolute;white-space:pre;cursor:text;transform-origin:0 0}.adf-pdf-viewer .textLayer .adf-highlight{margin:-1px;padding:1px;background-color:#b400aa;border-radius:4px}.adf-pdf-viewer .textLayer .adf-highlight.adf-begin{border-radius:4px 0 0 4px}.adf-pdf-viewer .textLayer .adf-highlight.adf-end{border-radius:0 4px 4px 0}.adf-pdf-viewer .textLayer .adf-highlight.adf-middle{border-radius:0}.adf-pdf-viewer .textLayer .adf-highlight.adf-selected{background-color:#006400}.adf-pdf-viewer .textLayer::selection{background:#00f}.adf-pdf-viewer .textLayer::-moz-selection{background:#00f}.adf-pdf-viewer .textLayer .adf-endOfContent{display:block;position:absolute;left:0;top:100%;right:0;bottom:0;z-index:-1;cursor:default;user-select:none;-webkit-user-select:none;-ms-user-select:none;-moz-user-select:none}.adf-pdf-viewer .textLayer .adf-endOfContent.adf-active{top:0}.adf-pdf-viewer .adf-annotationLayer section{position:absolute}.adf-pdf-viewer .adf-annotationLayer .adf-linkAnnotation>a{position:absolute;font-size:1em;top:0;left:0;width:100%;height:100%;background:url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)}.adf-pdf-viewer .adf-annotationLayer .adf-linkAnnotation>a:hover{opacity:.2;background:#ff0;box-shadow:0 2px 10px #ff0}.adf-pdf-viewer .adf-annotationLayer .adf-textAnnotation img{position:absolute;cursor:pointer}.adf-pdf-viewer .adf-annotationLayer .adf-popupWrapper{position:absolute;width:20em}.adf-pdf-viewer .adf-annotationLayer .adf-popup{position:absolute;z-index:200;max-width:20em;background-color:#ff9;box-shadow:0 2px 5px #333;border-radius:2px;padding:.6em;margin-left:5px;cursor:pointer;word-wrap:break-word}.adf-pdf-viewer .adf-annotationLayer .adf-popup h1{font-size:1em;border-bottom:1px solid #000;padding-bottom:.2em}.adf-pdf-viewer .adf-annotationLayer .adf-popup p{padding-top:.2em}.adf-pdf-viewer .adf-annotationLayer .adf-fileAttachmentAnnotation,.adf-pdf-viewer .adf-annotationLayer .adf-highlightAnnotation,.adf-pdf-viewer .adf-annotationLayer .adf-squigglyAnnotation,.adf-pdf-viewer .adf-annotationLayer .adf-strikeoutAnnotation,.adf-pdf-viewer .adf-annotationLayer .adf-underlineAnnotation{cursor:pointer}.adf-pdf-viewer .adf-pdfViewer .canvasWrapper{overflow:hidden}.adf-pdf-viewer .adf-pdfViewer .page{direction:ltr;width:816px;height:1056px;margin:1px auto -8px;position:relative;overflow:visible;border:9px solid transparent;background-clip:content-box;background-color:#fff}.adf-pdf-viewer .adf-pdfViewer .page canvas{margin:0;display:block}.adf-pdf-viewer .adf-pdfViewer .page .adf-loadingIcon{position:absolute;display:block;left:0;top:0;right:0;bottom:0}.adf-pdf-viewer .adf-pdfViewer .page *{padding:0;margin:0}.adf-pdf-viewer .adf-pdfViewer.adf-removePageBorders .adf-page{margin:0 auto 10px;border:none}.adf-pdf-viewer .adf-pdfViewer .adf-loadingIcon{width:100px;height:100px;left:50%!important;top:50%!important;margin-top:-50px;margin-left:-50px;font-size:5px;text-indent:-9999em;border-top:1.1em solid rgba(3,0,2,.2);border-right:1.1em solid rgba(3,0,2,.2);border-bottom:1.1em solid rgba(3,0,2,.2);border-left:1.1em solid #030002;transform:translateZ(0);-webkit-animation:1.1s linear infinite load8;animation:1.1s linear infinite load8;border-radius:50%}.adf-pdf-viewer .adf-pdfViewer .adf-loadingIcon::after{border-radius:50%}.adf-pdf-viewer .adf-hidden,.adf-pdf-viewer [hidden]{display:none!important}@-webkit-keyframes load8{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}@keyframes load8{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}.adf-viewer-pdf-viewer{overflow:auto;-webkit-overflow-scrolling:touch;position:absolute;top:0;right:0;bottom:0;left:0;outline:0}html[dir=ltr] .adf-viewer-pdf-viewer{box-shadow:inset 1px 0 0 rgba(255,255,255,.05)}html[dir=rtl] .adf-viewer-pdf-viewer{box-shadow:inset -1px 0 0 rgba(255,255,255,.05)}"]
            }] }
];
/** @nocollapse */
PdfViewerComponent.ctorParameters = () => [
    { type: MatDialog },
    { type: RenderingQueueServices },
    { type: LogService },
    { type: AppConfigService }
];
PdfViewerComponent.propDecorators = {
    urlFile: [{ type: Input }],
    blobFile: [{ type: Input }],
    nameFile: [{ type: Input }],
    showToolbar: [{ type: Input }],
    allowThumbnails: [{ type: Input }],
    thumbnailsTemplate: [{ type: Input }],
    rendered: [{ type: Output }],
    error: [{ type: Output }],
    close: [{ type: Output }],
    handleKeyboardEvent: [{ type: HostListener, args: ['document:keydown', ['$event'],] }]
};
if (false) {
    /** @type {?} */
    PdfViewerComponent.prototype.urlFile;
    /** @type {?} */
    PdfViewerComponent.prototype.blobFile;
    /** @type {?} */
    PdfViewerComponent.prototype.nameFile;
    /** @type {?} */
    PdfViewerComponent.prototype.showToolbar;
    /** @type {?} */
    PdfViewerComponent.prototype.allowThumbnails;
    /** @type {?} */
    PdfViewerComponent.prototype.thumbnailsTemplate;
    /** @type {?} */
    PdfViewerComponent.prototype.rendered;
    /** @type {?} */
    PdfViewerComponent.prototype.error;
    /** @type {?} */
    PdfViewerComponent.prototype.close;
    /** @type {?} */
    PdfViewerComponent.prototype.loadingTask;
    /** @type {?} */
    PdfViewerComponent.prototype.currentPdfDocument;
    /** @type {?} */
    PdfViewerComponent.prototype.page;
    /** @type {?} */
    PdfViewerComponent.prototype.displayPage;
    /** @type {?} */
    PdfViewerComponent.prototype.totalPages;
    /** @type {?} */
    PdfViewerComponent.prototype.loadingPercent;
    /** @type {?} */
    PdfViewerComponent.prototype.pdfViewer;
    /** @type {?} */
    PdfViewerComponent.prototype.documentContainer;
    /** @type {?} */
    PdfViewerComponent.prototype.currentScaleMode;
    /** @type {?} */
    PdfViewerComponent.prototype.currentScale;
    /** @type {?} */
    PdfViewerComponent.prototype.MAX_AUTO_SCALE;
    /** @type {?} */
    PdfViewerComponent.prototype.DEFAULT_SCALE_DELTA;
    /** @type {?} */
    PdfViewerComponent.prototype.MIN_SCALE;
    /** @type {?} */
    PdfViewerComponent.prototype.MAX_SCALE;
    /** @type {?} */
    PdfViewerComponent.prototype.isPanelDisabled;
    /** @type {?} */
    PdfViewerComponent.prototype.showThumbnails;
    /** @type {?} */
    PdfViewerComponent.prototype.pdfThumbnailsContext;
    /** @type {?} */
    PdfViewerComponent.prototype.randomPdfId;
    /**
     * @type {?}
     * @private
     */
    PdfViewerComponent.prototype.dialog;
    /**
     * @type {?}
     * @private
     */
    PdfViewerComponent.prototype.renderingQueueServices;
    /**
     * @type {?}
     * @private
     */
    PdfViewerComponent.prototype.logService;
    /**
     * @type {?}
     * @private
     */
    PdfViewerComponent.prototype.appConfigService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGRmVmlld2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInZpZXdlci9jb21wb25lbnRzL3BkZlZpZXdlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUNILFNBQVMsRUFDVCxXQUFXLEVBQ1gsWUFBWSxFQUNaLE1BQU0sRUFDTixLQUFLLEVBR0wsaUJBQWlCLEVBQ2pCLFlBQVksRUFFZixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDOUMsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQzlFLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQ3pFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDOzs7O0FBS3pFLHdDQUlDOzs7SUFIRyxpQ0FBYTs7SUFDYixrQ0FBVzs7SUFDWCw2Q0FBMEI7O0FBYzlCLE1BQU0sT0FBTyxrQkFBa0I7Ozs7Ozs7SUFzRDNCLFlBQ1ksTUFBaUIsRUFDakIsc0JBQThDLEVBQzlDLFVBQXNCLEVBQ3RCLGdCQUFrQztRQUhsQyxXQUFNLEdBQU4sTUFBTSxDQUFXO1FBQ2pCLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFDOUMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBOUM5QyxnQkFBVyxHQUFZLElBQUksQ0FBQztRQUc1QixvQkFBZSxHQUFHLEtBQUssQ0FBQztRQUd4Qix1QkFBa0IsR0FBcUIsSUFBSSxDQUFDO1FBRzVDLGFBQVEsR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBR25DLFVBQUssR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBR2hDLFVBQUssR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDO1FBVWhDLHFCQUFnQixHQUFXLE1BQU0sQ0FBQztRQUNsQyxpQkFBWSxHQUFXLENBQUMsQ0FBQztRQUV6QixtQkFBYyxHQUFXLElBQUksQ0FBQztRQUM5Qix3QkFBbUIsR0FBVyxHQUFHLENBQUM7UUFDbEMsY0FBUyxHQUFXLElBQUksQ0FBQztRQUN6QixjQUFTLEdBQVcsSUFBSSxDQUFDO1FBRXpCLG9CQUFlLEdBQUcsSUFBSSxDQUFDO1FBQ3ZCLG1CQUFjLEdBQVksS0FBSyxDQUFDO1FBQ2hDLHlCQUFvQixHQUFvQixFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsQ0FBQztRQVlyRCxvQ0FBb0M7UUFDcEMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNqRCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ25ELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7UUFDdkMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDOUMsQ0FBQzs7OztJQWZELElBQUksZ0JBQWdCO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsWUFBWSxHQUFHLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQztJQUNyRCxDQUFDOzs7O0lBZUQsY0FBYzs7Y0FDSixXQUFXLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBUywrQkFBK0IsRUFBRSxTQUFTLENBQUMsR0FBRyxHQUFHO1FBQ3ZHLElBQUksV0FBVyxFQUFFO1lBQ2IsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQ3hDO2FBQU07WUFDSCxPQUFPLENBQUMsQ0FBQztTQUNaO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxXQUFXLENBQUMsV0FBbUI7UUFDM0IsSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUM5QixPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7U0FDekI7YUFBTSxJQUFJLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ3JDLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztTQUN6QjthQUFNO1lBQ0gsT0FBTyxXQUFXLENBQUM7U0FDdEI7SUFDTCxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxPQUFzQjs7Y0FDeEIsUUFBUSxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUM7UUFFcEMsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFlBQVksRUFBRTs7a0JBQzdCLE1BQU0sR0FBRyxJQUFJLFVBQVUsRUFBRTtZQUMvQixNQUFNLENBQUMsTUFBTTs7O1lBQUcsR0FBRyxFQUFFOztzQkFDWCxPQUFPLEdBQUc7b0JBQ1osSUFBSSxFQUFFLE1BQU0sQ0FBQyxNQUFNO29CQUNuQixlQUFlLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBVSxzQkFBc0IsRUFBRSxTQUFTLENBQUM7aUJBQ3pGO2dCQUNELElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxDQUFDLENBQUM7WUFDN0IsQ0FBQyxDQUFBLENBQUM7WUFDRixNQUFNLENBQUMsaUJBQWlCLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQ25EOztjQUVLLE9BQU8sR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDO1FBQ2xDLElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxZQUFZLEVBQUU7O2tCQUMzQixPQUFPLEdBQUc7Z0JBQ1osR0FBRyxFQUFFLE9BQU8sQ0FBQyxZQUFZO2dCQUN6QixlQUFlLEVBQUUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBVSxzQkFBc0IsRUFBRSxTQUFTLENBQUM7YUFDekY7WUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQzVCO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2pDLE1BQU0sSUFBSSxLQUFLLENBQUMsMkNBQTJDLENBQUMsQ0FBQztTQUNoRTtJQUNMLENBQUM7Ozs7O0lBRUQsVUFBVSxDQUFDLFVBQThCO1FBQ3JDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLEdBQUcsbUJBQW1CLENBQUM7UUFFN0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxRQUFRLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBRXBELElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVTs7Ozs7UUFBRyxDQUFDLFFBQVEsRUFBRSxNQUFNLEVBQUUsRUFBRTtZQUMvQyxJQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsQ0FBQztRQUN6QyxDQUFDLENBQUEsQ0FBQztRQUVGLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVTs7OztRQUFHLENBQUMsWUFBWSxFQUFFLEVBQUU7O2tCQUNyQyxLQUFLLEdBQUcsWUFBWSxDQUFDLE1BQU0sR0FBRyxZQUFZLENBQUMsS0FBSztZQUN0RCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBQ2xELENBQUMsQ0FBQSxDQUFDO1FBRUYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJOzs7O1FBQUMsQ0FBQyxXQUFXLEVBQUUsRUFBRTtZQUNsQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsV0FBVyxDQUFDO1lBQ3RDLElBQUksQ0FBQyxVQUFVLEdBQUcsV0FBVyxDQUFDLFFBQVEsQ0FBQztZQUN2QyxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsQ0FBQztZQUNkLElBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDO1lBQ3JCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLENBQUM7WUFFNUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJOzs7WUFBQyxHQUFHLEVBQUU7Z0JBQ3pDLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7WUFDM0IsQ0FBQzs7O1lBQUUsR0FBRyxFQUFFO2dCQUNKLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxFQUFDLENBQUM7UUFFUCxDQUFDOzs7UUFBRSxHQUFHLEVBQUU7WUFDSixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO1FBQ3RCLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxhQUFhLENBQUMsV0FBZ0I7O2NBQ3BCLE1BQU0sR0FBUSxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsbUJBQW1CLENBQUM7O2NBQzdFLFNBQVMsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsb0JBQW9CLENBQUM7UUFFbEYsSUFBSSxNQUFNLElBQUksU0FBUyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxTQUFTLENBQUM7WUFFbkMsdUJBQXVCO1lBQ3ZCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxnQkFBZ0IsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUMvRSx1QkFBdUI7WUFDdkIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ2pGLHVCQUF1QjtZQUN2QixJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUV4RixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksV0FBVyxDQUFDLFNBQVMsQ0FBQztnQkFDdkMsU0FBUyxFQUFFLElBQUksQ0FBQyxpQkFBaUI7Z0JBQ2pDLE1BQU0sRUFBRSxNQUFNO2dCQUNkLGNBQWMsRUFBRSxJQUFJLENBQUMsc0JBQXNCO2FBQzlDLENBQUMsQ0FBQztZQUVILElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQ3RELElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3hDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQztTQUNyRDtJQUNMLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDeEIsdUJBQXVCO1lBQ3ZCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNsRix1QkFBdUI7WUFDdkIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLG1CQUFtQixDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3BGLHVCQUF1QjtZQUN2QixJQUFJLENBQUMsaUJBQWlCLENBQUMsbUJBQW1CLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUM5RjtRQUVELElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNsQixJQUFJO2dCQUNBLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDOUI7WUFBQyxXQUFNO2FBQ1A7WUFFRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztTQUMzQjtJQUNMLENBQUM7Ozs7SUFFRCxnQkFBZ0I7UUFDWixJQUFJLENBQUMsY0FBYyxHQUFHLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztJQUMvQyxDQUFDOzs7Ozs7O0lBT0QsU0FBUyxDQUFDLFNBQVM7UUFDZixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsU0FBUyxDQUFDOztjQUU1QixlQUFlLEdBQUcsUUFBUSxDQUFDLGNBQWMsQ0FBQyxHQUFHLElBQUksQ0FBQyxXQUFXLHdCQUF3QixDQUFDOztjQUN0RixpQkFBaUIsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFDLEdBQUcsSUFBSSxDQUFDLFdBQVcsb0JBQW9CLENBQUM7UUFFMUYsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLGlCQUFpQixFQUFFOztnQkFFakMsY0FBYzs7Z0JBQ2QsZUFBZTtZQUVuQixJQUFJLGVBQWUsSUFBSSxlQUFlLENBQUMsV0FBVyxJQUFJLGlCQUFpQixDQUFDLFdBQVcsRUFBRTtnQkFDakYsY0FBYyxHQUFHLGVBQWUsQ0FBQyxXQUFXLENBQUM7Z0JBQzdDLGVBQWUsR0FBRyxlQUFlLENBQUMsWUFBWSxDQUFDO2FBQ2xEO2lCQUFNO2dCQUNILGNBQWMsR0FBRyxpQkFBaUIsQ0FBQyxXQUFXLENBQUM7Z0JBQy9DLGVBQWUsR0FBRyxpQkFBaUIsQ0FBQyxZQUFZLENBQUM7YUFDcEQ7O2tCQUVLLFdBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGtCQUFrQixHQUFHLENBQUMsQ0FBQzs7a0JBRTFFLE9BQU8sR0FBRyxFQUFFOztrQkFDWixjQUFjLEdBQUcsQ0FBQyxjQUFjLEdBQUcsT0FBTyxDQUFDLEdBQUcsV0FBVyxDQUFDLEtBQUssR0FBRyxXQUFXLENBQUMsS0FBSzs7a0JBQ25GLGVBQWUsR0FBRyxDQUFDLGVBQWUsR0FBRyxPQUFPLENBQUMsR0FBRyxXQUFXLENBQUMsS0FBSyxHQUFHLFdBQVcsQ0FBQyxLQUFLOztnQkFFdkYsS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLEVBQUU7WUFDakMsSUFBSSxDQUFDLEtBQUssRUFBRTtnQkFDUixRQUFRLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtvQkFDM0IsS0FBSyxhQUFhO3dCQUNkLEtBQUssR0FBRyxDQUFDLENBQUM7d0JBQ1YsTUFBTTtvQkFDVixLQUFLLFlBQVk7d0JBQ2IsS0FBSyxHQUFHLGNBQWMsQ0FBQzt3QkFDdkIsTUFBTTtvQkFDVixLQUFLLGFBQWE7d0JBQ2QsS0FBSyxHQUFHLGVBQWUsQ0FBQzt3QkFDeEIsTUFBTTtvQkFDVixLQUFLLFVBQVU7d0JBQ1gsS0FBSyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLGVBQWUsQ0FBQyxDQUFDO3dCQUNsRCxNQUFNO29CQUNWLEtBQUssTUFBTTs7NEJBQ0gsZUFBZTt3QkFDbkIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFOzRCQUNsQixlQUFlLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsY0FBYyxDQUFDLENBQUM7eUJBQy9EOzZCQUFNOzRCQUNILGVBQWUsR0FBRyxjQUFjLENBQUM7eUJBQ3BDO3dCQUNELGVBQWUsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGVBQWUsQ0FBQyxDQUFDO3dCQUM5QyxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLGVBQWUsQ0FBQyxDQUFDO3dCQUV2RCxNQUFNO29CQUNWO3dCQUNJLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLHFCQUFxQixHQUFHLFNBQVMsR0FBRyw4QkFBOEIsQ0FBQyxDQUFDO3dCQUMxRixPQUFPO2lCQUNkO2dCQUVELElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNuQztpQkFBTTtnQkFDSCxJQUFJLENBQUMsWUFBWSxHQUFHLENBQUMsQ0FBQztnQkFDdEIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ25DO1NBQ0o7SUFDTCxDQUFDOzs7Ozs7O0lBT0QsbUJBQW1CLENBQUMsUUFBZ0I7UUFDaEMsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsUUFBUSxDQUFDLEVBQUU7Z0JBQ2hELElBQUksQ0FBQyxZQUFZLEdBQUcsUUFBUSxDQUFDO2dCQUU3QixJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxPQUFPOzs7O2dCQUFDLFVBQVUsV0FBVztvQkFDL0MsV0FBVyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztnQkFDakMsQ0FBQyxFQUFDLENBQUM7YUFDTjtZQUVELElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxFQUFFLENBQUM7U0FDM0I7SUFDTCxDQUFDOzs7Ozs7Ozs7SUFTRCxXQUFXLENBQUMsUUFBZ0IsRUFBRSxRQUFnQjtRQUMxQyxPQUFPLENBQUMsUUFBUSxLQUFLLFFBQVEsQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7Ozs7O0lBUUQsV0FBVyxDQUFDLEtBQWEsRUFBRSxNQUFjO1FBQ3JDLE9BQU8sQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLENBQUM7SUFDNUIsQ0FBQzs7Ozs7SUFLRCxRQUFRO1FBQ0osSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztJQUMxQyxDQUFDOzs7OztJQUtELE9BQU87UUFDSCxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxVQUFVLEVBQUU7WUFDdEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUM5QjthQUFNO1lBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUMxQjtJQUNMLENBQUM7Ozs7Ozs7SUFPRCxNQUFNLENBQUMsS0FBYzs7WUFDYixRQUFRLEdBQVEsSUFBSSxDQUFDLFlBQVk7UUFDckMsR0FBRztZQUNDLFFBQVEsR0FBRyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDNUQsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUN6QyxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQ2pELFFBQVEsRUFBRSxLQUFLLEdBQUcsQ0FBQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxFQUFFO1FBQ25ELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLENBQUM7UUFDL0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7Ozs7Ozs7SUFPRCxPQUFPLENBQUMsS0FBYzs7WUFDZCxRQUFRLEdBQVEsSUFBSSxDQUFDLFlBQVk7UUFDckMsR0FBRztZQUNDLFFBQVEsR0FBRyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDNUQsUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUMxQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQ2pELFFBQVEsRUFBRSxLQUFLLEdBQUcsQ0FBQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxFQUFFO1FBQ25ELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLENBQUM7UUFDL0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7Ozs7O0lBS0QsWUFBWTtRQUNSLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsRUFBRTtZQUNqQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDWixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7WUFFN0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ2hEO0lBQ0wsQ0FBQzs7Ozs7SUFLRCxRQUFRO1FBQ0osSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLFVBQVUsRUFBRTtZQUMvQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDWixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7WUFFN0IsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ2hEO0lBQ0wsQ0FBQzs7Ozs7OztJQU9ELFNBQVMsQ0FBQyxJQUFZOztjQUNaLFNBQVMsR0FBRyxRQUFRLENBQUMsSUFBSSxFQUFFLEVBQUUsQ0FBQztRQUVwQyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQyxJQUFJLFNBQVMsR0FBRyxDQUFDLElBQUksU0FBUyxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDcEUsSUFBSSxDQUFDLElBQUksR0FBRyxTQUFTLENBQUM7WUFDdEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQzdCLElBQUksQ0FBQyxTQUFTLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztTQUNoRDthQUFNO1lBQ0gsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1NBQ2hDO0lBQ0wsQ0FBQzs7Ozs7OztJQU9ELFlBQVksQ0FBQyxLQUFLO1FBQ2QsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDO1FBQzdCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQztJQUN4QyxDQUFDOzs7Ozs7SUFFRCxhQUFhLENBQUMsUUFBUSxFQUFFLE1BQU07UUFDMUIsSUFBSSxDQUFDLE1BQU07YUFDTixJQUFJLENBQUMsMEJBQTBCLEVBQUU7WUFDOUIsS0FBSyxFQUFFLE9BQU87WUFDZCxJQUFJLEVBQUUsRUFBRSxNQUFNLEVBQUU7U0FDbkIsQ0FBQzthQUNELFdBQVcsRUFBRSxDQUFDLFNBQVM7Ozs7UUFBQyxDQUFDLFFBQVEsRUFBRSxFQUFFO1lBQ2xDLElBQUksUUFBUSxFQUFFO2dCQUNWLFFBQVEsQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUN0QjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxDQUFDO2FBQ3JCO1FBQ1QsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUtELGNBQWM7UUFDVixJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3pCLENBQUM7Ozs7Ozs7SUFPRCxhQUFhLENBQUMsS0FBSztRQUNmLElBQUksQ0FBQyxlQUFlLEdBQUcsS0FBSyxDQUFDO0lBQ2pDLENBQUM7Ozs7OztJQU9ELG1CQUFtQixDQUFDLEtBQW9COztjQUM5QixHQUFHLEdBQUcsS0FBSyxDQUFDLE9BQU87UUFDekIsSUFBSSxHQUFHLEtBQUssRUFBRSxFQUFFLEVBQUUsY0FBYztZQUM1QixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDbkI7YUFBTSxJQUFJLEdBQUcsS0FBSyxFQUFFLEVBQUUsRUFBQyxhQUFhO1lBQ2pDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztTQUN2QjtJQUNMLENBQUM7Ozs7O0lBRU8sWUFBWTtRQUNoQixPQUFPLHNDQUFzQyxDQUFDLE9BQU8sQ0FBQyxPQUFPOzs7O1FBQUUsVUFBVSxDQUFDOztrQkFDaEUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQzs7a0JBQUUsQ0FBQyxHQUFHLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQztZQUNyRSxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDMUIsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7WUFwZEosU0FBUyxTQUFDO2dCQUNQLFFBQVEsRUFBRSxnQkFBZ0I7Z0JBQzFCLHFvSkFBeUM7Z0JBS3pDLFNBQVMsRUFBRSxDQUFDLHNCQUFzQixDQUFDO2dCQUNuQyxJQUFJLEVBQUUsRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUU7Z0JBQ25DLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOzthQUN4Qzs7OztZQXpCUSxTQUFTO1lBRVQsc0JBQXNCO1lBRHRCLFVBQVU7WUFHVixnQkFBZ0I7OztzQkF3QnBCLEtBQUs7dUJBR0wsS0FBSzt1QkFHTCxLQUFLOzBCQUdMLEtBQUs7OEJBR0wsS0FBSztpQ0FHTCxLQUFLO3VCQUdMLE1BQU07b0JBR04sTUFBTTtvQkFHTixNQUFNO2tDQWdhTixZQUFZLFNBQUMsa0JBQWtCLEVBQUUsQ0FBQyxRQUFRLENBQUM7Ozs7SUF4YjVDLHFDQUNnQjs7SUFFaEIsc0NBQ2U7O0lBRWYsc0NBQ2lCOztJQUVqQix5Q0FDNEI7O0lBRTVCLDZDQUN3Qjs7SUFFeEIsZ0RBQzRDOztJQUU1QyxzQ0FDbUM7O0lBRW5DLG1DQUNnQzs7SUFFaEMsbUNBQ2dDOztJQUVoQyx5Q0FBaUI7O0lBQ2pCLGdEQUF3Qjs7SUFDeEIsa0NBQWE7O0lBQ2IseUNBQW9COztJQUNwQix3Q0FBbUI7O0lBQ25CLDRDQUF1Qjs7SUFDdkIsdUNBQWU7O0lBQ2YsK0NBQXVCOztJQUN2Qiw4Q0FBa0M7O0lBQ2xDLDBDQUF5Qjs7SUFFekIsNENBQThCOztJQUM5QixpREFBa0M7O0lBQ2xDLHVDQUF5Qjs7SUFDekIsdUNBQXlCOztJQUV6Qiw2Q0FBdUI7O0lBQ3ZCLDRDQUFnQzs7SUFDaEMsa0RBQXlEOztJQUN6RCx5Q0FBb0I7Ozs7O0lBT2hCLG9DQUF5Qjs7Ozs7SUFDekIsb0RBQXNEOzs7OztJQUN0RCx3Q0FBOEI7Ozs7O0lBQzlCLDhDQUEwQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQge1xyXG4gICAgQ29tcG9uZW50LFxyXG4gICAgVGVtcGxhdGVSZWYsXHJcbiAgICBIb3N0TGlzdGVuZXIsXHJcbiAgICBPdXRwdXQsXHJcbiAgICBJbnB1dCxcclxuICAgIE9uQ2hhbmdlcyxcclxuICAgIE9uRGVzdHJveSxcclxuICAgIFZpZXdFbmNhcHN1bGF0aW9uLFxyXG4gICAgRXZlbnRFbWl0dGVyLFxyXG4gICAgU2ltcGxlQ2hhbmdlc1xyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2cgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9sb2cuc2VydmljZSc7XHJcbmltcG9ydCB7IFJlbmRlcmluZ1F1ZXVlU2VydmljZXMgfSBmcm9tICcuLi9zZXJ2aWNlcy9yZW5kZXJpbmctcXVldWUuc2VydmljZXMnO1xyXG5pbXBvcnQgeyBQZGZQYXNzd29yZERpYWxvZ0NvbXBvbmVudCB9IGZyb20gJy4vcGRmVmlld2VyLXBhc3N3b3JkLWRpYWxvZyc7XHJcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLy4uLy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuXHJcbmRlY2xhcmUgY29uc3QgcGRmanNMaWI6IGFueTtcclxuZGVjbGFyZSBjb25zdCBwZGZqc1ZpZXdlcjogYW55O1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBQZGZEb2N1bWVudE9wdGlvbnMge1xyXG4gICAgdXJsPzogc3RyaW5nO1xyXG4gICAgZGF0YT86IGFueTtcclxuICAgIHdpdGhDcmVkZW50aWFscz86IGJvb2xlYW47XHJcbn1cclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtcGRmLXZpZXdlcicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vcGRmVmlld2VyLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogW1xyXG4gICAgICAgICcuL3BkZlZpZXdlci5jb21wb25lbnQuc2NzcycsXHJcbiAgICAgICAgJy4vcGRmVmlld2VySG9zdC5jb21wb25lbnQuc2NzcydcclxuICAgIF0sXHJcbiAgICBwcm92aWRlcnM6IFtSZW5kZXJpbmdRdWV1ZVNlcnZpY2VzXSxcclxuICAgIGhvc3Q6IHsgJ2NsYXNzJzogJ2FkZi1wZGYtdmlld2VyJyB9LFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUGRmVmlld2VyQ29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzLCBPbkRlc3Ryb3kge1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICB1cmxGaWxlOiBzdHJpbmc7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGJsb2JGaWxlOiBCbG9iO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBuYW1lRmlsZTogc3RyaW5nO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzaG93VG9vbGJhcjogYm9vbGVhbiA9IHRydWU7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGFsbG93VGh1bWJuYWlscyA9IGZhbHNlO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICB0aHVtYm5haWxzVGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT4gPSBudWxsO1xyXG5cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgcmVuZGVyZWQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICBAT3V0cHV0KClcclxuICAgIGVycm9yID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgQE91dHB1dCgpXHJcbiAgICBjbG9zZSA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIGxvYWRpbmdUYXNrOiBhbnk7XHJcbiAgICBjdXJyZW50UGRmRG9jdW1lbnQ6IGFueTtcclxuICAgIHBhZ2U6IG51bWJlcjtcclxuICAgIGRpc3BsYXlQYWdlOiBudW1iZXI7XHJcbiAgICB0b3RhbFBhZ2VzOiBudW1iZXI7XHJcbiAgICBsb2FkaW5nUGVyY2VudDogbnVtYmVyO1xyXG4gICAgcGRmVmlld2VyOiBhbnk7XHJcbiAgICBkb2N1bWVudENvbnRhaW5lcjogYW55O1xyXG4gICAgY3VycmVudFNjYWxlTW9kZTogc3RyaW5nID0gJ2F1dG8nO1xyXG4gICAgY3VycmVudFNjYWxlOiBudW1iZXIgPSAxO1xyXG5cclxuICAgIE1BWF9BVVRPX1NDQUxFOiBudW1iZXIgPSAxLjI1O1xyXG4gICAgREVGQVVMVF9TQ0FMRV9ERUxUQTogbnVtYmVyID0gMS4xO1xyXG4gICAgTUlOX1NDQUxFOiBudW1iZXIgPSAwLjI1O1xyXG4gICAgTUFYX1NDQUxFOiBudW1iZXIgPSAxMC4wO1xyXG5cclxuICAgIGlzUGFuZWxEaXNhYmxlZCA9IHRydWU7XHJcbiAgICBzaG93VGh1bWJuYWlsczogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgcGRmVGh1bWJuYWlsc0NvbnRleHQ6IHsgdmlld2VyOiBhbnkgfSA9IHsgdmlld2VyOiBudWxsIH07XHJcbiAgICByYW5kb21QZGZJZDogc3RyaW5nO1xyXG5cclxuICAgIGdldCBjdXJyZW50U2NhbGVUZXh0KCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIE1hdGgucm91bmQodGhpcy5jdXJyZW50U2NhbGUgKiAxMDApICsgJyUnO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgZGlhbG9nOiBNYXREaWFsb2csXHJcbiAgICAgICAgcHJpdmF0ZSByZW5kZXJpbmdRdWV1ZVNlcnZpY2VzOiBSZW5kZXJpbmdRdWV1ZVNlcnZpY2VzLFxyXG4gICAgICAgIHByaXZhdGUgbG9nU2VydmljZTogTG9nU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIGFwcENvbmZpZ1NlcnZpY2U6IEFwcENvbmZpZ1NlcnZpY2UpIHtcclxuICAgICAgICAvLyBuZWVkZWQgdG8gcHJlc2VydmUgXCJ0aGlzXCIgY29udGV4dFxyXG4gICAgICAgIHRoaXMub25QYWdlQ2hhbmdlID0gdGhpcy5vblBhZ2VDaGFuZ2UuYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLm9uUGFnZXNMb2FkZWQgPSB0aGlzLm9uUGFnZXNMb2FkZWQuYmluZCh0aGlzKTtcclxuICAgICAgICB0aGlzLm9uUGFnZVJlbmRlcmVkID0gdGhpcy5vblBhZ2VSZW5kZXJlZC5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMucmFuZG9tUGRmSWQgPSB0aGlzLmdlbmVyYXRlVXVpZCgpO1xyXG4gICAgICAgIHRoaXMuY3VycmVudFNjYWxlID0gdGhpcy5nZXRVc2VyU2NhbGluZygpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFVzZXJTY2FsaW5nKCk6IG51bWJlciB7XHJcbiAgICAgICAgY29uc3Qgc2NhbGVDb25maWcgPSB0aGlzLmFwcENvbmZpZ1NlcnZpY2UuZ2V0PG51bWJlcj4oJ2FkZi12aWV3ZXIucGRmLXZpZXdlci1zY2FsaW5nJywgdW5kZWZpbmVkKSAvIDEwMDtcclxuICAgICAgICBpZiAoc2NhbGVDb25maWcpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuY2hlY2tMaW1pdHMoc2NhbGVDb25maWcpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiAxO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjaGVja0xpbWl0cyhzY2FsZUNvbmZpZzogbnVtYmVyKTogbnVtYmVyIHtcclxuICAgICAgICBpZiAoc2NhbGVDb25maWcgPiB0aGlzLk1BWF9TQ0FMRSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5NQVhfU0NBTEU7XHJcbiAgICAgICAgfSBlbHNlIGlmIChzY2FsZUNvbmZpZyA8IHRoaXMuTUlOX1NDQUxFKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLk1JTl9TQ0FMRTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gc2NhbGVDb25maWc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgICAgICBjb25zdCBibG9iRmlsZSA9IGNoYW5nZXNbJ2Jsb2JGaWxlJ107XHJcblxyXG4gICAgICAgIGlmIChibG9iRmlsZSAmJiBibG9iRmlsZS5jdXJyZW50VmFsdWUpIHtcclxuICAgICAgICAgICAgY29uc3QgcmVhZGVyID0gbmV3IEZpbGVSZWFkZXIoKTtcclxuICAgICAgICAgICAgcmVhZGVyLm9ubG9hZCA9ICgpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZGF0YTogcmVhZGVyLnJlc3VsdCxcclxuICAgICAgICAgICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRoaXMuYXBwQ29uZmlnU2VydmljZS5nZXQ8Ym9vbGVhbj4oJ2F1dGgud2l0aENyZWRlbnRpYWxzJywgdW5kZWZpbmVkKVxyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZXhlY3V0ZVBkZihvcHRpb25zKTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgcmVhZGVyLnJlYWRBc0FycmF5QnVmZmVyKGJsb2JGaWxlLmN1cnJlbnRWYWx1ZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB1cmxGaWxlID0gY2hhbmdlc1sndXJsRmlsZSddO1xyXG4gICAgICAgIGlmICh1cmxGaWxlICYmIHVybEZpbGUuY3VycmVudFZhbHVlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgICAgICB1cmw6IHVybEZpbGUuY3VycmVudFZhbHVlLFxyXG4gICAgICAgICAgICAgICAgd2l0aENyZWRlbnRpYWxzOiB0aGlzLmFwcENvbmZpZ1NlcnZpY2UuZ2V0PGJvb2xlYW4+KCdhdXRoLndpdGhDcmVkZW50aWFscycsIHVuZGVmaW5lZClcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgdGhpcy5leGVjdXRlUGRmKG9wdGlvbnMpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLnVybEZpbGUgJiYgIXRoaXMuYmxvYkZpbGUpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdBdHRyaWJ1dGUgdXJsRmlsZSBvciBibG9iRmlsZSBpcyByZXF1aXJlZCcpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBleGVjdXRlUGRmKHBkZk9wdGlvbnM6IFBkZkRvY3VtZW50T3B0aW9ucykge1xyXG4gICAgICAgIHBkZmpzTGliLkdsb2JhbFdvcmtlck9wdGlvbnMud29ya2VyU3JjID0gJ3BkZi53b3JrZXIubWluLmpzJztcclxuXHJcbiAgICAgICAgdGhpcy5sb2FkaW5nVGFzayA9IHBkZmpzTGliLmdldERvY3VtZW50KHBkZk9wdGlvbnMpO1xyXG5cclxuICAgICAgICB0aGlzLmxvYWRpbmdUYXNrLm9uUGFzc3dvcmQgPSAoY2FsbGJhY2ssIHJlYXNvbikgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm9uUGRmUGFzc3dvcmQoY2FsbGJhY2ssIHJlYXNvbik7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgdGhpcy5sb2FkaW5nVGFzay5vblByb2dyZXNzID0gKHByb2dyZXNzRGF0YSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBsZXZlbCA9IHByb2dyZXNzRGF0YS5sb2FkZWQgLyBwcm9ncmVzc0RhdGEudG90YWw7XHJcbiAgICAgICAgICAgIHRoaXMubG9hZGluZ1BlcmNlbnQgPSBNYXRoLnJvdW5kKGxldmVsICogMTAwKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICB0aGlzLmxvYWRpbmdUYXNrLnRoZW4oKHBkZkRvY3VtZW50KSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFBkZkRvY3VtZW50ID0gcGRmRG9jdW1lbnQ7XHJcbiAgICAgICAgICAgIHRoaXMudG90YWxQYWdlcyA9IHBkZkRvY3VtZW50Lm51bVBhZ2VzO1xyXG4gICAgICAgICAgICB0aGlzLnBhZ2UgPSAxO1xyXG4gICAgICAgICAgICB0aGlzLmRpc3BsYXlQYWdlID0gMTtcclxuICAgICAgICAgICAgdGhpcy5pbml0UERGVmlld2VyKHRoaXMuY3VycmVudFBkZkRvY3VtZW50KTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVudFBkZkRvY3VtZW50LmdldFBhZ2UoMSkudGhlbigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNjYWxlUGFnZSgnYXV0bycpO1xyXG4gICAgICAgICAgICB9LCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVycm9yLmVtaXQoKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5lcnJvci5lbWl0KCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdFBERlZpZXdlcihwZGZEb2N1bWVudDogYW55KSB7XHJcbiAgICAgICAgY29uc3Qgdmlld2VyOiBhbnkgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChgJHt0aGlzLnJhbmRvbVBkZklkfS12aWV3ZXItdmlld2VyUGRmYCk7XHJcbiAgICAgICAgY29uc3QgY29udGFpbmVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoYCR7dGhpcy5yYW5kb21QZGZJZH0tdmlld2VyLXBkZi12aWV3ZXJgKTtcclxuXHJcbiAgICAgICAgaWYgKHZpZXdlciAmJiBjb250YWluZXIpIHtcclxuICAgICAgICAgICAgdGhpcy5kb2N1bWVudENvbnRhaW5lciA9IGNvbnRhaW5lcjtcclxuXHJcbiAgICAgICAgICAgIC8vIGNzcGVsbDogZGlzYWJsZS1uZXh0XHJcbiAgICAgICAgICAgIHRoaXMuZG9jdW1lbnRDb250YWluZXIuYWRkRXZlbnRMaXN0ZW5lcigncGFnZWNoYW5nZScsIHRoaXMub25QYWdlQ2hhbmdlLCB0cnVlKTtcclxuICAgICAgICAgICAgLy8gY3NwZWxsOiBkaXNhYmxlLW5leHRcclxuICAgICAgICAgICAgdGhpcy5kb2N1bWVudENvbnRhaW5lci5hZGRFdmVudExpc3RlbmVyKCdwYWdlc2xvYWRlZCcsIHRoaXMub25QYWdlc0xvYWRlZCwgdHJ1ZSk7XHJcbiAgICAgICAgICAgIC8vIGNzcGVsbDogZGlzYWJsZS1uZXh0XHJcbiAgICAgICAgICAgIHRoaXMuZG9jdW1lbnRDb250YWluZXIuYWRkRXZlbnRMaXN0ZW5lcigndGV4dGxheWVycmVuZGVyZWQnLCB0aGlzLm9uUGFnZVJlbmRlcmVkLCB0cnVlKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMucGRmVmlld2VyID0gbmV3IHBkZmpzVmlld2VyLlBERlZpZXdlcih7XHJcbiAgICAgICAgICAgICAgICBjb250YWluZXI6IHRoaXMuZG9jdW1lbnRDb250YWluZXIsXHJcbiAgICAgICAgICAgICAgICB2aWV3ZXI6IHZpZXdlcixcclxuICAgICAgICAgICAgICAgIHJlbmRlcmluZ1F1ZXVlOiB0aGlzLnJlbmRlcmluZ1F1ZXVlU2VydmljZXNcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnJlbmRlcmluZ1F1ZXVlU2VydmljZXMuc2V0Vmlld2VyKHRoaXMucGRmVmlld2VyKTtcclxuICAgICAgICAgICAgdGhpcy5wZGZWaWV3ZXIuc2V0RG9jdW1lbnQocGRmRG9jdW1lbnQpO1xyXG4gICAgICAgICAgICB0aGlzLnBkZlRodW1ibmFpbHNDb250ZXh0LnZpZXdlciA9IHRoaXMucGRmVmlld2VyO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICBpZiAodGhpcy5kb2N1bWVudENvbnRhaW5lcikge1xyXG4gICAgICAgICAgICAvLyBjc3BlbGw6IGRpc2FibGUtbmV4dFxyXG4gICAgICAgICAgICB0aGlzLmRvY3VtZW50Q29udGFpbmVyLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3BhZ2VjaGFuZ2UnLCB0aGlzLm9uUGFnZUNoYW5nZSwgdHJ1ZSk7XHJcbiAgICAgICAgICAgIC8vIGNzcGVsbDogZGlzYWJsZS1uZXh0XHJcbiAgICAgICAgICAgIHRoaXMuZG9jdW1lbnRDb250YWluZXIucmVtb3ZlRXZlbnRMaXN0ZW5lcigncGFnZXNsb2FkZWQnLCB0aGlzLm9uUGFnZXNMb2FkZWQsIHRydWUpO1xyXG4gICAgICAgICAgICAvLyBjc3BlbGw6IGRpc2FibGUtbmV4dFxyXG4gICAgICAgICAgICB0aGlzLmRvY3VtZW50Q29udGFpbmVyLnJlbW92ZUV2ZW50TGlzdGVuZXIoJ3RleHRsYXllcnJlbmRlcmVkJywgdGhpcy5vblBhZ2VSZW5kZXJlZCwgdHJ1ZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5sb2FkaW5nVGFzaykge1xyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkaW5nVGFzay5kZXN0cm95KCk7XHJcbiAgICAgICAgICAgIH0gY2F0Y2gge1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB0aGlzLmxvYWRpbmdUYXNrID0gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgdG9nZ2xlVGh1bWJuYWlscygpIHtcclxuICAgICAgICB0aGlzLnNob3dUaHVtYm5haWxzID0gIXRoaXMuc2hvd1RodW1ibmFpbHM7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBNZXRob2QgdG8gc2NhbGUgdGhlIHBhZ2UgY3VycmVudCBzdXBwb3J0IGltcGxlbWVudGF0aW9uXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHNjYWxlTW9kZSAtIG5ldyBzY2FsZSBtb2RlXHJcbiAgICAgKi9cclxuICAgIHNjYWxlUGFnZShzY2FsZU1vZGUpIHtcclxuICAgICAgICB0aGlzLmN1cnJlbnRTY2FsZU1vZGUgPSBzY2FsZU1vZGU7XHJcblxyXG4gICAgICAgIGNvbnN0IHZpZXdlckNvbnRhaW5lciA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGAke3RoaXMucmFuZG9tUGRmSWR9LXZpZXdlci1tYWluLWNvbnRhaW5lcmApO1xyXG4gICAgICAgIGNvbnN0IGRvY3VtZW50Q29udGFpbmVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoYCR7dGhpcy5yYW5kb21QZGZJZH0tdmlld2VyLXBkZi12aWV3ZXJgKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMucGRmVmlld2VyICYmIGRvY3VtZW50Q29udGFpbmVyKSB7XHJcblxyXG4gICAgICAgICAgICBsZXQgd2lkdGhDb250YWluZXI7XHJcbiAgICAgICAgICAgIGxldCBoZWlnaHRDb250YWluZXI7XHJcblxyXG4gICAgICAgICAgICBpZiAodmlld2VyQ29udGFpbmVyICYmIHZpZXdlckNvbnRhaW5lci5jbGllbnRXaWR0aCA8PSBkb2N1bWVudENvbnRhaW5lci5jbGllbnRXaWR0aCkge1xyXG4gICAgICAgICAgICAgICAgd2lkdGhDb250YWluZXIgPSB2aWV3ZXJDb250YWluZXIuY2xpZW50V2lkdGg7XHJcbiAgICAgICAgICAgICAgICBoZWlnaHRDb250YWluZXIgPSB2aWV3ZXJDb250YWluZXIuY2xpZW50SGVpZ2h0O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgd2lkdGhDb250YWluZXIgPSBkb2N1bWVudENvbnRhaW5lci5jbGllbnRXaWR0aDtcclxuICAgICAgICAgICAgICAgIGhlaWdodENvbnRhaW5lciA9IGRvY3VtZW50Q29udGFpbmVyLmNsaWVudEhlaWdodDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgY3VycmVudFBhZ2UgPSB0aGlzLnBkZlZpZXdlci5fcGFnZXNbdGhpcy5wZGZWaWV3ZXIuX2N1cnJlbnRQYWdlTnVtYmVyIC0gMV07XHJcblxyXG4gICAgICAgICAgICBjb25zdCBwYWRkaW5nID0gMjA7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhZ2VXaWR0aFNjYWxlID0gKHdpZHRoQ29udGFpbmVyIC0gcGFkZGluZykgLyBjdXJyZW50UGFnZS53aWR0aCAqIGN1cnJlbnRQYWdlLnNjYWxlO1xyXG4gICAgICAgICAgICBjb25zdCBwYWdlSGVpZ2h0U2NhbGUgPSAoaGVpZ2h0Q29udGFpbmVyIC0gcGFkZGluZykgLyBjdXJyZW50UGFnZS53aWR0aCAqIGN1cnJlbnRQYWdlLnNjYWxlO1xyXG5cclxuICAgICAgICAgICAgbGV0IHNjYWxlID0gdGhpcy5nZXRVc2VyU2NhbGluZygpO1xyXG4gICAgICAgICAgICBpZiAoIXNjYWxlKSB7XHJcbiAgICAgICAgICAgICAgICBzd2l0Y2ggKHRoaXMuY3VycmVudFNjYWxlTW9kZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ3BhZ2UtYWN0dWFsJzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2NhbGUgPSAxO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdwYWdlLXdpZHRoJzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2NhbGUgPSBwYWdlV2lkdGhTY2FsZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAncGFnZS1oZWlnaHQnOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzY2FsZSA9IHBhZ2VIZWlnaHRTY2FsZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAncGFnZS1maXQnOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzY2FsZSA9IE1hdGgubWluKHBhZ2VXaWR0aFNjYWxlLCBwYWdlSGVpZ2h0U2NhbGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdhdXRvJzpcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGV0IGhvcml6b250YWxTY2FsZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRoaXMuaXNMYW5kc2NhcGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhvcml6b250YWxTY2FsZSA9IE1hdGgubWluKHBhZ2VIZWlnaHRTY2FsZSwgcGFnZVdpZHRoU2NhbGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaG9yaXpvbnRhbFNjYWxlID0gcGFnZVdpZHRoU2NhbGU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgaG9yaXpvbnRhbFNjYWxlID0gTWF0aC5yb3VuZChob3Jpem9udGFsU2NhbGUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzY2FsZSA9IE1hdGgubWluKHRoaXMuTUFYX0FVVE9fU0NBTEUsIGhvcml6b250YWxTY2FsZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZXJyb3IoJ3BkZlZpZXdTZXRTY2FsZTogXFwnJyArIHNjYWxlTW9kZSArICdcXCcgaXMgYW4gdW5rbm93biB6b29tIHZhbHVlLicpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTY2FsZVVwZGF0ZVBhZ2VzKHNjYWxlKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY3VycmVudFNjYWxlID0gMDtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2V0U2NhbGVVcGRhdGVQYWdlcyhzY2FsZSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBVcGRhdGUgYWxsIHRoZSBwYWdlcyB3aXRoIHRoZSBuZXdTY2FsZSBzY2FsZVxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBuZXdTY2FsZSAtIG5ldyBzY2FsZSBwYWdlXHJcbiAgICAgKi9cclxuICAgIHNldFNjYWxlVXBkYXRlUGFnZXMobmV3U2NhbGU6IG51bWJlcikge1xyXG4gICAgICAgIGlmICh0aGlzLnBkZlZpZXdlcikge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuaXNTYW1lU2NhbGUodGhpcy5jdXJyZW50U2NhbGUsIG5ld1NjYWxlKSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50U2NhbGUgPSBuZXdTY2FsZTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnBkZlZpZXdlci5fcGFnZXMuZm9yRWFjaChmdW5jdGlvbiAoY3VycmVudFBhZ2UpIHtcclxuICAgICAgICAgICAgICAgICAgICBjdXJyZW50UGFnZS51cGRhdGUobmV3U2NhbGUpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMucGRmVmlld2VyLnVwZGF0ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrIGlmIHRoZSByZXF1ZXN0IHNjYWxlIG9mIHRoZSBwYWdlIGlzIHRoZSBzYW1lIGZvciBhdm9pZCB1c2VsZXNzIHJlLXJlbmRlcmluZ1xyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBvbGRTY2FsZSAtIG9sZCBzY2FsZSBwYWdlXHJcbiAgICAgKiBAcGFyYW0gbmV3U2NhbGUgLSBuZXcgc2NhbGUgcGFnZVxyXG4gICAgICpcclxuICAgICAqL1xyXG4gICAgaXNTYW1lU2NhbGUob2xkU2NhbGU6IG51bWJlciwgbmV3U2NhbGU6IG51bWJlcik6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiAobmV3U2NhbGUgPT09IG9sZFNjYWxlKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrIGlmIGlzIGEgbGFuZCBzY2FwZSB2aWV3XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHdpZHRoXHJcbiAgICAgKiBAcGFyYW0gaGVpZ2h0XHJcbiAgICAgKi9cclxuICAgIGlzTGFuZHNjYXBlKHdpZHRoOiBudW1iZXIsIGhlaWdodDogbnVtYmVyKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuICh3aWR0aCA+IGhlaWdodCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBNZXRob2QgdHJpZ2dlcmVkIHdoZW4gdGhlIHBhZ2UgaXMgcmVzaXplZFxyXG4gICAgICovXHJcbiAgICBvblJlc2l6ZSgpIHtcclxuICAgICAgICB0aGlzLnNjYWxlUGFnZSh0aGlzLmN1cnJlbnRTY2FsZU1vZGUpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogdG9nZ2xlIHRoZSBmaXQgcGFnZSBwZGZcclxuICAgICAqL1xyXG4gICAgcGFnZUZpdCgpIHtcclxuICAgICAgICBpZiAodGhpcy5jdXJyZW50U2NhbGVNb2RlICE9PSAncGFnZS1maXQnKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2NhbGVQYWdlKCdwYWdlLWZpdCcpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuc2NhbGVQYWdlKCdhdXRvJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogem9vbSBpbiBwYWdlIHBkZlxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSB0aWNrc1xyXG4gICAgICovXHJcbiAgICB6b29tSW4odGlja3M/OiBudW1iZXIpIHtcclxuICAgICAgICBsZXQgbmV3U2NhbGU6IGFueSA9IHRoaXMuY3VycmVudFNjYWxlO1xyXG4gICAgICAgIGRvIHtcclxuICAgICAgICAgICAgbmV3U2NhbGUgPSAobmV3U2NhbGUgKiB0aGlzLkRFRkFVTFRfU0NBTEVfREVMVEEpLnRvRml4ZWQoMik7XHJcbiAgICAgICAgICAgIG5ld1NjYWxlID0gTWF0aC5jZWlsKG5ld1NjYWxlICogMTApIC8gMTA7XHJcbiAgICAgICAgICAgIG5ld1NjYWxlID0gTWF0aC5taW4odGhpcy5NQVhfU0NBTEUsIG5ld1NjYWxlKTtcclxuICAgICAgICB9IHdoaWxlICgtLXRpY2tzID4gMCAmJiBuZXdTY2FsZSA8IHRoaXMuTUFYX1NDQUxFKTtcclxuICAgICAgICB0aGlzLmN1cnJlbnRTY2FsZU1vZGUgPSAnYXV0byc7XHJcbiAgICAgICAgdGhpcy5zZXRTY2FsZVVwZGF0ZVBhZ2VzKG5ld1NjYWxlKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIHpvb20gb3V0IHBhZ2UgcGRmXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHRpY2tzXHJcbiAgICAgKi9cclxuICAgIHpvb21PdXQodGlja3M/OiBudW1iZXIpIHtcclxuICAgICAgICBsZXQgbmV3U2NhbGU6IGFueSA9IHRoaXMuY3VycmVudFNjYWxlO1xyXG4gICAgICAgIGRvIHtcclxuICAgICAgICAgICAgbmV3U2NhbGUgPSAobmV3U2NhbGUgLyB0aGlzLkRFRkFVTFRfU0NBTEVfREVMVEEpLnRvRml4ZWQoMik7XHJcbiAgICAgICAgICAgIG5ld1NjYWxlID0gTWF0aC5mbG9vcihuZXdTY2FsZSAqIDEwKSAvIDEwO1xyXG4gICAgICAgICAgICBuZXdTY2FsZSA9IE1hdGgubWF4KHRoaXMuTUlOX1NDQUxFLCBuZXdTY2FsZSk7XHJcbiAgICAgICAgfSB3aGlsZSAoLS10aWNrcyA+IDAgJiYgbmV3U2NhbGUgPiB0aGlzLk1JTl9TQ0FMRSk7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50U2NhbGVNb2RlID0gJ2F1dG8nO1xyXG4gICAgICAgIHRoaXMuc2V0U2NhbGVVcGRhdGVQYWdlcyhuZXdTY2FsZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBsb2FkIHRoZSBwcmV2aW91cyBwYWdlXHJcbiAgICAgKi9cclxuICAgIHByZXZpb3VzUGFnZSgpIHtcclxuICAgICAgICBpZiAodGhpcy5wZGZWaWV3ZXIgJiYgdGhpcy5wYWdlID4gMSkge1xyXG4gICAgICAgICAgICB0aGlzLnBhZ2UtLTtcclxuICAgICAgICAgICAgdGhpcy5kaXNwbGF5UGFnZSA9IHRoaXMucGFnZTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMucGRmVmlld2VyLmN1cnJlbnRQYWdlTnVtYmVyID0gdGhpcy5wYWdlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIGxvYWQgdGhlIG5leHQgcGFnZVxyXG4gICAgICovXHJcbiAgICBuZXh0UGFnZSgpIHtcclxuICAgICAgICBpZiAodGhpcy5wZGZWaWV3ZXIgJiYgdGhpcy5wYWdlIDwgdGhpcy50b3RhbFBhZ2VzKSB7XHJcbiAgICAgICAgICAgIHRoaXMucGFnZSsrO1xyXG4gICAgICAgICAgICB0aGlzLmRpc3BsYXlQYWdlID0gdGhpcy5wYWdlO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5wZGZWaWV3ZXIuY3VycmVudFBhZ2VOdW1iZXIgPSB0aGlzLnBhZ2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogbG9hZCB0aGUgcGFnZSBpbiBpbnB1dFxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBwYWdlIHRvIGxvYWRcclxuICAgICAqL1xyXG4gICAgaW5wdXRQYWdlKHBhZ2U6IHN0cmluZykge1xyXG4gICAgICAgIGNvbnN0IHBhZ2VJbnB1dCA9IHBhcnNlSW50KHBhZ2UsIDEwKTtcclxuXHJcbiAgICAgICAgaWYgKCFpc05hTihwYWdlSW5wdXQpICYmIHBhZ2VJbnB1dCA+IDAgJiYgcGFnZUlucHV0IDw9IHRoaXMudG90YWxQYWdlcykge1xyXG4gICAgICAgICAgICB0aGlzLnBhZ2UgPSBwYWdlSW5wdXQ7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheVBhZ2UgPSB0aGlzLnBhZ2U7XHJcbiAgICAgICAgICAgIHRoaXMucGRmVmlld2VyLmN1cnJlbnRQYWdlTnVtYmVyID0gdGhpcy5wYWdlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheVBhZ2UgPSB0aGlzLnBhZ2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUGFnZSBDaGFuZ2UgRXZlbnRcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqL1xyXG4gICAgb25QYWdlQ2hhbmdlKGV2ZW50KSB7XHJcbiAgICAgICAgdGhpcy5wYWdlID0gZXZlbnQucGFnZU51bWJlcjtcclxuICAgICAgICB0aGlzLmRpc3BsYXlQYWdlID0gZXZlbnQucGFnZU51bWJlcjtcclxuICAgIH1cclxuXHJcbiAgICBvblBkZlBhc3N3b3JkKGNhbGxiYWNrLCByZWFzb24pIHtcclxuICAgICAgICB0aGlzLmRpYWxvZ1xyXG4gICAgICAgICAgICAub3BlbihQZGZQYXNzd29yZERpYWxvZ0NvbXBvbmVudCwge1xyXG4gICAgICAgICAgICAgICAgd2lkdGg6ICc0MDBweCcsXHJcbiAgICAgICAgICAgICAgICBkYXRhOiB7IHJlYXNvbiB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5hZnRlckNsb3NlZCgpLnN1YnNjcmliZSgocGFzc3dvcmQpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChwYXNzd29yZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrKHBhc3N3b3JkKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jbG9zZS5lbWl0KCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBQYWdlIFJlbmRlcmVkIEV2ZW50XHJcbiAgICAgKi9cclxuICAgIG9uUGFnZVJlbmRlcmVkKCkge1xyXG4gICAgICAgIHRoaXMucmVuZGVyZWQuZW1pdCgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUGFnZXMgTG9hZGVkIEV2ZW50XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIGV2ZW50XHJcbiAgICAgKi9cclxuICAgIG9uUGFnZXNMb2FkZWQoZXZlbnQpIHtcclxuICAgICAgICB0aGlzLmlzUGFuZWxEaXNhYmxlZCA9IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogS2V5Ym9hcmQgRXZlbnQgTGlzdGVuZXJcclxuICAgICAqIEBwYXJhbSBLZXlib2FyZEV2ZW50IGV2ZW50XHJcbiAgICAgKi9cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2RvY3VtZW50OmtleWRvd24nLCBbJyRldmVudCddKVxyXG4gICAgaGFuZGxlS2V5Ym9hcmRFdmVudChldmVudDogS2V5Ym9hcmRFdmVudCkge1xyXG4gICAgICAgIGNvbnN0IGtleSA9IGV2ZW50LmtleUNvZGU7XHJcbiAgICAgICAgaWYgKGtleSA9PT0gMzkpIHsgLy8gcmlnaHQgYXJyb3dcclxuICAgICAgICAgICAgdGhpcy5uZXh0UGFnZSgpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoa2V5ID09PSAzNykgey8vIGxlZnQgYXJyb3dcclxuICAgICAgICAgICAgdGhpcy5wcmV2aW91c1BhZ2UoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZW5lcmF0ZVV1aWQoKSB7XHJcbiAgICAgICAgcmV0dXJuICd4eHh4eHh4eC14eHh4LTR4eHgteXh4eC14eHh4eHh4eHh4eHgnLnJlcGxhY2UoL1t4eV0vZywgZnVuY3Rpb24gKGMpIHtcclxuICAgICAgICAgICAgY29uc3QgciA9IE1hdGgucmFuZG9tKCkgKiAxNiB8IDAsIHYgPSBjID09PSAneCcgPyByIDogKHIgJiAweDMgfCAweDgpO1xyXG4gICAgICAgICAgICByZXR1cm4gdi50b1N0cmluZygxNik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuIl19