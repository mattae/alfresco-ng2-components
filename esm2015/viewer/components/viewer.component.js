/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ContentChild, ElementRef, EventEmitter, HostListener, Input, Output, TemplateRef, ViewEncapsulation } from '@angular/core';
import { BaseEvent } from '../../events';
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import { ViewerMoreActionsComponent } from './viewer-more-actions.component';
import { ViewerOpenWithComponent } from './viewer-open-with.component';
import { ViewerSidebarComponent } from './viewer-sidebar.component';
import { ViewerToolbarComponent } from './viewer-toolbar.component';
import { ViewUtilService } from '../services/view-util.service';
import { AppExtensionService } from '@alfresco/adf-extensions';
export class ViewerComponent {
    /**
     * @param {?} apiService
     * @param {?} viewUtils
     * @param {?} logService
     * @param {?} extensionService
     * @param {?} el
     */
    constructor(apiService, viewUtils, logService, extensionService, el) {
        this.apiService = apiService;
        this.viewUtils = viewUtils;
        this.logService = logService;
        this.extensionService = extensionService;
        this.el = el;
        /**
         * If you want to load an external file that does not come from ACS you
         * can use this URL to specify where to load the file from.
         */
        this.urlFile = '';
        /**
         * Viewer to use with the `urlFile` address (`pdf`, `image`, `media`, `text`).
         * Used when `urlFile` has no filename and extension.
         */
        this.urlFileViewer = null;
        /**
         * Node Id of the file to load.
         */
        this.nodeId = null;
        /**
         * Shared link id (to display shared file).
         */
        this.sharedLinkId = null;
        /**
         * If `true` then show the Viewer as a full page over the current content.
         * Otherwise fit inside the parent div.
         */
        this.overlayMode = false;
        /**
         * Hide or show the viewer
         */
        this.showViewer = true;
        /**
         * Hide or show the toolbar
         */
        this.showToolbar = true;
        /** @deprecated 3.2.0 */
        /**
         * Allows `back` navigation
         */
        this.allowGoBack = true;
        /**
         * Toggles downloading.
         */
        this.allowDownload = true;
        /**
         * Toggles printing.
         */
        this.allowPrint = false;
        /**
         * Toggles the 'Full Screen' feature.
         */
        this.allowFullScreen = true;
        /**
         * Toggles before/next navigation. You can use the arrow buttons to navigate
         * between documents in the collection.
         */
        this.allowNavigate = false;
        /**
         * Toggles the "before" ("<") button. Requires `allowNavigate` to be enabled.
         */
        this.canNavigateBefore = true;
        /**
         * Toggles the next (">") button. Requires `allowNavigate` to be enabled.
         */
        this.canNavigateNext = true;
        /**
         * Allow the left the sidebar.
         */
        this.allowLeftSidebar = false;
        /**
         * Allow the right sidebar.
         */
        this.allowRightSidebar = false;
        /**
         * Toggles PDF thumbnails.
         */
        this.allowThumbnails = true;
        /**
         * Toggles right sidebar visibility. Requires `allowRightSidebar` to be set to `true`.
         */
        this.showRightSidebar = false;
        /**
         * Toggles left sidebar visibility. Requires `allowLeftSidebar` to be set to `true`.
         */
        this.showLeftSidebar = false;
        /**
         * The template for the right sidebar. The template context contains the loaded node data.
         */
        this.sidebarRightTemplate = null;
        /**
         * The template for the left sidebar. The template context contains the loaded node data.
         */
        this.sidebarLeftTemplate = null;
        /**
         * The template for the pdf thumbnails.
         */
        this.thumbnailsTemplate = null;
        /**
         * Number of times the Viewer will retry fetching content Rendition.
         * There is a delay of at least one second between attempts.
         */
        this.maxRetries = 10;
        /**
         * Emitted when user clicks the 'Back' button.
         */
        this.goBack = new EventEmitter();
        /**
         * Emitted when user clicks the 'Print' button.
         */
        this.print = new EventEmitter();
        /**
         * Emitted when the viewer is shown or hidden.
         */
        this.showViewerChange = new EventEmitter();
        /**
         * Emitted when the filename extension changes.
         */
        this.extensionChange = new EventEmitter();
        /**
         * Emitted when user clicks 'Navigate Before' ("<") button.
         */
        this.navigateBefore = new EventEmitter();
        /**
         * Emitted when user clicks 'Navigate Next' (">") button.
         */
        this.navigateNext = new EventEmitter();
        /**
         * Emitted when the shared link used is not valid.
         */
        this.invalidSharedLink = new EventEmitter();
        this.TRY_TIMEOUT = 2000;
        this.viewerType = 'unknown';
        this.isLoading = false;
        this.extensionTemplates = [];
        this.externalExtensions = [];
        this.sidebarRightTemplateContext = { node: null };
        this.sidebarLeftTemplateContext = { node: null };
        this.viewerExtensions = [];
        this.subscriptions = [];
        // Extensions that are supported by the Viewer without conversion
        this.extensions = {
            image: ['png', 'jpg', 'jpeg', 'gif', 'bpm', 'svg'],
            media: ['wav', 'mp4', 'mp3', 'webm', 'ogg'],
            text: ['txt', 'xml', 'html', 'json', 'ts', 'css', 'md'],
            pdf: ['pdf']
        };
        // Mime types that are supported by the Viewer without conversion
        this.mimeTypes = {
            text: ['text/plain', 'text/csv', 'text/xml', 'text/html', 'application/x-javascript'],
            pdf: ['application/pdf'],
            image: ['image/png', 'image/jpeg', 'image/gif', 'image/bmp', 'image/svg+xml'],
            media: ['video/mp4', 'video/webm', 'video/ogg', 'audio/mpeg', 'audio/ogg', 'audio/wav']
        };
    }
    /**
     * @return {?}
     */
    isSourceDefined() {
        return (this.urlFile || this.blobFile || this.nodeId || this.sharedLinkId) ? true : false;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.subscriptions.push(this.apiService.nodeUpdated.subscribe((/**
         * @param {?} node
         * @return {?}
         */
        (node) => this.onNodeUpdated(node))));
        this.loadExtensions();
    }
    /**
     * @private
     * @return {?}
     */
    loadExtensions() {
        this.viewerExtensions = this.extensionService.getViewerExtensions();
        this.viewerExtensions
            .forEach((/**
         * @param {?} extension
         * @return {?}
         */
        (extension) => {
            this.externalExtensions.push(extension.fileExtension);
        }));
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.subscriptions.forEach((/**
         * @param {?} subscription
         * @return {?}
         */
        (subscription) => subscription.unsubscribe()));
        this.subscriptions = [];
    }
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    onNodeUpdated(node) {
        if (node && node.id === this.nodeId) {
            this.generateCacheBusterNumber();
            this.isLoading = true;
            this.setUpNodeFile(node).then((/**
             * @return {?}
             */
            () => {
                this.isLoading = false;
            }));
        }
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ngOnChanges(changes) {
        if (this.showViewer) {
            if (!this.isSourceDefined()) {
                throw new Error('A content source attribute value is missing.');
            }
            this.isLoading = true;
            if (this.blobFile) {
                this.setUpBlobData();
                this.isLoading = false;
            }
            else if (this.urlFile) {
                this.setUpUrlFile();
                this.isLoading = false;
            }
            else if (this.nodeId) {
                this.apiService.nodesApi.getNode(this.nodeId, { include: ['allowableOperations'] }).then((/**
                 * @param {?} node
                 * @return {?}
                 */
                (node) => {
                    this.nodeEntry = node;
                    this.setUpNodeFile(node.entry).then((/**
                     * @return {?}
                     */
                    () => {
                        this.isLoading = false;
                    }));
                }), (/**
                 * @param {?} error
                 * @return {?}
                 */
                (error) => {
                    this.isLoading = false;
                    this.logService.error('This node does not exist');
                }));
            }
            else if (this.sharedLinkId) {
                this.allowGoBack = false;
                this.apiService.sharedLinksApi.getSharedLink(this.sharedLinkId).then((/**
                 * @param {?} sharedLinkEntry
                 * @return {?}
                 */
                (sharedLinkEntry) => {
                    this.setUpSharedLinkFile(sharedLinkEntry);
                    this.isLoading = false;
                }), (/**
                 * @return {?}
                 */
                () => {
                    this.isLoading = false;
                    this.logService.error('This sharedLink does not exist');
                    this.invalidSharedLink.next();
                }));
            }
        }
    }
    /**
     * @private
     * @return {?}
     */
    setUpBlobData() {
        this.fileTitle = this.getDisplayName('Unknown');
        this.mimeType = this.blobFile.type;
        this.viewerType = this.getViewerTypeByMimeType(this.mimeType);
        this.allowDownload = false;
        // TODO: wrap blob into the data url and allow downloading
        this.extensionChange.emit(this.mimeType);
        this.scrollTop();
    }
    /**
     * @private
     * @return {?}
     */
    setUpUrlFile() {
        /** @type {?} */
        const filenameFromUrl = this.getFilenameFromUrl(this.urlFile);
        this.fileTitle = this.getDisplayName(filenameFromUrl);
        this.extension = this.getFileExtension(filenameFromUrl);
        this.urlFileContent = this.urlFile;
        this.fileName = this.displayName;
        this.viewerType = this.urlFileViewer || this.getViewerTypeByExtension(this.extension);
        if (this.viewerType === 'unknown') {
            this.viewerType = this.getViewerTypeByMimeType(this.mimeType);
        }
        this.extensionChange.emit(this.extension);
        this.scrollTop();
    }
    /**
     * @private
     * @param {?} data
     * @return {?}
     */
    setUpNodeFile(data) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            let setupNode;
            if (data.content) {
                this.mimeType = data.content.mimeType;
            }
            this.fileTitle = this.getDisplayName(data.name);
            this.urlFileContent = this.apiService.contentApi.getContentUrl(data.id);
            this.urlFileContent = this.cacheBusterNumber ? this.urlFileContent + '&' + this.cacheBusterNumber : this.urlFileContent;
            this.extension = this.getFileExtension(data.name);
            this.fileName = data.name;
            this.viewerType = this.getViewerTypeByExtension(this.extension);
            if (this.viewerType === 'unknown') {
                this.viewerType = this.getViewerTypeByMimeType(this.mimeType);
            }
            if (this.viewerType === 'unknown') {
                setupNode = this.displayNodeRendition(data.id);
            }
            this.extensionChange.emit(this.extension);
            this.sidebarRightTemplateContext.node = data;
            this.sidebarLeftTemplateContext.node = data;
            this.scrollTop();
            return setupNode;
        });
    }
    /**
     * @private
     * @param {?} details
     * @return {?}
     */
    setUpSharedLinkFile(details) {
        this.mimeType = details.entry.content.mimeType;
        this.fileTitle = this.getDisplayName(details.entry.name);
        this.extension = this.getFileExtension(details.entry.name);
        this.fileName = details.entry.name;
        this.urlFileContent = this.apiService.contentApi.getSharedLinkContentUrl(this.sharedLinkId, false);
        this.viewerType = this.getViewerTypeByMimeType(this.mimeType);
        if (this.viewerType === 'unknown') {
            this.viewerType = this.getViewerTypeByExtension(this.extension);
        }
        if (this.viewerType === 'unknown') {
            this.displaySharedLinkRendition(this.sharedLinkId);
        }
        this.extensionChange.emit(this.extension);
    }
    /**
     * @return {?}
     */
    toggleSidebar() {
        this.showRightSidebar = !this.showRightSidebar;
        if (this.showRightSidebar && this.nodeId) {
            this.apiService.getInstance().nodes.getNode(this.nodeId, { include: ['allowableOperations'] })
                .then((/**
             * @param {?} nodeEntry
             * @return {?}
             */
            (nodeEntry) => {
                this.sidebarRightTemplateContext.node = nodeEntry.entry;
            }));
        }
    }
    /**
     * @return {?}
     */
    toggleLeftSidebar() {
        this.showLeftSidebar = !this.showLeftSidebar;
        if (this.showRightSidebar && this.nodeId) {
            this.apiService.getInstance().nodes.getNode(this.nodeId, { include: ['allowableOperations'] })
                .then((/**
             * @param {?} nodeEntry
             * @return {?}
             */
            (nodeEntry) => {
                this.sidebarLeftTemplateContext.node = nodeEntry.entry;
            }));
        }
    }
    /**
     * @private
     * @param {?} name
     * @return {?}
     */
    getDisplayName(name) {
        return this.displayName || name;
    }
    /**
     * @return {?}
     */
    scrollTop() {
        window.scrollTo(0, 1);
    }
    /**
     * @param {?} mimeType
     * @return {?}
     */
    getViewerTypeByMimeType(mimeType) {
        if (mimeType) {
            mimeType = mimeType.toLowerCase();
            /** @type {?} */
            const editorTypes = Object.keys(this.mimeTypes);
            for (const type of editorTypes) {
                if (this.mimeTypes[type].indexOf(mimeType) >= 0) {
                    return type;
                }
            }
        }
        return 'unknown';
    }
    /**
     * @param {?} extension
     * @return {?}
     */
    getViewerTypeByExtension(extension) {
        if (extension) {
            extension = extension.toLowerCase();
        }
        if (this.isCustomViewerExtension(extension)) {
            return 'custom';
        }
        if (this.extensions.image.indexOf(extension) >= 0) {
            return 'image';
        }
        if (this.extensions.media.indexOf(extension) >= 0) {
            return 'media';
        }
        if (this.extensions.text.indexOf(extension) >= 0) {
            return 'text';
        }
        if (this.extensions.pdf.indexOf(extension) >= 0) {
            return 'pdf';
        }
        return 'unknown';
    }
    /**
     * @return {?}
     */
    onBackButtonClick() {
        this.close();
    }
    /**
     * @return {?}
     */
    onNavigateBeforeClick() {
        this.navigateBefore.next();
    }
    /**
     * @return {?}
     */
    onNavigateNextClick() {
        this.navigateNext.next();
    }
    /**
     * close the viewer
     * @return {?}
     */
    close() {
        if (this.otherMenu) {
            this.otherMenu.hidden = false;
        }
        this.showViewer = false;
        this.showViewerChange.emit(this.showViewer);
    }
    /**
     * get File name from url
     *
     * @param {?} url - url file
     * @return {?}
     */
    getFilenameFromUrl(url) {
        /** @type {?} */
        const anchor = url.indexOf('#');
        /** @type {?} */
        const query = url.indexOf('?');
        /** @type {?} */
        const end = Math.min(anchor > 0 ? anchor : url.length, query > 0 ? query : url.length);
        return url.substring(url.lastIndexOf('/', end) + 1, end);
    }
    /**
     * Get file extension from the string.
     * Supports the URL formats like:
     * http://localhost/test.jpg?cache=1000
     * http://localhost/test.jpg#cache=1000
     *
     * @param {?} fileName - file name
     * @return {?}
     */
    getFileExtension(fileName) {
        if (fileName) {
            /** @type {?} */
            const match = fileName.match(/\.([^\./\?\#]+)($|\?|\#)/);
            return match ? match[1] : null;
        }
        return null;
    }
    /**
     * @param {?} extension
     * @return {?}
     */
    isCustomViewerExtension(extension) {
        /** @type {?} */
        const extensions = this.externalExtensions || [];
        if (extension && extensions.length > 0) {
            extension = extension.toLowerCase();
            return extensions.flat().indexOf(extension) >= 0;
        }
        return false;
    }
    /**
     * Keyboard event listener
     * @param {?} event
     * @return {?}
     */
    handleKeyboardEvent(event) {
        /** @type {?} */
        const key = event.keyCode;
        // Esc
        if (key === 27 && this.overlayMode) { // esc
            this.close();
        }
        // Left arrow
        if (key === 37 && this.canNavigateBefore) {
            event.preventDefault();
            this.onNavigateBeforeClick();
        }
        // Right arrow
        if (key === 39 && this.canNavigateNext) {
            event.preventDefault();
            this.onNavigateNextClick();
        }
        // Ctrl+F
        if (key === 70 && event.ctrlKey) {
            event.preventDefault();
            this.enterFullScreen();
        }
    }
    /**
     * @return {?}
     */
    printContent() {
        if (this.allowPrint) {
            /** @type {?} */
            const args = new BaseEvent();
            this.print.next(args);
            if (!args.defaultPrevented) {
                this.viewUtils.printFileGeneric(this.nodeId, this.mimeType);
            }
        }
    }
    /**
     * Triggers full screen mode with a main content area displayed.
     * @return {?}
     */
    enterFullScreen() {
        if (this.allowFullScreen) {
            /** @type {?} */
            const container = this.el.nativeElement.querySelector('.adf-viewer__fullscreen-container');
            if (container) {
                if (container.requestFullscreen) {
                    container.requestFullscreen();
                }
                else if (container.webkitRequestFullscreen) {
                    container.webkitRequestFullscreen();
                }
                else if (container.mozRequestFullScreen) {
                    container.mozRequestFullScreen();
                }
                else if (container.msRequestFullscreen) {
                    container.msRequestFullscreen();
                }
            }
        }
    }
    /**
     * @private
     * @param {?} nodeId
     * @return {?}
     */
    displayNodeRendition(nodeId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                /** @type {?} */
                const rendition = yield this.resolveRendition(nodeId, 'pdf');
                if (rendition) {
                    /** @type {?} */
                    const renditionId = rendition.entry.id;
                    if (renditionId === 'pdf') {
                        this.viewerType = 'pdf';
                    }
                    else if (renditionId === 'imgpreview') {
                        this.viewerType = 'image';
                    }
                    this.urlFileContent = this.apiService.contentApi.getRenditionUrl(nodeId, renditionId);
                }
            }
            catch (err) {
                this.logService.error(err);
            }
        });
    }
    /**
     * @private
     * @param {?} sharedId
     * @return {?}
     */
    displaySharedLinkRendition(sharedId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            try {
                /** @type {?} */
                const rendition = yield this.apiService.renditionsApi.getSharedLinkRendition(sharedId, 'pdf');
                if (rendition.entry.status.toString() === 'CREATED') {
                    this.viewerType = 'pdf';
                    this.urlFileContent = this.apiService.contentApi.getSharedLinkRenditionUrl(sharedId, 'pdf');
                }
            }
            catch (error) {
                this.logService.error(error);
                try {
                    /** @type {?} */
                    const rendition = yield this.apiService.renditionsApi.getSharedLinkRendition(sharedId, 'imgpreview');
                    if (rendition.entry.status.toString() === 'CREATED') {
                        this.viewerType = 'image';
                        this.urlFileContent = this.apiService.contentApi.getSharedLinkRenditionUrl(sharedId, 'imgpreview');
                    }
                }
                catch (error) {
                    this.logService.error(error);
                }
            }
        });
    }
    /**
     * @private
     * @param {?} nodeId
     * @param {?} renditionId
     * @return {?}
     */
    resolveRendition(nodeId, renditionId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            renditionId = renditionId.toLowerCase();
            /** @type {?} */
            const supportedRendition = yield this.apiService.renditionsApi.getRenditions(nodeId);
            /** @type {?} */
            let rendition = supportedRendition.list.entries.find((/**
             * @param {?} renditionEntry
             * @return {?}
             */
            (renditionEntry) => renditionEntry.entry.id.toLowerCase() === renditionId));
            if (!rendition) {
                renditionId = 'imgpreview';
                rendition = supportedRendition.list.entries.find((/**
                 * @param {?} renditionEntry
                 * @return {?}
                 */
                (renditionEntry) => renditionEntry.entry.id.toLowerCase() === renditionId));
            }
            if (rendition) {
                /** @type {?} */
                const status = rendition.entry.status.toString();
                if (status === 'NOT_CREATED') {
                    try {
                        yield this.apiService.renditionsApi.createRendition(nodeId, { id: renditionId }).then((/**
                         * @return {?}
                         */
                        () => {
                            this.viewerType = 'in_creation';
                        }));
                        rendition = yield this.waitRendition(nodeId, renditionId);
                    }
                    catch (err) {
                        this.logService.error(err);
                    }
                }
            }
            return rendition;
        });
    }
    /**
     * @private
     * @param {?} nodeId
     * @param {?} renditionId
     * @return {?}
     */
    waitRendition(nodeId, renditionId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            let currentRetry = 0;
            return new Promise((/**
             * @param {?} resolve
             * @param {?} reject
             * @return {?}
             */
            (resolve, reject) => {
                /** @type {?} */
                const intervalId = setInterval((/**
                 * @return {?}
                 */
                () => {
                    currentRetry++;
                    if (this.maxRetries >= currentRetry) {
                        this.apiService.renditionsApi.getRendition(nodeId, renditionId).then((/**
                         * @param {?} rendition
                         * @return {?}
                         */
                        (rendition) => {
                            /** @type {?} */
                            const status = rendition.entry.status.toString();
                            if (status === 'CREATED') {
                                if (renditionId === 'pdf') {
                                    this.viewerType = 'pdf';
                                }
                                else if (renditionId === 'imgpreview') {
                                    this.viewerType = 'image';
                                }
                                this.urlFileContent = this.apiService.contentApi.getRenditionUrl(nodeId, renditionId);
                                clearInterval(intervalId);
                                return resolve(rendition);
                            }
                        }), (/**
                         * @return {?}
                         */
                        () => {
                            this.viewerType = 'error_in_creation';
                            return reject();
                        }));
                    }
                    else {
                        this.isLoading = false;
                        this.viewerType = 'error_in_creation';
                        clearInterval(intervalId);
                    }
                }), this.TRY_TIMEOUT);
            }));
        });
    }
    /**
     * @param {?} extensionAllowed
     * @return {?}
     */
    checkExtensions(extensionAllowed) {
        if (typeof extensionAllowed === 'string') {
            return this.extension.toLowerCase() === extensionAllowed.toLowerCase();
        }
        else if (extensionAllowed.length > 0) {
            return extensionAllowed.find((/**
             * @param {?} currentExtension
             * @return {?}
             */
            (currentExtension) => {
                return this.extension.toLowerCase() === currentExtension.toLowerCase();
            }));
        }
    }
    /**
     * @private
     * @return {?}
     */
    generateCacheBusterNumber() {
        this.cacheBusterNumber = Date.now();
    }
}
ViewerComponent.decorators = [
    { type: Component, args: [{
                selector: 'adf-viewer',
                template: "<div *ngIf=\"showViewer\"\r\n     class=\"adf-viewer-container\"\r\n     [class.adf-viewer-overlay-container]=\"overlayMode\"\r\n     [class.adf-viewer-inline-container]=\"!overlayMode\">\r\n\r\n    <div class=\"adf-viewer-content\" fxLayout=\"column\">\r\n        <ng-content select=\"adf-viewer-toolbar\"></ng-content>\r\n        <ng-container *ngIf=\"showToolbar && !toolbar\">\r\n            <adf-toolbar color=\"default\" id=\"adf-viewer-toolbar\" class=\"adf-viewer-toolbar\">\r\n\r\n                <adf-toolbar-title>\r\n\r\n                    <ng-container *ngIf=\"allowLeftSidebar\">\r\n                        <button\r\n                            mat-icon-button\r\n                            title=\"{{ 'ADF_VIEWER.ACTIONS.INFO' | translate }}\"\r\n                            data-automation-id=\"adf-toolbar-left-sidebar\"\r\n                            [color]=\"showLeftSidebar ? 'accent' : 'default'\"\r\n                            (click)=\"toggleLeftSidebar()\">\r\n                            <mat-icon>info_outline</mat-icon>\r\n                        </button>\r\n                    </ng-container>\r\n\r\n                    <button *ngIf=\"allowGoBack\"\r\n                            class=\"adf-viewer-close-button\"\r\n                            data-automation-id=\"adf-toolbar-back\"\r\n                            mat-icon-button\r\n                            title=\"{{ 'ADF_VIEWER.ACTIONS.CLOSE' | translate }}\"\r\n                            (click)=\"onBackButtonClick()\">\r\n                        <mat-icon>close</mat-icon>\r\n                    </button>\r\n                </adf-toolbar-title>\r\n\r\n                <div fxFlex=\"1 1 auto\" class=\"adf-viewer__file-title\">\r\n                    <button\r\n                        *ngIf=\"allowNavigate && canNavigateBefore\"\r\n                        data-automation-id=\"adf-toolbar-pref-file\"\r\n                        mat-icon-button\r\n                        title=\"{{ 'ADF_VIEWER.ACTIONS.PREV_FILE' | translate }}\"\r\n                        (click)=\"onNavigateBeforeClick()\">\r\n                        <mat-icon>navigate_before</mat-icon>\r\n                    </button>\r\n                    <img class=\"adf-viewer__mimeicon\" [alt]=\"mimeType | adfMimeTypeIcon\" [src]=\"mimeType | adfMimeTypeIcon\" data-automation-id=\"adf-file-thumbnail\">\r\n                    <span class=\"adf-viewer__display-name\" id=\"adf-viewer-display-name\">{{ fileTitle }}</span>\r\n                    <button\r\n                        *ngIf=\"allowNavigate && canNavigateNext\"\r\n                        data-automation-id=\"adf-toolbar-next-file\"\r\n                        mat-icon-button\r\n                        title=\"{{ 'ADF_VIEWER.ACTIONS.NEXT_FILE' | translate }}\"\r\n                        (click)=\"onNavigateNextClick()\">\r\n                        <mat-icon>navigate_next</mat-icon>\r\n                    </button>\r\n                </div>\r\n\r\n                <ng-content select=\"adf-viewer-toolbar-actions\"></ng-content>\r\n\r\n                <ng-container *ngIf=\"mnuOpenWith\" data-automation-id='adf-toolbar-custom-btn'>\r\n                    <button\r\n                        id=\"adf-viewer-openwith\"\r\n                        mat-button\r\n                        [matMenuTriggerFor]=\"mnuOpenWith\"\r\n                        data-automation-id=\"adf-toolbar-open-with\">\r\n                        <span>{{ 'ADF_VIEWER.ACTIONS.OPEN_WITH' | translate }}</span>\r\n                        <mat-icon>arrow_drop_down</mat-icon>\r\n                    </button>\r\n                    <mat-menu #mnuOpenWith=\"matMenu\" [overlapTrigger]=\"false\">\r\n                        <ng-content select=\"adf-viewer-open-with\"></ng-content>\r\n                    </mat-menu>\r\n                </ng-container>\r\n\r\n                <adf-toolbar-divider></adf-toolbar-divider>\r\n\r\n                <button\r\n                    id=\"adf-viewer-download\"\r\n                    *ngIf=\"allowDownload\"\r\n                    mat-icon-button\r\n                    title=\"{{ 'ADF_VIEWER.ACTIONS.DOWNLOAD' | translate }}\"\r\n                    data-automation-id=\"adf-toolbar-download\"\r\n                    [adfNodeDownload]=\"nodeEntry\">\r\n                    <mat-icon>file_download</mat-icon>\r\n                </button>\r\n\r\n                <button\r\n                    id=\"adf-viewer-print\"\r\n                    *ngIf=\"allowPrint\"\r\n                    mat-icon-button\r\n                    title=\"{{ 'ADF_VIEWER.ACTIONS.PRINT' | translate }}\"\r\n                    data-automation-id=\"adf-toolbar-print\"\r\n                    (click)=\"printContent()\">\r\n                    <mat-icon>print</mat-icon>\r\n                </button>\r\n\r\n                <button\r\n                    id=\"adf-viewer-fullscreen\"\r\n                    *ngIf=\"viewerType !== 'media' && allowFullScreen\"\r\n                    mat-icon-button\r\n                    title=\"{{ 'ADF_VIEWER.ACTIONS.FULLSCREEN' | translate }}\"\r\n                    data-automation-id=\"adf-toolbar-fullscreen\"\r\n                    (click)=\"enterFullScreen()\">\r\n                    <mat-icon>fullscreen</mat-icon>\r\n                </button>\r\n\r\n                <ng-container *ngIf=\"allowRightSidebar\">\r\n                    <adf-toolbar-divider></adf-toolbar-divider>\r\n\r\n                    <button\r\n                        mat-icon-button\r\n                        title=\"{{ 'ADF_VIEWER.ACTIONS.INFO' | translate }}\"\r\n                        data-automation-id=\"adf-toolbar-sidebar\"\r\n                        [color]=\"showRightSidebar ? 'accent' : 'default'\"\r\n                        (click)=\"toggleSidebar()\">\r\n                        <mat-icon>info_outline</mat-icon>\r\n                    </button>\r\n\r\n                </ng-container>\r\n\r\n                <ng-container *ngIf=\"mnuMoreActions\">\r\n                    <button\r\n                        id=\"adf-viewer-moreactions\"\r\n                        mat-icon-button\r\n                        [matMenuTriggerFor]=\"mnuMoreActions\"\r\n                        title=\"{{ 'ADF_VIEWER.ACTIONS.MORE_ACTIONS' | translate }}\"\r\n                        data-automation-id=\"adf-toolbar-more-actions\">\r\n                        <mat-icon>more_vert</mat-icon>\r\n                    </button>\r\n                    <mat-menu #mnuMoreActions=\"matMenu\" [overlapTrigger]=\"false\">\r\n                        <ng-content select=\"adf-viewer-more-actions\"></ng-content>\r\n                    </mat-menu>\r\n                </ng-container>\r\n\r\n            </adf-toolbar>\r\n        </ng-container>\r\n\r\n        <div fxLayout=\"row\" fxFlex=\"1 1 auto\">\r\n            <ng-container *ngIf=\"allowRightSidebar && showRightSidebar\">\r\n                <div class=\"adf-viewer__sidebar\" [ngClass]=\"'adf-viewer__sidebar__right'\" fxFlexOrder=\"4\"  id=\"adf-right-sidebar\" >\r\n                    <ng-container *ngIf=\"sidebarRightTemplate\">\r\n                        <ng-container *ngTemplateOutlet=\"sidebarRightTemplate;context:sidebarRightTemplateContext\"></ng-container>\r\n                    </ng-container>\r\n                    <ng-content *ngIf=\"!sidebarRightTemplate\" select=\"adf-viewer-sidebar\"></ng-content>\r\n                </div>\r\n            </ng-container>\r\n\r\n            <ng-container *ngIf=\"allowLeftSidebar && showLeftSidebar\">\r\n                <div class=\"adf-viewer__sidebar\" [ngClass]=\"'adf-viewer__sidebar__left'\" fxFlexOrder=\"1\"  id=\"adf-left-sidebar\" >\r\n                    <ng-container *ngIf=\"sidebarLeftTemplate\">\r\n                        <ng-container *ngTemplateOutlet=\"sidebarLeftTemplate;context:sidebarLeftTemplateContext\"></ng-container>\r\n                    </ng-container>\r\n                    <ng-content *ngIf=\"!sidebarLeftTemplate\" select=\"adf-viewer-sidebar\"></ng-content>\r\n                </div>\r\n            </ng-container>\r\n\r\n            <div  *ngIf=\"isLoading\"  class=\"adf-viewer-main\" fxFlexOrder=\"1\" fxFlex=\"1 1 auto\">\r\n                <div class=\"adf-viewer-layout-content adf-viewer__fullscreen-container\">\r\n                    <div class=\"adf-viewer-content-container\">\r\n                        <ng-container *ngIf=\"isLoading\">\r\n                            <div class=\"adf-viewer__loading-screen\" fxFlex=\"1 1 auto\">\r\n                                <h2>{{ 'ADF_VIEWER.LOADING' | translate }}</h2>\r\n                                <div>\r\n                                    <mat-spinner></mat-spinner>\r\n                                </div>\r\n                            </div>\r\n                        </ng-container>\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div  *ngIf=\"!isLoading\"  class=\"adf-viewer-main\" fxFlexOrder=\"1\" fxFlex=\"1 1 auto\">\r\n                <div class=\"adf-viewer-layout-content adf-viewer__fullscreen-container\">\r\n                    <div class=\"adf-viewer-content-container\" [ngSwitch]=\"viewerType\">\r\n\r\n                        <ng-container *ngSwitchCase=\"'pdf'\">\r\n                            <adf-pdf-viewer (close)=\"onBackButtonClick()\" [thumbnailsTemplate]=\"thumbnailsTemplate\" [allowThumbnails]=\"allowThumbnails\" [blobFile]=\"blobFile\" [urlFile]=\"urlFileContent\" [nameFile]=\"displayName\"></adf-pdf-viewer>\r\n                        </ng-container>\r\n\r\n                        <ng-container *ngSwitchCase=\"'image'\">\r\n                            <adf-img-viewer [urlFile]=\"urlFileContent\" [nameFile]=\"displayName\" [blobFile]=\"blobFile\"></adf-img-viewer>\r\n                        </ng-container>\r\n\r\n                        <ng-container *ngSwitchCase=\"'media'\">\r\n                            <adf-media-player id=\"adf-mdedia-player\" [urlFile]=\"urlFileContent\" [mimeType]=\"mimeType\" [blobFile]=\"blobFile\" [nameFile]=\"displayName\"></adf-media-player>\r\n                        </ng-container>\r\n\r\n                        <ng-container *ngSwitchCase=\"'text'\">\r\n                            <adf-txt-viewer [urlFile]=\"urlFileContent\" [blobFile]=\"blobFile\"></adf-txt-viewer>\r\n                        </ng-container>\r\n\r\n                        <ng-container *ngSwitchCase=\"'in_creation'\">\r\n                            <div class=\"adf-viewer__loading-screen\" fxFlex=\"1 1 auto\">\r\n                                <h2>{{ 'ADF_VIEWER.LOADING' | translate }}</h2>\r\n                                <div>\r\n                                    <mat-spinner></mat-spinner>\r\n                                </div>\r\n                            </div>\r\n                        </ng-container>\r\n\r\n                        <ng-container *ngSwitchCase=\"'custom'\">\r\n                            <ng-container *ngFor=\"let ext of viewerExtensions\">\r\n                                <adf-preview-extension\r\n                                    *ngIf=\"checkExtensions(ext.fileExtension)\"\r\n                                    [id]=\"ext.component\"\r\n                                    [node]=\"nodeEntry.entry\"\r\n                                    [url]=\"urlFileContent\"\r\n                                    [extension]=\"extension\"\r\n                                    [attr.data-automation-id]=\"ext.component\">\r\n                                </adf-preview-extension>\r\n                            </ng-container>\r\n\r\n                            <span class=\"adf-viewer-custom-content\" *ngFor=\"let extensionTemplate of extensionTemplates\">\r\n                                <ng-template\r\n                                    *ngIf=\"extensionTemplate.isVisible\"\r\n                                    [ngTemplateOutlet]=\"extensionTemplate.template\"\r\n                                    [ngTemplateOutletContext]=\"{ urlFileContent: urlFileContent, extension:extension }\">\r\n                                </ng-template>\r\n                            </span>\r\n                        </ng-container>\r\n\r\n                        <ng-container *ngSwitchDefault>\r\n                            <adf-viewer-unknown-format></adf-viewer-unknown-format>\r\n                        </ng-container>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n",
                host: { 'class': 'adf-viewer' },
                encapsulation: ViewEncapsulation.None,
                styles: [""]
            }] }
];
/** @nocollapse */
ViewerComponent.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: ViewUtilService },
    { type: LogService },
    { type: AppExtensionService },
    { type: ElementRef }
];
ViewerComponent.propDecorators = {
    toolbar: [{ type: ContentChild, args: [ViewerToolbarComponent, { static: true },] }],
    sidebar: [{ type: ContentChild, args: [ViewerSidebarComponent, { static: true },] }],
    mnuOpenWith: [{ type: ContentChild, args: [ViewerOpenWithComponent, { static: true },] }],
    mnuMoreActions: [{ type: ContentChild, args: [ViewerMoreActionsComponent, { static: true },] }],
    urlFile: [{ type: Input }],
    urlFileViewer: [{ type: Input }],
    blobFile: [{ type: Input }],
    nodeId: [{ type: Input }],
    sharedLinkId: [{ type: Input }],
    overlayMode: [{ type: Input }],
    showViewer: [{ type: Input }],
    showToolbar: [{ type: Input }],
    displayName: [{ type: Input }],
    allowGoBack: [{ type: Input }],
    allowDownload: [{ type: Input }],
    allowPrint: [{ type: Input }],
    allowFullScreen: [{ type: Input }],
    allowNavigate: [{ type: Input }],
    canNavigateBefore: [{ type: Input }],
    canNavigateNext: [{ type: Input }],
    allowLeftSidebar: [{ type: Input }],
    allowRightSidebar: [{ type: Input }],
    allowThumbnails: [{ type: Input }],
    showRightSidebar: [{ type: Input }],
    showLeftSidebar: [{ type: Input }],
    sidebarRightTemplate: [{ type: Input }],
    sidebarLeftTemplate: [{ type: Input }],
    thumbnailsTemplate: [{ type: Input }],
    mimeType: [{ type: Input }],
    fileName: [{ type: Input }],
    maxRetries: [{ type: Input }],
    goBack: [{ type: Output }],
    print: [{ type: Output }],
    showViewerChange: [{ type: Output }],
    extensionChange: [{ type: Output }],
    navigateBefore: [{ type: Output }],
    navigateNext: [{ type: Output }],
    invalidSharedLink: [{ type: Output }],
    handleKeyboardEvent: [{ type: HostListener, args: ['document:keyup', ['$event'],] }]
};
if (false) {
    /** @type {?} */
    ViewerComponent.prototype.toolbar;
    /** @type {?} */
    ViewerComponent.prototype.sidebar;
    /** @type {?} */
    ViewerComponent.prototype.mnuOpenWith;
    /** @type {?} */
    ViewerComponent.prototype.mnuMoreActions;
    /**
     * If you want to load an external file that does not come from ACS you
     * can use this URL to specify where to load the file from.
     * @type {?}
     */
    ViewerComponent.prototype.urlFile;
    /**
     * Viewer to use with the `urlFile` address (`pdf`, `image`, `media`, `text`).
     * Used when `urlFile` has no filename and extension.
     * @type {?}
     */
    ViewerComponent.prototype.urlFileViewer;
    /**
     * Loads a Blob File
     * @type {?}
     */
    ViewerComponent.prototype.blobFile;
    /**
     * Node Id of the file to load.
     * @type {?}
     */
    ViewerComponent.prototype.nodeId;
    /**
     * Shared link id (to display shared file).
     * @type {?}
     */
    ViewerComponent.prototype.sharedLinkId;
    /**
     * If `true` then show the Viewer as a full page over the current content.
     * Otherwise fit inside the parent div.
     * @type {?}
     */
    ViewerComponent.prototype.overlayMode;
    /**
     * Hide or show the viewer
     * @type {?}
     */
    ViewerComponent.prototype.showViewer;
    /**
     * Hide or show the toolbar
     * @type {?}
     */
    ViewerComponent.prototype.showToolbar;
    /**
     * Specifies the name of the file when it is not available from the URL.
     * @type {?}
     */
    ViewerComponent.prototype.displayName;
    /**
     * Allows `back` navigation
     * @type {?}
     */
    ViewerComponent.prototype.allowGoBack;
    /**
     * Toggles downloading.
     * @type {?}
     */
    ViewerComponent.prototype.allowDownload;
    /**
     * Toggles printing.
     * @type {?}
     */
    ViewerComponent.prototype.allowPrint;
    /**
     * Toggles the 'Full Screen' feature.
     * @type {?}
     */
    ViewerComponent.prototype.allowFullScreen;
    /**
     * Toggles before/next navigation. You can use the arrow buttons to navigate
     * between documents in the collection.
     * @type {?}
     */
    ViewerComponent.prototype.allowNavigate;
    /**
     * Toggles the "before" ("<") button. Requires `allowNavigate` to be enabled.
     * @type {?}
     */
    ViewerComponent.prototype.canNavigateBefore;
    /**
     * Toggles the next (">") button. Requires `allowNavigate` to be enabled.
     * @type {?}
     */
    ViewerComponent.prototype.canNavigateNext;
    /**
     * Allow the left the sidebar.
     * @type {?}
     */
    ViewerComponent.prototype.allowLeftSidebar;
    /**
     * Allow the right sidebar.
     * @type {?}
     */
    ViewerComponent.prototype.allowRightSidebar;
    /**
     * Toggles PDF thumbnails.
     * @type {?}
     */
    ViewerComponent.prototype.allowThumbnails;
    /**
     * Toggles right sidebar visibility. Requires `allowRightSidebar` to be set to `true`.
     * @type {?}
     */
    ViewerComponent.prototype.showRightSidebar;
    /**
     * Toggles left sidebar visibility. Requires `allowLeftSidebar` to be set to `true`.
     * @type {?}
     */
    ViewerComponent.prototype.showLeftSidebar;
    /**
     * The template for the right sidebar. The template context contains the loaded node data.
     * @type {?}
     */
    ViewerComponent.prototype.sidebarRightTemplate;
    /**
     * The template for the left sidebar. The template context contains the loaded node data.
     * @type {?}
     */
    ViewerComponent.prototype.sidebarLeftTemplate;
    /**
     * The template for the pdf thumbnails.
     * @type {?}
     */
    ViewerComponent.prototype.thumbnailsTemplate;
    /**
     * MIME type of the file content (when not determined by the filename extension).
     * @type {?}
     */
    ViewerComponent.prototype.mimeType;
    /**
     * Content filename.
     * @type {?}
     */
    ViewerComponent.prototype.fileName;
    /**
     * Number of times the Viewer will retry fetching content Rendition.
     * There is a delay of at least one second between attempts.
     * @type {?}
     */
    ViewerComponent.prototype.maxRetries;
    /**
     * Emitted when user clicks the 'Back' button.
     * @type {?}
     */
    ViewerComponent.prototype.goBack;
    /**
     * Emitted when user clicks the 'Print' button.
     * @type {?}
     */
    ViewerComponent.prototype.print;
    /**
     * Emitted when the viewer is shown or hidden.
     * @type {?}
     */
    ViewerComponent.prototype.showViewerChange;
    /**
     * Emitted when the filename extension changes.
     * @type {?}
     */
    ViewerComponent.prototype.extensionChange;
    /**
     * Emitted when user clicks 'Navigate Before' ("<") button.
     * @type {?}
     */
    ViewerComponent.prototype.navigateBefore;
    /**
     * Emitted when user clicks 'Navigate Next' (">") button.
     * @type {?}
     */
    ViewerComponent.prototype.navigateNext;
    /**
     * Emitted when the shared link used is not valid.
     * @type {?}
     */
    ViewerComponent.prototype.invalidSharedLink;
    /** @type {?} */
    ViewerComponent.prototype.TRY_TIMEOUT;
    /** @type {?} */
    ViewerComponent.prototype.viewerType;
    /** @type {?} */
    ViewerComponent.prototype.isLoading;
    /** @type {?} */
    ViewerComponent.prototype.nodeEntry;
    /** @type {?} */
    ViewerComponent.prototype.extensionTemplates;
    /** @type {?} */
    ViewerComponent.prototype.externalExtensions;
    /** @type {?} */
    ViewerComponent.prototype.urlFileContent;
    /** @type {?} */
    ViewerComponent.prototype.otherMenu;
    /** @type {?} */
    ViewerComponent.prototype.extension;
    /** @type {?} */
    ViewerComponent.prototype.sidebarRightTemplateContext;
    /** @type {?} */
    ViewerComponent.prototype.sidebarLeftTemplateContext;
    /** @type {?} */
    ViewerComponent.prototype.fileTitle;
    /** @type {?} */
    ViewerComponent.prototype.viewerExtensions;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.cacheBusterNumber;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.subscriptions;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.extensions;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.mimeTypes;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.viewUtils;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.logService;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.extensionService;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.el;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlld2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInZpZXdlci9jb21wb25lbnRzL3ZpZXdlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxPQUFPLEVBQ0gsU0FBUyxFQUNULFlBQVksRUFDWixVQUFVLEVBQ1YsWUFBWSxFQUNaLFlBQVksRUFDWixLQUFLLEVBSUwsTUFBTSxFQUVOLFdBQVcsRUFDWCxpQkFBaUIsRUFDcEIsTUFBTSxlQUFlLENBQUM7QUFFdkIsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUN6QyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUN6RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDeEQsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDN0UsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDdkUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDcEUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFFcEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQ2hFLE9BQU8sRUFBRSxtQkFBbUIsRUFBc0IsTUFBTSwwQkFBMEIsQ0FBQztBQVNuRixNQUFNLE9BQU8sZUFBZTs7Ozs7Ozs7SUFxTXhCLFlBQW9CLFVBQThCLEVBQzlCLFNBQTBCLEVBQzFCLFVBQXNCLEVBQ3RCLGdCQUFxQyxFQUNyQyxFQUFjO1FBSmQsZUFBVSxHQUFWLFVBQVUsQ0FBb0I7UUFDOUIsY0FBUyxHQUFULFNBQVMsQ0FBaUI7UUFDMUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQXFCO1FBQ3JDLE9BQUUsR0FBRixFQUFFLENBQVk7Ozs7O1FBdkxsQyxZQUFPLEdBQUcsRUFBRSxDQUFDOzs7OztRQU1iLGtCQUFhLEdBQVcsSUFBSSxDQUFDOzs7O1FBUTdCLFdBQU0sR0FBVyxJQUFJLENBQUM7Ozs7UUFJdEIsaUJBQVksR0FBVyxJQUFJLENBQUM7Ozs7O1FBTTVCLGdCQUFXLEdBQUcsS0FBSyxDQUFDOzs7O1FBSXBCLGVBQVUsR0FBRyxJQUFJLENBQUM7Ozs7UUFJbEIsZ0JBQVcsR0FBRyxJQUFJLENBQUM7Ozs7O1FBU25CLGdCQUFXLEdBQUcsSUFBSSxDQUFDOzs7O1FBSW5CLGtCQUFhLEdBQUcsSUFBSSxDQUFDOzs7O1FBSXJCLGVBQVUsR0FBRyxLQUFLLENBQUM7Ozs7UUFJbkIsb0JBQWUsR0FBRyxJQUFJLENBQUM7Ozs7O1FBTXZCLGtCQUFhLEdBQUcsS0FBSyxDQUFDOzs7O1FBSXRCLHNCQUFpQixHQUFHLElBQUksQ0FBQzs7OztRQUl6QixvQkFBZSxHQUFHLElBQUksQ0FBQzs7OztRQUl2QixxQkFBZ0IsR0FBRyxLQUFLLENBQUM7Ozs7UUFJekIsc0JBQWlCLEdBQUcsS0FBSyxDQUFDOzs7O1FBSTFCLG9CQUFlLEdBQUcsSUFBSSxDQUFDOzs7O1FBSXZCLHFCQUFnQixHQUFHLEtBQUssQ0FBQzs7OztRQUl6QixvQkFBZSxHQUFHLEtBQUssQ0FBQzs7OztRQUl4Qix5QkFBb0IsR0FBcUIsSUFBSSxDQUFDOzs7O1FBSTlDLHdCQUFtQixHQUFxQixJQUFJLENBQUM7Ozs7UUFJN0MsdUJBQWtCLEdBQXFCLElBQUksQ0FBQzs7Ozs7UUFjNUMsZUFBVSxHQUFHLEVBQUUsQ0FBQzs7OztRQUloQixXQUFNLEdBQUcsSUFBSSxZQUFZLEVBQWtCLENBQUM7Ozs7UUFJNUMsVUFBSyxHQUFHLElBQUksWUFBWSxFQUFrQixDQUFDOzs7O1FBSTNDLHFCQUFnQixHQUFHLElBQUksWUFBWSxFQUFXLENBQUM7Ozs7UUFJL0Msb0JBQWUsR0FBRyxJQUFJLFlBQVksRUFBVSxDQUFDOzs7O1FBSTdDLG1CQUFjLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQzs7OztRQUlwQyxpQkFBWSxHQUFHLElBQUksWUFBWSxFQUFFLENBQUM7Ozs7UUFJbEMsc0JBQWlCLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQUV2QyxnQkFBVyxHQUFXLElBQUksQ0FBQztRQUUzQixlQUFVLEdBQUcsU0FBUyxDQUFDO1FBQ3ZCLGNBQVMsR0FBRyxLQUFLLENBQUM7UUFHbEIsdUJBQWtCLEdBQXlELEVBQUUsQ0FBQztRQUM5RSx1QkFBa0IsR0FBYSxFQUFFLENBQUM7UUFJbEMsZ0NBQTJCLEdBQW1CLEVBQUMsSUFBSSxFQUFFLElBQUksRUFBQyxDQUFDO1FBQzNELCtCQUEwQixHQUFtQixFQUFDLElBQUksRUFBRSxJQUFJLEVBQUMsQ0FBQztRQUUxRCxxQkFBZ0IsR0FBOEIsRUFBRSxDQUFDO1FBSXpDLGtCQUFhLEdBQW1CLEVBQUUsQ0FBQzs7UUFHbkMsZUFBVSxHQUFHO1lBQ2pCLEtBQUssRUFBRSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDO1lBQ2xELEtBQUssRUFBRSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxLQUFLLENBQUM7WUFDM0MsSUFBSSxFQUFFLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDO1lBQ3ZELEdBQUcsRUFBRSxDQUFDLEtBQUssQ0FBQztTQUNmLENBQUM7O1FBR00sY0FBUyxHQUFHO1lBQ2hCLElBQUksRUFBRSxDQUFDLFlBQVksRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSwwQkFBMEIsQ0FBQztZQUNyRixHQUFHLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztZQUN4QixLQUFLLEVBQUUsQ0FBQyxXQUFXLEVBQUUsWUFBWSxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsZUFBZSxDQUFDO1lBQzdFLEtBQUssRUFBRSxDQUFDLFdBQVcsRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUUsV0FBVyxDQUFDO1NBQzFGLENBQUM7SUFPRixDQUFDOzs7O0lBRUQsZUFBZTtRQUNYLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQzlGLENBQUM7Ozs7SUFFRCxRQUFRO1FBQ0osSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQ25CLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxDQUFDLFNBQVM7Ozs7UUFBQyxDQUFDLElBQUksRUFBRSxFQUFFLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBQyxDQUM1RSxDQUFDO1FBRUYsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQzFCLENBQUM7Ozs7O0lBRU8sY0FBYztRQUNsQixJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixFQUFFLENBQUM7UUFDcEUsSUFBSSxDQUFDLGdCQUFnQjthQUNoQixPQUFPOzs7O1FBQUMsQ0FBQyxTQUE2QixFQUFFLEVBQUU7WUFDdkMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDMUQsQ0FBQyxFQUFDLENBQUM7SUFDWCxDQUFDOzs7O0lBRUQsV0FBVztRQUNQLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTzs7OztRQUFDLENBQUMsWUFBWSxFQUFFLEVBQUUsQ0FBQyxZQUFZLENBQUMsV0FBVyxFQUFFLEVBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7Ozs7SUFFTyxhQUFhLENBQUMsSUFBVTtRQUM1QixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDakMsSUFBSSxDQUFDLHlCQUF5QixFQUFFLENBQUM7WUFDakMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDdEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJOzs7WUFBQyxHQUFHLEVBQUU7Z0JBQy9CLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1lBQzNCLENBQUMsRUFBQyxDQUFDO1NBQ047SUFDTCxDQUFDOzs7OztJQUVELFdBQVcsQ0FBQyxPQUFzQjtRQUM5QixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsRUFBRTtnQkFDekIsTUFBTSxJQUFJLEtBQUssQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO2FBQ25FO1lBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFFdEIsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNmLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7YUFDMUI7aUJBQU0sSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNyQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO2FBQzFCO2lCQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDcEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBQyxPQUFPLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFDLENBQUMsQ0FBQyxJQUFJOzs7O2dCQUNsRixDQUFDLElBQWUsRUFBRSxFQUFFO29CQUNoQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztvQkFDdEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSTs7O29CQUFDLEdBQUcsRUFBRTt3QkFDckMsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7b0JBQzNCLENBQUMsRUFBQyxDQUFDO2dCQUNQLENBQUM7Ozs7Z0JBQ0QsQ0FBQyxLQUFLLEVBQUUsRUFBRTtvQkFDTixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztvQkFDdkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsMEJBQTBCLENBQUMsQ0FBQztnQkFDdEQsQ0FBQyxFQUNKLENBQUM7YUFDTDtpQkFBTSxJQUFJLElBQUksQ0FBQyxZQUFZLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSyxDQUFDO2dCQUV6QixJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLElBQUk7Ozs7Z0JBQ2hFLENBQUMsZUFBZ0MsRUFBRSxFQUFFO29CQUNqQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsZUFBZSxDQUFDLENBQUM7b0JBQzFDLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO2dCQUMzQixDQUFDOzs7Z0JBQ0QsR0FBRyxFQUFFO29CQUNELElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO29CQUN2QixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO29CQUN4RCxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ2xDLENBQUMsRUFBQyxDQUFDO2FBQ1Y7U0FDSjtJQUNMLENBQUM7Ozs7O0lBRU8sYUFBYTtRQUNqQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztRQUNuQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFOUQsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFDM0IsMERBQTBEO1FBRTFELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFTyxZQUFZOztjQUNWLGVBQWUsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUM3RCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDdEQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7UUFDeEQsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBRW5DLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUVqQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUN0RixJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssU0FBUyxFQUFFO1lBQy9CLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUNqRTtRQUVELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUMxQyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7O0lBRWEsYUFBYSxDQUFDLElBQVU7OztnQkFDOUIsU0FBUztZQUViLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDZCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDO2FBQ3pDO1lBRUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUVoRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7WUFDeEUsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxjQUFjLEdBQUcsR0FBRyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztZQUV4SCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFbEQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBRTFCLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUNoRSxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssU0FBUyxFQUFFO2dCQUMvQixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDakU7WUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssU0FBUyxFQUFFO2dCQUMvQixTQUFTLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUNsRDtZQUVELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUMxQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztZQUM3QyxJQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztZQUM1QyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7WUFFakIsT0FBTyxTQUFTLENBQUM7UUFDckIsQ0FBQztLQUFBOzs7Ozs7SUFFTyxtQkFBbUIsQ0FBQyxPQUFZO1FBQ3BDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDO1FBQy9DLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztRQUVuQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFFbkcsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzlELElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxTQUFTLEVBQUU7WUFDL0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ25FO1FBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLFNBQVMsRUFBRTtZQUMvQixJQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQ3REO1FBRUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzlDLENBQUM7Ozs7SUFFRCxhQUFhO1FBQ1QsSUFBSSxDQUFDLGdCQUFnQixHQUFHLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1FBQy9DLElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDdEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBQyxPQUFPLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFDLENBQUM7aUJBQ3ZGLElBQUk7Ozs7WUFBQyxDQUFDLFNBQW9CLEVBQUUsRUFBRTtnQkFDM0IsSUFBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDO1lBQzVELENBQUMsRUFBQyxDQUFDO1NBQ1Y7SUFDTCxDQUFDOzs7O0lBRUQsaUJBQWlCO1FBQ2IsSUFBSSxDQUFDLGVBQWUsR0FBRyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUM7UUFDN0MsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUN0QyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxFQUFDLE9BQU8sRUFBRSxDQUFDLHFCQUFxQixDQUFDLEVBQUMsQ0FBQztpQkFDdkYsSUFBSTs7OztZQUFDLENBQUMsU0FBb0IsRUFBRSxFQUFFO2dCQUMzQixJQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUM7WUFDM0QsQ0FBQyxFQUFDLENBQUM7U0FDVjtJQUNMLENBQUM7Ozs7OztJQUVPLGNBQWMsQ0FBQyxJQUFJO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUM7SUFDcEMsQ0FBQzs7OztJQUVELFNBQVM7UUFDTCxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUMxQixDQUFDOzs7OztJQUVELHVCQUF1QixDQUFDLFFBQWdCO1FBQ3BDLElBQUksUUFBUSxFQUFFO1lBQ1YsUUFBUSxHQUFHLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQzs7a0JBRTVCLFdBQVcsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDL0MsS0FBSyxNQUFNLElBQUksSUFBSSxXQUFXLEVBQUU7Z0JBQzVCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUM3QyxPQUFPLElBQUksQ0FBQztpQkFDZjthQUNKO1NBQ0o7UUFDRCxPQUFPLFNBQVMsQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELHdCQUF3QixDQUFDLFNBQWlCO1FBQ3RDLElBQUksU0FBUyxFQUFFO1lBQ1gsU0FBUyxHQUFHLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztTQUN2QztRQUVELElBQUksSUFBSSxDQUFDLHVCQUF1QixDQUFDLFNBQVMsQ0FBQyxFQUFFO1lBQ3pDLE9BQU8sUUFBUSxDQUFDO1NBQ25CO1FBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQy9DLE9BQU8sT0FBTyxDQUFDO1NBQ2xCO1FBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQy9DLE9BQU8sT0FBTyxDQUFDO1NBQ2xCO1FBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzlDLE9BQU8sTUFBTSxDQUFDO1NBQ2pCO1FBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQzdDLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBRUQsT0FBTyxTQUFTLENBQUM7SUFDckIsQ0FBQzs7OztJQUVELGlCQUFpQjtRQUNiLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztJQUNqQixDQUFDOzs7O0lBRUQscUJBQXFCO1FBQ2pCLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDL0IsQ0FBQzs7OztJQUVELG1CQUFtQjtRQUNmLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDN0IsQ0FBQzs7Ozs7SUFLRCxLQUFLO1FBQ0QsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUNqQztRQUNELElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ2hELENBQUM7Ozs7Ozs7SUFPRCxrQkFBa0IsQ0FBQyxHQUFXOztjQUNwQixNQUFNLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUM7O2NBQ3pCLEtBQUssR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQzs7Y0FDeEIsR0FBRyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQ2hCLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFDaEMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDO1FBQ25DLE9BQU8sR0FBRyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsR0FBRyxDQUFDLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDN0QsQ0FBQzs7Ozs7Ozs7OztJQVVELGdCQUFnQixDQUFDLFFBQWdCO1FBQzdCLElBQUksUUFBUSxFQUFFOztrQkFDSixLQUFLLEdBQUcsUUFBUSxDQUFDLEtBQUssQ0FBQywwQkFBMEIsQ0FBQztZQUN4RCxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7U0FDbEM7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOzs7OztJQUVELHVCQUF1QixDQUFDLFNBQWlCOztjQUMvQixVQUFVLEdBQVEsSUFBSSxDQUFDLGtCQUFrQixJQUFJLEVBQUU7UUFFckQsSUFBSSxTQUFTLElBQUksVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDcEMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNwQyxPQUFPLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3BEO1FBRUQsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7Ozs7O0lBT0QsbUJBQW1CLENBQUMsS0FBb0I7O2NBQzlCLEdBQUcsR0FBRyxLQUFLLENBQUMsT0FBTztRQUV6QixNQUFNO1FBQ04sSUFBSSxHQUFHLEtBQUssRUFBRSxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUUsRUFBRSxNQUFNO1lBQ3hDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUNoQjtRQUVELGFBQWE7UUFDYixJQUFJLEdBQUcsS0FBSyxFQUFFLElBQUksSUFBSSxDQUFDLGlCQUFpQixFQUFFO1lBQ3RDLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN2QixJQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztTQUNoQztRQUVELGNBQWM7UUFDZCxJQUFJLEdBQUcsS0FBSyxFQUFFLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUNwQyxLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdkIsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUM7U0FDOUI7UUFFRCxTQUFTO1FBQ1QsSUFBSSxHQUFHLEtBQUssRUFBRSxJQUFJLEtBQUssQ0FBQyxPQUFPLEVBQUU7WUFDN0IsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxlQUFlLEVBQUUsQ0FBQztTQUMxQjtJQUNMLENBQUM7Ozs7SUFFRCxZQUFZO1FBQ1IsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFOztrQkFDWCxJQUFJLEdBQUcsSUFBSSxTQUFTLEVBQUU7WUFDNUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7WUFFdEIsSUFBSSxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDeEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQzthQUMvRDtTQUNKO0lBQ0wsQ0FBQzs7Ozs7SUFLRCxlQUFlO1FBQ1gsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFOztrQkFDaEIsU0FBUyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxtQ0FBbUMsQ0FBQztZQUMxRixJQUFJLFNBQVMsRUFBRTtnQkFDWCxJQUFJLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRTtvQkFDN0IsU0FBUyxDQUFDLGlCQUFpQixFQUFFLENBQUM7aUJBQ2pDO3FCQUFNLElBQUksU0FBUyxDQUFDLHVCQUF1QixFQUFFO29CQUMxQyxTQUFTLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztpQkFDdkM7cUJBQU0sSUFBSSxTQUFTLENBQUMsb0JBQW9CLEVBQUU7b0JBQ3ZDLFNBQVMsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO2lCQUNwQztxQkFBTSxJQUFJLFNBQVMsQ0FBQyxtQkFBbUIsRUFBRTtvQkFDdEMsU0FBUyxDQUFDLG1CQUFtQixFQUFFLENBQUM7aUJBQ25DO2FBQ0o7U0FDSjtJQUNMLENBQUM7Ozs7OztJQUVhLG9CQUFvQixDQUFDLE1BQWM7O1lBQzdDLElBQUk7O3NCQUNNLFNBQVMsR0FBRyxNQUFNLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsS0FBSyxDQUFDO2dCQUM1RCxJQUFJLFNBQVMsRUFBRTs7MEJBQ0wsV0FBVyxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRTtvQkFFdEMsSUFBSSxXQUFXLEtBQUssS0FBSyxFQUFFO3dCQUN2QixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztxQkFDM0I7eUJBQU0sSUFBSSxXQUFXLEtBQUssWUFBWSxFQUFFO3dCQUNyQyxJQUFJLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQztxQkFDN0I7b0JBRUQsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDO2lCQUN6RjthQUNKO1lBQUMsT0FBTyxHQUFHLEVBQUU7Z0JBQ1YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUM7YUFDOUI7UUFDTCxDQUFDO0tBQUE7Ozs7OztJQUVhLDBCQUEwQixDQUFDLFFBQWdCOztZQUNyRCxJQUFJOztzQkFDTSxTQUFTLEdBQW1CLE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUMsUUFBUSxFQUFFLEtBQUssQ0FBQztnQkFDN0csSUFBSSxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsS0FBSyxTQUFTLEVBQUU7b0JBQ2pELElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO29CQUN4QixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLHlCQUF5QixDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsQ0FBQztpQkFDL0Y7YUFDSjtZQUFDLE9BQU8sS0FBSyxFQUFFO2dCQUNaLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUM3QixJQUFJOzswQkFDTSxTQUFTLEdBQW1CLE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsc0JBQXNCLENBQUMsUUFBUSxFQUFFLFlBQVksQ0FBQztvQkFDcEgsSUFBSSxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsS0FBSyxTQUFTLEVBQUU7d0JBQ2pELElBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDO3dCQUMxQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLHlCQUF5QixDQUFDLFFBQVEsRUFBRSxZQUFZLENBQUMsQ0FBQztxQkFDdEc7aUJBQ0o7Z0JBQUMsT0FBTyxLQUFLLEVBQUU7b0JBQ1osSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQ2hDO2FBQ0o7UUFDTCxDQUFDO0tBQUE7Ozs7Ozs7SUFFYSxnQkFBZ0IsQ0FBQyxNQUFjLEVBQUUsV0FBbUI7O1lBQzlELFdBQVcsR0FBRyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUM7O2tCQUVsQyxrQkFBa0IsR0FBb0IsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDOztnQkFFakcsU0FBUyxHQUFtQixrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUk7Ozs7WUFBQyxDQUFDLGNBQThCLEVBQUUsRUFBRSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxLQUFLLFdBQVcsRUFBQztZQUMvSixJQUFJLENBQUMsU0FBUyxFQUFFO2dCQUNaLFdBQVcsR0FBRyxZQUFZLENBQUM7Z0JBQzNCLFNBQVMsR0FBRyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUk7Ozs7Z0JBQUMsQ0FBQyxjQUE4QixFQUFFLEVBQUUsQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxXQUFXLEVBQUUsS0FBSyxXQUFXLEVBQUMsQ0FBQzthQUMvSTtZQUVELElBQUksU0FBUyxFQUFFOztzQkFDTCxNQUFNLEdBQVcsU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFO2dCQUV4RCxJQUFJLE1BQU0sS0FBSyxhQUFhLEVBQUU7b0JBQzFCLElBQUk7d0JBQ0EsTUFBTSxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLEVBQUMsRUFBRSxFQUFFLFdBQVcsRUFBQyxDQUFDLENBQUMsSUFBSTs7O3dCQUFDLEdBQUcsRUFBRTs0QkFDckYsSUFBSSxDQUFDLFVBQVUsR0FBRyxhQUFhLENBQUM7d0JBQ3BDLENBQUMsRUFBQyxDQUFDO3dCQUNILFNBQVMsR0FBRyxNQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDO3FCQUM3RDtvQkFBQyxPQUFPLEdBQUcsRUFBRTt3QkFDVixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDOUI7aUJBQ0o7YUFDSjtZQUVELE9BQU8sU0FBUyxDQUFDO1FBQ3JCLENBQUM7S0FBQTs7Ozs7OztJQUVhLGFBQWEsQ0FBQyxNQUFjLEVBQUUsV0FBbUI7OztnQkFDdkQsWUFBWSxHQUFXLENBQUM7WUFDNUIsT0FBTyxJQUFJLE9BQU87Ozs7O1lBQWlCLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRSxFQUFFOztzQkFDN0MsVUFBVSxHQUFHLFdBQVc7OztnQkFBQyxHQUFHLEVBQUU7b0JBQ2hDLFlBQVksRUFBRSxDQUFDO29CQUNmLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxZQUFZLEVBQUU7d0JBQ2pDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsV0FBVyxDQUFDLENBQUMsSUFBSTs7Ozt3QkFBQyxDQUFDLFNBQXlCLEVBQUUsRUFBRTs7a0NBQ3pGLE1BQU0sR0FBVyxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7NEJBQ3hELElBQUksTUFBTSxLQUFLLFNBQVMsRUFBRTtnQ0FFdEIsSUFBSSxXQUFXLEtBQUssS0FBSyxFQUFFO29DQUN2QixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztpQ0FDM0I7cUNBQU0sSUFBSSxXQUFXLEtBQUssWUFBWSxFQUFFO29DQUNyQyxJQUFJLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQztpQ0FDN0I7Z0NBRUQsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDO2dDQUV0RixhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7Z0NBQzFCLE9BQU8sT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDOzZCQUM3Qjt3QkFDTCxDQUFDOzs7d0JBQUUsR0FBRyxFQUFFOzRCQUNKLElBQUksQ0FBQyxVQUFVLEdBQUcsbUJBQW1CLENBQUM7NEJBQ3RDLE9BQU8sTUFBTSxFQUFFLENBQUM7d0JBQ3BCLENBQUMsRUFBQyxDQUFDO3FCQUNOO3lCQUFNO3dCQUNILElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO3dCQUN2QixJQUFJLENBQUMsVUFBVSxHQUFHLG1CQUFtQixDQUFDO3dCQUN0QyxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7cUJBQzdCO2dCQUNMLENBQUMsR0FBRSxJQUFJLENBQUMsV0FBVyxDQUFDO1lBQ3hCLENBQUMsRUFBQyxDQUFDO1FBQ1AsQ0FBQztLQUFBOzs7OztJQUVELGVBQWUsQ0FBQyxnQkFBZ0I7UUFDNUIsSUFBSSxPQUFPLGdCQUFnQixLQUFLLFFBQVEsRUFBRTtZQUN0QyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLEtBQUssZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDMUU7YUFBTSxJQUFJLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDcEMsT0FBTyxnQkFBZ0IsQ0FBQyxJQUFJOzs7O1lBQUMsQ0FBQyxnQkFBZ0IsRUFBRSxFQUFFO2dCQUM5QyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLEtBQUssZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDM0UsQ0FBQyxFQUFDLENBQUM7U0FDTjtJQUVMLENBQUM7Ozs7O0lBRU8seUJBQXlCO1FBQzdCLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7SUFDeEMsQ0FBQzs7O1lBNXFCSixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLFlBQVk7Z0JBQ3RCLDR2WUFBc0M7Z0JBRXRDLElBQUksRUFBRSxFQUFDLE9BQU8sRUFBRSxZQUFZLEVBQUM7Z0JBQzdCLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOzthQUN4Qzs7OztZQWhCUSxrQkFBa0I7WUFPbEIsZUFBZTtZQU5mLFVBQVU7WUFPVixtQkFBbUI7WUF0QnhCLFVBQVU7OztzQkFpQ1QsWUFBWSxTQUFDLHNCQUFzQixFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQztzQkFHbkQsWUFBWSxTQUFDLHNCQUFzQixFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQzswQkFHbkQsWUFBWSxTQUFDLHVCQUF1QixFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQzs2QkFHcEQsWUFBWSxTQUFDLDBCQUEwQixFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQztzQkFNdkQsS0FBSzs0QkFNTCxLQUFLO3VCQUlMLEtBQUs7cUJBSUwsS0FBSzsyQkFJTCxLQUFLOzBCQU1MLEtBQUs7eUJBSUwsS0FBSzswQkFJTCxLQUFLOzBCQUlMLEtBQUs7MEJBS0wsS0FBSzs0QkFJTCxLQUFLO3lCQUlMLEtBQUs7OEJBSUwsS0FBSzs0QkFNTCxLQUFLO2dDQUlMLEtBQUs7OEJBSUwsS0FBSzsrQkFJTCxLQUFLO2dDQUlMLEtBQUs7OEJBSUwsS0FBSzsrQkFJTCxLQUFLOzhCQUlMLEtBQUs7bUNBSUwsS0FBSztrQ0FJTCxLQUFLO2lDQUlMLEtBQUs7dUJBSUwsS0FBSzt1QkFJTCxLQUFLO3lCQU1MLEtBQUs7cUJBSUwsTUFBTTtvQkFJTixNQUFNOytCQUlOLE1BQU07OEJBSU4sTUFBTTs2QkFJTixNQUFNOzJCQUlOLE1BQU07Z0NBSU4sTUFBTTtrQ0F3Vk4sWUFBWSxTQUFDLGdCQUFnQixFQUFFLENBQUMsUUFBUSxDQUFDOzs7O0lBcGYxQyxrQ0FDZ0M7O0lBRWhDLGtDQUNnQzs7SUFFaEMsc0NBQ3FDOztJQUVyQyx5Q0FDMkM7Ozs7OztJQUszQyxrQ0FDYTs7Ozs7O0lBS2Isd0NBQzZCOzs7OztJQUc3QixtQ0FDZTs7Ozs7SUFHZixpQ0FDc0I7Ozs7O0lBR3RCLHVDQUM0Qjs7Ozs7O0lBSzVCLHNDQUNvQjs7Ozs7SUFHcEIscUNBQ2tCOzs7OztJQUdsQixzQ0FDbUI7Ozs7O0lBR25CLHNDQUNvQjs7Ozs7SUFJcEIsc0NBQ21COzs7OztJQUduQix3Q0FDcUI7Ozs7O0lBR3JCLHFDQUNtQjs7Ozs7SUFHbkIsMENBQ3VCOzs7Ozs7SUFLdkIsd0NBQ3NCOzs7OztJQUd0Qiw0Q0FDeUI7Ozs7O0lBR3pCLDBDQUN1Qjs7Ozs7SUFHdkIsMkNBQ3lCOzs7OztJQUd6Qiw0Q0FDMEI7Ozs7O0lBRzFCLDBDQUN1Qjs7Ozs7SUFHdkIsMkNBQ3lCOzs7OztJQUd6QiwwQ0FDd0I7Ozs7O0lBR3hCLCtDQUM4Qzs7Ozs7SUFHOUMsOENBQzZDOzs7OztJQUc3Qyw2Q0FDNEM7Ozs7O0lBRzVDLG1DQUNpQjs7Ozs7SUFHakIsbUNBQ2lCOzs7Ozs7SUFLakIscUNBQ2dCOzs7OztJQUdoQixpQ0FDNEM7Ozs7O0lBRzVDLGdDQUMyQzs7Ozs7SUFHM0MsMkNBQytDOzs7OztJQUcvQywwQ0FDNkM7Ozs7O0lBRzdDLHlDQUNvQzs7Ozs7SUFHcEMsdUNBQ2tDOzs7OztJQUdsQyw0Q0FDdUM7O0lBRXZDLHNDQUEyQjs7SUFFM0IscUNBQXVCOztJQUN2QixvQ0FBa0I7O0lBQ2xCLG9DQUFxQjs7SUFFckIsNkNBQThFOztJQUM5RSw2Q0FBa0M7O0lBQ2xDLHlDQUF1Qjs7SUFDdkIsb0NBQWU7O0lBQ2Ysb0NBQWtCOztJQUNsQixzREFBMkQ7O0lBQzNELHFEQUEwRDs7SUFDMUQsb0NBQWtCOztJQUNsQiwyQ0FBaUQ7Ozs7O0lBRWpELDRDQUEwQjs7Ozs7SUFFMUIsd0NBQTJDOzs7OztJQUczQyxxQ0FLRTs7Ozs7SUFHRixvQ0FLRTs7Ozs7SUFFVSxxQ0FBc0M7Ozs7O0lBQ3RDLG9DQUFrQzs7Ozs7SUFDbEMscUNBQThCOzs7OztJQUM5QiwyQ0FBNkM7Ozs7O0lBQzdDLDZCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxuXG4vKiFcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxuICpcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cbiAqL1xuXG5pbXBvcnQge1xuICAgIENvbXBvbmVudCxcbiAgICBDb250ZW50Q2hpbGQsXG4gICAgRWxlbWVudFJlZixcbiAgICBFdmVudEVtaXR0ZXIsXG4gICAgSG9zdExpc3RlbmVyLFxuICAgIElucHV0LFxuICAgIE9uQ2hhbmdlcyxcbiAgICBPbkRlc3Ryb3ksXG4gICAgT25Jbml0LFxuICAgIE91dHB1dCxcbiAgICBTaW1wbGVDaGFuZ2VzLFxuICAgIFRlbXBsYXRlUmVmLFxuICAgIFZpZXdFbmNhcHN1bGF0aW9uXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTm9kZSwgTm9kZUVudHJ5LCBSZW5kaXRpb25FbnRyeSwgUmVuZGl0aW9uUGFnaW5nLCBTaGFyZWRMaW5rRW50cnkgfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcbmltcG9ydCB7IEJhc2VFdmVudCB9IGZyb20gJy4uLy4uL2V2ZW50cyc7XG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9hbGZyZXNjby1hcGkuc2VydmljZSc7XG5pbXBvcnQgeyBMb2dTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbG9nLnNlcnZpY2UnO1xuaW1wb3J0IHsgVmlld2VyTW9yZUFjdGlvbnNDb21wb25lbnQgfSBmcm9tICcuL3ZpZXdlci1tb3JlLWFjdGlvbnMuY29tcG9uZW50JztcbmltcG9ydCB7IFZpZXdlck9wZW5XaXRoQ29tcG9uZW50IH0gZnJvbSAnLi92aWV3ZXItb3Blbi13aXRoLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBWaWV3ZXJTaWRlYmFyQ29tcG9uZW50IH0gZnJvbSAnLi92aWV3ZXItc2lkZWJhci5jb21wb25lbnQnO1xuaW1wb3J0IHsgVmlld2VyVG9vbGJhckNvbXBvbmVudCB9IGZyb20gJy4vdmlld2VyLXRvb2xiYXIuY29tcG9uZW50JztcbmltcG9ydCB7IFN1YnNjcmlwdGlvbiB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgVmlld1V0aWxTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvdmlldy11dGlsLnNlcnZpY2UnO1xuaW1wb3J0IHsgQXBwRXh0ZW5zaW9uU2VydmljZSwgVmlld2VyRXh0ZW5zaW9uUmVmIH0gZnJvbSAnQGFsZnJlc2NvL2FkZi1leHRlbnNpb25zJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdhZGYtdmlld2VyJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vdmlld2VyLmNvbXBvbmVudC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi92aWV3ZXIuY29tcG9uZW50LnNjc3MnXSxcbiAgICBob3N0OiB7J2NsYXNzJzogJ2FkZi12aWV3ZXInfSxcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXG59KVxuZXhwb3J0IGNsYXNzIFZpZXdlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcywgT25Jbml0LCBPbkRlc3Ryb3kge1xuXG4gICAgQENvbnRlbnRDaGlsZChWaWV3ZXJUb29sYmFyQ29tcG9uZW50LCB7c3RhdGljOiB0cnVlfSlcbiAgICB0b29sYmFyOiBWaWV3ZXJUb29sYmFyQ29tcG9uZW50O1xuXG4gICAgQENvbnRlbnRDaGlsZChWaWV3ZXJTaWRlYmFyQ29tcG9uZW50LCB7c3RhdGljOiB0cnVlfSlcbiAgICBzaWRlYmFyOiBWaWV3ZXJTaWRlYmFyQ29tcG9uZW50O1xuXG4gICAgQENvbnRlbnRDaGlsZChWaWV3ZXJPcGVuV2l0aENvbXBvbmVudCwge3N0YXRpYzogdHJ1ZX0pXG4gICAgbW51T3BlbldpdGg6IFZpZXdlck9wZW5XaXRoQ29tcG9uZW50O1xuXG4gICAgQENvbnRlbnRDaGlsZChWaWV3ZXJNb3JlQWN0aW9uc0NvbXBvbmVudCwge3N0YXRpYzogdHJ1ZX0pXG4gICAgbW51TW9yZUFjdGlvbnM6IFZpZXdlck1vcmVBY3Rpb25zQ29tcG9uZW50O1xuXG4gICAgLyoqIElmIHlvdSB3YW50IHRvIGxvYWQgYW4gZXh0ZXJuYWwgZmlsZSB0aGF0IGRvZXMgbm90IGNvbWUgZnJvbSBBQ1MgeW91XG4gICAgICogY2FuIHVzZSB0aGlzIFVSTCB0byBzcGVjaWZ5IHdoZXJlIHRvIGxvYWQgdGhlIGZpbGUgZnJvbS5cbiAgICAgKi9cbiAgICBASW5wdXQoKVxuICAgIHVybEZpbGUgPSAnJztcblxuICAgIC8qKiBWaWV3ZXIgdG8gdXNlIHdpdGggdGhlIGB1cmxGaWxlYCBhZGRyZXNzIChgcGRmYCwgYGltYWdlYCwgYG1lZGlhYCwgYHRleHRgKS5cbiAgICAgKiBVc2VkIHdoZW4gYHVybEZpbGVgIGhhcyBubyBmaWxlbmFtZSBhbmQgZXh0ZW5zaW9uLlxuICAgICAqL1xuICAgIEBJbnB1dCgpXG4gICAgdXJsRmlsZVZpZXdlcjogc3RyaW5nID0gbnVsbDtcblxuICAgIC8qKiBMb2FkcyBhIEJsb2IgRmlsZSAqL1xuICAgIEBJbnB1dCgpXG4gICAgYmxvYkZpbGU6IEJsb2I7XG5cbiAgICAvKiogTm9kZSBJZCBvZiB0aGUgZmlsZSB0byBsb2FkLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgbm9kZUlkOiBzdHJpbmcgPSBudWxsO1xuXG4gICAgLyoqIFNoYXJlZCBsaW5rIGlkICh0byBkaXNwbGF5IHNoYXJlZCBmaWxlKS4gKi9cbiAgICBASW5wdXQoKVxuICAgIHNoYXJlZExpbmtJZDogc3RyaW5nID0gbnVsbDtcblxuICAgIC8qKiBJZiBgdHJ1ZWAgdGhlbiBzaG93IHRoZSBWaWV3ZXIgYXMgYSBmdWxsIHBhZ2Ugb3ZlciB0aGUgY3VycmVudCBjb250ZW50LlxuICAgICAqIE90aGVyd2lzZSBmaXQgaW5zaWRlIHRoZSBwYXJlbnQgZGl2LlxuICAgICAqL1xuICAgIEBJbnB1dCgpXG4gICAgb3ZlcmxheU1vZGUgPSBmYWxzZTtcblxuICAgIC8qKiBIaWRlIG9yIHNob3cgdGhlIHZpZXdlciAqL1xuICAgIEBJbnB1dCgpXG4gICAgc2hvd1ZpZXdlciA9IHRydWU7XG5cbiAgICAvKiogSGlkZSBvciBzaG93IHRoZSB0b29sYmFyICovXG4gICAgQElucHV0KClcbiAgICBzaG93VG9vbGJhciA9IHRydWU7XG5cbiAgICAvKiogU3BlY2lmaWVzIHRoZSBuYW1lIG9mIHRoZSBmaWxlIHdoZW4gaXQgaXMgbm90IGF2YWlsYWJsZSBmcm9tIHRoZSBVUkwuICovXG4gICAgQElucHV0KClcbiAgICBkaXNwbGF5TmFtZTogc3RyaW5nO1xuXG4gICAgLyoqIEBkZXByZWNhdGVkIDMuMi4wICovXG4gICAgLyoqIEFsbG93cyBgYmFja2AgbmF2aWdhdGlvbiAqL1xuICAgIEBJbnB1dCgpXG4gICAgYWxsb3dHb0JhY2sgPSB0cnVlO1xuXG4gICAgLyoqIFRvZ2dsZXMgZG93bmxvYWRpbmcuICovXG4gICAgQElucHV0KClcbiAgICBhbGxvd0Rvd25sb2FkID0gdHJ1ZTtcblxuICAgIC8qKiBUb2dnbGVzIHByaW50aW5nLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgYWxsb3dQcmludCA9IGZhbHNlO1xuXG4gICAgLyoqIFRvZ2dsZXMgdGhlICdGdWxsIFNjcmVlbicgZmVhdHVyZS4gKi9cbiAgICBASW5wdXQoKVxuICAgIGFsbG93RnVsbFNjcmVlbiA9IHRydWU7XG5cbiAgICAvKiogVG9nZ2xlcyBiZWZvcmUvbmV4dCBuYXZpZ2F0aW9uLiBZb3UgY2FuIHVzZSB0aGUgYXJyb3cgYnV0dG9ucyB0byBuYXZpZ2F0ZVxuICAgICAqIGJldHdlZW4gZG9jdW1lbnRzIGluIHRoZSBjb2xsZWN0aW9uLlxuICAgICAqL1xuICAgIEBJbnB1dCgpXG4gICAgYWxsb3dOYXZpZ2F0ZSA9IGZhbHNlO1xuXG4gICAgLyoqIFRvZ2dsZXMgdGhlIFwiYmVmb3JlXCIgKFwiPFwiKSBidXR0b24uIFJlcXVpcmVzIGBhbGxvd05hdmlnYXRlYCB0byBiZSBlbmFibGVkLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgY2FuTmF2aWdhdGVCZWZvcmUgPSB0cnVlO1xuXG4gICAgLyoqIFRvZ2dsZXMgdGhlIG5leHQgKFwiPlwiKSBidXR0b24uIFJlcXVpcmVzIGBhbGxvd05hdmlnYXRlYCB0byBiZSBlbmFibGVkLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgY2FuTmF2aWdhdGVOZXh0ID0gdHJ1ZTtcblxuICAgIC8qKiBBbGxvdyB0aGUgbGVmdCB0aGUgc2lkZWJhci4gKi9cbiAgICBASW5wdXQoKVxuICAgIGFsbG93TGVmdFNpZGViYXIgPSBmYWxzZTtcblxuICAgIC8qKiBBbGxvdyB0aGUgcmlnaHQgc2lkZWJhci4gKi9cbiAgICBASW5wdXQoKVxuICAgIGFsbG93UmlnaHRTaWRlYmFyID0gZmFsc2U7XG5cbiAgICAvKiogVG9nZ2xlcyBQREYgdGh1bWJuYWlscy4gKi9cbiAgICBASW5wdXQoKVxuICAgIGFsbG93VGh1bWJuYWlscyA9IHRydWU7XG5cbiAgICAvKiogVG9nZ2xlcyByaWdodCBzaWRlYmFyIHZpc2liaWxpdHkuIFJlcXVpcmVzIGBhbGxvd1JpZ2h0U2lkZWJhcmAgdG8gYmUgc2V0IHRvIGB0cnVlYC4gKi9cbiAgICBASW5wdXQoKVxuICAgIHNob3dSaWdodFNpZGViYXIgPSBmYWxzZTtcblxuICAgIC8qKiBUb2dnbGVzIGxlZnQgc2lkZWJhciB2aXNpYmlsaXR5LiBSZXF1aXJlcyBgYWxsb3dMZWZ0U2lkZWJhcmAgdG8gYmUgc2V0IHRvIGB0cnVlYC4gKi9cbiAgICBASW5wdXQoKVxuICAgIHNob3dMZWZ0U2lkZWJhciA9IGZhbHNlO1xuXG4gICAgLyoqIFRoZSB0ZW1wbGF0ZSBmb3IgdGhlIHJpZ2h0IHNpZGViYXIuIFRoZSB0ZW1wbGF0ZSBjb250ZXh0IGNvbnRhaW5zIHRoZSBsb2FkZWQgbm9kZSBkYXRhLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgc2lkZWJhclJpZ2h0VGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT4gPSBudWxsO1xuXG4gICAgLyoqIFRoZSB0ZW1wbGF0ZSBmb3IgdGhlIGxlZnQgc2lkZWJhci4gVGhlIHRlbXBsYXRlIGNvbnRleHQgY29udGFpbnMgdGhlIGxvYWRlZCBub2RlIGRhdGEuICovXG4gICAgQElucHV0KClcbiAgICBzaWRlYmFyTGVmdFRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+ID0gbnVsbDtcblxuICAgIC8qKiBUaGUgdGVtcGxhdGUgZm9yIHRoZSBwZGYgdGh1bWJuYWlscy4gKi9cbiAgICBASW5wdXQoKVxuICAgIHRodW1ibmFpbHNUZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PiA9IG51bGw7XG5cbiAgICAvKiogTUlNRSB0eXBlIG9mIHRoZSBmaWxlIGNvbnRlbnQgKHdoZW4gbm90IGRldGVybWluZWQgYnkgdGhlIGZpbGVuYW1lIGV4dGVuc2lvbikuICovXG4gICAgQElucHV0KClcbiAgICBtaW1lVHlwZTogc3RyaW5nO1xuXG4gICAgLyoqIENvbnRlbnQgZmlsZW5hbWUuICovXG4gICAgQElucHV0KClcbiAgICBmaWxlTmFtZTogc3RyaW5nO1xuXG4gICAgLyoqIE51bWJlciBvZiB0aW1lcyB0aGUgVmlld2VyIHdpbGwgcmV0cnkgZmV0Y2hpbmcgY29udGVudCBSZW5kaXRpb24uXG4gICAgICogVGhlcmUgaXMgYSBkZWxheSBvZiBhdCBsZWFzdCBvbmUgc2Vjb25kIGJldHdlZW4gYXR0ZW1wdHMuXG4gICAgICovXG4gICAgQElucHV0KClcbiAgICBtYXhSZXRyaWVzID0gMTA7XG5cbiAgICAvKiogRW1pdHRlZCB3aGVuIHVzZXIgY2xpY2tzIHRoZSAnQmFjaycgYnV0dG9uLiAqL1xuICAgIEBPdXRwdXQoKVxuICAgIGdvQmFjayA9IG5ldyBFdmVudEVtaXR0ZXI8QmFzZUV2ZW50PGFueT4+KCk7XG5cbiAgICAvKiogRW1pdHRlZCB3aGVuIHVzZXIgY2xpY2tzIHRoZSAnUHJpbnQnIGJ1dHRvbi4gKi9cbiAgICBAT3V0cHV0KClcbiAgICBwcmludCA9IG5ldyBFdmVudEVtaXR0ZXI8QmFzZUV2ZW50PGFueT4+KCk7XG5cbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSB2aWV3ZXIgaXMgc2hvd24gb3IgaGlkZGVuLiAqL1xuICAgIEBPdXRwdXQoKVxuICAgIHNob3dWaWV3ZXJDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPGJvb2xlYW4+KCk7XG5cbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSBmaWxlbmFtZSBleHRlbnNpb24gY2hhbmdlcy4gKi9cbiAgICBAT3V0cHV0KClcbiAgICBleHRlbnNpb25DaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcblxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdXNlciBjbGlja3MgJ05hdmlnYXRlIEJlZm9yZScgKFwiPFwiKSBidXR0b24uICovXG4gICAgQE91dHB1dCgpXG4gICAgbmF2aWdhdGVCZWZvcmUgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgICAvKiogRW1pdHRlZCB3aGVuIHVzZXIgY2xpY2tzICdOYXZpZ2F0ZSBOZXh0JyAoXCI+XCIpIGJ1dHRvbi4gKi9cbiAgICBAT3V0cHV0KClcbiAgICBuYXZpZ2F0ZU5leHQgPSBuZXcgRXZlbnRFbWl0dGVyKCk7XG5cbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSBzaGFyZWQgbGluayB1c2VkIGlzIG5vdCB2YWxpZC4gKi9cbiAgICBAT3V0cHV0KClcbiAgICBpbnZhbGlkU2hhcmVkTGluayA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAgIFRSWV9USU1FT1VUOiBudW1iZXIgPSAyMDAwO1xuXG4gICAgdmlld2VyVHlwZSA9ICd1bmtub3duJztcbiAgICBpc0xvYWRpbmcgPSBmYWxzZTtcbiAgICBub2RlRW50cnk6IE5vZGVFbnRyeTtcblxuICAgIGV4dGVuc2lvblRlbXBsYXRlczogeyB0ZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PiwgaXNWaXNpYmxlOiBib29sZWFuIH1bXSA9IFtdO1xuICAgIGV4dGVybmFsRXh0ZW5zaW9uczogc3RyaW5nW10gPSBbXTtcbiAgICB1cmxGaWxlQ29udGVudDogc3RyaW5nO1xuICAgIG90aGVyTWVudTogYW55O1xuICAgIGV4dGVuc2lvbjogc3RyaW5nO1xuICAgIHNpZGViYXJSaWdodFRlbXBsYXRlQ29udGV4dDogeyBub2RlOiBOb2RlIH0gPSB7bm9kZTogbnVsbH07XG4gICAgc2lkZWJhckxlZnRUZW1wbGF0ZUNvbnRleHQ6IHsgbm9kZTogTm9kZSB9ID0ge25vZGU6IG51bGx9O1xuICAgIGZpbGVUaXRsZTogc3RyaW5nO1xuICAgIHZpZXdlckV4dGVuc2lvbnM6IEFycmF5PFZpZXdlckV4dGVuc2lvblJlZj4gPSBbXTtcblxuICAgIHByaXZhdGUgY2FjaGVCdXN0ZXJOdW1iZXI7XG5cbiAgICBwcml2YXRlIHN1YnNjcmlwdGlvbnM6IFN1YnNjcmlwdGlvbltdID0gW107XG5cbiAgICAvLyBFeHRlbnNpb25zIHRoYXQgYXJlIHN1cHBvcnRlZCBieSB0aGUgVmlld2VyIHdpdGhvdXQgY29udmVyc2lvblxuICAgIHByaXZhdGUgZXh0ZW5zaW9ucyA9IHtcbiAgICAgICAgaW1hZ2U6IFsncG5nJywgJ2pwZycsICdqcGVnJywgJ2dpZicsICdicG0nLCAnc3ZnJ10sXG4gICAgICAgIG1lZGlhOiBbJ3dhdicsICdtcDQnLCAnbXAzJywgJ3dlYm0nLCAnb2dnJ10sXG4gICAgICAgIHRleHQ6IFsndHh0JywgJ3htbCcsICdodG1sJywgJ2pzb24nLCAndHMnLCAnY3NzJywgJ21kJ10sXG4gICAgICAgIHBkZjogWydwZGYnXVxuICAgIH07XG5cbiAgICAvLyBNaW1lIHR5cGVzIHRoYXQgYXJlIHN1cHBvcnRlZCBieSB0aGUgVmlld2VyIHdpdGhvdXQgY29udmVyc2lvblxuICAgIHByaXZhdGUgbWltZVR5cGVzID0ge1xuICAgICAgICB0ZXh0OiBbJ3RleHQvcGxhaW4nLCAndGV4dC9jc3YnLCAndGV4dC94bWwnLCAndGV4dC9odG1sJywgJ2FwcGxpY2F0aW9uL3gtamF2YXNjcmlwdCddLFxuICAgICAgICBwZGY6IFsnYXBwbGljYXRpb24vcGRmJ10sXG4gICAgICAgIGltYWdlOiBbJ2ltYWdlL3BuZycsICdpbWFnZS9qcGVnJywgJ2ltYWdlL2dpZicsICdpbWFnZS9ibXAnLCAnaW1hZ2Uvc3ZnK3htbCddLFxuICAgICAgICBtZWRpYTogWyd2aWRlby9tcDQnLCAndmlkZW8vd2VibScsICd2aWRlby9vZ2cnLCAnYXVkaW8vbXBlZycsICdhdWRpby9vZ2cnLCAnYXVkaW8vd2F2J11cbiAgICB9O1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UsXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSB2aWV3VXRpbHM6IFZpZXdVdGlsU2VydmljZSxcbiAgICAgICAgICAgICAgICBwcml2YXRlIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UsXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBleHRlbnNpb25TZXJ2aWNlOiBBcHBFeHRlbnNpb25TZXJ2aWNlLFxuICAgICAgICAgICAgICAgIHByaXZhdGUgZWw6IEVsZW1lbnRSZWYpIHtcbiAgICB9XG5cbiAgICBpc1NvdXJjZURlZmluZWQoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiAodGhpcy51cmxGaWxlIHx8IHRoaXMuYmxvYkZpbGUgfHwgdGhpcy5ub2RlSWQgfHwgdGhpcy5zaGFyZWRMaW5rSWQpID8gdHJ1ZSA6IGZhbHNlO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMucHVzaChcbiAgICAgICAgICAgIHRoaXMuYXBpU2VydmljZS5ub2RlVXBkYXRlZC5zdWJzY3JpYmUoKG5vZGUpID0+IHRoaXMub25Ob2RlVXBkYXRlZChub2RlKSlcbiAgICAgICAgKTtcblxuICAgICAgICB0aGlzLmxvYWRFeHRlbnNpb25zKCk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBsb2FkRXh0ZW5zaW9ucygpIHtcbiAgICAgICAgdGhpcy52aWV3ZXJFeHRlbnNpb25zID0gdGhpcy5leHRlbnNpb25TZXJ2aWNlLmdldFZpZXdlckV4dGVuc2lvbnMoKTtcbiAgICAgICAgdGhpcy52aWV3ZXJFeHRlbnNpb25zXG4gICAgICAgICAgICAuZm9yRWFjaCgoZXh0ZW5zaW9uOiBWaWV3ZXJFeHRlbnNpb25SZWYpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLmV4dGVybmFsRXh0ZW5zaW9ucy5wdXNoKGV4dGVuc2lvbi5maWxlRXh0ZW5zaW9uKTtcbiAgICAgICAgICAgIH0pO1xuICAgIH1cblxuICAgIG5nT25EZXN0cm95KCkge1xuICAgICAgICB0aGlzLnN1YnNjcmlwdGlvbnMuZm9yRWFjaCgoc3Vic2NyaXB0aW9uKSA9PiBzdWJzY3JpcHRpb24udW5zdWJzY3JpYmUoKSk7XG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucyA9IFtdO1xuICAgIH1cblxuICAgIHByaXZhdGUgb25Ob2RlVXBkYXRlZChub2RlOiBOb2RlKSB7XG4gICAgICAgIGlmIChub2RlICYmIG5vZGUuaWQgPT09IHRoaXMubm9kZUlkKSB7XG4gICAgICAgICAgICB0aGlzLmdlbmVyYXRlQ2FjaGVCdXN0ZXJOdW1iZXIoKTtcbiAgICAgICAgICAgIHRoaXMuaXNMb2FkaW5nID0gdHJ1ZTtcbiAgICAgICAgICAgIHRoaXMuc2V0VXBOb2RlRmlsZShub2RlKS50aGVuKCgpID0+IHtcbiAgICAgICAgICAgICAgICB0aGlzLmlzTG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XG4gICAgICAgIGlmICh0aGlzLnNob3dWaWV3ZXIpIHtcbiAgICAgICAgICAgIGlmICghdGhpcy5pc1NvdXJjZURlZmluZWQoKSkge1xuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignQSBjb250ZW50IHNvdXJjZSBhdHRyaWJ1dGUgdmFsdWUgaXMgbWlzc2luZy4nKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuaXNMb2FkaW5nID0gdHJ1ZTtcblxuICAgICAgICAgICAgaWYgKHRoaXMuYmxvYkZpbGUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldFVwQmxvYkRhdGEoKTtcbiAgICAgICAgICAgICAgICB0aGlzLmlzTG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnVybEZpbGUpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNldFVwVXJsRmlsZSgpO1xuICAgICAgICAgICAgICAgIHRoaXMuaXNMb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMubm9kZUlkKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5hcGlTZXJ2aWNlLm5vZGVzQXBpLmdldE5vZGUodGhpcy5ub2RlSWQsIHtpbmNsdWRlOiBbJ2FsbG93YWJsZU9wZXJhdGlvbnMnXX0pLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgIChub2RlOiBOb2RlRW50cnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubm9kZUVudHJ5ID0gbm9kZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0VXBOb2RlRmlsZShub2RlLmVudHJ5KS50aGVuKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzTG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAgICAgICAgIChlcnJvcikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0xvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcignVGhpcyBub2RlIGRvZXMgbm90IGV4aXN0Jyk7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLnNoYXJlZExpbmtJZCkge1xuICAgICAgICAgICAgICAgIHRoaXMuYWxsb3dHb0JhY2sgPSBmYWxzZTtcblxuICAgICAgICAgICAgICAgIHRoaXMuYXBpU2VydmljZS5zaGFyZWRMaW5rc0FwaS5nZXRTaGFyZWRMaW5rKHRoaXMuc2hhcmVkTGlua0lkKS50aGVuKFxuICAgICAgICAgICAgICAgICAgICAoc2hhcmVkTGlua0VudHJ5OiBTaGFyZWRMaW5rRW50cnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2V0VXBTaGFyZWRMaW5rRmlsZShzaGFyZWRMaW5rRW50cnkpO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0xvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0xvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcignVGhpcyBzaGFyZWRMaW5rIGRvZXMgbm90IGV4aXN0Jyk7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmludmFsaWRTaGFyZWRMaW5rLm5leHQoKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIHNldFVwQmxvYkRhdGEoKSB7XG4gICAgICAgIHRoaXMuZmlsZVRpdGxlID0gdGhpcy5nZXREaXNwbGF5TmFtZSgnVW5rbm93bicpO1xuICAgICAgICB0aGlzLm1pbWVUeXBlID0gdGhpcy5ibG9iRmlsZS50eXBlO1xuICAgICAgICB0aGlzLnZpZXdlclR5cGUgPSB0aGlzLmdldFZpZXdlclR5cGVCeU1pbWVUeXBlKHRoaXMubWltZVR5cGUpO1xuXG4gICAgICAgIHRoaXMuYWxsb3dEb3dubG9hZCA9IGZhbHNlO1xuICAgICAgICAvLyBUT0RPOiB3cmFwIGJsb2IgaW50byB0aGUgZGF0YSB1cmwgYW5kIGFsbG93IGRvd25sb2FkaW5nXG5cbiAgICAgICAgdGhpcy5leHRlbnNpb25DaGFuZ2UuZW1pdCh0aGlzLm1pbWVUeXBlKTtcbiAgICAgICAgdGhpcy5zY3JvbGxUb3AoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHNldFVwVXJsRmlsZSgpIHtcbiAgICAgICAgY29uc3QgZmlsZW5hbWVGcm9tVXJsID0gdGhpcy5nZXRGaWxlbmFtZUZyb21VcmwodGhpcy51cmxGaWxlKTtcbiAgICAgICAgdGhpcy5maWxlVGl0bGUgPSB0aGlzLmdldERpc3BsYXlOYW1lKGZpbGVuYW1lRnJvbVVybCk7XG4gICAgICAgIHRoaXMuZXh0ZW5zaW9uID0gdGhpcy5nZXRGaWxlRXh0ZW5zaW9uKGZpbGVuYW1lRnJvbVVybCk7XG4gICAgICAgIHRoaXMudXJsRmlsZUNvbnRlbnQgPSB0aGlzLnVybEZpbGU7XG5cbiAgICAgICAgdGhpcy5maWxlTmFtZSA9IHRoaXMuZGlzcGxheU5hbWU7XG5cbiAgICAgICAgdGhpcy52aWV3ZXJUeXBlID0gdGhpcy51cmxGaWxlVmlld2VyIHx8IHRoaXMuZ2V0Vmlld2VyVHlwZUJ5RXh0ZW5zaW9uKHRoaXMuZXh0ZW5zaW9uKTtcbiAgICAgICAgaWYgKHRoaXMudmlld2VyVHlwZSA9PT0gJ3Vua25vd24nKSB7XG4gICAgICAgICAgICB0aGlzLnZpZXdlclR5cGUgPSB0aGlzLmdldFZpZXdlclR5cGVCeU1pbWVUeXBlKHRoaXMubWltZVR5cGUpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5leHRlbnNpb25DaGFuZ2UuZW1pdCh0aGlzLmV4dGVuc2lvbik7XG4gICAgICAgIHRoaXMuc2Nyb2xsVG9wKCk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhc3luYyBzZXRVcE5vZGVGaWxlKGRhdGE6IE5vZGUpIHtcbiAgICAgICAgbGV0IHNldHVwTm9kZTtcblxuICAgICAgICBpZiAoZGF0YS5jb250ZW50KSB7XG4gICAgICAgICAgICB0aGlzLm1pbWVUeXBlID0gZGF0YS5jb250ZW50Lm1pbWVUeXBlO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5maWxlVGl0bGUgPSB0aGlzLmdldERpc3BsYXlOYW1lKGRhdGEubmFtZSk7XG5cbiAgICAgICAgdGhpcy51cmxGaWxlQ29udGVudCA9IHRoaXMuYXBpU2VydmljZS5jb250ZW50QXBpLmdldENvbnRlbnRVcmwoZGF0YS5pZCk7XG4gICAgICAgIHRoaXMudXJsRmlsZUNvbnRlbnQgPSB0aGlzLmNhY2hlQnVzdGVyTnVtYmVyID8gdGhpcy51cmxGaWxlQ29udGVudCArICcmJyArIHRoaXMuY2FjaGVCdXN0ZXJOdW1iZXIgOiB0aGlzLnVybEZpbGVDb250ZW50O1xuXG4gICAgICAgIHRoaXMuZXh0ZW5zaW9uID0gdGhpcy5nZXRGaWxlRXh0ZW5zaW9uKGRhdGEubmFtZSk7XG5cbiAgICAgICAgdGhpcy5maWxlTmFtZSA9IGRhdGEubmFtZTtcblxuICAgICAgICB0aGlzLnZpZXdlclR5cGUgPSB0aGlzLmdldFZpZXdlclR5cGVCeUV4dGVuc2lvbih0aGlzLmV4dGVuc2lvbik7XG4gICAgICAgIGlmICh0aGlzLnZpZXdlclR5cGUgPT09ICd1bmtub3duJykge1xuICAgICAgICAgICAgdGhpcy52aWV3ZXJUeXBlID0gdGhpcy5nZXRWaWV3ZXJUeXBlQnlNaW1lVHlwZSh0aGlzLm1pbWVUeXBlKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0aGlzLnZpZXdlclR5cGUgPT09ICd1bmtub3duJykge1xuICAgICAgICAgICAgc2V0dXBOb2RlID0gdGhpcy5kaXNwbGF5Tm9kZVJlbmRpdGlvbihkYXRhLmlkKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuZXh0ZW5zaW9uQ2hhbmdlLmVtaXQodGhpcy5leHRlbnNpb24pO1xuICAgICAgICB0aGlzLnNpZGViYXJSaWdodFRlbXBsYXRlQ29udGV4dC5ub2RlID0gZGF0YTtcbiAgICAgICAgdGhpcy5zaWRlYmFyTGVmdFRlbXBsYXRlQ29udGV4dC5ub2RlID0gZGF0YTtcbiAgICAgICAgdGhpcy5zY3JvbGxUb3AoKTtcblxuICAgICAgICByZXR1cm4gc2V0dXBOb2RlO1xuICAgIH1cblxuICAgIHByaXZhdGUgc2V0VXBTaGFyZWRMaW5rRmlsZShkZXRhaWxzOiBhbnkpIHtcbiAgICAgICAgdGhpcy5taW1lVHlwZSA9IGRldGFpbHMuZW50cnkuY29udGVudC5taW1lVHlwZTtcbiAgICAgICAgdGhpcy5maWxlVGl0bGUgPSB0aGlzLmdldERpc3BsYXlOYW1lKGRldGFpbHMuZW50cnkubmFtZSk7XG4gICAgICAgIHRoaXMuZXh0ZW5zaW9uID0gdGhpcy5nZXRGaWxlRXh0ZW5zaW9uKGRldGFpbHMuZW50cnkubmFtZSk7XG4gICAgICAgIHRoaXMuZmlsZU5hbWUgPSBkZXRhaWxzLmVudHJ5Lm5hbWU7XG5cbiAgICAgICAgdGhpcy51cmxGaWxlQ29udGVudCA9IHRoaXMuYXBpU2VydmljZS5jb250ZW50QXBpLmdldFNoYXJlZExpbmtDb250ZW50VXJsKHRoaXMuc2hhcmVkTGlua0lkLCBmYWxzZSk7XG5cbiAgICAgICAgdGhpcy52aWV3ZXJUeXBlID0gdGhpcy5nZXRWaWV3ZXJUeXBlQnlNaW1lVHlwZSh0aGlzLm1pbWVUeXBlKTtcbiAgICAgICAgaWYgKHRoaXMudmlld2VyVHlwZSA9PT0gJ3Vua25vd24nKSB7XG4gICAgICAgICAgICB0aGlzLnZpZXdlclR5cGUgPSB0aGlzLmdldFZpZXdlclR5cGVCeUV4dGVuc2lvbih0aGlzLmV4dGVuc2lvbik7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy52aWV3ZXJUeXBlID09PSAndW5rbm93bicpIHtcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheVNoYXJlZExpbmtSZW5kaXRpb24odGhpcy5zaGFyZWRMaW5rSWQpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5leHRlbnNpb25DaGFuZ2UuZW1pdCh0aGlzLmV4dGVuc2lvbik7XG4gICAgfVxuXG4gICAgdG9nZ2xlU2lkZWJhcigpIHtcbiAgICAgICAgdGhpcy5zaG93UmlnaHRTaWRlYmFyID0gIXRoaXMuc2hvd1JpZ2h0U2lkZWJhcjtcbiAgICAgICAgaWYgKHRoaXMuc2hvd1JpZ2h0U2lkZWJhciAmJiB0aGlzLm5vZGVJZCkge1xuICAgICAgICAgICAgdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkubm9kZXMuZ2V0Tm9kZSh0aGlzLm5vZGVJZCwge2luY2x1ZGU6IFsnYWxsb3dhYmxlT3BlcmF0aW9ucyddfSlcbiAgICAgICAgICAgICAgICAudGhlbigobm9kZUVudHJ5OiBOb2RlRW50cnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaWRlYmFyUmlnaHRUZW1wbGF0ZUNvbnRleHQubm9kZSA9IG5vZGVFbnRyeS5lbnRyeTtcbiAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHRvZ2dsZUxlZnRTaWRlYmFyKCkge1xuICAgICAgICB0aGlzLnNob3dMZWZ0U2lkZWJhciA9ICF0aGlzLnNob3dMZWZ0U2lkZWJhcjtcbiAgICAgICAgaWYgKHRoaXMuc2hvd1JpZ2h0U2lkZWJhciAmJiB0aGlzLm5vZGVJZCkge1xuICAgICAgICAgICAgdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkubm9kZXMuZ2V0Tm9kZSh0aGlzLm5vZGVJZCwge2luY2x1ZGU6IFsnYWxsb3dhYmxlT3BlcmF0aW9ucyddfSlcbiAgICAgICAgICAgICAgICAudGhlbigobm9kZUVudHJ5OiBOb2RlRW50cnkpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zaWRlYmFyTGVmdFRlbXBsYXRlQ29udGV4dC5ub2RlID0gbm9kZUVudHJ5LmVudHJ5O1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXREaXNwbGF5TmFtZShuYW1lKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmRpc3BsYXlOYW1lIHx8IG5hbWU7XG4gICAgfVxuXG4gICAgc2Nyb2xsVG9wKCkge1xuICAgICAgICB3aW5kb3cuc2Nyb2xsVG8oMCwgMSk7XG4gICAgfVxuXG4gICAgZ2V0Vmlld2VyVHlwZUJ5TWltZVR5cGUobWltZVR5cGU6IHN0cmluZykge1xuICAgICAgICBpZiAobWltZVR5cGUpIHtcbiAgICAgICAgICAgIG1pbWVUeXBlID0gbWltZVR5cGUudG9Mb3dlckNhc2UoKTtcblxuICAgICAgICAgICAgY29uc3QgZWRpdG9yVHlwZXMgPSBPYmplY3Qua2V5cyh0aGlzLm1pbWVUeXBlcyk7XG4gICAgICAgICAgICBmb3IgKGNvbnN0IHR5cGUgb2YgZWRpdG9yVHlwZXMpIHtcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5taW1lVHlwZXNbdHlwZV0uaW5kZXhPZihtaW1lVHlwZSkgPj0gMCkge1xuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHlwZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuICd1bmtub3duJztcbiAgICB9XG5cbiAgICBnZXRWaWV3ZXJUeXBlQnlFeHRlbnNpb24oZXh0ZW5zaW9uOiBzdHJpbmcpIHtcbiAgICAgICAgaWYgKGV4dGVuc2lvbikge1xuICAgICAgICAgICAgZXh0ZW5zaW9uID0gZXh0ZW5zaW9uLnRvTG93ZXJDYXNlKCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5pc0N1c3RvbVZpZXdlckV4dGVuc2lvbihleHRlbnNpb24pKSB7XG4gICAgICAgICAgICByZXR1cm4gJ2N1c3RvbSc7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5leHRlbnNpb25zLmltYWdlLmluZGV4T2YoZXh0ZW5zaW9uKSA+PSAwKSB7XG4gICAgICAgICAgICByZXR1cm4gJ2ltYWdlJztcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0aGlzLmV4dGVuc2lvbnMubWVkaWEuaW5kZXhPZihleHRlbnNpb24pID49IDApIHtcbiAgICAgICAgICAgIHJldHVybiAnbWVkaWEnO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuZXh0ZW5zaW9ucy50ZXh0LmluZGV4T2YoZXh0ZW5zaW9uKSA+PSAwKSB7XG4gICAgICAgICAgICByZXR1cm4gJ3RleHQnO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuZXh0ZW5zaW9ucy5wZGYuaW5kZXhPZihleHRlbnNpb24pID49IDApIHtcbiAgICAgICAgICAgIHJldHVybiAncGRmJztcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiAndW5rbm93bic7XG4gICAgfVxuXG4gICAgb25CYWNrQnV0dG9uQ2xpY2soKSB7XG4gICAgICAgIHRoaXMuY2xvc2UoKTtcbiAgICB9XG5cbiAgICBvbk5hdmlnYXRlQmVmb3JlQ2xpY2soKSB7XG4gICAgICAgIHRoaXMubmF2aWdhdGVCZWZvcmUubmV4dCgpO1xuICAgIH1cblxuICAgIG9uTmF2aWdhdGVOZXh0Q2xpY2soKSB7XG4gICAgICAgIHRoaXMubmF2aWdhdGVOZXh0Lm5leHQoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBjbG9zZSB0aGUgdmlld2VyXG4gICAgICovXG4gICAgY2xvc2UoKSB7XG4gICAgICAgIGlmICh0aGlzLm90aGVyTWVudSkge1xuICAgICAgICAgICAgdGhpcy5vdGhlck1lbnUuaGlkZGVuID0gZmFsc2U7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5zaG93Vmlld2VyID0gZmFsc2U7XG4gICAgICAgIHRoaXMuc2hvd1ZpZXdlckNoYW5nZS5lbWl0KHRoaXMuc2hvd1ZpZXdlcik7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogZ2V0IEZpbGUgbmFtZSBmcm9tIHVybFxuICAgICAqXG4gICAgICogQHBhcmFtICB1cmwgLSB1cmwgZmlsZVxuICAgICAqL1xuICAgIGdldEZpbGVuYW1lRnJvbVVybCh1cmw6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgICAgIGNvbnN0IGFuY2hvciA9IHVybC5pbmRleE9mKCcjJyk7XG4gICAgICAgIGNvbnN0IHF1ZXJ5ID0gdXJsLmluZGV4T2YoJz8nKTtcbiAgICAgICAgY29uc3QgZW5kID0gTWF0aC5taW4oXG4gICAgICAgICAgICBhbmNob3IgPiAwID8gYW5jaG9yIDogdXJsLmxlbmd0aCxcbiAgICAgICAgICAgIHF1ZXJ5ID4gMCA/IHF1ZXJ5IDogdXJsLmxlbmd0aCk7XG4gICAgICAgIHJldHVybiB1cmwuc3Vic3RyaW5nKHVybC5sYXN0SW5kZXhPZignLycsIGVuZCkgKyAxLCBlbmQpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEdldCBmaWxlIGV4dGVuc2lvbiBmcm9tIHRoZSBzdHJpbmcuXG4gICAgICogU3VwcG9ydHMgdGhlIFVSTCBmb3JtYXRzIGxpa2U6XG4gICAgICogaHR0cDovL2xvY2FsaG9zdC90ZXN0LmpwZz9jYWNoZT0xMDAwXG4gICAgICogaHR0cDovL2xvY2FsaG9zdC90ZXN0LmpwZyNjYWNoZT0xMDAwXG4gICAgICpcbiAgICAgKiBAcGFyYW0gZmlsZU5hbWUgLSBmaWxlIG5hbWVcbiAgICAgKi9cbiAgICBnZXRGaWxlRXh0ZW5zaW9uKGZpbGVOYW1lOiBzdHJpbmcpOiBzdHJpbmcge1xuICAgICAgICBpZiAoZmlsZU5hbWUpIHtcbiAgICAgICAgICAgIGNvbnN0IG1hdGNoID0gZmlsZU5hbWUubWF0Y2goL1xcLihbXlxcLi9cXD9cXCNdKykoJHxcXD98XFwjKS8pO1xuICAgICAgICAgICAgcmV0dXJuIG1hdGNoID8gbWF0Y2hbMV0gOiBudWxsO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIGlzQ3VzdG9tVmlld2VyRXh0ZW5zaW9uKGV4dGVuc2lvbjogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgICAgIGNvbnN0IGV4dGVuc2lvbnM6IGFueSA9IHRoaXMuZXh0ZXJuYWxFeHRlbnNpb25zIHx8IFtdO1xuXG4gICAgICAgIGlmIChleHRlbnNpb24gJiYgZXh0ZW5zaW9ucy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICBleHRlbnNpb24gPSBleHRlbnNpb24udG9Mb3dlckNhc2UoKTtcbiAgICAgICAgICAgIHJldHVybiBleHRlbnNpb25zLmZsYXQoKS5pbmRleE9mKGV4dGVuc2lvbikgPj0gMDtcbiAgICAgICAgfVxuXG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBLZXlib2FyZCBldmVudCBsaXN0ZW5lclxuICAgICAqIEBwYXJhbSAgZXZlbnRcbiAgICAgKi9cbiAgICBASG9zdExpc3RlbmVyKCdkb2N1bWVudDprZXl1cCcsIFsnJGV2ZW50J10pXG4gICAgaGFuZGxlS2V5Ym9hcmRFdmVudChldmVudDogS2V5Ym9hcmRFdmVudCkge1xuICAgICAgICBjb25zdCBrZXkgPSBldmVudC5rZXlDb2RlO1xuXG4gICAgICAgIC8vIEVzY1xuICAgICAgICBpZiAoa2V5ID09PSAyNyAmJiB0aGlzLm92ZXJsYXlNb2RlKSB7IC8vIGVzY1xuICAgICAgICAgICAgdGhpcy5jbG9zZSgpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gTGVmdCBhcnJvd1xuICAgICAgICBpZiAoa2V5ID09PSAzNyAmJiB0aGlzLmNhbk5hdmlnYXRlQmVmb3JlKSB7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgdGhpcy5vbk5hdmlnYXRlQmVmb3JlQ2xpY2soKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIFJpZ2h0IGFycm93XG4gICAgICAgIGlmIChrZXkgPT09IDM5ICYmIHRoaXMuY2FuTmF2aWdhdGVOZXh0KSB7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgdGhpcy5vbk5hdmlnYXRlTmV4dENsaWNrKCk7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBDdHJsK0ZcbiAgICAgICAgaWYgKGtleSA9PT0gNzAgJiYgZXZlbnQuY3RybEtleSkge1xuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgIHRoaXMuZW50ZXJGdWxsU2NyZWVuKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcmludENvbnRlbnQoKSB7XG4gICAgICAgIGlmICh0aGlzLmFsbG93UHJpbnQpIHtcbiAgICAgICAgICAgIGNvbnN0IGFyZ3MgPSBuZXcgQmFzZUV2ZW50KCk7XG4gICAgICAgICAgICB0aGlzLnByaW50Lm5leHQoYXJncyk7XG5cbiAgICAgICAgICAgIGlmICghYXJncy5kZWZhdWx0UHJldmVudGVkKSB7XG4gICAgICAgICAgICAgICAgdGhpcy52aWV3VXRpbHMucHJpbnRGaWxlR2VuZXJpYyh0aGlzLm5vZGVJZCwgdGhpcy5taW1lVHlwZSk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBUcmlnZ2VycyBmdWxsIHNjcmVlbiBtb2RlIHdpdGggYSBtYWluIGNvbnRlbnQgYXJlYSBkaXNwbGF5ZWQuXG4gICAgICovXG4gICAgZW50ZXJGdWxsU2NyZWVuKCk6IHZvaWQge1xuICAgICAgICBpZiAodGhpcy5hbGxvd0Z1bGxTY3JlZW4pIHtcbiAgICAgICAgICAgIGNvbnN0IGNvbnRhaW5lciA9IHRoaXMuZWwubmF0aXZlRWxlbWVudC5xdWVyeVNlbGVjdG9yKCcuYWRmLXZpZXdlcl9fZnVsbHNjcmVlbi1jb250YWluZXInKTtcbiAgICAgICAgICAgIGlmIChjb250YWluZXIpIHtcbiAgICAgICAgICAgICAgICBpZiAoY29udGFpbmVyLnJlcXVlc3RGdWxsc2NyZWVuKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRhaW5lci5yZXF1ZXN0RnVsbHNjcmVlbigpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoY29udGFpbmVyLndlYmtpdFJlcXVlc3RGdWxsc2NyZWVuKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRhaW5lci53ZWJraXRSZXF1ZXN0RnVsbHNjcmVlbigpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoY29udGFpbmVyLm1velJlcXVlc3RGdWxsU2NyZWVuKSB7XG4gICAgICAgICAgICAgICAgICAgIGNvbnRhaW5lci5tb3pSZXF1ZXN0RnVsbFNjcmVlbigpO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoY29udGFpbmVyLm1zUmVxdWVzdEZ1bGxzY3JlZW4pIHtcbiAgICAgICAgICAgICAgICAgICAgY29udGFpbmVyLm1zUmVxdWVzdEZ1bGxzY3JlZW4oKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIGFzeW5jIGRpc3BsYXlOb2RlUmVuZGl0aW9uKG5vZGVJZDogc3RyaW5nKSB7XG4gICAgICAgIHRyeSB7XG4gICAgICAgICAgICBjb25zdCByZW5kaXRpb24gPSBhd2FpdCB0aGlzLnJlc29sdmVSZW5kaXRpb24obm9kZUlkLCAncGRmJyk7XG4gICAgICAgICAgICBpZiAocmVuZGl0aW9uKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgcmVuZGl0aW9uSWQgPSByZW5kaXRpb24uZW50cnkuaWQ7XG5cbiAgICAgICAgICAgICAgICBpZiAocmVuZGl0aW9uSWQgPT09ICdwZGYnKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudmlld2VyVHlwZSA9ICdwZGYnO1xuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocmVuZGl0aW9uSWQgPT09ICdpbWdwcmV2aWV3Jykge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnZpZXdlclR5cGUgPSAnaW1hZ2UnO1xuICAgICAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgICAgIHRoaXMudXJsRmlsZUNvbnRlbnQgPSB0aGlzLmFwaVNlcnZpY2UuY29udGVudEFwaS5nZXRSZW5kaXRpb25Vcmwobm9kZUlkLCByZW5kaXRpb25JZCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKGVycik7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIGFzeW5jIGRpc3BsYXlTaGFyZWRMaW5rUmVuZGl0aW9uKHNoYXJlZElkOiBzdHJpbmcpIHtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNvbnN0IHJlbmRpdGlvbjogUmVuZGl0aW9uRW50cnkgPSBhd2FpdCB0aGlzLmFwaVNlcnZpY2UucmVuZGl0aW9uc0FwaS5nZXRTaGFyZWRMaW5rUmVuZGl0aW9uKHNoYXJlZElkLCAncGRmJyk7XG4gICAgICAgICAgICBpZiAocmVuZGl0aW9uLmVudHJ5LnN0YXR1cy50b1N0cmluZygpID09PSAnQ1JFQVRFRCcpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnZpZXdlclR5cGUgPSAncGRmJztcbiAgICAgICAgICAgICAgICB0aGlzLnVybEZpbGVDb250ZW50ID0gdGhpcy5hcGlTZXJ2aWNlLmNvbnRlbnRBcGkuZ2V0U2hhcmVkTGlua1JlbmRpdGlvblVybChzaGFyZWRJZCwgJ3BkZicpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9IGNhdGNoIChlcnJvcikge1xuICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKGVycm9yKTtcbiAgICAgICAgICAgIHRyeSB7XG4gICAgICAgICAgICAgICAgY29uc3QgcmVuZGl0aW9uOiBSZW5kaXRpb25FbnRyeSA9IGF3YWl0IHRoaXMuYXBpU2VydmljZS5yZW5kaXRpb25zQXBpLmdldFNoYXJlZExpbmtSZW5kaXRpb24oc2hhcmVkSWQsICdpbWdwcmV2aWV3Jyk7XG4gICAgICAgICAgICAgICAgaWYgKHJlbmRpdGlvbi5lbnRyeS5zdGF0dXMudG9TdHJpbmcoKSA9PT0gJ0NSRUFURUQnKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudmlld2VyVHlwZSA9ICdpbWFnZSc7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudXJsRmlsZUNvbnRlbnQgPSB0aGlzLmFwaVNlcnZpY2UuY29udGVudEFwaS5nZXRTaGFyZWRMaW5rUmVuZGl0aW9uVXJsKHNoYXJlZElkLCAnaW1ncHJldmlldycpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKGVycm9yKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgcmVzb2x2ZVJlbmRpdGlvbihub2RlSWQ6IHN0cmluZywgcmVuZGl0aW9uSWQ6IHN0cmluZyk6IFByb21pc2U8UmVuZGl0aW9uRW50cnk+IHtcbiAgICAgICAgcmVuZGl0aW9uSWQgPSByZW5kaXRpb25JZC50b0xvd2VyQ2FzZSgpO1xuXG4gICAgICAgIGNvbnN0IHN1cHBvcnRlZFJlbmRpdGlvbjogUmVuZGl0aW9uUGFnaW5nID0gYXdhaXQgdGhpcy5hcGlTZXJ2aWNlLnJlbmRpdGlvbnNBcGkuZ2V0UmVuZGl0aW9ucyhub2RlSWQpO1xuXG4gICAgICAgIGxldCByZW5kaXRpb246IFJlbmRpdGlvbkVudHJ5ID0gc3VwcG9ydGVkUmVuZGl0aW9uLmxpc3QuZW50cmllcy5maW5kKChyZW5kaXRpb25FbnRyeTogUmVuZGl0aW9uRW50cnkpID0+IHJlbmRpdGlvbkVudHJ5LmVudHJ5LmlkLnRvTG93ZXJDYXNlKCkgPT09IHJlbmRpdGlvbklkKTtcbiAgICAgICAgaWYgKCFyZW5kaXRpb24pIHtcbiAgICAgICAgICAgIHJlbmRpdGlvbklkID0gJ2ltZ3ByZXZpZXcnO1xuICAgICAgICAgICAgcmVuZGl0aW9uID0gc3VwcG9ydGVkUmVuZGl0aW9uLmxpc3QuZW50cmllcy5maW5kKChyZW5kaXRpb25FbnRyeTogUmVuZGl0aW9uRW50cnkpID0+IHJlbmRpdGlvbkVudHJ5LmVudHJ5LmlkLnRvTG93ZXJDYXNlKCkgPT09IHJlbmRpdGlvbklkKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChyZW5kaXRpb24pIHtcbiAgICAgICAgICAgIGNvbnN0IHN0YXR1czogc3RyaW5nID0gcmVuZGl0aW9uLmVudHJ5LnN0YXR1cy50b1N0cmluZygpO1xuXG4gICAgICAgICAgICBpZiAoc3RhdHVzID09PSAnTk9UX0NSRUFURUQnKSB7XG4gICAgICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5hcGlTZXJ2aWNlLnJlbmRpdGlvbnNBcGkuY3JlYXRlUmVuZGl0aW9uKG5vZGVJZCwge2lkOiByZW5kaXRpb25JZH0pLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy52aWV3ZXJUeXBlID0gJ2luX2NyZWF0aW9uJztcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgICAgIHJlbmRpdGlvbiA9IGF3YWl0IHRoaXMud2FpdFJlbmRpdGlvbihub2RlSWQsIHJlbmRpdGlvbklkKTtcbiAgICAgICAgICAgICAgICB9IGNhdGNoIChlcnIpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKGVycik7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfVxuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHJlbmRpdGlvbjtcbiAgICB9XG5cbiAgICBwcml2YXRlIGFzeW5jIHdhaXRSZW5kaXRpb24obm9kZUlkOiBzdHJpbmcsIHJlbmRpdGlvbklkOiBzdHJpbmcpOiBQcm9taXNlPFJlbmRpdGlvbkVudHJ5PiB7XG4gICAgICAgIGxldCBjdXJyZW50UmV0cnk6IG51bWJlciA9IDA7XG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZTxSZW5kaXRpb25FbnRyeT4oKHJlc29sdmUsIHJlamVjdCkgPT4ge1xuICAgICAgICAgICAgY29uc3QgaW50ZXJ2YWxJZCA9IHNldEludGVydmFsKCgpID0+IHtcbiAgICAgICAgICAgICAgICBjdXJyZW50UmV0cnkrKztcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5tYXhSZXRyaWVzID49IGN1cnJlbnRSZXRyeSkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmFwaVNlcnZpY2UucmVuZGl0aW9uc0FwaS5nZXRSZW5kaXRpb24obm9kZUlkLCByZW5kaXRpb25JZCkudGhlbigocmVuZGl0aW9uOiBSZW5kaXRpb25FbnRyeSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgc3RhdHVzOiBzdHJpbmcgPSByZW5kaXRpb24uZW50cnkuc3RhdHVzLnRvU3RyaW5nKCk7XG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoc3RhdHVzID09PSAnQ1JFQVRFRCcpIHtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZW5kaXRpb25JZCA9PT0gJ3BkZicpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy52aWV3ZXJUeXBlID0gJ3BkZic7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmIChyZW5kaXRpb25JZCA9PT0gJ2ltZ3ByZXZpZXcnKSB7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudmlld2VyVHlwZSA9ICdpbWFnZSc7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy51cmxGaWxlQ29udGVudCA9IHRoaXMuYXBpU2VydmljZS5jb250ZW50QXBpLmdldFJlbmRpdGlvblVybChub2RlSWQsIHJlbmRpdGlvbklkKTtcblxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNsZWFySW50ZXJ2YWwoaW50ZXJ2YWxJZCk7XG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc29sdmUocmVuZGl0aW9uKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAgICAgfSwgKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy52aWV3ZXJUeXBlID0gJ2Vycm9yX2luX2NyZWF0aW9uJztcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiByZWplY3QoKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5pc0xvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52aWV3ZXJUeXBlID0gJ2Vycm9yX2luX2NyZWF0aW9uJztcbiAgICAgICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbChpbnRlcnZhbElkKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9LCB0aGlzLlRSWV9USU1FT1VUKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgY2hlY2tFeHRlbnNpb25zKGV4dGVuc2lvbkFsbG93ZWQpIHtcbiAgICAgICAgaWYgKHR5cGVvZiBleHRlbnNpb25BbGxvd2VkID09PSAnc3RyaW5nJykge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZXh0ZW5zaW9uLnRvTG93ZXJDYXNlKCkgPT09IGV4dGVuc2lvbkFsbG93ZWQudG9Mb3dlckNhc2UoKTtcbiAgICAgICAgfSBlbHNlIGlmIChleHRlbnNpb25BbGxvd2VkLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIHJldHVybiBleHRlbnNpb25BbGxvd2VkLmZpbmQoKGN1cnJlbnRFeHRlbnNpb24pID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5leHRlbnNpb24udG9Mb3dlckNhc2UoKSA9PT0gY3VycmVudEV4dGVuc2lvbi50b0xvd2VyQ2FzZSgpO1xuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgIH1cblxuICAgIHByaXZhdGUgZ2VuZXJhdGVDYWNoZUJ1c3Rlck51bWJlcigpIHtcbiAgICAgICAgdGhpcy5jYWNoZUJ1c3Rlck51bWJlciA9IERhdGUubm93KCk7XG4gICAgfVxufVxuIl19