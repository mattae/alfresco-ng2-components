/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ContentChild, Directive, Input, TemplateRef } from '@angular/core';
import { ViewerComponent } from '../components/viewer.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
export class ViewerExtensionDirective {
    /**
     * @param {?} viewerComponent
     */
    constructor(viewerComponent) {
        this.viewerComponent = viewerComponent;
        this.onDestroy$ = new Subject();
    }
    /**
     * @return {?}
     */
    ngAfterContentInit() {
        this.templateModel = { template: this.template, isVisible: false };
        this.viewerComponent.extensionTemplates.push(this.templateModel);
        this.viewerComponent.extensionChange
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} fileExtension
         * @return {?}
         */
        fileExtension => {
            this.templateModel.isVisible = this.isVisible(fileExtension);
        }));
        if (this.supportedExtensions instanceof Array) {
            this.supportedExtensions.forEach((/**
             * @param {?} extension
             * @return {?}
             */
            (extension) => {
                this.viewerComponent.externalExtensions.push(extension);
            }));
        }
    }
    /**
     * @return {?}
     */
    ngOnDestroy() {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    }
    /**
     * check if the current extension in the viewer is compatible with this extension checking against supportedExtensions
     * @param {?} fileExtension
     * @return {?}
     */
    isVisible(fileExtension) {
        /** @type {?} */
        let supportedExtension;
        if (this.supportedExtensions && (this.supportedExtensions instanceof Array)) {
            supportedExtension = this.supportedExtensions.find((/**
             * @param {?} extension
             * @return {?}
             */
            (extension) => {
                return extension.toLowerCase() === fileExtension;
            }));
        }
        return !!supportedExtension;
    }
}
ViewerExtensionDirective.decorators = [
    { type: Directive, args: [{
                selector: 'adf-viewer-extension'
            },] }
];
/** @nocollapse */
ViewerExtensionDirective.ctorParameters = () => [
    { type: ViewerComponent }
];
ViewerExtensionDirective.propDecorators = {
    template: [{ type: ContentChild, args: [TemplateRef, { static: true },] }],
    urlFileContent: [{ type: Input }],
    extension: [{ type: Input }],
    supportedExtensions: [{ type: Input }]
};
if (false) {
    /** @type {?} */
    ViewerExtensionDirective.prototype.template;
    /** @type {?} */
    ViewerExtensionDirective.prototype.urlFileContent;
    /** @type {?} */
    ViewerExtensionDirective.prototype.extension;
    /** @type {?} */
    ViewerExtensionDirective.prototype.supportedExtensions;
    /** @type {?} */
    ViewerExtensionDirective.prototype.templateModel;
    /**
     * @type {?}
     * @private
     */
    ViewerExtensionDirective.prototype.onDestroy$;
    /**
     * @type {?}
     * @private
     */
    ViewerExtensionDirective.prototype.viewerComponent;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlld2VyLWV4dGVuc2lvbi5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJ2aWV3ZXIvZGlyZWN0aXZlcy92aWV3ZXItZXh0ZW5zaW9uLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQ0EsT0FBTyxFQUFvQixZQUFZLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxXQUFXLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFDekcsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ2pFLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBSzNDLE1BQU0sT0FBTyx3QkFBd0I7Ozs7SUFrQmpDLFlBQW9CLGVBQWdDO1FBQWhDLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUY1QyxlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztJQUc1QyxDQUFDOzs7O0lBRUQsa0JBQWtCO1FBQ2QsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsQ0FBQztRQUVuRSxJQUFJLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFFakUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlO2FBQy9CLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2hDLFNBQVM7Ozs7UUFBQyxhQUFhLENBQUMsRUFBRTtZQUN2QixJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQ2pFLENBQUMsRUFBQyxDQUFDO1FBRVAsSUFBSSxJQUFJLENBQUMsbUJBQW1CLFlBQVksS0FBSyxFQUFFO1lBQzNDLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPOzs7O1lBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBRTtnQkFDM0MsSUFBSSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDNUQsQ0FBQyxFQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7Ozs7SUFFRCxXQUFXO1FBQ1AsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDOzs7Ozs7SUFLRCxTQUFTLENBQUMsYUFBYTs7WUFDZixrQkFBMEI7UUFFOUIsSUFBSSxJQUFJLENBQUMsbUJBQW1CLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLFlBQVksS0FBSyxDQUFDLEVBQUU7WUFDekUsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUk7Ozs7WUFBQyxDQUFDLFNBQVMsRUFBRSxFQUFFO2dCQUM3RCxPQUFPLFNBQVMsQ0FBQyxXQUFXLEVBQUUsS0FBSyxhQUFhLENBQUM7WUFFckQsQ0FBQyxFQUFDLENBQUM7U0FDTjtRQUVELE9BQU8sQ0FBQyxDQUFDLGtCQUFrQixDQUFDO0lBQ2hDLENBQUM7OztZQTdESixTQUFTLFNBQUM7Z0JBQ1AsUUFBUSxFQUFFLHNCQUFzQjthQUNuQzs7OztZQU5RLGVBQWU7Ozt1QkFTbkIsWUFBWSxTQUFDLFdBQVcsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7NkJBR3hDLEtBQUs7d0JBR0wsS0FBSztrQ0FHTCxLQUFLOzs7O0lBVE4sNENBQ2M7O0lBRWQsa0RBQ3VCOztJQUV2Qiw2Q0FDa0I7O0lBRWxCLHVEQUM4Qjs7SUFFOUIsaURBQW1COzs7OztJQUVuQiw4Q0FBNEM7Ozs7O0lBRWhDLG1EQUF3QyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxuXG4vKiFcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxuICpcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cbiAqL1xuXG5pbXBvcnQgeyBBZnRlckNvbnRlbnRJbml0LCBDb250ZW50Q2hpbGQsIERpcmVjdGl2ZSwgSW5wdXQsIFRlbXBsYXRlUmVmLCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFZpZXdlckNvbXBvbmVudCB9IGZyb20gJy4uL2NvbXBvbmVudHMvdmlld2VyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbkBEaXJlY3RpdmUoe1xuICAgIHNlbGVjdG9yOiAnYWRmLXZpZXdlci1leHRlbnNpb24nXG59KVxuZXhwb3J0IGNsYXNzIFZpZXdlckV4dGVuc2lvbkRpcmVjdGl2ZSBpbXBsZW1lbnRzIEFmdGVyQ29udGVudEluaXQsIE9uRGVzdHJveSB7XG5cbiAgICBAQ29udGVudENoaWxkKFRlbXBsYXRlUmVmLCB7c3RhdGljOiB0cnVlfSlcbiAgICB0ZW1wbGF0ZTogYW55O1xuXG4gICAgQElucHV0KClcbiAgICB1cmxGaWxlQ29udGVudDogc3RyaW5nO1xuXG4gICAgQElucHV0KClcbiAgICBleHRlbnNpb246IHN0cmluZztcblxuICAgIEBJbnB1dCgpXG4gICAgc3VwcG9ydGVkRXh0ZW5zaW9uczogc3RyaW5nW107XG5cbiAgICB0ZW1wbGF0ZU1vZGVsOiBhbnk7XG5cbiAgICBwcml2YXRlIG9uRGVzdHJveSQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSB2aWV3ZXJDb21wb25lbnQ6IFZpZXdlckNvbXBvbmVudCkge1xuICAgIH1cblxuICAgIG5nQWZ0ZXJDb250ZW50SW5pdCgpIHtcbiAgICAgICAgdGhpcy50ZW1wbGF0ZU1vZGVsID0geyB0ZW1wbGF0ZTogdGhpcy50ZW1wbGF0ZSwgaXNWaXNpYmxlOiBmYWxzZSB9O1xuXG4gICAgICAgIHRoaXMudmlld2VyQ29tcG9uZW50LmV4dGVuc2lvblRlbXBsYXRlcy5wdXNoKHRoaXMudGVtcGxhdGVNb2RlbCk7XG5cbiAgICAgICAgdGhpcy52aWV3ZXJDb21wb25lbnQuZXh0ZW5zaW9uQ2hhbmdlXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5vbkRlc3Ryb3kkKSlcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoZmlsZUV4dGVuc2lvbiA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy50ZW1wbGF0ZU1vZGVsLmlzVmlzaWJsZSA9IHRoaXMuaXNWaXNpYmxlKGZpbGVFeHRlbnNpb24pO1xuICAgICAgICAgICAgfSk7XG5cbiAgICAgICAgaWYgKHRoaXMuc3VwcG9ydGVkRXh0ZW5zaW9ucyBpbnN0YW5jZW9mIEFycmF5KSB7XG4gICAgICAgICAgICB0aGlzLnN1cHBvcnRlZEV4dGVuc2lvbnMuZm9yRWFjaCgoZXh0ZW5zaW9uKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy52aWV3ZXJDb21wb25lbnQuZXh0ZXJuYWxFeHRlbnNpb25zLnB1c2goZXh0ZW5zaW9uKTtcbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgbmdPbkRlc3Ryb3koKSB7XG4gICAgICAgIHRoaXMub25EZXN0cm95JC5uZXh0KHRydWUpO1xuICAgICAgICB0aGlzLm9uRGVzdHJveSQuY29tcGxldGUoKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBjaGVjayBpZiB0aGUgY3VycmVudCBleHRlbnNpb24gaW4gdGhlIHZpZXdlciBpcyBjb21wYXRpYmxlIHdpdGggdGhpcyBleHRlbnNpb24gY2hlY2tpbmcgYWdhaW5zdCBzdXBwb3J0ZWRFeHRlbnNpb25zXG4gICAgICovXG4gICAgaXNWaXNpYmxlKGZpbGVFeHRlbnNpb24pOiBib29sZWFuIHtcbiAgICAgICAgbGV0IHN1cHBvcnRlZEV4dGVuc2lvbjogc3RyaW5nO1xuXG4gICAgICAgIGlmICh0aGlzLnN1cHBvcnRlZEV4dGVuc2lvbnMgJiYgKHRoaXMuc3VwcG9ydGVkRXh0ZW5zaW9ucyBpbnN0YW5jZW9mIEFycmF5KSkge1xuICAgICAgICAgICAgc3VwcG9ydGVkRXh0ZW5zaW9uID0gdGhpcy5zdXBwb3J0ZWRFeHRlbnNpb25zLmZpbmQoKGV4dGVuc2lvbikgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiBleHRlbnNpb24udG9Mb3dlckNhc2UoKSA9PT0gZmlsZUV4dGVuc2lvbjtcblxuICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gISFzdXBwb3J0ZWRFeHRlbnNpb247XG4gICAgfVxuXG59XG4iXX0=