/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/* tslint:disable:adf-license-banner  */
/* Copyright 2012 Mozilla Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
/**
 *
 * RenderingQueueServices rendering of the views for pages and thumbnails.
 *
 */
export class RenderingQueueServices {
    constructor() {
        this.renderingStates = {
            INITIAL: 0,
            RUNNING: 1,
            PAUSED: 2,
            FINISHED: 3
        };
        this.CLEANUP_TIMEOUT = 30000;
        this.pdfViewer = null;
        this.pdfThumbnailViewer = null;
        this.onIdle = null;
        this.highestPriorityPage = null;
        this.idleTimeout = null;
        this.printing = false;
        this.isThumbnailViewEnabled = false;
    }
    /**
     * @param {?} pdfViewer
     * @return {?}
     */
    setViewer(pdfViewer) {
        this.pdfViewer = pdfViewer;
    }
    /**
     * @param {?} pdfThumbnailViewer
     * @return {?}
     */
    setThumbnailViewer(pdfThumbnailViewer) {
        this.pdfThumbnailViewer = pdfThumbnailViewer;
    }
    /**
     * @param {?} view
     * @return {?}
     */
    isHighestPriority(view) {
        return this.highestPriorityPage === view.renderingId;
    }
    /**
     * @param {?} currentlyVisiblePages
     * @return {?}
     */
    renderHighestPriority(currentlyVisiblePages) {
        if (this.idleTimeout) {
            clearTimeout(this.idleTimeout);
            this.idleTimeout = null;
        }
        // Pages have a higher priority than thumbnails, so check them first.
        if (this.pdfViewer.forceRendering(currentlyVisiblePages)) {
            return;
        }
        // No pages needed rendering so check thumbnails.
        if (this.pdfThumbnailViewer && this.isThumbnailViewEnabled) {
            if (this.pdfThumbnailViewer.forceRendering()) {
                return;
            }
        }
        if (this.printing) {
            // If printing is currently ongoing do not reschedule cleanup.
            return;
        }
        if (this.onIdle) {
            this.idleTimeout = setTimeout(this.onIdle.bind(this), this.CLEANUP_TIMEOUT);
        }
    }
    /**
     * @param {?} visible
     * @param {?} views
     * @param {?} scrolledDown
     * @return {?}
     */
    getHighestPriority(visible, views, scrolledDown) {
        // The state has changed figure out which page has the highest priority to
        // render next (if any).
        // Priority:
        // 1 visible pages
        // 2 if last scrolled down page after the visible pages
        // 2 if last scrolled up page before the visible pages
        /** @type {?} */
        const visibleViews = visible.views;
        /** @type {?} */
        const numVisible = visibleViews.length;
        if (numVisible === 0) {
            return false;
        }
        for (let i = 0; i < numVisible; ++i) {
            /** @type {?} */
            const view = visibleViews[i].view;
            if (!this.isViewFinished(view)) {
                return view;
            }
        }
        // All the visible views have rendered, try to render next/previous pages.
        if (scrolledDown) {
            /** @type {?} */
            const nextPageIndex = visible.last.id;
            // ID's start at 1 so no need to add 1.
            if (views[nextPageIndex] && !this.isViewFinished(views[nextPageIndex])) {
                return views[nextPageIndex];
            }
        }
        else {
            /** @type {?} */
            const previousPageIndex = visible.first.id - 2;
            if (views[previousPageIndex] && !this.isViewFinished(views[previousPageIndex])) {
                return views[previousPageIndex];
            }
        }
        // Everything that needs to be rendered has been.
        return null;
    }
    /**
     * @param {?} view
     * @return {?}
     */
    isViewFinished(view) {
        return view.renderingState === this.renderingStates.FINISHED;
    }
    /**
     * Render a page or thumbnail view. This calls the appropriate function
     * based on the views state. If the view is already rendered it will return
     * false.
     * @param {?} view
     * @return {?}
     */
    renderView(view) {
        /** @type {?} */
        const state = view.renderingState;
        switch (state) {
            case this.renderingStates.FINISHED:
                return false;
            case this.renderingStates.PAUSED:
                this.highestPriorityPage = view.renderingId;
                view.resume();
                break;
            case this.renderingStates.RUNNING:
                this.highestPriorityPage = view.renderingId;
                break;
            case this.renderingStates.INITIAL:
                this.highestPriorityPage = view.renderingId;
                /** @type {?} */
                const continueRendering = (/**
                 * @return {?}
                 */
                function () {
                    this.renderHighestPriority();
                }).bind(this);
                view.draw().then(continueRendering, continueRendering);
                break;
            default:
                break;
        }
        return true;
    }
}
RenderingQueueServices.decorators = [
    { type: Injectable }
];
if (false) {
    /** @type {?} */
    RenderingQueueServices.prototype.renderingStates;
    /** @type {?} */
    RenderingQueueServices.prototype.CLEANUP_TIMEOUT;
    /** @type {?} */
    RenderingQueueServices.prototype.pdfViewer;
    /** @type {?} */
    RenderingQueueServices.prototype.pdfThumbnailViewer;
    /** @type {?} */
    RenderingQueueServices.prototype.onIdle;
    /** @type {?} */
    RenderingQueueServices.prototype.highestPriorityPage;
    /** @type {?} */
    RenderingQueueServices.prototype.idleTimeout;
    /** @type {?} */
    RenderingQueueServices.prototype.printing;
    /** @type {?} */
    RenderingQueueServices.prototype.isThumbnailViewEnabled;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVuZGVyaW5nLXF1ZXVlLnNlcnZpY2VzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsidmlld2VyL3NlcnZpY2VzL3JlbmRlcmluZy1xdWV1ZS5zZXJ2aWNlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7OztBQVEzQyxNQUFNLE9BQU8sc0JBQXNCO0lBRG5DO1FBR0ksb0JBQWUsR0FBRztZQUNkLE9BQU8sRUFBRSxDQUFDO1lBQ1YsT0FBTyxFQUFFLENBQUM7WUFDVixNQUFNLEVBQUUsQ0FBQztZQUNULFFBQVEsRUFBRSxDQUFDO1NBQ2QsQ0FBQztRQUVGLG9CQUFlLEdBQVcsS0FBSyxDQUFDO1FBRWhDLGNBQVMsR0FBUSxJQUFJLENBQUM7UUFDdEIsdUJBQWtCLEdBQVEsSUFBSSxDQUFDO1FBQy9CLFdBQU0sR0FBUSxJQUFJLENBQUM7UUFFbkIsd0JBQW1CLEdBQVEsSUFBSSxDQUFDO1FBQ2hDLGdCQUFXLEdBQVEsSUFBSSxDQUFDO1FBQ3hCLGFBQVEsR0FBUSxLQUFLLENBQUM7UUFDdEIsMkJBQXNCLEdBQVEsS0FBSyxDQUFDO0lBNEh4QyxDQUFDOzs7OztJQXZIRyxTQUFTLENBQUMsU0FBUztRQUNmLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO0lBQy9CLENBQUM7Ozs7O0lBS0Qsa0JBQWtCLENBQUMsa0JBQWtCO1FBQ2pDLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxrQkFBa0IsQ0FBQztJQUNqRCxDQUFDOzs7OztJQUtELGlCQUFpQixDQUFDLElBQVM7UUFDdkIsT0FBTyxJQUFJLENBQUMsbUJBQW1CLEtBQUssSUFBSSxDQUFDLFdBQVcsQ0FBQztJQUN6RCxDQUFDOzs7OztJQUVELHFCQUFxQixDQUFDLHFCQUFxQjtRQUN2QyxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDbEIsWUFBWSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUMvQixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztTQUMzQjtRQUVELHFFQUFxRTtRQUNyRSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLHFCQUFxQixDQUFDLEVBQUU7WUFDdEQsT0FBTztTQUNWO1FBQ0QsaURBQWlEO1FBQ2pELElBQUksSUFBSSxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQyxzQkFBc0IsRUFBRTtZQUN4RCxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLEVBQUUsRUFBRTtnQkFDMUMsT0FBTzthQUNWO1NBQ0o7UUFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDZiw4REFBOEQ7WUFDOUQsT0FBTztTQUNWO1FBRUQsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2IsSUFBSSxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1NBQy9FO0lBQ0wsQ0FBQzs7Ozs7OztJQUVELGtCQUFrQixDQUFDLE9BQU8sRUFBRSxLQUFLLEVBQUUsWUFBWTs7Ozs7Ozs7Y0FPckMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxLQUFLOztjQUU1QixVQUFVLEdBQUcsWUFBWSxDQUFDLE1BQU07UUFDdEMsSUFBSSxVQUFVLEtBQUssQ0FBQyxFQUFFO1lBQ2xCLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBQ0QsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFVBQVUsRUFBRSxFQUFFLENBQUMsRUFBRTs7a0JBQzNCLElBQUksR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtZQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDNUIsT0FBTyxJQUFJLENBQUM7YUFDZjtTQUNKO1FBRUQsMEVBQTBFO1FBQzFFLElBQUksWUFBWSxFQUFFOztrQkFDUixhQUFhLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3JDLHVDQUF1QztZQUN2QyxJQUFJLEtBQUssQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUU7Z0JBQ3BFLE9BQU8sS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBQy9CO1NBQ0o7YUFBTTs7a0JBQ0csaUJBQWlCLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLEdBQUcsQ0FBQztZQUM5QyxJQUFJLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFO2dCQUM1RSxPQUFPLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2FBQ25DO1NBQ0o7UUFDRCxpREFBaUQ7UUFDakQsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQzs7Ozs7SUFLRCxjQUFjLENBQUMsSUFBSTtRQUNmLE9BQU8sSUFBSSxDQUFDLGNBQWMsS0FBSyxJQUFJLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQztJQUNqRSxDQUFDOzs7Ozs7OztJQVFELFVBQVUsQ0FBQyxJQUFTOztjQUNWLEtBQUssR0FBRyxJQUFJLENBQUMsY0FBYztRQUNqQyxRQUFRLEtBQUssRUFBRTtZQUNYLEtBQUssSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRO2dCQUM5QixPQUFPLEtBQUssQ0FBQztZQUNqQixLQUFLLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTTtnQkFDNUIsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7Z0JBQzVDLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztnQkFDZCxNQUFNO1lBQ1YsS0FBSyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU87Z0JBQzdCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO2dCQUM1QyxNQUFNO1lBQ1YsS0FBSyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU87Z0JBQzdCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDOztzQkFDdEMsaUJBQWlCLEdBQUc7OztnQkFBQTtvQkFDdEIsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7Z0JBQ2pDLENBQUMsRUFBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUNaLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsaUJBQWlCLENBQUMsQ0FBQztnQkFDdkQsTUFBTTtZQUNWO2dCQUNJLE1BQU07U0FDYjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7OztZQTlJSixVQUFVOzs7O0lBR1AsaURBS0U7O0lBRUYsaURBQWdDOztJQUVoQywyQ0FBc0I7O0lBQ3RCLG9EQUErQjs7SUFDL0Isd0NBQW1COztJQUVuQixxREFBZ0M7O0lBQ2hDLDZDQUF3Qjs7SUFDeEIsMENBQXNCOztJQUN0Qix3REFBb0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiB0c2xpbnQ6ZGlzYWJsZTphZGYtbGljZW5zZS1iYW5uZXIgICovXHJcblxyXG4vKiBDb3B5cmlnaHQgMjAxMiBNb3ppbGxhIEZvdW5kYXRpb25cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuLyoqXHJcbiAqXHJcbiAqIFJlbmRlcmluZ1F1ZXVlU2VydmljZXMgcmVuZGVyaW5nIG9mIHRoZSB2aWV3cyBmb3IgcGFnZXMgYW5kIHRodW1ibmFpbHMuXHJcbiAqXHJcbiAqL1xyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBSZW5kZXJpbmdRdWV1ZVNlcnZpY2VzIHtcclxuXHJcbiAgICByZW5kZXJpbmdTdGF0ZXMgPSB7XHJcbiAgICAgICAgSU5JVElBTDogMCxcclxuICAgICAgICBSVU5OSU5HOiAxLFxyXG4gICAgICAgIFBBVVNFRDogMixcclxuICAgICAgICBGSU5JU0hFRDogM1xyXG4gICAgfTtcclxuXHJcbiAgICBDTEVBTlVQX1RJTUVPVVQ6IG51bWJlciA9IDMwMDAwO1xyXG5cclxuICAgIHBkZlZpZXdlcjogYW55ID0gbnVsbDtcclxuICAgIHBkZlRodW1ibmFpbFZpZXdlcjogYW55ID0gbnVsbDtcclxuICAgIG9uSWRsZTogYW55ID0gbnVsbDtcclxuXHJcbiAgICBoaWdoZXN0UHJpb3JpdHlQYWdlOiBhbnkgPSBudWxsO1xyXG4gICAgaWRsZVRpbWVvdXQ6IGFueSA9IG51bGw7XHJcbiAgICBwcmludGluZzogYW55ID0gZmFsc2U7XHJcbiAgICBpc1RodW1ibmFpbFZpZXdFbmFibGVkOiBhbnkgPSBmYWxzZTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIEBwYXJhbSBwZGZWaWV3ZXJcclxuICAgICAqL1xyXG4gICAgc2V0Vmlld2VyKHBkZlZpZXdlcikge1xyXG4gICAgICAgIHRoaXMucGRmVmlld2VyID0gcGRmVmlld2VyO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQHBhcmFtIHBkZlRodW1ibmFpbFZpZXdlclxyXG4gICAgICovXHJcbiAgICBzZXRUaHVtYm5haWxWaWV3ZXIocGRmVGh1bWJuYWlsVmlld2VyKSB7XHJcbiAgICAgICAgdGhpcy5wZGZUaHVtYm5haWxWaWV3ZXIgPSBwZGZUaHVtYm5haWxWaWV3ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAcGFyYW0gIHZpZXdcclxuICAgICAqL1xyXG4gICAgaXNIaWdoZXN0UHJpb3JpdHkodmlldzogYW55KTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaGlnaGVzdFByaW9yaXR5UGFnZSA9PT0gdmlldy5yZW5kZXJpbmdJZDtcclxuICAgIH1cclxuXHJcbiAgICByZW5kZXJIaWdoZXN0UHJpb3JpdHkoY3VycmVudGx5VmlzaWJsZVBhZ2VzKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaWRsZVRpbWVvdXQpIHtcclxuICAgICAgICAgICAgY2xlYXJUaW1lb3V0KHRoaXMuaWRsZVRpbWVvdXQpO1xyXG4gICAgICAgICAgICB0aGlzLmlkbGVUaW1lb3V0ID0gbnVsbDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIC8vIFBhZ2VzIGhhdmUgYSBoaWdoZXIgcHJpb3JpdHkgdGhhbiB0aHVtYm5haWxzLCBzbyBjaGVjayB0aGVtIGZpcnN0LlxyXG4gICAgICAgIGlmICh0aGlzLnBkZlZpZXdlci5mb3JjZVJlbmRlcmluZyhjdXJyZW50bHlWaXNpYmxlUGFnZXMpKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gTm8gcGFnZXMgbmVlZGVkIHJlbmRlcmluZyBzbyBjaGVjayB0aHVtYm5haWxzLlxyXG4gICAgICAgIGlmICh0aGlzLnBkZlRodW1ibmFpbFZpZXdlciAmJiB0aGlzLmlzVGh1bWJuYWlsVmlld0VuYWJsZWQpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMucGRmVGh1bWJuYWlsVmlld2VyLmZvcmNlUmVuZGVyaW5nKCkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMucHJpbnRpbmcpIHtcclxuICAgICAgICAgICAgLy8gSWYgcHJpbnRpbmcgaXMgY3VycmVudGx5IG9uZ29pbmcgZG8gbm90IHJlc2NoZWR1bGUgY2xlYW51cC5cclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMub25JZGxlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaWRsZVRpbWVvdXQgPSBzZXRUaW1lb3V0KHRoaXMub25JZGxlLmJpbmQodGhpcyksIHRoaXMuQ0xFQU5VUF9USU1FT1VUKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0SGlnaGVzdFByaW9yaXR5KHZpc2libGUsIHZpZXdzLCBzY3JvbGxlZERvd24pIHtcclxuICAgICAgICAvLyBUaGUgc3RhdGUgaGFzIGNoYW5nZWQgZmlndXJlIG91dCB3aGljaCBwYWdlIGhhcyB0aGUgaGlnaGVzdCBwcmlvcml0eSB0b1xyXG4gICAgICAgIC8vIHJlbmRlciBuZXh0IChpZiBhbnkpLlxyXG4gICAgICAgIC8vIFByaW9yaXR5OlxyXG4gICAgICAgIC8vIDEgdmlzaWJsZSBwYWdlc1xyXG4gICAgICAgIC8vIDIgaWYgbGFzdCBzY3JvbGxlZCBkb3duIHBhZ2UgYWZ0ZXIgdGhlIHZpc2libGUgcGFnZXNcclxuICAgICAgICAvLyAyIGlmIGxhc3Qgc2Nyb2xsZWQgdXAgcGFnZSBiZWZvcmUgdGhlIHZpc2libGUgcGFnZXNcclxuICAgICAgICBjb25zdCB2aXNpYmxlVmlld3MgPSB2aXNpYmxlLnZpZXdzO1xyXG5cclxuICAgICAgICBjb25zdCBudW1WaXNpYmxlID0gdmlzaWJsZVZpZXdzLmxlbmd0aDtcclxuICAgICAgICBpZiAobnVtVmlzaWJsZSA9PT0gMCkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgbnVtVmlzaWJsZTsgKytpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHZpZXcgPSB2aXNpYmxlVmlld3NbaV0udmlldztcclxuICAgICAgICAgICAgaWYgKCF0aGlzLmlzVmlld0ZpbmlzaGVkKHZpZXcpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdmlldztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gQWxsIHRoZSB2aXNpYmxlIHZpZXdzIGhhdmUgcmVuZGVyZWQsIHRyeSB0byByZW5kZXIgbmV4dC9wcmV2aW91cyBwYWdlcy5cclxuICAgICAgICBpZiAoc2Nyb2xsZWREb3duKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG5leHRQYWdlSW5kZXggPSB2aXNpYmxlLmxhc3QuaWQ7XHJcbiAgICAgICAgICAgIC8vIElEJ3Mgc3RhcnQgYXQgMSBzbyBubyBuZWVkIHRvIGFkZCAxLlxyXG4gICAgICAgICAgICBpZiAodmlld3NbbmV4dFBhZ2VJbmRleF0gJiYgIXRoaXMuaXNWaWV3RmluaXNoZWQodmlld3NbbmV4dFBhZ2VJbmRleF0pKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdmlld3NbbmV4dFBhZ2VJbmRleF07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBwcmV2aW91c1BhZ2VJbmRleCA9IHZpc2libGUuZmlyc3QuaWQgLSAyO1xyXG4gICAgICAgICAgICBpZiAodmlld3NbcHJldmlvdXNQYWdlSW5kZXhdICYmICF0aGlzLmlzVmlld0ZpbmlzaGVkKHZpZXdzW3ByZXZpb3VzUGFnZUluZGV4XSkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB2aWV3c1twcmV2aW91c1BhZ2VJbmRleF07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgLy8gRXZlcnl0aGluZyB0aGF0IG5lZWRzIHRvIGJlIHJlbmRlcmVkIGhhcyBiZWVuLlxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQHBhcmFtIHZpZXdcclxuICAgICAqL1xyXG4gICAgaXNWaWV3RmluaXNoZWQodmlldyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB2aWV3LnJlbmRlcmluZ1N0YXRlID09PSB0aGlzLnJlbmRlcmluZ1N0YXRlcy5GSU5JU0hFRDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlbmRlciBhIHBhZ2Ugb3IgdGh1bWJuYWlsIHZpZXcuIFRoaXMgY2FsbHMgdGhlIGFwcHJvcHJpYXRlIGZ1bmN0aW9uXHJcbiAgICAgKiBiYXNlZCBvbiB0aGUgdmlld3Mgc3RhdGUuIElmIHRoZSB2aWV3IGlzIGFscmVhZHkgcmVuZGVyZWQgaXQgd2lsbCByZXR1cm5cclxuICAgICAqIGZhbHNlLlxyXG4gICAgICogQHBhcmFtIHZpZXdcclxuICAgICAqL1xyXG4gICAgcmVuZGVyVmlldyh2aWV3OiBhbnkpIHtcclxuICAgICAgICBjb25zdCBzdGF0ZSA9IHZpZXcucmVuZGVyaW5nU3RhdGU7XHJcbiAgICAgICAgc3dpdGNoIChzdGF0ZSkge1xyXG4gICAgICAgICAgICBjYXNlIHRoaXMucmVuZGVyaW5nU3RhdGVzLkZJTklTSEVEOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICBjYXNlIHRoaXMucmVuZGVyaW5nU3RhdGVzLlBBVVNFRDpcclxuICAgICAgICAgICAgICAgIHRoaXMuaGlnaGVzdFByaW9yaXR5UGFnZSA9IHZpZXcucmVuZGVyaW5nSWQ7XHJcbiAgICAgICAgICAgICAgICB2aWV3LnJlc3VtZSgpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgdGhpcy5yZW5kZXJpbmdTdGF0ZXMuUlVOTklORzpcclxuICAgICAgICAgICAgICAgIHRoaXMuaGlnaGVzdFByaW9yaXR5UGFnZSA9IHZpZXcucmVuZGVyaW5nSWQ7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSB0aGlzLnJlbmRlcmluZ1N0YXRlcy5JTklUSUFMOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5oaWdoZXN0UHJpb3JpdHlQYWdlID0gdmlldy5yZW5kZXJpbmdJZDtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGNvbnRpbnVlUmVuZGVyaW5nID0gZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmVuZGVySGlnaGVzdFByaW9yaXR5KCk7XHJcbiAgICAgICAgICAgICAgICB9LmJpbmQodGhpcyk7XHJcbiAgICAgICAgICAgICAgICB2aWV3LmRyYXcoKS50aGVuKGNvbnRpbnVlUmVuZGVyaW5nLCBjb250aW51ZVJlbmRlcmluZyk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxufVxyXG4iXX0=