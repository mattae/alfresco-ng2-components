/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import * as i0 from "@angular/core";
import * as i1 from "../../services/alfresco-api.service";
import * as i2 from "../../services/log.service";
export class ViewUtilService {
    /**
     * @param {?} apiService
     * @param {?} logService
     */
    constructor(apiService, logService) {
        this.apiService = apiService;
        this.logService = logService;
        /**
         * Based on ViewerComponent Implementation, this value is used to determine how many times we try
         * to get the rendition of a file for preview, or printing.
         */
        this.maxRetries = 5;
        /**
         * Mime-type grouping based on the ViewerComponent.
         */
        this.mimeTypes = {
            text: ['text/plain', 'text/csv', 'text/xml', 'text/html', 'application/x-javascript'],
            pdf: ['application/pdf'],
            image: ['image/png', 'image/jpeg', 'image/gif', 'image/bmp', 'image/svg+xml'],
            media: ['video/mp4', 'video/webm', 'video/ogg', 'audio/mpeg', 'audio/ogg', 'audio/wav']
        };
    }
    /**
     * This method takes a url to trigger the print dialog against, and the type of artifact that it
     * is.
     * This URL should be one that can be rendered in the browser, for example PDF, Image, or Text
     * @param {?} url
     * @param {?} type
     * @return {?}
     */
    printFile(url, type) {
        /** @type {?} */
        const pwa = window.open(url, ViewUtilService.TARGET);
        if (pwa) {
            // Because of the way chrome focus and close image window vs. pdf preview window
            if (type === ViewUtilService.ContentGroup.IMAGE) {
                pwa.onfocus = (/**
                 * @return {?}
                 */
                () => {
                    setTimeout((/**
                     * @return {?}
                     */
                    () => {
                        pwa.close();
                    }), 500);
                });
            }
            pwa.onload = (/**
             * @return {?}
             */
            () => {
                pwa.print();
            });
        }
    }
    /**
     * Launch the File Print dialog from anywhere other than the preview service, which resolves the
     * rendition of the object that can be printed from a web browser.
     * These are: images, PDF files, or PDF rendition of files.
     * We also force PDF rendition for TEXT type objects, otherwise the default URL is to download.
     * TODO there are different TEXT type objects, (HTML, plaintext, xml, etc. we should determine how these are handled)
     * @param {?} objectId
     * @param {?} mimeType
     * @return {?}
     */
    printFileGeneric(objectId, mimeType) {
        /** @type {?} */
        const nodeId = objectId;
        /** @type {?} */
        const type = this.getViewerTypeByMimeType(mimeType);
        this.getRendition(nodeId, ViewUtilService.ContentGroup.PDF)
            .then((/**
         * @param {?} value
         * @return {?}
         */
        (value) => {
            /** @type {?} */
            const url = this.getRenditionUrl(nodeId, type, (value ? true : false));
            /** @type {?} */
            const printType = (type === ViewUtilService.ContentGroup.PDF
                || type === ViewUtilService.ContentGroup.TEXT)
                ? ViewUtilService.ContentGroup.PDF : type;
            this.printFile(url, printType);
        }))
            .catch((/**
         * @param {?} err
         * @return {?}
         */
        (err) => {
            this.logService.error('Error with Printing');
            this.logService.error(err);
        }));
    }
    /**
     * @param {?} nodeId
     * @param {?} type
     * @param {?} renditionExists
     * @return {?}
     */
    getRenditionUrl(nodeId, type, renditionExists) {
        return (renditionExists && type !== ViewUtilService.ContentGroup.IMAGE) ?
            this.apiService.contentApi.getRenditionUrl(nodeId, ViewUtilService.ContentGroup.PDF) :
            this.apiService.contentApi.getContentUrl(nodeId, false);
    }
    /**
     * @private
     * @param {?} nodeId
     * @param {?} renditionId
     * @param {?} retries
     * @return {?}
     */
    waitRendition(nodeId, renditionId, retries) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const rendition = yield this.apiService.renditionsApi.getRendition(nodeId, renditionId);
            if (this.maxRetries < retries) {
                /** @type {?} */
                const status = rendition.entry.status.toString();
                if (status === 'CREATED') {
                    return rendition;
                }
                else {
                    retries += 1;
                    yield this.wait(1000);
                    return yield this.waitRendition(nodeId, renditionId, retries);
                }
            }
        });
    }
    /**
     * @param {?} mimeType
     * @return {?}
     */
    getViewerTypeByMimeType(mimeType) {
        if (mimeType) {
            mimeType = mimeType.toLowerCase();
            /** @type {?} */
            const editorTypes = Object.keys(this.mimeTypes);
            for (const type of editorTypes) {
                if (this.mimeTypes[type].indexOf(mimeType) >= 0) {
                    return type;
                }
            }
        }
        return 'unknown';
    }
    /**
     * @param {?} ms
     * @return {?}
     */
    wait(ms) {
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        (resolve) => setTimeout(resolve, ms)));
    }
    /**
     * @param {?} nodeId
     * @param {?} renditionId
     * @return {?}
     */
    getRendition(nodeId, renditionId) {
        return tslib_1.__awaiter(this, void 0, void 0, function* () {
            /** @type {?} */
            const renditionPaging = yield this.apiService.renditionsApi.getRenditions(nodeId);
            /** @type {?} */
            let rendition = renditionPaging.list.entries.find((/**
             * @param {?} renditionEntry
             * @return {?}
             */
            (renditionEntry) => renditionEntry.entry.id.toLowerCase() === renditionId));
            if (rendition) {
                /** @type {?} */
                const status = rendition.entry.status.toString();
                if (status === 'NOT_CREATED') {
                    try {
                        yield this.apiService.renditionsApi.createRendition(nodeId, { id: renditionId });
                        rendition = yield this.waitRendition(nodeId, renditionId, 0);
                    }
                    catch (err) {
                        this.logService.error(err);
                    }
                }
            }
            return new Promise((/**
             * @param {?} resolve
             * @return {?}
             */
            (resolve) => resolve(rendition)));
        });
    }
}
ViewUtilService.TARGET = '_new';
/**
 * Content groups based on categorization of files that can be viewed in the web browser. This
 * implementation or grouping is tied to the definition the ng component: ViewerComponent
 */
// tslint:disable-next-line:variable-name
ViewUtilService.ContentGroup = {
    IMAGE: 'image',
    MEDIA: 'media',
    PDF: 'pdf',
    TEXT: 'text'
};
ViewUtilService.decorators = [
    { type: Injectable, args: [{
                providedIn: 'root'
            },] }
];
/** @nocollapse */
ViewUtilService.ctorParameters = () => [
    { type: AlfrescoApiService },
    { type: LogService }
];
/** @nocollapse */ ViewUtilService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ViewUtilService_Factory() { return new ViewUtilService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: ViewUtilService, providedIn: "root" });
if (false) {
    /** @type {?} */
    ViewUtilService.TARGET;
    /**
     * Content groups based on categorization of files that can be viewed in the web browser. This
     * implementation or grouping is tied to the definition the ng component: ViewerComponent
     * @type {?}
     */
    ViewUtilService.ContentGroup;
    /**
     * Based on ViewerComponent Implementation, this value is used to determine how many times we try
     * to get the rendition of a file for preview, or printing.
     * @type {?}
     */
    ViewUtilService.prototype.maxRetries;
    /**
     * Mime-type grouping based on the ViewerComponent.
     * @type {?}
     * @private
     */
    ViewUtilService.prototype.mimeTypes;
    /**
     * @type {?}
     * @private
     */
    ViewUtilService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    ViewUtilService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy11dGlsLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJ2aWV3ZXIvc2VydmljZXMvdmlldy11dGlsLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDekUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLDRCQUE0QixDQUFDOzs7O0FBS3hELE1BQU0sT0FBTyxlQUFlOzs7OztJQStCeEIsWUFBb0IsVUFBOEIsRUFDOUIsVUFBc0I7UUFEdEIsZUFBVSxHQUFWLFVBQVUsQ0FBb0I7UUFDOUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTs7Ozs7UUFiMUMsZUFBVSxHQUFHLENBQUMsQ0FBQzs7OztRQUtQLGNBQVMsR0FBRztZQUNoQixJQUFJLEVBQUUsQ0FBQyxZQUFZLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQUUsMEJBQTBCLENBQUM7WUFDckYsR0FBRyxFQUFFLENBQUMsaUJBQWlCLENBQUM7WUFDeEIsS0FBSyxFQUFFLENBQUMsV0FBVyxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUUsV0FBVyxFQUFFLGVBQWUsQ0FBQztZQUM3RSxLQUFLLEVBQUUsQ0FBQyxXQUFXLEVBQUUsWUFBWSxFQUFFLFdBQVcsRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLFdBQVcsQ0FBQztTQUMxRixDQUFDO0lBSUYsQ0FBQzs7Ozs7Ozs7O0lBT0QsU0FBUyxDQUFDLEdBQVcsRUFBRSxJQUFZOztjQUN6QixHQUFHLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsZUFBZSxDQUFDLE1BQU0sQ0FBQztRQUNwRCxJQUFJLEdBQUcsRUFBRTtZQUNMLGdGQUFnRjtZQUNoRixJQUFJLElBQUksS0FBSyxlQUFlLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRTtnQkFDN0MsR0FBRyxDQUFDLE9BQU87OztnQkFBRyxHQUFHLEVBQUU7b0JBQ2YsVUFBVTs7O29CQUFDLEdBQUcsRUFBRTt3QkFDWixHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7b0JBQ2hCLENBQUMsR0FBRSxHQUFHLENBQUMsQ0FBQztnQkFDWixDQUFDLENBQUEsQ0FBQzthQUNMO1lBRUQsR0FBRyxDQUFDLE1BQU07OztZQUFHLEdBQUcsRUFBRTtnQkFDZCxHQUFHLENBQUMsS0FBSyxFQUFFLENBQUM7WUFDaEIsQ0FBQyxDQUFBLENBQUM7U0FDTDtJQUNMLENBQUM7Ozs7Ozs7Ozs7O0lBU0QsZ0JBQWdCLENBQUMsUUFBZ0IsRUFBRSxRQUFnQjs7Y0FDekMsTUFBTSxHQUFHLFFBQVE7O2NBQ2pCLElBQUksR0FBVyxJQUFJLENBQUMsdUJBQXVCLENBQUMsUUFBUSxDQUFDO1FBRTNELElBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLGVBQWUsQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDO2FBQ3RELElBQUk7Ozs7UUFBQyxDQUFDLEtBQUssRUFBRSxFQUFFOztrQkFDTixHQUFHLEdBQVcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDOztrQkFDeEUsU0FBUyxHQUFHLENBQUMsSUFBSSxLQUFLLGVBQWUsQ0FBQyxZQUFZLENBQUMsR0FBRzttQkFDckQsSUFBSSxLQUFLLGVBQWUsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDO2dCQUM5QyxDQUFDLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUk7WUFDN0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLEVBQUUsU0FBUyxDQUFDLENBQUM7UUFDbkMsQ0FBQyxFQUFDO2FBQ0QsS0FBSzs7OztRQUFDLENBQUMsR0FBRyxFQUFFLEVBQUU7WUFDWCxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQzdDLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1gsQ0FBQzs7Ozs7OztJQUVELGVBQWUsQ0FBQyxNQUFjLEVBQUUsSUFBWSxFQUFFLGVBQXdCO1FBQ2xFLE9BQU8sQ0FBQyxlQUFlLElBQUksSUFBSSxLQUFLLGVBQWUsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNyRSxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLGVBQWUsQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN0RixJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ2hFLENBQUM7Ozs7Ozs7O0lBRWEsYUFBYSxDQUFDLE1BQWMsRUFBRSxXQUFtQixFQUFFLE9BQWU7OztrQkFDdEUsU0FBUyxHQUFHLE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxXQUFXLENBQUM7WUFFdkYsSUFBSSxJQUFJLENBQUMsVUFBVSxHQUFHLE9BQU8sRUFBRTs7c0JBQ3JCLE1BQU0sR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7Z0JBRWhELElBQUksTUFBTSxLQUFLLFNBQVMsRUFBRTtvQkFDdEIsT0FBTyxTQUFTLENBQUM7aUJBQ3BCO3FCQUFNO29CQUNILE9BQU8sSUFBSSxDQUFDLENBQUM7b0JBQ2IsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUN0QixPQUFPLE1BQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsV0FBVyxFQUFFLE9BQU8sQ0FBQyxDQUFDO2lCQUNqRTthQUNKO1FBQ0wsQ0FBQztLQUFBOzs7OztJQUVELHVCQUF1QixDQUFDLFFBQWdCO1FBQ3BDLElBQUksUUFBUSxFQUFFO1lBQ1YsUUFBUSxHQUFHLFFBQVEsQ0FBQyxXQUFXLEVBQUUsQ0FBQzs7a0JBRTVCLFdBQVcsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7WUFDL0MsS0FBSyxNQUFNLElBQUksSUFBSSxXQUFXLEVBQUU7Z0JBQzVCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFO29CQUM3QyxPQUFPLElBQUksQ0FBQztpQkFDZjthQUNKO1NBQ0o7UUFDRCxPQUFPLFNBQVMsQ0FBQztJQUNyQixDQUFDOzs7OztJQUVELElBQUksQ0FBQyxFQUFVO1FBQ1gsT0FBTyxJQUFJLE9BQU87Ozs7UUFBQyxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsRUFBQyxDQUFDO0lBQzdELENBQUM7Ozs7OztJQUVLLFlBQVksQ0FBQyxNQUFjLEVBQUUsV0FBbUI7OztrQkFDNUMsZUFBZSxHQUFvQixNQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUM7O2dCQUM5RixTQUFTLEdBQW1CLGVBQWUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUk7Ozs7WUFBQyxDQUFDLGNBQThCLEVBQUUsRUFBRSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLFdBQVcsRUFBRSxLQUFLLFdBQVcsRUFBQztZQUU1SixJQUFJLFNBQVMsRUFBRTs7c0JBQ0wsTUFBTSxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTtnQkFFaEQsSUFBSSxNQUFNLEtBQUssYUFBYSxFQUFFO29CQUMxQixJQUFJO3dCQUNBLE1BQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRSxFQUFFLEVBQUUsRUFBRSxXQUFXLEVBQUUsQ0FBQyxDQUFDO3dCQUNqRixTQUFTLEdBQUcsTUFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUM7cUJBQ2hFO29CQUFDLE9BQU8sR0FBRyxFQUFFO3dCQUNWLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO3FCQUM5QjtpQkFDSjthQUNKO1lBQ0QsT0FBTyxJQUFJLE9BQU87Ozs7WUFBaUIsQ0FBQyxPQUFPLEVBQUUsRUFBRSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBQyxDQUFDO1FBQ3hFLENBQUM7S0FBQTs7QUEzSU0sc0JBQU0sR0FBRyxNQUFNLENBQUM7Ozs7OztBQU9oQiw0QkFBWSxHQUFHO0lBQ2xCLEtBQUssRUFBRSxPQUFPO0lBQ2QsS0FBSyxFQUFFLE9BQU87SUFDZCxHQUFHLEVBQUUsS0FBSztJQUNWLElBQUksRUFBRSxNQUFNO0NBQ2YsQ0FBQzs7WUFoQkwsVUFBVSxTQUFDO2dCQUNSLFVBQVUsRUFBRSxNQUFNO2FBQ3JCOzs7O1lBTFEsa0JBQWtCO1lBQ2xCLFVBQVU7Ozs7O0lBTWYsdUJBQXVCOzs7Ozs7SUFPdkIsNkJBS0U7Ozs7OztJQU1GLHFDQUFlOzs7Ozs7SUFLZixvQ0FLRTs7Ozs7SUFFVSxxQ0FBc0M7Ozs7O0lBQ3RDLHFDQUE4QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJlbmRpdGlvbkVudHJ5LCBSZW5kaXRpb25QYWdpbmcgfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMb2dTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbG9nLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBWaWV3VXRpbFNlcnZpY2Uge1xyXG4gICAgc3RhdGljIFRBUkdFVCA9ICdfbmV3JztcclxuXHJcbiAgICAvKipcclxuICAgICAqIENvbnRlbnQgZ3JvdXBzIGJhc2VkIG9uIGNhdGVnb3JpemF0aW9uIG9mIGZpbGVzIHRoYXQgY2FuIGJlIHZpZXdlZCBpbiB0aGUgd2ViIGJyb3dzZXIuIFRoaXNcclxuICAgICAqIGltcGxlbWVudGF0aW9uIG9yIGdyb3VwaW5nIGlzIHRpZWQgdG8gdGhlIGRlZmluaXRpb24gdGhlIG5nIGNvbXBvbmVudDogVmlld2VyQ29tcG9uZW50XHJcbiAgICAgKi9cclxuICAgICAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6dmFyaWFibGUtbmFtZVxyXG4gICAgc3RhdGljIENvbnRlbnRHcm91cCA9IHtcclxuICAgICAgICBJTUFHRTogJ2ltYWdlJyxcclxuICAgICAgICBNRURJQTogJ21lZGlhJyxcclxuICAgICAgICBQREY6ICdwZGYnLFxyXG4gICAgICAgIFRFWFQ6ICd0ZXh0J1xyXG4gICAgfTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIEJhc2VkIG9uIFZpZXdlckNvbXBvbmVudCBJbXBsZW1lbnRhdGlvbiwgdGhpcyB2YWx1ZSBpcyB1c2VkIHRvIGRldGVybWluZSBob3cgbWFueSB0aW1lcyB3ZSB0cnlcclxuICAgICAqIHRvIGdldCB0aGUgcmVuZGl0aW9uIG9mIGEgZmlsZSBmb3IgcHJldmlldywgb3IgcHJpbnRpbmcuXHJcbiAgICAgKi9cclxuICAgIG1heFJldHJpZXMgPSA1O1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogTWltZS10eXBlIGdyb3VwaW5nIGJhc2VkIG9uIHRoZSBWaWV3ZXJDb21wb25lbnQuXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgbWltZVR5cGVzID0ge1xyXG4gICAgICAgIHRleHQ6IFsndGV4dC9wbGFpbicsICd0ZXh0L2NzdicsICd0ZXh0L3htbCcsICd0ZXh0L2h0bWwnLCAnYXBwbGljYXRpb24veC1qYXZhc2NyaXB0J10sXHJcbiAgICAgICAgcGRmOiBbJ2FwcGxpY2F0aW9uL3BkZiddLFxyXG4gICAgICAgIGltYWdlOiBbJ2ltYWdlL3BuZycsICdpbWFnZS9qcGVnJywgJ2ltYWdlL2dpZicsICdpbWFnZS9ibXAnLCAnaW1hZ2Uvc3ZnK3htbCddLFxyXG4gICAgICAgIG1lZGlhOiBbJ3ZpZGVvL21wNCcsICd2aWRlby93ZWJtJywgJ3ZpZGVvL29nZycsICdhdWRpby9tcGVnJywgJ2F1ZGlvL29nZycsICdhdWRpby93YXYnXVxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgbG9nU2VydmljZTogTG9nU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGhpcyBtZXRob2QgdGFrZXMgYSB1cmwgdG8gdHJpZ2dlciB0aGUgcHJpbnQgZGlhbG9nIGFnYWluc3QsIGFuZCB0aGUgdHlwZSBvZiBhcnRpZmFjdCB0aGF0IGl0XHJcbiAgICAgKiBpcy5cclxuICAgICAqIFRoaXMgVVJMIHNob3VsZCBiZSBvbmUgdGhhdCBjYW4gYmUgcmVuZGVyZWQgaW4gdGhlIGJyb3dzZXIsIGZvciBleGFtcGxlIFBERiwgSW1hZ2UsIG9yIFRleHRcclxuICAgICAqL1xyXG4gICAgcHJpbnRGaWxlKHVybDogc3RyaW5nLCB0eXBlOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgICAgICBjb25zdCBwd2EgPSB3aW5kb3cub3Blbih1cmwsIFZpZXdVdGlsU2VydmljZS5UQVJHRVQpO1xyXG4gICAgICAgIGlmIChwd2EpIHtcclxuICAgICAgICAgICAgLy8gQmVjYXVzZSBvZiB0aGUgd2F5IGNocm9tZSBmb2N1cyBhbmQgY2xvc2UgaW1hZ2Ugd2luZG93IHZzLiBwZGYgcHJldmlldyB3aW5kb3dcclxuICAgICAgICAgICAgaWYgKHR5cGUgPT09IFZpZXdVdGlsU2VydmljZS5Db250ZW50R3JvdXAuSU1BR0UpIHtcclxuICAgICAgICAgICAgICAgIHB3YS5vbmZvY3VzID0gKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBwd2EuY2xvc2UoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCA1MDApO1xyXG4gICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcHdhLm9ubG9hZCA9ICgpID0+IHtcclxuICAgICAgICAgICAgICAgIHB3YS5wcmludCgpO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExhdW5jaCB0aGUgRmlsZSBQcmludCBkaWFsb2cgZnJvbSBhbnl3aGVyZSBvdGhlciB0aGFuIHRoZSBwcmV2aWV3IHNlcnZpY2UsIHdoaWNoIHJlc29sdmVzIHRoZVxyXG4gICAgICogcmVuZGl0aW9uIG9mIHRoZSBvYmplY3QgdGhhdCBjYW4gYmUgcHJpbnRlZCBmcm9tIGEgd2ViIGJyb3dzZXIuXHJcbiAgICAgKiBUaGVzZSBhcmU6IGltYWdlcywgUERGIGZpbGVzLCBvciBQREYgcmVuZGl0aW9uIG9mIGZpbGVzLlxyXG4gICAgICogV2UgYWxzbyBmb3JjZSBQREYgcmVuZGl0aW9uIGZvciBURVhUIHR5cGUgb2JqZWN0cywgb3RoZXJ3aXNlIHRoZSBkZWZhdWx0IFVSTCBpcyB0byBkb3dubG9hZC5cclxuICAgICAqIFRPRE8gdGhlcmUgYXJlIGRpZmZlcmVudCBURVhUIHR5cGUgb2JqZWN0cywgKEhUTUwsIHBsYWludGV4dCwgeG1sLCBldGMuIHdlIHNob3VsZCBkZXRlcm1pbmUgaG93IHRoZXNlIGFyZSBoYW5kbGVkKVxyXG4gICAgICovXHJcbiAgICBwcmludEZpbGVHZW5lcmljKG9iamVjdElkOiBzdHJpbmcsIG1pbWVUeXBlOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgICAgICBjb25zdCBub2RlSWQgPSBvYmplY3RJZDtcclxuICAgICAgICBjb25zdCB0eXBlOiBzdHJpbmcgPSB0aGlzLmdldFZpZXdlclR5cGVCeU1pbWVUeXBlKG1pbWVUeXBlKTtcclxuXHJcbiAgICAgICAgdGhpcy5nZXRSZW5kaXRpb24obm9kZUlkLCBWaWV3VXRpbFNlcnZpY2UuQ29udGVudEdyb3VwLlBERilcclxuICAgICAgICAgICAgLnRoZW4oKHZhbHVlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCB1cmw6IHN0cmluZyA9IHRoaXMuZ2V0UmVuZGl0aW9uVXJsKG5vZGVJZCwgdHlwZSwgKHZhbHVlID8gdHJ1ZSA6IGZhbHNlKSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBwcmludFR5cGUgPSAodHlwZSA9PT0gVmlld1V0aWxTZXJ2aWNlLkNvbnRlbnRHcm91cC5QREZcclxuICAgICAgICAgICAgICAgICAgICB8fCB0eXBlID09PSBWaWV3VXRpbFNlcnZpY2UuQ29udGVudEdyb3VwLlRFWFQpXHJcbiAgICAgICAgICAgICAgICAgICAgPyBWaWV3VXRpbFNlcnZpY2UuQ29udGVudEdyb3VwLlBERiA6IHR5cGU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnByaW50RmlsZSh1cmwsIHByaW50VHlwZSk7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5jYXRjaCgoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZXJyb3IoJ0Vycm9yIHdpdGggUHJpbnRpbmcnKTtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnIpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRSZW5kaXRpb25Vcmwobm9kZUlkOiBzdHJpbmcsIHR5cGU6IHN0cmluZywgcmVuZGl0aW9uRXhpc3RzOiBib29sZWFuKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gKHJlbmRpdGlvbkV4aXN0cyAmJiB0eXBlICE9PSBWaWV3VXRpbFNlcnZpY2UuQ29udGVudEdyb3VwLklNQUdFKSA/XHJcbiAgICAgICAgICAgIHRoaXMuYXBpU2VydmljZS5jb250ZW50QXBpLmdldFJlbmRpdGlvblVybChub2RlSWQsIFZpZXdVdGlsU2VydmljZS5Db250ZW50R3JvdXAuUERGKSA6XHJcbiAgICAgICAgICAgIHRoaXMuYXBpU2VydmljZS5jb250ZW50QXBpLmdldENvbnRlbnRVcmwobm9kZUlkLCBmYWxzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBhc3luYyB3YWl0UmVuZGl0aW9uKG5vZGVJZDogc3RyaW5nLCByZW5kaXRpb25JZDogc3RyaW5nLCByZXRyaWVzOiBudW1iZXIpOiBQcm9taXNlPFJlbmRpdGlvbkVudHJ5PiB7XHJcbiAgICAgICAgY29uc3QgcmVuZGl0aW9uID0gYXdhaXQgdGhpcy5hcGlTZXJ2aWNlLnJlbmRpdGlvbnNBcGkuZ2V0UmVuZGl0aW9uKG5vZGVJZCwgcmVuZGl0aW9uSWQpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5tYXhSZXRyaWVzIDwgcmV0cmllcykge1xyXG4gICAgICAgICAgICBjb25zdCBzdGF0dXMgPSByZW5kaXRpb24uZW50cnkuc3RhdHVzLnRvU3RyaW5nKCk7XHJcblxyXG4gICAgICAgICAgICBpZiAoc3RhdHVzID09PSAnQ1JFQVRFRCcpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiByZW5kaXRpb247XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXRyaWVzICs9IDE7XHJcbiAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLndhaXQoMTAwMCk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gYXdhaXQgdGhpcy53YWl0UmVuZGl0aW9uKG5vZGVJZCwgcmVuZGl0aW9uSWQsIHJldHJpZXMpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldFZpZXdlclR5cGVCeU1pbWVUeXBlKG1pbWVUeXBlOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIGlmIChtaW1lVHlwZSkge1xyXG4gICAgICAgICAgICBtaW1lVHlwZSA9IG1pbWVUeXBlLnRvTG93ZXJDYXNlKCk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBlZGl0b3JUeXBlcyA9IE9iamVjdC5rZXlzKHRoaXMubWltZVR5cGVzKTtcclxuICAgICAgICAgICAgZm9yIChjb25zdCB0eXBlIG9mIGVkaXRvclR5cGVzKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy5taW1lVHlwZXNbdHlwZV0uaW5kZXhPZihtaW1lVHlwZSkgPj0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0eXBlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiAndW5rbm93bic7XHJcbiAgICB9XHJcblxyXG4gICAgd2FpdChtczogbnVtYmVyKTogUHJvbWlzZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHNldFRpbWVvdXQocmVzb2x2ZSwgbXMpKTtcclxuICAgIH1cclxuXHJcbiAgICBhc3luYyBnZXRSZW5kaXRpb24obm9kZUlkOiBzdHJpbmcsIHJlbmRpdGlvbklkOiBzdHJpbmcpOiBQcm9taXNlPFJlbmRpdGlvbkVudHJ5PiB7XHJcbiAgICAgICAgY29uc3QgcmVuZGl0aW9uUGFnaW5nOiBSZW5kaXRpb25QYWdpbmcgPSBhd2FpdCB0aGlzLmFwaVNlcnZpY2UucmVuZGl0aW9uc0FwaS5nZXRSZW5kaXRpb25zKG5vZGVJZCk7XHJcbiAgICAgICAgbGV0IHJlbmRpdGlvbjogUmVuZGl0aW9uRW50cnkgPSByZW5kaXRpb25QYWdpbmcubGlzdC5lbnRyaWVzLmZpbmQoKHJlbmRpdGlvbkVudHJ5OiBSZW5kaXRpb25FbnRyeSkgPT4gcmVuZGl0aW9uRW50cnkuZW50cnkuaWQudG9Mb3dlckNhc2UoKSA9PT0gcmVuZGl0aW9uSWQpO1xyXG5cclxuICAgICAgICBpZiAocmVuZGl0aW9uKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHN0YXR1cyA9IHJlbmRpdGlvbi5lbnRyeS5zdGF0dXMudG9TdHJpbmcoKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChzdGF0dXMgPT09ICdOT1RfQ1JFQVRFRCcpIHtcclxuICAgICAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5hcGlTZXJ2aWNlLnJlbmRpdGlvbnNBcGkuY3JlYXRlUmVuZGl0aW9uKG5vZGVJZCwgeyBpZDogcmVuZGl0aW9uSWQgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVuZGl0aW9uID0gYXdhaXQgdGhpcy53YWl0UmVuZGl0aW9uKG5vZGVJZCwgcmVuZGl0aW9uSWQsIDApO1xyXG4gICAgICAgICAgICAgICAgfSBjYXRjaCAoZXJyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKGVycik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlPFJlbmRpdGlvbkVudHJ5PigocmVzb2x2ZSkgPT4gcmVzb2x2ZShyZW5kaXRpb24pKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19