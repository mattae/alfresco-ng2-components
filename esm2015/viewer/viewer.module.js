/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ExtensionsModule } from '@alfresco/adf-extensions';
import { MaterialModule } from '../material.module';
import { ToolbarModule } from '../toolbar/toolbar.module';
import { PipeModule } from '../pipes/pipe.module';
import { ImgViewerComponent } from './components/imgViewer.component';
import { MediaPlayerComponent } from './components/mediaPlayer.component';
import { PdfViewerComponent } from './components/pdfViewer.component';
import { PdfPasswordDialogComponent } from './components/pdfViewer-password-dialog';
import { PdfThumbComponent } from './components/pdfViewer-thumb.component';
import { PdfThumbListComponent } from './components/pdfViewer-thumbnails.component';
import { TxtViewerComponent } from './components/txtViewer.component';
import { UnknownFormatComponent } from './components/unknown-format/unknown-format.component';
import { ViewerMoreActionsComponent } from './components/viewer-more-actions.component';
import { ViewerOpenWithComponent } from './components/viewer-open-with.component';
import { ViewerSidebarComponent } from './components/viewer-sidebar.component';
import { ViewerToolbarComponent } from './components/viewer-toolbar.component';
import { ViewerComponent } from './components/viewer.component';
import { ViewerExtensionDirective } from './directives/viewer-extension.directive';
import { ViewerToolbarActionsComponent } from './components/viewer-toolbar-actions.component';
import { DirectiveModule } from '../directives/directive.module';
export class ViewerModule {
}
ViewerModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    ExtensionsModule,
                    CommonModule,
                    MaterialModule,
                    TranslateModule.forChild(),
                    FormsModule,
                    ReactiveFormsModule,
                    ToolbarModule,
                    PipeModule,
                    FlexLayoutModule,
                    DirectiveModule
                ],
                declarations: [
                    PdfPasswordDialogComponent,
                    ViewerComponent,
                    ImgViewerComponent,
                    TxtViewerComponent,
                    MediaPlayerComponent,
                    PdfViewerComponent,
                    PdfThumbComponent,
                    PdfThumbListComponent,
                    ViewerExtensionDirective,
                    UnknownFormatComponent,
                    ViewerToolbarComponent,
                    ViewerSidebarComponent,
                    ViewerOpenWithComponent,
                    ViewerMoreActionsComponent,
                    ViewerToolbarActionsComponent
                ],
                entryComponents: [
                    PdfPasswordDialogComponent
                ],
                exports: [
                    ViewerComponent,
                    ImgViewerComponent,
                    TxtViewerComponent,
                    MediaPlayerComponent,
                    PdfViewerComponent,
                    PdfPasswordDialogComponent,
                    PdfThumbComponent,
                    PdfThumbListComponent,
                    ViewerExtensionDirective,
                    UnknownFormatComponent,
                    ViewerToolbarComponent,
                    ViewerSidebarComponent,
                    ViewerOpenWithComponent,
                    ViewerMoreActionsComponent,
                    ViewerToolbarActionsComponent
                ]
            },] }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlld2VyLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInZpZXdlci92aWV3ZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN4RCxPQUFPLEVBQUUsV0FBVyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFbEUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFNUQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDdEUsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDMUUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDdEUsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDcEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDM0UsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDcEYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDdEUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDOUYsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sNENBQTRDLENBQUM7QUFDeEYsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDbEYsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDL0UsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDL0UsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQ2hFLE9BQU8sRUFBRSx3QkFBd0IsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ25GLE9BQU8sRUFBRSw2QkFBNkIsRUFBRSxNQUFNLCtDQUErQyxDQUFDO0FBQzlGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQXFEakUsTUFBTSxPQUFPLFlBQVk7OztZQW5EeEIsUUFBUSxTQUFDO2dCQUNOLE9BQU8sRUFBRTtvQkFDTCxnQkFBZ0I7b0JBQ2hCLFlBQVk7b0JBQ1osY0FBYztvQkFDZCxlQUFlLENBQUMsUUFBUSxFQUFFO29CQUMxQixXQUFXO29CQUNYLG1CQUFtQjtvQkFDbkIsYUFBYTtvQkFDYixVQUFVO29CQUNWLGdCQUFnQjtvQkFDaEIsZUFBZTtpQkFDbEI7Z0JBQ0QsWUFBWSxFQUFFO29CQUNWLDBCQUEwQjtvQkFDMUIsZUFBZTtvQkFDZixrQkFBa0I7b0JBQ2xCLGtCQUFrQjtvQkFDbEIsb0JBQW9CO29CQUNwQixrQkFBa0I7b0JBQ2xCLGlCQUFpQjtvQkFDakIscUJBQXFCO29CQUNyQix3QkFBd0I7b0JBQ3hCLHNCQUFzQjtvQkFDdEIsc0JBQXNCO29CQUN0QixzQkFBc0I7b0JBQ3RCLHVCQUF1QjtvQkFDdkIsMEJBQTBCO29CQUMxQiw2QkFBNkI7aUJBQ2hDO2dCQUNELGVBQWUsRUFBRTtvQkFDYiwwQkFBMEI7aUJBQzdCO2dCQUNELE9BQU8sRUFBRTtvQkFDTCxlQUFlO29CQUNmLGtCQUFrQjtvQkFDbEIsa0JBQWtCO29CQUNsQixvQkFBb0I7b0JBQ3BCLGtCQUFrQjtvQkFDbEIsMEJBQTBCO29CQUMxQixpQkFBaUI7b0JBQ2pCLHFCQUFxQjtvQkFDckIsd0JBQXdCO29CQUN4QixzQkFBc0I7b0JBQ3RCLHNCQUFzQjtvQkFDdEIsc0JBQXNCO29CQUN0Qix1QkFBdUI7b0JBQ3ZCLDBCQUEwQjtvQkFDMUIsNkJBQTZCO2lCQUNoQzthQUNKIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZU1vZHVsZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5pbXBvcnQgeyBGbGV4TGF5b3V0TW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZmxleC1sYXlvdXQnO1xyXG5pbXBvcnQgeyBGb3Jtc01vZHVsZSwgUmVhY3RpdmVGb3Jtc01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbmltcG9ydCB7IEV4dGVuc2lvbnNNb2R1bGUgfSBmcm9tICdAYWxmcmVzY28vYWRmLWV4dGVuc2lvbnMnO1xyXG5cclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuLi9tYXRlcmlhbC5tb2R1bGUnO1xyXG5pbXBvcnQgeyBUb29sYmFyTW9kdWxlIH0gZnJvbSAnLi4vdG9vbGJhci90b29sYmFyLm1vZHVsZSc7XHJcbmltcG9ydCB7IFBpcGVNb2R1bGUgfSBmcm9tICcuLi9waXBlcy9waXBlLm1vZHVsZSc7XHJcbmltcG9ydCB7IEltZ1ZpZXdlckNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9pbWdWaWV3ZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTWVkaWFQbGF5ZXJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvbWVkaWFQbGF5ZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgUGRmVmlld2VyQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3BkZlZpZXdlci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBQZGZQYXNzd29yZERpYWxvZ0NvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9wZGZWaWV3ZXItcGFzc3dvcmQtZGlhbG9nJztcclxuaW1wb3J0IHsgUGRmVGh1bWJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvcGRmVmlld2VyLXRodW1iLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFBkZlRodW1iTGlzdENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9wZGZWaWV3ZXItdGh1bWJuYWlscy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBUeHRWaWV3ZXJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvdHh0Vmlld2VyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFVua25vd25Gb3JtYXRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvdW5rbm93bi1mb3JtYXQvdW5rbm93bi1mb3JtYXQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVmlld2VyTW9yZUFjdGlvbnNDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvdmlld2VyLW1vcmUtYWN0aW9ucy5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBWaWV3ZXJPcGVuV2l0aENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy92aWV3ZXItb3Blbi13aXRoLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFZpZXdlclNpZGViYXJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvdmlld2VyLXNpZGViYXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVmlld2VyVG9vbGJhckNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy92aWV3ZXItdG9vbGJhci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBWaWV3ZXJDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvdmlld2VyLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFZpZXdlckV4dGVuc2lvbkRpcmVjdGl2ZSB9IGZyb20gJy4vZGlyZWN0aXZlcy92aWV3ZXItZXh0ZW5zaW9uLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IFZpZXdlclRvb2xiYXJBY3Rpb25zQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3ZpZXdlci10b29sYmFyLWFjdGlvbnMuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRGlyZWN0aXZlTW9kdWxlIH0gZnJvbSAnLi4vZGlyZWN0aXZlcy9kaXJlY3RpdmUubW9kdWxlJztcclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgRXh0ZW5zaW9uc01vZHVsZSxcclxuICAgICAgICBDb21tb25Nb2R1bGUsXHJcbiAgICAgICAgTWF0ZXJpYWxNb2R1bGUsXHJcbiAgICAgICAgVHJhbnNsYXRlTW9kdWxlLmZvckNoaWxkKCksXHJcbiAgICAgICAgRm9ybXNNb2R1bGUsXHJcbiAgICAgICAgUmVhY3RpdmVGb3Jtc01vZHVsZSxcclxuICAgICAgICBUb29sYmFyTW9kdWxlLFxyXG4gICAgICAgIFBpcGVNb2R1bGUsXHJcbiAgICAgICAgRmxleExheW91dE1vZHVsZSxcclxuICAgICAgICBEaXJlY3RpdmVNb2R1bGVcclxuICAgIF0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtcclxuICAgICAgICBQZGZQYXNzd29yZERpYWxvZ0NvbXBvbmVudCxcclxuICAgICAgICBWaWV3ZXJDb21wb25lbnQsXHJcbiAgICAgICAgSW1nVmlld2VyQ29tcG9uZW50LFxyXG4gICAgICAgIFR4dFZpZXdlckNvbXBvbmVudCxcclxuICAgICAgICBNZWRpYVBsYXllckNvbXBvbmVudCxcclxuICAgICAgICBQZGZWaWV3ZXJDb21wb25lbnQsXHJcbiAgICAgICAgUGRmVGh1bWJDb21wb25lbnQsXHJcbiAgICAgICAgUGRmVGh1bWJMaXN0Q29tcG9uZW50LFxyXG4gICAgICAgIFZpZXdlckV4dGVuc2lvbkRpcmVjdGl2ZSxcclxuICAgICAgICBVbmtub3duRm9ybWF0Q29tcG9uZW50LFxyXG4gICAgICAgIFZpZXdlclRvb2xiYXJDb21wb25lbnQsXHJcbiAgICAgICAgVmlld2VyU2lkZWJhckNvbXBvbmVudCxcclxuICAgICAgICBWaWV3ZXJPcGVuV2l0aENvbXBvbmVudCxcclxuICAgICAgICBWaWV3ZXJNb3JlQWN0aW9uc0NvbXBvbmVudCxcclxuICAgICAgICBWaWV3ZXJUb29sYmFyQWN0aW9uc0NvbXBvbmVudFxyXG4gICAgXSxcclxuICAgIGVudHJ5Q29tcG9uZW50czogW1xyXG4gICAgICAgIFBkZlBhc3N3b3JkRGlhbG9nQ29tcG9uZW50XHJcbiAgICBdLFxyXG4gICAgZXhwb3J0czogW1xyXG4gICAgICAgIFZpZXdlckNvbXBvbmVudCxcclxuICAgICAgICBJbWdWaWV3ZXJDb21wb25lbnQsXHJcbiAgICAgICAgVHh0Vmlld2VyQ29tcG9uZW50LFxyXG4gICAgICAgIE1lZGlhUGxheWVyQ29tcG9uZW50LFxyXG4gICAgICAgIFBkZlZpZXdlckNvbXBvbmVudCxcclxuICAgICAgICBQZGZQYXNzd29yZERpYWxvZ0NvbXBvbmVudCxcclxuICAgICAgICBQZGZUaHVtYkNvbXBvbmVudCxcclxuICAgICAgICBQZGZUaHVtYkxpc3RDb21wb25lbnQsXHJcbiAgICAgICAgVmlld2VyRXh0ZW5zaW9uRGlyZWN0aXZlLFxyXG4gICAgICAgIFVua25vd25Gb3JtYXRDb21wb25lbnQsXHJcbiAgICAgICAgVmlld2VyVG9vbGJhckNvbXBvbmVudCxcclxuICAgICAgICBWaWV3ZXJTaWRlYmFyQ29tcG9uZW50LFxyXG4gICAgICAgIFZpZXdlck9wZW5XaXRoQ29tcG9uZW50LFxyXG4gICAgICAgIFZpZXdlck1vcmVBY3Rpb25zQ29tcG9uZW50LFxyXG4gICAgICAgIFZpZXdlclRvb2xiYXJBY3Rpb25zQ29tcG9uZW50XHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBWaWV3ZXJNb2R1bGUge1xyXG59XHJcbiJdfQ==