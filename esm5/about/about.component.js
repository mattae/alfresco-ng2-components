/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ViewEncapsulation, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from '../services/authentication.service';
import { DiscoveryApiService } from '../services/discovery-api.service';
import { ObjectDataTableAdapter } from '../datatable/data/object-datatable-adapter';
import { AppConfigService, AppConfigValues } from '../app-config/app-config.service';
import { AppExtensionService } from '@alfresco/adf-extensions';
var AboutComponent = /** @class */ (function () {
    function AboutComponent(http, appConfig, authService, discovery, appExtensions) {
        this.http = http;
        this.appConfig = appConfig;
        this.authService = authService;
        this.discovery = discovery;
        this.extensionColumns = ['$id', '$name', '$version', '$vendor', '$license', '$runtime', '$description'];
        /**
         * Commit corresponding to the version of ADF to be used.
         */
        this.githubUrlCommitAlpha = 'https://github.com/Alfresco/alfresco-ng2-components/commits/';
        /**
         * Toggles showing/hiding of extensions block.
         */
        this.showExtensions = true;
        /**
         * Regular expression for filtering dependencies packages.
         */
        this.regexp = '^(@alfresco)';
        this.ecmHost = '';
        this.bpmHost = '';
        this.ecmVersion = null;
        this.bpmVersion = null;
        this.extensions$ = appExtensions.references$;
    }
    /**
     * @return {?}
     */
    AboutComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.authService.isEcmLoggedIn()) {
            this.discovery.getEcmProductInfo().subscribe((/**
             * @param {?} ecmVers
             * @return {?}
             */
            function (ecmVers) {
                _this.ecmVersion = ecmVers;
                _this.modules = new ObjectDataTableAdapter(_this.ecmVersion.modules, [
                    { type: 'text', key: 'id', title: 'ABOUT.TABLE_HEADERS.MODULES.ID', sortable: true },
                    { type: 'text', key: 'title', title: 'ABOUT.TABLE_HEADERS.MODULES.TITLE', sortable: true },
                    { type: 'text', key: 'version', title: 'ABOUT.TABLE_HEADERS.MODULES.DESCRIPTION', sortable: true },
                    {
                        type: 'text',
                        key: 'installDate',
                        title: 'ABOUT.TABLE_HEADERS.MODULES.INSTALL_DATE',
                        sortable: true
                    },
                    {
                        type: 'text',
                        key: 'installState',
                        title: 'ABOUT.TABLE_HEADERS.MODULES.INSTALL_STATE',
                        sortable: true
                    },
                    {
                        type: 'text',
                        key: 'versionMin',
                        title: 'ABOUT.TABLE_HEADERS.MODULES.VERSION_MIN',
                        sortable: true
                    },
                    {
                        type: 'text',
                        key: 'versionMax',
                        title: 'ABOUT.TABLE_HEADERS.MODULES.VERSION_MAX',
                        sortable: true
                    }
                ]);
                _this.status = new ObjectDataTableAdapter([_this.ecmVersion.status], [
                    { type: 'text', key: 'isReadOnly', title: 'ABOUT.TABLE_HEADERS.STATUS.READ_ONLY', sortable: true },
                    {
                        type: 'text',
                        key: 'isAuditEnabled',
                        title: 'ABOUT.TABLE_HEADERS.STATUS.AUDIT_ENABLED',
                        sortable: true
                    },
                    {
                        type: 'text',
                        key: 'isQuickShareEnabled',
                        title: 'ABOUT.TABLE_HEADERS.STATUS.QUICK_SHARE_ENABLED',
                        sortable: true
                    },
                    {
                        type: 'text',
                        key: 'isThumbnailGenerationEnabled',
                        title: 'ABOUT.TABLE_HEADERS.STATUS.THUMBNAIL_ENABLED',
                        sortable: true
                    }
                ]);
                _this.license = new ObjectDataTableAdapter([_this.ecmVersion.license], [
                    { type: 'text', key: 'issuedAt', title: 'ABOUT.TABLE_HEADERS.LICENSE.ISSUES_AT', sortable: true },
                    { type: 'text', key: 'expiresAt', title: 'ABOUT.TABLE_HEADERS.LICENSE.EXPIRES_AT', sortable: true },
                    {
                        type: 'text',
                        key: 'remainingDays',
                        title: 'ABOUT.TABLE_HEADERS.LICENSE.REMAINING_DAYS',
                        sortable: true
                    },
                    { type: 'text', key: 'holder', title: 'ABOUT.TABLE_HEADERS.LICENSE.HOLDER', sortable: true },
                    { type: 'text', key: 'mode', title: 'ABOUT.TABLE_HEADERS.LICENSE.MODE', sortable: true },
                    {
                        type: 'text',
                        key: 'isClusterEnabled',
                        title: 'ABOUT.TABLE_HEADERS.LICENSE.CLUSTER_ENABLED',
                        sortable: true
                    },
                    {
                        type: 'text',
                        key: 'isCryptodocEnabled',
                        title: 'ABOUT.TABLE_HEADERS.LICENSE.CRYPTODOC_ENABLED',
                        sortable: true
                    }
                ]);
            }));
        }
        if (this.authService.isBpmLoggedIn()) {
            this.discovery.getBpmProductInfo().subscribe((/**
             * @param {?} bpmVers
             * @return {?}
             */
            function (bpmVers) {
                _this.bpmVersion = bpmVers;
            }));
        }
        this.http.get('./versions.json?' + new Date()).subscribe((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            /** @type {?} */
            var alfrescoPackages = Object.keys(response.dependencies).filter((/**
             * @param {?} val
             * @return {?}
             */
            function (val) {
                return new RegExp(_this.regexp).test(val);
            }));
            /** @type {?} */
            var alfrescoPackagesTableRepresentation = [];
            alfrescoPackages.forEach((/**
             * @param {?} val
             * @return {?}
             */
            function (val) {
                alfrescoPackagesTableRepresentation.push({
                    name: val,
                    version: (response.dependencies[val].version || response.dependencies[val].required.version)
                });
            }));
            _this.gitHubLinkCreation(alfrescoPackagesTableRepresentation);
            _this.data = new ObjectDataTableAdapter(alfrescoPackagesTableRepresentation, [
                { type: 'text', key: 'name', title: 'Name', sortable: true },
                { type: 'text', key: 'version', title: 'Version', sortable: true }
            ]);
        }));
        this.ecmHost = this.appConfig.get(AppConfigValues.ECMHOST);
        this.bpmHost = this.appConfig.get(AppConfigValues.BPMHOST);
    };
    /**
     * @private
     * @param {?} alfrescoPackagesTableRepresentation
     * @return {?}
     */
    AboutComponent.prototype.gitHubLinkCreation = /**
     * @private
     * @param {?} alfrescoPackagesTableRepresentation
     * @return {?}
     */
    function (alfrescoPackagesTableRepresentation) {
        /** @type {?} */
        var corePackage = alfrescoPackagesTableRepresentation.find((/**
         * @param {?} packageUp
         * @return {?}
         */
        function (packageUp) {
            return packageUp.name === '@alfresco/adf-core';
        }));
        if (corePackage) {
            /** @type {?} */
            var commitIsh = corePackage.version.split('-');
            if (commitIsh.length > 1) {
                this.githubUrlCommitAlpha = this.githubUrlCommitAlpha + commitIsh[1];
            }
            else {
                this.githubUrlCommitAlpha = this.githubUrlCommitAlpha + corePackage.version;
            }
        }
    };
    AboutComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-about',
                    template: "<div class=\"adf-about-container\">\r\n    <div class=\"adf-extension-details-container\" *ngIf=\"showExtensions\">\r\n        <h3>{{ 'ABOUT.TITLE' | translate }}</h3>\r\n        <mat-table [dataSource]=\"extensions$ | async\">\r\n            <!-- $id Column -->\r\n            <ng-container matColumnDef=\"$id\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'ABOUT.TABLE_HEADERS.ID' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.$id}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <!-- $name Column -->\r\n            <ng-container matColumnDef=\"$name\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'ABOUT.TABLE_HEADERS.NAME' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.$name}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <!-- $version Column -->\r\n            <ng-container matColumnDef=\"$version\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'ABOUT.TABLE_HEADERS.VERSION' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.$version}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <!-- $vendor Column -->\r\n            <ng-container matColumnDef=\"$vendor\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'ABOUT.TABLE_HEADERS.VENDOR' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.$vendor}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <!-- $license Column -->\r\n            <ng-container matColumnDef=\"$license\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'ABOUT.TABLE_HEADERS.LICENSE' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.$license}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <!-- $runtime Column -->\r\n            <ng-container matColumnDef=\"$runtime\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'ABOUT.TABLE_HEADERS.RUNTIME' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.$runtime}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <!-- $description Column -->\r\n            <ng-container matColumnDef=\"$description\">\r\n                <mat-header-cell *matHeaderCellDef>{{ 'ABOUT.TABLE_HEADERS.DESCRIPTION' | translate }}</mat-header-cell>\r\n                <mat-cell *matCellDef=\"let element\">{{element.$description}}</mat-cell>\r\n            </ng-container>\r\n\r\n            <mat-header-row *matHeaderRowDef=\"extensionColumns\"></mat-header-row>\r\n            <mat-row *matRowDef=\"let row; columns: extensionColumns;\"></mat-row>\r\n        </mat-table>\r\n    </div>\r\n    <h3>{{ 'ABOUT.SERVER_SETTINGS.TITLE' | translate }}</h3>\r\n    <mat-list>\r\n        <small>{{ 'ABOUT.SERVER_SETTINGS.DESCRIPTION' | translate }}</small>\r\n        <mat-list-item>\r\n            <h4 matLine> {{ 'ABOUT.SERVER_SETTINGS.PROCESS_SERVICE_HOST' | translate: { value: bpmHost } }}</h4>\r\n        </mat-list-item>\r\n        <mat-divider></mat-divider>\r\n        <mat-list-item>\r\n            <h4 matLine>{{ 'ABOUT.SERVER_SETTINGS.CONTENT_SERVICE_HOST' | translate: { value: ecmHost } }}</h4>\r\n        </mat-list-item>\r\n    </mat-list>\r\n\r\n    <h3>{{ 'ABOUT.VERSIONS.TITLE' | translate }}</h3>\r\n    <div *ngIf=\"bpmVersion\">\r\n        <h3>{{ 'ABOUT.VERSIONS.PROCESS_SERVICE' | translate }}</h3>\r\n        <div> {{ 'ABOUT.VERSIONS.divS.EDITION' | translate }} </div> {{ bpmVersion.edition }}\r\n        <p></p>\r\n        <div> {{ 'ABOUT.VERSIONS.divS.VERSION' | translate }} </div> {{ bpmVersion.majorVersion }}.{{\r\n        bpmVersion.minorVersion }}.{{ bpmVersion.revisionVersion }}\r\n    </div>\r\n    <div *ngIf=\"ecmVersion\">\r\n        <h3>{{ 'ABOUT.VERSIONS.CONTENT_SERVICE' | translate }}</h3>\r\n        <div>{{ 'ABOUT.VERSIONS.divS.EDITION' | translate }}</div> {{ ecmVersion.edition }}\r\n        <p></p>\r\n        <div> {{ 'ABOUT.VERSIONS.divS.VERSION' | translate }} </div> {{ ecmVersion.version.display }}\r\n        <p></p>\r\n        <h4>{{ 'ABOUT.VERSIONS.divS.LICENSE' | translate }}</h4>\r\n        <adf-datatable [data]=\"license\"></adf-datatable>\r\n\r\n        <h4> {{ 'ABOUT.VERSIONS.divS.STATUS' | translate }}</h4>\r\n        <adf-datatable [data]=\"status\"></adf-datatable>\r\n\r\n        <h4>{{ 'ABOUT.VERSIONS.divS.MODULES' | translate }}</h4>\r\n\r\n        <adf-datatable [data]=\"modules\"></adf-datatable>\r\n    </div>\r\n\r\n    <div *ngIf=\"githubUrlCommitAlpha\">\r\n        <h3>{{ 'ABOUT.SOURCE_CODE.TITLE' | translate }}</h3>\r\n        <small>{{ 'ABOUT.SOURCE_CODE.DESCRIPTION' | translate }}</small>\r\n        <div>\r\n            <a [href]=\"githubUrlCommitAlpha\">{{githubUrlCommitAlpha}}</a>\r\n        </div>\r\n    </div>\r\n\r\n    <h3>{{ 'ABOUT.PACKAGES.TITLE' | translate }}</h3>\r\n    <small>{{ 'ABOUT.PACKAGES.DESCRIPTION' | translate }}</small>\r\n    <adf-datatable [data]=\"data\"></adf-datatable>\r\n</div>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    styles: [".adf-about-container{padding:10px}.adf-table-version{width:60%;border:0;border-spacing:0;text-align:center}"]
                }] }
    ];
    /** @nocollapse */
    AboutComponent.ctorParameters = function () { return [
        { type: HttpClient },
        { type: AppConfigService },
        { type: AuthenticationService },
        { type: DiscoveryApiService },
        { type: AppExtensionService }
    ]; };
    AboutComponent.propDecorators = {
        githubUrlCommitAlpha: [{ type: Input }],
        showExtensions: [{ type: Input }],
        regexp: [{ type: Input }]
    };
    return AboutComponent;
}());
export { AboutComponent };
if (false) {
    /** @type {?} */
    AboutComponent.prototype.data;
    /** @type {?} */
    AboutComponent.prototype.status;
    /** @type {?} */
    AboutComponent.prototype.license;
    /** @type {?} */
    AboutComponent.prototype.modules;
    /** @type {?} */
    AboutComponent.prototype.extensionColumns;
    /** @type {?} */
    AboutComponent.prototype.extensions$;
    /**
     * Commit corresponding to the version of ADF to be used.
     * @type {?}
     */
    AboutComponent.prototype.githubUrlCommitAlpha;
    /**
     * Toggles showing/hiding of extensions block.
     * @type {?}
     */
    AboutComponent.prototype.showExtensions;
    /**
     * Regular expression for filtering dependencies packages.
     * @type {?}
     */
    AboutComponent.prototype.regexp;
    /** @type {?} */
    AboutComponent.prototype.ecmHost;
    /** @type {?} */
    AboutComponent.prototype.bpmHost;
    /** @type {?} */
    AboutComponent.prototype.ecmVersion;
    /** @type {?} */
    AboutComponent.prototype.bpmVersion;
    /**
     * @type {?}
     * @private
     */
    AboutComponent.prototype.http;
    /**
     * @type {?}
     * @private
     */
    AboutComponent.prototype.appConfig;
    /**
     * @type {?}
     * @private
     */
    AboutComponent.prototype.authService;
    /**
     * @type {?}
     * @private
     */
    AboutComponent.prototype.discovery;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWJvdXQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiYWJvdXQvYWJvdXQuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQVUsaUJBQWlCLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzVFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNsRCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUUzRSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUN4RSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0Q0FBNEMsQ0FBQztBQUNwRixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsZUFBZSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFFckYsT0FBTyxFQUFnQixtQkFBbUIsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBRTdFO0lBZ0NJLHdCQUFvQixJQUFnQixFQUNoQixTQUEyQixFQUMzQixXQUFrQyxFQUNsQyxTQUE4QixFQUN0QyxhQUFrQztRQUoxQixTQUFJLEdBQUosSUFBSSxDQUFZO1FBQ2hCLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBQzNCLGdCQUFXLEdBQVgsV0FBVyxDQUF1QjtRQUNsQyxjQUFTLEdBQVQsU0FBUyxDQUFxQjtRQXZCbEQscUJBQWdCLEdBQWEsQ0FBQyxLQUFLLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxjQUFjLENBQUMsQ0FBQzs7OztRQUs3Ryx5QkFBb0IsR0FBRyw4REFBOEQsQ0FBQzs7OztRQUl0RixtQkFBYyxHQUFHLElBQUksQ0FBQzs7OztRQUdiLFdBQU0sR0FBRyxjQUFjLENBQUM7UUFFakMsWUFBTyxHQUFHLEVBQUUsQ0FBQztRQUNiLFlBQU8sR0FBRyxFQUFFLENBQUM7UUFFYixlQUFVLEdBQTJCLElBQUksQ0FBQztRQUMxQyxlQUFVLEdBQTJCLElBQUksQ0FBQztRQU90QyxJQUFJLENBQUMsV0FBVyxHQUFHLGFBQWEsQ0FBQyxXQUFXLENBQUM7SUFDakQsQ0FBQzs7OztJQUVELGlDQUFROzs7SUFBUjtRQUFBLGlCQW9IQztRQWxIRyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLEVBQUU7WUFDbEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLFNBQVM7Ozs7WUFBQyxVQUFDLE9BQU87Z0JBQ2pELEtBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDO2dCQUUxQixLQUFJLENBQUMsT0FBTyxHQUFHLElBQUksc0JBQXNCLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUU7b0JBQy9ELEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxnQ0FBZ0MsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFO29CQUNwRixFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsbUNBQW1DLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRTtvQkFDMUYsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLHlDQUF5QyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUU7b0JBQ2xHO3dCQUNJLElBQUksRUFBRSxNQUFNO3dCQUNaLEdBQUcsRUFBRSxhQUFhO3dCQUNsQixLQUFLLEVBQUUsMENBQTBDO3dCQUNqRCxRQUFRLEVBQUUsSUFBSTtxQkFDakI7b0JBQ0Q7d0JBQ0ksSUFBSSxFQUFFLE1BQU07d0JBQ1osR0FBRyxFQUFFLGNBQWM7d0JBQ25CLEtBQUssRUFBRSwyQ0FBMkM7d0JBQ2xELFFBQVEsRUFBRSxJQUFJO3FCQUNqQjtvQkFDRDt3QkFDSSxJQUFJLEVBQUUsTUFBTTt3QkFDWixHQUFHLEVBQUUsWUFBWTt3QkFDakIsS0FBSyxFQUFFLHlDQUF5Qzt3QkFDaEQsUUFBUSxFQUFFLElBQUk7cUJBQ2pCO29CQUNEO3dCQUNJLElBQUksRUFBRSxNQUFNO3dCQUNaLEdBQUcsRUFBRSxZQUFZO3dCQUNqQixLQUFLLEVBQUUseUNBQXlDO3dCQUNoRCxRQUFRLEVBQUUsSUFBSTtxQkFDakI7aUJBQ0osQ0FBQyxDQUFDO2dCQUVILEtBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxzQkFBc0IsQ0FBQyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsTUFBTSxDQUFDLEVBQUU7b0JBQy9ELEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxzQ0FBc0MsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFO29CQUNsRzt3QkFDSSxJQUFJLEVBQUUsTUFBTTt3QkFDWixHQUFHLEVBQUUsZ0JBQWdCO3dCQUNyQixLQUFLLEVBQUUsMENBQTBDO3dCQUNqRCxRQUFRLEVBQUUsSUFBSTtxQkFDakI7b0JBQ0Q7d0JBQ0ksSUFBSSxFQUFFLE1BQU07d0JBQ1osR0FBRyxFQUFFLHFCQUFxQjt3QkFDMUIsS0FBSyxFQUFFLGdEQUFnRDt3QkFDdkQsUUFBUSxFQUFFLElBQUk7cUJBQ2pCO29CQUNEO3dCQUNJLElBQUksRUFBRSxNQUFNO3dCQUNaLEdBQUcsRUFBRSw4QkFBOEI7d0JBQ25DLEtBQUssRUFBRSw4Q0FBOEM7d0JBQ3JELFFBQVEsRUFBRSxJQUFJO3FCQUNqQjtpQkFDSixDQUFDLENBQUM7Z0JBRUgsS0FBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLHNCQUFzQixDQUFDLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsRUFBRTtvQkFDakUsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLHVDQUF1QyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUU7b0JBQ2pHLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsV0FBVyxFQUFFLEtBQUssRUFBRSx3Q0FBd0MsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFO29CQUNuRzt3QkFDSSxJQUFJLEVBQUUsTUFBTTt3QkFDWixHQUFHLEVBQUUsZUFBZTt3QkFDcEIsS0FBSyxFQUFFLDRDQUE0Qzt3QkFDbkQsUUFBUSxFQUFFLElBQUk7cUJBQ2pCO29CQUNELEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxvQ0FBb0MsRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFO29CQUM1RixFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLE1BQU0sRUFBRSxLQUFLLEVBQUUsa0NBQWtDLEVBQUUsUUFBUSxFQUFFLElBQUksRUFBRTtvQkFDeEY7d0JBQ0ksSUFBSSxFQUFFLE1BQU07d0JBQ1osR0FBRyxFQUFFLGtCQUFrQjt3QkFDdkIsS0FBSyxFQUFFLDZDQUE2Qzt3QkFDcEQsUUFBUSxFQUFFLElBQUk7cUJBQ2pCO29CQUNEO3dCQUNJLElBQUksRUFBRSxNQUFNO3dCQUNaLEdBQUcsRUFBRSxvQkFBb0I7d0JBQ3pCLEtBQUssRUFBRSwrQ0FBK0M7d0JBQ3RELFFBQVEsRUFBRSxJQUFJO3FCQUNqQjtpQkFDSixDQUFDLENBQUM7WUFDUCxDQUFDLEVBQUMsQ0FBQztTQUNOO1FBRUQsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxFQUFFO1lBQ2xDLElBQUksQ0FBQyxTQUFTLENBQUMsaUJBQWlCLEVBQUUsQ0FBQyxTQUFTOzs7O1lBQUMsVUFBQyxPQUFPO2dCQUNqRCxLQUFJLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQztZQUM5QixDQUFDLEVBQUMsQ0FBQztTQUNOO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFDLFFBQWE7O2dCQUU3RCxnQkFBZ0IsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNOzs7O1lBQUMsVUFBQyxHQUFHO2dCQUNuRSxPQUFPLElBQUksTUFBTSxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDN0MsQ0FBQyxFQUFDOztnQkFFSSxtQ0FBbUMsR0FBRyxFQUFFO1lBQzlDLGdCQUFnQixDQUFDLE9BQU87Ozs7WUFBQyxVQUFDLEdBQUc7Z0JBQ3pCLG1DQUFtQyxDQUFDLElBQUksQ0FBQztvQkFDckMsSUFBSSxFQUFFLEdBQUc7b0JBQ1QsT0FBTyxFQUFFLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxPQUFPLElBQUksUUFBUSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDO2lCQUMvRixDQUFDLENBQUM7WUFDUCxDQUFDLEVBQUMsQ0FBQztZQUVILEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO1lBRTdELEtBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxzQkFBc0IsQ0FBQyxtQ0FBbUMsRUFBRTtnQkFDeEUsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFO2dCQUM1RCxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLFFBQVEsRUFBRSxJQUFJLEVBQUU7YUFDckUsQ0FBQyxDQUFDO1FBRVAsQ0FBQyxFQUFDLENBQUM7UUFFSCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFTLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNuRSxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFTLGVBQWUsQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN2RSxDQUFDOzs7Ozs7SUFFTywyQ0FBa0I7Ozs7O0lBQTFCLFVBQTJCLG1DQUFtQzs7WUFDcEQsV0FBVyxHQUFHLG1DQUFtQyxDQUFDLElBQUk7Ozs7UUFBQyxVQUFDLFNBQVM7WUFDbkUsT0FBTyxTQUFTLENBQUMsSUFBSSxLQUFLLG9CQUFvQixDQUFDO1FBQ25ELENBQUMsRUFBQztRQUVGLElBQUksV0FBVyxFQUFFOztnQkFDUCxTQUFTLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDO1lBQ2hELElBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3RCLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDO2FBQ3hFO2lCQUFNO2dCQUNILElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUMsb0JBQW9CLEdBQUcsV0FBVyxDQUFDLE9BQU8sQ0FBQzthQUMvRTtTQUNKO0lBQ0wsQ0FBQzs7Z0JBM0tKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsV0FBVztvQkFDckIsNC9KQUFxQztvQkFFckMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2lCQUN4Qzs7OztnQkFkUSxVQUFVO2dCQUtWLGdCQUFnQjtnQkFKaEIscUJBQXFCO2dCQUVyQixtQkFBbUI7Z0JBSUwsbUJBQW1COzs7dUNBa0JyQyxLQUFLO2lDQUlMLEtBQUs7eUJBSUwsS0FBSzs7SUFvSlYscUJBQUM7Q0FBQSxBQTVLRCxJQTRLQztTQXRLWSxjQUFjOzs7SUFFdkIsOEJBQTZCOztJQUM3QixnQ0FBK0I7O0lBQy9CLGlDQUFnQzs7SUFDaEMsaUNBQWdDOztJQUNoQywwQ0FBNkc7O0lBQzdHLHFDQUF3Qzs7Ozs7SUFHeEMsOENBQ3NGOzs7OztJQUd0Rix3Q0FDc0I7Ozs7O0lBR3RCLGdDQUFpQzs7SUFFakMsaUNBQWE7O0lBQ2IsaUNBQWE7O0lBRWIsb0NBQTBDOztJQUMxQyxvQ0FBMEM7Ozs7O0lBRTlCLDhCQUF3Qjs7Ozs7SUFDeEIsbUNBQW1DOzs7OztJQUNuQyxxQ0FBMEM7Ozs7O0lBQzFDLG1DQUFzQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCwgVmlld0VuY2Fwc3VsYXRpb24sIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEF1dGhlbnRpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2F1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBCcG1Qcm9kdWN0VmVyc2lvbk1vZGVsLCBFY21Qcm9kdWN0VmVyc2lvbk1vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL3Byb2R1Y3QtdmVyc2lvbi5tb2RlbCc7XHJcbmltcG9ydCB7IERpc2NvdmVyeUFwaVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9kaXNjb3ZlcnktYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPYmplY3REYXRhVGFibGVBZGFwdGVyIH0gZnJvbSAnLi4vZGF0YXRhYmxlL2RhdGEvb2JqZWN0LWRhdGF0YWJsZS1hZGFwdGVyJztcclxuaW1wb3J0IHsgQXBwQ29uZmlnU2VydmljZSwgQXBwQ29uZmlnVmFsdWVzIH0gZnJvbSAnLi4vYXBwLWNvbmZpZy9hcHAtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEV4dGVuc2lvblJlZiwgQXBwRXh0ZW5zaW9uU2VydmljZSB9IGZyb20gJ0BhbGZyZXNjby9hZGYtZXh0ZW5zaW9ucyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLWFib3V0JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9hYm91dC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9hYm91dC5jb21wb25lbnQuc2NzcyddLFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQWJvdXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIGRhdGE6IE9iamVjdERhdGFUYWJsZUFkYXB0ZXI7XHJcbiAgICBzdGF0dXM6IE9iamVjdERhdGFUYWJsZUFkYXB0ZXI7XHJcbiAgICBsaWNlbnNlOiBPYmplY3REYXRhVGFibGVBZGFwdGVyO1xyXG4gICAgbW9kdWxlczogT2JqZWN0RGF0YVRhYmxlQWRhcHRlcjtcclxuICAgIGV4dGVuc2lvbkNvbHVtbnM6IHN0cmluZ1tdID0gWyckaWQnLCAnJG5hbWUnLCAnJHZlcnNpb24nLCAnJHZlbmRvcicsICckbGljZW5zZScsICckcnVudGltZScsICckZGVzY3JpcHRpb24nXTtcclxuICAgIGV4dGVuc2lvbnMkOiBPYnNlcnZhYmxlPEV4dGVuc2lvblJlZltdPjtcclxuXHJcbiAgICAvKiogQ29tbWl0IGNvcnJlc3BvbmRpbmcgdG8gdGhlIHZlcnNpb24gb2YgQURGIHRvIGJlIHVzZWQuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgZ2l0aHViVXJsQ29tbWl0QWxwaGEgPSAnaHR0cHM6Ly9naXRodWIuY29tL0FsZnJlc2NvL2FsZnJlc2NvLW5nMi1jb21wb25lbnRzL2NvbW1pdHMvJztcclxuXHJcbiAgICAvKiogVG9nZ2xlcyBzaG93aW5nL2hpZGluZyBvZiBleHRlbnNpb25zIGJsb2NrLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHNob3dFeHRlbnNpb25zID0gdHJ1ZTtcclxuXHJcbiAgICAvKiogUmVndWxhciBleHByZXNzaW9uIGZvciBmaWx0ZXJpbmcgZGVwZW5kZW5jaWVzIHBhY2thZ2VzLiAqL1xyXG4gICAgQElucHV0KCkgcmVnZXhwID0gJ14oQGFsZnJlc2NvKSc7XHJcblxyXG4gICAgZWNtSG9zdCA9ICcnO1xyXG4gICAgYnBtSG9zdCA9ICcnO1xyXG5cclxuICAgIGVjbVZlcnNpb246IEVjbVByb2R1Y3RWZXJzaW9uTW9kZWwgPSBudWxsO1xyXG4gICAgYnBtVmVyc2lvbjogQnBtUHJvZHVjdFZlcnNpb25Nb2RlbCA9IG51bGw7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBhcHBDb25maWc6IEFwcENvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGF1dGhTZXJ2aWNlOiBBdXRoZW50aWNhdGlvblNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGRpc2NvdmVyeTogRGlzY292ZXJ5QXBpU2VydmljZSxcclxuICAgICAgICAgICAgICAgIGFwcEV4dGVuc2lvbnM6IEFwcEV4dGVuc2lvblNlcnZpY2UpIHtcclxuICAgICAgICB0aGlzLmV4dGVuc2lvbnMkID0gYXBwRXh0ZW5zaW9ucy5yZWZlcmVuY2VzJDtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuYXV0aFNlcnZpY2UuaXNFY21Mb2dnZWRJbigpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzY292ZXJ5LmdldEVjbVByb2R1Y3RJbmZvKCkuc3Vic2NyaWJlKChlY21WZXJzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVjbVZlcnNpb24gPSBlY21WZXJzO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMubW9kdWxlcyA9IG5ldyBPYmplY3REYXRhVGFibGVBZGFwdGVyKHRoaXMuZWNtVmVyc2lvbi5tb2R1bGVzLCBbXHJcbiAgICAgICAgICAgICAgICAgICAgeyB0eXBlOiAndGV4dCcsIGtleTogJ2lkJywgdGl0bGU6ICdBQk9VVC5UQUJMRV9IRUFERVJTLk1PRFVMRVMuSUQnLCBzb3J0YWJsZTogdHJ1ZSB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHsgdHlwZTogJ3RleHQnLCBrZXk6ICd0aXRsZScsIHRpdGxlOiAnQUJPVVQuVEFCTEVfSEVBREVSUy5NT0RVTEVTLlRJVExFJywgc29ydGFibGU6IHRydWUgfSxcclxuICAgICAgICAgICAgICAgICAgICB7IHR5cGU6ICd0ZXh0Jywga2V5OiAndmVyc2lvbicsIHRpdGxlOiAnQUJPVVQuVEFCTEVfSEVBREVSUy5NT0RVTEVTLkRFU0NSSVBUSU9OJywgc29ydGFibGU6IHRydWUgfSxcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICd0ZXh0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAnaW5zdGFsbERhdGUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0FCT1VULlRBQkxFX0hFQURFUlMuTU9EVUxFUy5JTlNUQUxMX0RBVEUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzb3J0YWJsZTogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJ2luc3RhbGxTdGF0ZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnQUJPVVQuVEFCTEVfSEVBREVSUy5NT0RVTEVTLklOU1RBTExfU1RBVEUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzb3J0YWJsZTogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJ3ZlcnNpb25NaW4nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0FCT1VULlRBQkxFX0hFQURFUlMuTU9EVUxFUy5WRVJTSU9OX01JTicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNvcnRhYmxlOiB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICd0ZXh0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAndmVyc2lvbk1heCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnQUJPVVQuVEFCTEVfSEVBREVSUy5NT0RVTEVTLlZFUlNJT05fTUFYJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc29ydGFibGU6IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBdKTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnN0YXR1cyA9IG5ldyBPYmplY3REYXRhVGFibGVBZGFwdGVyKFt0aGlzLmVjbVZlcnNpb24uc3RhdHVzXSwgW1xyXG4gICAgICAgICAgICAgICAgICAgIHsgdHlwZTogJ3RleHQnLCBrZXk6ICdpc1JlYWRPbmx5JywgdGl0bGU6ICdBQk9VVC5UQUJMRV9IRUFERVJTLlNUQVRVUy5SRUFEX09OTFknLCBzb3J0YWJsZTogdHJ1ZSB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3RleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBrZXk6ICdpc0F1ZGl0RW5hYmxlZCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnQUJPVVQuVEFCTEVfSEVBREVSUy5TVEFUVVMuQVVESVRfRU5BQkxFRCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNvcnRhYmxlOiB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICd0ZXh0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAnaXNRdWlja1NoYXJlRW5hYmxlZCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnQUJPVVQuVEFCTEVfSEVBREVSUy5TVEFUVVMuUVVJQ0tfU0hBUkVfRU5BQkxFRCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNvcnRhYmxlOiB0cnVlXHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICd0ZXh0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAnaXNUaHVtYm5haWxHZW5lcmF0aW9uRW5hYmxlZCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnQUJPVVQuVEFCTEVfSEVBREVSUy5TVEFUVVMuVEhVTUJOQUlMX0VOQUJMRUQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzb3J0YWJsZTogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIF0pO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMubGljZW5zZSA9IG5ldyBPYmplY3REYXRhVGFibGVBZGFwdGVyKFt0aGlzLmVjbVZlcnNpb24ubGljZW5zZV0sIFtcclxuICAgICAgICAgICAgICAgICAgICB7IHR5cGU6ICd0ZXh0Jywga2V5OiAnaXNzdWVkQXQnLCB0aXRsZTogJ0FCT1VULlRBQkxFX0hFQURFUlMuTElDRU5TRS5JU1NVRVNfQVQnLCBzb3J0YWJsZTogdHJ1ZSB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHsgdHlwZTogJ3RleHQnLCBrZXk6ICdleHBpcmVzQXQnLCB0aXRsZTogJ0FCT1VULlRBQkxFX0hFQURFUlMuTElDRU5TRS5FWFBJUkVTX0FUJywgc29ydGFibGU6IHRydWUgfSxcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICd0ZXh0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAga2V5OiAncmVtYWluaW5nRGF5cycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnQUJPVVQuVEFCTEVfSEVBREVSUy5MSUNFTlNFLlJFTUFJTklOR19EQVlTJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc29ydGFibGU6IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHsgdHlwZTogJ3RleHQnLCBrZXk6ICdob2xkZXInLCB0aXRsZTogJ0FCT1VULlRBQkxFX0hFQURFUlMuTElDRU5TRS5IT0xERVInLCBzb3J0YWJsZTogdHJ1ZSB9LFxyXG4gICAgICAgICAgICAgICAgICAgIHsgdHlwZTogJ3RleHQnLCBrZXk6ICdtb2RlJywgdGl0bGU6ICdBQk9VVC5UQUJMRV9IRUFERVJTLkxJQ0VOU0UuTU9ERScsIHNvcnRhYmxlOiB0cnVlIH0sXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJ2lzQ2x1c3RlckVuYWJsZWQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aXRsZTogJ0FCT1VULlRBQkxFX0hFQURFUlMuTElDRU5TRS5DTFVTVEVSX0VOQUJMRUQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzb3J0YWJsZTogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGtleTogJ2lzQ3J5cHRvZG9jRW5hYmxlZCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRpdGxlOiAnQUJPVVQuVEFCTEVfSEVBREVSUy5MSUNFTlNFLkNSWVBUT0RPQ19FTkFCTEVEJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc29ydGFibGU6IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBdKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5hdXRoU2VydmljZS5pc0JwbUxvZ2dlZEluKCkpIHtcclxuICAgICAgICAgICAgdGhpcy5kaXNjb3ZlcnkuZ2V0QnBtUHJvZHVjdEluZm8oKS5zdWJzY3JpYmUoKGJwbVZlcnMpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYnBtVmVyc2lvbiA9IGJwbVZlcnM7XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5odHRwLmdldCgnLi92ZXJzaW9ucy5qc29uPycgKyBuZXcgRGF0ZSgpKS5zdWJzY3JpYmUoKHJlc3BvbnNlOiBhbnkpID0+IHtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGFsZnJlc2NvUGFja2FnZXMgPSBPYmplY3Qua2V5cyhyZXNwb25zZS5kZXBlbmRlbmNpZXMpLmZpbHRlcigodmFsKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFJlZ0V4cCh0aGlzLnJlZ2V4cCkudGVzdCh2YWwpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGFsZnJlc2NvUGFja2FnZXNUYWJsZVJlcHJlc2VudGF0aW9uID0gW107XHJcbiAgICAgICAgICAgIGFsZnJlc2NvUGFja2FnZXMuZm9yRWFjaCgodmFsKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBhbGZyZXNjb1BhY2thZ2VzVGFibGVSZXByZXNlbnRhdGlvbi5wdXNoKHtcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiB2YWwsXHJcbiAgICAgICAgICAgICAgICAgICAgdmVyc2lvbjogKHJlc3BvbnNlLmRlcGVuZGVuY2llc1t2YWxdLnZlcnNpb24gfHwgcmVzcG9uc2UuZGVwZW5kZW5jaWVzW3ZhbF0ucmVxdWlyZWQudmVyc2lvbilcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZ2l0SHViTGlua0NyZWF0aW9uKGFsZnJlc2NvUGFja2FnZXNUYWJsZVJlcHJlc2VudGF0aW9uKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZGF0YSA9IG5ldyBPYmplY3REYXRhVGFibGVBZGFwdGVyKGFsZnJlc2NvUGFja2FnZXNUYWJsZVJlcHJlc2VudGF0aW9uLCBbXHJcbiAgICAgICAgICAgICAgICB7IHR5cGU6ICd0ZXh0Jywga2V5OiAnbmFtZScsIHRpdGxlOiAnTmFtZScsIHNvcnRhYmxlOiB0cnVlIH0sXHJcbiAgICAgICAgICAgICAgICB7IHR5cGU6ICd0ZXh0Jywga2V5OiAndmVyc2lvbicsIHRpdGxlOiAnVmVyc2lvbicsIHNvcnRhYmxlOiB0cnVlIH1cclxuICAgICAgICAgICAgXSk7XHJcblxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLmVjbUhvc3QgPSB0aGlzLmFwcENvbmZpZy5nZXQ8c3RyaW5nPihBcHBDb25maWdWYWx1ZXMuRUNNSE9TVCk7XHJcbiAgICAgICAgdGhpcy5icG1Ib3N0ID0gdGhpcy5hcHBDb25maWcuZ2V0PHN0cmluZz4oQXBwQ29uZmlnVmFsdWVzLkJQTUhPU1QpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2l0SHViTGlua0NyZWF0aW9uKGFsZnJlc2NvUGFja2FnZXNUYWJsZVJlcHJlc2VudGF0aW9uKTogdm9pZCB7XHJcbiAgICAgICAgY29uc3QgY29yZVBhY2thZ2UgPSBhbGZyZXNjb1BhY2thZ2VzVGFibGVSZXByZXNlbnRhdGlvbi5maW5kKChwYWNrYWdlVXApID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIHBhY2thZ2VVcC5uYW1lID09PSAnQGFsZnJlc2NvL2FkZi1jb3JlJztcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgaWYgKGNvcmVQYWNrYWdlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNvbW1pdElzaCA9IGNvcmVQYWNrYWdlLnZlcnNpb24uc3BsaXQoJy0nKTtcclxuICAgICAgICAgICAgaWYgKGNvbW1pdElzaC5sZW5ndGggPiAxKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdpdGh1YlVybENvbW1pdEFscGhhID0gdGhpcy5naXRodWJVcmxDb21taXRBbHBoYSArIGNvbW1pdElzaFsxXTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2l0aHViVXJsQ29tbWl0QWxwaGEgPSB0aGlzLmdpdGh1YlVybENvbW1pdEFscGhhICsgY29yZVBhY2thZ2UudmVyc2lvbjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=