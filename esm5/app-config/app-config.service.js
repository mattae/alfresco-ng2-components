/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { ObjectUtils } from '../utils/object-utils';
import { Subject } from 'rxjs';
import { map, distinctUntilChanged } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
/** @enum {string} */
var AppConfigValues = {
    APP_CONFIG_LANGUAGES_KEY: 'languages',
    PROVIDERS: 'providers',
    OAUTHCONFIG: 'oauth2',
    ECMHOST: 'ecmHost',
    BASESHAREURL: 'baseShareUrl',
    BPMHOST: 'bpmHost',
    IDENTITY_HOST: 'identityHost',
    AUTHTYPE: 'authType',
    CONTEXTROOTECM: 'contextRootEcm',
    CONTEXTROOTBPM: 'contextRootBpm',
    ALFRESCO_REPOSITORY_NAME: 'alfrescoRepositoryName',
    LOG_LEVEL: 'logLevel',
    LOGIN_ROUTE: 'loginRoute',
    DISABLECSRF: 'disableCSRF',
    AUTH_WITH_CREDENTIALS: 'auth.withCredentials',
    APPLICATION: 'application',
    STORAGE_PREFIX: 'application.storagePrefix',
    NOTIFY_DURATION: 'notificationDefaultDuration',
};
export { AppConfigValues };
/** @enum {string} */
var Status = {
    INIT: 'init',
    LOADING: 'loading',
    LOADED: 'loaded',
};
export { Status };
/* spellchecker: enable */
var AppConfigService = /** @class */ (function () {
    function AppConfigService(http) {
        this.http = http;
        this.config = {
            application: {
                name: 'Alfresco ADF Application'
            },
            ecmHost: 'http://{hostname}{:port}/ecm',
            bpmHost: 'http://{hostname}{:port}/bpm',
            logLevel: 'silent',
            alfrescoRepositoryName: 'alfresco-1'
        };
        this.status = Status.INIT;
        this.onLoadSubject = new Subject();
        this.onLoad = this.onLoadSubject.asObservable();
    }
    /**
     * Requests notification of a property value when it is loaded.
     * @param property The desired property value
     * @returns Property value, when loaded
     */
    /**
     * Requests notification of a property value when it is loaded.
     * @param {?} property The desired property value
     * @return {?} Property value, when loaded
     */
    AppConfigService.prototype.select = /**
     * Requests notification of a property value when it is loaded.
     * @param {?} property The desired property value
     * @return {?} Property value, when loaded
     */
    function (property) {
        return this.onLoadSubject
            .pipe(map((/**
         * @param {?} config
         * @return {?}
         */
        function (config) { return config[property]; })), distinctUntilChanged());
    };
    /**
     * Gets the value of a named property.
     * @param key Name of the property
     * @param defaultValue Value to return if the key is not found
     * @returns Value of the property
     */
    /**
     * Gets the value of a named property.
     * @template T
     * @param {?} key Name of the property
     * @param {?=} defaultValue Value to return if the key is not found
     * @return {?} Value of the property
     */
    AppConfigService.prototype.get = /**
     * Gets the value of a named property.
     * @template T
     * @param {?} key Name of the property
     * @param {?=} defaultValue Value to return if the key is not found
     * @return {?} Value of the property
     */
    function (key, defaultValue) {
        /** @type {?} */
        var result = ObjectUtils.getValue(this.config, key);
        if (typeof result === 'string') {
            /** @type {?} */
            var keywords = new Map();
            keywords.set('hostname', this.getLocationHostname());
            keywords.set(':port', this.getLocationPort(':'));
            keywords.set('port', this.getLocationPort());
            keywords.set('protocol', this.getLocationProtocol());
            result = this.formatString(result, keywords);
        }
        if (result === undefined) {
            return defaultValue;
        }
        return (/** @type {?} */ (result));
    };
    /**
     * Gets the location.protocol value.
     * @returns The location.protocol string
     */
    /**
     * Gets the location.protocol value.
     * @return {?} The location.protocol string
     */
    AppConfigService.prototype.getLocationProtocol = /**
     * Gets the location.protocol value.
     * @return {?} The location.protocol string
     */
    function () {
        return location.protocol;
    };
    /**
     * Gets the location.hostname property.
     * @returns Value of the property
     */
    /**
     * Gets the location.hostname property.
     * @return {?} Value of the property
     */
    AppConfigService.prototype.getLocationHostname = /**
     * Gets the location.hostname property.
     * @return {?} Value of the property
     */
    function () {
        return location.hostname;
    };
    /**
     * Gets the location.port property.
     * @param prefix Text added before port value
     * @returns Port with prefix
     */
    /**
     * Gets the location.port property.
     * @param {?=} prefix Text added before port value
     * @return {?} Port with prefix
     */
    AppConfigService.prototype.getLocationPort = /**
     * Gets the location.port property.
     * @param {?=} prefix Text added before port value
     * @return {?} Port with prefix
     */
    function (prefix) {
        if (prefix === void 0) { prefix = ''; }
        return location.port ? prefix + location.port : '';
    };
    /**
     * Loads the config file.
     * @returns Notification when loading is complete
     */
    /**
     * Loads the config file.
     * @return {?} Notification when loading is complete
     */
    AppConfigService.prototype.load = /**
     * Loads the config file.
     * @return {?} Notification when loading is complete
     */
    function () {
        var _this = this;
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        function (resolve) { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            var configUrl;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        configUrl = "app.config.json?v=" + Date.now();
                        if (!(this.status === Status.INIT)) return [3 /*break*/, 2];
                        this.status = Status.LOADING;
                        return [4 /*yield*/, this.http.get(configUrl).subscribe((/**
                             * @param {?} data
                             * @return {?}
                             */
                            function (data) {
                                _this.status = Status.LOADED;
                                _this.config = Object.assign({}, _this.config, data || {});
                                _this.onLoadSubject.next(_this.config);
                                resolve(_this.config);
                            }), (/**
                             * @return {?}
                             */
                            function () {
                                resolve(_this.config);
                            }))];
                    case 1:
                        _a.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        if (this.status === Status.LOADED) {
                            resolve(this.config);
                        }
                        else if (this.status === Status.LOADING) {
                            this.onLoad.subscribe((/**
                             * @return {?}
                             */
                            function () {
                                resolve(_this.config);
                            }));
                        }
                        _a.label = 3;
                    case 3: return [2 /*return*/];
                }
            });
        }); }));
    };
    /**
     * @private
     * @param {?} str
     * @param {?} keywords
     * @return {?}
     */
    AppConfigService.prototype.formatString = /**
     * @private
     * @param {?} str
     * @param {?} keywords
     * @return {?}
     */
    function (str, keywords) {
        /** @type {?} */
        var result = str;
        keywords.forEach((/**
         * @param {?} value
         * @param {?} key
         * @return {?}
         */
        function (value, key) {
            /** @type {?} */
            var expr = new RegExp('{' + key + '}', 'gm');
            result = result.replace(expr, value);
        }));
        return result;
    };
    AppConfigService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    AppConfigService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    /** @nocollapse */ AppConfigService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AppConfigService_Factory() { return new AppConfigService(i0.ɵɵinject(i1.HttpClient)); }, token: AppConfigService, providedIn: "root" });
    return AppConfigService;
}());
export { AppConfigService };
if (false) {
    /** @type {?} */
    AppConfigService.prototype.config;
    /** @type {?} */
    AppConfigService.prototype.status;
    /**
     * @type {?}
     * @protected
     */
    AppConfigService.prototype.onLoadSubject;
    /** @type {?} */
    AppConfigService.prototype.onLoad;
    /**
     * @type {?}
     * @private
     */
    AppConfigService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLWNvbmZpZy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiYXBwLWNvbmZpZy9hcHAtY29uZmlnLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNsRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUNwRCxPQUFPLEVBQWMsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxHQUFHLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7Ozs7SUFJdkQsMEJBQTJCLFdBQVc7SUFDdEMsV0FBWSxXQUFXO0lBQ3ZCLGFBQWMsUUFBUTtJQUN0QixTQUFVLFNBQVM7SUFDbkIsY0FBZSxjQUFjO0lBQzdCLFNBQVUsU0FBUztJQUNuQixlQUFnQixjQUFjO0lBQzlCLFVBQVcsVUFBVTtJQUNyQixnQkFBaUIsZ0JBQWdCO0lBQ2pDLGdCQUFpQixnQkFBZ0I7SUFDakMsMEJBQTJCLHdCQUF3QjtJQUNuRCxXQUFZLFVBQVU7SUFDdEIsYUFBYyxZQUFZO0lBQzFCLGFBQWMsYUFBYTtJQUMzQix1QkFBd0Isc0JBQXNCO0lBQzlDLGFBQWMsYUFBYTtJQUMzQixnQkFBaUIsMkJBQTJCO0lBQzVDLGlCQUFrQiw2QkFBNkI7Ozs7O0lBSS9DLE1BQU8sTUFBTTtJQUNiLFNBQVUsU0FBUztJQUNuQixRQUFTLFFBQVE7Ozs7QUFLckI7SUFtQkksMEJBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7UUFkcEMsV0FBTSxHQUFRO1lBQ1YsV0FBVyxFQUFFO2dCQUNULElBQUksRUFBRSwwQkFBMEI7YUFDbkM7WUFDRCxPQUFPLEVBQUUsOEJBQThCO1lBQ3ZDLE9BQU8sRUFBRSw4QkFBOEI7WUFDdkMsUUFBUSxFQUFFLFFBQVE7WUFDbEIsc0JBQXNCLEVBQUUsWUFBWTtTQUN2QyxDQUFDO1FBRUYsV0FBTSxHQUFXLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFLekIsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLE9BQU8sRUFBRSxDQUFDO1FBQ25DLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUNwRCxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsaUNBQU07Ozs7O0lBQU4sVUFBTyxRQUFnQjtRQUNuQixPQUFPLElBQUksQ0FBQyxhQUFhO2FBQ3BCLElBQUksQ0FDRCxHQUFHOzs7O1FBQUMsVUFBQyxNQUFNLElBQUssT0FBQSxNQUFNLENBQUMsUUFBUSxDQUFDLEVBQWhCLENBQWdCLEVBQUMsRUFDakMsb0JBQW9CLEVBQUUsQ0FDekIsQ0FBQztJQUNWLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7Ozs7SUFDSCw4QkFBRzs7Ozs7OztJQUFILFVBQU8sR0FBVyxFQUFFLFlBQWdCOztZQUM1QixNQUFNLEdBQVEsV0FBVyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEdBQUcsQ0FBQztRQUN4RCxJQUFJLE9BQU8sTUFBTSxLQUFLLFFBQVEsRUFBRTs7Z0JBQ3RCLFFBQVEsR0FBRyxJQUFJLEdBQUcsRUFBa0I7WUFDMUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUMsQ0FBQztZQUNyRCxRQUFRLENBQUMsR0FBRyxDQUFDLE9BQU8sRUFBRSxJQUFJLENBQUMsZUFBZSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDakQsUUFBUSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDLENBQUM7WUFDN0MsUUFBUSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixFQUFFLENBQUMsQ0FBQztZQUNyRCxNQUFNLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUM7U0FDaEQ7UUFDRCxJQUFJLE1BQU0sS0FBSyxTQUFTLEVBQUU7WUFDdEIsT0FBTyxZQUFZLENBQUM7U0FDdkI7UUFDRCxPQUFPLG1CQUFJLE1BQU0sRUFBQSxDQUFDO0lBQ3RCLENBQUM7SUFFRDs7O09BR0c7Ozs7O0lBQ0gsOENBQW1COzs7O0lBQW5CO1FBQ0ksT0FBTyxRQUFRLENBQUMsUUFBUSxDQUFDO0lBQzdCLENBQUM7SUFFRDs7O09BR0c7Ozs7O0lBQ0gsOENBQW1COzs7O0lBQW5CO1FBQ0ksT0FBTyxRQUFRLENBQUMsUUFBUSxDQUFDO0lBQzdCLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCwwQ0FBZTs7Ozs7SUFBZixVQUFnQixNQUFtQjtRQUFuQix1QkFBQSxFQUFBLFdBQW1CO1FBQy9CLE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsTUFBTSxHQUFHLFFBQVEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztJQUN2RCxDQUFDO0lBRUQ7OztPQUdHOzs7OztJQUNILCtCQUFJOzs7O0lBQUo7UUFBQSxpQkF5QkM7UUF4QkcsT0FBTyxJQUFJLE9BQU87Ozs7UUFBQyxVQUFPLE9BQU87Ozs7Ozt3QkFDdkIsU0FBUyxHQUFHLHVCQUFxQixJQUFJLENBQUMsR0FBRyxFQUFJOzZCQUUvQyxDQUFBLElBQUksQ0FBQyxNQUFNLEtBQUssTUFBTSxDQUFDLElBQUksQ0FBQSxFQUEzQix3QkFBMkI7d0JBQzNCLElBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQzt3QkFDN0IscUJBQU0sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsU0FBUyxDQUFDLENBQUMsU0FBUzs7Ozs0QkFDcEMsVUFBQyxJQUFTO2dDQUNOLEtBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQztnQ0FDNUIsS0FBSSxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxLQUFJLENBQUMsTUFBTSxFQUFFLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQztnQ0FDekQsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2dDQUNyQyxPQUFPLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDOzRCQUN6QixDQUFDOzs7NEJBQ0Q7Z0NBQ0ksT0FBTyxDQUFDLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQzs0QkFDekIsQ0FBQyxFQUNKLEVBQUE7O3dCQVZELFNBVUMsQ0FBQzs7O3dCQUNDLElBQUksSUFBSSxDQUFDLE1BQU0sS0FBSyxNQUFNLENBQUMsTUFBTSxFQUFFOzRCQUN0QyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO3lCQUN4Qjs2QkFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssTUFBTSxDQUFDLE9BQU8sRUFBRTs0QkFDdkMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTOzs7NEJBQUM7Z0NBQ2xCLE9BQU8sQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7NEJBQ3pCLENBQUMsRUFBQyxDQUFDO3lCQUNOOzs7OzthQUNKLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7Ozs7SUFFTyx1Q0FBWTs7Ozs7O0lBQXBCLFVBQXFCLEdBQVcsRUFBRSxRQUE2Qjs7WUFDdkQsTUFBTSxHQUFHLEdBQUc7UUFFaEIsUUFBUSxDQUFDLE9BQU87Ozs7O1FBQUMsVUFBQyxLQUFLLEVBQUUsR0FBRzs7Z0JBQ2xCLElBQUksR0FBRyxJQUFJLE1BQU0sQ0FBQyxHQUFHLEdBQUcsR0FBRyxHQUFHLEdBQUcsRUFBRSxJQUFJLENBQUM7WUFDOUMsTUFBTSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3pDLENBQUMsRUFBQyxDQUFDO1FBRUgsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQzs7Z0JBNUhKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7Z0JBdENRLFVBQVU7OzsyQkFqQm5CO0NBa0xDLEFBN0hELElBNkhDO1NBMUhZLGdCQUFnQjs7O0lBRXpCLGtDQVFFOztJQUVGLGtDQUE2Qjs7Ozs7SUFDN0IseUNBQXNDOztJQUN0QyxrQ0FBd0I7Ozs7O0lBRVosZ0NBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEh0dHBDbGllbnQgfSBmcm9tICdAYW5ndWxhci9jb21tb24vaHR0cCc7XHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JqZWN0VXRpbHMgfSBmcm9tICcuLi91dGlscy9vYmplY3QtdXRpbHMnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IG1hcCwgZGlzdGluY3RVbnRpbENoYW5nZWQgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG4vKiBzcGVsbGNoZWNrZXI6IGRpc2FibGUgKi9cclxuZXhwb3J0IGVudW0gQXBwQ29uZmlnVmFsdWVzIHtcclxuICAgIEFQUF9DT05GSUdfTEFOR1VBR0VTX0tFWSA9ICdsYW5ndWFnZXMnLFxyXG4gICAgUFJPVklERVJTID0gJ3Byb3ZpZGVycycsXHJcbiAgICBPQVVUSENPTkZJRyA9ICdvYXV0aDInLFxyXG4gICAgRUNNSE9TVCA9ICdlY21Ib3N0JyxcclxuICAgIEJBU0VTSEFSRVVSTCA9ICdiYXNlU2hhcmVVcmwnLFxyXG4gICAgQlBNSE9TVCA9ICdicG1Ib3N0JyxcclxuICAgIElERU5USVRZX0hPU1QgPSAnaWRlbnRpdHlIb3N0JyxcclxuICAgIEFVVEhUWVBFID0gJ2F1dGhUeXBlJyxcclxuICAgIENPTlRFWFRST09URUNNID0gJ2NvbnRleHRSb290RWNtJyxcclxuICAgIENPTlRFWFRST09UQlBNID0gJ2NvbnRleHRSb290QnBtJyxcclxuICAgIEFMRlJFU0NPX1JFUE9TSVRPUllfTkFNRSA9ICdhbGZyZXNjb1JlcG9zaXRvcnlOYW1lJyxcclxuICAgIExPR19MRVZFTCA9ICdsb2dMZXZlbCcsXHJcbiAgICBMT0dJTl9ST1VURSA9ICdsb2dpblJvdXRlJyxcclxuICAgIERJU0FCTEVDU1JGID0gJ2Rpc2FibGVDU1JGJyxcclxuICAgIEFVVEhfV0lUSF9DUkVERU5USUFMUyA9ICdhdXRoLndpdGhDcmVkZW50aWFscycsXHJcbiAgICBBUFBMSUNBVElPTiA9ICdhcHBsaWNhdGlvbicsXHJcbiAgICBTVE9SQUdFX1BSRUZJWCA9ICdhcHBsaWNhdGlvbi5zdG9yYWdlUHJlZml4JyxcclxuICAgIE5PVElGWV9EVVJBVElPTiA9ICdub3RpZmljYXRpb25EZWZhdWx0RHVyYXRpb24nXHJcbn1cclxuXHJcbmV4cG9ydCBlbnVtIFN0YXR1cyB7XHJcbiAgICBJTklUID0gJ2luaXQnLFxyXG4gICAgTE9BRElORyA9ICdsb2FkaW5nJyxcclxuICAgIExPQURFRCA9ICdsb2FkZWQnXHJcbn1cclxuXHJcbi8qIHNwZWxsY2hlY2tlcjogZW5hYmxlICovXHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEFwcENvbmZpZ1NlcnZpY2Uge1xyXG5cclxuICAgIGNvbmZpZzogYW55ID0ge1xyXG4gICAgICAgIGFwcGxpY2F0aW9uOiB7XHJcbiAgICAgICAgICAgIG5hbWU6ICdBbGZyZXNjbyBBREYgQXBwbGljYXRpb24nXHJcbiAgICAgICAgfSxcclxuICAgICAgICBlY21Ib3N0OiAnaHR0cDovL3tob3N0bmFtZX17OnBvcnR9L2VjbScsXHJcbiAgICAgICAgYnBtSG9zdDogJ2h0dHA6Ly97aG9zdG5hbWV9ezpwb3J0fS9icG0nLFxyXG4gICAgICAgIGxvZ0xldmVsOiAnc2lsZW50JyxcclxuICAgICAgICBhbGZyZXNjb1JlcG9zaXRvcnlOYW1lOiAnYWxmcmVzY28tMSdcclxuICAgIH07XHJcblxyXG4gICAgc3RhdHVzOiBTdGF0dXMgPSBTdGF0dXMuSU5JVDtcclxuICAgIHByb3RlY3RlZCBvbkxvYWRTdWJqZWN0OiBTdWJqZWN0PGFueT47XHJcbiAgICBvbkxvYWQ6IE9ic2VydmFibGU8YW55PjtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGh0dHA6IEh0dHBDbGllbnQpIHtcclxuICAgICAgICB0aGlzLm9uTG9hZFN1YmplY3QgPSBuZXcgU3ViamVjdCgpO1xyXG4gICAgICAgIHRoaXMub25Mb2FkID0gdGhpcy5vbkxvYWRTdWJqZWN0LmFzT2JzZXJ2YWJsZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVxdWVzdHMgbm90aWZpY2F0aW9uIG9mIGEgcHJvcGVydHkgdmFsdWUgd2hlbiBpdCBpcyBsb2FkZWQuXHJcbiAgICAgKiBAcGFyYW0gcHJvcGVydHkgVGhlIGRlc2lyZWQgcHJvcGVydHkgdmFsdWVcclxuICAgICAqIEByZXR1cm5zIFByb3BlcnR5IHZhbHVlLCB3aGVuIGxvYWRlZFxyXG4gICAgICovXHJcbiAgICBzZWxlY3QocHJvcGVydHk6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub25Mb2FkU3ViamVjdFxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCgoY29uZmlnKSA9PiBjb25maWdbcHJvcGVydHldKSxcclxuICAgICAgICAgICAgICAgIGRpc3RpbmN0VW50aWxDaGFuZ2VkKClcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIHZhbHVlIG9mIGEgbmFtZWQgcHJvcGVydHkuXHJcbiAgICAgKiBAcGFyYW0ga2V5IE5hbWUgb2YgdGhlIHByb3BlcnR5XHJcbiAgICAgKiBAcGFyYW0gZGVmYXVsdFZhbHVlIFZhbHVlIHRvIHJldHVybiBpZiB0aGUga2V5IGlzIG5vdCBmb3VuZFxyXG4gICAgICogQHJldHVybnMgVmFsdWUgb2YgdGhlIHByb3BlcnR5XHJcbiAgICAgKi9cclxuICAgIGdldDxUPihrZXk6IHN0cmluZywgZGVmYXVsdFZhbHVlPzogVCk6IFQge1xyXG4gICAgICAgIGxldCByZXN1bHQ6IGFueSA9IE9iamVjdFV0aWxzLmdldFZhbHVlKHRoaXMuY29uZmlnLCBrZXkpO1xyXG4gICAgICAgIGlmICh0eXBlb2YgcmVzdWx0ID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICBjb25zdCBrZXl3b3JkcyA9IG5ldyBNYXA8c3RyaW5nLCBzdHJpbmc+KCk7XHJcbiAgICAgICAgICAgIGtleXdvcmRzLnNldCgnaG9zdG5hbWUnLCB0aGlzLmdldExvY2F0aW9uSG9zdG5hbWUoKSk7XHJcbiAgICAgICAgICAgIGtleXdvcmRzLnNldCgnOnBvcnQnLCB0aGlzLmdldExvY2F0aW9uUG9ydCgnOicpKTtcclxuICAgICAgICAgICAga2V5d29yZHMuc2V0KCdwb3J0JywgdGhpcy5nZXRMb2NhdGlvblBvcnQoKSk7XHJcbiAgICAgICAgICAgIGtleXdvcmRzLnNldCgncHJvdG9jb2wnLCB0aGlzLmdldExvY2F0aW9uUHJvdG9jb2woKSk7XHJcbiAgICAgICAgICAgIHJlc3VsdCA9IHRoaXMuZm9ybWF0U3RyaW5nKHJlc3VsdCwga2V5d29yZHMpO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAocmVzdWx0ID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGRlZmF1bHRWYWx1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIDxUPiByZXN1bHQ7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBsb2NhdGlvbi5wcm90b2NvbCB2YWx1ZS5cclxuICAgICAqIEByZXR1cm5zIFRoZSBsb2NhdGlvbi5wcm90b2NvbCBzdHJpbmdcclxuICAgICAqL1xyXG4gICAgZ2V0TG9jYXRpb25Qcm90b2NvbCgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBsb2NhdGlvbi5wcm90b2NvbDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIGxvY2F0aW9uLmhvc3RuYW1lIHByb3BlcnR5LlxyXG4gICAgICogQHJldHVybnMgVmFsdWUgb2YgdGhlIHByb3BlcnR5XHJcbiAgICAgKi9cclxuICAgIGdldExvY2F0aW9uSG9zdG5hbWUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gbG9jYXRpb24uaG9zdG5hbWU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBsb2NhdGlvbi5wb3J0IHByb3BlcnR5LlxyXG4gICAgICogQHBhcmFtIHByZWZpeCBUZXh0IGFkZGVkIGJlZm9yZSBwb3J0IHZhbHVlXHJcbiAgICAgKiBAcmV0dXJucyBQb3J0IHdpdGggcHJlZml4XHJcbiAgICAgKi9cclxuICAgIGdldExvY2F0aW9uUG9ydChwcmVmaXg6IHN0cmluZyA9ICcnKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gbG9jYXRpb24ucG9ydCA/IHByZWZpeCArIGxvY2F0aW9uLnBvcnQgOiAnJztcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExvYWRzIHRoZSBjb25maWcgZmlsZS5cclxuICAgICAqIEByZXR1cm5zIE5vdGlmaWNhdGlvbiB3aGVuIGxvYWRpbmcgaXMgY29tcGxldGVcclxuICAgICAqL1xyXG4gICAgbG9hZCgpOiBQcm9taXNlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZShhc3luYyAocmVzb2x2ZSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBjb25maWdVcmwgPSBgYXBwLmNvbmZpZy5qc29uP3Y9JHtEYXRlLm5vdygpfWA7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5zdGF0dXMgPT09IFN0YXR1cy5JTklUKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnN0YXR1cyA9IFN0YXR1cy5MT0FESU5HO1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy5odHRwLmdldChjb25maWdVcmwpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgICAgICAoZGF0YTogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc3RhdHVzID0gU3RhdHVzLkxPQURFRDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb25maWcgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLmNvbmZpZywgZGF0YSB8fCB7fSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub25Mb2FkU3ViamVjdC5uZXh0KHRoaXMuY29uZmlnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzb2x2ZSh0aGlzLmNvbmZpZyk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc29sdmUodGhpcy5jb25maWcpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdGF0dXMgPT09IFN0YXR1cy5MT0FERUQpIHtcclxuICAgICAgICAgICAgICAgIHJlc29sdmUodGhpcy5jb25maWcpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc3RhdHVzID09PSBTdGF0dXMuTE9BRElORykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vbkxvYWQuc3Vic2NyaWJlKCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICByZXNvbHZlKHRoaXMuY29uZmlnKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBmb3JtYXRTdHJpbmcoc3RyOiBzdHJpbmcsIGtleXdvcmRzOiBNYXA8c3RyaW5nLCBzdHJpbmc+KTogc3RyaW5nIHtcclxuICAgICAgICBsZXQgcmVzdWx0ID0gc3RyO1xyXG5cclxuICAgICAgICBrZXl3b3Jkcy5mb3JFYWNoKCh2YWx1ZSwga2V5KSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGV4cHIgPSBuZXcgUmVnRXhwKCd7JyArIGtleSArICd9JywgJ2dtJyk7XHJcbiAgICAgICAgICAgIHJlc3VsdCA9IHJlc3VsdC5yZXBsYWNlKGV4cHIsIHZhbHVlKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxufVxyXG4iXX0=