/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService } from '../services/storage.service';
import { AppConfigService, AppConfigValues } from '../app-config/app-config.service';
var DebugAppConfigService = /** @class */ (function (_super) {
    tslib_1.__extends(DebugAppConfigService, _super);
    function DebugAppConfigService(storage, http) {
        var _this = _super.call(this, http) || this;
        _this.storage = storage;
        return _this;
    }
    /** @override */
    /**
     * @override
     * @template T
     * @param {?} key
     * @param {?=} defaultValue
     * @return {?}
     */
    DebugAppConfigService.prototype.get = /**
     * @override
     * @template T
     * @param {?} key
     * @param {?=} defaultValue
     * @return {?}
     */
    function (key, defaultValue) {
        if (key === AppConfigValues.OAUTHCONFIG) {
            return (/** @type {?} */ ((JSON.parse(this.storage.getItem(key)) || _super.prototype.get.call(this, key, defaultValue))));
        }
        else if (key === AppConfigValues.APPLICATION) {
            return undefined;
        }
        else {
            return (/** @type {?} */ (((/** @type {?} */ (this.storage.getItem(key))) || _super.prototype.get.call(this, key, defaultValue))));
        }
    };
    DebugAppConfigService.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    DebugAppConfigService.ctorParameters = function () { return [
        { type: StorageService },
        { type: HttpClient }
    ]; };
    return DebugAppConfigService;
}(AppConfigService));
export { DebugAppConfigService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    DebugAppConfigService.prototype.storage;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVidWctYXBwLWNvbmZpZy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiYXBwLWNvbmZpZy9kZWJ1Zy1hcHAtY29uZmlnLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsZUFBZSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFFckY7SUFDMkMsaURBQWdCO0lBQ3ZELCtCQUFvQixPQUF1QixFQUFFLElBQWdCO1FBQTdELFlBQ0ksa0JBQU0sSUFBSSxDQUFDLFNBQ2Q7UUFGbUIsYUFBTyxHQUFQLE9BQU8sQ0FBZ0I7O0lBRTNDLENBQUM7SUFFRCxnQkFBZ0I7Ozs7Ozs7O0lBQ2hCLG1DQUFHOzs7Ozs7O0lBQUgsVUFBTyxHQUFXLEVBQUUsWUFBZ0I7UUFDaEMsSUFBSSxHQUFHLEtBQUssZUFBZSxDQUFDLFdBQVcsRUFBRTtZQUNyQyxPQUFPLG1CQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLGlCQUFNLEdBQUcsWUFBSSxHQUFHLEVBQUUsWUFBWSxDQUFDLENBQUMsRUFBQSxDQUFDO1NBQ3pGO2FBQU0sSUFBSSxHQUFHLEtBQUssZUFBZSxDQUFDLFdBQVcsRUFBRTtZQUM1QyxPQUFPLFNBQVMsQ0FBQztTQUNwQjthQUFNO1lBQ0gsT0FBTyxtQkFBSSxDQUFDLG1CQUFNLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFBLElBQUksaUJBQU0sR0FBRyxZQUFJLEdBQUcsRUFBRSxZQUFZLENBQUMsQ0FBQyxFQUFBLENBQUM7U0FDbkY7SUFDTCxDQUFDOztnQkFmSixVQUFVOzs7O2dCQUhGLGNBQWM7Z0JBRGQsVUFBVTs7SUFvQm5CLDRCQUFDO0NBQUEsQUFoQkQsQ0FDMkMsZ0JBQWdCLEdBZTFEO1NBZlkscUJBQXFCOzs7Ozs7SUFDbEIsd0NBQStCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgU3RvcmFnZVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9zdG9yYWdlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBcHBDb25maWdTZXJ2aWNlLCBBcHBDb25maWdWYWx1ZXMgfSBmcm9tICcuLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XHJcblxyXG5ASW5qZWN0YWJsZSgpXHJcbmV4cG9ydCBjbGFzcyBEZWJ1Z0FwcENvbmZpZ1NlcnZpY2UgZXh0ZW5kcyBBcHBDb25maWdTZXJ2aWNlIHtcclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgc3RvcmFnZTogU3RvcmFnZVNlcnZpY2UsIGh0dHA6IEh0dHBDbGllbnQpIHtcclxuICAgICAgICBzdXBlcihodHRwKTtcclxuICAgIH1cclxuXHJcbiAgICAvKiogQG92ZXJyaWRlICovXHJcbiAgICBnZXQ8VD4oa2V5OiBzdHJpbmcsIGRlZmF1bHRWYWx1ZT86IFQpOiBUIHtcclxuICAgICAgICBpZiAoa2V5ID09PSBBcHBDb25maWdWYWx1ZXMuT0FVVEhDT05GSUcpIHtcclxuICAgICAgICAgICAgcmV0dXJuIDxUPiAoSlNPTi5wYXJzZSh0aGlzLnN0b3JhZ2UuZ2V0SXRlbShrZXkpKSB8fCBzdXBlci5nZXQ8VD4oa2V5LCBkZWZhdWx0VmFsdWUpKTtcclxuICAgICAgICB9IGVsc2UgaWYgKGtleSA9PT0gQXBwQ29uZmlnVmFsdWVzLkFQUExJQ0FUSU9OKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB1bmRlZmluZWQ7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIDxUPiAoPGFueT4gdGhpcy5zdG9yYWdlLmdldEl0ZW0oa2V5KSB8fCBzdXBlci5nZXQ8VD4oa2V5LCBkZWZhdWx0VmFsdWUpKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19