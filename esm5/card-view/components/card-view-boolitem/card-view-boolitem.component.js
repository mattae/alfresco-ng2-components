/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input } from '@angular/core';
import { CardViewBoolItemModel } from '../../models/card-view-boolitem.model';
import { CardViewUpdateService } from '../../services/card-view-update.service';
var CardViewBoolItemComponent = /** @class */ (function () {
    function CardViewBoolItemComponent(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
    }
    /**
     * @return {?}
     */
    CardViewBoolItemComponent.prototype.isEditable = /**
     * @return {?}
     */
    function () {
        return this.editable && this.property.editable;
    };
    /**
     * @param {?} change
     * @return {?}
     */
    CardViewBoolItemComponent.prototype.changed = /**
     * @param {?} change
     * @return {?}
     */
    function (change) {
        this.cardViewUpdateService.update(this.property, change.checked);
        this.property.value = change.checked;
    };
    CardViewBoolItemComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-card-view-boolitem',
                    template: "<ng-container *ngIf=\"!property.isEmpty() || isEditable()\">\r\n    <div [attr.data-automation-id]=\"'card-boolean-label-' + property.key\" class=\"adf-property-label\">{{ property.label | translate }}</div>\r\n    <div class=\"adf-property-value\">\r\n        <mat-checkbox\r\n            [attr.data-automation-id]=\"'card-boolean-' + property.key\"\r\n            [attr.title]=\"'CORE.METADATA.ACTIONS.TOGGLE' | translate\"\r\n            [checked]=\"property.displayValue\"\r\n            [disabled]=\"!isEditable()\"\r\n            (change)=\"changed($event)\">\r\n        </mat-checkbox>\r\n    </div>\r\n</ng-container>\r\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    CardViewBoolItemComponent.ctorParameters = function () { return [
        { type: CardViewUpdateService }
    ]; };
    CardViewBoolItemComponent.propDecorators = {
        property: [{ type: Input }],
        editable: [{ type: Input }]
    };
    return CardViewBoolItemComponent;
}());
export { CardViewBoolItemComponent };
if (false) {
    /** @type {?} */
    CardViewBoolItemComponent.prototype.property;
    /** @type {?} */
    CardViewBoolItemComponent.prototype.editable;
    /**
     * @type {?}
     * @private
     */
    CardViewBoolItemComponent.prototype.cardViewUpdateService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWJvb2xpdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNhcmQtdmlldy9jb21wb25lbnRzL2NhcmQtdmlldy1ib29saXRlbS9jYXJkLXZpZXctYm9vbGl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRWpELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBRWhGO0lBY0ksbUNBQW9CLHFCQUE0QztRQUE1QywwQkFBcUIsR0FBckIscUJBQXFCLENBQXVCO0lBQUcsQ0FBQzs7OztJQUVwRSw4Q0FBVTs7O0lBQVY7UUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7SUFDbkQsQ0FBQzs7Ozs7SUFFRCwyQ0FBTzs7OztJQUFQLFVBQVEsTUFBeUI7UUFDN0IsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUUsQ0FBQztRQUNsRSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsT0FBTyxDQUFDO0lBQ3pDLENBQUM7O2dCQXZCSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLHdCQUF3QjtvQkFDbEMsaW9CQUFrRDs7aUJBRXJEOzs7O2dCQU5RLHFCQUFxQjs7OzJCQVV6QixLQUFLOzJCQUdMLEtBQUs7O0lBYVYsZ0NBQUM7Q0FBQSxBQXhCRCxJQXdCQztTQWxCWSx5QkFBeUI7OztJQUVsQyw2Q0FDZ0M7O0lBRWhDLDZDQUNrQjs7Ozs7SUFFTiwwREFBb0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXRDaGVja2JveENoYW5nZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdCb29sSXRlbU1vZGVsIH0gZnJvbSAnLi4vLi4vbW9kZWxzL2NhcmQtdmlldy1ib29saXRlbS5tb2RlbCc7XHJcbmltcG9ydCB7IENhcmRWaWV3VXBkYXRlU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2NhcmQtdmlldy11cGRhdGUuc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLWNhcmQtdmlldy1ib29saXRlbScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vY2FyZC12aWV3LWJvb2xpdGVtLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2NhcmQtdmlldy1ib29saXRlbS5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQ2FyZFZpZXdCb29sSXRlbUNvbXBvbmVudCB7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHByb3BlcnR5OiBDYXJkVmlld0Jvb2xJdGVtTW9kZWw7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGVkaXRhYmxlOiBib29sZWFuO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgY2FyZFZpZXdVcGRhdGVTZXJ2aWNlOiBDYXJkVmlld1VwZGF0ZVNlcnZpY2UpIHt9XHJcblxyXG4gICAgaXNFZGl0YWJsZSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5lZGl0YWJsZSAmJiB0aGlzLnByb3BlcnR5LmVkaXRhYmxlO1xyXG4gICAgfVxyXG5cclxuICAgIGNoYW5nZWQoY2hhbmdlOiBNYXRDaGVja2JveENoYW5nZSkge1xyXG4gICAgICAgIHRoaXMuY2FyZFZpZXdVcGRhdGVTZXJ2aWNlLnVwZGF0ZSh0aGlzLnByb3BlcnR5LCBjaGFuZ2UuY2hlY2tlZCApO1xyXG4gICAgICAgIHRoaXMucHJvcGVydHkudmFsdWUgPSBjaGFuZ2UuY2hlY2tlZDtcclxuICAgIH1cclxufVxyXG4iXX0=