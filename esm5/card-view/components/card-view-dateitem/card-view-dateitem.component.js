/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewChild } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { DatetimeAdapter, MAT_DATETIME_FORMATS, MatDatetimepicker } from '@mat-datetimepicker/core';
import { MAT_MOMENT_DATETIME_FORMATS, MomentDatetimeAdapter } from '@mat-datetimepicker/moment';
import moment from 'moment-es6';
import { CardViewDateItemModel } from '../../models/card-view-dateitem.model';
import { CardViewUpdateService } from '../../services/card-view-update.service';
import { UserPreferencesService, UserPreferenceValues } from '../../../services/user-preferences.service';
import { MomentDateAdapter } from '../../../utils/momentDateAdapter';
import { MOMENT_DATE_FORMATS } from '../../../utils/moment-date-formats.model';
import { AppConfigService } from '../../../app-config/app-config.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
var ɵ0 = MOMENT_DATE_FORMATS, ɵ1 = MAT_MOMENT_DATETIME_FORMATS;
var CardViewDateItemComponent = /** @class */ (function () {
    function CardViewDateItemComponent(cardViewUpdateService, dateAdapter, userPreferencesService, appConfig) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.dateAdapter = dateAdapter;
        this.userPreferencesService = userPreferencesService;
        this.appConfig = appConfig;
        this.editable = false;
        this.displayEmpty = true;
        this.onDestroy$ = new Subject();
        this.dateFormat = this.appConfig.get('dateValues.defaultDateFormat');
    }
    /**
     * @return {?}
     */
    CardViewDateItemComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.userPreferencesService
            .select(UserPreferenceValues.Locale)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} locale
         * @return {?}
         */
        function (locale) { return _this.dateAdapter.setLocale(locale); }));
        ((/** @type {?} */ (this.dateAdapter))).overrideDisplayFormat = 'MMM DD';
        if (this.property.value) {
            this.valueDate = moment(this.property.value, this.dateFormat);
        }
    };
    /**
     * @return {?}
     */
    CardViewDateItemComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    };
    /**
     * @return {?}
     */
    CardViewDateItemComponent.prototype.showProperty = /**
     * @return {?}
     */
    function () {
        return this.displayEmpty || !this.property.isEmpty();
    };
    /**
     * @return {?}
     */
    CardViewDateItemComponent.prototype.isEditable = /**
     * @return {?}
     */
    function () {
        return this.editable && this.property.editable;
    };
    /**
     * @return {?}
     */
    CardViewDateItemComponent.prototype.showDatePicker = /**
     * @return {?}
     */
    function () {
        this.datepicker.open();
    };
    /**
     * @param {?} newDateValue
     * @return {?}
     */
    CardViewDateItemComponent.prototype.onDateChanged = /**
     * @param {?} newDateValue
     * @return {?}
     */
    function (newDateValue) {
        if (newDateValue) {
            /** @type {?} */
            var momentDate = moment(newDateValue.value, this.dateFormat, true);
            if (momentDate.isValid()) {
                this.valueDate = momentDate;
                this.cardViewUpdateService.update(this.property, momentDate.toDate());
                this.property.value = momentDate.toDate();
            }
        }
    };
    CardViewDateItemComponent.decorators = [
        { type: Component, args: [{
                    providers: [
                        { provide: DateAdapter, useClass: MomentDateAdapter },
                        { provide: MAT_DATE_FORMATS, useValue: ɵ0 },
                        { provide: DatetimeAdapter, useClass: MomentDatetimeAdapter },
                        { provide: MAT_DATETIME_FORMATS, useValue: ɵ1 }
                    ],
                    selector: 'adf-card-view-dateitem',
                    template: "<div [attr.data-automation-id]=\"'card-dateitem-label-' + property.key\" class=\"adf-property-label\" *ngIf=\"showProperty() || isEditable()\">{{ property.label | translate }}</div>\r\n<div class=\"adf-property-value\">\r\n    <span *ngIf=\"!isEditable()\" [attr.data-automation-id]=\"'card-' + property.type + '-value-' + property.key\">\r\n        <span [attr.data-automation-id]=\"'card-dateitem-' + property.key\">\r\n            <span *ngIf=\"showProperty()\">{{ property.displayValue }}</span>\r\n        </span>\r\n    </span>\r\n    <div *ngIf=\"isEditable()\" class=\"adf-dateitem-editable\">\r\n        <div class=\"adf-dateitem-editable-controls\">\r\n            <span\r\n                class=\"adf-datepicker-toggle\"\r\n                [attr.data-automation-id]=\"'datepicker-label-toggle-' + property.key\"\r\n                (click)=\"showDatePicker()\">\r\n                <span [attr.data-automation-id]=\"'card-' + property.type + '-value-' + property.key\" *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ property.displayValue }}</span>\r\n            </span>\r\n            <mat-datetimepicker-toggle\r\n                [attr.title]=\"'CORE.METADATA.ACTIONS.EDIT' | translate\"\r\n                [attr.data-automation-id]=\"'datepickertoggle-' + property.key\"\r\n                [for]=\"datetimePicker\">\r\n            </mat-datetimepicker-toggle>\r\n        </div>\r\n\r\n        <input class=\"adf-invisible-date-input\"\r\n            [matDatetimepicker]=\"datetimePicker\"\r\n            [value]=\"valueDate\"\r\n            (dateChange)=\"onDateChanged($event)\">\r\n\r\n        <mat-datetimepicker #datetimePicker\r\n            [type]=\"property.type\"\r\n            timeInterval=\"5\"\r\n            [attr.data-automation-id]=\"'datepicker-' + property.key\"\r\n            [startAt]=\"valueDate\">\r\n        </mat-datetimepicker>\r\n    </div>\r\n    <ng-template #elseEmptyValueBlock>\r\n        {{ property.default | translate }}\r\n    </ng-template>\r\n</div>\r\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    CardViewDateItemComponent.ctorParameters = function () { return [
        { type: CardViewUpdateService },
        { type: DateAdapter },
        { type: UserPreferencesService },
        { type: AppConfigService }
    ]; };
    CardViewDateItemComponent.propDecorators = {
        property: [{ type: Input }],
        editable: [{ type: Input }],
        displayEmpty: [{ type: Input }],
        datepicker: [{ type: ViewChild, args: ['datetimePicker', { static: true },] }]
    };
    return CardViewDateItemComponent;
}());
export { CardViewDateItemComponent };
if (false) {
    /** @type {?} */
    CardViewDateItemComponent.prototype.property;
    /** @type {?} */
    CardViewDateItemComponent.prototype.editable;
    /** @type {?} */
    CardViewDateItemComponent.prototype.displayEmpty;
    /** @type {?} */
    CardViewDateItemComponent.prototype.datepicker;
    /** @type {?} */
    CardViewDateItemComponent.prototype.valueDate;
    /** @type {?} */
    CardViewDateItemComponent.prototype.dateFormat;
    /**
     * @type {?}
     * @private
     */
    CardViewDateItemComponent.prototype.onDestroy$;
    /**
     * @type {?}
     * @private
     */
    CardViewDateItemComponent.prototype.cardViewUpdateService;
    /**
     * @type {?}
     * @private
     */
    CardViewDateItemComponent.prototype.dateAdapter;
    /**
     * @type {?}
     * @private
     */
    CardViewDateItemComponent.prototype.userPreferencesService;
    /**
     * @type {?}
     * @private
     */
    CardViewDateItemComponent.prototype.appConfig;
}
export { ɵ0, ɵ1 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWRhdGVpdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNhcmQtdmlldy9jb21wb25lbnRzL2NhcmQtdmlldy1kYXRlaXRlbS9jYXJkLXZpZXctZGF0ZWl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBcUIsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxXQUFXLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRSxPQUFPLEVBQUUsZUFBZSxFQUFFLG9CQUFvQixFQUFFLGlCQUFpQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDcEcsT0FBTyxFQUFFLDJCQUEyQixFQUFFLHFCQUFxQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDaEcsT0FBTyxNQUFNLE1BQU0sWUFBWSxDQUFDO0FBRWhDLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDRDQUE0QyxDQUFDO0FBQzFHLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLDBDQUEwQyxDQUFDO0FBQy9FLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO1NBS0ksbUJBQW1CLE9BRWYsMkJBQTJCO0FBTDlFO0lBOEJJLG1DQUFvQixxQkFBNEMsRUFDNUMsV0FBZ0MsRUFDaEMsc0JBQThDLEVBQzlDLFNBQTJCO1FBSDNCLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBdUI7UUFDNUMsZ0JBQVcsR0FBWCxXQUFXLENBQXFCO1FBQ2hDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFDOUMsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFoQi9DLGFBQVEsR0FBWSxLQUFLLENBQUM7UUFHMUIsaUJBQVksR0FBWSxJQUFJLENBQUM7UUFRckIsZUFBVSxHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7UUFNeEMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyw4QkFBOEIsQ0FBQyxDQUFDO0lBQ3pFLENBQUM7Ozs7SUFFRCw0Q0FBUTs7O0lBQVI7UUFBQSxpQkFXQztRQVZHLElBQUksQ0FBQyxzQkFBc0I7YUFDdEIsTUFBTSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQzthQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNoQyxTQUFTOzs7O1FBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBbEMsQ0FBa0MsRUFBQyxDQUFDO1FBRTdELENBQUMsbUJBQW9CLElBQUksQ0FBQyxXQUFXLEVBQUEsQ0FBQyxDQUFDLHFCQUFxQixHQUFHLFFBQVEsQ0FBQztRQUV4RSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxTQUFTLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUNqRTtJQUNMLENBQUM7Ozs7SUFFRCwrQ0FBVzs7O0lBQVg7UUFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQy9CLENBQUM7Ozs7SUFFRCxnREFBWTs7O0lBQVo7UUFDSSxPQUFPLElBQUksQ0FBQyxZQUFZLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQ3pELENBQUM7Ozs7SUFFRCw4Q0FBVTs7O0lBQVY7UUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUM7SUFDbkQsQ0FBQzs7OztJQUVELGtEQUFjOzs7SUFBZDtRQUNJLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUM7SUFDM0IsQ0FBQzs7Ozs7SUFFRCxpREFBYTs7OztJQUFiLFVBQWMsWUFBWTtRQUN0QixJQUFJLFlBQVksRUFBRTs7Z0JBQ1IsVUFBVSxHQUFHLE1BQU0sQ0FBQyxZQUFZLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDO1lBQ3BFLElBQUksVUFBVSxDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUN0QixJQUFJLENBQUMsU0FBUyxHQUFHLFVBQVUsQ0FBQztnQkFDNUIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQyxDQUFDO2dCQUN0RSxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUMsTUFBTSxFQUFFLENBQUM7YUFDN0M7U0FDSjtJQUNMLENBQUM7O2dCQTVFSixTQUFTLFNBQUM7b0JBQ1AsU0FBUyxFQUFFO3dCQUNQLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUU7d0JBQ3JELEVBQUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFFBQVEsSUFBcUIsRUFBRTt3QkFDNUQsRUFBRSxPQUFPLEVBQUUsZUFBZSxFQUFFLFFBQVEsRUFBRSxxQkFBcUIsRUFBRTt3QkFDN0QsRUFBRSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsUUFBUSxJQUE2QixFQUFFO3FCQUMzRTtvQkFDRCxRQUFRLEVBQUUsd0JBQXdCO29CQUNsQyxxK0RBQWtEOztpQkFFckQ7Ozs7Z0JBbEJRLHFCQUFxQjtnQkFOckIsV0FBVztnQkFPWCxzQkFBc0I7Z0JBR3RCLGdCQUFnQjs7OzJCQWlCcEIsS0FBSzsyQkFHTCxLQUFLOytCQUdMLEtBQUs7NkJBR0wsU0FBUyxTQUFDLGdCQUFnQixFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQzs7SUF3RC9DLGdDQUFDO0NBQUEsQUE5RUQsSUE4RUM7U0FuRVkseUJBQXlCOzs7SUFFbEMsNkNBQ2dDOztJQUVoQyw2Q0FDMEI7O0lBRTFCLGlEQUM2Qjs7SUFFN0IsK0NBQzBDOztJQUUxQyw4Q0FBa0I7O0lBQ2xCLCtDQUFtQjs7Ozs7SUFFbkIsK0NBQTRDOzs7OztJQUVoQywwREFBb0Q7Ozs7O0lBQ3BELGdEQUF3Qzs7Ozs7SUFDeEMsMkRBQXNEOzs7OztJQUN0RCw4Q0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cblxuLyohXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cbiAqXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4gKlxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuICpcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXG4gKi9cblxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25EZXN0cm95LCBPbkluaXQsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRGF0ZUFkYXB0ZXIsIE1BVF9EQVRFX0ZPUk1BVFMgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQgeyBEYXRldGltZUFkYXB0ZXIsIE1BVF9EQVRFVElNRV9GT1JNQVRTLCBNYXREYXRldGltZXBpY2tlciB9IGZyb20gJ0BtYXQtZGF0ZXRpbWVwaWNrZXIvY29yZSc7XG5pbXBvcnQgeyBNQVRfTU9NRU5UX0RBVEVUSU1FX0ZPUk1BVFMsIE1vbWVudERhdGV0aW1lQWRhcHRlciB9IGZyb20gJ0BtYXQtZGF0ZXRpbWVwaWNrZXIvbW9tZW50JztcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50LWVzNic7XG5pbXBvcnQgeyBNb21lbnQgfSBmcm9tICdtb21lbnQnO1xuaW1wb3J0IHsgQ2FyZFZpZXdEYXRlSXRlbU1vZGVsIH0gZnJvbSAnLi4vLi4vbW9kZWxzL2NhcmQtdmlldy1kYXRlaXRlbS5tb2RlbCc7XG5pbXBvcnQgeyBDYXJkVmlld1VwZGF0ZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9jYXJkLXZpZXctdXBkYXRlLnNlcnZpY2UnO1xuaW1wb3J0IHsgVXNlclByZWZlcmVuY2VzU2VydmljZSwgVXNlclByZWZlcmVuY2VWYWx1ZXMgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy91c2VyLXByZWZlcmVuY2VzLnNlcnZpY2UnO1xuaW1wb3J0IHsgTW9tZW50RGF0ZUFkYXB0ZXIgfSBmcm9tICcuLi8uLi8uLi91dGlscy9tb21lbnREYXRlQWRhcHRlcic7XG5pbXBvcnQgeyBNT01FTlRfREFURV9GT1JNQVRTIH0gZnJvbSAnLi4vLi4vLi4vdXRpbHMvbW9tZW50LWRhdGUtZm9ybWF0cy5tb2RlbCc7XG5pbXBvcnQgeyBBcHBDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vYXBwLWNvbmZpZy9hcHAtY29uZmlnLnNlcnZpY2UnO1xuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBwcm92aWRlcnM6IFtcbiAgICAgICAgeyBwcm92aWRlOiBEYXRlQWRhcHRlciwgdXNlQ2xhc3M6IE1vbWVudERhdGVBZGFwdGVyIH0sXG4gICAgICAgIHsgcHJvdmlkZTogTUFUX0RBVEVfRk9STUFUUywgdXNlVmFsdWU6IE1PTUVOVF9EQVRFX0ZPUk1BVFMgfSxcbiAgICAgICAgeyBwcm92aWRlOiBEYXRldGltZUFkYXB0ZXIsIHVzZUNsYXNzOiBNb21lbnREYXRldGltZUFkYXB0ZXIgfSxcbiAgICAgICAgeyBwcm92aWRlOiBNQVRfREFURVRJTUVfRk9STUFUUywgdXNlVmFsdWU6IE1BVF9NT01FTlRfREFURVRJTUVfRk9STUFUUyB9XG4gICAgXSxcbiAgICBzZWxlY3RvcjogJ2FkZi1jYXJkLXZpZXctZGF0ZWl0ZW0nLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9jYXJkLXZpZXctZGF0ZWl0ZW0uY29tcG9uZW50Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL2NhcmQtdmlldy1kYXRlaXRlbS5jb21wb25lbnQuc2NzcyddXG59KVxuZXhwb3J0IGNsYXNzIENhcmRWaWV3RGF0ZUl0ZW1Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XG5cbiAgICBASW5wdXQoKVxuICAgIHByb3BlcnR5OiBDYXJkVmlld0RhdGVJdGVtTW9kZWw7XG5cbiAgICBASW5wdXQoKVxuICAgIGVkaXRhYmxlOiBib29sZWFuID0gZmFsc2U7XG5cbiAgICBASW5wdXQoKVxuICAgIGRpc3BsYXlFbXB0eTogYm9vbGVhbiA9IHRydWU7XG5cbiAgICBAVmlld0NoaWxkKCdkYXRldGltZVBpY2tlcicsIHtzdGF0aWM6IHRydWV9KVxuICAgIHB1YmxpYyBkYXRlcGlja2VyOiBNYXREYXRldGltZXBpY2tlcjxhbnk+O1xuXG4gICAgdmFsdWVEYXRlOiBNb21lbnQ7XG4gICAgZGF0ZUZvcm1hdDogc3RyaW5nO1xuXG4gICAgcHJpdmF0ZSBvbkRlc3Ryb3kkID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgY2FyZFZpZXdVcGRhdGVTZXJ2aWNlOiBDYXJkVmlld1VwZGF0ZVNlcnZpY2UsXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBkYXRlQWRhcHRlcjogRGF0ZUFkYXB0ZXI8TW9tZW50PixcbiAgICAgICAgICAgICAgICBwcml2YXRlIHVzZXJQcmVmZXJlbmNlc1NlcnZpY2U6IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UsXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBhcHBDb25maWc6IEFwcENvbmZpZ1NlcnZpY2UpIHtcbiAgICAgICAgdGhpcy5kYXRlRm9ybWF0ID0gdGhpcy5hcHBDb25maWcuZ2V0KCdkYXRlVmFsdWVzLmRlZmF1bHREYXRlRm9ybWF0Jyk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VzU2VydmljZVxuICAgICAgICAgICAgLnNlbGVjdChVc2VyUHJlZmVyZW5jZVZhbHVlcy5Mb2NhbGUpXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5vbkRlc3Ryb3kkKSlcbiAgICAgICAgICAgIC5zdWJzY3JpYmUobG9jYWxlID0+IHRoaXMuZGF0ZUFkYXB0ZXIuc2V0TG9jYWxlKGxvY2FsZSkpO1xuXG4gICAgICAgICg8TW9tZW50RGF0ZUFkYXB0ZXI+IHRoaXMuZGF0ZUFkYXB0ZXIpLm92ZXJyaWRlRGlzcGxheUZvcm1hdCA9ICdNTU0gREQnO1xuXG4gICAgICAgIGlmICh0aGlzLnByb3BlcnR5LnZhbHVlKSB7XG4gICAgICAgICAgICB0aGlzLnZhbHVlRGF0ZSA9IG1vbWVudCh0aGlzLnByb3BlcnR5LnZhbHVlLCB0aGlzLmRhdGVGb3JtYXQpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgbmdPbkRlc3Ryb3koKSB7XG4gICAgICAgIHRoaXMub25EZXN0cm95JC5uZXh0KHRydWUpO1xuICAgICAgICB0aGlzLm9uRGVzdHJveSQuY29tcGxldGUoKTtcbiAgICB9XG5cbiAgICBzaG93UHJvcGVydHkoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmRpc3BsYXlFbXB0eSB8fCAhdGhpcy5wcm9wZXJ0eS5pc0VtcHR5KCk7XG4gICAgfVxuXG4gICAgaXNFZGl0YWJsZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZWRpdGFibGUgJiYgdGhpcy5wcm9wZXJ0eS5lZGl0YWJsZTtcbiAgICB9XG5cbiAgICBzaG93RGF0ZVBpY2tlcigpIHtcbiAgICAgICAgdGhpcy5kYXRlcGlja2VyLm9wZW4oKTtcbiAgICB9XG5cbiAgICBvbkRhdGVDaGFuZ2VkKG5ld0RhdGVWYWx1ZSkge1xuICAgICAgICBpZiAobmV3RGF0ZVZhbHVlKSB7XG4gICAgICAgICAgICBjb25zdCBtb21lbnREYXRlID0gbW9tZW50KG5ld0RhdGVWYWx1ZS52YWx1ZSwgdGhpcy5kYXRlRm9ybWF0LCB0cnVlKTtcbiAgICAgICAgICAgIGlmIChtb21lbnREYXRlLmlzVmFsaWQoKSkge1xuICAgICAgICAgICAgICAgIHRoaXMudmFsdWVEYXRlID0gbW9tZW50RGF0ZTtcbiAgICAgICAgICAgICAgICB0aGlzLmNhcmRWaWV3VXBkYXRlU2VydmljZS51cGRhdGUodGhpcy5wcm9wZXJ0eSwgbW9tZW50RGF0ZS50b0RhdGUoKSk7XG4gICAgICAgICAgICAgICAgdGhpcy5wcm9wZXJ0eS52YWx1ZSA9IG1vbWVudERhdGUudG9EYXRlKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbn1cbiJdfQ==