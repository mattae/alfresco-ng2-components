/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ComponentFactoryResolver, Input, ViewChild } from '@angular/core';
import { CardItemTypeService } from '../../services/card-item-types.service';
import { CardViewContentProxyDirective } from '../../directives/card-view-content-proxy.directive';
var CardViewItemDispatcherComponent = /** @class */ (function () {
    function CardViewItemDispatcherComponent(cardItemTypeService, resolver) {
        var _this = this;
        this.cardItemTypeService = cardItemTypeService;
        this.resolver = resolver;
        this.displayEmpty = true;
        this.loaded = false;
        this.componentReference = null;
        /** @type {?} */
        var dynamicLifeCycleMethods = [
            'ngOnInit',
            'ngDoCheck',
            'ngAfterContentInit',
            'ngAfterContentChecked',
            'ngAfterViewInit',
            'ngAfterViewChecked',
            'ngOnDestroy'
        ];
        dynamicLifeCycleMethods.forEach((/**
         * @param {?} method
         * @return {?}
         */
        function (method) {
            _this[method] = _this.proxy.bind(_this, method);
        }));
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    CardViewItemDispatcherComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        var _this = this;
        if (!this.loaded) {
            this.loadComponent();
            this.loaded = true;
        }
        Object.keys(changes)
            .map((/**
         * @param {?} changeName
         * @return {?}
         */
        function (changeName) { return [changeName, changes[changeName]]; }))
            .forEach((/**
         * @param {?} __0
         * @return {?}
         */
        function (_a) {
            var _b = tslib_1.__read(_a, 2), inputParamName = _b[0], simpleChange = _b[1];
            _this.componentReference.instance[inputParamName] = simpleChange.currentValue;
        }));
        this.proxy('ngOnChanges', changes);
    };
    /**
     * @private
     * @return {?}
     */
    CardViewItemDispatcherComponent.prototype.loadComponent = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var factoryClass = this.cardItemTypeService.resolveComponentType(this.property);
        /** @type {?} */
        var factory = this.resolver.resolveComponentFactory(factoryClass);
        this.componentReference = this.content.viewContainerRef.createComponent(factory);
        this.componentReference.instance.editable = this.editable;
        this.componentReference.instance.property = this.property;
        this.componentReference.instance.displayEmpty = this.displayEmpty;
    };
    /**
     * @private
     * @param {?} methodName
     * @param {...?} args
     * @return {?}
     */
    CardViewItemDispatcherComponent.prototype.proxy = /**
     * @private
     * @param {?} methodName
     * @param {...?} args
     * @return {?}
     */
    function (methodName) {
        var args = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            args[_i - 1] = arguments[_i];
        }
        if (this.componentReference.instance[methodName]) {
            this.componentReference.instance[methodName].apply(this.componentReference.instance, args);
        }
    };
    CardViewItemDispatcherComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-card-view-item-dispatcher',
                    template: '<ng-template adf-card-view-content-proxy></ng-template>'
                }] }
    ];
    /** @nocollapse */
    CardViewItemDispatcherComponent.ctorParameters = function () { return [
        { type: CardItemTypeService },
        { type: ComponentFactoryResolver }
    ]; };
    CardViewItemDispatcherComponent.propDecorators = {
        property: [{ type: Input }],
        editable: [{ type: Input }],
        displayEmpty: [{ type: Input }],
        content: [{ type: ViewChild, args: [CardViewContentProxyDirective, { static: true },] }]
    };
    return CardViewItemDispatcherComponent;
}());
export { CardViewItemDispatcherComponent };
if (false) {
    /** @type {?} */
    CardViewItemDispatcherComponent.prototype.property;
    /** @type {?} */
    CardViewItemDispatcherComponent.prototype.editable;
    /** @type {?} */
    CardViewItemDispatcherComponent.prototype.displayEmpty;
    /**
     * @type {?}
     * @private
     */
    CardViewItemDispatcherComponent.prototype.content;
    /**
     * @type {?}
     * @private
     */
    CardViewItemDispatcherComponent.prototype.loaded;
    /**
     * @type {?}
     * @private
     */
    CardViewItemDispatcherComponent.prototype.componentReference;
    /** @type {?} */
    CardViewItemDispatcherComponent.prototype.ngOnInit;
    /** @type {?} */
    CardViewItemDispatcherComponent.prototype.ngDoCheck;
    /**
     * @type {?}
     * @private
     */
    CardViewItemDispatcherComponent.prototype.cardItemTypeService;
    /**
     * @type {?}
     * @private
     */
    CardViewItemDispatcherComponent.prototype.resolver;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWl0ZW0tZGlzcGF0Y2hlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJjYXJkLXZpZXcvY29tcG9uZW50cy9jYXJkLXZpZXctaXRlbS1kaXNwYXRjaGVyL2NhcmQtdmlldy1pdGVtLWRpc3BhdGNoZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQ0EsT0FBTyxFQUNILFNBQVMsRUFDVCx3QkFBd0IsRUFDeEIsS0FBSyxFQUlMLFNBQVMsRUFDWixNQUFNLGVBQWUsQ0FBQztBQUV2QixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUM3RSxPQUFPLEVBQUUsNkJBQTZCLEVBQUUsTUFBTSxvREFBb0QsQ0FBQztBQUVuRztJQXVCSSx5Q0FBb0IsbUJBQXdDLEVBQ3hDLFFBQWtDO1FBRHRELGlCQWVDO1FBZm1CLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFDeEMsYUFBUSxHQUFSLFFBQVEsQ0FBMEI7UUFadEQsaUJBQVksR0FBWSxJQUFJLENBQUM7UUFLckIsV0FBTSxHQUFZLEtBQUssQ0FBQztRQUN4Qix1QkFBa0IsR0FBUSxJQUFJLENBQUM7O1lBTzdCLHVCQUF1QixHQUFHO1lBQzVCLFVBQVU7WUFDVixXQUFXO1lBQ1gsb0JBQW9CO1lBQ3BCLHVCQUF1QjtZQUN2QixpQkFBaUI7WUFDakIsb0JBQW9CO1lBQ3BCLGFBQWE7U0FDaEI7UUFFRCx1QkFBdUIsQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQyxNQUFNO1lBQ25DLEtBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFJLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDakQsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELHFEQUFXOzs7O0lBQVgsVUFBWSxPQUFzQjtRQUFsQyxpQkFhQztRQVpHLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2QsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ3JCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1NBQ3RCO1FBRUQsTUFBTSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7YUFDZixHQUFHOzs7O1FBQUMsVUFBQyxVQUFVLElBQUssT0FBQSxDQUFDLFVBQVUsRUFBRSxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUMsRUFBakMsQ0FBaUMsRUFBQzthQUN0RCxPQUFPOzs7O1FBQUMsVUFBQyxFQUFzRDtnQkFBdEQsMEJBQXNELEVBQXJELHNCQUFjLEVBQUUsb0JBQVk7WUFDbkMsS0FBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxjQUFjLENBQUMsR0FBRyxZQUFZLENBQUMsWUFBWSxDQUFDO1FBQ2pGLENBQUMsRUFBQyxDQUFDO1FBRVAsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDdkMsQ0FBQzs7Ozs7SUFFTyx1REFBYTs7OztJQUFyQjs7WUFDVSxZQUFZLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLG9CQUFvQixDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7O1lBRTNFLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLHVCQUF1QixDQUFDLFlBQVksQ0FBQztRQUNuRSxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxlQUFlLENBQUMsT0FBTyxDQUFDLENBQUM7UUFFakYsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUMxRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQzFELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDdEUsQ0FBQzs7Ozs7OztJQUVPLCtDQUFLOzs7Ozs7SUFBYixVQUFjLFVBQVU7UUFBRSxjQUFPO2FBQVAsVUFBTyxFQUFQLHFCQUFPLEVBQVAsSUFBTztZQUFQLDZCQUFPOztRQUM3QixJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxRQUFRLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDOUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUM5RjtJQUNMLENBQUM7O2dCQXRFSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLCtCQUErQjtvQkFDekMsUUFBUSxFQUFFLHlEQUF5RDtpQkFDdEU7Ozs7Z0JBTlEsbUJBQW1CO2dCQVJ4Qix3QkFBd0I7OzsyQkFnQnZCLEtBQUs7MkJBR0wsS0FBSzsrQkFHTCxLQUFLOzBCQUdMLFNBQVMsU0FBQyw2QkFBNkIsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7O0lBeUQ1RCxzQ0FBQztDQUFBLEFBdkVELElBdUVDO1NBbkVZLCtCQUErQjs7O0lBQ3hDLG1EQUN1Qjs7SUFFdkIsbURBQ2tCOztJQUVsQix1REFDNkI7Ozs7O0lBRTdCLGtEQUMrQzs7Ozs7SUFFL0MsaURBQWdDOzs7OztJQUNoQyw2REFBdUM7O0lBRXZDLG1EQUFnQjs7SUFDaEIsb0RBQWlCOzs7OztJQUVMLDhEQUFnRDs7Ozs7SUFDaEQsbURBQTBDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXG5cbi8qIVxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXG4gKlxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuICpcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbiAqXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxuICovXG5cbmltcG9ydCB7XG4gICAgQ29tcG9uZW50LFxuICAgIENvbXBvbmVudEZhY3RvcnlSZXNvbHZlcixcbiAgICBJbnB1dCxcbiAgICBPbkNoYW5nZXMsXG4gICAgU2ltcGxlQ2hhbmdlLFxuICAgIFNpbXBsZUNoYW5nZXMsXG4gICAgVmlld0NoaWxkXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgQ2FyZFZpZXdJdGVtIH0gZnJvbSAnLi4vLi4vaW50ZXJmYWNlcy9jYXJkLXZpZXctaXRlbS5pbnRlcmZhY2UnO1xuaW1wb3J0IHsgQ2FyZEl0ZW1UeXBlU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2NhcmQtaXRlbS10eXBlcy5zZXJ2aWNlJztcbmltcG9ydCB7IENhcmRWaWV3Q29udGVudFByb3h5RGlyZWN0aXZlIH0gZnJvbSAnLi4vLi4vZGlyZWN0aXZlcy9jYXJkLXZpZXctY29udGVudC1wcm94eS5kaXJlY3RpdmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2FkZi1jYXJkLXZpZXctaXRlbS1kaXNwYXRjaGVyJyxcbiAgICB0ZW1wbGF0ZTogJzxuZy10ZW1wbGF0ZSBhZGYtY2FyZC12aWV3LWNvbnRlbnQtcHJveHk+PC9uZy10ZW1wbGF0ZT4nXG59KVxuZXhwb3J0IGNsYXNzIENhcmRWaWV3SXRlbURpc3BhdGNoZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkNoYW5nZXMge1xuICAgIEBJbnB1dCgpXG4gICAgcHJvcGVydHk6IENhcmRWaWV3SXRlbTtcblxuICAgIEBJbnB1dCgpXG4gICAgZWRpdGFibGU6IGJvb2xlYW47XG5cbiAgICBASW5wdXQoKVxuICAgIGRpc3BsYXlFbXB0eTogYm9vbGVhbiA9IHRydWU7XG5cbiAgICBAVmlld0NoaWxkKENhcmRWaWV3Q29udGVudFByb3h5RGlyZWN0aXZlLCB7c3RhdGljOiB0cnVlfSlcbiAgICBwcml2YXRlIGNvbnRlbnQ6IENhcmRWaWV3Q29udGVudFByb3h5RGlyZWN0aXZlO1xuXG4gICAgcHJpdmF0ZSBsb2FkZWQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgICBwcml2YXRlIGNvbXBvbmVudFJlZmVyZW5jZTogYW55ID0gbnVsbDtcblxuICAgIHB1YmxpYyBuZ09uSW5pdDtcbiAgICBwdWJsaWMgbmdEb0NoZWNrO1xuXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBjYXJkSXRlbVR5cGVTZXJ2aWNlOiBDYXJkSXRlbVR5cGVTZXJ2aWNlLFxuICAgICAgICAgICAgICAgIHByaXZhdGUgcmVzb2x2ZXI6IENvbXBvbmVudEZhY3RvcnlSZXNvbHZlcikge1xuICAgICAgICBjb25zdCBkeW5hbWljTGlmZUN5Y2xlTWV0aG9kcyA9IFtcbiAgICAgICAgICAgICduZ09uSW5pdCcsXG4gICAgICAgICAgICAnbmdEb0NoZWNrJyxcbiAgICAgICAgICAgICduZ0FmdGVyQ29udGVudEluaXQnLFxuICAgICAgICAgICAgJ25nQWZ0ZXJDb250ZW50Q2hlY2tlZCcsXG4gICAgICAgICAgICAnbmdBZnRlclZpZXdJbml0JyxcbiAgICAgICAgICAgICduZ0FmdGVyVmlld0NoZWNrZWQnLFxuICAgICAgICAgICAgJ25nT25EZXN0cm95J1xuICAgICAgICBdO1xuXG4gICAgICAgIGR5bmFtaWNMaWZlQ3ljbGVNZXRob2RzLmZvckVhY2goKG1ldGhvZCkgPT4ge1xuICAgICAgICAgICAgdGhpc1ttZXRob2RdID0gdGhpcy5wcm94eS5iaW5kKHRoaXMsIG1ldGhvZCk7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcbiAgICAgICAgaWYgKCF0aGlzLmxvYWRlZCkge1xuICAgICAgICAgICAgdGhpcy5sb2FkQ29tcG9uZW50KCk7XG4gICAgICAgICAgICB0aGlzLmxvYWRlZCA9IHRydWU7XG4gICAgICAgIH1cblxuICAgICAgICBPYmplY3Qua2V5cyhjaGFuZ2VzKVxuICAgICAgICAgICAgLm1hcCgoY2hhbmdlTmFtZSkgPT4gW2NoYW5nZU5hbWUsIGNoYW5nZXNbY2hhbmdlTmFtZV1dKVxuICAgICAgICAgICAgLmZvckVhY2goKFtpbnB1dFBhcmFtTmFtZSwgc2ltcGxlQ2hhbmdlXTogW3N0cmluZywgU2ltcGxlQ2hhbmdlXSkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuY29tcG9uZW50UmVmZXJlbmNlLmluc3RhbmNlW2lucHV0UGFyYW1OYW1lXSA9IHNpbXBsZUNoYW5nZS5jdXJyZW50VmFsdWU7XG4gICAgICAgICAgICB9KTtcblxuICAgICAgICB0aGlzLnByb3h5KCduZ09uQ2hhbmdlcycsIGNoYW5nZXMpO1xuICAgIH1cblxuICAgIHByaXZhdGUgbG9hZENvbXBvbmVudCgpIHtcbiAgICAgICAgY29uc3QgZmFjdG9yeUNsYXNzID0gdGhpcy5jYXJkSXRlbVR5cGVTZXJ2aWNlLnJlc29sdmVDb21wb25lbnRUeXBlKHRoaXMucHJvcGVydHkpO1xuXG4gICAgICAgIGNvbnN0IGZhY3RvcnkgPSB0aGlzLnJlc29sdmVyLnJlc29sdmVDb21wb25lbnRGYWN0b3J5KGZhY3RvcnlDbGFzcyk7XG4gICAgICAgIHRoaXMuY29tcG9uZW50UmVmZXJlbmNlID0gdGhpcy5jb250ZW50LnZpZXdDb250YWluZXJSZWYuY3JlYXRlQ29tcG9uZW50KGZhY3RvcnkpO1xuXG4gICAgICAgIHRoaXMuY29tcG9uZW50UmVmZXJlbmNlLmluc3RhbmNlLmVkaXRhYmxlID0gdGhpcy5lZGl0YWJsZTtcbiAgICAgICAgdGhpcy5jb21wb25lbnRSZWZlcmVuY2UuaW5zdGFuY2UucHJvcGVydHkgPSB0aGlzLnByb3BlcnR5O1xuICAgICAgICB0aGlzLmNvbXBvbmVudFJlZmVyZW5jZS5pbnN0YW5jZS5kaXNwbGF5RW1wdHkgPSB0aGlzLmRpc3BsYXlFbXB0eTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHByb3h5KG1ldGhvZE5hbWUsIC4uLmFyZ3MpIHtcbiAgICAgICAgaWYgKHRoaXMuY29tcG9uZW50UmVmZXJlbmNlLmluc3RhbmNlW21ldGhvZE5hbWVdKSB7XG4gICAgICAgICAgICB0aGlzLmNvbXBvbmVudFJlZmVyZW5jZS5pbnN0YW5jZVttZXRob2ROYW1lXS5hcHBseSh0aGlzLmNvbXBvbmVudFJlZmVyZW5jZS5pbnN0YW5jZSwgYXJncyk7XG4gICAgICAgIH1cbiAgICB9XG59XG4iXX0=