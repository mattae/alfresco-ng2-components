/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input } from '@angular/core';
import { CardViewMapItemModel } from '../../models/card-view-mapitem.model';
import { CardViewUpdateService } from '../../services/card-view-update.service';
var CardViewMapItemComponent = /** @class */ (function () {
    function CardViewMapItemComponent(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.displayEmpty = true;
    }
    /**
     * @return {?}
     */
    CardViewMapItemComponent.prototype.showProperty = /**
     * @return {?}
     */
    function () {
        return this.displayEmpty || !this.property.isEmpty();
    };
    /**
     * @return {?}
     */
    CardViewMapItemComponent.prototype.isClickable = /**
     * @return {?}
     */
    function () {
        return this.property.clickable;
    };
    /**
     * @return {?}
     */
    CardViewMapItemComponent.prototype.clicked = /**
     * @return {?}
     */
    function () {
        this.cardViewUpdateService.clicked(this.property);
    };
    CardViewMapItemComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-card-view-mapitem',
                    template: "<div [attr.data-automation-id]=\"'card-mapitem-label-' + property.key\" class=\"adf-property-label\" *ngIf=\"showProperty()\">{{ property.label | translate }}</div>\r\n<div class=\"adf-property-value\">\r\n    <div>\r\n        <span *ngIf=\"!isClickable(); else elseBlock\" [attr.data-automation-id]=\"'card-mapitem-value-' + property.key\">\r\n            <span *ngIf=\"showProperty();\">{{ property.displayValue }}</span>\r\n        </span>\r\n        <ng-template #elseBlock>\r\n            <span class=\"adf-mapitem-clickable-value\" (click)=\"clicked()\" [attr.data-automation-id]=\"'card-mapitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ property.displayValue }}</span>\r\n            </span>\r\n        </ng-template>\r\n    </div>\r\n    <ng-template #elseEmptyValueBlock>\r\n        {{ property.default | translate }}\r\n    </ng-template>\r\n</div>\r\n",
                    styles: [".adf-mapitem-clickable-value{cursor:pointer!important}"]
                }] }
    ];
    /** @nocollapse */
    CardViewMapItemComponent.ctorParameters = function () { return [
        { type: CardViewUpdateService }
    ]; };
    CardViewMapItemComponent.propDecorators = {
        property: [{ type: Input }],
        displayEmpty: [{ type: Input }]
    };
    return CardViewMapItemComponent;
}());
export { CardViewMapItemComponent };
if (false) {
    /** @type {?} */
    CardViewMapItemComponent.prototype.property;
    /** @type {?} */
    CardViewMapItemComponent.prototype.displayEmpty;
    /**
     * @type {?}
     * @private
     */
    CardViewMapItemComponent.prototype.cardViewUpdateService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LW1hcGl0ZW0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiY2FyZC12aWV3L2NvbXBvbmVudHMvY2FyZC12aWV3LW1hcGl0ZW0vY2FyZC12aWV3LW1hcGl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2pELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBRWhGO0lBYUksa0NBQW9CLHFCQUE0QztRQUE1QywwQkFBcUIsR0FBckIscUJBQXFCLENBQXVCO1FBRmhFLGlCQUFZLEdBQVksSUFBSSxDQUFDO0lBRXNDLENBQUM7Ozs7SUFFcEUsK0NBQVk7OztJQUFaO1FBQ0ksT0FBTyxJQUFJLENBQUMsWUFBWSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUN6RCxDQUFDOzs7O0lBRUQsOENBQVc7OztJQUFYO1FBQ0ksT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQztJQUNuQyxDQUFDOzs7O0lBRUQsMENBQU87OztJQUFQO1FBQ0ksSUFBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDdEQsQ0FBQzs7Z0JBekJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsdUJBQXVCO29CQUNqQyx5NkJBQWlEOztpQkFFcEQ7Ozs7Z0JBTlEscUJBQXFCOzs7MkJBU3pCLEtBQUs7K0JBR0wsS0FBSzs7SUFnQlYsK0JBQUM7Q0FBQSxBQTFCRCxJQTBCQztTQXBCWSx3QkFBd0I7OztJQUNqQyw0Q0FDK0I7O0lBRS9CLGdEQUM2Qjs7Ozs7SUFFakIseURBQW9EIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdNYXBJdGVtTW9kZWwgfSBmcm9tICcuLi8uLi9tb2RlbHMvY2FyZC12aWV3LW1hcGl0ZW0ubW9kZWwnO1xyXG5pbXBvcnQgeyBDYXJkVmlld1VwZGF0ZVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9jYXJkLXZpZXctdXBkYXRlLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1jYXJkLXZpZXctbWFwaXRlbScsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vY2FyZC12aWV3LW1hcGl0ZW0uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vY2FyZC12aWV3LW1hcGl0ZW0uY29tcG9uZW50LnNjc3MnXVxyXG59KVxyXG5cclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3TWFwSXRlbUNvbXBvbmVudCB7XHJcbiAgICBASW5wdXQoKVxyXG4gICAgcHJvcGVydHk6IENhcmRWaWV3TWFwSXRlbU1vZGVsO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBkaXNwbGF5RW1wdHk6IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgY2FyZFZpZXdVcGRhdGVTZXJ2aWNlOiBDYXJkVmlld1VwZGF0ZVNlcnZpY2UpIHt9XHJcblxyXG4gICAgc2hvd1Byb3BlcnR5KCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmRpc3BsYXlFbXB0eSB8fCAhdGhpcy5wcm9wZXJ0eS5pc0VtcHR5KCk7XHJcbiAgICB9XHJcblxyXG4gICAgaXNDbGlja2FibGUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMucHJvcGVydHkuY2xpY2thYmxlO1xyXG4gICAgfVxyXG5cclxuICAgIGNsaWNrZWQoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jYXJkVmlld1VwZGF0ZVNlcnZpY2UuY2xpY2tlZCh0aGlzLnByb3BlcnR5KTtcclxuICAgIH1cclxufVxyXG4iXX0=