/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input } from '@angular/core';
import { CardViewSelectItemModel } from '../../models/card-view-selectitem.model';
import { CardViewUpdateService } from '../../services/card-view-update.service';
import { Observable } from 'rxjs';
var CardViewSelectItemComponent = /** @class */ (function () {
    function CardViewSelectItemComponent(cardViewUpdateService) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.editable = false;
    }
    /**
     * @return {?}
     */
    CardViewSelectItemComponent.prototype.ngOnChanges = /**
     * @return {?}
     */
    function () {
        this.value = this.property.value;
    };
    /**
     * @return {?}
     */
    CardViewSelectItemComponent.prototype.isEditable = /**
     * @return {?}
     */
    function () {
        return this.editable && this.property.editable;
    };
    /**
     * @return {?}
     */
    CardViewSelectItemComponent.prototype.getOptions = /**
     * @return {?}
     */
    function () {
        return this.options$ || this.property.options$;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    CardViewSelectItemComponent.prototype.onChange = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.cardViewUpdateService.update(this.property, event.value);
        this.property.value = event.value;
    };
    CardViewSelectItemComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-card-view-selectitem',
                    template: "<div [attr.data-automation-id]=\"'card-select-label-' + property.key\" class=\"adf-property-label\">{{ property.label | translate }}</div>\r\n<div class=\"adf-property-value\">\r\n    <div *ngIf=\"!isEditable()\" data-automation-class=\"read-only-value\">{{ property.displayValue | async }}</div>\r\n    <div *ngIf=\"isEditable()\">\r\n        <mat-form-field>\r\n            <mat-select [(value)]=\"value\" (selectionChange)=\"onChange($event)\" data-automation-class=\"select-box\">\r\n              <mat-option *ngFor=\"let option of getOptions() | async\" [value]=\"option.key\">\r\n                {{ option.label | translate }}\r\n              </mat-option>\r\n            </mat-select>\r\n          </mat-form-field>\r\n    </div>\r\n</div>\r\n",
                    styles: [".mat-form-field-type-mat-select{width:100%}"]
                }] }
    ];
    /** @nocollapse */
    CardViewSelectItemComponent.ctorParameters = function () { return [
        { type: CardViewUpdateService }
    ]; };
    CardViewSelectItemComponent.propDecorators = {
        property: [{ type: Input }],
        editable: [{ type: Input }],
        options$: [{ type: Input }]
    };
    return CardViewSelectItemComponent;
}());
export { CardViewSelectItemComponent };
if (false) {
    /** @type {?} */
    CardViewSelectItemComponent.prototype.property;
    /** @type {?} */
    CardViewSelectItemComponent.prototype.editable;
    /** @type {?} */
    CardViewSelectItemComponent.prototype.options$;
    /** @type {?} */
    CardViewSelectItemComponent.prototype.value;
    /**
     * @type {?}
     * @private
     */
    CardViewSelectItemComponent.prototype.cardViewUpdateService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LXNlbGVjdGl0ZW0uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiY2FyZC12aWV3L2NvbXBvbmVudHMvY2FyZC12aWV3LXNlbGVjdGl0ZW0vY2FyZC12aWV3LXNlbGVjdGl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFhLE1BQU0sZUFBZSxDQUFDO0FBQzVELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2xGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHlDQUF5QyxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFJbEM7SUFjSSxxQ0FBb0IscUJBQTRDO1FBQTVDLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBdUI7UUFOdkQsYUFBUSxHQUFZLEtBQUssQ0FBQztJQU1nQyxDQUFDOzs7O0lBRXBFLGlEQUFXOzs7SUFBWDtRQUNJLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUM7SUFDckMsQ0FBQzs7OztJQUVELGdEQUFVOzs7SUFBVjtRQUNJLE9BQU8sSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLFFBQVEsQ0FBQztJQUNuRCxDQUFDOzs7O0lBRUQsZ0RBQVU7OztJQUFWO1FBQ0ksT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO0lBQ25ELENBQUM7Ozs7O0lBRUQsOENBQVE7Ozs7SUFBUixVQUFTLEtBQXNCO1FBQzNCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDOUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztJQUN0QyxDQUFDOztnQkEvQkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSwwQkFBMEI7b0JBQ3BDLDR2QkFBb0Q7O2lCQUV2RDs7OztnQkFUUSxxQkFBcUI7OzsyQkFXekIsS0FBSzsyQkFFTCxLQUFLOzJCQUVMLEtBQUs7O0lBc0JWLGtDQUFDO0NBQUEsQUFoQ0QsSUFnQ0M7U0EzQlksMkJBQTJCOzs7SUFDcEMsK0NBQW1EOztJQUVuRCwrQ0FBbUM7O0lBRW5DLCtDQUFrRTs7SUFFbEUsNENBQWM7Ozs7O0lBRUYsNERBQW9EIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDYXJkVmlld1NlbGVjdEl0ZW1Nb2RlbCB9IGZyb20gJy4uLy4uL21vZGVscy9jYXJkLXZpZXctc2VsZWN0aXRlbS5tb2RlbCc7XHJcbmltcG9ydCB7IENhcmRWaWV3VXBkYXRlU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2NhcmQtdmlldy11cGRhdGUuc2VydmljZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdTZWxlY3RJdGVtT3B0aW9uIH0gZnJvbSAnLi4vLi4vaW50ZXJmYWNlcy9jYXJkLXZpZXcuaW50ZXJmYWNlcyc7XHJcbmltcG9ydCB7IE1hdFNlbGVjdENoYW5nZSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtY2FyZC12aWV3LXNlbGVjdGl0ZW0nLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2NhcmQtdmlldy1zZWxlY3RpdGVtLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2NhcmQtdmlldy1zZWxlY3RpdGVtLmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3U2VsZWN0SXRlbUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XHJcbiAgICBASW5wdXQoKSBwcm9wZXJ0eTogQ2FyZFZpZXdTZWxlY3RJdGVtTW9kZWw8c3RyaW5nPjtcclxuXHJcbiAgICBASW5wdXQoKSBlZGl0YWJsZTogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIEBJbnB1dCgpIG9wdGlvbnMkOiBPYnNlcnZhYmxlPENhcmRWaWV3U2VsZWN0SXRlbU9wdGlvbjxzdHJpbmc+W10+O1xyXG5cclxuICAgIHZhbHVlOiBzdHJpbmc7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBjYXJkVmlld1VwZGF0ZVNlcnZpY2U6IENhcmRWaWV3VXBkYXRlU2VydmljZSkge31cclxuXHJcbiAgICBuZ09uQ2hhbmdlcygpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnZhbHVlID0gdGhpcy5wcm9wZXJ0eS52YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBpc0VkaXRhYmxlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmVkaXRhYmxlICYmIHRoaXMucHJvcGVydHkuZWRpdGFibGU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0T3B0aW9ucygpOiBPYnNlcnZhYmxlPENhcmRWaWV3U2VsZWN0SXRlbU9wdGlvbjxzdHJpbmc+W10+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5vcHRpb25zJCB8fCB0aGlzLnByb3BlcnR5Lm9wdGlvbnMkO1xyXG4gICAgfVxyXG5cclxuICAgIG9uQ2hhbmdlKGV2ZW50OiBNYXRTZWxlY3RDaGFuZ2UpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmNhcmRWaWV3VXBkYXRlU2VydmljZS51cGRhdGUodGhpcy5wcm9wZXJ0eSwgZXZlbnQudmFsdWUpO1xyXG4gICAgICAgIHRoaXMucHJvcGVydHkudmFsdWUgPSBldmVudC52YWx1ZTtcclxuICAgIH1cclxufVxyXG4iXX0=