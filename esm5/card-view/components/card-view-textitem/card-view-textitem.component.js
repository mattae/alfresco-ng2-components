/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewChild } from '@angular/core';
import { CardViewTextItemModel } from '../../models/card-view-textitem.model';
import { CardViewUpdateService } from '../../services/card-view-update.service';
import { AppConfigService } from '../../../app-config/app-config.service';
var CardViewTextItemComponent = /** @class */ (function () {
    function CardViewTextItemComponent(cardViewUpdateService, appConfig) {
        this.cardViewUpdateService = cardViewUpdateService;
        this.appConfig = appConfig;
        this.editable = false;
        this.displayEmpty = true;
        this.inEdit = false;
        this.valueSeparator = this.appConfig.get('content-metadata.multi-value-pipe-separator') || CardViewTextItemComponent.DEFAULT_SEPARATOR;
    }
    /**
     * @return {?}
     */
    CardViewTextItemComponent.prototype.ngOnChanges = /**
     * @return {?}
     */
    function () {
        this.editedValue = this.property.multiline ? this.property.displayValue : this.property.value;
    };
    /**
     * @return {?}
     */
    CardViewTextItemComponent.prototype.showProperty = /**
     * @return {?}
     */
    function () {
        return this.displayEmpty || !this.property.isEmpty();
    };
    /**
     * @return {?}
     */
    CardViewTextItemComponent.prototype.isEditable = /**
     * @return {?}
     */
    function () {
        return this.editable && this.property.editable;
    };
    /**
     * @return {?}
     */
    CardViewTextItemComponent.prototype.isClickable = /**
     * @return {?}
     */
    function () {
        return !!this.property.clickable;
    };
    /**
     * @return {?}
     */
    CardViewTextItemComponent.prototype.hasIcon = /**
     * @return {?}
     */
    function () {
        return !!this.property.icon;
    };
    /**
     * @return {?}
     */
    CardViewTextItemComponent.prototype.hasErrors = /**
     * @return {?}
     */
    function () {
        return this.errorMessages && this.errorMessages.length > 0;
    };
    /**
     * @param {?} editStatus
     * @return {?}
     */
    CardViewTextItemComponent.prototype.setEditMode = /**
     * @param {?} editStatus
     * @return {?}
     */
    function (editStatus) {
        var _this = this;
        this.inEdit = editStatus;
        setTimeout((/**
         * @return {?}
         */
        function () {
            if (_this.editorInput) {
                _this.editorInput.nativeElement.click();
            }
        }), 0);
    };
    /**
     * @return {?}
     */
    CardViewTextItemComponent.prototype.reset = /**
     * @return {?}
     */
    function () {
        this.editedValue = this.property.multiline ? this.property.displayValue : this.property.value;
        this.setEditMode(false);
        this.resetErrorMessages();
    };
    /**
     * @private
     * @return {?}
     */
    CardViewTextItemComponent.prototype.resetErrorMessages = /**
     * @private
     * @return {?}
     */
    function () {
        this.errorMessages = [];
    };
    /**
     * @return {?}
     */
    CardViewTextItemComponent.prototype.update = /**
     * @return {?}
     */
    function () {
        if (this.property.isValid(this.editedValue)) {
            /** @type {?} */
            var updatedValue = this.prepareValueForUpload(this.property, this.editedValue);
            this.cardViewUpdateService.update(this.property, updatedValue);
            this.property.value = updatedValue;
            this.setEditMode(false);
            this.resetErrorMessages();
        }
        else {
            this.errorMessages = this.property.getValidationErrors(this.editedValue);
        }
    };
    /**
     * @param {?} property
     * @param {?} value
     * @return {?}
     */
    CardViewTextItemComponent.prototype.prepareValueForUpload = /**
     * @param {?} property
     * @param {?} value
     * @return {?}
     */
    function (property, value) {
        if (property.multivalued) {
            /** @type {?} */
            var listOfValues = value.split(this.valueSeparator.trim()).map((/**
             * @param {?} item
             * @return {?}
             */
            function (item) { return item.trim(); }));
            return listOfValues;
        }
        return value;
    };
    /**
     * @return {?}
     */
    CardViewTextItemComponent.prototype.onTextAreaInputChange = /**
     * @return {?}
     */
    function () {
        this.errorMessages = this.property.getValidationErrors(this.editedValue);
    };
    /**
     * @return {?}
     */
    CardViewTextItemComponent.prototype.clicked = /**
     * @return {?}
     */
    function () {
        if (typeof this.property.clickCallBack === 'function') {
            this.property.clickCallBack();
        }
        else {
            this.cardViewUpdateService.clicked(this.property);
        }
    };
    CardViewTextItemComponent.DEFAULT_SEPARATOR = ', ';
    CardViewTextItemComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-card-view-textitem',
                    template: "<div [attr.data-automation-id]=\"'card-textitem-label-' + property.key\" class=\"adf-property-label\" *ngIf=\"showProperty() || isEditable()\">{{ property.label | translate }}</div>\r\n<div class=\"adf-property-value\">\r\n    <span *ngIf=\"!isEditable()\">\r\n        <span *ngIf=\"!isClickable(); else elseBlock\" [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n            <span *ngIf=\"showProperty()\" class=\"adf-textitem-ellipsis\">{{ property.displayValue }}</span>\r\n        </span>\r\n        <ng-template #elseBlock>\r\n            <div class=\"adf-textitem-clickable\" (click)=\"clicked()\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n                <span class=\"adf-textitem-clickable-value\" [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                    <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ property.displayValue }}</span>\r\n                </span>\r\n            </div>\r\n        </ng-template>\r\n    </span>\r\n    <span *ngIf=\"isEditable()\">\r\n        <div *ngIf=\"!inEdit\" (click)=\"setEditMode(true)\" class=\"adf-textitem-readonly\" [attr.data-automation-id]=\"'card-textitem-edit-toggle-' + property.key\" fxLayout=\"row\" fxLayoutAlign=\"space-between center\">\r\n            <span [attr.data-automation-id]=\"'card-textitem-value-' + property.key\">\r\n                <span *ngIf=\"showProperty(); else elseEmptyValueBlock\">{{ property.displayValue }}</span>\r\n            </span>\r\n            <mat-icon fxFlex=\"0 0 auto\"\r\n                [attr.data-automation-id]=\"'card-textitem-edit-icon-' + property.key\"\r\n                [attr.title]=\"'CORE.METADATA.ACTIONS.EDIT' | translate\"\r\n                class=\"adf-textitem-icon\">create</mat-icon>\r\n        </div>\r\n        <div *ngIf=\"inEdit\" class=\"adf-textitem-editable\">\r\n            <div class=\"adf-textitem-editable-controls\">\r\n                <mat-form-field floatPlaceholder=\"never\" class=\"adf-input-container\">\r\n                    <input *ngIf=\"!property.multiline\" #editorInput\r\n                        matInput\r\n                        class=\"adf-input\"\r\n                        [placeholder]=\"property.default | translate\"\r\n                        [(ngModel)]=\"editedValue\"\r\n                        [attr.data-automation-id]=\"'card-textitem-editinput-' + property.key\">\r\n                    <textarea *ngIf=\"property.multiline\" #editorInput\r\n                        matInput\r\n                        matTextareaAutosize\r\n                        matAutosizeMaxRows=\"1\"\r\n                        matAutosizeMaxRows=\"5\"\r\n                        class=\"adf-textarea\"\r\n                        [placeholder]=\"property.default | translate\"\r\n                        [(ngModel)]=\"editedValue\"\r\n                        (input)=\"onTextAreaInputChange()\"\r\n                        [attr.data-automation-id]=\"'card-textitem-edittextarea-' + property.key\"></textarea>\r\n                </mat-form-field>\r\n                <mat-icon\r\n                    [ngClass]=\"{'disable': hasErrors()}\"\r\n                    (click)=\"update()\"\r\n                    [attr.data-automation-id]=\"'card-textitem-update-' + property.key\"\r\n                    class=\"adf-textitem-icon adf-update-icon\"\r\n                    [class.adf-button-disabled]=\"hasErrors()\"\r\n                    [attr.title]=\"'CORE.METADATA.ACTIONS.SAVE' | translate\">done</mat-icon>\r\n                <mat-icon\r\n                    class=\"adf-textitem-icon adf-reset-icon\"\r\n                    (click)=\"reset()\"\r\n                    [attr.title]=\"'CORE.METADATA.ACTIONS.CANCEL' | translate\"\r\n                    [attr.data-automation-id]=\"'card-textitem-reset-' + property.key\">clear</mat-icon>\r\n\r\n            </div>\r\n            <mat-error  [attr.data-automation-id]=\"'card-textitem-error-' + property.key\" class=\"adf-textitem-editable-error\" *ngIf=\"hasErrors()\">\r\n                <ul>\r\n                    <li *ngFor=\"let errorMessage of errorMessages\">{{ errorMessage | translate }}</li>\r\n                </ul>\r\n            </mat-error>\r\n        </div>\r\n    </span>\r\n    <ng-template #elseEmptyValueBlock>\r\n        <span class=\"adf-textitem-default-value\">{{ property.default | translate }}</span>\r\n    </ng-template>\r\n</div>\r\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    CardViewTextItemComponent.ctorParameters = function () { return [
        { type: CardViewUpdateService },
        { type: AppConfigService }
    ]; };
    CardViewTextItemComponent.propDecorators = {
        property: [{ type: Input }],
        editable: [{ type: Input }],
        displayEmpty: [{ type: Input }],
        editorInput: [{ type: ViewChild, args: ['editorInput', { static: true },] }]
    };
    return CardViewTextItemComponent;
}());
export { CardViewTextItemComponent };
if (false) {
    /** @type {?} */
    CardViewTextItemComponent.DEFAULT_SEPARATOR;
    /** @type {?} */
    CardViewTextItemComponent.prototype.property;
    /** @type {?} */
    CardViewTextItemComponent.prototype.editable;
    /** @type {?} */
    CardViewTextItemComponent.prototype.displayEmpty;
    /**
     * @type {?}
     * @private
     */
    CardViewTextItemComponent.prototype.editorInput;
    /** @type {?} */
    CardViewTextItemComponent.prototype.inEdit;
    /** @type {?} */
    CardViewTextItemComponent.prototype.editedValue;
    /** @type {?} */
    CardViewTextItemComponent.prototype.errorMessages;
    /** @type {?} */
    CardViewTextItemComponent.prototype.valueSeparator;
    /**
     * @type {?}
     * @private
     */
    CardViewTextItemComponent.prototype.cardViewUpdateService;
    /**
     * @type {?}
     * @private
     */
    CardViewTextItemComponent.prototype.appConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LXRleHRpdGVtLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNhcmQtdmlldy9jb21wb25lbnRzL2NhcmQtdmlldy10ZXh0aXRlbS9jYXJkLXZpZXctdGV4dGl0ZW0uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBYSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDOUUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDaEYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFFMUU7SUEwQkksbUNBQW9CLHFCQUE0QyxFQUM1QyxTQUEyQjtRQUQzQiwwQkFBcUIsR0FBckIscUJBQXFCLENBQXVCO1FBQzVDLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBZC9DLGFBQVEsR0FBWSxLQUFLLENBQUM7UUFHMUIsaUJBQVksR0FBWSxJQUFJLENBQUM7UUFLN0IsV0FBTSxHQUFZLEtBQUssQ0FBQztRQU9wQixJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFTLDZDQUE2QyxDQUFDLElBQUkseUJBQXlCLENBQUMsaUJBQWlCLENBQUM7SUFDbkosQ0FBQzs7OztJQUVELCtDQUFXOzs7SUFBWDtRQUNJLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQztJQUNsRyxDQUFDOzs7O0lBRUQsZ0RBQVk7OztJQUFaO1FBQ0ksT0FBTyxJQUFJLENBQUMsWUFBWSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUN6RCxDQUFDOzs7O0lBRUQsOENBQVU7OztJQUFWO1FBQ0ksT0FBTyxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDO0lBQ25ELENBQUM7Ozs7SUFFRCwrQ0FBVzs7O0lBQVg7UUFDSSxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQztJQUNyQyxDQUFDOzs7O0lBRUQsMkNBQU87OztJQUFQO1FBQ0ksT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUM7SUFDaEMsQ0FBQzs7OztJQUVELDZDQUFTOzs7SUFBVDtRQUNJLE9BQU8sSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDL0QsQ0FBQzs7Ozs7SUFFRCwrQ0FBVzs7OztJQUFYLFVBQVksVUFBbUI7UUFBL0IsaUJBT0M7UUFORyxJQUFJLENBQUMsTUFBTSxHQUFHLFVBQVUsQ0FBQztRQUN6QixVQUFVOzs7UUFBQztZQUNQLElBQUksS0FBSSxDQUFDLFdBQVcsRUFBRTtnQkFDbEIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLENBQUM7YUFDMUM7UUFDTCxDQUFDLEdBQUUsQ0FBQyxDQUFDLENBQUM7SUFDVixDQUFDOzs7O0lBRUQseUNBQUs7OztJQUFMO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDO1FBQzlGLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDeEIsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDOUIsQ0FBQzs7Ozs7SUFFTyxzREFBa0I7Ozs7SUFBMUI7UUFDSSxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQsMENBQU07OztJQUFOO1FBQ0ksSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQUU7O2dCQUNuQyxZQUFZLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQztZQUNoRixJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsWUFBWSxDQUFDLENBQUM7WUFDL0QsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLEdBQUcsWUFBWSxDQUFDO1lBQ25DLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDeEIsSUFBSSxDQUFDLGtCQUFrQixFQUFFLENBQUM7U0FDN0I7YUFBTTtZQUNILElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7U0FDNUU7SUFDTCxDQUFDOzs7Ozs7SUFFRCx5REFBcUI7Ozs7O0lBQXJCLFVBQXNCLFFBQStCLEVBQUUsS0FBYTtRQUNoRSxJQUFJLFFBQVEsQ0FBQyxXQUFXLEVBQUU7O2dCQUNoQixZQUFZLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsR0FBRzs7OztZQUFDLFVBQUMsSUFBSSxJQUFLLE9BQUEsSUFBSSxDQUFDLElBQUksRUFBRSxFQUFYLENBQVcsRUFBQztZQUN2RixPQUFPLFlBQVksQ0FBQztTQUN2QjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7SUFFRCx5REFBcUI7OztJQUFyQjtRQUNJLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDN0UsQ0FBQzs7OztJQUVELDJDQUFPOzs7SUFBUDtRQUNJLElBQUksT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDLGFBQWEsS0FBSyxVQUFVLEVBQUU7WUFDbkQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLEVBQUUsQ0FBQztTQUNqQzthQUFNO1lBQ0gsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDckQ7SUFDTCxDQUFDO0lBakdNLDJDQUFpQixHQUFHLElBQUksQ0FBQzs7Z0JBUG5DLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsd0JBQXdCO29CQUNsQyx3MUlBQWtEOztpQkFFckQ7Ozs7Z0JBUFEscUJBQXFCO2dCQUNyQixnQkFBZ0I7OzsyQkFXcEIsS0FBSzsyQkFHTCxLQUFLOytCQUdMLEtBQUs7OEJBR0wsU0FBUyxTQUFDLGFBQWEsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7O0lBdUY1QyxnQ0FBQztDQUFBLEFBekdELElBeUdDO1NBcEdZLHlCQUF5Qjs7O0lBRWxDLDRDQUFnQzs7SUFFaEMsNkNBQ2dDOztJQUVoQyw2Q0FDMEI7O0lBRTFCLGlEQUM2Qjs7Ozs7SUFFN0IsZ0RBQ3lCOztJQUV6QiwyQ0FBd0I7O0lBQ3hCLGdEQUFvQjs7SUFDcEIsa0RBQXdCOztJQUN4QixtREFBdUI7Ozs7O0lBRVgsMERBQW9EOzs7OztJQUNwRCw4Q0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cblxuLyohXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cbiAqXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4gKlxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuICpcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXG4gKi9cblxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25DaGFuZ2VzLCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IENhcmRWaWV3VGV4dEl0ZW1Nb2RlbCB9IGZyb20gJy4uLy4uL21vZGVscy9jYXJkLXZpZXctdGV4dGl0ZW0ubW9kZWwnO1xuaW1wb3J0IHsgQ2FyZFZpZXdVcGRhdGVTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvY2FyZC12aWV3LXVwZGF0ZS5zZXJ2aWNlJztcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYWRmLWNhcmQtdmlldy10ZXh0aXRlbScsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2NhcmQtdmlldy10ZXh0aXRlbS5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vY2FyZC12aWV3LXRleHRpdGVtLmNvbXBvbmVudC5zY3NzJ11cbn0pXG5leHBvcnQgY2xhc3MgQ2FyZFZpZXdUZXh0SXRlbUNvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XG5cbiAgICBzdGF0aWMgREVGQVVMVF9TRVBBUkFUT1IgPSAnLCAnO1xuXG4gICAgQElucHV0KClcbiAgICBwcm9wZXJ0eTogQ2FyZFZpZXdUZXh0SXRlbU1vZGVsO1xuXG4gICAgQElucHV0KClcbiAgICBlZGl0YWJsZTogYm9vbGVhbiA9IGZhbHNlO1xuXG4gICAgQElucHV0KClcbiAgICBkaXNwbGF5RW1wdHk6IGJvb2xlYW4gPSB0cnVlO1xuXG4gICAgQFZpZXdDaGlsZCgnZWRpdG9ySW5wdXQnLCB7c3RhdGljOiB0cnVlfSlcbiAgICBwcml2YXRlIGVkaXRvcklucHV0OiBhbnk7XG5cbiAgICBpbkVkaXQ6IGJvb2xlYW4gPSBmYWxzZTtcbiAgICBlZGl0ZWRWYWx1ZTogc3RyaW5nO1xuICAgIGVycm9yTWVzc2FnZXM6IHN0cmluZ1tdO1xuICAgIHZhbHVlU2VwYXJhdG9yOiBzdHJpbmc7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNhcmRWaWV3VXBkYXRlU2VydmljZTogQ2FyZFZpZXdVcGRhdGVTZXJ2aWNlLFxuICAgICAgICAgICAgICAgIHByaXZhdGUgYXBwQ29uZmlnOiBBcHBDb25maWdTZXJ2aWNlKSB7XG4gICAgICAgIHRoaXMudmFsdWVTZXBhcmF0b3IgPSB0aGlzLmFwcENvbmZpZy5nZXQ8c3RyaW5nPignY29udGVudC1tZXRhZGF0YS5tdWx0aS12YWx1ZS1waXBlLXNlcGFyYXRvcicpIHx8IENhcmRWaWV3VGV4dEl0ZW1Db21wb25lbnQuREVGQVVMVF9TRVBBUkFUT1I7XG4gICAgfVxuXG4gICAgbmdPbkNoYW5nZXMoKTogdm9pZCB7XG4gICAgICAgIHRoaXMuZWRpdGVkVmFsdWUgPSB0aGlzLnByb3BlcnR5Lm11bHRpbGluZSA/IHRoaXMucHJvcGVydHkuZGlzcGxheVZhbHVlIDogdGhpcy5wcm9wZXJ0eS52YWx1ZTtcbiAgICB9XG5cbiAgICBzaG93UHJvcGVydHkoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLmRpc3BsYXlFbXB0eSB8fCAhdGhpcy5wcm9wZXJ0eS5pc0VtcHR5KCk7XG4gICAgfVxuXG4gICAgaXNFZGl0YWJsZSgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZWRpdGFibGUgJiYgdGhpcy5wcm9wZXJ0eS5lZGl0YWJsZTtcbiAgICB9XG5cbiAgICBpc0NsaWNrYWJsZSgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuICEhdGhpcy5wcm9wZXJ0eS5jbGlja2FibGU7XG4gICAgfVxuXG4gICAgaGFzSWNvbigpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuICEhdGhpcy5wcm9wZXJ0eS5pY29uO1xuICAgIH1cblxuICAgIGhhc0Vycm9ycygpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZXJyb3JNZXNzYWdlcyAmJiB0aGlzLmVycm9yTWVzc2FnZXMubGVuZ3RoID4gMDtcbiAgICB9XG5cbiAgICBzZXRFZGl0TW9kZShlZGl0U3RhdHVzOiBib29sZWFuKTogdm9pZCB7XG4gICAgICAgIHRoaXMuaW5FZGl0ID0gZWRpdFN0YXR1cztcbiAgICAgICAgc2V0VGltZW91dCgoKSA9PiB7XG4gICAgICAgICAgICBpZiAodGhpcy5lZGl0b3JJbnB1dCkge1xuICAgICAgICAgICAgICAgIHRoaXMuZWRpdG9ySW5wdXQubmF0aXZlRWxlbWVudC5jbGljaygpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9LCAwKTtcbiAgICB9XG5cbiAgICByZXNldCgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5lZGl0ZWRWYWx1ZSA9IHRoaXMucHJvcGVydHkubXVsdGlsaW5lID8gdGhpcy5wcm9wZXJ0eS5kaXNwbGF5VmFsdWUgOiB0aGlzLnByb3BlcnR5LnZhbHVlO1xuICAgICAgICB0aGlzLnNldEVkaXRNb2RlKGZhbHNlKTtcbiAgICAgICAgdGhpcy5yZXNldEVycm9yTWVzc2FnZXMoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHJlc2V0RXJyb3JNZXNzYWdlcygpIHtcbiAgICAgICAgdGhpcy5lcnJvck1lc3NhZ2VzID0gW107XG4gICAgfVxuXG4gICAgdXBkYXRlKCk6IHZvaWQge1xuICAgICAgICBpZiAodGhpcy5wcm9wZXJ0eS5pc1ZhbGlkKHRoaXMuZWRpdGVkVmFsdWUpKSB7XG4gICAgICAgICAgICBjb25zdCB1cGRhdGVkVmFsdWUgPSB0aGlzLnByZXBhcmVWYWx1ZUZvclVwbG9hZCh0aGlzLnByb3BlcnR5LCB0aGlzLmVkaXRlZFZhbHVlKTtcbiAgICAgICAgICAgIHRoaXMuY2FyZFZpZXdVcGRhdGVTZXJ2aWNlLnVwZGF0ZSh0aGlzLnByb3BlcnR5LCB1cGRhdGVkVmFsdWUpO1xuICAgICAgICAgICAgdGhpcy5wcm9wZXJ0eS52YWx1ZSA9IHVwZGF0ZWRWYWx1ZTtcbiAgICAgICAgICAgIHRoaXMuc2V0RWRpdE1vZGUoZmFsc2UpO1xuICAgICAgICAgICAgdGhpcy5yZXNldEVycm9yTWVzc2FnZXMoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuZXJyb3JNZXNzYWdlcyA9IHRoaXMucHJvcGVydHkuZ2V0VmFsaWRhdGlvbkVycm9ycyh0aGlzLmVkaXRlZFZhbHVlKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByZXBhcmVWYWx1ZUZvclVwbG9hZChwcm9wZXJ0eTogQ2FyZFZpZXdUZXh0SXRlbU1vZGVsLCB2YWx1ZTogc3RyaW5nKTogc3RyaW5nIHwgc3RyaW5nIFtdIHtcbiAgICAgICAgaWYgKHByb3BlcnR5Lm11bHRpdmFsdWVkKSB7XG4gICAgICAgICAgICBjb25zdCBsaXN0T2ZWYWx1ZXMgPSB2YWx1ZS5zcGxpdCh0aGlzLnZhbHVlU2VwYXJhdG9yLnRyaW0oKSkubWFwKChpdGVtKSA9PiBpdGVtLnRyaW0oKSk7XG4gICAgICAgICAgICByZXR1cm4gbGlzdE9mVmFsdWVzO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiB2YWx1ZTtcbiAgICB9XG5cbiAgICBvblRleHRBcmVhSW5wdXRDaGFuZ2UoKSB7XG4gICAgICAgIHRoaXMuZXJyb3JNZXNzYWdlcyA9IHRoaXMucHJvcGVydHkuZ2V0VmFsaWRhdGlvbkVycm9ycyh0aGlzLmVkaXRlZFZhbHVlKTtcbiAgICB9XG5cbiAgICBjbGlja2VkKCk6IHZvaWQge1xuICAgICAgICBpZiAodHlwZW9mIHRoaXMucHJvcGVydHkuY2xpY2tDYWxsQmFjayA9PT0gJ2Z1bmN0aW9uJykge1xuICAgICAgICAgICAgdGhpcy5wcm9wZXJ0eS5jbGlja0NhbGxCYWNrKCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmNhcmRWaWV3VXBkYXRlU2VydmljZS5jbGlja2VkKHRoaXMucHJvcGVydHkpO1xuICAgICAgICB9XG4gICAgfVxufVxuIl19