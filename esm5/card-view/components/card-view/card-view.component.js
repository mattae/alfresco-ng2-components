/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input } from '@angular/core';
var CardViewComponent = /** @class */ (function () {
    function CardViewComponent() {
        /**
         * Toggles whether or not to show empty items in non-editable mode.
         */
        this.displayEmpty = true;
    }
    CardViewComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-card-view',
                    template: "<div class=\"adf-property-list\">\r\n    <div *ngFor=\"let property of properties\">\r\n        <div [attr.data-automation-id]=\"'header-'+property.key\" class=\"adf-property\">\r\n            <adf-card-view-item-dispatcher\r\n                [property]=\"property\"\r\n                [editable]=\"editable\"\r\n                [displayEmpty]=\"displayEmpty\">\r\n            </adf-card-view-item-dispatcher>\r\n        </div>\r\n    </div>\r\n</div>\r\n",
                    styles: [""]
                }] }
    ];
    CardViewComponent.propDecorators = {
        properties: [{ type: Input }],
        editable: [{ type: Input }],
        displayEmpty: [{ type: Input }]
    };
    return CardViewComponent;
}());
export { CardViewComponent };
if (false) {
    /**
     * (**required**) Items to show in the card view.
     * @type {?}
     */
    CardViewComponent.prototype.properties;
    /**
     * Toggles whether or not the items can be edited.
     * @type {?}
     */
    CardViewComponent.prototype.editable;
    /**
     * Toggles whether or not to show empty items in non-editable mode.
     * @type {?}
     */
    CardViewComponent.prototype.displayEmpty;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNhcmQtdmlldy9jb21wb25lbnRzL2NhcmQtdmlldy9jYXJkLXZpZXcuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBR2pEO0lBQUE7Ozs7UUFnQkksaUJBQVksR0FBWSxJQUFJLENBQUM7SUFDakMsQ0FBQzs7Z0JBakJBLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsZUFBZTtvQkFDekIsbWRBQXlDOztpQkFFNUM7Ozs2QkFHSSxLQUFLOzJCQUlMLEtBQUs7K0JBSUwsS0FBSzs7SUFFVix3QkFBQztDQUFBLEFBakJELElBaUJDO1NBWlksaUJBQWlCOzs7Ozs7SUFFMUIsdUNBQzRCOzs7OztJQUc1QixxQ0FDa0I7Ozs7O0lBR2xCLHlDQUM2QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENhcmRWaWV3SXRlbSB9IGZyb20gJy4uLy4uL2ludGVyZmFjZXMvY2FyZC12aWV3LWl0ZW0uaW50ZXJmYWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtY2FyZC12aWV3JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9jYXJkLXZpZXcuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vY2FyZC12aWV3LmNvbXBvbmVudC5zY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3Q29tcG9uZW50IHtcclxuICAgIC8qKiAoKipyZXF1aXJlZCoqKSBJdGVtcyB0byBzaG93IGluIHRoZSBjYXJkIHZpZXcuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgcHJvcGVydGllczogQ2FyZFZpZXdJdGVtIFtdO1xyXG5cclxuICAgIC8qKiBUb2dnbGVzIHdoZXRoZXIgb3Igbm90IHRoZSBpdGVtcyBjYW4gYmUgZWRpdGVkLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGVkaXRhYmxlOiBib29sZWFuO1xyXG5cclxuICAgIC8qKiBUb2dnbGVzIHdoZXRoZXIgb3Igbm90IHRvIHNob3cgZW1wdHkgaXRlbXMgaW4gbm9uLWVkaXRhYmxlIG1vZGUuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgZGlzcGxheUVtcHR5OiBib29sZWFuID0gdHJ1ZTtcclxufVxyXG4iXX0=