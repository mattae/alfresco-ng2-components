/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @record
 * @template T
 */
export function CardViewSelectItemOption() { }
if (false) {
    /** @type {?} */
    CardViewSelectItemOption.prototype.label;
    /** @type {?} */
    CardViewSelectItemOption.prototype.key;
}
/**
 * @record
 * @template T
 */
export function CardViewSelectItemProperties() { }
if (false) {
    /** @type {?} */
    CardViewSelectItemProperties.prototype.value;
    /** @type {?} */
    CardViewSelectItemProperties.prototype.options$;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LXNlbGVjdGl0ZW0tcHJvcGVydGllcy5pbnRlcmZhY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJjYXJkLXZpZXcvaW50ZXJmYWNlcy9jYXJkLXZpZXctc2VsZWN0aXRlbS1wcm9wZXJ0aWVzLmludGVyZmFjZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQkEsOENBR0M7OztJQUZHLHlDQUFjOztJQUNkLHVDQUFPOzs7Ozs7QUFHWCxrREFHQzs7O0lBRkcsNkNBQWM7O0lBQ2QsZ0RBQW9EIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENhcmRWaWV3SXRlbVByb3BlcnRpZXMgfSBmcm9tICcuL2NhcmQtdmlldy1pdGVtLXByb3BlcnRpZXMuaW50ZXJmYWNlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBDYXJkVmlld1NlbGVjdEl0ZW1PcHRpb248VD4ge1xyXG4gICAgbGFiZWw6IHN0cmluZztcclxuICAgIGtleTogVDtcclxufVxyXG5cclxuZXhwb3J0IGludGVyZmFjZSBDYXJkVmlld1NlbGVjdEl0ZW1Qcm9wZXJ0aWVzPFQ+IGV4dGVuZHMgQ2FyZFZpZXdJdGVtUHJvcGVydGllcyB7XHJcbiAgICB2YWx1ZTogc3RyaW5nO1xyXG4gICAgb3B0aW9ucyQ6IE9ic2VydmFibGU8Q2FyZFZpZXdTZWxlY3RJdGVtT3B0aW9uPFQ+W10+O1xyXG59XHJcbiJdfQ==