/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @record
 */
export function CardViewTextItemProperties() { }
if (false) {
    /** @type {?|undefined} */
    CardViewTextItemProperties.prototype.multiline;
    /** @type {?|undefined} */
    CardViewTextItemProperties.prototype.multivalued;
    /** @type {?|undefined} */
    CardViewTextItemProperties.prototype.pipes;
    /** @type {?|undefined} */
    CardViewTextItemProperties.prototype.clickCallBack;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LXRleHRpdGVtLXByb3BlcnRpZXMuaW50ZXJmYWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiY2FyZC12aWV3L2ludGVyZmFjZXMvY2FyZC12aWV3LXRleHRpdGVtLXByb3BlcnRpZXMuaW50ZXJmYWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBb0JBLGdEQUtDOzs7SUFKRywrQ0FBb0I7O0lBQ3BCLGlEQUFzQjs7SUFDdEIsMkNBQXVDOztJQUN2QyxtREFBb0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ2FyZFZpZXdJdGVtUHJvcGVydGllcyB9IGZyb20gJy4vY2FyZC12aWV3LWl0ZW0tcHJvcGVydGllcy5pbnRlcmZhY2UnO1xyXG5pbXBvcnQgeyBDYXJkVmlld1RleHRJdGVtUGlwZVByb3BlcnR5IH0gZnJvbSAnLi9jYXJkLXZpZXctdGV4dGl0ZW0tcGlwZS1wcm9wZXJ0eS5pbnRlcmZhY2UnO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBDYXJkVmlld1RleHRJdGVtUHJvcGVydGllcyBleHRlbmRzIENhcmRWaWV3SXRlbVByb3BlcnRpZXMge1xyXG4gICAgbXVsdGlsaW5lPzogYm9vbGVhbjtcclxuICAgIG11bHRpdmFsdWVkPzogYm9vbGVhbjtcclxuICAgIHBpcGVzPzogQ2FyZFZpZXdUZXh0SXRlbVBpcGVQcm9wZXJ0eVtdO1xyXG4gICAgY2xpY2tDYWxsQmFjaz86IGFueTtcclxufVxyXG4iXX0=