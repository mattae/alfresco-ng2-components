/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @abstract
 */
var /**
 * @abstract
 */
CardViewBaseItemModel = /** @class */ (function () {
    function CardViewBaseItemModel(cardViewItemProperties) {
        this.label = cardViewItemProperties.label || '';
        this.value = cardViewItemProperties.value && cardViewItemProperties.value.displayName || cardViewItemProperties.value;
        this.key = cardViewItemProperties.key;
        this.default = cardViewItemProperties.default;
        this.editable = !!cardViewItemProperties.editable;
        this.clickable = !!cardViewItemProperties.clickable;
        this.icon = cardViewItemProperties.icon || '';
        this.validators = cardViewItemProperties.validators || [];
        this.data = cardViewItemProperties.data || null;
    }
    /**
     * @return {?}
     */
    CardViewBaseItemModel.prototype.isEmpty = /**
     * @return {?}
     */
    function () {
        return this.value === undefined || this.value === null || this.value === '';
    };
    /**
     * @param {?} newValue
     * @return {?}
     */
    CardViewBaseItemModel.prototype.isValid = /**
     * @param {?} newValue
     * @return {?}
     */
    function (newValue) {
        if (!this.validators.length) {
            return true;
        }
        return this.validators
            .map((/**
         * @param {?} validator
         * @return {?}
         */
        function (validator) { return validator.isValid(newValue); }))
            .reduce((/**
         * @param {?} isValidUntilNow
         * @param {?} isValid
         * @return {?}
         */
        function (isValidUntilNow, isValid) { return isValidUntilNow && isValid; }), true);
    };
    /**
     * @param {?} value
     * @return {?}
     */
    CardViewBaseItemModel.prototype.getValidationErrors = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (!this.validators.length) {
            return [];
        }
        return this.validators.filter((/**
         * @param {?} validator
         * @return {?}
         */
        function (validator) { return !validator.isValid(value); })).map((/**
         * @param {?} validator
         * @return {?}
         */
        function (validator) { return validator.message; }));
    };
    return CardViewBaseItemModel;
}());
/**
 * @abstract
 */
export { CardViewBaseItemModel };
if (false) {
    /** @type {?} */
    CardViewBaseItemModel.prototype.label;
    /** @type {?} */
    CardViewBaseItemModel.prototype.value;
    /** @type {?} */
    CardViewBaseItemModel.prototype.key;
    /** @type {?} */
    CardViewBaseItemModel.prototype.default;
    /** @type {?} */
    CardViewBaseItemModel.prototype.editable;
    /** @type {?} */
    CardViewBaseItemModel.prototype.clickable;
    /** @type {?} */
    CardViewBaseItemModel.prototype.icon;
    /** @type {?} */
    CardViewBaseItemModel.prototype.validators;
    /** @type {?} */
    CardViewBaseItemModel.prototype.data;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWJhc2VpdGVtLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiY2FyZC12aWV3L21vZGVscy9jYXJkLXZpZXctYmFzZWl0ZW0ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7Ozs7SUFXSSwrQkFBWSxzQkFBOEM7UUFDdEQsSUFBSSxDQUFDLEtBQUssR0FBRyxzQkFBc0IsQ0FBQyxLQUFLLElBQUksRUFBRSxDQUFDO1FBQ2hELElBQUksQ0FBQyxLQUFLLEdBQUcsc0JBQXNCLENBQUMsS0FBSyxJQUFJLHNCQUFzQixDQUFDLEtBQUssQ0FBQyxXQUFXLElBQUksc0JBQXNCLENBQUMsS0FBSyxDQUFDO1FBQ3RILElBQUksQ0FBQyxHQUFHLEdBQUcsc0JBQXNCLENBQUMsR0FBRyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxPQUFPLEdBQUcsc0JBQXNCLENBQUMsT0FBTyxDQUFDO1FBQzlDLElBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxDQUFDLHNCQUFzQixDQUFDLFFBQVEsQ0FBQztRQUNsRCxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxzQkFBc0IsQ0FBQyxTQUFTLENBQUM7UUFDcEQsSUFBSSxDQUFDLElBQUksR0FBRyxzQkFBc0IsQ0FBQyxJQUFJLElBQUksRUFBRSxDQUFDO1FBQzlDLElBQUksQ0FBQyxVQUFVLEdBQUcsc0JBQXNCLENBQUMsVUFBVSxJQUFJLEVBQUUsQ0FBQztRQUMxRCxJQUFJLENBQUMsSUFBSSxHQUFHLHNCQUFzQixDQUFDLElBQUksSUFBSSxJQUFJLENBQUM7SUFDcEQsQ0FBQzs7OztJQUVELHVDQUFPOzs7SUFBUDtRQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssS0FBSyxTQUFTLElBQUksSUFBSSxDQUFDLEtBQUssS0FBSyxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssS0FBSyxFQUFFLENBQUM7SUFDaEYsQ0FBQzs7Ozs7SUFFRCx1Q0FBTzs7OztJQUFQLFVBQVEsUUFBYTtRQUNqQixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUU7WUFDekIsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUVELE9BQU8sSUFBSSxDQUFDLFVBQVU7YUFDakIsR0FBRzs7OztRQUFDLFVBQUMsU0FBUyxJQUFLLE9BQUEsU0FBUyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsRUFBM0IsQ0FBMkIsRUFBQzthQUMvQyxNQUFNOzs7OztRQUFDLFVBQUMsZUFBZSxFQUFFLE9BQU8sSUFBSyxPQUFBLGVBQWUsSUFBSSxPQUFPLEVBQTFCLENBQTBCLEdBQUUsSUFBSSxDQUFDLENBQUM7SUFDaEYsQ0FBQzs7Ozs7SUFFRCxtREFBbUI7Ozs7SUFBbkIsVUFBb0IsS0FBSztRQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUU7WUFDekIsT0FBTyxFQUFFLENBQUM7U0FDYjtRQUVELE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQyxTQUFTLElBQUssT0FBQSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLEVBQXpCLENBQXlCLEVBQUMsQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQyxTQUFTLElBQUssT0FBQSxTQUFTLENBQUMsT0FBTyxFQUFqQixDQUFpQixFQUFDLENBQUM7SUFDbEgsQ0FBQztJQUNMLDRCQUFDO0FBQUQsQ0FBQyxBQTVDRCxJQTRDQzs7Ozs7OztJQTNDRyxzQ0FBYzs7SUFDZCxzQ0FBVzs7SUFDWCxvQ0FBUzs7SUFDVCx3Q0FBYTs7SUFDYix5Q0FBa0I7O0lBQ2xCLDBDQUFtQjs7SUFDbkIscUNBQWM7O0lBQ2QsMkNBQXFDOztJQUNyQyxxQ0FBVyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDYXJkVmlld0l0ZW1Qcm9wZXJ0aWVzLCBDYXJkVmlld0l0ZW1WYWxpZGF0b3IgfSBmcm9tICcuLi9pbnRlcmZhY2VzL2NhcmQtdmlldy5pbnRlcmZhY2VzJztcclxuXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBDYXJkVmlld0Jhc2VJdGVtTW9kZWwge1xyXG4gICAgbGFiZWw6IHN0cmluZztcclxuICAgIHZhbHVlOiBhbnk7XHJcbiAgICBrZXk6IGFueTtcclxuICAgIGRlZmF1bHQ6IGFueTtcclxuICAgIGVkaXRhYmxlOiBib29sZWFuO1xyXG4gICAgY2xpY2thYmxlOiBib29sZWFuO1xyXG4gICAgaWNvbj86IHN0cmluZztcclxuICAgIHZhbGlkYXRvcnM/OiBDYXJkVmlld0l0ZW1WYWxpZGF0b3JbXTtcclxuICAgIGRhdGE/OiBhbnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoY2FyZFZpZXdJdGVtUHJvcGVydGllczogQ2FyZFZpZXdJdGVtUHJvcGVydGllcykge1xyXG4gICAgICAgIHRoaXMubGFiZWwgPSBjYXJkVmlld0l0ZW1Qcm9wZXJ0aWVzLmxhYmVsIHx8ICcnO1xyXG4gICAgICAgIHRoaXMudmFsdWUgPSBjYXJkVmlld0l0ZW1Qcm9wZXJ0aWVzLnZhbHVlICYmIGNhcmRWaWV3SXRlbVByb3BlcnRpZXMudmFsdWUuZGlzcGxheU5hbWUgfHwgY2FyZFZpZXdJdGVtUHJvcGVydGllcy52YWx1ZTtcclxuICAgICAgICB0aGlzLmtleSA9IGNhcmRWaWV3SXRlbVByb3BlcnRpZXMua2V5O1xyXG4gICAgICAgIHRoaXMuZGVmYXVsdCA9IGNhcmRWaWV3SXRlbVByb3BlcnRpZXMuZGVmYXVsdDtcclxuICAgICAgICB0aGlzLmVkaXRhYmxlID0gISFjYXJkVmlld0l0ZW1Qcm9wZXJ0aWVzLmVkaXRhYmxlO1xyXG4gICAgICAgIHRoaXMuY2xpY2thYmxlID0gISFjYXJkVmlld0l0ZW1Qcm9wZXJ0aWVzLmNsaWNrYWJsZTtcclxuICAgICAgICB0aGlzLmljb24gPSBjYXJkVmlld0l0ZW1Qcm9wZXJ0aWVzLmljb24gfHwgJyc7XHJcbiAgICAgICAgdGhpcy52YWxpZGF0b3JzID0gY2FyZFZpZXdJdGVtUHJvcGVydGllcy52YWxpZGF0b3JzIHx8IFtdO1xyXG4gICAgICAgIHRoaXMuZGF0YSA9IGNhcmRWaWV3SXRlbVByb3BlcnRpZXMuZGF0YSB8fCBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIGlzRW1wdHkoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudmFsdWUgPT09IHVuZGVmaW5lZCB8fCB0aGlzLnZhbHVlID09PSBudWxsIHx8IHRoaXMudmFsdWUgPT09ICcnO1xyXG4gICAgfVxyXG5cclxuICAgIGlzVmFsaWQobmV3VmFsdWU6IGFueSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICghdGhpcy52YWxpZGF0b3JzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLnZhbGlkYXRvcnNcclxuICAgICAgICAgICAgLm1hcCgodmFsaWRhdG9yKSA9PiB2YWxpZGF0b3IuaXNWYWxpZChuZXdWYWx1ZSkpXHJcbiAgICAgICAgICAgIC5yZWR1Y2UoKGlzVmFsaWRVbnRpbE5vdywgaXNWYWxpZCkgPT4gaXNWYWxpZFVudGlsTm93ICYmIGlzVmFsaWQsIHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFZhbGlkYXRpb25FcnJvcnModmFsdWUpOiBzdHJpbmdbXSB7XHJcbiAgICAgICAgaWYgKCF0aGlzLnZhbGlkYXRvcnMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBbXTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLnZhbGlkYXRvcnMuZmlsdGVyKCh2YWxpZGF0b3IpID0+ICF2YWxpZGF0b3IuaXNWYWxpZCh2YWx1ZSkpLm1hcCgodmFsaWRhdG9yKSA9PiB2YWxpZGF0b3IubWVzc2FnZSk7XHJcbiAgICB9XHJcbn1cclxuIl19