/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CardViewBaseItemModel } from './card-view-baseitem.model';
var CardViewBoolItemModel = /** @class */ (function (_super) {
    tslib_1.__extends(CardViewBoolItemModel, _super);
    function CardViewBoolItemModel(cardViewBoolItemProperties) {
        var _this = _super.call(this, cardViewBoolItemProperties) || this;
        _this.type = 'bool';
        _this.value = false;
        if (cardViewBoolItemProperties.value !== undefined) {
            _this.value = !!JSON.parse(cardViewBoolItemProperties.value);
        }
        return _this;
    }
    Object.defineProperty(CardViewBoolItemModel.prototype, "displayValue", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.isEmpty()) {
                return this.default;
            }
            else {
                return this.value;
            }
        },
        enumerable: true,
        configurable: true
    });
    return CardViewBoolItemModel;
}(CardViewBaseItemModel));
export { CardViewBoolItemModel };
if (false) {
    /** @type {?} */
    CardViewBoolItemModel.prototype.type;
    /** @type {?} */
    CardViewBoolItemModel.prototype.value;
    /** @type {?} */
    CardViewBoolItemModel.prototype.default;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWJvb2xpdGVtLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiY2FyZC12aWV3L21vZGVscy9jYXJkLXZpZXctYm9vbGl0ZW0ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBR25FO0lBQTJDLGlEQUFxQjtJQUs1RCwrQkFBWSwwQkFBc0Q7UUFBbEUsWUFDSSxrQkFBTSwwQkFBMEIsQ0FBQyxTQUtwQztRQVZELFVBQUksR0FBVyxNQUFNLENBQUM7UUFDdEIsV0FBSyxHQUFZLEtBQUssQ0FBQztRQU1uQixJQUFJLDBCQUEwQixDQUFDLEtBQUssS0FBSyxTQUFTLEVBQUU7WUFDaEQsS0FBSSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQywwQkFBMEIsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMvRDs7SUFDTCxDQUFDO0lBRUQsc0JBQUksK0NBQVk7Ozs7UUFBaEI7WUFDSSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFDaEIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO2FBQ3ZCO2lCQUFNO2dCQUNILE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQzthQUNyQjtRQUNMLENBQUM7OztPQUFBO0lBQ0wsNEJBQUM7QUFBRCxDQUFDLEFBcEJELENBQTJDLHFCQUFxQixHQW9CL0Q7Ozs7SUFuQkcscUNBQXNCOztJQUN0QixzQ0FBdUI7O0lBQ3ZCLHdDQUFpQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDYXJkVmlld0l0ZW0gfSBmcm9tICcuLi9pbnRlcmZhY2VzL2NhcmQtdmlldy1pdGVtLmludGVyZmFjZSc7XHJcbmltcG9ydCB7IER5bmFtaWNDb21wb25lbnRNb2RlbCB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2R5bmFtaWMtY29tcG9uZW50LW1hcHBlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdCYXNlSXRlbU1vZGVsIH0gZnJvbSAnLi9jYXJkLXZpZXctYmFzZWl0ZW0ubW9kZWwnO1xyXG5pbXBvcnQgeyBDYXJkVmlld0Jvb2xJdGVtUHJvcGVydGllcyB9IGZyb20gJy4uL2ludGVyZmFjZXMvY2FyZC12aWV3LmludGVyZmFjZXMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3Qm9vbEl0ZW1Nb2RlbCBleHRlbmRzIENhcmRWaWV3QmFzZUl0ZW1Nb2RlbCBpbXBsZW1lbnRzIENhcmRWaWV3SXRlbSwgRHluYW1pY0NvbXBvbmVudE1vZGVsIHtcclxuICAgIHR5cGU6IHN0cmluZyA9ICdib29sJztcclxuICAgIHZhbHVlOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBkZWZhdWx0OiBib29sZWFuO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGNhcmRWaWV3Qm9vbEl0ZW1Qcm9wZXJ0aWVzOiBDYXJkVmlld0Jvb2xJdGVtUHJvcGVydGllcykge1xyXG4gICAgICAgIHN1cGVyKGNhcmRWaWV3Qm9vbEl0ZW1Qcm9wZXJ0aWVzKTtcclxuXHJcbiAgICAgICAgaWYgKGNhcmRWaWV3Qm9vbEl0ZW1Qcm9wZXJ0aWVzLnZhbHVlICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgdGhpcy52YWx1ZSA9ICEhSlNPTi5wYXJzZShjYXJkVmlld0Jvb2xJdGVtUHJvcGVydGllcy52YWx1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldCBkaXNwbGF5VmFsdWUoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNFbXB0eSgpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmRlZmF1bHQ7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMudmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==