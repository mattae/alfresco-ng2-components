/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CardViewTextItemModel } from './card-view-textitem.model';
import { CardViewItemIntValidator } from '../validators/card-view.validators';
var CardViewIntItemModel = /** @class */ (function (_super) {
    tslib_1.__extends(CardViewIntItemModel, _super);
    function CardViewIntItemModel(cardViewTextItemProperties) {
        var _this = _super.call(this, cardViewTextItemProperties) || this;
        _this.type = 'int';
        _this.validators.push(new CardViewItemIntValidator());
        if (cardViewTextItemProperties.value) {
            _this.value = parseInt(cardViewTextItemProperties.value, 10);
        }
        return _this;
    }
    return CardViewIntItemModel;
}(CardViewTextItemModel));
export { CardViewIntItemModel };
if (false) {
    /** @type {?} */
    CardViewIntItemModel.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWludGl0ZW0ubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJjYXJkLXZpZXcvbW9kZWxzL2NhcmQtdmlldy1pbnRpdGVtLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUVuRSxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUU5RTtJQUEwQyxnREFBcUI7SUFHM0QsOEJBQVksMEJBQXNEO1FBQWxFLFlBQ0ksa0JBQU0sMEJBQTBCLENBQUMsU0FNcEM7UUFURCxVQUFJLEdBQVcsS0FBSyxDQUFDO1FBS2pCLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksd0JBQXdCLEVBQUUsQ0FBQyxDQUFDO1FBQ3JELElBQUksMEJBQTBCLENBQUMsS0FBSyxFQUFFO1lBQ2xDLEtBQUksQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLDBCQUEwQixDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztTQUMvRDs7SUFDTCxDQUFDO0lBQ0wsMkJBQUM7QUFBRCxDQUFDLEFBWEQsQ0FBMEMscUJBQXFCLEdBVzlEOzs7O0lBVkcsb0NBQXFCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENhcmRWaWV3SXRlbSB9IGZyb20gJy4uL2ludGVyZmFjZXMvY2FyZC12aWV3LWl0ZW0uaW50ZXJmYWNlJztcclxuaW1wb3J0IHsgRHluYW1pY0NvbXBvbmVudE1vZGVsIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvZHluYW1pYy1jb21wb25lbnQtbWFwcGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDYXJkVmlld1RleHRJdGVtTW9kZWwgfSBmcm9tICcuL2NhcmQtdmlldy10ZXh0aXRlbS5tb2RlbCc7XHJcbmltcG9ydCB7IENhcmRWaWV3VGV4dEl0ZW1Qcm9wZXJ0aWVzIH0gZnJvbSAnLi4vaW50ZXJmYWNlcy9jYXJkLXZpZXcuaW50ZXJmYWNlcyc7XHJcbmltcG9ydCB7IENhcmRWaWV3SXRlbUludFZhbGlkYXRvciB9IGZyb20gJy4uL3ZhbGlkYXRvcnMvY2FyZC12aWV3LnZhbGlkYXRvcnMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3SW50SXRlbU1vZGVsIGV4dGVuZHMgQ2FyZFZpZXdUZXh0SXRlbU1vZGVsIGltcGxlbWVudHMgQ2FyZFZpZXdJdGVtLCBEeW5hbWljQ29tcG9uZW50TW9kZWwge1xyXG4gICAgdHlwZTogc3RyaW5nID0gJ2ludCc7XHJcblxyXG4gICAgY29uc3RydWN0b3IoY2FyZFZpZXdUZXh0SXRlbVByb3BlcnRpZXM6IENhcmRWaWV3VGV4dEl0ZW1Qcm9wZXJ0aWVzKSB7XHJcbiAgICAgICAgc3VwZXIoY2FyZFZpZXdUZXh0SXRlbVByb3BlcnRpZXMpO1xyXG5cclxuICAgICAgICB0aGlzLnZhbGlkYXRvcnMucHVzaChuZXcgQ2FyZFZpZXdJdGVtSW50VmFsaWRhdG9yKCkpO1xyXG4gICAgICAgIGlmIChjYXJkVmlld1RleHRJdGVtUHJvcGVydGllcy52YWx1ZSkge1xyXG4gICAgICAgICAgICB0aGlzLnZhbHVlID0gcGFyc2VJbnQoY2FyZFZpZXdUZXh0SXRlbVByb3BlcnRpZXMudmFsdWUsIDEwKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19