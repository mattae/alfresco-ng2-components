/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CardViewBaseItemModel } from './card-view-baseitem.model';
var CardViewKeyValuePairsItemModel = /** @class */ (function (_super) {
    tslib_1.__extends(CardViewKeyValuePairsItemModel, _super);
    function CardViewKeyValuePairsItemModel(cardViewKeyValuePairsItemProperties) {
        var _this = _super.call(this, cardViewKeyValuePairsItemProperties) || this;
        _this.type = 'keyvaluepairs';
        return _this;
    }
    Object.defineProperty(CardViewKeyValuePairsItemModel.prototype, "displayValue", {
        get: /**
         * @return {?}
         */
        function () {
            return this.value;
        },
        enumerable: true,
        configurable: true
    });
    return CardViewKeyValuePairsItemModel;
}(CardViewBaseItemModel));
export { CardViewKeyValuePairsItemModel };
if (false) {
    /** @type {?} */
    CardViewKeyValuePairsItemModel.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWtleXZhbHVlcGFpcnMubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJjYXJkLXZpZXcvbW9kZWxzL2NhcmQtdmlldy1rZXl2YWx1ZXBhaXJzLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUduRTtJQUFvRCwwREFBcUI7SUFHckUsd0NBQVksbUNBQXdFO1FBQXBGLFlBQ0ksa0JBQU0sbUNBQW1DLENBQUMsU0FDN0M7UUFKRCxVQUFJLEdBQVcsZUFBZSxDQUFDOztJQUkvQixDQUFDO0lBRUQsc0JBQUksd0RBQVk7Ozs7UUFBaEI7WUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdEIsQ0FBQzs7O09BQUE7SUFDTCxxQ0FBQztBQUFELENBQUMsQUFWRCxDQUFvRCxxQkFBcUIsR0FVeEU7Ozs7SUFURyw4Q0FBK0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ2FyZFZpZXdJdGVtIH0gZnJvbSAnLi4vaW50ZXJmYWNlcy9jYXJkLXZpZXctaXRlbS5pbnRlcmZhY2UnO1xyXG5pbXBvcnQgeyBEeW5hbWljQ29tcG9uZW50TW9kZWwgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9keW5hbWljLWNvbXBvbmVudC1tYXBwZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IENhcmRWaWV3QmFzZUl0ZW1Nb2RlbCB9IGZyb20gJy4vY2FyZC12aWV3LWJhc2VpdGVtLm1vZGVsJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdLZXlWYWx1ZVBhaXJzSXRlbVByb3BlcnRpZXMgfSBmcm9tICcuLi9pbnRlcmZhY2VzL2NhcmQtdmlldy5pbnRlcmZhY2VzJztcclxuXHJcbmV4cG9ydCBjbGFzcyBDYXJkVmlld0tleVZhbHVlUGFpcnNJdGVtTW9kZWwgZXh0ZW5kcyBDYXJkVmlld0Jhc2VJdGVtTW9kZWwgaW1wbGVtZW50cyBDYXJkVmlld0l0ZW0sIER5bmFtaWNDb21wb25lbnRNb2RlbCB7XHJcbiAgICB0eXBlOiBzdHJpbmcgPSAna2V5dmFsdWVwYWlycyc7XHJcblxyXG4gICAgY29uc3RydWN0b3IoY2FyZFZpZXdLZXlWYWx1ZVBhaXJzSXRlbVByb3BlcnRpZXM6IENhcmRWaWV3S2V5VmFsdWVQYWlyc0l0ZW1Qcm9wZXJ0aWVzKSB7XHJcbiAgICAgICAgc3VwZXIoY2FyZFZpZXdLZXlWYWx1ZVBhaXJzSXRlbVByb3BlcnRpZXMpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBkaXNwbGF5VmFsdWUoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudmFsdWU7XHJcbiAgICB9XHJcbn1cclxuIl19