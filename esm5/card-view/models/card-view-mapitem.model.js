/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CardViewBaseItemModel } from './card-view-baseitem.model';
var CardViewMapItemModel = /** @class */ (function (_super) {
    tslib_1.__extends(CardViewMapItemModel, _super);
    function CardViewMapItemModel() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.type = 'map';
        return _this;
    }
    Object.defineProperty(CardViewMapItemModel.prototype, "displayValue", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.value && this.value.size > 0) {
                return this.value.values().next().value;
            }
            else {
                return this.default;
            }
        },
        enumerable: true,
        configurable: true
    });
    return CardViewMapItemModel;
}(CardViewBaseItemModel));
export { CardViewMapItemModel };
if (false) {
    /** @type {?} */
    CardViewMapItemModel.prototype.type;
    /** @type {?} */
    CardViewMapItemModel.prototype.value;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LW1hcGl0ZW0ubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJjYXJkLXZpZXcvbW9kZWxzL2NhcmQtdmlldy1tYXBpdGVtLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUVuRTtJQUEwQyxnREFBcUI7SUFBL0Q7UUFBQSxxRUFXQztRQVZHLFVBQUksR0FBVyxLQUFLLENBQUM7O0lBVXpCLENBQUM7SUFQRyxzQkFBSSw4Q0FBWTs7OztRQUFoQjtZQUNJLElBQUksSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxDQUFDLEVBQUU7Z0JBQ25DLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEVBQUUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUM7YUFDM0M7aUJBQU07Z0JBQ0gsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO2FBQ3ZCO1FBQ0wsQ0FBQzs7O09BQUE7SUFDTCwyQkFBQztBQUFELENBQUMsQUFYRCxDQUEwQyxxQkFBcUIsR0FXOUQ7Ozs7SUFWRyxvQ0FBcUI7O0lBQ3JCLHFDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDYXJkVmlld0l0ZW0gfSBmcm9tICcuLi9pbnRlcmZhY2VzL2NhcmQtdmlldy1pdGVtLmludGVyZmFjZSc7XHJcbmltcG9ydCB7IER5bmFtaWNDb21wb25lbnRNb2RlbCB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2R5bmFtaWMtY29tcG9uZW50LW1hcHBlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdCYXNlSXRlbU1vZGVsIH0gZnJvbSAnLi9jYXJkLXZpZXctYmFzZWl0ZW0ubW9kZWwnO1xyXG5cclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3TWFwSXRlbU1vZGVsIGV4dGVuZHMgQ2FyZFZpZXdCYXNlSXRlbU1vZGVsIGltcGxlbWVudHMgQ2FyZFZpZXdJdGVtLCBEeW5hbWljQ29tcG9uZW50TW9kZWwge1xyXG4gICAgdHlwZTogc3RyaW5nID0gJ21hcCc7XHJcbiAgICB2YWx1ZTogTWFwPHN0cmluZywgc3RyaW5nPjtcclxuXHJcbiAgICBnZXQgZGlzcGxheVZhbHVlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnZhbHVlICYmIHRoaXMudmFsdWUuc2l6ZSA+IDApIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMudmFsdWUudmFsdWVzKCkubmV4dCgpLnZhbHVlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmRlZmF1bHQ7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==