/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CardViewBaseItemModel } from './card-view-baseitem.model';
import { of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
/**
 * @template T
 */
var /**
 * @template T
 */
CardViewSelectItemModel = /** @class */ (function (_super) {
    tslib_1.__extends(CardViewSelectItemModel, _super);
    function CardViewSelectItemModel(cardViewSelectItemProperties) {
        var _this = _super.call(this, cardViewSelectItemProperties) || this;
        _this.type = 'select';
        _this.options$ = cardViewSelectItemProperties.options$;
        return _this;
    }
    Object.defineProperty(CardViewSelectItemModel.prototype, "displayValue", {
        get: /**
         * @return {?}
         */
        function () {
            var _this = this;
            return this.options$.pipe(switchMap((/**
             * @param {?} options
             * @return {?}
             */
            function (options) {
                /** @type {?} */
                var option = options.find((/**
                 * @param {?} o
                 * @return {?}
                 */
                function (o) { return o.key === _this.value; }));
                return of(option ? option.label : '');
            })));
        },
        enumerable: true,
        configurable: true
    });
    return CardViewSelectItemModel;
}(CardViewBaseItemModel));
/**
 * @template T
 */
export { CardViewSelectItemModel };
if (false) {
    /** @type {?} */
    CardViewSelectItemModel.prototype.type;
    /** @type {?} */
    CardViewSelectItemModel.prototype.options$;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LXNlbGVjdGl0ZW0ubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJjYXJkLXZpZXcvbW9kZWxzL2NhcmQtdmlldy1zZWxlY3RpdGVtLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUVuRSxPQUFPLEVBQWMsRUFBRSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3RDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7OztBQUUzQzs7OztJQUFnRCxtREFBcUI7SUFJakUsaUNBQVksNEJBQTZEO1FBQXpFLFlBQ0ksa0JBQU0sNEJBQTRCLENBQUMsU0FHdEM7UUFQRCxVQUFJLEdBQVcsUUFBUSxDQUFDO1FBTXBCLEtBQUksQ0FBQyxRQUFRLEdBQUcsNEJBQTRCLENBQUMsUUFBUSxDQUFDOztJQUMxRCxDQUFDO0lBRUQsc0JBQUksaURBQVk7Ozs7UUFBaEI7WUFBQSxpQkFPQztZQU5HLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQ3JCLFNBQVM7Ozs7WUFBQyxVQUFDLE9BQU87O29CQUNSLE1BQU0sR0FBRyxPQUFPLENBQUMsSUFBSTs7OztnQkFBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsQ0FBQyxHQUFHLEtBQUssS0FBSSxDQUFDLEtBQUssRUFBcEIsQ0FBb0IsRUFBQztnQkFDeEQsT0FBTyxFQUFFLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUMxQyxDQUFDLEVBQUMsQ0FDTCxDQUFDO1FBQ04sQ0FBQzs7O09BQUE7SUFDTCw4QkFBQztBQUFELENBQUMsQUFsQkQsQ0FBZ0QscUJBQXFCLEdBa0JwRTs7Ozs7OztJQWpCRyx1Q0FBd0I7O0lBQ3hCLDJDQUFvRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDYXJkVmlld0l0ZW0gfSBmcm9tICcuLi9pbnRlcmZhY2VzL2NhcmQtdmlldy1pdGVtLmludGVyZmFjZSc7XHJcbmltcG9ydCB7IER5bmFtaWNDb21wb25lbnRNb2RlbCB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2R5bmFtaWMtY29tcG9uZW50LW1hcHBlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdCYXNlSXRlbU1vZGVsIH0gZnJvbSAnLi9jYXJkLXZpZXctYmFzZWl0ZW0ubW9kZWwnO1xyXG5pbXBvcnQgeyBDYXJkVmlld1NlbGVjdEl0ZW1Qcm9wZXJ0aWVzLCBDYXJkVmlld1NlbGVjdEl0ZW1PcHRpb24gfSBmcm9tICcuLi9pbnRlcmZhY2VzL2NhcmQtdmlldy5pbnRlcmZhY2VzJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgb2YgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgc3dpdGNoTWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3U2VsZWN0SXRlbU1vZGVsPFQ+IGV4dGVuZHMgQ2FyZFZpZXdCYXNlSXRlbU1vZGVsIGltcGxlbWVudHMgQ2FyZFZpZXdJdGVtLCBEeW5hbWljQ29tcG9uZW50TW9kZWwge1xyXG4gICAgdHlwZTogc3RyaW5nID0gJ3NlbGVjdCc7XHJcbiAgICBvcHRpb25zJDogT2JzZXJ2YWJsZTxDYXJkVmlld1NlbGVjdEl0ZW1PcHRpb248VD5bXT47XHJcblxyXG4gICAgY29uc3RydWN0b3IoY2FyZFZpZXdTZWxlY3RJdGVtUHJvcGVydGllczogQ2FyZFZpZXdTZWxlY3RJdGVtUHJvcGVydGllczxUPikge1xyXG4gICAgICAgIHN1cGVyKGNhcmRWaWV3U2VsZWN0SXRlbVByb3BlcnRpZXMpO1xyXG5cclxuICAgICAgICB0aGlzLm9wdGlvbnMkID0gY2FyZFZpZXdTZWxlY3RJdGVtUHJvcGVydGllcy5vcHRpb25zJDtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZGlzcGxheVZhbHVlKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm9wdGlvbnMkLnBpcGUoXHJcbiAgICAgICAgICAgIHN3aXRjaE1hcCgob3B0aW9ucykgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgb3B0aW9uID0gb3B0aW9ucy5maW5kKChvKSA9PiBvLmtleSA9PT0gdGhpcy52YWx1ZSk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gb2Yob3B0aW9uID8gb3B0aW9uLmxhYmVsIDogJycpO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19