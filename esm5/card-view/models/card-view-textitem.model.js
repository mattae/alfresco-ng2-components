/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CardViewBaseItemModel } from './card-view-baseitem.model';
var CardViewTextItemModel = /** @class */ (function (_super) {
    tslib_1.__extends(CardViewTextItemModel, _super);
    function CardViewTextItemModel(cardViewTextItemProperties) {
        var _this = _super.call(this, cardViewTextItemProperties) || this;
        _this.type = 'text';
        _this.multiline = !!cardViewTextItemProperties.multiline;
        _this.multivalued = !!cardViewTextItemProperties.multivalued;
        _this.pipes = cardViewTextItemProperties.pipes || [];
        _this.clickCallBack = cardViewTextItemProperties.clickCallBack ? cardViewTextItemProperties.clickCallBack : null;
        return _this;
    }
    Object.defineProperty(CardViewTextItemModel.prototype, "displayValue", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.isEmpty()) {
                return this.default;
            }
            else {
                return this.applyPipes(this.value);
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @private
     * @param {?} displayValue
     * @return {?}
     */
    CardViewTextItemModel.prototype.applyPipes = /**
     * @private
     * @param {?} displayValue
     * @return {?}
     */
    function (displayValue) {
        if (this.pipes.length) {
            displayValue = this.pipes.reduce((/**
             * @param {?} accumulator
             * @param {?} __1
             * @return {?}
             */
            function (accumulator, _a) {
                var pipe = _a.pipe, _b = _a.params, params = _b === void 0 ? [] : _b;
                return pipe.transform.apply(pipe, tslib_1.__spread([accumulator], params));
            }), displayValue);
        }
        return displayValue;
    };
    return CardViewTextItemModel;
}(CardViewBaseItemModel));
export { CardViewTextItemModel };
if (false) {
    /** @type {?} */
    CardViewTextItemModel.prototype.type;
    /** @type {?} */
    CardViewTextItemModel.prototype.multiline;
    /** @type {?} */
    CardViewTextItemModel.prototype.multivalued;
    /** @type {?} */
    CardViewTextItemModel.prototype.pipes;
    /** @type {?} */
    CardViewTextItemModel.prototype.clickCallBack;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LXRleHRpdGVtLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiY2FyZC12aWV3L21vZGVscy9jYXJkLXZpZXctdGV4dGl0ZW0ubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBR25FO0lBQTJDLGlEQUFxQjtJQU81RCwrQkFBWSwwQkFBc0Q7UUFBbEUsWUFDSSxrQkFBTSwwQkFBMEIsQ0FBQyxTQUtwQztRQVpELFVBQUksR0FBVyxNQUFNLENBQUM7UUFRbEIsS0FBSSxDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUMsMEJBQTBCLENBQUMsU0FBUyxDQUFDO1FBQ3hELEtBQUksQ0FBQyxXQUFXLEdBQUcsQ0FBQyxDQUFDLDBCQUEwQixDQUFDLFdBQVcsQ0FBQztRQUM1RCxLQUFJLENBQUMsS0FBSyxHQUFHLDBCQUEwQixDQUFDLEtBQUssSUFBSSxFQUFFLENBQUM7UUFDcEQsS0FBSSxDQUFDLGFBQWEsR0FBRywwQkFBMEIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLDBCQUEwQixDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDOztJQUNwSCxDQUFDO0lBRUQsc0JBQUksK0NBQVk7Ozs7UUFBaEI7WUFDSSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFDaEIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO2FBQ3ZCO2lCQUFNO2dCQUNILE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDdEM7UUFDTCxDQUFDOzs7T0FBQTs7Ozs7O0lBRU8sMENBQVU7Ozs7O0lBQWxCLFVBQW1CLFlBQVk7UUFDM0IsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRTtZQUNuQixZQUFZLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNOzs7OztZQUFDLFVBQUMsV0FBVyxFQUFFLEVBQXFCO29CQUFuQixjQUFJLEVBQUUsY0FBVyxFQUFYLGdDQUFXO2dCQUM5RCxPQUFPLElBQUksQ0FBQyxTQUFTLE9BQWQsSUFBSSxvQkFBVyxXQUFXLEdBQUssTUFBTSxHQUFFO1lBQ2xELENBQUMsR0FBRSxZQUFZLENBQUMsQ0FBQztTQUNwQjtRQUVELE9BQU8sWUFBWSxDQUFDO0lBQ3hCLENBQUM7SUFDTCw0QkFBQztBQUFELENBQUMsQUFoQ0QsQ0FBMkMscUJBQXFCLEdBZ0MvRDs7OztJQS9CRyxxQ0FBc0I7O0lBQ3RCLDBDQUFvQjs7SUFDcEIsNENBQXNCOztJQUN0QixzQ0FBdUM7O0lBQ3ZDLDhDQUFvQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDYXJkVmlld0l0ZW0gfSBmcm9tICcuLi9pbnRlcmZhY2VzL2NhcmQtdmlldy1pdGVtLmludGVyZmFjZSc7XHJcbmltcG9ydCB7IER5bmFtaWNDb21wb25lbnRNb2RlbCB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2R5bmFtaWMtY29tcG9uZW50LW1hcHBlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ2FyZFZpZXdCYXNlSXRlbU1vZGVsIH0gZnJvbSAnLi9jYXJkLXZpZXctYmFzZWl0ZW0ubW9kZWwnO1xyXG5pbXBvcnQgeyBDYXJkVmlld1RleHRJdGVtUGlwZVByb3BlcnR5LCBDYXJkVmlld1RleHRJdGVtUHJvcGVydGllcyB9IGZyb20gJy4uL2ludGVyZmFjZXMvY2FyZC12aWV3LmludGVyZmFjZXMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3VGV4dEl0ZW1Nb2RlbCBleHRlbmRzIENhcmRWaWV3QmFzZUl0ZW1Nb2RlbCBpbXBsZW1lbnRzIENhcmRWaWV3SXRlbSwgRHluYW1pY0NvbXBvbmVudE1vZGVsIHtcclxuICAgIHR5cGU6IHN0cmluZyA9ICd0ZXh0JztcclxuICAgIG11bHRpbGluZT86IGJvb2xlYW47XHJcbiAgICBtdWx0aXZhbHVlZD86IGJvb2xlYW47XHJcbiAgICBwaXBlcz86IENhcmRWaWV3VGV4dEl0ZW1QaXBlUHJvcGVydHlbXTtcclxuICAgIGNsaWNrQ2FsbEJhY2s/OiBhbnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoY2FyZFZpZXdUZXh0SXRlbVByb3BlcnRpZXM6IENhcmRWaWV3VGV4dEl0ZW1Qcm9wZXJ0aWVzKSB7XHJcbiAgICAgICAgc3VwZXIoY2FyZFZpZXdUZXh0SXRlbVByb3BlcnRpZXMpO1xyXG4gICAgICAgIHRoaXMubXVsdGlsaW5lID0gISFjYXJkVmlld1RleHRJdGVtUHJvcGVydGllcy5tdWx0aWxpbmU7XHJcbiAgICAgICAgdGhpcy5tdWx0aXZhbHVlZCA9ICEhY2FyZFZpZXdUZXh0SXRlbVByb3BlcnRpZXMubXVsdGl2YWx1ZWQ7XHJcbiAgICAgICAgdGhpcy5waXBlcyA9IGNhcmRWaWV3VGV4dEl0ZW1Qcm9wZXJ0aWVzLnBpcGVzIHx8IFtdO1xyXG4gICAgICAgIHRoaXMuY2xpY2tDYWxsQmFjayA9IGNhcmRWaWV3VGV4dEl0ZW1Qcm9wZXJ0aWVzLmNsaWNrQ2FsbEJhY2sgPyBjYXJkVmlld1RleHRJdGVtUHJvcGVydGllcy5jbGlja0NhbGxCYWNrIDogbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZGlzcGxheVZhbHVlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzRW1wdHkoKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5kZWZhdWx0O1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFwcGx5UGlwZXModGhpcy52YWx1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYXBwbHlQaXBlcyhkaXNwbGF5VmFsdWUpIHtcclxuICAgICAgICBpZiAodGhpcy5waXBlcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgZGlzcGxheVZhbHVlID0gdGhpcy5waXBlcy5yZWR1Y2UoKGFjY3VtdWxhdG9yLCB7IHBpcGUsIHBhcmFtcyA9IFtdIH0pID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBwaXBlLnRyYW5zZm9ybShhY2N1bXVsYXRvciwgLi4ucGFyYW1zKTtcclxuICAgICAgICAgICAgfSwgZGlzcGxheVZhbHVlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBkaXNwbGF5VmFsdWU7XHJcbiAgICB9XHJcbn1cclxuIl19