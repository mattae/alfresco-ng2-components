/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { CardViewDateItemComponent } from '../components/card-view-dateitem/card-view-dateitem.component';
import { CardViewMapItemComponent } from '../components/card-view-mapitem/card-view-mapitem.component';
import { CardViewTextItemComponent } from '../components/card-view-textitem/card-view-textitem.component';
import { CardViewSelectItemComponent } from '../components/card-view-selectitem/card-view-selectitem.component';
import { CardViewBoolItemComponent } from '../components/card-view-boolitem/card-view-boolitem.component';
import { CardViewKeyValuePairsItemComponent } from '../components/card-view-keyvaluepairsitem/card-view-keyvaluepairsitem.component';
import { DynamicComponentMapper, DynamicComponentResolver } from '../../services/dynamic-component-mapper.service';
import * as i0 from "@angular/core";
var CardItemTypeService = /** @class */ (function (_super) {
    tslib_1.__extends(CardItemTypeService, _super);
    function CardItemTypeService() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.defaultValue = CardViewTextItemComponent;
        _this.types = {
            'text': DynamicComponentResolver.fromType(CardViewTextItemComponent),
            'select': DynamicComponentResolver.fromType(CardViewSelectItemComponent),
            'int': DynamicComponentResolver.fromType(CardViewTextItemComponent),
            'float': DynamicComponentResolver.fromType(CardViewTextItemComponent),
            'date': DynamicComponentResolver.fromType(CardViewDateItemComponent),
            'datetime': DynamicComponentResolver.fromType(CardViewDateItemComponent),
            'bool': DynamicComponentResolver.fromType(CardViewBoolItemComponent),
            'map': DynamicComponentResolver.fromType(CardViewMapItemComponent),
            'keyvaluepairs': DynamicComponentResolver.fromType(CardViewKeyValuePairsItemComponent)
        };
        return _this;
    }
    CardItemTypeService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */ CardItemTypeService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function CardItemTypeService_Factory() { return new CardItemTypeService(); }, token: CardItemTypeService, providedIn: "root" });
    return CardItemTypeService;
}(DynamicComponentMapper));
export { CardItemTypeService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    CardItemTypeService.prototype.defaultValue;
    /**
     * @type {?}
     * @protected
     */
    CardItemTypeService.prototype.types;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC1pdGVtLXR5cGVzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJjYXJkLXZpZXcvc2VydmljZXMvY2FyZC1pdGVtLXR5cGVzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQVEsTUFBTSxlQUFlLENBQUM7QUFDakQsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sK0RBQStELENBQUM7QUFDMUcsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sNkRBQTZELENBQUM7QUFDdkcsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sK0RBQStELENBQUM7QUFDMUcsT0FBTyxFQUFFLDJCQUEyQixFQUFFLE1BQU0sbUVBQW1FLENBQUM7QUFDaEgsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sK0RBQStELENBQUM7QUFDMUcsT0FBTyxFQUFFLGtDQUFrQyxFQUFFLE1BQU0saUZBQWlGLENBQUM7QUFDckksT0FBTyxFQUFFLHNCQUFzQixFQUFtQyx3QkFBd0IsRUFBRSxNQUFNLGlEQUFpRCxDQUFDOztBQUVwSjtJQUd5QywrQ0FBc0I7SUFIL0Q7UUFBQSxxRUFrQkM7UUFiYSxrQkFBWSxHQUFhLHlCQUF5QixDQUFDO1FBRW5ELFdBQUssR0FBdUQ7WUFDbEUsTUFBTSxFQUFFLHdCQUF3QixDQUFDLFFBQVEsQ0FBQyx5QkFBeUIsQ0FBQztZQUNwRSxRQUFRLEVBQUUsd0JBQXdCLENBQUMsUUFBUSxDQUFDLDJCQUEyQixDQUFDO1lBQ3hFLEtBQUssRUFBRSx3QkFBd0IsQ0FBQyxRQUFRLENBQUMseUJBQXlCLENBQUM7WUFDbkUsT0FBTyxFQUFFLHdCQUF3QixDQUFDLFFBQVEsQ0FBQyx5QkFBeUIsQ0FBQztZQUNyRSxNQUFNLEVBQUUsd0JBQXdCLENBQUMsUUFBUSxDQUFDLHlCQUF5QixDQUFDO1lBQ3BFLFVBQVUsRUFBRSx3QkFBd0IsQ0FBQyxRQUFRLENBQUMseUJBQXlCLENBQUM7WUFDeEUsTUFBTSxFQUFFLHdCQUF3QixDQUFDLFFBQVEsQ0FBQyx5QkFBeUIsQ0FBQztZQUNwRSxLQUFLLEVBQUUsd0JBQXdCLENBQUMsUUFBUSxDQUFDLHdCQUF3QixDQUFDO1lBQ2xFLGVBQWUsRUFBRSx3QkFBd0IsQ0FBQyxRQUFRLENBQUMsa0NBQWtDLENBQUM7U0FDekYsQ0FBQzs7S0FDTDs7Z0JBbEJBLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs4QkE1QkQ7Q0E0Q0MsQUFsQkQsQ0FHeUMsc0JBQXNCLEdBZTlEO1NBZlksbUJBQW1COzs7Ozs7SUFFNUIsMkNBQTZEOzs7OztJQUU3RCxvQ0FVRSIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlLCBUeXBlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENhcmRWaWV3RGF0ZUl0ZW1Db21wb25lbnQgfSBmcm9tICcuLi9jb21wb25lbnRzL2NhcmQtdmlldy1kYXRlaXRlbS9jYXJkLXZpZXctZGF0ZWl0ZW0uY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ2FyZFZpZXdNYXBJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi4vY29tcG9uZW50cy9jYXJkLXZpZXctbWFwaXRlbS9jYXJkLXZpZXctbWFwaXRlbS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDYXJkVmlld1RleHRJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi4vY29tcG9uZW50cy9jYXJkLXZpZXctdGV4dGl0ZW0vY2FyZC12aWV3LXRleHRpdGVtLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENhcmRWaWV3U2VsZWN0SXRlbUNvbXBvbmVudCB9IGZyb20gJy4uL2NvbXBvbmVudHMvY2FyZC12aWV3LXNlbGVjdGl0ZW0vY2FyZC12aWV3LXNlbGVjdGl0ZW0uY29tcG9uZW50JztcclxuaW1wb3J0IHsgQ2FyZFZpZXdCb29sSXRlbUNvbXBvbmVudCB9IGZyb20gJy4uL2NvbXBvbmVudHMvY2FyZC12aWV3LWJvb2xpdGVtL2NhcmQtdmlldy1ib29saXRlbS5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDYXJkVmlld0tleVZhbHVlUGFpcnNJdGVtQ29tcG9uZW50IH0gZnJvbSAnLi4vY29tcG9uZW50cy9jYXJkLXZpZXcta2V5dmFsdWVwYWlyc2l0ZW0vY2FyZC12aWV3LWtleXZhbHVlcGFpcnNpdGVtLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IER5bmFtaWNDb21wb25lbnRNYXBwZXIsIER5bmFtaWNDb21wb25lbnRSZXNvbHZlRnVuY3Rpb24sIER5bmFtaWNDb21wb25lbnRSZXNvbHZlciB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2R5bmFtaWMtY29tcG9uZW50LW1hcHBlci5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQ2FyZEl0ZW1UeXBlU2VydmljZSBleHRlbmRzIER5bmFtaWNDb21wb25lbnRNYXBwZXIge1xyXG5cclxuICAgIHByb3RlY3RlZCBkZWZhdWx0VmFsdWU6IFR5cGU8e30+ID0gQ2FyZFZpZXdUZXh0SXRlbUNvbXBvbmVudDtcclxuXHJcbiAgICBwcm90ZWN0ZWQgdHlwZXM6IHsgW2tleTogc3RyaW5nXTogRHluYW1pY0NvbXBvbmVudFJlc29sdmVGdW5jdGlvbiB9ID0ge1xyXG4gICAgICAgICd0ZXh0JzogRHluYW1pY0NvbXBvbmVudFJlc29sdmVyLmZyb21UeXBlKENhcmRWaWV3VGV4dEl0ZW1Db21wb25lbnQpLFxyXG4gICAgICAgICdzZWxlY3QnOiBEeW5hbWljQ29tcG9uZW50UmVzb2x2ZXIuZnJvbVR5cGUoQ2FyZFZpZXdTZWxlY3RJdGVtQ29tcG9uZW50KSxcclxuICAgICAgICAnaW50JzogRHluYW1pY0NvbXBvbmVudFJlc29sdmVyLmZyb21UeXBlKENhcmRWaWV3VGV4dEl0ZW1Db21wb25lbnQpLFxyXG4gICAgICAgICdmbG9hdCc6IER5bmFtaWNDb21wb25lbnRSZXNvbHZlci5mcm9tVHlwZShDYXJkVmlld1RleHRJdGVtQ29tcG9uZW50KSxcclxuICAgICAgICAnZGF0ZSc6IER5bmFtaWNDb21wb25lbnRSZXNvbHZlci5mcm9tVHlwZShDYXJkVmlld0RhdGVJdGVtQ29tcG9uZW50KSxcclxuICAgICAgICAnZGF0ZXRpbWUnOiBEeW5hbWljQ29tcG9uZW50UmVzb2x2ZXIuZnJvbVR5cGUoQ2FyZFZpZXdEYXRlSXRlbUNvbXBvbmVudCksXHJcbiAgICAgICAgJ2Jvb2wnOiBEeW5hbWljQ29tcG9uZW50UmVzb2x2ZXIuZnJvbVR5cGUoQ2FyZFZpZXdCb29sSXRlbUNvbXBvbmVudCksXHJcbiAgICAgICAgJ21hcCc6IER5bmFtaWNDb21wb25lbnRSZXNvbHZlci5mcm9tVHlwZShDYXJkVmlld01hcEl0ZW1Db21wb25lbnQpLFxyXG4gICAgICAgICdrZXl2YWx1ZXBhaXJzJzogRHluYW1pY0NvbXBvbmVudFJlc29sdmVyLmZyb21UeXBlKENhcmRWaWV3S2V5VmFsdWVQYWlyc0l0ZW1Db21wb25lbnQpXHJcbiAgICB9O1xyXG59XHJcbiJdfQ==