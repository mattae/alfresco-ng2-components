/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var CardViewItemFloatValidator = /** @class */ (function () {
    function CardViewItemFloatValidator() {
        this.message = 'CORE.CARDVIEW.VALIDATORS.FLOAT_VALIDATION_ERROR';
    }
    /**
     * @param {?} value
     * @return {?}
     */
    CardViewItemFloatValidator.prototype.isValid = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        return !isNaN(parseFloat(value)) && isFinite(value);
    };
    return CardViewItemFloatValidator;
}());
export { CardViewItemFloatValidator };
if (false) {
    /** @type {?} */
    CardViewItemFloatValidator.prototype.message;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWl0ZW0tZmxvYXQudmFsaWRhdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiY2FyZC12aWV3L3ZhbGlkYXRvcnMvY2FyZC12aWV3LWl0ZW0tZmxvYXQudmFsaWRhdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBO0lBQUE7UUFFSSxZQUFPLEdBQUcsaURBQWlELENBQUM7SUFLaEUsQ0FBQzs7Ozs7SUFIRyw0Q0FBTzs7OztJQUFQLFVBQVEsS0FBVTtRQUNkLE9BQU8sQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3hELENBQUM7SUFDTCxpQ0FBQztBQUFELENBQUMsQUFQRCxJQU9DOzs7O0lBTEcsNkNBQTREIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENhcmRWaWV3SXRlbVZhbGlkYXRvciB9IGZyb20gJy4uL2ludGVyZmFjZXMvY2FyZC12aWV3LmludGVyZmFjZXMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIENhcmRWaWV3SXRlbUZsb2F0VmFsaWRhdG9yIGltcGxlbWVudHMgQ2FyZFZpZXdJdGVtVmFsaWRhdG9yIHtcclxuXHJcbiAgICBtZXNzYWdlID0gJ0NPUkUuQ0FSRFZJRVcuVkFMSURBVE9SUy5GTE9BVF9WQUxJREFUSU9OX0VSUk9SJztcclxuXHJcbiAgICBpc1ZhbGlkKHZhbHVlOiBhbnkpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gIWlzTmFOKHBhcnNlRmxvYXQodmFsdWUpKSAmJiBpc0Zpbml0ZSh2YWx1ZSk7XHJcbiAgICB9XHJcbn1cclxuIl19