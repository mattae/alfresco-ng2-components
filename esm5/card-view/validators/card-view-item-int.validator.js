/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var CardViewItemIntValidator = /** @class */ (function () {
    function CardViewItemIntValidator() {
        this.message = 'CORE.CARDVIEW.VALIDATORS.INT_VALIDATION_ERROR';
    }
    /**
     * @param {?} value
     * @return {?}
     */
    CardViewItemIntValidator.prototype.isValid = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        return !isNaN(value) && ((/**
         * @param {?} x
         * @return {?}
         */
        function (x) { return (x | 0) === x; }))(parseFloat(value));
    };
    return CardViewItemIntValidator;
}());
export { CardViewItemIntValidator };
if (false) {
    /** @type {?} */
    CardViewItemIntValidator.prototype.message;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2FyZC12aWV3LWl0ZW0taW50LnZhbGlkYXRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNhcmQtdmlldy92YWxpZGF0b3JzL2NhcmQtdmlldy1pdGVtLWludC52YWxpZGF0b3IudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7SUFBQTtRQUVJLFlBQU8sR0FBRywrQ0FBK0MsQ0FBQztJQUs5RCxDQUFDOzs7OztJQUhHLDBDQUFPOzs7O0lBQVAsVUFBUSxLQUFVO1FBQ2QsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSTs7OztRQUFDLFVBQVMsQ0FBQyxJQUFJLE9BQU8sQ0FBQyxDQUFDLEdBQUcsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFDLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7SUFDdkYsQ0FBQztJQUNMLCtCQUFDO0FBQUQsQ0FBQyxBQVBELElBT0M7Ozs7SUFMRywyQ0FBMEQiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ2FyZFZpZXdJdGVtVmFsaWRhdG9yIH0gZnJvbSAnLi4vaW50ZXJmYWNlcy9jYXJkLXZpZXcuaW50ZXJmYWNlcyc7XHJcblxyXG5leHBvcnQgY2xhc3MgQ2FyZFZpZXdJdGVtSW50VmFsaWRhdG9yIGltcGxlbWVudHMgQ2FyZFZpZXdJdGVtVmFsaWRhdG9yIHtcclxuXHJcbiAgICBtZXNzYWdlID0gJ0NPUkUuQ0FSRFZJRVcuVkFMSURBVE9SUy5JTlRfVkFMSURBVElPTl9FUlJPUic7XHJcblxyXG4gICAgaXNWYWxpZCh2YWx1ZTogYW55KTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuICFpc05hTih2YWx1ZSkgJiYgKGZ1bmN0aW9uKHgpIHsgcmV0dXJuICh4IHwgMCkgPT09IHg7IH0pKHBhcnNlRmxvYXQodmFsdWUpKTtcclxuICAgIH1cclxufVxyXG4iXX0=