/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { EcmUserService } from '../userinfo/services/ecm-user.service';
import { PeopleProcessService } from '../services/people-process.service';
import { UserPreferencesService, UserPreferenceValues } from '../services/user-preferences.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
var CommentListComponent = /** @class */ (function () {
    function CommentListComponent(peopleProcessService, ecmUserService, userPreferenceService) {
        this.peopleProcessService = peopleProcessService;
        this.ecmUserService = ecmUserService;
        this.userPreferenceService = userPreferenceService;
        /**
         * Emitted when the user clicks on one of the comment rows.
         */
        this.clickRow = new EventEmitter();
        this.onDestroy$ = new Subject();
    }
    /**
     * @return {?}
     */
    CommentListComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.userPreferenceService
            .select(UserPreferenceValues.Locale)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} locale
         * @return {?}
         */
        function (locale) { return _this.currentLocale = locale; }));
    };
    /**
     * @return {?}
     */
    CommentListComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    };
    /**
     * @param {?} comment
     * @return {?}
     */
    CommentListComponent.prototype.selectComment = /**
     * @param {?} comment
     * @return {?}
     */
    function (comment) {
        if (this.selectedComment) {
            this.selectedComment.isSelected = false;
        }
        comment.isSelected = true;
        this.selectedComment = comment;
        this.clickRow.emit(this.selectedComment);
    };
    /**
     * @param {?} user
     * @return {?}
     */
    CommentListComponent.prototype.getUserShortName = /**
     * @param {?} user
     * @return {?}
     */
    function (user) {
        /** @type {?} */
        var shortName = '';
        if (user) {
            if (user.firstName) {
                shortName = user.firstName[0].toUpperCase();
            }
            if (user.lastName) {
                shortName += user.lastName[0].toUpperCase();
            }
        }
        return shortName;
    };
    /**
     * @param {?} user
     * @return {?}
     */
    CommentListComponent.prototype.isPictureDefined = /**
     * @param {?} user
     * @return {?}
     */
    function (user) {
        return user.pictureId || user.avatarId;
    };
    /**
     * @param {?} user
     * @return {?}
     */
    CommentListComponent.prototype.getUserImage = /**
     * @param {?} user
     * @return {?}
     */
    function (user) {
        if (this.isAContentUsers(user)) {
            return this.ecmUserService.getUserProfileImage(user.avatarId);
        }
        else {
            return this.peopleProcessService.getUserImage(user);
        }
    };
    /**
     * @private
     * @param {?} user
     * @return {?}
     */
    CommentListComponent.prototype.isAContentUsers = /**
     * @private
     * @param {?} user
     * @return {?}
     */
    function (user) {
        return user.avatarId;
    };
    CommentListComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-comment-list',
                    template: " <mat-list class=\"adf-comment-list\">\r\n    <mat-list-item *ngFor=\"let comment of comments\"\r\n                  (click)=\"selectComment(comment)\"\r\n                  class=\"adf-comment-list-item\"\r\n                  [class.adf-is-selected]=\"comment.isSelected\"\r\n                  id=\"adf-comment-{{comment?.id}}\">\r\n        <div id=\"comment-user-icon\" class=\"adf-comment-img-container\">\r\n            <div\r\n                *ngIf=\"!isPictureDefined(comment.createdBy)\"\r\n                class=\"adf-comment-user-icon\">\r\n                {{getUserShortName(comment.createdBy)}}\r\n            </div>\r\n            <div>\r\n                <img [alt]=\"comment.createdBy\" *ngIf=\"isPictureDefined(comment.createdBy)\"\r\n                      class=\"adf-people-img\"\r\n                     [src]=\"getUserImage(comment.createdBy)\" />\r\n            </div>\r\n        </div>\r\n        <div class=\"adf-comment-contents\">\r\n            <div matLine id=\"comment-user\" class=\"adf-comment-user-name\">\r\n                {{comment.createdBy?.firstName}} {{comment.createdBy?.lastName}}\r\n            </div>\r\n            <div matLine id=\"comment-message\" class=\"adf-comment-message\" [innerHTML]=\"comment.message\"></div>\r\n            <div matLine id=\"comment-time\" class=\"adf-comment-message-time\">\r\n                {{ comment.created | adfTimeAgo: currentLocale }}\r\n            </div>\r\n        </div>\r\n    </mat-list-item>\r\n</mat-list>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    CommentListComponent.ctorParameters = function () { return [
        { type: PeopleProcessService },
        { type: EcmUserService },
        { type: UserPreferencesService }
    ]; };
    CommentListComponent.propDecorators = {
        comments: [{ type: Input }],
        clickRow: [{ type: Output }]
    };
    return CommentListComponent;
}());
export { CommentListComponent };
if (false) {
    /**
     * The comments data used to populate the list.
     * @type {?}
     */
    CommentListComponent.prototype.comments;
    /**
     * Emitted when the user clicks on one of the comment rows.
     * @type {?}
     */
    CommentListComponent.prototype.clickRow;
    /** @type {?} */
    CommentListComponent.prototype.selectedComment;
    /** @type {?} */
    CommentListComponent.prototype.currentLocale;
    /**
     * @type {?}
     * @private
     */
    CommentListComponent.prototype.onDestroy$;
    /** @type {?} */
    CommentListComponent.prototype.peopleProcessService;
    /** @type {?} */
    CommentListComponent.prototype.ecmUserService;
    /** @type {?} */
    CommentListComponent.prototype.userPreferenceService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudC1saXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNvbW1lbnRzL2NvbW1lbnQtbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxpQkFBaUIsRUFBcUIsTUFBTSxlQUFlLENBQUM7QUFFN0csT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQzFFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ3BHLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTNDO0lBcUJJLDhCQUFtQixvQkFBMEMsRUFDMUMsY0FBOEIsRUFDOUIscUJBQTZDO1FBRjdDLHlCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7UUFDMUMsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBd0I7Ozs7UUFSaEUsYUFBUSxHQUErQixJQUFJLFlBQVksRUFBZ0IsQ0FBQztRQUloRSxlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztJQUs1QyxDQUFDOzs7O0lBRUQsdUNBQVE7OztJQUFSO1FBQUEsaUJBS0M7UUFKRyxJQUFJLENBQUMscUJBQXFCO2FBQ3JCLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUM7YUFDbkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDaEMsU0FBUzs7OztRQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsS0FBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLEVBQTNCLENBQTJCLEVBQUMsQ0FBQztJQUMxRCxDQUFDOzs7O0lBRUQsMENBQVc7OztJQUFYO1FBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDOzs7OztJQUVELDRDQUFhOzs7O0lBQWIsVUFBYyxPQUFxQjtRQUMvQixJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDdEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1NBQzNDO1FBQ0QsT0FBTyxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7UUFDMUIsSUFBSSxDQUFDLGVBQWUsR0FBRyxPQUFPLENBQUM7UUFDL0IsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO0lBQzdDLENBQUM7Ozs7O0lBRUQsK0NBQWdCOzs7O0lBQWhCLFVBQWlCLElBQVM7O1lBQ2xCLFNBQVMsR0FBRyxFQUFFO1FBQ2xCLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO2dCQUNoQixTQUFTLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUMvQztZQUNELElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDZixTQUFTLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxXQUFXLEVBQUUsQ0FBQzthQUMvQztTQUNKO1FBQ0QsT0FBTyxTQUFTLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCwrQ0FBZ0I7Ozs7SUFBaEIsVUFBaUIsSUFBUztRQUN0QixPQUFPLElBQUksQ0FBQyxTQUFTLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUMzQyxDQUFDOzs7OztJQUVELDJDQUFZOzs7O0lBQVosVUFBYSxJQUFTO1FBQ2xCLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUM1QixPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsbUJBQW1CLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ2pFO2FBQU07WUFDSCxPQUFPLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7U0FDdkQ7SUFDTCxDQUFDOzs7Ozs7SUFFTyw4Q0FBZTs7Ozs7SUFBdkIsVUFBd0IsSUFBUztRQUM3QixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7SUFDekIsQ0FBQzs7Z0JBMUVKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsa0JBQWtCO29CQUM1QixtK0NBQTRDO29CQUU1QyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7aUJBQ3hDOzs7O2dCQVZRLG9CQUFvQjtnQkFEcEIsY0FBYztnQkFFZCxzQkFBc0I7OzsyQkFjMUIsS0FBSzsyQkFJTCxNQUFNOztJQTZEWCwyQkFBQztDQUFBLEFBM0VELElBMkVDO1NBcEVZLG9CQUFvQjs7Ozs7O0lBRzdCLHdDQUN5Qjs7Ozs7SUFHekIsd0NBQ3dFOztJQUV4RSwrQ0FBOEI7O0lBQzlCLDZDQUFjOzs7OztJQUNkLDBDQUE0Qzs7SUFFaEMsb0RBQWlEOztJQUNqRCw4Q0FBcUM7O0lBQ3JDLHFEQUFvRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIEV2ZW50RW1pdHRlciwgSW5wdXQsIE91dHB1dCwgVmlld0VuY2Fwc3VsYXRpb24sIE9uSW5pdCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbW1lbnRNb2RlbCB9IGZyb20gJy4uL21vZGVscy9jb21tZW50Lm1vZGVsJztcclxuaW1wb3J0IHsgRWNtVXNlclNlcnZpY2UgfSBmcm9tICcuLi91c2VyaW5mby9zZXJ2aWNlcy9lY20tdXNlci5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUGVvcGxlUHJvY2Vzc1NlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9wZW9wbGUtcHJvY2Vzcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXNlclByZWZlcmVuY2VzU2VydmljZSwgVXNlclByZWZlcmVuY2VWYWx1ZXMgfSBmcm9tICcuLi9zZXJ2aWNlcy91c2VyLXByZWZlcmVuY2VzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtY29tbWVudC1saXN0JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9jb21tZW50LWxpc3QuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vY29tbWVudC1saXN0LmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcblxyXG5leHBvcnQgY2xhc3MgQ29tbWVudExpc3RDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcblxyXG4gICAgLyoqIFRoZSBjb21tZW50cyBkYXRhIHVzZWQgdG8gcG9wdWxhdGUgdGhlIGxpc3QuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgY29tbWVudHM6IENvbW1lbnRNb2RlbFtdO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIHVzZXIgY2xpY2tzIG9uIG9uZSBvZiB0aGUgY29tbWVudCByb3dzLiAqL1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICBjbGlja1JvdzogRXZlbnRFbWl0dGVyPENvbW1lbnRNb2RlbD4gPSBuZXcgRXZlbnRFbWl0dGVyPENvbW1lbnRNb2RlbD4oKTtcclxuXHJcbiAgICBzZWxlY3RlZENvbW1lbnQ6IENvbW1lbnRNb2RlbDtcclxuICAgIGN1cnJlbnRMb2NhbGU7XHJcbiAgICBwcml2YXRlIG9uRGVzdHJveSQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBwZW9wbGVQcm9jZXNzU2VydmljZTogUGVvcGxlUHJvY2Vzc1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgZWNtVXNlclNlcnZpY2U6IEVjbVVzZXJTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIHVzZXJQcmVmZXJlbmNlU2VydmljZTogVXNlclByZWZlcmVuY2VzU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5zZWxlY3QoVXNlclByZWZlcmVuY2VWYWx1ZXMuTG9jYWxlKVxyXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5vbkRlc3Ryb3kkKSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZShsb2NhbGUgPT4gdGhpcy5jdXJyZW50TG9jYWxlID0gbG9jYWxlKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQubmV4dCh0cnVlKTtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQuY29tcGxldGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBzZWxlY3RDb21tZW50KGNvbW1lbnQ6IENvbW1lbnRNb2RlbCk6IHZvaWQge1xyXG4gICAgICAgIGlmICh0aGlzLnNlbGVjdGVkQ29tbWVudCkge1xyXG4gICAgICAgICAgICB0aGlzLnNlbGVjdGVkQ29tbWVudC5pc1NlbGVjdGVkID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbW1lbnQuaXNTZWxlY3RlZCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZENvbW1lbnQgPSBjb21tZW50O1xyXG4gICAgICAgIHRoaXMuY2xpY2tSb3cuZW1pdCh0aGlzLnNlbGVjdGVkQ29tbWVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXNlclNob3J0TmFtZSh1c2VyOiBhbnkpOiBzdHJpbmcge1xyXG4gICAgICAgIGxldCBzaG9ydE5hbWUgPSAnJztcclxuICAgICAgICBpZiAodXNlcikge1xyXG4gICAgICAgICAgICBpZiAodXNlci5maXJzdE5hbWUpIHtcclxuICAgICAgICAgICAgICAgIHNob3J0TmFtZSA9IHVzZXIuZmlyc3ROYW1lWzBdLnRvVXBwZXJDYXNlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHVzZXIubGFzdE5hbWUpIHtcclxuICAgICAgICAgICAgICAgIHNob3J0TmFtZSArPSB1c2VyLmxhc3ROYW1lWzBdLnRvVXBwZXJDYXNlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHNob3J0TmFtZTtcclxuICAgIH1cclxuXHJcbiAgICBpc1BpY3R1cmVEZWZpbmVkKHVzZXI6IGFueSk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB1c2VyLnBpY3R1cmVJZCB8fCB1c2VyLmF2YXRhcklkO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFVzZXJJbWFnZSh1c2VyOiBhbnkpOiBzdHJpbmcge1xyXG4gICAgICAgIGlmICh0aGlzLmlzQUNvbnRlbnRVc2Vycyh1c2VyKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5lY21Vc2VyU2VydmljZS5nZXRVc2VyUHJvZmlsZUltYWdlKHVzZXIuYXZhdGFySWQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnBlb3BsZVByb2Nlc3NTZXJ2aWNlLmdldFVzZXJJbWFnZSh1c2VyKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBpc0FDb250ZW50VXNlcnModXNlcjogYW55KTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHVzZXIuYXZhdGFySWQ7XHJcbiAgICB9XHJcbn1cclxuIl19