/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommentProcessService } from '../services/comment-process.service';
import { CommentContentService } from '../services/comment-content.service';
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { share } from 'rxjs/operators';
var CommentsComponent = /** @class */ (function () {
    function CommentsComponent(commentProcessService, commentContentService) {
        var _this = this;
        this.commentProcessService = commentProcessService;
        this.commentContentService = commentContentService;
        /**
         * Are the comments read only?
         */
        this.readOnly = false;
        /**
         * Emitted when an error occurs while displaying/adding a comment.
         */
        this.error = new EventEmitter();
        this.comments = [];
        this.beingAdded = false;
        this.comment$ = new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        function (observer) { return _this.commentObserver = observer; }))
            .pipe(share());
        this.comment$.subscribe((/**
         * @param {?} comment
         * @return {?}
         */
        function (comment) {
            _this.comments.push(comment);
        }));
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    CommentsComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        this.taskId = null;
        this.nodeId = null;
        this.taskId = changes['taskId'] ? changes['taskId'].currentValue : null;
        this.nodeId = changes['nodeId'] ? changes['nodeId'].currentValue : null;
        if (this.taskId || this.nodeId) {
            this.getComments();
        }
        else {
            this.resetComments();
        }
    };
    /**
     * @private
     * @return {?}
     */
    CommentsComponent.prototype.getComments = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        this.resetComments();
        if (this.isATask()) {
            this.commentProcessService.getTaskComments(this.taskId).subscribe((/**
             * @param {?} comments
             * @return {?}
             */
            function (comments) {
                if (comments && comments instanceof Array) {
                    comments = comments.sort((/**
                     * @param {?} comment1
                     * @param {?} comment2
                     * @return {?}
                     */
                    function (comment1, comment2) {
                        /** @type {?} */
                        var date1 = new Date(comment1.created);
                        /** @type {?} */
                        var date2 = new Date(comment2.created);
                        return date1 > date2 ? -1 : date1 < date2 ? 1 : 0;
                    }));
                    comments.forEach((/**
                     * @param {?} currentComment
                     * @return {?}
                     */
                    function (currentComment) {
                        _this.commentObserver.next(currentComment);
                    }));
                }
            }), (/**
             * @param {?} err
             * @return {?}
             */
            function (err) {
                _this.error.emit(err);
            }));
        }
        if (this.isANode()) {
            this.commentContentService.getNodeComments(this.nodeId).subscribe((/**
             * @param {?} comments
             * @return {?}
             */
            function (comments) {
                if (comments && comments instanceof Array) {
                    comments = comments.sort((/**
                     * @param {?} comment1
                     * @param {?} comment2
                     * @return {?}
                     */
                    function (comment1, comment2) {
                        /** @type {?} */
                        var date1 = new Date(comment1.created);
                        /** @type {?} */
                        var date2 = new Date(comment2.created);
                        return date1 > date2 ? -1 : date1 < date2 ? 1 : 0;
                    }));
                    comments.forEach((/**
                     * @param {?} comment
                     * @return {?}
                     */
                    function (comment) {
                        _this.commentObserver.next(comment);
                    }));
                }
            }), (/**
             * @param {?} err
             * @return {?}
             */
            function (err) {
                _this.error.emit(err);
            }));
        }
    };
    /**
     * @private
     * @return {?}
     */
    CommentsComponent.prototype.resetComments = /**
     * @private
     * @return {?}
     */
    function () {
        this.comments = [];
    };
    /**
     * @return {?}
     */
    CommentsComponent.prototype.add = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.message && this.message.trim() && !this.beingAdded) {
            /** @type {?} */
            var comment = this.sanitize(this.message);
            this.beingAdded = true;
            if (this.isATask()) {
                this.commentProcessService.addTaskComment(this.taskId, comment)
                    .subscribe((/**
                 * @param {?} res
                 * @return {?}
                 */
                function (res) {
                    _this.comments.unshift(res);
                    _this.message = '';
                    _this.beingAdded = false;
                }), (/**
                 * @param {?} err
                 * @return {?}
                 */
                function (err) {
                    _this.error.emit(err);
                    _this.beingAdded = false;
                }));
            }
            if (this.isANode()) {
                this.commentContentService.addNodeComment(this.nodeId, comment)
                    .subscribe((/**
                 * @param {?} res
                 * @return {?}
                 */
                function (res) {
                    _this.comments.unshift(res);
                    _this.message = '';
                    _this.beingAdded = false;
                }), (/**
                 * @param {?} err
                 * @return {?}
                 */
                function (err) {
                    _this.error.emit(err);
                    _this.beingAdded = false;
                }));
            }
        }
    };
    /**
     * @return {?}
     */
    CommentsComponent.prototype.clear = /**
     * @return {?}
     */
    function () {
        this.message = '';
    };
    /**
     * @return {?}
     */
    CommentsComponent.prototype.isReadOnly = /**
     * @return {?}
     */
    function () {
        return this.readOnly;
    };
    /**
     * @return {?}
     */
    CommentsComponent.prototype.isATask = /**
     * @return {?}
     */
    function () {
        return this.taskId ? true : false;
    };
    /**
     * @return {?}
     */
    CommentsComponent.prototype.isANode = /**
     * @return {?}
     */
    function () {
        return this.nodeId ? true : false;
    };
    /**
     * @private
     * @param {?} input
     * @return {?}
     */
    CommentsComponent.prototype.sanitize = /**
     * @private
     * @param {?} input
     * @return {?}
     */
    function (input) {
        return input.replace(/<[^>]+>/g, '')
            .replace(/^\s+|\s+$|\s+(?=\s)/g, '')
            .replace(/\r?\n/g, '<br/>');
    };
    CommentsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-comments',
                    template: "<div class=\"adf-comments-container\">\r\n    <div id=\"comment-header\" class=\"adf-comments-header\">\r\n        {{'COMMENTS.HEADER' | translate: { count: comments?.length} }}\r\n    </div>\r\n    <div class=\"adf-comments-input-container\" *ngIf=\"!isReadOnly()\">\r\n            <mat-form-field class=\"adf-full-width\">\r\n                <textarea (keyup.escape)=\"clear()\" matInput id=\"comment-input\" placeholder=\"{{'COMMENTS.ADD' | translate}}\" [(ngModel)]=\"message\"></textarea>\r\n            </mat-form-field>\r\n\r\n            <div class=\"adf-comments-input-actions\">\r\n                <button mat-button\r\n                    class=\"adf-comments-input-add\"\r\n                    data-automation-id=\"comments-input-add\"\r\n                    color=\"primary\"\r\n                    (click)=\"add()\"\r\n                    [disabled]=\"!message\">\r\n                    {{ 'COMMENTS.ADD' | translate }}\r\n                </button>\r\n            </div>\r\n    </div>\r\n\r\n    <div *ngIf=\"comments.length > 0\">\r\n        <adf-comment-list [comments]=\"comments\">\r\n        </adf-comment-list>\r\n    </div>\r\n</div>",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    CommentsComponent.ctorParameters = function () { return [
        { type: CommentProcessService },
        { type: CommentContentService }
    ]; };
    CommentsComponent.propDecorators = {
        taskId: [{ type: Input }],
        nodeId: [{ type: Input }],
        readOnly: [{ type: Input }],
        error: [{ type: Output }]
    };
    return CommentsComponent;
}());
export { CommentsComponent };
if (false) {
    /**
     * The numeric ID of the task.
     * @type {?}
     */
    CommentsComponent.prototype.taskId;
    /**
     * The numeric ID of the node.
     * @type {?}
     */
    CommentsComponent.prototype.nodeId;
    /**
     * Are the comments read only?
     * @type {?}
     */
    CommentsComponent.prototype.readOnly;
    /**
     * Emitted when an error occurs while displaying/adding a comment.
     * @type {?}
     */
    CommentsComponent.prototype.error;
    /** @type {?} */
    CommentsComponent.prototype.comments;
    /**
     * @type {?}
     * @private
     */
    CommentsComponent.prototype.commentObserver;
    /** @type {?} */
    CommentsComponent.prototype.comment$;
    /** @type {?} */
    CommentsComponent.prototype.message;
    /** @type {?} */
    CommentsComponent.prototype.beingAdded;
    /**
     * @type {?}
     * @private
     */
    CommentsComponent.prototype.commentProcessService;
    /**
     * @type {?}
     * @private
     */
    CommentsComponent.prototype.commentContentService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudHMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiY29tbWVudHMvY29tbWVudHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQzVFLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBRTVFLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBYSxNQUFNLEVBQWlCLE1BQU0sZUFBZSxDQUFDO0FBQ2pHLE9BQU8sRUFBRSxVQUFVLEVBQVksTUFBTSxNQUFNLENBQUM7QUFDNUMsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRXZDO0lBZ0NJLDJCQUFvQixxQkFBNEMsRUFBVSxxQkFBNEM7UUFBdEgsaUJBTUM7UUFObUIsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtRQUFVLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBdUI7Ozs7UUFmdEgsYUFBUSxHQUFZLEtBQUssQ0FBQzs7OztRQUkxQixVQUFLLEdBQXNCLElBQUksWUFBWSxFQUFPLENBQUM7UUFFbkQsYUFBUSxHQUFvQixFQUFFLENBQUM7UUFPL0IsZUFBVSxHQUFZLEtBQUssQ0FBQztRQUd4QixJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksVUFBVTs7OztRQUFlLFVBQUMsUUFBUSxJQUFLLE9BQUEsS0FBSSxDQUFDLGVBQWUsR0FBRyxRQUFRLEVBQS9CLENBQStCLEVBQUM7YUFDdEYsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLENBQUM7UUFDbkIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxPQUFxQjtZQUMxQyxLQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUNoQyxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRUQsdUNBQVc7Ozs7SUFBWCxVQUFZLE9BQXNCO1FBQzlCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBQ25CLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDO1FBRW5CLElBQUksQ0FBQyxNQUFNLEdBQUcsT0FBTyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7UUFDeEUsSUFBSSxDQUFDLE1BQU0sR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztRQUV4RSxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUM1QixJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDdEI7YUFBTTtZQUNILElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztTQUN4QjtJQUNMLENBQUM7Ozs7O0lBRU8sdUNBQVc7Ozs7SUFBbkI7UUFBQSxpQkEyQ0M7UUExQ0csSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBQ3JCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRSxFQUFFO1lBQ2hCLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVM7Ozs7WUFDN0QsVUFBQyxRQUF3QjtnQkFDckIsSUFBSSxRQUFRLElBQUksUUFBUSxZQUFZLEtBQUssRUFBRTtvQkFDdkMsUUFBUSxHQUFHLFFBQVEsQ0FBQyxJQUFJOzs7OztvQkFBQyxVQUFDLFFBQXNCLEVBQUUsUUFBc0I7OzRCQUM5RCxLQUFLLEdBQUcsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQzs7NEJBQ2xDLEtBQUssR0FBRyxJQUFJLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDO3dCQUN4QyxPQUFPLEtBQUssR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztvQkFDdEQsQ0FBQyxFQUFDLENBQUM7b0JBQ0gsUUFBUSxDQUFDLE9BQU87Ozs7b0JBQUMsVUFBQyxjQUFjO3dCQUM1QixLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztvQkFDOUMsQ0FBQyxFQUFDLENBQUM7aUJBQ047WUFFTCxDQUFDOzs7O1lBQ0QsVUFBQyxHQUFHO2dCQUNBLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3pCLENBQUMsRUFDSixDQUFDO1NBQ0w7UUFFRCxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBRTtZQUNoQixJQUFJLENBQUMscUJBQXFCLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTOzs7O1lBQzdELFVBQUMsUUFBd0I7Z0JBQ3JCLElBQUksUUFBUSxJQUFJLFFBQVEsWUFBWSxLQUFLLEVBQUU7b0JBRXZDLFFBQVEsR0FBRyxRQUFRLENBQUMsSUFBSTs7Ozs7b0JBQUMsVUFBQyxRQUFzQixFQUFFLFFBQXNCOzs0QkFDOUQsS0FBSyxHQUFHLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUM7OzRCQUNsQyxLQUFLLEdBQUcsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQzt3QkFDeEMsT0FBTyxLQUFLLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7b0JBQ3RELENBQUMsRUFBQyxDQUFDO29CQUNILFFBQVEsQ0FBQyxPQUFPOzs7O29CQUFDLFVBQUMsT0FBTzt3QkFDckIsS0FBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7b0JBQ3ZDLENBQUMsRUFBQyxDQUFDO2lCQUNOO1lBQ0wsQ0FBQzs7OztZQUNELFVBQUMsR0FBRztnQkFDQSxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUN6QixDQUFDLEVBQ0osQ0FBQztTQUNMO0lBQ0wsQ0FBQzs7Ozs7SUFFTyx5Q0FBYTs7OztJQUFyQjtRQUNJLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO0lBQ3ZCLENBQUM7Ozs7SUFFRCwrQkFBRzs7O0lBQUg7UUFBQSxpQkFxQ0M7UUFwQ0csSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFOztnQkFDbkQsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztZQUUzQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztZQUN2QixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFDaEIsSUFBSSxDQUFDLHFCQUFxQixDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQztxQkFDMUQsU0FBUzs7OztnQkFDTixVQUFDLEdBQWlCO29CQUNkLEtBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUMzQixLQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztvQkFDbEIsS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUM7Z0JBRTVCLENBQUM7Ozs7Z0JBQ0QsVUFBQyxHQUFHO29CQUNBLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO29CQUNyQixLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztnQkFDNUIsQ0FBQyxFQUNKLENBQUM7YUFDVDtZQUVELElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUNoQixJQUFJLENBQUMscUJBQXFCLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDO3FCQUMxRCxTQUFTOzs7O2dCQUNOLFVBQUMsR0FBaUI7b0JBQ2QsS0FBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQzNCLEtBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDO29CQUNsQixLQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQztnQkFFNUIsQ0FBQzs7OztnQkFDRCxVQUFDLEdBQUc7b0JBQ0EsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7b0JBQ3JCLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO2dCQUM1QixDQUFDLEVBQ0osQ0FBQzthQUNUO1NBQ0o7SUFDTCxDQUFDOzs7O0lBRUQsaUNBQUs7OztJQUFMO1FBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7SUFDdEIsQ0FBQzs7OztJQUVELHNDQUFVOzs7SUFBVjtRQUNJLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDOzs7O0lBRUQsbUNBQU87OztJQUFQO1FBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUN0QyxDQUFDOzs7O0lBRUQsbUNBQU87OztJQUFQO1FBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUN0QyxDQUFDOzs7Ozs7SUFFTyxvQ0FBUTs7Ozs7SUFBaEIsVUFBaUIsS0FBYTtRQUMxQixPQUFPLEtBQUssQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQzthQUMvQixPQUFPLENBQUMsc0JBQXNCLEVBQUUsRUFBRSxDQUFDO2FBQ25DLE9BQU8sQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDLENBQUM7SUFDcEMsQ0FBQzs7Z0JBbEtKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsY0FBYztvQkFDeEIsOG9DQUF3Qzs7aUJBRTNDOzs7O2dCQVhRLHFCQUFxQjtnQkFDckIscUJBQXFCOzs7eUJBY3pCLEtBQUs7eUJBSUwsS0FBSzsyQkFJTCxLQUFLO3dCQUlMLE1BQU07O0lBK0lYLHdCQUFDO0NBQUEsQUFuS0QsSUFtS0M7U0E5SlksaUJBQWlCOzs7Ozs7SUFHMUIsbUNBQ2U7Ozs7O0lBR2YsbUNBQ2U7Ozs7O0lBR2YscUNBQzBCOzs7OztJQUcxQixrQ0FDbUQ7O0lBRW5ELHFDQUErQjs7Ozs7SUFFL0IsNENBQWdEOztJQUNoRCxxQ0FBbUM7O0lBRW5DLG9DQUFnQjs7SUFFaEIsdUNBQTRCOzs7OztJQUVoQixrREFBb0Q7Ozs7O0lBQUUsa0RBQW9EIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbW1lbnRQcm9jZXNzU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2NvbW1lbnQtcHJvY2Vzcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ29tbWVudENvbnRlbnRTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvY29tbWVudC1jb250ZW50LnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDb21tZW50TW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvY29tbWVudC5tb2RlbCc7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25DaGFuZ2VzLCBPdXRwdXQsIFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgT2JzZXJ2ZXIgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgc2hhcmUgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLWNvbW1lbnRzJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9jb21tZW50cy5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9jb21tZW50cy5jb21wb25lbnQuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb21tZW50c0NvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XHJcblxyXG4gICAgLyoqIFRoZSBudW1lcmljIElEIG9mIHRoZSB0YXNrLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHRhc2tJZDogc3RyaW5nO1xyXG5cclxuICAgIC8qKiBUaGUgbnVtZXJpYyBJRCBvZiB0aGUgbm9kZS4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBub2RlSWQ6IHN0cmluZztcclxuXHJcbiAgICAvKiogQXJlIHRoZSBjb21tZW50cyByZWFkIG9ubHk/ICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgcmVhZE9ubHk6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIGFuIGVycm9yIG9jY3VycyB3aGlsZSBkaXNwbGF5aW5nL2FkZGluZyBhIGNvbW1lbnQuICovXHJcbiAgICBAT3V0cHV0KClcclxuICAgIGVycm9yOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIGNvbW1lbnRzOiBDb21tZW50TW9kZWwgW10gPSBbXTtcclxuXHJcbiAgICBwcml2YXRlIGNvbW1lbnRPYnNlcnZlcjogT2JzZXJ2ZXI8Q29tbWVudE1vZGVsPjtcclxuICAgIGNvbW1lbnQkOiBPYnNlcnZhYmxlPENvbW1lbnRNb2RlbD47XHJcblxyXG4gICAgbWVzc2FnZTogc3RyaW5nO1xyXG5cclxuICAgIGJlaW5nQWRkZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNvbW1lbnRQcm9jZXNzU2VydmljZTogQ29tbWVudFByb2Nlc3NTZXJ2aWNlLCBwcml2YXRlIGNvbW1lbnRDb250ZW50U2VydmljZTogQ29tbWVudENvbnRlbnRTZXJ2aWNlKSB7XHJcbiAgICAgICAgdGhpcy5jb21tZW50JCA9IG5ldyBPYnNlcnZhYmxlPENvbW1lbnRNb2RlbD4oKG9ic2VydmVyKSA9PiB0aGlzLmNvbW1lbnRPYnNlcnZlciA9IG9ic2VydmVyKVxyXG4gICAgICAgICAgICAucGlwZShzaGFyZSgpKTtcclxuICAgICAgICB0aGlzLmNvbW1lbnQkLnN1YnNjcmliZSgoY29tbWVudDogQ29tbWVudE1vZGVsKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuY29tbWVudHMucHVzaChjb21tZW50KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICAgICAgdGhpcy50YXNrSWQgPSBudWxsO1xyXG4gICAgICAgIHRoaXMubm9kZUlkID0gbnVsbDtcclxuXHJcbiAgICAgICAgdGhpcy50YXNrSWQgPSBjaGFuZ2VzWyd0YXNrSWQnXSA/IGNoYW5nZXNbJ3Rhc2tJZCddLmN1cnJlbnRWYWx1ZSA6IG51bGw7XHJcbiAgICAgICAgdGhpcy5ub2RlSWQgPSBjaGFuZ2VzWydub2RlSWQnXSA/IGNoYW5nZXNbJ25vZGVJZCddLmN1cnJlbnRWYWx1ZSA6IG51bGw7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnRhc2tJZCB8fCB0aGlzLm5vZGVJZCkge1xyXG4gICAgICAgICAgICB0aGlzLmdldENvbW1lbnRzKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5yZXNldENvbW1lbnRzKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0Q29tbWVudHMoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5yZXNldENvbW1lbnRzKCk7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNBVGFzaygpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29tbWVudFByb2Nlc3NTZXJ2aWNlLmdldFRhc2tDb21tZW50cyh0aGlzLnRhc2tJZCkuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgKGNvbW1lbnRzOiBDb21tZW50TW9kZWxbXSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChjb21tZW50cyAmJiBjb21tZW50cyBpbnN0YW5jZW9mIEFycmF5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbW1lbnRzID0gY29tbWVudHMuc29ydCgoY29tbWVudDE6IENvbW1lbnRNb2RlbCwgY29tbWVudDI6IENvbW1lbnRNb2RlbCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZGF0ZTEgPSBuZXcgRGF0ZShjb21tZW50MS5jcmVhdGVkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGRhdGUyID0gbmV3IERhdGUoY29tbWVudDIuY3JlYXRlZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZGF0ZTEgPiBkYXRlMiA/IC0xIDogZGF0ZTEgPCBkYXRlMiA/IDEgOiAwO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29tbWVudHMuZm9yRWFjaCgoY3VycmVudENvbW1lbnQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29tbWVudE9ic2VydmVyLm5leHQoY3VycmVudENvbW1lbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIChlcnIpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmVycm9yLmVtaXQoZXJyKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmlzQU5vZGUoKSkge1xyXG4gICAgICAgICAgICB0aGlzLmNvbW1lbnRDb250ZW50U2VydmljZS5nZXROb2RlQ29tbWVudHModGhpcy5ub2RlSWQpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIChjb21tZW50czogQ29tbWVudE1vZGVsW10pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoY29tbWVudHMgJiYgY29tbWVudHMgaW5zdGFuY2VvZiBBcnJheSkge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgY29tbWVudHMgPSBjb21tZW50cy5zb3J0KChjb21tZW50MTogQ29tbWVudE1vZGVsLCBjb21tZW50MjogQ29tbWVudE1vZGVsKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBkYXRlMSA9IG5ldyBEYXRlKGNvbW1lbnQxLmNyZWF0ZWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgZGF0ZTIgPSBuZXcgRGF0ZShjb21tZW50Mi5jcmVhdGVkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBkYXRlMSA+IGRhdGUyID8gLTEgOiBkYXRlMSA8IGRhdGUyID8gMSA6IDA7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb21tZW50cy5mb3JFYWNoKChjb21tZW50KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbW1lbnRPYnNlcnZlci5uZXh0KGNvbW1lbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKGVycikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZXJyb3IuZW1pdChlcnIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHJlc2V0Q29tbWVudHMoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5jb21tZW50cyA9IFtdO1xyXG4gICAgfVxyXG5cclxuICAgIGFkZCgpOiB2b2lkIHtcclxuICAgICAgICBpZiAodGhpcy5tZXNzYWdlICYmIHRoaXMubWVzc2FnZS50cmltKCkgJiYgIXRoaXMuYmVpbmdBZGRlZCkge1xyXG4gICAgICAgICAgICBjb25zdCBjb21tZW50ID0gdGhpcy5zYW5pdGl6ZSh0aGlzLm1lc3NhZ2UpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5iZWluZ0FkZGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgaWYgKHRoaXMuaXNBVGFzaygpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbW1lbnRQcm9jZXNzU2VydmljZS5hZGRUYXNrQ29tbWVudCh0aGlzLnRhc2tJZCwgY29tbWVudClcclxuICAgICAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAocmVzOiBDb21tZW50TW9kZWwpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29tbWVudHMudW5zaGlmdChyZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5tZXNzYWdlID0gJyc7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmJlaW5nQWRkZWQgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChlcnIpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZXJyb3IuZW1pdChlcnIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5iZWluZ0FkZGVkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5pc0FOb2RlKCkpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY29tbWVudENvbnRlbnRTZXJ2aWNlLmFkZE5vZGVDb21tZW50KHRoaXMubm9kZUlkLCBjb21tZW50KVxyXG4gICAgICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChyZXM6IENvbW1lbnRNb2RlbCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jb21tZW50cy51bnNoaWZ0KHJlcyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm1lc3NhZ2UgPSAnJztcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYmVpbmdBZGRlZCA9IGZhbHNlO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgKGVycikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lcnJvci5lbWl0KGVycik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmJlaW5nQWRkZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY2xlYXIoKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5tZXNzYWdlID0gJyc7XHJcbiAgICB9XHJcblxyXG4gICAgaXNSZWFkT25seSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5yZWFkT25seTtcclxuICAgIH1cclxuXHJcbiAgICBpc0FUYXNrKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnRhc2tJZCA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBpc0FOb2RlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm5vZGVJZCA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHNhbml0aXplKGlucHV0OiBzdHJpbmcpIHtcclxuICAgICAgICByZXR1cm4gaW5wdXQucmVwbGFjZSgvPFtePl0rPi9nLCAnJylcclxuICAgICAgICAgICAgLnJlcGxhY2UoL15cXHMrfFxccyskfFxccysoPz1cXHMpL2csICcnKVxyXG4gICAgICAgICAgICAucmVwbGFjZSgvXFxyP1xcbi9nLCAnPGJyLz4nKTtcclxuICAgIH1cclxufVxyXG4iXX0=