/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { OverlayContainer } from '@angular/cdk/overlay';
import { ViewportRuler } from '@angular/cdk/scrolling';
import { Component, HostListener, Input, Renderer2, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material';
import { ContextMenuService } from './context-menu.service';
var ContextMenuHolderComponent = /** @class */ (function () {
    function ContextMenuHolderComponent(viewport, overlayContainer, contextMenuService, renderer) {
        this.viewport = viewport;
        this.overlayContainer = overlayContainer;
        this.contextMenuService = contextMenuService;
        this.renderer = renderer;
        this.links = [];
        this.mouseLocation = { left: 0, top: 0 };
        this.menuElement = null;
        this.subscriptions = [];
        this.showIcons = false;
    }
    /**
     * @param {?=} event
     * @return {?}
     */
    ContextMenuHolderComponent.prototype.onShowContextMenu = /**
     * @param {?=} event
     * @return {?}
     */
    function (event) {
        if (event) {
            event.preventDefault();
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ContextMenuHolderComponent.prototype.onResize = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.mdMenuElement) {
            this.updatePosition();
        }
    };
    /**
     * @return {?}
     */
    ContextMenuHolderComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.subscriptions.push(this.contextMenuService.show.subscribe((/**
         * @param {?} mouseEvent
         * @return {?}
         */
        function (mouseEvent) { return _this.showMenu(mouseEvent.event, mouseEvent.obj); })), this.menuTrigger.menuOpened.subscribe((/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var container = _this.overlayContainer.getContainerElement();
            if (container) {
                _this.contextMenuListenerFn = _this.renderer.listen(container, 'contextmenu', (/**
                 * @param {?} contextmenuEvent
                 * @return {?}
                 */
                function (contextmenuEvent) {
                    contextmenuEvent.preventDefault();
                }));
            }
            _this.menuElement = _this.getContextMenuElement();
        })), this.menuTrigger.menuClosed.subscribe((/**
         * @return {?}
         */
        function () {
            _this.menuElement = null;
            if (_this.contextMenuListenerFn) {
                _this.contextMenuListenerFn();
            }
        })));
    };
    /**
     * @return {?}
     */
    ContextMenuHolderComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.contextMenuListenerFn) {
            this.contextMenuListenerFn();
        }
        this.subscriptions.forEach((/**
         * @param {?} subscription
         * @return {?}
         */
        function (subscription) { return subscription.unsubscribe(); }));
        this.subscriptions = [];
        this.menuElement = null;
    };
    /**
     * @param {?} event
     * @param {?} menuItem
     * @return {?}
     */
    ContextMenuHolderComponent.prototype.onMenuItemClick = /**
     * @param {?} event
     * @param {?} menuItem
     * @return {?}
     */
    function (event, menuItem) {
        if (menuItem && menuItem.model && menuItem.model.disabled) {
            event.preventDefault();
            event.stopImmediatePropagation();
            return;
        }
        menuItem.subject.next(menuItem);
    };
    /**
     * @param {?} mouseEvent
     * @param {?} links
     * @return {?}
     */
    ContextMenuHolderComponent.prototype.showMenu = /**
     * @param {?} mouseEvent
     * @param {?} links
     * @return {?}
     */
    function (mouseEvent, links) {
        this.links = links;
        if (mouseEvent) {
            this.mouseLocation = {
                left: mouseEvent.clientX,
                top: mouseEvent.clientY
            };
        }
        this.menuTrigger.openMenu();
        if (this.mdMenuElement) {
            this.updatePosition();
        }
    };
    Object.defineProperty(ContextMenuHolderComponent.prototype, "mdMenuElement", {
        get: /**
         * @return {?}
         */
        function () {
            return this.menuElement;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @private
     * @return {?}
     */
    ContextMenuHolderComponent.prototype.locationCss = /**
     * @private
     * @return {?}
     */
    function () {
        return {
            left: this.mouseLocation.left + 'px',
            top: this.mouseLocation.top + 'px'
        };
    };
    /**
     * @private
     * @return {?}
     */
    ContextMenuHolderComponent.prototype.updatePosition = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        setTimeout((/**
         * @return {?}
         */
        function () {
            if (_this.mdMenuElement.parentElement) {
                if (_this.mdMenuElement.clientWidth + _this.mouseLocation.left > _this.viewport.getViewportRect().width) {
                    _this.menuTrigger.menu.xPosition = 'before';
                    _this.mdMenuElement.parentElement.style.left = _this.mouseLocation.left - _this.mdMenuElement.clientWidth + 'px';
                }
                else {
                    _this.menuTrigger.menu.xPosition = 'after';
                    _this.mdMenuElement.parentElement.style.left = _this.locationCss().left;
                }
                if (_this.mdMenuElement.clientHeight + _this.mouseLocation.top > _this.viewport.getViewportRect().height) {
                    _this.menuTrigger.menu.yPosition = 'above';
                    _this.mdMenuElement.parentElement.style.top = _this.mouseLocation.top - _this.mdMenuElement.clientHeight + 'px';
                }
                else {
                    _this.menuTrigger.menu.yPosition = 'below';
                    _this.mdMenuElement.parentElement.style.top = _this.locationCss().top;
                }
            }
        }), 0);
    };
    /**
     * @private
     * @return {?}
     */
    ContextMenuHolderComponent.prototype.getContextMenuElement = /**
     * @private
     * @return {?}
     */
    function () {
        return this.overlayContainer.getContainerElement().querySelector('.context-menu');
    };
    ContextMenuHolderComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-context-menu-holder',
                    template: "\n        <button mat-button [matMenuTriggerFor]=\"contextMenu\"></button>\n        <mat-menu #contextMenu=\"matMenu\" class=\"context-menu\">\n            <ng-container *ngFor=\"let link of links\">\n                <button *ngIf=\"link.model?.visible\"\n                        [attr.data-automation-id]=\"'context-'+((link.title || link.model?.title) | translate)\"\n                        mat-menu-item\n                        [disabled]=\"link.model?.disabled\"\n                        (click)=\"onMenuItemClick($event, link)\">\n                    <mat-icon *ngIf=\"showIcons && link.model?.icon\">{{ link.model.icon }}</mat-icon>\n                    {{ (link.title || link.model?.title) | translate }}\n                </button>\n            </ng-container>\n        </mat-menu>\n    "
                }] }
    ];
    /** @nocollapse */
    ContextMenuHolderComponent.ctorParameters = function () { return [
        { type: ViewportRuler },
        { type: OverlayContainer },
        { type: ContextMenuService },
        { type: Renderer2 }
    ]; };
    ContextMenuHolderComponent.propDecorators = {
        showIcons: [{ type: Input }],
        menuTrigger: [{ type: ViewChild, args: [MatMenuTrigger, { static: true },] }],
        onShowContextMenu: [{ type: HostListener, args: ['contextmenu', ['$event'],] }],
        onResize: [{ type: HostListener, args: ['window:resize', ['$event'],] }]
    };
    return ContextMenuHolderComponent;
}());
export { ContextMenuHolderComponent };
if (false) {
    /** @type {?} */
    ContextMenuHolderComponent.prototype.links;
    /**
     * @type {?}
     * @private
     */
    ContextMenuHolderComponent.prototype.mouseLocation;
    /**
     * @type {?}
     * @private
     */
    ContextMenuHolderComponent.prototype.menuElement;
    /**
     * @type {?}
     * @private
     */
    ContextMenuHolderComponent.prototype.subscriptions;
    /**
     * @type {?}
     * @private
     */
    ContextMenuHolderComponent.prototype.contextMenuListenerFn;
    /** @type {?} */
    ContextMenuHolderComponent.prototype.showIcons;
    /** @type {?} */
    ContextMenuHolderComponent.prototype.menuTrigger;
    /**
     * @type {?}
     * @private
     */
    ContextMenuHolderComponent.prototype.viewport;
    /**
     * @type {?}
     * @private
     */
    ContextMenuHolderComponent.prototype.overlayContainer;
    /**
     * @type {?}
     * @private
     */
    ContextMenuHolderComponent.prototype.contextMenuService;
    /**
     * @type {?}
     * @private
     */
    ContextMenuHolderComponent.prototype.renderer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGV4dC1tZW51LWhvbGRlci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJjb250ZXh0LW1lbnUvY29udGV4dC1tZW51LWhvbGRlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUNBLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN2RCxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQXFCLFNBQVMsRUFBRSxTQUFTLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDeEcsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBRW5ELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBRTVEO0lBOENJLG9DQUNZLFFBQXVCLEVBQ3ZCLGdCQUFrQyxFQUNsQyxrQkFBc0MsRUFDdEMsUUFBbUI7UUFIbkIsYUFBUSxHQUFSLFFBQVEsQ0FBZTtRQUN2QixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsYUFBUSxHQUFSLFFBQVEsQ0FBVztRQS9CL0IsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUVILGtCQUFhLEdBQWtDLEVBQUMsSUFBSSxFQUFFLENBQUMsRUFBRSxHQUFHLEVBQUUsQ0FBQyxFQUFDLENBQUM7UUFDakUsZ0JBQVcsR0FBRyxJQUFJLENBQUM7UUFDbkIsa0JBQWEsR0FBbUIsRUFBRSxDQUFDO1FBSTNDLGNBQVMsR0FBWSxLQUFLLENBQUM7SUF5QjNCLENBQUM7Ozs7O0lBbkJELHNEQUFpQjs7OztJQURqQixVQUNrQixLQUFrQjtRQUNoQyxJQUFJLEtBQUssRUFBRTtZQUNQLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUMxQjtJQUNMLENBQUM7Ozs7O0lBR0QsNkNBQVE7Ozs7SUFEUixVQUNTLEtBQUs7UUFDVixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDcEIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1NBQ3pCO0lBQ0wsQ0FBQzs7OztJQVVELDZDQUFROzs7SUFBUjtRQUFBLGlCQXFCQztRQXBCRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FDbkIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxVQUFVLElBQUssT0FBQSxLQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUEvQyxDQUErQyxFQUFDLEVBRXZHLElBQUksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLFNBQVM7OztRQUFDOztnQkFDNUIsU0FBUyxHQUFHLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxtQkFBbUIsRUFBRTtZQUM3RCxJQUFJLFNBQVMsRUFBRTtnQkFDWCxLQUFJLENBQUMscUJBQXFCLEdBQUcsS0FBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLGFBQWE7Ozs7Z0JBQUUsVUFBQyxnQkFBdUI7b0JBQ2hHLGdCQUFnQixDQUFDLGNBQWMsRUFBRSxDQUFDO2dCQUN0QyxDQUFDLEVBQUMsQ0FBQzthQUNOO1lBQ0QsS0FBSSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMscUJBQXFCLEVBQUUsQ0FBQztRQUNwRCxDQUFDLEVBQUMsRUFFRixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxTQUFTOzs7UUFBQztZQUNsQyxLQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztZQUN4QixJQUFJLEtBQUksQ0FBQyxxQkFBcUIsRUFBRTtnQkFDNUIsS0FBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7YUFDaEM7UUFDTCxDQUFDLEVBQUMsQ0FDTCxDQUFDO0lBQ04sQ0FBQzs7OztJQUVELGdEQUFXOzs7SUFBWDtRQUNJLElBQUksSUFBSSxDQUFDLHFCQUFxQixFQUFFO1lBQzVCLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1NBQ2hDO1FBRUQsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQyxZQUFZLElBQUssT0FBQSxZQUFZLENBQUMsV0FBVyxFQUFFLEVBQTFCLENBQTBCLEVBQUMsQ0FBQztRQUN6RSxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztRQUV4QixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztJQUM1QixDQUFDOzs7Ozs7SUFFRCxvREFBZTs7Ozs7SUFBZixVQUFnQixLQUFZLEVBQUUsUUFBYTtRQUN2QyxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsS0FBSyxJQUFJLFFBQVEsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFO1lBQ3ZELEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN2QixLQUFLLENBQUMsd0JBQXdCLEVBQUUsQ0FBQztZQUNqQyxPQUFPO1NBQ1Y7UUFDRCxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNwQyxDQUFDOzs7Ozs7SUFFRCw2Q0FBUTs7Ozs7SUFBUixVQUFTLFVBQVUsRUFBRSxLQUFLO1FBQ3RCLElBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1FBRW5CLElBQUksVUFBVSxFQUFFO1lBQ1osSUFBSSxDQUFDLGFBQWEsR0FBRztnQkFDakIsSUFBSSxFQUFFLFVBQVUsQ0FBQyxPQUFPO2dCQUN4QixHQUFHLEVBQUUsVUFBVSxDQUFDLE9BQU87YUFDMUIsQ0FBQztTQUNMO1FBRUQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEVBQUUsQ0FBQztRQUU1QixJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUU7WUFDcEIsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1NBQ3pCO0lBQ0wsQ0FBQztJQUVELHNCQUFJLHFEQUFhOzs7O1FBQWpCO1lBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO1FBQzVCLENBQUM7OztPQUFBOzs7OztJQUVPLGdEQUFXOzs7O0lBQW5CO1FBQ0ksT0FBTztZQUNILElBQUksRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksR0FBRyxJQUFJO1lBQ3BDLEdBQUcsRUFBRSxJQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsR0FBRyxJQUFJO1NBQ3JDLENBQUM7SUFDTixDQUFDOzs7OztJQUVPLG1EQUFjOzs7O0lBQXRCO1FBQUEsaUJBb0JDO1FBbkJHLFVBQVU7OztRQUFDO1lBQ1AsSUFBSSxLQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsRUFBRTtnQkFDbEMsSUFBSSxLQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsR0FBRyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksR0FBRyxLQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxDQUFDLEtBQUssRUFBRTtvQkFDbEcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQztvQkFDM0MsS0FBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLElBQUksR0FBRyxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksR0FBRyxLQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7aUJBQ2pIO3FCQUFNO29CQUNILEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUM7b0JBQzFDLEtBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQztpQkFDekU7Z0JBRUQsSUFBSSxLQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsR0FBRyxLQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsRUFBRSxDQUFDLE1BQU0sRUFBRTtvQkFDbkcsS0FBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQztvQkFDMUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxLQUFJLENBQUMsYUFBYSxDQUFDLEdBQUcsR0FBRyxLQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7aUJBQ2hIO3FCQUFNO29CQUNILEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxPQUFPLENBQUM7b0JBQzFDLEtBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxHQUFHLEdBQUcsS0FBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLEdBQUcsQ0FBQztpQkFDdkU7YUFDSjtRQUNMLENBQUMsR0FBRSxDQUFDLENBQUMsQ0FBQztJQUNWLENBQUM7Ozs7O0lBRU8sMERBQXFCOzs7O0lBQTdCO1FBQ0ksT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLEVBQUUsQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLENBQUM7SUFDdEYsQ0FBQzs7Z0JBckpKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUseUJBQXlCO29CQUNuQyxRQUFRLEVBQUUsOHhCQWNUO2lCQUNKOzs7O2dCQXZCUSxhQUFhO2dCQURiLGdCQUFnQjtnQkFLaEIsa0JBQWtCO2dCQUhpQyxTQUFTOzs7NEJBK0JoRSxLQUFLOzhCQUdMLFNBQVMsU0FBQyxjQUFjLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDO29DQUd4QyxZQUFZLFNBQUMsYUFBYSxFQUFFLENBQUMsUUFBUSxDQUFDOzJCQU90QyxZQUFZLFNBQUMsZUFBZSxFQUFFLENBQUMsUUFBUSxDQUFDOztJQStHN0MsaUNBQUM7Q0FBQSxBQXRKRCxJQXNKQztTQXBJWSwwQkFBMEI7OztJQUNuQywyQ0FBVzs7Ozs7SUFFWCxtREFBeUU7Ozs7O0lBQ3pFLGlEQUEyQjs7Ozs7SUFDM0IsbURBQTJDOzs7OztJQUMzQywyREFBMEM7O0lBRTFDLCtDQUMyQjs7SUFFM0IsaURBQzRCOzs7OztJQWlCeEIsOENBQStCOzs7OztJQUMvQixzREFBMEM7Ozs7O0lBQzFDLHdEQUE4Qzs7Ozs7SUFDOUMsOENBQTJCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXG5cbi8qIVxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXG4gKlxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuICpcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbiAqXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxuICovXG5cbmltcG9ydCB7IE92ZXJsYXlDb250YWluZXIgfSBmcm9tICdAYW5ndWxhci9jZGsvb3ZlcmxheSc7XG5pbXBvcnQgeyBWaWV3cG9ydFJ1bGVyIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL3Njcm9sbGluZyc7XG5pbXBvcnQgeyBDb21wb25lbnQsIEhvc3RMaXN0ZW5lciwgSW5wdXQsIE9uRGVzdHJveSwgT25Jbml0LCBSZW5kZXJlcjIsIFZpZXdDaGlsZCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTWF0TWVudVRyaWdnZXIgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQgeyBTdWJzY3JpcHRpb24gfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IENvbnRleHRNZW51U2VydmljZSB9IGZyb20gJy4vY29udGV4dC1tZW51LnNlcnZpY2UnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2FkZi1jb250ZXh0LW1lbnUtaG9sZGVyJyxcbiAgICB0ZW1wbGF0ZTogYFxuICAgICAgICA8YnV0dG9uIG1hdC1idXR0b24gW21hdE1lbnVUcmlnZ2VyRm9yXT1cImNvbnRleHRNZW51XCI+PC9idXR0b24+XG4gICAgICAgIDxtYXQtbWVudSAjY29udGV4dE1lbnU9XCJtYXRNZW51XCIgY2xhc3M9XCJjb250ZXh0LW1lbnVcIj5cbiAgICAgICAgICAgIDxuZy1jb250YWluZXIgKm5nRm9yPVwibGV0IGxpbmsgb2YgbGlua3NcIj5cbiAgICAgICAgICAgICAgICA8YnV0dG9uICpuZ0lmPVwibGluay5tb2RlbD8udmlzaWJsZVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBbYXR0ci5kYXRhLWF1dG9tYXRpb24taWRdPVwiJ2NvbnRleHQtJysoKGxpbmsudGl0bGUgfHwgbGluay5tb2RlbD8udGl0bGUpIHwgdHJhbnNsYXRlKVwiXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXQtbWVudS1pdGVtXG4gICAgICAgICAgICAgICAgICAgICAgICBbZGlzYWJsZWRdPVwibGluay5tb2RlbD8uZGlzYWJsZWRcIlxuICAgICAgICAgICAgICAgICAgICAgICAgKGNsaWNrKT1cIm9uTWVudUl0ZW1DbGljaygkZXZlbnQsIGxpbmspXCI+XG4gICAgICAgICAgICAgICAgICAgIDxtYXQtaWNvbiAqbmdJZj1cInNob3dJY29ucyAmJiBsaW5rLm1vZGVsPy5pY29uXCI+e3sgbGluay5tb2RlbC5pY29uIH19PC9tYXQtaWNvbj5cbiAgICAgICAgICAgICAgICAgICAge3sgKGxpbmsudGl0bGUgfHwgbGluay5tb2RlbD8udGl0bGUpIHwgdHJhbnNsYXRlIH19XG4gICAgICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICA8L25nLWNvbnRhaW5lcj5cbiAgICAgICAgPC9tYXQtbWVudT5cbiAgICBgXG59KVxuZXhwb3J0IGNsYXNzIENvbnRleHRNZW51SG9sZGVyQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xuICAgIGxpbmtzID0gW107XG5cbiAgICBwcml2YXRlIG1vdXNlTG9jYXRpb246IHsgbGVmdDogbnVtYmVyLCB0b3A6IG51bWJlciB9ID0ge2xlZnQ6IDAsIHRvcDogMH07XG4gICAgcHJpdmF0ZSBtZW51RWxlbWVudCA9IG51bGw7XG4gICAgcHJpdmF0ZSBzdWJzY3JpcHRpb25zOiBTdWJzY3JpcHRpb25bXSA9IFtdO1xuICAgIHByaXZhdGUgY29udGV4dE1lbnVMaXN0ZW5lckZuOiAoKSA9PiB2b2lkO1xuXG4gICAgQElucHV0KClcbiAgICBzaG93SWNvbnM6IGJvb2xlYW4gPSBmYWxzZTtcblxuICAgIEBWaWV3Q2hpbGQoTWF0TWVudVRyaWdnZXIsIHtzdGF0aWM6IHRydWV9KVxuICAgIG1lbnVUcmlnZ2VyOiBNYXRNZW51VHJpZ2dlcjtcblxuICAgIEBIb3N0TGlzdGVuZXIoJ2NvbnRleHRtZW51JywgWyckZXZlbnQnXSlcbiAgICBvblNob3dDb250ZXh0TWVudShldmVudD86IE1vdXNlRXZlbnQpIHtcbiAgICAgICAgaWYgKGV2ZW50KSB7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgQEhvc3RMaXN0ZW5lcignd2luZG93OnJlc2l6ZScsIFsnJGV2ZW50J10pXG4gICAgb25SZXNpemUoZXZlbnQpIHtcbiAgICAgICAgaWYgKHRoaXMubWRNZW51RWxlbWVudCkge1xuICAgICAgICAgICAgdGhpcy51cGRhdGVQb3NpdGlvbigpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgY29uc3RydWN0b3IoXG4gICAgICAgIHByaXZhdGUgdmlld3BvcnQ6IFZpZXdwb3J0UnVsZXIsXG4gICAgICAgIHByaXZhdGUgb3ZlcmxheUNvbnRhaW5lcjogT3ZlcmxheUNvbnRhaW5lcixcbiAgICAgICAgcHJpdmF0ZSBjb250ZXh0TWVudVNlcnZpY2U6IENvbnRleHRNZW51U2VydmljZSxcbiAgICAgICAgcHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyXG4gICAgKSB7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5wdXNoKFxuICAgICAgICAgICAgdGhpcy5jb250ZXh0TWVudVNlcnZpY2Uuc2hvdy5zdWJzY3JpYmUoKG1vdXNlRXZlbnQpID0+IHRoaXMuc2hvd01lbnUobW91c2VFdmVudC5ldmVudCwgbW91c2VFdmVudC5vYmopKSxcblxuICAgICAgICAgICAgdGhpcy5tZW51VHJpZ2dlci5tZW51T3BlbmVkLnN1YnNjcmliZSgoKSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3QgY29udGFpbmVyID0gdGhpcy5vdmVybGF5Q29udGFpbmVyLmdldENvbnRhaW5lckVsZW1lbnQoKTtcbiAgICAgICAgICAgICAgICBpZiAoY29udGFpbmVyKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29udGV4dE1lbnVMaXN0ZW5lckZuID0gdGhpcy5yZW5kZXJlci5saXN0ZW4oY29udGFpbmVyLCAnY29udGV4dG1lbnUnLCAoY29udGV4dG1lbnVFdmVudDogRXZlbnQpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnRleHRtZW51RXZlbnQucHJldmVudERlZmF1bHQoKTtcbiAgICAgICAgICAgICAgICAgICAgfSk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHRoaXMubWVudUVsZW1lbnQgPSB0aGlzLmdldENvbnRleHRNZW51RWxlbWVudCgpO1xuICAgICAgICAgICAgfSksXG5cbiAgICAgICAgICAgIHRoaXMubWVudVRyaWdnZXIubWVudUNsb3NlZC5zdWJzY3JpYmUoKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMubWVudUVsZW1lbnQgPSBudWxsO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLmNvbnRleHRNZW51TGlzdGVuZXJGbikge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRleHRNZW51TGlzdGVuZXJGbigpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0pXG4gICAgICAgICk7XG4gICAgfVxuXG4gICAgbmdPbkRlc3Ryb3koKSB7XG4gICAgICAgIGlmICh0aGlzLmNvbnRleHRNZW51TGlzdGVuZXJGbikge1xuICAgICAgICAgICAgdGhpcy5jb250ZXh0TWVudUxpc3RlbmVyRm4oKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5mb3JFYWNoKChzdWJzY3JpcHRpb24pID0+IHN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpKTtcbiAgICAgICAgdGhpcy5zdWJzY3JpcHRpb25zID0gW107XG5cbiAgICAgICAgdGhpcy5tZW51RWxlbWVudCA9IG51bGw7XG4gICAgfVxuXG4gICAgb25NZW51SXRlbUNsaWNrKGV2ZW50OiBFdmVudCwgbWVudUl0ZW06IGFueSk6IHZvaWQge1xuICAgICAgICBpZiAobWVudUl0ZW0gJiYgbWVudUl0ZW0ubW9kZWwgJiYgbWVudUl0ZW0ubW9kZWwuZGlzYWJsZWQpIHtcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICBldmVudC5zdG9wSW1tZWRpYXRlUHJvcGFnYXRpb24oKTtcbiAgICAgICAgICAgIHJldHVybjtcbiAgICAgICAgfVxuICAgICAgICBtZW51SXRlbS5zdWJqZWN0Lm5leHQobWVudUl0ZW0pO1xuICAgIH1cblxuICAgIHNob3dNZW51KG1vdXNlRXZlbnQsIGxpbmtzKSB7XG4gICAgICAgIHRoaXMubGlua3MgPSBsaW5rcztcblxuICAgICAgICBpZiAobW91c2VFdmVudCkge1xuICAgICAgICAgICAgdGhpcy5tb3VzZUxvY2F0aW9uID0ge1xuICAgICAgICAgICAgICAgIGxlZnQ6IG1vdXNlRXZlbnQuY2xpZW50WCxcbiAgICAgICAgICAgICAgICB0b3A6IG1vdXNlRXZlbnQuY2xpZW50WVxuICAgICAgICAgICAgfTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMubWVudVRyaWdnZXIub3Blbk1lbnUoKTtcblxuICAgICAgICBpZiAodGhpcy5tZE1lbnVFbGVtZW50KSB7XG4gICAgICAgICAgICB0aGlzLnVwZGF0ZVBvc2l0aW9uKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBnZXQgbWRNZW51RWxlbWVudCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubWVudUVsZW1lbnQ7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBsb2NhdGlvbkNzcygpIHtcbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGxlZnQ6IHRoaXMubW91c2VMb2NhdGlvbi5sZWZ0ICsgJ3B4JyxcbiAgICAgICAgICAgIHRvcDogdGhpcy5tb3VzZUxvY2F0aW9uLnRvcCArICdweCdcbiAgICAgICAgfTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHVwZGF0ZVBvc2l0aW9uKCkge1xuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcbiAgICAgICAgICAgIGlmICh0aGlzLm1kTWVudUVsZW1lbnQucGFyZW50RWxlbWVudCkge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLm1kTWVudUVsZW1lbnQuY2xpZW50V2lkdGggKyB0aGlzLm1vdXNlTG9jYXRpb24ubGVmdCA+IHRoaXMudmlld3BvcnQuZ2V0Vmlld3BvcnRSZWN0KCkud2lkdGgpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tZW51VHJpZ2dlci5tZW51LnhQb3NpdGlvbiA9ICdiZWZvcmUnO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm1kTWVudUVsZW1lbnQucGFyZW50RWxlbWVudC5zdHlsZS5sZWZ0ID0gdGhpcy5tb3VzZUxvY2F0aW9uLmxlZnQgLSB0aGlzLm1kTWVudUVsZW1lbnQuY2xpZW50V2lkdGggKyAncHgnO1xuICAgICAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWVudVRyaWdnZXIubWVudS54UG9zaXRpb24gPSAnYWZ0ZXInO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm1kTWVudUVsZW1lbnQucGFyZW50RWxlbWVudC5zdHlsZS5sZWZ0ID0gdGhpcy5sb2NhdGlvbkNzcygpLmxlZnQ7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMubWRNZW51RWxlbWVudC5jbGllbnRIZWlnaHQgKyB0aGlzLm1vdXNlTG9jYXRpb24udG9wID4gdGhpcy52aWV3cG9ydC5nZXRWaWV3cG9ydFJlY3QoKS5oZWlnaHQpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tZW51VHJpZ2dlci5tZW51LnlQb3NpdGlvbiA9ICdhYm92ZSc7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMubWRNZW51RWxlbWVudC5wYXJlbnRFbGVtZW50LnN0eWxlLnRvcCA9IHRoaXMubW91c2VMb2NhdGlvbi50b3AgLSB0aGlzLm1kTWVudUVsZW1lbnQuY2xpZW50SGVpZ2h0ICsgJ3B4JztcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLm1lbnVUcmlnZ2VyLm1lbnUueVBvc2l0aW9uID0gJ2JlbG93JztcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5tZE1lbnVFbGVtZW50LnBhcmVudEVsZW1lbnQuc3R5bGUudG9wID0gdGhpcy5sb2NhdGlvbkNzcygpLnRvcDtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH0sIDApO1xuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0Q29udGV4dE1lbnVFbGVtZW50KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5vdmVybGF5Q29udGFpbmVyLmdldENvbnRhaW5lckVsZW1lbnQoKS5xdWVyeVNlbGVjdG9yKCcuY29udGV4dC1tZW51Jyk7XG4gICAgfVxufVxuIl19