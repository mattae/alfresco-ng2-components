/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ViewEncapsulation, HostListener, Optional, Inject, QueryList, ViewChildren } from '@angular/core';
import { trigger } from '@angular/animations';
import { DOWN_ARROW, UP_ARROW } from '@angular/cdk/keycodes';
import { FocusKeyManager } from '@angular/cdk/a11y';
import { MatMenuItem } from '@angular/material';
import { ContextMenuOverlayRef } from './context-menu-overlay';
import { contextMenuAnimation } from './animations';
import { CONTEXT_MENU_DATA } from './context-menu.tokens';
var ContextMenuListComponent = /** @class */ (function () {
    function ContextMenuListComponent(contextMenuOverlayRef, data) {
        this.contextMenuOverlayRef = contextMenuOverlayRef;
        this.data = data;
        this.links = this.data;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    ContextMenuListComponent.prototype.handleKeydownEscape = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (event) {
            this.contextMenuOverlayRef.close();
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ContextMenuListComponent.prototype.handleKeydownEvent = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (event) {
            /** @type {?} */
            var keyCode = event.keyCode;
            if (keyCode === UP_ARROW || keyCode === DOWN_ARROW) {
                this.keyManager.onKeydown(event);
            }
        }
    };
    /**
     * @param {?} event
     * @param {?} menuItem
     * @return {?}
     */
    ContextMenuListComponent.prototype.onMenuItemClick = /**
     * @param {?} event
     * @param {?} menuItem
     * @return {?}
     */
    function (event, menuItem) {
        if (menuItem && menuItem.model && menuItem.model.disabled) {
            event.preventDefault();
            event.stopImmediatePropagation();
            return;
        }
        menuItem.subject.next(menuItem);
        this.contextMenuOverlayRef.close();
    };
    /**
     * @return {?}
     */
    ContextMenuListComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        this.keyManager = new FocusKeyManager(this.items);
        this.keyManager.setFirstItemActive();
    };
    ContextMenuListComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-context-menu',
                    template: "\n        <div mat-menu class=\"mat-menu-panel\" @panelAnimation>\n            <div id=\"adf-context-menu-content\" class=\"mat-menu-content\">\n                <ng-container *ngFor=\"let link of links\">\n                    <button *ngIf=\"link.model?.visible\"\n                            [attr.data-automation-id]=\"'context-'+((link.title || link.model?.title) | translate)\"\n                            mat-menu-item\n                            [disabled]=\"link.model?.disabled\"\n                            (click)=\"onMenuItemClick($event, link)\">\n                        <mat-icon *ngIf=\"link.model?.icon\">{{ link.model.icon }}</mat-icon>\n                        <span>{{ (link.title || link.model?.title) | translate }}</span>\n                    </button>\n                </ng-container>\n            </div>\n        </div>\n    ",
                    host: {
                        role: 'menu',
                        class: 'adf-context-menu'
                    },
                    encapsulation: ViewEncapsulation.None,
                    animations: [
                        trigger('panelAnimation', contextMenuAnimation)
                    ]
                }] }
    ];
    /** @nocollapse */
    ContextMenuListComponent.ctorParameters = function () { return [
        { type: ContextMenuOverlayRef, decorators: [{ type: Inject, args: [ContextMenuOverlayRef,] }] },
        { type: undefined, decorators: [{ type: Optional }, { type: Inject, args: [CONTEXT_MENU_DATA,] }] }
    ]; };
    ContextMenuListComponent.propDecorators = {
        items: [{ type: ViewChildren, args: [MatMenuItem,] }],
        handleKeydownEscape: [{ type: HostListener, args: ['document:keydown.Escape', ['$event'],] }],
        handleKeydownEvent: [{ type: HostListener, args: ['document:keydown', ['$event'],] }]
    };
    return ContextMenuListComponent;
}());
export { ContextMenuListComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    ContextMenuListComponent.prototype.keyManager;
    /** @type {?} */
    ContextMenuListComponent.prototype.items;
    /** @type {?} */
    ContextMenuListComponent.prototype.links;
    /**
     * @type {?}
     * @private
     */
    ContextMenuListComponent.prototype.contextMenuOverlayRef;
    /**
     * @type {?}
     * @private
     */
    ContextMenuListComponent.prototype.data;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGV4dC1tZW51LWxpc3QuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiY29udGV4dC1tZW51L2NvbnRleHQtbWVudS1saXN0LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQ0gsU0FBUyxFQUFFLGlCQUFpQixFQUFFLFlBQVksRUFDMUMsUUFBUSxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUM1QyxNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDOUMsT0FBTyxFQUFFLFVBQVUsRUFBRSxRQUFRLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDcEQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ2hELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQy9ELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUNwRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUUxRDtJQWlESSxrQ0FDMkMscUJBQTRDLEVBQ3BDLElBQVM7UUFEakIsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtRQUNwQyxTQUFJLEdBQUosSUFBSSxDQUFLO1FBRXhELElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztJQUMzQixDQUFDOzs7OztJQXJCRCxzREFBbUI7Ozs7SUFEbkIsVUFDb0IsS0FBb0I7UUFDcEMsSUFBSSxLQUFLLEVBQUU7WUFDUCxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxFQUFFLENBQUM7U0FDdEM7SUFDTCxDQUFDOzs7OztJQUdELHFEQUFrQjs7OztJQURsQixVQUNtQixLQUFvQjtRQUNuQyxJQUFJLEtBQUssRUFBRTs7Z0JBQ0QsT0FBTyxHQUFHLEtBQUssQ0FBQyxPQUFPO1lBQzdCLElBQUksT0FBTyxLQUFLLFFBQVEsSUFBSSxPQUFPLEtBQUssVUFBVSxFQUFFO2dCQUNoRCxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNwQztTQUNKO0lBQ0wsQ0FBQzs7Ozs7O0lBU0Qsa0RBQWU7Ozs7O0lBQWYsVUFBZ0IsS0FBWSxFQUFFLFFBQWE7UUFDdkMsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLEtBQUssSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRTtZQUN2RCxLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdkIsS0FBSyxDQUFDLHdCQUF3QixFQUFFLENBQUM7WUFDakMsT0FBTztTQUNWO1FBRUQsUUFBUSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFDaEMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ3ZDLENBQUM7Ozs7SUFFRCxrREFBZTs7O0lBQWY7UUFDSSxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksZUFBZSxDQUFjLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMvRCxJQUFJLENBQUMsVUFBVSxDQUFDLGtCQUFrQixFQUFFLENBQUM7SUFDekMsQ0FBQzs7Z0JBdEVKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsa0JBQWtCO29CQUM1QixRQUFRLEVBQUUsczFCQWVUO29CQUNELElBQUksRUFBRTt3QkFDRixJQUFJLEVBQUUsTUFBTTt3QkFDWixLQUFLLEVBQUUsa0JBQWtCO3FCQUM1QjtvQkFDRCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtvQkFDckMsVUFBVSxFQUFFO3dCQUNSLE9BQU8sQ0FBQyxnQkFBZ0IsRUFBRSxvQkFBb0IsQ0FBQztxQkFDbEQ7aUJBQ0o7Ozs7Z0JBOUJRLHFCQUFxQix1QkFzRHJCLE1BQU0sU0FBQyxxQkFBcUI7Z0RBQzVCLFFBQVEsWUFBSSxNQUFNLFNBQUMsaUJBQWlCOzs7d0JBdEJ4QyxZQUFZLFNBQUMsV0FBVztzQ0FHeEIsWUFBWSxTQUFDLHlCQUF5QixFQUFFLENBQUMsUUFBUSxDQUFDO3FDQU9sRCxZQUFZLFNBQUMsa0JBQWtCLEVBQUUsQ0FBQyxRQUFRLENBQUM7O0lBZ0NoRCwrQkFBQztDQUFBLEFBdkVELElBdUVDO1NBNUNZLHdCQUF3Qjs7Ozs7O0lBQ2pDLDhDQUFpRDs7SUFDakQseUNBQXlEOztJQUN6RCx5Q0FBYTs7Ozs7SUFvQlQseURBQW1GOzs7OztJQUNuRix3Q0FBd0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHtcclxuICAgIENvbXBvbmVudCwgVmlld0VuY2Fwc3VsYXRpb24sIEhvc3RMaXN0ZW5lciwgQWZ0ZXJWaWV3SW5pdCxcclxuICAgIE9wdGlvbmFsLCBJbmplY3QsIFF1ZXJ5TGlzdCwgVmlld0NoaWxkcmVuXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IHRyaWdnZXIgfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcclxuaW1wb3J0IHsgRE9XTl9BUlJPVywgVVBfQVJST1cgfSBmcm9tICdAYW5ndWxhci9jZGsva2V5Y29kZXMnO1xyXG5pbXBvcnQgeyBGb2N1c0tleU1hbmFnZXIgfSBmcm9tICdAYW5ndWxhci9jZGsvYTExeSc7XHJcbmltcG9ydCB7IE1hdE1lbnVJdGVtIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBDb250ZXh0TWVudU92ZXJsYXlSZWYgfSBmcm9tICcuL2NvbnRleHQtbWVudS1vdmVybGF5JztcclxuaW1wb3J0IHsgY29udGV4dE1lbnVBbmltYXRpb24gfSBmcm9tICcuL2FuaW1hdGlvbnMnO1xyXG5pbXBvcnQgeyBDT05URVhUX01FTlVfREFUQSB9IGZyb20gJy4vY29udGV4dC1tZW51LnRva2Vucyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLWNvbnRleHQtbWVudScsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxkaXYgbWF0LW1lbnUgY2xhc3M9XCJtYXQtbWVudS1wYW5lbFwiIEBwYW5lbEFuaW1hdGlvbj5cclxuICAgICAgICAgICAgPGRpdiBpZD1cImFkZi1jb250ZXh0LW1lbnUtY29udGVudFwiIGNsYXNzPVwibWF0LW1lbnUtY29udGVudFwiPlxyXG4gICAgICAgICAgICAgICAgPG5nLWNvbnRhaW5lciAqbmdGb3I9XCJsZXQgbGluayBvZiBsaW5rc1wiPlxyXG4gICAgICAgICAgICAgICAgICAgIDxidXR0b24gKm5nSWY9XCJsaW5rLm1vZGVsPy52aXNpYmxlXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFthdHRyLmRhdGEtYXV0b21hdGlvbi1pZF09XCInY29udGV4dC0nKygobGluay50aXRsZSB8fCBsaW5rLm1vZGVsPy50aXRsZSkgfCB0cmFuc2xhdGUpXCJcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1hdC1tZW51LWl0ZW1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIFtkaXNhYmxlZF09XCJsaW5rLm1vZGVsPy5kaXNhYmxlZFwiXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAoY2xpY2spPVwib25NZW51SXRlbUNsaWNrKCRldmVudCwgbGluaylcIj5cclxuICAgICAgICAgICAgICAgICAgICAgICAgPG1hdC1pY29uICpuZ0lmPVwibGluay5tb2RlbD8uaWNvblwiPnt7IGxpbmsubW9kZWwuaWNvbiB9fTwvbWF0LWljb24+XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIDxzcGFuPnt7IChsaW5rLnRpdGxlIHx8IGxpbmsubW9kZWw/LnRpdGxlKSB8IHRyYW5zbGF0ZSB9fTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgICAgICA8L2J1dHRvbj5cclxuICAgICAgICAgICAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgICAgICAgICA8L2Rpdj5cclxuICAgICAgICA8L2Rpdj5cclxuICAgIGAsXHJcbiAgICBob3N0OiB7XHJcbiAgICAgICAgcm9sZTogJ21lbnUnLFxyXG4gICAgICAgIGNsYXNzOiAnYWRmLWNvbnRleHQtbWVudSdcclxuICAgIH0sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gICAgYW5pbWF0aW9uczogW1xyXG4gICAgICAgIHRyaWdnZXIoJ3BhbmVsQW5pbWF0aW9uJywgY29udGV4dE1lbnVBbmltYXRpb24pXHJcbiAgICBdXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb250ZXh0TWVudUxpc3RDb21wb25lbnQgaW1wbGVtZW50cyBBZnRlclZpZXdJbml0IHtcclxuICAgIHByaXZhdGUga2V5TWFuYWdlcjogRm9jdXNLZXlNYW5hZ2VyPE1hdE1lbnVJdGVtPjtcclxuICAgIEBWaWV3Q2hpbGRyZW4oTWF0TWVudUl0ZW0pIGl0ZW1zOiBRdWVyeUxpc3Q8TWF0TWVudUl0ZW0+O1xyXG4gICAgbGlua3M6IGFueVtdO1xyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2RvY3VtZW50OmtleWRvd24uRXNjYXBlJywgWyckZXZlbnQnXSlcclxuICAgIGhhbmRsZUtleWRvd25Fc2NhcGUoZXZlbnQ6IEtleWJvYXJkRXZlbnQpIHtcclxuICAgICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5jb250ZXh0TWVudU92ZXJsYXlSZWYuY2xvc2UoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignZG9jdW1lbnQ6a2V5ZG93bicsIFsnJGV2ZW50J10pXHJcbiAgICBoYW5kbGVLZXlkb3duRXZlbnQoZXZlbnQ6IEtleWJvYXJkRXZlbnQpIHtcclxuICAgICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICAgICAgY29uc3Qga2V5Q29kZSA9IGV2ZW50LmtleUNvZGU7XHJcbiAgICAgICAgICAgIGlmIChrZXlDb2RlID09PSBVUF9BUlJPVyB8fCBrZXlDb2RlID09PSBET1dOX0FSUk9XKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmtleU1hbmFnZXIub25LZXlkb3duKGV2ZW50KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBASW5qZWN0KENvbnRleHRNZW51T3ZlcmxheVJlZikgcHJpdmF0ZSBjb250ZXh0TWVudU92ZXJsYXlSZWY6IENvbnRleHRNZW51T3ZlcmxheVJlZixcclxuICAgICAgICBAT3B0aW9uYWwoKSBASW5qZWN0KENPTlRFWFRfTUVOVV9EQVRBKSBwcml2YXRlIGRhdGE6IGFueVxyXG4gICAgKSB7XHJcbiAgICAgICAgdGhpcy5saW5rcyA9IHRoaXMuZGF0YTtcclxuICAgIH1cclxuXHJcbiAgICBvbk1lbnVJdGVtQ2xpY2soZXZlbnQ6IEV2ZW50LCBtZW51SXRlbTogYW55KSB7XHJcbiAgICAgICAgaWYgKG1lbnVJdGVtICYmIG1lbnVJdGVtLm1vZGVsICYmIG1lbnVJdGVtLm1vZGVsLmRpc2FibGVkKSB7XHJcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIGV2ZW50LnN0b3BJbW1lZGlhdGVQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBtZW51SXRlbS5zdWJqZWN0Lm5leHQobWVudUl0ZW0pO1xyXG4gICAgICAgIHRoaXMuY29udGV4dE1lbnVPdmVybGF5UmVmLmNsb3NlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdBZnRlclZpZXdJbml0KCkge1xyXG4gICAgICAgIHRoaXMua2V5TWFuYWdlciA9IG5ldyBGb2N1c0tleU1hbmFnZXI8TWF0TWVudUl0ZW0+KHRoaXMuaXRlbXMpO1xyXG4gICAgICAgIHRoaXMua2V5TWFuYWdlci5zZXRGaXJzdEl0ZW1BY3RpdmUoKTtcclxuICAgIH1cclxufVxyXG4iXX0=