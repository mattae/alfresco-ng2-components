/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable, Injector, ElementRef } from '@angular/core';
import { Overlay, OverlayConfig } from '@angular/cdk/overlay';
import { PortalInjector, ComponentPortal } from '@angular/cdk/portal';
import { ContextMenuOverlayRef } from './context-menu-overlay';
import { CONTEXT_MENU_DATA } from './context-menu.tokens';
import { ContextMenuListComponent } from './context-menu-list.component';
import * as i0 from "@angular/core";
import * as i1 from "@angular/cdk/overlay";
/** @type {?} */
var DEFAULT_CONFIG = {
    panelClass: 'cdk-overlay-pane',
    backdropClass: 'cdk-overlay-transparent-backdrop',
    hasBackdrop: true
};
var ContextMenuOverlayService = /** @class */ (function () {
    function ContextMenuOverlayService(injector, overlay) {
        this.injector = injector;
        this.overlay = overlay;
    }
    /**
     * @param {?} config
     * @return {?}
     */
    ContextMenuOverlayService.prototype.open = /**
     * @param {?} config
     * @return {?}
     */
    function (config) {
        /** @type {?} */
        var overlayConfig = tslib_1.__assign({}, DEFAULT_CONFIG, config);
        /** @type {?} */
        var overlay = this.createOverlay(overlayConfig);
        /** @type {?} */
        var overlayRef = new ContextMenuOverlayRef(overlay);
        this.attachDialogContainer(overlay, config, overlayRef);
        overlay.backdropClick().subscribe((/**
         * @return {?}
         */
        function () { return overlayRef.close(); }));
        // prevent native contextmenu on overlay element if config.hasBackdrop is true
        if (overlayConfig.hasBackdrop) {
            ((/** @type {?} */ (overlay)))._backdropElement
                .addEventListener('contextmenu', (/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                event.preventDefault();
                ((/** @type {?} */ (overlay)))._backdropClick.next(null);
            }), true);
        }
        return overlayRef;
    };
    /**
     * @private
     * @param {?} config
     * @return {?}
     */
    ContextMenuOverlayService.prototype.createOverlay = /**
     * @private
     * @param {?} config
     * @return {?}
     */
    function (config) {
        /** @type {?} */
        var overlayConfig = this.getOverlayConfig(config);
        return this.overlay.create(overlayConfig);
    };
    /**
     * @private
     * @param {?} overlay
     * @param {?} config
     * @param {?} contextMenuOverlayRef
     * @return {?}
     */
    ContextMenuOverlayService.prototype.attachDialogContainer = /**
     * @private
     * @param {?} overlay
     * @param {?} config
     * @param {?} contextMenuOverlayRef
     * @return {?}
     */
    function (overlay, config, contextMenuOverlayRef) {
        /** @type {?} */
        var injector = this.createInjector(config, contextMenuOverlayRef);
        /** @type {?} */
        var containerPortal = new ComponentPortal(ContextMenuListComponent, null, injector);
        /** @type {?} */
        var containerRef = overlay.attach(containerPortal);
        return containerRef.instance;
    };
    /**
     * @private
     * @param {?} config
     * @param {?} contextMenuOverlayRef
     * @return {?}
     */
    ContextMenuOverlayService.prototype.createInjector = /**
     * @private
     * @param {?} config
     * @param {?} contextMenuOverlayRef
     * @return {?}
     */
    function (config, contextMenuOverlayRef) {
        /** @type {?} */
        var injectionTokens = new WeakMap();
        injectionTokens.set(ContextMenuOverlayRef, contextMenuOverlayRef);
        injectionTokens.set(CONTEXT_MENU_DATA, config.data);
        return new PortalInjector(this.injector, injectionTokens);
    };
    /**
     * @private
     * @param {?} config
     * @return {?}
     */
    ContextMenuOverlayService.prototype.getOverlayConfig = /**
     * @private
     * @param {?} config
     * @return {?}
     */
    function (config) {
        var _a = config.source, clientY = _a.clientY, clientX = _a.clientX;
        /** @type {?} */
        var fakeElement = {
            getBoundingClientRect: (/**
             * @return {?}
             */
            function () { return ({
                bottom: clientY,
                height: 0,
                left: clientX,
                right: clientX,
                top: clientY,
                width: 0
            }); })
        };
        /** @type {?} */
        var positionStrategy = this.overlay.position()
            .connectedTo(new ElementRef(fakeElement), { originX: 'start', originY: 'bottom' }, { overlayX: 'start', overlayY: 'top' })
            .withFallbackPosition({ originX: 'start', originY: 'top' }, { overlayX: 'start', overlayY: 'bottom' })
            .withFallbackPosition({ originX: 'end', originY: 'top' }, { overlayX: 'start', overlayY: 'top' })
            .withFallbackPosition({ originX: 'start', originY: 'top' }, { overlayX: 'end', overlayY: 'top' })
            .withFallbackPosition({ originX: 'end', originY: 'center' }, { overlayX: 'start', overlayY: 'center' })
            .withFallbackPosition({ originX: 'start', originY: 'center' }, { overlayX: 'end', overlayY: 'center' });
        /** @type {?} */
        var overlayConfig = new OverlayConfig({
            hasBackdrop: config.hasBackdrop,
            backdropClass: config.backdropClass,
            panelClass: config.panelClass,
            scrollStrategy: this.overlay.scrollStrategies.close(),
            positionStrategy: positionStrategy
        });
        return overlayConfig;
    };
    ContextMenuOverlayService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    ContextMenuOverlayService.ctorParameters = function () { return [
        { type: Injector },
        { type: Overlay }
    ]; };
    /** @nocollapse */ ContextMenuOverlayService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ContextMenuOverlayService_Factory() { return new ContextMenuOverlayService(i0.ɵɵinject(i0.INJECTOR), i0.ɵɵinject(i1.Overlay)); }, token: ContextMenuOverlayService, providedIn: "root" });
    return ContextMenuOverlayService;
}());
export { ContextMenuOverlayService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    ContextMenuOverlayService.prototype.injector;
    /**
     * @type {?}
     * @private
     */
    ContextMenuOverlayService.prototype.overlay;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGV4dC1tZW51LW92ZXJsYXkuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNvbnRleHQtbWVudS9jb250ZXh0LW1lbnUtb3ZlcmxheS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQWdCLE1BQU0sZUFBZSxDQUFDO0FBQy9FLE9BQU8sRUFBRSxPQUFPLEVBQUUsYUFBYSxFQUFjLE1BQU0sc0JBQXNCLENBQUM7QUFDMUUsT0FBTyxFQUFFLGNBQWMsRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUUvRCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUMxRCxPQUFPLEVBQUUsd0JBQXdCLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQzs7OztJQUVuRSxjQUFjLEdBQTZCO0lBQzdDLFVBQVUsRUFBRSxrQkFBa0I7SUFDOUIsYUFBYSxFQUFFLGtDQUFrQztJQUNqRCxXQUFXLEVBQUUsSUFBSTtDQUNwQjtBQUVEO0lBS0ksbUNBQ1ksUUFBa0IsRUFDbEIsT0FBZ0I7UUFEaEIsYUFBUSxHQUFSLFFBQVEsQ0FBVTtRQUNsQixZQUFPLEdBQVAsT0FBTyxDQUFTO0lBQ3pCLENBQUM7Ozs7O0lBRUosd0NBQUk7Ozs7SUFBSixVQUFLLE1BQWdDOztZQUMzQixhQUFhLHdCQUFRLGNBQWMsRUFBSyxNQUFNLENBQUU7O1lBRWhELE9BQU8sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQzs7WUFFM0MsVUFBVSxHQUFHLElBQUkscUJBQXFCLENBQUMsT0FBTyxDQUFDO1FBRXJELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLFVBQVUsQ0FBQyxDQUFDO1FBRXhELE9BQU8sQ0FBQyxhQUFhLEVBQUUsQ0FBQyxTQUFTOzs7UUFBQyxjQUFNLE9BQUEsVUFBVSxDQUFDLEtBQUssRUFBRSxFQUFsQixDQUFrQixFQUFDLENBQUM7UUFFNUQsOEVBQThFO1FBQzlFLElBQUksYUFBYSxDQUFDLFdBQVcsRUFBRTtZQUMzQixDQUFDLG1CQUFNLE9BQU8sRUFBQSxDQUFDLENBQUMsZ0JBQWdCO2lCQUMzQixnQkFBZ0IsQ0FBQyxhQUFhOzs7O1lBQUUsVUFBQyxLQUFLO2dCQUNuQyxLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQ3ZCLENBQUMsbUJBQU0sT0FBTyxFQUFBLENBQUMsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQzlDLENBQUMsR0FBRSxJQUFJLENBQUMsQ0FBQztTQUNoQjtRQUVELE9BQU8sVUFBVSxDQUFDO0lBQ3RCLENBQUM7Ozs7OztJQUVPLGlEQUFhOzs7OztJQUFyQixVQUFzQixNQUFnQzs7WUFDNUMsYUFBYSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUM7UUFDbkQsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUM5QyxDQUFDOzs7Ozs7OztJQUVPLHlEQUFxQjs7Ozs7OztJQUE3QixVQUE4QixPQUFtQixFQUFFLE1BQWdDLEVBQUUscUJBQTRDOztZQUN2SCxRQUFRLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLEVBQUUscUJBQXFCLENBQUM7O1lBRTdELGVBQWUsR0FBRyxJQUFJLGVBQWUsQ0FBQyx3QkFBd0IsRUFBRSxJQUFJLEVBQUUsUUFBUSxDQUFDOztZQUMvRSxZQUFZLEdBQTJDLE9BQU8sQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDO1FBRTVGLE9BQU8sWUFBWSxDQUFDLFFBQVEsQ0FBQztJQUNqQyxDQUFDOzs7Ozs7O0lBRU8sa0RBQWM7Ozs7OztJQUF0QixVQUF1QixNQUFnQyxFQUFFLHFCQUE0Qzs7WUFDM0YsZUFBZSxHQUFHLElBQUksT0FBTyxFQUFFO1FBRXJDLGVBQWUsQ0FBQyxHQUFHLENBQUMscUJBQXFCLEVBQUUscUJBQXFCLENBQUMsQ0FBQztRQUNsRSxlQUFlLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUVwRCxPQUFPLElBQUksY0FBYyxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUUsZUFBZSxDQUFDLENBQUM7SUFDOUQsQ0FBQzs7Ozs7O0lBRU8sb0RBQWdCOzs7OztJQUF4QixVQUF5QixNQUFnQztRQUMvQyxJQUFBLGtCQUFxQyxFQUFuQyxvQkFBTyxFQUFFLG9CQUEwQjs7WUFFckMsV0FBVyxHQUFRO1lBQ3JCLHFCQUFxQjs7O1lBQUUsY0FBa0IsT0FBQSxDQUFDO2dCQUN0QyxNQUFNLEVBQUUsT0FBTztnQkFDZixNQUFNLEVBQUUsQ0FBQztnQkFDVCxJQUFJLEVBQUUsT0FBTztnQkFDYixLQUFLLEVBQUUsT0FBTztnQkFDZCxHQUFHLEVBQUUsT0FBTztnQkFDWixLQUFLLEVBQUUsQ0FBQzthQUNYLENBQUMsRUFQdUMsQ0FPdkMsQ0FBQTtTQUNMOztZQUVLLGdCQUFnQixHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFO2FBQzNDLFdBQVcsQ0FDUixJQUFJLFVBQVUsQ0FBQyxXQUFXLENBQUMsRUFDM0IsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsRUFDdkMsRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsQ0FBQzthQUMxQyxvQkFBb0IsQ0FDakIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsRUFDcEMsRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsQ0FBQzthQUM3QyxvQkFBb0IsQ0FDakIsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsRUFDbEMsRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsQ0FBQzthQUMxQyxvQkFBb0IsQ0FDakIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsRUFDcEMsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxLQUFLLEVBQUUsQ0FBQzthQUN4QyxvQkFBb0IsQ0FDakIsRUFBRSxPQUFPLEVBQUUsS0FBSyxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsRUFDckMsRUFBRSxRQUFRLEVBQUUsT0FBTyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsQ0FBQzthQUM3QyxvQkFBb0IsQ0FDakIsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsRUFDdkMsRUFBRSxRQUFRLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsQ0FDMUM7O1lBRUMsYUFBYSxHQUFHLElBQUksYUFBYSxDQUFDO1lBQ3BDLFdBQVcsRUFBRSxNQUFNLENBQUMsV0FBVztZQUMvQixhQUFhLEVBQUUsTUFBTSxDQUFDLGFBQWE7WUFDbkMsVUFBVSxFQUFFLE1BQU0sQ0FBQyxVQUFVO1lBQzdCLGNBQWMsRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLEtBQUssRUFBRTtZQUNyRCxnQkFBZ0Isa0JBQUE7U0FDbkIsQ0FBQztRQUVGLE9BQU8sYUFBYSxDQUFDO0lBQ3pCLENBQUM7O2dCQXJHSixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7O2dCQWhCb0IsUUFBUTtnQkFDcEIsT0FBTzs7O29DQWxCaEI7Q0FxSUMsQUF0R0QsSUFzR0M7U0FuR1kseUJBQXlCOzs7Ozs7SUFHOUIsNkNBQTBCOzs7OztJQUMxQiw0Q0FBd0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSwgSW5qZWN0b3IsIEVsZW1lbnRSZWYsIENvbXBvbmVudFJlZiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPdmVybGF5LCBPdmVybGF5Q29uZmlnLCBPdmVybGF5UmVmIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL292ZXJsYXknO1xyXG5pbXBvcnQgeyBQb3J0YWxJbmplY3RvciwgQ29tcG9uZW50UG9ydGFsIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL3BvcnRhbCc7XHJcbmltcG9ydCB7IENvbnRleHRNZW51T3ZlcmxheVJlZiB9IGZyb20gJy4vY29udGV4dC1tZW51LW92ZXJsYXknO1xyXG5pbXBvcnQgeyBDb250ZXh0TWVudU92ZXJsYXlDb25maWcgfSBmcm9tICcuL2ludGVyZmFjZXMnO1xyXG5pbXBvcnQgeyBDT05URVhUX01FTlVfREFUQSB9IGZyb20gJy4vY29udGV4dC1tZW51LnRva2Vucyc7XHJcbmltcG9ydCB7IENvbnRleHRNZW51TGlzdENvbXBvbmVudCB9IGZyb20gJy4vY29udGV4dC1tZW51LWxpc3QuY29tcG9uZW50JztcclxuXHJcbmNvbnN0IERFRkFVTFRfQ09ORklHOiBDb250ZXh0TWVudU92ZXJsYXlDb25maWcgPSB7XHJcbiAgICBwYW5lbENsYXNzOiAnY2RrLW92ZXJsYXktcGFuZScsXHJcbiAgICBiYWNrZHJvcENsYXNzOiAnY2RrLW92ZXJsYXktdHJhbnNwYXJlbnQtYmFja2Ryb3AnLFxyXG4gICAgaGFzQmFja2Ryb3A6IHRydWVcclxufTtcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29udGV4dE1lbnVPdmVybGF5U2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBpbmplY3RvcjogSW5qZWN0b3IsXHJcbiAgICAgICAgcHJpdmF0ZSBvdmVybGF5OiBPdmVybGF5XHJcbiAgICApIHt9XHJcblxyXG4gICAgb3Blbihjb25maWc6IENvbnRleHRNZW51T3ZlcmxheUNvbmZpZyk6IENvbnRleHRNZW51T3ZlcmxheVJlZiB7XHJcbiAgICAgICAgY29uc3Qgb3ZlcmxheUNvbmZpZyA9IHsgLi4uREVGQVVMVF9DT05GSUcsIC4uLmNvbmZpZyB9O1xyXG5cclxuICAgICAgICBjb25zdCBvdmVybGF5ID0gdGhpcy5jcmVhdGVPdmVybGF5KG92ZXJsYXlDb25maWcpO1xyXG5cclxuICAgICAgICBjb25zdCBvdmVybGF5UmVmID0gbmV3IENvbnRleHRNZW51T3ZlcmxheVJlZihvdmVybGF5KTtcclxuXHJcbiAgICAgICAgdGhpcy5hdHRhY2hEaWFsb2dDb250YWluZXIob3ZlcmxheSwgY29uZmlnLCBvdmVybGF5UmVmKTtcclxuXHJcbiAgICAgICAgb3ZlcmxheS5iYWNrZHJvcENsaWNrKCkuc3Vic2NyaWJlKCgpID0+IG92ZXJsYXlSZWYuY2xvc2UoKSk7XHJcblxyXG4gICAgICAgIC8vIHByZXZlbnQgbmF0aXZlIGNvbnRleHRtZW51IG9uIG92ZXJsYXkgZWxlbWVudCBpZiBjb25maWcuaGFzQmFja2Ryb3AgaXMgdHJ1ZVxyXG4gICAgICAgIGlmIChvdmVybGF5Q29uZmlnLmhhc0JhY2tkcm9wKSB7XHJcbiAgICAgICAgICAgICg8YW55PiBvdmVybGF5KS5fYmFja2Ryb3BFbGVtZW50XHJcbiAgICAgICAgICAgICAgICAuYWRkRXZlbnRMaXN0ZW5lcignY29udGV4dG1lbnUnLCAoZXZlbnQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICg8YW55PiBvdmVybGF5KS5fYmFja2Ryb3BDbGljay5uZXh0KG51bGwpO1xyXG4gICAgICAgICAgICAgICAgfSwgdHJ1ZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gb3ZlcmxheVJlZjtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNyZWF0ZU92ZXJsYXkoY29uZmlnOiBDb250ZXh0TWVudU92ZXJsYXlDb25maWcpOiBPdmVybGF5UmVmIHtcclxuICAgICAgICBjb25zdCBvdmVybGF5Q29uZmlnID0gdGhpcy5nZXRPdmVybGF5Q29uZmlnKGNvbmZpZyk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub3ZlcmxheS5jcmVhdGUob3ZlcmxheUNvbmZpZyk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBhdHRhY2hEaWFsb2dDb250YWluZXIob3ZlcmxheTogT3ZlcmxheVJlZiwgY29uZmlnOiBDb250ZXh0TWVudU92ZXJsYXlDb25maWcsIGNvbnRleHRNZW51T3ZlcmxheVJlZjogQ29udGV4dE1lbnVPdmVybGF5UmVmKSB7XHJcbiAgICAgICAgY29uc3QgaW5qZWN0b3IgPSB0aGlzLmNyZWF0ZUluamVjdG9yKGNvbmZpZywgY29udGV4dE1lbnVPdmVybGF5UmVmKTtcclxuXHJcbiAgICAgICAgY29uc3QgY29udGFpbmVyUG9ydGFsID0gbmV3IENvbXBvbmVudFBvcnRhbChDb250ZXh0TWVudUxpc3RDb21wb25lbnQsIG51bGwsIGluamVjdG9yKTtcclxuICAgICAgICBjb25zdCBjb250YWluZXJSZWY6IENvbXBvbmVudFJlZjxDb250ZXh0TWVudUxpc3RDb21wb25lbnQ+ID0gb3ZlcmxheS5hdHRhY2goY29udGFpbmVyUG9ydGFsKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGNvbnRhaW5lclJlZi5pbnN0YW5jZTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNyZWF0ZUluamVjdG9yKGNvbmZpZzogQ29udGV4dE1lbnVPdmVybGF5Q29uZmlnLCBjb250ZXh0TWVudU92ZXJsYXlSZWY6IENvbnRleHRNZW51T3ZlcmxheVJlZik6IFBvcnRhbEluamVjdG9yIHtcclxuICAgICAgICBjb25zdCBpbmplY3Rpb25Ub2tlbnMgPSBuZXcgV2Vha01hcCgpO1xyXG5cclxuICAgICAgICBpbmplY3Rpb25Ub2tlbnMuc2V0KENvbnRleHRNZW51T3ZlcmxheVJlZiwgY29udGV4dE1lbnVPdmVybGF5UmVmKTtcclxuICAgICAgICBpbmplY3Rpb25Ub2tlbnMuc2V0KENPTlRFWFRfTUVOVV9EQVRBLCBjb25maWcuZGF0YSk7XHJcblxyXG4gICAgICAgIHJldHVybiBuZXcgUG9ydGFsSW5qZWN0b3IodGhpcy5pbmplY3RvciwgaW5qZWN0aW9uVG9rZW5zKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldE92ZXJsYXlDb25maWcoY29uZmlnOiBDb250ZXh0TWVudU92ZXJsYXlDb25maWcpOiBPdmVybGF5Q29uZmlnIHtcclxuICAgICAgICBjb25zdCB7IGNsaWVudFksIGNsaWVudFggIH0gPSBjb25maWcuc291cmNlO1xyXG5cclxuICAgICAgICBjb25zdCBmYWtlRWxlbWVudDogYW55ID0ge1xyXG4gICAgICAgICAgICBnZXRCb3VuZGluZ0NsaWVudFJlY3Q6ICgpOiBDbGllbnRSZWN0ID0+ICh7XHJcbiAgICAgICAgICAgICAgICBib3R0b206IGNsaWVudFksXHJcbiAgICAgICAgICAgICAgICBoZWlnaHQ6IDAsXHJcbiAgICAgICAgICAgICAgICBsZWZ0OiBjbGllbnRYLFxyXG4gICAgICAgICAgICAgICAgcmlnaHQ6IGNsaWVudFgsXHJcbiAgICAgICAgICAgICAgICB0b3A6IGNsaWVudFksXHJcbiAgICAgICAgICAgICAgICB3aWR0aDogMFxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGNvbnN0IHBvc2l0aW9uU3RyYXRlZ3kgPSB0aGlzLm92ZXJsYXkucG9zaXRpb24oKVxyXG4gICAgICAgICAgICAuY29ubmVjdGVkVG8oXHJcbiAgICAgICAgICAgICAgICBuZXcgRWxlbWVudFJlZihmYWtlRWxlbWVudCksXHJcbiAgICAgICAgICAgICAgICB7IG9yaWdpblg6ICdzdGFydCcsIG9yaWdpblk6ICdib3R0b20nIH0sXHJcbiAgICAgICAgICAgICAgICB7IG92ZXJsYXlYOiAnc3RhcnQnLCBvdmVybGF5WTogJ3RvcCcgfSlcclxuICAgICAgICAgICAgLndpdGhGYWxsYmFja1Bvc2l0aW9uKFxyXG4gICAgICAgICAgICAgICAgeyBvcmlnaW5YOiAnc3RhcnQnLCBvcmlnaW5ZOiAndG9wJyB9LFxyXG4gICAgICAgICAgICAgICAgeyBvdmVybGF5WDogJ3N0YXJ0Jywgb3ZlcmxheVk6ICdib3R0b20nIH0pXHJcbiAgICAgICAgICAgIC53aXRoRmFsbGJhY2tQb3NpdGlvbihcclxuICAgICAgICAgICAgICAgIHsgb3JpZ2luWDogJ2VuZCcsIG9yaWdpblk6ICd0b3AnIH0sXHJcbiAgICAgICAgICAgICAgICB7IG92ZXJsYXlYOiAnc3RhcnQnLCBvdmVybGF5WTogJ3RvcCcgfSlcclxuICAgICAgICAgICAgLndpdGhGYWxsYmFja1Bvc2l0aW9uKFxyXG4gICAgICAgICAgICAgICAgeyBvcmlnaW5YOiAnc3RhcnQnLCBvcmlnaW5ZOiAndG9wJyB9LFxyXG4gICAgICAgICAgICAgICAgeyBvdmVybGF5WDogJ2VuZCcsIG92ZXJsYXlZOiAndG9wJyB9KVxyXG4gICAgICAgICAgICAud2l0aEZhbGxiYWNrUG9zaXRpb24oXHJcbiAgICAgICAgICAgICAgICB7IG9yaWdpblg6ICdlbmQnLCBvcmlnaW5ZOiAnY2VudGVyJyB9LFxyXG4gICAgICAgICAgICAgICAgeyBvdmVybGF5WDogJ3N0YXJ0Jywgb3ZlcmxheVk6ICdjZW50ZXInIH0pXHJcbiAgICAgICAgICAgIC53aXRoRmFsbGJhY2tQb3NpdGlvbihcclxuICAgICAgICAgICAgICAgIHsgb3JpZ2luWDogJ3N0YXJ0Jywgb3JpZ2luWTogJ2NlbnRlcicgfSxcclxuICAgICAgICAgICAgICAgIHsgb3ZlcmxheVg6ICdlbmQnLCBvdmVybGF5WTogJ2NlbnRlcicgfVxyXG4gICAgICAgICAgICApO1xyXG5cclxuICAgICAgICBjb25zdCBvdmVybGF5Q29uZmlnID0gbmV3IE92ZXJsYXlDb25maWcoe1xyXG4gICAgICAgICAgICBoYXNCYWNrZHJvcDogY29uZmlnLmhhc0JhY2tkcm9wLFxyXG4gICAgICAgICAgICBiYWNrZHJvcENsYXNzOiBjb25maWcuYmFja2Ryb3BDbGFzcyxcclxuICAgICAgICAgICAgcGFuZWxDbGFzczogY29uZmlnLnBhbmVsQ2xhc3MsXHJcbiAgICAgICAgICAgIHNjcm9sbFN0cmF0ZWd5OiB0aGlzLm92ZXJsYXkuc2Nyb2xsU3RyYXRlZ2llcy5jbG9zZSgpLFxyXG4gICAgICAgICAgICBwb3NpdGlvblN0cmF0ZWd5XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBvdmVybGF5Q29uZmlnO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==