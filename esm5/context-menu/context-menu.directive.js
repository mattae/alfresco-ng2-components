/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:no-input-rename  */
import { Directive, HostListener, Input } from '@angular/core';
import { ContextMenuOverlayService } from './context-menu-overlay.service';
var ContextMenuDirective = /** @class */ (function () {
    function ContextMenuDirective(contextMenuService) {
        this.contextMenuService = contextMenuService;
        /**
         * Is the menu enabled?
         */
        this.enabled = false;
    }
    /**
     * @param {?=} event
     * @return {?}
     */
    ContextMenuDirective.prototype.onShowContextMenu = /**
     * @param {?=} event
     * @return {?}
     */
    function (event) {
        if (this.enabled) {
            if (event) {
                event.preventDefault();
            }
            if (this.links && this.links.length > 0) {
                this.contextMenuService.open({
                    source: event,
                    data: this.links
                });
            }
        }
    };
    ContextMenuDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[adf-context-menu], [context-menu]'
                },] }
    ];
    /** @nocollapse */
    ContextMenuDirective.ctorParameters = function () { return [
        { type: ContextMenuOverlayService }
    ]; };
    ContextMenuDirective.propDecorators = {
        links: [{ type: Input, args: ['adf-context-menu',] }],
        enabled: [{ type: Input, args: ['adf-context-menu-enabled',] }],
        onShowContextMenu: [{ type: HostListener, args: ['contextmenu', ['$event'],] }]
    };
    return ContextMenuDirective;
}());
export { ContextMenuDirective };
if (false) {
    /**
     * Items for the menu.
     * @type {?}
     */
    ContextMenuDirective.prototype.links;
    /**
     * Is the menu enabled?
     * @type {?}
     */
    ContextMenuDirective.prototype.enabled;
    /**
     * @type {?}
     * @private
     */
    ContextMenuDirective.prototype.contextMenuService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGV4dC1tZW51LmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImNvbnRleHQtbWVudS9jb250ZXh0LW1lbnUuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDL0QsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFFM0U7SUFZSSw4QkFBb0Isa0JBQTZDO1FBQTdDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBMkI7Ozs7UUFGakUsWUFBTyxHQUFZLEtBQUssQ0FBQztJQUUyQyxDQUFDOzs7OztJQUdyRSxnREFBaUI7Ozs7SUFEakIsVUFDa0IsS0FBa0I7UUFDaEMsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2QsSUFBSSxLQUFLLEVBQUU7Z0JBQ1AsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO2FBQzFCO1lBRUQsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDckMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQztvQkFDekIsTUFBTSxFQUFFLEtBQUs7b0JBQ2IsSUFBSSxFQUFFLElBQUksQ0FBQyxLQUFLO2lCQUNuQixDQUFDLENBQUM7YUFDTjtTQUNKO0lBQ0wsQ0FBQzs7Z0JBNUJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsb0NBQW9DO2lCQUNqRDs7OztnQkFKUSx5QkFBeUI7Ozt3QkFPN0IsS0FBSyxTQUFDLGtCQUFrQjswQkFJeEIsS0FBSyxTQUFDLDBCQUEwQjtvQ0FLaEMsWUFBWSxTQUFDLGFBQWEsRUFBRSxDQUFDLFFBQVEsQ0FBQzs7SUFlM0MsMkJBQUM7Q0FBQSxBQTdCRCxJQTZCQztTQTFCWSxvQkFBb0I7Ozs7OztJQUU3QixxQ0FDYTs7Ozs7SUFHYix1Q0FDeUI7Ozs7O0lBRWIsa0RBQXFEIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOm5vLWlucHV0LXJlbmFtZSAgKi9cclxuXHJcbmltcG9ydCB7IERpcmVjdGl2ZSwgSG9zdExpc3RlbmVyLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb250ZXh0TWVudU92ZXJsYXlTZXJ2aWNlIH0gZnJvbSAnLi9jb250ZXh0LW1lbnUtb3ZlcmxheS5zZXJ2aWNlJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbYWRmLWNvbnRleHQtbWVudV0sIFtjb250ZXh0LW1lbnVdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQ29udGV4dE1lbnVEaXJlY3RpdmUge1xyXG4gICAgLyoqIEl0ZW1zIGZvciB0aGUgbWVudS4gKi9cclxuICAgIEBJbnB1dCgnYWRmLWNvbnRleHQtbWVudScpXHJcbiAgICBsaW5rczogYW55W107XHJcblxyXG4gICAgLyoqIElzIHRoZSBtZW51IGVuYWJsZWQ/ICovXHJcbiAgICBASW5wdXQoJ2FkZi1jb250ZXh0LW1lbnUtZW5hYmxlZCcpXHJcbiAgICBlbmFibGVkOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBjb250ZXh0TWVudVNlcnZpY2U6IENvbnRleHRNZW51T3ZlcmxheVNlcnZpY2UpIHt9XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignY29udGV4dG1lbnUnLCBbJyRldmVudCddKVxyXG4gICAgb25TaG93Q29udGV4dE1lbnUoZXZlbnQ/OiBNb3VzZUV2ZW50KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZW5hYmxlZCkge1xyXG4gICAgICAgICAgICBpZiAoZXZlbnQpIHtcclxuICAgICAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmxpbmtzICYmIHRoaXMubGlua3MubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jb250ZXh0TWVudVNlcnZpY2Uub3Blbih7XHJcbiAgICAgICAgICAgICAgICAgICAgc291cmNlOiBldmVudCxcclxuICAgICAgICAgICAgICAgICAgICBkYXRhOiB0aGlzLmxpbmtzXHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=