/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector no-input-rename  */
import { Component, ContentChild, Input, TemplateRef } from '@angular/core';
var DataColumnComponent = /** @class */ (function () {
    function DataColumnComponent() {
        /**
         * Value type for the column. Possible settings are 'text', 'image',
         * 'date', 'fileSize', 'location', and 'json'.
         */
        this.type = 'text';
        /**
         * Toggles ability to sort by this column, for example by clicking the column header.
         */
        this.sortable = true;
        /**
         * Display title of the column, typically used for column headers. You can use the
         * i18n resource key to get it translated automatically.
         */
        this.title = '';
    }
    /**
     * @return {?}
     */
    DataColumnComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (!this.srTitle && this.key === '$thumbnail') {
            this.srTitle = 'Thumbnail';
        }
    };
    DataColumnComponent.decorators = [
        { type: Component, args: [{
                    selector: 'data-column',
                    template: ''
                }] }
    ];
    DataColumnComponent.propDecorators = {
        key: [{ type: Input }],
        type: [{ type: Input }],
        format: [{ type: Input }],
        sortable: [{ type: Input }],
        title: [{ type: Input }],
        template: [{ type: ContentChild, args: [TemplateRef, { static: true },] }],
        formatTooltip: [{ type: Input }],
        srTitle: [{ type: Input, args: ['sr-title',] }],
        cssClass: [{ type: Input, args: ['class',] }],
        copyContent: [{ type: Input }]
    };
    return DataColumnComponent;
}());
export { DataColumnComponent };
if (false) {
    /**
     * Data source key. Can be either a column/property key like `title`
     *  or a property path like `createdBy.name`.
     * @type {?}
     */
    DataColumnComponent.prototype.key;
    /**
     * Value type for the column. Possible settings are 'text', 'image',
     * 'date', 'fileSize', 'location', and 'json'.
     * @type {?}
     */
    DataColumnComponent.prototype.type;
    /**
     * Value format (if supported by the parent component), for example format of the date.
     * @type {?}
     */
    DataColumnComponent.prototype.format;
    /**
     * Toggles ability to sort by this column, for example by clicking the column header.
     * @type {?}
     */
    DataColumnComponent.prototype.sortable;
    /**
     * Display title of the column, typically used for column headers. You can use the
     * i18n resource key to get it translated automatically.
     * @type {?}
     */
    DataColumnComponent.prototype.title;
    /** @type {?} */
    DataColumnComponent.prototype.template;
    /**
     * Custom tooltip formatter function.
     * @type {?}
     */
    DataColumnComponent.prototype.formatTooltip;
    /**
     * Title to be used for screen readers.
     * @type {?}
     */
    DataColumnComponent.prototype.srTitle;
    /**
     * Additional CSS class to be applied to column (header and cells).
     * @type {?}
     */
    DataColumnComponent.prototype.cssClass;
    /**
     * Enables/disables a Clipboard directive to allow copying of cell contents.
     * @type {?}
     */
    DataColumnComponent.prototype.copyContent;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS1jb2x1bW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZGF0YS1jb2x1bW4vZGF0YS1jb2x1bW4uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQ0EsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFVLFdBQVcsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUVwRjtJQUFBOzs7OztRQWdCSSxTQUFJLEdBQVcsTUFBTSxDQUFDOzs7O1FBUXRCLGFBQVEsR0FBWSxJQUFJLENBQUM7Ozs7O1FBTXpCLFVBQUssR0FBVyxFQUFFLENBQUM7SUEwQnZCLENBQUM7Ozs7SUFMRyxzQ0FBUTs7O0lBQVI7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsR0FBRyxLQUFLLFlBQVksRUFBRTtZQUM1QyxJQUFJLENBQUMsT0FBTyxHQUFHLFdBQVcsQ0FBQztTQUM5QjtJQUNMLENBQUM7O2dCQXZESixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGFBQWE7b0JBQ3ZCLFFBQVEsRUFBRSxFQUFFO2lCQUNmOzs7c0JBTUksS0FBSzt1QkFNTCxLQUFLO3lCQUlMLEtBQUs7MkJBSUwsS0FBSzt3QkFNTCxLQUFLOzJCQUdMLFlBQVksU0FBQyxXQUFXLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDO2dDQUl4QyxLQUFLOzBCQUlMLEtBQUssU0FBQyxVQUFVOzJCQUloQixLQUFLLFNBQUMsT0FBTzs4QkFJYixLQUFLOztJQVFWLDBCQUFDO0NBQUEsQUF4REQsSUF3REM7U0FwRFksbUJBQW1COzs7Ozs7O0lBSzVCLGtDQUNZOzs7Ozs7SUFLWixtQ0FDc0I7Ozs7O0lBR3RCLHFDQUNlOzs7OztJQUdmLHVDQUN5Qjs7Ozs7O0lBS3pCLG9DQUNtQjs7SUFFbkIsdUNBQ2M7Ozs7O0lBR2QsNENBQ3dCOzs7OztJQUd4QixzQ0FDZ0I7Ozs7O0lBR2hCLHVDQUNpQjs7Ozs7SUFHakIsMENBQ3FCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXG5cbi8qIVxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXG4gKlxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuICpcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbiAqXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxuICovXG5cbiAvKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3Igbm8taW5wdXQtcmVuYW1lICAqL1xuXG5pbXBvcnQgeyBDb21wb25lbnQsIENvbnRlbnRDaGlsZCwgSW5wdXQsIE9uSW5pdCwgVGVtcGxhdGVSZWYgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdkYXRhLWNvbHVtbicsXG4gICAgdGVtcGxhdGU6ICcnXG59KVxuZXhwb3J0IGNsYXNzIERhdGFDb2x1bW5Db21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgLyoqIERhdGEgc291cmNlIGtleS4gQ2FuIGJlIGVpdGhlciBhIGNvbHVtbi9wcm9wZXJ0eSBrZXkgbGlrZSBgdGl0bGVgXG4gICAgICogIG9yIGEgcHJvcGVydHkgcGF0aCBsaWtlIGBjcmVhdGVkQnkubmFtZWAuXG4gICAgICovXG4gICAgQElucHV0KClcbiAgICBrZXk6IHN0cmluZztcblxuICAgIC8qKiBWYWx1ZSB0eXBlIGZvciB0aGUgY29sdW1uLiBQb3NzaWJsZSBzZXR0aW5ncyBhcmUgJ3RleHQnLCAnaW1hZ2UnLFxuICAgICAqICdkYXRlJywgJ2ZpbGVTaXplJywgJ2xvY2F0aW9uJywgYW5kICdqc29uJy5cbiAgICAgKi9cbiAgICBASW5wdXQoKVxuICAgIHR5cGU6IHN0cmluZyA9ICd0ZXh0JztcblxuICAgIC8qKiBWYWx1ZSBmb3JtYXQgKGlmIHN1cHBvcnRlZCBieSB0aGUgcGFyZW50IGNvbXBvbmVudCksIGZvciBleGFtcGxlIGZvcm1hdCBvZiB0aGUgZGF0ZS4gKi9cbiAgICBASW5wdXQoKVxuICAgIGZvcm1hdDogc3RyaW5nO1xuXG4gICAgLyoqIFRvZ2dsZXMgYWJpbGl0eSB0byBzb3J0IGJ5IHRoaXMgY29sdW1uLCBmb3IgZXhhbXBsZSBieSBjbGlja2luZyB0aGUgY29sdW1uIGhlYWRlci4gKi9cbiAgICBASW5wdXQoKVxuICAgIHNvcnRhYmxlOiBib29sZWFuID0gdHJ1ZTtcblxuICAgIC8qKiBEaXNwbGF5IHRpdGxlIG9mIHRoZSBjb2x1bW4sIHR5cGljYWxseSB1c2VkIGZvciBjb2x1bW4gaGVhZGVycy4gWW91IGNhbiB1c2UgdGhlXG4gICAgICogaTE4biByZXNvdXJjZSBrZXkgdG8gZ2V0IGl0IHRyYW5zbGF0ZWQgYXV0b21hdGljYWxseS5cbiAgICAgKi9cbiAgICBASW5wdXQoKVxuICAgIHRpdGxlOiBzdHJpbmcgPSAnJztcblxuICAgIEBDb250ZW50Q2hpbGQoVGVtcGxhdGVSZWYsIHtzdGF0aWM6IHRydWV9KVxuICAgIHRlbXBsYXRlOiBhbnk7XG5cbiAgICAvKiogQ3VzdG9tIHRvb2x0aXAgZm9ybWF0dGVyIGZ1bmN0aW9uLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgZm9ybWF0VG9vbHRpcDogRnVuY3Rpb247XG5cbiAgICAvKiogVGl0bGUgdG8gYmUgdXNlZCBmb3Igc2NyZWVuIHJlYWRlcnMuICovXG4gICAgQElucHV0KCdzci10aXRsZScpXG4gICAgc3JUaXRsZTogc3RyaW5nO1xuXG4gICAgLyoqIEFkZGl0aW9uYWwgQ1NTIGNsYXNzIHRvIGJlIGFwcGxpZWQgdG8gY29sdW1uIChoZWFkZXIgYW5kIGNlbGxzKS4gKi9cbiAgICBASW5wdXQoJ2NsYXNzJylcbiAgICBjc3NDbGFzczogc3RyaW5nO1xuXG4gICAgIC8qKiBFbmFibGVzL2Rpc2FibGVzIGEgQ2xpcGJvYXJkIGRpcmVjdGl2ZSB0byBhbGxvdyBjb3B5aW5nIG9mIGNlbGwgY29udGVudHMuICovXG4gICAgQElucHV0KClcbiAgICBjb3B5Q29udGVudDogYm9vbGVhbjtcblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICBpZiAoIXRoaXMuc3JUaXRsZSAmJiB0aGlzLmtleSA9PT0gJyR0aHVtYm5haWwnKSB7XG4gICAgICAgICAgICB0aGlzLnNyVGl0bGUgPSAnVGh1bWJuYWlsJztcbiAgICAgICAgfVxuICAgIH1cbn1cbiJdfQ==