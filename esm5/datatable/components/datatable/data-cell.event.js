/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { BaseEvent } from '../../../events';
var DataCellEventModel = /** @class */ (function () {
    function DataCellEventModel(row, col, actions) {
        this.row = row;
        this.col = col;
        this.actions = actions || [];
    }
    return DataCellEventModel;
}());
export { DataCellEventModel };
if (false) {
    /** @type {?} */
    DataCellEventModel.prototype.row;
    /** @type {?} */
    DataCellEventModel.prototype.col;
    /** @type {?} */
    DataCellEventModel.prototype.actions;
}
var DataCellEvent = /** @class */ (function (_super) {
    tslib_1.__extends(DataCellEvent, _super);
    function DataCellEvent(row, col, actions) {
        var _this = _super.call(this) || this;
        _this.value = new DataCellEventModel(row, col, actions);
        return _this;
    }
    return DataCellEvent;
}(BaseEvent));
export { DataCellEvent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS1jZWxsLmV2ZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZGF0YXRhYmxlL2NvbXBvbmVudHMvZGF0YXRhYmxlL2RhdGEtY2VsbC5ldmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBSTVDO0lBTUksNEJBQVksR0FBWSxFQUFFLEdBQWUsRUFBRSxPQUFjO1FBQ3JELElBQUksQ0FBQyxHQUFHLEdBQUcsR0FBRyxDQUFDO1FBQ2YsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDZixJQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sSUFBSSxFQUFFLENBQUM7SUFDakMsQ0FBQztJQUVMLHlCQUFDO0FBQUQsQ0FBQyxBQVpELElBWUM7Ozs7SUFWRyxpQ0FBc0I7O0lBQ3RCLGlDQUF5Qjs7SUFDekIscUNBQWU7O0FBVW5CO0lBQW1DLHlDQUE2QjtJQUU1RCx1QkFBWSxHQUFZLEVBQUUsR0FBZSxFQUFFLE9BQWM7UUFBekQsWUFDSSxpQkFBTyxTQUVWO1FBREcsS0FBSSxDQUFDLEtBQUssR0FBRyxJQUFJLGtCQUFrQixDQUFDLEdBQUcsRUFBRSxHQUFHLEVBQUUsT0FBTyxDQUFDLENBQUM7O0lBQzNELENBQUM7SUFFTCxvQkFBQztBQUFELENBQUMsQUFQRCxDQUFtQyxTQUFTLEdBTzNDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEJhc2VFdmVudCB9IGZyb20gJy4uLy4uLy4uL2V2ZW50cyc7XHJcbmltcG9ydCB7IERhdGFDb2x1bW4gfSBmcm9tICcuLi8uLi9kYXRhL2RhdGEtY29sdW1uLm1vZGVsJztcclxuaW1wb3J0IHsgRGF0YVJvdyB9IGZyb20gJy4uLy4uL2RhdGEvZGF0YS1yb3cubW9kZWwnO1xyXG5cclxuZXhwb3J0IGNsYXNzIERhdGFDZWxsRXZlbnRNb2RlbCB7XHJcblxyXG4gICAgcmVhZG9ubHkgcm93OiBEYXRhUm93O1xyXG4gICAgcmVhZG9ubHkgY29sOiBEYXRhQ29sdW1uO1xyXG4gICAgYWN0aW9uczogYW55W107XHJcblxyXG4gICAgY29uc3RydWN0b3Iocm93OiBEYXRhUm93LCBjb2w6IERhdGFDb2x1bW4sIGFjdGlvbnM6IGFueVtdKSB7XHJcbiAgICAgICAgdGhpcy5yb3cgPSByb3c7XHJcbiAgICAgICAgdGhpcy5jb2wgPSBjb2w7XHJcbiAgICAgICAgdGhpcy5hY3Rpb25zID0gYWN0aW9ucyB8fCBbXTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBEYXRhQ2VsbEV2ZW50IGV4dGVuZHMgQmFzZUV2ZW50PERhdGFDZWxsRXZlbnRNb2RlbD4ge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHJvdzogRGF0YVJvdywgY29sOiBEYXRhQ29sdW1uLCBhY3Rpb25zOiBhbnlbXSkge1xyXG4gICAgICAgIHN1cGVyKCk7XHJcbiAgICAgICAgdGhpcy52YWx1ZSA9IG5ldyBEYXRhQ2VsbEV2ZW50TW9kZWwocm93LCBjb2wsIGFjdGlvbnMpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=