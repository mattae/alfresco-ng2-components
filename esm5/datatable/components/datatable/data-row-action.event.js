/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { BaseEvent } from '../../../events';
var DataRowActionModel = /** @class */ (function () {
    function DataRowActionModel(row, action) {
        this.row = row;
        this.action = action;
    }
    return DataRowActionModel;
}());
export { DataRowActionModel };
if (false) {
    /** @type {?} */
    DataRowActionModel.prototype.row;
    /** @type {?} */
    DataRowActionModel.prototype.action;
}
var DataRowActionEvent = /** @class */ (function (_super) {
    tslib_1.__extends(DataRowActionEvent, _super);
    function DataRowActionEvent(row, action) {
        var _this = _super.call(this) || this;
        _this.value = new DataRowActionModel(row, action);
        return _this;
    }
    Object.defineProperty(DataRowActionEvent.prototype, "args", {
        // backwards compatibility with 1.2.0 and earlier
        get: 
        // backwards compatibility with 1.2.0 and earlier
        /**
         * @return {?}
         */
        function () {
            return this.value;
        },
        enumerable: true,
        configurable: true
    });
    return DataRowActionEvent;
}(BaseEvent));
export { DataRowActionEvent };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS1yb3ctYWN0aW9uLmV2ZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZGF0YXRhYmxlL2NvbXBvbmVudHMvZGF0YXRhYmxlL2RhdGEtcm93LWFjdGlvbi5ldmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBRzVDO0lBS0ksNEJBQVksR0FBWSxFQUFFLE1BQVc7UUFDakMsSUFBSSxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUM7UUFDZixJQUFJLENBQUMsTUFBTSxHQUFHLE1BQU0sQ0FBQztJQUN6QixDQUFDO0lBQ0wseUJBQUM7QUFBRCxDQUFDLEFBVEQsSUFTQzs7OztJQVBHLGlDQUFhOztJQUNiLG9DQUFZOztBQVFoQjtJQUF3Qyw4Q0FBNkI7SUFPakUsNEJBQVksR0FBWSxFQUFFLE1BQVc7UUFBckMsWUFDSSxpQkFBTyxTQUVWO1FBREcsS0FBSSxDQUFDLEtBQUssR0FBRyxJQUFJLGtCQUFrQixDQUFDLEdBQUcsRUFBRSxNQUFNLENBQUMsQ0FBQzs7SUFDckQsQ0FBQztJQVBELHNCQUFJLG9DQUFJO1FBRFIsaURBQWlEOzs7Ozs7UUFDakQ7WUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUM7UUFDdEIsQ0FBQzs7O09BQUE7SUFPTCx5QkFBQztBQUFELENBQUMsQUFaRCxDQUF3QyxTQUFTLEdBWWhEIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEJhc2VFdmVudCB9IGZyb20gJy4uLy4uLy4uL2V2ZW50cyc7XHJcbmltcG9ydCB7IERhdGFSb3cgfSBmcm9tICcuLi8uLi9kYXRhL2RhdGEtcm93Lm1vZGVsJztcclxuXHJcbmV4cG9ydCBjbGFzcyBEYXRhUm93QWN0aW9uTW9kZWwge1xyXG5cclxuICAgIHJvdzogRGF0YVJvdztcclxuICAgIGFjdGlvbjogYW55O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHJvdzogRGF0YVJvdywgYWN0aW9uOiBhbnkpIHtcclxuICAgICAgICB0aGlzLnJvdyA9IHJvdztcclxuICAgICAgICB0aGlzLmFjdGlvbiA9IGFjdGlvbjtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIERhdGFSb3dBY3Rpb25FdmVudCBleHRlbmRzIEJhc2VFdmVudDxEYXRhUm93QWN0aW9uTW9kZWw+IHtcclxuXHJcbiAgICAvLyBiYWNrd2FyZHMgY29tcGF0aWJpbGl0eSB3aXRoIDEuMi4wIGFuZCBlYXJsaWVyXHJcbiAgICBnZXQgYXJncygpOiBEYXRhUm93QWN0aW9uTW9kZWwge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHJvdzogRGF0YVJvdywgYWN0aW9uOiBhbnkpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIHRoaXMudmFsdWUgPSBuZXcgRGF0YVJvd0FjdGlvbk1vZGVsKHJvdywgYWN0aW9uKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19