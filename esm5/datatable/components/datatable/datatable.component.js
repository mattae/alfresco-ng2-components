/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ContentChild, ElementRef, EventEmitter, Input, IterableDiffers, Output, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs';
import { DataColumnListComponent } from '../../../data-column/data-column-list.component';
import { DataRowEvent } from '../../data/data-row-event.model';
import { DataSorting } from '../../data/data-sorting.model';
import { ObjectDataRow } from '../../data/object-datarow.model';
import { ObjectDataTableAdapter } from '../../data/object-datatable-adapter';
import { DataCellEvent } from './data-cell.event';
import { DataRowActionEvent } from './data-row-action.event';
import { share, buffer, map, filter, debounceTime } from 'rxjs/operators';
/** @enum {string} */
var DisplayMode = {
    List: 'list',
    Gallery: 'gallery',
};
export { DisplayMode };
var DataTableComponent = /** @class */ (function () {
    function DataTableComponent(elementRef, differs) {
        var _this = this;
        this.elementRef = elementRef;
        /**
         * Selects the display mode of the table. Can be "list" or "gallery".
         */
        this.display = DisplayMode.List;
        /**
         * The rows that the datatable will show.
         */
        this.rows = [];
        /**
         * Define the sort order of the datatable. Possible values are :
         * [`created`, `desc`], [`created`, `asc`], [`due`, `desc`], [`due`, `asc`]
         */
        this.sorting = [];
        /**
         * The columns that the datatable will show.
         */
        this.columns = [];
        /**
         * Row selection mode. Can be none, `single` or `multiple`. For `multiple` mode,
         * you can use Cmd (macOS) or Ctrl (Win) modifier key to toggle selection for multiple rows.
         */
        this.selectionMode = 'single'; // none|single|multiple
        // none|single|multiple
        /**
         * Toggles multiple row selection, which renders checkboxes at the beginning of each row.
         */
        this.multiselect = false;
        /**
         * Toggles the data actions column.
         */
        this.actions = false;
        /**
         * Position of the actions dropdown menu. Can be "left" or "right".
         */
        this.actionsPosition = 'right'; // left|right
        /**
         * Toggles custom context menu for the component.
         */
        this.contextMenu = false;
        /**
         * Toggles file drop support for rows (see
         * [Upload directive](upload.directive.md) for further details).
         */
        this.allowDropFiles = false;
        /**
         * The CSS class to apply to every row.
         */
        this.rowStyleClass = '';
        /**
         * Toggles the header.
         */
        this.showHeader = true;
        /**
         * Toggles the sticky header mode.
         */
        this.stickyHeader = false;
        /**
         * Emitted when the user clicks a row.
         */
        this.rowClick = new EventEmitter();
        /**
         * Emitted when the user double-clicks a row.
         */
        this.rowDblClick = new EventEmitter();
        /**
         * Emitted before the context menu is displayed for a row.
         */
        this.showRowContextMenu = new EventEmitter();
        /**
         * Emitted before the actions menu is displayed for a row.
         */
        this.showRowActionsMenu = new EventEmitter();
        /**
         * Emitted when the user executes a row action.
         */
        this.executeRowAction = new EventEmitter();
        /**
         * Flag that indicates if the datatable is in loading state and needs to show the
         * loading template (see the docs to learn how to configure a loading template).
         */
        this.loading = false;
        /**
         * Flag that indicates if the datatable should show the "no permission" template.
         */
        this.noPermission = false;
        /**
         * Should the items for the row actions menu be cached for reuse after they are loaded
         * the first time?
         */
        this.rowMenuCacheEnabled = true;
        this.isSelectAllChecked = false;
        this.selection = new Array();
        /**
         * This array of fake rows fix the flex layout for the gallery view
         */
        this.fakeRows = [];
        this.rowMenuCache = {};
        this.subscriptions = [];
        if (differs) {
            this.differ = differs.find([]).create(null);
        }
        this.click$ = new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        function (observer) { return _this.clickObserver = observer; }))
            .pipe(share());
    }
    /**
     * @return {?}
     */
    DataTableComponent.prototype.ngAfterContentInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.columnList) {
            this.subscriptions.push(this.columnList.columns.changes.subscribe((/**
             * @return {?}
             */
            function () {
                _this.setTableSchema();
            })));
        }
        this.datatableLayoutFix();
        this.setTableSchema();
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    DataTableComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        this.initAndSubscribeClickStream();
        if (this.isPropertyChanged(changes['data'])) {
            if (this.isTableEmpty()) {
                this.initTable();
            }
            else {
                this.data = changes['data'].currentValue;
                this.resetSelection();
            }
            return;
        }
        if (this.isPropertyChanged(changes['rows'])) {
            if (this.isTableEmpty()) {
                this.initTable();
            }
            else {
                this.setTableRows(changes['rows'].currentValue);
            }
            return;
        }
        if (changes.selectionMode && !changes.selectionMode.isFirstChange()) {
            this.resetSelection();
            this.emitRowSelectionEvent('row-unselect', null);
        }
        if (this.isPropertyChanged(changes['sorting'])) {
            this.setTableSorting(changes['sorting'].currentValue);
        }
        if (this.isPropertyChanged(changes['display'])) {
            this.datatableLayoutFix();
        }
    };
    /**
     * @return {?}
     */
    DataTableComponent.prototype.ngDoCheck = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var changes = this.differ.diff(this.rows);
        if (changes) {
            this.setTableRows(this.rows);
        }
    };
    /**
     * @param {?} property
     * @return {?}
     */
    DataTableComponent.prototype.isPropertyChanged = /**
     * @param {?} property
     * @return {?}
     */
    function (property) {
        return property && property.currentValue ? true : false;
    };
    /**
     * @param {?} rows
     * @return {?}
     */
    DataTableComponent.prototype.convertToRowsData = /**
     * @param {?} rows
     * @return {?}
     */
    function (rows) {
        return rows.map((/**
         * @param {?} row
         * @return {?}
         */
        function (row) { return new ObjectDataRow(row, row.isSelected); }));
    };
    /**
     * @param {?} sorting
     * @return {?}
     */
    DataTableComponent.prototype.convertToDataSorting = /**
     * @param {?} sorting
     * @return {?}
     */
    function (sorting) {
        if (sorting && sorting.length > 0) {
            return new DataSorting(sorting[0], sorting[1]);
        }
    };
    /**
     * @private
     * @return {?}
     */
    DataTableComponent.prototype.initAndSubscribeClickStream = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        this.unsubscribeClickStream();
        /** @type {?} */
        var singleClickStream = this.click$
            .pipe(buffer(this.click$.pipe(debounceTime(250))), map((/**
         * @param {?} list
         * @return {?}
         */
        function (list) { return list; })), filter((/**
         * @param {?} x
         * @return {?}
         */
        function (x) { return x.length === 1; })));
        this.singleClickStreamSub = singleClickStream.subscribe((/**
         * @param {?} dataRowEvents
         * @return {?}
         */
        function (dataRowEvents) {
            /** @type {?} */
            var event = dataRowEvents[0];
            _this.handleRowSelection(event.value, (/** @type {?} */ (event.event)));
            _this.rowClick.emit(event);
            if (!event.defaultPrevented) {
                _this.elementRef.nativeElement.dispatchEvent(new CustomEvent('row-click', {
                    detail: event,
                    bubbles: true
                }));
            }
        }));
        /** @type {?} */
        var multiClickStream = this.click$
            .pipe(buffer(this.click$.pipe(debounceTime(250))), map((/**
         * @param {?} list
         * @return {?}
         */
        function (list) { return list; })), filter((/**
         * @param {?} x
         * @return {?}
         */
        function (x) { return x.length >= 2; })));
        this.multiClickStreamSub = multiClickStream.subscribe((/**
         * @param {?} dataRowEvents
         * @return {?}
         */
        function (dataRowEvents) {
            /** @type {?} */
            var event = dataRowEvents[0];
            _this.rowDblClick.emit(event);
            if (!event.defaultPrevented) {
                _this.elementRef.nativeElement.dispatchEvent(new CustomEvent('row-dblclick', {
                    detail: event,
                    bubbles: true
                }));
            }
        }));
    };
    /**
     * @private
     * @return {?}
     */
    DataTableComponent.prototype.unsubscribeClickStream = /**
     * @private
     * @return {?}
     */
    function () {
        if (this.singleClickStreamSub) {
            this.singleClickStreamSub.unsubscribe();
            this.singleClickStreamSub = null;
        }
        if (this.multiClickStreamSub) {
            this.multiClickStreamSub.unsubscribe();
            this.multiClickStreamSub = null;
        }
    };
    /**
     * @private
     * @return {?}
     */
    DataTableComponent.prototype.initTable = /**
     * @private
     * @return {?}
     */
    function () {
        this.data = new ObjectDataTableAdapter(this.rows, this.columns);
        this.setTableSorting(this.sorting);
        this.resetSelection();
        this.rowMenuCache = {};
    };
    /**
     * @return {?}
     */
    DataTableComponent.prototype.isTableEmpty = /**
     * @return {?}
     */
    function () {
        return this.data === undefined || this.data === null;
    };
    /**
     * @private
     * @param {?} rows
     * @return {?}
     */
    DataTableComponent.prototype.setTableRows = /**
     * @private
     * @param {?} rows
     * @return {?}
     */
    function (rows) {
        if (this.data) {
            this.resetSelection();
            this.data.setRows(this.convertToRowsData(rows));
        }
    };
    /**
     * @private
     * @return {?}
     */
    DataTableComponent.prototype.setTableSchema = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var schema = [];
        if (!this.columns || this.columns.length === 0) {
            schema = this.getSchemaFromHtml();
        }
        else {
            schema = this.columns.concat(this.getSchemaFromHtml());
        }
        this.columns = schema;
        if (this.data && this.columns && this.columns.length > 0) {
            this.data.setColumns(this.columns);
        }
    };
    /**
     * @private
     * @param {?} sorting
     * @return {?}
     */
    DataTableComponent.prototype.setTableSorting = /**
     * @private
     * @param {?} sorting
     * @return {?}
     */
    function (sorting) {
        if (this.data) {
            this.data.setSorting(this.convertToDataSorting(sorting));
        }
    };
    /**
     * @return {?}
     */
    DataTableComponent.prototype.getSchemaFromHtml = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var schema = [];
        if (this.columnList && this.columnList.columns && this.columnList.columns.length > 0) {
            schema = this.columnList.columns.map((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return (/** @type {?} */ (c)); }));
        }
        return schema;
    };
    /**
     * @param {?} row
     * @param {?} mouseEvent
     * @return {?}
     */
    DataTableComponent.prototype.onRowClick = /**
     * @param {?} row
     * @param {?} mouseEvent
     * @return {?}
     */
    function (row, mouseEvent) {
        if (mouseEvent) {
            mouseEvent.preventDefault();
        }
        if (row) {
            /** @type {?} */
            var dataRowEvent = new DataRowEvent(row, mouseEvent, this);
            this.clickObserver.next(dataRowEvent);
        }
    };
    /**
     * @param {?} row
     * @param {?} e
     * @return {?}
     */
    DataTableComponent.prototype.onEnterKeyPressed = /**
     * @param {?} row
     * @param {?} e
     * @return {?}
     */
    function (row, e) {
        if (row) {
            this.handleRowSelection(row, e);
        }
    };
    /**
     * @private
     * @param {?} row
     * @param {?} e
     * @return {?}
     */
    DataTableComponent.prototype.handleRowSelection = /**
     * @private
     * @param {?} row
     * @param {?} e
     * @return {?}
     */
    function (row, e) {
        if (this.data) {
            if (this.isSingleSelectionMode()) {
                this.resetSelection();
                this.selectRow(row, true);
                this.emitRowSelectionEvent('row-select', row);
            }
            if (this.isMultiSelectionMode()) {
                /** @type {?} */
                var modifier = e && (e.metaKey || e.ctrlKey);
                /** @type {?} */
                var newValue = void 0;
                if (this.selection.length === 1) {
                    newValue = !row.isSelected;
                }
                else {
                    newValue = modifier ? !row.isSelected : true;
                }
                /** @type {?} */
                var domEventName = newValue ? 'row-select' : 'row-unselect';
                if (!modifier) {
                    this.resetSelection();
                }
                this.selectRow(row, newValue);
                this.emitRowSelectionEvent(domEventName, row);
            }
        }
    };
    /**
     * @return {?}
     */
    DataTableComponent.prototype.resetSelection = /**
     * @return {?}
     */
    function () {
        if (this.data) {
            /** @type {?} */
            var rows = this.data.getRows();
            if (rows && rows.length > 0) {
                rows.forEach((/**
                 * @param {?} r
                 * @return {?}
                 */
                function (r) { return r.isSelected = false; }));
            }
            this.selection = [];
        }
        this.isSelectAllChecked = false;
    };
    /**
     * @param {?} row
     * @param {?=} event
     * @return {?}
     */
    DataTableComponent.prototype.onRowDblClick = /**
     * @param {?} row
     * @param {?=} event
     * @return {?}
     */
    function (row, event) {
        if (event) {
            event.preventDefault();
        }
        /** @type {?} */
        var dataRowEvent = new DataRowEvent(row, event, this);
        this.clickObserver.next(dataRowEvent);
    };
    /**
     * @param {?} row
     * @param {?} e
     * @return {?}
     */
    DataTableComponent.prototype.onRowKeyUp = /**
     * @param {?} row
     * @param {?} e
     * @return {?}
     */
    function (row, e) {
        /** @type {?} */
        var event = new CustomEvent('row-keyup', {
            detail: {
                row: row,
                keyboardEvent: e,
                sender: this
            },
            bubbles: true
        });
        this.elementRef.nativeElement.dispatchEvent(event);
        if (event.defaultPrevented) {
            e.preventDefault();
        }
        else {
            if (e.key === 'Enter') {
                this.onKeyboardNavigate(row, e);
            }
        }
    };
    /**
     * @private
     * @param {?} row
     * @param {?} keyboardEvent
     * @return {?}
     */
    DataTableComponent.prototype.onKeyboardNavigate = /**
     * @private
     * @param {?} row
     * @param {?} keyboardEvent
     * @return {?}
     */
    function (row, keyboardEvent) {
        if (keyboardEvent) {
            keyboardEvent.preventDefault();
        }
        /** @type {?} */
        var event = new DataRowEvent(row, keyboardEvent, this);
        this.rowDblClick.emit(event);
        this.elementRef.nativeElement.dispatchEvent(new CustomEvent('row-dblclick', {
            detail: event,
            bubbles: true
        }));
    };
    /**
     * @param {?} column
     * @return {?}
     */
    DataTableComponent.prototype.onColumnHeaderClick = /**
     * @param {?} column
     * @return {?}
     */
    function (column) {
        if (column && column.sortable) {
            /** @type {?} */
            var current = this.data.getSorting();
            /** @type {?} */
            var newDirection = 'asc';
            if (current && column.key === current.key) {
                newDirection = current.direction === 'asc' ? 'desc' : 'asc';
            }
            this.data.setSorting(new DataSorting(column.key, newDirection));
            this.emitSortingChangedEvent(column.key, newDirection);
        }
    };
    /**
     * @param {?} matCheckboxChange
     * @return {?}
     */
    DataTableComponent.prototype.onSelectAllClick = /**
     * @param {?} matCheckboxChange
     * @return {?}
     */
    function (matCheckboxChange) {
        this.isSelectAllChecked = matCheckboxChange.checked;
        if (this.multiselect) {
            /** @type {?} */
            var rows = this.data.getRows();
            if (rows && rows.length > 0) {
                for (var i = 0; i < rows.length; i++) {
                    this.selectRow(rows[i], matCheckboxChange.checked);
                }
            }
            /** @type {?} */
            var domEventName = matCheckboxChange.checked ? 'row-select' : 'row-unselect';
            /** @type {?} */
            var row = this.selection.length > 0 ? this.selection[0] : null;
            this.emitRowSelectionEvent(domEventName, row);
        }
    };
    /**
     * @param {?} row
     * @param {?} event
     * @return {?}
     */
    DataTableComponent.prototype.onCheckboxChange = /**
     * @param {?} row
     * @param {?} event
     * @return {?}
     */
    function (row, event) {
        /** @type {?} */
        var newValue = event.checked;
        this.selectRow(row, newValue);
        /** @type {?} */
        var domEventName = newValue ? 'row-select' : 'row-unselect';
        this.emitRowSelectionEvent(domEventName, row);
    };
    /**
     * @param {?} event
     * @param {?} row
     * @return {?}
     */
    DataTableComponent.prototype.onImageLoadingError = /**
     * @param {?} event
     * @param {?} row
     * @return {?}
     */
    function (event, row) {
        if (event) {
            /** @type {?} */
            var element = (/** @type {?} */ (event.target));
            if (this.fallbackThumbnail) {
                element.src = this.fallbackThumbnail;
            }
            else {
                element.src = row.imageErrorResolver(event);
            }
        }
    };
    /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    DataTableComponent.prototype.isIconValue = /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    function (row, col) {
        if (row && col) {
            /** @type {?} */
            var value = row.getValue(col.key);
            return value && value.startsWith('material-icons://');
        }
        return false;
    };
    /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    DataTableComponent.prototype.asIconValue = /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    function (row, col) {
        if (this.isIconValue(row, col)) {
            /** @type {?} */
            var value = row.getValue(col.key) || '';
            return value.replace('material-icons://', '');
        }
        return null;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    DataTableComponent.prototype.iconAltTextKey = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        return value ? 'ICONS.' + value.substring(value.lastIndexOf('/') + 1).replace(/\.[a-z]+/, '') : '';
    };
    /**
     * @param {?} col
     * @param {?} direction
     * @return {?}
     */
    DataTableComponent.prototype.isColumnSorted = /**
     * @param {?} col
     * @param {?} direction
     * @return {?}
     */
    function (col, direction) {
        if (col && direction) {
            /** @type {?} */
            var sorting = this.data.getSorting();
            return sorting && sorting.key === col.key && sorting.direction === direction;
        }
        return false;
    };
    /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    DataTableComponent.prototype.getContextMenuActions = /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    function (row, col) {
        /** @type {?} */
        var event = new DataCellEvent(row, col, []);
        this.showRowContextMenu.emit(event);
        return event.value.actions;
    };
    /**
     * @param {?} row
     * @param {?=} col
     * @return {?}
     */
    DataTableComponent.prototype.getRowActions = /**
     * @param {?} row
     * @param {?=} col
     * @return {?}
     */
    function (row, col) {
        /** @type {?} */
        var id = row.getValue('id');
        if (!this.rowMenuCache[id]) {
            /** @type {?} */
            var event_1 = new DataCellEvent(row, col, []);
            this.showRowActionsMenu.emit(event_1);
            if (!this.rowMenuCacheEnabled) {
                return event_1.value.actions;
            }
            this.rowMenuCache[id] = event_1.value.actions;
        }
        return this.rowMenuCache[id];
    };
    /**
     * @param {?} row
     * @param {?} action
     * @return {?}
     */
    DataTableComponent.prototype.onExecuteRowAction = /**
     * @param {?} row
     * @param {?} action
     * @return {?}
     */
    function (row, action) {
        if (action.disabled || action.disabled) {
            event.stopPropagation();
        }
        else {
            this.executeRowAction.emit(new DataRowActionEvent(row, action));
        }
    };
    /**
     * @param {?} row
     * @return {?}
     */
    DataTableComponent.prototype.rowAllowsDrop = /**
     * @param {?} row
     * @return {?}
     */
    function (row) {
        return row.isDropTarget === true;
    };
    /**
     * @return {?}
     */
    DataTableComponent.prototype.hasSelectionMode = /**
     * @return {?}
     */
    function () {
        return this.isSingleSelectionMode() || this.isMultiSelectionMode();
    };
    /**
     * @return {?}
     */
    DataTableComponent.prototype.isSingleSelectionMode = /**
     * @return {?}
     */
    function () {
        return this.selectionMode && this.selectionMode.toLowerCase() === 'single';
    };
    /**
     * @return {?}
     */
    DataTableComponent.prototype.isMultiSelectionMode = /**
     * @return {?}
     */
    function () {
        return this.selectionMode && this.selectionMode.toLowerCase() === 'multiple';
    };
    /**
     * @param {?} row
     * @return {?}
     */
    DataTableComponent.prototype.getRowStyle = /**
     * @param {?} row
     * @return {?}
     */
    function (row) {
        row.cssClass = row.cssClass ? row.cssClass : '';
        this.rowStyleClass = this.rowStyleClass ? this.rowStyleClass : '';
        return row.cssClass + " " + this.rowStyleClass;
    };
    /**
     * @return {?}
     */
    DataTableComponent.prototype.getSortingKey = /**
     * @return {?}
     */
    function () {
        if (this.data.getSorting()) {
            return this.data.getSorting().key;
        }
    };
    /**
     * @param {?} row
     * @param {?} value
     * @return {?}
     */
    DataTableComponent.prototype.selectRow = /**
     * @param {?} row
     * @param {?} value
     * @return {?}
     */
    function (row, value) {
        if (row) {
            row.isSelected = value;
            /** @type {?} */
            var idx = this.selection.indexOf(row);
            if (value) {
                if (idx < 0) {
                    this.selection.push(row);
                }
            }
            else {
                if (idx > -1) {
                    this.selection.splice(idx, 1);
                }
            }
        }
    };
    /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    DataTableComponent.prototype.getCellTooltip = /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    function (row, col) {
        if (row && col && col.formatTooltip) {
            /** @type {?} */
            var result = col.formatTooltip(row, col);
            if (result) {
                return result;
            }
        }
        return null;
    };
    /**
     * @return {?}
     */
    DataTableComponent.prototype.getSortableColumns = /**
     * @return {?}
     */
    function () {
        return this.data.getColumns().filter((/**
         * @param {?} column
         * @return {?}
         */
        function (column) {
            return column.sortable === true;
        }));
    };
    /**
     * @return {?}
     */
    DataTableComponent.prototype.isEmpty = /**
     * @return {?}
     */
    function () {
        return this.data.getRows().length === 0;
    };
    /**
     * @return {?}
     */
    DataTableComponent.prototype.isHeaderVisible = /**
     * @return {?}
     */
    function () {
        return !this.loading && !this.isEmpty() && !this.noPermission;
    };
    /**
     * @return {?}
     */
    DataTableComponent.prototype.isStickyHeaderEnabled = /**
     * @return {?}
     */
    function () {
        return this.stickyHeader && this.isHeaderVisible();
    };
    /**
     * @private
     * @param {?} name
     * @param {?} row
     * @return {?}
     */
    DataTableComponent.prototype.emitRowSelectionEvent = /**
     * @private
     * @param {?} name
     * @param {?} row
     * @return {?}
     */
    function (name, row) {
        /** @type {?} */
        var domEvent = new CustomEvent(name, {
            detail: {
                row: row,
                selection: this.selection
            },
            bubbles: true
        });
        this.elementRef.nativeElement.dispatchEvent(domEvent);
    };
    /**
     * @private
     * @param {?} key
     * @param {?} direction
     * @return {?}
     */
    DataTableComponent.prototype.emitSortingChangedEvent = /**
     * @private
     * @param {?} key
     * @param {?} direction
     * @return {?}
     */
    function (key, direction) {
        /** @type {?} */
        var domEvent = new CustomEvent('sorting-changed', {
            detail: {
                key: key,
                direction: direction
            },
            bubbles: true
        });
        this.elementRef.nativeElement.dispatchEvent(domEvent);
    };
    /**
     * @return {?}
     */
    DataTableComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.unsubscribeClickStream();
        this.subscriptions.forEach((/**
         * @param {?} s
         * @return {?}
         */
        function (s) { return s.unsubscribe(); }));
        this.subscriptions = [];
        if (this.dataRowsChanged) {
            this.dataRowsChanged.unsubscribe();
            this.dataRowsChanged = null;
        }
    };
    /**
     * @return {?}
     */
    DataTableComponent.prototype.datatableLayoutFix = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var maxGalleryRows = 25;
        if (this.display === 'gallery') {
            for (var i = 0; i < maxGalleryRows; i++) {
                this.fakeRows.push('');
            }
        }
        else {
            this.fakeRows = [];
        }
    };
    /**
     * @return {?}
     */
    DataTableComponent.prototype.getNameColumnValue = /**
     * @return {?}
     */
    function () {
        return this.data.getColumns().find((/**
         * @param {?} el
         * @return {?}
         */
        function (el) {
            return el.key.includes('name');
        }));
    };
    /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    DataTableComponent.prototype.getAutomationValue = /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    function (row, col) {
        /** @type {?} */
        var name = this.getNameColumnValue();
        return name ? row.getValue(name.key) : '';
    };
    DataTableComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-datatable',
                    template: "<div\r\n    role=\"grid\"\r\n    *ngIf=\"data\" class=\"adf-full-width\"\r\n    [class.adf-datatable-card]=\"display === 'gallery'\"\r\n    [class.adf-datatable-list]=\"display === 'list'\"\r\n    [class.adf-sticky-header]=\"isStickyHeaderEnabled()\"\r\n    [class.adf-datatable--empty]=\"!isHeaderVisible()\">\r\n    <div *ngIf=\"showHeader && isHeaderVisible()\" class=\"adf-datatable-header\" role=\"rowgroup\">\r\n        <div class=\"adf-datatable-row\" *ngIf=\"display === 'list'\" role=\"row\">\r\n            <!-- Actions (left) -->\r\n            <div *ngIf=\"actions && actionsPosition === 'left'\" class=\"adf-actions-column adf-datatable-cell-header\">\r\n                <span class=\"adf-sr-only\">Actions</span>\r\n            </div>\r\n            <!-- Columns -->\r\n            <div *ngIf=\"multiselect\" class=\"adf-datatable-cell-header adf-datatable-checkbox\">\r\n                <mat-checkbox [checked]=\"isSelectAllChecked\" (change)=\"onSelectAllClick($event)\" class=\"adf-checkbox-sr-only\">{{ 'ADF-DATATABLE.ACCESSIBILITY.SELECT_ALL' | translate }}</mat-checkbox>\r\n            </div>\r\n            <div class=\"adf-datatable-cell--{{col.type || 'text'}} {{col.cssClass}} adf-datatable-cell-header\"\r\n                 *ngFor=\"let col of data.getColumns()\"\r\n                 [class.adf-sortable]=\"col.sortable\"\r\n                 [attr.data-automation-id]=\"'auto_id_' + col.key\"\r\n                 [class.adf-datatable__header--sorted-asc]=\"isColumnSorted(col, 'asc')\"\r\n                 [class.adf-datatable__header--sorted-desc]=\"isColumnSorted(col, 'desc')\"\r\n                 (click)=\"onColumnHeaderClick(col)\"\r\n                 (keyup.enter)=\"onColumnHeaderClick(col)\"\r\n                 role=\"columnheader\"\r\n                 tabindex=\"0\"\r\n                 title=\"{{ col.title | translate }}\"\r\n                 adf-drop-zone dropTarget=\"header\" [dropColumn]=\"col\">\r\n                <span *ngIf=\"col.srTitle\" class=\"adf-sr-only\">{{ col.srTitle | translate }}</span>\r\n                <span *ngIf=\"col.title\" class=\"adf-datatable-cell-value\">{{ col.title | translate}}</span>\r\n            </div>\r\n            <!-- Actions (right) -->\r\n            <div *ngIf=\"actions && actionsPosition === 'right'\" class=\"adf-actions-column adf-datatable-cell-header adf-datatable__actions-cell\">\r\n                <span class=\"adf-sr-only\">Actions</span>\r\n            </div>\r\n        </div>\r\n        <mat-form-field *ngIf=\"display === 'gallery'\">\r\n            <mat-select [value]=\"getSortingKey()\" [attr.data-automation-id]=\"'grid-view-sorting'\">\r\n                <mat-option *ngFor=\"let col of getSortableColumns()\"\r\n                            [value]=\"col.key\"\r\n                            [attr.data-automation-id]=\"'grid-view-sorting-'+col.title\"\r\n                            (click)=\"onColumnHeaderClick(col)\"\r\n                            (keyup.enter)=\"onColumnHeaderClick(col)\">\r\n                    {{ col.title | translate}}\r\n                </mat-option>\r\n            </mat-select>\r\n        </mat-form-field>\r\n    </div>\r\n\r\n    <div class=\"adf-datatable-body\" role=\"rowgroup\">\r\n        <ng-container *ngIf=\"!loading && !noPermission\">\r\n            <div *ngFor=\"let row of data.getRows(); let idx = index\"\r\n                 class=\"adf-datatable-row\"\r\n                 role=\"row\"\r\n                 [class.adf-is-selected]=\"row.isSelected\"\r\n                 [adf-upload]=\"allowDropFiles && rowAllowsDrop(row)\" [adf-upload-data]=\"row\"\r\n                 [ngStyle]=\"rowStyle\"\r\n                 [ngClass]=\"getRowStyle(row)\"\r\n                 (keyup)=\"onRowKeyUp(row, $event)\">\r\n                <!-- Actions (left) -->\r\n                <div *ngIf=\"actions && actionsPosition === 'left'\" role=\"gridcell\" class=\"adf-datatable-cell\">\r\n                    <button mat-icon-button [matMenuTriggerFor]=\"menu\"\r\n                            [title]=\"'ADF-DATATABLE.CONTENT-ACTIONS.TOOLTIP' | translate\"\r\n                            [attr.id]=\"'action_menu_left_' + idx\"\r\n                            [attr.data-automation-id]=\"'action_menu_' + idx\">\r\n                        <mat-icon>more_vert</mat-icon>\r\n                    </button>\r\n                    <mat-menu #menu=\"matMenu\">\r\n                        <button mat-menu-item *ngFor=\"let action of getRowActions(row)\"\r\n                                [attr.data-automation-id]=\"action.title\"\r\n                                [disabled]=\"action.disabled\"\r\n                                (click)=\"onExecuteRowAction(row, action)\">\r\n                            <mat-icon *ngIf=\"action.icon\">{{ action.icon }}</mat-icon>\r\n                            <span>{{ action.title | translate }}</span>\r\n                        </button>\r\n                    </mat-menu>\r\n                </div>\r\n\r\n                <div *ngIf=\"multiselect\" class=\"adf-datatable-cell adf-datatable-checkbox\">\r\n                    <mat-checkbox\r\n                        [checked]=\"row.isSelected\"\r\n                        [attr.aria-checked]=\"row.isSelected\"\r\n                        role=\"checkbox\"\r\n                        (change)=\"onCheckboxChange(row, $event)\"\r\n                        class=\"adf-checkbox-sr-only\">\r\n                        {{ 'ADF-DATATABLE.ACCESSIBILITY.SELECT_FILE' | translate }}\r\n                    </mat-checkbox>\r\n                </div>\r\n                <div *ngFor=\"let col of data.getColumns()\"\r\n                     role=\"gridcell\"\r\n                     class=\" adf-datatable-cell adf-datatable-cell--{{col.type || 'text'}} {{col.cssClass}}\"\r\n                     [attr.title]=\"col.title | translate\"\r\n                     [attr.data-automation-id]=\"getAutomationValue(row, col)\"\r\n                     tabindex=\"0\"\r\n                     (click)=\"onRowClick(row, $event)\"\r\n                     (keydown.enter)=\"onEnterKeyPressed(row, $event)\"\r\n                     [adf-context-menu]=\"getContextMenuActions(row, col)\"\r\n                     [adf-context-menu-enabled]=\"contextMenu\"\r\n                     adf-drop-zone dropTarget=\"cell\" [dropColumn]=\"col\" [dropRow]=\"row\">\r\n                    <div *ngIf=\"!col.template\" class=\"adf-datatable-cell-container\">\r\n                        <ng-container [ngSwitch]=\"col.type\">\r\n                            <div *ngSwitchCase=\"'image'\" class=\"adf-cell-value\">\r\n                                <mat-icon *ngIf=\"isIconValue(row, col); else no_iconvalue\">{{ asIconValue(row, col) }}\r\n                                </mat-icon>\r\n                                <ng-template #no_iconvalue>\r\n                                    <mat-icon class=\"adf-datatable-selected\"\r\n                                              *ngIf=\"row.isSelected && !multiselect; else no_selected_row\" svgIcon=\"selected\">\r\n                                    </mat-icon>\r\n                                    <ng-template #no_selected_row>\r\n                                        <img class=\"adf-datatable-center-img-ie\"\r\n                                            [attr.aria-label]=\"data.getValue(row, col) | fileType\"\r\n                                            alt=\"{{ iconAltTextKey(data.getValue(row, col)) | translate }}\"\r\n                                            src=\"{{ data.getValue(row, col) }}\"\r\n                                            (error)=\"onImageLoadingError($event, row)\">\r\n                                    </ng-template>\r\n                                </ng-template>\r\n                            </div>\r\n                            <div *ngSwitchCase=\"'icon'\" class=\"adf-cell-value\">\r\n                                <span class=\"adf-sr-only\">{{ iconAltTextKey(data.getValue(row, col)) | translate }}</span>\r\n                                <mat-icon>{{ data.getValue(row, col) }}</mat-icon>\r\n                            </div>\r\n                            <div *ngSwitchCase=\"'date'\" class=\"adf-cell-value\"\r\n                                 [attr.data-automation-id]=\"'date_' + (data.getValue(row, col) | adfLocalizedDate: 'medium') \">\r\n                                <adf-date-cell class=\"adf-datatable-center-date-column-ie\"\r\n                                    [data]=\"data\"\r\n                                    [column]=\"col\"\r\n                                    [row]=\"row\"\r\n                                    [tooltip]=\"getCellTooltip(row, col)\">\r\n                                </adf-date-cell>\r\n                            </div>\r\n                            <div *ngSwitchCase=\"'location'\" class=\"adf-cell-value\"\r\n                                 [attr.data-automation-id]=\"'location' + data.getValue(row, col)\">\r\n                                <adf-location-cell\r\n                                    [data]=\"data\"\r\n                                    [column]=\"col\"\r\n                                    [row]=\"row\"\r\n                                    [tooltip]=\"getCellTooltip(row, col)\">\r\n                                </adf-location-cell>\r\n                            </div>\r\n                            <div *ngSwitchCase=\"'fileSize'\" class=\"adf-cell-value\"\r\n                                 [attr.data-automation-id]=\"'fileSize_' + data.getValue(row, col)\">\r\n                                <adf-filesize-cell class=\"adf-datatable-center-size-column-ie\"\r\n                                    [data]=\"data\"\r\n                                    [column]=\"col\"\r\n                                    [row]=\"row\"\r\n                                    [tooltip]=\"getCellTooltip(row, col)\">\r\n                                </adf-filesize-cell>\r\n                            </div>\r\n                            <div *ngSwitchCase=\"'text'\" class=\"adf-cell-value\"\r\n                                 [attr.data-automation-id]=\"'text_' + data.getValue(row, col)\">\r\n                                <adf-datatable-cell\r\n                                    [copyContent]=\"col.copyContent\"\r\n                                    [data]=\"data\"\r\n                                    [column]=\"col\"\r\n                                    [row]=\"row\"\r\n                                    [tooltip]=\"getCellTooltip(row, col)\">\r\n                                </adf-datatable-cell>\r\n                            </div>\r\n                            <div *ngSwitchCase=\"'json'\" class=\"adf-cell-value\">\r\n                                <adf-json-cell\r\n                                    [copyContent]=\"col.copyContent\"\r\n                                    [data]=\"data\"\r\n                                    [column]=\"col\"\r\n                                    [row]=\"row\">\r\n                                </adf-json-cell>\r\n                            </div>\r\n                            <span *ngSwitchDefault class=\"adf-cell-value\">\r\n                    <!-- empty cell for unknown column type -->\r\n                    </span>\r\n                        </ng-container>\r\n                    </div>\r\n                    <div *ngIf=\"col.template\" class=\"adf-datatable-cell-container\">\r\n                        <div class=\"adf-cell-value\">\r\n                            <ng-container\r\n                                [ngTemplateOutlet]=\"col.template\"\r\n                                [ngTemplateOutletContext]=\"{ $implicit: { data: data, row: row, col: col }, value: data.getValue(row, col) }\">\r\n                            </ng-container>\r\n                        </div>\r\n                    </div>\r\n                </div>\r\n\r\n                <!-- Actions (right) -->\r\n                <div *ngIf=\"actions && actionsPosition === 'right'\"\r\n                     role=\"gridcell\"\r\n                     class=\"adf-datatable-cell adf-datatable__actions-cell adf-datatable-center-actions-column-ie\">\r\n                    <button mat-icon-button [matMenuTriggerFor]=\"menu\"\r\n                            [title]=\"'ADF-DATATABLE.CONTENT-ACTIONS.TOOLTIP' | translate\"\r\n                            [attr.id]=\"'action_menu_right_' + idx\"\r\n                            [attr.data-automation-id]=\"'action_menu_' + idx\">\r\n                        <mat-icon>more_vert</mat-icon>\r\n                    </button>\r\n                    <mat-menu #menu=\"matMenu\">\r\n                        <button mat-menu-item *ngFor=\"let action of getRowActions(row)\"\r\n                                [attr.data-automation-id]=\"action.title\"\r\n                                [attr.aria-label]=\"action.title | translate\"\r\n                                [disabled]=\"action.disabled\"\r\n                                (click)=\"onExecuteRowAction(row, action)\">\r\n                            <mat-icon *ngIf=\"action.icon\">{{ action.icon }}</mat-icon>\r\n                            <span>{{ action.title | translate }}</span>\r\n                        </button>\r\n                    </mat-menu>\r\n                </div>\r\n\r\n            </div>\r\n            <div *ngIf=\"isEmpty()\"\r\n                 role=\"row\"\r\n                 [class.adf-datatable-row]=\"display === 'list'\"\r\n                 [class.adf-datatable-card-empty]=\"display === 'gallery'\">\r\n                <div class=\"adf-no-content-container adf-datatable-cell\" role=\"gridcell\">\r\n                    <ng-template *ngIf=\"noContentTemplate\"\r\n                                 ngFor [ngForOf]=\"[data]\"\r\n                                 [ngForTemplate]=\"noContentTemplate\">\r\n                    </ng-template>\r\n                    <ng-content select=\"adf-empty-list\"></ng-content>\r\n                </div>\r\n            </div>\r\n            <div *ngFor=\"let row of fakeRows\"\r\n                 class=\"adf-datatable-row adf-datatable-row-empty-card\">\r\n            </div>\r\n        </ng-container>\r\n        <div *ngIf=\"!loading && noPermission\"\r\n             role=\"row\"\r\n             [class.adf-datatable-row]=\"display === 'list'\"\r\n             [class.adf-datatable-card-permissions]=\"display === 'gallery'\"\r\n             class=\"adf-no-permission__row\">\r\n            <div class=\"adf-no-permission__cell adf-no-content-container adf-datatable-cell\">\r\n                <ng-template *ngIf=\"noPermissionTemplate\"\r\n                             ngFor [ngForOf]=\"[data]\"\r\n                             [ngForTemplate]=\"noPermissionTemplate\">\r\n                </ng-template>\r\n            </div>\r\n        </div>\r\n        <div *ngIf=\"loading\"\r\n             [class.adf-datatable-row]=\"display === 'list'\"\r\n             [class.adf-datatable-card-loading]=\"display === 'gallery'\">\r\n            <div class=\"adf-no-content-container adf-datatable-cell\">\r\n                <ng-template *ngIf=\"loadingTemplate\"\r\n                             ngFor [ngForOf]=\"[data]\"\r\n                             [ngForTemplate]=\"loadingTemplate\">\r\n                </ng-template>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    host: { class: 'adf-datatable' },
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    DataTableComponent.ctorParameters = function () { return [
        { type: ElementRef },
        { type: IterableDiffers }
    ]; };
    DataTableComponent.propDecorators = {
        columnList: [{ type: ContentChild, args: [DataColumnListComponent, { static: true },] }],
        data: [{ type: Input }],
        display: [{ type: Input }],
        rows: [{ type: Input }],
        sorting: [{ type: Input }],
        columns: [{ type: Input }],
        selectionMode: [{ type: Input }],
        multiselect: [{ type: Input }],
        actions: [{ type: Input }],
        actionsPosition: [{ type: Input }],
        fallbackThumbnail: [{ type: Input }],
        contextMenu: [{ type: Input }],
        allowDropFiles: [{ type: Input }],
        rowStyle: [{ type: Input }],
        rowStyleClass: [{ type: Input }],
        showHeader: [{ type: Input }],
        stickyHeader: [{ type: Input }],
        rowClick: [{ type: Output }],
        rowDblClick: [{ type: Output }],
        showRowContextMenu: [{ type: Output }],
        showRowActionsMenu: [{ type: Output }],
        executeRowAction: [{ type: Output }],
        loading: [{ type: Input }],
        noPermission: [{ type: Input }],
        rowMenuCacheEnabled: [{ type: Input }]
    };
    return DataTableComponent;
}());
export { DataTableComponent };
if (false) {
    /** @type {?} */
    DataTableComponent.prototype.columnList;
    /**
     * Data source for the table
     * @type {?}
     */
    DataTableComponent.prototype.data;
    /**
     * Selects the display mode of the table. Can be "list" or "gallery".
     * @type {?}
     */
    DataTableComponent.prototype.display;
    /**
     * The rows that the datatable will show.
     * @type {?}
     */
    DataTableComponent.prototype.rows;
    /**
     * Define the sort order of the datatable. Possible values are :
     * [`created`, `desc`], [`created`, `asc`], [`due`, `desc`], [`due`, `asc`]
     * @type {?}
     */
    DataTableComponent.prototype.sorting;
    /**
     * The columns that the datatable will show.
     * @type {?}
     */
    DataTableComponent.prototype.columns;
    /**
     * Row selection mode. Can be none, `single` or `multiple`. For `multiple` mode,
     * you can use Cmd (macOS) or Ctrl (Win) modifier key to toggle selection for multiple rows.
     * @type {?}
     */
    DataTableComponent.prototype.selectionMode;
    /**
     * Toggles multiple row selection, which renders checkboxes at the beginning of each row.
     * @type {?}
     */
    DataTableComponent.prototype.multiselect;
    /**
     * Toggles the data actions column.
     * @type {?}
     */
    DataTableComponent.prototype.actions;
    /**
     * Position of the actions dropdown menu. Can be "left" or "right".
     * @type {?}
     */
    DataTableComponent.prototype.actionsPosition;
    /**
     * Fallback image for rows where the thumbnail is missing.
     * @type {?}
     */
    DataTableComponent.prototype.fallbackThumbnail;
    /**
     * Toggles custom context menu for the component.
     * @type {?}
     */
    DataTableComponent.prototype.contextMenu;
    /**
     * Toggles file drop support for rows (see
     * [Upload directive](upload.directive.md) for further details).
     * @type {?}
     */
    DataTableComponent.prototype.allowDropFiles;
    /**
     * The inline style to apply to every row. See
     * [NgStyle](https://angular.io/docs/ts/latest/api/common/index/NgStyle-directive.html)
     * docs for more details and usage examples.
     * @type {?}
     */
    DataTableComponent.prototype.rowStyle;
    /**
     * The CSS class to apply to every row.
     * @type {?}
     */
    DataTableComponent.prototype.rowStyleClass;
    /**
     * Toggles the header.
     * @type {?}
     */
    DataTableComponent.prototype.showHeader;
    /**
     * Toggles the sticky header mode.
     * @type {?}
     */
    DataTableComponent.prototype.stickyHeader;
    /**
     * Emitted when the user clicks a row.
     * @type {?}
     */
    DataTableComponent.prototype.rowClick;
    /**
     * Emitted when the user double-clicks a row.
     * @type {?}
     */
    DataTableComponent.prototype.rowDblClick;
    /**
     * Emitted before the context menu is displayed for a row.
     * @type {?}
     */
    DataTableComponent.prototype.showRowContextMenu;
    /**
     * Emitted before the actions menu is displayed for a row.
     * @type {?}
     */
    DataTableComponent.prototype.showRowActionsMenu;
    /**
     * Emitted when the user executes a row action.
     * @type {?}
     */
    DataTableComponent.prototype.executeRowAction;
    /**
     * Flag that indicates if the datatable is in loading state and needs to show the
     * loading template (see the docs to learn how to configure a loading template).
     * @type {?}
     */
    DataTableComponent.prototype.loading;
    /**
     * Flag that indicates if the datatable should show the "no permission" template.
     * @type {?}
     */
    DataTableComponent.prototype.noPermission;
    /**
     * Should the items for the row actions menu be cached for reuse after they are loaded
     * the first time?
     * @type {?}
     */
    DataTableComponent.prototype.rowMenuCacheEnabled;
    /** @type {?} */
    DataTableComponent.prototype.noContentTemplate;
    /** @type {?} */
    DataTableComponent.prototype.noPermissionTemplate;
    /** @type {?} */
    DataTableComponent.prototype.loadingTemplate;
    /** @type {?} */
    DataTableComponent.prototype.isSelectAllChecked;
    /** @type {?} */
    DataTableComponent.prototype.selection;
    /**
     * This array of fake rows fix the flex layout for the gallery view
     * @type {?}
     */
    DataTableComponent.prototype.fakeRows;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.clickObserver;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.click$;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.differ;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.rowMenuCache;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.subscriptions;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.singleClickStreamSub;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.multiClickStreamSub;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.dataRowsChanged;
    /**
     * @type {?}
     * @private
     */
    DataTableComponent.prototype.elementRef;
}
/**
 * @record
 */
export function DataTableDropEvent() { }
if (false) {
    /** @type {?} */
    DataTableDropEvent.prototype.detail;
    /**
     * @return {?}
     */
    DataTableDropEvent.prototype.preventDefault = function () { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YXRhYmxlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRhdGF0YWJsZS9jb21wb25lbnRzL2RhdGF0YWJsZS9kYXRhdGFibGUuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxPQUFPLEVBQ2UsU0FBUyxFQUFFLFlBQVksRUFBVyxVQUFVLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFDbkYsZUFBZSxFQUFhLE1BQU0sRUFBNEMsaUJBQWlCLEVBQ2xHLE1BQU0sZUFBZSxDQUFDO0FBRXZCLE9BQU8sRUFBZ0IsVUFBVSxFQUFZLE1BQU0sTUFBTSxDQUFDO0FBQzFELE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLGlEQUFpRCxDQUFDO0FBRTFGLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUUvRCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFHNUQsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ2hFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxHQUFHLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7SUFHdEUsTUFBTyxNQUFNO0lBQ2IsU0FBVSxTQUFTOzs7QUFHdkI7SUErSUksNEJBQW9CLFVBQXNCLEVBQzlCLE9BQXdCO1FBRHBDLGlCQU9DO1FBUG1CLGVBQVUsR0FBVixVQUFVLENBQVk7Ozs7UUE3SDFDLFlBQU8sR0FBVyxXQUFXLENBQUMsSUFBSSxDQUFDOzs7O1FBSW5DLFNBQUksR0FBVSxFQUFFLENBQUM7Ozs7O1FBTWpCLFlBQU8sR0FBVSxFQUFFLENBQUM7Ozs7UUFJcEIsWUFBTyxHQUFVLEVBQUUsQ0FBQzs7Ozs7UUFNcEIsa0JBQWEsR0FBVyxRQUFRLENBQUMsQ0FBQyx1QkFBdUI7Ozs7O1FBSXpELGdCQUFXLEdBQVksS0FBSyxDQUFDOzs7O1FBSTdCLFlBQU8sR0FBWSxLQUFLLENBQUM7Ozs7UUFJekIsb0JBQWUsR0FBVyxPQUFPLENBQUMsQ0FBQyxhQUFhOzs7O1FBUWhELGdCQUFXLEdBQVksS0FBSyxDQUFDOzs7OztRQU03QixtQkFBYyxHQUFZLEtBQUssQ0FBQzs7OztRQVdoQyxrQkFBYSxHQUFXLEVBQUUsQ0FBQzs7OztRQUkzQixlQUFVLEdBQVksSUFBSSxDQUFDOzs7O1FBSTNCLGlCQUFZLEdBQVksS0FBSyxDQUFDOzs7O1FBSTlCLGFBQVEsR0FBRyxJQUFJLFlBQVksRUFBZ0IsQ0FBQzs7OztRQUk1QyxnQkFBVyxHQUFHLElBQUksWUFBWSxFQUFnQixDQUFDOzs7O1FBSS9DLHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFpQixDQUFDOzs7O1FBSXZELHVCQUFrQixHQUFHLElBQUksWUFBWSxFQUFpQixDQUFDOzs7O1FBSXZELHFCQUFnQixHQUFHLElBQUksWUFBWSxFQUFzQixDQUFDOzs7OztRQU0xRCxZQUFPLEdBQVksS0FBSyxDQUFDOzs7O1FBSXpCLGlCQUFZLEdBQVksS0FBSyxDQUFDOzs7OztRQU85Qix3QkFBbUIsR0FBRyxJQUFJLENBQUM7UUFNM0IsdUJBQWtCLEdBQVksS0FBSyxDQUFDO1FBQ3BDLGNBQVMsR0FBRyxJQUFJLEtBQUssRUFBVyxDQUFDOzs7O1FBR2pDLGFBQVEsR0FBRyxFQUFFLENBQUM7UUFNTixpQkFBWSxHQUFXLEVBQUUsQ0FBQztRQUUxQixrQkFBYSxHQUFtQixFQUFFLENBQUM7UUFPdkMsSUFBSSxPQUFPLEVBQUU7WUFDVCxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQy9DO1FBQ0QsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLFVBQVU7Ozs7UUFBZSxVQUFDLFFBQVEsSUFBSyxPQUFBLEtBQUksQ0FBQyxhQUFhLEdBQUcsUUFBUSxFQUE3QixDQUE2QixFQUFDO2FBQ2xGLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZCLENBQUM7Ozs7SUFFRCwrQ0FBa0I7OztJQUFsQjtRQUFBLGlCQVVDO1FBVEcsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUNuQixJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsU0FBUzs7O1lBQUM7Z0JBQ3RDLEtBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUMxQixDQUFDLEVBQUMsQ0FDTCxDQUFDO1NBQ0w7UUFDRCxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztRQUMxQixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDMUIsQ0FBQzs7Ozs7SUFFRCx3Q0FBVzs7OztJQUFYLFVBQVksT0FBc0I7UUFDOUIsSUFBSSxDQUFDLDJCQUEyQixFQUFFLENBQUM7UUFDbkMsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDLEVBQUU7WUFDekMsSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFLEVBQUU7Z0JBQ3JCLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQzthQUNwQjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsSUFBSSxHQUFHLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxZQUFZLENBQUM7Z0JBQ3pDLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQzthQUN6QjtZQUNELE9BQU87U0FDVjtRQUVELElBQUksSUFBSSxDQUFDLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxFQUFFO1lBQ3pDLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRSxFQUFFO2dCQUNyQixJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7YUFDcEI7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDbkQ7WUFDRCxPQUFPO1NBQ1Y7UUFFRCxJQUFJLE9BQU8sQ0FBQyxhQUFhLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYSxDQUFDLGFBQWEsRUFBRSxFQUFFO1lBQ2pFLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN0QixJQUFJLENBQUMscUJBQXFCLENBQUMsY0FBYyxFQUFFLElBQUksQ0FBQyxDQUFDO1NBQ3BEO1FBRUQsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUU7WUFDNUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDekQ7UUFFRCxJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUMsRUFBRTtZQUM1QyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztTQUM3QjtJQUNMLENBQUM7Ozs7SUFFRCxzQ0FBUzs7O0lBQVQ7O1lBQ1UsT0FBTyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUM7UUFDM0MsSUFBSSxPQUFPLEVBQUU7WUFDVCxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNoQztJQUNMLENBQUM7Ozs7O0lBRUQsOENBQWlCOzs7O0lBQWpCLFVBQWtCLFFBQXNCO1FBQ3BDLE9BQU8sUUFBUSxJQUFJLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQzVELENBQUM7Ozs7O0lBRUQsOENBQWlCOzs7O0lBQWpCLFVBQWtCLElBQVk7UUFDMUIsT0FBTyxJQUFJLENBQUMsR0FBRzs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsSUFBSSxhQUFhLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxVQUFVLENBQUMsRUFBdEMsQ0FBc0MsRUFBQyxDQUFDO0lBQ3JFLENBQUM7Ozs7O0lBRUQsaURBQW9COzs7O0lBQXBCLFVBQXFCLE9BQWM7UUFDL0IsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDL0IsT0FBTyxJQUFJLFdBQVcsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDbEQ7SUFDTCxDQUFDOzs7OztJQUVPLHdEQUEyQjs7OztJQUFuQztRQUFBLGlCQWtEQztRQWpERyxJQUFJLENBQUMsc0JBQXNCLEVBQUUsQ0FBQzs7WUFDeEIsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLE1BQU07YUFDaEMsSUFBSSxDQUNELE1BQU0sQ0FDRixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FDWixZQUFZLENBQUMsR0FBRyxDQUFDLENBQ3BCLENBQ0osRUFDRCxHQUFHOzs7O1FBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLEVBQUosQ0FBSSxFQUFDLEVBQ25CLE1BQU07Ozs7UUFBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFkLENBQWMsRUFBQyxDQUNoQztRQUVMLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxpQkFBaUIsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxhQUE2Qjs7Z0JBQzVFLEtBQUssR0FBaUIsYUFBYSxDQUFDLENBQUMsQ0FBQztZQUM1QyxLQUFJLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxtQkFBNkIsS0FBSyxDQUFDLEtBQUssRUFBQSxDQUFDLENBQUM7WUFDL0UsS0FBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDMUIsSUFBSSxDQUFDLEtBQUssQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDekIsS0FBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUN2QyxJQUFJLFdBQVcsQ0FBQyxXQUFXLEVBQUU7b0JBQ3pCLE1BQU0sRUFBRSxLQUFLO29CQUNiLE9BQU8sRUFBRSxJQUFJO2lCQUNoQixDQUFDLENBQ0wsQ0FBQzthQUNMO1FBQ0wsQ0FBQyxFQUFDLENBQUM7O1lBRUcsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLE1BQU07YUFDL0IsSUFBSSxDQUNELE1BQU0sQ0FDRixJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FDWixZQUFZLENBQUMsR0FBRyxDQUFDLENBQ3BCLENBQ0osRUFDRCxHQUFHOzs7O1FBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLEVBQUosQ0FBSSxFQUFDLEVBQ25CLE1BQU07Ozs7UUFBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsQ0FBQyxNQUFNLElBQUksQ0FBQyxFQUFiLENBQWEsRUFBQyxDQUMvQjtRQUVMLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxnQkFBZ0IsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxhQUE2Qjs7Z0JBQzFFLEtBQUssR0FBaUIsYUFBYSxDQUFDLENBQUMsQ0FBQztZQUM1QyxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsS0FBSyxDQUFDLGdCQUFnQixFQUFFO2dCQUN6QixLQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQ3ZDLElBQUksV0FBVyxDQUFDLGNBQWMsRUFBRTtvQkFDNUIsTUFBTSxFQUFFLEtBQUs7b0JBQ2IsT0FBTyxFQUFFLElBQUk7aUJBQ2hCLENBQUMsQ0FDTCxDQUFDO2FBQ0w7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRU8sbURBQXNCOzs7O0lBQTlCO1FBQ0ksSUFBSSxJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDM0IsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3hDLElBQUksQ0FBQyxvQkFBb0IsR0FBRyxJQUFJLENBQUM7U0FDcEM7UUFDRCxJQUFJLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUMxQixJQUFJLENBQUMsbUJBQW1CLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDdkMsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQztTQUNuQztJQUNMLENBQUM7Ozs7O0lBRU8sc0NBQVM7Ozs7SUFBakI7UUFDSSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksc0JBQXNCLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDaEUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDbkMsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxZQUFZLEdBQUcsRUFBRSxDQUFDO0lBQzNCLENBQUM7Ozs7SUFFRCx5Q0FBWTs7O0lBQVo7UUFDSSxPQUFPLElBQUksQ0FBQyxJQUFJLEtBQUssU0FBUyxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDO0lBQ3pELENBQUM7Ozs7OztJQUVPLHlDQUFZOzs7OztJQUFwQixVQUFxQixJQUFXO1FBQzVCLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtZQUNYLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN0QixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUNuRDtJQUNMLENBQUM7Ozs7O0lBRU8sMkNBQWM7Ozs7SUFBdEI7O1lBQ1EsTUFBTSxHQUFHLEVBQUU7UUFDZixJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDNUMsTUFBTSxHQUFHLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO1NBQ3JDO2FBQU07WUFDSCxNQUFNLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUMsQ0FBQztTQUMxRDtRQUVELElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1FBRXRCLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUN0RCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7U0FDdEM7SUFDTCxDQUFDOzs7Ozs7SUFFTyw0Q0FBZTs7Ozs7SUFBdkIsVUFBd0IsT0FBTztRQUMzQixJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDWCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQztTQUM1RDtJQUNMLENBQUM7Ozs7SUFFTSw4Q0FBaUI7OztJQUF4Qjs7WUFDUSxNQUFNLEdBQUcsRUFBRTtRQUNmLElBQUksSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ2xGLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxHQUFHOzs7O1lBQUMsVUFBQyxDQUFDLFdBQUssbUJBQWEsQ0FBQyxFQUFBLEdBQUEsRUFBQyxDQUFDO1NBQy9EO1FBQ0QsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQzs7Ozs7O0lBRUQsdUNBQVU7Ozs7O0lBQVYsVUFBVyxHQUFZLEVBQUUsVUFBc0I7UUFDM0MsSUFBSSxVQUFVLEVBQUU7WUFDWixVQUFVLENBQUMsY0FBYyxFQUFFLENBQUM7U0FDL0I7UUFFRCxJQUFJLEdBQUcsRUFBRTs7Z0JBQ0MsWUFBWSxHQUFHLElBQUksWUFBWSxDQUFDLEdBQUcsRUFBRSxVQUFVLEVBQUUsSUFBSSxDQUFDO1lBQzVELElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQ3pDO0lBQ0wsQ0FBQzs7Ozs7O0lBRUQsOENBQWlCOzs7OztJQUFqQixVQUFrQixHQUFZLEVBQUUsQ0FBZ0I7UUFDNUMsSUFBSSxHQUFHLEVBQUU7WUFDTCxJQUFJLENBQUMsa0JBQWtCLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO1NBQ25DO0lBQ0wsQ0FBQzs7Ozs7OztJQUVPLCtDQUFrQjs7Ozs7O0lBQTFCLFVBQTJCLEdBQVksRUFBRSxDQUE2QjtRQUNsRSxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDWCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxFQUFFO2dCQUM5QixJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxDQUFDO2dCQUMxQixJQUFJLENBQUMscUJBQXFCLENBQUMsWUFBWSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2FBQ2pEO1lBRUQsSUFBSSxJQUFJLENBQUMsb0JBQW9CLEVBQUUsRUFBRTs7b0JBQ3ZCLFFBQVEsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxJQUFJLENBQUMsQ0FBQyxPQUFPLENBQUM7O29CQUMxQyxRQUFRLFNBQVM7Z0JBQ3JCLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO29CQUM3QixRQUFRLEdBQUcsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDO2lCQUM5QjtxQkFBTTtvQkFDSCxRQUFRLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztpQkFDaEQ7O29CQUNLLFlBQVksR0FBRyxRQUFRLENBQUMsQ0FBQyxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsY0FBYztnQkFFN0QsSUFBSSxDQUFDLFFBQVEsRUFBRTtvQkFDWCxJQUFJLENBQUMsY0FBYyxFQUFFLENBQUM7aUJBQ3pCO2dCQUNELElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLFFBQVEsQ0FBQyxDQUFDO2dCQUM5QixJQUFJLENBQUMscUJBQXFCLENBQUMsWUFBWSxFQUFFLEdBQUcsQ0FBQyxDQUFDO2FBQ2pEO1NBQ0o7SUFDTCxDQUFDOzs7O0lBRUQsMkNBQWM7OztJQUFkO1FBQ0ksSUFBSSxJQUFJLENBQUMsSUFBSSxFQUFFOztnQkFDTCxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDaEMsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3pCLElBQUksQ0FBQyxPQUFPOzs7O2dCQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLFVBQVUsR0FBRyxLQUFLLEVBQXBCLENBQW9CLEVBQUMsQ0FBQzthQUM3QztZQUNELElBQUksQ0FBQyxTQUFTLEdBQUcsRUFBRSxDQUFDO1NBQ3ZCO1FBQ0QsSUFBSSxDQUFDLGtCQUFrQixHQUFHLEtBQUssQ0FBQztJQUNwQyxDQUFDOzs7Ozs7SUFFRCwwQ0FBYTs7Ozs7SUFBYixVQUFjLEdBQVksRUFBRSxLQUFhO1FBQ3JDLElBQUksS0FBSyxFQUFFO1lBQ1AsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1NBQzFCOztZQUNLLFlBQVksR0FBRyxJQUFJLFlBQVksQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQztRQUN2RCxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUMxQyxDQUFDOzs7Ozs7SUFFRCx1Q0FBVTs7Ozs7SUFBVixVQUFXLEdBQVksRUFBRSxDQUFnQjs7WUFDL0IsS0FBSyxHQUFHLElBQUksV0FBVyxDQUFDLFdBQVcsRUFBRTtZQUN2QyxNQUFNLEVBQUU7Z0JBQ0osR0FBRyxFQUFFLEdBQUc7Z0JBQ1IsYUFBYSxFQUFFLENBQUM7Z0JBQ2hCLE1BQU0sRUFBRSxJQUFJO2FBQ2Y7WUFDRCxPQUFPLEVBQUUsSUFBSTtTQUNoQixDQUFDO1FBRUYsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRW5ELElBQUksS0FBSyxDQUFDLGdCQUFnQixFQUFFO1lBQ3hCLENBQUMsQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUN0QjthQUFNO1lBQ0gsSUFBSSxDQUFDLENBQUMsR0FBRyxLQUFLLE9BQU8sRUFBRTtnQkFDbkIsSUFBSSxDQUFDLGtCQUFrQixDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQzthQUNuQztTQUNKO0lBQ0wsQ0FBQzs7Ozs7OztJQUVPLCtDQUFrQjs7Ozs7O0lBQTFCLFVBQTJCLEdBQVksRUFBRSxhQUE0QjtRQUNqRSxJQUFJLGFBQWEsRUFBRTtZQUNmLGFBQWEsQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUNsQzs7WUFFSyxLQUFLLEdBQUcsSUFBSSxZQUFZLENBQUMsR0FBRyxFQUFFLGFBQWEsRUFBRSxJQUFJLENBQUM7UUFFeEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUN2QyxJQUFJLFdBQVcsQ0FBQyxjQUFjLEVBQUU7WUFDNUIsTUFBTSxFQUFFLEtBQUs7WUFDYixPQUFPLEVBQUUsSUFBSTtTQUNoQixDQUFDLENBQ0wsQ0FBQztJQUNOLENBQUM7Ozs7O0lBRUQsZ0RBQW1COzs7O0lBQW5CLFVBQW9CLE1BQWtCO1FBQ2xDLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUU7O2dCQUNyQixPQUFPLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7O2dCQUNsQyxZQUFZLEdBQUcsS0FBSztZQUN4QixJQUFJLE9BQU8sSUFBSSxNQUFNLENBQUMsR0FBRyxLQUFLLE9BQU8sQ0FBQyxHQUFHLEVBQUU7Z0JBQ3ZDLFlBQVksR0FBRyxPQUFPLENBQUMsU0FBUyxLQUFLLEtBQUssQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7YUFDL0Q7WUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLFdBQVcsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLFlBQVksQ0FBQyxDQUFDLENBQUM7WUFDaEUsSUFBSSxDQUFDLHVCQUF1QixDQUFDLE1BQU0sQ0FBQyxHQUFHLEVBQUUsWUFBWSxDQUFDLENBQUM7U0FDMUQ7SUFDTCxDQUFDOzs7OztJQUVELDZDQUFnQjs7OztJQUFoQixVQUFpQixpQkFBb0M7UUFDakQsSUFBSSxDQUFDLGtCQUFrQixHQUFHLGlCQUFpQixDQUFDLE9BQU8sQ0FBQztRQUVwRCxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7O2dCQUNaLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNoQyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDekIsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7b0JBQ2xDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxFQUFFLGlCQUFpQixDQUFDLE9BQU8sQ0FBQyxDQUFDO2lCQUN0RDthQUNKOztnQkFFSyxZQUFZLEdBQUcsaUJBQWlCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLGNBQWM7O2dCQUN4RSxHQUFHLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJO1lBRWhFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxZQUFZLEVBQUUsR0FBRyxDQUFDLENBQUM7U0FDakQ7SUFDTCxDQUFDOzs7Ozs7SUFFRCw2Q0FBZ0I7Ozs7O0lBQWhCLFVBQWlCLEdBQVksRUFBRSxLQUF3Qjs7WUFDN0MsUUFBUSxHQUFHLEtBQUssQ0FBQyxPQUFPO1FBRTlCLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLFFBQVEsQ0FBQyxDQUFDOztZQUV4QixZQUFZLEdBQUcsUUFBUSxDQUFDLENBQUMsQ0FBQyxZQUFZLENBQUMsQ0FBQyxDQUFDLGNBQWM7UUFDN0QsSUFBSSxDQUFDLHFCQUFxQixDQUFDLFlBQVksRUFBRSxHQUFHLENBQUMsQ0FBQztJQUNsRCxDQUFDOzs7Ozs7SUFFRCxnREFBbUI7Ozs7O0lBQW5CLFVBQW9CLEtBQVksRUFBRSxHQUFZO1FBQzFDLElBQUksS0FBSyxFQUFFOztnQkFDRCxPQUFPLEdBQUcsbUJBQU0sS0FBSyxDQUFDLE1BQU0sRUFBQTtZQUVsQyxJQUFJLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtnQkFDeEIsT0FBTyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUM7YUFDeEM7aUJBQU07Z0JBQ0gsT0FBTyxDQUFDLEdBQUcsR0FBRyxHQUFHLENBQUMsa0JBQWtCLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDL0M7U0FDSjtJQUNMLENBQUM7Ozs7OztJQUVELHdDQUFXOzs7OztJQUFYLFVBQVksR0FBWSxFQUFFLEdBQWU7UUFDckMsSUFBSSxHQUFHLElBQUksR0FBRyxFQUFFOztnQkFDTixLQUFLLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDO1lBQ25DLE9BQU8sS0FBSyxJQUFJLEtBQUssQ0FBQyxVQUFVLENBQUMsbUJBQW1CLENBQUMsQ0FBQztTQUN6RDtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7OztJQUVELHdDQUFXOzs7OztJQUFYLFVBQVksR0FBWSxFQUFFLEdBQWU7UUFDckMsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsRUFBRTs7Z0JBQ3RCLEtBQUssR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsSUFBSSxFQUFFO1lBQ3pDLE9BQU8sS0FBSyxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsRUFBRSxFQUFFLENBQUMsQ0FBQztTQUNqRDtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7O0lBRUQsMkNBQWM7Ozs7SUFBZCxVQUFlLEtBQWE7UUFDeEIsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQ3ZHLENBQUM7Ozs7OztJQUVELDJDQUFjOzs7OztJQUFkLFVBQWUsR0FBZSxFQUFFLFNBQWlCO1FBQzdDLElBQUksR0FBRyxJQUFJLFNBQVMsRUFBRTs7Z0JBQ1osT0FBTyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ3RDLE9BQU8sT0FBTyxJQUFJLE9BQU8sQ0FBQyxHQUFHLEtBQUssR0FBRyxDQUFDLEdBQUcsSUFBSSxPQUFPLENBQUMsU0FBUyxLQUFLLFNBQVMsQ0FBQztTQUNoRjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7OztJQUVELGtEQUFxQjs7Ozs7SUFBckIsVUFBc0IsR0FBWSxFQUFFLEdBQWU7O1lBQ3pDLEtBQUssR0FBRyxJQUFJLGFBQWEsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQztRQUM3QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQ3BDLE9BQU8sS0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7SUFDL0IsQ0FBQzs7Ozs7O0lBRUQsMENBQWE7Ozs7O0lBQWIsVUFBYyxHQUFZLEVBQUUsR0FBZ0I7O1lBQ2xDLEVBQUUsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztRQUU3QixJQUFJLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsRUFBRTs7Z0JBQ2xCLE9BQUssR0FBRyxJQUFJLGFBQWEsQ0FBQyxHQUFHLEVBQUUsR0FBRyxFQUFFLEVBQUUsQ0FBQztZQUM3QyxJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE9BQUssQ0FBQyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQUU7Z0JBQzNCLE9BQU8sT0FBSyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7YUFDOUI7WUFDRCxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDO1NBQy9DO1FBRUQsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLEVBQUUsQ0FBQyxDQUFDO0lBQ2pDLENBQUM7Ozs7OztJQUVELCtDQUFrQjs7Ozs7SUFBbEIsVUFBbUIsR0FBWSxFQUFFLE1BQVc7UUFDeEMsSUFBSSxNQUFNLENBQUMsUUFBUSxJQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUU7WUFDcEMsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1NBQzNCO2FBQU07WUFDSCxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLElBQUksa0JBQWtCLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQyxDQUFDLENBQUM7U0FDbkU7SUFDTCxDQUFDOzs7OztJQUVELDBDQUFhOzs7O0lBQWIsVUFBYyxHQUFZO1FBQ3RCLE9BQU8sR0FBRyxDQUFDLFlBQVksS0FBSyxJQUFJLENBQUM7SUFDckMsQ0FBQzs7OztJQUVELDZDQUFnQjs7O0lBQWhCO1FBQ0ksT0FBTyxJQUFJLENBQUMscUJBQXFCLEVBQUUsSUFBSSxJQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztJQUN2RSxDQUFDOzs7O0lBRUQsa0RBQXFCOzs7SUFBckI7UUFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsS0FBSyxRQUFRLENBQUM7SUFDL0UsQ0FBQzs7OztJQUVELGlEQUFvQjs7O0lBQXBCO1FBQ0ksT0FBTyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxFQUFFLEtBQUssVUFBVSxDQUFDO0lBQ2pGLENBQUM7Ozs7O0lBRUQsd0NBQVc7Ozs7SUFBWCxVQUFZLEdBQVk7UUFDcEIsR0FBRyxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDaEQsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7UUFDbEUsT0FBVSxHQUFHLENBQUMsUUFBUSxTQUFJLElBQUksQ0FBQyxhQUFlLENBQUM7SUFDbkQsQ0FBQzs7OztJQUVELDBDQUFhOzs7SUFBYjtRQUNJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRTtZQUN4QixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUMsR0FBRyxDQUFDO1NBQ3JDO0lBQ0wsQ0FBQzs7Ozs7O0lBRUQsc0NBQVM7Ozs7O0lBQVQsVUFBVSxHQUFZLEVBQUUsS0FBYztRQUNsQyxJQUFJLEdBQUcsRUFBRTtZQUNMLEdBQUcsQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDOztnQkFDakIsR0FBRyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQztZQUN2QyxJQUFJLEtBQUssRUFBRTtnQkFDUCxJQUFJLEdBQUcsR0FBRyxDQUFDLEVBQUU7b0JBQ1QsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQzVCO2FBQ0o7aUJBQU07Z0JBQ0gsSUFBSSxHQUFHLEdBQUcsQ0FBQyxDQUFDLEVBQUU7b0JBQ1YsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQyxDQUFDO2lCQUNqQzthQUNKO1NBQ0o7SUFDTCxDQUFDOzs7Ozs7SUFFRCwyQ0FBYzs7Ozs7SUFBZCxVQUFlLEdBQVksRUFBRSxHQUFlO1FBQ3hDLElBQUksR0FBRyxJQUFJLEdBQUcsSUFBSSxHQUFHLENBQUMsYUFBYSxFQUFFOztnQkFDM0IsTUFBTSxHQUFXLEdBQUcsQ0FBQyxhQUFhLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQztZQUNsRCxJQUFJLE1BQU0sRUFBRTtnQkFDUixPQUFPLE1BQU0sQ0FBQzthQUNqQjtTQUNKO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQzs7OztJQUVELCtDQUFrQjs7O0lBQWxCO1FBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDLE1BQU07Ozs7UUFBQyxVQUFDLE1BQU07WUFDeEMsT0FBTyxNQUFNLENBQUMsUUFBUSxLQUFLLElBQUksQ0FBQztRQUNwQyxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7SUFFRCxvQ0FBTzs7O0lBQVA7UUFDSSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQztJQUM1QyxDQUFDOzs7O0lBRUQsNENBQWU7OztJQUFmO1FBQ0ksT0FBTyxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDO0lBQ2xFLENBQUM7Ozs7SUFFRCxrREFBcUI7OztJQUFyQjtRQUNJLE9BQU8sSUFBSSxDQUFDLFlBQVksSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7SUFDdkQsQ0FBQzs7Ozs7OztJQUVPLGtEQUFxQjs7Ozs7O0lBQTdCLFVBQThCLElBQVksRUFBRSxHQUFZOztZQUM5QyxRQUFRLEdBQUcsSUFBSSxXQUFXLENBQUMsSUFBSSxFQUFFO1lBQ25DLE1BQU0sRUFBRTtnQkFDSixHQUFHLEVBQUUsR0FBRztnQkFDUixTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVM7YUFDNUI7WUFDRCxPQUFPLEVBQUUsSUFBSTtTQUNoQixDQUFDO1FBQ0YsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzFELENBQUM7Ozs7Ozs7SUFFTyxvREFBdUI7Ozs7OztJQUEvQixVQUFnQyxHQUFXLEVBQUUsU0FBaUI7O1lBQ3BELFFBQVEsR0FBRyxJQUFJLFdBQVcsQ0FBQyxpQkFBaUIsRUFBRTtZQUNoRCxNQUFNLEVBQUU7Z0JBQ0osR0FBRyxLQUFBO2dCQUNILFNBQVMsV0FBQTthQUNaO1lBQ0QsT0FBTyxFQUFFLElBQUk7U0FDaEIsQ0FBQztRQUNGLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUMxRCxDQUFDOzs7O0lBRUQsd0NBQVc7OztJQUFYO1FBQ0ksSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7UUFFOUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLENBQUMsV0FBVyxFQUFFLEVBQWYsQ0FBZSxFQUFDLENBQUM7UUFDbkQsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7UUFFeEIsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxlQUFlLENBQUMsV0FBVyxFQUFFLENBQUM7WUFDbkMsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUM7U0FDL0I7SUFDTCxDQUFDOzs7O0lBRUQsK0NBQWtCOzs7SUFBbEI7O1lBQ1UsY0FBYyxHQUFHLEVBQUU7UUFFekIsSUFBSSxJQUFJLENBQUMsT0FBTyxLQUFLLFNBQVMsRUFBRTtZQUM1QixLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsY0FBYyxFQUFFLENBQUMsRUFBRSxFQUFFO2dCQUN0QyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUN6QjtTQUNKO2FBQU07WUFDSCxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQztTQUN0QjtJQUNMLENBQUM7Ozs7SUFFRCwrQ0FBa0I7OztJQUFsQjtRQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQyxJQUFJOzs7O1FBQUUsVUFBQyxFQUFPO1lBQ3hDLE9BQU8sRUFBRSxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDbkMsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFFRCwrQ0FBa0I7Ozs7O0lBQWxCLFVBQW1CLEdBQVksRUFBRSxHQUFlOztZQUN0QyxJQUFJLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixFQUFFO1FBQ3RDLE9BQU8sSUFBSSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQzlDLENBQUM7O2dCQXpwQkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxlQUFlO29CQUV6Qix3aWVBQXlDO29CQUN6QyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtvQkFDckMsSUFBSSxFQUFFLEVBQUUsS0FBSyxFQUFFLGVBQWUsRUFBRTs7aUJBQ25DOzs7O2dCQTdCdUQsVUFBVTtnQkFDOUQsZUFBZTs7OzZCQStCZCxZQUFZLFNBQUMsdUJBQXVCLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDO3VCQUlwRCxLQUFLOzBCQUlMLEtBQUs7dUJBSUwsS0FBSzswQkFNTCxLQUFLOzBCQUlMLEtBQUs7Z0NBTUwsS0FBSzs4QkFJTCxLQUFLOzBCQUlMLEtBQUs7a0NBSUwsS0FBSztvQ0FJTCxLQUFLOzhCQUlMLEtBQUs7aUNBTUwsS0FBSzsyQkFPTCxLQUFLO2dDQUlMLEtBQUs7NkJBSUwsS0FBSzsrQkFJTCxLQUFLOzJCQUlMLE1BQU07OEJBSU4sTUFBTTtxQ0FJTixNQUFNO3FDQUlOLE1BQU07bUNBSU4sTUFBTTswQkFNTixLQUFLOytCQUlMLEtBQUs7c0NBT0wsS0FBSzs7SUFtaUJWLHlCQUFDO0NBQUEsQUExcEJELElBMHBCQztTQW5wQlksa0JBQWtCOzs7SUFFM0Isd0NBQ29DOzs7OztJQUdwQyxrQ0FDdUI7Ozs7O0lBR3ZCLHFDQUNtQzs7Ozs7SUFHbkMsa0NBQ2lCOzs7Ozs7SUFLakIscUNBQ29COzs7OztJQUdwQixxQ0FDb0I7Ozs7OztJQUtwQiwyQ0FDaUM7Ozs7O0lBR2pDLHlDQUM2Qjs7Ozs7SUFHN0IscUNBQ3lCOzs7OztJQUd6Qiw2Q0FDa0M7Ozs7O0lBR2xDLCtDQUMwQjs7Ozs7SUFHMUIseUNBQzZCOzs7Ozs7SUFLN0IsNENBQ2dDOzs7Ozs7O0lBTWhDLHNDQUNpQjs7Ozs7SUFHakIsMkNBQzJCOzs7OztJQUczQix3Q0FDMkI7Ozs7O0lBRzNCLDBDQUM4Qjs7Ozs7SUFHOUIsc0NBQzRDOzs7OztJQUc1Qyx5Q0FDK0M7Ozs7O0lBRy9DLGdEQUN1RDs7Ozs7SUFHdkQsZ0RBQ3VEOzs7OztJQUd2RCw4Q0FDMEQ7Ozs7OztJQUsxRCxxQ0FDeUI7Ozs7O0lBR3pCLDBDQUM4Qjs7Ozs7O0lBTTlCLGlEQUMyQjs7SUFFM0IsK0NBQW9DOztJQUNwQyxrREFBdUM7O0lBQ3ZDLDZDQUFrQzs7SUFFbEMsZ0RBQW9DOztJQUNwQyx1Q0FBaUM7Ozs7O0lBR2pDLHNDQUFjOzs7OztJQUVkLDJDQUE4Qzs7Ozs7SUFDOUMsb0NBQXlDOzs7OztJQUV6QyxvQ0FBb0I7Ozs7O0lBQ3BCLDBDQUFrQzs7Ozs7SUFFbEMsMkNBQTJDOzs7OztJQUMzQyxrREFBMkM7Ozs7O0lBQzNDLGlEQUEwQzs7Ozs7SUFDMUMsNkNBQXNDOzs7OztJQUUxQix3Q0FBOEI7Ozs7O0FBNmdCOUMsd0NBU0M7OztJQVJHLG9DQUtFOzs7O0lBRUYsOERBQXVCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXG5cbi8qIVxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXG4gKlxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuICpcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbiAqXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxuICovXG5cbmltcG9ydCB7XG4gICAgQWZ0ZXJDb250ZW50SW5pdCwgQ29tcG9uZW50LCBDb250ZW50Q2hpbGQsIERvQ2hlY2ssIEVsZW1lbnRSZWYsIEV2ZW50RW1pdHRlciwgSW5wdXQsXG4gICAgSXRlcmFibGVEaWZmZXJzLCBPbkNoYW5nZXMsIE91dHB1dCwgU2ltcGxlQ2hhbmdlLCBTaW1wbGVDaGFuZ2VzLCBUZW1wbGF0ZVJlZiwgVmlld0VuY2Fwc3VsYXRpb24sIE9uRGVzdHJveVxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1hdENoZWNrYm94Q2hhbmdlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xuaW1wb3J0IHsgU3Vic2NyaXB0aW9uLCBPYnNlcnZhYmxlLCBPYnNlcnZlciB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgRGF0YUNvbHVtbkxpc3RDb21wb25lbnQgfSBmcm9tICcuLi8uLi8uLi9kYXRhLWNvbHVtbi9kYXRhLWNvbHVtbi1saXN0LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEYXRhQ29sdW1uIH0gZnJvbSAnLi4vLi4vZGF0YS9kYXRhLWNvbHVtbi5tb2RlbCc7XG5pbXBvcnQgeyBEYXRhUm93RXZlbnQgfSBmcm9tICcuLi8uLi9kYXRhL2RhdGEtcm93LWV2ZW50Lm1vZGVsJztcbmltcG9ydCB7IERhdGFSb3cgfSBmcm9tICcuLi8uLi9kYXRhL2RhdGEtcm93Lm1vZGVsJztcbmltcG9ydCB7IERhdGFTb3J0aW5nIH0gZnJvbSAnLi4vLi4vZGF0YS9kYXRhLXNvcnRpbmcubW9kZWwnO1xuaW1wb3J0IHsgRGF0YVRhYmxlQWRhcHRlciB9IGZyb20gJy4uLy4uL2RhdGEvZGF0YXRhYmxlLWFkYXB0ZXInO1xuXG5pbXBvcnQgeyBPYmplY3REYXRhUm93IH0gZnJvbSAnLi4vLi4vZGF0YS9vYmplY3QtZGF0YXJvdy5tb2RlbCc7XG5pbXBvcnQgeyBPYmplY3REYXRhVGFibGVBZGFwdGVyIH0gZnJvbSAnLi4vLi4vZGF0YS9vYmplY3QtZGF0YXRhYmxlLWFkYXB0ZXInO1xuaW1wb3J0IHsgRGF0YUNlbGxFdmVudCB9IGZyb20gJy4vZGF0YS1jZWxsLmV2ZW50JztcbmltcG9ydCB7IERhdGFSb3dBY3Rpb25FdmVudCB9IGZyb20gJy4vZGF0YS1yb3ctYWN0aW9uLmV2ZW50JztcbmltcG9ydCB7IHNoYXJlLCBidWZmZXIsIG1hcCwgZmlsdGVyLCBkZWJvdW5jZVRpbWUgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbmV4cG9ydCBlbnVtIERpc3BsYXlNb2RlIHtcbiAgICBMaXN0ID0gJ2xpc3QnLFxuICAgIEdhbGxlcnkgPSAnZ2FsbGVyeSdcbn1cblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdhZGYtZGF0YXRhYmxlJyxcbiAgICBzdHlsZVVybHM6IFsnLi9kYXRhdGFibGUuY29tcG9uZW50LnNjc3MnXSxcbiAgICB0ZW1wbGF0ZVVybDogJy4vZGF0YXRhYmxlLmNvbXBvbmVudC5odG1sJyxcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxuICAgIGhvc3Q6IHsgY2xhc3M6ICdhZGYtZGF0YXRhYmxlJyB9XG59KVxuZXhwb3J0IGNsYXNzIERhdGFUYWJsZUNvbXBvbmVudCBpbXBsZW1lbnRzIEFmdGVyQ29udGVudEluaXQsIE9uQ2hhbmdlcywgRG9DaGVjaywgT25EZXN0cm95IHtcblxuICAgIEBDb250ZW50Q2hpbGQoRGF0YUNvbHVtbkxpc3RDb21wb25lbnQsIHtzdGF0aWM6IHRydWV9KVxuICAgIGNvbHVtbkxpc3Q6IERhdGFDb2x1bW5MaXN0Q29tcG9uZW50O1xuXG4gICAgLyoqIERhdGEgc291cmNlIGZvciB0aGUgdGFibGUgKi9cbiAgICBASW5wdXQoKVxuICAgIGRhdGE6IERhdGFUYWJsZUFkYXB0ZXI7XG5cbiAgICAvKiogU2VsZWN0cyB0aGUgZGlzcGxheSBtb2RlIG9mIHRoZSB0YWJsZS4gQ2FuIGJlIFwibGlzdFwiIG9yIFwiZ2FsbGVyeVwiLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgZGlzcGxheTogc3RyaW5nID0gRGlzcGxheU1vZGUuTGlzdDtcblxuICAgIC8qKiBUaGUgcm93cyB0aGF0IHRoZSBkYXRhdGFibGUgd2lsbCBzaG93LiAqL1xuICAgIEBJbnB1dCgpXG4gICAgcm93czogYW55W10gPSBbXTtcblxuICAgIC8qKiBEZWZpbmUgdGhlIHNvcnQgb3JkZXIgb2YgdGhlIGRhdGF0YWJsZS4gUG9zc2libGUgdmFsdWVzIGFyZSA6XG4gICAgICogW2BjcmVhdGVkYCwgYGRlc2NgXSwgW2BjcmVhdGVkYCwgYGFzY2BdLCBbYGR1ZWAsIGBkZXNjYF0sIFtgZHVlYCwgYGFzY2BdXG4gICAgICovXG4gICAgQElucHV0KClcbiAgICBzb3J0aW5nOiBhbnlbXSA9IFtdO1xuXG4gICAgLyoqIFRoZSBjb2x1bW5zIHRoYXQgdGhlIGRhdGF0YWJsZSB3aWxsIHNob3cuICovXG4gICAgQElucHV0KClcbiAgICBjb2x1bW5zOiBhbnlbXSA9IFtdO1xuXG4gICAgLyoqIFJvdyBzZWxlY3Rpb24gbW9kZS4gQ2FuIGJlIG5vbmUsIGBzaW5nbGVgIG9yIGBtdWx0aXBsZWAuIEZvciBgbXVsdGlwbGVgIG1vZGUsXG4gICAgICogeW91IGNhbiB1c2UgQ21kIChtYWNPUykgb3IgQ3RybCAoV2luKSBtb2RpZmllciBrZXkgdG8gdG9nZ2xlIHNlbGVjdGlvbiBmb3IgbXVsdGlwbGUgcm93cy5cbiAgICAgKi9cbiAgICBASW5wdXQoKVxuICAgIHNlbGVjdGlvbk1vZGU6IHN0cmluZyA9ICdzaW5nbGUnOyAvLyBub25lfHNpbmdsZXxtdWx0aXBsZVxuXG4gICAgLyoqIFRvZ2dsZXMgbXVsdGlwbGUgcm93IHNlbGVjdGlvbiwgd2hpY2ggcmVuZGVycyBjaGVja2JveGVzIGF0IHRoZSBiZWdpbm5pbmcgb2YgZWFjaCByb3cuICovXG4gICAgQElucHV0KClcbiAgICBtdWx0aXNlbGVjdDogYm9vbGVhbiA9IGZhbHNlO1xuXG4gICAgLyoqIFRvZ2dsZXMgdGhlIGRhdGEgYWN0aW9ucyBjb2x1bW4uICovXG4gICAgQElucHV0KClcbiAgICBhY3Rpb25zOiBib29sZWFuID0gZmFsc2U7XG5cbiAgICAvKiogUG9zaXRpb24gb2YgdGhlIGFjdGlvbnMgZHJvcGRvd24gbWVudS4gQ2FuIGJlIFwibGVmdFwiIG9yIFwicmlnaHRcIi4gKi9cbiAgICBASW5wdXQoKVxuICAgIGFjdGlvbnNQb3NpdGlvbjogc3RyaW5nID0gJ3JpZ2h0JzsgLy8gbGVmdHxyaWdodFxuXG4gICAgLyoqIEZhbGxiYWNrIGltYWdlIGZvciByb3dzIHdoZXJlIHRoZSB0aHVtYm5haWwgaXMgbWlzc2luZy4gKi9cbiAgICBASW5wdXQoKVxuICAgIGZhbGxiYWNrVGh1bWJuYWlsOiBzdHJpbmc7XG5cbiAgICAvKiogVG9nZ2xlcyBjdXN0b20gY29udGV4dCBtZW51IGZvciB0aGUgY29tcG9uZW50LiAqL1xuICAgIEBJbnB1dCgpXG4gICAgY29udGV4dE1lbnU6IGJvb2xlYW4gPSBmYWxzZTtcblxuICAgIC8qKiBUb2dnbGVzIGZpbGUgZHJvcCBzdXBwb3J0IGZvciByb3dzIChzZWVcbiAgICAgKiBbVXBsb2FkIGRpcmVjdGl2ZV0odXBsb2FkLmRpcmVjdGl2ZS5tZCkgZm9yIGZ1cnRoZXIgZGV0YWlscykuXG4gICAgICovXG4gICAgQElucHV0KClcbiAgICBhbGxvd0Ryb3BGaWxlczogYm9vbGVhbiA9IGZhbHNlO1xuXG4gICAgLyoqIFRoZSBpbmxpbmUgc3R5bGUgdG8gYXBwbHkgdG8gZXZlcnkgcm93LiBTZWVcbiAgICAgKiBbTmdTdHlsZV0oaHR0cHM6Ly9hbmd1bGFyLmlvL2RvY3MvdHMvbGF0ZXN0L2FwaS9jb21tb24vaW5kZXgvTmdTdHlsZS1kaXJlY3RpdmUuaHRtbClcbiAgICAgKiBkb2NzIGZvciBtb3JlIGRldGFpbHMgYW5kIHVzYWdlIGV4YW1wbGVzLlxuICAgICAqL1xuICAgIEBJbnB1dCgpXG4gICAgcm93U3R5bGU6IHN0cmluZztcblxuICAgIC8qKiBUaGUgQ1NTIGNsYXNzIHRvIGFwcGx5IHRvIGV2ZXJ5IHJvdy4gKi9cbiAgICBASW5wdXQoKVxuICAgIHJvd1N0eWxlQ2xhc3M6IHN0cmluZyA9ICcnO1xuXG4gICAgLyoqIFRvZ2dsZXMgdGhlIGhlYWRlci4gKi9cbiAgICBASW5wdXQoKVxuICAgIHNob3dIZWFkZXI6IGJvb2xlYW4gPSB0cnVlO1xuXG4gICAgLyoqIFRvZ2dsZXMgdGhlIHN0aWNreSBoZWFkZXIgbW9kZS4gKi9cbiAgICBASW5wdXQoKVxuICAgIHN0aWNreUhlYWRlcjogYm9vbGVhbiA9IGZhbHNlO1xuXG4gICAgLyoqIEVtaXR0ZWQgd2hlbiB0aGUgdXNlciBjbGlja3MgYSByb3cuICovXG4gICAgQE91dHB1dCgpXG4gICAgcm93Q2xpY2sgPSBuZXcgRXZlbnRFbWl0dGVyPERhdGFSb3dFdmVudD4oKTtcblxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIHVzZXIgZG91YmxlLWNsaWNrcyBhIHJvdy4gKi9cbiAgICBAT3V0cHV0KClcbiAgICByb3dEYmxDbGljayA9IG5ldyBFdmVudEVtaXR0ZXI8RGF0YVJvd0V2ZW50PigpO1xuXG4gICAgLyoqIEVtaXR0ZWQgYmVmb3JlIHRoZSBjb250ZXh0IG1lbnUgaXMgZGlzcGxheWVkIGZvciBhIHJvdy4gKi9cbiAgICBAT3V0cHV0KClcbiAgICBzaG93Um93Q29udGV4dE1lbnUgPSBuZXcgRXZlbnRFbWl0dGVyPERhdGFDZWxsRXZlbnQ+KCk7XG5cbiAgICAvKiogRW1pdHRlZCBiZWZvcmUgdGhlIGFjdGlvbnMgbWVudSBpcyBkaXNwbGF5ZWQgZm9yIGEgcm93LiAqL1xuICAgIEBPdXRwdXQoKVxuICAgIHNob3dSb3dBY3Rpb25zTWVudSA9IG5ldyBFdmVudEVtaXR0ZXI8RGF0YUNlbGxFdmVudD4oKTtcblxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIHVzZXIgZXhlY3V0ZXMgYSByb3cgYWN0aW9uLiAqL1xuICAgIEBPdXRwdXQoKVxuICAgIGV4ZWN1dGVSb3dBY3Rpb24gPSBuZXcgRXZlbnRFbWl0dGVyPERhdGFSb3dBY3Rpb25FdmVudD4oKTtcblxuICAgIC8qKiBGbGFnIHRoYXQgaW5kaWNhdGVzIGlmIHRoZSBkYXRhdGFibGUgaXMgaW4gbG9hZGluZyBzdGF0ZSBhbmQgbmVlZHMgdG8gc2hvdyB0aGVcbiAgICAgKiBsb2FkaW5nIHRlbXBsYXRlIChzZWUgdGhlIGRvY3MgdG8gbGVhcm4gaG93IHRvIGNvbmZpZ3VyZSBhIGxvYWRpbmcgdGVtcGxhdGUpLlxuICAgICAqL1xuICAgIEBJbnB1dCgpXG4gICAgbG9hZGluZzogYm9vbGVhbiA9IGZhbHNlO1xuXG4gICAgLyoqIEZsYWcgdGhhdCBpbmRpY2F0ZXMgaWYgdGhlIGRhdGF0YWJsZSBzaG91bGQgc2hvdyB0aGUgXCJubyBwZXJtaXNzaW9uXCIgdGVtcGxhdGUuICovXG4gICAgQElucHV0KClcbiAgICBub1Blcm1pc3Npb246IGJvb2xlYW4gPSBmYWxzZTtcblxuICAgIC8qKlxuICAgICAqIFNob3VsZCB0aGUgaXRlbXMgZm9yIHRoZSByb3cgYWN0aW9ucyBtZW51IGJlIGNhY2hlZCBmb3IgcmV1c2UgYWZ0ZXIgdGhleSBhcmUgbG9hZGVkXG4gICAgICogdGhlIGZpcnN0IHRpbWU/XG4gICAgICovXG4gICAgQElucHV0KClcbiAgICByb3dNZW51Q2FjaGVFbmFibGVkID0gdHJ1ZTtcblxuICAgIG5vQ29udGVudFRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xuICAgIG5vUGVybWlzc2lvblRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+O1xuICAgIGxvYWRpbmdUZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcblxuICAgIGlzU2VsZWN0QWxsQ2hlY2tlZDogYm9vbGVhbiA9IGZhbHNlO1xuICAgIHNlbGVjdGlvbiA9IG5ldyBBcnJheTxEYXRhUm93PigpO1xuXG4gICAgLyoqIFRoaXMgYXJyYXkgb2YgZmFrZSByb3dzIGZpeCB0aGUgZmxleCBsYXlvdXQgZm9yIHRoZSBnYWxsZXJ5IHZpZXcgKi9cbiAgICBmYWtlUm93cyA9IFtdO1xuXG4gICAgcHJpdmF0ZSBjbGlja09ic2VydmVyOiBPYnNlcnZlcjxEYXRhUm93RXZlbnQ+O1xuICAgIHByaXZhdGUgY2xpY2skOiBPYnNlcnZhYmxlPERhdGFSb3dFdmVudD47XG5cbiAgICBwcml2YXRlIGRpZmZlcjogYW55O1xuICAgIHByaXZhdGUgcm93TWVudUNhY2hlOiBvYmplY3QgPSB7fTtcblxuICAgIHByaXZhdGUgc3Vic2NyaXB0aW9uczogU3Vic2NyaXB0aW9uW10gPSBbXTtcbiAgICBwcml2YXRlIHNpbmdsZUNsaWNrU3RyZWFtU3ViOiBTdWJzY3JpcHRpb247XG4gICAgcHJpdmF0ZSBtdWx0aUNsaWNrU3RyZWFtU3ViOiBTdWJzY3JpcHRpb247XG4gICAgcHJpdmF0ZSBkYXRhUm93c0NoYW5nZWQ6IFN1YnNjcmlwdGlvbjtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgZWxlbWVudFJlZjogRWxlbWVudFJlZixcbiAgICAgICAgICAgICAgICBkaWZmZXJzOiBJdGVyYWJsZURpZmZlcnMpIHtcbiAgICAgICAgaWYgKGRpZmZlcnMpIHtcbiAgICAgICAgICAgIHRoaXMuZGlmZmVyID0gZGlmZmVycy5maW5kKFtdKS5jcmVhdGUobnVsbCk7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5jbGljayQgPSBuZXcgT2JzZXJ2YWJsZTxEYXRhUm93RXZlbnQ+KChvYnNlcnZlcikgPT4gdGhpcy5jbGlja09ic2VydmVyID0gb2JzZXJ2ZXIpXG4gICAgICAgICAgICAucGlwZShzaGFyZSgpKTtcbiAgICB9XG5cbiAgICBuZ0FmdGVyQ29udGVudEluaXQoKSB7XG4gICAgICAgIGlmICh0aGlzLmNvbHVtbkxpc3QpIHtcbiAgICAgICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5wdXNoKFxuICAgICAgICAgICAgICAgIHRoaXMuY29sdW1uTGlzdC5jb2x1bW5zLmNoYW5nZXMuc3Vic2NyaWJlKCgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRUYWJsZVNjaGVtYSgpO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgICAgIHRoaXMuZGF0YXRhYmxlTGF5b3V0Rml4KCk7XG4gICAgICAgIHRoaXMuc2V0VGFibGVTY2hlbWEoKTtcbiAgICB9XG5cbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XG4gICAgICAgIHRoaXMuaW5pdEFuZFN1YnNjcmliZUNsaWNrU3RyZWFtKCk7XG4gICAgICAgIGlmICh0aGlzLmlzUHJvcGVydHlDaGFuZ2VkKGNoYW5nZXNbJ2RhdGEnXSkpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmlzVGFibGVFbXB0eSgpKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5pbml0VGFibGUoKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgdGhpcy5kYXRhID0gY2hhbmdlc1snZGF0YSddLmN1cnJlbnRWYWx1ZTtcbiAgICAgICAgICAgICAgICB0aGlzLnJlc2V0U2VsZWN0aW9uKCk7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICByZXR1cm47XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5pc1Byb3BlcnR5Q2hhbmdlZChjaGFuZ2VzWydyb3dzJ10pKSB7XG4gICAgICAgICAgICBpZiAodGhpcy5pc1RhYmxlRW1wdHkoKSkge1xuICAgICAgICAgICAgICAgIHRoaXMuaW5pdFRhYmxlKCk7XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0VGFibGVSb3dzKGNoYW5nZXNbJ3Jvd3MnXS5jdXJyZW50VmFsdWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgcmV0dXJuO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGNoYW5nZXMuc2VsZWN0aW9uTW9kZSAmJiAhY2hhbmdlcy5zZWxlY3Rpb25Nb2RlLmlzRmlyc3RDaGFuZ2UoKSkge1xuICAgICAgICAgICAgdGhpcy5yZXNldFNlbGVjdGlvbigpO1xuICAgICAgICAgICAgdGhpcy5lbWl0Um93U2VsZWN0aW9uRXZlbnQoJ3Jvdy11bnNlbGVjdCcsIG51bGwpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuaXNQcm9wZXJ0eUNoYW5nZWQoY2hhbmdlc1snc29ydGluZyddKSkge1xuICAgICAgICAgICAgdGhpcy5zZXRUYWJsZVNvcnRpbmcoY2hhbmdlc1snc29ydGluZyddLmN1cnJlbnRWYWx1ZSk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5pc1Byb3BlcnR5Q2hhbmdlZChjaGFuZ2VzWydkaXNwbGF5J10pKSB7XG4gICAgICAgICAgICB0aGlzLmRhdGF0YWJsZUxheW91dEZpeCgpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgbmdEb0NoZWNrKCkge1xuICAgICAgICBjb25zdCBjaGFuZ2VzID0gdGhpcy5kaWZmZXIuZGlmZih0aGlzLnJvd3MpO1xuICAgICAgICBpZiAoY2hhbmdlcykge1xuICAgICAgICAgICAgdGhpcy5zZXRUYWJsZVJvd3ModGhpcy5yb3dzKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGlzUHJvcGVydHlDaGFuZ2VkKHByb3BlcnR5OiBTaW1wbGVDaGFuZ2UpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHByb3BlcnR5ICYmIHByb3BlcnR5LmN1cnJlbnRWYWx1ZSA/IHRydWUgOiBmYWxzZTtcbiAgICB9XG5cbiAgICBjb252ZXJ0VG9Sb3dzRGF0YShyb3dzOiBhbnkgW10pOiBPYmplY3REYXRhUm93W10ge1xuICAgICAgICByZXR1cm4gcm93cy5tYXAoKHJvdykgPT4gbmV3IE9iamVjdERhdGFSb3cocm93LCByb3cuaXNTZWxlY3RlZCkpO1xuICAgIH1cblxuICAgIGNvbnZlcnRUb0RhdGFTb3J0aW5nKHNvcnRpbmc6IGFueVtdKTogRGF0YVNvcnRpbmcge1xuICAgICAgICBpZiAoc29ydGluZyAmJiBzb3J0aW5nLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIHJldHVybiBuZXcgRGF0YVNvcnRpbmcoc29ydGluZ1swXSwgc29ydGluZ1sxXSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIGluaXRBbmRTdWJzY3JpYmVDbGlja1N0cmVhbSgpIHtcbiAgICAgICAgdGhpcy51bnN1YnNjcmliZUNsaWNrU3RyZWFtKCk7XG4gICAgICAgIGNvbnN0IHNpbmdsZUNsaWNrU3RyZWFtID0gdGhpcy5jbGljayRcbiAgICAgICAgICAgIC5waXBlKFxuICAgICAgICAgICAgICAgIGJ1ZmZlcihcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jbGljayQucGlwZShcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlYm91bmNlVGltZSgyNTApXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgIG1hcCgobGlzdCkgPT4gbGlzdCksXG4gICAgICAgICAgICAgICAgZmlsdGVyKCh4KSA9PiB4Lmxlbmd0aCA9PT0gMSlcbiAgICAgICAgICAgICk7XG5cbiAgICAgICAgdGhpcy5zaW5nbGVDbGlja1N0cmVhbVN1YiA9IHNpbmdsZUNsaWNrU3RyZWFtLnN1YnNjcmliZSgoZGF0YVJvd0V2ZW50czogRGF0YVJvd0V2ZW50W10pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50OiBEYXRhUm93RXZlbnQgPSBkYXRhUm93RXZlbnRzWzBdO1xuICAgICAgICAgICAgdGhpcy5oYW5kbGVSb3dTZWxlY3Rpb24oZXZlbnQudmFsdWUsIDxNb3VzZUV2ZW50IHwgS2V5Ym9hcmRFdmVudD4gZXZlbnQuZXZlbnQpO1xuICAgICAgICAgICAgdGhpcy5yb3dDbGljay5lbWl0KGV2ZW50KTtcbiAgICAgICAgICAgIGlmICghZXZlbnQuZGVmYXVsdFByZXZlbnRlZCkge1xuICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LmRpc3BhdGNoRXZlbnQoXG4gICAgICAgICAgICAgICAgICAgIG5ldyBDdXN0b21FdmVudCgncm93LWNsaWNrJywge1xuICAgICAgICAgICAgICAgICAgICAgICAgZGV0YWlsOiBldmVudCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGJ1YmJsZXM6IHRydWVcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICBjb25zdCBtdWx0aUNsaWNrU3RyZWFtID0gdGhpcy5jbGljayRcbiAgICAgICAgICAgIC5waXBlKFxuICAgICAgICAgICAgICAgIGJ1ZmZlcihcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jbGljayQucGlwZShcbiAgICAgICAgICAgICAgICAgICAgICAgIGRlYm91bmNlVGltZSgyNTApXG4gICAgICAgICAgICAgICAgICAgIClcbiAgICAgICAgICAgICAgICApLFxuICAgICAgICAgICAgICAgIG1hcCgobGlzdCkgPT4gbGlzdCksXG4gICAgICAgICAgICAgICAgZmlsdGVyKCh4KSA9PiB4Lmxlbmd0aCA+PSAyKVxuICAgICAgICAgICAgKTtcblxuICAgICAgICB0aGlzLm11bHRpQ2xpY2tTdHJlYW1TdWIgPSBtdWx0aUNsaWNrU3RyZWFtLnN1YnNjcmliZSgoZGF0YVJvd0V2ZW50czogRGF0YVJvd0V2ZW50W10pID0+IHtcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50OiBEYXRhUm93RXZlbnQgPSBkYXRhUm93RXZlbnRzWzBdO1xuICAgICAgICAgICAgdGhpcy5yb3dEYmxDbGljay5lbWl0KGV2ZW50KTtcbiAgICAgICAgICAgIGlmICghZXZlbnQuZGVmYXVsdFByZXZlbnRlZCkge1xuICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LmRpc3BhdGNoRXZlbnQoXG4gICAgICAgICAgICAgICAgICAgIG5ldyBDdXN0b21FdmVudCgncm93LWRibGNsaWNrJywge1xuICAgICAgICAgICAgICAgICAgICAgICAgZGV0YWlsOiBldmVudCxcbiAgICAgICAgICAgICAgICAgICAgICAgIGJ1YmJsZXM6IHRydWVcbiAgICAgICAgICAgICAgICAgICAgfSlcbiAgICAgICAgICAgICAgICApO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHVuc3Vic2NyaWJlQ2xpY2tTdHJlYW0oKSB7XG4gICAgICAgIGlmICh0aGlzLnNpbmdsZUNsaWNrU3RyZWFtU3ViKSB7XG4gICAgICAgICAgICB0aGlzLnNpbmdsZUNsaWNrU3RyZWFtU3ViLnVuc3Vic2NyaWJlKCk7XG4gICAgICAgICAgICB0aGlzLnNpbmdsZUNsaWNrU3RyZWFtU3ViID0gbnVsbDtcbiAgICAgICAgfVxuICAgICAgICBpZiAodGhpcy5tdWx0aUNsaWNrU3RyZWFtU3ViKSB7XG4gICAgICAgICAgICB0aGlzLm11bHRpQ2xpY2tTdHJlYW1TdWIudW5zdWJzY3JpYmUoKTtcbiAgICAgICAgICAgIHRoaXMubXVsdGlDbGlja1N0cmVhbVN1YiA9IG51bGw7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIGluaXRUYWJsZSgpIHtcbiAgICAgICAgdGhpcy5kYXRhID0gbmV3IE9iamVjdERhdGFUYWJsZUFkYXB0ZXIodGhpcy5yb3dzLCB0aGlzLmNvbHVtbnMpO1xuICAgICAgICB0aGlzLnNldFRhYmxlU29ydGluZyh0aGlzLnNvcnRpbmcpO1xuICAgICAgICB0aGlzLnJlc2V0U2VsZWN0aW9uKCk7XG4gICAgICAgIHRoaXMucm93TWVudUNhY2hlID0ge307XG4gICAgfVxuXG4gICAgaXNUYWJsZUVtcHR5KCkge1xuICAgICAgICByZXR1cm4gdGhpcy5kYXRhID09PSB1bmRlZmluZWQgfHwgdGhpcy5kYXRhID09PSBudWxsO1xuICAgIH1cblxuICAgIHByaXZhdGUgc2V0VGFibGVSb3dzKHJvd3M6IGFueVtdKSB7XG4gICAgICAgIGlmICh0aGlzLmRhdGEpIHtcbiAgICAgICAgICAgIHRoaXMucmVzZXRTZWxlY3Rpb24oKTtcbiAgICAgICAgICAgIHRoaXMuZGF0YS5zZXRSb3dzKHRoaXMuY29udmVydFRvUm93c0RhdGEocm93cykpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBzZXRUYWJsZVNjaGVtYSgpIHtcbiAgICAgICAgbGV0IHNjaGVtYSA9IFtdO1xuICAgICAgICBpZiAoIXRoaXMuY29sdW1ucyB8fCB0aGlzLmNvbHVtbnMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICBzY2hlbWEgPSB0aGlzLmdldFNjaGVtYUZyb21IdG1sKCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBzY2hlbWEgPSB0aGlzLmNvbHVtbnMuY29uY2F0KHRoaXMuZ2V0U2NoZW1hRnJvbUh0bWwoKSk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmNvbHVtbnMgPSBzY2hlbWE7XG5cbiAgICAgICAgaWYgKHRoaXMuZGF0YSAmJiB0aGlzLmNvbHVtbnMgJiYgdGhpcy5jb2x1bW5zLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIHRoaXMuZGF0YS5zZXRDb2x1bW5zKHRoaXMuY29sdW1ucyk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIHNldFRhYmxlU29ydGluZyhzb3J0aW5nKSB7XG4gICAgICAgIGlmICh0aGlzLmRhdGEpIHtcbiAgICAgICAgICAgIHRoaXMuZGF0YS5zZXRTb3J0aW5nKHRoaXMuY29udmVydFRvRGF0YVNvcnRpbmcoc29ydGluZykpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIGdldFNjaGVtYUZyb21IdG1sKCk6IGFueSB7XG4gICAgICAgIGxldCBzY2hlbWEgPSBbXTtcbiAgICAgICAgaWYgKHRoaXMuY29sdW1uTGlzdCAmJiB0aGlzLmNvbHVtbkxpc3QuY29sdW1ucyAmJiB0aGlzLmNvbHVtbkxpc3QuY29sdW1ucy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICBzY2hlbWEgPSB0aGlzLmNvbHVtbkxpc3QuY29sdW1ucy5tYXAoKGMpID0+IDxEYXRhQ29sdW1uPiBjKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gc2NoZW1hO1xuICAgIH1cblxuICAgIG9uUm93Q2xpY2socm93OiBEYXRhUm93LCBtb3VzZUV2ZW50OiBNb3VzZUV2ZW50KSB7XG4gICAgICAgIGlmIChtb3VzZUV2ZW50KSB7XG4gICAgICAgICAgICBtb3VzZUV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAocm93KSB7XG4gICAgICAgICAgICBjb25zdCBkYXRhUm93RXZlbnQgPSBuZXcgRGF0YVJvd0V2ZW50KHJvdywgbW91c2VFdmVudCwgdGhpcyk7XG4gICAgICAgICAgICB0aGlzLmNsaWNrT2JzZXJ2ZXIubmV4dChkYXRhUm93RXZlbnQpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgb25FbnRlcktleVByZXNzZWQocm93OiBEYXRhUm93LCBlOiBLZXlib2FyZEV2ZW50KSB7XG4gICAgICAgIGlmIChyb3cpIHtcbiAgICAgICAgICAgIHRoaXMuaGFuZGxlUm93U2VsZWN0aW9uKHJvdywgZSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIGhhbmRsZVJvd1NlbGVjdGlvbihyb3c6IERhdGFSb3csIGU6IEtleWJvYXJkRXZlbnQgfCBNb3VzZUV2ZW50KSB7XG4gICAgICAgIGlmICh0aGlzLmRhdGEpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmlzU2luZ2xlU2VsZWN0aW9uTW9kZSgpKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5yZXNldFNlbGVjdGlvbigpO1xuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0Um93KHJvdywgdHJ1ZSk7XG4gICAgICAgICAgICAgICAgdGhpcy5lbWl0Um93U2VsZWN0aW9uRXZlbnQoJ3Jvdy1zZWxlY3QnLCByb3cpO1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5pc011bHRpU2VsZWN0aW9uTW9kZSgpKSB7XG4gICAgICAgICAgICAgICAgY29uc3QgbW9kaWZpZXIgPSBlICYmIChlLm1ldGFLZXkgfHwgZS5jdHJsS2V5KTtcbiAgICAgICAgICAgICAgICBsZXQgbmV3VmFsdWU6IGJvb2xlYW47XG4gICAgICAgICAgICAgICAgaWYgKHRoaXMuc2VsZWN0aW9uLmxlbmd0aCA9PT0gMSkge1xuICAgICAgICAgICAgICAgICAgICBuZXdWYWx1ZSA9ICFyb3cuaXNTZWxlY3RlZDtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICBuZXdWYWx1ZSA9IG1vZGlmaWVyID8gIXJvdy5pc1NlbGVjdGVkIDogdHJ1ZTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgY29uc3QgZG9tRXZlbnROYW1lID0gbmV3VmFsdWUgPyAncm93LXNlbGVjdCcgOiAncm93LXVuc2VsZWN0JztcblxuICAgICAgICAgICAgICAgIGlmICghbW9kaWZpZXIpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yZXNldFNlbGVjdGlvbigpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdFJvdyhyb3csIG5ld1ZhbHVlKTtcbiAgICAgICAgICAgICAgICB0aGlzLmVtaXRSb3dTZWxlY3Rpb25FdmVudChkb21FdmVudE5hbWUsIHJvdyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICByZXNldFNlbGVjdGlvbigpOiB2b2lkIHtcbiAgICAgICAgaWYgKHRoaXMuZGF0YSkge1xuICAgICAgICAgICAgY29uc3Qgcm93cyA9IHRoaXMuZGF0YS5nZXRSb3dzKCk7XG4gICAgICAgICAgICBpZiAocm93cyAmJiByb3dzLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgICAgICByb3dzLmZvckVhY2goKHIpID0+IHIuaXNTZWxlY3RlZCA9IGZhbHNlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgICAgIHRoaXMuc2VsZWN0aW9uID0gW107XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5pc1NlbGVjdEFsbENoZWNrZWQgPSBmYWxzZTtcbiAgICB9XG5cbiAgICBvblJvd0RibENsaWNrKHJvdzogRGF0YVJvdywgZXZlbnQ/OiBFdmVudCkge1xuICAgICAgICBpZiAoZXZlbnQpIHtcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIH1cbiAgICAgICAgY29uc3QgZGF0YVJvd0V2ZW50ID0gbmV3IERhdGFSb3dFdmVudChyb3csIGV2ZW50LCB0aGlzKTtcbiAgICAgICAgdGhpcy5jbGlja09ic2VydmVyLm5leHQoZGF0YVJvd0V2ZW50KTtcbiAgICB9XG5cbiAgICBvblJvd0tleVVwKHJvdzogRGF0YVJvdywgZTogS2V5Ym9hcmRFdmVudCkge1xuICAgICAgICBjb25zdCBldmVudCA9IG5ldyBDdXN0b21FdmVudCgncm93LWtleXVwJywge1xuICAgICAgICAgICAgZGV0YWlsOiB7XG4gICAgICAgICAgICAgICAgcm93OiByb3csXG4gICAgICAgICAgICAgICAga2V5Ym9hcmRFdmVudDogZSxcbiAgICAgICAgICAgICAgICBzZW5kZXI6IHRoaXNcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBidWJibGVzOiB0cnVlXG4gICAgICAgIH0pO1xuXG4gICAgICAgIHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LmRpc3BhdGNoRXZlbnQoZXZlbnQpO1xuXG4gICAgICAgIGlmIChldmVudC5kZWZhdWx0UHJldmVudGVkKSB7XG4gICAgICAgICAgICBlLnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBpZiAoZS5rZXkgPT09ICdFbnRlcicpIHtcbiAgICAgICAgICAgICAgICB0aGlzLm9uS2V5Ym9hcmROYXZpZ2F0ZShyb3csIGUpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBvbktleWJvYXJkTmF2aWdhdGUocm93OiBEYXRhUm93LCBrZXlib2FyZEV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XG4gICAgICAgIGlmIChrZXlib2FyZEV2ZW50KSB7XG4gICAgICAgICAgICBrZXlib2FyZEV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgIH1cblxuICAgICAgICBjb25zdCBldmVudCA9IG5ldyBEYXRhUm93RXZlbnQocm93LCBrZXlib2FyZEV2ZW50LCB0aGlzKTtcblxuICAgICAgICB0aGlzLnJvd0RibENsaWNrLmVtaXQoZXZlbnQpO1xuICAgICAgICB0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5kaXNwYXRjaEV2ZW50KFxuICAgICAgICAgICAgbmV3IEN1c3RvbUV2ZW50KCdyb3ctZGJsY2xpY2snLCB7XG4gICAgICAgICAgICAgICAgZGV0YWlsOiBldmVudCxcbiAgICAgICAgICAgICAgICBidWJibGVzOiB0cnVlXG4gICAgICAgICAgICB9KVxuICAgICAgICApO1xuICAgIH1cblxuICAgIG9uQ29sdW1uSGVhZGVyQ2xpY2soY29sdW1uOiBEYXRhQ29sdW1uKSB7XG4gICAgICAgIGlmIChjb2x1bW4gJiYgY29sdW1uLnNvcnRhYmxlKSB7XG4gICAgICAgICAgICBjb25zdCBjdXJyZW50ID0gdGhpcy5kYXRhLmdldFNvcnRpbmcoKTtcbiAgICAgICAgICAgIGxldCBuZXdEaXJlY3Rpb24gPSAnYXNjJztcbiAgICAgICAgICAgIGlmIChjdXJyZW50ICYmIGNvbHVtbi5rZXkgPT09IGN1cnJlbnQua2V5KSB7XG4gICAgICAgICAgICAgICAgbmV3RGlyZWN0aW9uID0gY3VycmVudC5kaXJlY3Rpb24gPT09ICdhc2MnID8gJ2Rlc2MnIDogJ2FzYyc7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLmRhdGEuc2V0U29ydGluZyhuZXcgRGF0YVNvcnRpbmcoY29sdW1uLmtleSwgbmV3RGlyZWN0aW9uKSk7XG4gICAgICAgICAgICB0aGlzLmVtaXRTb3J0aW5nQ2hhbmdlZEV2ZW50KGNvbHVtbi5rZXksIG5ld0RpcmVjdGlvbik7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBvblNlbGVjdEFsbENsaWNrKG1hdENoZWNrYm94Q2hhbmdlOiBNYXRDaGVja2JveENoYW5nZSkge1xuICAgICAgICB0aGlzLmlzU2VsZWN0QWxsQ2hlY2tlZCA9IG1hdENoZWNrYm94Q2hhbmdlLmNoZWNrZWQ7XG5cbiAgICAgICAgaWYgKHRoaXMubXVsdGlzZWxlY3QpIHtcbiAgICAgICAgICAgIGNvbnN0IHJvd3MgPSB0aGlzLmRhdGEuZ2V0Um93cygpO1xuICAgICAgICAgICAgaWYgKHJvd3MgJiYgcm93cy5sZW5ndGggPiAwKSB7XG4gICAgICAgICAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCByb3dzLmxlbmd0aDsgaSsrKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0Um93KHJvd3NbaV0sIG1hdENoZWNrYm94Q2hhbmdlLmNoZWNrZWQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgY29uc3QgZG9tRXZlbnROYW1lID0gbWF0Q2hlY2tib3hDaGFuZ2UuY2hlY2tlZCA/ICdyb3ctc2VsZWN0JyA6ICdyb3ctdW5zZWxlY3QnO1xuICAgICAgICAgICAgY29uc3Qgcm93ID0gdGhpcy5zZWxlY3Rpb24ubGVuZ3RoID4gMCA/IHRoaXMuc2VsZWN0aW9uWzBdIDogbnVsbDtcblxuICAgICAgICAgICAgdGhpcy5lbWl0Um93U2VsZWN0aW9uRXZlbnQoZG9tRXZlbnROYW1lLCByb3cpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgb25DaGVja2JveENoYW5nZShyb3c6IERhdGFSb3csIGV2ZW50OiBNYXRDaGVja2JveENoYW5nZSkge1xuICAgICAgICBjb25zdCBuZXdWYWx1ZSA9IGV2ZW50LmNoZWNrZWQ7XG5cbiAgICAgICAgdGhpcy5zZWxlY3RSb3cocm93LCBuZXdWYWx1ZSk7XG5cbiAgICAgICAgY29uc3QgZG9tRXZlbnROYW1lID0gbmV3VmFsdWUgPyAncm93LXNlbGVjdCcgOiAncm93LXVuc2VsZWN0JztcbiAgICAgICAgdGhpcy5lbWl0Um93U2VsZWN0aW9uRXZlbnQoZG9tRXZlbnROYW1lLCByb3cpO1xuICAgIH1cblxuICAgIG9uSW1hZ2VMb2FkaW5nRXJyb3IoZXZlbnQ6IEV2ZW50LCByb3c6IERhdGFSb3cpIHtcbiAgICAgICAgaWYgKGV2ZW50KSB7XG4gICAgICAgICAgICBjb25zdCBlbGVtZW50ID0gPGFueT4gZXZlbnQudGFyZ2V0O1xuXG4gICAgICAgICAgICBpZiAodGhpcy5mYWxsYmFja1RodW1ibmFpbCkge1xuICAgICAgICAgICAgICAgIGVsZW1lbnQuc3JjID0gdGhpcy5mYWxsYmFja1RodW1ibmFpbDtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgZWxlbWVudC5zcmMgPSByb3cuaW1hZ2VFcnJvclJlc29sdmVyKGV2ZW50KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIGlzSWNvblZhbHVlKHJvdzogRGF0YVJvdywgY29sOiBEYXRhQ29sdW1uKTogYm9vbGVhbiB7XG4gICAgICAgIGlmIChyb3cgJiYgY29sKSB7XG4gICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHJvdy5nZXRWYWx1ZShjb2wua2V5KTtcbiAgICAgICAgICAgIHJldHVybiB2YWx1ZSAmJiB2YWx1ZS5zdGFydHNXaXRoKCdtYXRlcmlhbC1pY29uczovLycpO1xuICAgICAgICB9XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG5cbiAgICBhc0ljb25WYWx1ZShyb3c6IERhdGFSb3csIGNvbDogRGF0YUNvbHVtbik6IHN0cmluZyB7XG4gICAgICAgIGlmICh0aGlzLmlzSWNvblZhbHVlKHJvdywgY29sKSkge1xuICAgICAgICAgICAgY29uc3QgdmFsdWUgPSByb3cuZ2V0VmFsdWUoY29sLmtleSkgfHwgJyc7XG4gICAgICAgICAgICByZXR1cm4gdmFsdWUucmVwbGFjZSgnbWF0ZXJpYWwtaWNvbnM6Ly8nLCAnJyk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgaWNvbkFsdFRleHRLZXkodmFsdWU6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgICAgIHJldHVybiB2YWx1ZSA/ICdJQ09OUy4nICsgdmFsdWUuc3Vic3RyaW5nKHZhbHVlLmxhc3RJbmRleE9mKCcvJykgKyAxKS5yZXBsYWNlKC9cXC5bYS16XSsvLCAnJykgOiAnJztcbiAgICB9XG5cbiAgICBpc0NvbHVtblNvcnRlZChjb2w6IERhdGFDb2x1bW4sIGRpcmVjdGlvbjogc3RyaW5nKTogYm9vbGVhbiB7XG4gICAgICAgIGlmIChjb2wgJiYgZGlyZWN0aW9uKSB7XG4gICAgICAgICAgICBjb25zdCBzb3J0aW5nID0gdGhpcy5kYXRhLmdldFNvcnRpbmcoKTtcbiAgICAgICAgICAgIHJldHVybiBzb3J0aW5nICYmIHNvcnRpbmcua2V5ID09PSBjb2wua2V5ICYmIHNvcnRpbmcuZGlyZWN0aW9uID09PSBkaXJlY3Rpb247XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIGdldENvbnRleHRNZW51QWN0aW9ucyhyb3c6IERhdGFSb3csIGNvbDogRGF0YUNvbHVtbik6IGFueVtdIHtcbiAgICAgICAgY29uc3QgZXZlbnQgPSBuZXcgRGF0YUNlbGxFdmVudChyb3csIGNvbCwgW10pO1xuICAgICAgICB0aGlzLnNob3dSb3dDb250ZXh0TWVudS5lbWl0KGV2ZW50KTtcbiAgICAgICAgcmV0dXJuIGV2ZW50LnZhbHVlLmFjdGlvbnM7XG4gICAgfVxuXG4gICAgZ2V0Um93QWN0aW9ucyhyb3c6IERhdGFSb3csIGNvbD86IERhdGFDb2x1bW4pOiBhbnlbXSB7XG4gICAgICAgIGNvbnN0IGlkID0gcm93LmdldFZhbHVlKCdpZCcpO1xuXG4gICAgICAgIGlmICghdGhpcy5yb3dNZW51Q2FjaGVbaWRdKSB7XG4gICAgICAgICAgICBjb25zdCBldmVudCA9IG5ldyBEYXRhQ2VsbEV2ZW50KHJvdywgY29sLCBbXSk7XG4gICAgICAgICAgICB0aGlzLnNob3dSb3dBY3Rpb25zTWVudS5lbWl0KGV2ZW50KTtcbiAgICAgICAgICAgIGlmICghdGhpcy5yb3dNZW51Q2FjaGVFbmFibGVkKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIGV2ZW50LnZhbHVlLmFjdGlvbnM7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgICB0aGlzLnJvd01lbnVDYWNoZVtpZF0gPSBldmVudC52YWx1ZS5hY3Rpb25zO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIHRoaXMucm93TWVudUNhY2hlW2lkXTtcbiAgICB9XG5cbiAgICBvbkV4ZWN1dGVSb3dBY3Rpb24ocm93OiBEYXRhUm93LCBhY3Rpb246IGFueSkge1xuICAgICAgICBpZiAoYWN0aW9uLmRpc2FibGVkIHx8IGFjdGlvbi5kaXNhYmxlZCkge1xuICAgICAgICAgICAgZXZlbnQuc3RvcFByb3BhZ2F0aW9uKCk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmV4ZWN1dGVSb3dBY3Rpb24uZW1pdChuZXcgRGF0YVJvd0FjdGlvbkV2ZW50KHJvdywgYWN0aW9uKSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICByb3dBbGxvd3NEcm9wKHJvdzogRGF0YVJvdyk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gcm93LmlzRHJvcFRhcmdldCA9PT0gdHJ1ZTtcbiAgICB9XG5cbiAgICBoYXNTZWxlY3Rpb25Nb2RlKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5pc1NpbmdsZVNlbGVjdGlvbk1vZGUoKSB8fCB0aGlzLmlzTXVsdGlTZWxlY3Rpb25Nb2RlKCk7XG4gICAgfVxuXG4gICAgaXNTaW5nbGVTZWxlY3Rpb25Nb2RlKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5zZWxlY3Rpb25Nb2RlICYmIHRoaXMuc2VsZWN0aW9uTW9kZS50b0xvd2VyQ2FzZSgpID09PSAnc2luZ2xlJztcbiAgICB9XG5cbiAgICBpc011bHRpU2VsZWN0aW9uTW9kZSgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VsZWN0aW9uTW9kZSAmJiB0aGlzLnNlbGVjdGlvbk1vZGUudG9Mb3dlckNhc2UoKSA9PT0gJ211bHRpcGxlJztcbiAgICB9XG5cbiAgICBnZXRSb3dTdHlsZShyb3c6IERhdGFSb3cpOiBzdHJpbmcge1xuICAgICAgICByb3cuY3NzQ2xhc3MgPSByb3cuY3NzQ2xhc3MgPyByb3cuY3NzQ2xhc3MgOiAnJztcbiAgICAgICAgdGhpcy5yb3dTdHlsZUNsYXNzID0gdGhpcy5yb3dTdHlsZUNsYXNzID8gdGhpcy5yb3dTdHlsZUNsYXNzIDogJyc7XG4gICAgICAgIHJldHVybiBgJHtyb3cuY3NzQ2xhc3N9ICR7dGhpcy5yb3dTdHlsZUNsYXNzfWA7XG4gICAgfVxuXG4gICAgZ2V0U29ydGluZ0tleSgpOiBzdHJpbmcge1xuICAgICAgICBpZiAodGhpcy5kYXRhLmdldFNvcnRpbmcoKSkge1xuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZGF0YS5nZXRTb3J0aW5nKCkua2V5O1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgc2VsZWN0Um93KHJvdzogRGF0YVJvdywgdmFsdWU6IGJvb2xlYW4pIHtcbiAgICAgICAgaWYgKHJvdykge1xuICAgICAgICAgICAgcm93LmlzU2VsZWN0ZWQgPSB2YWx1ZTtcbiAgICAgICAgICAgIGNvbnN0IGlkeCA9IHRoaXMuc2VsZWN0aW9uLmluZGV4T2Yocm93KTtcbiAgICAgICAgICAgIGlmICh2YWx1ZSkge1xuICAgICAgICAgICAgICAgIGlmIChpZHggPCAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0aW9uLnB1c2gocm93KTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgIGlmIChpZHggPiAtMSkge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNlbGVjdGlvbi5zcGxpY2UoaWR4LCAxKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBnZXRDZWxsVG9vbHRpcChyb3c6IERhdGFSb3csIGNvbDogRGF0YUNvbHVtbik6IHN0cmluZyB7XG4gICAgICAgIGlmIChyb3cgJiYgY29sICYmIGNvbC5mb3JtYXRUb29sdGlwKSB7XG4gICAgICAgICAgICBjb25zdCByZXN1bHQ6IHN0cmluZyA9IGNvbC5mb3JtYXRUb29sdGlwKHJvdywgY29sKTtcbiAgICAgICAgICAgIGlmIChyZXN1bHQpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4gcmVzdWx0O1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgICAgIHJldHVybiBudWxsO1xuICAgIH1cblxuICAgIGdldFNvcnRhYmxlQ29sdW1ucygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGF0YS5nZXRDb2x1bW5zKCkuZmlsdGVyKChjb2x1bW4pID0+IHtcbiAgICAgICAgICAgIHJldHVybiBjb2x1bW4uc29ydGFibGUgPT09IHRydWU7XG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGlzRW1wdHkoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmRhdGEuZ2V0Um93cygpLmxlbmd0aCA9PT0gMDtcbiAgICB9XG5cbiAgICBpc0hlYWRlclZpc2libGUoKSB7XG4gICAgICAgIHJldHVybiAhdGhpcy5sb2FkaW5nICYmICF0aGlzLmlzRW1wdHkoKSAmJiAhdGhpcy5ub1Blcm1pc3Npb247XG4gICAgfVxuXG4gICAgaXNTdGlja3lIZWFkZXJFbmFibGVkKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5zdGlja3lIZWFkZXIgJiYgdGhpcy5pc0hlYWRlclZpc2libGUoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGVtaXRSb3dTZWxlY3Rpb25FdmVudChuYW1lOiBzdHJpbmcsIHJvdzogRGF0YVJvdykge1xuICAgICAgICBjb25zdCBkb21FdmVudCA9IG5ldyBDdXN0b21FdmVudChuYW1lLCB7XG4gICAgICAgICAgICBkZXRhaWw6IHtcbiAgICAgICAgICAgICAgICByb3c6IHJvdyxcbiAgICAgICAgICAgICAgICBzZWxlY3Rpb246IHRoaXMuc2VsZWN0aW9uXG4gICAgICAgICAgICB9LFxuICAgICAgICAgICAgYnViYmxlczogdHJ1ZVxuICAgICAgICB9KTtcbiAgICAgICAgdGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQuZGlzcGF0Y2hFdmVudChkb21FdmVudCk7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBlbWl0U29ydGluZ0NoYW5nZWRFdmVudChrZXk6IHN0cmluZywgZGlyZWN0aW9uOiBzdHJpbmcpIHtcbiAgICAgICAgY29uc3QgZG9tRXZlbnQgPSBuZXcgQ3VzdG9tRXZlbnQoJ3NvcnRpbmctY2hhbmdlZCcsIHtcbiAgICAgICAgICAgIGRldGFpbDoge1xuICAgICAgICAgICAgICAgIGtleSxcbiAgICAgICAgICAgICAgICBkaXJlY3Rpb25cbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICBidWJibGVzOiB0cnVlXG4gICAgICAgIH0pO1xuICAgICAgICB0aGlzLmVsZW1lbnRSZWYubmF0aXZlRWxlbWVudC5kaXNwYXRjaEV2ZW50KGRvbUV2ZW50KTtcbiAgICB9XG5cbiAgICBuZ09uRGVzdHJveSgpIHtcbiAgICAgICAgdGhpcy51bnN1YnNjcmliZUNsaWNrU3RyZWFtKCk7XG5cbiAgICAgICAgdGhpcy5zdWJzY3JpcHRpb25zLmZvckVhY2goKHMpID0+IHMudW5zdWJzY3JpYmUoKSk7XG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucyA9IFtdO1xuXG4gICAgICAgIGlmICh0aGlzLmRhdGFSb3dzQ2hhbmdlZCkge1xuICAgICAgICAgICAgdGhpcy5kYXRhUm93c0NoYW5nZWQudW5zdWJzY3JpYmUoKTtcbiAgICAgICAgICAgIHRoaXMuZGF0YVJvd3NDaGFuZ2VkID0gbnVsbDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGRhdGF0YWJsZUxheW91dEZpeCgpIHtcbiAgICAgICAgY29uc3QgbWF4R2FsbGVyeVJvd3MgPSAyNTtcblxuICAgICAgICBpZiAodGhpcy5kaXNwbGF5ID09PSAnZ2FsbGVyeScpIHtcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgbWF4R2FsbGVyeVJvd3M7IGkrKykge1xuICAgICAgICAgICAgICAgdGhpcy5mYWtlUm93cy5wdXNoKCcnKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuZmFrZVJvd3MgPSBbXTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGdldE5hbWVDb2x1bW5WYWx1ZSgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGF0YS5nZXRDb2x1bW5zKCkuZmluZCggKGVsOiBhbnkpID0+IHtcbiAgICAgICAgICAgIHJldHVybiBlbC5rZXkuaW5jbHVkZXMoJ25hbWUnKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgZ2V0QXV0b21hdGlvblZhbHVlKHJvdzogRGF0YVJvdywgY29sOiBEYXRhQ29sdW1uKSB7XG4gICAgICAgIGNvbnN0IG5hbWUgPSB0aGlzLmdldE5hbWVDb2x1bW5WYWx1ZSgpO1xuICAgICAgICByZXR1cm4gbmFtZSA/IHJvdy5nZXRWYWx1ZShuYW1lLmtleSkgOiAnJztcbiAgICB9XG59XG5cbmV4cG9ydCBpbnRlcmZhY2UgRGF0YVRhYmxlRHJvcEV2ZW50IHtcbiAgICBkZXRhaWw6IHtcbiAgICAgICAgdGFyZ2V0OiAnY2VsbCcgfCAnaGVhZGVyJztcbiAgICAgICAgZXZlbnQ6IEV2ZW50O1xuICAgICAgICBjb2x1bW46IERhdGFDb2x1bW47XG4gICAgICAgIHJvdz86IERhdGFSb3dcbiAgICB9O1xuXG4gICAgcHJldmVudERlZmF1bHQoKTogdm9pZDtcbn1cbiJdfQ==