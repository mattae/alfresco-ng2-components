/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ViewEncapsulation } from '@angular/core';
import { DataTableCellComponent } from './datatable-cell.component';
import { UserPreferencesService, UserPreferenceValues } from '../../../services/user-preferences.service';
import { AlfrescoApiService } from '../../../services/alfresco-api.service';
import { AppConfigService } from '../../../app-config/app-config.service';
import { takeUntil } from 'rxjs/operators';
var DateCellComponent = /** @class */ (function (_super) {
    tslib_1.__extends(DateCellComponent, _super);
    function DateCellComponent(userPreferenceService, alfrescoApiService, appConfig) {
        var _this = _super.call(this, alfrescoApiService) || this;
        _this.dateFormat = appConfig.get('dateValues.defaultDateFormat', DateCellComponent.DATE_FORMAT);
        if (userPreferenceService) {
            userPreferenceService
                .select(UserPreferenceValues.Locale)
                .pipe(takeUntil(_this.onDestroy$))
                .subscribe((/**
             * @param {?} locale
             * @return {?}
             */
            function (locale) { return _this.currentLocale = locale; }));
        }
        return _this;
    }
    Object.defineProperty(DateCellComponent.prototype, "format", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.column) {
                return this.column.format || this.dateFormat;
            }
            return this.dateFormat;
        },
        enumerable: true,
        configurable: true
    });
    DateCellComponent.DATE_FORMAT = 'medium';
    DateCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-date-cell',
                    template: "\n        <ng-container>\n            <span\n                [attr.aria-label]=\"value$ | async | adfTimeAgo: currentLocale\"\n                title=\"{{ tooltip | adfLocalizedDate: 'medium' }}\"\n                class=\"adf-datatable-cell-value\"\n                *ngIf=\"format === 'timeAgo'; else standard_date\">\n                {{ value$ | async | adfTimeAgo: currentLocale }}\n            </span>\n        </ng-container>\n        <ng-template #standard_date>\n            <span\n                class=\"adf-datatable-cell-value\"\n                title=\"{{ tooltip | adfLocalizedDate: format }}\"\n                class=\"adf-datatable-cell-value\"\n                [attr.aria-label]=\"value$ | async | adfLocalizedDate: format\">\n                {{ value$ | async | adfLocalizedDate: format }}\n            </span>\n        </ng-template>\n    ",
                    encapsulation: ViewEncapsulation.None,
                    host: { class: 'adf-date-cell adf-datatable-content-cell' }
                }] }
    ];
    /** @nocollapse */
    DateCellComponent.ctorParameters = function () { return [
        { type: UserPreferencesService },
        { type: AlfrescoApiService },
        { type: AppConfigService }
    ]; };
    return DateCellComponent;
}(DataTableCellComponent));
export { DateCellComponent };
if (false) {
    /** @type {?} */
    DateCellComponent.DATE_FORMAT;
    /** @type {?} */
    DateCellComponent.prototype.currentLocale;
    /** @type {?} */
    DateCellComponent.prototype.dateFormat;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRhdGF0YWJsZS9jb21wb25lbnRzL2RhdGF0YWJsZS9kYXRlLWNlbGwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsU0FBUyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzdELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFDSCxzQkFBc0IsRUFDdEIsb0JBQW9CLEVBQ3ZCLE1BQU0sNENBQTRDLENBQUM7QUFDcEQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDNUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFDMUUsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTNDO0lBMEJ1Qyw2Q0FBc0I7SUFjekQsMkJBQ0kscUJBQTZDLEVBQzdDLGtCQUFzQyxFQUN0QyxTQUEyQjtRQUgvQixZQUtJLGtCQUFNLGtCQUFrQixDQUFDLFNBUzVCO1FBUEcsS0FBSSxDQUFDLFVBQVUsR0FBRyxTQUFTLENBQUMsR0FBRyxDQUFDLDhCQUE4QixFQUFFLGlCQUFpQixDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQy9GLElBQUkscUJBQXFCLEVBQUU7WUFDdkIscUJBQXFCO2lCQUNoQixNQUFNLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDO2lCQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztpQkFDaEMsU0FBUzs7OztZQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsS0FBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLEVBQTNCLENBQTJCLEVBQUMsQ0FBQztTQUN6RDs7SUFDTCxDQUFDO0lBckJELHNCQUFJLHFDQUFNOzs7O1FBQVY7WUFDSSxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ2IsT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDO2FBQ2hEO1lBQ0QsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDO1FBQzNCLENBQUM7OztPQUFBO0lBVk0sNkJBQVcsR0FBRyxRQUFRLENBQUM7O2dCQTVCakMsU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxlQUFlO29CQUV6QixRQUFRLEVBQUUseTFCQW1CVDtvQkFDRCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtvQkFDckMsSUFBSSxFQUFFLEVBQUUsS0FBSyxFQUFFLDBDQUEwQyxFQUFFO2lCQUM5RDs7OztnQkFoQ0csc0JBQXNCO2dCQUdqQixrQkFBa0I7Z0JBQ2xCLGdCQUFnQjs7SUEwRHpCLHdCQUFDO0NBQUEsQUF2REQsQ0EwQnVDLHNCQUFzQixHQTZCNUQ7U0E3QlksaUJBQWlCOzs7SUFFMUIsOEJBQThCOztJQUU5QiwwQ0FBc0I7O0lBQ3RCLHVDQUFtQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERhdGFUYWJsZUNlbGxDb21wb25lbnQgfSBmcm9tICcuL2RhdGF0YWJsZS1jZWxsLmNvbXBvbmVudCc7XHJcbmltcG9ydCB7XHJcbiAgICBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlLFxyXG4gICAgVXNlclByZWZlcmVuY2VWYWx1ZXNcclxufSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy91c2VyLXByZWZlcmVuY2VzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtZGF0ZS1jZWxsJyxcclxuXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxuZy1jb250YWluZXI+XHJcbiAgICAgICAgICAgIDxzcGFuXHJcbiAgICAgICAgICAgICAgICBbYXR0ci5hcmlhLWxhYmVsXT1cInZhbHVlJCB8IGFzeW5jIHwgYWRmVGltZUFnbzogY3VycmVudExvY2FsZVwiXHJcbiAgICAgICAgICAgICAgICB0aXRsZT1cInt7IHRvb2x0aXAgfCBhZGZMb2NhbGl6ZWREYXRlOiAnbWVkaXVtJyB9fVwiXHJcbiAgICAgICAgICAgICAgICBjbGFzcz1cImFkZi1kYXRhdGFibGUtY2VsbC12YWx1ZVwiXHJcbiAgICAgICAgICAgICAgICAqbmdJZj1cImZvcm1hdCA9PT0gJ3RpbWVBZ28nOyBlbHNlIHN0YW5kYXJkX2RhdGVcIj5cclxuICAgICAgICAgICAgICAgIHt7IHZhbHVlJCB8IGFzeW5jIHwgYWRmVGltZUFnbzogY3VycmVudExvY2FsZSB9fVxyXG4gICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgPC9uZy1jb250YWluZXI+XHJcbiAgICAgICAgPG5nLXRlbXBsYXRlICNzdGFuZGFyZF9kYXRlPlxyXG4gICAgICAgICAgICA8c3BhblxyXG4gICAgICAgICAgICAgICAgY2xhc3M9XCJhZGYtZGF0YXRhYmxlLWNlbGwtdmFsdWVcIlxyXG4gICAgICAgICAgICAgICAgdGl0bGU9XCJ7eyB0b29sdGlwIHwgYWRmTG9jYWxpemVkRGF0ZTogZm9ybWF0IH19XCJcclxuICAgICAgICAgICAgICAgIGNsYXNzPVwiYWRmLWRhdGF0YWJsZS1jZWxsLXZhbHVlXCJcclxuICAgICAgICAgICAgICAgIFthdHRyLmFyaWEtbGFiZWxdPVwidmFsdWUkIHwgYXN5bmMgfCBhZGZMb2NhbGl6ZWREYXRlOiBmb3JtYXRcIj5cclxuICAgICAgICAgICAgICAgIHt7IHZhbHVlJCB8IGFzeW5jIHwgYWRmTG9jYWxpemVkRGF0ZTogZm9ybWF0IH19XHJcbiAgICAgICAgICAgIDwvc3Bhbj5cclxuICAgICAgICA8L25nLXRlbXBsYXRlPlxyXG4gICAgYCxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXHJcbiAgICBob3N0OiB7IGNsYXNzOiAnYWRmLWRhdGUtY2VsbCBhZGYtZGF0YXRhYmxlLWNvbnRlbnQtY2VsbCcgfVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRGF0ZUNlbGxDb21wb25lbnQgZXh0ZW5kcyBEYXRhVGFibGVDZWxsQ29tcG9uZW50IHtcclxuXHJcbiAgICBzdGF0aWMgREFURV9GT1JNQVQgPSAnbWVkaXVtJztcclxuXHJcbiAgICBjdXJyZW50TG9jYWxlOiBzdHJpbmc7XHJcbiAgICBkYXRlRm9ybWF0OiBzdHJpbmc7XHJcblxyXG4gICAgZ2V0IGZvcm1hdCgpOiBzdHJpbmcge1xyXG4gICAgICAgIGlmICh0aGlzLmNvbHVtbikge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jb2x1bW4uZm9ybWF0IHx8IHRoaXMuZGF0ZUZvcm1hdDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZGF0ZUZvcm1hdDtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICB1c2VyUHJlZmVyZW5jZVNlcnZpY2U6IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UsXHJcbiAgICAgICAgYWxmcmVzY29BcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UsXHJcbiAgICAgICAgYXBwQ29uZmlnOiBBcHBDb25maWdTZXJ2aWNlXHJcbiAgICApIHtcclxuICAgICAgICBzdXBlcihhbGZyZXNjb0FwaVNlcnZpY2UpO1xyXG5cclxuICAgICAgICB0aGlzLmRhdGVGb3JtYXQgPSBhcHBDb25maWcuZ2V0KCdkYXRlVmFsdWVzLmRlZmF1bHREYXRlRm9ybWF0JywgRGF0ZUNlbGxDb21wb25lbnQuREFURV9GT1JNQVQpO1xyXG4gICAgICAgIGlmICh1c2VyUHJlZmVyZW5jZVNlcnZpY2UpIHtcclxuICAgICAgICAgICAgdXNlclByZWZlcmVuY2VTZXJ2aWNlXHJcbiAgICAgICAgICAgICAgICAuc2VsZWN0KFVzZXJQcmVmZXJlbmNlVmFsdWVzLkxvY2FsZSlcclxuICAgICAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLm9uRGVzdHJveSQpKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShsb2NhbGUgPT4gdGhpcy5jdXJyZW50TG9jYWxlID0gbG9jYWxlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19