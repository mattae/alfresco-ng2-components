/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Directive, ViewEncapsulation } from '@angular/core';
var EmptyListComponent = /** @class */ (function () {
    function EmptyListComponent() {
    }
    EmptyListComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-empty-list',
                    template: "<div class=\"adf-empty-list_template\">\r\n    <ng-content select=\"[adf-empty-list-header]\"></ng-content>\r\n    <ng-content select=\"[adf-empty-list-body]\"></ng-content>\r\n    <ng-content select=\"[adf-empty-list-footer]\"></ng-content>\r\n    <ng-content></ng-content>\r\n</div>",
                    encapsulation: ViewEncapsulation.None,
                    styles: [".adf-empty-list_template{text-align:center;margin-top:20px;margin-bottom:20px}"]
                }] }
    ];
    return EmptyListComponent;
}());
export { EmptyListComponent };
var EmptyListHeaderDirective = /** @class */ (function () {
    function EmptyListHeaderDirective() {
    }
    EmptyListHeaderDirective.decorators = [
        { type: Directive, args: [{ selector: '[adf-empty-list-header]' },] }
    ];
    return EmptyListHeaderDirective;
}());
export { EmptyListHeaderDirective };
var EmptyListBodyDirective = /** @class */ (function () {
    function EmptyListBodyDirective() {
    }
    EmptyListBodyDirective.decorators = [
        { type: Directive, args: [{ selector: '[adf-empty-list-body]' },] }
    ];
    return EmptyListBodyDirective;
}());
export { EmptyListBodyDirective };
var EmptyListFooterDirective = /** @class */ (function () {
    function EmptyListFooterDirective() {
    }
    EmptyListFooterDirective.decorators = [
        { type: Directive, args: [{ selector: '[adf-empty-list-footer]' },] }
    ];
    return EmptyListFooterDirective;
}());
export { EmptyListFooterDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW1wdHktbGlzdC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJkYXRhdGFibGUvY29tcG9uZW50cy9kYXRhdGFibGUvZW1wdHktbGlzdC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFeEU7SUFBQTtJQU1pQyxDQUFDOztnQkFOakMsU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxnQkFBZ0I7b0JBRTFCLHdTQUEwQztvQkFDMUMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2lCQUN4Qzs7SUFDZ0MseUJBQUM7Q0FBQSxBQU5sQyxJQU1rQztTQUFyQixrQkFBa0I7QUFFL0I7SUFBQTtJQUEyRixDQUFDOztnQkFBM0YsU0FBUyxTQUFDLEVBQUUsUUFBUSxFQUFFLHlCQUF5QixFQUFFOztJQUF5QywrQkFBQztDQUFBLEFBQTVGLElBQTRGO1NBQTNCLHdCQUF3QjtBQUN6RjtJQUFBO0lBQXVGLENBQUM7O2dCQUF2RixTQUFTLFNBQUMsRUFBRSxRQUFRLEVBQUUsdUJBQXVCLEVBQUU7O0lBQXVDLDZCQUFDO0NBQUEsQUFBeEYsSUFBd0Y7U0FBekIsc0JBQXNCO0FBQ3JGO0lBQUE7SUFBMkYsQ0FBQzs7Z0JBQTNGLFNBQVMsU0FBQyxFQUFFLFFBQVEsRUFBRSx5QkFBeUIsRUFBRTs7SUFBeUMsK0JBQUM7Q0FBQSxBQUE1RixJQUE0RjtTQUEzQix3QkFBd0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ29tcG9uZW50LCBEaXJlY3RpdmUsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLWVtcHR5LWxpc3QnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vZW1wdHktbGlzdC5jb21wb25lbnQuc2NzcyddLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2VtcHR5LWxpc3QuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRW1wdHlMaXN0Q29tcG9uZW50IHt9XHJcblxyXG5ARGlyZWN0aXZlKHsgc2VsZWN0b3I6ICdbYWRmLWVtcHR5LWxpc3QtaGVhZGVyXScgfSkgZXhwb3J0IGNsYXNzIEVtcHR5TGlzdEhlYWRlckRpcmVjdGl2ZSB7fVxyXG5ARGlyZWN0aXZlKHsgc2VsZWN0b3I6ICdbYWRmLWVtcHR5LWxpc3QtYm9keV0nIH0pIGV4cG9ydCBjbGFzcyBFbXB0eUxpc3RCb2R5RGlyZWN0aXZlIHt9XHJcbkBEaXJlY3RpdmUoeyBzZWxlY3RvcjogJ1thZGYtZW1wdHktbGlzdC1mb290ZXJdJyB9KSBleHBvcnQgY2xhc3MgRW1wdHlMaXN0Rm9vdGVyRGlyZWN0aXZlIHt9XHJcbiJdfQ==