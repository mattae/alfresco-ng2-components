/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, Component, ViewEncapsulation, Input } from '@angular/core';
import { DataTableCellComponent } from './datatable-cell.component';
var JsonCellComponent = /** @class */ (function (_super) {
    tslib_1.__extends(JsonCellComponent, _super);
    function JsonCellComponent() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /**
     * @return {?}
     */
    JsonCellComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.column && this.column.key && this.row && this.data) {
            this.value$.next(this.data.getValue(this.row, this.column));
        }
    };
    JsonCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-json-cell',
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    template: "\n        <ng-container>\n            <span *ngIf=\"copyContent; else defaultJsonTemplate\" class=\"adf-datatable-cell-value\">\n                <pre\n                    class=\"adf-datatable-json-cell\"\n                    [adf-clipboard]=\"'CLIPBOARD.CLICK_TO_COPY'\"\n                    [clipboard-notification]=\"'CLIPBOARD.SUCCESS_COPY'\">{{ value$ | async | json }}</pre>\n            </span>\n        </ng-container>\n        <ng-template #defaultJsonTemplate>\n            <span class=\"adf-datatable-cell-value\">\n                <pre class=\"adf-datatable-json-cell\">{{ value$ | async | json }}</pre>\n            </span>\n        </ng-template>\n    ",
                    encapsulation: ViewEncapsulation.None,
                    host: { class: 'adf-datatable-content-cell' },
                    styles: [".adf-datatable-json-cell{white-space:pre-wrap;word-wrap:break-word}.adf-datatable-cell-value{position:relative}"]
                }] }
    ];
    JsonCellComponent.propDecorators = {
        copyContent: [{ type: Input }]
    };
    return JsonCellComponent;
}(DataTableCellComponent));
export { JsonCellComponent };
if (false) {
    /**
     * Enables/disables a Clipboard directive to allow copying of the cell's content.
     * @type {?}
     */
    JsonCellComponent.prototype.copyContent;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoianNvbi1jZWxsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRhdGF0YWJsZS9jb21wb25lbnRzL2RhdGF0YWJsZS9qc29uLWNlbGwuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsdUJBQXVCLEVBQUUsU0FBUyxFQUFVLGlCQUFpQixFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNyRyxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUVwRTtJQXNCdUMsNkNBQXNCO0lBdEI3RDs7SUFpQ0EsQ0FBQzs7OztJQUxHLG9DQUFROzs7SUFBUjtRQUNJLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsSUFBSSxJQUFJLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDekQsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztTQUMvRDtJQUNMLENBQUM7O2dCQWhDSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGVBQWU7b0JBQ3pCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsNHBCQWNUO29CQUVELGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO29CQUNyQyxJQUFJLEVBQUUsRUFBRSxLQUFLLEVBQUUsNEJBQTRCLEVBQUU7O2lCQUNoRDs7OzhCQUlLLEtBQUs7O0lBUVgsd0JBQUM7Q0FBQSxBQWpDRCxDQXNCdUMsc0JBQXNCLEdBVzVEO1NBWFksaUJBQWlCOzs7Ozs7SUFHekIsd0NBQ3FCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENoYW5nZURldGVjdGlvblN0cmF0ZWd5LCBDb21wb25lbnQsIE9uSW5pdCwgVmlld0VuY2Fwc3VsYXRpb24sIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERhdGFUYWJsZUNlbGxDb21wb25lbnQgfSBmcm9tICcuL2RhdGF0YWJsZS1jZWxsLmNvbXBvbmVudCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLWpzb24tY2VsbCcsXHJcbiAgICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcclxuICAgIHRlbXBsYXRlOiBgXHJcbiAgICAgICAgPG5nLWNvbnRhaW5lcj5cclxuICAgICAgICAgICAgPHNwYW4gKm5nSWY9XCJjb3B5Q29udGVudDsgZWxzZSBkZWZhdWx0SnNvblRlbXBsYXRlXCIgY2xhc3M9XCJhZGYtZGF0YXRhYmxlLWNlbGwtdmFsdWVcIj5cclxuICAgICAgICAgICAgICAgIDxwcmVcclxuICAgICAgICAgICAgICAgICAgICBjbGFzcz1cImFkZi1kYXRhdGFibGUtanNvbi1jZWxsXCJcclxuICAgICAgICAgICAgICAgICAgICBbYWRmLWNsaXBib2FyZF09XCInQ0xJUEJPQVJELkNMSUNLX1RPX0NPUFknXCJcclxuICAgICAgICAgICAgICAgICAgICBbY2xpcGJvYXJkLW5vdGlmaWNhdGlvbl09XCInQ0xJUEJPQVJELlNVQ0NFU1NfQ09QWSdcIj57eyB2YWx1ZSQgfCBhc3luYyB8IGpzb24gfX08L3ByZT5cclxuICAgICAgICAgICAgPC9zcGFuPlxyXG4gICAgICAgIDwvbmctY29udGFpbmVyPlxyXG4gICAgICAgIDxuZy10ZW1wbGF0ZSAjZGVmYXVsdEpzb25UZW1wbGF0ZT5cclxuICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJhZGYtZGF0YXRhYmxlLWNlbGwtdmFsdWVcIj5cclxuICAgICAgICAgICAgICAgIDxwcmUgY2xhc3M9XCJhZGYtZGF0YXRhYmxlLWpzb24tY2VsbFwiPnt7IHZhbHVlJCB8IGFzeW5jIHwganNvbiB9fTwvcHJlPlxyXG4gICAgICAgICAgICA8L3NwYW4+XHJcbiAgICAgICAgPC9uZy10ZW1wbGF0ZT5cclxuICAgIGAsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9qc29uLWNlbGwuY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXHJcbiAgICBob3N0OiB7IGNsYXNzOiAnYWRmLWRhdGF0YWJsZS1jb250ZW50LWNlbGwnIH1cclxufSlcclxuZXhwb3J0IGNsYXNzIEpzb25DZWxsQ29tcG9uZW50IGV4dGVuZHMgRGF0YVRhYmxlQ2VsbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgIC8qKiBFbmFibGVzL2Rpc2FibGVzIGEgQ2xpcGJvYXJkIGRpcmVjdGl2ZSB0byBhbGxvdyBjb3B5aW5nIG9mIHRoZSBjZWxsJ3MgY29udGVudC4gKi9cclxuICAgICBASW5wdXQoKVxyXG4gICAgIGNvcHlDb250ZW50OiBib29sZWFuO1xyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvbHVtbiAmJiB0aGlzLmNvbHVtbi5rZXkgJiYgdGhpcy5yb3cgJiYgdGhpcy5kYXRhKSB7XHJcbiAgICAgICAgICAgIHRoaXMudmFsdWUkLm5leHQodGhpcy5kYXRhLmdldFZhbHVlKHRoaXMucm93LCB0aGlzLmNvbHVtbikpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=