/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, Component, Input, ViewEncapsulation } from '@angular/core';
import { DataTableCellComponent } from './datatable-cell.component';
import { AlfrescoApiService } from '../../../services/alfresco-api.service';
var LocationCellComponent = /** @class */ (function (_super) {
    tslib_1.__extends(LocationCellComponent, _super);
    function LocationCellComponent(alfrescoApiService) {
        return _super.call(this, alfrescoApiService) || this;
    }
    /** @override */
    /**
     * @override
     * @return {?}
     */
    LocationCellComponent.prototype.ngOnInit = /**
     * @override
     * @return {?}
     */
    function () {
        if (this.column && this.column.key && this.row && this.data) {
            /** @type {?} */
            var path = this.data.getValue(this.row, this.column);
            if (path && path.name && path.elements) {
                this.value$.next(path.name.split('/').pop());
                if (!this.tooltip) {
                    this.tooltip = path.name;
                }
                /** @type {?} */
                var parent_1 = path.elements[path.elements.length - 1];
                this.link = [this.column.format, parent_1.id];
            }
        }
    };
    LocationCellComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-location-cell',
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    template: "\n        <ng-container>\n            <a href=\"\" [title]=\"tooltip\" [routerLink]=\"link\">\n                {{ value$ | async }}\n            </a>\n        </ng-container>\n    ",
                    encapsulation: ViewEncapsulation.None,
                    host: { class: 'adf-location-cell adf-datatable-content-cell' }
                }] }
    ];
    /** @nocollapse */
    LocationCellComponent.ctorParameters = function () { return [
        { type: AlfrescoApiService }
    ]; };
    LocationCellComponent.propDecorators = {
        link: [{ type: Input }]
    };
    return LocationCellComponent;
}(DataTableCellComponent));
export { LocationCellComponent };
if (false) {
    /** @type {?} */
    LocationCellComponent.prototype.link;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYXRpb24tY2VsbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJkYXRhdGFibGUvY29tcG9uZW50cy9kYXRhdGFibGUvbG9jYXRpb24tY2VsbC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFDSCx1QkFBdUIsRUFDdkIsU0FBUyxFQUNULEtBQUssRUFFTCxpQkFBaUIsRUFDcEIsTUFBTSxlQUFlLENBQUM7QUFFdkIsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDcEUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sd0NBQXdDLENBQUM7QUFFNUU7SUFhMkMsaURBQXNCO0lBSTdELCtCQUFZLGtCQUFzQztlQUM5QyxrQkFBTSxrQkFBa0IsQ0FBQztJQUM3QixDQUFDO0lBRUQsZ0JBQWdCOzs7OztJQUNoQix3Q0FBUTs7OztJQUFSO1FBQ0ksSUFBSSxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxJQUFJLElBQUksQ0FBQyxHQUFHLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTs7Z0JBQ25ELElBQUksR0FBbUIsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQzNDLElBQUksQ0FBQyxHQUFHLEVBQ1IsSUFBSSxDQUFDLE1BQU0sQ0FDZDtZQUVELElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDcEMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsR0FBRyxFQUFFLENBQUMsQ0FBQztnQkFFN0MsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7b0JBQ2YsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO2lCQUM1Qjs7b0JBRUssUUFBTSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO2dCQUN0RCxJQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsUUFBTSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQy9DO1NBQ0o7SUFDTCxDQUFDOztnQkF4Q0osU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxtQkFBbUI7b0JBQzdCLGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxRQUFRLEVBQUUsc0xBTVQ7b0JBQ0QsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7b0JBQ3JDLElBQUksRUFBRSxFQUFFLEtBQUssRUFBRSw4Q0FBOEMsRUFBRTtpQkFDbEU7Ozs7Z0JBZFEsa0JBQWtCOzs7dUJBZ0J0QixLQUFLOztJQTJCViw0QkFBQztDQUFBLEFBekNELENBYTJDLHNCQUFzQixHQTRCaEU7U0E1QlkscUJBQXFCOzs7SUFDOUIscUNBQ1kiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHtcclxuICAgIENoYW5nZURldGVjdGlvblN0cmF0ZWd5LFxyXG4gICAgQ29tcG9uZW50LFxyXG4gICAgSW5wdXQsXHJcbiAgICBPbkluaXQsXHJcbiAgICBWaWV3RW5jYXBzdWxhdGlvblxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBQYXRoSW5mb0VudGl0eSB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5pbXBvcnQgeyBEYXRhVGFibGVDZWxsQ29tcG9uZW50IH0gZnJvbSAnLi9kYXRhdGFibGUtY2VsbC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLWxvY2F0aW9uLWNlbGwnLFxyXG4gICAgY2hhbmdlRGV0ZWN0aW9uOiBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneS5PblB1c2gsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgIDxuZy1jb250YWluZXI+XHJcbiAgICAgICAgICAgIDxhIGhyZWY9XCJcIiBbdGl0bGVdPVwidG9vbHRpcFwiIFtyb3V0ZXJMaW5rXT1cImxpbmtcIj5cclxuICAgICAgICAgICAgICAgIHt7IHZhbHVlJCB8IGFzeW5jIH19XHJcbiAgICAgICAgICAgIDwvYT5cclxuICAgICAgICA8L25nLWNvbnRhaW5lcj5cclxuICAgIGAsXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gICAgaG9zdDogeyBjbGFzczogJ2FkZi1sb2NhdGlvbi1jZWxsIGFkZi1kYXRhdGFibGUtY29udGVudC1jZWxsJyB9XHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMb2NhdGlvbkNlbGxDb21wb25lbnQgZXh0ZW5kcyBEYXRhVGFibGVDZWxsQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIEBJbnB1dCgpXHJcbiAgICBsaW5rOiBhbnlbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihhbGZyZXNjb0FwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSkge1xyXG4gICAgICAgIHN1cGVyKGFsZnJlc2NvQXBpU2VydmljZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqIEBvdmVycmlkZSAqL1xyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY29sdW1uICYmIHRoaXMuY29sdW1uLmtleSAmJiB0aGlzLnJvdyAmJiB0aGlzLmRhdGEpIHtcclxuICAgICAgICAgICAgY29uc3QgcGF0aDogUGF0aEluZm9FbnRpdHkgPSB0aGlzLmRhdGEuZ2V0VmFsdWUoXHJcbiAgICAgICAgICAgICAgICB0aGlzLnJvdyxcclxuICAgICAgICAgICAgICAgIHRoaXMuY29sdW1uXHJcbiAgICAgICAgICAgICk7XHJcblxyXG4gICAgICAgICAgICBpZiAocGF0aCAmJiBwYXRoLm5hbWUgJiYgcGF0aC5lbGVtZW50cykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy52YWx1ZSQubmV4dChwYXRoLm5hbWUuc3BsaXQoJy8nKS5wb3AoKSk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKCF0aGlzLnRvb2x0aXApIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnRvb2x0aXAgPSBwYXRoLm5hbWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgY29uc3QgcGFyZW50ID0gcGF0aC5lbGVtZW50c1twYXRoLmVsZW1lbnRzLmxlbmd0aCAtIDFdO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5saW5rID0gW3RoaXMuY29sdW1uLmZvcm1hdCwgcGFyZW50LmlkXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=