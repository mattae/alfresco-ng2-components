/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @record
 */
export function DataRow() { }
if (false) {
    /** @type {?} */
    DataRow.prototype.isSelected;
    /** @type {?|undefined} */
    DataRow.prototype.isDropTarget;
    /** @type {?|undefined} */
    DataRow.prototype.cssClass;
    /**
     * @param {?} key
     * @return {?}
     */
    DataRow.prototype.hasValue = function (key) { };
    /**
     * @param {?} key
     * @return {?}
     */
    DataRow.prototype.getValue = function (key) { };
    /**
     * @param {?} event
     * @return {?}
     */
    DataRow.prototype.imageErrorResolver = function (event) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS1yb3cubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJkYXRhdGFibGUvZGF0YS9kYXRhLXJvdy5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSw2QkFVQzs7O0lBVEcsNkJBQW9COztJQUNwQiwrQkFBdUI7O0lBQ3ZCLDJCQUFrQjs7Ozs7SUFFbEIsZ0RBQStCOzs7OztJQUUvQixnREFBMkI7Ozs7O0lBRTNCLDREQUF1QyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIERhdGFSb3cge1xyXG4gICAgaXNTZWxlY3RlZDogYm9vbGVhbjtcclxuICAgIGlzRHJvcFRhcmdldD86IGJvb2xlYW47XHJcbiAgICBjc3NDbGFzcz86IHN0cmluZztcclxuXHJcbiAgICBoYXNWYWx1ZShrZXk6IHN0cmluZyk6IGJvb2xlYW47XHJcblxyXG4gICAgZ2V0VmFsdWUoa2V5OiBzdHJpbmcpOiBhbnk7XHJcblxyXG4gICAgaW1hZ2VFcnJvclJlc29sdmVyPyhldmVudDogRXZlbnQpOiBhbnk7XHJcbn1cclxuIl19