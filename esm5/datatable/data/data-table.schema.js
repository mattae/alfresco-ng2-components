/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ContentChild, Input } from '@angular/core';
import { DataColumnListComponent } from '../../data-column/data-column-list.component';
import { ObjectDataColumn } from './object-datacolumn.model';
/**
 * @abstract
 */
var DataTableSchema = /** @class */ (function () {
    function DataTableSchema(appConfigService, presetKey, presetsModel) {
        this.appConfigService = appConfigService;
        this.presetKey = presetKey;
        this.presetsModel = presetsModel;
        this.layoutPresets = {};
    }
    /**
     * @return {?}
     */
    DataTableSchema.prototype.createDatatableSchema = /**
     * @return {?}
     */
    function () {
        this.loadLayoutPresets();
        if (!this.columns || this.columns.length === 0) {
            this.columns = this.mergeJsonAndHtmlSchema();
        }
    };
    /**
     * @return {?}
     */
    DataTableSchema.prototype.loadLayoutPresets = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var externalSettings = this.appConfigService.get(this.presetKey, null);
        if (externalSettings) {
            this.layoutPresets = Object.assign({}, this.presetsModel, externalSettings);
        }
        else {
            this.layoutPresets = this.presetsModel;
        }
    };
    /**
     * @return {?}
     */
    DataTableSchema.prototype.mergeJsonAndHtmlSchema = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var customSchemaColumns = this.getSchemaFromConfig(this.presetColumn).concat(this.getSchemaFromHtml(this.columnList));
        if (customSchemaColumns.length === 0) {
            customSchemaColumns = this.getDefaultLayoutPreset();
        }
        return customSchemaColumns;
    };
    /**
     * @param {?} columnList
     * @return {?}
     */
    DataTableSchema.prototype.getSchemaFromHtml = /**
     * @param {?} columnList
     * @return {?}
     */
    function (columnList) {
        /** @type {?} */
        var schema = [];
        if (columnList && columnList.columns && columnList.columns.length > 0) {
            schema = columnList.columns.map((/**
             * @param {?} c
             * @return {?}
             */
            function (c) { return (/** @type {?} */ (c)); }));
        }
        return schema;
    };
    /**
     * @param {?} presetColumn
     * @return {?}
     */
    DataTableSchema.prototype.getSchemaFromConfig = /**
     * @param {?} presetColumn
     * @return {?}
     */
    function (presetColumn) {
        return presetColumn ? (this.layoutPresets[presetColumn]).map((/**
         * @param {?} col
         * @return {?}
         */
        function (col) { return new ObjectDataColumn(col); })) : [];
    };
    /**
     * @private
     * @return {?}
     */
    DataTableSchema.prototype.getDefaultLayoutPreset = /**
     * @private
     * @return {?}
     */
    function () {
        return (this.layoutPresets['default']).map((/**
         * @param {?} col
         * @return {?}
         */
        function (col) { return new ObjectDataColumn(col); }));
    };
    DataTableSchema.propDecorators = {
        columnList: [{ type: ContentChild, args: [DataColumnListComponent, { static: true },] }],
        presetColumn: [{ type: Input }]
    };
    return DataTableSchema;
}());
export { DataTableSchema };
if (false) {
    /** @type {?} */
    DataTableSchema.prototype.columnList;
    /**
     * Custom preset column schema in JSON format.
     * @type {?}
     */
    DataTableSchema.prototype.presetColumn;
    /** @type {?} */
    DataTableSchema.prototype.columns;
    /**
     * @type {?}
     * @private
     */
    DataTableSchema.prototype.layoutPresets;
    /**
     * @type {?}
     * @private
     */
    DataTableSchema.prototype.appConfigService;
    /**
     * @type {?}
     * @protected
     */
    DataTableSchema.prototype.presetKey;
    /**
     * @type {?}
     * @protected
     */
    DataTableSchema.prototype.presetsModel;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YS10YWJsZS5zY2hlbWEuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJkYXRhdGFibGUvZGF0YS9kYXRhLXRhYmxlLnNjaGVtYS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQ0EsT0FBTyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFcEQsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFFdkYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sMkJBQTJCLENBQUM7Ozs7QUFFN0Q7SUFZSSx5QkFBb0IsZ0JBQWtDLEVBQ2hDLFNBQWlCLEVBQ2pCLFlBQWlCO1FBRm5CLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDaEMsY0FBUyxHQUFULFNBQVMsQ0FBUTtRQUNqQixpQkFBWSxHQUFaLFlBQVksQ0FBSztRQUovQixrQkFBYSxHQUFHLEVBQUUsQ0FBQztJQUlnQixDQUFDOzs7O0lBRXJDLCtDQUFxQjs7O0lBQTVCO1FBQ0ksSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7UUFDekIsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQzVDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7U0FDaEQ7SUFDTCxDQUFDOzs7O0lBRU0sMkNBQWlCOzs7SUFBeEI7O1lBQ1UsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQztRQUN4RSxJQUFJLGdCQUFnQixFQUFFO1lBQ2xCLElBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxnQkFBZ0IsQ0FBQyxDQUFDO1NBQy9FO2FBQU07WUFDSCxJQUFJLENBQUMsYUFBYSxHQUFHLElBQUksQ0FBQyxZQUFZLENBQUM7U0FDMUM7SUFDTCxDQUFDOzs7O0lBRU0sZ0RBQXNCOzs7SUFBN0I7O1lBQ1EsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUNySCxJQUFJLG1CQUFtQixDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDbEMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7U0FDdkQ7UUFDRCxPQUFPLG1CQUFtQixDQUFDO0lBQy9CLENBQUM7Ozs7O0lBRU0sMkNBQWlCOzs7O0lBQXhCLFVBQXlCLFVBQW1DOztZQUNwRCxNQUFNLEdBQUcsRUFBRTtRQUNmLElBQUksVUFBVSxJQUFJLFVBQVUsQ0FBQyxPQUFPLElBQUksVUFBVSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ25FLE1BQU0sR0FBRyxVQUFVLENBQUMsT0FBTyxDQUFDLEdBQUc7Ozs7WUFBQyxVQUFDLENBQUMsV0FBSyxtQkFBYSxDQUFDLEVBQUEsR0FBQSxFQUFDLENBQUM7U0FDMUQ7UUFDRCxPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDOzs7OztJQUVLLDZDQUFtQjs7OztJQUExQixVQUEyQixZQUFvQjtRQUMxQyxPQUFPLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsR0FBRzs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsSUFBSSxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsRUFBekIsQ0FBeUIsRUFBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLENBQUM7SUFDMUcsQ0FBQzs7Ozs7SUFFTyxnREFBc0I7Ozs7SUFBOUI7UUFDSSxPQUFPLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLENBQUMsQ0FBQyxDQUFDLEdBQUc7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLElBQUksZ0JBQWdCLENBQUMsR0FBRyxDQUFDLEVBQXpCLENBQXlCLEVBQUMsQ0FBQztJQUNuRixDQUFDOzs2QkFwREEsWUFBWSxTQUFDLHVCQUF1QixFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQzsrQkFHcEQsS0FBSzs7SUFrRFYsc0JBQUM7Q0FBQSxBQXZERCxJQXVEQztTQXZEcUIsZUFBZTs7O0lBRWpDLHFDQUEyRjs7Ozs7SUFHM0YsdUNBQ3FCOztJQUVyQixrQ0FBYTs7Ozs7SUFFYix3Q0FBMkI7Ozs7O0lBRWYsMkNBQTBDOzs7OztJQUMxQyxvQ0FBMkI7Ozs7O0lBQzNCLHVDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxuXG4vKiFcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxuICpcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cbiAqL1xuXG5pbXBvcnQgeyBDb250ZW50Q2hpbGQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBBcHBDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vYXBwLWNvbmZpZy9hcHAtY29uZmlnLnNlcnZpY2UnO1xuaW1wb3J0IHsgRGF0YUNvbHVtbkxpc3RDb21wb25lbnQgfSBmcm9tICcuLi8uLi9kYXRhLWNvbHVtbi9kYXRhLWNvbHVtbi1saXN0LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBEYXRhQ29sdW1uIH0gZnJvbSAnLi9kYXRhLWNvbHVtbi5tb2RlbCc7XG5pbXBvcnQgeyBPYmplY3REYXRhQ29sdW1uIH0gZnJvbSAnLi9vYmplY3QtZGF0YWNvbHVtbi5tb2RlbCc7XG5cbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBEYXRhVGFibGVTY2hlbWEge1xuXG4gICAgQENvbnRlbnRDaGlsZChEYXRhQ29sdW1uTGlzdENvbXBvbmVudCwge3N0YXRpYzogdHJ1ZX0pIGNvbHVtbkxpc3Q6IERhdGFDb2x1bW5MaXN0Q29tcG9uZW50O1xuXG4gICAgLyoqIEN1c3RvbSBwcmVzZXQgY29sdW1uIHNjaGVtYSBpbiBKU09OIGZvcm1hdC4gKi9cbiAgICBASW5wdXQoKVxuICAgIHByZXNldENvbHVtbjogc3RyaW5nO1xuXG4gICAgY29sdW1uczogYW55O1xuXG4gICAgcHJpdmF0ZSBsYXlvdXRQcmVzZXRzID0ge307XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFwcENvbmZpZ1NlcnZpY2U6IEFwcENvbmZpZ1NlcnZpY2UsXG4gICAgICAgICAgICAgICAgcHJvdGVjdGVkIHByZXNldEtleTogc3RyaW5nLFxuICAgICAgICAgICAgICAgIHByb3RlY3RlZCBwcmVzZXRzTW9kZWw6IGFueSkgeyB9XG5cbiAgICBwdWJsaWMgY3JlYXRlRGF0YXRhYmxlU2NoZW1hKCk6IHZvaWQge1xuICAgICAgICB0aGlzLmxvYWRMYXlvdXRQcmVzZXRzKCk7XG4gICAgICAgIGlmICghdGhpcy5jb2x1bW5zIHx8IHRoaXMuY29sdW1ucy5sZW5ndGggPT09IDApIHtcbiAgICAgICAgICAgIHRoaXMuY29sdW1ucyA9IHRoaXMubWVyZ2VKc29uQW5kSHRtbFNjaGVtYSgpO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHVibGljIGxvYWRMYXlvdXRQcmVzZXRzKCk6IHZvaWQge1xuICAgICAgICBjb25zdCBleHRlcm5hbFNldHRpbmdzID0gdGhpcy5hcHBDb25maWdTZXJ2aWNlLmdldCh0aGlzLnByZXNldEtleSwgbnVsbCk7XG4gICAgICAgIGlmIChleHRlcm5hbFNldHRpbmdzKSB7XG4gICAgICAgICAgICB0aGlzLmxheW91dFByZXNldHMgPSBPYmplY3QuYXNzaWduKHt9LCB0aGlzLnByZXNldHNNb2RlbCwgZXh0ZXJuYWxTZXR0aW5ncyk7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLmxheW91dFByZXNldHMgPSB0aGlzLnByZXNldHNNb2RlbDtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHB1YmxpYyBtZXJnZUpzb25BbmRIdG1sU2NoZW1hKCk6IGFueSB7XG4gICAgICAgIGxldCBjdXN0b21TY2hlbWFDb2x1bW5zID0gdGhpcy5nZXRTY2hlbWFGcm9tQ29uZmlnKHRoaXMucHJlc2V0Q29sdW1uKS5jb25jYXQodGhpcy5nZXRTY2hlbWFGcm9tSHRtbCh0aGlzLmNvbHVtbkxpc3QpKTtcbiAgICAgICAgaWYgKGN1c3RvbVNjaGVtYUNvbHVtbnMubGVuZ3RoID09PSAwKSB7XG4gICAgICAgICAgICBjdXN0b21TY2hlbWFDb2x1bW5zID0gdGhpcy5nZXREZWZhdWx0TGF5b3V0UHJlc2V0KCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIGN1c3RvbVNjaGVtYUNvbHVtbnM7XG4gICAgfVxuXG4gICAgcHVibGljIGdldFNjaGVtYUZyb21IdG1sKGNvbHVtbkxpc3Q6IERhdGFDb2x1bW5MaXN0Q29tcG9uZW50KTogYW55IHtcbiAgICAgICAgbGV0IHNjaGVtYSA9IFtdO1xuICAgICAgICBpZiAoY29sdW1uTGlzdCAmJiBjb2x1bW5MaXN0LmNvbHVtbnMgJiYgY29sdW1uTGlzdC5jb2x1bW5zLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIHNjaGVtYSA9IGNvbHVtbkxpc3QuY29sdW1ucy5tYXAoKGMpID0+IDxEYXRhQ29sdW1uPiBjKTtcbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gc2NoZW1hO1xuICAgIH1cblxuICAgcHVibGljIGdldFNjaGVtYUZyb21Db25maWcocHJlc2V0Q29sdW1uOiBzdHJpbmcpOiBEYXRhQ29sdW1uW10ge1xuICAgICAgICByZXR1cm4gcHJlc2V0Q29sdW1uID8gKHRoaXMubGF5b3V0UHJlc2V0c1twcmVzZXRDb2x1bW5dKS5tYXAoKGNvbCkgPT4gbmV3IE9iamVjdERhdGFDb2x1bW4oY29sKSkgOiBbXTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldERlZmF1bHRMYXlvdXRQcmVzZXQoKTogRGF0YUNvbHVtbltdIHtcbiAgICAgICAgcmV0dXJuICh0aGlzLmxheW91dFByZXNldHNbJ2RlZmF1bHQnXSkubWFwKChjb2wpID0+IG5ldyBPYmplY3REYXRhQ29sdW1uKGNvbCkpO1xuICAgIH1cbn1cbiJdfQ==