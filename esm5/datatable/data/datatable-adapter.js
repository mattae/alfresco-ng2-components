/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @record
 */
export function DataTableAdapter() { }
if (false) {
    /** @type {?|undefined} */
    DataTableAdapter.prototype.rowsChanged;
    /** @type {?} */
    DataTableAdapter.prototype.selectedRow;
    /**
     * @return {?}
     */
    DataTableAdapter.prototype.getRows = function () { };
    /**
     * @param {?} rows
     * @return {?}
     */
    DataTableAdapter.prototype.setRows = function (rows) { };
    /**
     * @return {?}
     */
    DataTableAdapter.prototype.getColumns = function () { };
    /**
     * @param {?} columns
     * @return {?}
     */
    DataTableAdapter.prototype.setColumns = function (columns) { };
    /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    DataTableAdapter.prototype.getValue = function (row, col) { };
    /**
     * @return {?}
     */
    DataTableAdapter.prototype.getSorting = function () { };
    /**
     * @param {?} sorting
     * @return {?}
     */
    DataTableAdapter.prototype.setSorting = function (sorting) { };
    /**
     * @param {?=} key
     * @param {?=} direction
     * @return {?}
     */
    DataTableAdapter.prototype.sort = function (key, direction) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0YXRhYmxlLWFkYXB0ZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJkYXRhdGFibGUvZGF0YS9kYXRhdGFibGUtYWRhcHRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXNCQSxzQ0FZQzs7O0lBWEcsdUNBQXNDOztJQUV0Qyx1Q0FBcUI7Ozs7SUFDckIscURBQTBCOzs7OztJQUMxQix5REFBb0M7Ozs7SUFDcEMsd0RBQWdDOzs7OztJQUNoQywrREFBNkM7Ozs7OztJQUM3Qyw4REFBNkM7Ozs7SUFDN0Msd0RBQTBCOzs7OztJQUMxQiwrREFBdUM7Ozs7OztJQUN2QyxnRUFBNkMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgRGF0YUNvbHVtbiB9IGZyb20gJy4vZGF0YS1jb2x1bW4ubW9kZWwnO1xyXG5pbXBvcnQgeyBEYXRhUm93IH0gZnJvbSAnLi9kYXRhLXJvdy5tb2RlbCc7XHJcbmltcG9ydCB7IERhdGFTb3J0aW5nIH0gZnJvbSAnLi9kYXRhLXNvcnRpbmcubW9kZWwnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIERhdGFUYWJsZUFkYXB0ZXIge1xyXG4gICAgcm93c0NoYW5nZWQ/OiBTdWJqZWN0PEFycmF5PERhdGFSb3c+PjtcclxuXHJcbiAgICBzZWxlY3RlZFJvdzogRGF0YVJvdztcclxuICAgIGdldFJvd3MoKTogQXJyYXk8RGF0YVJvdz47XHJcbiAgICBzZXRSb3dzKHJvd3M6IEFycmF5PERhdGFSb3c+KTogdm9pZDtcclxuICAgIGdldENvbHVtbnMoKTogQXJyYXk8RGF0YUNvbHVtbj47XHJcbiAgICBzZXRDb2x1bW5zKGNvbHVtbnM6IEFycmF5PERhdGFDb2x1bW4+KTogdm9pZDtcclxuICAgIGdldFZhbHVlKHJvdzogRGF0YVJvdywgY29sOiBEYXRhQ29sdW1uKTogYW55O1xyXG4gICAgZ2V0U29ydGluZygpOiBEYXRhU29ydGluZztcclxuICAgIHNldFNvcnRpbmcoc29ydGluZzogRGF0YVNvcnRpbmcpOiB2b2lkO1xyXG4gICAgc29ydChrZXk/OiBzdHJpbmcsIGRpcmVjdGlvbj86IHN0cmluZyk6IHZvaWQ7XHJcbn1cclxuIl19