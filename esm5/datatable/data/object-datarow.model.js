/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ObjectUtils } from '../../utils';
// Simple implementation of the DataRow interface.
var 
// Simple implementation of the DataRow interface.
ObjectDataRow = /** @class */ (function () {
    function ObjectDataRow(obj, isSelected) {
        if (isSelected === void 0) { isSelected = false; }
        this.obj = obj;
        this.isSelected = isSelected;
        if (!obj) {
            throw new Error('Object source not found');
        }
    }
    /**
     * @param {?} key
     * @return {?}
     */
    ObjectDataRow.prototype.getValue = /**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        return ObjectUtils.getValue(this.obj, key);
    };
    /**
     * @param {?} key
     * @return {?}
     */
    ObjectDataRow.prototype.hasValue = /**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        return this.getValue(key) !== undefined;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ObjectDataRow.prototype.imageErrorResolver = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        return '';
    };
    return ObjectDataRow;
}());
// Simple implementation of the DataRow interface.
export { ObjectDataRow };
if (false) {
    /**
     * @type {?}
     * @private
     */
    ObjectDataRow.prototype.obj;
    /** @type {?} */
    ObjectDataRow.prototype.isSelected;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2JqZWN0LWRhdGFyb3cubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJkYXRhdGFibGUvZGF0YS9vYmplY3QtZGF0YXJvdy5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sYUFBYSxDQUFDOztBQUkxQzs7O0lBRUksdUJBQW9CLEdBQVEsRUFBUyxVQUEyQjtRQUEzQiwyQkFBQSxFQUFBLGtCQUEyQjtRQUE1QyxRQUFHLEdBQUgsR0FBRyxDQUFLO1FBQVMsZUFBVSxHQUFWLFVBQVUsQ0FBaUI7UUFDNUQsSUFBSSxDQUFDLEdBQUcsRUFBRTtZQUNOLE1BQU0sSUFBSSxLQUFLLENBQUMseUJBQXlCLENBQUMsQ0FBQztTQUM5QztJQUVMLENBQUM7Ozs7O0lBRUQsZ0NBQVE7Ozs7SUFBUixVQUFTLEdBQVc7UUFDaEIsT0FBTyxXQUFXLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDL0MsQ0FBQzs7Ozs7SUFFRCxnQ0FBUTs7OztJQUFSLFVBQVMsR0FBVztRQUNoQixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEtBQUssU0FBUyxDQUFDO0lBQzVDLENBQUM7Ozs7O0lBRUQsMENBQWtCOzs7O0lBQWxCLFVBQW1CLEtBQVk7UUFDM0IsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDO0lBQ0wsb0JBQUM7QUFBRCxDQUFDLEFBcEJELElBb0JDOzs7Ozs7OztJQWxCZSw0QkFBZ0I7O0lBQUUsbUNBQWtDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IE9iamVjdFV0aWxzIH0gZnJvbSAnLi4vLi4vdXRpbHMnO1xyXG5pbXBvcnQgeyBEYXRhUm93IH0gZnJvbSAnLi9kYXRhLXJvdy5tb2RlbCc7XHJcblxyXG4vLyBTaW1wbGUgaW1wbGVtZW50YXRpb24gb2YgdGhlIERhdGFSb3cgaW50ZXJmYWNlLlxyXG5leHBvcnQgY2xhc3MgT2JqZWN0RGF0YVJvdyBpbXBsZW1lbnRzIERhdGFSb3cge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgb2JqOiBhbnksIHB1YmxpYyBpc1NlbGVjdGVkOiBib29sZWFuID0gZmFsc2UpIHtcclxuICAgICAgICBpZiAoIW9iaikge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ09iamVjdCBzb3VyY2Ugbm90IGZvdW5kJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBnZXRWYWx1ZShrZXk6IHN0cmluZyk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIE9iamVjdFV0aWxzLmdldFZhbHVlKHRoaXMub2JqLCBrZXkpO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc1ZhbHVlKGtleTogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0VmFsdWUoa2V5KSAhPT0gdW5kZWZpbmVkO1xyXG4gICAgfVxyXG5cclxuICAgIGltYWdlRXJyb3JSZXNvbHZlcihldmVudDogRXZlbnQpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiAnJztcclxuICAgIH1cclxufVxyXG4iXX0=