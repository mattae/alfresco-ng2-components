/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ObjectDataRow } from './object-datarow.model';
import { ObjectDataColumn } from './object-datacolumn.model';
import { DataSorting } from './data-sorting.model';
import { Subject } from 'rxjs';
// Simple implementation of the DataTableAdapter interface.
var 
// Simple implementation of the DataTableAdapter interface.
ObjectDataTableAdapter = /** @class */ (function () {
    function ObjectDataTableAdapter(data, schema) {
        if (data === void 0) { data = []; }
        if (schema === void 0) { schema = []; }
        this._rows = [];
        this._columns = [];
        if (data && data.length > 0) {
            this._rows = data.map((/**
             * @param {?} item
             * @return {?}
             */
            function (item) {
                return new ObjectDataRow(item);
            }));
        }
        if (schema && schema.length > 0) {
            this._columns = schema.map((/**
             * @param {?} item
             * @return {?}
             */
            function (item) {
                return new ObjectDataColumn(item);
            }));
            // Sort by first sortable or just first column
            /** @type {?} */
            var sortable = this._columns.filter((/**
             * @param {?} column
             * @return {?}
             */
            function (column) { return column.sortable; }));
            if (sortable.length > 0) {
                this.sort(sortable[0].key, 'asc');
            }
        }
        this.rowsChanged = new Subject();
    }
    /**
     * @param {?} data
     * @return {?}
     */
    ObjectDataTableAdapter.generateSchema = /**
     * @param {?} data
     * @return {?}
     */
    function (data) {
        /** @type {?} */
        var schema = [];
        if (data && data.length) {
            /** @type {?} */
            var rowToExaminate = data[0];
            if (typeof rowToExaminate === 'object') {
                for (var key in rowToExaminate) {
                    if (rowToExaminate.hasOwnProperty(key)) {
                        schema.push({
                            type: 'text',
                            key: key,
                            title: key,
                            sortable: false
                        });
                    }
                }
            }
        }
        return schema;
    };
    /**
     * @return {?}
     */
    ObjectDataTableAdapter.prototype.getRows = /**
     * @return {?}
     */
    function () {
        return this._rows;
    };
    /**
     * @param {?} rows
     * @return {?}
     */
    ObjectDataTableAdapter.prototype.setRows = /**
     * @param {?} rows
     * @return {?}
     */
    function (rows) {
        this._rows = rows || [];
        this.sort();
        this.rowsChanged.next(this._rows);
    };
    /**
     * @return {?}
     */
    ObjectDataTableAdapter.prototype.getColumns = /**
     * @return {?}
     */
    function () {
        return this._columns;
    };
    /**
     * @param {?} columns
     * @return {?}
     */
    ObjectDataTableAdapter.prototype.setColumns = /**
     * @param {?} columns
     * @return {?}
     */
    function (columns) {
        this._columns = columns || [];
    };
    /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    ObjectDataTableAdapter.prototype.getValue = /**
     * @param {?} row
     * @param {?} col
     * @return {?}
     */
    function (row, col) {
        if (!row) {
            throw new Error('Row not found');
        }
        if (!col) {
            throw new Error('Column not found');
        }
        /** @type {?} */
        var value = row.getValue(col.key);
        if (col.type === 'icon') {
            /** @type {?} */
            var icon = row.getValue(col.key);
            return icon;
        }
        return value;
    };
    /**
     * @return {?}
     */
    ObjectDataTableAdapter.prototype.getSorting = /**
     * @return {?}
     */
    function () {
        return this._sorting;
    };
    /**
     * @param {?} sorting
     * @return {?}
     */
    ObjectDataTableAdapter.prototype.setSorting = /**
     * @param {?} sorting
     * @return {?}
     */
    function (sorting) {
        this._sorting = sorting;
        if (sorting && sorting.key) {
            this._rows.sort((/**
             * @param {?} a
             * @param {?} b
             * @return {?}
             */
            function (a, b) {
                /** @type {?} */
                var left = a.getValue(sorting.key);
                if (left) {
                    left = (left instanceof Date) ? left.valueOf().toString() : left.toString();
                }
                else {
                    left = '';
                }
                /** @type {?} */
                var right = b.getValue(sorting.key);
                if (right) {
                    right = (right instanceof Date) ? right.valueOf().toString() : right.toString();
                }
                else {
                    right = '';
                }
                return sorting.direction === 'asc'
                    ? left.localeCompare(right)
                    : right.localeCompare(left);
            }));
        }
    };
    /**
     * @param {?=} key
     * @param {?=} direction
     * @return {?}
     */
    ObjectDataTableAdapter.prototype.sort = /**
     * @param {?=} key
     * @param {?=} direction
     * @return {?}
     */
    function (key, direction) {
        /** @type {?} */
        var sorting = this._sorting || new DataSorting();
        if (key) {
            sorting.key = key;
            sorting.direction = direction || 'asc';
        }
        this.setSorting(sorting);
    };
    return ObjectDataTableAdapter;
}());
// Simple implementation of the DataTableAdapter interface.
export { ObjectDataTableAdapter };
if (false) {
    /**
     * @type {?}
     * @private
     */
    ObjectDataTableAdapter.prototype._sorting;
    /**
     * @type {?}
     * @private
     */
    ObjectDataTableAdapter.prototype._rows;
    /**
     * @type {?}
     * @private
     */
    ObjectDataTableAdapter.prototype._columns;
    /** @type {?} */
    ObjectDataTableAdapter.prototype.selectedRow;
    /** @type {?} */
    ObjectDataTableAdapter.prototype.rowsChanged;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2JqZWN0LWRhdGF0YWJsZS1hZGFwdGVyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZGF0YXRhYmxlL2RhdGEvb2JqZWN0LWRhdGF0YWJsZS1hZGFwdGVyLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUN2RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUM3RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFFbkQsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQzs7QUFHL0I7OztJQWdDSSxnQ0FBWSxJQUFnQixFQUFFLE1BQXlCO1FBQTNDLHFCQUFBLEVBQUEsU0FBZ0I7UUFBRSx1QkFBQSxFQUFBLFdBQXlCO1FBQ25ELElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBRW5CLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQ3pCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUc7Ozs7WUFBQyxVQUFDLElBQUk7Z0JBQ3ZCLE9BQU8sSUFBSSxhQUFhLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDbkMsQ0FBQyxFQUFDLENBQUM7U0FDTjtRQUVELElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzdCLElBQUksQ0FBQyxRQUFRLEdBQUcsTUFBTSxDQUFDLEdBQUc7Ozs7WUFBQyxVQUFDLElBQUk7Z0JBQzVCLE9BQU8sSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0QyxDQUFDLEVBQUMsQ0FBQzs7O2dCQUdHLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU07Ozs7WUFBQyxVQUFDLE1BQU0sSUFBSyxPQUFBLE1BQU0sQ0FBQyxRQUFRLEVBQWYsQ0FBZSxFQUFDO1lBQ2xFLElBQUksUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3JCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsRUFBRSxLQUFLLENBQUMsQ0FBQzthQUNyQztTQUNKO1FBRUQsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLE9BQU8sRUFBa0IsQ0FBQztJQUNyRCxDQUFDOzs7OztJQTlDTSxxQ0FBYzs7OztJQUFyQixVQUFzQixJQUFXOztZQUN2QixNQUFNLEdBQUcsRUFBRTtRQUVqQixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFOztnQkFDZixjQUFjLEdBQUcsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUU5QixJQUFJLE9BQU8sY0FBYyxLQUFLLFFBQVEsRUFBRTtnQkFDcEMsS0FBSyxJQUFNLEdBQUcsSUFBSSxjQUFjLEVBQUU7b0JBQzlCLElBQUksY0FBYyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsRUFBRTt3QkFDcEMsTUFBTSxDQUFDLElBQUksQ0FBQzs0QkFDUixJQUFJLEVBQUUsTUFBTTs0QkFDWixHQUFHLEVBQUUsR0FBRzs0QkFDUixLQUFLLEVBQUUsR0FBRzs0QkFDVixRQUFRLEVBQUUsS0FBSzt5QkFDbEIsQ0FBQyxDQUFDO3FCQUNOO2lCQUNKO2FBQ0o7U0FFSjtRQUNELE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7Ozs7SUEyQkQsd0NBQU87OztJQUFQO1FBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBRUQsd0NBQU87Ozs7SUFBUCxVQUFRLElBQW9CO1FBQ3hCLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDWixJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7OztJQUVELDJDQUFVOzs7SUFBVjtRQUNJLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDOzs7OztJQUVELDJDQUFVOzs7O0lBQVYsVUFBVyxPQUEwQjtRQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLE9BQU8sSUFBSSxFQUFFLENBQUM7SUFDbEMsQ0FBQzs7Ozs7O0lBRUQseUNBQVE7Ozs7O0lBQVIsVUFBUyxHQUFZLEVBQUUsR0FBZTtRQUNsQyxJQUFJLENBQUMsR0FBRyxFQUFFO1lBQ04sTUFBTSxJQUFJLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUNwQztRQUNELElBQUksQ0FBQyxHQUFHLEVBQUU7WUFDTixNQUFNLElBQUksS0FBSyxDQUFDLGtCQUFrQixDQUFDLENBQUM7U0FDdkM7O1lBRUssS0FBSyxHQUFHLEdBQUcsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEdBQUcsQ0FBQztRQUVuQyxJQUFJLEdBQUcsQ0FBQyxJQUFJLEtBQUssTUFBTSxFQUFFOztnQkFDZixJQUFJLEdBQUcsR0FBRyxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDO1lBQ2xDLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFFRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDOzs7O0lBRUQsMkNBQVU7OztJQUFWO1FBQ0ksT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO0lBQ3pCLENBQUM7Ozs7O0lBRUQsMkNBQVU7Ozs7SUFBVixVQUFXLE9BQW9CO1FBQzNCLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO1FBRXhCLElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxHQUFHLEVBQUU7WUFDeEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJOzs7OztZQUFDLFVBQUMsQ0FBVSxFQUFFLENBQVU7O29CQUMvQixJQUFJLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDO2dCQUNsQyxJQUFJLElBQUksRUFBRTtvQkFDTixJQUFJLEdBQUcsQ0FBQyxJQUFJLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUMvRTtxQkFBTTtvQkFDSCxJQUFJLEdBQUcsRUFBRSxDQUFDO2lCQUNiOztvQkFFRyxLQUFLLEdBQUcsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDO2dCQUNuQyxJQUFJLEtBQUssRUFBRTtvQkFDUCxLQUFLLEdBQUcsQ0FBQyxLQUFLLFlBQVksSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUNuRjtxQkFBTTtvQkFDSCxLQUFLLEdBQUcsRUFBRSxDQUFDO2lCQUNkO2dCQUVELE9BQU8sT0FBTyxDQUFDLFNBQVMsS0FBSyxLQUFLO29CQUM5QixDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLENBQUM7b0JBQzNCLENBQUMsQ0FBQyxLQUFLLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3BDLENBQUMsRUFBQyxDQUFDO1NBQ047SUFDTCxDQUFDOzs7Ozs7SUFFRCxxQ0FBSTs7Ozs7SUFBSixVQUFLLEdBQVksRUFBRSxTQUFrQjs7WUFDM0IsT0FBTyxHQUFHLElBQUksQ0FBQyxRQUFRLElBQUksSUFBSSxXQUFXLEVBQUU7UUFDbEQsSUFBSSxHQUFHLEVBQUU7WUFDTCxPQUFPLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQztZQUNsQixPQUFPLENBQUMsU0FBUyxHQUFHLFNBQVMsSUFBSSxLQUFLLENBQUM7U0FDMUM7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQzdCLENBQUM7SUFDTCw2QkFBQztBQUFELENBQUMsQUFuSUQsSUFtSUM7Ozs7Ozs7O0lBaklHLDBDQUE4Qjs7Ozs7SUFDOUIsdUNBQXlCOzs7OztJQUN6QiwwQ0FBK0I7O0lBRS9CLDZDQUFxQjs7SUFDckIsNkNBQXFDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IERhdGFDb2x1bW4gfSBmcm9tICcuL2RhdGEtY29sdW1uLm1vZGVsJztcclxuaW1wb3J0IHsgRGF0YVJvdyB9IGZyb20gJy4vZGF0YS1yb3cubW9kZWwnO1xyXG5pbXBvcnQgeyBPYmplY3REYXRhUm93IH0gZnJvbSAnLi9vYmplY3QtZGF0YXJvdy5tb2RlbCc7XHJcbmltcG9ydCB7IE9iamVjdERhdGFDb2x1bW4gfSBmcm9tICcuL29iamVjdC1kYXRhY29sdW1uLm1vZGVsJztcclxuaW1wb3J0IHsgRGF0YVNvcnRpbmcgfSBmcm9tICcuL2RhdGEtc29ydGluZy5tb2RlbCc7XHJcbmltcG9ydCB7IERhdGFUYWJsZUFkYXB0ZXIgfSBmcm9tICcuL2RhdGF0YWJsZS1hZGFwdGVyJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5cclxuLy8gU2ltcGxlIGltcGxlbWVudGF0aW9uIG9mIHRoZSBEYXRhVGFibGVBZGFwdGVyIGludGVyZmFjZS5cclxuZXhwb3J0IGNsYXNzIE9iamVjdERhdGFUYWJsZUFkYXB0ZXIgaW1wbGVtZW50cyBEYXRhVGFibGVBZGFwdGVyIHtcclxuXHJcbiAgICBwcml2YXRlIF9zb3J0aW5nOiBEYXRhU29ydGluZztcclxuICAgIHByaXZhdGUgX3Jvd3M6IERhdGFSb3dbXTtcclxuICAgIHByaXZhdGUgX2NvbHVtbnM6IERhdGFDb2x1bW5bXTtcclxuXHJcbiAgICBzZWxlY3RlZFJvdzogRGF0YVJvdztcclxuICAgIHJvd3NDaGFuZ2VkOiBTdWJqZWN0PEFycmF5PERhdGFSb3c+PjtcclxuXHJcbiAgICBzdGF0aWMgZ2VuZXJhdGVTY2hlbWEoZGF0YTogYW55W10pIHtcclxuICAgICAgICBjb25zdCBzY2hlbWEgPSBbXTtcclxuXHJcbiAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5sZW5ndGgpIHtcclxuICAgICAgICAgICAgY29uc3Qgcm93VG9FeGFtaW5hdGUgPSBkYXRhWzBdO1xyXG5cclxuICAgICAgICAgICAgaWYgKHR5cGVvZiByb3dUb0V4YW1pbmF0ZSA9PT0gJ29iamVjdCcpIHtcclxuICAgICAgICAgICAgICAgIGZvciAoY29uc3Qga2V5IGluIHJvd1RvRXhhbWluYXRlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJvd1RvRXhhbWluYXRlLmhhc093blByb3BlcnR5KGtleSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2NoZW1hLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3RleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAga2V5OiBrZXksXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aXRsZToga2V5LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgc29ydGFibGU6IGZhbHNlXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHNjaGVtYTtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvcihkYXRhOiBhbnlbXSA9IFtdLCBzY2hlbWE6IERhdGFDb2x1bW5bXSA9IFtdKSB7XHJcbiAgICAgICAgdGhpcy5fcm93cyA9IFtdO1xyXG4gICAgICAgIHRoaXMuX2NvbHVtbnMgPSBbXTtcclxuXHJcbiAgICAgICAgaWYgKGRhdGEgJiYgZGF0YS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3Jvd3MgPSBkYXRhLm1hcCgoaXRlbSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG5ldyBPYmplY3REYXRhUm93KGl0ZW0pO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChzY2hlbWEgJiYgc2NoZW1hLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgdGhpcy5fY29sdW1ucyA9IHNjaGVtYS5tYXAoKGl0ZW0pID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBuZXcgT2JqZWN0RGF0YUNvbHVtbihpdGVtKTtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICAvLyBTb3J0IGJ5IGZpcnN0IHNvcnRhYmxlIG9yIGp1c3QgZmlyc3QgY29sdW1uXHJcbiAgICAgICAgICAgIGNvbnN0IHNvcnRhYmxlID0gdGhpcy5fY29sdW1ucy5maWx0ZXIoKGNvbHVtbikgPT4gY29sdW1uLnNvcnRhYmxlKTtcclxuICAgICAgICAgICAgaWYgKHNvcnRhYmxlLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc29ydChzb3J0YWJsZVswXS5rZXksICdhc2MnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5yb3dzQ2hhbmdlZCA9IG5ldyBTdWJqZWN0PEFycmF5PERhdGFSb3c+PigpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFJvd3MoKTogQXJyYXk8RGF0YVJvdz4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9yb3dzO1xyXG4gICAgfVxyXG5cclxuICAgIHNldFJvd3Mocm93czogQXJyYXk8RGF0YVJvdz4pIHtcclxuICAgICAgICB0aGlzLl9yb3dzID0gcm93cyB8fCBbXTtcclxuICAgICAgICB0aGlzLnNvcnQoKTtcclxuICAgICAgICB0aGlzLnJvd3NDaGFuZ2VkLm5leHQodGhpcy5fcm93cyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q29sdW1ucygpOiBBcnJheTxEYXRhQ29sdW1uPiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2NvbHVtbnM7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0Q29sdW1ucyhjb2x1bW5zOiBBcnJheTxEYXRhQ29sdW1uPikge1xyXG4gICAgICAgIHRoaXMuX2NvbHVtbnMgPSBjb2x1bW5zIHx8IFtdO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFZhbHVlKHJvdzogRGF0YVJvdywgY29sOiBEYXRhQ29sdW1uKTogYW55IHtcclxuICAgICAgICBpZiAoIXJvdykge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ1JvdyBub3QgZm91bmQnKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCFjb2wpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdDb2x1bW4gbm90IGZvdW5kJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB2YWx1ZSA9IHJvdy5nZXRWYWx1ZShjb2wua2V5KTtcclxuXHJcbiAgICAgICAgaWYgKGNvbC50eXBlID09PSAnaWNvbicpIHtcclxuICAgICAgICAgICAgY29uc3QgaWNvbiA9IHJvdy5nZXRWYWx1ZShjb2wua2V5KTtcclxuICAgICAgICAgICAgcmV0dXJuIGljb247XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0U29ydGluZygpOiBEYXRhU29ydGluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3NvcnRpbmc7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0U29ydGluZyhzb3J0aW5nOiBEYXRhU29ydGluZyk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuX3NvcnRpbmcgPSBzb3J0aW5nO1xyXG5cclxuICAgICAgICBpZiAoc29ydGluZyAmJiBzb3J0aW5nLmtleSkge1xyXG4gICAgICAgICAgICB0aGlzLl9yb3dzLnNvcnQoKGE6IERhdGFSb3csIGI6IERhdGFSb3cpID0+IHtcclxuICAgICAgICAgICAgICAgIGxldCBsZWZ0ID0gYS5nZXRWYWx1ZShzb3J0aW5nLmtleSk7XHJcbiAgICAgICAgICAgICAgICBpZiAobGVmdCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGxlZnQgPSAobGVmdCBpbnN0YW5jZW9mIERhdGUpID8gbGVmdC52YWx1ZU9mKCkudG9TdHJpbmcoKSA6IGxlZnQudG9TdHJpbmcoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGVmdCA9ICcnO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGxldCByaWdodCA9IGIuZ2V0VmFsdWUoc29ydGluZy5rZXkpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHJpZ2h0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmlnaHQgPSAocmlnaHQgaW5zdGFuY2VvZiBEYXRlKSA/IHJpZ2h0LnZhbHVlT2YoKS50b1N0cmluZygpIDogcmlnaHQudG9TdHJpbmcoKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmlnaHQgPSAnJztcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gc29ydGluZy5kaXJlY3Rpb24gPT09ICdhc2MnXHJcbiAgICAgICAgICAgICAgICAgICAgPyBsZWZ0LmxvY2FsZUNvbXBhcmUocmlnaHQpXHJcbiAgICAgICAgICAgICAgICAgICAgOiByaWdodC5sb2NhbGVDb21wYXJlKGxlZnQpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc29ydChrZXk/OiBzdHJpbmcsIGRpcmVjdGlvbj86IHN0cmluZyk6IHZvaWQge1xyXG4gICAgICAgIGNvbnN0IHNvcnRpbmcgPSB0aGlzLl9zb3J0aW5nIHx8IG5ldyBEYXRhU29ydGluZygpO1xyXG4gICAgICAgIGlmIChrZXkpIHtcclxuICAgICAgICAgICAgc29ydGluZy5rZXkgPSBrZXk7XHJcbiAgICAgICAgICAgIHNvcnRpbmcuZGlyZWN0aW9uID0gZGlyZWN0aW9uIHx8ICdhc2MnO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNldFNvcnRpbmcoc29ydGluZyk7XHJcbiAgICB9XHJcbn1cclxuIl19