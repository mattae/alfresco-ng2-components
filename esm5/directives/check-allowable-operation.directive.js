/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:no-input-rename  */
import { ChangeDetectorRef, Directive, ElementRef, Host, Inject, Input, Optional, Renderer2 } from '@angular/core';
import { ContentService } from './../services/content.service';
import { EXTENDIBLE_COMPONENT } from './../interface/injection.tokens';
/**
 * @record
 */
export function NodeAllowableOperationSubject() { }
if (false) {
    /** @type {?} */
    NodeAllowableOperationSubject.prototype.disabled;
}
var CheckAllowableOperationDirective = /** @class */ (function () {
    function CheckAllowableOperationDirective(elementRef, renderer, contentService, changeDetector, parentComponent) {
        this.elementRef = elementRef;
        this.renderer = renderer;
        this.contentService = contentService;
        this.changeDetector = changeDetector;
        this.parentComponent = parentComponent;
        /**
         * Node permission to check (create, delete, update, updatePermissions,
         * !create, !delete, !update, !updatePermissions).
         */
        this.permission = null;
        /**
         * Nodes to check permission for.
         */
        this.nodes = [];
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    CheckAllowableOperationDirective.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes.nodes && !changes.nodes.firstChange) {
            this.updateElement();
        }
    };
    /**
     * Updates disabled state for the decorated element
     *
     * @memberof CheckAllowableOperationDirective
     */
    /**
     * Updates disabled state for the decorated element
     *
     * \@memberof CheckAllowableOperationDirective
     * @return {?}
     */
    CheckAllowableOperationDirective.prototype.updateElement = /**
     * Updates disabled state for the decorated element
     *
     * \@memberof CheckAllowableOperationDirective
     * @return {?}
     */
    function () {
        /** @type {?} */
        var enable = this.hasAllowableOperations(this.nodes, this.permission);
        if (enable) {
            this.enable();
        }
        else {
            this.disable();
        }
        return enable;
    };
    /**
     * @private
     * @return {?}
     */
    CheckAllowableOperationDirective.prototype.enable = /**
     * @private
     * @return {?}
     */
    function () {
        if (this.parentComponent) {
            this.parentComponent.disabled = false;
            this.changeDetector.detectChanges();
        }
        else {
            this.enableElement();
        }
    };
    /**
     * @private
     * @return {?}
     */
    CheckAllowableOperationDirective.prototype.disable = /**
     * @private
     * @return {?}
     */
    function () {
        if (this.parentComponent) {
            this.parentComponent.disabled = true;
            this.changeDetector.detectChanges();
        }
        else {
            this.disableElement();
        }
    };
    /**
     * Enables decorated element
     *
     * @memberof CheckAllowableOperationDirective
     */
    /**
     * Enables decorated element
     *
     * \@memberof CheckAllowableOperationDirective
     * @return {?}
     */
    CheckAllowableOperationDirective.prototype.enableElement = /**
     * Enables decorated element
     *
     * \@memberof CheckAllowableOperationDirective
     * @return {?}
     */
    function () {
        this.renderer.removeAttribute(this.elementRef.nativeElement, 'disabled');
    };
    /**
     * Disables decorated element
     *
     * @memberof CheckAllowableOperationDirective
     */
    /**
     * Disables decorated element
     *
     * \@memberof CheckAllowableOperationDirective
     * @return {?}
     */
    CheckAllowableOperationDirective.prototype.disableElement = /**
     * Disables decorated element
     *
     * \@memberof CheckAllowableOperationDirective
     * @return {?}
     */
    function () {
        this.renderer.setAttribute(this.elementRef.nativeElement, 'disabled', 'true');
    };
    /**
     * Checks whether all nodes have a particular permission
     *
     * @param  nodes Node collection to check
     * @param  permission Permission to check for each node
     * @memberof CheckAllowableOperationDirective
     */
    /**
     * Checks whether all nodes have a particular permission
     *
     * \@memberof CheckAllowableOperationDirective
     * @param {?} nodes Node collection to check
     * @param {?} permission Permission to check for each node
     * @return {?}
     */
    CheckAllowableOperationDirective.prototype.hasAllowableOperations = /**
     * Checks whether all nodes have a particular permission
     *
     * \@memberof CheckAllowableOperationDirective
     * @param {?} nodes Node collection to check
     * @param {?} permission Permission to check for each node
     * @return {?}
     */
    function (nodes, permission) {
        var _this = this;
        if (nodes && nodes.length > 0) {
            return nodes.every((/**
             * @param {?} node
             * @return {?}
             */
            function (node) { return _this.contentService.hasAllowableOperations(node.entry, permission); }));
        }
        return false;
    };
    CheckAllowableOperationDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[adf-check-allowable-operation]'
                },] }
    ];
    /** @nocollapse */
    CheckAllowableOperationDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: Renderer2 },
        { type: ContentService },
        { type: ChangeDetectorRef },
        { type: undefined, decorators: [{ type: Host }, { type: Optional }, { type: Inject, args: [EXTENDIBLE_COMPONENT,] }] }
    ]; };
    CheckAllowableOperationDirective.propDecorators = {
        permission: [{ type: Input, args: ['adf-check-allowable-operation',] }],
        nodes: [{ type: Input, args: ['adf-nodes',] }]
    };
    return CheckAllowableOperationDirective;
}());
export { CheckAllowableOperationDirective };
if (false) {
    /**
     * Node permission to check (create, delete, update, updatePermissions,
     * !create, !delete, !update, !updatePermissions).
     * @type {?}
     */
    CheckAllowableOperationDirective.prototype.permission;
    /**
     * Nodes to check permission for.
     * @type {?}
     */
    CheckAllowableOperationDirective.prototype.nodes;
    /**
     * @type {?}
     * @private
     */
    CheckAllowableOperationDirective.prototype.elementRef;
    /**
     * @type {?}
     * @private
     */
    CheckAllowableOperationDirective.prototype.renderer;
    /**
     * @type {?}
     * @private
     */
    CheckAllowableOperationDirective.prototype.contentService;
    /**
     * @type {?}
     * @private
     */
    CheckAllowableOperationDirective.prototype.changeDetector;
    /**
     * @type {?}
     * @private
     */
    CheckAllowableOperationDirective.prototype.parentComponent;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY2hlY2stYWxsb3dhYmxlLW9wZXJhdGlvbi5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJkaXJlY3RpdmVzL2NoZWNrLWFsbG93YWJsZS1vcGVyYXRpb24uZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFFLEtBQUssRUFBYSxRQUFRLEVBQUUsU0FBUyxFQUFrQixNQUFNLGVBQWUsQ0FBQztBQUU5SSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDL0QsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0saUNBQWlDLENBQUM7Ozs7QUFFdkUsbURBRUM7OztJQURHLGlEQUFrQjs7QUFHdEI7SUFlSSwwQ0FBb0IsVUFBc0IsRUFDdEIsUUFBbUIsRUFDbkIsY0FBOEIsRUFDOUIsY0FBaUMsRUFJSCxlQUErQztRQVA3RSxlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDbkIsbUJBQWMsR0FBZCxjQUFjLENBQWdCO1FBQzlCLG1CQUFjLEdBQWQsY0FBYyxDQUFtQjtRQUlILG9CQUFlLEdBQWYsZUFBZSxDQUFnQzs7Ozs7UUFiakcsZUFBVSxHQUFZLElBQUksQ0FBQzs7OztRQUkzQixVQUFLLEdBQWdCLEVBQUUsQ0FBQztJQVV4QixDQUFDOzs7OztJQUVELHNEQUFXOzs7O0lBQVgsVUFBWSxPQUFzQjtRQUM5QixJQUFJLE9BQU8sQ0FBQyxLQUFLLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLFdBQVcsRUFBRTtZQUM3QyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7U0FDeEI7SUFDTCxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7OztJQUNILHdEQUFhOzs7Ozs7SUFBYjs7WUFDVSxNQUFNLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQztRQUV2RSxJQUFJLE1BQU0sRUFBRTtZQUNSLElBQUksQ0FBQyxNQUFNLEVBQUUsQ0FBQztTQUNqQjthQUFNO1lBQ0gsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO1NBQ2xCO1FBRUQsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQzs7Ozs7SUFFTyxpREFBTTs7OztJQUFkO1FBQ0ksSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3RCLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztZQUN0QyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsRUFBRSxDQUFDO1NBQ3ZDO2FBQU07WUFDSCxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7U0FDeEI7SUFDTCxDQUFDOzs7OztJQUVPLGtEQUFPOzs7O0lBQWY7UUFDSSxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDdEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3JDLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxFQUFFLENBQUM7U0FDdkM7YUFBTTtZQUNILElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUN6QjtJQUNMLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsd0RBQWE7Ozs7OztJQUFiO1FBQ0ksSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLEVBQUUsVUFBVSxDQUFDLENBQUM7SUFDN0UsQ0FBQztJQUVEOzs7O09BSUc7Ozs7Ozs7SUFDSCx5REFBYzs7Ozs7O0lBQWQ7UUFDSSxJQUFJLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDbEYsQ0FBQztJQUVEOzs7Ozs7T0FNRzs7Ozs7Ozs7O0lBQ0gsaUVBQXNCOzs7Ozs7OztJQUF0QixVQUF1QixLQUFrQixFQUFFLFVBQWtCO1FBQTdELGlCQU1DO1FBTEcsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDM0IsT0FBTyxLQUFLLENBQUMsS0FBSzs7OztZQUFDLFVBQUMsSUFBSSxJQUFLLE9BQUEsS0FBSSxDQUFDLGNBQWMsQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxFQUFsRSxDQUFrRSxFQUFDLENBQUM7U0FDcEc7UUFFRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDOztnQkFqR0osU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxpQ0FBaUM7aUJBQzlDOzs7O2dCQVhzQyxVQUFVO2dCQUE0QyxTQUFTO2dCQUU3RixjQUFjO2dCQUZkLGlCQUFpQjtnREE2QlQsSUFBSSxZQUNKLFFBQVEsWUFDUixNQUFNLFNBQUMsb0JBQW9COzs7NkJBZHZDLEtBQUssU0FBQywrQkFBK0I7d0JBSXJDLEtBQUssU0FBQyxXQUFXOztJQXNGdEIsdUNBQUM7Q0FBQSxBQWxHRCxJQWtHQztTQS9GWSxnQ0FBZ0M7Ozs7Ozs7SUFLekMsc0RBQzJCOzs7OztJQUczQixpREFDd0I7Ozs7O0lBRVosc0RBQThCOzs7OztJQUM5QixvREFBMkI7Ozs7O0lBQzNCLDBEQUFzQzs7Ozs7SUFDdEMsMERBQXlDOzs7OztJQUV6QywyREFFcUYiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6bm8taW5wdXQtcmVuYW1lICAqL1xyXG5cclxuaW1wb3J0IHsgQ2hhbmdlRGV0ZWN0b3JSZWYsIERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgSG9zdCwgSW5qZWN0LCBJbnB1dCwgT25DaGFuZ2VzLCBPcHRpb25hbCwgUmVuZGVyZXIyLCAgU2ltcGxlQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOb2RlRW50cnkgfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuaW1wb3J0IHsgQ29udGVudFNlcnZpY2UgfSBmcm9tICcuLy4uL3NlcnZpY2VzL2NvbnRlbnQuc2VydmljZSc7XHJcbmltcG9ydCB7IEVYVEVORElCTEVfQ09NUE9ORU5UIH0gZnJvbSAnLi8uLi9pbnRlcmZhY2UvaW5qZWN0aW9uLnRva2Vucyc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIE5vZGVBbGxvd2FibGVPcGVyYXRpb25TdWJqZWN0IHtcclxuICAgIGRpc2FibGVkOiBib29sZWFuO1xyXG59XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW2FkZi1jaGVjay1hbGxvd2FibGUtb3BlcmF0aW9uXSdcclxufSlcclxuZXhwb3J0IGNsYXNzIENoZWNrQWxsb3dhYmxlT3BlcmF0aW9uRGlyZWN0aXZlIGltcGxlbWVudHMgT25DaGFuZ2VzIHtcclxuXHJcbiAgICAvKiogTm9kZSBwZXJtaXNzaW9uIHRvIGNoZWNrIChjcmVhdGUsIGRlbGV0ZSwgdXBkYXRlLCB1cGRhdGVQZXJtaXNzaW9ucyxcclxuICAgICAqICFjcmVhdGUsICFkZWxldGUsICF1cGRhdGUsICF1cGRhdGVQZXJtaXNzaW9ucykuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgnYWRmLWNoZWNrLWFsbG93YWJsZS1vcGVyYXRpb24nKVxyXG4gICAgcGVybWlzc2lvbjogc3RyaW5nICA9IG51bGw7XHJcblxyXG4gICAgLyoqIE5vZGVzIHRvIGNoZWNrIHBlcm1pc3Npb24gZm9yLiAqL1xyXG4gICAgQElucHV0KCdhZGYtbm9kZXMnKVxyXG4gICAgbm9kZXM6IE5vZGVFbnRyeVtdID0gW107XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBlbGVtZW50UmVmOiBFbGVtZW50UmVmLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSByZW5kZXJlcjogUmVuZGVyZXIyLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBjb250ZW50U2VydmljZTogQ29udGVudFNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGNoYW5nZURldGVjdG9yOiBDaGFuZ2VEZXRlY3RvclJlZixcclxuXHJcbiAgICAgICAgICAgICAgICBASG9zdCgpXHJcbiAgICAgICAgICAgICAgICBAT3B0aW9uYWwoKVxyXG4gICAgICAgICAgICAgICAgQEluamVjdChFWFRFTkRJQkxFX0NPTVBPTkVOVCkgcHJpdmF0ZSBwYXJlbnRDb21wb25lbnQ/OiBOb2RlQWxsb3dhYmxlT3BlcmF0aW9uU3ViamVjdCkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcclxuICAgICAgICBpZiAoY2hhbmdlcy5ub2RlcyAmJiAhY2hhbmdlcy5ub2Rlcy5maXJzdENoYW5nZSkge1xyXG4gICAgICAgICAgICB0aGlzLnVwZGF0ZUVsZW1lbnQoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBVcGRhdGVzIGRpc2FibGVkIHN0YXRlIGZvciB0aGUgZGVjb3JhdGVkIGVsZW1lbnRcclxuICAgICAqXHJcbiAgICAgKiBAbWVtYmVyb2YgQ2hlY2tBbGxvd2FibGVPcGVyYXRpb25EaXJlY3RpdmVcclxuICAgICAqL1xyXG4gICAgdXBkYXRlRWxlbWVudCgpOiBib29sZWFuIHtcclxuICAgICAgICBjb25zdCBlbmFibGUgPSB0aGlzLmhhc0FsbG93YWJsZU9wZXJhdGlvbnModGhpcy5ub2RlcywgdGhpcy5wZXJtaXNzaW9uKTtcclxuXHJcbiAgICAgICAgaWYgKGVuYWJsZSkge1xyXG4gICAgICAgICAgICB0aGlzLmVuYWJsZSgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzYWJsZSgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGVuYWJsZTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGVuYWJsZSgpOiB2b2lkIHtcclxuICAgICAgICBpZiAodGhpcy5wYXJlbnRDb21wb25lbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5wYXJlbnRDb21wb25lbnQuZGlzYWJsZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5jaGFuZ2VEZXRlY3Rvci5kZXRlY3RDaGFuZ2VzKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5lbmFibGVFbGVtZW50KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZGlzYWJsZSgpOiB2b2lkIHtcclxuICAgICAgICBpZiAodGhpcy5wYXJlbnRDb21wb25lbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5wYXJlbnRDb21wb25lbnQuZGlzYWJsZWQgPSB0cnVlO1xyXG4gICAgICAgICAgICB0aGlzLmNoYW5nZURldGVjdG9yLmRldGVjdENoYW5nZXMoKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmRpc2FibGVFbGVtZW50KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRW5hYmxlcyBkZWNvcmF0ZWQgZWxlbWVudFxyXG4gICAgICpcclxuICAgICAqIEBtZW1iZXJvZiBDaGVja0FsbG93YWJsZU9wZXJhdGlvbkRpcmVjdGl2ZVxyXG4gICAgICovXHJcbiAgICBlbmFibGVFbGVtZW50KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMucmVuZGVyZXIucmVtb3ZlQXR0cmlidXRlKHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCAnZGlzYWJsZWQnKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERpc2FibGVzIGRlY29yYXRlZCBlbGVtZW50XHJcbiAgICAgKlxyXG4gICAgICogQG1lbWJlcm9mIENoZWNrQWxsb3dhYmxlT3BlcmF0aW9uRGlyZWN0aXZlXHJcbiAgICAgKi9cclxuICAgIGRpc2FibGVFbGVtZW50KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMucmVuZGVyZXIuc2V0QXR0cmlidXRlKHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LCAnZGlzYWJsZWQnLCAndHJ1ZScpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2tzIHdoZXRoZXIgYWxsIG5vZGVzIGhhdmUgYSBwYXJ0aWN1bGFyIHBlcm1pc3Npb25cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gIG5vZGVzIE5vZGUgY29sbGVjdGlvbiB0byBjaGVja1xyXG4gICAgICogQHBhcmFtICBwZXJtaXNzaW9uIFBlcm1pc3Npb24gdG8gY2hlY2sgZm9yIGVhY2ggbm9kZVxyXG4gICAgICogQG1lbWJlcm9mIENoZWNrQWxsb3dhYmxlT3BlcmF0aW9uRGlyZWN0aXZlXHJcbiAgICAgKi9cclxuICAgIGhhc0FsbG93YWJsZU9wZXJhdGlvbnMobm9kZXM6IE5vZGVFbnRyeVtdLCBwZXJtaXNzaW9uOiBzdHJpbmcpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAobm9kZXMgJiYgbm9kZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICByZXR1cm4gbm9kZXMuZXZlcnkoKG5vZGUpID0+IHRoaXMuY29udGVudFNlcnZpY2UuaGFzQWxsb3dhYmxlT3BlcmF0aW9ucyhub2RlLmVudHJ5LCBwZXJtaXNzaW9uKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbn1cclxuIl19