/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../material.module';
import { HighlightDirective } from './highlight.directive';
import { LogoutDirective } from './logout.directive';
import { NodeDeleteDirective } from './node-delete.directive';
import { NodeFavoriteDirective } from './node-favorite.directive';
import { CheckAllowableOperationDirective } from './check-allowable-operation.directive';
import { NodeRestoreDirective } from './node-restore.directive';
import { UploadDirective } from './upload.directive';
import { NodeDownloadDirective } from './node-download.directive';
var DirectiveModule = /** @class */ (function () {
    function DirectiveModule() {
    }
    DirectiveModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        MaterialModule
                    ],
                    declarations: [
                        HighlightDirective,
                        LogoutDirective,
                        NodeDeleteDirective,
                        NodeFavoriteDirective,
                        CheckAllowableOperationDirective,
                        NodeRestoreDirective,
                        NodeDownloadDirective,
                        UploadDirective
                    ],
                    exports: [
                        HighlightDirective,
                        LogoutDirective,
                        NodeDeleteDirective,
                        NodeFavoriteDirective,
                        CheckAllowableOperationDirective,
                        NodeRestoreDirective,
                        NodeDownloadDirective,
                        UploadDirective
                    ]
                },] }
    ];
    return DirectiveModule;
}());
export { DirectiveModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlyZWN0aXZlLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvZGlyZWN0aXZlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsWUFBWSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDL0MsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6QyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFFcEQsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDM0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLHlCQUF5QixDQUFDO0FBQzlELE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxnQ0FBZ0MsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3pGLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ2hFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNyRCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSwyQkFBMkIsQ0FBQztBQUVsRTtJQUFBO0lBMEI4QixDQUFDOztnQkExQjlCLFFBQVEsU0FBQztvQkFDTixPQUFPLEVBQUU7d0JBQ0wsWUFBWTt3QkFDWixjQUFjO3FCQUNqQjtvQkFDRCxZQUFZLEVBQUU7d0JBQ1Ysa0JBQWtCO3dCQUNsQixlQUFlO3dCQUNmLG1CQUFtQjt3QkFDbkIscUJBQXFCO3dCQUNyQixnQ0FBZ0M7d0JBQ2hDLG9CQUFvQjt3QkFDcEIscUJBQXFCO3dCQUNyQixlQUFlO3FCQUNsQjtvQkFDRCxPQUFPLEVBQUU7d0JBQ0wsa0JBQWtCO3dCQUNsQixlQUFlO3dCQUNmLG1CQUFtQjt3QkFDbkIscUJBQXFCO3dCQUNyQixnQ0FBZ0M7d0JBQ2hDLG9CQUFvQjt3QkFDcEIscUJBQXFCO3dCQUNyQixlQUFlO3FCQUNsQjtpQkFDSjs7SUFDNkIsc0JBQUM7Q0FBQSxBQTFCL0IsSUEwQitCO1NBQWxCLGVBQWUiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ29tbW9uTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0ZXJpYWxNb2R1bGUgfSBmcm9tICcuLi9tYXRlcmlhbC5tb2R1bGUnO1xyXG5cclxuaW1wb3J0IHsgSGlnaGxpZ2h0RGlyZWN0aXZlIH0gZnJvbSAnLi9oaWdobGlnaHQuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgTG9nb3V0RGlyZWN0aXZlIH0gZnJvbSAnLi9sb2dvdXQuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgTm9kZURlbGV0ZURpcmVjdGl2ZSB9IGZyb20gJy4vbm9kZS1kZWxldGUuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgTm9kZUZhdm9yaXRlRGlyZWN0aXZlIH0gZnJvbSAnLi9ub2RlLWZhdm9yaXRlLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IENoZWNrQWxsb3dhYmxlT3BlcmF0aW9uRGlyZWN0aXZlIH0gZnJvbSAnLi9jaGVjay1hbGxvd2FibGUtb3BlcmF0aW9uLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IE5vZGVSZXN0b3JlRGlyZWN0aXZlIH0gZnJvbSAnLi9ub2RlLXJlc3RvcmUuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgVXBsb2FkRGlyZWN0aXZlIH0gZnJvbSAnLi91cGxvYWQuZGlyZWN0aXZlJztcclxuaW1wb3J0IHsgTm9kZURvd25sb2FkRGlyZWN0aXZlIH0gZnJvbSAnLi9ub2RlLWRvd25sb2FkLmRpcmVjdGl2ZSc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICBNYXRlcmlhbE1vZHVsZVxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIEhpZ2hsaWdodERpcmVjdGl2ZSxcclxuICAgICAgICBMb2dvdXREaXJlY3RpdmUsXHJcbiAgICAgICAgTm9kZURlbGV0ZURpcmVjdGl2ZSxcclxuICAgICAgICBOb2RlRmF2b3JpdGVEaXJlY3RpdmUsXHJcbiAgICAgICAgQ2hlY2tBbGxvd2FibGVPcGVyYXRpb25EaXJlY3RpdmUsXHJcbiAgICAgICAgTm9kZVJlc3RvcmVEaXJlY3RpdmUsXHJcbiAgICAgICAgTm9kZURvd25sb2FkRGlyZWN0aXZlLFxyXG4gICAgICAgIFVwbG9hZERpcmVjdGl2ZVxyXG4gICAgXSxcclxuICAgIGV4cG9ydHM6IFtcclxuICAgICAgICBIaWdobGlnaHREaXJlY3RpdmUsXHJcbiAgICAgICAgTG9nb3V0RGlyZWN0aXZlLFxyXG4gICAgICAgIE5vZGVEZWxldGVEaXJlY3RpdmUsXHJcbiAgICAgICAgTm9kZUZhdm9yaXRlRGlyZWN0aXZlLFxyXG4gICAgICAgIENoZWNrQWxsb3dhYmxlT3BlcmF0aW9uRGlyZWN0aXZlLFxyXG4gICAgICAgIE5vZGVSZXN0b3JlRGlyZWN0aXZlLFxyXG4gICAgICAgIE5vZGVEb3dubG9hZERpcmVjdGl2ZSxcclxuICAgICAgICBVcGxvYWREaXJlY3RpdmVcclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIERpcmVjdGl2ZU1vZHVsZSB7fVxyXG4iXX0=