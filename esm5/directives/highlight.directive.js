/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:no-input-rename  */
import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';
import { HighlightTransformService } from '../services/highlight-transform.service';
var HighlightDirective = /** @class */ (function () {
    function HighlightDirective(el, renderer, highlightTransformService) {
        this.el = el;
        this.renderer = renderer;
        this.highlightTransformService = highlightTransformService;
        /**
         * Class selector for highlightable elements.
         */
        this.selector = '';
        /**
         * Text to highlight.
         */
        this.search = '';
        /**
         * CSS class used to apply highlighting.
         */
        this.classToApply = 'adf-highlight';
    }
    /**
     * @return {?}
     */
    HighlightDirective.prototype.ngAfterViewChecked = /**
     * @return {?}
     */
    function () {
        this.highlight();
    };
    /**
     * @param {?=} search
     * @param {?=} selector
     * @param {?=} classToApply
     * @return {?}
     */
    HighlightDirective.prototype.highlight = /**
     * @param {?=} search
     * @param {?=} selector
     * @param {?=} classToApply
     * @return {?}
     */
    function (search, selector, classToApply) {
        var _this = this;
        if (search === void 0) { search = this.search; }
        if (selector === void 0) { selector = this.selector; }
        if (classToApply === void 0) { classToApply = this.classToApply; }
        if (search && selector) {
            /** @type {?} */
            var elements = this.el.nativeElement.querySelectorAll(selector);
            elements.forEach((/**
             * @param {?} element
             * @return {?}
             */
            function (element) {
                /** @type {?} */
                var highlightTransformResult = _this.highlightTransformService.highlight(element.innerHTML, search, classToApply);
                if (highlightTransformResult.changed) {
                    _this.renderer.setProperty(element, 'innerHTML', highlightTransformResult.text);
                }
            }));
        }
    };
    HighlightDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[adf-highlight]'
                },] }
    ];
    /** @nocollapse */
    HighlightDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: Renderer2 },
        { type: HighlightTransformService }
    ]; };
    HighlightDirective.propDecorators = {
        selector: [{ type: Input, args: ['adf-highlight-selector',] }],
        search: [{ type: Input, args: ['adf-highlight',] }],
        classToApply: [{ type: Input, args: ['adf-highlight-class',] }]
    };
    return HighlightDirective;
}());
export { HighlightDirective };
if (false) {
    /**
     * Class selector for highlightable elements.
     * @type {?}
     */
    HighlightDirective.prototype.selector;
    /**
     * Text to highlight.
     * @type {?}
     */
    HighlightDirective.prototype.search;
    /**
     * CSS class used to apply highlighting.
     * @type {?}
     */
    HighlightDirective.prototype.classToApply;
    /**
     * @type {?}
     * @private
     */
    HighlightDirective.prototype.el;
    /**
     * @type {?}
     * @private
     */
    HighlightDirective.prototype.renderer;
    /**
     * @type {?}
     * @private
     */
    HighlightDirective.prototype.highlightTransformService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGlnaGxpZ2h0LmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvaGlnaGxpZ2h0LmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBb0IsTUFBTSxlQUFlLENBQUM7QUFDMUYsT0FBTyxFQUFFLHlCQUF5QixFQUE0QixNQUFNLHlDQUF5QyxDQUFDO0FBRTlHO0lBaUJJLDRCQUNZLEVBQWMsRUFDZCxRQUFtQixFQUNuQix5QkFBb0Q7UUFGcEQsT0FBRSxHQUFGLEVBQUUsQ0FBWTtRQUNkLGFBQVEsR0FBUixRQUFRLENBQVc7UUFDbkIsOEJBQXlCLEdBQXpCLHlCQUF5QixDQUEyQjs7OztRQWJoRSxhQUFRLEdBQVcsRUFBRSxDQUFDOzs7O1FBSXRCLFdBQU0sR0FBVyxFQUFFLENBQUM7Ozs7UUFJcEIsaUJBQVksR0FBVyxlQUFlLENBQUM7SUFNdkMsQ0FBQzs7OztJQUVELCtDQUFrQjs7O0lBQWxCO1FBQ0ksSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ3JCLENBQUM7Ozs7Ozs7SUFFTSxzQ0FBUzs7Ozs7O0lBQWhCLFVBQWlCLE1BQW9CLEVBQUUsUUFBd0IsRUFBRSxZQUFnQztRQUFqRyxpQkFXQztRQVhnQix1QkFBQSxFQUFBLFNBQVMsSUFBSSxDQUFDLE1BQU07UUFBRSx5QkFBQSxFQUFBLFdBQVcsSUFBSSxDQUFDLFFBQVE7UUFBRSw2QkFBQSxFQUFBLGVBQWUsSUFBSSxDQUFDLFlBQVk7UUFDN0YsSUFBSSxNQUFNLElBQUksUUFBUSxFQUFFOztnQkFDZCxRQUFRLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxDQUFDO1lBRWpFLFFBQVEsQ0FBQyxPQUFPOzs7O1lBQUMsVUFBQyxPQUFPOztvQkFDZix3QkFBd0IsR0FBNkIsS0FBSSxDQUFDLHlCQUF5QixDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFLE1BQU0sRUFBRSxZQUFZLENBQUM7Z0JBQzVJLElBQUksd0JBQXdCLENBQUMsT0FBTyxFQUFFO29CQUNsQyxLQUFJLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsV0FBVyxFQUFFLHdCQUF3QixDQUFDLElBQUksQ0FBQyxDQUFDO2lCQUNsRjtZQUNMLENBQUMsRUFBQyxDQUFDO1NBQ047SUFDTCxDQUFDOztnQkF0Q0osU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxpQkFBaUI7aUJBQzlCOzs7O2dCQUxtQixVQUFVO2dCQUFTLFNBQVM7Z0JBQ3ZDLHlCQUF5Qjs7OzJCQVE3QixLQUFLLFNBQUMsd0JBQXdCO3lCQUk5QixLQUFLLFNBQUMsZUFBZTsrQkFJckIsS0FBSyxTQUFDLHFCQUFxQjs7SUF5QmhDLHlCQUFDO0NBQUEsQUF2Q0QsSUF1Q0M7U0FwQ1ksa0JBQWtCOzs7Ozs7SUFHM0Isc0NBQ3NCOzs7OztJQUd0QixvQ0FDb0I7Ozs7O0lBR3BCLDBDQUN1Qzs7Ozs7SUFHbkMsZ0NBQXNCOzs7OztJQUN0QixzQ0FBMkI7Ozs7O0lBQzNCLHVEQUE0RCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4vKiB0c2xpbnQ6ZGlzYWJsZTpuby1pbnB1dC1yZW5hbWUgICovXHJcblxyXG5pbXBvcnQgeyBEaXJlY3RpdmUsIEVsZW1lbnRSZWYsIElucHV0LCBSZW5kZXJlcjIsIEFmdGVyVmlld0NoZWNrZWQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgSGlnaGxpZ2h0VHJhbnNmb3JtU2VydmljZSwgSGlnaGxpZ2h0VHJhbnNmb3JtUmVzdWx0IH0gZnJvbSAnLi4vc2VydmljZXMvaGlnaGxpZ2h0LXRyYW5zZm9ybS5zZXJ2aWNlJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbYWRmLWhpZ2hsaWdodF0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBIaWdobGlnaHREaXJlY3RpdmUgaW1wbGVtZW50cyBBZnRlclZpZXdDaGVja2VkIHtcclxuXHJcbiAgICAvKiogQ2xhc3Mgc2VsZWN0b3IgZm9yIGhpZ2hsaWdodGFibGUgZWxlbWVudHMuICovXHJcbiAgICBASW5wdXQoJ2FkZi1oaWdobGlnaHQtc2VsZWN0b3InKVxyXG4gICAgc2VsZWN0b3I6IHN0cmluZyA9ICcnO1xyXG5cclxuICAgIC8qKiBUZXh0IHRvIGhpZ2hsaWdodC4gKi9cclxuICAgIEBJbnB1dCgnYWRmLWhpZ2hsaWdodCcpXHJcbiAgICBzZWFyY2g6IHN0cmluZyA9ICcnO1xyXG5cclxuICAgIC8qKiBDU1MgY2xhc3MgdXNlZCB0byBhcHBseSBoaWdobGlnaHRpbmcuICovXHJcbiAgICBASW5wdXQoJ2FkZi1oaWdobGlnaHQtY2xhc3MnKVxyXG4gICAgY2xhc3NUb0FwcGx5OiBzdHJpbmcgPSAnYWRmLWhpZ2hsaWdodCc7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBlbDogRWxlbWVudFJlZixcclxuICAgICAgICBwcml2YXRlIHJlbmRlcmVyOiBSZW5kZXJlcjIsXHJcbiAgICAgICAgcHJpdmF0ZSBoaWdobGlnaHRUcmFuc2Zvcm1TZXJ2aWNlOiBIaWdobGlnaHRUcmFuc2Zvcm1TZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdBZnRlclZpZXdDaGVja2VkKCkge1xyXG4gICAgICAgIHRoaXMuaGlnaGxpZ2h0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGhpZ2hsaWdodChzZWFyY2ggPSB0aGlzLnNlYXJjaCwgc2VsZWN0b3IgPSB0aGlzLnNlbGVjdG9yLCBjbGFzc1RvQXBwbHkgPSB0aGlzLmNsYXNzVG9BcHBseSkge1xyXG4gICAgICAgIGlmIChzZWFyY2ggJiYgc2VsZWN0b3IpIHtcclxuICAgICAgICAgICAgY29uc3QgZWxlbWVudHMgPSB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvckFsbChzZWxlY3Rvcik7XHJcblxyXG4gICAgICAgICAgICBlbGVtZW50cy5mb3JFYWNoKChlbGVtZW50KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBoaWdobGlnaHRUcmFuc2Zvcm1SZXN1bHQ6IEhpZ2hsaWdodFRyYW5zZm9ybVJlc3VsdCA9IHRoaXMuaGlnaGxpZ2h0VHJhbnNmb3JtU2VydmljZS5oaWdobGlnaHQoZWxlbWVudC5pbm5lckhUTUwsIHNlYXJjaCwgY2xhc3NUb0FwcGx5KTtcclxuICAgICAgICAgICAgICAgIGlmIChoaWdobGlnaHRUcmFuc2Zvcm1SZXN1bHQuY2hhbmdlZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmVuZGVyZXIuc2V0UHJvcGVydHkoZWxlbWVudCwgJ2lubmVySFRNTCcsIGhpZ2hsaWdodFRyYW5zZm9ybVJlc3VsdC50ZXh0KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==