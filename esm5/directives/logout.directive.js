/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Input, Directive, ElementRef, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
var LogoutDirective = /** @class */ (function () {
    function LogoutDirective(elementRef, renderer, router, auth) {
        this.elementRef = elementRef;
        this.renderer = renderer;
        this.router = router;
        this.auth = auth;
        /**
         * URI to redirect to after logging out.
         */
        this.redirectUri = '/login';
        /**
         * Enable redirecting after logout
         */
        this.enableRedirect = true;
    }
    /**
     * @return {?}
     */
    LogoutDirective.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.elementRef.nativeElement) {
            this.renderer.listen(this.elementRef.nativeElement, 'click', (/**
             * @param {?} evt
             * @return {?}
             */
            function (evt) {
                evt.preventDefault();
                _this.logout();
            }));
        }
    };
    /**
     * @return {?}
     */
    LogoutDirective.prototype.logout = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.auth.logout().subscribe((/**
         * @return {?}
         */
        function () { return _this.redirectToUri(); }), (/**
         * @return {?}
         */
        function () { return _this.redirectToUri(); }));
    };
    /**
     * @return {?}
     */
    LogoutDirective.prototype.redirectToUri = /**
     * @return {?}
     */
    function () {
        if (this.enableRedirect) {
            this.router.navigate([this.redirectUri]);
        }
    };
    LogoutDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[adf-logout]'
                },] }
    ];
    /** @nocollapse */
    LogoutDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: Renderer2 },
        { type: Router },
        { type: AuthenticationService }
    ]; };
    LogoutDirective.propDecorators = {
        redirectUri: [{ type: Input }],
        enableRedirect: [{ type: Input }]
    };
    return LogoutDirective;
}());
export { LogoutDirective };
if (false) {
    /**
     * URI to redirect to after logging out.
     * @type {?}
     */
    LogoutDirective.prototype.redirectUri;
    /**
     * Enable redirecting after logout
     * @type {?}
     */
    LogoutDirective.prototype.enableRedirect;
    /**
     * @type {?}
     * @private
     */
    LogoutDirective.prototype.elementRef;
    /**
     * @type {?}
     * @private
     */
    LogoutDirective.prototype.renderer;
    /**
     * @type {?}
     * @private
     */
    LogoutDirective.prototype.router;
    /**
     * @type {?}
     * @private
     */
    LogoutDirective.prototype.auth;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nb3V0LmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvbG9nb3V0LmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQVUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUN6QyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUUzRTtJQWFJLHlCQUFvQixVQUFzQixFQUN0QixRQUFtQixFQUNuQixNQUFjLEVBQ2QsSUFBMkI7UUFIM0IsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixhQUFRLEdBQVIsUUFBUSxDQUFXO1FBQ25CLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxTQUFJLEdBQUosSUFBSSxDQUF1Qjs7OztRQVQvQyxnQkFBVyxHQUFXLFFBQVEsQ0FBQzs7OztRQUkvQixtQkFBYyxHQUFZLElBQUksQ0FBQztJQU0vQixDQUFDOzs7O0lBRUQsa0NBQVE7OztJQUFSO1FBQUEsaUJBT0M7UUFORyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFO1lBQy9CLElBQUksQ0FBQyxRQUFRLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLE9BQU87Ozs7WUFBRSxVQUFDLEdBQUc7Z0JBQzdELEdBQUcsQ0FBQyxjQUFjLEVBQUUsQ0FBQztnQkFDckIsS0FBSSxDQUFDLE1BQU0sRUFBRSxDQUFDO1lBQ2xCLENBQUMsRUFBQyxDQUFDO1NBQ047SUFDTCxDQUFDOzs7O0lBRUQsZ0NBQU07OztJQUFOO1FBQUEsaUJBS0M7UUFKRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxDQUFDLFNBQVM7OztRQUN4QixjQUFNLE9BQUEsS0FBSSxDQUFDLGFBQWEsRUFBRSxFQUFwQixDQUFvQjs7O1FBQzFCLGNBQU0sT0FBQSxLQUFJLENBQUMsYUFBYSxFQUFFLEVBQXBCLENBQW9CLEVBQzdCLENBQUM7SUFDTixDQUFDOzs7O0lBRUQsdUNBQWE7OztJQUFiO1FBQ0ksSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFO1lBQ3JCLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7U0FDNUM7SUFDTCxDQUFDOztnQkF2Q0osU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxjQUFjO2lCQUMzQjs7OztnQkFOMEIsVUFBVTtnQkFBVSxTQUFTO2dCQUMvQyxNQUFNO2dCQUNOLHFCQUFxQjs7OzhCQVF6QixLQUFLO2lDQUlMLEtBQUs7O0lBOEJWLHNCQUFDO0NBQUEsQUF4Q0QsSUF3Q0M7U0FyQ1ksZUFBZTs7Ozs7O0lBR3hCLHNDQUMrQjs7Ozs7SUFHL0IseUNBQytCOzs7OztJQUVuQixxQ0FBOEI7Ozs7O0lBQzlCLG1DQUEyQjs7Ozs7SUFDM0IsaUNBQXNCOzs7OztJQUN0QiwrQkFBbUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5wdXQsIERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgT25Jbml0LCBSZW5kZXJlcjIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgUm91dGVyIH0gZnJvbSAnQGFuZ3VsYXIvcm91dGVyJztcclxuaW1wb3J0IHsgQXV0aGVudGljYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYXV0aGVudGljYXRpb24uc2VydmljZSc7XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW2FkZi1sb2dvdXRdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTG9nb3V0RGlyZWN0aXZlIGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICAvKiogVVJJIHRvIHJlZGlyZWN0IHRvIGFmdGVyIGxvZ2dpbmcgb3V0LiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHJlZGlyZWN0VXJpOiBzdHJpbmcgPSAnL2xvZ2luJztcclxuXHJcbiAgICAvKiogRW5hYmxlIHJlZGlyZWN0aW5nIGFmdGVyIGxvZ291dCAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGVuYWJsZVJlZGlyZWN0OiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGVsZW1lbnRSZWY6IEVsZW1lbnRSZWYsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHJlbmRlcmVyOiBSZW5kZXJlcjIsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHJvdXRlcjogUm91dGVyLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBhdXRoOiBBdXRoZW50aWNhdGlvblNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBpZiAodGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQpIHtcclxuICAgICAgICAgICAgdGhpcy5yZW5kZXJlci5saXN0ZW4odGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQsICdjbGljaycsIChldnQpID0+IHtcclxuICAgICAgICAgICAgICAgIGV2dC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2dvdXQoKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGxvZ291dCgpIHtcclxuICAgICAgICB0aGlzLmF1dGgubG9nb3V0KCkuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAoKSA9PiB0aGlzLnJlZGlyZWN0VG9VcmkoKSxcclxuICAgICAgICAgICAgKCkgPT4gdGhpcy5yZWRpcmVjdFRvVXJpKClcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIHJlZGlyZWN0VG9VcmkoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZW5hYmxlUmVkaXJlY3QpIHtcclxuICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW3RoaXMucmVkaXJlY3RVcmldKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19