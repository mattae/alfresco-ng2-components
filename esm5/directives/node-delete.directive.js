/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:no-input-rename  */
import { Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { forkJoin, from, of } from 'rxjs';
import { AlfrescoApiService } from '../services/alfresco-api.service';
import { TranslationService } from '../services/translation.service';
import { map, catchError } from 'rxjs/operators';
/**
 * @record
 */
function ProcessedNodeData() { }
if (false) {
    /** @type {?} */
    ProcessedNodeData.prototype.entry;
    /** @type {?} */
    ProcessedNodeData.prototype.status;
}
/**
 * @record
 */
function ProcessStatus() { }
if (false) {
    /** @type {?} */
    ProcessStatus.prototype.success;
    /** @type {?} */
    ProcessStatus.prototype.failed;
    /**
     * @return {?}
     */
    ProcessStatus.prototype.someFailed = function () { };
    /**
     * @return {?}
     */
    ProcessStatus.prototype.someSucceeded = function () { };
    /**
     * @return {?}
     */
    ProcessStatus.prototype.oneFailed = function () { };
    /**
     * @return {?}
     */
    ProcessStatus.prototype.oneSucceeded = function () { };
    /**
     * @return {?}
     */
    ProcessStatus.prototype.allSucceeded = function () { };
    /**
     * @return {?}
     */
    ProcessStatus.prototype.allFailed = function () { };
}
var NodeDeleteDirective = /** @class */ (function () {
    function NodeDeleteDirective(alfrescoApiService, translation, elementRef) {
        this.alfrescoApiService = alfrescoApiService;
        this.translation = translation;
        this.elementRef = elementRef;
        /**
         * If true then the nodes are deleted immediately rather than being put in the trash
         */
        this.permanent = false;
        /**
         * Emitted when the nodes have been deleted.
         */
        this.delete = new EventEmitter();
    }
    /**
     * @return {?}
     */
    NodeDeleteDirective.prototype.onClick = /**
     * @return {?}
     */
    function () {
        this.process(this.selection);
    };
    /**
     * @return {?}
     */
    NodeDeleteDirective.prototype.ngOnChanges = /**
     * @return {?}
     */
    function () {
        if (!this.selection || (this.selection && this.selection.length === 0)) {
            this.setDisableAttribute(true);
        }
        else {
            if (!this.elementRef.nativeElement.hasAttribute('adf-check-allowable-operation')) {
                this.setDisableAttribute(false);
            }
        }
    };
    /**
     * @private
     * @param {?} disable
     * @return {?}
     */
    NodeDeleteDirective.prototype.setDisableAttribute = /**
     * @private
     * @param {?} disable
     * @return {?}
     */
    function (disable) {
        this.elementRef.nativeElement.disabled = disable;
    };
    /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    NodeDeleteDirective.prototype.process = /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    function (selection) {
        var _this = this;
        if (selection && selection.length) {
            /** @type {?} */
            var batch = this.getDeleteNodesBatch(selection);
            forkJoin.apply(void 0, tslib_1.__spread(batch)).subscribe((/**
             * @param {?} data
             * @return {?}
             */
            function (data) {
                /** @type {?} */
                var processedItems = _this.processStatus(data);
                /** @type {?} */
                var message = _this.getMessage(processedItems);
                _this.delete.emit(message);
            }));
        }
    };
    /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    NodeDeleteDirective.prototype.getDeleteNodesBatch = /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    function (selection) {
        var _this = this;
        return selection.map((/**
         * @param {?} node
         * @return {?}
         */
        function (node) { return _this.deleteNode(node); }));
    };
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    NodeDeleteDirective.prototype.deleteNode = /**
     * @private
     * @param {?} node
     * @return {?}
     */
    function (node) {
        /** @type {?} */
        var id = ((/** @type {?} */ (node.entry))).nodeId || node.entry.id;
        /** @type {?} */
        var promise;
        if (node.entry.hasOwnProperty('archivedAt') && node.entry['archivedAt']) {
            promise = this.alfrescoApiService.nodesApi.purgeDeletedNode(id);
        }
        else {
            promise = this.alfrescoApiService.nodesApi.deleteNode(id, { permanent: this.permanent });
        }
        return from(promise).pipe(map((/**
         * @return {?}
         */
        function () { return ({
            entry: node.entry,
            status: 1
        }); })), catchError((/**
         * @return {?}
         */
        function () { return of({
            entry: node.entry,
            status: 0
        }); })));
    };
    /**
     * @private
     * @param {?} data
     * @return {?}
     */
    NodeDeleteDirective.prototype.processStatus = /**
     * @private
     * @param {?} data
     * @return {?}
     */
    function (data) {
        /** @type {?} */
        var deleteStatus = {
            success: [],
            failed: [],
            /**
             * @return {?}
             */
            get someFailed() {
                return !!(this.failed.length);
            },
            /**
             * @return {?}
             */
            get someSucceeded() {
                return !!(this.success.length);
            },
            /**
             * @return {?}
             */
            get oneFailed() {
                return this.failed.length === 1;
            },
            /**
             * @return {?}
             */
            get oneSucceeded() {
                return this.success.length === 1;
            },
            /**
             * @return {?}
             */
            get allSucceeded() {
                return this.someSucceeded && !this.someFailed;
            },
            /**
             * @return {?}
             */
            get allFailed() {
                return this.someFailed && !this.someSucceeded;
            }
        };
        return data.reduce((/**
         * @param {?} acc
         * @param {?} next
         * @return {?}
         */
        function (acc, next) {
            if (next.status === 1) {
                acc.success.push(next);
            }
            else {
                acc.failed.push(next);
            }
            return acc;
        }), deleteStatus);
    };
    /**
     * @private
     * @param {?} status
     * @return {?}
     */
    NodeDeleteDirective.prototype.getMessage = /**
     * @private
     * @param {?} status
     * @return {?}
     */
    function (status) {
        if (status.allFailed && !status.oneFailed) {
            return this.translation.instant('CORE.DELETE_NODE.ERROR_PLURAL', { number: status.failed.length });
        }
        if (status.allSucceeded && !status.oneSucceeded) {
            return this.translation.instant('CORE.DELETE_NODE.PLURAL', { number: status.success.length });
        }
        if (status.someFailed && status.someSucceeded && !status.oneSucceeded) {
            return this.translation.instant('CORE.DELETE_NODE.PARTIAL_PLURAL', {
                success: status.success.length,
                failed: status.failed.length
            });
        }
        if (status.someFailed && status.oneSucceeded) {
            return this.translation.instant('CORE.DELETE_NODE.PARTIAL_SINGULAR', {
                success: status.success.length,
                failed: status.failed.length
            });
        }
        if (status.oneFailed && !status.someSucceeded) {
            return this.translation.instant('CORE.DELETE_NODE.ERROR_SINGULAR', { name: status.failed[0].entry.name });
        }
        if (status.oneSucceeded && !status.someFailed) {
            return this.translation.instant('CORE.DELETE_NODE.SINGULAR', { name: status.success[0].entry.name });
        }
    };
    NodeDeleteDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[adf-delete]'
                },] }
    ];
    /** @nocollapse */
    NodeDeleteDirective.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: TranslationService },
        { type: ElementRef }
    ]; };
    NodeDeleteDirective.propDecorators = {
        selection: [{ type: Input, args: ['adf-delete',] }],
        permanent: [{ type: Input }],
        delete: [{ type: Output }],
        onClick: [{ type: HostListener, args: ['click',] }]
    };
    return NodeDeleteDirective;
}());
export { NodeDeleteDirective };
if (false) {
    /**
     * Array of nodes to delete.
     * @type {?}
     */
    NodeDeleteDirective.prototype.selection;
    /**
     * If true then the nodes are deleted immediately rather than being put in the trash
     * @type {?}
     */
    NodeDeleteDirective.prototype.permanent;
    /**
     * Emitted when the nodes have been deleted.
     * @type {?}
     */
    NodeDeleteDirective.prototype.delete;
    /**
     * @type {?}
     * @private
     */
    NodeDeleteDirective.prototype.alfrescoApiService;
    /**
     * @type {?}
     * @private
     */
    NodeDeleteDirective.prototype.translation;
    /**
     * @type {?}
     * @private
     */
    NodeDeleteDirective.prototype.elementRef;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9kZS1kZWxldGUuZGlyZWN0aXZlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZGlyZWN0aXZlcy9ub2RlLWRlbGV0ZS5kaXJlY3RpdmUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxZQUFZLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBYSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFNUcsT0FBTyxFQUFjLFFBQVEsRUFBRSxJQUFJLEVBQUUsRUFBRSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3RELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3RFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxHQUFHLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7QUFFakQsZ0NBR0M7OztJQUZHLGtDQUEwQjs7SUFDMUIsbUNBQWU7Ozs7O0FBR25CLDRCQWVDOzs7SUFkRyxnQ0FBNkI7O0lBQzdCLCtCQUE0Qjs7OztJQUU1QixxREFBYTs7OztJQUViLHdEQUFnQjs7OztJQUVoQixvREFBWTs7OztJQUVaLHVEQUFlOzs7O0lBRWYsdURBQWU7Ozs7SUFFZixvREFBWTs7QUFHaEI7SUFxQkksNkJBQW9CLGtCQUFzQyxFQUN0QyxXQUErQixFQUMvQixVQUFzQjtRQUZ0Qix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLGdCQUFXLEdBQVgsV0FBVyxDQUFvQjtRQUMvQixlQUFVLEdBQVYsVUFBVSxDQUFZOzs7O1FBYjFDLGNBQVMsR0FBWSxLQUFLLENBQUM7Ozs7UUFJM0IsV0FBTSxHQUFzQixJQUFJLFlBQVksRUFBRSxDQUFDO0lBVS9DLENBQUM7Ozs7SUFQRCxxQ0FBTzs7O0lBRFA7UUFFSSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7O0lBT0QseUNBQVc7OztJQUFYO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxFQUFFO1lBQ3BFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNsQzthQUFNO1lBQ0gsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQywrQkFBK0IsQ0FBQyxFQUFFO2dCQUM5RSxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDbkM7U0FDSjtJQUNMLENBQUM7Ozs7OztJQUVPLGlEQUFtQjs7Ozs7SUFBM0IsVUFBNEIsT0FBZ0I7UUFDeEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsUUFBUSxHQUFHLE9BQU8sQ0FBQztJQUNyRCxDQUFDOzs7Ozs7SUFFTyxxQ0FBTzs7Ozs7SUFBZixVQUFnQixTQUE0QztRQUE1RCxpQkFhQztRQVpHLElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEVBQUU7O2dCQUV6QixLQUFLLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsQ0FBQztZQUVqRCxRQUFRLGdDQUFJLEtBQUssR0FDWixTQUFTOzs7O1lBQUMsVUFBQyxJQUF5Qjs7b0JBQzNCLGNBQWMsR0FBa0IsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUM7O29CQUN4RCxPQUFPLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUM7Z0JBRS9DLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzlCLENBQUMsRUFBQyxDQUFDO1NBQ1Y7SUFDTCxDQUFDOzs7Ozs7SUFFTyxpREFBbUI7Ozs7O0lBQTNCLFVBQTRCLFNBQWM7UUFBMUMsaUJBRUM7UUFERyxPQUFPLFNBQVMsQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQUM7SUFDMUQsQ0FBQzs7Ozs7O0lBRU8sd0NBQVU7Ozs7O0lBQWxCLFVBQW1CLElBQW1DOztZQUM1QyxFQUFFLEdBQUcsQ0FBQyxtQkFBTSxJQUFJLENBQUMsS0FBSyxFQUFBLENBQUMsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFOztZQUVqRCxPQUFPO1FBRVgsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxFQUFFO1lBQ3JFLE9BQU8sR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ25FO2FBQU07WUFDSCxPQUFPLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsRUFBRSxFQUFFLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxDQUFDO1NBQzVGO1FBRUQsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUNyQixHQUFHOzs7UUFBQyxjQUFNLE9BQUEsQ0FBQztZQUNQLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixNQUFNLEVBQUUsQ0FBQztTQUNaLENBQUMsRUFIUSxDQUdSLEVBQUMsRUFDSCxVQUFVOzs7UUFBQyxjQUFNLE9BQUEsRUFBRSxDQUFDO1lBQ2hCLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztZQUNqQixNQUFNLEVBQUUsQ0FBQztTQUNaLENBQUMsRUFIZSxDQUdmLEVBQUMsQ0FDTixDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBRU8sMkNBQWE7Ozs7O0lBQXJCLFVBQXNCLElBQUk7O1lBQ2hCLFlBQVksR0FBRztZQUNqQixPQUFPLEVBQUUsRUFBRTtZQUNYLE1BQU0sRUFBRSxFQUFFOzs7O1lBQ1YsSUFBSSxVQUFVO2dCQUNWLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNsQyxDQUFDOzs7O1lBQ0QsSUFBSSxhQUFhO2dCQUNiLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNuQyxDQUFDOzs7O1lBQ0QsSUFBSSxTQUFTO2dCQUNULE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO1lBQ3BDLENBQUM7Ozs7WUFDRCxJQUFJLFlBQVk7Z0JBQ1osT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUM7WUFDckMsQ0FBQzs7OztZQUNELElBQUksWUFBWTtnQkFDWixPQUFPLElBQUksQ0FBQyxhQUFhLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ2xELENBQUM7Ozs7WUFDRCxJQUFJLFNBQVM7Z0JBQ1QsT0FBTyxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztZQUNsRCxDQUFDO1NBQ0o7UUFFRCxPQUFPLElBQUksQ0FBQyxNQUFNOzs7OztRQUNkLFVBQUMsR0FBRyxFQUFFLElBQUk7WUFDTixJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO2dCQUNuQixHQUFHLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUMxQjtpQkFBTTtnQkFDSCxHQUFHLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUN6QjtZQUVELE9BQU8sR0FBRyxDQUFDO1FBQ2YsQ0FBQyxHQUNELFlBQVksQ0FDZixDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBRU8sd0NBQVU7Ozs7O0lBQWxCLFVBQW1CLE1BQU07UUFDckIsSUFBSSxNQUFNLENBQUMsU0FBUyxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRTtZQUN2QyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUMzQiwrQkFBK0IsRUFDL0IsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FDbkMsQ0FBQztTQUNMO1FBRUQsSUFBSSxNQUFNLENBQUMsWUFBWSxJQUFJLENBQUMsTUFBTSxDQUFDLFlBQVksRUFBRTtZQUM3QyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUMzQix5QkFBeUIsRUFDekIsRUFBRSxNQUFNLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FDcEMsQ0FBQztTQUNMO1FBRUQsSUFBSSxNQUFNLENBQUMsVUFBVSxJQUFJLE1BQU0sQ0FBQyxhQUFhLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFO1lBQ25FLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQzNCLGlDQUFpQyxFQUNqQztnQkFDSSxPQUFPLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxNQUFNO2dCQUM5QixNQUFNLEVBQUUsTUFBTSxDQUFDLE1BQU0sQ0FBQyxNQUFNO2FBQy9CLENBQ0osQ0FBQztTQUNMO1FBRUQsSUFBSSxNQUFNLENBQUMsVUFBVSxJQUFJLE1BQU0sQ0FBQyxZQUFZLEVBQUU7WUFDMUMsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FDM0IsbUNBQW1DLEVBQ25DO2dCQUNJLE9BQU8sRUFBRSxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU07Z0JBQzlCLE1BQU0sRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLE1BQU07YUFDL0IsQ0FDSixDQUFDO1NBQ0w7UUFFRCxJQUFJLE1BQU0sQ0FBQyxTQUFTLElBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxFQUFFO1lBQzNDLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQzNCLGlDQUFpQyxFQUNqQyxFQUFFLElBQUksRUFBRSxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FDeEMsQ0FBQztTQUNMO1FBRUQsSUFBSSxNQUFNLENBQUMsWUFBWSxJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVUsRUFBRTtZQUMzQyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUMzQiwyQkFBMkIsRUFDM0IsRUFBRSxJQUFJLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQ3pDLENBQUM7U0FDTDtJQUNMLENBQUM7O2dCQXhLSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGNBQWM7aUJBQzNCOzs7O2dCQTVCUSxrQkFBa0I7Z0JBQ2xCLGtCQUFrQjtnQkFKUCxVQUFVOzs7NEJBa0N6QixLQUFLLFNBQUMsWUFBWTs0QkFJbEIsS0FBSzt5QkFJTCxNQUFNOzBCQUdOLFlBQVksU0FBQyxPQUFPOztJQXlKekIsMEJBQUM7Q0FBQSxBQXpLRCxJQXlLQztTQXRLWSxtQkFBbUI7Ozs7OztJQUU1Qix3Q0FDNkM7Ozs7O0lBRzdDLHdDQUMyQjs7Ozs7SUFHM0IscUNBQytDOzs7OztJQU9uQyxpREFBOEM7Ozs7O0lBQzlDLDBDQUF1Qzs7Ozs7SUFDdkMseUNBQThCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOm5vLWlucHV0LXJlbmFtZSAgKi9cclxuXHJcbmltcG9ydCB7IERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgRXZlbnRFbWl0dGVyLCBIb3N0TGlzdGVuZXIsIElucHV0LCBPbkNoYW5nZXMsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOb2RlRW50cnksIE5vZGUsIERlbGV0ZWROb2RlRW50aXR5LCBEZWxldGVkTm9kZSB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmb3JrSm9pbiwgZnJvbSwgb2YgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGlvblNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy90cmFuc2xhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgbWFwLCBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuaW50ZXJmYWNlIFByb2Nlc3NlZE5vZGVEYXRhIHtcclxuICAgIGVudHJ5OiBOb2RlIHwgRGVsZXRlZE5vZGU7XHJcbiAgICBzdGF0dXM6IG51bWJlcjtcclxufVxyXG5cclxuaW50ZXJmYWNlIFByb2Nlc3NTdGF0dXMge1xyXG4gICAgc3VjY2VzczogUHJvY2Vzc2VkTm9kZURhdGFbXTtcclxuICAgIGZhaWxlZDogUHJvY2Vzc2VkTm9kZURhdGFbXTtcclxuXHJcbiAgICBzb21lRmFpbGVkKCk7XHJcblxyXG4gICAgc29tZVN1Y2NlZWRlZCgpO1xyXG5cclxuICAgIG9uZUZhaWxlZCgpO1xyXG5cclxuICAgIG9uZVN1Y2NlZWRlZCgpO1xyXG5cclxuICAgIGFsbFN1Y2NlZWRlZCgpO1xyXG5cclxuICAgIGFsbEZhaWxlZCgpO1xyXG59XHJcblxyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW2FkZi1kZWxldGVdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTm9kZURlbGV0ZURpcmVjdGl2ZSBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XHJcbiAgICAvKiogQXJyYXkgb2Ygbm9kZXMgdG8gZGVsZXRlLiAqL1xyXG4gICAgQElucHV0KCdhZGYtZGVsZXRlJylcclxuICAgIHNlbGVjdGlvbjogTm9kZUVudHJ5W10gfCBEZWxldGVkTm9kZUVudGl0eVtdO1xyXG5cclxuICAgIC8qKiBJZiB0cnVlIHRoZW4gdGhlIG5vZGVzIGFyZSBkZWxldGVkIGltbWVkaWF0ZWx5IHJhdGhlciB0aGFuIGJlaW5nIHB1dCBpbiB0aGUgdHJhc2ggKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBwZXJtYW5lbnQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSBub2RlcyBoYXZlIGJlZW4gZGVsZXRlZC4gKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgZGVsZXRlOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdjbGljaycpXHJcbiAgICBvbkNsaWNrKCkge1xyXG4gICAgICAgIHRoaXMucHJvY2Vzcyh0aGlzLnNlbGVjdGlvbik7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhbGZyZXNjb0FwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgdHJhbnNsYXRpb246IFRyYW5zbGF0aW9uU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgZWxlbWVudFJlZjogRWxlbWVudFJlZikge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25DaGFuZ2VzKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5zZWxlY3Rpb24gfHwgKHRoaXMuc2VsZWN0aW9uICYmIHRoaXMuc2VsZWN0aW9uLmxlbmd0aCA9PT0gMCkpIHtcclxuICAgICAgICAgICAgdGhpcy5zZXREaXNhYmxlQXR0cmlidXRlKHRydWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQuaGFzQXR0cmlidXRlKCdhZGYtY2hlY2stYWxsb3dhYmxlLW9wZXJhdGlvbicpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldERpc2FibGVBdHRyaWJ1dGUoZmFsc2UpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgc2V0RGlzYWJsZUF0dHJpYnV0ZShkaXNhYmxlOiBib29sZWFuKSB7XHJcbiAgICAgICAgdGhpcy5lbGVtZW50UmVmLm5hdGl2ZUVsZW1lbnQuZGlzYWJsZWQgPSBkaXNhYmxlO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgcHJvY2VzcyhzZWxlY3Rpb246IE5vZGVFbnRyeVtdIHwgRGVsZXRlZE5vZGVFbnRpdHlbXSkge1xyXG4gICAgICAgIGlmIChzZWxlY3Rpb24gJiYgc2VsZWN0aW9uLmxlbmd0aCkge1xyXG5cclxuICAgICAgICAgICAgY29uc3QgYmF0Y2ggPSB0aGlzLmdldERlbGV0ZU5vZGVzQmF0Y2goc2VsZWN0aW9uKTtcclxuXHJcbiAgICAgICAgICAgIGZvcmtKb2luKC4uLmJhdGNoKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZSgoZGF0YTogUHJvY2Vzc2VkTm9kZURhdGFbXSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHByb2Nlc3NlZEl0ZW1zOiBQcm9jZXNzU3RhdHVzID0gdGhpcy5wcm9jZXNzU3RhdHVzKGRhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IG1lc3NhZ2UgPSB0aGlzLmdldE1lc3NhZ2UocHJvY2Vzc2VkSXRlbXMpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmRlbGV0ZS5lbWl0KG1lc3NhZ2UpO1xyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0RGVsZXRlTm9kZXNCYXRjaChzZWxlY3Rpb246IGFueSk6IE9ic2VydmFibGU8UHJvY2Vzc2VkTm9kZURhdGE+W10ge1xyXG4gICAgICAgIHJldHVybiBzZWxlY3Rpb24ubWFwKChub2RlKSA9PiB0aGlzLmRlbGV0ZU5vZGUobm9kZSkpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZGVsZXRlTm9kZShub2RlOiBOb2RlRW50cnkgfCBEZWxldGVkTm9kZUVudGl0eSk6IE9ic2VydmFibGU8UHJvY2Vzc2VkTm9kZURhdGE+IHtcclxuICAgICAgICBjb25zdCBpZCA9ICg8YW55PiBub2RlLmVudHJ5KS5ub2RlSWQgfHwgbm9kZS5lbnRyeS5pZDtcclxuXHJcbiAgICAgICAgbGV0IHByb21pc2U7XHJcblxyXG4gICAgICAgIGlmIChub2RlLmVudHJ5Lmhhc093blByb3BlcnR5KCdhcmNoaXZlZEF0JykgJiYgbm9kZS5lbnRyeVsnYXJjaGl2ZWRBdCddKSB7XHJcbiAgICAgICAgICAgIHByb21pc2UgPSB0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5ub2Rlc0FwaS5wdXJnZURlbGV0ZWROb2RlKGlkKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBwcm9taXNlID0gdGhpcy5hbGZyZXNjb0FwaVNlcnZpY2Uubm9kZXNBcGkuZGVsZXRlTm9kZShpZCwgeyBwZXJtYW5lbnQ6IHRoaXMucGVybWFuZW50IH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20ocHJvbWlzZSkucGlwZShcclxuICAgICAgICAgICAgbWFwKCgpID0+ICh7XHJcbiAgICAgICAgICAgICAgICBlbnRyeTogbm9kZS5lbnRyeSxcclxuICAgICAgICAgICAgICAgIHN0YXR1czogMVxyXG4gICAgICAgICAgICB9KSksXHJcbiAgICAgICAgICAgIGNhdGNoRXJyb3IoKCkgPT4gb2Yoe1xyXG4gICAgICAgICAgICAgICAgZW50cnk6IG5vZGUuZW50cnksXHJcbiAgICAgICAgICAgICAgICBzdGF0dXM6IDBcclxuICAgICAgICAgICAgfSkpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHByb2Nlc3NTdGF0dXMoZGF0YSk6IFByb2Nlc3NTdGF0dXMge1xyXG4gICAgICAgIGNvbnN0IGRlbGV0ZVN0YXR1cyA9IHtcclxuICAgICAgICAgICAgc3VjY2VzczogW10sXHJcbiAgICAgICAgICAgIGZhaWxlZDogW10sXHJcbiAgICAgICAgICAgIGdldCBzb21lRmFpbGVkKCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICEhKHRoaXMuZmFpbGVkLmxlbmd0aCk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGdldCBzb21lU3VjY2VlZGVkKCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICEhKHRoaXMuc3VjY2Vzcy5sZW5ndGgpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBnZXQgb25lRmFpbGVkKCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZmFpbGVkLmxlbmd0aCA9PT0gMTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZ2V0IG9uZVN1Y2NlZWRlZCgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnN1Y2Nlc3MubGVuZ3RoID09PSAxO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBnZXQgYWxsU3VjY2VlZGVkKCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuc29tZVN1Y2NlZWRlZCAmJiAhdGhpcy5zb21lRmFpbGVkO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBnZXQgYWxsRmFpbGVkKCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuc29tZUZhaWxlZCAmJiAhdGhpcy5zb21lU3VjY2VlZGVkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGRhdGEucmVkdWNlKFxyXG4gICAgICAgICAgICAoYWNjLCBuZXh0KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAobmV4dC5zdGF0dXMgPT09IDEpIHtcclxuICAgICAgICAgICAgICAgICAgICBhY2Muc3VjY2Vzcy5wdXNoKG5leHQpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBhY2MuZmFpbGVkLnB1c2gobmV4dCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGFjYztcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZGVsZXRlU3RhdHVzXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldE1lc3NhZ2Uoc3RhdHVzKTogc3RyaW5nIHtcclxuICAgICAgICBpZiAoc3RhdHVzLmFsbEZhaWxlZCAmJiAhc3RhdHVzLm9uZUZhaWxlZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy50cmFuc2xhdGlvbi5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgJ0NPUkUuREVMRVRFX05PREUuRVJST1JfUExVUkFMJyxcclxuICAgICAgICAgICAgICAgIHsgbnVtYmVyOiBzdGF0dXMuZmFpbGVkLmxlbmd0aCB9XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoc3RhdHVzLmFsbFN1Y2NlZWRlZCAmJiAhc3RhdHVzLm9uZVN1Y2NlZWRlZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy50cmFuc2xhdGlvbi5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgJ0NPUkUuREVMRVRFX05PREUuUExVUkFMJyxcclxuICAgICAgICAgICAgICAgIHsgbnVtYmVyOiBzdGF0dXMuc3VjY2Vzcy5sZW5ndGggfVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHN0YXR1cy5zb21lRmFpbGVkICYmIHN0YXR1cy5zb21lU3VjY2VlZGVkICYmICFzdGF0dXMub25lU3VjY2VlZGVkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnRyYW5zbGF0aW9uLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAnQ09SRS5ERUxFVEVfTk9ERS5QQVJUSUFMX1BMVVJBTCcsXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgc3VjY2Vzczogc3RhdHVzLnN1Y2Nlc3MubGVuZ3RoLFxyXG4gICAgICAgICAgICAgICAgICAgIGZhaWxlZDogc3RhdHVzLmZhaWxlZC5sZW5ndGhcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChzdGF0dXMuc29tZUZhaWxlZCAmJiBzdGF0dXMub25lU3VjY2VlZGVkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnRyYW5zbGF0aW9uLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAnQ09SRS5ERUxFVEVfTk9ERS5QQVJUSUFMX1NJTkdVTEFSJyxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBzdWNjZXNzOiBzdGF0dXMuc3VjY2Vzcy5sZW5ndGgsXHJcbiAgICAgICAgICAgICAgICAgICAgZmFpbGVkOiBzdGF0dXMuZmFpbGVkLmxlbmd0aFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHN0YXR1cy5vbmVGYWlsZWQgJiYgIXN0YXR1cy5zb21lU3VjY2VlZGVkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnRyYW5zbGF0aW9uLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAnQ09SRS5ERUxFVEVfTk9ERS5FUlJPUl9TSU5HVUxBUicsXHJcbiAgICAgICAgICAgICAgICB7IG5hbWU6IHN0YXR1cy5mYWlsZWRbMF0uZW50cnkubmFtZSB9XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoc3RhdHVzLm9uZVN1Y2NlZWRlZCAmJiAhc3RhdHVzLnNvbWVGYWlsZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMudHJhbnNsYXRpb24uaW5zdGFudChcclxuICAgICAgICAgICAgICAgICdDT1JFLkRFTEVURV9OT0RFLlNJTkdVTEFSJyxcclxuICAgICAgICAgICAgICAgIHsgbmFtZTogc3RhdHVzLnN1Y2Nlc3NbMF0uZW50cnkubmFtZSB9XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==