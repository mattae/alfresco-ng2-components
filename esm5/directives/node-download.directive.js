/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Directive, Input, HostListener } from '@angular/core';
import { MatDialog } from '@angular/material';
import { AlfrescoApiService } from '../services/alfresco-api.service';
import { DownloadZipDialogComponent } from '../dialogs/download-zip.dialog';
/**
 * Directive selectors without adf- prefix will be deprecated on 3.0.0
 */
var NodeDownloadDirective = /** @class */ (function () {
    function NodeDownloadDirective(apiService, dialog) {
        this.apiService = apiService;
        this.dialog = dialog;
    }
    /**
     * @return {?}
     */
    NodeDownloadDirective.prototype.onClick = /**
     * @return {?}
     */
    function () {
        this.downloadNodes(this.nodes);
    };
    /**
     * Downloads multiple selected nodes.
     * Packs result into a .ZIP archive if there is more than one node selected.
     * @param selection Multiple selected nodes to download
     */
    /**
     * Downloads multiple selected nodes.
     * Packs result into a .ZIP archive if there is more than one node selected.
     * @param {?} selection Multiple selected nodes to download
     * @return {?}
     */
    NodeDownloadDirective.prototype.downloadNodes = /**
     * Downloads multiple selected nodes.
     * Packs result into a .ZIP archive if there is more than one node selected.
     * @param {?} selection Multiple selected nodes to download
     * @return {?}
     */
    function (selection) {
        if (!this.isSelectionValid(selection)) {
            return;
        }
        if (selection instanceof Array) {
            if (selection.length === 1) {
                this.downloadNode(selection[0]);
            }
            else {
                this.downloadZip(selection);
            }
        }
        else {
            this.downloadNode(selection);
        }
    };
    /**
     * Downloads a single node.
     * Packs result into a .ZIP archive is the node is a Folder.
     * @param node Node to download
     */
    /**
     * Downloads a single node.
     * Packs result into a .ZIP archive is the node is a Folder.
     * @param {?} node Node to download
     * @return {?}
     */
    NodeDownloadDirective.prototype.downloadNode = /**
     * Downloads a single node.
     * Packs result into a .ZIP archive is the node is a Folder.
     * @param {?} node Node to download
     * @return {?}
     */
    function (node) {
        if (node && node.entry) {
            /** @type {?} */
            var entry = node.entry;
            if (entry.isFile) {
                this.downloadFile(node);
            }
            if (entry.isFolder) {
                this.downloadZip([node]);
            }
            // Check if there's nodeId for Shared Files
            if (!entry.isFile && !entry.isFolder && ((/** @type {?} */ (entry))).nodeId) {
                this.downloadFile(node);
            }
        }
    };
    /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    NodeDownloadDirective.prototype.isSelectionValid = /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    function (selection) {
        return selection || (selection instanceof Array && selection.length > 0);
    };
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    NodeDownloadDirective.prototype.downloadFile = /**
     * @private
     * @param {?} node
     * @return {?}
     */
    function (node) {
        if (node && node.entry) {
            /** @type {?} */
            var contentApi = this.apiService.getInstance().content;
            // nodeId for Shared node
            /** @type {?} */
            var id = ((/** @type {?} */ (node.entry))).nodeId || node.entry.id;
            /** @type {?} */
            var url = contentApi.getContentUrl(id, true);
            /** @type {?} */
            var fileName = node.entry.name;
            this.download(url, fileName);
        }
    };
    /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    NodeDownloadDirective.prototype.downloadZip = /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    function (selection) {
        if (selection && selection.length > 0) {
            // nodeId for Shared node
            /** @type {?} */
            var nodeIds = selection.map((/**
             * @param {?} node
             * @return {?}
             */
            function (node) { return (node.entry.nodeId || node.entry.id); }));
            this.dialog.open(DownloadZipDialogComponent, {
                width: '600px',
                disableClose: true,
                data: {
                    nodeIds: nodeIds
                }
            });
        }
    };
    /**
     * @private
     * @param {?} url
     * @param {?} fileName
     * @return {?}
     */
    NodeDownloadDirective.prototype.download = /**
     * @private
     * @param {?} url
     * @param {?} fileName
     * @return {?}
     */
    function (url, fileName) {
        if (url && fileName) {
            /** @type {?} */
            var link = document.createElement('a');
            link.style.display = 'none';
            link.download = fileName;
            link.href = url;
            document.body.appendChild(link);
            link.click();
            document.body.removeChild(link);
        }
    };
    NodeDownloadDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[adf-node-download], [adfNodeDownload]'
                },] }
    ];
    /** @nocollapse */
    NodeDownloadDirective.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: MatDialog }
    ]; };
    NodeDownloadDirective.propDecorators = {
        nodes: [{ type: Input, args: ['adfNodeDownload',] }],
        onClick: [{ type: HostListener, args: ['click',] }]
    };
    return NodeDownloadDirective;
}());
export { NodeDownloadDirective };
if (false) {
    /**
     * Nodes to download.
     * @type {?}
     */
    NodeDownloadDirective.prototype.nodes;
    /**
     * @type {?}
     * @private
     */
    NodeDownloadDirective.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    NodeDownloadDirective.prototype.dialog;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9kZS1kb3dubG9hZC5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJkaXJlY3RpdmVzL25vZGUtZG93bmxvYWQuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMvRCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDOUMsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDdEUsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0sZ0NBQWdDLENBQUM7Ozs7QUFNNUU7SUFlSSwrQkFDWSxVQUE4QixFQUM5QixNQUFpQjtRQURqQixlQUFVLEdBQVYsVUFBVSxDQUFvQjtRQUM5QixXQUFNLEdBQU4sTUFBTSxDQUFXO0lBQzdCLENBQUM7Ozs7SUFQRCx1Q0FBTzs7O0lBRFA7UUFFSSxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNuQyxDQUFDO0lBT0Q7Ozs7T0FJRzs7Ozs7OztJQUNILDZDQUFhOzs7Ozs7SUFBYixVQUFjLFNBQXVDO1FBRWpELElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDbkMsT0FBTztTQUNWO1FBQ0QsSUFBSSxTQUFTLFlBQVksS0FBSyxFQUFFO1lBQzVCLElBQUksU0FBUyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxZQUFZLENBQUMsU0FBUyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDbkM7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUMvQjtTQUNKO2FBQU07WUFDSCxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ2hDO0lBQ0wsQ0FBQztJQUVEOzs7O09BSUc7Ozs7Ozs7SUFDSCw0Q0FBWTs7Ozs7O0lBQVosVUFBYSxJQUFlO1FBQ3hCLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7O2dCQUNkLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSztZQUV4QixJQUFJLEtBQUssQ0FBQyxNQUFNLEVBQUU7Z0JBQ2QsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUMzQjtZQUVELElBQUksS0FBSyxDQUFDLFFBQVEsRUFBRTtnQkFDaEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7YUFDNUI7WUFFRCwyQ0FBMkM7WUFDM0MsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxJQUFJLENBQUMsbUJBQU0sS0FBSyxFQUFBLENBQUMsQ0FBQyxNQUFNLEVBQUU7Z0JBQzFELElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDM0I7U0FDSjtJQUNMLENBQUM7Ozs7OztJQUVPLGdEQUFnQjs7Ozs7SUFBeEIsVUFBeUIsU0FBdUM7UUFDNUQsT0FBTyxTQUFTLElBQUksQ0FBQyxTQUFTLFlBQVksS0FBSyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDN0UsQ0FBQzs7Ozs7O0lBRU8sNENBQVk7Ozs7O0lBQXBCLFVBQXFCLElBQWU7UUFDaEMsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTs7Z0JBQ2QsVUFBVSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTzs7O2dCQUVsRCxFQUFFLEdBQUcsQ0FBQyxtQkFBTSxJQUFJLENBQUMsS0FBSyxFQUFBLENBQUMsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFOztnQkFFL0MsR0FBRyxHQUFHLFVBQVUsQ0FBQyxhQUFhLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQzs7Z0JBQ3hDLFFBQVEsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUk7WUFFaEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLEVBQUUsUUFBUSxDQUFDLENBQUM7U0FDaEM7SUFDTCxDQUFDOzs7Ozs7SUFFTywyQ0FBVzs7Ozs7SUFBbkIsVUFBb0IsU0FBMkI7UUFDM0MsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7OztnQkFFN0IsT0FBTyxHQUFHLFNBQVMsQ0FBQyxHQUFHOzs7O1lBQUMsVUFBQyxJQUFTLElBQUssT0FBQSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDLEVBQXBDLENBQW9DLEVBQUM7WUFFbEYsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsMEJBQTBCLEVBQUU7Z0JBQ3pDLEtBQUssRUFBRSxPQUFPO2dCQUNkLFlBQVksRUFBRSxJQUFJO2dCQUNsQixJQUFJLEVBQUU7b0JBQ0YsT0FBTyxTQUFBO2lCQUNWO2FBQ0osQ0FBQyxDQUFDO1NBQ047SUFDTCxDQUFDOzs7Ozs7O0lBRU8sd0NBQVE7Ozs7OztJQUFoQixVQUFpQixHQUFXLEVBQUUsUUFBZ0I7UUFDMUMsSUFBSSxHQUFHLElBQUksUUFBUSxFQUFFOztnQkFDWCxJQUFJLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUM7WUFFeEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1lBQzVCLElBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDO1lBRWhCLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ2hDLElBQUksQ0FBQyxLQUFLLEVBQUUsQ0FBQztZQUNiLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ25DO0lBQ0wsQ0FBQzs7Z0JBN0dKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsd0NBQXdDO2lCQUNyRDs7OztnQkFUUSxrQkFBa0I7Z0JBRGxCLFNBQVM7Ozt3QkFlYixLQUFLLFNBQUMsaUJBQWlCOzBCQUd2QixZQUFZLFNBQUMsT0FBTzs7SUFvR3pCLDRCQUFDO0NBQUEsQUE5R0QsSUE4R0M7U0EzR1kscUJBQXFCOzs7Ozs7SUFJOUIsc0NBQytCOzs7OztJQVEzQiwyQ0FBc0M7Ozs7O0lBQ3RDLHVDQUF5QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBEaXJlY3RpdmUsIElucHV0LCBIb3N0TGlzdGVuZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTWF0RGlhbG9nIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IERvd25sb2FkWmlwRGlhbG9nQ29tcG9uZW50IH0gZnJvbSAnLi4vZGlhbG9ncy9kb3dubG9hZC16aXAuZGlhbG9nJztcclxuaW1wb3J0IHsgTm9kZUVudHJ5IH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcblxyXG4vKipcclxuICogRGlyZWN0aXZlIHNlbGVjdG9ycyB3aXRob3V0IGFkZi0gcHJlZml4IHdpbGwgYmUgZGVwcmVjYXRlZCBvbiAzLjAuMFxyXG4gKi9cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1thZGYtbm9kZS1kb3dubG9hZF0sIFthZGZOb2RlRG93bmxvYWRdJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTm9kZURvd25sb2FkRGlyZWN0aXZlIHtcclxuXHJcbiAgICAvKiogTm9kZXMgdG8gZG93bmxvYWQuICovXHJcbiAgICAvLyB0c2xpbnQ6ZGlzYWJsZS1uZXh0LWxpbmU6bm8taW5wdXQtcmVuYW1lXHJcbiAgICBASW5wdXQoJ2FkZk5vZGVEb3dubG9hZCcpXHJcbiAgICBub2RlczogTm9kZUVudHJ5IHwgTm9kZUVudHJ5W107XHJcblxyXG4gICAgQEhvc3RMaXN0ZW5lcignY2xpY2snKVxyXG4gICAgb25DbGljaygpIHtcclxuICAgICAgICB0aGlzLmRvd25sb2FkTm9kZXModGhpcy5ub2Rlcyk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBkaWFsb2c6IE1hdERpYWxvZykge1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRG93bmxvYWRzIG11bHRpcGxlIHNlbGVjdGVkIG5vZGVzLlxyXG4gICAgICogUGFja3MgcmVzdWx0IGludG8gYSAuWklQIGFyY2hpdmUgaWYgdGhlcmUgaXMgbW9yZSB0aGFuIG9uZSBub2RlIHNlbGVjdGVkLlxyXG4gICAgICogQHBhcmFtIHNlbGVjdGlvbiBNdWx0aXBsZSBzZWxlY3RlZCBub2RlcyB0byBkb3dubG9hZFxyXG4gICAgICovXHJcbiAgICBkb3dubG9hZE5vZGVzKHNlbGVjdGlvbjogTm9kZUVudHJ5IHwgQXJyYXk8Tm9kZUVudHJ5Pikge1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMuaXNTZWxlY3Rpb25WYWxpZChzZWxlY3Rpb24pKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKHNlbGVjdGlvbiBpbnN0YW5jZW9mIEFycmF5KSB7XHJcbiAgICAgICAgICAgIGlmIChzZWxlY3Rpb24ubGVuZ3RoID09PSAxKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRvd25sb2FkTm9kZShzZWxlY3Rpb25bMF0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5kb3dubG9hZFppcChzZWxlY3Rpb24pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5kb3dubG9hZE5vZGUoc2VsZWN0aW9uKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEb3dubG9hZHMgYSBzaW5nbGUgbm9kZS5cclxuICAgICAqIFBhY2tzIHJlc3VsdCBpbnRvIGEgLlpJUCBhcmNoaXZlIGlzIHRoZSBub2RlIGlzIGEgRm9sZGVyLlxyXG4gICAgICogQHBhcmFtIG5vZGUgTm9kZSB0byBkb3dubG9hZFxyXG4gICAgICovXHJcbiAgICBkb3dubG9hZE5vZGUobm9kZTogTm9kZUVudHJ5KSB7XHJcbiAgICAgICAgaWYgKG5vZGUgJiYgbm9kZS5lbnRyeSkge1xyXG4gICAgICAgICAgICBjb25zdCBlbnRyeSA9IG5vZGUuZW50cnk7XHJcblxyXG4gICAgICAgICAgICBpZiAoZW50cnkuaXNGaWxlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRvd25sb2FkRmlsZShub2RlKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGVudHJ5LmlzRm9sZGVyKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRvd25sb2FkWmlwKFtub2RlXSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIENoZWNrIGlmIHRoZXJlJ3Mgbm9kZUlkIGZvciBTaGFyZWQgRmlsZXNcclxuICAgICAgICAgICAgaWYgKCFlbnRyeS5pc0ZpbGUgJiYgIWVudHJ5LmlzRm9sZGVyICYmICg8YW55PiBlbnRyeSkubm9kZUlkKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmRvd25sb2FkRmlsZShub2RlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGlzU2VsZWN0aW9uVmFsaWQoc2VsZWN0aW9uOiBOb2RlRW50cnkgfCBBcnJheTxOb2RlRW50cnk+KSB7XHJcbiAgICAgICAgcmV0dXJuIHNlbGVjdGlvbiB8fCAoc2VsZWN0aW9uIGluc3RhbmNlb2YgQXJyYXkgJiYgc2VsZWN0aW9uLmxlbmd0aCA+IDApO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZG93bmxvYWRGaWxlKG5vZGU6IE5vZGVFbnRyeSkge1xyXG4gICAgICAgIGlmIChub2RlICYmIG5vZGUuZW50cnkpIHtcclxuICAgICAgICAgICAgY29uc3QgY29udGVudEFwaSA9IHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmNvbnRlbnQ7XHJcbiAgICAgICAgICAgIC8vIG5vZGVJZCBmb3IgU2hhcmVkIG5vZGVcclxuICAgICAgICAgICAgY29uc3QgaWQgPSAoPGFueT4gbm9kZS5lbnRyeSkubm9kZUlkIHx8IG5vZGUuZW50cnkuaWQ7XHJcblxyXG4gICAgICAgICAgICBjb25zdCB1cmwgPSBjb250ZW50QXBpLmdldENvbnRlbnRVcmwoaWQsIHRydWUpO1xyXG4gICAgICAgICAgICBjb25zdCBmaWxlTmFtZSA9IG5vZGUuZW50cnkubmFtZTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZG93bmxvYWQodXJsLCBmaWxlTmFtZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZG93bmxvYWRaaXAoc2VsZWN0aW9uOiBBcnJheTxOb2RlRW50cnk+KSB7XHJcbiAgICAgICAgaWYgKHNlbGVjdGlvbiAmJiBzZWxlY3Rpb24ubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAvLyBub2RlSWQgZm9yIFNoYXJlZCBub2RlXHJcbiAgICAgICAgICAgIGNvbnN0IG5vZGVJZHMgPSBzZWxlY3Rpb24ubWFwKChub2RlOiBhbnkpID0+IChub2RlLmVudHJ5Lm5vZGVJZCB8fCBub2RlLmVudHJ5LmlkKSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmRpYWxvZy5vcGVuKERvd25sb2FkWmlwRGlhbG9nQ29tcG9uZW50LCB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogJzYwMHB4JyxcclxuICAgICAgICAgICAgICAgIGRpc2FibGVDbG9zZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIGRhdGE6IHtcclxuICAgICAgICAgICAgICAgICAgICBub2RlSWRzXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGRvd25sb2FkKHVybDogc3RyaW5nLCBmaWxlTmFtZTogc3RyaW5nKSB7XHJcbiAgICAgICAgaWYgKHVybCAmJiBmaWxlTmFtZSkge1xyXG4gICAgICAgICAgICBjb25zdCBsaW5rID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpO1xyXG5cclxuICAgICAgICAgICAgbGluay5zdHlsZS5kaXNwbGF5ID0gJ25vbmUnO1xyXG4gICAgICAgICAgICBsaW5rLmRvd25sb2FkID0gZmlsZU5hbWU7XHJcbiAgICAgICAgICAgIGxpbmsuaHJlZiA9IHVybDtcclxuXHJcbiAgICAgICAgICAgIGRvY3VtZW50LmJvZHkuYXBwZW5kQ2hpbGQobGluayk7XHJcbiAgICAgICAgICAgIGxpbmsuY2xpY2soKTtcclxuICAgICAgICAgICAgZG9jdW1lbnQuYm9keS5yZW1vdmVDaGlsZChsaW5rKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19