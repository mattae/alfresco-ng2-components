/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:no-input-rename  */
import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { from, forkJoin, of } from 'rxjs';
import { AlfrescoApiService } from '../services/alfresco-api.service';
import { catchError, map } from 'rxjs/operators';
var NodeFavoriteDirective = /** @class */ (function () {
    function NodeFavoriteDirective(alfrescoApiService) {
        this.alfrescoApiService = alfrescoApiService;
        this.favorites = [];
        /**
         * Array of nodes to toggle as favorites.
         */
        this.selection = [];
        /**
         * Emitted when the favorite setting is complete.
         */
        this.toggle = new EventEmitter();
        /**
         * Emitted when the favorite setting fails.
         */
        this.error = new EventEmitter();
    }
    /**
     * @return {?}
     */
    NodeFavoriteDirective.prototype.onClick = /**
     * @return {?}
     */
    function () {
        this.toggleFavorite();
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    NodeFavoriteDirective.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (!changes.selection.currentValue.length) {
            this.favorites = [];
            return;
        }
        this.markFavoritesNodes(changes.selection.currentValue);
    };
    /**
     * @return {?}
     */
    NodeFavoriteDirective.prototype.toggleFavorite = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.favorites.length) {
            return;
        }
        /** @type {?} */
        var every = this.favorites.every((/**
         * @param {?} selected
         * @return {?}
         */
        function (selected) { return selected.entry.isFavorite; }));
        if (every) {
            /** @type {?} */
            var batch = this.favorites.map((/**
             * @param {?} selected
             * @return {?}
             */
            function (selected) {
                // shared files have nodeId
                /** @type {?} */
                var id = ((/** @type {?} */ (selected))).entry.nodeId || selected.entry.id;
                return from(_this.alfrescoApiService.favoritesApi.removeFavoriteSite('-me-', id));
            }));
            forkJoin(batch).subscribe((/**
             * @return {?}
             */
            function () {
                _this.favorites.map((/**
                 * @param {?} selected
                 * @return {?}
                 */
                function (selected) { return selected.entry.isFavorite = false; }));
                _this.toggle.emit();
            }), (/**
             * @param {?} error
             * @return {?}
             */
            function (error) { return _this.error.emit(error); }));
        }
        if (!every) {
            /** @type {?} */
            var notFavorite_1 = this.favorites.filter((/**
             * @param {?} node
             * @return {?}
             */
            function (node) { return !node.entry.isFavorite; }));
            /** @type {?} */
            var body = notFavorite_1.map((/**
             * @param {?} node
             * @return {?}
             */
            function (node) { return _this.createFavoriteBody(node); }));
            from(this.alfrescoApiService.favoritesApi.addFavorite('-me-', (/** @type {?} */ (body))))
                .subscribe((/**
             * @return {?}
             */
            function () {
                notFavorite_1.map((/**
                 * @param {?} selected
                 * @return {?}
                 */
                function (selected) { return selected.entry.isFavorite = true; }));
                _this.toggle.emit();
            }), (/**
             * @param {?} error
             * @return {?}
             */
            function (error) { return _this.error.emit(error); }));
        }
    };
    /**
     * @param {?} selection
     * @return {?}
     */
    NodeFavoriteDirective.prototype.markFavoritesNodes = /**
     * @param {?} selection
     * @return {?}
     */
    function (selection) {
        var _this = this;
        if (selection.length <= this.favorites.length) {
            /** @type {?} */
            var newFavorites = this.reduce(this.favorites, selection);
            this.favorites = newFavorites;
        }
        /** @type {?} */
        var result = this.diff(selection, this.favorites);
        /** @type {?} */
        var batch = this.getProcessBatch(result);
        forkJoin(batch).subscribe((/**
         * @param {?} data
         * @return {?}
         */
        function (data) {
            var _a;
            (_a = _this.favorites).push.apply(_a, tslib_1.__spread(data));
        }));
    };
    /**
     * @return {?}
     */
    NodeFavoriteDirective.prototype.hasFavorites = /**
     * @return {?}
     */
    function () {
        if (this.favorites && !this.favorites.length) {
            return false;
        }
        return this.favorites.every((/**
         * @param {?} selected
         * @return {?}
         */
        function (selected) { return selected.entry.isFavorite; }));
    };
    /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    NodeFavoriteDirective.prototype.getProcessBatch = /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    function (selection) {
        var _this = this;
        return selection.map((/**
         * @param {?} selected
         * @return {?}
         */
        function (selected) { return _this.getFavorite(selected); }));
    };
    /**
     * @private
     * @param {?} selected
     * @return {?}
     */
    NodeFavoriteDirective.prototype.getFavorite = /**
     * @private
     * @param {?} selected
     * @return {?}
     */
    function (selected) {
        /** @type {?} */
        var node = selected.entry;
        // ACS 6.x with 'isFavorite' include
        if (node && node.hasOwnProperty('isFavorite')) {
            return of(selected);
        }
        // ACS 5.x and 6.x without 'isFavorite' include
        var _a = (/** @type {?} */ (node)), name = _a.name, isFile = _a.isFile, isFolder = _a.isFolder;
        /** @type {?} */
        var id = ((/** @type {?} */ (node))).nodeId || node.id;
        /** @type {?} */
        var promise = this.alfrescoApiService.favoritesApi.getFavorite('-me-', id);
        return from(promise).pipe(map((/**
         * @return {?}
         */
        function () { return ({
            entry: {
                id: id,
                isFolder: isFolder,
                isFile: isFile,
                name: name,
                isFavorite: true
            }
        }); })), catchError((/**
         * @return {?}
         */
        function () {
            return of({
                entry: {
                    id: id,
                    isFolder: isFolder,
                    isFile: isFile,
                    name: name,
                    isFavorite: false
                }
            });
        })));
    };
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    NodeFavoriteDirective.prototype.createFavoriteBody = /**
     * @private
     * @param {?} node
     * @return {?}
     */
    function (node) {
        var _a;
        /** @type {?} */
        var type = this.getNodeType(node);
        // shared files have nodeId
        /** @type {?} */
        var id = node.entry.nodeId || node.entry.id;
        return {
            target: (_a = {},
                _a[type] = {
                    guid: id
                },
                _a)
        };
    };
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    NodeFavoriteDirective.prototype.getNodeType = /**
     * @private
     * @param {?} node
     * @return {?}
     */
    function (node) {
        // shared could only be files
        if (!node.entry.isFile && !node.entry.isFolder) {
            return 'file';
        }
        return node.entry.isFile ? 'file' : 'folder';
    };
    /**
     * @private
     * @param {?} list
     * @param {?} patch
     * @return {?}
     */
    NodeFavoriteDirective.prototype.diff = /**
     * @private
     * @param {?} list
     * @param {?} patch
     * @return {?}
     */
    function (list, patch) {
        /** @type {?} */
        var ids = patch.map((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.entry.id; }));
        return list.filter((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return ids.includes(item.entry.id) ? null : item; }));
    };
    /**
     * @private
     * @param {?} patch
     * @param {?} comparator
     * @return {?}
     */
    NodeFavoriteDirective.prototype.reduce = /**
     * @private
     * @param {?} patch
     * @param {?} comparator
     * @return {?}
     */
    function (patch, comparator) {
        /** @type {?} */
        var ids = comparator.map((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.entry.id; }));
        return patch.filter((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return ids.includes(item.entry.id) ? item : null; }));
    };
    NodeFavoriteDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[adf-node-favorite]',
                    exportAs: 'adfFavorite'
                },] }
    ];
    /** @nocollapse */
    NodeFavoriteDirective.ctorParameters = function () { return [
        { type: AlfrescoApiService }
    ]; };
    NodeFavoriteDirective.propDecorators = {
        selection: [{ type: Input, args: ['adf-node-favorite',] }],
        toggle: [{ type: Output }],
        error: [{ type: Output }],
        onClick: [{ type: HostListener, args: ['click',] }]
    };
    return NodeFavoriteDirective;
}());
export { NodeFavoriteDirective };
if (false) {
    /** @type {?} */
    NodeFavoriteDirective.prototype.favorites;
    /**
     * Array of nodes to toggle as favorites.
     * @type {?}
     */
    NodeFavoriteDirective.prototype.selection;
    /**
     * Emitted when the favorite setting is complete.
     * @type {?}
     */
    NodeFavoriteDirective.prototype.toggle;
    /**
     * Emitted when the favorite setting fails.
     * @type {?}
     */
    NodeFavoriteDirective.prototype.error;
    /**
     * @type {?}
     * @private
     */
    NodeFavoriteDirective.prototype.alfrescoApiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9kZS1mYXZvcml0ZS5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJkaXJlY3RpdmVzL25vZGUtZmF2b3JpdGUuZGlyZWN0aXZlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBYSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFaEcsT0FBTyxFQUFjLElBQUksRUFBRSxRQUFRLEVBQUUsRUFBRSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3RELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3RFLE9BQU8sRUFBRSxVQUFVLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFakQ7SUFzQkksK0JBQW9CLGtCQUFzQztRQUF0Qyx1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBakIxRCxjQUFTLEdBQVUsRUFBRSxDQUFDOzs7O1FBSXRCLGNBQVMsR0FBZ0IsRUFBRSxDQUFDOzs7O1FBR2xCLFdBQU0sR0FBc0IsSUFBSSxZQUFZLEVBQUUsQ0FBQzs7OztRQUcvQyxVQUFLLEdBQXNCLElBQUksWUFBWSxFQUFFLENBQUM7SUFReEQsQ0FBQzs7OztJQUxELHVDQUFPOzs7SUFEUDtRQUVJLElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUMxQixDQUFDOzs7OztJQUtELDJDQUFXOzs7O0lBQVgsVUFBWSxPQUFPO1FBQ2YsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRTtZQUN4QyxJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztZQUVwQixPQUFPO1NBQ1Y7UUFFRCxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUM1RCxDQUFDOzs7O0lBRUQsOENBQWM7OztJQUFkO1FBQUEsaUJBcUNDO1FBcENHLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRTtZQUN4QixPQUFPO1NBQ1Y7O1lBRUssS0FBSyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSzs7OztRQUFDLFVBQUMsUUFBUSxJQUFLLE9BQUEsUUFBUSxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQXpCLENBQXlCLEVBQUM7UUFFM0UsSUFBSSxLQUFLLEVBQUU7O2dCQUNELEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUc7Ozs7WUFBQyxVQUFDLFFBQXFDOzs7b0JBRTdELEVBQUUsR0FBRyxDQUFDLG1CQUFrQixRQUFRLEVBQUEsQ0FBQyxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFO2dCQUV6RSxPQUFPLElBQUksQ0FBQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLGtCQUFrQixDQUFDLE1BQU0sRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3JGLENBQUMsRUFBQztZQUVGLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxTQUFTOzs7WUFDckI7Z0JBQ0ksS0FBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHOzs7O2dCQUFDLFVBQUMsUUFBUSxJQUFLLE9BQUEsUUFBUSxDQUFDLEtBQUssQ0FBQyxVQUFVLEdBQUcsS0FBSyxFQUFqQyxDQUFpQyxFQUFDLENBQUM7Z0JBQ3BFLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDdkIsQ0FBQzs7OztZQUNELFVBQUMsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQXRCLENBQXNCLEVBQ3BDLENBQUM7U0FDTDtRQUVELElBQUksQ0FBQyxLQUFLLEVBQUU7O2dCQUNGLGFBQVcsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU07Ozs7WUFBQyxVQUFDLElBQUksSUFBSyxPQUFBLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQXRCLENBQXNCLEVBQUM7O2dCQUNyRSxJQUFJLEdBQW1CLGFBQVcsQ0FBQyxHQUFHOzs7O1lBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxLQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLEVBQTdCLENBQTZCLEVBQUM7WUFFckYsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxZQUFZLENBQUMsV0FBVyxDQUFDLE1BQU0sRUFBRSxtQkFBTSxJQUFJLEVBQUEsQ0FBQyxDQUFDO2lCQUNyRSxTQUFTOzs7WUFDTjtnQkFDSSxhQUFXLENBQUMsR0FBRzs7OztnQkFBQyxVQUFDLFFBQVEsSUFBSyxPQUFBLFFBQVEsQ0FBQyxLQUFLLENBQUMsVUFBVSxHQUFHLElBQUksRUFBaEMsQ0FBZ0MsRUFBQyxDQUFDO2dCQUNoRSxLQUFJLENBQUMsTUFBTSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ3ZCLENBQUM7Ozs7WUFDRCxVQUFDLEtBQUssSUFBSyxPQUFBLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUF0QixDQUFzQixFQUNwQyxDQUFDO1NBQ1Q7SUFDTCxDQUFDOzs7OztJQUVELGtEQUFrQjs7OztJQUFsQixVQUFtQixTQUFzQjtRQUF6QyxpQkFZQztRQVhHLElBQUksU0FBUyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sRUFBRTs7Z0JBQ3JDLFlBQVksR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUUsU0FBUyxDQUFDO1lBQzNELElBQUksQ0FBQyxTQUFTLEdBQUcsWUFBWSxDQUFDO1NBQ2pDOztZQUVLLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDOztZQUM3QyxLQUFLLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUM7UUFFMUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFDLElBQUk7O1lBQzNCLENBQUEsS0FBQSxLQUFJLENBQUMsU0FBUyxDQUFBLENBQUMsSUFBSSw0QkFBSSxJQUFJLEdBQUU7UUFDakMsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsNENBQVk7OztJQUFaO1FBQ0ksSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUU7WUFDMUMsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsS0FBSzs7OztRQUFDLFVBQUMsUUFBUSxJQUFLLE9BQUEsUUFBUSxDQUFDLEtBQUssQ0FBQyxVQUFVLEVBQXpCLENBQXlCLEVBQUMsQ0FBQztJQUN6RSxDQUFDOzs7Ozs7SUFFTywrQ0FBZTs7Ozs7SUFBdkIsVUFBd0IsU0FBUztRQUFqQyxpQkFFQztRQURHLE9BQU8sU0FBUyxDQUFDLEdBQUc7Ozs7UUFBQyxVQUFDLFFBQW1CLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLFFBQVEsQ0FBQyxFQUExQixDQUEwQixFQUFDLENBQUM7SUFDOUUsQ0FBQzs7Ozs7O0lBRU8sMkNBQVc7Ozs7O0lBQW5CLFVBQW9CLFFBQXFDOztZQUMvQyxJQUFJLEdBQXNCLFFBQVEsQ0FBQyxLQUFLO1FBRTlDLG9DQUFvQztRQUNwQyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxFQUFFO1lBQzNDLE9BQU8sRUFBRSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1NBQ3ZCOztRQUdLLElBQUEsOEJBQXdDLEVBQXRDLGNBQUksRUFBRSxrQkFBTSxFQUFFLHNCQUF3Qjs7WUFDeEMsRUFBRSxHQUFJLENBQUMsbUJBQWEsSUFBSSxFQUFBLENBQUMsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLEVBQUU7O1lBRTNDLE9BQU8sR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsWUFBWSxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsRUFBRSxDQUFDO1FBRTVFLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FDckIsR0FBRzs7O1FBQUMsY0FBTSxPQUFBLENBQUM7WUFDUCxLQUFLLEVBQUU7Z0JBQ0gsRUFBRSxJQUFBO2dCQUNGLFFBQVEsVUFBQTtnQkFDUixNQUFNLFFBQUE7Z0JBQ04sSUFBSSxNQUFBO2dCQUNKLFVBQVUsRUFBRSxJQUFJO2FBQ25CO1NBQ0osQ0FBQyxFQVJRLENBUVIsRUFBQyxFQUNILFVBQVU7OztRQUFDO1lBQ1AsT0FBTyxFQUFFLENBQUM7Z0JBQ04sS0FBSyxFQUFFO29CQUNILEVBQUUsSUFBQTtvQkFDRixRQUFRLFVBQUE7b0JBQ1IsTUFBTSxRQUFBO29CQUNOLElBQUksTUFBQTtvQkFDSixVQUFVLEVBQUUsS0FBSztpQkFDcEI7YUFDSixDQUFDLENBQUM7UUFDUCxDQUFDLEVBQUMsQ0FDTCxDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBRU8sa0RBQWtCOzs7OztJQUExQixVQUEyQixJQUFJOzs7WUFDckIsSUFBSSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDOzs7WUFFN0IsRUFBRSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsRUFBRTtRQUU3QyxPQUFPO1lBQ0gsTUFBTTtnQkFDRixHQUFDLElBQUksSUFBRztvQkFDSixJQUFJLEVBQUUsRUFBRTtpQkFDWDttQkFDSjtTQUNKLENBQUM7SUFDTixDQUFDOzs7Ozs7SUFFTywyQ0FBVzs7Ozs7SUFBbkIsVUFBb0IsSUFBSTtRQUNwQiw2QkFBNkI7UUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUU7WUFDNUMsT0FBTyxNQUFNLENBQUM7U0FDakI7UUFFRCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLFFBQVEsQ0FBQztJQUNqRCxDQUFDOzs7Ozs7O0lBRU8sb0NBQUk7Ozs7OztJQUFaLFVBQWEsSUFBSSxFQUFFLEtBQUs7O1lBQ2QsR0FBRyxHQUFHLEtBQUssQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBYixDQUFhLEVBQUM7UUFFOUMsT0FBTyxJQUFJLENBQUMsTUFBTTs7OztRQUFDLFVBQUMsSUFBSSxJQUFLLE9BQUEsR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBekMsQ0FBeUMsRUFBQyxDQUFDO0lBQzVFLENBQUM7Ozs7Ozs7SUFFTyxzQ0FBTTs7Ozs7O0lBQWQsVUFBZSxLQUFLLEVBQUUsVUFBVTs7WUFDdEIsR0FBRyxHQUFHLFVBQVUsQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBYixDQUFhLEVBQUM7UUFFbkQsT0FBTyxLQUFLLENBQUMsTUFBTTs7OztRQUFDLFVBQUMsSUFBSSxJQUFLLE9BQUEsR0FBRyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksRUFBekMsQ0FBeUMsRUFBQyxDQUFDO0lBQzdFLENBQUM7O2dCQTNLSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLHFCQUFxQjtvQkFDL0IsUUFBUSxFQUFFLGFBQWE7aUJBQzFCOzs7O2dCQU5RLGtCQUFrQjs7OzRCQVd0QixLQUFLLFNBQUMsbUJBQW1CO3lCQUl6QixNQUFNO3dCQUdOLE1BQU07MEJBRU4sWUFBWSxTQUFDLE9BQU87O0lBMkp6Qiw0QkFBQztDQUFBLEFBNUtELElBNEtDO1NBeEtZLHFCQUFxQjs7O0lBQzlCLDBDQUFzQjs7Ozs7SUFHdEIsMENBQzRCOzs7OztJQUc1Qix1Q0FBeUQ7Ozs7O0lBR3pELHNDQUF3RDs7Ozs7SUFPNUMsbURBQThDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOm5vLWlucHV0LXJlbmFtZSAgKi9cclxuXHJcbmltcG9ydCB7IERpcmVjdGl2ZSwgRXZlbnRFbWl0dGVyLCBIb3N0TGlzdGVuZXIsIElucHV0LCBPbkNoYW5nZXMsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGYXZvcml0ZUJvZHksIE5vZGVFbnRyeSwgU2hhcmVkTGlua0VudHJ5LCBOb2RlLCBTaGFyZWRMaW5rIH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZyb20sIGZvcmtKb2luLCBvZiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IGNhdGNoRXJyb3IsIG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbYWRmLW5vZGUtZmF2b3JpdGVdJyxcclxuICAgIGV4cG9ydEFzOiAnYWRmRmF2b3JpdGUnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOb2RlRmF2b3JpdGVEaXJlY3RpdmUgaW1wbGVtZW50cyBPbkNoYW5nZXMge1xyXG4gICAgZmF2b3JpdGVzOiBhbnlbXSA9IFtdO1xyXG5cclxuICAgIC8qKiBBcnJheSBvZiBub2RlcyB0byB0b2dnbGUgYXMgZmF2b3JpdGVzLiAqL1xyXG4gICAgQElucHV0KCdhZGYtbm9kZS1mYXZvcml0ZScpXHJcbiAgICBzZWxlY3Rpb246IE5vZGVFbnRyeVtdID0gW107XHJcblxyXG4gICAgLyoqIEVtaXR0ZWQgd2hlbiB0aGUgZmF2b3JpdGUgc2V0dGluZyBpcyBjb21wbGV0ZS4gKi9cclxuICAgIEBPdXRwdXQoKSB0b2dnbGU6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIGZhdm9yaXRlIHNldHRpbmcgZmFpbHMuICovXHJcbiAgICBAT3V0cHV0KCkgZXJyb3I6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2NsaWNrJylcclxuICAgIG9uQ2xpY2soKSB7XHJcbiAgICAgICAgdGhpcy50b2dnbGVGYXZvcml0ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYWxmcmVzY29BcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzKSB7XHJcbiAgICAgICAgaWYgKCFjaGFuZ2VzLnNlbGVjdGlvbi5jdXJyZW50VmFsdWUubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZmF2b3JpdGVzID0gW107XHJcblxyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLm1hcmtGYXZvcml0ZXNOb2RlcyhjaGFuZ2VzLnNlbGVjdGlvbi5jdXJyZW50VmFsdWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHRvZ2dsZUZhdm9yaXRlKCkge1xyXG4gICAgICAgIGlmICghdGhpcy5mYXZvcml0ZXMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGV2ZXJ5ID0gdGhpcy5mYXZvcml0ZXMuZXZlcnkoKHNlbGVjdGVkKSA9PiBzZWxlY3RlZC5lbnRyeS5pc0Zhdm9yaXRlKTtcclxuXHJcbiAgICAgICAgaWYgKGV2ZXJ5KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGJhdGNoID0gdGhpcy5mYXZvcml0ZXMubWFwKChzZWxlY3RlZDogTm9kZUVudHJ5IHwgU2hhcmVkTGlua0VudHJ5KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAvLyBzaGFyZWQgZmlsZXMgaGF2ZSBub2RlSWRcclxuICAgICAgICAgICAgICAgIGNvbnN0IGlkID0gKDxTaGFyZWRMaW5rRW50cnk+IHNlbGVjdGVkKS5lbnRyeS5ub2RlSWQgfHwgc2VsZWN0ZWQuZW50cnkuaWQ7XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZmF2b3JpdGVzQXBpLnJlbW92ZUZhdm9yaXRlU2l0ZSgnLW1lLScsIGlkKSk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgZm9ya0pvaW4oYmF0Y2gpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZhdm9yaXRlcy5tYXAoKHNlbGVjdGVkKSA9PiBzZWxlY3RlZC5lbnRyeS5pc0Zhdm9yaXRlID0gZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudG9nZ2xlLmVtaXQoKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAoZXJyb3IpID0+IHRoaXMuZXJyb3IuZW1pdChlcnJvcilcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghZXZlcnkpIHtcclxuICAgICAgICAgICAgY29uc3Qgbm90RmF2b3JpdGUgPSB0aGlzLmZhdm9yaXRlcy5maWx0ZXIoKG5vZGUpID0+ICFub2RlLmVudHJ5LmlzRmF2b3JpdGUpO1xyXG4gICAgICAgICAgICBjb25zdCBib2R5OiBGYXZvcml0ZUJvZHlbXSA9IG5vdEZhdm9yaXRlLm1hcCgobm9kZSkgPT4gdGhpcy5jcmVhdGVGYXZvcml0ZUJvZHkobm9kZSkpO1xyXG5cclxuICAgICAgICAgICAgZnJvbSh0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5mYXZvcml0ZXNBcGkuYWRkRmF2b3JpdGUoJy1tZS0nLCA8YW55PiBib2R5KSlcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBub3RGYXZvcml0ZS5tYXAoKHNlbGVjdGVkKSA9PiBzZWxlY3RlZC5lbnRyeS5pc0Zhdm9yaXRlID0gdHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudG9nZ2xlLmVtaXQoKTtcclxuICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgIChlcnJvcikgPT4gdGhpcy5lcnJvci5lbWl0KGVycm9yKVxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbWFya0Zhdm9yaXRlc05vZGVzKHNlbGVjdGlvbjogTm9kZUVudHJ5W10pIHtcclxuICAgICAgICBpZiAoc2VsZWN0aW9uLmxlbmd0aCA8PSB0aGlzLmZhdm9yaXRlcy5sZW5ndGgpIHtcclxuICAgICAgICAgICAgY29uc3QgbmV3RmF2b3JpdGVzID0gdGhpcy5yZWR1Y2UodGhpcy5mYXZvcml0ZXMsIHNlbGVjdGlvbik7XHJcbiAgICAgICAgICAgIHRoaXMuZmF2b3JpdGVzID0gbmV3RmF2b3JpdGVzO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gdGhpcy5kaWZmKHNlbGVjdGlvbiwgdGhpcy5mYXZvcml0ZXMpO1xyXG4gICAgICAgIGNvbnN0IGJhdGNoID0gdGhpcy5nZXRQcm9jZXNzQmF0Y2gocmVzdWx0KTtcclxuXHJcbiAgICAgICAgZm9ya0pvaW4oYmF0Y2gpLnN1YnNjcmliZSgoZGF0YSkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZhdm9yaXRlcy5wdXNoKC4uLmRhdGEpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc0Zhdm9yaXRlcygpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodGhpcy5mYXZvcml0ZXMgJiYgIXRoaXMuZmF2b3JpdGVzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5mYXZvcml0ZXMuZXZlcnkoKHNlbGVjdGVkKSA9PiBzZWxlY3RlZC5lbnRyeS5pc0Zhdm9yaXRlKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldFByb2Nlc3NCYXRjaChzZWxlY3Rpb24pOiBhbnlbXSB7XHJcbiAgICAgICAgcmV0dXJuIHNlbGVjdGlvbi5tYXAoKHNlbGVjdGVkOiBOb2RlRW50cnkpID0+IHRoaXMuZ2V0RmF2b3JpdGUoc2VsZWN0ZWQpKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldEZhdm9yaXRlKHNlbGVjdGVkOiBOb2RlRW50cnkgfCBTaGFyZWRMaW5rRW50cnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IG5vZGU6IE5vZGUgfCBTaGFyZWRMaW5rID0gc2VsZWN0ZWQuZW50cnk7XHJcblxyXG4gICAgICAgIC8vIEFDUyA2Lnggd2l0aCAnaXNGYXZvcml0ZScgaW5jbHVkZVxyXG4gICAgICAgIGlmIChub2RlICYmIG5vZGUuaGFzT3duUHJvcGVydHkoJ2lzRmF2b3JpdGUnKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gb2Yoc2VsZWN0ZWQpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gQUNTIDUueCBhbmQgNi54IHdpdGhvdXQgJ2lzRmF2b3JpdGUnIGluY2x1ZGVcclxuICAgICAgICBjb25zdCB7IG5hbWUsIGlzRmlsZSwgaXNGb2xkZXIgfSA9IDxOb2RlPiBub2RlO1xyXG4gICAgICAgIGNvbnN0IGlkID0gICg8U2hhcmVkTGluaz4gbm9kZSkubm9kZUlkIHx8IG5vZGUuaWQ7XHJcblxyXG4gICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5mYXZvcml0ZXNBcGkuZ2V0RmF2b3JpdGUoJy1tZS0nLCBpZCk7XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHByb21pc2UpLnBpcGUoXHJcbiAgICAgICAgICAgIG1hcCgoKSA9PiAoe1xyXG4gICAgICAgICAgICAgICAgZW50cnk6IHtcclxuICAgICAgICAgICAgICAgICAgICBpZCxcclxuICAgICAgICAgICAgICAgICAgICBpc0ZvbGRlcixcclxuICAgICAgICAgICAgICAgICAgICBpc0ZpbGUsXHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZSxcclxuICAgICAgICAgICAgICAgICAgICBpc0Zhdm9yaXRlOiB0cnVlXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pKSxcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gb2Yoe1xyXG4gICAgICAgICAgICAgICAgICAgIGVudHJ5OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0ZvbGRlcixcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNGaWxlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpc0Zhdm9yaXRlOiBmYWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBjcmVhdGVGYXZvcml0ZUJvZHkobm9kZSk6IEZhdm9yaXRlQm9keSB7XHJcbiAgICAgICAgY29uc3QgdHlwZSA9IHRoaXMuZ2V0Tm9kZVR5cGUobm9kZSk7XHJcbiAgICAgICAgLy8gc2hhcmVkIGZpbGVzIGhhdmUgbm9kZUlkXHJcbiAgICAgICAgY29uc3QgaWQgPSBub2RlLmVudHJ5Lm5vZGVJZCB8fCBub2RlLmVudHJ5LmlkO1xyXG5cclxuICAgICAgICByZXR1cm4ge1xyXG4gICAgICAgICAgICB0YXJnZXQ6IHtcclxuICAgICAgICAgICAgICAgIFt0eXBlXToge1xyXG4gICAgICAgICAgICAgICAgICAgIGd1aWQ6IGlkXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0Tm9kZVR5cGUobm9kZSk6IHN0cmluZyB7XHJcbiAgICAgICAgLy8gc2hhcmVkIGNvdWxkIG9ubHkgYmUgZmlsZXNcclxuICAgICAgICBpZiAoIW5vZGUuZW50cnkuaXNGaWxlICYmICFub2RlLmVudHJ5LmlzRm9sZGVyKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAnZmlsZSc7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gbm9kZS5lbnRyeS5pc0ZpbGUgPyAnZmlsZScgOiAnZm9sZGVyJztcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGRpZmYobGlzdCwgcGF0Y2gpOiBhbnlbXSB7XHJcbiAgICAgICAgY29uc3QgaWRzID0gcGF0Y2gubWFwKChpdGVtKSA9PiBpdGVtLmVudHJ5LmlkKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGxpc3QuZmlsdGVyKChpdGVtKSA9PiBpZHMuaW5jbHVkZXMoaXRlbS5lbnRyeS5pZCkgPyBudWxsIDogaXRlbSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSByZWR1Y2UocGF0Y2gsIGNvbXBhcmF0b3IpOiBhbnlbXSB7XHJcbiAgICAgICAgY29uc3QgaWRzID0gY29tcGFyYXRvci5tYXAoKGl0ZW0pID0+IGl0ZW0uZW50cnkuaWQpO1xyXG5cclxuICAgICAgICByZXR1cm4gcGF0Y2guZmlsdGVyKChpdGVtKSA9PiBpZHMuaW5jbHVkZXMoaXRlbS5lbnRyeS5pZCkgPyBpdGVtIDogbnVsbCk7XHJcbiAgICB9XHJcbn1cclxuIl19