/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector no-input-rename */
import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { forkJoin, from, of } from 'rxjs';
import { AlfrescoApiService } from '../services/alfresco-api.service';
import { TranslationService } from '../services/translation.service';
import { tap, mergeMap, map, catchError } from 'rxjs/operators';
var RestoreMessageModel = /** @class */ (function () {
    function RestoreMessageModel() {
    }
    return RestoreMessageModel;
}());
export { RestoreMessageModel };
if (false) {
    /** @type {?} */
    RestoreMessageModel.prototype.message;
    /** @type {?} */
    RestoreMessageModel.prototype.path;
    /** @type {?} */
    RestoreMessageModel.prototype.action;
}
var NodeRestoreDirective = /** @class */ (function () {
    function NodeRestoreDirective(alfrescoApiService, translation) {
        this.alfrescoApiService = alfrescoApiService;
        this.translation = translation;
        /**
         * Emitted when restoration is complete.
         */
        this.restore = new EventEmitter();
        this.restoreProcessStatus = this.processStatus();
    }
    /**
     * @return {?}
     */
    NodeRestoreDirective.prototype.onClick = /**
     * @return {?}
     */
    function () {
        this.recover(this.selection);
    };
    /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    NodeRestoreDirective.prototype.recover = /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    function (selection) {
        var _this = this;
        var _a;
        if (!selection.length) {
            return;
        }
        /** @type {?} */
        var nodesWithPath = this.getNodesWithPath(selection);
        if (selection.length && nodesWithPath.length) {
            this.restoreNodesBatch(nodesWithPath).pipe(tap((/**
             * @param {?} restoredNodes
             * @return {?}
             */
            function (restoredNodes) {
                var _a, _b;
                /** @type {?} */
                var status = _this.processStatus(restoredNodes);
                (_a = _this.restoreProcessStatus.fail).push.apply(_a, tslib_1.__spread(status.fail));
                (_b = _this.restoreProcessStatus.success).push.apply(_b, tslib_1.__spread(status.success));
            })), mergeMap((/**
             * @return {?}
             */
            function () { return _this.getDeletedNodes(); })))
                .subscribe((/**
             * @param {?} deletedNodesList
             * @return {?}
             */
            function (deletedNodesList) {
                var nodeList = deletedNodesList.list.entries;
                var restoreErrorNodes = _this.restoreProcessStatus.fail;
                /** @type {?} */
                var selectedNodes = _this.diff(restoreErrorNodes, selection, false);
                /** @type {?} */
                var remainingNodes = _this.diff(selectedNodes, nodeList);
                if (!remainingNodes.length) {
                    _this.notification();
                }
                else {
                    _this.recover(remainingNodes);
                }
            }));
        }
        else {
            (_a = this.restoreProcessStatus.fail).push.apply(_a, tslib_1.__spread(selection));
            this.notification();
            return;
        }
    };
    /**
     * @private
     * @param {?} batch
     * @return {?}
     */
    NodeRestoreDirective.prototype.restoreNodesBatch = /**
     * @private
     * @param {?} batch
     * @return {?}
     */
    function (batch) {
        var _this = this;
        return forkJoin(batch.map((/**
         * @param {?} node
         * @return {?}
         */
        function (node) { return _this.restoreNode(node); })));
    };
    /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    NodeRestoreDirective.prototype.getNodesWithPath = /**
     * @private
     * @param {?} selection
     * @return {?}
     */
    function (selection) {
        return selection.filter((/**
         * @param {?} node
         * @return {?}
         */
        function (node) { return node.entry.path; }));
    };
    /**
     * @private
     * @return {?}
     */
    NodeRestoreDirective.prototype.getDeletedNodes = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var promise = this.alfrescoApiService.getInstance()
            .core.nodesApi.getDeletedNodes({ include: ['path'] });
        return from(promise);
    };
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    NodeRestoreDirective.prototype.restoreNode = /**
     * @private
     * @param {?} node
     * @return {?}
     */
    function (node) {
        var entry = node.entry;
        /** @type {?} */
        var promise = this.alfrescoApiService.getInstance().nodes.restoreNode(entry.id);
        return from(promise).pipe(map((/**
         * @return {?}
         */
        function () { return ({
            status: 1,
            entry: entry
        }); })), catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            var statusCode = (JSON.parse(error.message)).error.statusCode;
            return of({
                status: 0,
                statusCode: statusCode,
                entry: entry
            });
        })));
    };
    /**
     * @private
     * @param {?} selection
     * @param {?} list
     * @param {?=} fromList
     * @return {?}
     */
    NodeRestoreDirective.prototype.diff = /**
     * @private
     * @param {?} selection
     * @param {?} list
     * @param {?=} fromList
     * @return {?}
     */
    function (selection, list, fromList) {
        if (fromList === void 0) { fromList = true; }
        /** @type {?} */
        var ids = selection.map((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.entry.id; }));
        return list.filter((/**
         * @param {?} item
         * @return {?}
         */
        function (item) {
            if (fromList) {
                return ids.includes(item.entry.id) ? item : null;
            }
            else {
                return !ids.includes(item.entry.id) ? item : null;
            }
        }));
    };
    /**
     * @private
     * @param {?=} data
     * @return {?}
     */
    NodeRestoreDirective.prototype.processStatus = /**
     * @private
     * @param {?=} data
     * @return {?}
     */
    function (data) {
        if (data === void 0) { data = []; }
        /** @type {?} */
        var status = {
            fail: [],
            success: [],
            /**
             * @return {?}
             */
            get someFailed() {
                return !!(this.fail.length);
            },
            /**
             * @return {?}
             */
            get someSucceeded() {
                return !!(this.success.length);
            },
            /**
             * @return {?}
             */
            get oneFailed() {
                return this.fail.length === 1;
            },
            /**
             * @return {?}
             */
            get oneSucceeded() {
                return this.success.length === 1;
            },
            /**
             * @return {?}
             */
            get allSucceeded() {
                return this.someSucceeded && !this.someFailed;
            },
            /**
             * @return {?}
             */
            get allFailed() {
                return this.someFailed && !this.someSucceeded;
            },
            reset: /**
             * @return {?}
             */
            function () {
                this.fail = [];
                this.success = [];
            }
        };
        return data.reduce((/**
         * @param {?} acc
         * @param {?} node
         * @return {?}
         */
        function (acc, node) {
            if (node.status) {
                acc.success.push(node);
            }
            else {
                acc.fail.push(node);
            }
            return acc;
        }), status);
    };
    /**
     * @private
     * @return {?}
     */
    NodeRestoreDirective.prototype.getRestoreMessage = /**
     * @private
     * @return {?}
     */
    function () {
        var status = this.restoreProcessStatus;
        if (status.someFailed && !status.oneFailed) {
            return this.translation.instant('CORE.RESTORE_NODE.PARTIAL_PLURAL', {
                number: status.fail.length
            });
        }
        if (status.oneFailed && status.fail[0].statusCode) {
            if (status.fail[0].statusCode === 409) {
                return this.translation.instant('CORE.RESTORE_NODE.NODE_EXISTS', {
                    name: status.fail[0].entry.name
                });
            }
            else {
                return this.translation.instant('CORE.RESTORE_NODE.GENERIC', {
                    name: status.fail[0].entry.name
                });
            }
        }
        if (status.oneFailed && !status.fail[0].statusCode) {
            return this.translation.instant('CORE.RESTORE_NODE.LOCATION_MISSING', {
                name: status.fail[0].entry.name
            });
        }
        if (status.allSucceeded && !status.oneSucceeded) {
            return this.translation.instant('CORE.RESTORE_NODE.PLURAL');
        }
        if (status.allSucceeded && status.oneSucceeded) {
            return this.translation.instant('CORE.RESTORE_NODE.SINGULAR', {
                name: status.success[0].entry.name
            });
        }
    };
    /**
     * @private
     * @return {?}
     */
    NodeRestoreDirective.prototype.notification = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var status = Object.assign({}, this.restoreProcessStatus);
        /** @type {?} */
        var message = this.getRestoreMessage();
        this.reset();
        /** @type {?} */
        var action = (status.oneSucceeded && !status.someFailed) ? this.translation.instant('CORE.RESTORE_NODE.VIEW') : '';
        /** @type {?} */
        var path;
        if (status.success && status.success.length > 0) {
            path = status.success[0].entry.path;
        }
        this.restore.emit({
            message: message,
            action: action,
            path: path
        });
    };
    /**
     * @private
     * @return {?}
     */
    NodeRestoreDirective.prototype.reset = /**
     * @private
     * @return {?}
     */
    function () {
        this.restoreProcessStatus.reset();
        this.selection = [];
    };
    NodeRestoreDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[adf-restore]'
                },] }
    ];
    /** @nocollapse */
    NodeRestoreDirective.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: TranslationService }
    ]; };
    NodeRestoreDirective.propDecorators = {
        selection: [{ type: Input, args: ['adf-restore',] }],
        restore: [{ type: Output }],
        onClick: [{ type: HostListener, args: ['click',] }]
    };
    return NodeRestoreDirective;
}());
export { NodeRestoreDirective };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NodeRestoreDirective.prototype.restoreProcessStatus;
    /**
     * Array of deleted nodes to restore.
     * @type {?}
     */
    NodeRestoreDirective.prototype.selection;
    /**
     * Emitted when restoration is complete.
     * @type {?}
     */
    NodeRestoreDirective.prototype.restore;
    /**
     * @type {?}
     * @private
     */
    NodeRestoreDirective.prototype.alfrescoApiService;
    /**
     * @type {?}
     * @private
     */
    NodeRestoreDirective.prototype.translation;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9kZS1yZXN0b3JlLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvbm9kZS1yZXN0b3JlLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRXJGLE9BQU8sRUFBYyxRQUFRLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN0RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUN0RSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUNyRSxPQUFPLEVBQUUsR0FBRyxFQUFFLFFBQVEsRUFBRSxHQUFHLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7QUFFaEU7SUFBQTtJQUlBLENBQUM7SUFBRCwwQkFBQztBQUFELENBQUMsQUFKRCxJQUlDOzs7O0lBSEcsc0NBQWdCOztJQUNoQixtQ0FBcUI7O0lBQ3JCLHFDQUFlOztBQUduQjtJQW1CSSw4QkFBb0Isa0JBQXNDLEVBQ3RDLFdBQStCO1FBRC9CLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMsZ0JBQVcsR0FBWCxXQUFXLENBQW9COzs7O1FBUm5ELFlBQU8sR0FBc0MsSUFBSSxZQUFZLEVBQUUsQ0FBQztRQVM1RCxJQUFJLENBQUMsb0JBQW9CLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3JELENBQUM7Ozs7SUFQRCxzQ0FBTzs7O0lBRFA7UUFFSSxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7Ozs7SUFPTyxzQ0FBTzs7Ozs7SUFBZixVQUFnQixTQUFjO1FBQTlCLGlCQW1DQzs7UUFsQ0csSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUU7WUFDbkIsT0FBTztTQUNWOztZQUVLLGFBQWEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDO1FBRXRELElBQUksU0FBUyxDQUFDLE1BQU0sSUFBSSxhQUFhLENBQUMsTUFBTSxFQUFFO1lBRTFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxhQUFhLENBQUMsQ0FBQyxJQUFJLENBQ3RDLEdBQUc7Ozs7WUFBQyxVQUFDLGFBQWE7OztvQkFDUixNQUFNLEdBQUcsS0FBSSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUM7Z0JBRWhELENBQUEsS0FBQSxLQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFBLENBQUMsSUFBSSw0QkFBSSxNQUFNLENBQUMsSUFBSSxHQUFFO2dCQUNwRCxDQUFBLEtBQUEsS0FBSSxDQUFDLG9CQUFvQixDQUFDLE9BQU8sQ0FBQSxDQUFDLElBQUksNEJBQUksTUFBTSxDQUFDLE9BQU8sR0FBRTtZQUM5RCxDQUFDLEVBQUMsRUFDRixRQUFROzs7WUFBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLGVBQWUsRUFBRSxFQUF0QixDQUFzQixFQUFDLENBQ3pDO2lCQUNBLFNBQVM7Ozs7WUFBQyxVQUFDLGdCQUFnQjtnQkFDaEIsSUFBQSx3Q0FBaUI7Z0JBQ2pCLElBQUEsbURBQXVCOztvQkFDekIsYUFBYSxHQUFHLEtBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsU0FBUyxFQUFFLEtBQUssQ0FBQzs7b0JBQzlELGNBQWMsR0FBRyxLQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsRUFBRSxRQUFRLENBQUM7Z0JBRXpELElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFO29CQUN4QixLQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7aUJBQ3ZCO3FCQUFNO29CQUNILEtBQUksQ0FBQyxPQUFPLENBQUMsY0FBYyxDQUFDLENBQUM7aUJBQ2hDO1lBQ0wsQ0FBQyxFQUFDLENBQUM7U0FDTjthQUFNO1lBQ0gsQ0FBQSxLQUFBLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUEsQ0FBQyxJQUFJLDRCQUFJLFNBQVMsR0FBRTtZQUNsRCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7WUFDcEIsT0FBTztTQUNWO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8sZ0RBQWlCOzs7OztJQUF6QixVQUEwQixLQUF5QjtRQUFuRCxpQkFFQztRQURHLE9BQU8sUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxFQUF0QixDQUFzQixFQUFDLENBQUMsQ0FBQztJQUNqRSxDQUFDOzs7Ozs7SUFFTywrQ0FBZ0I7Ozs7O0lBQXhCLFVBQXlCLFNBQVM7UUFDOUIsT0FBTyxTQUFTLENBQUMsTUFBTTs7OztRQUFDLFVBQUMsSUFBSSxJQUFLLE9BQUEsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQWYsQ0FBZSxFQUFDLENBQUM7SUFDdkQsQ0FBQzs7Ozs7SUFFTyw4Q0FBZTs7OztJQUF2Qjs7WUFDVSxPQUFPLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRTthQUNoRCxJQUFJLENBQUMsUUFBUSxDQUFDLGVBQWUsQ0FBQyxFQUFFLE9BQU8sRUFBRSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUM7UUFFekQsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDekIsQ0FBQzs7Ozs7O0lBRU8sMENBQVc7Ozs7O0lBQW5CLFVBQW9CLElBQUk7UUFDWixJQUFBLGtCQUFLOztZQUVQLE9BQU8sR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBRSxDQUFDO1FBRWpGLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FDckIsR0FBRzs7O1FBQUMsY0FBTSxPQUFBLENBQUM7WUFDUCxNQUFNLEVBQUUsQ0FBQztZQUNULEtBQUssT0FBQTtTQUNSLENBQUMsRUFIUSxDQUdSLEVBQUMsRUFDSCxVQUFVOzs7O1FBQUMsVUFBQyxLQUFLO1lBQ0wsSUFBQSx5REFBVTtZQUVsQixPQUFPLEVBQUUsQ0FBQztnQkFDTixNQUFNLEVBQUUsQ0FBQztnQkFDVCxVQUFVLFlBQUE7Z0JBQ1YsS0FBSyxPQUFBO2FBQ1IsQ0FBQyxDQUFDO1FBQ1AsQ0FBQyxFQUFDLENBQ0wsQ0FBQztJQUNOLENBQUM7Ozs7Ozs7O0lBRU8sbUNBQUk7Ozs7Ozs7SUFBWixVQUFhLFNBQVMsRUFBRSxJQUFJLEVBQUUsUUFBZTtRQUFmLHlCQUFBLEVBQUEsZUFBZTs7WUFDbkMsR0FBRyxHQUFHLFNBQVMsQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsRUFBYixDQUFhLEVBQUM7UUFFbEQsT0FBTyxJQUFJLENBQUMsTUFBTTs7OztRQUFDLFVBQUMsSUFBSTtZQUNwQixJQUFJLFFBQVEsRUFBRTtnQkFDVixPQUFPLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7YUFDcEQ7aUJBQU07Z0JBQ0gsT0FBTyxDQUFDLEdBQUcsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7YUFDckQ7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQUVPLDRDQUFhOzs7OztJQUFyQixVQUFzQixJQUFTO1FBQVQscUJBQUEsRUFBQSxTQUFTOztZQUNyQixNQUFNLEdBQUc7WUFDWCxJQUFJLEVBQUUsRUFBRTtZQUNSLE9BQU8sRUFBRSxFQUFFOzs7O1lBQ1gsSUFBSSxVQUFVO2dCQUNWLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNoQyxDQUFDOzs7O1lBQ0QsSUFBSSxhQUFhO2dCQUNiLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUNuQyxDQUFDOzs7O1lBQ0QsSUFBSSxTQUFTO2dCQUNULE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO1lBQ2xDLENBQUM7Ozs7WUFDRCxJQUFJLFlBQVk7Z0JBQ1osT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUM7WUFDckMsQ0FBQzs7OztZQUNELElBQUksWUFBWTtnQkFDWixPQUFPLElBQUksQ0FBQyxhQUFhLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ2xELENBQUM7Ozs7WUFDRCxJQUFJLFNBQVM7Z0JBQ1QsT0FBTyxJQUFJLENBQUMsVUFBVSxJQUFJLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQztZQUNsRCxDQUFDO1lBQ0QsS0FBSzs7OztnQkFDRCxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztnQkFDZixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztZQUN0QixDQUFDO1NBQ0o7UUFFRCxPQUFPLElBQUksQ0FBQyxNQUFNOzs7OztRQUNkLFVBQUMsR0FBRyxFQUFFLElBQUk7WUFDTixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ2IsR0FBRyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDMUI7aUJBQU07Z0JBQ0gsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDdkI7WUFFRCxPQUFPLEdBQUcsQ0FBQztRQUNmLENBQUMsR0FDRCxNQUFNLENBQ1QsQ0FBQztJQUNOLENBQUM7Ozs7O0lBRU8sZ0RBQWlCOzs7O0lBQXpCO1FBQ1ksSUFBQSxrQ0FBNEI7UUFFcEMsSUFBSSxNQUFNLENBQUMsVUFBVSxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRTtZQUN4QyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUMzQixrQ0FBa0MsRUFDbEM7Z0JBQ0ksTUFBTSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsTUFBTTthQUM3QixDQUNKLENBQUM7U0FDTDtRQUVELElBQUksTUFBTSxDQUFDLFNBQVMsSUFBSSxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsRUFBRTtZQUMvQyxJQUFJLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsVUFBVSxLQUFLLEdBQUcsRUFBRTtnQkFDbkMsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FDM0IsK0JBQStCLEVBQy9CO29CQUNJLElBQUksRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxJQUFJO2lCQUNsQyxDQUNKLENBQUM7YUFDTDtpQkFBTTtnQkFDSCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUMzQiwyQkFBMkIsRUFDM0I7b0JBQ0ksSUFBSSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUk7aUJBQ2xDLENBQ0osQ0FBQzthQUNMO1NBQ0o7UUFFRCxJQUFJLE1BQU0sQ0FBQyxTQUFTLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLFVBQVUsRUFBRTtZQUNoRCxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUMzQixvQ0FBb0MsRUFDcEM7Z0JBQ0ksSUFBSSxFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUk7YUFDbEMsQ0FDSixDQUFDO1NBQ0w7UUFFRCxJQUFJLE1BQU0sQ0FBQyxZQUFZLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxFQUFFO1lBQzdDLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsMEJBQTBCLENBQUMsQ0FBQztTQUMvRDtRQUVELElBQUksTUFBTSxDQUFDLFlBQVksSUFBSSxNQUFNLENBQUMsWUFBWSxFQUFFO1lBQzVDLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQzNCLDRCQUE0QixFQUM1QjtnQkFDSSxJQUFJLEVBQUUsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsSUFBSTthQUNyQyxDQUNKLENBQUM7U0FDTDtJQUNMLENBQUM7Ozs7O0lBRU8sMkNBQVk7Ozs7SUFBcEI7O1lBQ1UsTUFBTSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLElBQUksQ0FBQyxvQkFBb0IsQ0FBQzs7WUFFckQsT0FBTyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtRQUN4QyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7O1lBRVAsTUFBTSxHQUFHLENBQUMsTUFBTSxDQUFDLFlBQVksSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsd0JBQXdCLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRTs7WUFFaEgsSUFBSTtRQUNSLElBQUksTUFBTSxDQUFDLE9BQU8sSUFBSSxNQUFNLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDN0MsSUFBSSxHQUFHLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztTQUN2QztRQUNELElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDO1lBQ2QsT0FBTyxFQUFFLE9BQU87WUFDaEIsTUFBTSxFQUFFLE1BQU07WUFDZCxJQUFJLEVBQUUsSUFBSTtTQUNiLENBQUMsQ0FBQztJQUNQLENBQUM7Ozs7O0lBRU8sb0NBQUs7Ozs7SUFBYjtRQUNJLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNsQyxJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN4QixDQUFDOztnQkFuT0osU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxlQUFlO2lCQUM1Qjs7OztnQkFaUSxrQkFBa0I7Z0JBQ2xCLGtCQUFrQjs7OzRCQWdCdEIsS0FBSyxTQUFDLGFBQWE7MEJBSW5CLE1BQU07MEJBR04sWUFBWSxTQUFDLE9BQU87O0lBc056QiwyQkFBQztDQUFBLEFBcE9ELElBb09DO1NBak9ZLG9CQUFvQjs7Ozs7O0lBQzdCLG9EQUE2Qjs7Ozs7SUFHN0IseUNBQzhCOzs7OztJQUc5Qix1Q0FDZ0U7Ozs7O0lBT3BELGtEQUE4Qzs7Ozs7SUFDOUMsMkNBQXVDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciBuby1pbnB1dC1yZW5hbWUgKi9cclxuXHJcbmltcG9ydCB7IERpcmVjdGl2ZSwgRXZlbnRFbWl0dGVyLCBIb3N0TGlzdGVuZXIsIElucHV0LCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRGVsZXRlZE5vZGVFbnRyeSwgRGVsZXRlZE5vZGVzUGFnaW5nLCBQYXRoSW5mb0VudGl0eSB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmb3JrSm9pbiwgZnJvbSwgb2YgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGlvblNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy90cmFuc2xhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgdGFwLCBtZXJnZU1hcCwgbWFwLCBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuZXhwb3J0IGNsYXNzIFJlc3RvcmVNZXNzYWdlTW9kZWwge1xyXG4gICAgbWVzc2FnZTogc3RyaW5nO1xyXG4gICAgcGF0aDogUGF0aEluZm9FbnRpdHk7XHJcbiAgICBhY3Rpb246IHN0cmluZztcclxufVxyXG5cclxuQERpcmVjdGl2ZSh7XHJcbiAgICBzZWxlY3RvcjogJ1thZGYtcmVzdG9yZV0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOb2RlUmVzdG9yZURpcmVjdGl2ZSB7XHJcbiAgICBwcml2YXRlIHJlc3RvcmVQcm9jZXNzU3RhdHVzO1xyXG5cclxuICAgIC8qKiBBcnJheSBvZiBkZWxldGVkIG5vZGVzIHRvIHJlc3RvcmUuICovXHJcbiAgICBASW5wdXQoJ2FkZi1yZXN0b3JlJylcclxuICAgIHNlbGVjdGlvbjogRGVsZXRlZE5vZGVFbnRyeVtdO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gcmVzdG9yYXRpb24gaXMgY29tcGxldGUuICovXHJcbiAgICBAT3V0cHV0KClcclxuICAgIHJlc3RvcmU6IEV2ZW50RW1pdHRlcjxSZXN0b3JlTWVzc2FnZU1vZGVsPiA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdjbGljaycpXHJcbiAgICBvbkNsaWNrKCkge1xyXG4gICAgICAgIHRoaXMucmVjb3Zlcih0aGlzLnNlbGVjdGlvbik7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhbGZyZXNjb0FwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgdHJhbnNsYXRpb246IFRyYW5zbGF0aW9uU2VydmljZSkge1xyXG4gICAgICAgIHRoaXMucmVzdG9yZVByb2Nlc3NTdGF0dXMgPSB0aGlzLnByb2Nlc3NTdGF0dXMoKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHJlY292ZXIoc2VsZWN0aW9uOiBhbnkpIHtcclxuICAgICAgICBpZiAoIXNlbGVjdGlvbi5sZW5ndGgpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3Qgbm9kZXNXaXRoUGF0aCA9IHRoaXMuZ2V0Tm9kZXNXaXRoUGF0aChzZWxlY3Rpb24pO1xyXG5cclxuICAgICAgICBpZiAoc2VsZWN0aW9uLmxlbmd0aCAmJiBub2Rlc1dpdGhQYXRoLmxlbmd0aCkge1xyXG5cclxuICAgICAgICAgICAgdGhpcy5yZXN0b3JlTm9kZXNCYXRjaChub2Rlc1dpdGhQYXRoKS5waXBlKFxyXG4gICAgICAgICAgICAgICAgdGFwKChyZXN0b3JlZE5vZGVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgc3RhdHVzID0gdGhpcy5wcm9jZXNzU3RhdHVzKHJlc3RvcmVkTm9kZXMpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJlc3RvcmVQcm9jZXNzU3RhdHVzLmZhaWwucHVzaCguLi5zdGF0dXMuZmFpbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yZXN0b3JlUHJvY2Vzc1N0YXR1cy5zdWNjZXNzLnB1c2goLi4uc3RhdHVzLnN1Y2Nlc3MpO1xyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBtZXJnZU1hcCgoKSA9PiB0aGlzLmdldERlbGV0ZWROb2RlcygpKVxyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKGRlbGV0ZWROb2Rlc0xpc3QpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHsgZW50cmllczogbm9kZUxpc3QgfSA9IGRlbGV0ZWROb2Rlc0xpc3QubGlzdDtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHsgZmFpbDogcmVzdG9yZUVycm9yTm9kZXMgfSA9IHRoaXMucmVzdG9yZVByb2Nlc3NTdGF0dXM7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBzZWxlY3RlZE5vZGVzID0gdGhpcy5kaWZmKHJlc3RvcmVFcnJvck5vZGVzLCBzZWxlY3Rpb24sIGZhbHNlKTtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHJlbWFpbmluZ05vZGVzID0gdGhpcy5kaWZmKHNlbGVjdGVkTm9kZXMsIG5vZGVMaXN0KTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoIXJlbWFpbmluZ05vZGVzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uKCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucmVjb3ZlcihyZW1haW5pbmdOb2Rlcyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVzdG9yZVByb2Nlc3NTdGF0dXMuZmFpbC5wdXNoKC4uLnNlbGVjdGlvbik7XHJcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9uKCk7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSByZXN0b3JlTm9kZXNCYXRjaChiYXRjaDogRGVsZXRlZE5vZGVFbnRyeVtdKTogT2JzZXJ2YWJsZTxEZWxldGVkTm9kZUVudHJ5W10+IHtcclxuICAgICAgICByZXR1cm4gZm9ya0pvaW4oYmF0Y2gubWFwKChub2RlKSA9PiB0aGlzLnJlc3RvcmVOb2RlKG5vZGUpKSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXROb2Rlc1dpdGhQYXRoKHNlbGVjdGlvbik6IERlbGV0ZWROb2RlRW50cnlbXSB7XHJcbiAgICAgICAgcmV0dXJuIHNlbGVjdGlvbi5maWx0ZXIoKG5vZGUpID0+IG5vZGUuZW50cnkucGF0aCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXREZWxldGVkTm9kZXMoKTogT2JzZXJ2YWJsZTxEZWxldGVkTm9kZXNQYWdpbmc+IHtcclxuICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKVxyXG4gICAgICAgICAgICAuY29yZS5ub2Rlc0FwaS5nZXREZWxldGVkTm9kZXMoeyBpbmNsdWRlOiBbJ3BhdGgnXSB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20ocHJvbWlzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSByZXN0b3JlTm9kZShub2RlKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCB7IGVudHJ5IH0gPSBub2RlO1xyXG5cclxuICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5ub2Rlcy5yZXN0b3JlTm9kZShlbnRyeS5pZCk7XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHByb21pc2UpLnBpcGUoXHJcbiAgICAgICAgICAgIG1hcCgoKSA9PiAoe1xyXG4gICAgICAgICAgICAgICAgc3RhdHVzOiAxLFxyXG4gICAgICAgICAgICAgICAgZW50cnlcclxuICAgICAgICAgICAgfSkpLFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgeyBzdGF0dXNDb2RlIH0gPSAoSlNPTi5wYXJzZShlcnJvci5tZXNzYWdlKSkuZXJyb3I7XHJcblxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG9mKHtcclxuICAgICAgICAgICAgICAgICAgICBzdGF0dXM6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgc3RhdHVzQ29kZSxcclxuICAgICAgICAgICAgICAgICAgICBlbnRyeVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGRpZmYoc2VsZWN0aW9uLCBsaXN0LCBmcm9tTGlzdCA9IHRydWUpOiBhbnkge1xyXG4gICAgICAgIGNvbnN0IGlkcyA9IHNlbGVjdGlvbi5tYXAoKGl0ZW0pID0+IGl0ZW0uZW50cnkuaWQpO1xyXG5cclxuICAgICAgICByZXR1cm4gbGlzdC5maWx0ZXIoKGl0ZW0pID0+IHtcclxuICAgICAgICAgICAgaWYgKGZyb21MaXN0KSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gaWRzLmluY2x1ZGVzKGl0ZW0uZW50cnkuaWQpID8gaXRlbSA6IG51bGw7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gIWlkcy5pbmNsdWRlcyhpdGVtLmVudHJ5LmlkKSA/IGl0ZW0gOiBudWxsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBwcm9jZXNzU3RhdHVzKGRhdGEgPSBbXSk6IGFueSB7XHJcbiAgICAgICAgY29uc3Qgc3RhdHVzID0ge1xyXG4gICAgICAgICAgICBmYWlsOiBbXSxcclxuICAgICAgICAgICAgc3VjY2VzczogW10sXHJcbiAgICAgICAgICAgIGdldCBzb21lRmFpbGVkKCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICEhKHRoaXMuZmFpbC5sZW5ndGgpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBnZXQgc29tZVN1Y2NlZWRlZCgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAhISh0aGlzLnN1Y2Nlc3MubGVuZ3RoKTtcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZ2V0IG9uZUZhaWxlZCgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmZhaWwubGVuZ3RoID09PSAxO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBnZXQgb25lU3VjY2VlZGVkKCkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuc3VjY2Vzcy5sZW5ndGggPT09IDE7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGdldCBhbGxTdWNjZWVkZWQoKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5zb21lU3VjY2VlZGVkICYmICF0aGlzLnNvbWVGYWlsZWQ7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIGdldCBhbGxGYWlsZWQoKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5zb21lRmFpbGVkICYmICF0aGlzLnNvbWVTdWNjZWVkZWQ7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHJlc2V0KCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mYWlsID0gW107XHJcbiAgICAgICAgICAgICAgICB0aGlzLnN1Y2Nlc3MgPSBbXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiBkYXRhLnJlZHVjZShcclxuICAgICAgICAgICAgKGFjYywgbm9kZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKG5vZGUuc3RhdHVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWNjLnN1Y2Nlc3MucHVzaChub2RlKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYWNjLmZhaWwucHVzaChub2RlKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICByZXR1cm4gYWNjO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBzdGF0dXNcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0UmVzdG9yZU1lc3NhZ2UoKTogc3RyaW5nIHtcclxuICAgICAgICBjb25zdCB7IHJlc3RvcmVQcm9jZXNzU3RhdHVzOiBzdGF0dXMgfSA9IHRoaXM7XHJcblxyXG4gICAgICAgIGlmIChzdGF0dXMuc29tZUZhaWxlZCAmJiAhc3RhdHVzLm9uZUZhaWxlZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy50cmFuc2xhdGlvbi5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgJ0NPUkUuUkVTVE9SRV9OT0RFLlBBUlRJQUxfUExVUkFMJyxcclxuICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICBudW1iZXI6IHN0YXR1cy5mYWlsLmxlbmd0aFxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHN0YXR1cy5vbmVGYWlsZWQgJiYgc3RhdHVzLmZhaWxbMF0uc3RhdHVzQ29kZSkge1xyXG4gICAgICAgICAgICBpZiAoc3RhdHVzLmZhaWxbMF0uc3RhdHVzQ29kZSA9PT0gNDA5KSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy50cmFuc2xhdGlvbi5pbnN0YW50KFxyXG4gICAgICAgICAgICAgICAgICAgICdDT1JFLlJFU1RPUkVfTk9ERS5OT0RFX0VYSVNUUycsXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiBzdGF0dXMuZmFpbFswXS5lbnRyeS5uYW1lXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnRyYW5zbGF0aW9uLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAgICAgJ0NPUkUuUkVTVE9SRV9OT0RFLkdFTkVSSUMnLFxyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogc3RhdHVzLmZhaWxbMF0uZW50cnkubmFtZVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChzdGF0dXMub25lRmFpbGVkICYmICFzdGF0dXMuZmFpbFswXS5zdGF0dXNDb2RlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnRyYW5zbGF0aW9uLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAnQ09SRS5SRVNUT1JFX05PREUuTE9DQVRJT05fTUlTU0lORycsXHJcbiAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgbmFtZTogc3RhdHVzLmZhaWxbMF0uZW50cnkubmFtZVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHN0YXR1cy5hbGxTdWNjZWVkZWQgJiYgIXN0YXR1cy5vbmVTdWNjZWVkZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMudHJhbnNsYXRpb24uaW5zdGFudCgnQ09SRS5SRVNUT1JFX05PREUuUExVUkFMJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoc3RhdHVzLmFsbFN1Y2NlZWRlZCAmJiBzdGF0dXMub25lU3VjY2VlZGVkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnRyYW5zbGF0aW9uLmluc3RhbnQoXHJcbiAgICAgICAgICAgICAgICAnQ09SRS5SRVNUT1JFX05PREUuU0lOR1VMQVInLFxyXG4gICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6IHN0YXR1cy5zdWNjZXNzWzBdLmVudHJ5Lm5hbWVcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBub3RpZmljYXRpb24oKTogdm9pZCB7XHJcbiAgICAgICAgY29uc3Qgc3RhdHVzID0gT2JqZWN0LmFzc2lnbih7fSwgdGhpcy5yZXN0b3JlUHJvY2Vzc1N0YXR1cyk7XHJcblxyXG4gICAgICAgIGNvbnN0IG1lc3NhZ2UgPSB0aGlzLmdldFJlc3RvcmVNZXNzYWdlKCk7XHJcbiAgICAgICAgdGhpcy5yZXNldCgpO1xyXG5cclxuICAgICAgICBjb25zdCBhY3Rpb24gPSAoc3RhdHVzLm9uZVN1Y2NlZWRlZCAmJiAhc3RhdHVzLnNvbWVGYWlsZWQpID8gdGhpcy50cmFuc2xhdGlvbi5pbnN0YW50KCdDT1JFLlJFU1RPUkVfTk9ERS5WSUVXJykgOiAnJztcclxuXHJcbiAgICAgICAgbGV0IHBhdGg7XHJcbiAgICAgICAgaWYgKHN0YXR1cy5zdWNjZXNzICYmIHN0YXR1cy5zdWNjZXNzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgcGF0aCA9IHN0YXR1cy5zdWNjZXNzWzBdLmVudHJ5LnBhdGg7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMucmVzdG9yZS5lbWl0KHtcclxuICAgICAgICAgICAgbWVzc2FnZTogbWVzc2FnZSxcclxuICAgICAgICAgICAgYWN0aW9uOiBhY3Rpb24sXHJcbiAgICAgICAgICAgIHBhdGg6IHBhdGhcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHJlc2V0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMucmVzdG9yZVByb2Nlc3NTdGF0dXMucmVzZXQoKTtcclxuICAgICAgICB0aGlzLnNlbGVjdGlvbiA9IFtdO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==