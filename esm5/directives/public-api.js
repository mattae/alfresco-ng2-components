/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export { HighlightDirective } from './highlight.directive';
export { LogoutDirective } from './logout.directive';
export { NodeDeleteDirective } from './node-delete.directive';
export { NodeFavoriteDirective } from './node-favorite.directive';
export { CheckAllowableOperationDirective } from './check-allowable-operation.directive';
export { RestoreMessageModel, NodeRestoreDirective } from './node-restore.directive';
export { NodeDownloadDirective } from './node-download.directive';
export { UploadDirective } from './upload.directive';
export { DirectiveModule } from './directive.module';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvcHVibGljLWFwaS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxtQ0FBYyx1QkFBdUIsQ0FBQztBQUN0QyxnQ0FBYyxvQkFBb0IsQ0FBQztBQUNuQyxvQ0FBYyx5QkFBeUIsQ0FBQztBQUN4QyxzQ0FBYywyQkFBMkIsQ0FBQztBQUMxQyxpREFBYyx1Q0FBdUMsQ0FBQztBQUN0RCwwREFBYywwQkFBMEIsQ0FBQztBQUN6QyxzQ0FBYywyQkFBMkIsQ0FBQztBQUMxQyxnQ0FBYyxvQkFBb0IsQ0FBQztBQUVuQyxnQ0FBYyxvQkFBb0IsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5leHBvcnQgKiBmcm9tICcuL2hpZ2hsaWdodC5kaXJlY3RpdmUnO1xyXG5leHBvcnQgKiBmcm9tICcuL2xvZ291dC5kaXJlY3RpdmUnO1xyXG5leHBvcnQgKiBmcm9tICcuL25vZGUtZGVsZXRlLmRpcmVjdGl2ZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vbm9kZS1mYXZvcml0ZS5kaXJlY3RpdmUnO1xyXG5leHBvcnQgKiBmcm9tICcuL2NoZWNrLWFsbG93YWJsZS1vcGVyYXRpb24uZGlyZWN0aXZlJztcclxuZXhwb3J0ICogZnJvbSAnLi9ub2RlLXJlc3RvcmUuZGlyZWN0aXZlJztcclxuZXhwb3J0ICogZnJvbSAnLi9ub2RlLWRvd25sb2FkLmRpcmVjdGl2ZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vdXBsb2FkLmRpcmVjdGl2ZSc7XHJcblxyXG5leHBvcnQgKiBmcm9tICcuL2RpcmVjdGl2ZS5tb2R1bGUnO1xyXG4iXX0=