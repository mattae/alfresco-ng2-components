/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:no-input-rename  */
import { Directive, ElementRef, HostListener, Input, NgZone, Renderer2 } from '@angular/core';
import { FileUtils } from '../utils/file-utils';
var UploadDirective = /** @class */ (function () {
    function UploadDirective(el, renderer, ngZone) {
        this.el = el;
        this.renderer = renderer;
        this.ngZone = ngZone;
        /**
         * Enables/disables uploading.
         */
        this.enabled = true;
        /**
         * Upload mode. Can be "drop" (receives dropped files) or "click"
         * (clicking opens a file dialog). Both modes can be active at once.
         */
        this.mode = ['drop']; // click|drop
        this.isDragging = false;
        this.cssClassName = 'adf-upload__dragging';
        this.element = el.nativeElement;
    }
    /**
     * @return {?}
     */
    UploadDirective.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.isClickMode() && this.renderer) {
            /** @type {?} */
            var inputUpload = this.renderer.createElement('input');
            this.upload = this.el.nativeElement.parentElement.appendChild(inputUpload);
            this.upload.type = 'file';
            this.upload.style.display = 'none';
            this.upload.addEventListener('change', (/**
             * @param {?} event
             * @return {?}
             */
            function (event) { return _this.onSelectFiles(event); }));
            if (this.multiple) {
                this.upload.setAttribute('multiple', '');
            }
            if (this.accept) {
                this.upload.setAttribute('accept', this.accept);
            }
            if (this.directory) {
                this.upload.setAttribute('webkitdirectory', '');
            }
        }
        if (this.isDropMode()) {
            this.ngZone.runOutsideAngular((/**
             * @return {?}
             */
            function () {
                _this.element.addEventListener('dragenter', _this.onDragEnter.bind(_this));
                _this.element.addEventListener('dragover', _this.onDragOver.bind(_this));
                _this.element.addEventListener('dragleave', _this.onDragLeave.bind(_this));
                _this.element.addEventListener('drop', _this.onDrop.bind(_this));
            }));
        }
    };
    /**
     * @return {?}
     */
    UploadDirective.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.element.removeEventListener('dragenter', this.onDragEnter);
        this.element.removeEventListener('dragover', this.onDragOver);
        this.element.removeEventListener('dragleave', this.onDragLeave);
        this.element.removeEventListener('drop', this.onDrop);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UploadDirective.prototype.onClick = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.isClickMode() && this.upload) {
            event.preventDefault();
            this.upload.click();
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UploadDirective.prototype.onDragEnter = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.isDropMode()) {
            this.element.classList.add(this.cssClassName);
            this.isDragging = true;
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UploadDirective.prototype.onDragOver = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        event.preventDefault();
        if (this.isDropMode()) {
            this.element.classList.add(this.cssClassName);
            this.isDragging = true;
        }
        return false;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UploadDirective.prototype.onDragLeave = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.isDropMode()) {
            this.element.classList.remove(this.cssClassName);
            this.isDragging = false;
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UploadDirective.prototype.onDrop = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        var _this = this;
        if (this.isDropMode()) {
            event.stopPropagation();
            event.preventDefault();
            this.element.classList.remove(this.cssClassName);
            this.isDragging = false;
            /** @type {?} */
            var dataTransfer = this.getDataTransfer(event);
            if (dataTransfer) {
                this.getFilesDropped(dataTransfer).then((/**
                 * @param {?} files
                 * @return {?}
                 */
                function (files) {
                    _this.onUploadFiles(files);
                }));
            }
        }
        return false;
    };
    /**
     * @param {?} files
     * @return {?}
     */
    UploadDirective.prototype.onUploadFiles = /**
     * @param {?} files
     * @return {?}
     */
    function (files) {
        if (this.enabled && files.length > 0) {
            /** @type {?} */
            var customEvent = new CustomEvent('upload-files', {
                detail: {
                    sender: this,
                    data: this.data,
                    files: files
                },
                bubbles: true
            });
            this.el.nativeElement.dispatchEvent(customEvent);
        }
    };
    /**
     * @protected
     * @param {?} mode
     * @return {?}
     */
    UploadDirective.prototype.hasMode = /**
     * @protected
     * @param {?} mode
     * @return {?}
     */
    function (mode) {
        return this.enabled && mode && this.mode && this.mode.indexOf(mode) > -1;
    };
    /**
     * @protected
     * @return {?}
     */
    UploadDirective.prototype.isDropMode = /**
     * @protected
     * @return {?}
     */
    function () {
        return this.hasMode('drop');
    };
    /**
     * @protected
     * @return {?}
     */
    UploadDirective.prototype.isClickMode = /**
     * @protected
     * @return {?}
     */
    function () {
        return this.hasMode('click');
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UploadDirective.prototype.getDataTransfer = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (event && event.dataTransfer) {
            return event.dataTransfer;
        }
        if (event && event.originalEvent && event.originalEvent.dataTransfer) {
            return event.originalEvent.dataTransfer;
        }
        return null;
    };
    /**
     * Extract files from the DataTransfer object used to hold the data that is being dragged during a drag and drop operation.
     * @param dataTransfer DataTransfer object
     */
    /**
     * Extract files from the DataTransfer object used to hold the data that is being dragged during a drag and drop operation.
     * @param {?} dataTransfer DataTransfer object
     * @return {?}
     */
    UploadDirective.prototype.getFilesDropped = /**
     * Extract files from the DataTransfer object used to hold the data that is being dragged during a drag and drop operation.
     * @param {?} dataTransfer DataTransfer object
     * @return {?}
     */
    function (dataTransfer) {
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        function (resolve) {
            /** @type {?} */
            var iterations = [];
            if (dataTransfer) {
                /** @type {?} */
                var items = dataTransfer.items;
                if (items) {
                    var _loop_1 = function (i) {
                        if (typeof items[i].webkitGetAsEntry !== 'undefined') {
                            /** @type {?} */
                            var item_1 = items[i].webkitGetAsEntry();
                            if (item_1) {
                                if (item_1.isFile) {
                                    iterations.push(Promise.resolve((/** @type {?} */ ({
                                        entry: item_1,
                                        file: items[i].getAsFile(),
                                        relativeFolder: '/'
                                    }))));
                                }
                                else if (item_1.isDirectory) {
                                    iterations.push(new Promise((/**
                                     * @param {?} resolveFolder
                                     * @return {?}
                                     */
                                    function (resolveFolder) {
                                        FileUtils.flatten(item_1).then((/**
                                         * @param {?} files
                                         * @return {?}
                                         */
                                        function (files) { return resolveFolder(files); }));
                                    })));
                                }
                            }
                        }
                        else {
                            iterations.push(Promise.resolve((/** @type {?} */ ({
                                entry: null,
                                file: items[i].getAsFile(),
                                relativeFolder: '/'
                            }))));
                        }
                    };
                    for (var i = 0; i < items.length; i++) {
                        _loop_1(i);
                    }
                }
                else {
                    // safari or FF
                    /** @type {?} */
                    var files = FileUtils
                        .toFileArray(dataTransfer.files)
                        .map((/**
                     * @param {?} file
                     * @return {?}
                     */
                    function (file) { return (/** @type {?} */ ({
                        entry: null,
                        file: file,
                        relativeFolder: '/'
                    })); }));
                    iterations.push(Promise.resolve(files));
                }
            }
            Promise.all(iterations).then((/**
             * @param {?} result
             * @return {?}
             */
            function (result) {
                resolve(result.reduce((/**
                 * @param {?} a
                 * @param {?} b
                 * @return {?}
                 */
                function (a, b) { return a.concat(b); }), []));
            }));
        }));
    };
    /**
     * Invoked when user selects files or folders by means of File Dialog
     * @param event DOM event
     */
    /**
     * Invoked when user selects files or folders by means of File Dialog
     * @param {?} event DOM event
     * @return {?}
     */
    UploadDirective.prototype.onSelectFiles = /**
     * Invoked when user selects files or folders by means of File Dialog
     * @param {?} event DOM event
     * @return {?}
     */
    function (event) {
        if (this.isClickMode()) {
            /** @type {?} */
            var input = ((/** @type {?} */ (event.currentTarget)));
            /** @type {?} */
            var files = FileUtils.toFileArray(input.files);
            this.onUploadFiles(files.map((/**
             * @param {?} file
             * @return {?}
             */
            function (file) { return (/** @type {?} */ ({
                entry: null,
                file: file,
                relativeFolder: '/'
            })); })));
            event.target.value = '';
        }
    };
    UploadDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[adf-upload]'
                },] }
    ];
    /** @nocollapse */
    UploadDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: Renderer2 },
        { type: NgZone }
    ]; };
    UploadDirective.propDecorators = {
        enabled: [{ type: Input, args: ['adf-upload',] }],
        data: [{ type: Input, args: ['adf-upload-data',] }],
        mode: [{ type: Input }],
        multiple: [{ type: Input }],
        accept: [{ type: Input }],
        directory: [{ type: Input }],
        onClick: [{ type: HostListener, args: ['click', ['$event'],] }]
    };
    return UploadDirective;
}());
export { UploadDirective };
if (false) {
    /**
     * Enables/disables uploading.
     * @type {?}
     */
    UploadDirective.prototype.enabled;
    /**
     * Data to upload.
     * @type {?}
     */
    UploadDirective.prototype.data;
    /**
     * Upload mode. Can be "drop" (receives dropped files) or "click"
     * (clicking opens a file dialog). Both modes can be active at once.
     * @type {?}
     */
    UploadDirective.prototype.mode;
    /**
     * Toggles multiple file uploads.
     * @type {?}
     */
    UploadDirective.prototype.multiple;
    /**
     * (Click mode only) MIME type filter for files to accept.
     * @type {?}
     */
    UploadDirective.prototype.accept;
    /**
     * (Click mode only) Toggles uploading of directories.
     * @type {?}
     */
    UploadDirective.prototype.directory;
    /** @type {?} */
    UploadDirective.prototype.isDragging;
    /**
     * @type {?}
     * @private
     */
    UploadDirective.prototype.cssClassName;
    /**
     * @type {?}
     * @private
     */
    UploadDirective.prototype.upload;
    /**
     * @type {?}
     * @private
     */
    UploadDirective.prototype.element;
    /**
     * @type {?}
     * @private
     */
    UploadDirective.prototype.el;
    /**
     * @type {?}
     * @private
     */
    UploadDirective.prototype.renderer;
    /**
     * @type {?}
     * @private
     */
    UploadDirective.prototype.ngZone;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLmRpcmVjdGl2ZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImRpcmVjdGl2ZXMvdXBsb2FkLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxVQUFVLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQXFCLFNBQVMsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNqSCxPQUFPLEVBQVksU0FBUyxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFMUQ7SUFxQ0kseUJBQW9CLEVBQWMsRUFBVSxRQUFtQixFQUFVLE1BQWM7UUFBbkUsT0FBRSxHQUFGLEVBQUUsQ0FBWTtRQUFVLGFBQVEsR0FBUixRQUFRLENBQVc7UUFBVSxXQUFNLEdBQU4sTUFBTSxDQUFROzs7O1FBOUJ2RixZQUFPLEdBQVksSUFBSSxDQUFDOzs7OztRQVV4QixTQUFJLEdBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLGFBQWE7UUFjeEMsZUFBVSxHQUFZLEtBQUssQ0FBQztRQUVwQixpQkFBWSxHQUFXLHNCQUFzQixDQUFDO1FBS2xELElBQUksQ0FBQyxPQUFPLEdBQUcsRUFBRSxDQUFDLGFBQWEsQ0FBQztJQUNwQyxDQUFDOzs7O0lBRUQsa0NBQVE7OztJQUFSO1FBQUEsaUJBOEJDO1FBN0JHLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7O2dCQUMvQixXQUFXLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxhQUFhLENBQUMsT0FBTyxDQUFDO1lBQ3hELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUUzRSxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksR0FBRyxNQUFNLENBQUM7WUFDMUIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztZQUNuQyxJQUFJLENBQUMsTUFBTSxDQUFDLGdCQUFnQixDQUFDLFFBQVE7Ozs7WUFBRSxVQUFDLEtBQUssSUFBSyxPQUFBLEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEVBQXpCLENBQXlCLEVBQUMsQ0FBQztZQUU3RSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQ2YsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQyxDQUFDO2FBQzVDO1lBRUQsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUNiLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDbkQ7WUFFRCxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7Z0JBQ2hCLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLGlCQUFpQixFQUFFLEVBQUUsQ0FBQyxDQUFDO2FBQ25EO1NBQ0o7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRTtZQUNuQixJQUFJLENBQUMsTUFBTSxDQUFDLGlCQUFpQjs7O1lBQUM7Z0JBQzFCLEtBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ3hFLEtBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxFQUFFLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ3RFLEtBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLEtBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFDLENBQUM7Z0JBQ3hFLEtBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxFQUFFLEtBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUksQ0FBQyxDQUFDLENBQUM7WUFDbEUsQ0FBQyxFQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7Ozs7SUFFRCxxQ0FBVzs7O0lBQVg7UUFDSSxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDaEUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxVQUFVLEVBQUUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQzlELElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztRQUNoRSxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDMUQsQ0FBQzs7Ozs7SUFHRCxpQ0FBTzs7OztJQURQLFVBQ1EsS0FBWTtRQUNoQixJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ25DLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN2QixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ3ZCO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxxQ0FBVzs7OztJQUFYLFVBQVksS0FBWTtRQUNwQixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRTtZQUNuQixJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQzlDLElBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDO1NBQzFCO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxvQ0FBVTs7OztJQUFWLFVBQVcsS0FBWTtRQUNuQixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7UUFDdkIsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUU7WUFDbkIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUM5QyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQztTQUMxQjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7O0lBRUQscUNBQVc7Ozs7SUFBWCxVQUFZLEtBQUs7UUFDYixJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRTtZQUNuQixJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2pELElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1NBQzNCO0lBQ0wsQ0FBQzs7Ozs7SUFFRCxnQ0FBTTs7OztJQUFOLFVBQU8sS0FBWTtRQUFuQixpQkFrQkM7UUFqQkcsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUU7WUFFbkIsS0FBSyxDQUFDLGVBQWUsRUFBRSxDQUFDO1lBQ3hCLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUV2QixJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1lBQ2pELElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDOztnQkFFbEIsWUFBWSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsS0FBSyxDQUFDO1lBQ2hELElBQUksWUFBWSxFQUFFO2dCQUNkLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSTs7OztnQkFBQyxVQUFDLEtBQUs7b0JBQzFDLEtBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7Z0JBQzlCLENBQUMsRUFBQyxDQUFDO2FBRU47U0FDSjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7O0lBRUQsdUNBQWE7Ozs7SUFBYixVQUFjLEtBQWlCO1FBQzNCLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTs7Z0JBQzVCLFdBQVcsR0FBRyxJQUFJLFdBQVcsQ0FBQyxjQUFjLEVBQUU7Z0JBQ2hELE1BQU0sRUFBRTtvQkFDSixNQUFNLEVBQUUsSUFBSTtvQkFDWixJQUFJLEVBQUUsSUFBSSxDQUFDLElBQUk7b0JBQ2YsS0FBSyxFQUFFLEtBQUs7aUJBQ2Y7Z0JBQ0QsT0FBTyxFQUFFLElBQUk7YUFDaEIsQ0FBQztZQUVGLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxXQUFXLENBQUMsQ0FBQztTQUNwRDtJQUNMLENBQUM7Ozs7OztJQUVTLGlDQUFPOzs7OztJQUFqQixVQUFrQixJQUFZO1FBQzFCLE9BQU8sSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUM3RSxDQUFDOzs7OztJQUVTLG9DQUFVOzs7O0lBQXBCO1FBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ2hDLENBQUM7Ozs7O0lBRVMscUNBQVc7Ozs7SUFBckI7UUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFRCx5Q0FBZTs7OztJQUFmLFVBQWdCLEtBQWtCO1FBQzlCLElBQUksS0FBSyxJQUFJLEtBQUssQ0FBQyxZQUFZLEVBQUU7WUFDN0IsT0FBTyxLQUFLLENBQUMsWUFBWSxDQUFDO1NBQzdCO1FBQ0QsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLGFBQWEsSUFBSSxLQUFLLENBQUMsYUFBYSxDQUFDLFlBQVksRUFBRTtZQUNsRSxPQUFPLEtBQUssQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDO1NBQzNDO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7O0lBQ0gseUNBQWU7Ozs7O0lBQWYsVUFBZ0IsWUFBMEI7UUFDdEMsT0FBTyxJQUFJLE9BQU87Ozs7UUFBQyxVQUFDLE9BQU87O2dCQUNqQixVQUFVLEdBQUcsRUFBRTtZQUVyQixJQUFJLFlBQVksRUFBRTs7b0JBQ1IsS0FBSyxHQUFHLFlBQVksQ0FBQyxLQUFLO2dCQUNoQyxJQUFJLEtBQUssRUFBRTs0Q0FDRSxDQUFDO3dCQUNOLElBQUksT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsZ0JBQWdCLEtBQUssV0FBVyxFQUFFOztnQ0FDNUMsTUFBSSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxnQkFBZ0IsRUFBRTs0QkFDeEMsSUFBSSxNQUFJLEVBQUU7Z0NBQ04sSUFBSSxNQUFJLENBQUMsTUFBTSxFQUFFO29DQUNiLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxtQkFBVzt3Q0FDdkMsS0FBSyxFQUFFLE1BQUk7d0NBQ1gsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxTQUFTLEVBQUU7d0NBQzFCLGNBQWMsRUFBRSxHQUFHO3FDQUN0QixFQUFBLENBQUMsQ0FBQyxDQUFDO2lDQUNQO3FDQUFNLElBQUksTUFBSSxDQUFDLFdBQVcsRUFBRTtvQ0FDekIsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLE9BQU87Ozs7b0NBQUMsVUFBQyxhQUFhO3dDQUN0QyxTQUFTLENBQUMsT0FBTyxDQUFDLE1BQUksQ0FBQyxDQUFDLElBQUk7Ozs7d0NBQUMsVUFBQyxLQUFLLElBQUssT0FBQSxhQUFhLENBQUMsS0FBSyxDQUFDLEVBQXBCLENBQW9CLEVBQUMsQ0FBQztvQ0FDbEUsQ0FBQyxFQUFDLENBQUMsQ0FBQztpQ0FDUDs2QkFDSjt5QkFDSjs2QkFBTTs0QkFDSCxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsbUJBQVc7Z0NBQ3ZDLEtBQUssRUFBRSxJQUFJO2dDQUNYLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxFQUFFO2dDQUMxQixjQUFjLEVBQUUsR0FBRzs2QkFDdEIsRUFBQSxDQUFDLENBQUMsQ0FBQzt5QkFDUDs7b0JBdEJMLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRTtnQ0FBNUIsQ0FBQztxQkF1QlQ7aUJBQ0o7cUJBQU07Ozt3QkFFRyxLQUFLLEdBQUcsU0FBUzt5QkFDbEIsV0FBVyxDQUFDLFlBQVksQ0FBQyxLQUFLLENBQUM7eUJBQy9CLEdBQUc7Ozs7b0JBQUMsVUFBQyxJQUFJLFdBQUssbUJBQVc7d0JBQ3RCLEtBQUssRUFBRSxJQUFJO3dCQUNYLElBQUksRUFBRSxJQUFJO3dCQUNWLGNBQWMsRUFBRSxHQUFHO3FCQUN0QixFQUFBLEdBQUEsRUFBQztvQkFFTixVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztpQkFDM0M7YUFDSjtZQUVELE9BQU8sQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUMsSUFBSTs7OztZQUFDLFVBQUMsTUFBTTtnQkFDaEMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxNQUFNOzs7OztnQkFBQyxVQUFDLENBQUMsRUFBRSxDQUFDLElBQUssT0FBQSxDQUFDLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxFQUFYLENBQVcsR0FBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3RELENBQUMsRUFBQyxDQUFDO1FBQ1AsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7OztPQUdHOzs7Ozs7SUFDSCx1Q0FBYTs7Ozs7SUFBYixVQUFjLEtBQVU7UUFDcEIsSUFBSSxJQUFJLENBQUMsV0FBVyxFQUFFLEVBQUU7O2dCQUNkLEtBQUssR0FBRyxDQUFDLG1CQUFtQixLQUFLLENBQUMsYUFBYSxFQUFBLENBQUM7O2dCQUNoRCxLQUFLLEdBQUcsU0FBUyxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBQ2hELElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLEdBQUc7Ozs7WUFBQyxVQUFDLElBQUksV0FBSyxtQkFBVztnQkFDOUMsS0FBSyxFQUFFLElBQUk7Z0JBQ1gsSUFBSSxFQUFFLElBQUk7Z0JBQ1YsY0FBYyxFQUFFLEdBQUc7YUFDdEIsRUFBQSxHQUFBLEVBQUMsQ0FBQyxDQUFDO1lBQ0osS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1NBQzNCO0lBQ0wsQ0FBQzs7Z0JBOU9KLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsY0FBYztpQkFDM0I7Ozs7Z0JBTG1CLFVBQVU7Z0JBQWtELFNBQVM7Z0JBQXBDLE1BQU07OzswQkFTdEQsS0FBSyxTQUFDLFlBQVk7dUJBSWxCLEtBQUssU0FBQyxpQkFBaUI7dUJBTXZCLEtBQUs7MkJBSUwsS0FBSzt5QkFJTCxLQUFLOzRCQUlMLEtBQUs7MEJBb0RMLFlBQVksU0FBQyxPQUFPLEVBQUUsQ0FBQyxRQUFRLENBQUM7O0lBK0pyQyxzQkFBQztDQUFBLEFBL09ELElBK09DO1NBNU9ZLGVBQWU7Ozs7OztJQUd4QixrQ0FDd0I7Ozs7O0lBR3hCLCtCQUNVOzs7Ozs7SUFLViwrQkFDMEI7Ozs7O0lBRzFCLG1DQUNrQjs7Ozs7SUFHbEIsaUNBQ2U7Ozs7O0lBR2Ysb0NBQ21COztJQUVuQixxQ0FBNEI7Ozs7O0lBRTVCLHVDQUFzRDs7Ozs7SUFDdEQsaUNBQWlDOzs7OztJQUNqQyxrQ0FBNkI7Ozs7O0lBRWpCLDZCQUFzQjs7Ozs7SUFBRSxtQ0FBMkI7Ozs7O0lBQUUsaUNBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOm5vLWlucHV0LXJlbmFtZSAgKi9cclxuXHJcbmltcG9ydCB7IERpcmVjdGl2ZSwgRWxlbWVudFJlZiwgSG9zdExpc3RlbmVyLCBJbnB1dCwgTmdab25lLCBPbkRlc3Ryb3ksIE9uSW5pdCwgUmVuZGVyZXIyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZpbGVJbmZvLCBGaWxlVXRpbHMgfSBmcm9tICcuLi91dGlscy9maWxlLXV0aWxzJztcclxuXHJcbkBEaXJlY3RpdmUoe1xyXG4gICAgc2VsZWN0b3I6ICdbYWRmLXVwbG9hZF0nXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBVcGxvYWREaXJlY3RpdmUgaW1wbGVtZW50cyBPbkluaXQsIE9uRGVzdHJveSB7XHJcblxyXG4gICAgLyoqIEVuYWJsZXMvZGlzYWJsZXMgdXBsb2FkaW5nLiAqL1xyXG4gICAgQElucHV0KCdhZGYtdXBsb2FkJylcclxuICAgIGVuYWJsZWQ6IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICAgIC8qKiBEYXRhIHRvIHVwbG9hZC4gKi9cclxuICAgIEBJbnB1dCgnYWRmLXVwbG9hZC1kYXRhJylcclxuICAgIGRhdGE6IGFueTtcclxuXHJcbiAgICAvKiogVXBsb2FkIG1vZGUuIENhbiBiZSBcImRyb3BcIiAocmVjZWl2ZXMgZHJvcHBlZCBmaWxlcykgb3IgXCJjbGlja1wiXHJcbiAgICAgKiAoY2xpY2tpbmcgb3BlbnMgYSBmaWxlIGRpYWxvZykuIEJvdGggbW9kZXMgY2FuIGJlIGFjdGl2ZSBhdCBvbmNlLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgbW9kZTogc3RyaW5nW10gPSBbJ2Ryb3AnXTsgLy8gY2xpY2t8ZHJvcFxyXG5cclxuICAgIC8qKiBUb2dnbGVzIG11bHRpcGxlIGZpbGUgdXBsb2Fkcy4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBtdWx0aXBsZTogYm9vbGVhbjtcclxuXHJcbiAgICAvKiogKENsaWNrIG1vZGUgb25seSkgTUlNRSB0eXBlIGZpbHRlciBmb3IgZmlsZXMgdG8gYWNjZXB0LiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGFjY2VwdDogc3RyaW5nO1xyXG5cclxuICAgIC8qKiAoQ2xpY2sgbW9kZSBvbmx5KSBUb2dnbGVzIHVwbG9hZGluZyBvZiBkaXJlY3Rvcmllcy4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBkaXJlY3Rvcnk6IGJvb2xlYW47XHJcblxyXG4gICAgaXNEcmFnZ2luZzogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIHByaXZhdGUgY3NzQ2xhc3NOYW1lOiBzdHJpbmcgPSAnYWRmLXVwbG9hZF9fZHJhZ2dpbmcnO1xyXG4gICAgcHJpdmF0ZSB1cGxvYWQ6IEhUTUxJbnB1dEVsZW1lbnQ7XHJcbiAgICBwcml2YXRlIGVsZW1lbnQ6IEhUTUxFbGVtZW50O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgZWw6IEVsZW1lbnRSZWYsIHByaXZhdGUgcmVuZGVyZXI6IFJlbmRlcmVyMiwgcHJpdmF0ZSBuZ1pvbmU6IE5nWm9uZSkge1xyXG4gICAgICAgIHRoaXMuZWxlbWVudCA9IGVsLm5hdGl2ZUVsZW1lbnQ7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNDbGlja01vZGUoKSAmJiB0aGlzLnJlbmRlcmVyKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGlucHV0VXBsb2FkID0gdGhpcy5yZW5kZXJlci5jcmVhdGVFbGVtZW50KCdpbnB1dCcpO1xyXG4gICAgICAgICAgICB0aGlzLnVwbG9hZCA9IHRoaXMuZWwubmF0aXZlRWxlbWVudC5wYXJlbnRFbGVtZW50LmFwcGVuZENoaWxkKGlucHV0VXBsb2FkKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMudXBsb2FkLnR5cGUgPSAnZmlsZSc7XHJcbiAgICAgICAgICAgIHRoaXMudXBsb2FkLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XHJcbiAgICAgICAgICAgIHRoaXMudXBsb2FkLmFkZEV2ZW50TGlzdGVuZXIoJ2NoYW5nZScsIChldmVudCkgPT4gdGhpcy5vblNlbGVjdEZpbGVzKGV2ZW50KSk7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5tdWx0aXBsZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy51cGxvYWQuc2V0QXR0cmlidXRlKCdtdWx0aXBsZScsICcnKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuYWNjZXB0KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVwbG9hZC5zZXRBdHRyaWJ1dGUoJ2FjY2VwdCcsIHRoaXMuYWNjZXB0KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuZGlyZWN0b3J5KSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVwbG9hZC5zZXRBdHRyaWJ1dGUoJ3dlYmtpdGRpcmVjdG9yeScsICcnKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuaXNEcm9wTW9kZSgpKSB7XHJcbiAgICAgICAgICAgIHRoaXMubmdab25lLnJ1bk91dHNpZGVBbmd1bGFyKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdkcmFnZW50ZXInLCB0aGlzLm9uRHJhZ0VudGVyLmJpbmQodGhpcykpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2RyYWdvdmVyJywgdGhpcy5vbkRyYWdPdmVyLmJpbmQodGhpcykpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5lbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ2RyYWdsZWF2ZScsIHRoaXMub25EcmFnTGVhdmUuYmluZCh0aGlzKSk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignZHJvcCcsIHRoaXMub25Ecm9wLmJpbmQodGhpcykpO1xyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAgICAgdGhpcy5lbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2RyYWdlbnRlcicsIHRoaXMub25EcmFnRW50ZXIpO1xyXG4gICAgICAgIHRoaXMuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdkcmFnb3ZlcicsIHRoaXMub25EcmFnT3Zlcik7XHJcbiAgICAgICAgdGhpcy5lbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ2RyYWdsZWF2ZScsIHRoaXMub25EcmFnTGVhdmUpO1xyXG4gICAgICAgIHRoaXMuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdkcm9wJywgdGhpcy5vbkRyb3ApO1xyXG4gICAgfVxyXG5cclxuICAgIEBIb3N0TGlzdGVuZXIoJ2NsaWNrJywgWyckZXZlbnQnXSlcclxuICAgIG9uQ2xpY2soZXZlbnQ6IEV2ZW50KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNDbGlja01vZGUoKSAmJiB0aGlzLnVwbG9hZCkge1xyXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG4gICAgICAgICAgICB0aGlzLnVwbG9hZC5jbGljaygpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvbkRyYWdFbnRlcihldmVudDogRXZlbnQpIHtcclxuICAgICAgICBpZiAodGhpcy5pc0Ryb3BNb2RlKCkpIHtcclxuICAgICAgICAgICAgdGhpcy5lbGVtZW50LmNsYXNzTGlzdC5hZGQodGhpcy5jc3NDbGFzc05hbWUpO1xyXG4gICAgICAgICAgICB0aGlzLmlzRHJhZ2dpbmcgPSB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvbkRyYWdPdmVyKGV2ZW50OiBFdmVudCkge1xyXG4gICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNEcm9wTW9kZSgpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QuYWRkKHRoaXMuY3NzQ2xhc3NOYW1lKTtcclxuICAgICAgICAgICAgdGhpcy5pc0RyYWdnaW5nID0gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRHJhZ0xlYXZlKGV2ZW50KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNEcm9wTW9kZSgpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5jbGFzc0xpc3QucmVtb3ZlKHRoaXMuY3NzQ2xhc3NOYW1lKTtcclxuICAgICAgICAgICAgdGhpcy5pc0RyYWdnaW5nID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uRHJvcChldmVudDogRXZlbnQpIHtcclxuICAgICAgICBpZiAodGhpcy5pc0Ryb3BNb2RlKCkpIHtcclxuXHJcbiAgICAgICAgICAgIGV2ZW50LnN0b3BQcm9wYWdhdGlvbigpO1xyXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5lbGVtZW50LmNsYXNzTGlzdC5yZW1vdmUodGhpcy5jc3NDbGFzc05hbWUpO1xyXG4gICAgICAgICAgICB0aGlzLmlzRHJhZ2dpbmcgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGRhdGFUcmFuc2ZlciA9IHRoaXMuZ2V0RGF0YVRyYW5zZmVyKGV2ZW50KTtcclxuICAgICAgICAgICAgaWYgKGRhdGFUcmFuc2Zlcikge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5nZXRGaWxlc0Ryb3BwZWQoZGF0YVRyYW5zZmVyKS50aGVuKChmaWxlcykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub25VcGxvYWRGaWxlcyhmaWxlcyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIG9uVXBsb2FkRmlsZXMoZmlsZXM6IEZpbGVJbmZvW10pIHtcclxuICAgICAgICBpZiAodGhpcy5lbmFibGVkICYmIGZpbGVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgY29uc3QgY3VzdG9tRXZlbnQgPSBuZXcgQ3VzdG9tRXZlbnQoJ3VwbG9hZC1maWxlcycsIHtcclxuICAgICAgICAgICAgICAgIGRldGFpbDoge1xyXG4gICAgICAgICAgICAgICAgICAgIHNlbmRlcjogdGhpcyxcclxuICAgICAgICAgICAgICAgICAgICBkYXRhOiB0aGlzLmRhdGEsXHJcbiAgICAgICAgICAgICAgICAgICAgZmlsZXM6IGZpbGVzXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgYnViYmxlczogdHJ1ZVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZWwubmF0aXZlRWxlbWVudC5kaXNwYXRjaEV2ZW50KGN1c3RvbUV2ZW50KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJvdGVjdGVkIGhhc01vZGUobW9kZTogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZW5hYmxlZCAmJiBtb2RlICYmIHRoaXMubW9kZSAmJiB0aGlzLm1vZGUuaW5kZXhPZihtb2RlKSA+IC0xO1xyXG4gICAgfVxyXG5cclxuICAgIHByb3RlY3RlZCBpc0Ryb3BNb2RlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmhhc01vZGUoJ2Ryb3AnKTtcclxuICAgIH1cclxuXHJcbiAgICBwcm90ZWN0ZWQgaXNDbGlja01vZGUoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaGFzTW9kZSgnY2xpY2snKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREYXRhVHJhbnNmZXIoZXZlbnQ6IEV2ZW50IHwgYW55KTogRGF0YVRyYW5zZmVyIHtcclxuICAgICAgICBpZiAoZXZlbnQgJiYgZXZlbnQuZGF0YVRyYW5zZmVyKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBldmVudC5kYXRhVHJhbnNmZXI7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChldmVudCAmJiBldmVudC5vcmlnaW5hbEV2ZW50ICYmIGV2ZW50Lm9yaWdpbmFsRXZlbnQuZGF0YVRyYW5zZmVyKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBldmVudC5vcmlnaW5hbEV2ZW50LmRhdGFUcmFuc2ZlcjtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBFeHRyYWN0IGZpbGVzIGZyb20gdGhlIERhdGFUcmFuc2ZlciBvYmplY3QgdXNlZCB0byBob2xkIHRoZSBkYXRhIHRoYXQgaXMgYmVpbmcgZHJhZ2dlZCBkdXJpbmcgYSBkcmFnIGFuZCBkcm9wIG9wZXJhdGlvbi5cclxuICAgICAqIEBwYXJhbSBkYXRhVHJhbnNmZXIgRGF0YVRyYW5zZmVyIG9iamVjdFxyXG4gICAgICovXHJcbiAgICBnZXRGaWxlc0Ryb3BwZWQoZGF0YVRyYW5zZmVyOiBEYXRhVHJhbnNmZXIpOiBQcm9taXNlPEZpbGVJbmZvW10+IHtcclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgaXRlcmF0aW9ucyA9IFtdO1xyXG5cclxuICAgICAgICAgICAgaWYgKGRhdGFUcmFuc2Zlcikge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgaXRlbXMgPSBkYXRhVHJhbnNmZXIuaXRlbXM7XHJcbiAgICAgICAgICAgICAgICBpZiAoaXRlbXMpIHtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGl0ZW1zLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICh0eXBlb2YgaXRlbXNbaV0ud2Via2l0R2V0QXNFbnRyeSAhPT0gJ3VuZGVmaW5lZCcpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGl0ZW0gPSBpdGVtc1tpXS53ZWJraXRHZXRBc0VudHJ5KCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZiAoaXRlbSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChpdGVtLmlzRmlsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVyYXRpb25zLnB1c2goUHJvbWlzZS5yZXNvbHZlKDxGaWxlSW5mbz4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZW50cnk6IGl0ZW0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlOiBpdGVtc1tpXS5nZXRBc0ZpbGUoKSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlbGF0aXZlRm9sZGVyOiAnLydcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoaXRlbS5pc0RpcmVjdG9yeSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVyYXRpb25zLnB1c2gobmV3IFByb21pc2UoKHJlc29sdmVGb2xkZXIpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIEZpbGVVdGlscy5mbGF0dGVuKGl0ZW0pLnRoZW4oKGZpbGVzKSA9PiByZXNvbHZlRm9sZGVyKGZpbGVzKSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBpdGVyYXRpb25zLnB1c2goUHJvbWlzZS5yZXNvbHZlKDxGaWxlSW5mbz4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVudHJ5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGU6IGl0ZW1zW2ldLmdldEFzRmlsZSgpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlbGF0aXZlRm9sZGVyOiAnLydcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gc2FmYXJpIG9yIEZGXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZmlsZXMgPSBGaWxlVXRpbHNcclxuICAgICAgICAgICAgICAgICAgICAgICAgLnRvRmlsZUFycmF5KGRhdGFUcmFuc2Zlci5maWxlcylcclxuICAgICAgICAgICAgICAgICAgICAgICAgLm1hcCgoZmlsZSkgPT4gPEZpbGVJbmZvPiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBlbnRyeTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGU6IGZpbGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZWxhdGl2ZUZvbGRlcjogJy8nXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBpdGVyYXRpb25zLnB1c2goUHJvbWlzZS5yZXNvbHZlKGZpbGVzKSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIFByb21pc2UuYWxsKGl0ZXJhdGlvbnMpLnRoZW4oKHJlc3VsdCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmVzb2x2ZShyZXN1bHQucmVkdWNlKChhLCBiKSA9PiBhLmNvbmNhdChiKSwgW10pKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBJbnZva2VkIHdoZW4gdXNlciBzZWxlY3RzIGZpbGVzIG9yIGZvbGRlcnMgYnkgbWVhbnMgb2YgRmlsZSBEaWFsb2dcclxuICAgICAqIEBwYXJhbSBldmVudCBET00gZXZlbnRcclxuICAgICAqL1xyXG4gICAgb25TZWxlY3RGaWxlcyhldmVudDogYW55KTogdm9pZCB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNDbGlja01vZGUoKSkge1xyXG4gICAgICAgICAgICBjb25zdCBpbnB1dCA9ICg8SFRNTElucHV0RWxlbWVudD4gZXZlbnQuY3VycmVudFRhcmdldCk7XHJcbiAgICAgICAgICAgIGNvbnN0IGZpbGVzID0gRmlsZVV0aWxzLnRvRmlsZUFycmF5KGlucHV0LmZpbGVzKTtcclxuICAgICAgICAgICAgdGhpcy5vblVwbG9hZEZpbGVzKGZpbGVzLm1hcCgoZmlsZSkgPT4gPEZpbGVJbmZvPiB7XHJcbiAgICAgICAgICAgICAgICBlbnRyeTogbnVsbCxcclxuICAgICAgICAgICAgICAgIGZpbGU6IGZpbGUsXHJcbiAgICAgICAgICAgICAgICByZWxhdGl2ZUZvbGRlcjogJy8nXHJcbiAgICAgICAgICAgIH0pKTtcclxuICAgICAgICAgICAgZXZlbnQudGFyZ2V0LnZhbHVlID0gJyc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==