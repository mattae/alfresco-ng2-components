/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * Base cancellable event implementation
 * @template T
 */
var /**
 * Base cancellable event implementation
 * @template T
 */
BaseEvent = /** @class */ (function () {
    function BaseEvent() {
        this.isDefaultPrevented = false;
    }
    Object.defineProperty(BaseEvent.prototype, "defaultPrevented", {
        get: /**
         * @return {?}
         */
        function () {
            return this.isDefaultPrevented;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    BaseEvent.prototype.preventDefault = /**
     * @return {?}
     */
    function () {
        this.isDefaultPrevented = true;
    };
    return BaseEvent;
}());
/**
 * Base cancellable event implementation
 * @template T
 */
export { BaseEvent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    BaseEvent.prototype.isDefaultPrevented;
    /** @type {?} */
    BaseEvent.prototype.value;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFzZS5ldmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImV2ZW50cy9iYXNlLmV2ZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQTs7Ozs7SUFBQTtRQUVZLHVCQUFrQixHQUFZLEtBQUssQ0FBQztJQVloRCxDQUFDO0lBUkcsc0JBQUksdUNBQWdCOzs7O1FBQXBCO1lBQ0ksT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUM7UUFDbkMsQ0FBQzs7O09BQUE7Ozs7SUFFRCxrQ0FBYzs7O0lBQWQ7UUFDSSxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO0lBQ25DLENBQUM7SUFFTCxnQkFBQztBQUFELENBQUMsQUFkRCxJQWNDOzs7Ozs7Ozs7OztJQVpHLHVDQUE0Qzs7SUFFNUMsMEJBQVMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyoqIEJhc2UgY2FuY2VsbGFibGUgZXZlbnQgaW1wbGVtZW50YXRpb24gKi9cclxuZXhwb3J0IGNsYXNzIEJhc2VFdmVudDxUPiB7XHJcblxyXG4gICAgcHJpdmF0ZSBpc0RlZmF1bHRQcmV2ZW50ZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICB2YWx1ZTogVDtcclxuXHJcbiAgICBnZXQgZGVmYXVsdFByZXZlbnRlZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pc0RlZmF1bHRQcmV2ZW50ZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHJldmVudERlZmF1bHQoKSB7XHJcbiAgICAgICAgdGhpcy5pc0RlZmF1bHRQcmV2ZW50ZWQgPSB0cnVlO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=