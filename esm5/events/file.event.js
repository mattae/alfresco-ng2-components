/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FileUploadStatus } from '../models/file.model';
var FileUploadEvent = /** @class */ (function () {
    function FileUploadEvent(file, status, error) {
        if (status === void 0) { status = FileUploadStatus.Pending; }
        if (error === void 0) { error = null; }
        this.file = file;
        this.status = status;
        this.error = error;
    }
    return FileUploadEvent;
}());
export { FileUploadEvent };
if (false) {
    /** @type {?} */
    FileUploadEvent.prototype.file;
    /** @type {?} */
    FileUploadEvent.prototype.status;
    /** @type {?} */
    FileUploadEvent.prototype.error;
}
var FileUploadCompleteEvent = /** @class */ (function (_super) {
    tslib_1.__extends(FileUploadCompleteEvent, _super);
    function FileUploadCompleteEvent(file, totalComplete, data, totalAborted) {
        if (totalComplete === void 0) { totalComplete = 0; }
        if (totalAborted === void 0) { totalAborted = 0; }
        var _this = _super.call(this, file, FileUploadStatus.Complete) || this;
        _this.totalComplete = totalComplete;
        _this.data = data;
        _this.totalAborted = totalAborted;
        return _this;
    }
    return FileUploadCompleteEvent;
}(FileUploadEvent));
export { FileUploadCompleteEvent };
if (false) {
    /** @type {?} */
    FileUploadCompleteEvent.prototype.totalComplete;
    /** @type {?} */
    FileUploadCompleteEvent.prototype.data;
    /** @type {?} */
    FileUploadCompleteEvent.prototype.totalAborted;
}
var FileUploadDeleteEvent = /** @class */ (function (_super) {
    tslib_1.__extends(FileUploadDeleteEvent, _super);
    function FileUploadDeleteEvent(file, totalComplete) {
        if (totalComplete === void 0) { totalComplete = 0; }
        var _this = _super.call(this, file, FileUploadStatus.Deleted) || this;
        _this.totalComplete = totalComplete;
        return _this;
    }
    return FileUploadDeleteEvent;
}(FileUploadEvent));
export { FileUploadDeleteEvent };
if (false) {
    /** @type {?} */
    FileUploadDeleteEvent.prototype.totalComplete;
}
var FileUploadErrorEvent = /** @class */ (function (_super) {
    tslib_1.__extends(FileUploadErrorEvent, _super);
    function FileUploadErrorEvent(file, error, totalError) {
        if (totalError === void 0) { totalError = 0; }
        var _this = _super.call(this, file, FileUploadStatus.Error) || this;
        _this.error = error;
        _this.totalError = totalError;
        return _this;
    }
    return FileUploadErrorEvent;
}(FileUploadEvent));
export { FileUploadErrorEvent };
if (false) {
    /** @type {?} */
    FileUploadErrorEvent.prototype.error;
    /** @type {?} */
    FileUploadErrorEvent.prototype.totalError;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZS5ldmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImV2ZW50cy9maWxlLmV2ZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQWEsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUVuRTtJQUVJLHlCQUNvQixJQUFlLEVBQ2YsTUFBbUQsRUFDbkQsS0FBaUI7UUFEakIsdUJBQUEsRUFBQSxTQUEyQixnQkFBZ0IsQ0FBQyxPQUFPO1FBQ25ELHNCQUFBLEVBQUEsWUFBaUI7UUFGakIsU0FBSSxHQUFKLElBQUksQ0FBVztRQUNmLFdBQU0sR0FBTixNQUFNLENBQTZDO1FBQ25ELFVBQUssR0FBTCxLQUFLLENBQVk7SUFDckMsQ0FBQztJQUVMLHNCQUFDO0FBQUQsQ0FBQyxBQVJELElBUUM7Ozs7SUFMTywrQkFBK0I7O0lBQy9CLGlDQUFtRTs7SUFDbkUsZ0NBQWlDOztBQUt6QztJQUE2QyxtREFBZTtJQUV4RCxpQ0FBWSxJQUFlLEVBQVMsYUFBeUIsRUFBUyxJQUFVLEVBQVMsWUFBd0I7UUFBN0UsOEJBQUEsRUFBQSxpQkFBeUI7UUFBNEIsNkJBQUEsRUFBQSxnQkFBd0I7UUFBakgsWUFDSSxrQkFBTSxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsUUFBUSxDQUFDLFNBQ3pDO1FBRm1DLG1CQUFhLEdBQWIsYUFBYSxDQUFZO1FBQVMsVUFBSSxHQUFKLElBQUksQ0FBTTtRQUFTLGtCQUFZLEdBQVosWUFBWSxDQUFZOztJQUVqSCxDQUFDO0lBRUwsOEJBQUM7QUFBRCxDQUFDLEFBTkQsQ0FBNkMsZUFBZSxHQU0zRDs7OztJQUpnQyxnREFBZ0M7O0lBQUUsdUNBQWlCOztJQUFFLCtDQUErQjs7QUFNckg7SUFBMkMsaURBQWU7SUFFdEQsK0JBQVksSUFBZSxFQUFTLGFBQXlCO1FBQXpCLDhCQUFBLEVBQUEsaUJBQXlCO1FBQTdELFlBQ0ksa0JBQU0sSUFBSSxFQUFFLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxTQUN4QztRQUZtQyxtQkFBYSxHQUFiLGFBQWEsQ0FBWTs7SUFFN0QsQ0FBQztJQUVMLDRCQUFDO0FBQUQsQ0FBQyxBQU5ELENBQTJDLGVBQWUsR0FNekQ7Ozs7SUFKZ0MsOENBQWdDOztBQU1qRTtJQUEwQyxnREFBZTtJQUVyRCw4QkFBWSxJQUFlLEVBQVMsS0FBVSxFQUFTLFVBQXNCO1FBQXRCLDJCQUFBLEVBQUEsY0FBc0I7UUFBN0UsWUFDSSxrQkFBTSxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsS0FBSyxDQUFDLFNBQ3RDO1FBRm1DLFdBQUssR0FBTCxLQUFLLENBQUs7UUFBUyxnQkFBVSxHQUFWLFVBQVUsQ0FBWTs7SUFFN0UsQ0FBQztJQUVMLDJCQUFDO0FBQUQsQ0FBQyxBQU5ELENBQTBDLGVBQWUsR0FNeEQ7Ozs7SUFKZ0MscUNBQWlCOztJQUFFLDBDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBGaWxlTW9kZWwsIEZpbGVVcGxvYWRTdGF0dXMgfSBmcm9tICcuLi9tb2RlbHMvZmlsZS5tb2RlbCc7XHJcblxyXG5leHBvcnQgY2xhc3MgRmlsZVVwbG9hZEV2ZW50IHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwdWJsaWMgcmVhZG9ubHkgZmlsZTogRmlsZU1vZGVsLFxyXG4gICAgICAgIHB1YmxpYyByZWFkb25seSBzdGF0dXM6IEZpbGVVcGxvYWRTdGF0dXMgPSBGaWxlVXBsb2FkU3RhdHVzLlBlbmRpbmcsXHJcbiAgICAgICAgcHVibGljIHJlYWRvbmx5IGVycm9yOiBhbnkgPSBudWxsKSB7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgRmlsZVVwbG9hZENvbXBsZXRlRXZlbnQgZXh0ZW5kcyBGaWxlVXBsb2FkRXZlbnQge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGZpbGU6IEZpbGVNb2RlbCwgcHVibGljIHRvdGFsQ29tcGxldGU6IG51bWJlciA9IDAsIHB1YmxpYyBkYXRhPzogYW55LCBwdWJsaWMgdG90YWxBYm9ydGVkOiBudW1iZXIgPSAwKSB7XHJcbiAgICAgICAgc3VwZXIoZmlsZSwgRmlsZVVwbG9hZFN0YXR1cy5Db21wbGV0ZSk7XHJcbiAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgRmlsZVVwbG9hZERlbGV0ZUV2ZW50IGV4dGVuZHMgRmlsZVVwbG9hZEV2ZW50IHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihmaWxlOiBGaWxlTW9kZWwsIHB1YmxpYyB0b3RhbENvbXBsZXRlOiBudW1iZXIgPSAwKSB7XHJcbiAgICAgICAgc3VwZXIoZmlsZSwgRmlsZVVwbG9hZFN0YXR1cy5EZWxldGVkKTtcclxuICAgIH1cclxuXHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBGaWxlVXBsb2FkRXJyb3JFdmVudCBleHRlbmRzIEZpbGVVcGxvYWRFdmVudCB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoZmlsZTogRmlsZU1vZGVsLCBwdWJsaWMgZXJyb3I6IGFueSwgcHVibGljIHRvdGFsRXJyb3I6IG51bWJlciA9IDApIHtcclxuICAgICAgICBzdXBlcihmaWxlLCBGaWxlVXBsb2FkU3RhdHVzLkVycm9yKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19