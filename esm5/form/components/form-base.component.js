/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FormOutcomeModel } from './widgets';
import { EventEmitter, Input, Output } from '@angular/core';
/**
 * @abstract
 */
var FormBaseComponent = /** @class */ (function () {
    function FormBaseComponent() {
        /**
         * Toggle rendering of the form title.
         */
        this.showTitle = true;
        /**
         * Toggle rendering of the `Complete` outcome button.
         */
        this.showCompleteButton = true;
        /**
         * If true then the `Complete` outcome button is shown but it will be disabled.
         */
        this.disableCompleteButton = false;
        /**
         * If true then the `Save` outcome button is shown but will be disabled.
         */
        this.disableSaveButton = false;
        /**
         * If true then the `Start Process` outcome button is shown but it will be disabled.
         */
        this.disableStartProcessButton = false;
        /**
         * Toggle rendering of the `Save` outcome button.
         */
        this.showSaveButton = true;
        /**
         * Toggle readonly state of the form. Forces all form widgets to render as readonly if enabled.
         */
        this.readOnly = false;
        /**
         * Toggle rendering of the `Refresh` button.
         */
        this.showRefreshButton = true;
        /**
         * Toggle rendering of the validation icon next to the form title.
         */
        this.showValidationIcon = true;
        /**
         * Emitted when the supplied form values have a validation error.
         */
        this.formError = new EventEmitter();
        /**
         * Emitted when any outcome is executed. Default behaviour can be prevented
         * via `event.preventDefault()`.
         */
        this.executeOutcome = new EventEmitter();
        /**
         * Emitted when any error occurs.
         */
        this.error = new EventEmitter();
    }
    /**
     * @return {?}
     */
    FormBaseComponent.prototype.getParsedFormDefinition = /**
     * @return {?}
     */
    function () {
        return this;
    };
    /**
     * @return {?}
     */
    FormBaseComponent.prototype.hasForm = /**
     * @return {?}
     */
    function () {
        return this.form ? true : false;
    };
    /**
     * @return {?}
     */
    FormBaseComponent.prototype.isTitleEnabled = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var titleEnabled = false;
        if (this.showTitle && this.form) {
            titleEnabled = true;
        }
        return titleEnabled;
    };
    /**
     * @param {?} outcomeName
     * @return {?}
     */
    FormBaseComponent.prototype.getColorForOutcome = /**
     * @param {?} outcomeName
     * @return {?}
     */
    function (outcomeName) {
        return outcomeName === FormBaseComponent.COMPLETE_OUTCOME_NAME ? FormBaseComponent.COMPLETE_BUTTON_COLOR : '';
    };
    /**
     * @param {?} outcome
     * @return {?}
     */
    FormBaseComponent.prototype.isOutcomeButtonEnabled = /**
     * @param {?} outcome
     * @return {?}
     */
    function (outcome) {
        if (this.form.readOnly) {
            return false;
        }
        if (outcome) {
            if (outcome.name === FormOutcomeModel.SAVE_ACTION) {
                return this.disableSaveButton ? false : this.form.isValid;
            }
            if (outcome.name === FormOutcomeModel.COMPLETE_ACTION) {
                return this.disableCompleteButton ? false : this.form.isValid;
            }
            if (outcome.name === FormOutcomeModel.START_PROCESS_ACTION) {
                return this.disableStartProcessButton ? false : this.form.isValid;
            }
            return this.form.isValid;
        }
        return false;
    };
    /**
     * @param {?} outcome
     * @param {?} isFormReadOnly
     * @return {?}
     */
    FormBaseComponent.prototype.isOutcomeButtonVisible = /**
     * @param {?} outcome
     * @param {?} isFormReadOnly
     * @return {?}
     */
    function (outcome, isFormReadOnly) {
        if (outcome && outcome.name) {
            if (outcome.name === FormOutcomeModel.COMPLETE_ACTION) {
                return this.showCompleteButton;
            }
            if (isFormReadOnly) {
                return outcome.isSelected;
            }
            if (outcome.name === FormOutcomeModel.SAVE_ACTION) {
                return this.showSaveButton;
            }
            if (outcome.name === FormOutcomeModel.START_PROCESS_ACTION) {
                return false;
            }
            return true;
        }
        return false;
    };
    /**
     * Invoked when user clicks outcome button.
     * @param outcome Form outcome model
     */
    /**
     * Invoked when user clicks outcome button.
     * @param {?} outcome Form outcome model
     * @return {?}
     */
    FormBaseComponent.prototype.onOutcomeClicked = /**
     * Invoked when user clicks outcome button.
     * @param {?} outcome Form outcome model
     * @return {?}
     */
    function (outcome) {
        if (!this.readOnly && outcome && this.form) {
            if (!this.onExecuteOutcome(outcome)) {
                return false;
            }
            if (outcome.isSystem) {
                if (outcome.id === FormBaseComponent.SAVE_OUTCOME_ID) {
                    this.saveTaskForm();
                    return true;
                }
                if (outcome.id === FormBaseComponent.COMPLETE_OUTCOME_ID) {
                    this.completeTaskForm();
                    return true;
                }
                if (outcome.id === FormBaseComponent.START_PROCESS_OUTCOME_ID) {
                    this.completeTaskForm();
                    return true;
                }
                if (outcome.id === FormBaseComponent.CUSTOM_OUTCOME_ID) {
                    this.onTaskSaved(this.form);
                    this.storeFormAsMetadata();
                    return true;
                }
            }
            else {
                // Note: Activiti is using NAME field rather than ID for outcomes
                if (outcome.name) {
                    this.onTaskSaved(this.form);
                    this.completeTaskForm(outcome.name);
                    return true;
                }
            }
        }
        return false;
    };
    /**
     * @param {?} err
     * @return {?}
     */
    FormBaseComponent.prototype.handleError = /**
     * @param {?} err
     * @return {?}
     */
    function (err) {
        this.error.emit(err);
    };
    FormBaseComponent.SAVE_OUTCOME_ID = '$save';
    FormBaseComponent.COMPLETE_OUTCOME_ID = '$complete';
    FormBaseComponent.START_PROCESS_OUTCOME_ID = '$startProcess';
    FormBaseComponent.CUSTOM_OUTCOME_ID = '$custom';
    FormBaseComponent.COMPLETE_BUTTON_COLOR = 'primary';
    FormBaseComponent.COMPLETE_OUTCOME_NAME = 'COMPLETE';
    FormBaseComponent.propDecorators = {
        path: [{ type: Input }],
        nameNode: [{ type: Input }],
        showTitle: [{ type: Input }],
        showCompleteButton: [{ type: Input }],
        disableCompleteButton: [{ type: Input }],
        disableSaveButton: [{ type: Input }],
        disableStartProcessButton: [{ type: Input }],
        showSaveButton: [{ type: Input }],
        readOnly: [{ type: Input }],
        showRefreshButton: [{ type: Input }],
        showValidationIcon: [{ type: Input }],
        fieldValidators: [{ type: Input }],
        formError: [{ type: Output }],
        executeOutcome: [{ type: Output }],
        error: [{ type: Output }]
    };
    return FormBaseComponent;
}());
export { FormBaseComponent };
if (false) {
    /** @type {?} */
    FormBaseComponent.SAVE_OUTCOME_ID;
    /** @type {?} */
    FormBaseComponent.COMPLETE_OUTCOME_ID;
    /** @type {?} */
    FormBaseComponent.START_PROCESS_OUTCOME_ID;
    /** @type {?} */
    FormBaseComponent.CUSTOM_OUTCOME_ID;
    /** @type {?} */
    FormBaseComponent.COMPLETE_BUTTON_COLOR;
    /** @type {?} */
    FormBaseComponent.COMPLETE_OUTCOME_NAME;
    /**
     * Path of the folder where the metadata will be stored.
     * @type {?}
     */
    FormBaseComponent.prototype.path;
    /**
     * Name to assign to the new node where the metadata are stored.
     * @type {?}
     */
    FormBaseComponent.prototype.nameNode;
    /**
     * Toggle rendering of the form title.
     * @type {?}
     */
    FormBaseComponent.prototype.showTitle;
    /**
     * Toggle rendering of the `Complete` outcome button.
     * @type {?}
     */
    FormBaseComponent.prototype.showCompleteButton;
    /**
     * If true then the `Complete` outcome button is shown but it will be disabled.
     * @type {?}
     */
    FormBaseComponent.prototype.disableCompleteButton;
    /**
     * If true then the `Save` outcome button is shown but will be disabled.
     * @type {?}
     */
    FormBaseComponent.prototype.disableSaveButton;
    /**
     * If true then the `Start Process` outcome button is shown but it will be disabled.
     * @type {?}
     */
    FormBaseComponent.prototype.disableStartProcessButton;
    /**
     * Toggle rendering of the `Save` outcome button.
     * @type {?}
     */
    FormBaseComponent.prototype.showSaveButton;
    /**
     * Toggle readonly state of the form. Forces all form widgets to render as readonly if enabled.
     * @type {?}
     */
    FormBaseComponent.prototype.readOnly;
    /**
     * Toggle rendering of the `Refresh` button.
     * @type {?}
     */
    FormBaseComponent.prototype.showRefreshButton;
    /**
     * Toggle rendering of the validation icon next to the form title.
     * @type {?}
     */
    FormBaseComponent.prototype.showValidationIcon;
    /**
     * Contains a list of form field validator instances.
     * @type {?}
     */
    FormBaseComponent.prototype.fieldValidators;
    /**
     * Emitted when the supplied form values have a validation error.
     * @type {?}
     */
    FormBaseComponent.prototype.formError;
    /**
     * Emitted when any outcome is executed. Default behaviour can be prevented
     * via `event.preventDefault()`.
     * @type {?}
     */
    FormBaseComponent.prototype.executeOutcome;
    /**
     * Emitted when any error occurs.
     * @type {?}
     */
    FormBaseComponent.prototype.error;
    /** @type {?} */
    FormBaseComponent.prototype.form;
    /**
     * @abstract
     * @return {?}
     */
    FormBaseComponent.prototype.onRefreshClicked = function () { };
    /**
     * @abstract
     * @return {?}
     */
    FormBaseComponent.prototype.saveTaskForm = function () { };
    /**
     * @abstract
     * @param {?=} outcome
     * @return {?}
     */
    FormBaseComponent.prototype.completeTaskForm = function (outcome) { };
    /**
     * @abstract
     * @protected
     * @param {?} form
     * @return {?}
     */
    FormBaseComponent.prototype.onTaskSaved = function (form) { };
    /**
     * @abstract
     * @protected
     * @return {?}
     */
    FormBaseComponent.prototype.storeFormAsMetadata = function () { };
    /**
     * @abstract
     * @protected
     * @param {?} outcome
     * @return {?}
     */
    FormBaseComponent.prototype.onExecuteOutcome = function (outcome) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1iYXNlLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy9mb3JtLWJhc2UuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBLE9BQU8sRUFBRSxnQkFBZ0IsRUFBd0QsTUFBTSxXQUFXLENBQUM7QUFDbkcsT0FBTyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDOzs7O0FBRTVEO0lBQUE7Ozs7UUFtQkksY0FBUyxHQUFZLElBQUksQ0FBQzs7OztRQUkxQix1QkFBa0IsR0FBWSxJQUFJLENBQUM7Ozs7UUFJbkMsMEJBQXFCLEdBQVksS0FBSyxDQUFDOzs7O1FBSXZDLHNCQUFpQixHQUFZLEtBQUssQ0FBQzs7OztRQUluQyw4QkFBeUIsR0FBWSxLQUFLLENBQUM7Ozs7UUFJM0MsbUJBQWMsR0FBWSxJQUFJLENBQUM7Ozs7UUFJL0IsYUFBUSxHQUFZLEtBQUssQ0FBQzs7OztRQUkxQixzQkFBaUIsR0FBWSxJQUFJLENBQUM7Ozs7UUFJbEMsdUJBQWtCLEdBQVksSUFBSSxDQUFDOzs7O1FBUW5DLGNBQVMsR0FBbUMsSUFBSSxZQUFZLEVBQW9CLENBQUM7Ozs7O1FBTWpGLG1CQUFjLEdBQW1DLElBQUksWUFBWSxFQUFvQixDQUFDOzs7O1FBTXRGLFVBQUssR0FBc0IsSUFBSSxZQUFZLEVBQU8sQ0FBQztJQTJIdkQsQ0FBQzs7OztJQXZIRyxtREFBdUI7OztJQUF2QjtRQUNJLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7SUFFRCxtQ0FBTzs7O0lBQVA7UUFDSSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ3BDLENBQUM7Ozs7SUFFRCwwQ0FBYzs7O0lBQWQ7O1lBQ1EsWUFBWSxHQUFHLEtBQUs7UUFDeEIsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDN0IsWUFBWSxHQUFHLElBQUksQ0FBQztTQUN2QjtRQUNELE9BQU8sWUFBWSxDQUFDO0lBQ3hCLENBQUM7Ozs7O0lBRUQsOENBQWtCOzs7O0lBQWxCLFVBQW1CLFdBQW1CO1FBQ2xDLE9BQU8sV0FBVyxLQUFLLGlCQUFpQixDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxpQkFBaUIsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQ2xILENBQUM7Ozs7O0lBRUQsa0RBQXNCOzs7O0lBQXRCLFVBQXVCLE9BQXlCO1FBQzVDLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDcEIsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxJQUFJLE9BQU8sRUFBRTtZQUNULElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUU7Z0JBQy9DLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO2FBQzdEO1lBQ0QsSUFBSSxPQUFPLENBQUMsSUFBSSxLQUFLLGdCQUFnQixDQUFDLGVBQWUsRUFBRTtnQkFDbkQsT0FBTyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7YUFDakU7WUFDRCxJQUFJLE9BQU8sQ0FBQyxJQUFJLEtBQUssZ0JBQWdCLENBQUMsb0JBQW9CLEVBQUU7Z0JBQ3hELE9BQU8sSUFBSSxDQUFDLHlCQUF5QixDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO2FBQ3JFO1lBQ0QsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztTQUM1QjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7OztJQUVELGtEQUFzQjs7Ozs7SUFBdEIsVUFBdUIsT0FBeUIsRUFBRSxjQUF1QjtRQUNyRSxJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMsSUFBSSxFQUFFO1lBQ3pCLElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxnQkFBZ0IsQ0FBQyxlQUFlLEVBQUU7Z0JBQ25ELE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDO2FBQ2xDO1lBQ0QsSUFBSSxjQUFjLEVBQUU7Z0JBQ2hCLE9BQU8sT0FBTyxDQUFDLFVBQVUsQ0FBQzthQUM3QjtZQUNELElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUU7Z0JBQy9DLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQzthQUM5QjtZQUNELElBQUksT0FBTyxDQUFDLElBQUksS0FBSyxnQkFBZ0IsQ0FBQyxvQkFBb0IsRUFBRTtnQkFDeEQsT0FBTyxLQUFLLENBQUM7YUFDaEI7WUFDRCxPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7O0lBQ0gsNENBQWdCOzs7OztJQUFoQixVQUFpQixPQUF5QjtRQUN0QyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxPQUFPLElBQUksSUFBSSxDQUFDLElBQUksRUFBRTtZQUV4QyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUNqQyxPQUFPLEtBQUssQ0FBQzthQUNoQjtZQUVELElBQUksT0FBTyxDQUFDLFFBQVEsRUFBRTtnQkFDbEIsSUFBSSxPQUFPLENBQUMsRUFBRSxLQUFLLGlCQUFpQixDQUFDLGVBQWUsRUFBRTtvQkFDbEQsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO29CQUNwQixPQUFPLElBQUksQ0FBQztpQkFDZjtnQkFFRCxJQUFJLE9BQU8sQ0FBQyxFQUFFLEtBQUssaUJBQWlCLENBQUMsbUJBQW1CLEVBQUU7b0JBQ3RELElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO29CQUN4QixPQUFPLElBQUksQ0FBQztpQkFDZjtnQkFFRCxJQUFJLE9BQU8sQ0FBQyxFQUFFLEtBQUssaUJBQWlCLENBQUMsd0JBQXdCLEVBQUU7b0JBQzNELElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDO29CQUN4QixPQUFPLElBQUksQ0FBQztpQkFDZjtnQkFFRCxJQUFJLE9BQU8sQ0FBQyxFQUFFLEtBQUssaUJBQWlCLENBQUMsaUJBQWlCLEVBQUU7b0JBQ3BELElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO29CQUM1QixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztvQkFDM0IsT0FBTyxJQUFJLENBQUM7aUJBQ2Y7YUFDSjtpQkFBTTtnQkFDSCxpRUFBaUU7Z0JBQ2pFLElBQUksT0FBTyxDQUFDLElBQUksRUFBRTtvQkFDZCxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDNUIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztvQkFDcEMsT0FBTyxJQUFJLENBQUM7aUJBQ2Y7YUFDSjtTQUNKO1FBRUQsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7Ozs7SUFFRCx1Q0FBVzs7OztJQUFYLFVBQVksR0FBUTtRQUNoQixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBbkxNLGlDQUFlLEdBQVcsT0FBTyxDQUFDO0lBQ2xDLHFDQUFtQixHQUFXLFdBQVcsQ0FBQztJQUMxQywwQ0FBd0IsR0FBVyxlQUFlLENBQUM7SUFDbkQsbUNBQWlCLEdBQVcsU0FBUyxDQUFDO0lBQ3RDLHVDQUFxQixHQUFXLFNBQVMsQ0FBQztJQUMxQyx1Q0FBcUIsR0FBVyxVQUFVLENBQUM7O3VCQUdqRCxLQUFLOzJCQUlMLEtBQUs7NEJBSUwsS0FBSztxQ0FJTCxLQUFLO3dDQUlMLEtBQUs7b0NBSUwsS0FBSzs0Q0FJTCxLQUFLO2lDQUlMLEtBQUs7MkJBSUwsS0FBSztvQ0FJTCxLQUFLO3FDQUlMLEtBQUs7a0NBSUwsS0FBSzs0QkFJTCxNQUFNO2lDQU1OLE1BQU07d0JBTU4sTUFBTTs7SUE0SFgsd0JBQUM7Q0FBQSxBQWxNRCxJQWtNQztTQWxNcUIsaUJBQWlCOzs7SUFFbkMsa0NBQXlDOztJQUN6QyxzQ0FBaUQ7O0lBQ2pELDJDQUEwRDs7SUFDMUQsb0NBQTZDOztJQUM3Qyx3Q0FBaUQ7O0lBQ2pELHdDQUFrRDs7Ozs7SUFHbEQsaUNBQ2E7Ozs7O0lBR2IscUNBQ2lCOzs7OztJQUdqQixzQ0FDMEI7Ozs7O0lBRzFCLCtDQUNtQzs7Ozs7SUFHbkMsa0RBQ3VDOzs7OztJQUd2Qyw4Q0FDbUM7Ozs7O0lBR25DLHNEQUMyQzs7Ozs7SUFHM0MsMkNBQytCOzs7OztJQUcvQixxQ0FDMEI7Ozs7O0lBRzFCLDhDQUNrQzs7Ozs7SUFHbEMsK0NBQ21DOzs7OztJQUduQyw0Q0FDc0M7Ozs7O0lBR3RDLHNDQUNpRjs7Ozs7O0lBS2pGLDJDQUNzRjs7Ozs7SUFLdEYsa0NBQ21EOztJQUVuRCxpQ0FBb0I7Ozs7O0lBOEdwQiwrREFBNEI7Ozs7O0lBRTVCLDJEQUF3Qjs7Ozs7O0lBRXhCLHNFQUE0Qzs7Ozs7OztJQUU1Qyw4REFBb0Q7Ozs7OztJQUVwRCxrRUFBeUM7Ozs7Ozs7SUFFekMsc0VBQStEIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEZvcm1CYXNlTW9kZWwgfSBmcm9tICcuL2Zvcm0tYmFzZS5tb2RlbCc7XHJcbmltcG9ydCB7IEZvcm1PdXRjb21lTW9kZWwsIEZvcm1GaWVsZFZhbGlkYXRvciwgRm9ybUZpZWxkTW9kZWwsIEZvcm1PdXRjb21lRXZlbnQgfSBmcm9tICcuL3dpZGdldHMnO1xyXG5pbXBvcnQgeyBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmV4cG9ydCBhYnN0cmFjdCBjbGFzcyBGb3JtQmFzZUNvbXBvbmVudCB7XHJcblxyXG4gICAgc3RhdGljIFNBVkVfT1VUQ09NRV9JRDogc3RyaW5nID0gJyRzYXZlJztcclxuICAgIHN0YXRpYyBDT01QTEVURV9PVVRDT01FX0lEOiBzdHJpbmcgPSAnJGNvbXBsZXRlJztcclxuICAgIHN0YXRpYyBTVEFSVF9QUk9DRVNTX09VVENPTUVfSUQ6IHN0cmluZyA9ICckc3RhcnRQcm9jZXNzJztcclxuICAgIHN0YXRpYyBDVVNUT01fT1VUQ09NRV9JRDogc3RyaW5nID0gJyRjdXN0b20nO1xyXG4gICAgc3RhdGljIENPTVBMRVRFX0JVVFRPTl9DT0xPUjogc3RyaW5nID0gJ3ByaW1hcnknO1xyXG4gICAgc3RhdGljIENPTVBMRVRFX09VVENPTUVfTkFNRTogc3RyaW5nID0gJ0NPTVBMRVRFJztcclxuXHJcbiAgICAvKiogUGF0aCBvZiB0aGUgZm9sZGVyIHdoZXJlIHRoZSBtZXRhZGF0YSB3aWxsIGJlIHN0b3JlZC4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBwYXRoOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqIE5hbWUgdG8gYXNzaWduIHRvIHRoZSBuZXcgbm9kZSB3aGVyZSB0aGUgbWV0YWRhdGEgYXJlIHN0b3JlZC4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBuYW1lTm9kZTogc3RyaW5nO1xyXG5cclxuICAgIC8qKiBUb2dnbGUgcmVuZGVyaW5nIG9mIHRoZSBmb3JtIHRpdGxlLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHNob3dUaXRsZTogYm9vbGVhbiA9IHRydWU7XHJcblxyXG4gICAgLyoqIFRvZ2dsZSByZW5kZXJpbmcgb2YgdGhlIGBDb21wbGV0ZWAgb3V0Y29tZSBidXR0b24uICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgc2hvd0NvbXBsZXRlQnV0dG9uOiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgICAvKiogSWYgdHJ1ZSB0aGVuIHRoZSBgQ29tcGxldGVgIG91dGNvbWUgYnV0dG9uIGlzIHNob3duIGJ1dCBpdCB3aWxsIGJlIGRpc2FibGVkLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGRpc2FibGVDb21wbGV0ZUJ1dHRvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIC8qKiBJZiB0cnVlIHRoZW4gdGhlIGBTYXZlYCBvdXRjb21lIGJ1dHRvbiBpcyBzaG93biBidXQgd2lsbCBiZSBkaXNhYmxlZC4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBkaXNhYmxlU2F2ZUJ1dHRvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIC8qKiBJZiB0cnVlIHRoZW4gdGhlIGBTdGFydCBQcm9jZXNzYCBvdXRjb21lIGJ1dHRvbiBpcyBzaG93biBidXQgaXQgd2lsbCBiZSBkaXNhYmxlZC4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBkaXNhYmxlU3RhcnRQcm9jZXNzQnV0dG9uOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgLyoqIFRvZ2dsZSByZW5kZXJpbmcgb2YgdGhlIGBTYXZlYCBvdXRjb21lIGJ1dHRvbi4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzaG93U2F2ZUJ1dHRvbjogYm9vbGVhbiA9IHRydWU7XHJcblxyXG4gICAgLyoqIFRvZ2dsZSByZWFkb25seSBzdGF0ZSBvZiB0aGUgZm9ybS4gRm9yY2VzIGFsbCBmb3JtIHdpZGdldHMgdG8gcmVuZGVyIGFzIHJlYWRvbmx5IGlmIGVuYWJsZWQuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgcmVhZE9ubHk6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICAvKiogVG9nZ2xlIHJlbmRlcmluZyBvZiB0aGUgYFJlZnJlc2hgIGJ1dHRvbi4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzaG93UmVmcmVzaEJ1dHRvbjogYm9vbGVhbiA9IHRydWU7XHJcblxyXG4gICAgLyoqIFRvZ2dsZSByZW5kZXJpbmcgb2YgdGhlIHZhbGlkYXRpb24gaWNvbiBuZXh0IHRvIHRoZSBmb3JtIHRpdGxlLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHNob3dWYWxpZGF0aW9uSWNvbjogYm9vbGVhbiA9IHRydWU7XHJcblxyXG4gICAgLyoqIENvbnRhaW5zIGEgbGlzdCBvZiBmb3JtIGZpZWxkIHZhbGlkYXRvciBpbnN0YW5jZXMuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgZmllbGRWYWxpZGF0b3JzOiBGb3JtRmllbGRWYWxpZGF0b3JbXTtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSBzdXBwbGllZCBmb3JtIHZhbHVlcyBoYXZlIGEgdmFsaWRhdGlvbiBlcnJvci4gKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgZm9ybUVycm9yOiBFdmVudEVtaXR0ZXI8Rm9ybUZpZWxkTW9kZWxbXT4gPSBuZXcgRXZlbnRFbWl0dGVyPEZvcm1GaWVsZE1vZGVsW10+KCk7XHJcblxyXG4gICAgLyoqIEVtaXR0ZWQgd2hlbiBhbnkgb3V0Y29tZSBpcyBleGVjdXRlZC4gRGVmYXVsdCBiZWhhdmlvdXIgY2FuIGJlIHByZXZlbnRlZFxyXG4gICAgICogdmlhIGBldmVudC5wcmV2ZW50RGVmYXVsdCgpYC5cclxuICAgICAqL1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICBleGVjdXRlT3V0Y29tZTogRXZlbnRFbWl0dGVyPEZvcm1PdXRjb21lRXZlbnQ+ID0gbmV3IEV2ZW50RW1pdHRlcjxGb3JtT3V0Y29tZUV2ZW50PigpO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogRW1pdHRlZCB3aGVuIGFueSBlcnJvciBvY2N1cnMuXHJcbiAgICAgKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgZXJyb3I6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgZm9ybTogRm9ybUJhc2VNb2RlbDtcclxuXHJcbiAgICBnZXRQYXJzZWRGb3JtRGVmaW5pdGlvbigpOiBGb3JtQmFzZUNvbXBvbmVudCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXM7XHJcbiAgICB9XHJcblxyXG4gICAgaGFzRm9ybSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5mb3JtID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzVGl0bGVFbmFibGVkKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGxldCB0aXRsZUVuYWJsZWQgPSBmYWxzZTtcclxuICAgICAgICBpZiAodGhpcy5zaG93VGl0bGUgJiYgdGhpcy5mb3JtKSB7XHJcbiAgICAgICAgICAgIHRpdGxlRW5hYmxlZCA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aXRsZUVuYWJsZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q29sb3JGb3JPdXRjb21lKG91dGNvbWVOYW1lOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBvdXRjb21lTmFtZSA9PT0gRm9ybUJhc2VDb21wb25lbnQuQ09NUExFVEVfT1VUQ09NRV9OQU1FID8gRm9ybUJhc2VDb21wb25lbnQuQ09NUExFVEVfQlVUVE9OX0NPTE9SIDogJyc7XHJcbiAgICB9XHJcblxyXG4gICAgaXNPdXRjb21lQnV0dG9uRW5hYmxlZChvdXRjb21lOiBGb3JtT3V0Y29tZU1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHRoaXMuZm9ybS5yZWFkT25seSkge1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAob3V0Y29tZSkge1xyXG4gICAgICAgICAgICBpZiAob3V0Y29tZS5uYW1lID09PSBGb3JtT3V0Y29tZU1vZGVsLlNBVkVfQUNUSU9OKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5kaXNhYmxlU2F2ZUJ1dHRvbiA/IGZhbHNlIDogdGhpcy5mb3JtLmlzVmFsaWQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKG91dGNvbWUubmFtZSA9PT0gRm9ybU91dGNvbWVNb2RlbC5DT01QTEVURV9BQ1RJT04pIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmRpc2FibGVDb21wbGV0ZUJ1dHRvbiA/IGZhbHNlIDogdGhpcy5mb3JtLmlzVmFsaWQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKG91dGNvbWUubmFtZSA9PT0gRm9ybU91dGNvbWVNb2RlbC5TVEFSVF9QUk9DRVNTX0FDVElPTikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZGlzYWJsZVN0YXJ0UHJvY2Vzc0J1dHRvbiA/IGZhbHNlIDogdGhpcy5mb3JtLmlzVmFsaWQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZm9ybS5pc1ZhbGlkO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaXNPdXRjb21lQnV0dG9uVmlzaWJsZShvdXRjb21lOiBGb3JtT3V0Y29tZU1vZGVsLCBpc0Zvcm1SZWFkT25seTogYm9vbGVhbik6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmIChvdXRjb21lICYmIG91dGNvbWUubmFtZSkge1xyXG4gICAgICAgICAgICBpZiAob3V0Y29tZS5uYW1lID09PSBGb3JtT3V0Y29tZU1vZGVsLkNPTVBMRVRFX0FDVElPTikge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuc2hvd0NvbXBsZXRlQnV0dG9uO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChpc0Zvcm1SZWFkT25seSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIG91dGNvbWUuaXNTZWxlY3RlZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAob3V0Y29tZS5uYW1lID09PSBGb3JtT3V0Y29tZU1vZGVsLlNBVkVfQUNUSU9OKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5zaG93U2F2ZUJ1dHRvbjtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAob3V0Y29tZS5uYW1lID09PSBGb3JtT3V0Y29tZU1vZGVsLlNUQVJUX1BST0NFU1NfQUNUSU9OKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEludm9rZWQgd2hlbiB1c2VyIGNsaWNrcyBvdXRjb21lIGJ1dHRvbi5cclxuICAgICAqIEBwYXJhbSBvdXRjb21lIEZvcm0gb3V0Y29tZSBtb2RlbFxyXG4gICAgICovXHJcbiAgICBvbk91dGNvbWVDbGlja2VkKG91dGNvbWU6IEZvcm1PdXRjb21lTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAoIXRoaXMucmVhZE9ubHkgJiYgb3V0Y29tZSAmJiB0aGlzLmZvcm0pIHtcclxuXHJcbiAgICAgICAgICAgIGlmICghdGhpcy5vbkV4ZWN1dGVPdXRjb21lKG91dGNvbWUpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChvdXRjb21lLmlzU3lzdGVtKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAob3V0Y29tZS5pZCA9PT0gRm9ybUJhc2VDb21wb25lbnQuU0FWRV9PVVRDT01FX0lEKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZlVGFza0Zvcm0oKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAob3V0Y29tZS5pZCA9PT0gRm9ybUJhc2VDb21wb25lbnQuQ09NUExFVEVfT1VUQ09NRV9JRCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29tcGxldGVUYXNrRm9ybSgpO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIGlmIChvdXRjb21lLmlkID09PSBGb3JtQmFzZUNvbXBvbmVudC5TVEFSVF9QUk9DRVNTX09VVENPTUVfSUQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbXBsZXRlVGFza0Zvcm0oKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAob3V0Y29tZS5pZCA9PT0gRm9ybUJhc2VDb21wb25lbnQuQ1VTVE9NX09VVENPTUVfSUQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9uVGFza1NhdmVkKHRoaXMuZm9ybSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zdG9yZUZvcm1Bc01ldGFkYXRhKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAvLyBOb3RlOiBBY3Rpdml0aSBpcyB1c2luZyBOQU1FIGZpZWxkIHJhdGhlciB0aGFuIElEIGZvciBvdXRjb21lc1xyXG4gICAgICAgICAgICAgICAgaWYgKG91dGNvbWUubmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub25UYXNrU2F2ZWQodGhpcy5mb3JtKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbXBsZXRlVGFza0Zvcm0ob3V0Y29tZS5uYW1lKTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGhhbmRsZUVycm9yKGVycjogYW55KTogYW55IHtcclxuICAgICAgICB0aGlzLmVycm9yLmVtaXQoZXJyKTtcclxuICAgIH1cclxuXHJcbiAgICBhYnN0cmFjdCBvblJlZnJlc2hDbGlja2VkKCk7XHJcblxyXG4gICAgYWJzdHJhY3Qgc2F2ZVRhc2tGb3JtKCk7XHJcblxyXG4gICAgYWJzdHJhY3QgY29tcGxldGVUYXNrRm9ybShvdXRjb21lPzogc3RyaW5nKTtcclxuXHJcbiAgICBwcm90ZWN0ZWQgYWJzdHJhY3Qgb25UYXNrU2F2ZWQoZm9ybTogRm9ybUJhc2VNb2RlbCk7XHJcblxyXG4gICAgcHJvdGVjdGVkIGFic3RyYWN0IHN0b3JlRm9ybUFzTWV0YWRhdGEoKTtcclxuXHJcbiAgICBwcm90ZWN0ZWQgYWJzdHJhY3Qgb25FeGVjdXRlT3V0Y29tZShvdXRjb21lOiBGb3JtT3V0Y29tZU1vZGVsKTtcclxufVxyXG4iXX0=