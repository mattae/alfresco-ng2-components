/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ContainerModel } from './widgets/core/container.model';
/**
 * @abstract
 */
var FormBaseModel = /** @class */ (function () {
    function FormBaseModel() {
        this.values = {};
        this.tabs = [];
        this.fields = [];
        this.outcomes = [];
        this.readOnly = false;
        this.isValid = true;
    }
    /**
     * @return {?}
     */
    FormBaseModel.prototype.hasTabs = /**
     * @return {?}
     */
    function () {
        return this.tabs && this.tabs.length > 0;
    };
    /**
     * @return {?}
     */
    FormBaseModel.prototype.hasFields = /**
     * @return {?}
     */
    function () {
        return this.fields && this.fields.length > 0;
    };
    /**
     * @return {?}
     */
    FormBaseModel.prototype.hasOutcomes = /**
     * @return {?}
     */
    function () {
        return this.outcomes && this.outcomes.length > 0;
    };
    /**
     * @param {?} fieldId
     * @return {?}
     */
    FormBaseModel.prototype.getFieldById = /**
     * @param {?} fieldId
     * @return {?}
     */
    function (fieldId) {
        return this.getFormFields().find((/**
         * @param {?} field
         * @return {?}
         */
        function (field) { return field.id === fieldId; }));
    };
    // TODO: consider evaluating and caching once the form is loaded
    // TODO: consider evaluating and caching once the form is loaded
    /**
     * @return {?}
     */
    FormBaseModel.prototype.getFormFields = 
    // TODO: consider evaluating and caching once the form is loaded
    /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var formFieldModel = [];
        for (var i = 0; i < this.fields.length; i++) {
            /** @type {?} */
            var field = this.fields[i];
            if (field instanceof ContainerModel) {
                /** @type {?} */
                var container = (/** @type {?} */ (field));
                formFieldModel.push(container.field);
                container.field.columns.forEach((/**
                 * @param {?} column
                 * @return {?}
                 */
                function (column) {
                    formFieldModel.push.apply(formFieldModel, tslib_1.__spread(column.fields));
                }));
            }
        }
        return formFieldModel;
    };
    /**
     * @return {?}
     */
    FormBaseModel.prototype.markAsInvalid = /**
     * @return {?}
     */
    function () {
        this.isValid = false;
    };
    FormBaseModel.UNSET_TASK_NAME = 'Nameless task';
    FormBaseModel.SAVE_OUTCOME = '$save';
    FormBaseModel.COMPLETE_OUTCOME = '$complete';
    FormBaseModel.START_PROCESS_OUTCOME = '$startProcess';
    return FormBaseModel;
}());
export { FormBaseModel };
if (false) {
    /** @type {?} */
    FormBaseModel.UNSET_TASK_NAME;
    /** @type {?} */
    FormBaseModel.SAVE_OUTCOME;
    /** @type {?} */
    FormBaseModel.COMPLETE_OUTCOME;
    /** @type {?} */
    FormBaseModel.START_PROCESS_OUTCOME;
    /** @type {?} */
    FormBaseModel.prototype.json;
    /** @type {?} */
    FormBaseModel.prototype.values;
    /** @type {?} */
    FormBaseModel.prototype.tabs;
    /** @type {?} */
    FormBaseModel.prototype.fields;
    /** @type {?} */
    FormBaseModel.prototype.outcomes;
    /** @type {?} */
    FormBaseModel.prototype.className;
    /** @type {?} */
    FormBaseModel.prototype.readOnly;
    /** @type {?} */
    FormBaseModel.prototype.taskName;
    /** @type {?} */
    FormBaseModel.prototype.isValid;
    /**
     * @abstract
     * @return {?}
     */
    FormBaseModel.prototype.validateForm = function () { };
    /**
     * @abstract
     * @param {?} field
     * @return {?}
     */
    FormBaseModel.prototype.validateField = function (field) { };
    /**
     * @abstract
     * @param {?} field
     * @return {?}
     */
    FormBaseModel.prototype.onFormFieldChanged = function (field) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1iYXNlLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL2Zvcm0tYmFzZS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFzQkEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLGdDQUFnQyxDQUFDOzs7O0FBRWhFO0lBQUE7UUFTSSxXQUFNLEdBQWUsRUFBRSxDQUFDO1FBQ3hCLFNBQUksR0FBZSxFQUFFLENBQUM7UUFDdEIsV0FBTSxHQUFzQixFQUFFLENBQUM7UUFDL0IsYUFBUSxHQUF1QixFQUFFLENBQUM7UUFHbEMsYUFBUSxHQUFZLEtBQUssQ0FBQztRQUcxQixZQUFPLEdBQVksSUFBSSxDQUFDO0lBNkM1QixDQUFDOzs7O0lBM0NHLCtCQUFPOzs7SUFBUDtRQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDN0MsQ0FBQzs7OztJQUVELGlDQUFTOzs7SUFBVDtRQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDakQsQ0FBQzs7OztJQUVELG1DQUFXOzs7SUFBWDtRQUNJLE9BQU8sSUFBSSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDckQsQ0FBQzs7Ozs7SUFFRCxvQ0FBWTs7OztJQUFaLFVBQWEsT0FBZTtRQUN4QixPQUFPLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFLLENBQUMsRUFBRSxLQUFLLE9BQU8sRUFBcEIsQ0FBb0IsRUFBQyxDQUFDO0lBQ3RFLENBQUM7SUFFRCxnRUFBZ0U7Ozs7O0lBQ2hFLHFDQUFhOzs7OztJQUFiOztZQUNVLGNBQWMsR0FBcUIsRUFBRTtRQUUzQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUU7O2dCQUNuQyxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7WUFFNUIsSUFBSSxLQUFLLFlBQVksY0FBYyxFQUFFOztvQkFDM0IsU0FBUyxHQUFHLG1CQUFpQixLQUFLLEVBQUE7Z0JBQ3hDLGNBQWMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUVyQyxTQUFTLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxPQUFPOzs7O2dCQUFDLFVBQUMsTUFBTTtvQkFDbkMsY0FBYyxDQUFDLElBQUksT0FBbkIsY0FBYyxtQkFBUyxNQUFNLENBQUMsTUFBTSxHQUFFO2dCQUMxQyxDQUFDLEVBQUMsQ0FBQzthQUNOO1NBQ0o7UUFFRCxPQUFPLGNBQWMsQ0FBQztJQUMxQixDQUFDOzs7O0lBRUQscUNBQWE7OztJQUFiO1FBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7SUFDekIsQ0FBQztJQXhETSw2QkFBZSxHQUFXLGVBQWUsQ0FBQztJQUMxQywwQkFBWSxHQUFXLE9BQU8sQ0FBQztJQUMvQiw4QkFBZ0IsR0FBVyxXQUFXLENBQUM7SUFDdkMsbUNBQXFCLEdBQVcsZUFBZSxDQUFDO0lBMEQzRCxvQkFBQztDQUFBLEFBL0RELElBK0RDO1NBL0RxQixhQUFhOzs7SUFFL0IsOEJBQWlEOztJQUNqRCwyQkFBc0M7O0lBQ3RDLCtCQUE4Qzs7SUFDOUMsb0NBQXVEOztJQUV2RCw2QkFBVTs7SUFFViwrQkFBd0I7O0lBQ3hCLDZCQUFzQjs7SUFDdEIsK0JBQStCOztJQUMvQixpQ0FBa0M7O0lBRWxDLGtDQUFrQjs7SUFDbEIsaUNBQTBCOztJQUMxQixpQ0FBUzs7SUFFVCxnQ0FBd0I7Ozs7O0lBMEN4Qix1REFBd0I7Ozs7OztJQUN4Qiw2REFBOEM7Ozs7OztJQUM5QyxrRUFBbUQiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgRm9ybVZhbHVlcyB9IGZyb20gJy4vd2lkZ2V0cy9jb3JlL2Zvcm0tdmFsdWVzJztcclxuaW1wb3J0IHsgVGFiTW9kZWwgfSBmcm9tICcuL3dpZGdldHMvY29yZS90YWIubW9kZWwnO1xyXG5pbXBvcnQgeyBGb3JtV2lkZ2V0TW9kZWwgfSBmcm9tICcuL3dpZGdldHMvY29yZS9mb3JtLXdpZGdldC5tb2RlbCc7XHJcbmltcG9ydCB7IEZvcm1PdXRjb21lTW9kZWwgfSBmcm9tICcuL3dpZGdldHMvY29yZS9mb3JtLW91dGNvbWUubW9kZWwnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRNb2RlbCB9IGZyb20gJy4vd2lkZ2V0cy9jb3JlL2Zvcm0tZmllbGQubW9kZWwnO1xyXG5pbXBvcnQgeyBDb250YWluZXJNb2RlbCB9IGZyb20gJy4vd2lkZ2V0cy9jb3JlL2NvbnRhaW5lci5tb2RlbCc7XHJcblxyXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgRm9ybUJhc2VNb2RlbCB7XHJcblxyXG4gICAgc3RhdGljIFVOU0VUX1RBU0tfTkFNRTogc3RyaW5nID0gJ05hbWVsZXNzIHRhc2snO1xyXG4gICAgc3RhdGljIFNBVkVfT1VUQ09NRTogc3RyaW5nID0gJyRzYXZlJztcclxuICAgIHN0YXRpYyBDT01QTEVURV9PVVRDT01FOiBzdHJpbmcgPSAnJGNvbXBsZXRlJztcclxuICAgIHN0YXRpYyBTVEFSVF9QUk9DRVNTX09VVENPTUU6IHN0cmluZyA9ICckc3RhcnRQcm9jZXNzJztcclxuXHJcbiAgICBqc29uOiBhbnk7XHJcblxyXG4gICAgdmFsdWVzOiBGb3JtVmFsdWVzID0ge307XHJcbiAgICB0YWJzOiBUYWJNb2RlbFtdID0gW107XHJcbiAgICBmaWVsZHM6IEZvcm1XaWRnZXRNb2RlbFtdID0gW107XHJcbiAgICBvdXRjb21lczogRm9ybU91dGNvbWVNb2RlbFtdID0gW107XHJcblxyXG4gICAgY2xhc3NOYW1lOiBzdHJpbmc7XHJcbiAgICByZWFkT25seTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgdGFza05hbWU7XHJcblxyXG4gICAgaXNWYWxpZDogYm9vbGVhbiA9IHRydWU7XHJcblxyXG4gICAgaGFzVGFicygpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy50YWJzICYmIHRoaXMudGFicy5sZW5ndGggPiAwO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc0ZpZWxkcygpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5maWVsZHMgJiYgdGhpcy5maWVsZHMubGVuZ3RoID4gMDtcclxuICAgIH1cclxuXHJcbiAgICBoYXNPdXRjb21lcygpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5vdXRjb21lcyAmJiB0aGlzLm91dGNvbWVzLmxlbmd0aCA+IDA7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RmllbGRCeUlkKGZpZWxkSWQ6IHN0cmluZyk6IEZvcm1GaWVsZE1vZGVsIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRGb3JtRmllbGRzKCkuZmluZCgoZmllbGQpID0+IGZpZWxkLmlkID09PSBmaWVsZElkKTtcclxuICAgIH1cclxuXHJcbiAgICAvLyBUT0RPOiBjb25zaWRlciBldmFsdWF0aW5nIGFuZCBjYWNoaW5nIG9uY2UgdGhlIGZvcm0gaXMgbG9hZGVkXHJcbiAgICBnZXRGb3JtRmllbGRzKCk6IEZvcm1GaWVsZE1vZGVsW10ge1xyXG4gICAgICAgIGNvbnN0IGZvcm1GaWVsZE1vZGVsOiBGb3JtRmllbGRNb2RlbFtdID0gW107XHJcblxyXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5maWVsZHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgY29uc3QgZmllbGQgPSB0aGlzLmZpZWxkc1tpXTtcclxuXHJcbiAgICAgICAgICAgIGlmIChmaWVsZCBpbnN0YW5jZW9mIENvbnRhaW5lck1vZGVsKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBjb250YWluZXIgPSA8Q29udGFpbmVyTW9kZWw+IGZpZWxkO1xyXG4gICAgICAgICAgICAgICAgZm9ybUZpZWxkTW9kZWwucHVzaChjb250YWluZXIuZmllbGQpO1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnRhaW5lci5maWVsZC5jb2x1bW5zLmZvckVhY2goKGNvbHVtbikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGZvcm1GaWVsZE1vZGVsLnB1c2goLi4uY29sdW1uLmZpZWxkcyk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZvcm1GaWVsZE1vZGVsO1xyXG4gICAgfVxyXG5cclxuICAgIG1hcmtBc0ludmFsaWQoKSB7XHJcbiAgICAgICAgdGhpcy5pc1ZhbGlkID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgYWJzdHJhY3QgdmFsaWRhdGVGb3JtKCk7XHJcbiAgICBhYnN0cmFjdCB2YWxpZGF0ZUZpZWxkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk7XHJcbiAgICBhYnN0cmFjdCBvbkZvcm1GaWVsZENoYW5nZWQoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTtcclxufVxyXG4iXX0=