/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { FormService } from './../services/form.service';
var FormListComponent = /** @class */ (function () {
    function FormListComponent(formService) {
        this.formService = formService;
        /**
         * The array that contains the information to show inside the list.
         */
        this.forms = [];
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    FormListComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        this.getForms();
    };
    /**
     * @return {?}
     */
    FormListComponent.prototype.isEmpty = /**
     * @return {?}
     */
    function () {
        return this.forms && this.forms.length === 0;
    };
    /**
     * @return {?}
     */
    FormListComponent.prototype.getForms = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.formService.getForms().subscribe((/**
         * @param {?} forms
         * @return {?}
         */
        function (forms) {
            var _a;
            (_a = _this.forms).push.apply(_a, tslib_1.__spread(forms));
        }));
    };
    FormListComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-form-list',
                    template: "<adf-datatable *ngIf=\"!isEmpty()\"\r\n    [rows]=\"forms\">\r\n    <data-columns>\r\n        <data-column key=\"name\" type=\"text\" title=\"Name\" class=\"adf-ellipsis-cell\" [sortable]=\"true\"></data-column>\r\n        <data-column key=\"lastUpdatedByFullName\" type=\"text\" title=\"User\" class=\"adf-ellipsis-cell\" [sortable]=\"true\"></data-column>\r\n        <data-column key=\"lastUpdated\" type=\"date\" format=\"shortDate\" title=\"Date\"></data-column>\r\n    </data-columns>\r\n</adf-datatable>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    FormListComponent.ctorParameters = function () { return [
        { type: FormService }
    ]; };
    FormListComponent.propDecorators = {
        forms: [{ type: Input }]
    };
    return FormListComponent;
}());
export { FormListComponent };
if (false) {
    /**
     * The array that contains the information to show inside the list.
     * @type {?}
     */
    FormListComponent.prototype.forms;
    /**
     * @type {?}
     * @protected
     */
    FormListComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1saXN0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy9mb3JtLWxpc3QuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBNEIsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDOUYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBRXpEO0lBWUksMkJBQXNCLFdBQXdCO1FBQXhCLGdCQUFXLEdBQVgsV0FBVyxDQUFhOzs7O1FBRjlDLFVBQUssR0FBVyxFQUFFLENBQUM7SUFHbkIsQ0FBQzs7Ozs7SUFFRCx1Q0FBVzs7OztJQUFYLFVBQVksT0FBc0I7UUFDOUIsSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ3BCLENBQUM7Ozs7SUFFRCxtQ0FBTzs7O0lBQVA7UUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO0lBQ2pELENBQUM7Ozs7SUFFRCxvQ0FBUTs7O0lBQVI7UUFBQSxpQkFJQztRQUhHLElBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLENBQUMsU0FBUzs7OztRQUFDLFVBQUMsS0FBSzs7WUFDeEMsQ0FBQSxLQUFBLEtBQUksQ0FBQyxLQUFLLENBQUEsQ0FBQyxJQUFJLDRCQUFJLEtBQUssR0FBRTtRQUM5QixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7O2dCQTNCSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGVBQWU7b0JBQ3pCLDZnQkFBeUM7b0JBRXpDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOztpQkFDeEM7Ozs7Z0JBUFEsV0FBVzs7O3dCQVdmLEtBQUs7O0lBb0JWLHdCQUFDO0NBQUEsQUE3QkQsSUE2QkM7U0F2QlksaUJBQWlCOzs7Ozs7SUFHMUIsa0NBQ21COzs7OztJQUVQLHdDQUFrQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkNoYW5nZXMsIFNpbXBsZUNoYW5nZXMsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi8uLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1mb3JtLWxpc3QnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Zvcm0tbGlzdC5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9mb3JtLWxpc3QuY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIEZvcm1MaXN0Q29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzIHtcclxuXHJcbiAgICAvKiogVGhlIGFycmF5IHRoYXQgY29udGFpbnMgdGhlIGluZm9ybWF0aW9uIHRvIHNob3cgaW5zaWRlIHRoZSBsaXN0LiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGZvcm1zOiBhbnkgW10gPSBbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgZm9ybVNlcnZpY2U6IEZvcm1TZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgICAgIHRoaXMuZ2V0Rm9ybXMoKTtcclxuICAgIH1cclxuXHJcbiAgICBpc0VtcHR5KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZvcm1zICYmIHRoaXMuZm9ybXMubGVuZ3RoID09PSAwO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEZvcm1zKCkge1xyXG4gICAgICAgIHRoaXMuZm9ybVNlcnZpY2UuZ2V0Rm9ybXMoKS5zdWJzY3JpYmUoKGZvcm1zKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZm9ybXMucHVzaCguLi5mb3Jtcyk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==