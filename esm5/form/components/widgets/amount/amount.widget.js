/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, ViewEncapsulation } from '@angular/core';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
var AmountWidgetComponent = /** @class */ (function (_super) {
    tslib_1.__extends(AmountWidgetComponent, _super);
    function AmountWidgetComponent(formService) {
        var _this = _super.call(this, formService) || this;
        _this.formService = formService;
        _this.currency = AmountWidgetComponent.DEFAULT_CURRENCY;
        return _this;
    }
    /**
     * @return {?}
     */
    AmountWidgetComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.field && this.field.currency) {
            this.currency = this.field.currency;
        }
    };
    AmountWidgetComponent.DEFAULT_CURRENCY = '$';
    AmountWidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'amount-widget',
                    template: "<div class=\"adf-amount-widget__container adf-amount-widget {{field.className}}\" [class.adf-invalid]=\"!field.isValid\" [class.adf-readonly]=\"field.readOnly\">\r\n    <mat-form-field class=\"adf-amount-widget__input\">\r\n        <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}<span *ngIf=\"isRequired()\">*</span></label>\r\n        <span matPrefix class=\"adf-amount-widget__prefix-spacing\"> {{currency }}</span>\r\n        <input matInput\r\n                class=\"adf-amount-widget\"\r\n                type=\"text\"\r\n                [id]=\"field.id\"\r\n                [required]=\"isRequired()\"\r\n                [placeholder]=\"field.placeholder\"\r\n                [value]=\"field.value\"\r\n                [(ngModel)]=\"field.value\"\r\n                (ngModelChange)=\"onFieldChanged(field)\"\r\n                [disabled]=\"field.readOnly\">\r\n    </mat-form-field>\r\n    <error-widget [error]=\"field.validationSummary\" ></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n</div>\r\n",
                    host: baseHost,
                    encapsulation: ViewEncapsulation.None,
                    styles: [".adf-amount-widget{width:100%;vertical-align:baseline!important}.adf-amount-widget .mat-input-placeholder{margin-top:5px;display:none}.adf-amount-widget .mat-form-field-flex{position:relative;padding-top:18.5px}.adf-amount-widget .mat-form-field-infix{position:static;padding-top:19px}.adf-amount-widget .adf-label{position:absolute;top:18.5px;left:0}.adf-amount-widget__container{max-width:100%}.adf-amount-widget__container .mat-form-field-label-wrapper{top:34px!important;left:13px}.adf-amount-widget__input .mat-focused{transition:none}.adf-amount-widget__prefix-spacing{padding-right:5px}"]
                }] }
    ];
    /** @nocollapse */
    AmountWidgetComponent.ctorParameters = function () { return [
        { type: FormService }
    ]; };
    return AmountWidgetComponent;
}(WidgetComponent));
export { AmountWidgetComponent };
if (false) {
    /** @type {?} */
    AmountWidgetComponent.DEFAULT_CURRENCY;
    /** @type {?} */
    AmountWidgetComponent.prototype.currency;
    /** @type {?} */
    AmountWidgetComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW1vdW50LndpZGdldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2Ftb3VudC9hbW91bnQud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFNBQVMsRUFBVSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNyRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDL0QsT0FBTyxFQUFFLFFBQVEsRUFBRyxlQUFlLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUVuRTtJQU8yQyxpREFBZTtJQU10RCwrQkFBbUIsV0FBd0I7UUFBM0MsWUFDSSxrQkFBTSxXQUFXLENBQUMsU0FDckI7UUFGa0IsaUJBQVcsR0FBWCxXQUFXLENBQWE7UUFGM0MsY0FBUSxHQUFXLHFCQUFxQixDQUFDLGdCQUFnQixDQUFDOztJQUkxRCxDQUFDOzs7O0lBRUQsd0NBQVE7OztJQUFSO1FBQ0ksSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFO1lBQ25DLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUM7U0FDdkM7SUFDTCxDQUFDO0lBWk0sc0NBQWdCLEdBQVcsR0FBRyxDQUFDOztnQkFUekMsU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxlQUFlO29CQUN6Qix5bkNBQW1DO29CQUVuQyxJQUFJLEVBQUUsUUFBUTtvQkFDZCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7aUJBQ3hDOzs7O2dCQVRRLFdBQVc7O0lBMEJwQiw0QkFBQztDQUFBLEFBdkJELENBTzJDLGVBQWUsR0FnQnpEO1NBaEJZLHFCQUFxQjs7O0lBRTlCLHVDQUFzQzs7SUFFdEMseUNBQTBEOztJQUU5Qyw0Q0FBK0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuIC8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4vLi4vLi4vLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgYmFzZUhvc3QgLCBXaWRnZXRDb21wb25lbnQgfSBmcm9tICcuLy4uL3dpZGdldC5jb21wb25lbnQnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2Ftb3VudC13aWRnZXQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Ftb3VudC53aWRnZXQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9hbW91bnQud2lkZ2V0LnNjc3MnXSxcclxuICAgIGhvc3Q6IGJhc2VIb3N0LFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQW1vdW50V2lkZ2V0Q29tcG9uZW50IGV4dGVuZHMgV2lkZ2V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBzdGF0aWMgREVGQVVMVF9DVVJSRU5DWTogc3RyaW5nID0gJyQnO1xyXG5cclxuICAgIGN1cnJlbmN5OiBzdHJpbmcgPSBBbW91bnRXaWRnZXRDb21wb25lbnQuREVGQVVMVF9DVVJSRU5DWTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZm9ybVNlcnZpY2U6IEZvcm1TZXJ2aWNlKSB7XHJcbiAgICAgICAgc3VwZXIoZm9ybVNlcnZpY2UpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmZpZWxkICYmIHRoaXMuZmllbGQuY3VycmVuY3kpIHtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW5jeSA9IHRoaXMuZmllbGQuY3VycmVuY3k7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=