/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, ViewEncapsulation } from '@angular/core';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
import { ContainerWidgetComponentModel } from './container.widget.model';
var ContainerWidgetComponent = /** @class */ (function (_super) {
    tslib_1.__extends(ContainerWidgetComponent, _super);
    function ContainerWidgetComponent(formService) {
        var _this = _super.call(this, formService) || this;
        _this.formService = formService;
        return _this;
    }
    /**
     * @return {?}
     */
    ContainerWidgetComponent.prototype.onExpanderClicked = /**
     * @return {?}
     */
    function () {
        if (this.content && this.content.isCollapsible()) {
            this.content.isExpanded = !this.content.isExpanded;
        }
    };
    /**
     * @return {?}
     */
    ContainerWidgetComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.field) {
            this.content = new ContainerWidgetComponentModel(this.field);
        }
    };
    Object.defineProperty(ContainerWidgetComponent.prototype, "fields", {
        /**
         * Serializes column fields
         */
        get: /**
         * Serializes column fields
         * @return {?}
         */
        function () {
            /** @type {?} */
            var fields = [];
            /** @type {?} */
            var rowContainsElement = true;
            /** @type {?} */
            var rowIndex = 0;
            while (rowContainsElement) {
                rowContainsElement = false;
                for (var i = 0; i < this.content.columns.length; i++) {
                    /** @type {?} */
                    var field = this.content.columns[i].fields[rowIndex];
                    if (field) {
                        rowContainsElement = true;
                    }
                    fields.push(field);
                }
                rowIndex++;
            }
            return fields;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Calculate the column width based on the numberOfColumns and current field's colspan property
     *
     * @param field
     */
    /**
     * Calculate the column width based on the numberOfColumns and current field's colspan property
     *
     * @param {?} field
     * @return {?}
     */
    ContainerWidgetComponent.prototype.getColumnWith = /**
     * Calculate the column width based on the numberOfColumns and current field's colspan property
     *
     * @param {?} field
     * @return {?}
     */
    function (field) {
        /** @type {?} */
        var colspan = field ? field.colspan : 1;
        return (100 / this.content.json.numberOfColumns) * colspan + '%';
    };
    ContainerWidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'container-widget',
                    template: "<div [hidden]=\"!content?.isGroup()\" class=\"adf-container-widget__header\">\r\n    <h4 class=\"adf-container-widget__header-text\" id=\"container-header\"\r\n        [class.adf-collapsible]=\"content?.isCollapsible()\">\r\n        <button *ngIf=\"content?.isCollapsible()\"\r\n                mat-icon-button\r\n                class=\"mdl-button--icon\"\r\n                (click)=\"onExpanderClicked()\">\r\n            <mat-icon>{{ content?.isExpanded ? 'expand_more' : 'expand_less' }}</mat-icon>\r\n        </button>\r\n        <span (click)=\"onExpanderClicked()\" id=\"container-header-label\">{{content.name | translate }}</span>\r\n    </h4>\r\n</div>\r\n\r\n<section class=\"adf-grid-list\" *ngIf=\"content?.isExpanded\">\r\n    <div class=\"adf-grid-list-item\" *ngFor=\"let field of fields\" [style.width]=\"getColumnWith(field)\">\r\n        <adf-form-field *ngIf=\"field\" [field]=\"field\"></adf-form-field>\r\n    </div>\r\n</section>\r\n\r\n\r\n",
                    host: baseHost,
                    encapsulation: ViewEncapsulation.None,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    ContainerWidgetComponent.ctorParameters = function () { return [
        { type: FormService }
    ]; };
    return ContainerWidgetComponent;
}(WidgetComponent));
export { ContainerWidgetComponent };
if (false) {
    /** @type {?} */
    ContainerWidgetComponent.prototype.content;
    /** @type {?} */
    ContainerWidgetComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLndpZGdldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2NvbnRhaW5lci9jb250YWluZXIud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFpQixTQUFTLEVBQVUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDcEYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBRS9ELE9BQU8sRUFBRSxRQUFRLEVBQUcsZUFBZSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDbkUsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFekU7SUFPOEMsb0RBQWU7SUFJekQsa0NBQW1CLFdBQXdCO1FBQTNDLFlBQ0ssa0JBQU0sV0FBVyxDQUFDLFNBQ3RCO1FBRmtCLGlCQUFXLEdBQVgsV0FBVyxDQUFhOztJQUUzQyxDQUFDOzs7O0lBRUQsb0RBQWlCOzs7SUFBakI7UUFDSSxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLEVBQUUsRUFBRTtZQUM5QyxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsR0FBRyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDO1NBQ3REO0lBQ0wsQ0FBQzs7OztJQUVELDJDQUFROzs7SUFBUjtRQUNJLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNaLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSw2QkFBNkIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDaEU7SUFDTCxDQUFDO0lBS0Qsc0JBQUksNENBQU07UUFIVjs7V0FFRzs7Ozs7UUFDSDs7Z0JBQ1UsTUFBTSxHQUFHLEVBQUU7O2dCQUViLGtCQUFrQixHQUFHLElBQUk7O2dCQUN6QixRQUFRLEdBQUcsQ0FBQztZQUVoQixPQUFPLGtCQUFrQixFQUFFO2dCQUN2QixrQkFBa0IsR0FBRyxLQUFLLENBQUM7Z0JBQzNCLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFLEVBQUc7O3dCQUM3QyxLQUFLLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztvQkFDdEQsSUFBSSxLQUFLLEVBQUU7d0JBQ1Asa0JBQWtCLEdBQUcsSUFBSSxDQUFDO3FCQUM3QjtvQkFFRCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUN0QjtnQkFDRCxRQUFRLEVBQUUsQ0FBQzthQUNkO1lBRUQsT0FBTyxNQUFNLENBQUM7UUFDbEIsQ0FBQzs7O09BQUE7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsZ0RBQWE7Ozs7OztJQUFiLFVBQWMsS0FBcUI7O1lBQ3pCLE9BQU8sR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDekMsT0FBTyxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsR0FBRyxPQUFPLEdBQUcsR0FBRyxDQUFDO0lBQ3JFLENBQUM7O2dCQTVESixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGtCQUFrQjtvQkFDNUIsZzlCQUFzQztvQkFFdEMsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2lCQUN4Qzs7OztnQkFYUSxXQUFXOztJQWtFcEIsK0JBQUM7Q0FBQSxBQTdERCxDQU84QyxlQUFlLEdBc0Q1RDtTQXREWSx3QkFBd0I7OztJQUVqQywyQ0FBdUM7O0lBRTNCLCtDQUErQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4gLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgQWZ0ZXJWaWV3SW5pdCwgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi8uLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRNb2RlbCB9IGZyb20gJy4vLi4vY29yZS9mb3JtLWZpZWxkLm1vZGVsJztcclxuaW1wb3J0IHsgYmFzZUhvc3QgLCBXaWRnZXRDb21wb25lbnQgfSBmcm9tICcuLy4uL3dpZGdldC5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBDb250YWluZXJXaWRnZXRDb21wb25lbnRNb2RlbCB9IGZyb20gJy4vY29udGFpbmVyLndpZGdldC5tb2RlbCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnY29udGFpbmVyLXdpZGdldCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vY29udGFpbmVyLndpZGdldC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2NvbnRhaW5lci53aWRnZXQuc2NzcyddLFxyXG4gICAgaG9zdDogYmFzZUhvc3QsXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb250YWluZXJXaWRnZXRDb21wb25lbnQgZXh0ZW5kcyBXaWRnZXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQsIEFmdGVyVmlld0luaXQge1xyXG5cclxuICAgIGNvbnRlbnQ6IENvbnRhaW5lcldpZGdldENvbXBvbmVudE1vZGVsO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBmb3JtU2VydmljZTogRm9ybVNlcnZpY2UpIHtcclxuICAgICAgICAgc3VwZXIoZm9ybVNlcnZpY2UpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRXhwYW5kZXJDbGlja2VkKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvbnRlbnQgJiYgdGhpcy5jb250ZW50LmlzQ29sbGFwc2libGUoKSkge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRlbnQuaXNFeHBhbmRlZCA9ICF0aGlzLmNvbnRlbnQuaXNFeHBhbmRlZDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZmllbGQpIHtcclxuICAgICAgICAgICAgdGhpcy5jb250ZW50ID0gbmV3IENvbnRhaW5lcldpZGdldENvbXBvbmVudE1vZGVsKHRoaXMuZmllbGQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNlcmlhbGl6ZXMgY29sdW1uIGZpZWxkc1xyXG4gICAgICovXHJcbiAgICBnZXQgZmllbGRzKCk6IEZvcm1GaWVsZE1vZGVsW10ge1xyXG4gICAgICAgIGNvbnN0IGZpZWxkcyA9IFtdO1xyXG5cclxuICAgICAgICBsZXQgcm93Q29udGFpbnNFbGVtZW50ID0gdHJ1ZSxcclxuICAgICAgICAgICAgcm93SW5kZXggPSAwO1xyXG5cclxuICAgICAgICB3aGlsZSAocm93Q29udGFpbnNFbGVtZW50KSB7XHJcbiAgICAgICAgICAgIHJvd0NvbnRhaW5zRWxlbWVudCA9IGZhbHNlO1xyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMuY29udGVudC5jb2x1bW5zLmxlbmd0aDsgaSsrICkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZmllbGQgPSB0aGlzLmNvbnRlbnQuY29sdW1uc1tpXS5maWVsZHNbcm93SW5kZXhdO1xyXG4gICAgICAgICAgICAgICAgaWYgKGZpZWxkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcm93Q29udGFpbnNFbGVtZW50ID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBmaWVsZHMucHVzaChmaWVsZCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcm93SW5kZXgrKztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBmaWVsZHM7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDYWxjdWxhdGUgdGhlIGNvbHVtbiB3aWR0aCBiYXNlZCBvbiB0aGUgbnVtYmVyT2ZDb2x1bW5zIGFuZCBjdXJyZW50IGZpZWxkJ3MgY29sc3BhbiBwcm9wZXJ0eVxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBmaWVsZFxyXG4gICAgICovXHJcbiAgICBnZXRDb2x1bW5XaXRoKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IHN0cmluZyB7XHJcbiAgICAgICAgY29uc3QgY29sc3BhbiA9IGZpZWxkID8gZmllbGQuY29sc3BhbiA6IDE7XHJcbiAgICAgICAgcmV0dXJuICgxMDAgLyB0aGlzLmNvbnRlbnQuanNvbi5udW1iZXJPZkNvbHVtbnMpICogY29sc3BhbiArICclJztcclxuICAgIH1cclxufVxyXG4iXX0=