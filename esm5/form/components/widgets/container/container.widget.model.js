/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { ContainerModel } from './../core/container.model';
import { FormFieldTypes } from './../core/form-field-types';
var ContainerWidgetComponentModel = /** @class */ (function (_super) {
    tslib_1.__extends(ContainerWidgetComponentModel, _super);
    function ContainerWidgetComponentModel(field) {
        var _this = _super.call(this, field) || this;
        _this.columns = [];
        _this.isExpanded = true;
        _this.rowspan = 1;
        _this.colspan = 1;
        if (_this.field) {
            _this.columns = _this.field.columns || [];
            _this.isExpanded = !_this.isCollapsedByDefault();
            _this.colspan = field.colspan;
            _this.rowspan = field.rowspan;
        }
        return _this;
    }
    /**
     * @return {?}
     */
    ContainerWidgetComponentModel.prototype.isGroup = /**
     * @return {?}
     */
    function () {
        return this.type === FormFieldTypes.GROUP;
    };
    /**
     * @return {?}
     */
    ContainerWidgetComponentModel.prototype.isCollapsible = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var allowCollapse = false;
        if (this.isGroup() && this.field.params['allowCollapse']) {
            allowCollapse = (/** @type {?} */ (this.field.params['allowCollapse']));
        }
        return allowCollapse;
    };
    /**
     * @return {?}
     */
    ContainerWidgetComponentModel.prototype.isCollapsedByDefault = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var collapseByDefault = false;
        if (this.isCollapsible() && this.field.params['collapseByDefault']) {
            collapseByDefault = (/** @type {?} */ (this.field.params['collapseByDefault']));
        }
        return collapseByDefault;
    };
    return ContainerWidgetComponentModel;
}(ContainerModel));
export { ContainerWidgetComponentModel };
if (false) {
    /** @type {?} */
    ContainerWidgetComponentModel.prototype.columns;
    /** @type {?} */
    ContainerWidgetComponentModel.prototype.isExpanded;
    /** @type {?} */
    ContainerWidgetComponentModel.prototype.rowspan;
    /** @type {?} */
    ContainerWidgetComponentModel.prototype.colspan;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLndpZGdldC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2NvbnRhaW5lci9jb250YWluZXIud2lkZ2V0Lm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQkEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQzNELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUc1RDtJQUFtRCx5REFBYztJQStCN0QsdUNBQVksS0FBcUI7UUFBakMsWUFDSSxrQkFBTSxLQUFLLENBQUMsU0FRZjtRQXRDRCxhQUFPLEdBQTJCLEVBQUUsQ0FBQztRQUNyQyxnQkFBVSxHQUFZLElBQUksQ0FBQztRQUMzQixhQUFPLEdBQVcsQ0FBQyxDQUFDO1FBQ3BCLGFBQU8sR0FBVyxDQUFDLENBQUM7UUE2QmhCLElBQUksS0FBSSxDQUFDLEtBQUssRUFBRTtZQUNaLEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLElBQUksRUFBRSxDQUFDO1lBQ3hDLEtBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxLQUFJLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztZQUMvQyxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUM7WUFDN0IsS0FBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO1NBQ2hDOztJQUNMLENBQUM7Ozs7SUFqQ0QsK0NBQU87OztJQUFQO1FBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxLQUFLLENBQUM7SUFDOUMsQ0FBQzs7OztJQUVELHFEQUFhOzs7SUFBYjs7WUFDUSxhQUFhLEdBQUcsS0FBSztRQUV6QixJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxlQUFlLENBQUMsRUFBRTtZQUN0RCxhQUFhLEdBQUcsbUJBQVUsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsZUFBZSxDQUFDLEVBQUEsQ0FBQztTQUNoRTtRQUVELE9BQU8sYUFBYSxDQUFDO0lBQ3pCLENBQUM7Ozs7SUFFRCw0REFBb0I7OztJQUFwQjs7WUFDUSxpQkFBaUIsR0FBRyxLQUFLO1FBRTdCLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLG1CQUFtQixDQUFDLEVBQUU7WUFDaEUsaUJBQWlCLEdBQUcsbUJBQVUsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsbUJBQW1CLENBQUMsRUFBQSxDQUFDO1NBQ3hFO1FBRUQsT0FBTyxpQkFBaUIsQ0FBQztJQUM3QixDQUFDO0lBWUwsb0NBQUM7QUFBRCxDQUFDLEFBekNELENBQW1ELGNBQWMsR0F5Q2hFOzs7O0lBdkNHLGdEQUFxQzs7SUFDckMsbURBQTJCOztJQUMzQixnREFBb0I7O0lBQ3BCLGdEQUFvQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4vKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgeyBDb250YWluZXJDb2x1bW5Nb2RlbCB9IGZyb20gJy4vLi4vY29yZS9jb250YWluZXItY29sdW1uLm1vZGVsJztcclxuaW1wb3J0IHsgQ29udGFpbmVyTW9kZWwgfSBmcm9tICcuLy4uL2NvcmUvY29udGFpbmVyLm1vZGVsJztcclxuaW1wb3J0IHsgRm9ybUZpZWxkVHlwZXMgfSBmcm9tICcuLy4uL2NvcmUvZm9ybS1maWVsZC10eXBlcyc7XHJcbmltcG9ydCB7IEZvcm1GaWVsZE1vZGVsIH0gZnJvbSAnLi8uLi9jb3JlL2Zvcm0tZmllbGQubW9kZWwnO1xyXG5cclxuZXhwb3J0IGNsYXNzIENvbnRhaW5lcldpZGdldENvbXBvbmVudE1vZGVsIGV4dGVuZHMgQ29udGFpbmVyTW9kZWwge1xyXG5cclxuICAgIGNvbHVtbnM6IENvbnRhaW5lckNvbHVtbk1vZGVsW10gPSBbXTtcclxuICAgIGlzRXhwYW5kZWQ6IGJvb2xlYW4gPSB0cnVlO1xyXG4gICAgcm93c3BhbjogbnVtYmVyID0gMTtcclxuICAgIGNvbHNwYW46IG51bWJlciA9IDE7XHJcblxyXG4gICAgaXNHcm91cCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy50eXBlID09PSBGb3JtRmllbGRUeXBlcy5HUk9VUDtcclxuICAgIH1cclxuXHJcbiAgICBpc0NvbGxhcHNpYmxlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGxldCBhbGxvd0NvbGxhcHNlID0gZmFsc2U7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmlzR3JvdXAoKSAmJiB0aGlzLmZpZWxkLnBhcmFtc1snYWxsb3dDb2xsYXBzZSddKSB7XHJcbiAgICAgICAgICAgIGFsbG93Q29sbGFwc2UgPSA8Ym9vbGVhbj4gdGhpcy5maWVsZC5wYXJhbXNbJ2FsbG93Q29sbGFwc2UnXTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBhbGxvd0NvbGxhcHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzQ29sbGFwc2VkQnlEZWZhdWx0KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGxldCBjb2xsYXBzZUJ5RGVmYXVsdCA9IGZhbHNlO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5pc0NvbGxhcHNpYmxlKCkgJiYgdGhpcy5maWVsZC5wYXJhbXNbJ2NvbGxhcHNlQnlEZWZhdWx0J10pIHtcclxuICAgICAgICAgICAgY29sbGFwc2VCeURlZmF1bHQgPSA8Ym9vbGVhbj4gdGhpcy5maWVsZC5wYXJhbXNbJ2NvbGxhcHNlQnlEZWZhdWx0J107XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gY29sbGFwc2VCeURlZmF1bHQ7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKSB7XHJcbiAgICAgICAgc3VwZXIoZmllbGQpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5maWVsZCkge1xyXG4gICAgICAgICAgICB0aGlzLmNvbHVtbnMgPSB0aGlzLmZpZWxkLmNvbHVtbnMgfHwgW107XHJcbiAgICAgICAgICAgIHRoaXMuaXNFeHBhbmRlZCA9ICF0aGlzLmlzQ29sbGFwc2VkQnlEZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIHRoaXMuY29sc3BhbiA9IGZpZWxkLmNvbHNwYW47XHJcbiAgICAgICAgICAgIHRoaXMucm93c3BhbiA9IGZpZWxkLnJvd3NwYW47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==