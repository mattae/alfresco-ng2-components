/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ContentService } from '../../../../services/content.service';
import { LogService } from '../../../../services/log.service';
import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { ProcessContentService } from '../../../services/process-content.service';
import { ContentLinkModel } from '../core/content-link.model';
import { FormService } from './../../../services/form.service';
var ContentWidgetComponent = /** @class */ (function () {
    function ContentWidgetComponent(formService, logService, contentService, processContentService) {
        this.formService = formService;
        this.logService = logService;
        this.contentService = contentService;
        this.processContentService = processContentService;
        /**
         * Toggles showing document content.
         */
        this.showDocumentContent = true;
        /**
         * Emitted when the content is clicked.
         */
        this.contentClick = new EventEmitter();
        /**
         * Emitted when the thumbnail has loaded.
         */
        this.thumbnailLoaded = new EventEmitter();
        /**
         * Emitted when the content has loaded.
         */
        this.contentLoaded = new EventEmitter();
        /**
         * Emitted when an error occurs.
         */
        this.error = new EventEmitter();
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ContentWidgetComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        /** @type {?} */
        var contentId = changes['id'];
        if (contentId && contentId.currentValue) {
            this.loadContent(contentId.currentValue);
        }
    };
    /**
     * @param {?} id
     * @return {?}
     */
    ContentWidgetComponent.prototype.loadContent = /**
     * @param {?} id
     * @return {?}
     */
    function (id) {
        var _this = this;
        this.processContentService
            .getFileContent(id)
            .subscribe((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            _this.content = new ContentLinkModel(response);
            _this.contentLoaded.emit(_this.content);
            _this.loadThumbnailUrl(_this.content);
        }), (/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            _this.error.emit(error);
        }));
    };
    /**
     * @param {?} content
     * @return {?}
     */
    ContentWidgetComponent.prototype.loadThumbnailUrl = /**
     * @param {?} content
     * @return {?}
     */
    function (content) {
        var _this = this;
        if (this.content.isThumbnailSupported()) {
            /** @type {?} */
            var observable = void 0;
            if (this.content.isTypeImage()) {
                observable = this.processContentService.getFileRawContent(content.id);
            }
            else {
                observable = this.processContentService.getContentThumbnail(content.id);
            }
            if (observable) {
                observable.subscribe((/**
                 * @param {?} response
                 * @return {?}
                 */
                function (response) {
                    _this.content.thumbnailUrl = _this.contentService.createTrustedUrl(response);
                    _this.thumbnailLoaded.emit(_this.content.thumbnailUrl);
                }), (/**
                 * @param {?} error
                 * @return {?}
                 */
                function (error) {
                    _this.error.emit(error);
                }));
            }
        }
    };
    /**
     * @param {?} content
     * @return {?}
     */
    ContentWidgetComponent.prototype.openViewer = /**
     * @param {?} content
     * @return {?}
     */
    function (content) {
        var _this = this;
        /** @type {?} */
        var fetch = this.processContentService.getContentPreview(content.id);
        if (content.isTypeImage() || content.isTypePdf()) {
            fetch = this.processContentService.getFileRawContent(content.id);
        }
        fetch.subscribe((/**
         * @param {?} blob
         * @return {?}
         */
        function (blob) {
            content.contentBlob = blob;
            _this.contentClick.emit(content);
            _this.logService.info('Content clicked' + content.id);
            _this.formService.formContentClicked.next(content);
        }), (/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            _this.error.emit(error);
        }));
    };
    /**
     * Invoke content download.
     */
    /**
     * Invoke content download.
     * @param {?} content
     * @return {?}
     */
    ContentWidgetComponent.prototype.download = /**
     * Invoke content download.
     * @param {?} content
     * @return {?}
     */
    function (content) {
        var _this = this;
        this.processContentService.getFileRawContent(content.id).subscribe((/**
         * @param {?} blob
         * @return {?}
         */
        function (blob) { return _this.contentService.downloadBlob(blob, content.name); }), (/**
         * @param {?} error
         * @return {?}
         */
        function (error) {
            _this.error.emit(error);
        }));
    };
    ContentWidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-content',
                    template: "<mat-card class=\"adf-content-container\" *ngIf=\"content\">\r\n    <mat-card-content *ngIf=\"showDocumentContent\">\r\n        <div *ngIf=\"content.isThumbnailSupported()\" >\r\n            <img id=\"thumbnailPreview\" class=\"adf-img-upload-widget\" [src]=\"content.thumbnailUrl\" alt=\"{{content.name}}\">\r\n        </div>\r\n        <div *ngIf=\"!content.isThumbnailSupported()\">\r\n            <mat-icon>image</mat-icon>\r\n            <div id=\"unsupported-thumbnail\" class=\"adf-content-widget-preview-text\">{{ 'FORM.PREVIEW.IMAGE_NOT_AVAILABLE' | translate }}\r\n            </div>\r\n        </div>\r\n        <div class=\"mdl-card__supporting-text upload-widget__content-text\">{{content.name | translate }}</div>\r\n    </mat-card-content>\r\n\r\n    <mat-card-actions>\r\n        <button mat-icon-button id=\"view\" (click)=\"openViewer(content)\">\r\n            <mat-icon class=\"mat-24\">zoom_in</mat-icon>\r\n        </button>\r\n        <button mat-icon-button id=\"download\" (click)=\"download(content)\">\r\n            <mat-icon class=\"mat-24\">file_download</mat-icon>\r\n        </button>\r\n    </mat-card-actions>\r\n</mat-card>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    styles: [".adf-img-upload-widget{width:100%;height:100%;border:1px solid rgba(117,117,117,.57);box-shadow:1px 1px 2px #ddd;background-color:#fff}.adf-content-widget-preview-text{word-wrap:break-word;word-break:break-all;text-align:center}"]
                }] }
    ];
    /** @nocollapse */
    ContentWidgetComponent.ctorParameters = function () { return [
        { type: FormService },
        { type: LogService },
        { type: ContentService },
        { type: ProcessContentService }
    ]; };
    ContentWidgetComponent.propDecorators = {
        id: [{ type: Input }],
        showDocumentContent: [{ type: Input }],
        contentClick: [{ type: Output }],
        thumbnailLoaded: [{ type: Output }],
        contentLoaded: [{ type: Output }],
        error: [{ type: Output }]
    };
    return ContentWidgetComponent;
}());
export { ContentWidgetComponent };
if (false) {
    /**
     * The content id to show.
     * @type {?}
     */
    ContentWidgetComponent.prototype.id;
    /**
     * Toggles showing document content.
     * @type {?}
     */
    ContentWidgetComponent.prototype.showDocumentContent;
    /**
     * Emitted when the content is clicked.
     * @type {?}
     */
    ContentWidgetComponent.prototype.contentClick;
    /**
     * Emitted when the thumbnail has loaded.
     * @type {?}
     */
    ContentWidgetComponent.prototype.thumbnailLoaded;
    /**
     * Emitted when the content has loaded.
     * @type {?}
     */
    ContentWidgetComponent.prototype.contentLoaded;
    /**
     * Emitted when an error occurs.
     * @type {?}
     */
    ContentWidgetComponent.prototype.error;
    /** @type {?} */
    ContentWidgetComponent.prototype.content;
    /**
     * @type {?}
     * @protected
     */
    ContentWidgetComponent.prototype.formService;
    /**
     * @type {?}
     * @private
     */
    ContentWidgetComponent.prototype.logService;
    /**
     * @type {?}
     * @private
     */
    ContentWidgetComponent.prototype.contentService;
    /**
     * @type {?}
     * @private
     */
    ContentWidgetComponent.prototype.processContentService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC53aWRnZXQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9jb250ZW50L2NvbnRlbnQud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxzQ0FBc0MsQ0FBQztBQUN0RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDOUQsT0FBTyxFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFhLE1BQU0sRUFBaUIsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFcEgsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFDbEYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDOUQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBRS9EO0lBa0NJLGdDQUFzQixXQUF3QixFQUMxQixVQUFzQixFQUN0QixjQUE4QixFQUM5QixxQkFBNEM7UUFIMUMsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDMUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF1Qjs7OztRQXZCaEUsd0JBQW1CLEdBQVksSUFBSSxDQUFDOzs7O1FBSXBDLGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQzs7OztRQUlsQyxvQkFBZSxHQUFzQixJQUFJLFlBQVksRUFBTyxDQUFDOzs7O1FBSTdELGtCQUFhLEdBQXNCLElBQUksWUFBWSxFQUFPLENBQUM7Ozs7UUFJM0QsVUFBSyxHQUFzQixJQUFJLFlBQVksRUFBTyxDQUFDO0lBUW5ELENBQUM7Ozs7O0lBRUQsNENBQVc7Ozs7SUFBWCxVQUFZLE9BQXNCOztZQUN4QixTQUFTLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQztRQUMvQixJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsWUFBWSxFQUFFO1lBQ3JDLElBQUksQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQzVDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCw0Q0FBVzs7OztJQUFYLFVBQVksRUFBVTtRQUF0QixpQkFhQztRQVpHLElBQUksQ0FBQyxxQkFBcUI7YUFDckIsY0FBYyxDQUFDLEVBQUUsQ0FBQzthQUNsQixTQUFTOzs7O1FBQ04sVUFBQyxRQUEwQjtZQUN2QixLQUFJLENBQUMsT0FBTyxHQUFHLElBQUksZ0JBQWdCLENBQUMsUUFBUSxDQUFDLENBQUM7WUFDOUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ3RDLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUM7UUFDeEMsQ0FBQzs7OztRQUNELFVBQUMsS0FBSztZQUNGLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzNCLENBQUMsRUFDSixDQUFDO0lBQ1YsQ0FBQzs7Ozs7SUFFRCxpREFBZ0I7Ozs7SUFBaEIsVUFBaUIsT0FBeUI7UUFBMUMsaUJBdUJDO1FBdEJHLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsRUFBRSxFQUFFOztnQkFDakMsVUFBVSxTQUFpQjtZQUUvQixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxFQUFFLEVBQUU7Z0JBQzVCLFVBQVUsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ3pFO2lCQUFNO2dCQUNILFVBQVUsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsbUJBQW1CLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQzNFO1lBRUQsSUFBSSxVQUFVLEVBQUU7Z0JBQ1osVUFBVSxDQUFDLFNBQVM7Ozs7Z0JBQ2hCLFVBQUMsUUFBYztvQkFDWCxLQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDO29CQUMzRSxLQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQyxDQUFDO2dCQUN6RCxDQUFDOzs7O2dCQUNELFVBQUMsS0FBSztvQkFDRixLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFFM0IsQ0FBQyxFQUNKLENBQUM7YUFDTDtTQUNKO0lBQ0wsQ0FBQzs7Ozs7SUFFRCwyQ0FBVTs7OztJQUFWLFVBQVcsT0FBeUI7UUFBcEMsaUJBZ0JDOztZQWZPLEtBQUssR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQztRQUNwRSxJQUFJLE9BQU8sQ0FBQyxXQUFXLEVBQUUsSUFBSSxPQUFPLENBQUMsU0FBUyxFQUFFLEVBQUU7WUFDOUMsS0FBSyxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUM7U0FDcEU7UUFDRCxLQUFLLENBQUMsU0FBUzs7OztRQUNYLFVBQUMsSUFBVTtZQUNQLE9BQU8sQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO1lBQzNCLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQ2hDLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGlCQUFpQixHQUFHLE9BQU8sQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUNyRCxLQUFJLENBQUMsV0FBVyxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztRQUN0RCxDQUFDOzs7O1FBQ0QsVUFBQyxLQUFLO1lBQ0YsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDM0IsQ0FBQyxFQUNKLENBQUM7SUFDTixDQUFDO0lBRUQ7O09BRUc7Ozs7OztJQUNILHlDQUFROzs7OztJQUFSLFVBQVMsT0FBeUI7UUFBbEMsaUJBT0M7UUFORyxJQUFJLENBQUMscUJBQXFCLENBQUMsaUJBQWlCLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLFNBQVM7Ozs7UUFDOUQsVUFBQyxJQUFVLElBQUssT0FBQSxLQUFJLENBQUMsY0FBYyxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFwRCxDQUFvRDs7OztRQUNwRSxVQUFDLEtBQUs7WUFDRixLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUMzQixDQUFDLEVBQ0osQ0FBQztJQUNOLENBQUM7O2dCQW5ISixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGFBQWE7b0JBQ3ZCLHNwQ0FBb0M7b0JBRXBDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOztpQkFDeEM7Ozs7Z0JBUFEsV0FBVztnQkFMWCxVQUFVO2dCQURWLGNBQWM7Z0JBSWQscUJBQXFCOzs7cUJBYXpCLEtBQUs7c0NBSUwsS0FBSzsrQkFJTCxNQUFNO2tDQUlOLE1BQU07Z0NBSU4sTUFBTTt3QkFJTixNQUFNOztJQXVGWCw2QkFBQztDQUFBLEFBcEhELElBb0hDO1NBOUdZLHNCQUFzQjs7Ozs7O0lBRy9CLG9DQUNXOzs7OztJQUdYLHFEQUNvQzs7Ozs7SUFHcEMsOENBQ2tDOzs7OztJQUdsQyxpREFDNkQ7Ozs7O0lBRzdELCtDQUMyRDs7Ozs7SUFHM0QsdUNBQ21EOztJQUVuRCx5Q0FBMEI7Ozs7O0lBRWQsNkNBQWtDOzs7OztJQUNsQyw0Q0FBOEI7Ozs7O0lBQzlCLGdEQUFzQzs7Ozs7SUFDdEMsdURBQW9EIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbnRlbnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vc2VydmljZXMvY29udGVudC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL3NlcnZpY2VzL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPbkNoYW5nZXMsIE91dHB1dCwgU2ltcGxlQ2hhbmdlcywgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBQcm9jZXNzQ29udGVudFNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9wcm9jZXNzLWNvbnRlbnQuc2VydmljZSc7XHJcbmltcG9ydCB7IENvbnRlbnRMaW5rTW9kZWwgfSBmcm9tICcuLi9jb3JlL2NvbnRlbnQtbGluay5tb2RlbCc7XHJcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi8uLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1jb250ZW50JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9jb250ZW50LndpZGdldC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2NvbnRlbnQud2lkZ2V0LnNjc3MnXSxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIENvbnRlbnRXaWRnZXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkNoYW5nZXMge1xyXG5cclxuICAgIC8qKiBUaGUgY29udGVudCBpZCB0byBzaG93LiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGlkOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqIFRvZ2dsZXMgc2hvd2luZyBkb2N1bWVudCBjb250ZW50LiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHNob3dEb2N1bWVudENvbnRlbnQ6IGJvb2xlYW4gPSB0cnVlO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIGNvbnRlbnQgaXMgY2xpY2tlZC4gKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgY29udGVudENsaWNrID0gbmV3IEV2ZW50RW1pdHRlcigpO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIHRodW1ibmFpbCBoYXMgbG9hZGVkLiAqL1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICB0aHVtYm5haWxMb2FkZWQ6IEV2ZW50RW1pdHRlcjxhbnk+ID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgLyoqIEVtaXR0ZWQgd2hlbiB0aGUgY29udGVudCBoYXMgbG9hZGVkLiAqL1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICBjb250ZW50TG9hZGVkOiBFdmVudEVtaXR0ZXI8YW55PiA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gYW4gZXJyb3Igb2NjdXJzLiAqL1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICBlcnJvcjogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICBjb250ZW50OiBDb250ZW50TGlua01vZGVsO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBmb3JtU2VydmljZTogRm9ybVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGNvbnRlbnRTZXJ2aWNlOiBDb250ZW50U2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgcHJvY2Vzc0NvbnRlbnRTZXJ2aWNlOiBQcm9jZXNzQ29udGVudFNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICAgICAgY29uc3QgY29udGVudElkID0gY2hhbmdlc1snaWQnXTtcclxuICAgICAgICBpZiAoY29udGVudElkICYmIGNvbnRlbnRJZC5jdXJyZW50VmFsdWUpIHtcclxuICAgICAgICAgICAgdGhpcy5sb2FkQ29udGVudChjb250ZW50SWQuY3VycmVudFZhbHVlKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbG9hZENvbnRlbnQoaWQ6IG51bWJlcikge1xyXG4gICAgICAgIHRoaXMucHJvY2Vzc0NvbnRlbnRTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5nZXRGaWxlQ29udGVudChpZClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIChyZXNwb25zZTogQ29udGVudExpbmtNb2RlbCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29udGVudCA9IG5ldyBDb250ZW50TGlua01vZGVsKHJlc3BvbnNlKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbnRlbnRMb2FkZWQuZW1pdCh0aGlzLmNvbnRlbnQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZFRodW1ibmFpbFVybCh0aGlzLmNvbnRlbnQpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIChlcnJvcikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZXJyb3IuZW1pdChlcnJvcik7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgbG9hZFRodW1ibmFpbFVybChjb250ZW50OiBDb250ZW50TGlua01vZGVsKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY29udGVudC5pc1RodW1ibmFpbFN1cHBvcnRlZCgpKSB7XHJcbiAgICAgICAgICAgIGxldCBvYnNlcnZhYmxlOiBPYnNlcnZhYmxlPGFueT47XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy5jb250ZW50LmlzVHlwZUltYWdlKCkpIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmFibGUgPSB0aGlzLnByb2Nlc3NDb250ZW50U2VydmljZS5nZXRGaWxlUmF3Q29udGVudChjb250ZW50LmlkKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIG9ic2VydmFibGUgPSB0aGlzLnByb2Nlc3NDb250ZW50U2VydmljZS5nZXRDb250ZW50VGh1bWJuYWlsKGNvbnRlbnQuaWQpO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAob2JzZXJ2YWJsZSkge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2YWJsZS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgKHJlc3BvbnNlOiBCbG9iKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY29udGVudC50aHVtYm5haWxVcmwgPSB0aGlzLmNvbnRlbnRTZXJ2aWNlLmNyZWF0ZVRydXN0ZWRVcmwocmVzcG9uc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnRodW1ibmFpbExvYWRlZC5lbWl0KHRoaXMuY29udGVudC50aHVtYm5haWxVcmwpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZXJyb3IuZW1pdChlcnJvcik7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb3BlblZpZXdlcihjb250ZW50OiBDb250ZW50TGlua01vZGVsKTogdm9pZCB7XHJcbiAgICAgICAgbGV0IGZldGNoID0gdGhpcy5wcm9jZXNzQ29udGVudFNlcnZpY2UuZ2V0Q29udGVudFByZXZpZXcoY29udGVudC5pZCk7XHJcbiAgICAgICAgaWYgKGNvbnRlbnQuaXNUeXBlSW1hZ2UoKSB8fCBjb250ZW50LmlzVHlwZVBkZigpKSB7XHJcbiAgICAgICAgICAgIGZldGNoID0gdGhpcy5wcm9jZXNzQ29udGVudFNlcnZpY2UuZ2V0RmlsZVJhd0NvbnRlbnQoY29udGVudC5pZCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGZldGNoLnN1YnNjcmliZShcclxuICAgICAgICAgICAgKGJsb2I6IEJsb2IpID0+IHtcclxuICAgICAgICAgICAgICAgIGNvbnRlbnQuY29udGVudEJsb2IgPSBibG9iO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jb250ZW50Q2xpY2suZW1pdChjb250ZW50KTtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9nU2VydmljZS5pbmZvKCdDb250ZW50IGNsaWNrZWQnICsgY29udGVudC5pZCk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZvcm1TZXJ2aWNlLmZvcm1Db250ZW50Q2xpY2tlZC5uZXh0KGNvbnRlbnQpO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZXJyb3IuZW1pdChlcnJvcik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogSW52b2tlIGNvbnRlbnQgZG93bmxvYWQuXHJcbiAgICAgKi9cclxuICAgIGRvd25sb2FkKGNvbnRlbnQ6IENvbnRlbnRMaW5rTW9kZWwpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnByb2Nlc3NDb250ZW50U2VydmljZS5nZXRGaWxlUmF3Q29udGVudChjb250ZW50LmlkKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgIChibG9iOiBCbG9iKSA9PiB0aGlzLmNvbnRlbnRTZXJ2aWNlLmRvd25sb2FkQmxvYihibG9iLCBjb250ZW50Lm5hbWUpLFxyXG4gICAgICAgICAgICAoZXJyb3IpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZXJyb3IuZW1pdChlcnJvcik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==