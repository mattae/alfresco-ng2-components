/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
var ContainerColumnModel = /** @class */ (function () {
    function ContainerColumnModel() {
        this.size = 12;
        this.fields = [];
        this.colspan = 1;
        this.rowspan = 1;
    }
    /**
     * @return {?}
     */
    ContainerColumnModel.prototype.hasFields = /**
     * @return {?}
     */
    function () {
        return this.fields && this.fields.length > 0;
    };
    return ContainerColumnModel;
}());
export { ContainerColumnModel };
if (false) {
    /** @type {?} */
    ContainerColumnModel.prototype.size;
    /** @type {?} */
    ContainerColumnModel.prototype.fields;
    /** @type {?} */
    ContainerColumnModel.prototype.colspan;
    /** @type {?} */
    ContainerColumnModel.prototype.rowspan;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLWNvbHVtbi5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2NvcmUvY29udGFpbmVyLWNvbHVtbi5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQkE7SUFBQTtRQUVJLFNBQUksR0FBVyxFQUFFLENBQUM7UUFDbEIsV0FBTSxHQUFxQixFQUFFLENBQUM7UUFDOUIsWUFBTyxHQUFXLENBQUMsQ0FBQztRQUNwQixZQUFPLEdBQVcsQ0FBQyxDQUFDO0lBS3hCLENBQUM7Ozs7SUFIRyx3Q0FBUzs7O0lBQVQ7UUFDSSxPQUFPLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO0lBQ2pELENBQUM7SUFDTCwyQkFBQztBQUFELENBQUMsQUFWRCxJQVVDOzs7O0lBUkcsb0NBQWtCOztJQUNsQixzQ0FBOEI7O0lBQzlCLHVDQUFvQjs7SUFDcEIsdUNBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IEZvcm1GaWVsZE1vZGVsIH0gZnJvbSAnLi9mb3JtLWZpZWxkLm1vZGVsJztcclxuXHJcbmV4cG9ydCBjbGFzcyBDb250YWluZXJDb2x1bW5Nb2RlbCB7XHJcblxyXG4gICAgc2l6ZTogbnVtYmVyID0gMTI7XHJcbiAgICBmaWVsZHM6IEZvcm1GaWVsZE1vZGVsW10gPSBbXTtcclxuICAgIGNvbHNwYW46IG51bWJlciA9IDE7XHJcbiAgICByb3dzcGFuOiBudW1iZXIgPSAxO1xyXG5cclxuICAgIGhhc0ZpZWxkcygpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5maWVsZHMgJiYgdGhpcy5maWVsZHMubGVuZ3RoID4gMDtcclxuICAgIH1cclxufVxyXG4iXX0=