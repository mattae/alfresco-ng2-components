/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { FormWidgetModel } from './form-widget.model';
var ContainerModel = /** @class */ (function (_super) {
    tslib_1.__extends(ContainerModel, _super);
    function ContainerModel(field) {
        var _this = _super.call(this, field.form, field.json) || this;
        if (field) {
            _this.field = field;
        }
        return _this;
    }
    Object.defineProperty(ContainerModel.prototype, "isVisible", {
        get: /**
         * @return {?}
         */
        function () {
            return this.field.isVisible;
        },
        enumerable: true,
        configurable: true
    });
    return ContainerModel;
}(FormWidgetModel));
export { ContainerModel };
if (false) {
    /** @type {?} */
    ContainerModel.prototype.field;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGFpbmVyLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvY29yZS9jb250YWluZXIubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW9CQSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFFdEQ7SUFBb0MsMENBQWU7SUFRL0Msd0JBQVksS0FBcUI7UUFBakMsWUFDSSxrQkFBTSxLQUFLLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsU0FLaEM7UUFIRyxJQUFJLEtBQUssRUFBRTtZQUNQLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1NBQ3RCOztJQUNMLENBQUM7SUFWRCxzQkFBSSxxQ0FBUzs7OztRQUFiO1lBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLFNBQVMsQ0FBQztRQUNoQyxDQUFDOzs7T0FBQTtJQVVMLHFCQUFDO0FBQUQsQ0FBQyxBQWhCRCxDQUFvQyxlQUFlLEdBZ0JsRDs7OztJQWRHLCtCQUFzQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4gLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgRm9ybUZpZWxkTW9kZWwgfSBmcm9tICcuL2Zvcm0tZmllbGQubW9kZWwnO1xyXG5pbXBvcnQgeyBGb3JtV2lkZ2V0TW9kZWwgfSBmcm9tICcuL2Zvcm0td2lkZ2V0Lm1vZGVsJztcclxuXHJcbmV4cG9ydCBjbGFzcyBDb250YWluZXJNb2RlbCBleHRlbmRzIEZvcm1XaWRnZXRNb2RlbCB7XHJcblxyXG4gICAgZmllbGQ6IEZvcm1GaWVsZE1vZGVsO1xyXG5cclxuICAgIGdldCBpc1Zpc2libGUoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZmllbGQuaXNWaXNpYmxlO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKGZpZWxkOiBGb3JtRmllbGRNb2RlbCkge1xyXG4gICAgICAgIHN1cGVyKGZpZWxkLmZvcm0sIGZpZWxkLmpzb24pO1xyXG5cclxuICAgICAgICBpZiAoZmllbGQpIHtcclxuICAgICAgICAgICAgdGhpcy5maWVsZCA9IGZpZWxkO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuIl19