/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
var ContentLinkModel = /** @class */ (function () {
    function ContentLinkModel(obj) {
        this.contentAvailable = obj && obj.contentAvailable;
        this.created = obj && obj.created;
        this.createdBy = obj && obj.createdBy || {};
        this.id = obj && obj.id;
        this.link = obj && obj.link;
        this.mimeType = obj && obj.mimeType;
        this.name = obj && obj.name;
        this.previewStatus = obj && obj.previewStatus;
        this.relatedContent = obj && obj.relatedContent;
        this.simpleType = obj && obj.simpleType;
        this.thumbnailStatus = obj && obj.thumbnailStatus;
    }
    /**
     * @return {?}
     */
    ContentLinkModel.prototype.hasPreviewStatus = /**
     * @return {?}
     */
    function () {
        return this.previewStatus === 'supported' ? true : false;
    };
    /**
     * @return {?}
     */
    ContentLinkModel.prototype.isTypeImage = /**
     * @return {?}
     */
    function () {
        return this.simpleType === 'image' ? true : false;
    };
    /**
     * @return {?}
     */
    ContentLinkModel.prototype.isTypePdf = /**
     * @return {?}
     */
    function () {
        return this.simpleType === 'pdf' ? true : false;
    };
    /**
     * @return {?}
     */
    ContentLinkModel.prototype.isTypeDoc = /**
     * @return {?}
     */
    function () {
        return this.simpleType === 'word' || this.simpleType === 'content' ? true : false;
    };
    /**
     * @return {?}
     */
    ContentLinkModel.prototype.isThumbnailReady = /**
     * @return {?}
     */
    function () {
        return this.thumbnailStatus === 'created';
    };
    /**
     * @return {?}
     */
    ContentLinkModel.prototype.isThumbnailSupported = /**
     * @return {?}
     */
    function () {
        return this.isTypeImage() || ((this.isTypePdf() || this.isTypeDoc()) && this.isThumbnailReady());
    };
    return ContentLinkModel;
}());
export { ContentLinkModel };
if (false) {
    /** @type {?} */
    ContentLinkModel.prototype.contentAvailable;
    /** @type {?} */
    ContentLinkModel.prototype.created;
    /** @type {?} */
    ContentLinkModel.prototype.createdBy;
    /** @type {?} */
    ContentLinkModel.prototype.id;
    /** @type {?} */
    ContentLinkModel.prototype.link;
    /** @type {?} */
    ContentLinkModel.prototype.mimeType;
    /** @type {?} */
    ContentLinkModel.prototype.name;
    /** @type {?} */
    ContentLinkModel.prototype.previewStatus;
    /** @type {?} */
    ContentLinkModel.prototype.relatedContent;
    /** @type {?} */
    ContentLinkModel.prototype.simpleType;
    /** @type {?} */
    ContentLinkModel.prototype.thumbnailUrl;
    /** @type {?} */
    ContentLinkModel.prototype.contentRawUrl;
    /** @type {?} */
    ContentLinkModel.prototype.contentBlob;
    /** @type {?} */
    ContentLinkModel.prototype.thumbnailStatus;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC1saW5rLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvY29yZS9jb250ZW50LWxpbmsubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBcUJDO0lBaUJHLDBCQUFZLEdBQVM7UUFDakIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsZ0JBQWdCLENBQUM7UUFDcEQsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQztRQUNsQyxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsU0FBUyxJQUFJLEVBQUUsQ0FBQztRQUM1QyxJQUFJLENBQUMsRUFBRSxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsRUFBRSxDQUFDO1FBQ3hCLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxJQUFJLENBQUM7UUFDNUIsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLFFBQVEsQ0FBQztRQUNwQyxJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDO1FBQzVCLElBQUksQ0FBQyxhQUFhLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxhQUFhLENBQUM7UUFDOUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLGNBQWMsQ0FBQztRQUNoRCxJQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsVUFBVSxDQUFDO1FBQ3hDLElBQUksQ0FBQyxlQUFlLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxlQUFlLENBQUM7SUFDdEQsQ0FBQzs7OztJQUVELDJDQUFnQjs7O0lBQWhCO1FBQ0ksT0FBTyxJQUFJLENBQUMsYUFBYSxLQUFLLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDN0QsQ0FBQzs7OztJQUVELHNDQUFXOzs7SUFBWDtRQUNJLE9BQU8sSUFBSSxDQUFDLFVBQVUsS0FBSyxPQUFPLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ3RELENBQUM7Ozs7SUFFRCxvQ0FBUzs7O0lBQVQ7UUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLEtBQUssS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUNwRCxDQUFDOzs7O0lBRUQsb0NBQVM7OztJQUFUO1FBQ0ksT0FBTyxJQUFJLENBQUMsVUFBVSxLQUFLLE1BQU0sSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDdEYsQ0FBQzs7OztJQUVELDJDQUFnQjs7O0lBQWhCO1FBQ0ksT0FBTyxJQUFJLENBQUMsZUFBZSxLQUFLLFNBQVMsQ0FBQztJQUM5QyxDQUFDOzs7O0lBRUQsK0NBQW9COzs7SUFBcEI7UUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUUsQ0FBQyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRSxDQUFDLENBQUM7SUFDckcsQ0FBQztJQUNMLHVCQUFDO0FBQUQsQ0FBQyxBQXREQSxJQXNEQTs7OztJQXBERyw0Q0FBMEI7O0lBQzFCLG1DQUFjOztJQUNkLHFDQUFlOztJQUNmLDhCQUFXOztJQUNYLGdDQUFjOztJQUNkLG9DQUFpQjs7SUFDakIsZ0NBQWE7O0lBQ2IseUNBQXNCOztJQUN0QiwwQ0FBd0I7O0lBQ3hCLHNDQUFtQjs7SUFDbkIsd0NBQXFCOztJQUNyQix5Q0FBc0I7O0lBQ3RCLHVDQUFrQjs7SUFDbEIsMkNBQXdCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbiAvKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG4gaW1wb3J0IHsgUmVsYXRlZENvbnRlbnRSZXByZXNlbnRhdGlvbiB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5cclxuIGV4cG9ydCBjbGFzcyBDb250ZW50TGlua01vZGVsIGltcGxlbWVudHMgUmVsYXRlZENvbnRlbnRSZXByZXNlbnRhdGlvbiB7XHJcblxyXG4gICAgY29udGVudEF2YWlsYWJsZTogYm9vbGVhbjtcclxuICAgIGNyZWF0ZWQ6IERhdGU7XHJcbiAgICBjcmVhdGVkQnk6IGFueTtcclxuICAgIGlkOiBudW1iZXI7XHJcbiAgICBsaW5rOiBib29sZWFuO1xyXG4gICAgbWltZVR5cGU6IHN0cmluZztcclxuICAgIG5hbWU6IHN0cmluZztcclxuICAgIHByZXZpZXdTdGF0dXM6IHN0cmluZztcclxuICAgIHJlbGF0ZWRDb250ZW50OiBib29sZWFuO1xyXG4gICAgc2ltcGxlVHlwZTogc3RyaW5nO1xyXG4gICAgdGh1bWJuYWlsVXJsOiBzdHJpbmc7XHJcbiAgICBjb250ZW50UmF3VXJsOiBzdHJpbmc7XHJcbiAgICBjb250ZW50QmxvYjogQmxvYjtcclxuICAgIHRodW1ibmFpbFN0YXR1czogc3RyaW5nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKG9iaj86IGFueSkge1xyXG4gICAgICAgIHRoaXMuY29udGVudEF2YWlsYWJsZSA9IG9iaiAmJiBvYmouY29udGVudEF2YWlsYWJsZTtcclxuICAgICAgICB0aGlzLmNyZWF0ZWQgPSBvYmogJiYgb2JqLmNyZWF0ZWQ7XHJcbiAgICAgICAgdGhpcy5jcmVhdGVkQnkgPSBvYmogJiYgb2JqLmNyZWF0ZWRCeSB8fCB7fTtcclxuICAgICAgICB0aGlzLmlkID0gb2JqICYmIG9iai5pZDtcclxuICAgICAgICB0aGlzLmxpbmsgPSBvYmogJiYgb2JqLmxpbms7XHJcbiAgICAgICAgdGhpcy5taW1lVHlwZSA9IG9iaiAmJiBvYmoubWltZVR5cGU7XHJcbiAgICAgICAgdGhpcy5uYW1lID0gb2JqICYmIG9iai5uYW1lO1xyXG4gICAgICAgIHRoaXMucHJldmlld1N0YXR1cyA9IG9iaiAmJiBvYmoucHJldmlld1N0YXR1cztcclxuICAgICAgICB0aGlzLnJlbGF0ZWRDb250ZW50ID0gb2JqICYmIG9iai5yZWxhdGVkQ29udGVudDtcclxuICAgICAgICB0aGlzLnNpbXBsZVR5cGUgPSBvYmogJiYgb2JqLnNpbXBsZVR5cGU7XHJcbiAgICAgICAgdGhpcy50aHVtYm5haWxTdGF0dXMgPSBvYmogJiYgb2JqLnRodW1ibmFpbFN0YXR1cztcclxuICAgIH1cclxuXHJcbiAgICBoYXNQcmV2aWV3U3RhdHVzKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnByZXZpZXdTdGF0dXMgPT09ICdzdXBwb3J0ZWQnID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzVHlwZUltYWdlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNpbXBsZVR5cGUgPT09ICdpbWFnZScgPyB0cnVlIDogZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaXNUeXBlUGRmKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNpbXBsZVR5cGUgPT09ICdwZGYnID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzVHlwZURvYygpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zaW1wbGVUeXBlID09PSAnd29yZCcgfHwgdGhpcy5zaW1wbGVUeXBlID09PSAnY29udGVudCcgPyB0cnVlIDogZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaXNUaHVtYm5haWxSZWFkeSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy50aHVtYm5haWxTdGF0dXMgPT09ICdjcmVhdGVkJztcclxuICAgIH1cclxuXHJcbiAgICBpc1RodW1ibmFpbFN1cHBvcnRlZCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pc1R5cGVJbWFnZSgpIHx8ICgodGhpcy5pc1R5cGVQZGYoKSB8fCB0aGlzLmlzVHlwZURvYygpKSAmJiB0aGlzLmlzVGh1bWJuYWlsUmVhZHkoKSk7XHJcbiAgICB9XHJcbn1cclxuIl19