/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
var FormFieldTypes = /** @class */ (function () {
    function FormFieldTypes() {
    }
    /**
     * @param {?} type
     * @return {?}
     */
    FormFieldTypes.isReadOnlyType = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return FormFieldTypes.READONLY_TYPES.indexOf(type) > -1;
    };
    /**
     * @param {?} type
     * @return {?}
     */
    FormFieldTypes.isContainerType = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return type === FormFieldTypes.CONTAINER || type === FormFieldTypes.GROUP;
    };
    FormFieldTypes.CONTAINER = 'container';
    FormFieldTypes.GROUP = 'group';
    FormFieldTypes.DYNAMIC_TABLE = 'dynamic-table';
    FormFieldTypes.TEXT = 'text';
    FormFieldTypes.MULTILINE_TEXT = 'multi-line-text';
    FormFieldTypes.DROPDOWN = 'dropdown';
    FormFieldTypes.HYPERLINK = 'hyperlink';
    FormFieldTypes.RADIO_BUTTONS = 'radio-buttons';
    FormFieldTypes.DISPLAY_VALUE = 'readonly';
    FormFieldTypes.READONLY_TEXT = 'readonly-text';
    FormFieldTypes.UPLOAD = 'upload';
    FormFieldTypes.TYPEAHEAD = 'typeahead';
    FormFieldTypes.FUNCTIONAL_GROUP = 'functional-group';
    FormFieldTypes.PEOPLE = 'people';
    FormFieldTypes.BOOLEAN = 'boolean';
    FormFieldTypes.NUMBER = 'integer';
    FormFieldTypes.DATE = 'date';
    FormFieldTypes.AMOUNT = 'amount';
    FormFieldTypes.DOCUMENT = 'document';
    FormFieldTypes.DATETIME = 'datetime';
    FormFieldTypes.ATTACH_FOLDER = 'select-folder';
    FormFieldTypes.READONLY_TYPES = [
        FormFieldTypes.HYPERLINK,
        FormFieldTypes.DISPLAY_VALUE,
        FormFieldTypes.READONLY_TEXT
    ];
    return FormFieldTypes;
}());
export { FormFieldTypes };
if (false) {
    /** @type {?} */
    FormFieldTypes.CONTAINER;
    /** @type {?} */
    FormFieldTypes.GROUP;
    /** @type {?} */
    FormFieldTypes.DYNAMIC_TABLE;
    /** @type {?} */
    FormFieldTypes.TEXT;
    /** @type {?} */
    FormFieldTypes.MULTILINE_TEXT;
    /** @type {?} */
    FormFieldTypes.DROPDOWN;
    /** @type {?} */
    FormFieldTypes.HYPERLINK;
    /** @type {?} */
    FormFieldTypes.RADIO_BUTTONS;
    /** @type {?} */
    FormFieldTypes.DISPLAY_VALUE;
    /** @type {?} */
    FormFieldTypes.READONLY_TEXT;
    /** @type {?} */
    FormFieldTypes.UPLOAD;
    /** @type {?} */
    FormFieldTypes.TYPEAHEAD;
    /** @type {?} */
    FormFieldTypes.FUNCTIONAL_GROUP;
    /** @type {?} */
    FormFieldTypes.PEOPLE;
    /** @type {?} */
    FormFieldTypes.BOOLEAN;
    /** @type {?} */
    FormFieldTypes.NUMBER;
    /** @type {?} */
    FormFieldTypes.DATE;
    /** @type {?} */
    FormFieldTypes.AMOUNT;
    /** @type {?} */
    FormFieldTypes.DOCUMENT;
    /** @type {?} */
    FormFieldTypes.DATETIME;
    /** @type {?} */
    FormFieldTypes.ATTACH_FOLDER;
    /** @type {?} */
    FormFieldTypes.READONLY_TYPES;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC10eXBlcy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2NvcmUvZm9ybS1maWVsZC10eXBlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7SUFBQTtJQW9DQSxDQUFDOzs7OztJQVBVLDZCQUFjOzs7O0lBQXJCLFVBQXNCLElBQVk7UUFDOUIsT0FBTyxjQUFjLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUM1RCxDQUFDOzs7OztJQUVNLDhCQUFlOzs7O0lBQXRCLFVBQXVCLElBQVk7UUFDL0IsT0FBTyxJQUFJLEtBQUssY0FBYyxDQUFDLFNBQVMsSUFBSSxJQUFJLEtBQUssY0FBYyxDQUFDLEtBQUssQ0FBQztJQUM5RSxDQUFDO0lBbENNLHdCQUFTLEdBQVcsV0FBVyxDQUFDO0lBQ2hDLG9CQUFLLEdBQVcsT0FBTyxDQUFDO0lBQ3hCLDRCQUFhLEdBQVcsZUFBZSxDQUFDO0lBQ3hDLG1CQUFJLEdBQVcsTUFBTSxDQUFDO0lBQ3RCLDZCQUFjLEdBQVcsaUJBQWlCLENBQUM7SUFDM0MsdUJBQVEsR0FBVyxVQUFVLENBQUM7SUFDOUIsd0JBQVMsR0FBVyxXQUFXLENBQUM7SUFDaEMsNEJBQWEsR0FBVyxlQUFlLENBQUM7SUFDeEMsNEJBQWEsR0FBVyxVQUFVLENBQUM7SUFDbkMsNEJBQWEsR0FBVyxlQUFlLENBQUM7SUFDeEMscUJBQU0sR0FBVyxRQUFRLENBQUM7SUFDMUIsd0JBQVMsR0FBVyxXQUFXLENBQUM7SUFDaEMsK0JBQWdCLEdBQVcsa0JBQWtCLENBQUM7SUFDOUMscUJBQU0sR0FBVyxRQUFRLENBQUM7SUFDMUIsc0JBQU8sR0FBVyxTQUFTLENBQUM7SUFDNUIscUJBQU0sR0FBVyxTQUFTLENBQUM7SUFDM0IsbUJBQUksR0FBVyxNQUFNLENBQUM7SUFDdEIscUJBQU0sR0FBVyxRQUFRLENBQUM7SUFDMUIsdUJBQVEsR0FBVyxVQUFVLENBQUM7SUFDOUIsdUJBQVEsR0FBVyxVQUFVLENBQUM7SUFDOUIsNEJBQWEsR0FBVyxlQUFlLENBQUM7SUFFeEMsNkJBQWMsR0FBYTtRQUM5QixjQUFjLENBQUMsU0FBUztRQUN4QixjQUFjLENBQUMsYUFBYTtRQUM1QixjQUFjLENBQUMsYUFBYTtLQUMvQixDQUFDO0lBU04scUJBQUM7Q0FBQSxBQXBDRCxJQW9DQztTQXBDWSxjQUFjOzs7SUFDdkIseUJBQXVDOztJQUN2QyxxQkFBK0I7O0lBQy9CLDZCQUErQzs7SUFDL0Msb0JBQTZCOztJQUM3Qiw4QkFBa0Q7O0lBQ2xELHdCQUFxQzs7SUFDckMseUJBQXVDOztJQUN2Qyw2QkFBK0M7O0lBQy9DLDZCQUEwQzs7SUFDMUMsNkJBQStDOztJQUMvQyxzQkFBaUM7O0lBQ2pDLHlCQUF1Qzs7SUFDdkMsZ0NBQXFEOztJQUNyRCxzQkFBaUM7O0lBQ2pDLHVCQUFtQzs7SUFDbkMsc0JBQWtDOztJQUNsQyxvQkFBNkI7O0lBQzdCLHNCQUFpQzs7SUFDakMsd0JBQXFDOztJQUNyQyx3QkFBcUM7O0lBQ3JDLDZCQUErQzs7SUFFL0MsOEJBSUUiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuIC8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmV4cG9ydCBjbGFzcyBGb3JtRmllbGRUeXBlcyB7XHJcbiAgICBzdGF0aWMgQ09OVEFJTkVSOiBzdHJpbmcgPSAnY29udGFpbmVyJztcclxuICAgIHN0YXRpYyBHUk9VUDogc3RyaW5nID0gJ2dyb3VwJztcclxuICAgIHN0YXRpYyBEWU5BTUlDX1RBQkxFOiBzdHJpbmcgPSAnZHluYW1pYy10YWJsZSc7XHJcbiAgICBzdGF0aWMgVEVYVDogc3RyaW5nID0gJ3RleHQnO1xyXG4gICAgc3RhdGljIE1VTFRJTElORV9URVhUOiBzdHJpbmcgPSAnbXVsdGktbGluZS10ZXh0JztcclxuICAgIHN0YXRpYyBEUk9QRE9XTjogc3RyaW5nID0gJ2Ryb3Bkb3duJztcclxuICAgIHN0YXRpYyBIWVBFUkxJTks6IHN0cmluZyA9ICdoeXBlcmxpbmsnO1xyXG4gICAgc3RhdGljIFJBRElPX0JVVFRPTlM6IHN0cmluZyA9ICdyYWRpby1idXR0b25zJztcclxuICAgIHN0YXRpYyBESVNQTEFZX1ZBTFVFOiBzdHJpbmcgPSAncmVhZG9ubHknO1xyXG4gICAgc3RhdGljIFJFQURPTkxZX1RFWFQ6IHN0cmluZyA9ICdyZWFkb25seS10ZXh0JztcclxuICAgIHN0YXRpYyBVUExPQUQ6IHN0cmluZyA9ICd1cGxvYWQnO1xyXG4gICAgc3RhdGljIFRZUEVBSEVBRDogc3RyaW5nID0gJ3R5cGVhaGVhZCc7XHJcbiAgICBzdGF0aWMgRlVOQ1RJT05BTF9HUk9VUDogc3RyaW5nID0gJ2Z1bmN0aW9uYWwtZ3JvdXAnO1xyXG4gICAgc3RhdGljIFBFT1BMRTogc3RyaW5nID0gJ3Blb3BsZSc7XHJcbiAgICBzdGF0aWMgQk9PTEVBTjogc3RyaW5nID0gJ2Jvb2xlYW4nO1xyXG4gICAgc3RhdGljIE5VTUJFUjogc3RyaW5nID0gJ2ludGVnZXInO1xyXG4gICAgc3RhdGljIERBVEU6IHN0cmluZyA9ICdkYXRlJztcclxuICAgIHN0YXRpYyBBTU9VTlQ6IHN0cmluZyA9ICdhbW91bnQnO1xyXG4gICAgc3RhdGljIERPQ1VNRU5UOiBzdHJpbmcgPSAnZG9jdW1lbnQnO1xyXG4gICAgc3RhdGljIERBVEVUSU1FOiBzdHJpbmcgPSAnZGF0ZXRpbWUnO1xyXG4gICAgc3RhdGljIEFUVEFDSF9GT0xERVI6IHN0cmluZyA9ICdzZWxlY3QtZm9sZGVyJztcclxuXHJcbiAgICBzdGF0aWMgUkVBRE9OTFlfVFlQRVM6IHN0cmluZ1tdID0gW1xyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLkhZUEVSTElOSyxcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5ESVNQTEFZX1ZBTFVFLFxyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLlJFQURPTkxZX1RFWFRcclxuICAgIF07XHJcblxyXG4gICAgc3RhdGljIGlzUmVhZE9ubHlUeXBlKHR5cGU6IHN0cmluZykge1xyXG4gICAgICAgIHJldHVybiBGb3JtRmllbGRUeXBlcy5SRUFET05MWV9UWVBFUy5pbmRleE9mKHR5cGUpID4gLTE7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGlzQ29udGFpbmVyVHlwZSh0eXBlOiBzdHJpbmcpIHtcclxuICAgICAgICByZXR1cm4gdHlwZSA9PT0gRm9ybUZpZWxkVHlwZXMuQ09OVEFJTkVSIHx8IHR5cGUgPT09IEZvcm1GaWVsZFR5cGVzLkdST1VQO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==