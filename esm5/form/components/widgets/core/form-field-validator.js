/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import moment from 'moment-es6';
import { FormFieldTypes } from './form-field-types';
/**
 * @record
 */
export function FormFieldValidator() { }
if (false) {
    /**
     * @param {?} field
     * @return {?}
     */
    FormFieldValidator.prototype.isSupported = function (field) { };
    /**
     * @param {?} field
     * @return {?}
     */
    FormFieldValidator.prototype.validate = function (field) { };
}
var RequiredFieldValidator = /** @class */ (function () {
    function RequiredFieldValidator() {
        this.supportedTypes = [
            FormFieldTypes.TEXT,
            FormFieldTypes.MULTILINE_TEXT,
            FormFieldTypes.NUMBER,
            FormFieldTypes.BOOLEAN,
            FormFieldTypes.TYPEAHEAD,
            FormFieldTypes.DROPDOWN,
            FormFieldTypes.PEOPLE,
            FormFieldTypes.FUNCTIONAL_GROUP,
            FormFieldTypes.RADIO_BUTTONS,
            FormFieldTypes.UPLOAD,
            FormFieldTypes.AMOUNT,
            FormFieldTypes.DYNAMIC_TABLE,
            FormFieldTypes.DATE,
            FormFieldTypes.DATETIME,
            FormFieldTypes.ATTACH_FOLDER
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    RequiredFieldValidator.prototype.isSupported = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 &&
            field.required;
    };
    /**
     * @param {?} field
     * @return {?}
     */
    RequiredFieldValidator.prototype.validate = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        if (this.isSupported(field) && field.isVisible) {
            if (field.type === FormFieldTypes.DROPDOWN) {
                if (field.hasEmptyValue && field.emptyOption) {
                    if (field.value === field.emptyOption.id) {
                        return false;
                    }
                }
            }
            if (field.type === FormFieldTypes.RADIO_BUTTONS) {
                /** @type {?} */
                var option = field.options.find((/**
                 * @param {?} opt
                 * @return {?}
                 */
                function (opt) { return opt.id === field.value; }));
                return !!option;
            }
            if (field.type === FormFieldTypes.UPLOAD) {
                return field.value && field.value.length > 0;
            }
            if (field.type === FormFieldTypes.DYNAMIC_TABLE) {
                return field.value && field.value instanceof Array && field.value.length > 0;
            }
            if (field.type === FormFieldTypes.BOOLEAN) {
                return field.value ? true : false;
            }
            if (field.value === null || field.value === undefined || field.value === '') {
                return false;
            }
        }
        return true;
    };
    return RequiredFieldValidator;
}());
export { RequiredFieldValidator };
if (false) {
    /**
     * @type {?}
     * @private
     */
    RequiredFieldValidator.prototype.supportedTypes;
}
var NumberFieldValidator = /** @class */ (function () {
    function NumberFieldValidator() {
        this.supportedTypes = [
            FormFieldTypes.NUMBER,
            FormFieldTypes.AMOUNT
        ];
    }
    /**
     * @param {?} value
     * @return {?}
     */
    NumberFieldValidator.isNumber = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (value === null || value === undefined || value === '') {
            return false;
        }
        return !isNaN(+value);
    };
    /**
     * @param {?} field
     * @return {?}
     */
    NumberFieldValidator.prototype.isSupported = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field && this.supportedTypes.indexOf(field.type) > -1;
    };
    /**
     * @param {?} field
     * @return {?}
     */
    NumberFieldValidator.prototype.validate = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        if (this.isSupported(field) && field.isVisible) {
            if (field.value === null ||
                field.value === undefined ||
                field.value === '') {
                return true;
            }
            /** @type {?} */
            var valueStr = '' + field.value;
            /** @type {?} */
            var pattern = new RegExp(/^-?\d+$/);
            if (field.enableFractions) {
                pattern = new RegExp(/^-?[0-9]+(\.[0-9]{1,2})?$/);
            }
            if (valueStr.match(pattern)) {
                return true;
            }
            field.validationSummary.message = 'FORM.FIELD.VALIDATOR.INVALID_NUMBER';
            return false;
        }
        return true;
    };
    return NumberFieldValidator;
}());
export { NumberFieldValidator };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NumberFieldValidator.prototype.supportedTypes;
}
var DateFieldValidator = /** @class */ (function () {
    function DateFieldValidator() {
        this.supportedTypes = [
            FormFieldTypes.DATE
        ];
    }
    // Validates that the input string is a valid date formatted as <dateFormat> (default D-M-YYYY)
    // Validates that the input string is a valid date formatted as <dateFormat> (default D-M-YYYY)
    /**
     * @param {?} inputDate
     * @param {?=} dateFormat
     * @return {?}
     */
    DateFieldValidator.isValidDate = 
    // Validates that the input string is a valid date formatted as <dateFormat> (default D-M-YYYY)
    /**
     * @param {?} inputDate
     * @param {?=} dateFormat
     * @return {?}
     */
    function (inputDate, dateFormat) {
        if (dateFormat === void 0) { dateFormat = 'D-M-YYYY'; }
        if (inputDate) {
            /** @type {?} */
            var d = moment(inputDate, dateFormat, true);
            return d.isValid();
        }
        return false;
    };
    /**
     * @param {?} field
     * @return {?}
     */
    DateFieldValidator.prototype.isSupported = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field && this.supportedTypes.indexOf(field.type) > -1;
    };
    /**
     * @param {?} field
     * @return {?}
     */
    DateFieldValidator.prototype.validate = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        if (this.isSupported(field) && field.value && field.isVisible) {
            if (DateFieldValidator.isValidDate(field.value, field.dateDisplayFormat)) {
                return true;
            }
            field.validationSummary.message = field.dateDisplayFormat;
            return false;
        }
        return true;
    };
    return DateFieldValidator;
}());
export { DateFieldValidator };
if (false) {
    /**
     * @type {?}
     * @private
     */
    DateFieldValidator.prototype.supportedTypes;
}
var MinDateFieldValidator = /** @class */ (function () {
    function MinDateFieldValidator() {
        this.supportedTypes = [
            FormFieldTypes.DATE
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    MinDateFieldValidator.prototype.isSupported = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 && !!field.minValue;
    };
    /**
     * @param {?} field
     * @return {?}
     */
    MinDateFieldValidator.prototype.validate = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        /** @type {?} */
        var isValid = true;
        if (this.isSupported(field) && field.value && field.isVisible) {
            /** @type {?} */
            var dateFormat = field.dateDisplayFormat;
            if (!DateFieldValidator.isValidDate(field.value, dateFormat)) {
                field.validationSummary.message = 'FORM.FIELD.VALIDATOR.INVALID_DATE';
                isValid = false;
            }
            else {
                isValid = this.checkDate(field, dateFormat);
            }
        }
        return isValid;
    };
    /**
     * @private
     * @param {?} field
     * @param {?} dateFormat
     * @return {?}
     */
    MinDateFieldValidator.prototype.checkDate = /**
     * @private
     * @param {?} field
     * @param {?} dateFormat
     * @return {?}
     */
    function (field, dateFormat) {
        /** @type {?} */
        var MIN_DATE_FORMAT = 'DD-MM-YYYY';
        /** @type {?} */
        var isValid = true;
        // remove time and timezone info
        /** @type {?} */
        var fieldValueData;
        if (typeof field.value === 'string') {
            fieldValueData = moment(field.value.split('T')[0], dateFormat);
        }
        else {
            fieldValueData = field.value;
        }
        /** @type {?} */
        var min = moment(field.minValue, MIN_DATE_FORMAT);
        if (fieldValueData.isBefore(min)) {
            field.validationSummary.message = "FORM.FIELD.VALIDATOR.NOT_LESS_THAN";
            field.validationSummary.attributes.set('minValue', min.format(field.dateDisplayFormat).toLocaleUpperCase());
            isValid = false;
        }
        return isValid;
    };
    return MinDateFieldValidator;
}());
export { MinDateFieldValidator };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MinDateFieldValidator.prototype.supportedTypes;
}
var MaxDateFieldValidator = /** @class */ (function () {
    function MaxDateFieldValidator() {
        this.MAX_DATE_FORMAT = 'DD-MM-YYYY';
        this.supportedTypes = [
            FormFieldTypes.DATE
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    MaxDateFieldValidator.prototype.isSupported = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 && !!field.maxValue;
    };
    /**
     * @param {?} field
     * @return {?}
     */
    MaxDateFieldValidator.prototype.validate = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        if (this.isSupported(field) && field.value && field.isVisible) {
            /** @type {?} */
            var dateFormat = field.dateDisplayFormat;
            if (!DateFieldValidator.isValidDate(field.value, dateFormat)) {
                field.validationSummary.message = 'FORM.FIELD.VALIDATOR.INVALID_DATE';
                return false;
            }
            // remove time and timezone info
            /** @type {?} */
            var d = void 0;
            if (typeof field.value === 'string') {
                d = moment(field.value.split('T')[0], dateFormat);
            }
            else {
                d = field.value;
            }
            /** @type {?} */
            var max = moment(field.maxValue, this.MAX_DATE_FORMAT);
            if (d.isAfter(max)) {
                field.validationSummary.message = "FORM.FIELD.VALIDATOR.NOT_GREATER_THAN";
                field.validationSummary.attributes.set('maxValue', max.format(field.dateDisplayFormat).toLocaleUpperCase());
                return false;
            }
        }
        return true;
    };
    return MaxDateFieldValidator;
}());
export { MaxDateFieldValidator };
if (false) {
    /** @type {?} */
    MaxDateFieldValidator.prototype.MAX_DATE_FORMAT;
    /**
     * @type {?}
     * @private
     */
    MaxDateFieldValidator.prototype.supportedTypes;
}
var MinDateTimeFieldValidator = /** @class */ (function () {
    function MinDateTimeFieldValidator() {
        this.supportedTypes = [
            FormFieldTypes.DATETIME
        ];
        this.MIN_DATETIME_FORMAT = 'YYYY-MM-DD hh:mm AZ';
    }
    /**
     * @param {?} field
     * @return {?}
     */
    MinDateTimeFieldValidator.prototype.isSupported = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 && !!field.minValue;
    };
    /**
     * @param {?} field
     * @return {?}
     */
    MinDateTimeFieldValidator.prototype.validate = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        /** @type {?} */
        var isValid = true;
        if (this.isSupported(field) && field.value && field.isVisible) {
            /** @type {?} */
            var dateFormat = field.dateDisplayFormat;
            if (!DateFieldValidator.isValidDate(field.value, dateFormat)) {
                field.validationSummary.message = 'FORM.FIELD.VALIDATOR.INVALID_DATE';
                isValid = false;
            }
            else {
                isValid = this.checkDateTime(field, dateFormat);
            }
        }
        return isValid;
    };
    /**
     * @private
     * @param {?} field
     * @param {?} dateFormat
     * @return {?}
     */
    MinDateTimeFieldValidator.prototype.checkDateTime = /**
     * @private
     * @param {?} field
     * @param {?} dateFormat
     * @return {?}
     */
    function (field, dateFormat) {
        /** @type {?} */
        var isValid = true;
        /** @type {?} */
        var fieldValueDate;
        if (typeof field.value === 'string') {
            fieldValueDate = moment(field.value, dateFormat);
        }
        else {
            fieldValueDate = field.value;
        }
        /** @type {?} */
        var min = moment(field.minValue, this.MIN_DATETIME_FORMAT);
        if (fieldValueDate.isBefore(min)) {
            field.validationSummary.message = "FORM.FIELD.VALIDATOR.NOT_LESS_THAN";
            field.validationSummary.attributes.set('minValue', min.format(field.dateDisplayFormat).replace(':', '-'));
            isValid = false;
        }
        return isValid;
    };
    return MinDateTimeFieldValidator;
}());
export { MinDateTimeFieldValidator };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MinDateTimeFieldValidator.prototype.supportedTypes;
    /** @type {?} */
    MinDateTimeFieldValidator.prototype.MIN_DATETIME_FORMAT;
}
var MaxDateTimeFieldValidator = /** @class */ (function () {
    function MaxDateTimeFieldValidator() {
        this.supportedTypes = [
            FormFieldTypes.DATETIME
        ];
        this.MAX_DATETIME_FORMAT = 'YYYY-MM-DD hh:mm AZ';
    }
    /**
     * @param {?} field
     * @return {?}
     */
    MaxDateTimeFieldValidator.prototype.isSupported = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 && !!field.maxValue;
    };
    /**
     * @param {?} field
     * @return {?}
     */
    MaxDateTimeFieldValidator.prototype.validate = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        /** @type {?} */
        var isValid = true;
        if (this.isSupported(field) && field.value && field.isVisible) {
            /** @type {?} */
            var dateFormat = field.dateDisplayFormat;
            if (!DateFieldValidator.isValidDate(field.value, dateFormat)) {
                field.validationSummary.message = 'FORM.FIELD.VALIDATOR.INVALID_DATE';
                isValid = false;
            }
            else {
                isValid = this.checkDateTime(field, dateFormat);
            }
        }
        return isValid;
    };
    /**
     * @private
     * @param {?} field
     * @param {?} dateFormat
     * @return {?}
     */
    MaxDateTimeFieldValidator.prototype.checkDateTime = /**
     * @private
     * @param {?} field
     * @param {?} dateFormat
     * @return {?}
     */
    function (field, dateFormat) {
        /** @type {?} */
        var isValid = true;
        /** @type {?} */
        var fieldValueDate;
        if (typeof field.value === 'string') {
            fieldValueDate = moment(field.value, dateFormat);
        }
        else {
            fieldValueDate = field.value;
        }
        /** @type {?} */
        var max = moment(field.maxValue, this.MAX_DATETIME_FORMAT);
        if (fieldValueDate.isAfter(max)) {
            field.validationSummary.message = "FORM.FIELD.VALIDATOR.NOT_GREATER_THAN";
            field.validationSummary.attributes.set('maxValue', max.format(field.dateDisplayFormat).replace(':', '-'));
            isValid = false;
        }
        return isValid;
    };
    return MaxDateTimeFieldValidator;
}());
export { MaxDateTimeFieldValidator };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MaxDateTimeFieldValidator.prototype.supportedTypes;
    /** @type {?} */
    MaxDateTimeFieldValidator.prototype.MAX_DATETIME_FORMAT;
}
var MinLengthFieldValidator = /** @class */ (function () {
    function MinLengthFieldValidator() {
        this.supportedTypes = [
            FormFieldTypes.TEXT,
            FormFieldTypes.MULTILINE_TEXT
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    MinLengthFieldValidator.prototype.isSupported = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 &&
            field.minLength > 0;
    };
    /**
     * @param {?} field
     * @return {?}
     */
    MinLengthFieldValidator.prototype.validate = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        if (this.isSupported(field) && field.value && field.isVisible) {
            if (field.value.length >= field.minLength) {
                return true;
            }
            field.validationSummary.message = "FORM.FIELD.VALIDATOR.AT_LEAST_LONG";
            field.validationSummary.attributes.set('minLength', field.minLength.toLocaleString());
            return false;
        }
        return true;
    };
    return MinLengthFieldValidator;
}());
export { MinLengthFieldValidator };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MinLengthFieldValidator.prototype.supportedTypes;
}
var MaxLengthFieldValidator = /** @class */ (function () {
    function MaxLengthFieldValidator() {
        this.supportedTypes = [
            FormFieldTypes.TEXT,
            FormFieldTypes.MULTILINE_TEXT
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    MaxLengthFieldValidator.prototype.isSupported = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 &&
            field.maxLength > 0;
    };
    /**
     * @param {?} field
     * @return {?}
     */
    MaxLengthFieldValidator.prototype.validate = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        if (this.isSupported(field) && field.value && field.isVisible) {
            if (field.value.length <= field.maxLength) {
                return true;
            }
            field.validationSummary.message = "FORM.FIELD.VALIDATOR.NO_LONGER_THAN";
            field.validationSummary.attributes.set('maxLength', field.maxLength.toLocaleString());
            return false;
        }
        return true;
    };
    return MaxLengthFieldValidator;
}());
export { MaxLengthFieldValidator };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MaxLengthFieldValidator.prototype.supportedTypes;
}
var MinValueFieldValidator = /** @class */ (function () {
    function MinValueFieldValidator() {
        this.supportedTypes = [
            FormFieldTypes.NUMBER,
            FormFieldTypes.AMOUNT
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    MinValueFieldValidator.prototype.isSupported = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 &&
            NumberFieldValidator.isNumber(field.minValue);
    };
    /**
     * @param {?} field
     * @return {?}
     */
    MinValueFieldValidator.prototype.validate = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        if (this.isSupported(field) && field.value && field.isVisible) {
            /** @type {?} */
            var value = +field.value;
            /** @type {?} */
            var minValue = +field.minValue;
            if (value >= minValue) {
                return true;
            }
            field.validationSummary.message = "FORM.FIELD.VALIDATOR.NOT_LESS_THAN";
            field.validationSummary.attributes.set('minValue', field.minValue.toLocaleString());
            return false;
        }
        return true;
    };
    return MinValueFieldValidator;
}());
export { MinValueFieldValidator };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MinValueFieldValidator.prototype.supportedTypes;
}
var MaxValueFieldValidator = /** @class */ (function () {
    function MaxValueFieldValidator() {
        this.supportedTypes = [
            FormFieldTypes.NUMBER,
            FormFieldTypes.AMOUNT
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    MaxValueFieldValidator.prototype.isSupported = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 &&
            NumberFieldValidator.isNumber(field.maxValue);
    };
    /**
     * @param {?} field
     * @return {?}
     */
    MaxValueFieldValidator.prototype.validate = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        if (this.isSupported(field) && field.value && field.isVisible) {
            /** @type {?} */
            var value = +field.value;
            /** @type {?} */
            var maxValue = +field.maxValue;
            if (value <= maxValue) {
                return true;
            }
            field.validationSummary.message = "FORM.FIELD.VALIDATOR.NOT_GREATER_THAN";
            field.validationSummary.attributes.set('maxValue', field.maxValue.toLocaleString());
            return false;
        }
        return true;
    };
    return MaxValueFieldValidator;
}());
export { MaxValueFieldValidator };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MaxValueFieldValidator.prototype.supportedTypes;
}
var RegExFieldValidator = /** @class */ (function () {
    function RegExFieldValidator() {
        this.supportedTypes = [
            FormFieldTypes.TEXT,
            FormFieldTypes.MULTILINE_TEXT
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    RegExFieldValidator.prototype.isSupported = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field &&
            this.supportedTypes.indexOf(field.type) > -1 && !!field.regexPattern;
    };
    /**
     * @param {?} field
     * @return {?}
     */
    RegExFieldValidator.prototype.validate = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        if (this.isSupported(field) && field.value && field.isVisible) {
            if (field.value.length > 0 && field.value.match(new RegExp('^' + field.regexPattern + '$'))) {
                return true;
            }
            field.validationSummary.message = 'FORM.FIELD.VALIDATOR.INVALID_VALUE';
            return false;
        }
        return true;
    };
    return RegExFieldValidator;
}());
export { RegExFieldValidator };
if (false) {
    /**
     * @type {?}
     * @private
     */
    RegExFieldValidator.prototype.supportedTypes;
}
var FixedValueFieldValidator = /** @class */ (function () {
    function FixedValueFieldValidator() {
        this.supportedTypes = [
            FormFieldTypes.TYPEAHEAD
        ];
    }
    /**
     * @param {?} field
     * @return {?}
     */
    FixedValueFieldValidator.prototype.isSupported = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field && this.supportedTypes.indexOf(field.type) > -1;
    };
    /**
     * @param {?} field
     * @return {?}
     */
    FixedValueFieldValidator.prototype.hasValidNameOrValidId = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return this.hasValidName(field) || this.hasValidId(field);
    };
    /**
     * @param {?} field
     * @return {?}
     */
    FixedValueFieldValidator.prototype.hasValidName = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field.options.find((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.name && item.name.toLocaleLowerCase() === field.value.toLocaleLowerCase(); })) ? true : false;
    };
    /**
     * @param {?} field
     * @return {?}
     */
    FixedValueFieldValidator.prototype.hasValidId = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field.options.find((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.id === field.value; })) ? true : false;
    };
    /**
     * @param {?} field
     * @return {?}
     */
    FixedValueFieldValidator.prototype.hasStringValue = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field.value && typeof field.value === 'string';
    };
    /**
     * @param {?} field
     * @return {?}
     */
    FixedValueFieldValidator.prototype.hasOptions = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return field.options && field.options.length > 0;
    };
    /**
     * @param {?} field
     * @return {?}
     */
    FixedValueFieldValidator.prototype.validate = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        if (this.isSupported(field) && field.isVisible) {
            if (this.hasStringValue(field) && this.hasOptions(field) && !this.hasValidNameOrValidId(field)) {
                field.validationSummary.message = 'FORM.FIELD.VALIDATOR.INVALID_VALUE';
                return false;
            }
        }
        return true;
    };
    return FixedValueFieldValidator;
}());
export { FixedValueFieldValidator };
if (false) {
    /**
     * @type {?}
     * @private
     */
    FixedValueFieldValidator.prototype.supportedTypes;
}
/** @type {?} */
export var FORM_FIELD_VALIDATORS = [
    new RequiredFieldValidator(),
    new NumberFieldValidator(),
    new MinLengthFieldValidator(),
    new MaxLengthFieldValidator(),
    new MinValueFieldValidator(),
    new MaxValueFieldValidator(),
    new RegExFieldValidator(),
    new DateFieldValidator(),
    new MinDateFieldValidator(),
    new MaxDateFieldValidator(),
    new FixedValueFieldValidator(),
    new MinDateTimeFieldValidator(),
    new MaxDateTimeFieldValidator()
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC12YWxpZGF0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9jb3JlL2Zvcm0tZmllbGQtdmFsaWRhdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLE1BQU0sTUFBTSxZQUFZLENBQUM7QUFDaEMsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDOzs7O0FBR3BELHdDQU1DOzs7Ozs7SUFKRyxnRUFBNEM7Ozs7O0lBRTVDLDZEQUF5Qzs7QUFJN0M7SUFBQTtRQUVZLG1CQUFjLEdBQUc7WUFDckIsY0FBYyxDQUFDLElBQUk7WUFDbkIsY0FBYyxDQUFDLGNBQWM7WUFDN0IsY0FBYyxDQUFDLE1BQU07WUFDckIsY0FBYyxDQUFDLE9BQU87WUFDdEIsY0FBYyxDQUFDLFNBQVM7WUFDeEIsY0FBYyxDQUFDLFFBQVE7WUFDdkIsY0FBYyxDQUFDLE1BQU07WUFDckIsY0FBYyxDQUFDLGdCQUFnQjtZQUMvQixjQUFjLENBQUMsYUFBYTtZQUM1QixjQUFjLENBQUMsTUFBTTtZQUNyQixjQUFjLENBQUMsTUFBTTtZQUNyQixjQUFjLENBQUMsYUFBYTtZQUM1QixjQUFjLENBQUMsSUFBSTtZQUNuQixjQUFjLENBQUMsUUFBUTtZQUN2QixjQUFjLENBQUMsYUFBYTtTQUMvQixDQUFDO0lBMkNOLENBQUM7Ozs7O0lBekNHLDRDQUFXOzs7O0lBQVgsVUFBWSxLQUFxQjtRQUM3QixPQUFPLEtBQUs7WUFDUixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzVDLEtBQUssQ0FBQyxRQUFRLENBQUM7SUFDdkIsQ0FBQzs7Ozs7SUFFRCx5Q0FBUTs7OztJQUFSLFVBQVMsS0FBcUI7UUFDMUIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFFNUMsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3hDLElBQUksS0FBSyxDQUFDLGFBQWEsSUFBSSxLQUFLLENBQUMsV0FBVyxFQUFFO29CQUMxQyxJQUFJLEtBQUssQ0FBQyxLQUFLLEtBQUssS0FBSyxDQUFDLFdBQVcsQ0FBQyxFQUFFLEVBQUU7d0JBQ3RDLE9BQU8sS0FBSyxDQUFDO3FCQUNoQjtpQkFDSjthQUNKO1lBRUQsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxhQUFhLEVBQUU7O29CQUN2QyxNQUFNLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJOzs7O2dCQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsR0FBRyxDQUFDLEVBQUUsS0FBSyxLQUFLLENBQUMsS0FBSyxFQUF0QixDQUFzQixFQUFDO2dCQUNsRSxPQUFPLENBQUMsQ0FBQyxNQUFNLENBQUM7YUFDbkI7WUFFRCxJQUFJLEtBQUssQ0FBQyxJQUFJLEtBQUssY0FBYyxDQUFDLE1BQU0sRUFBRTtnQkFDdEMsT0FBTyxLQUFLLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQzthQUNoRDtZQUVELElBQUksS0FBSyxDQUFDLElBQUksS0FBSyxjQUFjLENBQUMsYUFBYSxFQUFFO2dCQUM3QyxPQUFPLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssWUFBWSxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDO2FBQ2hGO1lBRUQsSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxPQUFPLEVBQUU7Z0JBQ3ZDLE9BQU8sS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7YUFDckM7WUFFRCxJQUFJLEtBQUssQ0FBQyxLQUFLLEtBQUssSUFBSSxJQUFJLEtBQUssQ0FBQyxLQUFLLEtBQUssU0FBUyxJQUFJLEtBQUssQ0FBQyxLQUFLLEtBQUssRUFBRSxFQUFFO2dCQUN6RSxPQUFPLEtBQUssQ0FBQzthQUNoQjtTQUNKO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVMLDZCQUFDO0FBQUQsQ0FBQyxBQTdERCxJQTZEQzs7Ozs7OztJQTNERyxnREFnQkU7O0FBNkNOO0lBQUE7UUFFWSxtQkFBYyxHQUFHO1lBQ3JCLGNBQWMsQ0FBQyxNQUFNO1lBQ3JCLGNBQWMsQ0FBQyxNQUFNO1NBQ3hCLENBQUM7SUFrQ04sQ0FBQzs7Ozs7SUFoQ1UsNkJBQVE7Ozs7SUFBZixVQUFnQixLQUFVO1FBQ3RCLElBQUksS0FBSyxLQUFLLElBQUksSUFBSSxLQUFLLEtBQUssU0FBUyxJQUFJLEtBQUssS0FBSyxFQUFFLEVBQUU7WUFDdkQsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDMUIsQ0FBQzs7Ozs7SUFFRCwwQ0FBVzs7OztJQUFYLFVBQVksS0FBcUI7UUFDN0IsT0FBTyxLQUFLLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ2pFLENBQUM7Ozs7O0lBRUQsdUNBQVE7Ozs7SUFBUixVQUFTLEtBQXFCO1FBQzFCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsU0FBUyxFQUFFO1lBQzVDLElBQUksS0FBSyxDQUFDLEtBQUssS0FBSyxJQUFJO2dCQUNwQixLQUFLLENBQUMsS0FBSyxLQUFLLFNBQVM7Z0JBQ3pCLEtBQUssQ0FBQyxLQUFLLEtBQUssRUFBRSxFQUFFO2dCQUNwQixPQUFPLElBQUksQ0FBQzthQUNmOztnQkFDSyxRQUFRLEdBQUcsRUFBRSxHQUFHLEtBQUssQ0FBQyxLQUFLOztnQkFDN0IsT0FBTyxHQUFHLElBQUksTUFBTSxDQUFDLFNBQVMsQ0FBQztZQUNuQyxJQUFJLEtBQUssQ0FBQyxlQUFlLEVBQUU7Z0JBQ3ZCLE9BQU8sR0FBRyxJQUFJLE1BQU0sQ0FBQywyQkFBMkIsQ0FBQyxDQUFDO2FBQ3JEO1lBQ0QsSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxFQUFFO2dCQUN6QixPQUFPLElBQUksQ0FBQzthQUNmO1lBQ0QsS0FBSyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sR0FBRyxxQ0FBcUMsQ0FBQztZQUN4RSxPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFDTCwyQkFBQztBQUFELENBQUMsQUF2Q0QsSUF1Q0M7Ozs7Ozs7SUFyQ0csOENBR0U7O0FBb0NOO0lBQUE7UUFFWSxtQkFBYyxHQUFHO1lBQ3JCLGNBQWMsQ0FBQyxJQUFJO1NBQ3RCLENBQUM7SUEwQk4sQ0FBQztJQXhCRywrRkFBK0Y7Ozs7Ozs7SUFDeEYsOEJBQVc7Ozs7Ozs7SUFBbEIsVUFBbUIsU0FBaUIsRUFBRSxVQUErQjtRQUEvQiwyQkFBQSxFQUFBLHVCQUErQjtRQUNqRSxJQUFJLFNBQVMsRUFBRTs7Z0JBQ0wsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxTQUFTLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQztZQUM3QyxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUN0QjtRQUVELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7O0lBRUQsd0NBQVc7Ozs7SUFBWCxVQUFZLEtBQXFCO1FBQzdCLE9BQU8sS0FBSyxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUNqRSxDQUFDOzs7OztJQUVELHFDQUFROzs7O0lBQVIsVUFBUyxLQUFxQjtRQUMxQixJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsU0FBUyxFQUFFO1lBQzNELElBQUksa0JBQWtCLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLEVBQUU7Z0JBQ3RFLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7WUFDRCxLQUFLLENBQUMsaUJBQWlCLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQztZQUMxRCxPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFDTCx5QkFBQztBQUFELENBQUMsQUE5QkQsSUE4QkM7Ozs7Ozs7SUE1QkcsNENBRUU7O0FBNEJOO0lBQUE7UUFFWSxtQkFBYyxHQUFHO1lBQ3JCLGNBQWMsQ0FBQyxJQUFJO1NBQ3RCLENBQUM7SUF5Q04sQ0FBQzs7Ozs7SUF2Q0csMkNBQVc7Ozs7SUFBWCxVQUFZLEtBQXFCO1FBQzdCLE9BQU8sS0FBSztZQUNSLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztJQUN6RSxDQUFDOzs7OztJQUVELHdDQUFROzs7O0lBQVIsVUFBUyxLQUFxQjs7WUFDdEIsT0FBTyxHQUFHLElBQUk7UUFDbEIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLFNBQVMsRUFBRTs7Z0JBQ3JELFVBQVUsR0FBRyxLQUFLLENBQUMsaUJBQWlCO1lBRTFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsRUFBRTtnQkFDMUQsS0FBSyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sR0FBRyxtQ0FBbUMsQ0FBQztnQkFDdEUsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUNuQjtpQkFBTTtnQkFDSCxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUM7YUFDL0M7U0FDSjtRQUNELE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7Ozs7Ozs7SUFFTyx5Q0FBUzs7Ozs7O0lBQWpCLFVBQWtCLEtBQXFCLEVBQUUsVUFBa0I7O1lBQ2pELGVBQWUsR0FBRyxZQUFZOztZQUNoQyxPQUFPLEdBQUcsSUFBSTs7O1lBRWQsY0FBYztRQUNsQixJQUFJLE9BQU8sS0FBSyxDQUFDLEtBQUssS0FBSyxRQUFRLEVBQUU7WUFDakMsY0FBYyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsQ0FBQztTQUNsRTthQUFNO1lBQ0gsY0FBYyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7U0FDaEM7O1lBQ0ssR0FBRyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLGVBQWUsQ0FBQztRQUVuRCxJQUFJLGNBQWMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDOUIsS0FBSyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sR0FBRyxvQ0FBb0MsQ0FBQztZQUN2RSxLQUFLLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLENBQUM7WUFDNUcsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUNuQjtRQUNELE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7SUFDTCw0QkFBQztBQUFELENBQUMsQUE3Q0QsSUE2Q0M7Ozs7Ozs7SUEzQ0csK0NBRUU7O0FBMkNOO0lBQUE7UUFFSSxvQkFBZSxHQUFHLFlBQVksQ0FBQztRQUV2QixtQkFBYyxHQUFHO1lBQ3JCLGNBQWMsQ0FBQyxJQUFJO1NBQ3RCLENBQUM7SUFpQ04sQ0FBQzs7Ozs7SUEvQkcsMkNBQVc7Ozs7SUFBWCxVQUFZLEtBQXFCO1FBQzdCLE9BQU8sS0FBSztZQUNSLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztJQUN6RSxDQUFDOzs7OztJQUVELHdDQUFROzs7O0lBQVIsVUFBUyxLQUFxQjtRQUMxQixJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsU0FBUyxFQUFFOztnQkFDckQsVUFBVSxHQUFHLEtBQUssQ0FBQyxpQkFBaUI7WUFFMUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxFQUFFO2dCQUMxRCxLQUFLLENBQUMsaUJBQWlCLENBQUMsT0FBTyxHQUFHLG1DQUFtQyxDQUFDO2dCQUN0RSxPQUFPLEtBQUssQ0FBQzthQUNoQjs7O2dCQUdHLENBQUMsU0FBQTtZQUNMLElBQUksT0FBTyxLQUFLLENBQUMsS0FBSyxLQUFLLFFBQVEsRUFBRTtnQkFDakMsQ0FBQyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsQ0FBQzthQUNyRDtpQkFBTTtnQkFDSCxDQUFDLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQzthQUNuQjs7Z0JBQ0ssR0FBRyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxlQUFlLENBQUM7WUFFeEQsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNoQixLQUFLLENBQUMsaUJBQWlCLENBQUMsT0FBTyxHQUFHLHVDQUF1QyxDQUFDO2dCQUMxRSxLQUFLLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLENBQUM7Z0JBQzVHLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1NBQ0o7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBQ0wsNEJBQUM7QUFBRCxDQUFDLEFBdkNELElBdUNDOzs7O0lBckNHLGdEQUErQjs7Ozs7SUFFL0IsK0NBRUU7O0FBbUNOO0lBQUE7UUFFWSxtQkFBYyxHQUFHO1lBQ3JCLGNBQWMsQ0FBQyxRQUFRO1NBQzFCLENBQUM7UUFDRix3QkFBbUIsR0FBRyxxQkFBcUIsQ0FBQztJQXVDaEQsQ0FBQzs7Ozs7SUFyQ0csK0NBQVc7Ozs7SUFBWCxVQUFZLEtBQXFCO1FBQzdCLE9BQU8sS0FBSztZQUNSLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztJQUN6RSxDQUFDOzs7OztJQUVELDRDQUFROzs7O0lBQVIsVUFBUyxLQUFxQjs7WUFDdEIsT0FBTyxHQUFHLElBQUk7UUFDbEIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLFNBQVMsRUFBRTs7Z0JBQ3JELFVBQVUsR0FBRyxLQUFLLENBQUMsaUJBQWlCO1lBRTFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsRUFBRTtnQkFDMUQsS0FBSyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sR0FBRyxtQ0FBbUMsQ0FBQztnQkFDdEUsT0FBTyxHQUFHLEtBQUssQ0FBQzthQUNuQjtpQkFBTTtnQkFDSCxPQUFPLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsVUFBVSxDQUFDLENBQUM7YUFDbkQ7U0FDSjtRQUNELE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7Ozs7Ozs7SUFFTyxpREFBYTs7Ozs7O0lBQXJCLFVBQXNCLEtBQXFCLEVBQUUsVUFBa0I7O1lBQ3ZELE9BQU8sR0FBRyxJQUFJOztZQUNkLGNBQWM7UUFDbEIsSUFBSSxPQUFPLEtBQUssQ0FBQyxLQUFLLEtBQUssUUFBUSxFQUFFO1lBQ2pDLGNBQWMsR0FBRyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsQ0FBQztTQUNwRDthQUFNO1lBQ0gsY0FBYyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7U0FDaEM7O1lBQ0ssR0FBRyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztRQUU1RCxJQUFJLGNBQWMsQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDOUIsS0FBSyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sR0FBRyxvQ0FBb0MsQ0FBQztZQUN2RSxLQUFLLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsR0FBRyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDMUcsT0FBTyxHQUFHLEtBQUssQ0FBQztTQUNuQjtRQUNELE9BQU8sT0FBTyxDQUFDO0lBQ25CLENBQUM7SUFDTCxnQ0FBQztBQUFELENBQUMsQUE1Q0QsSUE0Q0M7Ozs7Ozs7SUExQ0csbURBRUU7O0lBQ0Ysd0RBQTRDOztBQXlDaEQ7SUFBQTtRQUVZLG1CQUFjLEdBQUc7WUFDckIsY0FBYyxDQUFDLFFBQVE7U0FDMUIsQ0FBQztRQUNGLHdCQUFtQixHQUFHLHFCQUFxQixDQUFDO0lBd0NoRCxDQUFDOzs7OztJQXRDRywrQ0FBVzs7OztJQUFYLFVBQVksS0FBcUI7UUFDN0IsT0FBTyxLQUFLO1lBQ1IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDO0lBQ3pFLENBQUM7Ozs7O0lBRUQsNENBQVE7Ozs7SUFBUixVQUFTLEtBQXFCOztZQUN0QixPQUFPLEdBQUcsSUFBSTtRQUNsQixJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsU0FBUyxFQUFFOztnQkFDckQsVUFBVSxHQUFHLEtBQUssQ0FBQyxpQkFBaUI7WUFFMUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxFQUFFO2dCQUMxRCxLQUFLLENBQUMsaUJBQWlCLENBQUMsT0FBTyxHQUFHLG1DQUFtQyxDQUFDO2dCQUN0RSxPQUFPLEdBQUcsS0FBSyxDQUFDO2FBQ25CO2lCQUFNO2dCQUNILE9BQU8sR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxVQUFVLENBQUMsQ0FBQzthQUNuRDtTQUNKO1FBQ0QsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQzs7Ozs7OztJQUVPLGlEQUFhOzs7Ozs7SUFBckIsVUFBc0IsS0FBcUIsRUFBRSxVQUFrQjs7WUFDdkQsT0FBTyxHQUFHLElBQUk7O1lBQ2QsY0FBYztRQUVsQixJQUFJLE9BQU8sS0FBSyxDQUFDLEtBQUssS0FBSyxRQUFRLEVBQUU7WUFDakMsY0FBYyxHQUFHLE1BQU0sQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxDQUFDO1NBQ3BEO2FBQU07WUFDSCxjQUFjLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQztTQUNoQzs7WUFDSyxHQUFHLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLG1CQUFtQixDQUFDO1FBRTVELElBQUksY0FBYyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUMsRUFBRTtZQUM3QixLQUFLLENBQUMsaUJBQWlCLENBQUMsT0FBTyxHQUFHLHVDQUF1QyxDQUFDO1lBQzFFLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxHQUFHLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUMxRyxPQUFPLEdBQUcsS0FBSyxDQUFDO1NBQ25CO1FBQ0QsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQztJQUNMLGdDQUFDO0FBQUQsQ0FBQyxBQTdDRCxJQTZDQzs7Ozs7OztJQTNDRyxtREFFRTs7SUFDRix3REFBNEM7O0FBMENoRDtJQUFBO1FBRVksbUJBQWMsR0FBRztZQUNyQixjQUFjLENBQUMsSUFBSTtZQUNuQixjQUFjLENBQUMsY0FBYztTQUNoQyxDQUFDO0lBbUJOLENBQUM7Ozs7O0lBakJHLDZDQUFXOzs7O0lBQVgsVUFBWSxLQUFxQjtRQUM3QixPQUFPLEtBQUs7WUFDUixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzVDLEtBQUssQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO0lBQzVCLENBQUM7Ozs7O0lBRUQsMENBQVE7Ozs7SUFBUixVQUFTLEtBQXFCO1FBQzFCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFDM0QsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sSUFBSSxLQUFLLENBQUMsU0FBUyxFQUFFO2dCQUN2QyxPQUFPLElBQUksQ0FBQzthQUNmO1lBQ0QsS0FBSyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sR0FBRyxvQ0FBb0MsQ0FBQztZQUN2RSxLQUFLLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxXQUFXLEVBQUUsS0FBSyxDQUFDLFNBQVMsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDO1lBQ3RGLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUNMLDhCQUFDO0FBQUQsQ0FBQyxBQXhCRCxJQXdCQzs7Ozs7OztJQXRCRyxpREFHRTs7QUFxQk47SUFBQTtRQUVZLG1CQUFjLEdBQUc7WUFDckIsY0FBYyxDQUFDLElBQUk7WUFDbkIsY0FBYyxDQUFDLGNBQWM7U0FDaEMsQ0FBQztJQW1CTixDQUFDOzs7OztJQWpCRyw2Q0FBVzs7OztJQUFYLFVBQVksS0FBcUI7UUFDN0IsT0FBTyxLQUFLO1lBQ1IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUM1QyxLQUFLLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQztJQUM1QixDQUFDOzs7OztJQUVELDBDQUFROzs7O0lBQVIsVUFBUyxLQUFxQjtRQUMxQixJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsU0FBUyxFQUFFO1lBQzNELElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksS0FBSyxDQUFDLFNBQVMsRUFBRTtnQkFDdkMsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUNELEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEdBQUcscUNBQXFDLENBQUM7WUFDeEUsS0FBSyxDQUFDLGlCQUFpQixDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsV0FBVyxFQUFFLEtBQUssQ0FBQyxTQUFTLENBQUMsY0FBYyxFQUFFLENBQUMsQ0FBQztZQUN0RixPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFDTCw4QkFBQztBQUFELENBQUMsQUF4QkQsSUF3QkM7Ozs7Ozs7SUF0QkcsaURBR0U7O0FBcUJOO0lBQUE7UUFFWSxtQkFBYyxHQUFHO1lBQ3JCLGNBQWMsQ0FBQyxNQUFNO1lBQ3JCLGNBQWMsQ0FBQyxNQUFNO1NBQ3hCLENBQUM7SUF1Qk4sQ0FBQzs7Ozs7SUFyQkcsNENBQVc7Ozs7SUFBWCxVQUFZLEtBQXFCO1FBQzdCLE9BQU8sS0FBSztZQUNSLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDNUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN0RCxDQUFDOzs7OztJQUVELHlDQUFROzs7O0lBQVIsVUFBUyxLQUFxQjtRQUMxQixJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsU0FBUyxFQUFFOztnQkFDckQsS0FBSyxHQUFXLENBQUMsS0FBSyxDQUFDLEtBQUs7O2dCQUM1QixRQUFRLEdBQVcsQ0FBQyxLQUFLLENBQUMsUUFBUTtZQUV4QyxJQUFJLEtBQUssSUFBSSxRQUFRLEVBQUU7Z0JBQ25CLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7WUFDRCxLQUFLLENBQUMsaUJBQWlCLENBQUMsT0FBTyxHQUFHLG9DQUFvQyxDQUFDO1lBQ3ZFLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLGNBQWMsRUFBRSxDQUFDLENBQUM7WUFDcEYsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBQ0wsNkJBQUM7QUFBRCxDQUFDLEFBNUJELElBNEJDOzs7Ozs7O0lBMUJHLGdEQUdFOztBQXlCTjtJQUFBO1FBRVksbUJBQWMsR0FBRztZQUNyQixjQUFjLENBQUMsTUFBTTtZQUNyQixjQUFjLENBQUMsTUFBTTtTQUN4QixDQUFDO0lBdUJOLENBQUM7Ozs7O0lBckJHLDRDQUFXOzs7O0lBQVgsVUFBWSxLQUFxQjtRQUM3QixPQUFPLEtBQUs7WUFDUixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzVDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDdEQsQ0FBQzs7Ozs7SUFFRCx5Q0FBUTs7OztJQUFSLFVBQVMsS0FBcUI7UUFDMUIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLFNBQVMsRUFBRTs7Z0JBQ3JELEtBQUssR0FBVyxDQUFDLEtBQUssQ0FBQyxLQUFLOztnQkFDNUIsUUFBUSxHQUFXLENBQUMsS0FBSyxDQUFDLFFBQVE7WUFFeEMsSUFBSSxLQUFLLElBQUksUUFBUSxFQUFFO2dCQUNuQixPQUFPLElBQUksQ0FBQzthQUNmO1lBQ0QsS0FBSyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sR0FBRyx1Q0FBdUMsQ0FBQztZQUMxRSxLQUFLLENBQUMsaUJBQWlCLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLFFBQVEsQ0FBQyxjQUFjLEVBQUUsQ0FBQyxDQUFDO1lBQ3BGLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUNMLDZCQUFDO0FBQUQsQ0FBQyxBQTVCRCxJQTRCQzs7Ozs7OztJQTFCRyxnREFHRTs7QUF5Qk47SUFBQTtRQUVZLG1CQUFjLEdBQUc7WUFDckIsY0FBYyxDQUFDLElBQUk7WUFDbkIsY0FBYyxDQUFDLGNBQWM7U0FDaEMsQ0FBQztJQWtCTixDQUFDOzs7OztJQWhCRyx5Q0FBVzs7OztJQUFYLFVBQVksS0FBcUI7UUFDN0IsT0FBTyxLQUFLO1lBQ1IsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDO0lBQzdFLENBQUM7Ozs7O0lBRUQsc0NBQVE7Ozs7SUFBUixVQUFTLEtBQXFCO1FBQzFCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLENBQUMsS0FBSyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFDM0QsSUFBSSxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxNQUFNLENBQUMsR0FBRyxHQUFHLEtBQUssQ0FBQyxZQUFZLEdBQUcsR0FBRyxDQUFDLENBQUMsRUFBRTtnQkFDekYsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUNELEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEdBQUcsb0NBQW9DLENBQUM7WUFDdkUsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUwsMEJBQUM7QUFBRCxDQUFDLEFBdkJELElBdUJDOzs7Ozs7O0lBckJHLDZDQUdFOztBQW9CTjtJQUFBO1FBRVksbUJBQWMsR0FBRztZQUNyQixjQUFjLENBQUMsU0FBUztTQUMzQixDQUFDO0lBbUNOLENBQUM7Ozs7O0lBakNHLDhDQUFXOzs7O0lBQVgsVUFBWSxLQUFxQjtRQUM3QixPQUFPLEtBQUssSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDakUsQ0FBQzs7Ozs7SUFFRCx3REFBcUI7Ozs7SUFBckIsVUFBc0IsS0FBcUI7UUFDdkMsT0FBTyxJQUFJLENBQUMsWUFBWSxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDOUQsQ0FBQzs7Ozs7SUFFRCwrQ0FBWTs7OztJQUFaLFVBQWEsS0FBcUI7UUFDOUIsT0FBTyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUk7Ozs7UUFBQyxVQUFDLElBQUksSUFBSyxPQUFBLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxLQUFLLEtBQUssQ0FBQyxLQUFLLENBQUMsaUJBQWlCLEVBQUUsRUFBOUUsQ0FBOEUsRUFBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUN2SSxDQUFDOzs7OztJQUVELDZDQUFVOzs7O0lBQVYsVUFBVyxLQUFxQjtRQUM1QixPQUFPLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSTs7OztRQUFDLFVBQUMsSUFBSSxJQUFLLE9BQUEsSUFBSSxDQUFDLEVBQUUsS0FBSyxLQUFLLENBQUMsS0FBSyxFQUF2QixDQUF1QixFQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ2hGLENBQUM7Ozs7O0lBRUQsaURBQWM7Ozs7SUFBZCxVQUFlLEtBQXFCO1FBQ2hDLE9BQU8sS0FBSyxDQUFDLEtBQUssSUFBSSxPQUFPLEtBQUssQ0FBQyxLQUFLLEtBQUssUUFBUSxDQUFDO0lBQzFELENBQUM7Ozs7O0lBRUQsNkNBQVU7Ozs7SUFBVixVQUFXLEtBQXFCO1FBQzVCLE9BQU8sS0FBSyxDQUFDLE9BQU8sSUFBSSxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7SUFDckQsQ0FBQzs7Ozs7SUFFRCwyQ0FBUTs7OztJQUFSLFVBQVMsS0FBcUI7UUFDMUIsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssQ0FBQyxTQUFTLEVBQUU7WUFDNUMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQzVGLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEdBQUcsb0NBQW9DLENBQUM7Z0JBQ3ZFLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1NBQ0o7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBQ0wsK0JBQUM7QUFBRCxDQUFDLEFBdkNELElBdUNDOzs7Ozs7O0lBckNHLGtEQUVFOzs7QUFxQ04sTUFBTSxLQUFPLHFCQUFxQixHQUFHO0lBQ2pDLElBQUksc0JBQXNCLEVBQUU7SUFDNUIsSUFBSSxvQkFBb0IsRUFBRTtJQUMxQixJQUFJLHVCQUF1QixFQUFFO0lBQzdCLElBQUksdUJBQXVCLEVBQUU7SUFDN0IsSUFBSSxzQkFBc0IsRUFBRTtJQUM1QixJQUFJLHNCQUFzQixFQUFFO0lBQzVCLElBQUksbUJBQW1CLEVBQUU7SUFDekIsSUFBSSxrQkFBa0IsRUFBRTtJQUN4QixJQUFJLHFCQUFxQixFQUFFO0lBQzNCLElBQUkscUJBQXFCLEVBQUU7SUFDM0IsSUFBSSx3QkFBd0IsRUFBRTtJQUM5QixJQUFJLHlCQUF5QixFQUFFO0lBQy9CLElBQUkseUJBQXlCLEVBQUU7Q0FDbEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IG1vbWVudCBmcm9tICdtb21lbnQtZXM2JztcclxuaW1wb3J0IHsgRm9ybUZpZWxkVHlwZXMgfSBmcm9tICcuL2Zvcm0tZmllbGQtdHlwZXMnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRNb2RlbCB9IGZyb20gJy4vZm9ybS1maWVsZC5tb2RlbCc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEZvcm1GaWVsZFZhbGlkYXRvciB7XHJcblxyXG4gICAgaXNTdXBwb3J0ZWQoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbjtcclxuXHJcbiAgICB2YWxpZGF0ZShmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuO1xyXG5cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIFJlcXVpcmVkRmllbGRWYWxpZGF0b3IgaW1wbGVtZW50cyBGb3JtRmllbGRWYWxpZGF0b3Ige1xyXG5cclxuICAgIHByaXZhdGUgc3VwcG9ydGVkVHlwZXMgPSBbXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuVEVYVCxcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5NVUxUSUxJTkVfVEVYVCxcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5OVU1CRVIsXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuQk9PTEVBTixcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5UWVBFQUhFQUQsXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuRFJPUERPV04sXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuUEVPUExFLFxyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLkZVTkNUSU9OQUxfR1JPVVAsXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuUkFESU9fQlVUVE9OUyxcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5VUExPQUQsXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuQU1PVU5ULFxyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLkRZTkFNSUNfVEFCTEUsXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuREFURSxcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5EQVRFVElNRSxcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5BVFRBQ0hfRk9MREVSXHJcbiAgICBdO1xyXG5cclxuICAgIGlzU3VwcG9ydGVkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmaWVsZCAmJlxyXG4gICAgICAgICAgICB0aGlzLnN1cHBvcnRlZFR5cGVzLmluZGV4T2YoZmllbGQudHlwZSkgPiAtMSAmJlxyXG4gICAgICAgICAgICBmaWVsZC5yZXF1aXJlZDtcclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZShmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodGhpcy5pc1N1cHBvcnRlZChmaWVsZCkgJiYgZmllbGQuaXNWaXNpYmxlKSB7XHJcblxyXG4gICAgICAgICAgICBpZiAoZmllbGQudHlwZSA9PT0gRm9ybUZpZWxkVHlwZXMuRFJPUERPV04pIHtcclxuICAgICAgICAgICAgICAgIGlmIChmaWVsZC5oYXNFbXB0eVZhbHVlICYmIGZpZWxkLmVtcHR5T3B0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGZpZWxkLnZhbHVlID09PSBmaWVsZC5lbXB0eU9wdGlvbi5pZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoZmllbGQudHlwZSA9PT0gRm9ybUZpZWxkVHlwZXMuUkFESU9fQlVUVE9OUykge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgb3B0aW9uID0gZmllbGQub3B0aW9ucy5maW5kKChvcHQpID0+IG9wdC5pZCA9PT0gZmllbGQudmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICEhb3B0aW9uO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoZmllbGQudHlwZSA9PT0gRm9ybUZpZWxkVHlwZXMuVVBMT0FEKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmllbGQudmFsdWUgJiYgZmllbGQudmFsdWUubGVuZ3RoID4gMDtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGZpZWxkLnR5cGUgPT09IEZvcm1GaWVsZFR5cGVzLkRZTkFNSUNfVEFCTEUpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmaWVsZC52YWx1ZSAmJiBmaWVsZC52YWx1ZSBpbnN0YW5jZW9mIEFycmF5ICYmIGZpZWxkLnZhbHVlLmxlbmd0aCA+IDA7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChmaWVsZC50eXBlID09PSBGb3JtRmllbGRUeXBlcy5CT09MRUFOKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmllbGQudmFsdWUgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChmaWVsZC52YWx1ZSA9PT0gbnVsbCB8fCBmaWVsZC52YWx1ZSA9PT0gdW5kZWZpbmVkIHx8IGZpZWxkLnZhbHVlID09PSAnJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIE51bWJlckZpZWxkVmFsaWRhdG9yIGltcGxlbWVudHMgRm9ybUZpZWxkVmFsaWRhdG9yIHtcclxuXHJcbiAgICBwcml2YXRlIHN1cHBvcnRlZFR5cGVzID0gW1xyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLk5VTUJFUixcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5BTU9VTlRcclxuICAgIF07XHJcblxyXG4gICAgc3RhdGljIGlzTnVtYmVyKHZhbHVlOiBhbnkpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodmFsdWUgPT09IG51bGwgfHwgdmFsdWUgPT09IHVuZGVmaW5lZCB8fCB2YWx1ZSA9PT0gJycpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuICFpc05hTigrdmFsdWUpO1xyXG4gICAgfVxyXG5cclxuICAgIGlzU3VwcG9ydGVkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmaWVsZCAmJiB0aGlzLnN1cHBvcnRlZFR5cGVzLmluZGV4T2YoZmllbGQudHlwZSkgPiAtMTtcclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZShmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodGhpcy5pc1N1cHBvcnRlZChmaWVsZCkgJiYgZmllbGQuaXNWaXNpYmxlKSB7XHJcbiAgICAgICAgICAgIGlmIChmaWVsZC52YWx1ZSA9PT0gbnVsbCB8fFxyXG4gICAgICAgICAgICAgICAgZmllbGQudmFsdWUgPT09IHVuZGVmaW5lZCB8fFxyXG4gICAgICAgICAgICAgICAgZmllbGQudmFsdWUgPT09ICcnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zdCB2YWx1ZVN0ciA9ICcnICsgZmllbGQudmFsdWU7XHJcbiAgICAgICAgICAgIGxldCBwYXR0ZXJuID0gbmV3IFJlZ0V4cCgvXi0/XFxkKyQvKTtcclxuICAgICAgICAgICAgaWYgKGZpZWxkLmVuYWJsZUZyYWN0aW9ucykge1xyXG4gICAgICAgICAgICAgICAgcGF0dGVybiA9IG5ldyBSZWdFeHAoL14tP1swLTldKyhcXC5bMC05XXsxLDJ9KT8kLyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHZhbHVlU3RyLm1hdGNoKHBhdHRlcm4pKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5tZXNzYWdlID0gJ0ZPUk0uRklFTEQuVkFMSURBVE9SLklOVkFMSURfTlVNQkVSJztcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIERhdGVGaWVsZFZhbGlkYXRvciBpbXBsZW1lbnRzIEZvcm1GaWVsZFZhbGlkYXRvciB7XHJcblxyXG4gICAgcHJpdmF0ZSBzdXBwb3J0ZWRUeXBlcyA9IFtcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5EQVRFXHJcbiAgICBdO1xyXG5cclxuICAgIC8vIFZhbGlkYXRlcyB0aGF0IHRoZSBpbnB1dCBzdHJpbmcgaXMgYSB2YWxpZCBkYXRlIGZvcm1hdHRlZCBhcyA8ZGF0ZUZvcm1hdD4gKGRlZmF1bHQgRC1NLVlZWVkpXHJcbiAgICBzdGF0aWMgaXNWYWxpZERhdGUoaW5wdXREYXRlOiBzdHJpbmcsIGRhdGVGb3JtYXQ6IHN0cmluZyA9ICdELU0tWVlZWScpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAoaW5wdXREYXRlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGQgPSBtb21lbnQoaW5wdXREYXRlLCBkYXRlRm9ybWF0LCB0cnVlKTtcclxuICAgICAgICAgICAgcmV0dXJuIGQuaXNWYWxpZCgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzU3VwcG9ydGVkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmaWVsZCAmJiB0aGlzLnN1cHBvcnRlZFR5cGVzLmluZGV4T2YoZmllbGQudHlwZSkgPiAtMTtcclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZShmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodGhpcy5pc1N1cHBvcnRlZChmaWVsZCkgJiYgZmllbGQudmFsdWUgJiYgZmllbGQuaXNWaXNpYmxlKSB7XHJcbiAgICAgICAgICAgIGlmIChEYXRlRmllbGRWYWxpZGF0b3IuaXNWYWxpZERhdGUoZmllbGQudmFsdWUsIGZpZWxkLmRhdGVEaXNwbGF5Rm9ybWF0KSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZmllbGQudmFsaWRhdGlvblN1bW1hcnkubWVzc2FnZSA9IGZpZWxkLmRhdGVEaXNwbGF5Rm9ybWF0O1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgTWluRGF0ZUZpZWxkVmFsaWRhdG9yIGltcGxlbWVudHMgRm9ybUZpZWxkVmFsaWRhdG9yIHtcclxuXHJcbiAgICBwcml2YXRlIHN1cHBvcnRlZFR5cGVzID0gW1xyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLkRBVEVcclxuICAgIF07XHJcblxyXG4gICAgaXNTdXBwb3J0ZWQoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGZpZWxkICYmXHJcbiAgICAgICAgICAgIHRoaXMuc3VwcG9ydGVkVHlwZXMuaW5kZXhPZihmaWVsZC50eXBlKSA+IC0xICYmICEhZmllbGQubWluVmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgdmFsaWRhdGUoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgbGV0IGlzVmFsaWQgPSB0cnVlO1xyXG4gICAgICAgIGlmICh0aGlzLmlzU3VwcG9ydGVkKGZpZWxkKSAmJiBmaWVsZC52YWx1ZSAmJiBmaWVsZC5pc1Zpc2libGUpIHtcclxuICAgICAgICAgICAgY29uc3QgZGF0ZUZvcm1hdCA9IGZpZWxkLmRhdGVEaXNwbGF5Rm9ybWF0O1xyXG5cclxuICAgICAgICAgICAgaWYgKCFEYXRlRmllbGRWYWxpZGF0b3IuaXNWYWxpZERhdGUoZmllbGQudmFsdWUsIGRhdGVGb3JtYXQpKSB7XHJcbiAgICAgICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5tZXNzYWdlID0gJ0ZPUk0uRklFTEQuVkFMSURBVE9SLklOVkFMSURfREFURSc7XHJcbiAgICAgICAgICAgICAgICBpc1ZhbGlkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBpc1ZhbGlkID0gdGhpcy5jaGVja0RhdGUoZmllbGQsIGRhdGVGb3JtYXQpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBpc1ZhbGlkO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgY2hlY2tEYXRlKGZpZWxkOiBGb3JtRmllbGRNb2RlbCwgZGF0ZUZvcm1hdDogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgY29uc3QgTUlOX0RBVEVfRk9STUFUID0gJ0RELU1NLVlZWVknO1xyXG4gICAgICAgIGxldCBpc1ZhbGlkID0gdHJ1ZTtcclxuICAgICAgICAvLyByZW1vdmUgdGltZSBhbmQgdGltZXpvbmUgaW5mb1xyXG4gICAgICAgIGxldCBmaWVsZFZhbHVlRGF0YTtcclxuICAgICAgICBpZiAodHlwZW9mIGZpZWxkLnZhbHVlID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICBmaWVsZFZhbHVlRGF0YSA9IG1vbWVudChmaWVsZC52YWx1ZS5zcGxpdCgnVCcpWzBdLCBkYXRlRm9ybWF0KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBmaWVsZFZhbHVlRGF0YSA9IGZpZWxkLnZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBtaW4gPSBtb21lbnQoZmllbGQubWluVmFsdWUsIE1JTl9EQVRFX0ZPUk1BVCk7XHJcblxyXG4gICAgICAgIGlmIChmaWVsZFZhbHVlRGF0YS5pc0JlZm9yZShtaW4pKSB7XHJcbiAgICAgICAgICAgIGZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5Lm1lc3NhZ2UgPSBgRk9STS5GSUVMRC5WQUxJREFUT1IuTk9UX0xFU1NfVEhBTmA7XHJcbiAgICAgICAgICAgIGZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5LmF0dHJpYnV0ZXMuc2V0KCdtaW5WYWx1ZScsIG1pbi5mb3JtYXQoZmllbGQuZGF0ZURpc3BsYXlGb3JtYXQpLnRvTG9jYWxlVXBwZXJDYXNlKCkpO1xyXG4gICAgICAgICAgICBpc1ZhbGlkID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBpc1ZhbGlkO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgTWF4RGF0ZUZpZWxkVmFsaWRhdG9yIGltcGxlbWVudHMgRm9ybUZpZWxkVmFsaWRhdG9yIHtcclxuXHJcbiAgICBNQVhfREFURV9GT1JNQVQgPSAnREQtTU0tWVlZWSc7XHJcblxyXG4gICAgcHJpdmF0ZSBzdXBwb3J0ZWRUeXBlcyA9IFtcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5EQVRFXHJcbiAgICBdO1xyXG5cclxuICAgIGlzU3VwcG9ydGVkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmaWVsZCAmJlxyXG4gICAgICAgICAgICB0aGlzLnN1cHBvcnRlZFR5cGVzLmluZGV4T2YoZmllbGQudHlwZSkgPiAtMSAmJiAhIWZpZWxkLm1heFZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIHZhbGlkYXRlKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLmlzU3VwcG9ydGVkKGZpZWxkKSAmJiBmaWVsZC52YWx1ZSAmJiBmaWVsZC5pc1Zpc2libGUpIHtcclxuICAgICAgICAgICAgY29uc3QgZGF0ZUZvcm1hdCA9IGZpZWxkLmRhdGVEaXNwbGF5Rm9ybWF0O1xyXG5cclxuICAgICAgICAgICAgaWYgKCFEYXRlRmllbGRWYWxpZGF0b3IuaXNWYWxpZERhdGUoZmllbGQudmFsdWUsIGRhdGVGb3JtYXQpKSB7XHJcbiAgICAgICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5tZXNzYWdlID0gJ0ZPUk0uRklFTEQuVkFMSURBVE9SLklOVkFMSURfREFURSc7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIC8vIHJlbW92ZSB0aW1lIGFuZCB0aW1lem9uZSBpbmZvXHJcbiAgICAgICAgICAgIGxldCBkO1xyXG4gICAgICAgICAgICBpZiAodHlwZW9mIGZpZWxkLnZhbHVlID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgZCA9IG1vbWVudChmaWVsZC52YWx1ZS5zcGxpdCgnVCcpWzBdLCBkYXRlRm9ybWF0KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGQgPSBmaWVsZC52YWx1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBjb25zdCBtYXggPSBtb21lbnQoZmllbGQubWF4VmFsdWUsIHRoaXMuTUFYX0RBVEVfRk9STUFUKTtcclxuXHJcbiAgICAgICAgICAgIGlmIChkLmlzQWZ0ZXIobWF4KSkge1xyXG4gICAgICAgICAgICAgICAgZmllbGQudmFsaWRhdGlvblN1bW1hcnkubWVzc2FnZSA9IGBGT1JNLkZJRUxELlZBTElEQVRPUi5OT1RfR1JFQVRFUl9USEFOYDtcclxuICAgICAgICAgICAgICAgIGZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5LmF0dHJpYnV0ZXMuc2V0KCdtYXhWYWx1ZScsIG1heC5mb3JtYXQoZmllbGQuZGF0ZURpc3BsYXlGb3JtYXQpLnRvTG9jYWxlVXBwZXJDYXNlKCkpO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgTWluRGF0ZVRpbWVGaWVsZFZhbGlkYXRvciBpbXBsZW1lbnRzIEZvcm1GaWVsZFZhbGlkYXRvciB7XHJcblxyXG4gICAgcHJpdmF0ZSBzdXBwb3J0ZWRUeXBlcyA9IFtcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5EQVRFVElNRVxyXG4gICAgXTtcclxuICAgIE1JTl9EQVRFVElNRV9GT1JNQVQgPSAnWVlZWS1NTS1ERCBoaDptbSBBWic7XHJcblxyXG4gICAgaXNTdXBwb3J0ZWQoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGZpZWxkICYmXHJcbiAgICAgICAgICAgIHRoaXMuc3VwcG9ydGVkVHlwZXMuaW5kZXhPZihmaWVsZC50eXBlKSA+IC0xICYmICEhZmllbGQubWluVmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgdmFsaWRhdGUoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgbGV0IGlzVmFsaWQgPSB0cnVlO1xyXG4gICAgICAgIGlmICh0aGlzLmlzU3VwcG9ydGVkKGZpZWxkKSAmJiBmaWVsZC52YWx1ZSAmJiBmaWVsZC5pc1Zpc2libGUpIHtcclxuICAgICAgICAgICAgY29uc3QgZGF0ZUZvcm1hdCA9IGZpZWxkLmRhdGVEaXNwbGF5Rm9ybWF0O1xyXG5cclxuICAgICAgICAgICAgaWYgKCFEYXRlRmllbGRWYWxpZGF0b3IuaXNWYWxpZERhdGUoZmllbGQudmFsdWUsIGRhdGVGb3JtYXQpKSB7XHJcbiAgICAgICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5tZXNzYWdlID0gJ0ZPUk0uRklFTEQuVkFMSURBVE9SLklOVkFMSURfREFURSc7XHJcbiAgICAgICAgICAgICAgICBpc1ZhbGlkID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBpc1ZhbGlkID0gdGhpcy5jaGVja0RhdGVUaW1lKGZpZWxkLCBkYXRlRm9ybWF0KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gaXNWYWxpZDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNoZWNrRGF0ZVRpbWUoZmllbGQ6IEZvcm1GaWVsZE1vZGVsLCBkYXRlRm9ybWF0OiBzdHJpbmcpOiBib29sZWFuIHtcclxuICAgICAgICBsZXQgaXNWYWxpZCA9IHRydWU7XHJcbiAgICAgICAgbGV0IGZpZWxkVmFsdWVEYXRlO1xyXG4gICAgICAgIGlmICh0eXBlb2YgZmllbGQudmFsdWUgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgIGZpZWxkVmFsdWVEYXRlID0gbW9tZW50KGZpZWxkLnZhbHVlLCBkYXRlRm9ybWF0KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBmaWVsZFZhbHVlRGF0ZSA9IGZpZWxkLnZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCBtaW4gPSBtb21lbnQoZmllbGQubWluVmFsdWUsIHRoaXMuTUlOX0RBVEVUSU1FX0ZPUk1BVCk7XHJcblxyXG4gICAgICAgIGlmIChmaWVsZFZhbHVlRGF0ZS5pc0JlZm9yZShtaW4pKSB7XHJcbiAgICAgICAgICAgIGZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5Lm1lc3NhZ2UgPSBgRk9STS5GSUVMRC5WQUxJREFUT1IuTk9UX0xFU1NfVEhBTmA7XHJcbiAgICAgICAgICAgIGZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5LmF0dHJpYnV0ZXMuc2V0KCdtaW5WYWx1ZScsIG1pbi5mb3JtYXQoZmllbGQuZGF0ZURpc3BsYXlGb3JtYXQpLnJlcGxhY2UoJzonLCAnLScpKTtcclxuICAgICAgICAgICAgaXNWYWxpZCA9IGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gaXNWYWxpZDtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIE1heERhdGVUaW1lRmllbGRWYWxpZGF0b3IgaW1wbGVtZW50cyBGb3JtRmllbGRWYWxpZGF0b3Ige1xyXG5cclxuICAgIHByaXZhdGUgc3VwcG9ydGVkVHlwZXMgPSBbXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuREFURVRJTUVcclxuICAgIF07XHJcbiAgICBNQVhfREFURVRJTUVfRk9STUFUID0gJ1lZWVktTU0tREQgaGg6bW0gQVonO1xyXG5cclxuICAgIGlzU3VwcG9ydGVkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmaWVsZCAmJlxyXG4gICAgICAgICAgICB0aGlzLnN1cHBvcnRlZFR5cGVzLmluZGV4T2YoZmllbGQudHlwZSkgPiAtMSAmJiAhIWZpZWxkLm1heFZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIHZhbGlkYXRlKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGxldCBpc1ZhbGlkID0gdHJ1ZTtcclxuICAgICAgICBpZiAodGhpcy5pc1N1cHBvcnRlZChmaWVsZCkgJiYgZmllbGQudmFsdWUgJiYgZmllbGQuaXNWaXNpYmxlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGRhdGVGb3JtYXQgPSBmaWVsZC5kYXRlRGlzcGxheUZvcm1hdDtcclxuXHJcbiAgICAgICAgICAgIGlmICghRGF0ZUZpZWxkVmFsaWRhdG9yLmlzVmFsaWREYXRlKGZpZWxkLnZhbHVlLCBkYXRlRm9ybWF0KSkge1xyXG4gICAgICAgICAgICAgICAgZmllbGQudmFsaWRhdGlvblN1bW1hcnkubWVzc2FnZSA9ICdGT1JNLkZJRUxELlZBTElEQVRPUi5JTlZBTElEX0RBVEUnO1xyXG4gICAgICAgICAgICAgICAgaXNWYWxpZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaXNWYWxpZCA9IHRoaXMuY2hlY2tEYXRlVGltZShmaWVsZCwgZGF0ZUZvcm1hdCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGlzVmFsaWQ7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBjaGVja0RhdGVUaW1lKGZpZWxkOiBGb3JtRmllbGRNb2RlbCwgZGF0ZUZvcm1hdDogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgbGV0IGlzVmFsaWQgPSB0cnVlO1xyXG4gICAgICAgIGxldCBmaWVsZFZhbHVlRGF0ZTtcclxuXHJcbiAgICAgICAgaWYgKHR5cGVvZiBmaWVsZC52YWx1ZSA9PT0gJ3N0cmluZycpIHtcclxuICAgICAgICAgICAgZmllbGRWYWx1ZURhdGUgPSBtb21lbnQoZmllbGQudmFsdWUsIGRhdGVGb3JtYXQpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGZpZWxkVmFsdWVEYXRlID0gZmllbGQudmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IG1heCA9IG1vbWVudChmaWVsZC5tYXhWYWx1ZSwgdGhpcy5NQVhfREFURVRJTUVfRk9STUFUKTtcclxuXHJcbiAgICAgICAgaWYgKGZpZWxkVmFsdWVEYXRlLmlzQWZ0ZXIobWF4KSkge1xyXG4gICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5tZXNzYWdlID0gYEZPUk0uRklFTEQuVkFMSURBVE9SLk5PVF9HUkVBVEVSX1RIQU5gO1xyXG4gICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5hdHRyaWJ1dGVzLnNldCgnbWF4VmFsdWUnLCBtYXguZm9ybWF0KGZpZWxkLmRhdGVEaXNwbGF5Rm9ybWF0KS5yZXBsYWNlKCc6JywgJy0nKSk7XHJcbiAgICAgICAgICAgIGlzVmFsaWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGlzVmFsaWQ7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBNaW5MZW5ndGhGaWVsZFZhbGlkYXRvciBpbXBsZW1lbnRzIEZvcm1GaWVsZFZhbGlkYXRvciB7XHJcblxyXG4gICAgcHJpdmF0ZSBzdXBwb3J0ZWRUeXBlcyA9IFtcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5URVhULFxyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLk1VTFRJTElORV9URVhUXHJcbiAgICBdO1xyXG5cclxuICAgIGlzU3VwcG9ydGVkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmaWVsZCAmJlxyXG4gICAgICAgICAgICB0aGlzLnN1cHBvcnRlZFR5cGVzLmluZGV4T2YoZmllbGQudHlwZSkgPiAtMSAmJlxyXG4gICAgICAgICAgICBmaWVsZC5taW5MZW5ndGggPiAwO1xyXG4gICAgfVxyXG5cclxuICAgIHZhbGlkYXRlKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLmlzU3VwcG9ydGVkKGZpZWxkKSAmJiBmaWVsZC52YWx1ZSAmJiBmaWVsZC5pc1Zpc2libGUpIHtcclxuICAgICAgICAgICAgaWYgKGZpZWxkLnZhbHVlLmxlbmd0aCA+PSBmaWVsZC5taW5MZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5Lm1lc3NhZ2UgPSBgRk9STS5GSUVMRC5WQUxJREFUT1IuQVRfTEVBU1RfTE9OR2A7XHJcbiAgICAgICAgICAgIGZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5LmF0dHJpYnV0ZXMuc2V0KCdtaW5MZW5ndGgnLCBmaWVsZC5taW5MZW5ndGgudG9Mb2NhbGVTdHJpbmcoKSk7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBNYXhMZW5ndGhGaWVsZFZhbGlkYXRvciBpbXBsZW1lbnRzIEZvcm1GaWVsZFZhbGlkYXRvciB7XHJcblxyXG4gICAgcHJpdmF0ZSBzdXBwb3J0ZWRUeXBlcyA9IFtcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5URVhULFxyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLk1VTFRJTElORV9URVhUXHJcbiAgICBdO1xyXG5cclxuICAgIGlzU3VwcG9ydGVkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmaWVsZCAmJlxyXG4gICAgICAgICAgICB0aGlzLnN1cHBvcnRlZFR5cGVzLmluZGV4T2YoZmllbGQudHlwZSkgPiAtMSAmJlxyXG4gICAgICAgICAgICBmaWVsZC5tYXhMZW5ndGggPiAwO1xyXG4gICAgfVxyXG5cclxuICAgIHZhbGlkYXRlKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLmlzU3VwcG9ydGVkKGZpZWxkKSAmJiBmaWVsZC52YWx1ZSAmJiBmaWVsZC5pc1Zpc2libGUpIHtcclxuICAgICAgICAgICAgaWYgKGZpZWxkLnZhbHVlLmxlbmd0aCA8PSBmaWVsZC5tYXhMZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5Lm1lc3NhZ2UgPSBgRk9STS5GSUVMRC5WQUxJREFUT1IuTk9fTE9OR0VSX1RIQU5gO1xyXG4gICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5hdHRyaWJ1dGVzLnNldCgnbWF4TGVuZ3RoJywgZmllbGQubWF4TGVuZ3RoLnRvTG9jYWxlU3RyaW5nKCkpO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgTWluVmFsdWVGaWVsZFZhbGlkYXRvciBpbXBsZW1lbnRzIEZvcm1GaWVsZFZhbGlkYXRvciB7XHJcblxyXG4gICAgcHJpdmF0ZSBzdXBwb3J0ZWRUeXBlcyA9IFtcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5OVU1CRVIsXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuQU1PVU5UXHJcbiAgICBdO1xyXG5cclxuICAgIGlzU3VwcG9ydGVkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBmaWVsZCAmJlxyXG4gICAgICAgICAgICB0aGlzLnN1cHBvcnRlZFR5cGVzLmluZGV4T2YoZmllbGQudHlwZSkgPiAtMSAmJlxyXG4gICAgICAgICAgICBOdW1iZXJGaWVsZFZhbGlkYXRvci5pc051bWJlcihmaWVsZC5taW5WYWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgdmFsaWRhdGUoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNTdXBwb3J0ZWQoZmllbGQpICYmIGZpZWxkLnZhbHVlICYmIGZpZWxkLmlzVmlzaWJsZSkge1xyXG4gICAgICAgICAgICBjb25zdCB2YWx1ZTogbnVtYmVyID0gK2ZpZWxkLnZhbHVlO1xyXG4gICAgICAgICAgICBjb25zdCBtaW5WYWx1ZTogbnVtYmVyID0gK2ZpZWxkLm1pblZhbHVlO1xyXG5cclxuICAgICAgICAgICAgaWYgKHZhbHVlID49IG1pblZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5tZXNzYWdlID0gYEZPUk0uRklFTEQuVkFMSURBVE9SLk5PVF9MRVNTX1RIQU5gO1xyXG4gICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5hdHRyaWJ1dGVzLnNldCgnbWluVmFsdWUnLCBmaWVsZC5taW5WYWx1ZS50b0xvY2FsZVN0cmluZygpKTtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbn1cclxuXHJcbmV4cG9ydCBjbGFzcyBNYXhWYWx1ZUZpZWxkVmFsaWRhdG9yIGltcGxlbWVudHMgRm9ybUZpZWxkVmFsaWRhdG9yIHtcclxuXHJcbiAgICBwcml2YXRlIHN1cHBvcnRlZFR5cGVzID0gW1xyXG4gICAgICAgIEZvcm1GaWVsZFR5cGVzLk5VTUJFUixcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5BTU9VTlRcclxuICAgIF07XHJcblxyXG4gICAgaXNTdXBwb3J0ZWQoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGZpZWxkICYmXHJcbiAgICAgICAgICAgIHRoaXMuc3VwcG9ydGVkVHlwZXMuaW5kZXhPZihmaWVsZC50eXBlKSA+IC0xICYmXHJcbiAgICAgICAgICAgIE51bWJlckZpZWxkVmFsaWRhdG9yLmlzTnVtYmVyKGZpZWxkLm1heFZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZShmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodGhpcy5pc1N1cHBvcnRlZChmaWVsZCkgJiYgZmllbGQudmFsdWUgJiYgZmllbGQuaXNWaXNpYmxlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHZhbHVlOiBudW1iZXIgPSArZmllbGQudmFsdWU7XHJcbiAgICAgICAgICAgIGNvbnN0IG1heFZhbHVlOiBudW1iZXIgPSArZmllbGQubWF4VmFsdWU7XHJcblxyXG4gICAgICAgICAgICBpZiAodmFsdWUgPD0gbWF4VmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5Lm1lc3NhZ2UgPSBgRk9STS5GSUVMRC5WQUxJREFUT1IuTk9UX0dSRUFURVJfVEhBTmA7XHJcbiAgICAgICAgICAgIGZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5LmF0dHJpYnV0ZXMuc2V0KCdtYXhWYWx1ZScsIGZpZWxkLm1heFZhbHVlLnRvTG9jYWxlU3RyaW5nKCkpO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIFJlZ0V4RmllbGRWYWxpZGF0b3IgaW1wbGVtZW50cyBGb3JtRmllbGRWYWxpZGF0b3Ige1xyXG5cclxuICAgIHByaXZhdGUgc3VwcG9ydGVkVHlwZXMgPSBbXHJcbiAgICAgICAgRm9ybUZpZWxkVHlwZXMuVEVYVCxcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5NVUxUSUxJTkVfVEVYVFxyXG4gICAgXTtcclxuXHJcbiAgICBpc1N1cHBvcnRlZChmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gZmllbGQgJiZcclxuICAgICAgICAgICAgdGhpcy5zdXBwb3J0ZWRUeXBlcy5pbmRleE9mKGZpZWxkLnR5cGUpID4gLTEgJiYgISFmaWVsZC5yZWdleFBhdHRlcm47XHJcbiAgICB9XHJcblxyXG4gICAgdmFsaWRhdGUoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNTdXBwb3J0ZWQoZmllbGQpICYmIGZpZWxkLnZhbHVlICYmIGZpZWxkLmlzVmlzaWJsZSkge1xyXG4gICAgICAgICAgICBpZiAoZmllbGQudmFsdWUubGVuZ3RoID4gMCAmJiBmaWVsZC52YWx1ZS5tYXRjaChuZXcgUmVnRXhwKCdeJyArIGZpZWxkLnJlZ2V4UGF0dGVybiArICckJykpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5tZXNzYWdlID0gJ0ZPUk0uRklFTEQuVkFMSURBVE9SLklOVkFMSURfVkFMVUUnO1xyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG5cclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIEZpeGVkVmFsdWVGaWVsZFZhbGlkYXRvciBpbXBsZW1lbnRzIEZvcm1GaWVsZFZhbGlkYXRvciB7XHJcblxyXG4gICAgcHJpdmF0ZSBzdXBwb3J0ZWRUeXBlcyA9IFtcclxuICAgICAgICBGb3JtRmllbGRUeXBlcy5UWVBFQUhFQURcclxuICAgIF07XHJcblxyXG4gICAgaXNTdXBwb3J0ZWQoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGZpZWxkICYmIHRoaXMuc3VwcG9ydGVkVHlwZXMuaW5kZXhPZihmaWVsZC50eXBlKSA+IC0xO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc1ZhbGlkTmFtZU9yVmFsaWRJZChmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5oYXNWYWxpZE5hbWUoZmllbGQpIHx8IHRoaXMuaGFzVmFsaWRJZChmaWVsZCk7XHJcbiAgICB9XHJcblxyXG4gICAgaGFzVmFsaWROYW1lKGZpZWxkOiBGb3JtRmllbGRNb2RlbCkge1xyXG4gICAgICAgIHJldHVybiBmaWVsZC5vcHRpb25zLmZpbmQoKGl0ZW0pID0+IGl0ZW0ubmFtZSAmJiBpdGVtLm5hbWUudG9Mb2NhbGVMb3dlckNhc2UoKSA9PT0gZmllbGQudmFsdWUudG9Mb2NhbGVMb3dlckNhc2UoKSkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaGFzVmFsaWRJZChmaWVsZDogRm9ybUZpZWxkTW9kZWwpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gZmllbGQub3B0aW9ucy5maW5kKChpdGVtKSA9PiBpdGVtLmlkID09PSBmaWVsZC52YWx1ZSkgPyB0cnVlIDogZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgaGFzU3RyaW5nVmFsdWUoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKSB7XHJcbiAgICAgICAgcmV0dXJuIGZpZWxkLnZhbHVlICYmIHR5cGVvZiBmaWVsZC52YWx1ZSA9PT0gJ3N0cmluZyc7XHJcbiAgICB9XHJcblxyXG4gICAgaGFzT3B0aW9ucyhmaWVsZDogRm9ybUZpZWxkTW9kZWwpIHtcclxuICAgICAgICByZXR1cm4gZmllbGQub3B0aW9ucyAmJiBmaWVsZC5vcHRpb25zLmxlbmd0aCA+IDA7XHJcbiAgICB9XHJcblxyXG4gICAgdmFsaWRhdGUoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNTdXBwb3J0ZWQoZmllbGQpICYmIGZpZWxkLmlzVmlzaWJsZSkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5oYXNTdHJpbmdWYWx1ZShmaWVsZCkgJiYgdGhpcy5oYXNPcHRpb25zKGZpZWxkKSAmJiAhdGhpcy5oYXNWYWxpZE5hbWVPclZhbGlkSWQoZmllbGQpKSB7XHJcbiAgICAgICAgICAgICAgICBmaWVsZC52YWxpZGF0aW9uU3VtbWFyeS5tZXNzYWdlID0gJ0ZPUk0uRklFTEQuVkFMSURBVE9SLklOVkFMSURfVkFMVUUnO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgY29uc3QgRk9STV9GSUVMRF9WQUxJREFUT1JTID0gW1xyXG4gICAgbmV3IFJlcXVpcmVkRmllbGRWYWxpZGF0b3IoKSxcclxuICAgIG5ldyBOdW1iZXJGaWVsZFZhbGlkYXRvcigpLFxyXG4gICAgbmV3IE1pbkxlbmd0aEZpZWxkVmFsaWRhdG9yKCksXHJcbiAgICBuZXcgTWF4TGVuZ3RoRmllbGRWYWxpZGF0b3IoKSxcclxuICAgIG5ldyBNaW5WYWx1ZUZpZWxkVmFsaWRhdG9yKCksXHJcbiAgICBuZXcgTWF4VmFsdWVGaWVsZFZhbGlkYXRvcigpLFxyXG4gICAgbmV3IFJlZ0V4RmllbGRWYWxpZGF0b3IoKSxcclxuICAgIG5ldyBEYXRlRmllbGRWYWxpZGF0b3IoKSxcclxuICAgIG5ldyBNaW5EYXRlRmllbGRWYWxpZGF0b3IoKSxcclxuICAgIG5ldyBNYXhEYXRlRmllbGRWYWxpZGF0b3IoKSxcclxuICAgIG5ldyBGaXhlZFZhbHVlRmllbGRWYWxpZGF0b3IoKSxcclxuICAgIG5ldyBNaW5EYXRlVGltZUZpZWxkVmFsaWRhdG9yKCksXHJcbiAgICBuZXcgTWF4RGF0ZVRpbWVGaWVsZFZhbGlkYXRvcigpXHJcbl07XHJcbiJdfQ==