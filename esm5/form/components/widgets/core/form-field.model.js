/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import moment from 'moment-es6';
import { WidgetVisibilityModel } from '../../../models/widget-visibility.model';
import { ContainerColumnModel } from './container-column.model';
import { ErrorMessageModel } from './error-message.model';
import { FormFieldTypes } from './form-field-types';
import { NumberFieldValidator } from './form-field-validator';
import { FormWidgetModel } from './form-widget.model';
// Maps to FormFieldRepresentation
var 
// Maps to FormFieldRepresentation
FormFieldModel = /** @class */ (function (_super) {
    tslib_1.__extends(FormFieldModel, _super);
    function FormFieldModel(form, json) {
        var _this = _super.call(this, form, json) || this;
        _this._readOnly = false;
        _this._isValid = true;
        _this._required = false;
        _this.defaultDateFormat = 'D-M-YYYY';
        _this.defaultDateTimeFormat = 'D-M-YYYY hh:mm A';
        _this.rowspan = 1;
        _this.colspan = 1;
        _this.placeholder = null;
        _this.minLength = 0;
        _this.maxLength = 0;
        _this.options = [];
        _this.params = {};
        _this.isVisible = true;
        _this.visibilityCondition = null;
        _this.enableFractions = false;
        _this.currency = null;
        _this.dateDisplayFormat = _this.defaultDateFormat;
        // container model members
        _this.numberOfColumns = 1;
        _this.fields = [];
        _this.columns = [];
        if (json) {
            _this.fieldType = json.fieldType;
            _this.id = json.id;
            _this.name = json.name;
            _this.type = json.type;
            _this._required = (/** @type {?} */ (json.required));
            _this._readOnly = (/** @type {?} */ (json.readOnly)) || json.type === 'readonly';
            _this.overrideId = (/** @type {?} */ (json.overrideId));
            _this.tab = json.tab;
            _this.restUrl = json.restUrl;
            _this.restResponsePath = json.restResponsePath;
            _this.restIdProperty = json.restIdProperty;
            _this.restLabelProperty = json.restLabelProperty;
            _this.colspan = (/** @type {?} */ (json.colspan));
            _this.minLength = (/** @type {?} */ (json.minLength)) || 0;
            _this.maxLength = (/** @type {?} */ (json.maxLength)) || 0;
            _this.minValue = json.minValue;
            _this.maxValue = json.maxValue;
            _this.regexPattern = json.regexPattern;
            _this.options = (/** @type {?} */ (json.options)) || [];
            _this.hasEmptyValue = (/** @type {?} */ (json.hasEmptyValue));
            _this.className = json.className;
            _this.optionType = json.optionType;
            _this.params = (/** @type {?} */ (json.params)) || {};
            _this.hyperlinkUrl = json.hyperlinkUrl;
            _this.displayText = json.displayText;
            _this.visibilityCondition = json.visibilityCondition ? new WidgetVisibilityModel(json.visibilityCondition) : undefined;
            _this.enableFractions = (/** @type {?} */ (json.enableFractions));
            _this.currency = json.currency;
            _this.dateDisplayFormat = json.dateDisplayFormat || _this.getDefaultDateFormat(json);
            _this._value = _this.parseValue(json);
            _this.validationSummary = new ErrorMessageModel();
            if (json.placeholder && json.placeholder !== '' && json.placeholder !== 'null') {
                _this.placeholder = json.placeholder;
            }
            if (FormFieldTypes.isReadOnlyType(json.type)) {
                if (json.params && json.params.field) {
                    if (form.processVariables) {
                        /** @type {?} */
                        var processVariable = _this.getProcessVariableValue(json.params.field, form);
                        if (processVariable) {
                            _this.value = processVariable;
                        }
                    }
                    else if (json.params.responseVariable && form.json.variables) {
                        /** @type {?} */
                        var formVariable = _this.getVariablesValue(json.params.field.name, form);
                        if (formVariable) {
                            _this.value = formVariable;
                        }
                    }
                }
            }
            if (FormFieldTypes.isContainerType(json.type)) {
                _this.containerFactory(json, form);
            }
        }
        if (_this.hasEmptyValue && _this.options && _this.options.length > 0) {
            _this.emptyOption = _this.options[0];
        }
        _this.updateForm();
        return _this;
    }
    Object.defineProperty(FormFieldModel.prototype, "value", {
        get: /**
         * @return {?}
         */
        function () {
            return this._value;
        },
        set: /**
         * @param {?} v
         * @return {?}
         */
        function (v) {
            this._value = v;
            this.updateForm();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormFieldModel.prototype, "readOnly", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.form && this.form.readOnly) {
                return true;
            }
            return this._readOnly;
        },
        set: /**
         * @param {?} readOnly
         * @return {?}
         */
        function (readOnly) {
            this._readOnly = readOnly;
            this.updateForm();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormFieldModel.prototype, "required", {
        get: /**
         * @return {?}
         */
        function () {
            return this._required;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._required = value;
            this.updateForm();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormFieldModel.prototype, "isValid", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isValid;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    FormFieldModel.prototype.markAsInvalid = /**
     * @return {?}
     */
    function () {
        this._isValid = false;
    };
    /**
     * @return {?}
     */
    FormFieldModel.prototype.validate = /**
     * @return {?}
     */
    function () {
        var e_1, _a;
        this.validationSummary = new ErrorMessageModel();
        if (!this.readOnly) {
            /** @type {?} */
            var validators = this.form.fieldValidators || [];
            try {
                for (var validators_1 = tslib_1.__values(validators), validators_1_1 = validators_1.next(); !validators_1_1.done; validators_1_1 = validators_1.next()) {
                    var validator = validators_1_1.value;
                    if (!validator.validate(this)) {
                        this._isValid = false;
                        return this._isValid;
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (validators_1_1 && !validators_1_1.done && (_a = validators_1.return)) _a.call(validators_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        this._isValid = true;
        return this._isValid;
    };
    /**
     * @private
     * @param {?} jsonField
     * @return {?}
     */
    FormFieldModel.prototype.getDefaultDateFormat = /**
     * @private
     * @param {?} jsonField
     * @return {?}
     */
    function (jsonField) {
        /** @type {?} */
        var originalType = jsonField.type;
        if (FormFieldTypes.isReadOnlyType(jsonField.type) &&
            jsonField.params &&
            jsonField.params.field) {
            originalType = jsonField.params.field.type;
        }
        return originalType === FormFieldTypes.DATETIME ? this.defaultDateTimeFormat : this.defaultDateFormat;
    };
    /**
     * @private
     * @param {?} type
     * @return {?}
     */
    FormFieldModel.prototype.isTypeaheadFieldType = /**
     * @private
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return type === 'typeahead' ? true : false;
    };
    /**
     * @private
     * @param {?} name
     * @return {?}
     */
    FormFieldModel.prototype.getFieldNameWithLabel = /**
     * @private
     * @param {?} name
     * @return {?}
     */
    function (name) {
        return name += '_LABEL';
    };
    /**
     * @private
     * @param {?} field
     * @param {?} form
     * @return {?}
     */
    FormFieldModel.prototype.getProcessVariableValue = /**
     * @private
     * @param {?} field
     * @param {?} form
     * @return {?}
     */
    function (field, form) {
        /** @type {?} */
        var fieldName = field.name;
        if (this.isTypeaheadFieldType(field.type)) {
            fieldName = this.getFieldNameWithLabel(field.id);
        }
        return this.findProcessVariableValue(fieldName, form);
    };
    /**
     * @private
     * @param {?} variableName
     * @param {?} form
     * @return {?}
     */
    FormFieldModel.prototype.getVariablesValue = /**
     * @private
     * @param {?} variableName
     * @param {?} form
     * @return {?}
     */
    function (variableName, form) {
        /** @type {?} */
        var variable = form.json.variables.find((/**
         * @param {?} currentVariable
         * @return {?}
         */
        function (currentVariable) {
            return currentVariable.name === variableName;
        }));
        if (variable) {
            if (variable.type === 'boolean') {
                return JSON.parse(variable.value);
            }
            return variable.value;
        }
        return null;
    };
    /**
     * @private
     * @param {?} variableName
     * @param {?} form
     * @return {?}
     */
    FormFieldModel.prototype.findProcessVariableValue = /**
     * @private
     * @param {?} variableName
     * @param {?} form
     * @return {?}
     */
    function (variableName, form) {
        if (form.processVariables) {
            /** @type {?} */
            var variable = form.processVariables.find((/**
             * @param {?} currentVariable
             * @return {?}
             */
            function (currentVariable) {
                return currentVariable.name === variableName;
            }));
            if (variable) {
                return variable.type === 'boolean' ? JSON.parse(variable.value) : variable.value;
            }
        }
        return undefined;
    };
    /**
     * @private
     * @param {?} json
     * @param {?} form
     * @return {?}
     */
    FormFieldModel.prototype.containerFactory = /**
     * @private
     * @param {?} json
     * @param {?} form
     * @return {?}
     */
    function (json, form) {
        var _this = this;
        this.numberOfColumns = (/** @type {?} */ (json.numberOfColumns)) || 1;
        this.fields = json.fields;
        this.rowspan = 1;
        this.colspan = 1;
        if (json.fields) {
            for (var currentField in json.fields) {
                if (json.fields.hasOwnProperty(currentField)) {
                    /** @type {?} */
                    var col = new ContainerColumnModel();
                    /** @type {?} */
                    var fields = (json.fields[currentField] || []).map((/**
                     * @param {?} f
                     * @return {?}
                     */
                    function (f) { return new FormFieldModel(form, f); }));
                    col.fields = fields;
                    col.rowspan = json.fields[currentField].length;
                    col.fields.forEach((/**
                     * @param {?} colFields
                     * @return {?}
                     */
                    function (colFields) {
                        _this.colspan = colFields.colspan > _this.colspan ? colFields.colspan : _this.colspan;
                    }));
                    this.rowspan = this.rowspan < col.rowspan ? col.rowspan : this.rowspan;
                    this.columns.push(col);
                }
            }
        }
    };
    /**
     * @param {?} json
     * @return {?}
     */
    FormFieldModel.prototype.parseValue = /**
     * @param {?} json
     * @return {?}
     */
    function (json) {
        /** @type {?} */
        var value = json.hasOwnProperty('value') ? json.value : null;
        /*
         This is needed due to Activiti issue related to reading dropdown values as value string
         but saving back as object: { id: <id>, name: <name> }
         */
        if (json.type === FormFieldTypes.DROPDOWN) {
            if (json.hasEmptyValue && json.options) {
                /** @type {?} */
                var options = (/** @type {?} */ (json.options)) || [];
                if (options.length > 0) {
                    /** @type {?} */
                    var emptyOption = json.options[0];
                    if (value === '' || value === emptyOption.id || value === emptyOption.name) {
                        value = emptyOption.id;
                    }
                    else if (value.id && value.name) {
                        value = value.id;
                    }
                }
            }
        }
        /*
         This is needed due to Activiti issue related to reading radio button values as value string
         but saving back as object: { id: <id>, name: <name> }
         */
        if (json.type === FormFieldTypes.RADIO_BUTTONS) {
            // Activiti has a bug with default radio button value where initial selection passed as `name` value
            // so try resolving current one with a fallback to first entry via name or id
            // TODO: needs to be reported and fixed at Activiti side
            /** @type {?} */
            var entry = this.options.filter((/**
             * @param {?} opt
             * @return {?}
             */
            function (opt) {
                return opt.id === value || opt.name === value || (value && (opt.id === value.id || opt.name === value.name));
            }));
            if (entry.length > 0) {
                value = entry[0].id;
            }
        }
        /*
         This is needed due to Activiti displaying/editing dates in d-M-YYYY format
         but storing on server in ISO8601 format (i.e. 2013-02-04T22:44:30.652Z)
         */
        if (this.isDateField(json) || this.isDateTimeField(json)) {
            if (value) {
                /** @type {?} */
                var dateValue = void 0;
                if (NumberFieldValidator.isNumber(value)) {
                    dateValue = moment(value);
                }
                else {
                    dateValue = this.isDateTimeField(json) ? moment(value, 'YYYY-MM-DD hh:mm A') : moment(value.split('T')[0], 'YYYY-M-D');
                }
                if (dateValue && dateValue.isValid()) {
                    value = dateValue.format(this.dateDisplayFormat);
                }
            }
        }
        return value;
    };
    /**
     * @return {?}
     */
    FormFieldModel.prototype.updateForm = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (!this.form) {
            return;
        }
        switch (this.type) {
            case FormFieldTypes.DROPDOWN:
                /*
                 This is needed due to Activiti reading dropdown values as string
                 but saving back as object: { id: <id>, name: <name> }
                 */
                if (this.value === 'empty' || this.value === '') {
                    this.form.values[this.id] = {};
                }
                else {
                    /** @type {?} */
                    var entry = this.options.filter((/**
                     * @param {?} opt
                     * @return {?}
                     */
                    function (opt) { return opt.id === _this.value; }));
                    if (entry.length > 0) {
                        this.form.values[this.id] = entry[0];
                    }
                }
                break;
            case FormFieldTypes.RADIO_BUTTONS:
                /*
                                 This is needed due to Activiti issue related to reading radio button values as value string
                                 but saving back as object: { id: <id>, name: <name> }
                                 */
                /** @type {?} */
                var rbEntry = this.options.filter((/**
                 * @param {?} opt
                 * @return {?}
                 */
                function (opt) { return opt.id === _this.value; }));
                if (rbEntry.length > 0) {
                    this.form.values[this.id] = rbEntry[0];
                }
                break;
            case FormFieldTypes.UPLOAD:
                this.form.hasUpload = true;
                if (this.value && this.value.length > 0) {
                    this.form.values[this.id] = this.value.map((/**
                     * @param {?} elem
                     * @return {?}
                     */
                    function (elem) { return elem.id; })).join(',');
                }
                else {
                    this.form.values[this.id] = null;
                }
                break;
            case FormFieldTypes.TYPEAHEAD:
                /** @type {?} */
                var taEntry = this.options.filter((/**
                 * @param {?} opt
                 * @return {?}
                 */
                function (opt) { return opt.id === _this.value || opt.name === _this.value; }));
                if (taEntry.length > 0) {
                    this.form.values[this.id] = taEntry[0];
                }
                else if (this.options.length > 0) {
                    this.form.values[this.id] = null;
                }
                break;
            case FormFieldTypes.DATE:
                /** @type {?} */
                var dateValue = moment(this.value, this.dateDisplayFormat, true);
                if (dateValue && dateValue.isValid()) {
                    this.form.values[this.id] = dateValue.format('YYYY-MM-DD') + "T00:00:00.000Z";
                }
                else {
                    this.form.values[this.id] = null;
                    this._value = this.value;
                }
                break;
            case FormFieldTypes.DATETIME:
                /** @type {?} */
                var dateTimeValue = moment(this.value, this.dateDisplayFormat, true);
                if (dateTimeValue && dateTimeValue.isValid()) {
                    /* cspell:disable-next-line */
                    this.form.values[this.id] = dateTimeValue.format('YYYY-MM-DDTHH:mm:ssZ');
                }
                else {
                    this.form.values[this.id] = null;
                    this._value = this.value;
                }
                break;
            case FormFieldTypes.NUMBER:
                this.form.values[this.id] = parseInt(this.value, 10);
                break;
            case FormFieldTypes.AMOUNT:
                this.form.values[this.id] = this.enableFractions ? parseFloat(this.value) : parseInt(this.value, 10);
                break;
            default:
                if (!FormFieldTypes.isReadOnlyType(this.type) && !this.isInvalidFieldType(this.type)) {
                    this.form.values[this.id] = this.value;
                }
        }
        this.form.onFormFieldChanged(this);
    };
    /**
     * Skip the invalid field type
     * @param type
     */
    /**
     * Skip the invalid field type
     * @param {?} type
     * @return {?}
     */
    FormFieldModel.prototype.isInvalidFieldType = /**
     * Skip the invalid field type
     * @param {?} type
     * @return {?}
     */
    function (type) {
        if (type === 'container') {
            return true;
        }
        else {
            return false;
        }
    };
    /**
     * @return {?}
     */
    FormFieldModel.prototype.getOptionName = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var option = this.options.find((/**
         * @param {?} opt
         * @return {?}
         */
        function (opt) { return opt.id === _this.value; }));
        return option ? option.name : null;
    };
    /**
     * @return {?}
     */
    FormFieldModel.prototype.hasOptions = /**
     * @return {?}
     */
    function () {
        return this.options && this.options.length > 0;
    };
    /**
     * @private
     * @param {?} json
     * @return {?}
     */
    FormFieldModel.prototype.isDateField = /**
     * @private
     * @param {?} json
     * @return {?}
     */
    function (json) {
        return (json.params &&
            json.params.field &&
            json.params.field.type === FormFieldTypes.DATE) ||
            json.type === FormFieldTypes.DATE;
    };
    /**
     * @private
     * @param {?} json
     * @return {?}
     */
    FormFieldModel.prototype.isDateTimeField = /**
     * @private
     * @param {?} json
     * @return {?}
     */
    function (json) {
        return (json.params &&
            json.params.field &&
            json.params.field.type === FormFieldTypes.DATETIME) ||
            json.type === FormFieldTypes.DATETIME;
    };
    return FormFieldModel;
}(FormWidgetModel));
// Maps to FormFieldRepresentation
export { FormFieldModel };
if (false) {
    /**
     * @type {?}
     * @private
     */
    FormFieldModel.prototype._value;
    /**
     * @type {?}
     * @private
     */
    FormFieldModel.prototype._readOnly;
    /**
     * @type {?}
     * @private
     */
    FormFieldModel.prototype._isValid;
    /**
     * @type {?}
     * @private
     */
    FormFieldModel.prototype._required;
    /** @type {?} */
    FormFieldModel.prototype.defaultDateFormat;
    /** @type {?} */
    FormFieldModel.prototype.defaultDateTimeFormat;
    /** @type {?} */
    FormFieldModel.prototype.fieldType;
    /** @type {?} */
    FormFieldModel.prototype.id;
    /** @type {?} */
    FormFieldModel.prototype.name;
    /** @type {?} */
    FormFieldModel.prototype.type;
    /** @type {?} */
    FormFieldModel.prototype.overrideId;
    /** @type {?} */
    FormFieldModel.prototype.tab;
    /** @type {?} */
    FormFieldModel.prototype.rowspan;
    /** @type {?} */
    FormFieldModel.prototype.colspan;
    /** @type {?} */
    FormFieldModel.prototype.placeholder;
    /** @type {?} */
    FormFieldModel.prototype.minLength;
    /** @type {?} */
    FormFieldModel.prototype.maxLength;
    /** @type {?} */
    FormFieldModel.prototype.minValue;
    /** @type {?} */
    FormFieldModel.prototype.maxValue;
    /** @type {?} */
    FormFieldModel.prototype.regexPattern;
    /** @type {?} */
    FormFieldModel.prototype.options;
    /** @type {?} */
    FormFieldModel.prototype.restUrl;
    /** @type {?} */
    FormFieldModel.prototype.restResponsePath;
    /** @type {?} */
    FormFieldModel.prototype.restIdProperty;
    /** @type {?} */
    FormFieldModel.prototype.restLabelProperty;
    /** @type {?} */
    FormFieldModel.prototype.hasEmptyValue;
    /** @type {?} */
    FormFieldModel.prototype.className;
    /** @type {?} */
    FormFieldModel.prototype.optionType;
    /** @type {?} */
    FormFieldModel.prototype.params;
    /** @type {?} */
    FormFieldModel.prototype.hyperlinkUrl;
    /** @type {?} */
    FormFieldModel.prototype.displayText;
    /** @type {?} */
    FormFieldModel.prototype.isVisible;
    /** @type {?} */
    FormFieldModel.prototype.visibilityCondition;
    /** @type {?} */
    FormFieldModel.prototype.enableFractions;
    /** @type {?} */
    FormFieldModel.prototype.currency;
    /** @type {?} */
    FormFieldModel.prototype.dateDisplayFormat;
    /** @type {?} */
    FormFieldModel.prototype.numberOfColumns;
    /** @type {?} */
    FormFieldModel.prototype.fields;
    /** @type {?} */
    FormFieldModel.prototype.columns;
    /** @type {?} */
    FormFieldModel.prototype.emptyOption;
    /** @type {?} */
    FormFieldModel.prototype.validationSummary;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1maWVsZC5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2NvcmUvZm9ybS1maWVsZC5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBLE9BQU8sTUFBTSxNQUFNLFlBQVksQ0FBQztBQUNoQyxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUNoRixPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNoRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUcxRCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFFLG9CQUFvQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDOUQsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHFCQUFxQixDQUFDOztBQUl0RDs7O0lBQW9DLDBDQUFlO0lBMEcvQyx3QkFBWSxJQUFlLEVBQUUsSUFBVTtRQUF2QyxZQUNJLGtCQUFNLElBQUksRUFBRSxJQUFJLENBQUMsU0FnRXBCO1FBeEtPLGVBQVMsR0FBWSxLQUFLLENBQUM7UUFDM0IsY0FBUSxHQUFZLElBQUksQ0FBQztRQUN6QixlQUFTLEdBQVksS0FBSyxDQUFDO1FBRTFCLHVCQUFpQixHQUFXLFVBQVUsQ0FBQztRQUN2QywyQkFBcUIsR0FBVyxrQkFBa0IsQ0FBQztRQVM1RCxhQUFPLEdBQVcsQ0FBQyxDQUFDO1FBQ3BCLGFBQU8sR0FBVyxDQUFDLENBQUM7UUFDcEIsaUJBQVcsR0FBVyxJQUFJLENBQUM7UUFDM0IsZUFBUyxHQUFXLENBQUMsQ0FBQztRQUN0QixlQUFTLEdBQVcsQ0FBQyxDQUFDO1FBSXRCLGFBQU8sR0FBc0IsRUFBRSxDQUFDO1FBUWhDLFlBQU0sR0FBc0IsRUFBRSxDQUFDO1FBRy9CLGVBQVMsR0FBWSxJQUFJLENBQUM7UUFDMUIseUJBQW1CLEdBQTBCLElBQUksQ0FBQztRQUNsRCxxQkFBZSxHQUFZLEtBQUssQ0FBQztRQUNqQyxjQUFRLEdBQVcsSUFBSSxDQUFDO1FBQ3hCLHVCQUFpQixHQUFXLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQzs7UUFHbkQscUJBQWUsR0FBVyxDQUFDLENBQUM7UUFDNUIsWUFBTSxHQUFxQixFQUFFLENBQUM7UUFDOUIsYUFBTyxHQUEyQixFQUFFLENBQUM7UUErRGpDLElBQUksSUFBSSxFQUFFO1lBQ04sS0FBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ2hDLEtBQUksQ0FBQyxFQUFFLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztZQUNsQixLQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDdEIsS0FBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBQ3RCLEtBQUksQ0FBQyxTQUFTLEdBQUcsbUJBQVUsSUFBSSxDQUFDLFFBQVEsRUFBQSxDQUFDO1lBQ3pDLEtBQUksQ0FBQyxTQUFTLEdBQUcsbUJBQVUsSUFBSSxDQUFDLFFBQVEsRUFBQSxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssVUFBVSxDQUFDO1lBQ3JFLEtBQUksQ0FBQyxVQUFVLEdBQUcsbUJBQVUsSUFBSSxDQUFDLFVBQVUsRUFBQSxDQUFDO1lBQzVDLEtBQUksQ0FBQyxHQUFHLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQztZQUNwQixLQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUM7WUFDNUIsS0FBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztZQUM5QyxLQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUM7WUFDMUMsS0FBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQztZQUNoRCxLQUFJLENBQUMsT0FBTyxHQUFHLG1CQUFTLElBQUksQ0FBQyxPQUFPLEVBQUEsQ0FBQztZQUNyQyxLQUFJLENBQUMsU0FBUyxHQUFHLG1CQUFTLElBQUksQ0FBQyxTQUFTLEVBQUEsSUFBSSxDQUFDLENBQUM7WUFDOUMsS0FBSSxDQUFDLFNBQVMsR0FBRyxtQkFBUyxJQUFJLENBQUMsU0FBUyxFQUFBLElBQUksQ0FBQyxDQUFDO1lBQzlDLEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUM5QixLQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7WUFDOUIsS0FBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQ3RDLEtBQUksQ0FBQyxPQUFPLEdBQUcsbUJBQW9CLElBQUksQ0FBQyxPQUFPLEVBQUEsSUFBSSxFQUFFLENBQUM7WUFDdEQsS0FBSSxDQUFDLGFBQWEsR0FBRyxtQkFBVSxJQUFJLENBQUMsYUFBYSxFQUFBLENBQUM7WUFDbEQsS0FBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDO1lBQ2hDLEtBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxDQUFDLFVBQVUsQ0FBQztZQUNsQyxLQUFJLENBQUMsTUFBTSxHQUFHLG1CQUFvQixJQUFJLENBQUMsTUFBTSxFQUFBLElBQUksRUFBRSxDQUFDO1lBQ3BELEtBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQztZQUN0QyxLQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7WUFDcEMsS0FBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxxQkFBcUIsQ0FBQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxDQUFDLENBQUMsU0FBUyxDQUFDO1lBQ3RILEtBQUksQ0FBQyxlQUFlLEdBQUcsbUJBQVUsSUFBSSxDQUFDLGVBQWUsRUFBQSxDQUFDO1lBQ3RELEtBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUM5QixLQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixJQUFJLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUNuRixLQUFJLENBQUMsTUFBTSxHQUFHLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLENBQUM7WUFDcEMsS0FBSSxDQUFDLGlCQUFpQixHQUFHLElBQUksaUJBQWlCLEVBQUUsQ0FBQztZQUVqRCxJQUFJLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxFQUFFLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxNQUFNLEVBQUU7Z0JBQzVFLEtBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQzthQUN2QztZQUVELElBQUksY0FBYyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7Z0JBQzFDLElBQUksSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRTtvQkFDbEMsSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7OzRCQUNqQixlQUFlLEdBQUcsS0FBSSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQzt3QkFDN0UsSUFBSSxlQUFlLEVBQUU7NEJBQ2pCLEtBQUksQ0FBQyxLQUFLLEdBQUcsZUFBZSxDQUFDO3lCQUNoQztxQkFDSjt5QkFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUU7OzRCQUN0RCxZQUFZLEdBQUcsS0FBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksRUFBRSxJQUFJLENBQUM7d0JBQ3pFLElBQUksWUFBWSxFQUFFOzRCQUNkLEtBQUksQ0FBQyxLQUFLLEdBQUcsWUFBWSxDQUFDO3lCQUM3QjtxQkFDSjtpQkFDSjthQUNKO1lBRUQsSUFBSSxjQUFjLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDM0MsS0FBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQzthQUNyQztTQUNKO1FBRUQsSUFBSSxLQUFJLENBQUMsYUFBYSxJQUFJLEtBQUksQ0FBQyxPQUFPLElBQUksS0FBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQy9ELEtBQUksQ0FBQyxXQUFXLEdBQUcsS0FBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUN0QztRQUVELEtBQUksQ0FBQyxVQUFVLEVBQUUsQ0FBQzs7SUFDdEIsQ0FBQztJQXhIRCxzQkFBSSxpQ0FBSzs7OztRQUFUO1lBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBQ3ZCLENBQUM7Ozs7O1FBRUQsVUFBVSxDQUFNO1lBQ1osSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7WUFDaEIsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQ3RCLENBQUM7OztPQUxBO0lBT0Qsc0JBQUksb0NBQVE7Ozs7UUFBWjtZQUNJLElBQUksSUFBSSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtnQkFDakMsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUNELE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUMxQixDQUFDOzs7OztRQUVELFVBQWEsUUFBaUI7WUFDMUIsSUFBSSxDQUFDLFNBQVMsR0FBRyxRQUFRLENBQUM7WUFDMUIsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQ3RCLENBQUM7OztPQUxBO0lBT0Qsc0JBQUksb0NBQVE7Ozs7UUFBWjtZQUNJLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUMxQixDQUFDOzs7OztRQUVELFVBQWEsS0FBYztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztZQUN2QixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDdEIsQ0FBQzs7O09BTEE7SUFPRCxzQkFBSSxtQ0FBTzs7OztRQUFYO1lBQ0ksT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3pCLENBQUM7OztPQUFBOzs7O0lBRUQsc0NBQWE7OztJQUFiO1FBQ0ksSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7SUFDMUIsQ0FBQzs7OztJQUVELGlDQUFROzs7SUFBUjs7UUFDSSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxpQkFBaUIsRUFBRSxDQUFDO1FBRWpELElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFOztnQkFDVixVQUFVLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLElBQUksRUFBRTs7Z0JBQ2xELEtBQXdCLElBQUEsZUFBQSxpQkFBQSxVQUFVLENBQUEsc0NBQUEsOERBQUU7b0JBQS9CLElBQU0sU0FBUyx1QkFBQTtvQkFDaEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUU7d0JBQzNCLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO3dCQUN0QixPQUFPLElBQUksQ0FBQyxRQUFRLENBQUM7cUJBQ3hCO2lCQUNKOzs7Ozs7Ozs7U0FDSjtRQUVELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQztJQUN6QixDQUFDOzs7Ozs7SUFxRU8sNkNBQW9COzs7OztJQUE1QixVQUE2QixTQUFjOztZQUNuQyxZQUFZLEdBQUcsU0FBUyxDQUFDLElBQUk7UUFDakMsSUFBSSxjQUFjLENBQUMsY0FBYyxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUM7WUFDN0MsU0FBUyxDQUFDLE1BQU07WUFDaEIsU0FBUyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUU7WUFDeEIsWUFBWSxHQUFHLFNBQVMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztTQUM5QztRQUNELE9BQU8sWUFBWSxLQUFLLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDO0lBQzFHLENBQUM7Ozs7OztJQUVPLDZDQUFvQjs7Ozs7SUFBNUIsVUFBNkIsSUFBWTtRQUNyQyxPQUFPLElBQUksS0FBSyxXQUFXLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQy9DLENBQUM7Ozs7OztJQUVPLDhDQUFxQjs7Ozs7SUFBN0IsVUFBOEIsSUFBWTtRQUN0QyxPQUFPLElBQUksSUFBSSxRQUFRLENBQUM7SUFDNUIsQ0FBQzs7Ozs7OztJQUVPLGdEQUF1Qjs7Ozs7O0lBQS9CLFVBQWdDLEtBQVUsRUFBRSxJQUFlOztZQUNuRCxTQUFTLEdBQUcsS0FBSyxDQUFDLElBQUk7UUFDMUIsSUFBSSxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3ZDLFNBQVMsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ3BEO1FBQ0QsT0FBTyxJQUFJLENBQUMsd0JBQXdCLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQzFELENBQUM7Ozs7Ozs7SUFFTywwQ0FBaUI7Ozs7OztJQUF6QixVQUEwQixZQUFvQixFQUFFLElBQWU7O1lBQ3JELFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQyxlQUFlO1lBQ3RELE9BQU8sZUFBZSxDQUFDLElBQUksS0FBSyxZQUFZLENBQUM7UUFDakQsQ0FBQyxFQUFDO1FBRUYsSUFBSSxRQUFRLEVBQUU7WUFDVixJQUFJLFFBQVEsQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO2dCQUM3QixPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3JDO1lBRUQsT0FBTyxRQUFRLENBQUMsS0FBSyxDQUFDO1NBQ3pCO1FBRUQsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQzs7Ozs7OztJQUVPLGlEQUF3Qjs7Ozs7O0lBQWhDLFVBQWlDLFlBQW9CLEVBQUUsSUFBZTtRQUNsRSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTs7Z0JBQ2pCLFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSTs7OztZQUFDLFVBQUMsZUFBZTtnQkFDeEQsT0FBTyxlQUFlLENBQUMsSUFBSSxLQUFLLFlBQVksQ0FBQztZQUNqRCxDQUFDLEVBQUM7WUFFRixJQUFJLFFBQVEsRUFBRTtnQkFDVixPQUFPLFFBQVEsQ0FBQyxJQUFJLEtBQUssU0FBUyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQzthQUNwRjtTQUNKO1FBRUQsT0FBTyxTQUFTLENBQUM7SUFDckIsQ0FBQzs7Ozs7OztJQUVPLHlDQUFnQjs7Ozs7O0lBQXhCLFVBQXlCLElBQVMsRUFBRSxJQUFlO1FBQW5ELGlCQTBCQztRQXpCRyxJQUFJLENBQUMsZUFBZSxHQUFHLG1CQUFTLElBQUksQ0FBQyxlQUFlLEVBQUEsSUFBSSxDQUFDLENBQUM7UUFFMUQsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBRTFCLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBQ2pCLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBRWpCLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNiLEtBQUssSUFBTSxZQUFZLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDcEMsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLGNBQWMsQ0FBQyxZQUFZLENBQUMsRUFBRTs7d0JBQ3BDLEdBQUcsR0FBRyxJQUFJLG9CQUFvQixFQUFFOzt3QkFFaEMsTUFBTSxHQUFxQixDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsR0FBRzs7OztvQkFBQyxVQUFDLENBQUMsSUFBSyxPQUFBLElBQUksY0FBYyxDQUFDLElBQUksRUFBRSxDQUFDLENBQUMsRUFBM0IsQ0FBMkIsRUFBQztvQkFDMUcsR0FBRyxDQUFDLE1BQU0sR0FBRyxNQUFNLENBQUM7b0JBQ3BCLEdBQUcsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxNQUFNLENBQUM7b0JBRS9DLEdBQUcsQ0FBQyxNQUFNLENBQUMsT0FBTzs7OztvQkFBQyxVQUFDLFNBQWM7d0JBQzlCLEtBQUksQ0FBQyxPQUFPLEdBQUcsU0FBUyxDQUFDLE9BQU8sR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsT0FBTyxDQUFDO29CQUN2RixDQUFDLEVBQUMsQ0FBQztvQkFFSCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQztvQkFDdkUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7aUJBQzFCO2FBQ0o7U0FDSjtJQUNMLENBQUM7Ozs7O0lBRUQsbUNBQVU7Ozs7SUFBVixVQUFXLElBQVM7O1lBQ1osS0FBSyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUk7UUFFNUQ7OztXQUdHO1FBQ0gsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxRQUFRLEVBQUU7WUFDdkMsSUFBSSxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7O29CQUM5QixPQUFPLEdBQUcsbUJBQW9CLElBQUksQ0FBQyxPQUFPLEVBQUEsSUFBSSxFQUFFO2dCQUN0RCxJQUFJLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOzt3QkFDZCxXQUFXLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUM7b0JBQ25DLElBQUksS0FBSyxLQUFLLEVBQUUsSUFBSSxLQUFLLEtBQUssV0FBVyxDQUFDLEVBQUUsSUFBSSxLQUFLLEtBQUssV0FBVyxDQUFDLElBQUksRUFBRTt3QkFDeEUsS0FBSyxHQUFHLFdBQVcsQ0FBQyxFQUFFLENBQUM7cUJBQzFCO3lCQUFNLElBQUksS0FBSyxDQUFDLEVBQUUsSUFBSSxLQUFLLENBQUMsSUFBSSxFQUFFO3dCQUMvQixLQUFLLEdBQUcsS0FBSyxDQUFDLEVBQUUsQ0FBQztxQkFDcEI7aUJBQ0o7YUFDSjtTQUNKO1FBRUQ7OztXQUdHO1FBQ0gsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxhQUFhLEVBQUU7Ozs7O2dCQUl0QyxLQUFLLEdBQXNCLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTTs7OztZQUFDLFVBQUMsR0FBRztnQkFDckQsT0FBQSxHQUFHLENBQUMsRUFBRSxLQUFLLEtBQUssSUFBSSxHQUFHLENBQUMsSUFBSSxLQUFLLEtBQUssSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLEtBQUssS0FBSyxDQUFDLEVBQUUsSUFBSSxHQUFHLENBQUMsSUFBSSxLQUFLLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUFyRyxDQUFxRyxFQUFDO1lBQzFHLElBQUksS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ2xCLEtBQUssR0FBRyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO2FBQ3ZCO1NBQ0o7UUFFRDs7O1dBR0c7UUFDSCxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBRTtZQUN0RCxJQUFJLEtBQUssRUFBRTs7b0JBQ0gsU0FBUyxTQUFBO2dCQUNiLElBQUksb0JBQW9CLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFO29CQUN0QyxTQUFTLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUM3QjtxQkFBTTtvQkFDSCxTQUFTLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRSxvQkFBb0IsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxVQUFVLENBQUMsQ0FBQztpQkFDMUg7Z0JBQ0QsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLE9BQU8sRUFBRSxFQUFFO29CQUNsQyxLQUFLLEdBQUcsU0FBUyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsaUJBQWlCLENBQUMsQ0FBQztpQkFDcEQ7YUFDSjtTQUNKO1FBRUQsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7OztJQUVELG1DQUFVOzs7SUFBVjtRQUFBLGlCQThFQztRQTdFRyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNaLE9BQU87U0FDVjtRQUVELFFBQVEsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNmLEtBQUssY0FBYyxDQUFDLFFBQVE7Z0JBQ3hCOzs7bUJBR0c7Z0JBQ0gsSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLE9BQU8sSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLEVBQUUsRUFBRTtvQkFDN0MsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztpQkFDbEM7cUJBQU07O3dCQUNHLEtBQUssR0FBc0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNOzs7O29CQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsR0FBRyxDQUFDLEVBQUUsS0FBSyxLQUFJLENBQUMsS0FBSyxFQUFyQixDQUFxQixFQUFDO29CQUNwRixJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO3dCQUNsQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDO3FCQUN4QztpQkFDSjtnQkFDRCxNQUFNO1lBQ1YsS0FBSyxjQUFjLENBQUMsYUFBYTs7Ozs7O29CQUt2QixPQUFPLEdBQXNCLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTTs7OztnQkFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLEdBQUcsQ0FBQyxFQUFFLEtBQUssS0FBSSxDQUFDLEtBQUssRUFBckIsQ0FBcUIsRUFBQztnQkFDdEYsSUFBSSxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDMUM7Z0JBQ0QsTUFBTTtZQUNWLEtBQUssY0FBYyxDQUFDLE1BQU07Z0JBQ3RCLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztnQkFDM0IsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDckMsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRzs7OztvQkFBQyxVQUFDLElBQUksSUFBSyxPQUFBLElBQUksQ0FBQyxFQUFFLEVBQVAsQ0FBTyxFQUFDLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2lCQUMzRTtxQkFBTTtvQkFDSCxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQUcsSUFBSSxDQUFDO2lCQUNwQztnQkFDRCxNQUFNO1lBQ1YsS0FBSyxjQUFjLENBQUMsU0FBUzs7b0JBQ25CLE9BQU8sR0FBc0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNOzs7O2dCQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsR0FBRyxDQUFDLEVBQUUsS0FBSyxLQUFJLENBQUMsS0FBSyxJQUFJLEdBQUcsQ0FBQyxJQUFJLEtBQUssS0FBSSxDQUFDLEtBQUssRUFBaEQsQ0FBZ0QsRUFBQztnQkFDakgsSUFBSSxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFDcEIsSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxHQUFHLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDMUM7cUJBQU0sSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ2hDLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUM7aUJBQ3BDO2dCQUNELE1BQU07WUFDVixLQUFLLGNBQWMsQ0FBQyxJQUFJOztvQkFDZCxTQUFTLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQztnQkFDbEUsSUFBSSxTQUFTLElBQUksU0FBUyxDQUFDLE9BQU8sRUFBRSxFQUFFO29CQUNsQyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLEdBQU0sU0FBUyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsbUJBQWdCLENBQUM7aUJBQ2pGO3FCQUFNO29CQUNILElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUM7b0JBQ2pDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztpQkFDNUI7Z0JBQ0QsTUFBTTtZQUNWLEtBQUssY0FBYyxDQUFDLFFBQVE7O29CQUNsQixhQUFhLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQztnQkFDdEUsSUFBSSxhQUFhLElBQUksYUFBYSxDQUFDLE9BQU8sRUFBRSxFQUFFO29CQUMxQyw4QkFBOEI7b0JBQzlCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxhQUFhLENBQUMsTUFBTSxDQUFDLHNCQUFzQixDQUFDLENBQUM7aUJBQzVFO3FCQUFNO29CQUNILElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUM7b0JBQ2pDLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztpQkFDNUI7Z0JBQ0QsTUFBTTtZQUNWLEtBQUssY0FBYyxDQUFDLE1BQU07Z0JBQ3RCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDckQsTUFBTTtZQUNWLEtBQUssY0FBYyxDQUFDLE1BQU07Z0JBQ3RCLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRSxFQUFFLENBQUMsQ0FBQztnQkFDckcsTUFBTTtZQUNWO2dCQUNJLElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUU7b0JBQ2xGLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDO2lCQUMxQztTQUNSO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN2QyxDQUFDO0lBRUQ7OztPQUdHOzs7Ozs7SUFDSCwyQ0FBa0I7Ozs7O0lBQWxCLFVBQW1CLElBQVk7UUFDM0IsSUFBSSxJQUFJLEtBQUssV0FBVyxFQUFFO1lBQ3RCLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7YUFBTTtZQUNILE9BQU8sS0FBSyxDQUFDO1NBQ2hCO0lBQ0wsQ0FBQzs7OztJQUVELHNDQUFhOzs7SUFBYjtRQUFBLGlCQUdDOztZQUZTLE1BQU0sR0FBb0IsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxHQUFHLENBQUMsRUFBRSxLQUFLLEtBQUksQ0FBQyxLQUFLLEVBQXJCLENBQXFCLEVBQUM7UUFDakYsT0FBTyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUN2QyxDQUFDOzs7O0lBRUQsbUNBQVU7OztJQUFWO1FBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUNuRCxDQUFDOzs7Ozs7SUFFTyxvQ0FBVzs7Ozs7SUFBbkIsVUFBb0IsSUFBUztRQUN6QixPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU07WUFDZixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUs7WUFDakIsSUFBSSxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxJQUFJLENBQUM7WUFDL0MsSUFBSSxDQUFDLElBQUksS0FBSyxjQUFjLENBQUMsSUFBSSxDQUFDO0lBQzFDLENBQUM7Ozs7OztJQUVPLHdDQUFlOzs7OztJQUF2QixVQUF3QixJQUFTO1FBQzdCLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTTtZQUNmLElBQUksQ0FBQyxNQUFNLENBQUMsS0FBSztZQUNqQixJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLEtBQUssY0FBYyxDQUFDLFFBQVEsQ0FBQztZQUNuRCxJQUFJLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxRQUFRLENBQUM7SUFDOUMsQ0FBQztJQUVMLHFCQUFDO0FBQUQsQ0FBQyxBQTdhRCxDQUFvQyxlQUFlLEdBNmFsRDs7Ozs7Ozs7SUEzYUcsZ0NBQXVCOzs7OztJQUN2QixtQ0FBbUM7Ozs7O0lBQ25DLGtDQUFpQzs7Ozs7SUFDakMsbUNBQW1DOztJQUVuQywyQ0FBZ0Q7O0lBQ2hELCtDQUE0RDs7SUFHNUQsbUNBQWtCOztJQUNsQiw0QkFBVzs7SUFDWCw4QkFBYTs7SUFDYiw4QkFBYTs7SUFDYixvQ0FBb0I7O0lBQ3BCLDZCQUFZOztJQUNaLGlDQUFvQjs7SUFDcEIsaUNBQW9COztJQUNwQixxQ0FBMkI7O0lBQzNCLG1DQUFzQjs7SUFDdEIsbUNBQXNCOztJQUN0QixrQ0FBaUI7O0lBQ2pCLGtDQUFpQjs7SUFDakIsc0NBQXFCOztJQUNyQixpQ0FBZ0M7O0lBQ2hDLGlDQUFnQjs7SUFDaEIsMENBQXlCOztJQUN6Qix3Q0FBdUI7O0lBQ3ZCLDJDQUEwQjs7SUFDMUIsdUNBQXVCOztJQUN2QixtQ0FBa0I7O0lBQ2xCLG9DQUFtQjs7SUFDbkIsZ0NBQStCOztJQUMvQixzQ0FBcUI7O0lBQ3JCLHFDQUFvQjs7SUFDcEIsbUNBQTBCOztJQUMxQiw2Q0FBa0Q7O0lBQ2xELHlDQUFpQzs7SUFDakMsa0NBQXdCOztJQUN4QiwyQ0FBbUQ7O0lBR25ELHlDQUE0Qjs7SUFDNUIsZ0NBQThCOztJQUM5QixpQ0FBcUM7O0lBR3JDLHFDQUE2Qjs7SUFDN0IsMkNBQXFDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuaW1wb3J0IG1vbWVudCBmcm9tICdtb21lbnQtZXM2JztcclxuaW1wb3J0IHsgV2lkZ2V0VmlzaWJpbGl0eU1vZGVsIH0gZnJvbSAnLi4vLi4vLi4vbW9kZWxzL3dpZGdldC12aXNpYmlsaXR5Lm1vZGVsJztcclxuaW1wb3J0IHsgQ29udGFpbmVyQ29sdW1uTW9kZWwgfSBmcm9tICcuL2NvbnRhaW5lci1jb2x1bW4ubW9kZWwnO1xyXG5pbXBvcnQgeyBFcnJvck1lc3NhZ2VNb2RlbCB9IGZyb20gJy4vZXJyb3ItbWVzc2FnZS5tb2RlbCc7XHJcbmltcG9ydCB7IEZvcm1GaWVsZE1ldGFkYXRhIH0gZnJvbSAnLi9mb3JtLWZpZWxkLW1ldGFkYXRhJztcclxuaW1wb3J0IHsgRm9ybUZpZWxkT3B0aW9uIH0gZnJvbSAnLi9mb3JtLWZpZWxkLW9wdGlvbic7XHJcbmltcG9ydCB7IEZvcm1GaWVsZFR5cGVzIH0gZnJvbSAnLi9mb3JtLWZpZWxkLXR5cGVzJztcclxuaW1wb3J0IHsgTnVtYmVyRmllbGRWYWxpZGF0b3IgfSBmcm9tICcuL2Zvcm0tZmllbGQtdmFsaWRhdG9yJztcclxuaW1wb3J0IHsgRm9ybVdpZGdldE1vZGVsIH0gZnJvbSAnLi9mb3JtLXdpZGdldC5tb2RlbCc7XHJcbmltcG9ydCB7IEZvcm1Nb2RlbCB9IGZyb20gJy4vZm9ybS5tb2RlbCc7XHJcblxyXG4vLyBNYXBzIHRvIEZvcm1GaWVsZFJlcHJlc2VudGF0aW9uXHJcbmV4cG9ydCBjbGFzcyBGb3JtRmllbGRNb2RlbCBleHRlbmRzIEZvcm1XaWRnZXRNb2RlbCB7XHJcblxyXG4gICAgcHJpdmF0ZSBfdmFsdWU6IHN0cmluZztcclxuICAgIHByaXZhdGUgX3JlYWRPbmx5OiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBwcml2YXRlIF9pc1ZhbGlkOiBib29sZWFuID0gdHJ1ZTtcclxuICAgIHByaXZhdGUgX3JlcXVpcmVkOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgcmVhZG9ubHkgZGVmYXVsdERhdGVGb3JtYXQ6IHN0cmluZyA9ICdELU0tWVlZWSc7XHJcbiAgICByZWFkb25seSBkZWZhdWx0RGF0ZVRpbWVGb3JtYXQ6IHN0cmluZyA9ICdELU0tWVlZWSBoaDptbSBBJztcclxuXHJcbiAgICAvLyBtb2RlbCBtZW1iZXJzXHJcbiAgICBmaWVsZFR5cGU6IHN0cmluZztcclxuICAgIGlkOiBzdHJpbmc7XHJcbiAgICBuYW1lOiBzdHJpbmc7XHJcbiAgICB0eXBlOiBzdHJpbmc7XHJcbiAgICBvdmVycmlkZUlkOiBib29sZWFuO1xyXG4gICAgdGFiOiBzdHJpbmc7XHJcbiAgICByb3dzcGFuOiBudW1iZXIgPSAxO1xyXG4gICAgY29sc3BhbjogbnVtYmVyID0gMTtcclxuICAgIHBsYWNlaG9sZGVyOiBzdHJpbmcgPSBudWxsO1xyXG4gICAgbWluTGVuZ3RoOiBudW1iZXIgPSAwO1xyXG4gICAgbWF4TGVuZ3RoOiBudW1iZXIgPSAwO1xyXG4gICAgbWluVmFsdWU6IHN0cmluZztcclxuICAgIG1heFZhbHVlOiBzdHJpbmc7XHJcbiAgICByZWdleFBhdHRlcm46IHN0cmluZztcclxuICAgIG9wdGlvbnM6IEZvcm1GaWVsZE9wdGlvbltdID0gW107XHJcbiAgICByZXN0VXJsOiBzdHJpbmc7XHJcbiAgICByZXN0UmVzcG9uc2VQYXRoOiBzdHJpbmc7XHJcbiAgICByZXN0SWRQcm9wZXJ0eTogc3RyaW5nO1xyXG4gICAgcmVzdExhYmVsUHJvcGVydHk6IHN0cmluZztcclxuICAgIGhhc0VtcHR5VmFsdWU6IGJvb2xlYW47XHJcbiAgICBjbGFzc05hbWU6IHN0cmluZztcclxuICAgIG9wdGlvblR5cGU6IHN0cmluZztcclxuICAgIHBhcmFtczogRm9ybUZpZWxkTWV0YWRhdGEgPSB7fTtcclxuICAgIGh5cGVybGlua1VybDogc3RyaW5nO1xyXG4gICAgZGlzcGxheVRleHQ6IHN0cmluZztcclxuICAgIGlzVmlzaWJsZTogYm9vbGVhbiA9IHRydWU7XHJcbiAgICB2aXNpYmlsaXR5Q29uZGl0aW9uOiBXaWRnZXRWaXNpYmlsaXR5TW9kZWwgPSBudWxsO1xyXG4gICAgZW5hYmxlRnJhY3Rpb25zOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBjdXJyZW5jeTogc3RyaW5nID0gbnVsbDtcclxuICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBzdHJpbmcgPSB0aGlzLmRlZmF1bHREYXRlRm9ybWF0O1xyXG5cclxuICAgIC8vIGNvbnRhaW5lciBtb2RlbCBtZW1iZXJzXHJcbiAgICBudW1iZXJPZkNvbHVtbnM6IG51bWJlciA9IDE7XHJcbiAgICBmaWVsZHM6IEZvcm1GaWVsZE1vZGVsW10gPSBbXTtcclxuICAgIGNvbHVtbnM6IENvbnRhaW5lckNvbHVtbk1vZGVsW10gPSBbXTtcclxuXHJcbiAgICAvLyB1dGlsIG1lbWJlcnNcclxuICAgIGVtcHR5T3B0aW9uOiBGb3JtRmllbGRPcHRpb247XHJcbiAgICB2YWxpZGF0aW9uU3VtbWFyeTogRXJyb3JNZXNzYWdlTW9kZWw7XHJcblxyXG4gICAgZ2V0IHZhbHVlKCk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3ZhbHVlO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCB2YWx1ZSh2OiBhbnkpIHtcclxuICAgICAgICB0aGlzLl92YWx1ZSA9IHY7XHJcbiAgICAgICAgdGhpcy51cGRhdGVGb3JtKCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHJlYWRPbmx5KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLmZvcm0gJiYgdGhpcy5mb3JtLnJlYWRPbmx5KSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5fcmVhZE9ubHk7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IHJlYWRPbmx5KHJlYWRPbmx5OiBib29sZWFuKSB7XHJcbiAgICAgICAgdGhpcy5fcmVhZE9ubHkgPSByZWFkT25seTtcclxuICAgICAgICB0aGlzLnVwZGF0ZUZvcm0oKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgcmVxdWlyZWQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3JlcXVpcmVkO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCByZXF1aXJlZCh2YWx1ZTogYm9vbGVhbikge1xyXG4gICAgICAgIHRoaXMuX3JlcXVpcmVkID0gdmFsdWU7XHJcbiAgICAgICAgdGhpcy51cGRhdGVGb3JtKCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGlzVmFsaWQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzVmFsaWQ7XHJcbiAgICB9XHJcblxyXG4gICAgbWFya0FzSW52YWxpZCgpIHtcclxuICAgICAgICB0aGlzLl9pc1ZhbGlkID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgdmFsaWRhdGUoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgdGhpcy52YWxpZGF0aW9uU3VtbWFyeSA9IG5ldyBFcnJvck1lc3NhZ2VNb2RlbCgpO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMucmVhZE9ubHkpIHtcclxuICAgICAgICAgICAgY29uc3QgdmFsaWRhdG9ycyA9IHRoaXMuZm9ybS5maWVsZFZhbGlkYXRvcnMgfHwgW107XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgdmFsaWRhdG9yIG9mIHZhbGlkYXRvcnMpIHtcclxuICAgICAgICAgICAgICAgIGlmICghdmFsaWRhdG9yLnZhbGlkYXRlKHRoaXMpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5faXNWYWxpZCA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLl9pc1ZhbGlkO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLl9pc1ZhbGlkID0gdHJ1ZTtcclxuICAgICAgICByZXR1cm4gdGhpcy5faXNWYWxpZDtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihmb3JtOiBGb3JtTW9kZWwsIGpzb24/OiBhbnkpIHtcclxuICAgICAgICBzdXBlcihmb3JtLCBqc29uKTtcclxuICAgICAgICBpZiAoanNvbikge1xyXG4gICAgICAgICAgICB0aGlzLmZpZWxkVHlwZSA9IGpzb24uZmllbGRUeXBlO1xyXG4gICAgICAgICAgICB0aGlzLmlkID0ganNvbi5pZDtcclxuICAgICAgICAgICAgdGhpcy5uYW1lID0ganNvbi5uYW1lO1xyXG4gICAgICAgICAgICB0aGlzLnR5cGUgPSBqc29uLnR5cGU7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlcXVpcmVkID0gPGJvb2xlYW4+IGpzb24ucmVxdWlyZWQ7XHJcbiAgICAgICAgICAgIHRoaXMuX3JlYWRPbmx5ID0gPGJvb2xlYW4+IGpzb24ucmVhZE9ubHkgfHwganNvbi50eXBlID09PSAncmVhZG9ubHknO1xyXG4gICAgICAgICAgICB0aGlzLm92ZXJyaWRlSWQgPSA8Ym9vbGVhbj4ganNvbi5vdmVycmlkZUlkO1xyXG4gICAgICAgICAgICB0aGlzLnRhYiA9IGpzb24udGFiO1xyXG4gICAgICAgICAgICB0aGlzLnJlc3RVcmwgPSBqc29uLnJlc3RVcmw7XHJcbiAgICAgICAgICAgIHRoaXMucmVzdFJlc3BvbnNlUGF0aCA9IGpzb24ucmVzdFJlc3BvbnNlUGF0aDtcclxuICAgICAgICAgICAgdGhpcy5yZXN0SWRQcm9wZXJ0eSA9IGpzb24ucmVzdElkUHJvcGVydHk7XHJcbiAgICAgICAgICAgIHRoaXMucmVzdExhYmVsUHJvcGVydHkgPSBqc29uLnJlc3RMYWJlbFByb3BlcnR5O1xyXG4gICAgICAgICAgICB0aGlzLmNvbHNwYW4gPSA8bnVtYmVyPiBqc29uLmNvbHNwYW47XHJcbiAgICAgICAgICAgIHRoaXMubWluTGVuZ3RoID0gPG51bWJlcj4ganNvbi5taW5MZW5ndGggfHwgMDtcclxuICAgICAgICAgICAgdGhpcy5tYXhMZW5ndGggPSA8bnVtYmVyPiBqc29uLm1heExlbmd0aCB8fCAwO1xyXG4gICAgICAgICAgICB0aGlzLm1pblZhbHVlID0ganNvbi5taW5WYWx1ZTtcclxuICAgICAgICAgICAgdGhpcy5tYXhWYWx1ZSA9IGpzb24ubWF4VmFsdWU7XHJcbiAgICAgICAgICAgIHRoaXMucmVnZXhQYXR0ZXJuID0ganNvbi5yZWdleFBhdHRlcm47XHJcbiAgICAgICAgICAgIHRoaXMub3B0aW9ucyA9IDxGb3JtRmllbGRPcHRpb25bXT4ganNvbi5vcHRpb25zIHx8IFtdO1xyXG4gICAgICAgICAgICB0aGlzLmhhc0VtcHR5VmFsdWUgPSA8Ym9vbGVhbj4ganNvbi5oYXNFbXB0eVZhbHVlO1xyXG4gICAgICAgICAgICB0aGlzLmNsYXNzTmFtZSA9IGpzb24uY2xhc3NOYW1lO1xyXG4gICAgICAgICAgICB0aGlzLm9wdGlvblR5cGUgPSBqc29uLm9wdGlvblR5cGU7XHJcbiAgICAgICAgICAgIHRoaXMucGFyYW1zID0gPEZvcm1GaWVsZE1ldGFkYXRhPiBqc29uLnBhcmFtcyB8fCB7fTtcclxuICAgICAgICAgICAgdGhpcy5oeXBlcmxpbmtVcmwgPSBqc29uLmh5cGVybGlua1VybDtcclxuICAgICAgICAgICAgdGhpcy5kaXNwbGF5VGV4dCA9IGpzb24uZGlzcGxheVRleHQ7XHJcbiAgICAgICAgICAgIHRoaXMudmlzaWJpbGl0eUNvbmRpdGlvbiA9IGpzb24udmlzaWJpbGl0eUNvbmRpdGlvbiA/IG5ldyBXaWRnZXRWaXNpYmlsaXR5TW9kZWwoanNvbi52aXNpYmlsaXR5Q29uZGl0aW9uKSA6IHVuZGVmaW5lZDtcclxuICAgICAgICAgICAgdGhpcy5lbmFibGVGcmFjdGlvbnMgPSA8Ym9vbGVhbj4ganNvbi5lbmFibGVGcmFjdGlvbnM7XHJcbiAgICAgICAgICAgIHRoaXMuY3VycmVuY3kgPSBqc29uLmN1cnJlbmN5O1xyXG4gICAgICAgICAgICB0aGlzLmRhdGVEaXNwbGF5Rm9ybWF0ID0ganNvbi5kYXRlRGlzcGxheUZvcm1hdCB8fCB0aGlzLmdldERlZmF1bHREYXRlRm9ybWF0KGpzb24pO1xyXG4gICAgICAgICAgICB0aGlzLl92YWx1ZSA9IHRoaXMucGFyc2VWYWx1ZShqc29uKTtcclxuICAgICAgICAgICAgdGhpcy52YWxpZGF0aW9uU3VtbWFyeSA9IG5ldyBFcnJvck1lc3NhZ2VNb2RlbCgpO1xyXG5cclxuICAgICAgICAgICAgaWYgKGpzb24ucGxhY2Vob2xkZXIgJiYganNvbi5wbGFjZWhvbGRlciAhPT0gJycgJiYganNvbi5wbGFjZWhvbGRlciAhPT0gJ251bGwnKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnBsYWNlaG9sZGVyID0ganNvbi5wbGFjZWhvbGRlcjtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKEZvcm1GaWVsZFR5cGVzLmlzUmVhZE9ubHlUeXBlKGpzb24udHlwZSkpIHtcclxuICAgICAgICAgICAgICAgIGlmIChqc29uLnBhcmFtcyAmJiBqc29uLnBhcmFtcy5maWVsZCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChmb3JtLnByb2Nlc3NWYXJpYWJsZXMpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgcHJvY2Vzc1ZhcmlhYmxlID0gdGhpcy5nZXRQcm9jZXNzVmFyaWFibGVWYWx1ZShqc29uLnBhcmFtcy5maWVsZCwgZm9ybSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChwcm9jZXNzVmFyaWFibGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudmFsdWUgPSBwcm9jZXNzVmFyaWFibGU7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKGpzb24ucGFyYW1zLnJlc3BvbnNlVmFyaWFibGUgJiYgZm9ybS5qc29uLnZhcmlhYmxlcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBmb3JtVmFyaWFibGUgPSB0aGlzLmdldFZhcmlhYmxlc1ZhbHVlKGpzb24ucGFyYW1zLmZpZWxkLm5hbWUsIGZvcm0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoZm9ybVZhcmlhYmxlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnZhbHVlID0gZm9ybVZhcmlhYmxlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoRm9ybUZpZWxkVHlwZXMuaXNDb250YWluZXJUeXBlKGpzb24udHlwZSkpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY29udGFpbmVyRmFjdG9yeShqc29uLCBmb3JtKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuaGFzRW1wdHlWYWx1ZSAmJiB0aGlzLm9wdGlvbnMgJiYgdGhpcy5vcHRpb25zLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgdGhpcy5lbXB0eU9wdGlvbiA9IHRoaXMub3B0aW9uc1swXTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMudXBkYXRlRm9ybSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0RGVmYXVsdERhdGVGb3JtYXQoanNvbkZpZWxkOiBhbnkpOiBzdHJpbmcge1xyXG4gICAgICAgIGxldCBvcmlnaW5hbFR5cGUgPSBqc29uRmllbGQudHlwZTtcclxuICAgICAgICBpZiAoRm9ybUZpZWxkVHlwZXMuaXNSZWFkT25seVR5cGUoanNvbkZpZWxkLnR5cGUpICYmXHJcbiAgICAgICAgICAgIGpzb25GaWVsZC5wYXJhbXMgJiZcclxuICAgICAgICAgICAganNvbkZpZWxkLnBhcmFtcy5maWVsZCkge1xyXG4gICAgICAgICAgICBvcmlnaW5hbFR5cGUgPSBqc29uRmllbGQucGFyYW1zLmZpZWxkLnR5cGU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBvcmlnaW5hbFR5cGUgPT09IEZvcm1GaWVsZFR5cGVzLkRBVEVUSU1FID8gdGhpcy5kZWZhdWx0RGF0ZVRpbWVGb3JtYXQgOiB0aGlzLmRlZmF1bHREYXRlRm9ybWF0O1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaXNUeXBlYWhlYWRGaWVsZFR5cGUodHlwZTogc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHR5cGUgPT09ICd0eXBlYWhlYWQnID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0RmllbGROYW1lV2l0aExhYmVsKG5hbWU6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIG5hbWUgKz0gJ19MQUJFTCc7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRQcm9jZXNzVmFyaWFibGVWYWx1ZShmaWVsZDogYW55LCBmb3JtOiBGb3JtTW9kZWwpIHtcclxuICAgICAgICBsZXQgZmllbGROYW1lID0gZmllbGQubmFtZTtcclxuICAgICAgICBpZiAodGhpcy5pc1R5cGVhaGVhZEZpZWxkVHlwZShmaWVsZC50eXBlKSkge1xyXG4gICAgICAgICAgICBmaWVsZE5hbWUgPSB0aGlzLmdldEZpZWxkTmFtZVdpdGhMYWJlbChmaWVsZC5pZCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0aGlzLmZpbmRQcm9jZXNzVmFyaWFibGVWYWx1ZShmaWVsZE5hbWUsIGZvcm0pO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0VmFyaWFibGVzVmFsdWUodmFyaWFibGVOYW1lOiBzdHJpbmcsIGZvcm06IEZvcm1Nb2RlbCkge1xyXG4gICAgICAgIGNvbnN0IHZhcmlhYmxlID0gZm9ybS5qc29uLnZhcmlhYmxlcy5maW5kKChjdXJyZW50VmFyaWFibGUpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGN1cnJlbnRWYXJpYWJsZS5uYW1lID09PSB2YXJpYWJsZU5hbWU7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIGlmICh2YXJpYWJsZSkge1xyXG4gICAgICAgICAgICBpZiAodmFyaWFibGUudHlwZSA9PT0gJ2Jvb2xlYW4nKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gSlNPTi5wYXJzZSh2YXJpYWJsZS52YWx1ZSk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHJldHVybiB2YXJpYWJsZS52YWx1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZmluZFByb2Nlc3NWYXJpYWJsZVZhbHVlKHZhcmlhYmxlTmFtZTogc3RyaW5nLCBmb3JtOiBGb3JtTW9kZWwpIHtcclxuICAgICAgICBpZiAoZm9ybS5wcm9jZXNzVmFyaWFibGVzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHZhcmlhYmxlID0gZm9ybS5wcm9jZXNzVmFyaWFibGVzLmZpbmQoKGN1cnJlbnRWYXJpYWJsZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGN1cnJlbnRWYXJpYWJsZS5uYW1lID09PSB2YXJpYWJsZU5hbWU7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgaWYgKHZhcmlhYmxlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdmFyaWFibGUudHlwZSA9PT0gJ2Jvb2xlYW4nID8gSlNPTi5wYXJzZSh2YXJpYWJsZS52YWx1ZSkgOiB2YXJpYWJsZS52YWx1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNvbnRhaW5lckZhY3RvcnkoanNvbjogYW55LCBmb3JtOiBGb3JtTW9kZWwpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLm51bWJlck9mQ29sdW1ucyA9IDxudW1iZXI+IGpzb24ubnVtYmVyT2ZDb2x1bW5zIHx8IDE7XHJcblxyXG4gICAgICAgIHRoaXMuZmllbGRzID0ganNvbi5maWVsZHM7XHJcblxyXG4gICAgICAgIHRoaXMucm93c3BhbiA9IDE7XHJcbiAgICAgICAgdGhpcy5jb2xzcGFuID0gMTtcclxuXHJcbiAgICAgICAgaWYgKGpzb24uZmllbGRzKSB7XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgY3VycmVudEZpZWxkIGluIGpzb24uZmllbGRzKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoanNvbi5maWVsZHMuaGFzT3duUHJvcGVydHkoY3VycmVudEZpZWxkKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGNvbCA9IG5ldyBDb250YWluZXJDb2x1bW5Nb2RlbCgpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWVsZHM6IEZvcm1GaWVsZE1vZGVsW10gPSAoanNvbi5maWVsZHNbY3VycmVudEZpZWxkXSB8fCBbXSkubWFwKChmKSA9PiBuZXcgRm9ybUZpZWxkTW9kZWwoZm9ybSwgZikpO1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbC5maWVsZHMgPSBmaWVsZHM7XHJcbiAgICAgICAgICAgICAgICAgICAgY29sLnJvd3NwYW4gPSBqc29uLmZpZWxkc1tjdXJyZW50RmllbGRdLmxlbmd0aDtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgY29sLmZpZWxkcy5mb3JFYWNoKChjb2xGaWVsZHM6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmNvbHNwYW4gPSBjb2xGaWVsZHMuY29sc3BhbiA+IHRoaXMuY29sc3BhbiA/IGNvbEZpZWxkcy5jb2xzcGFuIDogdGhpcy5jb2xzcGFuO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnJvd3NwYW4gPSB0aGlzLnJvd3NwYW4gPCBjb2wucm93c3BhbiA/IGNvbC5yb3dzcGFuIDogdGhpcy5yb3dzcGFuO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29sdW1ucy5wdXNoKGNvbCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcGFyc2VWYWx1ZShqc29uOiBhbnkpOiBhbnkge1xyXG4gICAgICAgIGxldCB2YWx1ZSA9IGpzb24uaGFzT3duUHJvcGVydHkoJ3ZhbHVlJykgPyBqc29uLnZhbHVlIDogbnVsbDtcclxuXHJcbiAgICAgICAgLypcclxuICAgICAgICAgVGhpcyBpcyBuZWVkZWQgZHVlIHRvIEFjdGl2aXRpIGlzc3VlIHJlbGF0ZWQgdG8gcmVhZGluZyBkcm9wZG93biB2YWx1ZXMgYXMgdmFsdWUgc3RyaW5nXHJcbiAgICAgICAgIGJ1dCBzYXZpbmcgYmFjayBhcyBvYmplY3Q6IHsgaWQ6IDxpZD4sIG5hbWU6IDxuYW1lPiB9XHJcbiAgICAgICAgICovXHJcbiAgICAgICAgaWYgKGpzb24udHlwZSA9PT0gRm9ybUZpZWxkVHlwZXMuRFJPUERPV04pIHtcclxuICAgICAgICAgICAgaWYgKGpzb24uaGFzRW1wdHlWYWx1ZSAmJiBqc29uLm9wdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IG9wdGlvbnMgPSA8Rm9ybUZpZWxkT3B0aW9uW10+IGpzb24ub3B0aW9ucyB8fCBbXTtcclxuICAgICAgICAgICAgICAgIGlmIChvcHRpb25zLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBlbXB0eU9wdGlvbiA9IGpzb24ub3B0aW9uc1swXTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodmFsdWUgPT09ICcnIHx8IHZhbHVlID09PSBlbXB0eU9wdGlvbi5pZCB8fCB2YWx1ZSA9PT0gZW1wdHlPcHRpb24ubmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZSA9IGVtcHR5T3B0aW9uLmlkO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodmFsdWUuaWQgJiYgdmFsdWUubmFtZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZSA9IHZhbHVlLmlkO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLypcclxuICAgICAgICAgVGhpcyBpcyBuZWVkZWQgZHVlIHRvIEFjdGl2aXRpIGlzc3VlIHJlbGF0ZWQgdG8gcmVhZGluZyByYWRpbyBidXR0b24gdmFsdWVzIGFzIHZhbHVlIHN0cmluZ1xyXG4gICAgICAgICBidXQgc2F2aW5nIGJhY2sgYXMgb2JqZWN0OiB7IGlkOiA8aWQ+LCBuYW1lOiA8bmFtZT4gfVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIGlmIChqc29uLnR5cGUgPT09IEZvcm1GaWVsZFR5cGVzLlJBRElPX0JVVFRPTlMpIHtcclxuICAgICAgICAgICAgLy8gQWN0aXZpdGkgaGFzIGEgYnVnIHdpdGggZGVmYXVsdCByYWRpbyBidXR0b24gdmFsdWUgd2hlcmUgaW5pdGlhbCBzZWxlY3Rpb24gcGFzc2VkIGFzIGBuYW1lYCB2YWx1ZVxyXG4gICAgICAgICAgICAvLyBzbyB0cnkgcmVzb2x2aW5nIGN1cnJlbnQgb25lIHdpdGggYSBmYWxsYmFjayB0byBmaXJzdCBlbnRyeSB2aWEgbmFtZSBvciBpZFxyXG4gICAgICAgICAgICAvLyBUT0RPOiBuZWVkcyB0byBiZSByZXBvcnRlZCBhbmQgZml4ZWQgYXQgQWN0aXZpdGkgc2lkZVxyXG4gICAgICAgICAgICBjb25zdCBlbnRyeTogRm9ybUZpZWxkT3B0aW9uW10gPSB0aGlzLm9wdGlvbnMuZmlsdGVyKChvcHQpID0+XHJcbiAgICAgICAgICAgICAgICBvcHQuaWQgPT09IHZhbHVlIHx8IG9wdC5uYW1lID09PSB2YWx1ZSB8fCAodmFsdWUgJiYgKG9wdC5pZCA9PT0gdmFsdWUuaWQgfHwgb3B0Lm5hbWUgPT09IHZhbHVlLm5hbWUpKSk7XHJcbiAgICAgICAgICAgIGlmIChlbnRyeS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICB2YWx1ZSA9IGVudHJ5WzBdLmlkO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvKlxyXG4gICAgICAgICBUaGlzIGlzIG5lZWRlZCBkdWUgdG8gQWN0aXZpdGkgZGlzcGxheWluZy9lZGl0aW5nIGRhdGVzIGluIGQtTS1ZWVlZIGZvcm1hdFxyXG4gICAgICAgICBidXQgc3RvcmluZyBvbiBzZXJ2ZXIgaW4gSVNPODYwMSBmb3JtYXQgKGkuZS4gMjAxMy0wMi0wNFQyMjo0NDozMC42NTJaKVxyXG4gICAgICAgICAqL1xyXG4gICAgICAgIGlmICh0aGlzLmlzRGF0ZUZpZWxkKGpzb24pIHx8IHRoaXMuaXNEYXRlVGltZUZpZWxkKGpzb24pKSB7XHJcbiAgICAgICAgICAgIGlmICh2YWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgbGV0IGRhdGVWYWx1ZTtcclxuICAgICAgICAgICAgICAgIGlmIChOdW1iZXJGaWVsZFZhbGlkYXRvci5pc051bWJlcih2YWx1ZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICBkYXRlVmFsdWUgPSBtb21lbnQodmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICBkYXRlVmFsdWUgPSB0aGlzLmlzRGF0ZVRpbWVGaWVsZChqc29uKSA/IG1vbWVudCh2YWx1ZSwgJ1lZWVktTU0tREQgaGg6bW0gQScpIDogbW9tZW50KHZhbHVlLnNwbGl0KCdUJylbMF0sICdZWVlZLU0tRCcpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgaWYgKGRhdGVWYWx1ZSAmJiBkYXRlVmFsdWUuaXNWYWxpZCgpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWUgPSBkYXRlVmFsdWUuZm9ybWF0KHRoaXMuZGF0ZURpc3BsYXlGb3JtYXQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgdXBkYXRlRm9ybSgpIHtcclxuICAgICAgICBpZiAoIXRoaXMuZm9ybSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBzd2l0Y2ggKHRoaXMudHlwZSkge1xyXG4gICAgICAgICAgICBjYXNlIEZvcm1GaWVsZFR5cGVzLkRST1BET1dOOlxyXG4gICAgICAgICAgICAgICAgLypcclxuICAgICAgICAgICAgICAgICBUaGlzIGlzIG5lZWRlZCBkdWUgdG8gQWN0aXZpdGkgcmVhZGluZyBkcm9wZG93biB2YWx1ZXMgYXMgc3RyaW5nXHJcbiAgICAgICAgICAgICAgICAgYnV0IHNhdmluZyBiYWNrIGFzIG9iamVjdDogeyBpZDogPGlkPiwgbmFtZTogPG5hbWU+IH1cclxuICAgICAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMudmFsdWUgPT09ICdlbXB0eScgfHwgdGhpcy52YWx1ZSA9PT0gJycpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm0udmFsdWVzW3RoaXMuaWRdID0ge307XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGVudHJ5OiBGb3JtRmllbGRPcHRpb25bXSA9IHRoaXMub3B0aW9ucy5maWx0ZXIoKG9wdCkgPT4gb3B0LmlkID09PSB0aGlzLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZW50cnkubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm0udmFsdWVzW3RoaXMuaWRdID0gZW50cnlbMF07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgRm9ybUZpZWxkVHlwZXMuUkFESU9fQlVUVE9OUzpcclxuICAgICAgICAgICAgICAgIC8qXHJcbiAgICAgICAgICAgICAgICAgVGhpcyBpcyBuZWVkZWQgZHVlIHRvIEFjdGl2aXRpIGlzc3VlIHJlbGF0ZWQgdG8gcmVhZGluZyByYWRpbyBidXR0b24gdmFsdWVzIGFzIHZhbHVlIHN0cmluZ1xyXG4gICAgICAgICAgICAgICAgIGJ1dCBzYXZpbmcgYmFjayBhcyBvYmplY3Q6IHsgaWQ6IDxpZD4sIG5hbWU6IDxuYW1lPiB9XHJcbiAgICAgICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgICAgIGNvbnN0IHJiRW50cnk6IEZvcm1GaWVsZE9wdGlvbltdID0gdGhpcy5vcHRpb25zLmZpbHRlcigob3B0KSA9PiBvcHQuaWQgPT09IHRoaXMudmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHJiRW50cnkubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZm9ybS52YWx1ZXNbdGhpcy5pZF0gPSByYkVudHJ5WzBdO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgRm9ybUZpZWxkVHlwZXMuVVBMT0FEOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5mb3JtLmhhc1VwbG9hZCA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy52YWx1ZSAmJiB0aGlzLnZhbHVlLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm0udmFsdWVzW3RoaXMuaWRdID0gdGhpcy52YWx1ZS5tYXAoKGVsZW0pID0+IGVsZW0uaWQpLmpvaW4oJywnKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JtLnZhbHVlc1t0aGlzLmlkXSA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBGb3JtRmllbGRUeXBlcy5UWVBFQUhFQUQ6XHJcbiAgICAgICAgICAgICAgICBjb25zdCB0YUVudHJ5OiBGb3JtRmllbGRPcHRpb25bXSA9IHRoaXMub3B0aW9ucy5maWx0ZXIoKG9wdCkgPT4gb3B0LmlkID09PSB0aGlzLnZhbHVlIHx8IG9wdC5uYW1lID09PSB0aGlzLnZhbHVlKTtcclxuICAgICAgICAgICAgICAgIGlmICh0YUVudHJ5Lmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm0udmFsdWVzW3RoaXMuaWRdID0gdGFFbnRyeVswXTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5vcHRpb25zLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm0udmFsdWVzW3RoaXMuaWRdID0gbnVsbDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIEZvcm1GaWVsZFR5cGVzLkRBVEU6XHJcbiAgICAgICAgICAgICAgICBjb25zdCBkYXRlVmFsdWUgPSBtb21lbnQodGhpcy52YWx1ZSwgdGhpcy5kYXRlRGlzcGxheUZvcm1hdCwgdHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICBpZiAoZGF0ZVZhbHVlICYmIGRhdGVWYWx1ZS5pc1ZhbGlkKCkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm0udmFsdWVzW3RoaXMuaWRdID0gYCR7ZGF0ZVZhbHVlLmZvcm1hdCgnWVlZWS1NTS1ERCcpfVQwMDowMDowMC4wMDBaYDtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mb3JtLnZhbHVlc1t0aGlzLmlkXSA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5fdmFsdWUgPSB0aGlzLnZhbHVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgRm9ybUZpZWxkVHlwZXMuREFURVRJTUU6XHJcbiAgICAgICAgICAgICAgICBjb25zdCBkYXRlVGltZVZhbHVlID0gbW9tZW50KHRoaXMudmFsdWUsIHRoaXMuZGF0ZURpc3BsYXlGb3JtYXQsIHRydWUpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGRhdGVUaW1lVmFsdWUgJiYgZGF0ZVRpbWVWYWx1ZS5pc1ZhbGlkKCkpIHtcclxuICAgICAgICAgICAgICAgICAgICAvKiBjc3BlbGw6ZGlzYWJsZS1uZXh0LWxpbmUgKi9cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm0udmFsdWVzW3RoaXMuaWRdID0gZGF0ZVRpbWVWYWx1ZS5mb3JtYXQoJ1lZWVktTU0tRERUSEg6bW06c3NaJyk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZm9ybS52YWx1ZXNbdGhpcy5pZF0gPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuX3ZhbHVlID0gdGhpcy52YWx1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIEZvcm1GaWVsZFR5cGVzLk5VTUJFUjpcclxuICAgICAgICAgICAgICAgIHRoaXMuZm9ybS52YWx1ZXNbdGhpcy5pZF0gPSBwYXJzZUludCh0aGlzLnZhbHVlLCAxMCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSBGb3JtRmllbGRUeXBlcy5BTU9VTlQ6XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZvcm0udmFsdWVzW3RoaXMuaWRdID0gdGhpcy5lbmFibGVGcmFjdGlvbnMgPyBwYXJzZUZsb2F0KHRoaXMudmFsdWUpIDogcGFyc2VJbnQodGhpcy52YWx1ZSwgMTApO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICBpZiAoIUZvcm1GaWVsZFR5cGVzLmlzUmVhZE9ubHlUeXBlKHRoaXMudHlwZSkgJiYgIXRoaXMuaXNJbnZhbGlkRmllbGRUeXBlKHRoaXMudHlwZSkpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZvcm0udmFsdWVzW3RoaXMuaWRdID0gdGhpcy52YWx1ZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuZm9ybS5vbkZvcm1GaWVsZENoYW5nZWQodGhpcyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTa2lwIHRoZSBpbnZhbGlkIGZpZWxkIHR5cGVcclxuICAgICAqIEBwYXJhbSB0eXBlXHJcbiAgICAgKi9cclxuICAgIGlzSW52YWxpZEZpZWxkVHlwZSh0eXBlOiBzdHJpbmcpIHtcclxuICAgICAgICBpZiAodHlwZSA9PT0gJ2NvbnRhaW5lcicpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRPcHRpb25OYW1lKCk6IHN0cmluZyB7XHJcbiAgICAgICAgY29uc3Qgb3B0aW9uOiBGb3JtRmllbGRPcHRpb24gPSB0aGlzLm9wdGlvbnMuZmluZCgob3B0KSA9PiBvcHQuaWQgPT09IHRoaXMudmFsdWUpO1xyXG4gICAgICAgIHJldHVybiBvcHRpb24gPyBvcHRpb24ubmFtZSA6IG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgaGFzT3B0aW9ucygpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5vcHRpb25zICYmIHRoaXMub3B0aW9ucy5sZW5ndGggPiAwO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaXNEYXRlRmllbGQoanNvbjogYW55KSB7XHJcbiAgICAgICAgcmV0dXJuIChqc29uLnBhcmFtcyAmJlxyXG4gICAgICAgICAgICBqc29uLnBhcmFtcy5maWVsZCAmJlxyXG4gICAgICAgICAgICBqc29uLnBhcmFtcy5maWVsZC50eXBlID09PSBGb3JtRmllbGRUeXBlcy5EQVRFKSB8fFxyXG4gICAgICAgICAgICBqc29uLnR5cGUgPT09IEZvcm1GaWVsZFR5cGVzLkRBVEU7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBpc0RhdGVUaW1lRmllbGQoanNvbjogYW55KTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIChqc29uLnBhcmFtcyAmJlxyXG4gICAgICAgICAgICBqc29uLnBhcmFtcy5maWVsZCAmJlxyXG4gICAgICAgICAgICBqc29uLnBhcmFtcy5maWVsZC50eXBlID09PSBGb3JtRmllbGRUeXBlcy5EQVRFVElNRSkgfHxcclxuICAgICAgICAgICAganNvbi50eXBlID09PSBGb3JtRmllbGRUeXBlcy5EQVRFVElNRTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19