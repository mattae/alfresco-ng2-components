/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
var FormOutcomeEvent = /** @class */ (function () {
    function FormOutcomeEvent(outcome) {
        this._defaultPrevented = false;
        this._outcome = outcome;
    }
    Object.defineProperty(FormOutcomeEvent.prototype, "outcome", {
        get: /**
         * @return {?}
         */
        function () {
            return this._outcome;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormOutcomeEvent.prototype, "defaultPrevented", {
        get: /**
         * @return {?}
         */
        function () {
            return this._defaultPrevented;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    FormOutcomeEvent.prototype.preventDefault = /**
     * @return {?}
     */
    function () {
        this._defaultPrevented = true;
    };
    return FormOutcomeEvent;
}());
export { FormOutcomeEvent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    FormOutcomeEvent.prototype._outcome;
    /**
     * @type {?}
     * @private
     */
    FormOutcomeEvent.prototype._defaultPrevented;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1vdXRjb21lLWV2ZW50Lm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvY29yZS9mb3JtLW91dGNvbWUtZXZlbnQubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBcUJBO0lBYUksMEJBQVksT0FBeUI7UUFWN0Isc0JBQWlCLEdBQVksS0FBSyxDQUFDO1FBV3ZDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDO0lBQzVCLENBQUM7SUFWRCxzQkFBSSxxQ0FBTzs7OztRQUFYO1lBQ0ksT0FBTyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3pCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksOENBQWdCOzs7O1FBQXBCO1lBQ0ksT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDbEMsQ0FBQzs7O09BQUE7Ozs7SUFNRCx5Q0FBYzs7O0lBQWQ7UUFDSSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO0lBQ2xDLENBQUM7SUFFTCx1QkFBQztBQUFELENBQUMsQUFyQkQsSUFxQkM7Ozs7Ozs7SUFuQkcsb0NBQW1DOzs7OztJQUNuQyw2Q0FBMkMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuIC8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IEZvcm1PdXRjb21lTW9kZWwgfSBmcm9tICcuL2Zvcm0tb3V0Y29tZS5tb2RlbCc7XHJcblxyXG5leHBvcnQgY2xhc3MgRm9ybU91dGNvbWVFdmVudCB7XHJcblxyXG4gICAgcHJpdmF0ZSBfb3V0Y29tZTogRm9ybU91dGNvbWVNb2RlbDtcclxuICAgIHByaXZhdGUgX2RlZmF1bHRQcmV2ZW50ZWQ6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBnZXQgb3V0Y29tZSgpOiBGb3JtT3V0Y29tZU1vZGVsIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fb3V0Y29tZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZGVmYXVsdFByZXZlbnRlZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fZGVmYXVsdFByZXZlbnRlZDtcclxuICAgIH1cclxuXHJcbiAgICBjb25zdHJ1Y3RvcihvdXRjb21lOiBGb3JtT3V0Y29tZU1vZGVsKSB7XHJcbiAgICAgICAgdGhpcy5fb3V0Y29tZSA9IG91dGNvbWU7XHJcbiAgICB9XHJcblxyXG4gICAgcHJldmVudERlZmF1bHQoKSB7XHJcbiAgICAgICAgdGhpcy5fZGVmYXVsdFByZXZlbnRlZCA9IHRydWU7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==