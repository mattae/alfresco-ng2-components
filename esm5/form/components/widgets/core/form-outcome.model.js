/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { FormWidgetModel } from './form-widget.model';
var FormOutcomeModel = /** @class */ (function (_super) {
    tslib_1.__extends(FormOutcomeModel, _super);
    function FormOutcomeModel(form, json) {
        var _this = _super.call(this, form, json) || this;
        // Activiti 'Start Process' action name
        _this.isSystem = false;
        _this.isSelected = false;
        if (json) {
            _this.isSystem = json.isSystem ? true : false;
            _this.isSelected = form && json.name === form.selectedOutcome ? true : false;
        }
        return _this;
    }
    FormOutcomeModel.SAVE_ACTION = 'SAVE'; // Activiti 'Save' action name
    // Activiti 'Save' action name
    FormOutcomeModel.COMPLETE_ACTION = 'COMPLETE'; // Activiti 'Complete' action name
    // Activiti 'Complete' action name
    FormOutcomeModel.START_PROCESS_ACTION = 'START PROCESS'; // Activiti 'Start Process' action name
    return FormOutcomeModel;
}(FormWidgetModel));
export { FormOutcomeModel };
if (false) {
    /** @type {?} */
    FormOutcomeModel.SAVE_ACTION;
    /** @type {?} */
    FormOutcomeModel.COMPLETE_ACTION;
    /** @type {?} */
    FormOutcomeModel.START_PROCESS_ACTION;
    /** @type {?} */
    FormOutcomeModel.prototype.isSystem;
    /** @type {?} */
    FormOutcomeModel.prototype.isSelected;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1vdXRjb21lLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvY29yZS9mb3JtLW91dGNvbWUubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFHdEQ7SUFBc0MsNENBQWU7SUFTakQsMEJBQVksSUFBZSxFQUFFLElBQVU7UUFBdkMsWUFDSSxrQkFBTSxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBTXBCOztRQVZELGNBQVEsR0FBWSxLQUFLLENBQUM7UUFDMUIsZ0JBQVUsR0FBWSxLQUFLLENBQUM7UUFLeEIsSUFBSSxJQUFJLEVBQUU7WUFDTixLQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1lBQzdDLEtBQUksQ0FBQyxVQUFVLEdBQUcsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLEtBQUssSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7U0FDL0U7O0lBQ0wsQ0FBQztJQWRNLDRCQUFXLEdBQVcsTUFBTSxDQUFDLENBQVksOEJBQThCOztJQUN2RSxnQ0FBZSxHQUFXLFVBQVUsQ0FBQyxDQUFJLGtDQUFrQzs7SUFDM0UscUNBQW9CLEdBQVcsZUFBZSxDQUFDLENBQUksdUNBQXVDO0lBYXJHLHVCQUFDO0NBQUEsQUFqQkQsQ0FBc0MsZUFBZSxHQWlCcEQ7U0FqQlksZ0JBQWdCOzs7SUFFekIsNkJBQW9DOztJQUNwQyxpQ0FBNEM7O0lBQzVDLHNDQUFzRDs7SUFFdEQsb0NBQTBCOztJQUMxQixzQ0FBNEIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuIC8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IEZvcm1XaWRnZXRNb2RlbCB9IGZyb20gJy4vZm9ybS13aWRnZXQubW9kZWwnO1xyXG5pbXBvcnQgeyBGb3JtTW9kZWwgfSBmcm9tICcuL2Zvcm0ubW9kZWwnO1xyXG5cclxuZXhwb3J0IGNsYXNzIEZvcm1PdXRjb21lTW9kZWwgZXh0ZW5kcyBGb3JtV2lkZ2V0TW9kZWwge1xyXG5cclxuICAgIHN0YXRpYyBTQVZFX0FDVElPTjogc3RyaW5nID0gJ1NBVkUnOyAgICAgICAgICAgIC8vIEFjdGl2aXRpICdTYXZlJyBhY3Rpb24gbmFtZVxyXG4gICAgc3RhdGljIENPTVBMRVRFX0FDVElPTjogc3RyaW5nID0gJ0NPTVBMRVRFJzsgICAgLy8gQWN0aXZpdGkgJ0NvbXBsZXRlJyBhY3Rpb24gbmFtZVxyXG4gICAgc3RhdGljIFNUQVJUX1BST0NFU1NfQUNUSU9OOiBzdHJpbmcgPSAnU1RBUlQgUFJPQ0VTUyc7ICAgIC8vIEFjdGl2aXRpICdTdGFydCBQcm9jZXNzJyBhY3Rpb24gbmFtZVxyXG5cclxuICAgIGlzU3lzdGVtOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBpc1NlbGVjdGVkOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgY29uc3RydWN0b3IoZm9ybTogRm9ybU1vZGVsLCBqc29uPzogYW55KSB7XHJcbiAgICAgICAgc3VwZXIoZm9ybSwganNvbik7XHJcblxyXG4gICAgICAgIGlmIChqc29uKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNTeXN0ZW0gPSBqc29uLmlzU3lzdGVtID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICB0aGlzLmlzU2VsZWN0ZWQgPSBmb3JtICYmIGpzb24ubmFtZSA9PT0gZm9ybS5zZWxlY3RlZE91dGNvbWUgPyB0cnVlIDogZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==