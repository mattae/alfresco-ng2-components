/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { FormFieldEvent } from './../../../events/form-field.event';
import { ValidateFormFieldEvent } from './../../../events/validate-form-field.event';
import { ValidateFormEvent } from './../../../events/validate-form.event';
import { ContainerModel } from './container.model';
import { FormFieldTypes } from './form-field-types';
import { FormFieldModel } from './form-field.model';
import { FormOutcomeModel } from './form-outcome.model';
import { TabModel } from './tab.model';
import { FORM_FIELD_VALIDATORS } from './form-field-validator';
import { FormBaseModel } from '../../form-base.model';
var FormModel = /** @class */ (function (_super) {
    tslib_1.__extends(FormModel, _super);
    function FormModel(formRepresentationJSON, formValues, readOnly, formService) {
        if (readOnly === void 0) { readOnly = false; }
        var _this = _super.call(this) || this;
        _this.formService = formService;
        _this.taskName = FormModel.UNSET_TASK_NAME;
        _this.customFieldTemplates = {};
        _this.fieldValidators = tslib_1.__spread(FORM_FIELD_VALIDATORS);
        _this.readOnly = readOnly;
        if (formRepresentationJSON) {
            _this.json = formRepresentationJSON;
            _this.id = formRepresentationJSON.id;
            _this.name = formRepresentationJSON.name;
            _this.taskId = formRepresentationJSON.taskId;
            _this.taskName = formRepresentationJSON.taskName || formRepresentationJSON.name || FormModel.UNSET_TASK_NAME;
            _this.processDefinitionId = formRepresentationJSON.processDefinitionId;
            _this.customFieldTemplates = formRepresentationJSON.customFieldTemplates || {};
            _this.selectedOutcome = formRepresentationJSON.selectedOutcome || {};
            _this.className = formRepresentationJSON.className || '';
            /** @type {?} */
            var tabCache_1 = {};
            _this.processVariables = formRepresentationJSON.processVariables;
            _this.tabs = (formRepresentationJSON.tabs || []).map((/**
             * @param {?} t
             * @return {?}
             */
            function (t) {
                /** @type {?} */
                var model = new TabModel(_this, t);
                tabCache_1[model.id] = model;
                return model;
            }));
            _this.fields = _this.parseRootFields(formRepresentationJSON);
            if (formValues) {
                _this.loadData(formValues);
            }
            for (var i = 0; i < _this.fields.length; i++) {
                /** @type {?} */
                var field = _this.fields[i];
                if (field.tab) {
                    /** @type {?} */
                    var tab = tabCache_1[field.tab];
                    if (tab) {
                        tab.fields.push(field);
                    }
                }
            }
            if (formRepresentationJSON.fields) {
                /** @type {?} */
                var saveOutcome = new FormOutcomeModel(_this, {
                    id: FormModel.SAVE_OUTCOME,
                    name: 'SAVE',
                    isSystem: true
                });
                /** @type {?} */
                var completeOutcome = new FormOutcomeModel(_this, {
                    id: FormModel.COMPLETE_OUTCOME,
                    name: 'COMPLETE',
                    isSystem: true
                });
                /** @type {?} */
                var startProcessOutcome = new FormOutcomeModel(_this, {
                    id: FormModel.START_PROCESS_OUTCOME,
                    name: 'START PROCESS',
                    isSystem: true
                });
                /** @type {?} */
                var customOutcomes = (formRepresentationJSON.outcomes || []).map((/**
                 * @param {?} obj
                 * @return {?}
                 */
                function (obj) { return new FormOutcomeModel(_this, obj); }));
                _this.outcomes = [saveOutcome].concat(customOutcomes.length > 0 ? customOutcomes : [completeOutcome, startProcessOutcome]);
            }
        }
        _this.validateForm();
        return _this;
    }
    /**
     * @param {?} field
     * @return {?}
     */
    FormModel.prototype.onFormFieldChanged = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        this.validateField(field);
        if (this.formService) {
            this.formService.formFieldValueChanged.next(new FormFieldEvent(this, field));
        }
    };
    /**
     * Validates entire form and all form fields.
     *
     * @memberof FormModel
     */
    /**
     * Validates entire form and all form fields.
     *
     * \@memberof FormModel
     * @return {?}
     */
    FormModel.prototype.validateForm = /**
     * Validates entire form and all form fields.
     *
     * \@memberof FormModel
     * @return {?}
     */
    function () {
        /** @type {?} */
        var validateFormEvent = new ValidateFormEvent(this);
        /** @type {?} */
        var errorsField = [];
        /** @type {?} */
        var fields = this.getFormFields();
        for (var i = 0; i < fields.length; i++) {
            if (!fields[i].validate()) {
                errorsField.push(fields[i]);
            }
        }
        this.isValid = errorsField.length > 0 ? false : true;
        if (this.formService) {
            validateFormEvent.isValid = this.isValid;
            validateFormEvent.errorsField = errorsField;
            this.formService.validateForm.next(validateFormEvent);
        }
    };
    /**
     * Validates a specific form field, triggers form validation.
     *
     * @param field Form field to validate.
     * @memberof FormModel
     */
    /**
     * Validates a specific form field, triggers form validation.
     *
     * \@memberof FormModel
     * @param {?} field Form field to validate.
     * @return {?}
     */
    FormModel.prototype.validateField = /**
     * Validates a specific form field, triggers form validation.
     *
     * \@memberof FormModel
     * @param {?} field Form field to validate.
     * @return {?}
     */
    function (field) {
        if (!field) {
            return;
        }
        /** @type {?} */
        var validateFieldEvent = new ValidateFormFieldEvent(this, field);
        if (this.formService) {
            this.formService.validateFormField.next(validateFieldEvent);
        }
        if (!validateFieldEvent.isValid) {
            this.markAsInvalid();
            return;
        }
        if (validateFieldEvent.defaultPrevented) {
            return;
        }
        if (!field.validate()) {
            this.markAsInvalid();
        }
        this.validateForm();
    };
    // Activiti supports 3 types of root fields: container|group|dynamic-table
    // Activiti supports 3 types of root fields: container|group|dynamic-table
    /**
     * @private
     * @param {?} json
     * @return {?}
     */
    FormModel.prototype.parseRootFields = 
    // Activiti supports 3 types of root fields: container|group|dynamic-table
    /**
     * @private
     * @param {?} json
     * @return {?}
     */
    function (json) {
        var e_1, _a;
        /** @type {?} */
        var fields = [];
        if (json.fields) {
            fields = json.fields;
        }
        else if (json.formDefinition && json.formDefinition.fields) {
            fields = json.formDefinition.fields;
        }
        /** @type {?} */
        var formWidgetModel = [];
        try {
            for (var fields_1 = tslib_1.__values(fields), fields_1_1 = fields_1.next(); !fields_1_1.done; fields_1_1 = fields_1.next()) {
                var field = fields_1_1.value;
                if (field.type === FormFieldTypes.DISPLAY_VALUE) {
                    // workaround for dynamic table on a completed/readonly form
                    if (field.params) {
                        /** @type {?} */
                        var originalField = field.params['field'];
                        if (originalField.type === FormFieldTypes.DYNAMIC_TABLE) {
                            formWidgetModel.push(new ContainerModel(new FormFieldModel(this, field)));
                        }
                    }
                }
                else {
                    formWidgetModel.push(new ContainerModel(new FormFieldModel(this, field)));
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (fields_1_1 && !fields_1_1.done && (_a = fields_1.return)) _a.call(fields_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return formWidgetModel;
    };
    // Loads external data and overrides field values
    // Typically used when form definition and form data coming from different sources
    // Loads external data and overrides field values
    // Typically used when form definition and form data coming from different sources
    /**
     * @private
     * @param {?} formValues
     * @return {?}
     */
    FormModel.prototype.loadData = 
    // Loads external data and overrides field values
    // Typically used when form definition and form data coming from different sources
    /**
     * @private
     * @param {?} formValues
     * @return {?}
     */
    function (formValues) {
        var e_2, _a;
        try {
            for (var _b = tslib_1.__values(this.getFormFields()), _c = _b.next(); !_c.done; _c = _b.next()) {
                var field = _c.value;
                if (formValues[field.id]) {
                    field.json.value = formValues[field.id];
                    field.value = field.parseValue(field.json);
                }
            }
        }
        catch (e_2_1) { e_2 = { error: e_2_1 }; }
        finally {
            try {
                if (_c && !_c.done && (_a = _b.return)) _a.call(_b);
            }
            finally { if (e_2) throw e_2.error; }
        }
    };
    return FormModel;
}(FormBaseModel));
export { FormModel };
if (false) {
    /** @type {?} */
    FormModel.prototype.id;
    /** @type {?} */
    FormModel.prototype.name;
    /** @type {?} */
    FormModel.prototype.taskId;
    /** @type {?} */
    FormModel.prototype.taskName;
    /** @type {?} */
    FormModel.prototype.processDefinitionId;
    /** @type {?} */
    FormModel.prototype.customFieldTemplates;
    /** @type {?} */
    FormModel.prototype.fieldValidators;
    /** @type {?} */
    FormModel.prototype.selectedOutcome;
    /** @type {?} */
    FormModel.prototype.processVariables;
    /**
     * @type {?}
     * @protected
     */
    FormModel.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2NvcmUvZm9ybS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUNwRSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUNyRixPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSx1Q0FBdUMsQ0FBQztBQUUxRSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFFbkQsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3BELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNwRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUd4RCxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBRXZDLE9BQU8sRUFDSCxxQkFBcUIsRUFFeEIsTUFBTSx3QkFBd0IsQ0FBQztBQUNoQyxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFFdEQ7SUFBK0IscUNBQWE7SUFjeEMsbUJBQVksc0JBQTRCLEVBQUUsVUFBdUIsRUFBRSxRQUF5QixFQUFZLFdBQXlCO1FBQTlELHlCQUFBLEVBQUEsZ0JBQXlCO1FBQTVGLFlBQ0ksaUJBQU8sU0FtRVY7UUFwRXVHLGlCQUFXLEdBQVgsV0FBVyxDQUFjO1FBVHhILGNBQVEsR0FBVyxTQUFTLENBQUMsZUFBZSxDQUFDO1FBR3RELDBCQUFvQixHQUF1QixFQUFFLENBQUM7UUFDOUMscUJBQWUsb0JBQTZCLHFCQUFxQixFQUFFO1FBTy9ELEtBQUksQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO1FBRXpCLElBQUksc0JBQXNCLEVBQUU7WUFDeEIsS0FBSSxDQUFDLElBQUksR0FBRyxzQkFBc0IsQ0FBQztZQUVuQyxLQUFJLENBQUMsRUFBRSxHQUFHLHNCQUFzQixDQUFDLEVBQUUsQ0FBQztZQUNwQyxLQUFJLENBQUMsSUFBSSxHQUFHLHNCQUFzQixDQUFDLElBQUksQ0FBQztZQUN4QyxLQUFJLENBQUMsTUFBTSxHQUFHLHNCQUFzQixDQUFDLE1BQU0sQ0FBQztZQUM1QyxLQUFJLENBQUMsUUFBUSxHQUFHLHNCQUFzQixDQUFDLFFBQVEsSUFBSSxzQkFBc0IsQ0FBQyxJQUFJLElBQUksU0FBUyxDQUFDLGVBQWUsQ0FBQztZQUM1RyxLQUFJLENBQUMsbUJBQW1CLEdBQUcsc0JBQXNCLENBQUMsbUJBQW1CLENBQUM7WUFDdEUsS0FBSSxDQUFDLG9CQUFvQixHQUFHLHNCQUFzQixDQUFDLG9CQUFvQixJQUFJLEVBQUUsQ0FBQztZQUM5RSxLQUFJLENBQUMsZUFBZSxHQUFHLHNCQUFzQixDQUFDLGVBQWUsSUFBSSxFQUFFLENBQUM7WUFDcEUsS0FBSSxDQUFDLFNBQVMsR0FBRyxzQkFBc0IsQ0FBQyxTQUFTLElBQUksRUFBRSxDQUFDOztnQkFFbEQsVUFBUSxHQUFtQyxFQUFFO1lBRW5ELEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxzQkFBc0IsQ0FBQyxnQkFBZ0IsQ0FBQztZQUVoRSxLQUFJLENBQUMsSUFBSSxHQUFHLENBQUMsc0JBQXNCLENBQUMsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDLEdBQUc7Ozs7WUFBQyxVQUFDLENBQUM7O29CQUM1QyxLQUFLLEdBQUcsSUFBSSxRQUFRLENBQUMsS0FBSSxFQUFFLENBQUMsQ0FBQztnQkFDbkMsVUFBUSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUM7Z0JBQzNCLE9BQU8sS0FBSyxDQUFDO1lBQ2pCLENBQUMsRUFBQyxDQUFDO1lBRUgsS0FBSSxDQUFDLE1BQU0sR0FBRyxLQUFJLENBQUMsZUFBZSxDQUFDLHNCQUFzQixDQUFDLENBQUM7WUFFM0QsSUFBSSxVQUFVLEVBQUU7Z0JBQ1osS0FBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUM3QjtZQUVELEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTs7b0JBQ25DLEtBQUssR0FBRyxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQztnQkFDNUIsSUFBSSxLQUFLLENBQUMsR0FBRyxFQUFFOzt3QkFDTCxHQUFHLEdBQUcsVUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7b0JBQy9CLElBQUksR0FBRyxFQUFFO3dCQUNMLEdBQUcsQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO3FCQUMxQjtpQkFDSjthQUNKO1lBRUQsSUFBSSxzQkFBc0IsQ0FBQyxNQUFNLEVBQUU7O29CQUN6QixXQUFXLEdBQUcsSUFBSSxnQkFBZ0IsQ0FBQyxLQUFJLEVBQUU7b0JBQzNDLEVBQUUsRUFBRSxTQUFTLENBQUMsWUFBWTtvQkFDMUIsSUFBSSxFQUFFLE1BQU07b0JBQ1osUUFBUSxFQUFFLElBQUk7aUJBQ2pCLENBQUM7O29CQUNJLGVBQWUsR0FBRyxJQUFJLGdCQUFnQixDQUFDLEtBQUksRUFBRTtvQkFDL0MsRUFBRSxFQUFFLFNBQVMsQ0FBQyxnQkFBZ0I7b0JBQzlCLElBQUksRUFBRSxVQUFVO29CQUNoQixRQUFRLEVBQUUsSUFBSTtpQkFDakIsQ0FBQzs7b0JBQ0ksbUJBQW1CLEdBQUcsSUFBSSxnQkFBZ0IsQ0FBQyxLQUFJLEVBQUU7b0JBQ25ELEVBQUUsRUFBRSxTQUFTLENBQUMscUJBQXFCO29CQUNuQyxJQUFJLEVBQUUsZUFBZTtvQkFDckIsUUFBUSxFQUFFLElBQUk7aUJBQ2pCLENBQUM7O29CQUVJLGNBQWMsR0FBRyxDQUFDLHNCQUFzQixDQUFDLFFBQVEsSUFBSSxFQUFFLENBQUMsQ0FBQyxHQUFHOzs7O2dCQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsSUFBSSxnQkFBZ0IsQ0FBQyxLQUFJLEVBQUUsR0FBRyxDQUFDLEVBQS9CLENBQStCLEVBQUM7Z0JBRTVHLEtBQUksQ0FBQyxRQUFRLEdBQUcsQ0FBQyxXQUFXLENBQUMsQ0FBQyxNQUFNLENBQ2hDLGNBQWMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxjQUFjLENBQUMsQ0FBQyxDQUFDLENBQUMsZUFBZSxFQUFFLG1CQUFtQixDQUFDLENBQ3RGLENBQUM7YUFDTDtTQUNKO1FBRUQsS0FBSSxDQUFDLFlBQVksRUFBRSxDQUFDOztJQUN4QixDQUFDOzs7OztJQUVELHNDQUFrQjs7OztJQUFsQixVQUFtQixLQUFxQjtRQUNwQyxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzFCLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNsQixJQUFJLENBQUMsV0FBVyxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxJQUFJLGNBQWMsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUMsQ0FBQztTQUNoRjtJQUNMLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsZ0NBQVk7Ozs7OztJQUFaOztZQUNVLGlCQUFpQixHQUFRLElBQUksaUJBQWlCLENBQUMsSUFBSSxDQUFDOztZQUVwRCxXQUFXLEdBQXFCLEVBQUU7O1lBRWxDLE1BQU0sR0FBRyxJQUFJLENBQUMsYUFBYSxFQUFFO1FBQ25DLEtBQUssSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFLENBQUMsR0FBRyxNQUFNLENBQUMsTUFBTSxFQUFFLENBQUMsRUFBRSxFQUFFO1lBQ3BDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxFQUFFLEVBQUU7Z0JBQ3ZCLFdBQVcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7YUFDL0I7U0FDSjtRQUVELElBQUksQ0FBQyxPQUFPLEdBQUcsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO1FBRXJELElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNsQixpQkFBaUIsQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztZQUN6QyxpQkFBaUIsQ0FBQyxXQUFXLEdBQUcsV0FBVyxDQUFDO1lBQzVDLElBQUksQ0FBQyxXQUFXLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1NBQ3pEO0lBRUwsQ0FBQztJQUVEOzs7OztPQUtHOzs7Ozs7OztJQUNILGlDQUFhOzs7Ozs7O0lBQWIsVUFBYyxLQUFxQjtRQUMvQixJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1IsT0FBTztTQUNWOztZQUVLLGtCQUFrQixHQUFHLElBQUksc0JBQXNCLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQztRQUVsRSxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztTQUMvRDtRQUVELElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLEVBQUU7WUFDN0IsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQ3JCLE9BQU87U0FDVjtRQUVELElBQUksa0JBQWtCLENBQUMsZ0JBQWdCLEVBQUU7WUFDckMsT0FBTztTQUNWO1FBRUQsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsRUFBRTtZQUNuQixJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7U0FDeEI7UUFFRCxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDeEIsQ0FBQztJQUVELDBFQUEwRTs7Ozs7OztJQUNsRSxtQ0FBZTs7Ozs7OztJQUF2QixVQUF3QixJQUFTOzs7WUFDekIsTUFBTSxHQUFHLEVBQUU7UUFFZixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztTQUN4QjthQUFNLElBQUksSUFBSSxDQUFDLGNBQWMsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sRUFBRTtZQUMxRCxNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7U0FDdkM7O1lBRUssZUFBZSxHQUFzQixFQUFFOztZQUU3QyxLQUFvQixJQUFBLFdBQUEsaUJBQUEsTUFBTSxDQUFBLDhCQUFBLGtEQUFFO2dCQUF2QixJQUFNLEtBQUssbUJBQUE7Z0JBQ1osSUFBSSxLQUFLLENBQUMsSUFBSSxLQUFLLGNBQWMsQ0FBQyxhQUFhLEVBQUU7b0JBQzdDLDREQUE0RDtvQkFDNUQsSUFBSSxLQUFLLENBQUMsTUFBTSxFQUFFOzs0QkFDUixhQUFhLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUM7d0JBQzNDLElBQUksYUFBYSxDQUFDLElBQUksS0FBSyxjQUFjLENBQUMsYUFBYSxFQUFFOzRCQUNyRCxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksY0FBYyxDQUFDLElBQUksY0FBYyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7eUJBQzdFO3FCQUNKO2lCQUNKO3FCQUFNO29CQUNILGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxjQUFjLENBQUMsSUFBSSxjQUFjLENBQUMsSUFBSSxFQUFFLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztpQkFDN0U7YUFDSjs7Ozs7Ozs7O1FBRUQsT0FBTyxlQUFlLENBQUM7SUFDM0IsQ0FBQztJQUVELGlEQUFpRDtJQUNqRCxrRkFBa0Y7Ozs7Ozs7O0lBQzFFLDRCQUFROzs7Ozs7OztJQUFoQixVQUFpQixVQUFzQjs7O1lBQ25DLEtBQW9CLElBQUEsS0FBQSxpQkFBQSxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUEsZ0JBQUEsNEJBQUU7Z0JBQXJDLElBQU0sS0FBSyxXQUFBO2dCQUNaLElBQUksVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsRUFBRTtvQkFDdEIsS0FBSyxDQUFDLElBQUksQ0FBQyxLQUFLLEdBQUcsVUFBVSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsQ0FBQztvQkFDeEMsS0FBSyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDOUM7YUFDSjs7Ozs7Ozs7O0lBQ0wsQ0FBQztJQUNMLGdCQUFDO0FBQUQsQ0FBQyxBQTlMRCxDQUErQixhQUFhLEdBOEwzQzs7OztJQTVMRyx1QkFBb0I7O0lBQ3BCLHlCQUFzQjs7SUFDdEIsMkJBQXdCOztJQUN4Qiw2QkFBc0Q7O0lBQ3RELHdDQUE0Qjs7SUFFNUIseUNBQThDOztJQUM5QyxvQ0FBbUU7O0lBQ25FLG9DQUFpQzs7SUFFakMscUNBQXNCOzs7OztJQUV3RSxnQ0FBbUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgRm9ybUZpZWxkRXZlbnQgfSBmcm9tICcuLy4uLy4uLy4uL2V2ZW50cy9mb3JtLWZpZWxkLmV2ZW50JztcclxuaW1wb3J0IHsgVmFsaWRhdGVGb3JtRmllbGRFdmVudCB9IGZyb20gJy4vLi4vLi4vLi4vZXZlbnRzL3ZhbGlkYXRlLWZvcm0tZmllbGQuZXZlbnQnO1xyXG5pbXBvcnQgeyBWYWxpZGF0ZUZvcm1FdmVudCB9IGZyb20gJy4vLi4vLi4vLi4vZXZlbnRzL3ZhbGlkYXRlLWZvcm0uZXZlbnQnO1xyXG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4vLi4vLi4vLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ29udGFpbmVyTW9kZWwgfSBmcm9tICcuL2NvbnRhaW5lci5tb2RlbCc7XHJcbmltcG9ydCB7IEZvcm1GaWVsZFRlbXBsYXRlcyB9IGZyb20gJy4vZm9ybS1maWVsZC10ZW1wbGF0ZXMnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRUeXBlcyB9IGZyb20gJy4vZm9ybS1maWVsZC10eXBlcyc7XHJcbmltcG9ydCB7IEZvcm1GaWVsZE1vZGVsIH0gZnJvbSAnLi9mb3JtLWZpZWxkLm1vZGVsJztcclxuaW1wb3J0IHsgRm9ybU91dGNvbWVNb2RlbCB9IGZyb20gJy4vZm9ybS1vdXRjb21lLm1vZGVsJztcclxuaW1wb3J0IHsgRm9ybVZhbHVlcyB9IGZyb20gJy4vZm9ybS12YWx1ZXMnO1xyXG5pbXBvcnQgeyBGb3JtV2lkZ2V0TW9kZWwsIEZvcm1XaWRnZXRNb2RlbENhY2hlIH0gZnJvbSAnLi9mb3JtLXdpZGdldC5tb2RlbCc7XHJcbmltcG9ydCB7IFRhYk1vZGVsIH0gZnJvbSAnLi90YWIubW9kZWwnO1xyXG5cclxuaW1wb3J0IHtcclxuICAgIEZPUk1fRklFTERfVkFMSURBVE9SUyxcclxuICAgIEZvcm1GaWVsZFZhbGlkYXRvclxyXG59IGZyb20gJy4vZm9ybS1maWVsZC12YWxpZGF0b3InO1xyXG5pbXBvcnQgeyBGb3JtQmFzZU1vZGVsIH0gZnJvbSAnLi4vLi4vZm9ybS1iYXNlLm1vZGVsJztcclxuXHJcbmV4cG9ydCBjbGFzcyBGb3JtTW9kZWwgZXh0ZW5kcyBGb3JtQmFzZU1vZGVsIHtcclxuXHJcbiAgICByZWFkb25seSBpZDogbnVtYmVyO1xyXG4gICAgcmVhZG9ubHkgbmFtZTogc3RyaW5nO1xyXG4gICAgcmVhZG9ubHkgdGFza0lkOiBzdHJpbmc7XHJcbiAgICByZWFkb25seSB0YXNrTmFtZTogc3RyaW5nID0gRm9ybU1vZGVsLlVOU0VUX1RBU0tfTkFNRTtcclxuICAgIHByb2Nlc3NEZWZpbml0aW9uSWQ6IHN0cmluZztcclxuXHJcbiAgICBjdXN0b21GaWVsZFRlbXBsYXRlczogRm9ybUZpZWxkVGVtcGxhdGVzID0ge307XHJcbiAgICBmaWVsZFZhbGlkYXRvcnM6IEZvcm1GaWVsZFZhbGlkYXRvcltdID0gWy4uLkZPUk1fRklFTERfVkFMSURBVE9SU107XHJcbiAgICByZWFkb25seSBzZWxlY3RlZE91dGNvbWU6IHN0cmluZztcclxuXHJcbiAgICBwcm9jZXNzVmFyaWFibGVzOiBhbnk7XHJcblxyXG4gICAgY29uc3RydWN0b3IoZm9ybVJlcHJlc2VudGF0aW9uSlNPTj86IGFueSwgZm9ybVZhbHVlcz86IEZvcm1WYWx1ZXMsIHJlYWRPbmx5OiBib29sZWFuID0gZmFsc2UsIHByb3RlY3RlZCBmb3JtU2VydmljZT86IEZvcm1TZXJ2aWNlKSB7XHJcbiAgICAgICAgc3VwZXIoKTtcclxuICAgICAgICB0aGlzLnJlYWRPbmx5ID0gcmVhZE9ubHk7XHJcblxyXG4gICAgICAgIGlmIChmb3JtUmVwcmVzZW50YXRpb25KU09OKSB7XHJcbiAgICAgICAgICAgIHRoaXMuanNvbiA9IGZvcm1SZXByZXNlbnRhdGlvbkpTT047XHJcblxyXG4gICAgICAgICAgICB0aGlzLmlkID0gZm9ybVJlcHJlc2VudGF0aW9uSlNPTi5pZDtcclxuICAgICAgICAgICAgdGhpcy5uYW1lID0gZm9ybVJlcHJlc2VudGF0aW9uSlNPTi5uYW1lO1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tJZCA9IGZvcm1SZXByZXNlbnRhdGlvbkpTT04udGFza0lkO1xyXG4gICAgICAgICAgICB0aGlzLnRhc2tOYW1lID0gZm9ybVJlcHJlc2VudGF0aW9uSlNPTi50YXNrTmFtZSB8fCBmb3JtUmVwcmVzZW50YXRpb25KU09OLm5hbWUgfHwgRm9ybU1vZGVsLlVOU0VUX1RBU0tfTkFNRTtcclxuICAgICAgICAgICAgdGhpcy5wcm9jZXNzRGVmaW5pdGlvbklkID0gZm9ybVJlcHJlc2VudGF0aW9uSlNPTi5wcm9jZXNzRGVmaW5pdGlvbklkO1xyXG4gICAgICAgICAgICB0aGlzLmN1c3RvbUZpZWxkVGVtcGxhdGVzID0gZm9ybVJlcHJlc2VudGF0aW9uSlNPTi5jdXN0b21GaWVsZFRlbXBsYXRlcyB8fCB7fTtcclxuICAgICAgICAgICAgdGhpcy5zZWxlY3RlZE91dGNvbWUgPSBmb3JtUmVwcmVzZW50YXRpb25KU09OLnNlbGVjdGVkT3V0Y29tZSB8fCB7fTtcclxuICAgICAgICAgICAgdGhpcy5jbGFzc05hbWUgPSBmb3JtUmVwcmVzZW50YXRpb25KU09OLmNsYXNzTmFtZSB8fCAnJztcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHRhYkNhY2hlOiBGb3JtV2lkZ2V0TW9kZWxDYWNoZTxUYWJNb2RlbD4gPSB7fTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMucHJvY2Vzc1ZhcmlhYmxlcyA9IGZvcm1SZXByZXNlbnRhdGlvbkpTT04ucHJvY2Vzc1ZhcmlhYmxlcztcclxuXHJcbiAgICAgICAgICAgIHRoaXMudGFicyA9IChmb3JtUmVwcmVzZW50YXRpb25KU09OLnRhYnMgfHwgW10pLm1hcCgodCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbW9kZWwgPSBuZXcgVGFiTW9kZWwodGhpcywgdCk7XHJcbiAgICAgICAgICAgICAgICB0YWJDYWNoZVttb2RlbC5pZF0gPSBtb2RlbDtcclxuICAgICAgICAgICAgICAgIHJldHVybiBtb2RlbDtcclxuICAgICAgICAgICAgfSk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLmZpZWxkcyA9IHRoaXMucGFyc2VSb290RmllbGRzKGZvcm1SZXByZXNlbnRhdGlvbkpTT04pO1xyXG5cclxuICAgICAgICAgICAgaWYgKGZvcm1WYWx1ZXMpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubG9hZERhdGEoZm9ybVZhbHVlcyk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5maWVsZHMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IGZpZWxkID0gdGhpcy5maWVsZHNbaV07XHJcbiAgICAgICAgICAgICAgICBpZiAoZmllbGQudGFiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgdGFiID0gdGFiQ2FjaGVbZmllbGQudGFiXTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGFiKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhYi5maWVsZHMucHVzaChmaWVsZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoZm9ybVJlcHJlc2VudGF0aW9uSlNPTi5maWVsZHMpIHtcclxuICAgICAgICAgICAgICAgIGNvbnN0IHNhdmVPdXRjb21lID0gbmV3IEZvcm1PdXRjb21lTW9kZWwodGhpcywge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiBGb3JtTW9kZWwuU0FWRV9PVVRDT01FLFxyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6ICdTQVZFJyxcclxuICAgICAgICAgICAgICAgICAgICBpc1N5c3RlbTogdHJ1ZVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBjb21wbGV0ZU91dGNvbWUgPSBuZXcgRm9ybU91dGNvbWVNb2RlbCh0aGlzLCB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWQ6IEZvcm1Nb2RlbC5DT01QTEVURV9PVVRDT01FLFxyXG4gICAgICAgICAgICAgICAgICAgIG5hbWU6ICdDT01QTEVURScsXHJcbiAgICAgICAgICAgICAgICAgICAgaXNTeXN0ZW06IHRydWVcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgc3RhcnRQcm9jZXNzT3V0Y29tZSA9IG5ldyBGb3JtT3V0Y29tZU1vZGVsKHRoaXMsIHtcclxuICAgICAgICAgICAgICAgICAgICBpZDogRm9ybU1vZGVsLlNUQVJUX1BST0NFU1NfT1VUQ09NRSxcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiAnU1RBUlQgUFJPQ0VTUycsXHJcbiAgICAgICAgICAgICAgICAgICAgaXNTeXN0ZW06IHRydWVcclxuICAgICAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IGN1c3RvbU91dGNvbWVzID0gKGZvcm1SZXByZXNlbnRhdGlvbkpTT04ub3V0Y29tZXMgfHwgW10pLm1hcCgob2JqKSA9PiBuZXcgRm9ybU91dGNvbWVNb2RlbCh0aGlzLCBvYmopKTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLm91dGNvbWVzID0gW3NhdmVPdXRjb21lXS5jb25jYXQoXHJcbiAgICAgICAgICAgICAgICAgICAgY3VzdG9tT3V0Y29tZXMubGVuZ3RoID4gMCA/IGN1c3RvbU91dGNvbWVzIDogW2NvbXBsZXRlT3V0Y29tZSwgc3RhcnRQcm9jZXNzT3V0Y29tZV1cclxuICAgICAgICAgICAgICAgICk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMudmFsaWRhdGVGb3JtKCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25Gb3JtRmllbGRDaGFuZ2VkKGZpZWxkOiBGb3JtRmllbGRNb2RlbCkge1xyXG4gICAgICAgIHRoaXMudmFsaWRhdGVGaWVsZChmaWVsZCk7XHJcbiAgICAgICAgaWYgKHRoaXMuZm9ybVNlcnZpY2UpIHtcclxuICAgICAgICAgICAgdGhpcy5mb3JtU2VydmljZS5mb3JtRmllbGRWYWx1ZUNoYW5nZWQubmV4dChuZXcgRm9ybUZpZWxkRXZlbnQodGhpcywgZmllbGQpKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBWYWxpZGF0ZXMgZW50aXJlIGZvcm0gYW5kIGFsbCBmb3JtIGZpZWxkcy5cclxuICAgICAqXHJcbiAgICAgKiBAbWVtYmVyb2YgRm9ybU1vZGVsXHJcbiAgICAgKi9cclxuICAgIHZhbGlkYXRlRm9ybSgpOiB2b2lkIHtcclxuICAgICAgICBjb25zdCB2YWxpZGF0ZUZvcm1FdmVudDogYW55ID0gbmV3IFZhbGlkYXRlRm9ybUV2ZW50KHRoaXMpO1xyXG5cclxuICAgICAgICBjb25zdCBlcnJvcnNGaWVsZDogRm9ybUZpZWxkTW9kZWxbXSA9IFtdO1xyXG5cclxuICAgICAgICBjb25zdCBmaWVsZHMgPSB0aGlzLmdldEZvcm1GaWVsZHMoKTtcclxuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IGZpZWxkcy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICBpZiAoIWZpZWxkc1tpXS52YWxpZGF0ZSgpKSB7XHJcbiAgICAgICAgICAgICAgICBlcnJvcnNGaWVsZC5wdXNoKGZpZWxkc1tpXSk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuaXNWYWxpZCA9IGVycm9yc0ZpZWxkLmxlbmd0aCA+IDAgPyBmYWxzZSA6IHRydWU7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmZvcm1TZXJ2aWNlKSB7XHJcbiAgICAgICAgICAgIHZhbGlkYXRlRm9ybUV2ZW50LmlzVmFsaWQgPSB0aGlzLmlzVmFsaWQ7XHJcbiAgICAgICAgICAgIHZhbGlkYXRlRm9ybUV2ZW50LmVycm9yc0ZpZWxkID0gZXJyb3JzRmllbGQ7XHJcbiAgICAgICAgICAgIHRoaXMuZm9ybVNlcnZpY2UudmFsaWRhdGVGb3JtLm5leHQodmFsaWRhdGVGb3JtRXZlbnQpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBWYWxpZGF0ZXMgYSBzcGVjaWZpYyBmb3JtIGZpZWxkLCB0cmlnZ2VycyBmb3JtIHZhbGlkYXRpb24uXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIGZpZWxkIEZvcm0gZmllbGQgdG8gdmFsaWRhdGUuXHJcbiAgICAgKiBAbWVtYmVyb2YgRm9ybU1vZGVsXHJcbiAgICAgKi9cclxuICAgIHZhbGlkYXRlRmllbGQoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKCFmaWVsZCkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB2YWxpZGF0ZUZpZWxkRXZlbnQgPSBuZXcgVmFsaWRhdGVGb3JtRmllbGRFdmVudCh0aGlzLCBmaWVsZCk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmZvcm1TZXJ2aWNlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZm9ybVNlcnZpY2UudmFsaWRhdGVGb3JtRmllbGQubmV4dCh2YWxpZGF0ZUZpZWxkRXZlbnQpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCF2YWxpZGF0ZUZpZWxkRXZlbnQuaXNWYWxpZCkge1xyXG4gICAgICAgICAgICB0aGlzLm1hcmtBc0ludmFsaWQoKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHZhbGlkYXRlRmllbGRFdmVudC5kZWZhdWx0UHJldmVudGVkKSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghZmllbGQudmFsaWRhdGUoKSkge1xyXG4gICAgICAgICAgICB0aGlzLm1hcmtBc0ludmFsaWQoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMudmFsaWRhdGVGb3JtKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLy8gQWN0aXZpdGkgc3VwcG9ydHMgMyB0eXBlcyBvZiByb290IGZpZWxkczogY29udGFpbmVyfGdyb3VwfGR5bmFtaWMtdGFibGVcclxuICAgIHByaXZhdGUgcGFyc2VSb290RmllbGRzKGpzb246IGFueSk6IEZvcm1XaWRnZXRNb2RlbFtdIHtcclxuICAgICAgICBsZXQgZmllbGRzID0gW107XHJcblxyXG4gICAgICAgIGlmIChqc29uLmZpZWxkcykge1xyXG4gICAgICAgICAgICBmaWVsZHMgPSBqc29uLmZpZWxkcztcclxuICAgICAgICB9IGVsc2UgaWYgKGpzb24uZm9ybURlZmluaXRpb24gJiYganNvbi5mb3JtRGVmaW5pdGlvbi5maWVsZHMpIHtcclxuICAgICAgICAgICAgZmllbGRzID0ganNvbi5mb3JtRGVmaW5pdGlvbi5maWVsZHM7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCBmb3JtV2lkZ2V0TW9kZWw6IEZvcm1XaWRnZXRNb2RlbFtdID0gW107XHJcblxyXG4gICAgICAgIGZvciAoY29uc3QgZmllbGQgb2YgZmllbGRzKSB7XHJcbiAgICAgICAgICAgIGlmIChmaWVsZC50eXBlID09PSBGb3JtRmllbGRUeXBlcy5ESVNQTEFZX1ZBTFVFKSB7XHJcbiAgICAgICAgICAgICAgICAvLyB3b3JrYXJvdW5kIGZvciBkeW5hbWljIHRhYmxlIG9uIGEgY29tcGxldGVkL3JlYWRvbmx5IGZvcm1cclxuICAgICAgICAgICAgICAgIGlmIChmaWVsZC5wYXJhbXMpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBvcmlnaW5hbEZpZWxkID0gZmllbGQucGFyYW1zWydmaWVsZCddO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChvcmlnaW5hbEZpZWxkLnR5cGUgPT09IEZvcm1GaWVsZFR5cGVzLkRZTkFNSUNfVEFCTEUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZm9ybVdpZGdldE1vZGVsLnB1c2gobmV3IENvbnRhaW5lck1vZGVsKG5ldyBGb3JtRmllbGRNb2RlbCh0aGlzLCBmaWVsZCkpKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBmb3JtV2lkZ2V0TW9kZWwucHVzaChuZXcgQ29udGFpbmVyTW9kZWwobmV3IEZvcm1GaWVsZE1vZGVsKHRoaXMsIGZpZWxkKSkpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZm9ybVdpZGdldE1vZGVsO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIExvYWRzIGV4dGVybmFsIGRhdGEgYW5kIG92ZXJyaWRlcyBmaWVsZCB2YWx1ZXNcclxuICAgIC8vIFR5cGljYWxseSB1c2VkIHdoZW4gZm9ybSBkZWZpbml0aW9uIGFuZCBmb3JtIGRhdGEgY29taW5nIGZyb20gZGlmZmVyZW50IHNvdXJjZXNcclxuICAgIHByaXZhdGUgbG9hZERhdGEoZm9ybVZhbHVlczogRm9ybVZhbHVlcykge1xyXG4gICAgICAgIGZvciAoY29uc3QgZmllbGQgb2YgdGhpcy5nZXRGb3JtRmllbGRzKCkpIHtcclxuICAgICAgICAgICAgaWYgKGZvcm1WYWx1ZXNbZmllbGQuaWRdKSB7XHJcbiAgICAgICAgICAgICAgICBmaWVsZC5qc29uLnZhbHVlID0gZm9ybVZhbHVlc1tmaWVsZC5pZF07XHJcbiAgICAgICAgICAgICAgICBmaWVsZC52YWx1ZSA9IGZpZWxkLnBhcnNlVmFsdWUoZmllbGQuanNvbik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19