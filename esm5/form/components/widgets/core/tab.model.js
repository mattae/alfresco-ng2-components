/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { FormWidgetModel } from './form-widget.model';
var TabModel = /** @class */ (function (_super) {
    tslib_1.__extends(TabModel, _super);
    function TabModel(form, json) {
        var _this = _super.call(this, form, json) || this;
        _this.isVisible = true;
        _this.fields = [];
        if (json) {
            _this.title = json.title;
            _this.visibilityCondition = (/** @type {?} */ (json.visibilityCondition));
        }
        return _this;
    }
    /**
     * @return {?}
     */
    TabModel.prototype.hasContent = /**
     * @return {?}
     */
    function () {
        return this.fields && this.fields.length > 0;
    };
    return TabModel;
}(FormWidgetModel));
export { TabModel };
if (false) {
    /** @type {?} */
    TabModel.prototype.title;
    /** @type {?} */
    TabModel.prototype.isVisible;
    /** @type {?} */
    TabModel.prototype.visibilityCondition;
    /** @type {?} */
    TabModel.prototype.fields;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGFiLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvY29yZS90YWIubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW9CQSxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFHdEQ7SUFBOEIsb0NBQWU7SUFZekMsa0JBQVksSUFBZSxFQUFFLElBQVU7UUFBdkMsWUFDSSxrQkFBTSxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBTXBCO1FBaEJELGVBQVMsR0FBWSxJQUFJLENBQUM7UUFHMUIsWUFBTSxHQUFzQixFQUFFLENBQUM7UUFTM0IsSUFBSSxJQUFJLEVBQUU7WUFDTixLQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUM7WUFDeEIsS0FBSSxDQUFDLG1CQUFtQixHQUFHLG1CQUF3QixJQUFJLENBQUMsbUJBQW1CLEVBQUEsQ0FBQztTQUMvRTs7SUFDTCxDQUFDOzs7O0lBWEQsNkJBQVU7OztJQUFWO1FBQ0ksT0FBTyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBVUwsZUFBQztBQUFELENBQUMsQUFwQkQsQ0FBOEIsZUFBZSxHQW9CNUM7Ozs7SUFsQkcseUJBQWM7O0lBQ2QsNkJBQTBCOztJQUMxQix1Q0FBMkM7O0lBRTNDLDBCQUErQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4gLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgV2lkZ2V0VmlzaWJpbGl0eU1vZGVsIH0gZnJvbSAnLi4vLi4vLi4vbW9kZWxzL3dpZGdldC12aXNpYmlsaXR5Lm1vZGVsJztcclxuaW1wb3J0IHsgRm9ybVdpZGdldE1vZGVsIH0gZnJvbSAnLi9mb3JtLXdpZGdldC5tb2RlbCc7XHJcbmltcG9ydCB7IEZvcm1Nb2RlbCB9IGZyb20gJy4vZm9ybS5tb2RlbCc7XHJcblxyXG5leHBvcnQgY2xhc3MgVGFiTW9kZWwgZXh0ZW5kcyBGb3JtV2lkZ2V0TW9kZWwge1xyXG5cclxuICAgIHRpdGxlOiBzdHJpbmc7XHJcbiAgICBpc1Zpc2libGU6IGJvb2xlYW4gPSB0cnVlO1xyXG4gICAgdmlzaWJpbGl0eUNvbmRpdGlvbjogV2lkZ2V0VmlzaWJpbGl0eU1vZGVsO1xyXG5cclxuICAgIGZpZWxkczogRm9ybVdpZGdldE1vZGVsW10gPSBbXTtcclxuXHJcbiAgICBoYXNDb250ZW50KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZpZWxkcyAmJiB0aGlzLmZpZWxkcy5sZW5ndGggPiAwO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKGZvcm06IEZvcm1Nb2RlbCwganNvbj86IGFueSkge1xyXG4gICAgICAgIHN1cGVyKGZvcm0sIGpzb24pO1xyXG5cclxuICAgICAgICBpZiAoanNvbikge1xyXG4gICAgICAgICAgICB0aGlzLnRpdGxlID0ganNvbi50aXRsZTtcclxuICAgICAgICAgICAgdGhpcy52aXNpYmlsaXR5Q29uZGl0aW9uID0gPFdpZGdldFZpc2liaWxpdHlNb2RlbD4ganNvbi52aXNpYmlsaXR5Q29uZGl0aW9uO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=