/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, ViewEncapsulation } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import { DatetimeAdapter, MAT_DATETIME_FORMATS } from '@mat-datetimepicker/core';
import { MomentDatetimeAdapter, MAT_MOMENT_DATETIME_FORMATS } from '@mat-datetimepicker/moment';
import moment from 'moment-es6';
import { UserPreferencesService, UserPreferenceValues } from '../../../../services/user-preferences.service';
import { MomentDateAdapter } from '../../../../utils/momentDateAdapter';
import { MOMENT_DATE_FORMATS } from '../../../../utils/moment-date-formats.model';
import { FormService } from './../../../services/form.service';
import { WidgetComponent } from './../widget.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
var ɵ0 = MOMENT_DATE_FORMATS, ɵ1 = MAT_MOMENT_DATETIME_FORMATS;
var DateTimeWidgetComponent = /** @class */ (function (_super) {
    tslib_1.__extends(DateTimeWidgetComponent, _super);
    function DateTimeWidgetComponent(formService, dateAdapter, userPreferencesService) {
        var _this = _super.call(this, formService) || this;
        _this.formService = formService;
        _this.dateAdapter = dateAdapter;
        _this.userPreferencesService = userPreferencesService;
        _this.onDestroy$ = new Subject();
        return _this;
    }
    /**
     * @return {?}
     */
    DateTimeWidgetComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.userPreferencesService
            .select(UserPreferenceValues.Locale)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} locale
         * @return {?}
         */
        function (locale) { return _this.dateAdapter.setLocale(locale); }));
        /** @type {?} */
        var momentDateAdapter = (/** @type {?} */ (this.dateAdapter));
        momentDateAdapter.overrideDisplayFormat = this.field.dateDisplayFormat;
        if (this.field) {
            if (this.field.minValue) {
                this.minDate = moment(this.field.minValue, 'YYYY-MM-DDTHH:mm:ssZ');
            }
            if (this.field.maxValue) {
                this.maxDate = moment(this.field.maxValue, 'YYYY-MM-DDTHH:mm:ssZ');
            }
        }
        this.displayDate = moment(this.field.value);
    };
    /**
     * @return {?}
     */
    DateTimeWidgetComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    };
    /**
     * @param {?} newDateValue
     * @return {?}
     */
    DateTimeWidgetComponent.prototype.onDateChanged = /**
     * @param {?} newDateValue
     * @return {?}
     */
    function (newDateValue) {
        if (newDateValue && newDateValue.value) {
            this.field.value = newDateValue.value.format(this.field.dateDisplayFormat);
        }
        else if (newDateValue) {
            this.field.value = newDateValue;
        }
        else {
            this.field.value = null;
        }
        this.onFieldChanged(this.field);
    };
    DateTimeWidgetComponent.decorators = [
        { type: Component, args: [{
                    providers: [
                        { provide: DateAdapter, useClass: MomentDateAdapter },
                        { provide: MAT_DATE_FORMATS, useValue: ɵ0 },
                        { provide: DatetimeAdapter, useClass: MomentDatetimeAdapter },
                        { provide: MAT_DATETIME_FORMATS, useValue: ɵ1 }
                    ],
                    selector: 'date-time-widget',
                    template: "<div class=\"{{field.className}}\"\r\n     id=\"data-time-widget\" [class.adf-invalid]=\"!field.isValid\">\r\n    <mat-form-field class=\"adf-date-time-widget\">\r\n        <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }} ({{field.dateDisplayFormat}})<span *ngIf=\"isRequired()\">*</span></label>\r\n        <input matInput\r\n               [matDatetimepicker]=\"datetimePicker\"\r\n               [id]=\"field.id\"\r\n               [(ngModel)]=\"displayDate\"\r\n               [required]=\"isRequired()\"\r\n               [disabled]=\"field.readOnly\"\r\n               [min]=\"minDate\"\r\n               [max]=\"maxDate\"\r\n               (focusout)=\"onDateChanged($event.srcElement.value)\"\r\n               (dateChange)=\"onDateChanged($event)\"\r\n               placeholder=\"{{field.placeholder}}\">\r\n        <mat-datetimepicker-toggle matSuffix [for]=\"datetimePicker\" [disabled]=\"field.readOnly\"></mat-datetimepicker-toggle>\r\n    </mat-form-field>\r\n    <error-widget [error]=\"field.validationSummary\"></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n    <mat-datetimepicker #datetimePicker type=\"datetime\" openOnFocus=\"true\" timeInterval=\"5\"></mat-datetimepicker>\r\n</div>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    styles: [".adf-date-time-widget .mat-form-field-suffix{text-align:right;position:absolute;margin-top:30px;width:100%}@media screen and (-ms-high-contrast:active),screen and (-ms-high-contrast:none){.adf-date-widget .mat-form-field-suffix{position:relative;width:auto}}"]
                }] }
    ];
    /** @nocollapse */
    DateTimeWidgetComponent.ctorParameters = function () { return [
        { type: FormService },
        { type: DateAdapter },
        { type: UserPreferencesService }
    ]; };
    return DateTimeWidgetComponent;
}(WidgetComponent));
export { DateTimeWidgetComponent };
if (false) {
    /** @type {?} */
    DateTimeWidgetComponent.prototype.minDate;
    /** @type {?} */
    DateTimeWidgetComponent.prototype.maxDate;
    /** @type {?} */
    DateTimeWidgetComponent.prototype.displayDate;
    /**
     * @type {?}
     * @private
     */
    DateTimeWidgetComponent.prototype.onDestroy$;
    /** @type {?} */
    DateTimeWidgetComponent.prototype.formService;
    /**
     * @type {?}
     * @private
     */
    DateTimeWidgetComponent.prototype.dateAdapter;
    /**
     * @type {?}
     * @private
     */
    DateTimeWidgetComponent.prototype.userPreferencesService;
}
export { ɵ0, ɵ1 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS10aW1lLndpZGdldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2RhdGUtdGltZS9kYXRlLXRpbWUud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFNBQVMsRUFBVSxpQkFBaUIsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUNoRixPQUFPLEVBQUUsV0FBVyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ2pGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSwyQkFBMkIsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ2hHLE9BQU8sTUFBTSxNQUFNLFlBQVksQ0FBQztBQUVoQyxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSwrQ0FBK0MsQ0FBQztBQUM3RyxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUN4RSxPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSw2Q0FBNkMsQ0FBQztBQUNsRixPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDL0QsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO1NBS0ksbUJBQW1CLE9BRWYsMkJBQTJCO0FBTDlFO0lBWTZDLG1EQUFlO0lBUXhELGlDQUFtQixXQUF3QixFQUN2QixXQUFnQyxFQUNoQyxzQkFBOEM7UUFGbEUsWUFHSSxrQkFBTSxXQUFXLENBQUMsU0FDckI7UUFKa0IsaUJBQVcsR0FBWCxXQUFXLENBQWE7UUFDdkIsaUJBQVcsR0FBWCxXQUFXLENBQXFCO1FBQ2hDLDRCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFKMUQsZ0JBQVUsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDOztJQU01QyxDQUFDOzs7O0lBRUQsMENBQVE7OztJQUFSO1FBQUEsaUJBbUJDO1FBbEJHLElBQUksQ0FBQyxzQkFBc0I7YUFDdEIsTUFBTSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQzthQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNoQyxTQUFTOzs7O1FBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBbEMsQ0FBa0MsRUFBQyxDQUFDOztZQUV2RCxpQkFBaUIsR0FBRyxtQkFBb0IsSUFBSSxDQUFDLFdBQVcsRUFBQTtRQUM5RCxpQkFBaUIsQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDO1FBRXZFLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNaLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUU7Z0JBQ3JCLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLHNCQUFzQixDQUFDLENBQUM7YUFDdEU7WUFFRCxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFO2dCQUNyQixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO2FBQ3RFO1NBQ0o7UUFDRCxJQUFJLENBQUMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2hELENBQUM7Ozs7SUFFRCw2Q0FBVzs7O0lBQVg7UUFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQy9CLENBQUM7Ozs7O0lBRUQsK0NBQWE7Ozs7SUFBYixVQUFjLFlBQVk7UUFDdEIsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLEtBQUssRUFBRTtZQUNwQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxZQUFZLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLENBQUM7U0FDOUU7YUFBTSxJQUFJLFlBQVksRUFBRTtZQUNyQixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxZQUFZLENBQUM7U0FDbkM7YUFBTTtZQUNILElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQztTQUMzQjtRQUNELElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3BDLENBQUM7O2dCQTdESixTQUFTLFNBQUM7b0JBQ1AsU0FBUyxFQUFFO3dCQUNQLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUU7d0JBQ3JELEVBQUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFFBQVEsSUFBcUIsRUFBRTt3QkFDNUQsRUFBRSxPQUFPLEVBQUUsZUFBZSxFQUFFLFFBQVEsRUFBRSxxQkFBcUIsRUFBRTt3QkFDN0QsRUFBRSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsUUFBUSxJQUE2QixFQUFFO3FCQUMzRTtvQkFDRCxRQUFRLEVBQUUsa0JBQWtCO29CQUM1QixvMENBQXNDO29CQUV0QyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7aUJBQ3hDOzs7O2dCQWhCUSxXQUFXO2dCQVJYLFdBQVc7Z0JBS1gsc0JBQXNCOztJQXVFL0IsOEJBQUM7Q0FBQSxBQS9ERCxDQVk2QyxlQUFlLEdBbUQzRDtTQW5EWSx1QkFBdUI7OztJQUVoQywwQ0FBZ0I7O0lBQ2hCLDBDQUFnQjs7SUFDaEIsOENBQW9COzs7OztJQUVwQiw2Q0FBNEM7O0lBRWhDLDhDQUErQjs7Ozs7SUFDL0IsOENBQXdDOzs7OztJQUN4Qyx5REFBc0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuIC8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERhdGVBZGFwdGVyLCBNQVRfREFURV9GT1JNQVRTIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBEYXRldGltZUFkYXB0ZXIsIE1BVF9EQVRFVElNRV9GT1JNQVRTIH0gZnJvbSAnQG1hdC1kYXRldGltZXBpY2tlci9jb3JlJztcclxuaW1wb3J0IHsgTW9tZW50RGF0ZXRpbWVBZGFwdGVyLCBNQVRfTU9NRU5UX0RBVEVUSU1FX0ZPUk1BVFMgfSBmcm9tICdAbWF0LWRhdGV0aW1lcGlja2VyL21vbWVudCc7XHJcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50LWVzNic7XHJcbmltcG9ydCB7IE1vbWVudCB9IGZyb20gJ21vbWVudCc7XHJcbmltcG9ydCB7IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UsIFVzZXJQcmVmZXJlbmNlVmFsdWVzIH0gZnJvbSAnLi4vLi4vLi4vLi4vc2VydmljZXMvdXNlci1wcmVmZXJlbmNlcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTW9tZW50RGF0ZUFkYXB0ZXIgfSBmcm9tICcuLi8uLi8uLi8uLi91dGlscy9tb21lbnREYXRlQWRhcHRlcic7XHJcbmltcG9ydCB7IE1PTUVOVF9EQVRFX0ZPUk1BVFMgfSBmcm9tICcuLi8uLi8uLi8uLi91dGlscy9tb21lbnQtZGF0ZS1mb3JtYXRzLm1vZGVsJztcclxuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLy4uLy4uLy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XHJcbmltcG9ydCB7IFdpZGdldENvbXBvbmVudCB9IGZyb20gJy4vLi4vd2lkZ2V0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBwcm92aWRlcnM6IFtcclxuICAgICAgICB7IHByb3ZpZGU6IERhdGVBZGFwdGVyLCB1c2VDbGFzczogTW9tZW50RGF0ZUFkYXB0ZXIgfSxcclxuICAgICAgICB7IHByb3ZpZGU6IE1BVF9EQVRFX0ZPUk1BVFMsIHVzZVZhbHVlOiBNT01FTlRfREFURV9GT1JNQVRTIH0sXHJcbiAgICAgICAgeyBwcm92aWRlOiBEYXRldGltZUFkYXB0ZXIsIHVzZUNsYXNzOiBNb21lbnREYXRldGltZUFkYXB0ZXIgfSxcclxuICAgICAgICB7IHByb3ZpZGU6IE1BVF9EQVRFVElNRV9GT1JNQVRTLCB1c2VWYWx1ZTogTUFUX01PTUVOVF9EQVRFVElNRV9GT1JNQVRTIH1cclxuICAgIF0sXHJcbiAgICBzZWxlY3RvcjogJ2RhdGUtdGltZS13aWRnZXQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2RhdGUtdGltZS53aWRnZXQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9kYXRlLXRpbWUud2lkZ2V0LnNjc3MnXSxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIERhdGVUaW1lV2lkZ2V0Q29tcG9uZW50IGV4dGVuZHMgV2lkZ2V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG5cclxuICAgIG1pbkRhdGU6IE1vbWVudDtcclxuICAgIG1heERhdGU6IE1vbWVudDtcclxuICAgIGRpc3BsYXlEYXRlOiBNb21lbnQ7XHJcblxyXG4gICAgcHJpdmF0ZSBvbkRlc3Ryb3kkID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZm9ybVNlcnZpY2U6IEZvcm1TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBkYXRlQWRhcHRlcjogRGF0ZUFkYXB0ZXI8TW9tZW50PixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgdXNlclByZWZlcmVuY2VzU2VydmljZTogVXNlclByZWZlcmVuY2VzU2VydmljZSkge1xyXG4gICAgICAgIHN1cGVyKGZvcm1TZXJ2aWNlKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlc1NlcnZpY2VcclxuICAgICAgICAgICAgLnNlbGVjdChVc2VyUHJlZmVyZW5jZVZhbHVlcy5Mb2NhbGUpXHJcbiAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLm9uRGVzdHJveSQpKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKGxvY2FsZSA9PiB0aGlzLmRhdGVBZGFwdGVyLnNldExvY2FsZShsb2NhbGUpKTtcclxuXHJcbiAgICAgICAgY29uc3QgbW9tZW50RGF0ZUFkYXB0ZXIgPSA8TW9tZW50RGF0ZUFkYXB0ZXI+IHRoaXMuZGF0ZUFkYXB0ZXI7XHJcbiAgICAgICAgbW9tZW50RGF0ZUFkYXB0ZXIub3ZlcnJpZGVEaXNwbGF5Rm9ybWF0ID0gdGhpcy5maWVsZC5kYXRlRGlzcGxheUZvcm1hdDtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuZmllbGQpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZmllbGQubWluVmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubWluRGF0ZSA9IG1vbWVudCh0aGlzLmZpZWxkLm1pblZhbHVlLCAnWVlZWS1NTS1ERFRISDptbTpzc1onKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKHRoaXMuZmllbGQubWF4VmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMubWF4RGF0ZSA9IG1vbWVudCh0aGlzLmZpZWxkLm1heFZhbHVlLCAnWVlZWS1NTS1ERFRISDptbTpzc1onKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmRpc3BsYXlEYXRlID0gbW9tZW50KHRoaXMuZmllbGQudmFsdWUpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIHRoaXMub25EZXN0cm95JC5uZXh0KHRydWUpO1xyXG4gICAgICAgIHRoaXMub25EZXN0cm95JC5jb21wbGV0ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRGF0ZUNoYW5nZWQobmV3RGF0ZVZhbHVlKSB7XHJcbiAgICAgICAgaWYgKG5ld0RhdGVWYWx1ZSAmJiBuZXdEYXRlVmFsdWUudmFsdWUpIHtcclxuICAgICAgICAgICAgdGhpcy5maWVsZC52YWx1ZSA9IG5ld0RhdGVWYWx1ZS52YWx1ZS5mb3JtYXQodGhpcy5maWVsZC5kYXRlRGlzcGxheUZvcm1hdCk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChuZXdEYXRlVmFsdWUpIHtcclxuICAgICAgICAgICAgdGhpcy5maWVsZC52YWx1ZSA9IG5ld0RhdGVWYWx1ZTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLmZpZWxkLnZhbHVlID0gbnVsbDtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5vbkZpZWxkQ2hhbmdlZCh0aGlzLmZpZWxkKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19