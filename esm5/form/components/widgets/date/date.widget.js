/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { UserPreferencesService, UserPreferenceValues } from '../../../../services/user-preferences.service';
import { MomentDateAdapter } from '../../../../utils/momentDateAdapter';
import { MOMENT_DATE_FORMATS } from '../../../../utils/moment-date-formats.model';
import { Component, ViewEncapsulation } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import moment from 'moment-es6';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
var ɵ0 = MOMENT_DATE_FORMATS;
var DateWidgetComponent = /** @class */ (function (_super) {
    tslib_1.__extends(DateWidgetComponent, _super);
    function DateWidgetComponent(formService, dateAdapter, userPreferencesService) {
        var _this = _super.call(this, formService) || this;
        _this.formService = formService;
        _this.dateAdapter = dateAdapter;
        _this.userPreferencesService = userPreferencesService;
        _this.DATE_FORMAT = 'DD/MM/YYYY';
        _this.onDestroy$ = new Subject();
        return _this;
    }
    /**
     * @return {?}
     */
    DateWidgetComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.userPreferencesService
            .select(UserPreferenceValues.Locale)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} locale
         * @return {?}
         */
        function (locale) { return _this.dateAdapter.setLocale(locale); }));
        /** @type {?} */
        var momentDateAdapter = (/** @type {?} */ (this.dateAdapter));
        momentDateAdapter.overrideDisplayFormat = this.field.dateDisplayFormat;
        if (this.field) {
            if (this.field.minValue) {
                this.minDate = moment(this.field.minValue, this.DATE_FORMAT);
            }
            if (this.field.maxValue) {
                this.maxDate = moment(this.field.maxValue, this.DATE_FORMAT);
            }
        }
        this.displayDate = moment(this.field.value);
    };
    /**
     * @return {?}
     */
    DateWidgetComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    };
    /**
     * @param {?} newDateValue
     * @return {?}
     */
    DateWidgetComponent.prototype.onDateChanged = /**
     * @param {?} newDateValue
     * @return {?}
     */
    function (newDateValue) {
        if (newDateValue && newDateValue.value) {
            this.field.value = newDateValue.value.format(this.field.dateDisplayFormat);
        }
        else if (newDateValue) {
            this.field.value = newDateValue;
        }
        else {
            this.field.value = null;
        }
        this.onFieldChanged(this.field);
    };
    DateWidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'date-widget',
                    providers: [
                        { provide: DateAdapter, useClass: MomentDateAdapter },
                        { provide: MAT_DATE_FORMATS, useValue: ɵ0 }
                    ],
                    template: "<div class=\"{{field.className}}\" id=\"data-widget\" [class.adf-invalid]=\"!field.isValid\">\r\n    <mat-form-field class=\"adf-date-widget\">\r\n        <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }} ({{field.dateDisplayFormat}})<span *ngIf=\"isRequired()\">*</span></label>\r\n        <input matInput\r\n               [id]=\"field.id\"\r\n               [matDatepicker]=\"datePicker\"\r\n               [(ngModel)]=\"displayDate\"\r\n               [required]=\"isRequired()\"\r\n               [disabled]=\"field.readOnly\"\r\n               [min]=\"minDate\"\r\n               [max]=\"maxDate\"\r\n               (focusout)=\"onDateChanged($event.srcElement.value)\"\r\n               (dateChange)=\"onDateChanged($event)\"\r\n               placeholder=\"{{field.placeholder}}\">\r\n        <mat-datepicker-toggle  matSuffix [for]=\"datePicker\" [disabled]=\"field.readOnly\" ></mat-datepicker-toggle>\r\n    </mat-form-field>\r\n    <error-widget [error]=\"field.validationSummary\"></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n    <mat-datepicker #datePicker [touchUi]=\"true\" [startAt]=\"displayDate\" ></mat-datepicker>\r\n</div>\r\n\r\n",
                    host: baseHost,
                    encapsulation: ViewEncapsulation.None,
                    styles: [".adf-date-widget .mat-form-field-suffix{text-align:right;position:absolute;margin-top:30px;width:100%}.adf-date-widget-date-widget-button{position:relative;float:right}.adf-date-widget-date-input{padding-top:5px;padding-bottom:5px}.adf-date-widget-grid-date-widget{align-items:center;padding:0}.adf-date-widget-date-widget-button__cell{margin-top:0;margin-bottom:0}@media screen and (-ms-high-contrast:active),screen and (-ms-high-contrast:none){.adf-date-widget .mat-form-field-suffix{position:relative;width:auto}}"]
                }] }
    ];
    /** @nocollapse */
    DateWidgetComponent.ctorParameters = function () { return [
        { type: FormService },
        { type: DateAdapter },
        { type: UserPreferencesService }
    ]; };
    return DateWidgetComponent;
}(WidgetComponent));
export { DateWidgetComponent };
if (false) {
    /** @type {?} */
    DateWidgetComponent.prototype.DATE_FORMAT;
    /** @type {?} */
    DateWidgetComponent.prototype.minDate;
    /** @type {?} */
    DateWidgetComponent.prototype.maxDate;
    /** @type {?} */
    DateWidgetComponent.prototype.displayDate;
    /**
     * @type {?}
     * @private
     */
    DateWidgetComponent.prototype.onDestroy$;
    /** @type {?} */
    DateWidgetComponent.prototype.formService;
    /**
     * @type {?}
     * @private
     */
    DateWidgetComponent.prototype.dateAdapter;
    /**
     * @type {?}
     * @private
     */
    DateWidgetComponent.prototype.userPreferencesService;
}
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS53aWRnZXQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9kYXRlL2RhdGUud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLHNCQUFzQixFQUFFLG9CQUFvQixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDN0csT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDeEUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDbEYsT0FBTyxFQUFFLFNBQVMsRUFBVSxpQkFBaUIsRUFBYSxNQUFNLGVBQWUsQ0FBQztBQUNoRixPQUFPLEVBQUUsV0FBVyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbEUsT0FBTyxNQUFNLE1BQU0sWUFBWSxDQUFDO0FBRWhDLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUMvRCxPQUFPLEVBQUUsUUFBUSxFQUFFLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO1NBTUksbUJBQW1CO0FBSmxFO0lBVXlDLCtDQUFlO0lBVXBELDZCQUFtQixXQUF3QixFQUN2QixXQUFnQyxFQUNoQyxzQkFBOEM7UUFGbEUsWUFHSSxrQkFBTSxXQUFXLENBQUMsU0FDckI7UUFKa0IsaUJBQVcsR0FBWCxXQUFXLENBQWE7UUFDdkIsaUJBQVcsR0FBWCxXQUFXLENBQXFCO1FBQ2hDLDRCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFWbEUsaUJBQVcsR0FBRyxZQUFZLENBQUM7UUFNbkIsZ0JBQVUsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDOztJQU01QyxDQUFDOzs7O0lBRUQsc0NBQVE7OztJQUFSO1FBQUEsaUJBbUJDO1FBbEJHLElBQUksQ0FBQyxzQkFBc0I7YUFDdEIsTUFBTSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQzthQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNoQyxTQUFTOzs7O1FBQUMsVUFBQSxNQUFNLElBQUksT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsRUFBbEMsQ0FBa0MsRUFBQyxDQUFDOztZQUV2RCxpQkFBaUIsR0FBRyxtQkFBb0IsSUFBSSxDQUFDLFdBQVcsRUFBQTtRQUM5RCxpQkFBaUIsQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDO1FBRXZFLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNaLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUU7Z0JBQ3JCLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUNoRTtZQUVELElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUU7Z0JBQ3JCLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzthQUNoRTtTQUNKO1FBQ0QsSUFBSSxDQUFDLFdBQVcsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNoRCxDQUFDOzs7O0lBRUQseUNBQVc7OztJQUFYO1FBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDOzs7OztJQUVELDJDQUFhOzs7O0lBQWIsVUFBYyxZQUFZO1FBQ3RCLElBQUksWUFBWSxJQUFJLFlBQVksQ0FBQyxLQUFLLEVBQUU7WUFDcEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsWUFBWSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO1NBQzlFO2FBQU0sSUFBSSxZQUFZLEVBQUU7WUFDckIsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsWUFBWSxDQUFDO1NBQ25DO2FBQU07WUFDSCxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUM7U0FDM0I7UUFDRCxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNwQyxDQUFDOztnQkE3REosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxhQUFhO29CQUN2QixTQUFTLEVBQUU7d0JBQ1AsRUFBRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFBRSxpQkFBaUIsRUFBRTt3QkFDckQsRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsUUFBUSxJQUFxQixFQUFFO3FCQUFDO29CQUNqRSw0d0NBQWlDO29CQUVqQyxJQUFJLEVBQUUsUUFBUTtvQkFDZCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7aUJBQ3hDOzs7O2dCQWRRLFdBQVc7Z0JBSFgsV0FBVztnQkFKWCxzQkFBc0I7O0lBMEUvQiwwQkFBQztDQUFBLEFBOURELENBVXlDLGVBQWUsR0FvRHZEO1NBcERZLG1CQUFtQjs7O0lBRTVCLDBDQUEyQjs7SUFFM0Isc0NBQWdCOztJQUNoQixzQ0FBZ0I7O0lBQ2hCLDBDQUFvQjs7Ozs7SUFFcEIseUNBQTRDOztJQUVoQywwQ0FBK0I7Ozs7O0lBQy9CLDBDQUF3Qzs7Ozs7SUFDeEMscURBQXNEIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UsIFVzZXJQcmVmZXJlbmNlVmFsdWVzIH0gZnJvbSAnLi4vLi4vLi4vLi4vc2VydmljZXMvdXNlci1wcmVmZXJlbmNlcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTW9tZW50RGF0ZUFkYXB0ZXIgfSBmcm9tICcuLi8uLi8uLi8uLi91dGlscy9tb21lbnREYXRlQWRhcHRlcic7XHJcbmltcG9ydCB7IE1PTUVOVF9EQVRFX0ZPUk1BVFMgfSBmcm9tICcuLi8uLi8uLi8uLi91dGlscy9tb21lbnQtZGF0ZS1mb3JtYXRzLm1vZGVsJztcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdFbmNhcHN1bGF0aW9uLCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRGF0ZUFkYXB0ZXIsIE1BVF9EQVRFX0ZPUk1BVFMgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50LWVzNic7XHJcbmltcG9ydCB7IE1vbWVudCB9IGZyb20gJ21vbWVudCc7XHJcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi8uLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBiYXNlSG9zdCwgV2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi8uLi93aWRnZXQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnZGF0ZS13aWRnZXQnLFxyXG4gICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAgeyBwcm92aWRlOiBEYXRlQWRhcHRlciwgdXNlQ2xhc3M6IE1vbWVudERhdGVBZGFwdGVyIH0sXHJcbiAgICAgICAgeyBwcm92aWRlOiBNQVRfREFURV9GT1JNQVRTLCB1c2VWYWx1ZTogTU9NRU5UX0RBVEVfRk9STUFUUyB9XSxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9kYXRlLndpZGdldC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2RhdGUud2lkZ2V0LnNjc3MnXSxcclxuICAgIGhvc3Q6IGJhc2VIb3N0LFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRGF0ZVdpZGdldENvbXBvbmVudCBleHRlbmRzIFdpZGdldENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuXHJcbiAgICBEQVRFX0ZPUk1BVCA9ICdERC9NTS9ZWVlZJztcclxuXHJcbiAgICBtaW5EYXRlOiBNb21lbnQ7XHJcbiAgICBtYXhEYXRlOiBNb21lbnQ7XHJcbiAgICBkaXNwbGF5RGF0ZTogTW9tZW50O1xyXG5cclxuICAgIHByaXZhdGUgb25EZXN0cm95JCA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgZGF0ZUFkYXB0ZXI6IERhdGVBZGFwdGVyPE1vbWVudD4sXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHVzZXJQcmVmZXJlbmNlc1NlcnZpY2U6IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UpIHtcclxuICAgICAgICBzdXBlcihmb3JtU2VydmljZSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZXNTZXJ2aWNlXHJcbiAgICAgICAgICAgIC5zZWxlY3QoVXNlclByZWZlcmVuY2VWYWx1ZXMuTG9jYWxlKVxyXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5vbkRlc3Ryb3kkKSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZShsb2NhbGUgPT4gdGhpcy5kYXRlQWRhcHRlci5zZXRMb2NhbGUobG9jYWxlKSk7XHJcblxyXG4gICAgICAgIGNvbnN0IG1vbWVudERhdGVBZGFwdGVyID0gPE1vbWVudERhdGVBZGFwdGVyPiB0aGlzLmRhdGVBZGFwdGVyO1xyXG4gICAgICAgIG1vbWVudERhdGVBZGFwdGVyLm92ZXJyaWRlRGlzcGxheUZvcm1hdCA9IHRoaXMuZmllbGQuZGF0ZURpc3BsYXlGb3JtYXQ7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmZpZWxkKSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmZpZWxkLm1pblZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1pbkRhdGUgPSBtb21lbnQodGhpcy5maWVsZC5taW5WYWx1ZSwgdGhpcy5EQVRFX0ZPUk1BVCk7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0aGlzLmZpZWxkLm1heFZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm1heERhdGUgPSBtb21lbnQodGhpcy5maWVsZC5tYXhWYWx1ZSwgdGhpcy5EQVRFX0ZPUk1BVCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5kaXNwbGF5RGF0ZSA9IG1vbWVudCh0aGlzLmZpZWxkLnZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQubmV4dCh0cnVlKTtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQuY29tcGxldGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRhdGVDaGFuZ2VkKG5ld0RhdGVWYWx1ZSkge1xyXG4gICAgICAgIGlmIChuZXdEYXRlVmFsdWUgJiYgbmV3RGF0ZVZhbHVlLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZmllbGQudmFsdWUgPSBuZXdEYXRlVmFsdWUudmFsdWUuZm9ybWF0KHRoaXMuZmllbGQuZGF0ZURpc3BsYXlGb3JtYXQpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAobmV3RGF0ZVZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZmllbGQudmFsdWUgPSBuZXdEYXRlVmFsdWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5maWVsZC52YWx1ZSA9IG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMub25GaWVsZENoYW5nZWQodGhpcy5maWVsZCk7XHJcbiAgICB9XHJcbn1cclxuIl19