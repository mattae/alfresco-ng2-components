/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, ViewEncapsulation } from '@angular/core';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
var DisplayTextWidgetComponentComponent = /** @class */ (function (_super) {
    tslib_1.__extends(DisplayTextWidgetComponentComponent, _super);
    function DisplayTextWidgetComponentComponent(formService) {
        var _this = _super.call(this, formService) || this;
        _this.formService = formService;
        return _this;
    }
    DisplayTextWidgetComponentComponent.decorators = [
        { type: Component, args: [{
                    selector: 'display-text-widget',
                    template: "<div class=\"adf-display-text-widget {{field.className}}\">{{field.value | translate}}</div>\r\n",
                    host: baseHost,
                    encapsulation: ViewEncapsulation.None,
                    styles: [".adf-display-text-widget{white-space:pre-wrap}"]
                }] }
    ];
    /** @nocollapse */
    DisplayTextWidgetComponentComponent.ctorParameters = function () { return [
        { type: FormService }
    ]; };
    return DisplayTextWidgetComponentComponent;
}(WidgetComponent));
export { DisplayTextWidgetComponentComponent };
if (false) {
    /** @type {?} */
    DisplayTextWidgetComponentComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlzcGxheS10ZXh0LndpZGdldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2Rpc3BsYXktdGV4dC9kaXNwbGF5LXRleHQud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDL0QsT0FBTyxFQUFFLFFBQVEsRUFBRyxlQUFlLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUVuRTtJQU95RCwrREFBZTtJQUVwRSw2Q0FBbUIsV0FBd0I7UUFBM0MsWUFDSyxrQkFBTSxXQUFXLENBQUMsU0FDdEI7UUFGa0IsaUJBQVcsR0FBWCxXQUFXLENBQWE7O0lBRTNDLENBQUM7O2dCQVhKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUscUJBQXFCO29CQUMvQiw0R0FBeUM7b0JBRXpDLElBQUksRUFBRSxRQUFRO29CQUNkLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOztpQkFDeEM7Ozs7Z0JBVFEsV0FBVzs7SUFnQnBCLDBDQUFDO0NBQUEsQUFiRCxDQU95RCxlQUFlLEdBTXZFO1NBTlksbUNBQW1DOzs7SUFFaEMsMERBQStCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbiAvKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi8uLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBiYXNlSG9zdCAsIFdpZGdldENvbXBvbmVudCB9IGZyb20gJy4vLi4vd2lkZ2V0LmNvbXBvbmVudCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnZGlzcGxheS10ZXh0LXdpZGdldCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vZGlzcGxheS10ZXh0LndpZGdldC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2Rpc3BsYXktdGV4dC53aWRnZXQuc2NzcyddLFxyXG4gICAgaG9zdDogYmFzZUhvc3QsXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEaXNwbGF5VGV4dFdpZGdldENvbXBvbmVudENvbXBvbmVudCBleHRlbmRzIFdpZGdldENvbXBvbmVudCB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSkge1xyXG4gICAgICAgICBzdXBlcihmb3JtU2VydmljZSk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==