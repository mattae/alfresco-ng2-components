/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ViewEncapsulation } from '@angular/core';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
var DocumentWidgetComponent = /** @class */ (function (_super) {
    tslib_1.__extends(DocumentWidgetComponent, _super);
    function DocumentWidgetComponent(formService) {
        var _this = _super.call(this, formService) || this;
        _this.formService = formService;
        _this.fileId = null;
        _this.hasFile = false;
        return _this;
    }
    /**
     * @return {?}
     */
    DocumentWidgetComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.field) {
            /** @type {?} */
            var file = this.field.value;
            if (file) {
                this.fileId = file.id;
                this.hasFile = true;
            }
            else {
                this.fileId = null;
                this.hasFile = false;
            }
        }
    };
    DocumentWidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-form-document-widget',
                    template: "<div class=\"adf-form-document-widget {{field.className}}\">\r\n    <ng-container *ngIf=\"hasFile\">\r\n        <adf-content [id]=\"fileId\" [showDocumentContent]=\"true\"></adf-content>\r\n    </ng-container>\r\n</div>\r\n",
                    host: baseHost,
                    encapsulation: ViewEncapsulation.None
                }] }
    ];
    /** @nocollapse */
    DocumentWidgetComponent.ctorParameters = function () { return [
        { type: FormService }
    ]; };
    return DocumentWidgetComponent;
}(WidgetComponent));
export { DocumentWidgetComponent };
if (false) {
    /** @type {?} */
    DocumentWidgetComponent.prototype.fileId;
    /** @type {?} */
    DocumentWidgetComponent.prototype.hasFile;
    /** @type {?} */
    DocumentWidgetComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG9jdW1lbnQud2lkZ2V0LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvZG9jdW1lbnQvZG9jdW1lbnQud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsU0FBUyxFQUFVLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3JFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUMvRCxPQUFPLEVBQUUsUUFBUSxFQUFHLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBRW5FO0lBTTZDLG1EQUFlO0lBS3hELGlDQUFtQixXQUF3QjtRQUEzQyxZQUNLLGtCQUFNLFdBQVcsQ0FBQyxTQUN0QjtRQUZrQixpQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUgzQyxZQUFNLEdBQVcsSUFBSSxDQUFDO1FBQ3RCLGFBQU8sR0FBWSxLQUFLLENBQUM7O0lBSXpCLENBQUM7Ozs7SUFFRCwwQ0FBUTs7O0lBQVI7UUFDSSxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7O2dCQUNOLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUs7WUFFN0IsSUFBSSxJQUFJLEVBQUU7Z0JBQ04sSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsRUFBRSxDQUFDO2dCQUN0QixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQzthQUN2QjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQztnQkFDbkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7YUFDeEI7U0FDSjtJQUNMLENBQUM7O2dCQTNCSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLDBCQUEwQjtvQkFDcEMsMk9BQW1DO29CQUNuQyxJQUFJLEVBQUUsUUFBUTtvQkFDZCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtpQkFDeEM7Ozs7Z0JBUlEsV0FBVzs7SUErQnBCLDhCQUFDO0NBQUEsQUE1QkQsQ0FNNkMsZUFBZSxHQXNCM0Q7U0F0QlksdUJBQXVCOzs7SUFFaEMseUNBQXNCOztJQUN0QiwwQ0FBeUI7O0lBRWIsOENBQStCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4vLi4vLi4vLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgYmFzZUhvc3QgLCBXaWRnZXRDb21wb25lbnQgfSBmcm9tICcuLy4uL3dpZGdldC5jb21wb25lbnQnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1mb3JtLWRvY3VtZW50LXdpZGdldCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJ2RvY3VtZW50LndpZGdldC5odG1sJyxcclxuICAgIGhvc3Q6IGJhc2VIb3N0LFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRG9jdW1lbnRXaWRnZXRDb21wb25lbnQgZXh0ZW5kcyBXaWRnZXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIGZpbGVJZDogc3RyaW5nID0gbnVsbDtcclxuICAgIGhhc0ZpbGU6IGJvb2xlYW4gPSBmYWxzZTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZm9ybVNlcnZpY2U6IEZvcm1TZXJ2aWNlKSB7XHJcbiAgICAgICAgIHN1cGVyKGZvcm1TZXJ2aWNlKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBpZiAodGhpcy5maWVsZCkge1xyXG4gICAgICAgICAgICBjb25zdCBmaWxlID0gdGhpcy5maWVsZC52YWx1ZTtcclxuXHJcbiAgICAgICAgICAgIGlmIChmaWxlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZpbGVJZCA9IGZpbGUuaWQ7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmhhc0ZpbGUgPSB0cnVlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5maWxlSWQgPSBudWxsO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5oYXNGaWxlID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19