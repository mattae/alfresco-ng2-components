/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import moment from 'moment-es6';
var DateCellValidator = /** @class */ (function () {
    function DateCellValidator() {
        this.supportedTypes = [
            'Date'
        ];
    }
    /**
     * @param {?} column
     * @return {?}
     */
    DateCellValidator.prototype.isSupported = /**
     * @param {?} column
     * @return {?}
     */
    function (column) {
        return column && column.editable && this.supportedTypes.indexOf(column.type) > -1;
    };
    /**
     * @param {?} row
     * @param {?} column
     * @param {?=} summary
     * @return {?}
     */
    DateCellValidator.prototype.validate = /**
     * @param {?} row
     * @param {?} column
     * @param {?=} summary
     * @return {?}
     */
    function (row, column, summary) {
        if (this.isSupported(column)) {
            /** @type {?} */
            var value = row.value[column.id];
            if (!value && !column.required) {
                return true;
            }
            /** @type {?} */
            var dateValue = moment(value, 'YYYY-MM-DDTHH:mm:ss.SSSSZ', true);
            if (!dateValue.isValid()) {
                if (summary) {
                    summary.isValid = false;
                    summary.message = "Invalid '" + column.name + "' format.";
                }
                return false;
            }
        }
        return true;
    };
    return DateCellValidator;
}());
export { DateCellValidator };
if (false) {
    /**
     * @type {?}
     * @private
     */
    DateCellValidator.prototype.supportedTypes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS1jZWxsLXZhbGlkYXRvci1tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2R5bmFtaWMtdGFibGUvZGF0ZS1jZWxsLXZhbGlkYXRvci1tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxNQUFNLE1BQU0sWUFBWSxDQUFDO0FBTWhDO0lBQUE7UUFFWSxtQkFBYyxHQUFhO1lBQy9CLE1BQU07U0FDVCxDQUFDO0lBMkJOLENBQUM7Ozs7O0lBekJHLHVDQUFXOzs7O0lBQVgsVUFBWSxNQUEwQjtRQUNsQyxPQUFPLE1BQU0sSUFBSSxNQUFNLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztJQUN0RixDQUFDOzs7Ozs7O0lBRUQsb0NBQVE7Ozs7OztJQUFSLFVBQVMsR0FBb0IsRUFBRSxNQUEwQixFQUFFLE9BQXFDO1FBRTVGLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxNQUFNLENBQUMsRUFBRTs7Z0JBQ3BCLEtBQUssR0FBRyxHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUM7WUFFbEMsSUFBSSxDQUFDLEtBQUssSUFBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7Z0JBQzVCLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7O2dCQUVLLFNBQVMsR0FBRyxNQUFNLENBQUMsS0FBSyxFQUFFLDJCQUEyQixFQUFFLElBQUksQ0FBQztZQUNsRSxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUN0QixJQUFJLE9BQU8sRUFBRTtvQkFDVCxPQUFPLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztvQkFDeEIsT0FBTyxDQUFDLE9BQU8sR0FBRyxjQUFZLE1BQU0sQ0FBQyxJQUFJLGNBQVcsQ0FBQztpQkFDeEQ7Z0JBQ0QsT0FBTyxLQUFLLENBQUM7YUFDaEI7U0FDSjtRQUVELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFDTCx3QkFBQztBQUFELENBQUMsQUEvQkQsSUErQkM7Ozs7Ozs7SUE3QkcsMkNBRUUiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IG1vbWVudCBmcm9tICdtb21lbnQtZXM2JztcclxuaW1wb3J0IHsgQ2VsbFZhbGlkYXRvciB9IGZyb20gJy4vY2VsbC12YWxpZGF0b3IubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljUm93VmFsaWRhdGlvblN1bW1hcnkgfSBmcm9tICcuL2R5bmFtaWMtcm93LXZhbGlkYXRpb24tc3VtbWFyeS5tb2RlbCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZUNvbHVtbiB9IGZyb20gJy4vZHluYW1pYy10YWJsZS1jb2x1bW4ubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVSb3cgfSBmcm9tICcuL2R5bmFtaWMtdGFibGUtcm93Lm1vZGVsJztcclxuXHJcbmV4cG9ydCBjbGFzcyBEYXRlQ2VsbFZhbGlkYXRvciBpbXBsZW1lbnRzIENlbGxWYWxpZGF0b3Ige1xyXG5cclxuICAgIHByaXZhdGUgc3VwcG9ydGVkVHlwZXM6IHN0cmluZ1tdID0gW1xyXG4gICAgICAgICdEYXRlJ1xyXG4gICAgXTtcclxuXHJcbiAgICBpc1N1cHBvcnRlZChjb2x1bW46IER5bmFtaWNUYWJsZUNvbHVtbik6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBjb2x1bW4gJiYgY29sdW1uLmVkaXRhYmxlICYmIHRoaXMuc3VwcG9ydGVkVHlwZXMuaW5kZXhPZihjb2x1bW4udHlwZSkgPiAtMTtcclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZShyb3c6IER5bmFtaWNUYWJsZVJvdywgY29sdW1uOiBEeW5hbWljVGFibGVDb2x1bW4sIHN1bW1hcnk/OiBEeW5hbWljUm93VmFsaWRhdGlvblN1bW1hcnkpOiBib29sZWFuIHtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuaXNTdXBwb3J0ZWQoY29sdW1uKSkge1xyXG4gICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHJvdy52YWx1ZVtjb2x1bW4uaWRdO1xyXG5cclxuICAgICAgICAgICAgaWYgKCF2YWx1ZSAmJiAhY29sdW1uLnJlcXVpcmVkKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgY29uc3QgZGF0ZVZhbHVlID0gbW9tZW50KHZhbHVlLCAnWVlZWS1NTS1ERFRISDptbTpzcy5TU1NTWicsIHRydWUpO1xyXG4gICAgICAgICAgICBpZiAoIWRhdGVWYWx1ZS5pc1ZhbGlkKCkpIHtcclxuICAgICAgICAgICAgICAgIGlmIChzdW1tYXJ5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgc3VtbWFyeS5pc1ZhbGlkID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgc3VtbWFyeS5tZXNzYWdlID0gYEludmFsaWQgJyR7Y29sdW1uLm5hbWV9JyBmb3JtYXQuYDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICB9XHJcbn1cclxuIl19