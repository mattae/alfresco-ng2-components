/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { LogService } from '../../../../services/log.service';
import { ChangeDetectorRef, Component, ElementRef, ViewEncapsulation } from '@angular/core';
import { WidgetVisibilityService } from '../../../services/widget-visibility.service';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
import { DynamicTableModel } from './dynamic-table.widget.model';
var DynamicTableWidgetComponent = /** @class */ (function (_super) {
    tslib_1.__extends(DynamicTableWidgetComponent, _super);
    function DynamicTableWidgetComponent(formService, elementRef, visibilityService, logService, cd) {
        var _this = _super.call(this, formService) || this;
        _this.formService = formService;
        _this.elementRef = elementRef;
        _this.visibilityService = visibilityService;
        _this.logService = logService;
        _this.cd = cd;
        _this.ERROR_MODEL_NOT_FOUND = 'Table model not found';
        _this.editMode = false;
        _this.editRow = null;
        _this.selectArrayCode = [32, 0, 13];
        return _this;
    }
    /**
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.field) {
            this.content = new DynamicTableModel(this.field, this.formService);
            this.visibilityService.refreshVisibility(this.field.form);
        }
    };
    /**
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.forceFocusOnAddButton = /**
     * @return {?}
     */
    function () {
        if (this.content) {
            this.cd.detectChanges();
            /** @type {?} */
            var buttonAddRow = (/** @type {?} */ (this.elementRef.nativeElement.querySelector('#' + this.content.id + '-add-row')));
            if (this.isDynamicTableReady(buttonAddRow)) {
                buttonAddRow.focus();
            }
        }
    };
    /**
     * @private
     * @param {?} buttonAddRow
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.isDynamicTableReady = /**
     * @private
     * @param {?} buttonAddRow
     * @return {?}
     */
    function (buttonAddRow) {
        return this.field && !this.editMode && buttonAddRow;
    };
    /**
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.isValid = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var valid = true;
        if (this.content && this.content.field) {
            valid = this.content.field.isValid;
        }
        return valid;
    };
    /**
     * @param {?} row
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.onRowClicked = /**
     * @param {?} row
     * @return {?}
     */
    function (row) {
        if (this.content) {
            this.content.selectedRow = row;
        }
    };
    /**
     * @param {?} $event
     * @param {?} row
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.onKeyPressed = /**
     * @param {?} $event
     * @param {?} row
     * @return {?}
     */
    function ($event, row) {
        if (this.content && this.isEnterOrSpacePressed($event.keyCode)) {
            this.content.selectedRow = row;
        }
    };
    /**
     * @private
     * @param {?} keyCode
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.isEnterOrSpacePressed = /**
     * @private
     * @param {?} keyCode
     * @return {?}
     */
    function (keyCode) {
        return this.selectArrayCode.indexOf(keyCode) !== -1;
    };
    /**
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.hasSelection = /**
     * @return {?}
     */
    function () {
        return !!(this.content && this.content.selectedRow);
    };
    /**
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.moveSelectionUp = /**
     * @return {?}
     */
    function () {
        if (this.content && !this.readOnly) {
            this.content.moveRow(this.content.selectedRow, -1);
            return true;
        }
        return false;
    };
    /**
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.moveSelectionDown = /**
     * @return {?}
     */
    function () {
        if (this.content && !this.readOnly) {
            this.content.moveRow(this.content.selectedRow, 1);
            return true;
        }
        return false;
    };
    /**
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.deleteSelection = /**
     * @return {?}
     */
    function () {
        if (this.content && !this.readOnly) {
            this.content.deleteRow(this.content.selectedRow);
            return true;
        }
        return false;
    };
    /**
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.addNewRow = /**
     * @return {?}
     */
    function () {
        if (this.content && !this.readOnly) {
            this.editRow = (/** @type {?} */ ({
                isNew: true,
                selected: false,
                value: {}
            }));
            this.editMode = true;
            return true;
        }
        return false;
    };
    /**
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.editSelection = /**
     * @return {?}
     */
    function () {
        if (this.content && !this.readOnly) {
            this.editRow = this.copyRow(this.content.selectedRow);
            this.editMode = true;
            return true;
        }
        return false;
    };
    /**
     * @param {?} row
     * @param {?} column
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.getCellValue = /**
     * @param {?} row
     * @param {?} column
     * @return {?}
     */
    function (row, column) {
        if (this.content) {
            /** @type {?} */
            var cellValue = this.content.getCellValue(row, column);
            if (column.type === 'Amount') {
                return (column.amountCurrency || '$') + ' ' + (cellValue || 0);
            }
            return cellValue;
        }
        return null;
    };
    /**
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.onSaveChanges = /**
     * @return {?}
     */
    function () {
        if (this.content) {
            if (this.editRow.isNew) {
                /** @type {?} */
                var row = this.copyRow(this.editRow);
                this.content.selectedRow = null;
                this.content.addRow(row);
                this.editRow.isNew = false;
            }
            else {
                this.content.selectedRow.value = this.copyObject(this.editRow.value);
            }
            this.content.flushValue();
        }
        else {
            this.logService.error(this.ERROR_MODEL_NOT_FOUND);
        }
        this.editMode = false;
        this.forceFocusOnAddButton();
    };
    /**
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.onCancelChanges = /**
     * @return {?}
     */
    function () {
        this.editMode = false;
        this.editRow = null;
        this.forceFocusOnAddButton();
    };
    /**
     * @param {?} row
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.copyRow = /**
     * @param {?} row
     * @return {?}
     */
    function (row) {
        return (/** @type {?} */ ({
            value: this.copyObject(row.value)
        }));
    };
    /**
     * @private
     * @param {?} obj
     * @return {?}
     */
    DynamicTableWidgetComponent.prototype.copyObject = /**
     * @private
     * @param {?} obj
     * @return {?}
     */
    function (obj) {
        var _this = this;
        /** @type {?} */
        var result = obj;
        if (typeof obj === 'object' && obj !== null && obj !== undefined) {
            result = Object.assign({}, obj);
            Object.keys(obj).forEach((/**
             * @param {?} key
             * @return {?}
             */
            function (key) {
                if (typeof obj[key] === 'object') {
                    result[key] = _this.copyObject(obj[key]);
                }
            }));
        }
        return result;
    };
    DynamicTableWidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'dynamic-table-widget',
                    template: "<div class=\"adf-dynamic-table-scrolling {{field.className}}\"\r\n    [class.adf-invalid]=\"!isValid()\">\r\n    <div class=\"adf-label\">{{content.name | translate }}<span *ngIf=\"isRequired()\">*</span></div>\r\n\r\n    <div *ngIf=\"!editMode\">\r\n        <div class=\"adf-table-container\">\r\n            <table class=\"adf-full-width adf-dynamic-table\" id=\"dynamic-table-{{content.id}}\">\r\n                <thead>\r\n                    <tr>\r\n                        <th *ngFor=\"let column of content.visibleColumns\">\r\n                            {{column.name}}\r\n                        </th>\r\n                    </tr>\r\n                </thead>\r\n                <tbody>\r\n                    <tr *ngFor=\"let row of content.rows; let idx = index\" tabindex=\"0\" id=\"{{content.id}}-row-{{idx}}\"\r\n                        [class.adf-dynamic-table-widget__row-selected]=\"row.selected\" (keyup)=\"onKeyPressed($event, row)\">\r\n                        <td *ngFor=\"let column of content.visibleColumns\"\r\n                            (click)=\"onRowClicked(row)\">\r\n                            <span *ngIf=\"column.type !== 'Boolean' else checkbox\">\r\n                                {{ getCellValue(row, column) }}\r\n                            </span>\r\n                            <ng-template #checkbox>\r\n                                <mat-checkbox disabled [checked]=\"getCellValue(row, column)\">\r\n                                </mat-checkbox>\r\n                            </ng-template>\r\n                        </td>\r\n                    </tr>\r\n                </tbody>\r\n            </table>\r\n        </div>\r\n\r\n        <div *ngIf=\"!readOnly\">\r\n            <button mat-button\r\n                    [disabled]=\"!hasSelection()\"\r\n                    (click)=\"moveSelectionUp()\">\r\n                <mat-icon>arrow_upward</mat-icon>\r\n            </button>\r\n            <button mat-button\r\n                    [disabled]=\"!hasSelection()\"\r\n                    (click)=\"moveSelectionDown()\">\r\n                <mat-icon>arrow_downward</mat-icon>\r\n            </button>\r\n            <button mat-button\r\n                    [disabled]=\"field.readOnly\"\r\n                    id=\"{{content.id}}-add-row\"\r\n                    (click)=\"addNewRow()\">\r\n                <mat-icon>add_circle_outline</mat-icon>\r\n            </button>\r\n            <button mat-button\r\n                    [disabled]=\"!hasSelection()\"\r\n                    (click)=\"deleteSelection()\">\r\n                <mat-icon>remove_circle_outline</mat-icon>\r\n            </button>\r\n            <button mat-button\r\n                    [disabled]=\"!hasSelection()\"\r\n                    (click)=\"editSelection()\">\r\n                <mat-icon>edit</mat-icon>\r\n            </button>\r\n        </div>\r\n     </div>\r\n\r\n     <row-editor *ngIf=\"editMode\"\r\n        [table]=\"content\"\r\n        [row]=\"editRow\"\r\n        (save)=\"onSaveChanges()\"\r\n        (cancel)=\"onCancelChanges()\">\r\n     </row-editor>\r\n    <error-widget [error]=\"field.validationSummary\" ></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n</div>\r\n",
                    host: baseHost,
                    encapsulation: ViewEncapsulation.None,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    DynamicTableWidgetComponent.ctorParameters = function () { return [
        { type: FormService },
        { type: ElementRef },
        { type: WidgetVisibilityService },
        { type: LogService },
        { type: ChangeDetectorRef }
    ]; };
    return DynamicTableWidgetComponent;
}(WidgetComponent));
export { DynamicTableWidgetComponent };
if (false) {
    /** @type {?} */
    DynamicTableWidgetComponent.prototype.ERROR_MODEL_NOT_FOUND;
    /** @type {?} */
    DynamicTableWidgetComponent.prototype.content;
    /** @type {?} */
    DynamicTableWidgetComponent.prototype.editMode;
    /** @type {?} */
    DynamicTableWidgetComponent.prototype.editRow;
    /**
     * @type {?}
     * @private
     */
    DynamicTableWidgetComponent.prototype.selectArrayCode;
    /** @type {?} */
    DynamicTableWidgetComponent.prototype.formService;
    /** @type {?} */
    DynamicTableWidgetComponent.prototype.elementRef;
    /**
     * @type {?}
     * @private
     */
    DynamicTableWidgetComponent.prototype.visibilityService;
    /**
     * @type {?}
     * @private
     */
    DynamicTableWidgetComponent.prototype.logService;
    /**
     * @type {?}
     * @private
     */
    DynamicTableWidgetComponent.prototype.cd;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHluYW1pYy10YWJsZS53aWRnZXQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9keW5hbWljLXRhYmxlL2R5bmFtaWMtdGFibGUud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQzlELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFVLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3BHLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBQ3RGLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUMvRCxPQUFPLEVBQUUsUUFBUSxFQUFFLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBR2xFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLDhCQUE4QixDQUFDO0FBRWpFO0lBT2lELHVEQUFlO0lBVzVELHFDQUFtQixXQUF3QixFQUN4QixVQUFzQixFQUNyQixpQkFBMEMsRUFDMUMsVUFBc0IsRUFDdEIsRUFBcUI7UUFKekMsWUFLSSxrQkFBTSxXQUFXLENBQUMsU0FDckI7UUFOa0IsaUJBQVcsR0FBWCxXQUFXLENBQWE7UUFDeEIsZ0JBQVUsR0FBVixVQUFVLENBQVk7UUFDckIsdUJBQWlCLEdBQWpCLGlCQUFpQixDQUF5QjtRQUMxQyxnQkFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixRQUFFLEdBQUYsRUFBRSxDQUFtQjtRQWJ6QywyQkFBcUIsR0FBRyx1QkFBdUIsQ0FBQztRQUloRCxjQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLGFBQU8sR0FBb0IsSUFBSSxDQUFDO1FBRXhCLHFCQUFlLEdBQUcsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDOztJQVF0QyxDQUFDOzs7O0lBRUQsOENBQVE7OztJQUFSO1FBQ0ksSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1osSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLGlCQUFpQixDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ25FLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQzdEO0lBQ0wsQ0FBQzs7OztJQUVELDJEQUFxQjs7O0lBQXJCO1FBQ0ksSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2QsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLEVBQUUsQ0FBQzs7Z0JBQ2xCLFlBQVksR0FBRyxtQkFBb0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLEdBQUcsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEVBQUUsR0FBRyxVQUFVLENBQUMsRUFBQTtZQUN4SCxJQUFJLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLENBQUMsRUFBRTtnQkFDeEMsWUFBWSxDQUFDLEtBQUssRUFBRSxDQUFDO2FBQ3hCO1NBQ0o7SUFDTCxDQUFDOzs7Ozs7SUFFTyx5REFBbUI7Ozs7O0lBQTNCLFVBQTRCLFlBQVk7UUFDcEMsT0FBTyxJQUFJLENBQUMsS0FBSyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsSUFBSSxZQUFZLENBQUM7SUFDeEQsQ0FBQzs7OztJQUVELDZDQUFPOzs7SUFBUDs7WUFDUSxLQUFLLEdBQUcsSUFBSTtRQUVoQixJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUU7WUFDcEMsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQztTQUN0QztRQUVELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7O0lBRUQsa0RBQVk7Ozs7SUFBWixVQUFhLEdBQW9CO1FBQzdCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNkLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFHLEdBQUcsQ0FBQztTQUNsQztJQUNMLENBQUM7Ozs7OztJQUVELGtEQUFZOzs7OztJQUFaLFVBQWEsTUFBcUIsRUFBRSxHQUFvQjtRQUNwRCxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUMsRUFBRTtZQUM1RCxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7U0FDbEM7SUFDTCxDQUFDOzs7Ozs7SUFFTywyREFBcUI7Ozs7O0lBQTdCLFVBQThCLE9BQU87UUFDakMsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztJQUN4RCxDQUFDOzs7O0lBRUQsa0RBQVk7OztJQUFaO1FBQ0ksT0FBTyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDeEQsQ0FBQzs7OztJQUVELHFEQUFlOzs7SUFBZjtRQUNJLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDaEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsQ0FBQyxDQUFDLENBQUMsQ0FBQztZQUNuRCxPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7OztJQUVELHVEQUFpQjs7O0lBQWpCO1FBQ0ksSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNoQyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQztZQUNsRCxPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7OztJQUVELHFEQUFlOzs7SUFBZjtRQUNJLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDaEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUNqRCxPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7OztJQUVELCtDQUFTOzs7SUFBVDtRQUNJLElBQUksSUFBSSxDQUFDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDaEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxtQkFBa0I7Z0JBQzdCLEtBQUssRUFBRSxJQUFJO2dCQUNYLFFBQVEsRUFBRSxLQUFLO2dCQUNmLEtBQUssRUFBRSxFQUFFO2FBQ1osRUFBQSxDQUFDO1lBQ0YsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7WUFDckIsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7Ozs7SUFFRCxtREFBYTs7O0lBQWI7UUFDSSxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ2hDLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDO1lBQ3RELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDO1lBQ3JCLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDOzs7Ozs7SUFFRCxrREFBWTs7Ozs7SUFBWixVQUFhLEdBQW9CLEVBQUUsTUFBMEI7UUFDekQsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFOztnQkFDUixTQUFTLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsR0FBRyxFQUFFLE1BQU0sQ0FBQztZQUN4RCxJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssUUFBUSxFQUFFO2dCQUMxQixPQUFPLENBQUMsTUFBTSxDQUFDLGNBQWMsSUFBSSxHQUFHLENBQUMsR0FBRyxHQUFHLEdBQUcsQ0FBQyxTQUFTLElBQUksQ0FBQyxDQUFDLENBQUM7YUFDbEU7WUFDRCxPQUFPLFNBQVMsQ0FBQztTQUNwQjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7SUFFRCxtREFBYTs7O0lBQWI7UUFDSSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDZCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFOztvQkFDZCxHQUFHLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO2dCQUN0QyxJQUFJLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7Z0JBQ2hDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDO2dCQUN6QixJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7YUFDOUI7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLE9BQU8sQ0FBQyxXQUFXLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUN4RTtZQUNELElBQUksQ0FBQyxPQUFPLENBQUMsVUFBVSxFQUFFLENBQUM7U0FDN0I7YUFBTTtZQUNILElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1NBQ3JEO1FBQ0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUM7UUFDdEIsSUFBSSxDQUFDLHFCQUFxQixFQUFFLENBQUM7SUFDakMsQ0FBQzs7OztJQUVELHFEQUFlOzs7SUFBZjtRQUNJLElBQUksQ0FBQyxRQUFRLEdBQUcsS0FBSyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1FBQ3BCLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO0lBQ2pDLENBQUM7Ozs7O0lBRUQsNkNBQU87Ozs7SUFBUCxVQUFRLEdBQW9CO1FBQ3hCLE9BQU8sbUJBQWtCO1lBQ3JCLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUM7U0FDcEMsRUFBQSxDQUFDO0lBQ04sQ0FBQzs7Ozs7O0lBRU8sZ0RBQVU7Ozs7O0lBQWxCLFVBQW1CLEdBQVE7UUFBM0IsaUJBYUM7O1lBWk8sTUFBTSxHQUFHLEdBQUc7UUFFaEIsSUFBSSxPQUFPLEdBQUcsS0FBSyxRQUFRLElBQUksR0FBRyxLQUFLLElBQUksSUFBSSxHQUFHLEtBQUssU0FBUyxFQUFFO1lBQzlELE1BQU0sR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUNoQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLE9BQU87Ozs7WUFBQyxVQUFDLEdBQUc7Z0JBQ3pCLElBQUksT0FBTyxHQUFHLENBQUMsR0FBRyxDQUFDLEtBQUssUUFBUSxFQUFFO29CQUM5QixNQUFNLENBQUMsR0FBRyxDQUFDLEdBQUcsS0FBSSxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztpQkFDM0M7WUFDTCxDQUFDLEVBQUMsQ0FBQztTQUNOO1FBRUQsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQzs7Z0JBakxKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsc0JBQXNCO29CQUNoQyxtd0dBQTBDO29CQUUxQyxJQUFJLEVBQUUsUUFBUTtvQkFDZCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7aUJBQ3hDOzs7O2dCQVpRLFdBQVc7Z0JBRm1CLFVBQVU7Z0JBQ3hDLHVCQUF1QjtnQkFGdkIsVUFBVTtnQkFDVixpQkFBaUI7O0lBMEwxQixrQ0FBQztDQUFBLEFBbExELENBT2lELGVBQWUsR0EySy9EO1NBM0tZLDJCQUEyQjs7O0lBRXBDLDREQUFnRDs7SUFFaEQsOENBQTJCOztJQUUzQiwrQ0FBMEI7O0lBQzFCLDhDQUFnQzs7Ozs7SUFFaEMsc0RBQXNDOztJQUUxQixrREFBK0I7O0lBQy9CLGlEQUE2Qjs7Ozs7SUFDN0Isd0RBQWtEOzs7OztJQUNsRCxpREFBOEI7Ozs7O0lBQzlCLHlDQUE2QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4vKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgeyBMb2dTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vc2VydmljZXMvbG9nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDaGFuZ2VEZXRlY3RvclJlZiwgQ29tcG9uZW50LCBFbGVtZW50UmVmLCBPbkluaXQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFdpZGdldFZpc2liaWxpdHlTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2VydmljZXMvd2lkZ2V0LXZpc2liaWxpdHkuc2VydmljZSc7XHJcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi8uLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBiYXNlSG9zdCwgV2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi8uLi93aWRnZXQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlQ29sdW1uIH0gZnJvbSAnLi9keW5hbWljLXRhYmxlLWNvbHVtbi5tb2RlbCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZVJvdyB9IGZyb20gJy4vZHluYW1pYy10YWJsZS1yb3cubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVNb2RlbCB9IGZyb20gJy4vZHluYW1pYy10YWJsZS53aWRnZXQubW9kZWwnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2R5bmFtaWMtdGFibGUtd2lkZ2V0JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9keW5hbWljLXRhYmxlLndpZGdldC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2R5bmFtaWMtdGFibGUud2lkZ2V0LnNjc3MnXSxcclxuICAgIGhvc3Q6IGJhc2VIb3N0LFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRHluYW1pY1RhYmxlV2lkZ2V0Q29tcG9uZW50IGV4dGVuZHMgV2lkZ2V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBFUlJPUl9NT0RFTF9OT1RfRk9VTkQgPSAnVGFibGUgbW9kZWwgbm90IGZvdW5kJztcclxuXHJcbiAgICBjb250ZW50OiBEeW5hbWljVGFibGVNb2RlbDtcclxuXHJcbiAgICBlZGl0TW9kZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgZWRpdFJvdzogRHluYW1pY1RhYmxlUm93ID0gbnVsbDtcclxuXHJcbiAgICBwcml2YXRlIHNlbGVjdEFycmF5Q29kZSA9IFszMiwgMCwgMTNdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyBmb3JtU2VydmljZTogRm9ybVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgZWxlbWVudFJlZjogRWxlbWVudFJlZixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgdmlzaWJpbGl0eVNlcnZpY2U6IFdpZGdldFZpc2liaWxpdHlTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBjZDogQ2hhbmdlRGV0ZWN0b3JSZWYpIHtcclxuICAgICAgICBzdXBlcihmb3JtU2VydmljZSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZmllbGQpIHtcclxuICAgICAgICAgICAgdGhpcy5jb250ZW50ID0gbmV3IER5bmFtaWNUYWJsZU1vZGVsKHRoaXMuZmllbGQsIHRoaXMuZm9ybVNlcnZpY2UpO1xyXG4gICAgICAgICAgICB0aGlzLnZpc2liaWxpdHlTZXJ2aWNlLnJlZnJlc2hWaXNpYmlsaXR5KHRoaXMuZmllbGQuZm9ybSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGZvcmNlRm9jdXNPbkFkZEJ1dHRvbigpIHtcclxuICAgICAgICBpZiAodGhpcy5jb250ZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuY2QuZGV0ZWN0Q2hhbmdlcygpO1xyXG4gICAgICAgICAgICBjb25zdCBidXR0b25BZGRSb3cgPSA8SFRNTEJ1dHRvbkVsZW1lbnQ+IHRoaXMuZWxlbWVudFJlZi5uYXRpdmVFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJyMnICsgdGhpcy5jb250ZW50LmlkICsgJy1hZGQtcm93Jyk7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmlzRHluYW1pY1RhYmxlUmVhZHkoYnV0dG9uQWRkUm93KSkge1xyXG4gICAgICAgICAgICAgICAgYnV0dG9uQWRkUm93LmZvY3VzKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBpc0R5bmFtaWNUYWJsZVJlYWR5KGJ1dHRvbkFkZFJvdykge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZpZWxkICYmICF0aGlzLmVkaXRNb2RlICYmIGJ1dHRvbkFkZFJvdztcclxuICAgIH1cclxuXHJcbiAgICBpc1ZhbGlkKCkge1xyXG4gICAgICAgIGxldCB2YWxpZCA9IHRydWU7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmNvbnRlbnQgJiYgdGhpcy5jb250ZW50LmZpZWxkKSB7XHJcbiAgICAgICAgICAgIHZhbGlkID0gdGhpcy5jb250ZW50LmZpZWxkLmlzVmFsaWQ7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdmFsaWQ7XHJcbiAgICB9XHJcblxyXG4gICAgb25Sb3dDbGlja2VkKHJvdzogRHluYW1pY1RhYmxlUm93KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY29udGVudCkge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRlbnQuc2VsZWN0ZWRSb3cgPSByb3c7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uS2V5UHJlc3NlZCgkZXZlbnQ6IEtleWJvYXJkRXZlbnQsIHJvdzogRHluYW1pY1RhYmxlUm93KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY29udGVudCAmJiB0aGlzLmlzRW50ZXJPclNwYWNlUHJlc3NlZCgkZXZlbnQua2V5Q29kZSkpIHtcclxuICAgICAgICAgICAgdGhpcy5jb250ZW50LnNlbGVjdGVkUm93ID0gcm93O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGlzRW50ZXJPclNwYWNlUHJlc3NlZChrZXlDb2RlKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc2VsZWN0QXJyYXlDb2RlLmluZGV4T2Yoa2V5Q29kZSkgIT09IC0xO1xyXG4gICAgfVxyXG5cclxuICAgIGhhc1NlbGVjdGlvbigpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gISEodGhpcy5jb250ZW50ICYmIHRoaXMuY29udGVudC5zZWxlY3RlZFJvdyk7XHJcbiAgICB9XHJcblxyXG4gICAgbW92ZVNlbGVjdGlvblVwKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLmNvbnRlbnQgJiYgIXRoaXMucmVhZE9ubHkpIHtcclxuICAgICAgICAgICAgdGhpcy5jb250ZW50Lm1vdmVSb3codGhpcy5jb250ZW50LnNlbGVjdGVkUm93LCAtMSk7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgbW92ZVNlbGVjdGlvbkRvd24oKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHRoaXMuY29udGVudCAmJiAhdGhpcy5yZWFkT25seSkge1xyXG4gICAgICAgICAgICB0aGlzLmNvbnRlbnQubW92ZVJvdyh0aGlzLmNvbnRlbnQuc2VsZWN0ZWRSb3csIDEpO1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGRlbGV0ZVNlbGVjdGlvbigpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodGhpcy5jb250ZW50ICYmICF0aGlzLnJlYWRPbmx5KSB7XHJcbiAgICAgICAgICAgIHRoaXMuY29udGVudC5kZWxldGVSb3codGhpcy5jb250ZW50LnNlbGVjdGVkUm93KTtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBhZGROZXdSb3coKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHRoaXMuY29udGVudCAmJiAhdGhpcy5yZWFkT25seSkge1xyXG4gICAgICAgICAgICB0aGlzLmVkaXRSb3cgPSA8RHluYW1pY1RhYmxlUm93PiB7XHJcbiAgICAgICAgICAgICAgICBpc05ldzogdHJ1ZSxcclxuICAgICAgICAgICAgICAgIHNlbGVjdGVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiB7fVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmVkaXRNb2RlID0gdHJ1ZTtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBlZGl0U2VsZWN0aW9uKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLmNvbnRlbnQgJiYgIXRoaXMucmVhZE9ubHkpIHtcclxuICAgICAgICAgICAgdGhpcy5lZGl0Um93ID0gdGhpcy5jb3B5Um93KHRoaXMuY29udGVudC5zZWxlY3RlZFJvdyk7XHJcbiAgICAgICAgICAgIHRoaXMuZWRpdE1vZGUgPSB0cnVlO1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGdldENlbGxWYWx1ZShyb3c6IER5bmFtaWNUYWJsZVJvdywgY29sdW1uOiBEeW5hbWljVGFibGVDb2x1bW4pOiBhbnkge1xyXG4gICAgICAgIGlmICh0aGlzLmNvbnRlbnQpIHtcclxuICAgICAgICAgICAgY29uc3QgY2VsbFZhbHVlID0gdGhpcy5jb250ZW50LmdldENlbGxWYWx1ZShyb3csIGNvbHVtbik7XHJcbiAgICAgICAgICAgIGlmIChjb2x1bW4udHlwZSA9PT0gJ0Ftb3VudCcpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAoY29sdW1uLmFtb3VudEN1cnJlbmN5IHx8ICckJykgKyAnICcgKyAoY2VsbFZhbHVlIHx8IDApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBjZWxsVmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIG9uU2F2ZUNoYW5nZXMoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY29udGVudCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5lZGl0Um93LmlzTmV3KSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByb3cgPSB0aGlzLmNvcHlSb3codGhpcy5lZGl0Um93KTtcclxuICAgICAgICAgICAgICAgIHRoaXMuY29udGVudC5zZWxlY3RlZFJvdyA9IG51bGw7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRlbnQuYWRkUm93KHJvdyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVkaXRSb3cuaXNOZXcgPSBmYWxzZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY29udGVudC5zZWxlY3RlZFJvdy52YWx1ZSA9IHRoaXMuY29weU9iamVjdCh0aGlzLmVkaXRSb3cudmFsdWUpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHRoaXMuY29udGVudC5mbHVzaFZhbHVlKCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKHRoaXMuRVJST1JfTU9ERUxfTk9UX0ZPVU5EKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5lZGl0TW9kZSA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuZm9yY2VGb2N1c09uQWRkQnV0dG9uKCk7XHJcbiAgICB9XHJcblxyXG4gICAgb25DYW5jZWxDaGFuZ2VzKCkge1xyXG4gICAgICAgIHRoaXMuZWRpdE1vZGUgPSBmYWxzZTtcclxuICAgICAgICB0aGlzLmVkaXRSb3cgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuZm9yY2VGb2N1c09uQWRkQnV0dG9uKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY29weVJvdyhyb3c6IER5bmFtaWNUYWJsZVJvdyk6IER5bmFtaWNUYWJsZVJvdyB7XHJcbiAgICAgICAgcmV0dXJuIDxEeW5hbWljVGFibGVSb3c+IHtcclxuICAgICAgICAgICAgdmFsdWU6IHRoaXMuY29weU9iamVjdChyb3cudmFsdWUpXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNvcHlPYmplY3Qob2JqOiBhbnkpOiBhbnkge1xyXG4gICAgICAgIGxldCByZXN1bHQgPSBvYmo7XHJcblxyXG4gICAgICAgIGlmICh0eXBlb2Ygb2JqID09PSAnb2JqZWN0JyAmJiBvYmogIT09IG51bGwgJiYgb2JqICE9PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgcmVzdWx0ID0gT2JqZWN0LmFzc2lnbih7fSwgb2JqKTtcclxuICAgICAgICAgICAgT2JqZWN0LmtleXMob2JqKS5mb3JFYWNoKChrZXkpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICh0eXBlb2Ygb2JqW2tleV0gPT09ICdvYmplY3QnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0W2tleV0gPSB0aGlzLmNvcHlPYmplY3Qob2JqW2tleV0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcbn1cclxuIl19