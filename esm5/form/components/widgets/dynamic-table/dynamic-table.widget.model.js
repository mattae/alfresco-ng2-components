/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import moment from 'moment-es6';
import { ValidateDynamicTableRowEvent } from '../../../events/validate-dynamic-table-row.event';
import { FormWidgetModel } from './../core/form-widget.model';
import { DateCellValidator } from './date-cell-validator-model';
import { DynamicRowValidationSummary } from './dynamic-row-validation-summary.model';
import { NumberCellValidator } from './number-cell-validator.model';
import { RequiredCellValidator } from './required-cell-validator.model';
var DynamicTableModel = /** @class */ (function (_super) {
    tslib_1.__extends(DynamicTableModel, _super);
    function DynamicTableModel(field, formService) {
        var _this = _super.call(this, field.form, field.json) || this;
        _this.formService = formService;
        _this.columns = [];
        _this.visibleColumns = [];
        _this.rows = [];
        _this._validators = [];
        _this.field = field;
        if (field.json) {
            /** @type {?} */
            var columns = _this.getColumns(field);
            if (columns) {
                _this.columns = columns;
                _this.visibleColumns = _this.columns.filter((/**
                 * @param {?} col
                 * @return {?}
                 */
                function (col) { return col.visible; }));
            }
            if (field.json.value) {
                _this.rows = field.json.value.map((/**
                 * @param {?} obj
                 * @return {?}
                 */
                function (obj) { return (/** @type {?} */ ({ selected: false, value: obj })); }));
            }
        }
        _this._validators = [
            new RequiredCellValidator(),
            new DateCellValidator(),
            new NumberCellValidator()
        ];
        return _this;
    }
    Object.defineProperty(DynamicTableModel.prototype, "selectedRow", {
        get: /**
         * @return {?}
         */
        function () {
            return this._selectedRow;
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            if (this._selectedRow && this._selectedRow === value) {
                this._selectedRow.selected = false;
                this._selectedRow = null;
                return;
            }
            this.rows.forEach((/**
             * @param {?} row
             * @return {?}
             */
            function (row) { return row.selected = false; }));
            this._selectedRow = value;
            if (value) {
                this._selectedRow.selected = true;
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @private
     * @param {?} field
     * @return {?}
     */
    DynamicTableModel.prototype.getColumns = /**
     * @private
     * @param {?} field
     * @return {?}
     */
    function (field) {
        if (field && field.json) {
            /** @type {?} */
            var definitions = field.json.columnDefinitions;
            if (!definitions && field.json.params && field.json.params.field) {
                definitions = field.json.params.field.columnDefinitions;
            }
            if (definitions) {
                return definitions.map((/**
                 * @param {?} obj
                 * @return {?}
                 */
                function (obj) { return (/** @type {?} */ (obj)); }));
            }
        }
        return null;
    };
    /**
     * @return {?}
     */
    DynamicTableModel.prototype.flushValue = /**
     * @return {?}
     */
    function () {
        if (this.field) {
            this.field.value = this.rows.map((/**
             * @param {?} r
             * @return {?}
             */
            function (r) { return r.value; }));
            this.field.updateForm();
        }
    };
    /**
     * @param {?} row
     * @param {?} offset
     * @return {?}
     */
    DynamicTableModel.prototype.moveRow = /**
     * @param {?} row
     * @param {?} offset
     * @return {?}
     */
    function (row, offset) {
        /** @type {?} */
        var oldIndex = this.rows.indexOf(row);
        if (oldIndex > -1) {
            /** @type {?} */
            var newIndex = (oldIndex + offset);
            if (newIndex < 0) {
                newIndex = 0;
            }
            else if (newIndex >= this.rows.length) {
                newIndex = this.rows.length;
            }
            /** @type {?} */
            var arr = this.rows.slice();
            arr.splice(oldIndex, 1);
            arr.splice(newIndex, 0, row);
            this.rows = arr;
            this.flushValue();
        }
    };
    /**
     * @param {?} row
     * @return {?}
     */
    DynamicTableModel.prototype.deleteRow = /**
     * @param {?} row
     * @return {?}
     */
    function (row) {
        if (row) {
            if (this.selectedRow === row) {
                this.selectedRow = null;
            }
            /** @type {?} */
            var idx = this.rows.indexOf(row);
            if (idx > -1) {
                this.rows.splice(idx, 1);
                this.flushValue();
            }
        }
    };
    /**
     * @param {?} row
     * @return {?}
     */
    DynamicTableModel.prototype.addRow = /**
     * @param {?} row
     * @return {?}
     */
    function (row) {
        if (row) {
            this.rows.push(row);
            // this.selectedRow = row;
        }
    };
    /**
     * @param {?} row
     * @return {?}
     */
    DynamicTableModel.prototype.validateRow = /**
     * @param {?} row
     * @return {?}
     */
    function (row) {
        var e_1, _a, e_2, _b;
        /** @type {?} */
        var summary = new DynamicRowValidationSummary({
            isValid: true,
            message: null
        });
        /** @type {?} */
        var event = new ValidateDynamicTableRowEvent(this.form, this.field, row, summary);
        this.formService.validateDynamicTableRow.next(event);
        if (event.defaultPrevented || !summary.isValid) {
            return summary;
        }
        if (row) {
            try {
                for (var _c = tslib_1.__values(this.columns), _d = _c.next(); !_d.done; _d = _c.next()) {
                    var col = _d.value;
                    try {
                        for (var _e = tslib_1.__values(this._validators), _f = _e.next(); !_f.done; _f = _e.next()) {
                            var validator = _f.value;
                            if (!validator.validate(row, col, summary)) {
                                return summary;
                            }
                        }
                    }
                    catch (e_2_1) { e_2 = { error: e_2_1 }; }
                    finally {
                        try {
                            if (_f && !_f.done && (_b = _e.return)) _b.call(_e);
                        }
                        finally { if (e_2) throw e_2.error; }
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (_d && !_d.done && (_a = _c.return)) _a.call(_c);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        return summary;
    };
    /**
     * @param {?} row
     * @param {?} column
     * @return {?}
     */
    DynamicTableModel.prototype.getCellValue = /**
     * @param {?} row
     * @param {?} column
     * @return {?}
     */
    function (row, column) {
        /** @type {?} */
        var rowValue = row.value[column.id];
        if (column.type === 'Dropdown') {
            if (rowValue) {
                return rowValue.name;
            }
        }
        if (column.type === 'Boolean') {
            return rowValue ? true : false;
        }
        if (column.type === 'Date') {
            if (rowValue) {
                return moment(rowValue.split('T')[0], 'YYYY-MM-DD').format('DD-MM-YYYY');
            }
        }
        return rowValue || '';
    };
    /**
     * @param {?} column
     * @return {?}
     */
    DynamicTableModel.prototype.getDisplayText = /**
     * @param {?} column
     * @return {?}
     */
    function (column) {
        /** @type {?} */
        var columnName = column.name;
        if (column.type === 'Amount') {
            /** @type {?} */
            var currency = column.amountCurrency || '$';
            columnName = column.name + " (" + currency + ")";
        }
        return columnName;
    };
    return DynamicTableModel;
}(FormWidgetModel));
export { DynamicTableModel };
if (false) {
    /** @type {?} */
    DynamicTableModel.prototype.field;
    /** @type {?} */
    DynamicTableModel.prototype.columns;
    /** @type {?} */
    DynamicTableModel.prototype.visibleColumns;
    /** @type {?} */
    DynamicTableModel.prototype.rows;
    /**
     * @type {?}
     * @private
     */
    DynamicTableModel.prototype._selectedRow;
    /**
     * @type {?}
     * @private
     */
    DynamicTableModel.prototype._validators;
    /**
     * @type {?}
     * @private
     */
    DynamicTableModel.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHluYW1pYy10YWJsZS53aWRnZXQubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9keW5hbWljLXRhYmxlL2R5bmFtaWMtdGFibGUud2lkZ2V0Lm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxNQUFNLE1BQU0sWUFBWSxDQUFDO0FBQ2hDLE9BQU8sRUFBRSw0QkFBNEIsRUFBRSxNQUFNLGtEQUFrRCxDQUFDO0FBR2hHLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUU5RCxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQztBQUNoRSxPQUFPLEVBQUUsMkJBQTJCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUdyRixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUNwRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUV4RTtJQUF1Qyw2Q0FBZTtJQThCbEQsMkJBQVksS0FBcUIsRUFBVSxXQUF3QjtRQUFuRSxZQUNJLGtCQUFNLEtBQUssQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLElBQUksQ0FBQyxTQW9CaEM7UUFyQjBDLGlCQUFXLEdBQVgsV0FBVyxDQUFhO1FBM0JuRSxhQUFPLEdBQXlCLEVBQUUsQ0FBQztRQUNuQyxvQkFBYyxHQUF5QixFQUFFLENBQUM7UUFDMUMsVUFBSSxHQUFzQixFQUFFLENBQUM7UUFHckIsaUJBQVcsR0FBb0IsRUFBRSxDQUFDO1FBd0J0QyxLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQztRQUVuQixJQUFJLEtBQUssQ0FBQyxJQUFJLEVBQUU7O2dCQUNOLE9BQU8sR0FBRyxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQztZQUN0QyxJQUFJLE9BQU8sRUFBRTtnQkFDVCxLQUFJLENBQUMsT0FBTyxHQUFHLE9BQU8sQ0FBQztnQkFDdkIsS0FBSSxDQUFDLGNBQWMsR0FBRyxLQUFJLENBQUMsT0FBTyxDQUFDLE1BQU07Ozs7Z0JBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxHQUFHLENBQUMsT0FBTyxFQUFYLENBQVcsRUFBQyxDQUFDO2FBQ25FO1lBRUQsSUFBSSxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtnQkFDbEIsS0FBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxHQUFHOzs7O2dCQUFDLFVBQUMsR0FBRyxXQUFLLG1CQUFrQixFQUFDLFFBQVEsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLEdBQUcsRUFBQyxFQUFBLEdBQUEsRUFBQyxDQUFDO2FBQzlGO1NBQ0o7UUFFRCxLQUFJLENBQUMsV0FBVyxHQUFHO1lBQ2YsSUFBSSxxQkFBcUIsRUFBRTtZQUMzQixJQUFJLGlCQUFpQixFQUFFO1lBQ3ZCLElBQUksbUJBQW1CLEVBQUU7U0FDNUIsQ0FBQzs7SUFDTixDQUFDO0lBekNELHNCQUFJLDBDQUFXOzs7O1FBQWY7WUFDSSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7UUFDN0IsQ0FBQzs7Ozs7UUFFRCxVQUFnQixLQUFzQjtZQUNsQyxJQUFJLElBQUksQ0FBQyxZQUFZLElBQUksSUFBSSxDQUFDLFlBQVksS0FBSyxLQUFLLEVBQUU7Z0JBQ2xELElBQUksQ0FBQyxZQUFZLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQztnQkFDbkMsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUM7Z0JBQ3pCLE9BQU87YUFDVjtZQUVELElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTzs7OztZQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsR0FBRyxDQUFDLFFBQVEsR0FBRyxLQUFLLEVBQXBCLENBQW9CLEVBQUMsQ0FBQztZQUVqRCxJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQztZQUUxQixJQUFJLEtBQUssRUFBRTtnQkFDUCxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUM7YUFDckM7UUFDTCxDQUFDOzs7T0FoQkE7Ozs7OztJQXlDTyxzQ0FBVTs7Ozs7SUFBbEIsVUFBbUIsS0FBcUI7UUFDcEMsSUFBSSxLQUFLLElBQUksS0FBSyxDQUFDLElBQUksRUFBRTs7Z0JBQ2pCLFdBQVcsR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDLGlCQUFpQjtZQUM5QyxJQUFJLENBQUMsV0FBVyxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxJQUFJLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssRUFBRTtnQkFDOUQsV0FBVyxHQUFHLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQzthQUMzRDtZQUVELElBQUksV0FBVyxFQUFFO2dCQUNiLE9BQU8sV0FBVyxDQUFDLEdBQUc7Ozs7Z0JBQUMsVUFBQyxHQUFHLFdBQUssbUJBQXFCLEdBQUcsRUFBQSxHQUFBLEVBQUMsQ0FBQzthQUM3RDtTQUNKO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQzs7OztJQUVELHNDQUFVOzs7SUFBVjtRQUNJLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNaLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRzs7OztZQUFDLFVBQUMsQ0FBQyxJQUFLLE9BQUEsQ0FBQyxDQUFDLEtBQUssRUFBUCxDQUFPLEVBQUMsQ0FBQztZQUNqRCxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxDQUFDO1NBQzNCO0lBQ0wsQ0FBQzs7Ozs7O0lBRUQsbUNBQU87Ozs7O0lBQVAsVUFBUSxHQUFvQixFQUFFLE1BQWM7O1lBQ2xDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUM7UUFDdkMsSUFBSSxRQUFRLEdBQUcsQ0FBQyxDQUFDLEVBQUU7O2dCQUNYLFFBQVEsR0FBRyxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUM7WUFFbEMsSUFBSSxRQUFRLEdBQUcsQ0FBQyxFQUFFO2dCQUNkLFFBQVEsR0FBRyxDQUFDLENBQUM7YUFDaEI7aUJBQU0sSUFBSSxRQUFRLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUU7Z0JBQ3JDLFFBQVEsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQzthQUMvQjs7Z0JBRUssR0FBRyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQzdCLEdBQUcsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsQ0FBQyxDQUFDO1lBQ3hCLEdBQUcsQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUMsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUM3QixJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQztZQUVoQixJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7U0FDckI7SUFDTCxDQUFDOzs7OztJQUVELHFDQUFTOzs7O0lBQVQsVUFBVSxHQUFvQjtRQUMxQixJQUFJLEdBQUcsRUFBRTtZQUNMLElBQUksSUFBSSxDQUFDLFdBQVcsS0FBSyxHQUFHLEVBQUU7Z0JBQzFCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDO2FBQzNCOztnQkFDSyxHQUFHLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDO1lBQ2xDLElBQUksR0FBRyxHQUFHLENBQUMsQ0FBQyxFQUFFO2dCQUNWLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEdBQUcsRUFBRSxDQUFDLENBQUMsQ0FBQztnQkFDekIsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO2FBQ3JCO1NBQ0o7SUFDTCxDQUFDOzs7OztJQUVELGtDQUFNOzs7O0lBQU4sVUFBTyxHQUFvQjtRQUN2QixJQUFJLEdBQUcsRUFBRTtZQUNMLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQ3BCLDBCQUEwQjtTQUM3QjtJQUNMLENBQUM7Ozs7O0lBRUQsdUNBQVc7Ozs7SUFBWCxVQUFZLEdBQW9COzs7WUFDdEIsT0FBTyxHQUFHLElBQUksMkJBQTJCLENBQUU7WUFDN0MsT0FBTyxFQUFFLElBQUk7WUFDYixPQUFPLEVBQUUsSUFBSTtTQUNoQixDQUFDOztZQUVJLEtBQUssR0FBRyxJQUFJLDRCQUE0QixDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLEtBQUssRUFBRSxHQUFHLEVBQUUsT0FBTyxDQUFDO1FBQ25GLElBQUksQ0FBQyxXQUFXLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBRXJELElBQUksS0FBSyxDQUFDLGdCQUFnQixJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sRUFBRTtZQUM1QyxPQUFPLE9BQU8sQ0FBQztTQUNsQjtRQUVELElBQUksR0FBRyxFQUFFOztnQkFDTCxLQUFrQixJQUFBLEtBQUEsaUJBQUEsSUFBSSxDQUFDLE9BQU8sQ0FBQSxnQkFBQSw0QkFBRTtvQkFBM0IsSUFBTSxHQUFHLFdBQUE7O3dCQUNWLEtBQXdCLElBQUEsS0FBQSxpQkFBQSxJQUFJLENBQUMsV0FBVyxDQUFBLGdCQUFBLDRCQUFFOzRCQUFyQyxJQUFNLFNBQVMsV0FBQTs0QkFDaEIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsR0FBRyxFQUFFLEdBQUcsRUFBRSxPQUFPLENBQUMsRUFBRTtnQ0FDeEMsT0FBTyxPQUFPLENBQUM7NkJBQ2xCO3lCQUNKOzs7Ozs7Ozs7aUJBQ0o7Ozs7Ozs7OztTQUNKO1FBRUQsT0FBTyxPQUFPLENBQUM7SUFDbkIsQ0FBQzs7Ozs7O0lBRUQsd0NBQVk7Ozs7O0lBQVosVUFBYSxHQUFvQixFQUFFLE1BQTBCOztZQUNuRCxRQUFRLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDO1FBRXJDLElBQUksTUFBTSxDQUFDLElBQUksS0FBSyxVQUFVLEVBQUU7WUFDNUIsSUFBSSxRQUFRLEVBQUU7Z0JBQ1YsT0FBTyxRQUFRLENBQUMsSUFBSSxDQUFDO2FBQ3hCO1NBQ0o7UUFFRCxJQUFJLE1BQU0sQ0FBQyxJQUFJLEtBQUssU0FBUyxFQUFFO1lBQzNCLE9BQU8sUUFBUSxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztTQUNsQztRQUVELElBQUksTUFBTSxDQUFDLElBQUksS0FBSyxNQUFNLEVBQUU7WUFDeEIsSUFBSSxRQUFRLEVBQUU7Z0JBQ1YsT0FBTyxNQUFNLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxZQUFZLENBQUMsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDNUU7U0FDSjtRQUVELE9BQU8sUUFBUSxJQUFJLEVBQUUsQ0FBQztJQUMxQixDQUFDOzs7OztJQUVELDBDQUFjOzs7O0lBQWQsVUFBZSxNQUEwQjs7WUFDakMsVUFBVSxHQUFHLE1BQU0sQ0FBQyxJQUFJO1FBQzVCLElBQUksTUFBTSxDQUFDLElBQUksS0FBSyxRQUFRLEVBQUU7O2dCQUNwQixRQUFRLEdBQUcsTUFBTSxDQUFDLGNBQWMsSUFBSSxHQUFHO1lBQzdDLFVBQVUsR0FBTSxNQUFNLENBQUMsSUFBSSxVQUFLLFFBQVEsTUFBRyxDQUFDO1NBQy9DO1FBQ0QsT0FBTyxVQUFVLENBQUM7SUFDdEIsQ0FBQztJQUNMLHdCQUFDO0FBQUQsQ0FBQyxBQTFLRCxDQUF1QyxlQUFlLEdBMEtyRDs7OztJQXhLRyxrQ0FBc0I7O0lBQ3RCLG9DQUFtQzs7SUFDbkMsMkNBQTBDOztJQUMxQyxpQ0FBNkI7Ozs7O0lBRTdCLHlDQUFzQzs7Ozs7SUFDdEMsd0NBQTBDOzs7OztJQXNCUCx3Q0FBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuIC8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50LWVzNic7XHJcbmltcG9ydCB7IFZhbGlkYXRlRHluYW1pY1RhYmxlUm93RXZlbnQgfSBmcm9tICcuLi8uLi8uLi9ldmVudHMvdmFsaWRhdGUtZHluYW1pYy10YWJsZS1yb3cuZXZlbnQnO1xyXG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4vLi4vLi4vLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRm9ybUZpZWxkTW9kZWwgfSBmcm9tICcuLy4uL2NvcmUvZm9ybS1maWVsZC5tb2RlbCc7XHJcbmltcG9ydCB7IEZvcm1XaWRnZXRNb2RlbCB9IGZyb20gJy4vLi4vY29yZS9mb3JtLXdpZGdldC5tb2RlbCc7XHJcbmltcG9ydCB7IENlbGxWYWxpZGF0b3IgfSBmcm9tICcuL2NlbGwtdmFsaWRhdG9yLm1vZGVsJztcclxuaW1wb3J0IHsgRGF0ZUNlbGxWYWxpZGF0b3IgfSBmcm9tICcuL2RhdGUtY2VsbC12YWxpZGF0b3ItbW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljUm93VmFsaWRhdGlvblN1bW1hcnkgfSBmcm9tICcuL2R5bmFtaWMtcm93LXZhbGlkYXRpb24tc3VtbWFyeS5tb2RlbCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZUNvbHVtbiB9IGZyb20gJy4vZHluYW1pYy10YWJsZS1jb2x1bW4ubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVSb3cgfSBmcm9tICcuL2R5bmFtaWMtdGFibGUtcm93Lm1vZGVsJztcclxuaW1wb3J0IHsgTnVtYmVyQ2VsbFZhbGlkYXRvciB9IGZyb20gJy4vbnVtYmVyLWNlbGwtdmFsaWRhdG9yLm1vZGVsJztcclxuaW1wb3J0IHsgUmVxdWlyZWRDZWxsVmFsaWRhdG9yIH0gZnJvbSAnLi9yZXF1aXJlZC1jZWxsLXZhbGlkYXRvci5tb2RlbCc7XHJcblxyXG5leHBvcnQgY2xhc3MgRHluYW1pY1RhYmxlTW9kZWwgZXh0ZW5kcyBGb3JtV2lkZ2V0TW9kZWwge1xyXG5cclxuICAgIGZpZWxkOiBGb3JtRmllbGRNb2RlbDtcclxuICAgIGNvbHVtbnM6IER5bmFtaWNUYWJsZUNvbHVtbltdID0gW107XHJcbiAgICB2aXNpYmxlQ29sdW1uczogRHluYW1pY1RhYmxlQ29sdW1uW10gPSBbXTtcclxuICAgIHJvd3M6IER5bmFtaWNUYWJsZVJvd1tdID0gW107XHJcblxyXG4gICAgcHJpdmF0ZSBfc2VsZWN0ZWRSb3c6IER5bmFtaWNUYWJsZVJvdztcclxuICAgIHByaXZhdGUgX3ZhbGlkYXRvcnM6IENlbGxWYWxpZGF0b3JbXSA9IFtdO1xyXG5cclxuICAgIGdldCBzZWxlY3RlZFJvdygpOiBEeW5hbWljVGFibGVSb3cge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9zZWxlY3RlZFJvdztcclxuICAgIH1cclxuXHJcbiAgICBzZXQgc2VsZWN0ZWRSb3codmFsdWU6IER5bmFtaWNUYWJsZVJvdykge1xyXG4gICAgICAgIGlmICh0aGlzLl9zZWxlY3RlZFJvdyAmJiB0aGlzLl9zZWxlY3RlZFJvdyA9PT0gdmFsdWUpIHtcclxuICAgICAgICAgICAgdGhpcy5fc2VsZWN0ZWRSb3cuc2VsZWN0ZWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgdGhpcy5fc2VsZWN0ZWRSb3cgPSBudWxsO1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLnJvd3MuZm9yRWFjaCgocm93KSA9PiByb3cuc2VsZWN0ZWQgPSBmYWxzZSk7XHJcblxyXG4gICAgICAgIHRoaXMuX3NlbGVjdGVkUm93ID0gdmFsdWU7XHJcblxyXG4gICAgICAgIGlmICh2YWx1ZSkge1xyXG4gICAgICAgICAgICB0aGlzLl9zZWxlY3RlZFJvdy5zZWxlY3RlZCA9IHRydWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKGZpZWxkOiBGb3JtRmllbGRNb2RlbCwgcHJpdmF0ZSBmb3JtU2VydmljZTogRm9ybVNlcnZpY2UpIHtcclxuICAgICAgICBzdXBlcihmaWVsZC5mb3JtLCBmaWVsZC5qc29uKTtcclxuICAgICAgICB0aGlzLmZpZWxkID0gZmllbGQ7XHJcblxyXG4gICAgICAgIGlmIChmaWVsZC5qc29uKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGNvbHVtbnMgPSB0aGlzLmdldENvbHVtbnMoZmllbGQpO1xyXG4gICAgICAgICAgICBpZiAoY29sdW1ucykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jb2x1bW5zID0gY29sdW1ucztcclxuICAgICAgICAgICAgICAgIHRoaXMudmlzaWJsZUNvbHVtbnMgPSB0aGlzLmNvbHVtbnMuZmlsdGVyKChjb2wpID0+IGNvbC52aXNpYmxlKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGZpZWxkLmpzb24udmFsdWUpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucm93cyA9IGZpZWxkLmpzb24udmFsdWUubWFwKChvYmopID0+IDxEeW5hbWljVGFibGVSb3c+IHtzZWxlY3RlZDogZmFsc2UsIHZhbHVlOiBvYmp9KTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy5fdmFsaWRhdG9ycyA9IFtcclxuICAgICAgICAgICAgbmV3IFJlcXVpcmVkQ2VsbFZhbGlkYXRvcigpLFxyXG4gICAgICAgICAgICBuZXcgRGF0ZUNlbGxWYWxpZGF0b3IoKSxcclxuICAgICAgICAgICAgbmV3IE51bWJlckNlbGxWYWxpZGF0b3IoKVxyXG4gICAgICAgIF07XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRDb2x1bW5zKGZpZWxkOiBGb3JtRmllbGRNb2RlbCk6IER5bmFtaWNUYWJsZUNvbHVtbltdIHtcclxuICAgICAgICBpZiAoZmllbGQgJiYgZmllbGQuanNvbikge1xyXG4gICAgICAgICAgICBsZXQgZGVmaW5pdGlvbnMgPSBmaWVsZC5qc29uLmNvbHVtbkRlZmluaXRpb25zO1xyXG4gICAgICAgICAgICBpZiAoIWRlZmluaXRpb25zICYmIGZpZWxkLmpzb24ucGFyYW1zICYmIGZpZWxkLmpzb24ucGFyYW1zLmZpZWxkKSB7XHJcbiAgICAgICAgICAgICAgICBkZWZpbml0aW9ucyA9IGZpZWxkLmpzb24ucGFyYW1zLmZpZWxkLmNvbHVtbkRlZmluaXRpb25zO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoZGVmaW5pdGlvbnMpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBkZWZpbml0aW9ucy5tYXAoKG9iaikgPT4gPER5bmFtaWNUYWJsZUNvbHVtbj4gb2JqKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBmbHVzaFZhbHVlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLmZpZWxkKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZmllbGQudmFsdWUgPSB0aGlzLnJvd3MubWFwKChyKSA9PiByLnZhbHVlKTtcclxuICAgICAgICAgICAgdGhpcy5maWVsZC51cGRhdGVGb3JtKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG1vdmVSb3cocm93OiBEeW5hbWljVGFibGVSb3csIG9mZnNldDogbnVtYmVyKSB7XHJcbiAgICAgICAgY29uc3Qgb2xkSW5kZXggPSB0aGlzLnJvd3MuaW5kZXhPZihyb3cpO1xyXG4gICAgICAgIGlmIChvbGRJbmRleCA+IC0xKSB7XHJcbiAgICAgICAgICAgIGxldCBuZXdJbmRleCA9IChvbGRJbmRleCArIG9mZnNldCk7XHJcblxyXG4gICAgICAgICAgICBpZiAobmV3SW5kZXggPCAwKSB7XHJcbiAgICAgICAgICAgICAgICBuZXdJbmRleCA9IDA7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAobmV3SW5kZXggPj0gdGhpcy5yb3dzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgbmV3SW5kZXggPSB0aGlzLnJvd3MubGVuZ3RoO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb25zdCBhcnIgPSB0aGlzLnJvd3Muc2xpY2UoKTtcclxuICAgICAgICAgICAgYXJyLnNwbGljZShvbGRJbmRleCwgMSk7XHJcbiAgICAgICAgICAgIGFyci5zcGxpY2UobmV3SW5kZXgsIDAsIHJvdyk7XHJcbiAgICAgICAgICAgIHRoaXMucm93cyA9IGFycjtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZmx1c2hWYWx1ZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBkZWxldGVSb3cocm93OiBEeW5hbWljVGFibGVSb3cpIHtcclxuICAgICAgICBpZiAocm93KSB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLnNlbGVjdGVkUm93ID09PSByb3cpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2VsZWN0ZWRSb3cgPSBudWxsO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNvbnN0IGlkeCA9IHRoaXMucm93cy5pbmRleE9mKHJvdyk7XHJcbiAgICAgICAgICAgIGlmIChpZHggPiAtMSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yb3dzLnNwbGljZShpZHgsIDEpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mbHVzaFZhbHVlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgYWRkUm93KHJvdzogRHluYW1pY1RhYmxlUm93KSB7XHJcbiAgICAgICAgaWYgKHJvdykge1xyXG4gICAgICAgICAgICB0aGlzLnJvd3MucHVzaChyb3cpO1xyXG4gICAgICAgICAgICAvLyB0aGlzLnNlbGVjdGVkUm93ID0gcm93O1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB2YWxpZGF0ZVJvdyhyb3c6IER5bmFtaWNUYWJsZVJvdyk6IER5bmFtaWNSb3dWYWxpZGF0aW9uU3VtbWFyeSB7XHJcbiAgICAgICAgY29uc3Qgc3VtbWFyeSA9IG5ldyBEeW5hbWljUm93VmFsaWRhdGlvblN1bW1hcnkoIHtcclxuICAgICAgICAgICAgaXNWYWxpZDogdHJ1ZSxcclxuICAgICAgICAgICAgbWVzc2FnZTogbnVsbFxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICBjb25zdCBldmVudCA9IG5ldyBWYWxpZGF0ZUR5bmFtaWNUYWJsZVJvd0V2ZW50KHRoaXMuZm9ybSwgdGhpcy5maWVsZCwgcm93LCBzdW1tYXJ5KTtcclxuICAgICAgICB0aGlzLmZvcm1TZXJ2aWNlLnZhbGlkYXRlRHluYW1pY1RhYmxlUm93Lm5leHQoZXZlbnQpO1xyXG5cclxuICAgICAgICBpZiAoZXZlbnQuZGVmYXVsdFByZXZlbnRlZCB8fCAhc3VtbWFyeS5pc1ZhbGlkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBzdW1tYXJ5O1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHJvdykge1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IGNvbCBvZiB0aGlzLmNvbHVtbnMpIHtcclxuICAgICAgICAgICAgICAgIGZvciAoY29uc3QgdmFsaWRhdG9yIG9mIHRoaXMuX3ZhbGlkYXRvcnMpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIXZhbGlkYXRvci52YWxpZGF0ZShyb3csIGNvbCwgc3VtbWFyeSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHN1bW1hcnk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gc3VtbWFyeTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRDZWxsVmFsdWUocm93OiBEeW5hbWljVGFibGVSb3csIGNvbHVtbjogRHluYW1pY1RhYmxlQ29sdW1uKTogYW55IHtcclxuICAgICAgICBjb25zdCByb3dWYWx1ZSA9IHJvdy52YWx1ZVtjb2x1bW4uaWRdO1xyXG5cclxuICAgICAgICBpZiAoY29sdW1uLnR5cGUgPT09ICdEcm9wZG93bicpIHtcclxuICAgICAgICAgICAgaWYgKHJvd1ZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcm93VmFsdWUubmFtZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGNvbHVtbi50eXBlID09PSAnQm9vbGVhbicpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHJvd1ZhbHVlID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGNvbHVtbi50eXBlID09PSAnRGF0ZScpIHtcclxuICAgICAgICAgICAgaWYgKHJvd1ZhbHVlKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbW9tZW50KHJvd1ZhbHVlLnNwbGl0KCdUJylbMF0sICdZWVlZLU1NLUREJykuZm9ybWF0KCdERC1NTS1ZWVlZJyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiByb3dWYWx1ZSB8fCAnJztcclxuICAgIH1cclxuXHJcbiAgICBnZXREaXNwbGF5VGV4dChjb2x1bW46IER5bmFtaWNUYWJsZUNvbHVtbik6IHN0cmluZyB7XHJcbiAgICAgICAgbGV0IGNvbHVtbk5hbWUgPSBjb2x1bW4ubmFtZTtcclxuICAgICAgICBpZiAoY29sdW1uLnR5cGUgPT09ICdBbW91bnQnKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGN1cnJlbmN5ID0gY29sdW1uLmFtb3VudEN1cnJlbmN5IHx8ICckJztcclxuICAgICAgICAgICAgY29sdW1uTmFtZSA9IGAke2NvbHVtbi5uYW1lfSAoJHtjdXJyZW5jeX0pYDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGNvbHVtbk5hbWU7XHJcbiAgICB9XHJcbn1cclxuIl19