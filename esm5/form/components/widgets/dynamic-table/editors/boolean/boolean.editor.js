/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, Input } from '@angular/core';
import { DynamicTableModel } from './../../dynamic-table.widget.model';
var BooleanEditorComponent = /** @class */ (function () {
    function BooleanEditorComponent() {
    }
    /**
     * @param {?} row
     * @param {?} column
     * @param {?} event
     * @return {?}
     */
    BooleanEditorComponent.prototype.onValueChanged = /**
     * @param {?} row
     * @param {?} column
     * @param {?} event
     * @return {?}
     */
    function (row, column, event) {
        /** @type {?} */
        var value = ((/** @type {?} */ (event))).checked;
        row.value[column.id] = value;
    };
    BooleanEditorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-boolean-editor',
                    template: " <label [attr.for]=\"column.id\">\r\n    <mat-checkbox\r\n        color=\"primary\"\r\n        [id]=\"column.id\"\r\n        [checked]=\"table.getCellValue(row, column)\"\r\n        [required]=\"column.required\"\r\n        [disabled]=\"!column.editable\"\r\n        (change)=\"onValueChanged(row, column, $event)\">\r\n    <span class=\"adf-checkbox-label\">{{column.name}}</span>\r\n    </mat-checkbox>\r\n</label>\r\n",
                    styles: [".adf-checkbox-label{position:relative;cursor:pointer;font-size:16px;line-height:24px;margin:0}"]
                }] }
    ];
    BooleanEditorComponent.propDecorators = {
        table: [{ type: Input }],
        row: [{ type: Input }],
        column: [{ type: Input }]
    };
    return BooleanEditorComponent;
}());
export { BooleanEditorComponent };
if (false) {
    /** @type {?} */
    BooleanEditorComponent.prototype.table;
    /** @type {?} */
    BooleanEditorComponent.prototype.row;
    /** @type {?} */
    BooleanEditorComponent.prototype.column;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYm9vbGVhbi5lZGl0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9keW5hbWljLXRhYmxlL2VkaXRvcnMvYm9vbGVhbi9ib29sZWFuLmVkaXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHakQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFFdkU7SUFBQTtJQXFCQSxDQUFDOzs7Ozs7O0lBTEcsK0NBQWM7Ozs7OztJQUFkLFVBQWUsR0FBb0IsRUFBRSxNQUEwQixFQUFFLEtBQVU7O1lBQ2pFLEtBQUssR0FBWSxDQUFDLG1CQUFtQixLQUFLLEVBQUEsQ0FBQyxDQUFDLE9BQU87UUFDekQsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEdBQUcsS0FBSyxDQUFDO0lBQ2pDLENBQUM7O2dCQW5CSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLG9CQUFvQjtvQkFDOUIsZ2JBQW9DOztpQkFFdkM7Ozt3QkFHSSxLQUFLO3NCQUdMLEtBQUs7eUJBR0wsS0FBSzs7SUFRViw2QkFBQztDQUFBLEFBckJELElBcUJDO1NBaEJZLHNCQUFzQjs7O0lBRS9CLHVDQUN5Qjs7SUFFekIscUNBQ3FCOztJQUVyQix3Q0FDMkIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVDb2x1bW4gfSBmcm9tICcuLy4uLy4uL2R5bmFtaWMtdGFibGUtY29sdW1uLm1vZGVsJztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlUm93IH0gZnJvbSAnLi8uLi8uLi9keW5hbWljLXRhYmxlLXJvdy5tb2RlbCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZU1vZGVsIH0gZnJvbSAnLi8uLi8uLi9keW5hbWljLXRhYmxlLndpZGdldC5tb2RlbCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLWJvb2xlYW4tZWRpdG9yJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9ib29sZWFuLmVkaXRvci5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2Jvb2xlYW4uZWRpdG9yLnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgQm9vbGVhbkVkaXRvckNvbXBvbmVudCB7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHRhYmxlOiBEeW5hbWljVGFibGVNb2RlbDtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgcm93OiBEeW5hbWljVGFibGVSb3c7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGNvbHVtbjogRHluYW1pY1RhYmxlQ29sdW1uO1xyXG5cclxuICAgIG9uVmFsdWVDaGFuZ2VkKHJvdzogRHluYW1pY1RhYmxlUm93LCBjb2x1bW46IER5bmFtaWNUYWJsZUNvbHVtbiwgZXZlbnQ6IGFueSkge1xyXG4gICAgICAgIGNvbnN0IHZhbHVlOiBib29sZWFuID0gKDxIVE1MSW5wdXRFbGVtZW50PiBldmVudCkuY2hlY2tlZDtcclxuICAgICAgICByb3cudmFsdWVbY29sdW1uLmlkXSA9IHZhbHVlO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=