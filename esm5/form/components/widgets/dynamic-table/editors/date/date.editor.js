/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { UserPreferencesService, UserPreferenceValues } from '../../../../../../services/user-preferences.service';
import { MomentDateAdapter } from '../../../../../../utils/momentDateAdapter';
import { MOMENT_DATE_FORMATS } from '../../../../../../utils/moment-date-formats.model';
import { Component, Input } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import moment from 'moment-es6';
import { DynamicTableModel } from './../../dynamic-table.widget.model';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
var ɵ0 = MOMENT_DATE_FORMATS;
var DateEditorComponent = /** @class */ (function () {
    function DateEditorComponent(dateAdapter, userPreferencesService) {
        this.dateAdapter = dateAdapter;
        this.userPreferencesService = userPreferencesService;
        this.DATE_FORMAT = 'DD-MM-YYYY';
        this.onDestroy$ = new Subject();
    }
    /**
     * @return {?}
     */
    DateEditorComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.userPreferencesService
            .select(UserPreferenceValues.Locale)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} locale
         * @return {?}
         */
        function (locale) { return _this.dateAdapter.setLocale(locale); }));
        /** @type {?} */
        var momentDateAdapter = (/** @type {?} */ (this.dateAdapter));
        momentDateAdapter.overrideDisplayFormat = this.DATE_FORMAT;
        this.value = moment(this.table.getCellValue(this.row, this.column), this.DATE_FORMAT);
    };
    /**
     * @return {?}
     */
    DateEditorComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    };
    /**
     * @param {?} newDateValue
     * @return {?}
     */
    DateEditorComponent.prototype.onDateChanged = /**
     * @param {?} newDateValue
     * @return {?}
     */
    function (newDateValue) {
        if (newDateValue && newDateValue.value) {
            /* validates the user inputs */
            /** @type {?} */
            var momentDate = moment(newDateValue.value, this.DATE_FORMAT, true);
            if (!momentDate.isValid()) {
                this.row.value[this.column.id] = newDateValue.value;
            }
            else {
                this.row.value[this.column.id] = momentDate.format('YYYY-MM-DD') + "T00:00:00.000Z";
                this.table.flushValue();
            }
        }
        else {
            /* removes the date  */
            this.row.value[this.column.id] = '';
        }
    };
    DateEditorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-date-editor',
                    template: "<div>\r\n    <mat-form-field class=\"adf-date-editor\">\r\n        <label [attr.for]=\"column.id\">{{column.name}} ({{DATE_FORMAT}})</label>\r\n        <input matInput\r\n            id=\"dateInput\"\r\n            type=\"text\"\r\n            [matDatepicker]=\"datePicker\"\r\n            [value]=\"value\"\r\n            [id]=\"column.id\"\r\n            [required]=\"column.required\"\r\n            [disabled]=\"!column.editable\"\r\n            (focusout)=\"onDateChanged($event.srcElement)\"\r\n            (dateChange)=\"onDateChanged($event)\">\r\n        <mat-datepicker-toggle  *ngIf=\"column.editable\" matSuffix [for]=\"datePicker\" class=\"adf-date-editor-button\" ></mat-datepicker-toggle>\r\n    </mat-form-field>\r\n    <mat-datepicker #datePicker [touchUi]=\"true\"></mat-datepicker>\r\n</div>\r\n",
                    providers: [
                        { provide: DateAdapter, useClass: MomentDateAdapter },
                        { provide: MAT_DATE_FORMATS, useValue: ɵ0 }
                    ],
                    styles: [".adf-date-editor{width:100%}.adf-date-editor-button{position:relative;top:25px}"]
                }] }
    ];
    /** @nocollapse */
    DateEditorComponent.ctorParameters = function () { return [
        { type: DateAdapter },
        { type: UserPreferencesService }
    ]; };
    DateEditorComponent.propDecorators = {
        table: [{ type: Input }],
        row: [{ type: Input }],
        column: [{ type: Input }]
    };
    return DateEditorComponent;
}());
export { DateEditorComponent };
if (false) {
    /** @type {?} */
    DateEditorComponent.prototype.DATE_FORMAT;
    /** @type {?} */
    DateEditorComponent.prototype.value;
    /** @type {?} */
    DateEditorComponent.prototype.table;
    /** @type {?} */
    DateEditorComponent.prototype.row;
    /** @type {?} */
    DateEditorComponent.prototype.column;
    /** @type {?} */
    DateEditorComponent.prototype.minDate;
    /** @type {?} */
    DateEditorComponent.prototype.maxDate;
    /**
     * @type {?}
     * @private
     */
    DateEditorComponent.prototype.onDestroy$;
    /**
     * @type {?}
     * @private
     */
    DateEditorComponent.prototype.dateAdapter;
    /**
     * @type {?}
     * @private
     */
    DateEditorComponent.prototype.userPreferencesService;
}
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZS5lZGl0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9keW5hbWljLXRhYmxlL2VkaXRvcnMvZGF0ZS9kYXRlLmVkaXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLHNCQUFzQixFQUFFLG9CQUFvQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFFbkgsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFDOUUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sbURBQW1ELENBQUM7QUFDeEYsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQXFCLE1BQU0sZUFBZSxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxXQUFXLEVBQUUsZ0JBQWdCLEVBQTJCLE1BQU0sbUJBQW1CLENBQUM7QUFDM0YsT0FBTyxNQUFNLE1BQU0sWUFBWSxDQUFDO0FBSWhDLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLG9DQUFvQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO1NBT0csbUJBQW1CO0FBTGpFO0lBNEJJLDZCQUFvQixXQUFnQyxFQUNoQyxzQkFBOEM7UUFEOUMsZ0JBQVcsR0FBWCxXQUFXLENBQXFCO1FBQ2hDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFuQmxFLGdCQUFXLEdBQVcsWUFBWSxDQUFDO1FBZ0IzQixlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztJQUk1QyxDQUFDOzs7O0lBRUQsc0NBQVE7OztJQUFSO1FBQUEsaUJBVUM7UUFURyxJQUFJLENBQUMsc0JBQXNCO2FBQ3RCLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUM7YUFDbkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDaEMsU0FBUzs7OztRQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQWxDLENBQWtDLEVBQUMsQ0FBQzs7WUFFdkQsaUJBQWlCLEdBQUcsbUJBQW9CLElBQUksQ0FBQyxXQUFXLEVBQUE7UUFDOUQsaUJBQWlCLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztRQUUzRCxJQUFJLENBQUMsS0FBSyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7SUFDMUYsQ0FBQzs7OztJQUVELHlDQUFXOzs7SUFBWDtRQUNJLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQzNCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDL0IsQ0FBQzs7Ozs7SUFFRCwyQ0FBYTs7OztJQUFiLFVBQWMsWUFBNkQ7UUFDdkUsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLEtBQUssRUFBRTs7O2dCQUU5QixVQUFVLEdBQUcsTUFBTSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUUsSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUM7WUFFckUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLEVBQUUsRUFBRTtnQkFDdkIsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsR0FBSSxZQUFZLENBQUMsS0FBSyxDQUFDO2FBQ3hEO2lCQUFNO2dCQUNILElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEdBQU0sVUFBVSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsbUJBQWdCLENBQUM7Z0JBQ3BGLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUM7YUFDM0I7U0FDSjthQUFNO1lBQ0gsdUJBQXVCO1lBQ3ZCLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDLEdBQUcsRUFBRSxDQUFDO1NBQ3ZDO0lBQ0wsQ0FBQzs7Z0JBaEVKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsaUJBQWlCO29CQUMzQiwwekJBQWlDO29CQUNqQyxTQUFTLEVBQUU7d0JBQ1AsRUFBQyxPQUFPLEVBQUUsV0FBVyxFQUFFLFFBQVEsRUFBRSxpQkFBaUIsRUFBQzt3QkFDbkQsRUFBQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsUUFBUSxJQUFxQixFQUFDO3FCQUFDOztpQkFFbEU7Ozs7Z0JBaEJRLFdBQVc7Z0JBTFgsc0JBQXNCOzs7d0JBNEIxQixLQUFLO3NCQUdMLEtBQUs7eUJBR0wsS0FBSzs7SUE4Q1YsMEJBQUM7Q0FBQSxBQWxFRCxJQWtFQztTQTFEWSxtQkFBbUI7OztJQUU1QiwwQ0FBbUM7O0lBRW5DLG9DQUFXOztJQUVYLG9DQUN5Qjs7SUFFekIsa0NBQ3FCOztJQUVyQixxQ0FDMkI7O0lBRTNCLHNDQUFnQjs7SUFDaEIsc0NBQWdCOzs7OztJQUVoQix5Q0FBNEM7Ozs7O0lBRWhDLDBDQUF3Qzs7Ozs7SUFDeEMscURBQXNEIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UsIFVzZXJQcmVmZXJlbmNlVmFsdWVzIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vLi4vc2VydmljZXMvdXNlci1wcmVmZXJlbmNlcy5zZXJ2aWNlJztcclxuXHJcbmltcG9ydCB7IE1vbWVudERhdGVBZGFwdGVyIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vLi4vdXRpbHMvbW9tZW50RGF0ZUFkYXB0ZXInO1xyXG5pbXBvcnQgeyBNT01FTlRfREFURV9GT1JNQVRTIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vLi4vdXRpbHMvbW9tZW50LWRhdGUtZm9ybWF0cy5tb2RlbCc7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERhdGVBZGFwdGVyLCBNQVRfREFURV9GT1JNQVRTLCBNYXREYXRlcGlja2VySW5wdXRFdmVudCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IG1vbWVudCBmcm9tICdtb21lbnQtZXM2JztcclxuaW1wb3J0IHsgTW9tZW50IH0gZnJvbSAnbW9tZW50JztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlQ29sdW1uIH0gZnJvbSAnLi8uLi8uLi9keW5hbWljLXRhYmxlLWNvbHVtbi5tb2RlbCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZVJvdyB9IGZyb20gJy4vLi4vLi4vZHluYW1pYy10YWJsZS1yb3cubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVNb2RlbCB9IGZyb20gJy4vLi4vLi4vZHluYW1pYy10YWJsZS53aWRnZXQubW9kZWwnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtZGF0ZS1lZGl0b3InLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2RhdGUuZWRpdG9yLmh0bWwnLFxyXG4gICAgcHJvdmlkZXJzOiBbXHJcbiAgICAgICAge3Byb3ZpZGU6IERhdGVBZGFwdGVyLCB1c2VDbGFzczogTW9tZW50RGF0ZUFkYXB0ZXJ9LFxyXG4gICAgICAgIHtwcm92aWRlOiBNQVRfREFURV9GT1JNQVRTLCB1c2VWYWx1ZTogTU9NRU5UX0RBVEVfRk9STUFUU31dLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vZGF0ZS5lZGl0b3Iuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEYXRlRWRpdG9yQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3kge1xyXG5cclxuICAgIERBVEVfRk9STUFUOiBzdHJpbmcgPSAnREQtTU0tWVlZWSc7XHJcblxyXG4gICAgdmFsdWU6IGFueTtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgdGFibGU6IER5bmFtaWNUYWJsZU1vZGVsO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICByb3c6IER5bmFtaWNUYWJsZVJvdztcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgY29sdW1uOiBEeW5hbWljVGFibGVDb2x1bW47XHJcblxyXG4gICAgbWluRGF0ZTogTW9tZW50O1xyXG4gICAgbWF4RGF0ZTogTW9tZW50O1xyXG5cclxuICAgIHByaXZhdGUgb25EZXN0cm95JCA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBkYXRlQWRhcHRlcjogRGF0ZUFkYXB0ZXI8TW9tZW50PixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgdXNlclByZWZlcmVuY2VzU2VydmljZTogVXNlclByZWZlcmVuY2VzU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VzU2VydmljZVxyXG4gICAgICAgICAgICAuc2VsZWN0KFVzZXJQcmVmZXJlbmNlVmFsdWVzLkxvY2FsZSlcclxuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMub25EZXN0cm95JCkpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUobG9jYWxlID0+IHRoaXMuZGF0ZUFkYXB0ZXIuc2V0TG9jYWxlKGxvY2FsZSkpO1xyXG5cclxuICAgICAgICBjb25zdCBtb21lbnREYXRlQWRhcHRlciA9IDxNb21lbnREYXRlQWRhcHRlcj4gdGhpcy5kYXRlQWRhcHRlcjtcclxuICAgICAgICBtb21lbnREYXRlQWRhcHRlci5vdmVycmlkZURpc3BsYXlGb3JtYXQgPSB0aGlzLkRBVEVfRk9STUFUO1xyXG5cclxuICAgICAgICB0aGlzLnZhbHVlID0gbW9tZW50KHRoaXMudGFibGUuZ2V0Q2VsbFZhbHVlKHRoaXMucm93LCB0aGlzLmNvbHVtbiksIHRoaXMuREFURV9GT1JNQVQpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIHRoaXMub25EZXN0cm95JC5uZXh0KHRydWUpO1xyXG4gICAgICAgIHRoaXMub25EZXN0cm95JC5jb21wbGV0ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uRGF0ZUNoYW5nZWQobmV3RGF0ZVZhbHVlOiBNYXREYXRlcGlja2VySW5wdXRFdmVudDxhbnk+IHwgSFRNTElucHV0RWxlbWVudCkge1xyXG4gICAgICAgIGlmIChuZXdEYXRlVmFsdWUgJiYgbmV3RGF0ZVZhbHVlLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIC8qIHZhbGlkYXRlcyB0aGUgdXNlciBpbnB1dHMgKi9cclxuICAgICAgICAgICAgY29uc3QgbW9tZW50RGF0ZSA9IG1vbWVudChuZXdEYXRlVmFsdWUudmFsdWUsIHRoaXMuREFURV9GT1JNQVQsIHRydWUpO1xyXG5cclxuICAgICAgICAgICAgaWYgKCFtb21lbnREYXRlLmlzVmFsaWQoKSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yb3cudmFsdWVbdGhpcy5jb2x1bW4uaWRdID0gIG5ld0RhdGVWYWx1ZS52YWx1ZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucm93LnZhbHVlW3RoaXMuY29sdW1uLmlkXSA9IGAke21vbWVudERhdGUuZm9ybWF0KCdZWVlZLU1NLUREJyl9VDAwOjAwOjAwLjAwMFpgO1xyXG4gICAgICAgICAgICAgICAgdGhpcy50YWJsZS5mbHVzaFZhbHVlKCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvKiByZW1vdmVzIHRoZSBkYXRlICAqL1xyXG4gICAgICAgICAgICB0aGlzLnJvdy52YWx1ZVt0aGlzLmNvbHVtbi5pZF0gPSAnJztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==