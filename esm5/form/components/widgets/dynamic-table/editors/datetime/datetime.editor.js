/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { UserPreferencesService, UserPreferenceValues } from '../../../../../../services/user-preferences.service';
import { MomentDateAdapter } from '../../../../../../utils/momentDateAdapter';
import { MOMENT_DATE_FORMATS } from '../../../../../../utils/moment-date-formats.model';
import { Component, Input } from '@angular/core';
import { DateAdapter, MAT_DATE_FORMATS } from '@angular/material';
import moment from 'moment-es6';
import { DynamicTableModel } from './../../dynamic-table.widget.model';
import { DatetimeAdapter, MAT_DATETIME_FORMATS } from '@mat-datetimepicker/core';
import { MomentDatetimeAdapter, MAT_MOMENT_DATETIME_FORMATS } from '@mat-datetimepicker/moment';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
var ɵ0 = MOMENT_DATE_FORMATS, ɵ1 = MAT_MOMENT_DATETIME_FORMATS;
var DateTimeEditorComponent = /** @class */ (function () {
    function DateTimeEditorComponent(dateAdapter, userPreferencesService) {
        this.dateAdapter = dateAdapter;
        this.userPreferencesService = userPreferencesService;
        this.DATE_TIME_FORMAT = 'DD/MM/YYYY HH:mm';
        this.onDestroy$ = new Subject();
    }
    /**
     * @return {?}
     */
    DateTimeEditorComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.userPreferencesService
            .select(UserPreferenceValues.Locale)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} locale
         * @return {?}
         */
        function (locale) { return _this.dateAdapter.setLocale(locale); }));
        /** @type {?} */
        var momentDateAdapter = (/** @type {?} */ (this.dateAdapter));
        momentDateAdapter.overrideDisplayFormat = this.DATE_TIME_FORMAT;
        this.value = moment(this.table.getCellValue(this.row, this.column), this.DATE_TIME_FORMAT);
    };
    /**
     * @return {?}
     */
    DateTimeEditorComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    };
    /**
     * @param {?} newDateValue
     * @return {?}
     */
    DateTimeEditorComponent.prototype.onDateChanged = /**
     * @param {?} newDateValue
     * @return {?}
     */
    function (newDateValue) {
        if (newDateValue && newDateValue.value) {
            /** @type {?} */
            var newValue = moment(newDateValue.value, this.DATE_TIME_FORMAT);
            this.row.value[this.column.id] = newDateValue.value.format(this.DATE_TIME_FORMAT);
            this.value = newValue;
            this.table.flushValue();
        }
        else if (newDateValue) {
            /** @type {?} */
            var newValue = moment(newDateValue, this.DATE_TIME_FORMAT);
            this.value = newValue;
            this.row.value[this.column.id] = newDateValue;
            this.table.flushValue();
        }
        else {
            this.row.value[this.column.id] = '';
        }
    };
    DateTimeEditorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-datetime-editor',
                    template: "<div>\r\n    <mat-form-field class=\"adf-date-editor\">\r\n        <label [attr.for]=\"column.id\">{{column.name}} {{DATE_TIME_FORMAT}}</label>\r\n        <input matInput\r\n            [matDatetimepicker]=\"datetimePicker\"\r\n            [(ngModel)]=\"value\"\r\n            [id]=\"column.id\"\r\n            [required]=\"column.required\"\r\n            [disabled]=\"!column.editable\"\r\n            (focusout)=\"onDateChanged($event.srcElement.value)\"\r\n            (dateChange)=\"onDateChanged($event)\">\r\n            <mat-datetimepicker-toggle\r\n                matSuffix\r\n                [for]=\"datetimePicker\"\r\n                class=\"adf-date-editor-button\">\r\n            </mat-datetimepicker-toggle>\r\n    </mat-form-field>\r\n    <mat-datetimepicker\r\n        #datetimePicker\r\n        type=\"datetime\"\r\n        openOnFocus=\"true\"\r\n        timeInterval=\"5\">\r\n    </mat-datetimepicker>\r\n</div>\r\n",
                    providers: [
                        { provide: DateAdapter, useClass: MomentDateAdapter },
                        { provide: MAT_DATE_FORMATS, useValue: ɵ0 },
                        { provide: DatetimeAdapter, useClass: MomentDatetimeAdapter },
                        { provide: MAT_DATETIME_FORMATS, useValue: ɵ1 }
                    ],
                    styles: [".adf-date-editor{width:100%}.adf-date-editor-button{position:relative;top:25px}"]
                }] }
    ];
    /** @nocollapse */
    DateTimeEditorComponent.ctorParameters = function () { return [
        { type: DateAdapter },
        { type: UserPreferencesService }
    ]; };
    DateTimeEditorComponent.propDecorators = {
        table: [{ type: Input }],
        row: [{ type: Input }],
        column: [{ type: Input }]
    };
    return DateTimeEditorComponent;
}());
export { DateTimeEditorComponent };
if (false) {
    /** @type {?} */
    DateTimeEditorComponent.prototype.DATE_TIME_FORMAT;
    /** @type {?} */
    DateTimeEditorComponent.prototype.value;
    /** @type {?} */
    DateTimeEditorComponent.prototype.table;
    /** @type {?} */
    DateTimeEditorComponent.prototype.row;
    /** @type {?} */
    DateTimeEditorComponent.prototype.column;
    /** @type {?} */
    DateTimeEditorComponent.prototype.minDate;
    /** @type {?} */
    DateTimeEditorComponent.prototype.maxDate;
    /**
     * @type {?}
     * @private
     */
    DateTimeEditorComponent.prototype.onDestroy$;
    /**
     * @type {?}
     * @private
     */
    DateTimeEditorComponent.prototype.dateAdapter;
    /**
     * @type {?}
     * @private
     */
    DateTimeEditorComponent.prototype.userPreferencesService;
}
export { ɵ0, ɵ1 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGF0ZXRpbWUuZWRpdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvZHluYW1pYy10YWJsZS9lZGl0b3JzL2RhdGV0aW1lL2RhdGV0aW1lLmVkaXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLHNCQUFzQixFQUFFLG9CQUFvQixFQUFFLE1BQU0scURBQXFELENBQUM7QUFDbkgsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFDOUUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sbURBQW1ELENBQUM7QUFDeEYsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQXFCLE1BQU0sZUFBZSxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxXQUFXLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRSxPQUFPLE1BQU0sTUFBTSxZQUFZLENBQUM7QUFJaEMsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFDdkUsT0FBTyxFQUFFLGVBQWUsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ2pGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSwyQkFBMkIsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ2hHLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO1NBT0ksbUJBQW1CLE9BRWYsMkJBQTJCO0FBUDlFO0lBK0JJLGlDQUFvQixXQUFnQyxFQUNoQyxzQkFBOEM7UUFEOUMsZ0JBQVcsR0FBWCxXQUFXLENBQXFCO1FBQ2hDLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUFuQmxFLHFCQUFnQixHQUFXLGtCQUFrQixDQUFDO1FBZ0J0QyxlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztJQUk1QyxDQUFDOzs7O0lBRUQsMENBQVE7OztJQUFSO1FBQUEsaUJBVUM7UUFURyxJQUFJLENBQUMsc0JBQXNCO2FBQ3RCLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUM7YUFDbkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDaEMsU0FBUzs7OztRQUFDLFVBQUEsTUFBTSxJQUFJLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLEVBQWxDLENBQWtDLEVBQUMsQ0FBQzs7WUFFdkQsaUJBQWlCLEdBQUcsbUJBQW9CLElBQUksQ0FBQyxXQUFXLEVBQUE7UUFDOUQsaUJBQWlCLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1FBRWhFLElBQUksQ0FBQyxLQUFLLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQy9GLENBQUM7Ozs7SUFFRCw2Q0FBVzs7O0lBQVg7UUFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQy9CLENBQUM7Ozs7O0lBRUQsK0NBQWE7Ozs7SUFBYixVQUFjLFlBQVk7UUFDdEIsSUFBSSxZQUFZLElBQUksWUFBWSxDQUFDLEtBQUssRUFBRTs7Z0JBQzlCLFFBQVEsR0FBRyxNQUFNLENBQUMsWUFBWSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUM7WUFDbEUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsR0FBRyxZQUFZLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztZQUNsRixJQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQztZQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxDQUFDO1NBQzNCO2FBQU0sSUFBSSxZQUFZLEVBQUU7O2dCQUNmLFFBQVEsR0FBRyxNQUFNLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztZQUM1RCxJQUFJLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQztZQUN0QixJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxHQUFHLFlBQVksQ0FBQztZQUM5QyxJQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxDQUFDO1NBQzNCO2FBQU07WUFDSCxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztTQUN2QztJQUNMLENBQUM7O2dCQWxFSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLHFCQUFxQjtvQkFDL0IsczdCQUFxQztvQkFDckMsU0FBUyxFQUFFO3dCQUNQLEVBQUUsT0FBTyxFQUFFLFdBQVcsRUFBRSxRQUFRLEVBQUUsaUJBQWlCLEVBQUU7d0JBQ3JELEVBQUUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLFFBQVEsSUFBcUIsRUFBRTt3QkFDNUQsRUFBRSxPQUFPLEVBQUUsZUFBZSxFQUFFLFFBQVEsRUFBRSxxQkFBcUIsRUFBRTt3QkFDN0QsRUFBRSxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsUUFBUSxJQUE2QixFQUFFO3FCQUMzRTs7aUJBRUo7Ozs7Z0JBckJRLFdBQVc7Z0JBSlgsc0JBQXNCOzs7d0JBZ0MxQixLQUFLO3NCQUdMLEtBQUs7eUJBR0wsS0FBSzs7SUE2Q1YsOEJBQUM7Q0FBQSxBQXBFRCxJQW9FQztTQXpEWSx1QkFBdUI7OztJQUVoQyxtREFBOEM7O0lBRTlDLHdDQUFXOztJQUVYLHdDQUN5Qjs7SUFFekIsc0NBQ3FCOztJQUVyQix5Q0FDMkI7O0lBRTNCLDBDQUFnQjs7SUFDaEIsMENBQWdCOzs7OztJQUVoQiw2Q0FBNEM7Ozs7O0lBRWhDLDhDQUF3Qzs7Ozs7SUFDeEMseURBQXNEIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UsIFVzZXJQcmVmZXJlbmNlVmFsdWVzIH0gZnJvbSAnLi4vLi4vLi4vLi4vLi4vLi4vc2VydmljZXMvdXNlci1wcmVmZXJlbmNlcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTW9tZW50RGF0ZUFkYXB0ZXIgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi8uLi91dGlscy9tb21lbnREYXRlQWRhcHRlcic7XHJcbmltcG9ydCB7IE1PTUVOVF9EQVRFX0ZPUk1BVFMgfSBmcm9tICcuLi8uLi8uLi8uLi8uLi8uLi91dGlscy9tb21lbnQtZGF0ZS1mb3JtYXRzLm1vZGVsJztcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0LCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRGF0ZUFkYXB0ZXIsIE1BVF9EQVRFX0ZPUk1BVFMgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50LWVzNic7XHJcbmltcG9ydCB7IE1vbWVudCB9IGZyb20gJ21vbWVudCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZUNvbHVtbiB9IGZyb20gJy4vLi4vLi4vZHluYW1pYy10YWJsZS1jb2x1bW4ubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVSb3cgfSBmcm9tICcuLy4uLy4uL2R5bmFtaWMtdGFibGUtcm93Lm1vZGVsJztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlTW9kZWwgfSBmcm9tICcuLy4uLy4uL2R5bmFtaWMtdGFibGUud2lkZ2V0Lm1vZGVsJztcclxuaW1wb3J0IHsgRGF0ZXRpbWVBZGFwdGVyLCBNQVRfREFURVRJTUVfRk9STUFUUyB9IGZyb20gJ0BtYXQtZGF0ZXRpbWVwaWNrZXIvY29yZSc7XHJcbmltcG9ydCB7IE1vbWVudERhdGV0aW1lQWRhcHRlciwgTUFUX01PTUVOVF9EQVRFVElNRV9GT1JNQVRTIH0gZnJvbSAnQG1hdC1kYXRldGltZXBpY2tlci9tb21lbnQnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtZGF0ZXRpbWUtZWRpdG9yJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9kYXRldGltZS5lZGl0b3IuaHRtbCcsXHJcbiAgICBwcm92aWRlcnM6IFtcclxuICAgICAgICB7IHByb3ZpZGU6IERhdGVBZGFwdGVyLCB1c2VDbGFzczogTW9tZW50RGF0ZUFkYXB0ZXIgfSxcclxuICAgICAgICB7IHByb3ZpZGU6IE1BVF9EQVRFX0ZPUk1BVFMsIHVzZVZhbHVlOiBNT01FTlRfREFURV9GT1JNQVRTIH0sXHJcbiAgICAgICAgeyBwcm92aWRlOiBEYXRldGltZUFkYXB0ZXIsIHVzZUNsYXNzOiBNb21lbnREYXRldGltZUFkYXB0ZXIgfSxcclxuICAgICAgICB7IHByb3ZpZGU6IE1BVF9EQVRFVElNRV9GT1JNQVRTLCB1c2VWYWx1ZTogTUFUX01PTUVOVF9EQVRFVElNRV9GT1JNQVRTIH1cclxuICAgIF0sXHJcbiAgICBzdHlsZVVybHM6IFsnLi9kYXRldGltZS5lZGl0b3Iuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEYXRlVGltZUVkaXRvckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuXHJcbiAgICBEQVRFX1RJTUVfRk9STUFUOiBzdHJpbmcgPSAnREQvTU0vWVlZWSBISDptbSc7XHJcblxyXG4gICAgdmFsdWU6IGFueTtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgdGFibGU6IER5bmFtaWNUYWJsZU1vZGVsO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICByb3c6IER5bmFtaWNUYWJsZVJvdztcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgY29sdW1uOiBEeW5hbWljVGFibGVDb2x1bW47XHJcblxyXG4gICAgbWluRGF0ZTogTW9tZW50O1xyXG4gICAgbWF4RGF0ZTogTW9tZW50O1xyXG5cclxuICAgIHByaXZhdGUgb25EZXN0cm95JCA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBkYXRlQWRhcHRlcjogRGF0ZUFkYXB0ZXI8TW9tZW50PixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgdXNlclByZWZlcmVuY2VzU2VydmljZTogVXNlclByZWZlcmVuY2VzU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VzU2VydmljZVxyXG4gICAgICAgICAgICAuc2VsZWN0KFVzZXJQcmVmZXJlbmNlVmFsdWVzLkxvY2FsZSlcclxuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMub25EZXN0cm95JCkpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUobG9jYWxlID0+IHRoaXMuZGF0ZUFkYXB0ZXIuc2V0TG9jYWxlKGxvY2FsZSkpO1xyXG5cclxuICAgICAgICBjb25zdCBtb21lbnREYXRlQWRhcHRlciA9IDxNb21lbnREYXRlQWRhcHRlcj4gdGhpcy5kYXRlQWRhcHRlcjtcclxuICAgICAgICBtb21lbnREYXRlQWRhcHRlci5vdmVycmlkZURpc3BsYXlGb3JtYXQgPSB0aGlzLkRBVEVfVElNRV9GT1JNQVQ7XHJcblxyXG4gICAgICAgIHRoaXMudmFsdWUgPSBtb21lbnQodGhpcy50YWJsZS5nZXRDZWxsVmFsdWUodGhpcy5yb3csIHRoaXMuY29sdW1uKSwgdGhpcy5EQVRFX1RJTUVfRk9STUFUKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQubmV4dCh0cnVlKTtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQuY29tcGxldGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkRhdGVDaGFuZ2VkKG5ld0RhdGVWYWx1ZSkge1xyXG4gICAgICAgIGlmIChuZXdEYXRlVmFsdWUgJiYgbmV3RGF0ZVZhbHVlLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG5ld1ZhbHVlID0gbW9tZW50KG5ld0RhdGVWYWx1ZS52YWx1ZSwgdGhpcy5EQVRFX1RJTUVfRk9STUFUKTtcclxuICAgICAgICAgICAgdGhpcy5yb3cudmFsdWVbdGhpcy5jb2x1bW4uaWRdID0gbmV3RGF0ZVZhbHVlLnZhbHVlLmZvcm1hdCh0aGlzLkRBVEVfVElNRV9GT1JNQVQpO1xyXG4gICAgICAgICAgICB0aGlzLnZhbHVlID0gbmV3VmFsdWU7XHJcbiAgICAgICAgICAgIHRoaXMudGFibGUuZmx1c2hWYWx1ZSgpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAobmV3RGF0ZVZhbHVlKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG5ld1ZhbHVlID0gbW9tZW50KG5ld0RhdGVWYWx1ZSwgdGhpcy5EQVRFX1RJTUVfRk9STUFUKTtcclxuICAgICAgICAgICAgdGhpcy52YWx1ZSA9IG5ld1ZhbHVlO1xyXG4gICAgICAgICAgICB0aGlzLnJvdy52YWx1ZVt0aGlzLmNvbHVtbi5pZF0gPSBuZXdEYXRlVmFsdWU7XHJcbiAgICAgICAgICAgIHRoaXMudGFibGUuZmx1c2hWYWx1ZSgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMucm93LnZhbHVlW3RoaXMuY29sdW1uLmlkXSA9ICcnO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbn1cclxuIl19