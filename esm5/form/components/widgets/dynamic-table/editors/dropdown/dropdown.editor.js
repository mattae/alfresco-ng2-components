/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { LogService } from '../../../../../../services/log.service';
import { Component, Input } from '@angular/core';
import { FormService } from './../../../../../services/form.service';
import { DynamicTableModel } from './../../dynamic-table.widget.model';
var DropdownEditorComponent = /** @class */ (function () {
    function DropdownEditorComponent(formService, logService) {
        this.formService = formService;
        this.logService = logService;
        this.value = null;
        this.options = [];
    }
    /**
     * @return {?}
     */
    DropdownEditorComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var field = this.table.field;
        if (field) {
            if (this.column.optionType === 'rest') {
                if (this.table.form && this.table.form.taskId) {
                    this.getValuesByTaskId(field);
                }
                else {
                    this.getValuesByProcessDefinitionId(field);
                }
            }
            else {
                this.options = this.column.options || [];
                this.value = this.table.getCellValue(this.row, this.column);
            }
        }
    };
    /**
     * @param {?} field
     * @return {?}
     */
    DropdownEditorComponent.prototype.getValuesByTaskId = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        var _this = this;
        this.formService
            .getRestFieldValuesColumn(field.form.taskId, field.id, this.column.id)
            .subscribe((/**
         * @param {?} dynamicTableColumnOption
         * @return {?}
         */
        function (dynamicTableColumnOption) {
            _this.column.options = dynamicTableColumnOption || [];
            _this.options = _this.column.options;
            _this.value = _this.table.getCellValue(_this.row, _this.column);
        }), (/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); }));
    };
    /**
     * @param {?} field
     * @return {?}
     */
    DropdownEditorComponent.prototype.getValuesByProcessDefinitionId = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        var _this = this;
        this.formService
            .getRestFieldValuesColumnByProcessId(field.form.processDefinitionId, field.id, this.column.id)
            .subscribe((/**
         * @param {?} dynamicTableColumnOption
         * @return {?}
         */
        function (dynamicTableColumnOption) {
            _this.column.options = dynamicTableColumnOption || [];
            _this.options = _this.column.options;
            _this.value = _this.table.getCellValue(_this.row, _this.column);
        }), (/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); }));
    };
    /**
     * @param {?} row
     * @param {?} column
     * @param {?} event
     * @return {?}
     */
    DropdownEditorComponent.prototype.onValueChanged = /**
     * @param {?} row
     * @param {?} column
     * @param {?} event
     * @return {?}
     */
    function (row, column, event) {
        /** @type {?} */
        var value = ((/** @type {?} */ (event))).value;
        value = column.options.find((/**
         * @param {?} opt
         * @return {?}
         */
        function (opt) { return opt.name === value; }));
        row.value[column.id] = value;
    };
    /**
     * @param {?} error
     * @return {?}
     */
    DropdownEditorComponent.prototype.handleError = /**
     * @param {?} error
     * @return {?}
     */
    function (error) {
        this.logService.error(error);
    };
    DropdownEditorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-dropdown-editor',
                    template: "<div class=\"dropdown-editor\">\r\n    <label [attr.for]=\"column.id\">{{column.name}}</label>\r\n    <mat-form-field>\r\n        <mat-select\r\n            floatPlaceholder=\"never\"\r\n            class=\"adf-dropdown-editor-select\"\r\n            [id]=\"column.id\"\r\n            [(ngModel)]=\"value\"\r\n            [required]=\"column.required\"\r\n            [disabled]=\"!column.editable\"\r\n            (selectionChange)=\"onValueChanged(row, column, $event)\">\r\n            <mat-option></mat-option>\r\n            <mat-option *ngFor=\"let opt of options\" [value]=\"opt.name\" [id]=\"opt.id\">{{opt.name}}</mat-option>\r\n        </mat-select>\r\n    </mat-form-field>\r\n</div>\r\n",
                    styles: [".adf-dropdown-editor-select{width:100%}"]
                }] }
    ];
    /** @nocollapse */
    DropdownEditorComponent.ctorParameters = function () { return [
        { type: FormService },
        { type: LogService }
    ]; };
    DropdownEditorComponent.propDecorators = {
        table: [{ type: Input }],
        row: [{ type: Input }],
        column: [{ type: Input }]
    };
    return DropdownEditorComponent;
}());
export { DropdownEditorComponent };
if (false) {
    /** @type {?} */
    DropdownEditorComponent.prototype.value;
    /** @type {?} */
    DropdownEditorComponent.prototype.options;
    /** @type {?} */
    DropdownEditorComponent.prototype.table;
    /** @type {?} */
    DropdownEditorComponent.prototype.row;
    /** @type {?} */
    DropdownEditorComponent.prototype.column;
    /** @type {?} */
    DropdownEditorComponent.prototype.formService;
    /**
     * @type {?}
     * @private
     */
    DropdownEditorComponent.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHJvcGRvd24uZWRpdG9yLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvZHluYW1pYy10YWJsZS9lZGl0b3JzL2Ryb3Bkb3duL2Ryb3Bkb3duLmVkaXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHdDQUF3QyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ3pELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUlyRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUV2RTtJQW1CSSxpQ0FBbUIsV0FBd0IsRUFDdkIsVUFBc0I7UUFEdkIsZ0JBQVcsR0FBWCxXQUFXLENBQWE7UUFDdkIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQWIxQyxVQUFLLEdBQVEsSUFBSSxDQUFDO1FBQ2xCLFlBQU8sR0FBK0IsRUFBRSxDQUFDO0lBYXpDLENBQUM7Ozs7SUFFRCwwQ0FBUTs7O0lBQVI7O1lBQ1UsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSztRQUM5QixJQUFJLEtBQUssRUFBRTtZQUNQLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxVQUFVLEtBQUssTUFBTSxFQUFFO2dCQUNuQyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRTtvQkFDM0MsSUFBSSxDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUNqQztxQkFBTTtvQkFDSCxJQUFJLENBQUMsOEJBQThCLENBQUMsS0FBSyxDQUFDLENBQUM7aUJBQzlDO2FBQ0o7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sSUFBSSxFQUFFLENBQUM7Z0JBQ3pDLElBQUksQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDL0Q7U0FDSjtJQUNMLENBQUM7Ozs7O0lBRUQsbURBQWlCOzs7O0lBQWpCLFVBQWtCLEtBQUs7UUFBdkIsaUJBZUM7UUFkRyxJQUFJLENBQUMsV0FBVzthQUNYLHdCQUF3QixDQUNyQixLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFDakIsS0FBSyxDQUFDLEVBQUUsRUFDUixJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FDakI7YUFDQSxTQUFTOzs7O1FBQ04sVUFBQyx3QkFBb0Q7WUFDakQsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLEdBQUcsd0JBQXdCLElBQUksRUFBRSxDQUFDO1lBQ3JELEtBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSSxDQUFDLE1BQU0sQ0FBQyxPQUFPLENBQUM7WUFDbkMsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFJLENBQUMsS0FBSyxDQUFDLFlBQVksQ0FBQyxLQUFJLENBQUMsR0FBRyxFQUFFLEtBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNoRSxDQUFDOzs7O1FBQ0QsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUNqQyxDQUFDO0lBQ1YsQ0FBQzs7Ozs7SUFFRCxnRUFBOEI7Ozs7SUFBOUIsVUFBK0IsS0FBSztRQUFwQyxpQkFlQztRQWRHLElBQUksQ0FBQyxXQUFXO2FBQ1gsbUNBQW1DLENBQ2hDLEtBQUssQ0FBQyxJQUFJLENBQUMsbUJBQW1CLEVBQzlCLEtBQUssQ0FBQyxFQUFFLEVBQ1IsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQ2pCO2FBQ0EsU0FBUzs7OztRQUNOLFVBQUMsd0JBQW9EO1lBQ2pELEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxHQUFHLHdCQUF3QixJQUFJLEVBQUUsQ0FBQztZQUNyRCxLQUFJLENBQUMsT0FBTyxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsT0FBTyxDQUFDO1lBQ25DLEtBQUksQ0FBQyxLQUFLLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxZQUFZLENBQUMsS0FBSSxDQUFDLEdBQUcsRUFBRSxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDaEUsQ0FBQzs7OztRQUNELFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFDakMsQ0FBQztJQUNWLENBQUM7Ozs7Ozs7SUFFRCxnREFBYzs7Ozs7O0lBQWQsVUFBZSxHQUFvQixFQUFFLE1BQTBCLEVBQUUsS0FBVTs7WUFDbkUsS0FBSyxHQUFRLENBQUMsbUJBQW1CLEtBQUssRUFBQSxDQUFDLENBQUMsS0FBSztRQUNqRCxLQUFLLEdBQUcsTUFBTSxDQUFDLE9BQU8sQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxHQUFHLENBQUMsSUFBSSxLQUFLLEtBQUssRUFBbEIsQ0FBa0IsRUFBQyxDQUFDO1FBQ3pELEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxHQUFHLEtBQUssQ0FBQztJQUNqQyxDQUFDOzs7OztJQUVELDZDQUFXOzs7O0lBQVgsVUFBWSxLQUFVO1FBQ2xCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2pDLENBQUM7O2dCQWpGSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLHFCQUFxQjtvQkFDL0Isc3NCQUFxQzs7aUJBRXhDOzs7O2dCQVZRLFdBQVc7Z0JBRlgsVUFBVTs7O3dCQWtCZCxLQUFLO3NCQUdMLEtBQUs7eUJBR0wsS0FBSzs7SUFrRVYsOEJBQUM7Q0FBQSxBQWxGRCxJQWtGQztTQTdFWSx1QkFBdUI7OztJQUVoQyx3Q0FBa0I7O0lBQ2xCLDBDQUF5Qzs7SUFFekMsd0NBQ3lCOztJQUV6QixzQ0FDcUI7O0lBRXJCLHlDQUMyQjs7SUFFZiw4Q0FBK0I7Ozs7O0lBQy9CLDZDQUE4QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4gLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uLy4uLy4uL3NlcnZpY2VzL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi8uLi8uLi8uLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVDb2x1bW5PcHRpb24gfSBmcm9tICcuLy4uLy4uL2R5bmFtaWMtdGFibGUtY29sdW1uLW9wdGlvbi5tb2RlbCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZUNvbHVtbiB9IGZyb20gJy4vLi4vLi4vZHluYW1pYy10YWJsZS1jb2x1bW4ubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVSb3cgfSBmcm9tICcuLy4uLy4uL2R5bmFtaWMtdGFibGUtcm93Lm1vZGVsJztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlTW9kZWwgfSBmcm9tICcuLy4uLy4uL2R5bmFtaWMtdGFibGUud2lkZ2V0Lm1vZGVsJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtZHJvcGRvd24tZWRpdG9yJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9kcm9wZG93bi5lZGl0b3IuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9kcm9wZG93bi5lZGl0b3Iuc2NzcyddXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEcm9wZG93bkVkaXRvckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgdmFsdWU6IGFueSA9IG51bGw7XHJcbiAgICBvcHRpb25zOiBEeW5hbWljVGFibGVDb2x1bW5PcHRpb25bXSA9IFtdO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICB0YWJsZTogRHluYW1pY1RhYmxlTW9kZWw7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHJvdzogRHluYW1pY1RhYmxlUm93O1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBjb2x1bW46IER5bmFtaWNUYWJsZUNvbHVtbjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZm9ybVNlcnZpY2U6IEZvcm1TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgY29uc3QgZmllbGQgPSB0aGlzLnRhYmxlLmZpZWxkO1xyXG4gICAgICAgIGlmIChmaWVsZCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5jb2x1bW4ub3B0aW9uVHlwZSA9PT0gJ3Jlc3QnKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy50YWJsZS5mb3JtICYmIHRoaXMudGFibGUuZm9ybS50YXNrSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmdldFZhbHVlc0J5VGFza0lkKGZpZWxkKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5nZXRWYWx1ZXNCeVByb2Nlc3NEZWZpbml0aW9uSWQoZmllbGQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vcHRpb25zID0gdGhpcy5jb2x1bW4ub3B0aW9ucyB8fCBbXTtcclxuICAgICAgICAgICAgICAgIHRoaXMudmFsdWUgPSB0aGlzLnRhYmxlLmdldENlbGxWYWx1ZSh0aGlzLnJvdywgdGhpcy5jb2x1bW4pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldFZhbHVlc0J5VGFza0lkKGZpZWxkKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtU2VydmljZVxyXG4gICAgICAgICAgICAuZ2V0UmVzdEZpZWxkVmFsdWVzQ29sdW1uKFxyXG4gICAgICAgICAgICAgICAgZmllbGQuZm9ybS50YXNrSWQsXHJcbiAgICAgICAgICAgICAgICBmaWVsZC5pZCxcclxuICAgICAgICAgICAgICAgIHRoaXMuY29sdW1uLmlkXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIChkeW5hbWljVGFibGVDb2x1bW5PcHRpb246IER5bmFtaWNUYWJsZUNvbHVtbk9wdGlvbltdKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5jb2x1bW4ub3B0aW9ucyA9IGR5bmFtaWNUYWJsZUNvbHVtbk9wdGlvbiB8fCBbXTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMgPSB0aGlzLmNvbHVtbi5vcHRpb25zO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMudmFsdWUgPSB0aGlzLnRhYmxlLmdldENlbGxWYWx1ZSh0aGlzLnJvdywgdGhpcy5jb2x1bW4pO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFZhbHVlc0J5UHJvY2Vzc0RlZmluaXRpb25JZChmaWVsZCkge1xyXG4gICAgICAgIHRoaXMuZm9ybVNlcnZpY2VcclxuICAgICAgICAgICAgLmdldFJlc3RGaWVsZFZhbHVlc0NvbHVtbkJ5UHJvY2Vzc0lkKFxyXG4gICAgICAgICAgICAgICAgZmllbGQuZm9ybS5wcm9jZXNzRGVmaW5pdGlvbklkLFxyXG4gICAgICAgICAgICAgICAgZmllbGQuaWQsXHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbHVtbi5pZFxyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAoZHluYW1pY1RhYmxlQ29sdW1uT3B0aW9uOiBEeW5hbWljVGFibGVDb2x1bW5PcHRpb25bXSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY29sdW1uLm9wdGlvbnMgPSBkeW5hbWljVGFibGVDb2x1bW5PcHRpb24gfHwgW107XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5vcHRpb25zID0gdGhpcy5jb2x1bW4ub3B0aW9ucztcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnZhbHVlID0gdGhpcy50YWJsZS5nZXRDZWxsVmFsdWUodGhpcy5yb3csIHRoaXMuY29sdW1uKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycilcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBvblZhbHVlQ2hhbmdlZChyb3c6IER5bmFtaWNUYWJsZVJvdywgY29sdW1uOiBEeW5hbWljVGFibGVDb2x1bW4sIGV2ZW50OiBhbnkpIHtcclxuICAgICAgICBsZXQgdmFsdWU6IGFueSA9ICg8SFRNTElucHV0RWxlbWVudD4gZXZlbnQpLnZhbHVlO1xyXG4gICAgICAgIHZhbHVlID0gY29sdW1uLm9wdGlvbnMuZmluZCgob3B0KSA9PiBvcHQubmFtZSA9PT0gdmFsdWUpO1xyXG4gICAgICAgIHJvdy52YWx1ZVtjb2x1bW4uaWRdID0gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgaGFuZGxlRXJyb3IoZXJyb3I6IGFueSkge1xyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJvcik7XHJcbiAgICB9XHJcbn1cclxuIl19