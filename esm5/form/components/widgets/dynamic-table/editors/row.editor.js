/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, EventEmitter, Input, Output } from '@angular/core';
import { DynamicRowValidationSummary } from './../dynamic-row-validation-summary.model';
import { DynamicTableModel } from './../dynamic-table.widget.model';
var RowEditorComponent = /** @class */ (function () {
    function RowEditorComponent() {
        this.save = new EventEmitter();
        this.cancel = new EventEmitter();
        this.validationSummary = new DynamicRowValidationSummary({ isValid: true, message: '' });
    }
    /**
     * @return {?}
     */
    RowEditorComponent.prototype.onCancelChanges = /**
     * @return {?}
     */
    function () {
        this.cancel.emit({
            table: this.table,
            row: this.row,
            column: this.column
        });
    };
    /**
     * @return {?}
     */
    RowEditorComponent.prototype.onSaveChanges = /**
     * @return {?}
     */
    function () {
        this.validate();
        if (this.isValid()) {
            this.save.emit({
                table: this.table,
                row: this.row,
                column: this.column
            });
        }
    };
    /**
     * @private
     * @return {?}
     */
    RowEditorComponent.prototype.isValid = /**
     * @private
     * @return {?}
     */
    function () {
        return this.validationSummary && this.validationSummary.isValid;
    };
    /**
     * @private
     * @return {?}
     */
    RowEditorComponent.prototype.validate = /**
     * @private
     * @return {?}
     */
    function () {
        this.validationSummary = this.table.validateRow(this.row);
    };
    RowEditorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'row-editor',
                    template: "<div class=\"row-editor mdl-shadow--2dp\"\r\n    [class.row-editor__invalid]=\"!validationSummary.isValid\">\r\n    <div class=\"mdl-grid\" *ngFor=\"let column of table.columns\">\r\n        <div class=\"mdl-cell mdl-cell--6-col\" [ngSwitch]=\"column.type\">\r\n            <div *ngSwitchCase=\"'Dropdown'\">\r\n                <adf-dropdown-editor\r\n                    [table]=\"table\"\r\n                    [row]=\"row\"\r\n                    [column]=\"column\">\r\n                </adf-dropdown-editor>\r\n                </div>\r\n                <div *ngSwitchCase=\"'Date'\">\r\n                    <adf-date-editor\r\n                    [table]=\"table\"\r\n                    [row]=\"row\"\r\n                    [column]=\"column\">\r\n                </adf-date-editor>\r\n                </div>\r\n                <div *ngSwitchCase=\"'Datetime'\">\r\n                    <adf-datetime-editor\r\n                        [table]=\"table\"\r\n                        [row]=\"row\"\r\n                        [column]=\"column\">\r\n                    </adf-datetime-editor>\r\n                </div>\r\n                <div *ngSwitchCase=\"'Boolean'\">\r\n                <adf-boolean-editor\r\n                    [table]=\"table\"\r\n                    [row]=\"row\"\r\n                    [column]=\"column\">\r\n                </adf-boolean-editor>\r\n                </div>\r\n                <div *ngSwitchDefault>\r\n                <adf-text-editor\r\n                    [table]=\"table\"\r\n                    [row]=\"row\"\r\n                    [column]=\"column\">\r\n                </adf-text-editor>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <error-widget [error]=\"validationSummary\"></error-widget>\r\n    <div>\r\n        <button mat-button (click)=\"onCancelChanges()\">Cancel</button>\r\n        <button mat-button (click)=\"onSaveChanges()\">Save</button>\r\n    </div>\r\n</div>\r\n",
                    styles: [".row-editor{padding:8px}.row-editor__validation-summary{visibility:hidden}.row-editor__invalid .row-editor__validation-summary{color:#d50000;visibility:visible;padding:8px 16px}"]
                }] }
    ];
    /** @nocollapse */
    RowEditorComponent.ctorParameters = function () { return []; };
    RowEditorComponent.propDecorators = {
        table: [{ type: Input }],
        row: [{ type: Input }],
        column: [{ type: Input }],
        save: [{ type: Output }],
        cancel: [{ type: Output }]
    };
    return RowEditorComponent;
}());
export { RowEditorComponent };
if (false) {
    /** @type {?} */
    RowEditorComponent.prototype.table;
    /** @type {?} */
    RowEditorComponent.prototype.row;
    /** @type {?} */
    RowEditorComponent.prototype.column;
    /** @type {?} */
    RowEditorComponent.prototype.save;
    /** @type {?} */
    RowEditorComponent.prototype.cancel;
    /** @type {?} */
    RowEditorComponent.prototype.validationSummary;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicm93LmVkaXRvci5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2R5bmFtaWMtdGFibGUvZWRpdG9ycy9yb3cuZWRpdG9yLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1CQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLDJDQUEyQyxDQUFDO0FBR3hGLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGlDQUFpQyxDQUFDO0FBRXBFO0lBd0JJO1FBUEEsU0FBSSxHQUFzQixJQUFJLFlBQVksRUFBTyxDQUFDO1FBR2xELFdBQU0sR0FBc0IsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUtoRCxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSwyQkFBMkIsQ0FBQyxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFFLEVBQUUsRUFBRSxDQUFDLENBQUM7SUFDN0YsQ0FBQzs7OztJQUVELDRDQUFlOzs7SUFBZjtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDO1lBQ2IsS0FBSyxFQUFFLElBQUksQ0FBQyxLQUFLO1lBQ2pCLEdBQUcsRUFBRSxJQUFJLENBQUMsR0FBRztZQUNiLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTtTQUN0QixDQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQsMENBQWE7OztJQUFiO1FBQ0ksSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO1FBQ2hCLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRSxFQUFFO1lBQ2hCLElBQUksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO2dCQUNYLEtBQUssRUFBRSxJQUFJLENBQUMsS0FBSztnQkFDakIsR0FBRyxFQUFFLElBQUksQ0FBQyxHQUFHO2dCQUNiLE1BQU0sRUFBRSxJQUFJLENBQUMsTUFBTTthQUN0QixDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7Ozs7O0lBRU8sb0NBQU87Ozs7SUFBZjtRQUNJLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixJQUFJLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLENBQUM7SUFDcEUsQ0FBQzs7Ozs7SUFFTyxxQ0FBUTs7OztJQUFoQjtRQUNJLElBQUksQ0FBQyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUM7SUFDOUQsQ0FBQzs7Z0JBckRKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsWUFBWTtvQkFDdEIsdTZEQUFnQzs7aUJBRW5DOzs7Ozt3QkFHSSxLQUFLO3NCQUdMLEtBQUs7eUJBR0wsS0FBSzt1QkFHTCxNQUFNO3lCQUdOLE1BQU07O0lBb0NYLHlCQUFDO0NBQUEsQUF2REQsSUF1REM7U0FsRFksa0JBQWtCOzs7SUFFM0IsbUNBQ3lCOztJQUV6QixpQ0FDcUI7O0lBRXJCLG9DQUMyQjs7SUFFM0Isa0NBQ2tEOztJQUVsRCxvQ0FDb0Q7O0lBRXBELCtDQUErQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4gLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRHluYW1pY1Jvd1ZhbGlkYXRpb25TdW1tYXJ5IH0gZnJvbSAnLi8uLi9keW5hbWljLXJvdy12YWxpZGF0aW9uLXN1bW1hcnkubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVDb2x1bW4gfSBmcm9tICcuLy4uL2R5bmFtaWMtdGFibGUtY29sdW1uLm1vZGVsJztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlUm93IH0gZnJvbSAnLi8uLi9keW5hbWljLXRhYmxlLXJvdy5tb2RlbCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZU1vZGVsIH0gZnJvbSAnLi8uLi9keW5hbWljLXRhYmxlLndpZGdldC5tb2RlbCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAncm93LWVkaXRvcicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vcm93LmVkaXRvci5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL3Jvdy5lZGl0b3IuY3NzJ11cclxufSlcclxuZXhwb3J0IGNsYXNzIFJvd0VkaXRvckNvbXBvbmVudCB7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHRhYmxlOiBEeW5hbWljVGFibGVNb2RlbDtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgcm93OiBEeW5hbWljVGFibGVSb3c7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGNvbHVtbjogRHluYW1pY1RhYmxlQ29sdW1uO1xyXG5cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgc2F2ZTogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICBAT3V0cHV0KClcclxuICAgIGNhbmNlbDogRXZlbnRFbWl0dGVyPGFueT4gPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICB2YWxpZGF0aW9uU3VtbWFyeTogRHluYW1pY1Jvd1ZhbGlkYXRpb25TdW1tYXJ5O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMudmFsaWRhdGlvblN1bW1hcnkgPSBuZXcgRHluYW1pY1Jvd1ZhbGlkYXRpb25TdW1tYXJ5KHsgaXNWYWxpZDogdHJ1ZSwgbWVzc2FnZTogJycgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25DYW5jZWxDaGFuZ2VzKCkge1xyXG4gICAgICAgIHRoaXMuY2FuY2VsLmVtaXQoe1xyXG4gICAgICAgICAgICB0YWJsZTogdGhpcy50YWJsZSxcclxuICAgICAgICAgICAgcm93OiB0aGlzLnJvdyxcclxuICAgICAgICAgICAgY29sdW1uOiB0aGlzLmNvbHVtblxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIG9uU2F2ZUNoYW5nZXMoKSB7XHJcbiAgICAgICAgdGhpcy52YWxpZGF0ZSgpO1xyXG4gICAgICAgIGlmICh0aGlzLmlzVmFsaWQoKSkge1xyXG4gICAgICAgICAgICB0aGlzLnNhdmUuZW1pdCh7XHJcbiAgICAgICAgICAgICAgICB0YWJsZTogdGhpcy50YWJsZSxcclxuICAgICAgICAgICAgICAgIHJvdzogdGhpcy5yb3csXHJcbiAgICAgICAgICAgICAgICBjb2x1bW46IHRoaXMuY29sdW1uXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGlzVmFsaWQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudmFsaWRhdGlvblN1bW1hcnkgJiYgdGhpcy52YWxpZGF0aW9uU3VtbWFyeS5pc1ZhbGlkO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgdmFsaWRhdGUoKSB7XHJcbiAgICAgICAgdGhpcy52YWxpZGF0aW9uU3VtbWFyeSA9IHRoaXMudGFibGUudmFsaWRhdGVSb3codGhpcy5yb3cpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=