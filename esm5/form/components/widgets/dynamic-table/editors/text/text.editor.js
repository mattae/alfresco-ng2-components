/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, Input } from '@angular/core';
import { DynamicTableModel } from './../../dynamic-table.widget.model';
var TextEditorComponent = /** @class */ (function () {
    function TextEditorComponent() {
    }
    /**
     * @return {?}
     */
    TextEditorComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.displayName = this.table.getDisplayText(this.column);
    };
    /**
     * @param {?} row
     * @param {?} column
     * @param {?} event
     * @return {?}
     */
    TextEditorComponent.prototype.onValueChanged = /**
     * @param {?} row
     * @param {?} column
     * @param {?} event
     * @return {?}
     */
    function (row, column, event) {
        /** @type {?} */
        var value = ((/** @type {?} */ (event.target))).value;
        row.value[column.id] = value;
    };
    TextEditorComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-text-editor',
                    template: "<div class=\"adf-text-editor\">\r\n    <mat-form-field>\r\n    <label [attr.for]=\"column.id\">{{displayName}}</label>\r\n    <input matInput\r\n        type=\"text\"\r\n        [value]=\"table.getCellValue(row, column)\"\r\n        (keyup)=\"onValueChanged(row, column, $event)\"\r\n        [required]=\"column.required\"\r\n        [disabled]=\"!column.editable\"\r\n        [id]=\"column.id\">\r\n    </mat-form-field>\r\n</div>\r\n",
                    styles: [".adf-text-editor{width:100%}"]
                }] }
    ];
    TextEditorComponent.propDecorators = {
        table: [{ type: Input }],
        row: [{ type: Input }],
        column: [{ type: Input }]
    };
    return TextEditorComponent;
}());
export { TextEditorComponent };
if (false) {
    /** @type {?} */
    TextEditorComponent.prototype.table;
    /** @type {?} */
    TextEditorComponent.prototype.row;
    /** @type {?} */
    TextEditorComponent.prototype.column;
    /** @type {?} */
    TextEditorComponent.prototype.displayName;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC5lZGl0b3IuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9keW5hbWljLXRhYmxlL2VkaXRvcnMvdGV4dC90ZXh0LmVkaXRvci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFHekQsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sb0NBQW9DLENBQUM7QUFFdkU7SUFBQTtJQTJCQSxDQUFDOzs7O0lBVEcsc0NBQVE7OztJQUFSO1FBQ0ksSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDOUQsQ0FBQzs7Ozs7OztJQUVELDRDQUFjOzs7Ozs7SUFBZCxVQUFlLEdBQW9CLEVBQUUsTUFBMEIsRUFBRSxLQUFVOztZQUNqRSxLQUFLLEdBQVEsQ0FBQyxtQkFBbUIsS0FBSyxDQUFDLE1BQU0sRUFBQSxDQUFDLENBQUMsS0FBSztRQUMxRCxHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxFQUFFLENBQUMsR0FBRyxLQUFLLENBQUM7SUFDakMsQ0FBQzs7Z0JBekJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsaUJBQWlCO29CQUMzQiwrYkFBaUM7O2lCQUVwQzs7O3dCQUdJLEtBQUs7c0JBR0wsS0FBSzt5QkFHTCxLQUFLOztJQWNWLDBCQUFDO0NBQUEsQUEzQkQsSUEyQkM7U0F0QlksbUJBQW1COzs7SUFFNUIsb0NBQ3lCOztJQUV6QixrQ0FDcUI7O0lBRXJCLHFDQUMyQjs7SUFFM0IsMENBQW9CIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVDb2x1bW4gfSBmcm9tICcuLy4uLy4uL2R5bmFtaWMtdGFibGUtY29sdW1uLm1vZGVsJztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlUm93IH0gZnJvbSAnLi8uLi8uLi9keW5hbWljLXRhYmxlLXJvdy5tb2RlbCc7XHJcbmltcG9ydCB7IER5bmFtaWNUYWJsZU1vZGVsIH0gZnJvbSAnLi8uLi8uLi9keW5hbWljLXRhYmxlLndpZGdldC5tb2RlbCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLXRleHQtZWRpdG9yJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi90ZXh0LmVkaXRvci5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL3RleHQuZWRpdG9yLnNjc3MnXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgVGV4dEVkaXRvckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHRhYmxlOiBEeW5hbWljVGFibGVNb2RlbDtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgcm93OiBEeW5hbWljVGFibGVSb3c7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGNvbHVtbjogRHluYW1pY1RhYmxlQ29sdW1uO1xyXG5cclxuICAgIGRpc3BsYXlOYW1lOiBzdHJpbmc7XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5kaXNwbGF5TmFtZSA9IHRoaXMudGFibGUuZ2V0RGlzcGxheVRleHQodGhpcy5jb2x1bW4pO1xyXG4gICAgfVxyXG5cclxuICAgIG9uVmFsdWVDaGFuZ2VkKHJvdzogRHluYW1pY1RhYmxlUm93LCBjb2x1bW46IER5bmFtaWNUYWJsZUNvbHVtbiwgZXZlbnQ6IGFueSkge1xyXG4gICAgICAgIGNvbnN0IHZhbHVlOiBhbnkgPSAoPEhUTUxJbnB1dEVsZW1lbnQ+IGV2ZW50LnRhcmdldCkudmFsdWU7XHJcbiAgICAgICAgcm93LnZhbHVlW2NvbHVtbi5pZF0gPSB2YWx1ZTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19