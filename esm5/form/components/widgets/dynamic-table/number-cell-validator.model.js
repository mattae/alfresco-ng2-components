/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
var NumberCellValidator = /** @class */ (function () {
    function NumberCellValidator() {
        this.supportedTypes = [
            'Number',
            'Amount'
        ];
    }
    /**
     * @param {?} column
     * @return {?}
     */
    NumberCellValidator.prototype.isSupported = /**
     * @param {?} column
     * @return {?}
     */
    function (column) {
        return column && column.required && this.supportedTypes.indexOf(column.type) > -1;
    };
    /**
     * @param {?} value
     * @return {?}
     */
    NumberCellValidator.prototype.isNumber = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        if (value === null || value === undefined || value === '') {
            return false;
        }
        return !isNaN(+value);
    };
    /**
     * @param {?} row
     * @param {?} column
     * @param {?=} summary
     * @return {?}
     */
    NumberCellValidator.prototype.validate = /**
     * @param {?} row
     * @param {?} column
     * @param {?=} summary
     * @return {?}
     */
    function (row, column, summary) {
        if (this.isSupported(column)) {
            /** @type {?} */
            var value = row.value[column.id];
            if (value === null ||
                value === undefined ||
                value === '' ||
                this.isNumber(value)) {
                return true;
            }
            if (summary) {
                summary.isValid = false;
                summary.message = "Field '" + column.name + "' must be a number.";
            }
            return false;
        }
        return true;
    };
    return NumberCellValidator;
}());
export { NumberCellValidator };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NumberCellValidator.prototype.supportedTypes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibnVtYmVyLWNlbGwtdmFsaWRhdG9yLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvZHluYW1pYy10YWJsZS9udW1iZXItY2VsbC12YWxpZGF0b3IubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBd0JBO0lBQUE7UUFFWSxtQkFBYyxHQUFhO1lBQy9CLFFBQVE7WUFDUixRQUFRO1NBQ1gsQ0FBQztJQWlDTixDQUFDOzs7OztJQS9CRyx5Q0FBVzs7OztJQUFYLFVBQVksTUFBMEI7UUFDbEMsT0FBTyxNQUFNLElBQUksTUFBTSxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7SUFDdEYsQ0FBQzs7Ozs7SUFFRCxzQ0FBUTs7OztJQUFSLFVBQVMsS0FBVTtRQUNmLElBQUksS0FBSyxLQUFLLElBQUksSUFBSSxLQUFLLEtBQUssU0FBUyxJQUFJLEtBQUssS0FBSyxFQUFFLEVBQUU7WUFDdkQsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxPQUFPLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDMUIsQ0FBQzs7Ozs7OztJQUVELHNDQUFROzs7Ozs7SUFBUixVQUFTLEdBQW9CLEVBQUUsTUFBMEIsRUFBRSxPQUFxQztRQUU1RixJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsTUFBTSxDQUFDLEVBQUU7O2dCQUNwQixLQUFLLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsRUFBRSxDQUFDO1lBQ2xDLElBQUksS0FBSyxLQUFLLElBQUk7Z0JBQ2QsS0FBSyxLQUFLLFNBQVM7Z0JBQ25CLEtBQUssS0FBSyxFQUFFO2dCQUNaLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUU7Z0JBQ3RCLE9BQU8sSUFBSSxDQUFDO2FBQ2Y7WUFFRCxJQUFJLE9BQU8sRUFBRTtnQkFDVCxPQUFPLENBQUMsT0FBTyxHQUFHLEtBQUssQ0FBQztnQkFDeEIsT0FBTyxDQUFDLE9BQU8sR0FBRyxZQUFVLE1BQU0sQ0FBQyxJQUFJLHdCQUFxQixDQUFDO2FBQ2hFO1lBQ0QsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBQ0wsMEJBQUM7QUFBRCxDQUFDLEFBdENELElBc0NDOzs7Ozs7O0lBcENHLDZDQUdFIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IENlbGxWYWxpZGF0b3IgfSBmcm9tICcuL2NlbGwtdmFsaWRhdG9yLm1vZGVsJztcclxuaW1wb3J0IHsgRHluYW1pY1Jvd1ZhbGlkYXRpb25TdW1tYXJ5IH0gZnJvbSAnLi9keW5hbWljLXJvdy12YWxpZGF0aW9uLXN1bW1hcnkubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVDb2x1bW4gfSBmcm9tICcuL2R5bmFtaWMtdGFibGUtY29sdW1uLm1vZGVsJztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlUm93IH0gZnJvbSAnLi9keW5hbWljLXRhYmxlLXJvdy5tb2RlbCc7XHJcblxyXG5leHBvcnQgY2xhc3MgTnVtYmVyQ2VsbFZhbGlkYXRvciBpbXBsZW1lbnRzIENlbGxWYWxpZGF0b3Ige1xyXG5cclxuICAgIHByaXZhdGUgc3VwcG9ydGVkVHlwZXM6IHN0cmluZ1tdID0gW1xyXG4gICAgICAgICdOdW1iZXInLFxyXG4gICAgICAgICdBbW91bnQnXHJcbiAgICBdO1xyXG5cclxuICAgIGlzU3VwcG9ydGVkKGNvbHVtbjogRHluYW1pY1RhYmxlQ29sdW1uKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGNvbHVtbiAmJiBjb2x1bW4ucmVxdWlyZWQgJiYgdGhpcy5zdXBwb3J0ZWRUeXBlcy5pbmRleE9mKGNvbHVtbi50eXBlKSA+IC0xO1xyXG4gICAgfVxyXG5cclxuICAgIGlzTnVtYmVyKHZhbHVlOiBhbnkpOiBib29sZWFuIHtcclxuICAgICAgICBpZiAodmFsdWUgPT09IG51bGwgfHwgdmFsdWUgPT09IHVuZGVmaW5lZCB8fCB2YWx1ZSA9PT0gJycpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuICFpc05hTigrdmFsdWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHZhbGlkYXRlKHJvdzogRHluYW1pY1RhYmxlUm93LCBjb2x1bW46IER5bmFtaWNUYWJsZUNvbHVtbiwgc3VtbWFyeT86IER5bmFtaWNSb3dWYWxpZGF0aW9uU3VtbWFyeSk6IGJvb2xlYW4ge1xyXG5cclxuICAgICAgICBpZiAodGhpcy5pc1N1cHBvcnRlZChjb2x1bW4pKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHZhbHVlID0gcm93LnZhbHVlW2NvbHVtbi5pZF07XHJcbiAgICAgICAgICAgIGlmICh2YWx1ZSA9PT0gbnVsbCB8fFxyXG4gICAgICAgICAgICAgICAgdmFsdWUgPT09IHVuZGVmaW5lZCB8fFxyXG4gICAgICAgICAgICAgICAgdmFsdWUgPT09ICcnIHx8XHJcbiAgICAgICAgICAgICAgICB0aGlzLmlzTnVtYmVyKHZhbHVlKSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChzdW1tYXJ5KSB7XHJcbiAgICAgICAgICAgICAgICBzdW1tYXJ5LmlzVmFsaWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgIHN1bW1hcnkubWVzc2FnZSA9IGBGaWVsZCAnJHtjb2x1bW4ubmFtZX0nIG11c3QgYmUgYSBudW1iZXIuYDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==