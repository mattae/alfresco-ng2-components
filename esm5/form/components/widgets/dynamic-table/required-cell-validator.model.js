/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
var RequiredCellValidator = /** @class */ (function () {
    function RequiredCellValidator() {
        this.supportedTypes = [
            'String',
            'Number',
            'Amount',
            'Date',
            'Dropdown'
        ];
    }
    /**
     * @param {?} column
     * @return {?}
     */
    RequiredCellValidator.prototype.isSupported = /**
     * @param {?} column
     * @return {?}
     */
    function (column) {
        return column && column.required && this.supportedTypes.indexOf(column.type) > -1;
    };
    /**
     * @param {?} row
     * @param {?} column
     * @param {?=} summary
     * @return {?}
     */
    RequiredCellValidator.prototype.validate = /**
     * @param {?} row
     * @param {?} column
     * @param {?=} summary
     * @return {?}
     */
    function (row, column, summary) {
        if (this.isSupported(column)) {
            /** @type {?} */
            var value = row.value[column.id];
            if (column.required) {
                if (value === null || value === undefined || value === '') {
                    if (summary) {
                        summary.isValid = false;
                        summary.message = "Field '" + column.name + "' is required.";
                    }
                    return false;
                }
            }
        }
        return true;
    };
    return RequiredCellValidator;
}());
export { RequiredCellValidator };
if (false) {
    /**
     * @type {?}
     * @private
     */
    RequiredCellValidator.prototype.supportedTypes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVxdWlyZWQtY2VsbC12YWxpZGF0b3IubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9keW5hbWljLXRhYmxlL3JlcXVpcmVkLWNlbGwtdmFsaWRhdG9yLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQXdCQTtJQUFBO1FBRVksbUJBQWMsR0FBYTtZQUMvQixRQUFRO1lBQ1IsUUFBUTtZQUNSLFFBQVE7WUFDUixNQUFNO1lBQ04sVUFBVTtTQUNiLENBQUM7SUFzQk4sQ0FBQzs7Ozs7SUFwQkcsMkNBQVc7Ozs7SUFBWCxVQUFZLE1BQTBCO1FBQ2xDLE9BQU8sTUFBTSxJQUFJLE1BQU0sQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO0lBQ3RGLENBQUM7Ozs7Ozs7SUFFRCx3Q0FBUTs7Ozs7O0lBQVIsVUFBUyxHQUFvQixFQUFFLE1BQTBCLEVBQUUsT0FBcUM7UUFDNUYsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxFQUFFOztnQkFDcEIsS0FBSyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQztZQUNsQyxJQUFJLE1BQU0sQ0FBQyxRQUFRLEVBQUU7Z0JBQ2pCLElBQUksS0FBSyxLQUFLLElBQUksSUFBSSxLQUFLLEtBQUssU0FBUyxJQUFJLEtBQUssS0FBSyxFQUFFLEVBQUU7b0JBQ3ZELElBQUksT0FBTyxFQUFFO3dCQUNULE9BQU8sQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDO3dCQUN4QixPQUFPLENBQUMsT0FBTyxHQUFHLFlBQVUsTUFBTSxDQUFDLElBQUksbUJBQWdCLENBQUM7cUJBQzNEO29CQUNELE9BQU8sS0FBSyxDQUFDO2lCQUNoQjthQUNKO1NBQ0o7UUFFRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBQ0wsNEJBQUM7QUFBRCxDQUFDLEFBOUJELElBOEJDOzs7Ozs7O0lBNUJHLCtDQU1FIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IENlbGxWYWxpZGF0b3IgfSBmcm9tICcuL2NlbGwtdmFsaWRhdG9yLm1vZGVsJztcclxuaW1wb3J0IHsgRHluYW1pY1Jvd1ZhbGlkYXRpb25TdW1tYXJ5IH0gZnJvbSAnLi9keW5hbWljLXJvdy12YWxpZGF0aW9uLXN1bW1hcnkubW9kZWwnO1xyXG5pbXBvcnQgeyBEeW5hbWljVGFibGVDb2x1bW4gfSBmcm9tICcuL2R5bmFtaWMtdGFibGUtY29sdW1uLm1vZGVsJztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlUm93IH0gZnJvbSAnLi9keW5hbWljLXRhYmxlLXJvdy5tb2RlbCc7XHJcblxyXG5leHBvcnQgY2xhc3MgUmVxdWlyZWRDZWxsVmFsaWRhdG9yIGltcGxlbWVudHMgQ2VsbFZhbGlkYXRvciB7XHJcblxyXG4gICAgcHJpdmF0ZSBzdXBwb3J0ZWRUeXBlczogc3RyaW5nW10gPSBbXHJcbiAgICAgICAgJ1N0cmluZycsXHJcbiAgICAgICAgJ051bWJlcicsXHJcbiAgICAgICAgJ0Ftb3VudCcsXHJcbiAgICAgICAgJ0RhdGUnLFxyXG4gICAgICAgICdEcm9wZG93bidcclxuICAgIF07XHJcblxyXG4gICAgaXNTdXBwb3J0ZWQoY29sdW1uOiBEeW5hbWljVGFibGVDb2x1bW4pOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gY29sdW1uICYmIGNvbHVtbi5yZXF1aXJlZCAmJiB0aGlzLnN1cHBvcnRlZFR5cGVzLmluZGV4T2YoY29sdW1uLnR5cGUpID4gLTE7XHJcbiAgICB9XHJcblxyXG4gICAgdmFsaWRhdGUocm93OiBEeW5hbWljVGFibGVSb3csIGNvbHVtbjogRHluYW1pY1RhYmxlQ29sdW1uLCBzdW1tYXJ5PzogRHluYW1pY1Jvd1ZhbGlkYXRpb25TdW1tYXJ5KTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNTdXBwb3J0ZWQoY29sdW1uKSkge1xyXG4gICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHJvdy52YWx1ZVtjb2x1bW4uaWRdO1xyXG4gICAgICAgICAgICBpZiAoY29sdW1uLnJlcXVpcmVkKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAodmFsdWUgPT09IG51bGwgfHwgdmFsdWUgPT09IHVuZGVmaW5lZCB8fCB2YWx1ZSA9PT0gJycpIHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoc3VtbWFyeSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBzdW1tYXJ5LmlzVmFsaWQgPSBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgc3VtbWFyeS5tZXNzYWdlID0gYEZpZWxkICcke2NvbHVtbi5uYW1lfScgaXMgcmVxdWlyZWQuYDtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxufVxyXG4iXX0=