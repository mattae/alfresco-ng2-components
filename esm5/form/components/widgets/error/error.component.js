/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { ErrorMessageModel } from '../core/index';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
var ErrorWidgetComponent = /** @class */ (function (_super) {
    tslib_1.__extends(ErrorWidgetComponent, _super);
    function ErrorWidgetComponent(formService) {
        var _this = _super.call(this, formService) || this;
        _this.formService = formService;
        _this.translateParameters = null;
        _this._subscriptAnimationState = '';
        return _this;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    ErrorWidgetComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes['required']) {
            this.required = changes.required.currentValue;
            this._subscriptAnimationState = 'enter';
        }
        if (changes['error'] && changes['error'].currentValue) {
            if (changes.error.currentValue.isActive()) {
                this.error = changes.error.currentValue;
                this.translateParameters = this.error.getAttributesAsJsonObj();
                this._subscriptAnimationState = 'enter';
            }
        }
    };
    ErrorWidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'error-widget',
                    template: "<div class=\"adf-error-text-container\">\r\n    <div *ngIf=\"error?.isActive()\" [@transitionMessages]=\"_subscriptAnimationState\">\r\n        <div class=\"adf-error-text\">{{error.message | translate:translateParameters}}</div>\r\n        <mat-icon class=\"adf-error-icon\">warning</mat-icon>\r\n    </div>\r\n    <div *ngIf=\"required\" [@transitionMessages]=\"_subscriptAnimationState\">\r\n            <div class=\"adf-error-text\">{{required}}</div>\r\n    </div>\r\n</div>\r\n",
                    animations: [
                        trigger('transitionMessages', [
                            state('enter', style({ opacity: 1, transform: 'translateY(0%)' })),
                            transition('void => enter', [
                                style({ opacity: 0, transform: 'translateY(-100%)' }),
                                animate('300ms cubic-bezier(0.55, 0, 0.55, 0.2)')
                            ])
                        ])
                    ],
                    host: baseHost,
                    encapsulation: ViewEncapsulation.None,
                    styles: [".adf-error-text{width:85%}"]
                }] }
    ];
    /** @nocollapse */
    ErrorWidgetComponent.ctorParameters = function () { return [
        { type: FormService }
    ]; };
    ErrorWidgetComponent.propDecorators = {
        error: [{ type: Input }],
        required: [{ type: Input }]
    };
    return ErrorWidgetComponent;
}(WidgetComponent));
export { ErrorWidgetComponent };
if (false) {
    /** @type {?} */
    ErrorWidgetComponent.prototype.error;
    /** @type {?} */
    ErrorWidgetComponent.prototype.required;
    /** @type {?} */
    ErrorWidgetComponent.prototype.translateParameters;
    /** @type {?} */
    ErrorWidgetComponent.prototype._subscriptAnimationState;
    /** @type {?} */
    ErrorWidgetComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXJyb3IuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvZXJyb3IvZXJyb3IuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLE9BQU8sRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUNqRixPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBNEIsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDOUYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2xELE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUMvRCxPQUFPLEVBQUUsUUFBUSxFQUFHLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBRW5FO0lBZ0IwQyxnREFBZTtJQVlyRCw4QkFBbUIsV0FBd0I7UUFBM0MsWUFDSSxrQkFBTSxXQUFXLENBQUMsU0FDckI7UUFGa0IsaUJBQVcsR0FBWCxXQUFXLENBQWE7UUFKM0MseUJBQW1CLEdBQVEsSUFBSSxDQUFDO1FBRWhDLDhCQUF3QixHQUFXLEVBQUUsQ0FBQzs7SUFJdEMsQ0FBQzs7Ozs7SUFFRCwwQ0FBVzs7OztJQUFYLFVBQVksT0FBc0I7UUFDOUIsSUFBSSxPQUFPLENBQUMsVUFBVSxDQUFDLEVBQUU7WUFDckIsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQztZQUM5QyxJQUFJLENBQUMsd0JBQXdCLEdBQUcsT0FBTyxDQUFDO1NBQzNDO1FBQ0QsSUFBSSxPQUFPLENBQUMsT0FBTyxDQUFDLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDLFlBQVksRUFBRTtZQUNuRCxJQUFJLE9BQU8sQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLFFBQVEsRUFBRSxFQUFFO2dCQUN2QyxJQUFJLENBQUMsS0FBSyxHQUFHLE9BQU8sQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDO2dCQUN4QyxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxzQkFBc0IsRUFBRSxDQUFDO2dCQUMvRCxJQUFJLENBQUMsd0JBQXdCLEdBQUcsT0FBTyxDQUFDO2FBQzNDO1NBQ0o7SUFDTCxDQUFDOztnQkE1Q0osU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxjQUFjO29CQUN4QiwrZUFBcUM7b0JBRXJDLFVBQVUsRUFBRTt3QkFDUixPQUFPLENBQUMsb0JBQW9CLEVBQUU7NEJBQzFCLEtBQUssQ0FBQyxPQUFPLEVBQUUsS0FBSyxDQUFDLEVBQUMsT0FBTyxFQUFFLENBQUMsRUFBRSxTQUFTLEVBQUUsZ0JBQWdCLEVBQUMsQ0FBQyxDQUFDOzRCQUNoRSxVQUFVLENBQUMsZUFBZSxFQUFFO2dDQUN4QixLQUFLLENBQUMsRUFBQyxPQUFPLEVBQUUsQ0FBQyxFQUFFLFNBQVMsRUFBRSxtQkFBbUIsRUFBQyxDQUFDO2dDQUNuRCxPQUFPLENBQUMsd0NBQXdDLENBQUM7NkJBQ3BELENBQUM7eUJBQ0wsQ0FBQztxQkFDTDtvQkFDRCxJQUFJLEVBQUUsUUFBUTtvQkFDZCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7aUJBQ3hDOzs7O2dCQWxCUSxXQUFXOzs7d0JBcUJmLEtBQUs7MkJBR0wsS0FBSzs7SUF3QlYsMkJBQUM7Q0FBQSxBQTdDRCxDQWdCMEMsZUFBZSxHQTZCeEQ7U0E3Qlksb0JBQW9COzs7SUFFN0IscUNBQ3lCOztJQUV6Qix3Q0FDaUI7O0lBRWpCLG1EQUFnQzs7SUFFaEMsd0RBQXNDOztJQUUxQiwyQ0FBK0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgYW5pbWF0ZSwgc3RhdGUsIHN0eWxlLCB0cmFuc2l0aW9uLCB0cmlnZ2VyIH0gZnJvbSAnQGFuZ3VsYXIvYW5pbWF0aW9ucyc7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgSW5wdXQsIE9uQ2hhbmdlcywgU2ltcGxlQ2hhbmdlcywgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRXJyb3JNZXNzYWdlTW9kZWwgfSBmcm9tICcuLi9jb3JlL2luZGV4JztcclxuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLy4uLy4uLy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XHJcbmltcG9ydCB7IGJhc2VIb3N0ICwgV2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi8uLi93aWRnZXQuY29tcG9uZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdlcnJvci13aWRnZXQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2Vycm9yLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2Vycm9yLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBhbmltYXRpb25zOiBbXHJcbiAgICAgICAgdHJpZ2dlcigndHJhbnNpdGlvbk1lc3NhZ2VzJywgW1xyXG4gICAgICAgICAgICBzdGF0ZSgnZW50ZXInLCBzdHlsZSh7b3BhY2l0eTogMSwgdHJhbnNmb3JtOiAndHJhbnNsYXRlWSgwJSknfSkpLFxyXG4gICAgICAgICAgICB0cmFuc2l0aW9uKCd2b2lkID0+IGVudGVyJywgW1xyXG4gICAgICAgICAgICAgICAgc3R5bGUoe29wYWNpdHk6IDAsIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZVkoLTEwMCUpJ30pLFxyXG4gICAgICAgICAgICAgICAgYW5pbWF0ZSgnMzAwbXMgY3ViaWMtYmV6aWVyKDAuNTUsIDAsIDAuNTUsIDAuMiknKVxyXG4gICAgICAgICAgICBdKVxyXG4gICAgICAgIF0pXHJcbiAgICBdLFxyXG4gICAgaG9zdDogYmFzZUhvc3QsXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBFcnJvcldpZGdldENvbXBvbmVudCBleHRlbmRzIFdpZGdldENvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIGVycm9yOiBFcnJvck1lc3NhZ2VNb2RlbDtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgcmVxdWlyZWQ6IHN0cmluZztcclxuXHJcbiAgICB0cmFuc2xhdGVQYXJhbWV0ZXJzOiBhbnkgPSBudWxsO1xyXG5cclxuICAgIF9zdWJzY3JpcHRBbmltYXRpb25TdGF0ZTogc3RyaW5nID0gJyc7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSkge1xyXG4gICAgICAgIHN1cGVyKGZvcm1TZXJ2aWNlKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICAgICAgaWYgKGNoYW5nZXNbJ3JlcXVpcmVkJ10pIHtcclxuICAgICAgICAgICAgdGhpcy5yZXF1aXJlZCA9IGNoYW5nZXMucmVxdWlyZWQuY3VycmVudFZhbHVlO1xyXG4gICAgICAgICAgICB0aGlzLl9zdWJzY3JpcHRBbmltYXRpb25TdGF0ZSA9ICdlbnRlcic7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmIChjaGFuZ2VzWydlcnJvciddICYmIGNoYW5nZXNbJ2Vycm9yJ10uY3VycmVudFZhbHVlKSB7XHJcbiAgICAgICAgICAgIGlmIChjaGFuZ2VzLmVycm9yLmN1cnJlbnRWYWx1ZS5pc0FjdGl2ZSgpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmVycm9yID0gY2hhbmdlcy5lcnJvci5jdXJyZW50VmFsdWU7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnRyYW5zbGF0ZVBhcmFtZXRlcnMgPSB0aGlzLmVycm9yLmdldEF0dHJpYnV0ZXNBc0pzb25PYmooKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuX3N1YnNjcmlwdEFuaW1hdGlvblN0YXRlID0gJ2VudGVyJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=