/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, ViewEncapsulation } from '@angular/core';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
var HyperlinkWidgetComponent = /** @class */ (function (_super) {
    tslib_1.__extends(HyperlinkWidgetComponent, _super);
    function HyperlinkWidgetComponent(formService) {
        var _this = _super.call(this, formService) || this;
        _this.formService = formService;
        _this.linkUrl = WidgetComponent.DEFAULT_HYPERLINK_URL;
        _this.linkText = null;
        return _this;
    }
    /**
     * @return {?}
     */
    HyperlinkWidgetComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.field) {
            this.linkUrl = this.getHyperlinkUrl(this.field);
            this.linkText = this.getHyperlinkText(this.field);
        }
    };
    HyperlinkWidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'hyperlink-widget',
                    template: "<div class=\"adf-hyperlink-widget {{field.className}}\">\r\n    <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}<span *ngIf=\"isRequired()\">*</span></label>\r\n    <div>\r\n        <a [href]=\"linkUrl\" target=\"_blank\" rel=\"nofollow\">{{linkText}}</a>\r\n    </div>\r\n</div>\r\n",
                    host: baseHost,
                    encapsulation: ViewEncapsulation.None,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    HyperlinkWidgetComponent.ctorParameters = function () { return [
        { type: FormService }
    ]; };
    return HyperlinkWidgetComponent;
}(WidgetComponent));
export { HyperlinkWidgetComponent };
if (false) {
    /** @type {?} */
    HyperlinkWidgetComponent.prototype.linkUrl;
    /** @type {?} */
    HyperlinkWidgetComponent.prototype.linkText;
    /** @type {?} */
    HyperlinkWidgetComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaHlwZXJsaW5rLndpZGdldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL2h5cGVybGluay9oeXBlcmxpbmsud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFNBQVMsRUFBVSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNyRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDL0QsT0FBTyxFQUFFLFFBQVEsRUFBRyxlQUFlLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUVuRTtJQU84QyxvREFBZTtJQUt6RCxrQ0FBbUIsV0FBd0I7UUFBM0MsWUFDSyxrQkFBTSxXQUFXLENBQUMsU0FDdEI7UUFGa0IsaUJBQVcsR0FBWCxXQUFXLENBQWE7UUFIM0MsYUFBTyxHQUFXLGVBQWUsQ0FBQyxxQkFBcUIsQ0FBQztRQUN4RCxjQUFRLEdBQVcsSUFBSSxDQUFDOztJQUl4QixDQUFDOzs7O0lBRUQsMkNBQVE7OztJQUFSO1FBQ0ksSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1osSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNoRCxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDckQ7SUFDTCxDQUFDOztnQkFyQkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxrQkFBa0I7b0JBQzVCLHFVQUFzQztvQkFFdEMsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2lCQUN4Qzs7OztnQkFUUSxXQUFXOztJQTBCcEIsK0JBQUM7Q0FBQSxBQXZCRCxDQU84QyxlQUFlLEdBZ0I1RDtTQWhCWSx3QkFBd0I7OztJQUVqQywyQ0FBd0Q7O0lBQ3hELDRDQUF3Qjs7SUFFWiwrQ0FBK0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuIC8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4vLi4vLi4vLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgYmFzZUhvc3QgLCBXaWRnZXRDb21wb25lbnQgfSBmcm9tICcuLy4uL3dpZGdldC5jb21wb25lbnQnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2h5cGVybGluay13aWRnZXQnLFxyXG4gICAgdGVtcGxhdGVVcmw6ICcuL2h5cGVybGluay53aWRnZXQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9oeXBlcmxpbmsud2lkZ2V0LnNjc3MnXSxcclxuICAgIGhvc3Q6IGJhc2VIb3N0LFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgSHlwZXJsaW5rV2lkZ2V0Q29tcG9uZW50IGV4dGVuZHMgV2lkZ2V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBsaW5rVXJsOiBzdHJpbmcgPSBXaWRnZXRDb21wb25lbnQuREVGQVVMVF9IWVBFUkxJTktfVVJMO1xyXG4gICAgbGlua1RleHQ6IHN0cmluZyA9IG51bGw7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSkge1xyXG4gICAgICAgICBzdXBlcihmb3JtU2VydmljZSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZmllbGQpIHtcclxuICAgICAgICAgICAgdGhpcy5saW5rVXJsID0gdGhpcy5nZXRIeXBlcmxpbmtVcmwodGhpcy5maWVsZCk7XHJcbiAgICAgICAgICAgIHRoaXMubGlua1RleHQgPSB0aGlzLmdldEh5cGVybGlua1RleHQodGhpcy5maWVsZCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=