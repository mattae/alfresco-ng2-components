/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, ViewEncapsulation } from '@angular/core';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
var MultilineTextWidgetComponentComponent = /** @class */ (function (_super) {
    tslib_1.__extends(MultilineTextWidgetComponentComponent, _super);
    function MultilineTextWidgetComponentComponent(formService) {
        var _this = _super.call(this, formService) || this;
        _this.formService = formService;
        return _this;
    }
    MultilineTextWidgetComponentComponent.decorators = [
        { type: Component, args: [{
                    selector: 'multiline-text-widget',
                    template: "<div class=\"adf-multiline-text-widget {{field.className}}\"\r\n     [class.adf-invalid]=\"!field.isValid\" [class.adf-readonly]=\"field.readOnly\">\r\n    <mat-form-field floatPlaceholder=\"never\">\r\n        <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}<span *ngIf=\"isRequired()\">*</span></label>\r\n        <textarea matInput class=\"adf-input\"\r\n                  matTextareaAutosize\r\n                  type=\"text\"\r\n                  rows=\"3\"\r\n                  [id]=\"field.id\"\r\n                  [required]=\"isRequired()\"\r\n                  [(ngModel)]=\"field.value\"\r\n                  (ngModelChange)=\"onFieldChanged(field)\"\r\n                  [disabled]=\"field.readOnly || readOnly\"\r\n                  placeholder=\"{{field.placeholder}}\">\r\n        </textarea>\r\n    </mat-form-field>\r\n    <div *ngIf=\"field.maxLength > 0\" class=\"adf-multiline-word-counter\">\r\n        <span>{{field?.value?.length || 0}}/{{field.maxLength}}</span>\r\n    </div>\r\n    <error-widget [error]=\"field.validationSummary\"></error-widget>\r\n    <error-widget class=\"adf-multiline-required-message\" *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n</div>\r\n\r\n",
                    host: baseHost,
                    encapsulation: ViewEncapsulation.None,
                    styles: [".adf-multiline-text-widget{width:100%}.adf-multiline-word-counter{float:right;margin-top:-20px!important;min-height:24px;min-width:1px;font-size:12px;line-height:14px;overflow:hidden;transition:.3s cubic-bezier(.55,0,.55,.2);opacity:1;padding-top:5px;text-align:right;padding-right:2px;padding-left:0}.adf-multiline-required-message{display:flex}"]
                }] }
    ];
    /** @nocollapse */
    MultilineTextWidgetComponentComponent.ctorParameters = function () { return [
        { type: FormService }
    ]; };
    return MultilineTextWidgetComponentComponent;
}(WidgetComponent));
export { MultilineTextWidgetComponentComponent };
if (false) {
    /** @type {?} */
    MultilineTextWidgetComponentComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXVsdGlsaW5lLXRleHQud2lkZ2V0LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9jb21wb25lbnRzL3dpZGdldHMvbXVsdGlsaW5lLXRleHQvbXVsdGlsaW5lLXRleHQud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDL0QsT0FBTyxFQUFFLFFBQVEsRUFBRyxlQUFlLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUVuRTtJQU8yRCxpRUFBZTtJQUV0RSwrQ0FBbUIsV0FBd0I7UUFBM0MsWUFDSSxrQkFBTSxXQUFXLENBQUMsU0FDckI7UUFGa0IsaUJBQVcsR0FBWCxXQUFXLENBQWE7O0lBRTNDLENBQUM7O2dCQVhKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsdUJBQXVCO29CQUNqQyxveENBQTJDO29CQUUzQyxJQUFJLEVBQUUsUUFBUTtvQkFDZCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7aUJBQ3hDOzs7O2dCQVRRLFdBQVc7O0lBZ0JwQiw0Q0FBQztDQUFBLEFBYkQsQ0FPMkQsZUFBZSxHQU16RTtTQU5ZLHFDQUFxQzs7O0lBRWxDLDREQUErQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4vKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi8uLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBiYXNlSG9zdCAsIFdpZGdldENvbXBvbmVudCB9IGZyb20gJy4vLi4vd2lkZ2V0LmNvbXBvbmVudCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnbXVsdGlsaW5lLXRleHQtd2lkZ2V0JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9tdWx0aWxpbmUtdGV4dC53aWRnZXQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9tdWx0aWxpbmUtdGV4dC53aWRnZXQuc2NzcyddLFxyXG4gICAgaG9zdDogYmFzZUhvc3QsXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBNdWx0aWxpbmVUZXh0V2lkZ2V0Q29tcG9uZW50Q29tcG9uZW50IGV4dGVuZHMgV2lkZ2V0Q29tcG9uZW50ICB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSkge1xyXG4gICAgICAgIHN1cGVyKGZvcm1TZXJ2aWNlKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19