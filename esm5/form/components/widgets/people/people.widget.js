/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { PeopleProcessService } from '../../../../services/people-process.service';
import { Component, ElementRef, EventEmitter, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormService } from '../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
import { FormControl } from '@angular/forms';
import { of } from 'rxjs';
import { catchError, distinctUntilChanged, map, switchMap, tap } from 'rxjs/operators';
var PeopleWidgetComponent = /** @class */ (function (_super) {
    tslib_1.__extends(PeopleWidgetComponent, _super);
    function PeopleWidgetComponent(formService, peopleProcessService) {
        var _this = _super.call(this, formService) || this;
        _this.formService = formService;
        _this.peopleProcessService = peopleProcessService;
        _this.searchTerm = new FormControl();
        _this.errorMsg = '';
        _this.searchTerms$ = _this.searchTerm.valueChanges;
        _this.users$ = _this.searchTerms$.pipe(tap((/**
         * @return {?}
         */
        function () {
            _this.errorMsg = '';
        })), distinctUntilChanged(), switchMap((/**
         * @param {?} searchTerm
         * @return {?}
         */
        function (searchTerm) {
            /** @type {?} */
            var value = searchTerm.email ? _this.getDisplayName(searchTerm) : searchTerm;
            return _this.formService.getWorkflowUsers(value, _this.groupId)
                .pipe(catchError((/**
             * @param {?} err
             * @return {?}
             */
            function (err) {
                _this.errorMsg = err.message;
                return of();
            })));
        })), map((/**
         * @param {?} list
         * @return {?}
         */
        function (list) {
            /** @type {?} */
            var value = _this.searchTerm.value.email ? _this.getDisplayName(_this.searchTerm.value) : _this.searchTerm.value;
            _this.checkUserAndValidateForm(list, value);
            return list;
        })));
        _this.peopleSelected = new EventEmitter();
        return _this;
    }
    /**
     * @return {?}
     */
    PeopleWidgetComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.field) {
            if (this.field.value) {
                this.searchTerm.setValue(this.field.value);
            }
            if (this.field.readOnly) {
                this.searchTerm.disable();
            }
            /** @type {?} */
            var params = this.field.params;
            if (params && params.restrictWithGroup) {
                /** @type {?} */
                var restrictWithGroup = (/** @type {?} */ (params.restrictWithGroup));
                this.groupId = restrictWithGroup.id;
            }
        }
    };
    /**
     * @param {?} list
     * @param {?} value
     * @return {?}
     */
    PeopleWidgetComponent.prototype.checkUserAndValidateForm = /**
     * @param {?} list
     * @param {?} value
     * @return {?}
     */
    function (list, value) {
        /** @type {?} */
        var isValidUser = this.isValidUser(list, value);
        if (isValidUser || value === '') {
            this.field.validationSummary.message = '';
            this.field.validate();
            this.field.form.validateForm();
        }
        else {
            this.field.validationSummary.message = 'FORM.FIELD.VALIDATOR.INVALID_VALUE';
            this.field.markAsInvalid();
            this.field.form.markAsInvalid();
        }
    };
    /**
     * @param {?} users
     * @param {?} name
     * @return {?}
     */
    PeopleWidgetComponent.prototype.isValidUser = /**
     * @param {?} users
     * @param {?} name
     * @return {?}
     */
    function (users, name) {
        var _this = this;
        if (users) {
            return users.find((/**
             * @param {?} user
             * @return {?}
             */
            function (user) {
                /** @type {?} */
                var selectedUser = _this.getDisplayName(user).toLocaleLowerCase() === name.toLocaleLowerCase();
                if (selectedUser) {
                    _this.peopleSelected.emit(user && user.id || undefined);
                }
                return selectedUser;
            }));
        }
    };
    /**
     * @param {?} model
     * @return {?}
     */
    PeopleWidgetComponent.prototype.getDisplayName = /**
     * @param {?} model
     * @return {?}
     */
    function (model) {
        if (model) {
            /** @type {?} */
            var displayName = (model.firstName || '') + " " + (model.lastName || '');
            return displayName.trim();
        }
        return '';
    };
    /**
     * @param {?} item
     * @return {?}
     */
    PeopleWidgetComponent.prototype.onItemSelect = /**
     * @param {?} item
     * @return {?}
     */
    function (item) {
        if (item) {
            this.field.value = item;
        }
    };
    PeopleWidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'people-widget',
                    template: "<div class=\"adf-people-widget {{field.className}}\"\r\n     [class.adf-invalid]=\"!field.isValid\"\r\n     [class.adf-readonly]=\"field.readOnly\"\r\n     id=\"people-widget-content\">\r\n    <mat-form-field>\r\n        <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}<span *ngIf=\"isRequired()\">*</span></label>\r\n        <input #inputValue\r\n               matInput\r\n               class=\"adf-input\"\r\n               data-automation-id=\"adf-people-search-input\"\r\n               type=\"text\"\r\n               [id]=\"field.id\"\r\n               [formControl]=\"searchTerm\"\r\n               placeholder=\"{{field.placeholder}}\"\r\n               [matAutocomplete]=\"auto\">\r\n        <mat-autocomplete class=\"adf-people-widget-list\"\r\n                          #auto=\"matAutocomplete\"\r\n                          (optionSelected)=\"onItemSelect($event.option.value)\"\r\n                          [displayWith]=\"getDisplayName\">\r\n            <mat-option *ngFor=\"let user of users$ | async; let i = index\" [value]=\"user\">\r\n                <div class=\"adf-people-widget-row\" id=\"adf-people-widget-user-{{i}}\">\r\n                    <div [outerHTML]=\"user | usernameInitials:'adf-people-widget-pic'\"></div>\r\n                    <div *ngIf=\"user.pictureId\" class=\"adf-people-widget-image-row\">\r\n                        <img id=\"adf-people-widget-pic-{{i}}\" class=\"adf-people-widget-image\"\r\n                             [alt]=\"getDisplayName(user)\" [src]=\"peopleProcessService.getUserImage(user)\"/>\r\n                    </div>\r\n                    <span class=\"adf-people-label-name\">{{getDisplayName(user)}}</span>\r\n                </div>\r\n            </mat-option>\r\n        </mat-autocomplete>\r\n    </mat-form-field>\r\n    <error-widget [error]=\"field.validationSummary\"></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n</div>\r\n",
                    host: baseHost,
                    encapsulation: ViewEncapsulation.None,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    PeopleWidgetComponent.ctorParameters = function () { return [
        { type: FormService },
        { type: PeopleProcessService }
    ]; };
    PeopleWidgetComponent.propDecorators = {
        input: [{ type: ViewChild, args: ['inputValue', { static: true },] }],
        peopleSelected: [{ type: Output }]
    };
    return PeopleWidgetComponent;
}(WidgetComponent));
export { PeopleWidgetComponent };
if (false) {
    /** @type {?} */
    PeopleWidgetComponent.prototype.input;
    /** @type {?} */
    PeopleWidgetComponent.prototype.peopleSelected;
    /** @type {?} */
    PeopleWidgetComponent.prototype.groupId;
    /** @type {?} */
    PeopleWidgetComponent.prototype.value;
    /** @type {?} */
    PeopleWidgetComponent.prototype.searchTerm;
    /** @type {?} */
    PeopleWidgetComponent.prototype.errorMsg;
    /** @type {?} */
    PeopleWidgetComponent.prototype.searchTerms$;
    /** @type {?} */
    PeopleWidgetComponent.prototype.users$;
    /** @type {?} */
    PeopleWidgetComponent.prototype.formService;
    /** @type {?} */
    PeopleWidgetComponent.prototype.peopleProcessService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVvcGxlLndpZGdldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL3Blb3BsZS9wZW9wbGUud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBcUNBLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLDZDQUE2QyxDQUFDO0FBRW5GLE9BQU8sRUFBRSxTQUFTLEVBQUUsVUFBVSxFQUFFLFlBQVksRUFBVSxNQUFNLEVBQUUsU0FBUyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2xILE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUU3RCxPQUFPLEVBQUUsUUFBUSxFQUFFLGVBQWUsRUFBRSxNQUFNLHVCQUF1QixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM3QyxPQUFPLEVBQWMsRUFBRSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3RDLE9BQU8sRUFDSCxVQUFVLEVBQ1Ysb0JBQW9CLEVBQ3BCLEdBQUcsRUFDSCxTQUFTLEVBQ1QsR0FBRyxFQUNOLE1BQU0sZ0JBQWdCLENBQUM7QUFFeEI7SUFPMkMsaURBQWU7SUFxQ3RELCtCQUFtQixXQUF3QixFQUFTLG9CQUEwQztRQUE5RixZQUNJLGtCQUFNLFdBQVcsQ0FBQyxTQUVyQjtRQUhrQixpQkFBVyxHQUFYLFdBQVcsQ0FBYTtRQUFTLDBCQUFvQixHQUFwQixvQkFBb0IsQ0FBc0I7UUExQjlGLGdCQUFVLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQztRQUMvQixjQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2Qsa0JBQVksR0FBb0IsS0FBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLENBQUM7UUFFN0QsWUFBTSxHQUFHLEtBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUMzQixHQUFHOzs7UUFBQztZQUNBLEtBQUksQ0FBQyxRQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ3ZCLENBQUMsRUFBQyxFQUNGLG9CQUFvQixFQUFFLEVBQ3RCLFNBQVM7Ozs7UUFBQyxVQUFDLFVBQVU7O2dCQUNYLEtBQUssR0FBRyxVQUFVLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFJLENBQUMsY0FBYyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQyxVQUFVO1lBQzdFLE9BQU8sS0FBSSxDQUFDLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLEVBQUUsS0FBSSxDQUFDLE9BQU8sQ0FBQztpQkFDeEQsSUFBSSxDQUNELFVBQVU7Ozs7WUFBQyxVQUFDLEdBQUc7Z0JBQ1gsS0FBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUMsT0FBTyxDQUFDO2dCQUM1QixPQUFPLEVBQUUsRUFBRSxDQUFDO1lBQ2hCLENBQUMsRUFBQyxDQUNMLENBQUM7UUFDVixDQUFDLEVBQUMsRUFDRixHQUFHOzs7O1FBQUMsVUFBQyxJQUF3Qjs7Z0JBQ25CLEtBQUssR0FBRyxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLEtBQUksQ0FBQyxjQUFjLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLO1lBQzlHLEtBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLEVBQUUsS0FBSyxDQUFDLENBQUM7WUFDM0MsT0FBTyxJQUFJLENBQUM7UUFDaEIsQ0FBQyxFQUFDLENBQ0wsQ0FBQztRQUlFLEtBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQzs7SUFDN0MsQ0FBQzs7OztJQUVELHdDQUFROzs7SUFBUjtRQUNJLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNaLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUU7Z0JBQ2xCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDOUM7WUFDRCxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFO2dCQUNyQixJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxDQUFDO2FBQzdCOztnQkFDSyxNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNO1lBQ2hDLElBQUksTUFBTSxJQUFJLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRTs7b0JBQzlCLGlCQUFpQixHQUFHLG1CQUFhLE1BQU0sQ0FBQyxpQkFBaUIsRUFBQTtnQkFDL0QsSUFBSSxDQUFDLE9BQU8sR0FBRyxpQkFBaUIsQ0FBQyxFQUFFLENBQUM7YUFDdkM7U0FDSjtJQUNMLENBQUM7Ozs7OztJQUVELHdEQUF3Qjs7Ozs7SUFBeEIsVUFBeUIsSUFBSSxFQUFFLEtBQUs7O1lBQzFCLFdBQVcsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxLQUFLLENBQUM7UUFDakQsSUFBSSxXQUFXLElBQUksS0FBSyxLQUFLLEVBQUUsRUFBRTtZQUM3QixJQUFJLENBQUMsS0FBSyxDQUFDLGlCQUFpQixDQUFDLE9BQU8sR0FBRyxFQUFFLENBQUM7WUFDMUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztTQUNsQzthQUFNO1lBQ0gsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxPQUFPLEdBQUcsb0NBQW9DLENBQUM7WUFDNUUsSUFBSSxDQUFDLEtBQUssQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUMzQixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztTQUNuQztJQUNMLENBQUM7Ozs7OztJQUVELDJDQUFXOzs7OztJQUFYLFVBQVksS0FBeUIsRUFBRSxJQUFZO1FBQW5ELGlCQVVDO1FBVEcsSUFBSSxLQUFLLEVBQUU7WUFDUCxPQUFPLEtBQUssQ0FBQyxJQUFJOzs7O1lBQUMsVUFBQyxJQUFJOztvQkFDYixZQUFZLEdBQUcsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxpQkFBaUIsRUFBRSxLQUFLLElBQUksQ0FBQyxpQkFBaUIsRUFBRTtnQkFDL0YsSUFBSSxZQUFZLEVBQUU7b0JBQ2QsS0FBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxFQUFFLElBQUksU0FBUyxDQUFDLENBQUM7aUJBQzFEO2dCQUNELE9BQU8sWUFBWSxDQUFDO1lBQ3hCLENBQUMsRUFBQyxDQUFDO1NBQ047SUFDTCxDQUFDOzs7OztJQUVELDhDQUFjOzs7O0lBQWQsVUFBZSxLQUF1QjtRQUNsQyxJQUFJLEtBQUssRUFBRTs7Z0JBQ0QsV0FBVyxHQUFHLENBQUcsS0FBSyxDQUFDLFNBQVMsSUFBSSxFQUFFLFdBQUksS0FBSyxDQUFDLFFBQVEsSUFBSSxFQUFFLENBQUU7WUFDdEUsT0FBTyxXQUFXLENBQUMsSUFBSSxFQUFFLENBQUM7U0FDN0I7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7Ozs7O0lBRUQsNENBQVk7Ozs7SUFBWixVQUFhLElBQXNCO1FBQy9CLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDO1NBQzNCO0lBQ0wsQ0FBQzs7Z0JBdEdKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsZUFBZTtvQkFDekIseS9EQUFtQztvQkFFbkMsSUFBSSxFQUFFLFFBQVE7b0JBQ2QsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2lCQUN4Qzs7OztnQkFuQlEsV0FBVztnQkFIWCxvQkFBb0I7Ozt3QkF5QnhCLFNBQVMsU0FBQyxZQUFZLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDO2lDQUd0QyxNQUFNOztJQTJGWCw0QkFBQztDQUFBLEFBdkdELENBTzJDLGVBQWUsR0FnR3pEO1NBaEdZLHFCQUFxQjs7O0lBRTlCLHNDQUNrQjs7SUFFbEIsK0NBQ3FDOztJQUVyQyx3Q0FBZ0I7O0lBQ2hCLHNDQUFXOztJQUVYLDJDQUErQjs7SUFDL0IseUNBQWM7O0lBQ2QsNkNBQTZEOztJQUU3RCx1Q0FvQkU7O0lBRVUsNENBQStCOztJQUFFLHFEQUFpRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxuXG4vKiFcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxuICpcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cbiAqL1xuXG4vKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXG5cbmltcG9ydCB7IFBlb3BsZVByb2Nlc3NTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vc2VydmljZXMvcGVvcGxlLXByb2Nlc3Muc2VydmljZSc7XG5pbXBvcnQgeyBVc2VyUHJvY2Vzc01vZGVsIH0gZnJvbSAnLi4vLi4vLi4vLi4vbW9kZWxzJztcbmltcG9ydCB7IENvbXBvbmVudCwgRWxlbWVudFJlZiwgRXZlbnRFbWl0dGVyLCBPbkluaXQsIE91dHB1dCwgVmlld0NoaWxkLCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xuaW1wb3J0IHsgR3JvdXBNb2RlbCB9IGZyb20gJy4uL2NvcmUvZ3JvdXAubW9kZWwnO1xuaW1wb3J0IHsgYmFzZUhvc3QsIFdpZGdldENvbXBvbmVudCB9IGZyb20gJy4vLi4vd2lkZ2V0LmNvbXBvbmVudCc7XG5pbXBvcnQgeyBGb3JtQ29udHJvbCB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcbmltcG9ydCB7IE9ic2VydmFibGUsIG9mIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQge1xuICAgIGNhdGNoRXJyb3IsXG4gICAgZGlzdGluY3RVbnRpbENoYW5nZWQsXG4gICAgbWFwLFxuICAgIHN3aXRjaE1hcCxcbiAgICB0YXBcbn0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ3Blb3BsZS13aWRnZXQnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9wZW9wbGUud2lkZ2V0Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL3Blb3BsZS53aWRnZXQuc2NzcyddLFxuICAgIGhvc3Q6IGJhc2VIb3N0LFxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcbn0pXG5leHBvcnQgY2xhc3MgUGVvcGxlV2lkZ2V0Q29tcG9uZW50IGV4dGVuZHMgV2lkZ2V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAgIEBWaWV3Q2hpbGQoJ2lucHV0VmFsdWUnLCB7c3RhdGljOiB0cnVlfSlcbiAgICBpbnB1dDogRWxlbWVudFJlZjtcblxuICAgIEBPdXRwdXQoKVxuICAgIHBlb3BsZVNlbGVjdGVkOiBFdmVudEVtaXR0ZXI8bnVtYmVyPjtcblxuICAgIGdyb3VwSWQ6IHN0cmluZztcbiAgICB2YWx1ZTogYW55O1xuXG4gICAgc2VhcmNoVGVybSA9IG5ldyBGb3JtQ29udHJvbCgpO1xuICAgIGVycm9yTXNnID0gJyc7XG4gICAgc2VhcmNoVGVybXMkOiBPYnNlcnZhYmxlPGFueT4gPSB0aGlzLnNlYXJjaFRlcm0udmFsdWVDaGFuZ2VzO1xuXG4gICAgdXNlcnMkID0gdGhpcy5zZWFyY2hUZXJtcyQucGlwZShcbiAgICAgICAgdGFwKCgpID0+IHtcbiAgICAgICAgICAgIHRoaXMuZXJyb3JNc2cgPSAnJztcbiAgICAgICAgfSksXG4gICAgICAgIGRpc3RpbmN0VW50aWxDaGFuZ2VkKCksXG4gICAgICAgIHN3aXRjaE1hcCgoc2VhcmNoVGVybSkgPT4ge1xuICAgICAgICAgICAgY29uc3QgdmFsdWUgPSBzZWFyY2hUZXJtLmVtYWlsID8gdGhpcy5nZXREaXNwbGF5TmFtZShzZWFyY2hUZXJtKSA6IHNlYXJjaFRlcm07XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5mb3JtU2VydmljZS5nZXRXb3JrZmxvd1VzZXJzKHZhbHVlLCB0aGlzLmdyb3VwSWQpXG4gICAgICAgICAgICAgICAgLnBpcGUoXG4gICAgICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lcnJvck1zZyA9IGVyci5tZXNzYWdlO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG9mKCk7XG4gICAgICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAgICAgKTtcbiAgICAgICAgfSksXG4gICAgICAgIG1hcCgobGlzdDogVXNlclByb2Nlc3NNb2RlbFtdKSA9PiB7XG4gICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHRoaXMuc2VhcmNoVGVybS52YWx1ZS5lbWFpbCA/IHRoaXMuZ2V0RGlzcGxheU5hbWUodGhpcy5zZWFyY2hUZXJtLnZhbHVlKSA6IHRoaXMuc2VhcmNoVGVybS52YWx1ZTtcbiAgICAgICAgICAgIHRoaXMuY2hlY2tVc2VyQW5kVmFsaWRhdGVGb3JtKGxpc3QsIHZhbHVlKTtcbiAgICAgICAgICAgIHJldHVybiBsaXN0O1xuICAgICAgICB9KVxuICAgICk7XG5cbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZm9ybVNlcnZpY2U6IEZvcm1TZXJ2aWNlLCBwdWJsaWMgcGVvcGxlUHJvY2Vzc1NlcnZpY2U6IFBlb3BsZVByb2Nlc3NTZXJ2aWNlKSB7XG4gICAgICAgIHN1cGVyKGZvcm1TZXJ2aWNlKTtcbiAgICAgICAgdGhpcy5wZW9wbGVTZWxlY3RlZCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcbiAgICB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgaWYgKHRoaXMuZmllbGQpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLmZpZWxkLnZhbHVlKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5zZWFyY2hUZXJtLnNldFZhbHVlKHRoaXMuZmllbGQudmFsdWUpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgaWYgKHRoaXMuZmllbGQucmVhZE9ubHkpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnNlYXJjaFRlcm0uZGlzYWJsZSgpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgY29uc3QgcGFyYW1zID0gdGhpcy5maWVsZC5wYXJhbXM7XG4gICAgICAgICAgICBpZiAocGFyYW1zICYmIHBhcmFtcy5yZXN0cmljdFdpdGhHcm91cCkge1xuICAgICAgICAgICAgICAgIGNvbnN0IHJlc3RyaWN0V2l0aEdyb3VwID0gPEdyb3VwTW9kZWw+IHBhcmFtcy5yZXN0cmljdFdpdGhHcm91cDtcbiAgICAgICAgICAgICAgICB0aGlzLmdyb3VwSWQgPSByZXN0cmljdFdpdGhHcm91cC5pZDtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIGNoZWNrVXNlckFuZFZhbGlkYXRlRm9ybShsaXN0LCB2YWx1ZSkge1xuICAgICAgICBjb25zdCBpc1ZhbGlkVXNlciA9IHRoaXMuaXNWYWxpZFVzZXIobGlzdCwgdmFsdWUpO1xuICAgICAgICBpZiAoaXNWYWxpZFVzZXIgfHwgdmFsdWUgPT09ICcnKSB7XG4gICAgICAgICAgICB0aGlzLmZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5Lm1lc3NhZ2UgPSAnJztcbiAgICAgICAgICAgIHRoaXMuZmllbGQudmFsaWRhdGUoKTtcbiAgICAgICAgICAgIHRoaXMuZmllbGQuZm9ybS52YWxpZGF0ZUZvcm0oKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuZmllbGQudmFsaWRhdGlvblN1bW1hcnkubWVzc2FnZSA9ICdGT1JNLkZJRUxELlZBTElEQVRPUi5JTlZBTElEX1ZBTFVFJztcbiAgICAgICAgICAgIHRoaXMuZmllbGQubWFya0FzSW52YWxpZCgpO1xuICAgICAgICAgICAgdGhpcy5maWVsZC5mb3JtLm1hcmtBc0ludmFsaWQoKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGlzVmFsaWRVc2VyKHVzZXJzOiBVc2VyUHJvY2Vzc01vZGVsW10sIG5hbWU6IHN0cmluZykge1xuICAgICAgICBpZiAodXNlcnMpIHtcbiAgICAgICAgICAgIHJldHVybiB1c2Vycy5maW5kKCh1c2VyKSA9PiB7XG4gICAgICAgICAgICAgICAgY29uc3Qgc2VsZWN0ZWRVc2VyID0gdGhpcy5nZXREaXNwbGF5TmFtZSh1c2VyKS50b0xvY2FsZUxvd2VyQ2FzZSgpID09PSBuYW1lLnRvTG9jYWxlTG93ZXJDYXNlKCk7XG4gICAgICAgICAgICAgICAgaWYgKHNlbGVjdGVkVXNlcikge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnBlb3BsZVNlbGVjdGVkLmVtaXQodXNlciAmJiB1c2VyLmlkIHx8IHVuZGVmaW5lZCk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgIHJldHVybiBzZWxlY3RlZFVzZXI7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGdldERpc3BsYXlOYW1lKG1vZGVsOiBVc2VyUHJvY2Vzc01vZGVsKSB7XG4gICAgICAgIGlmIChtb2RlbCkge1xuICAgICAgICAgICAgY29uc3QgZGlzcGxheU5hbWUgPSBgJHttb2RlbC5maXJzdE5hbWUgfHwgJyd9ICR7bW9kZWwubGFzdE5hbWUgfHwgJyd9YDtcbiAgICAgICAgICAgIHJldHVybiBkaXNwbGF5TmFtZS50cmltKCk7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuICcnO1xuICAgIH1cblxuICAgIG9uSXRlbVNlbGVjdChpdGVtOiBVc2VyUHJvY2Vzc01vZGVsKSB7XG4gICAgICAgIGlmIChpdGVtKSB7XG4gICAgICAgICAgICB0aGlzLmZpZWxkLnZhbHVlID0gaXRlbTtcbiAgICAgICAgfVxuICAgIH1cbn1cbiJdfQ==