/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { LogService } from '../../../../services/log.service';
import { Component, ViewEncapsulation } from '@angular/core';
import { FormService } from '../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
var RadioButtonsWidgetComponent = /** @class */ (function (_super) {
    tslib_1.__extends(RadioButtonsWidgetComponent, _super);
    function RadioButtonsWidgetComponent(formService, logService) {
        var _this = _super.call(this, formService) || this;
        _this.formService = formService;
        _this.logService = logService;
        return _this;
    }
    /**
     * @return {?}
     */
    RadioButtonsWidgetComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.field && this.field.restUrl) {
            if (this.field.form.taskId) {
                this.getOptionsByTaskId();
            }
            else {
                this.getOptionsByProcessDefinitionId();
            }
        }
    };
    /**
     * @return {?}
     */
    RadioButtonsWidgetComponent.prototype.getOptionsByTaskId = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.formService
            .getRestFieldValues(this.field.form.taskId, this.field.id)
            .subscribe((/**
         * @param {?} formFieldOption
         * @return {?}
         */
        function (formFieldOption) {
            _this.field.options = formFieldOption || [];
            _this.field.updateForm();
        }), (/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); }));
    };
    /**
     * @return {?}
     */
    RadioButtonsWidgetComponent.prototype.getOptionsByProcessDefinitionId = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.formService
            .getRestFieldValuesByProcessId(this.field.form.processDefinitionId, this.field.id)
            .subscribe((/**
         * @param {?} formFieldOption
         * @return {?}
         */
        function (formFieldOption) {
            _this.field.options = formFieldOption || [];
            _this.field.updateForm();
        }), (/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); }));
    };
    /**
     * @param {?} optionSelected
     * @return {?}
     */
    RadioButtonsWidgetComponent.prototype.onOptionClick = /**
     * @param {?} optionSelected
     * @return {?}
     */
    function (optionSelected) {
        this.field.value = optionSelected;
        this.fieldChanged.emit(this.field);
    };
    /**
     * @param {?} error
     * @return {?}
     */
    RadioButtonsWidgetComponent.prototype.handleError = /**
     * @param {?} error
     * @return {?}
     */
    function (error) {
        this.logService.error(error);
    };
    RadioButtonsWidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'radio-buttons-widget',
                    template: "<div class=\"adf-radio-buttons-widget {{field.className}}\"\r\n     [class.adf-invalid]=\"!field.isValid\" [class.adf-readonly]=\"field.readOnly\" [id]=\"field.id\">\r\n    <div class=\"adf-radio-button-container\">\r\n        <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}<span *ngIf=\"isRequired()\">*</span></label>\r\n        <mat-radio-group class=\"adf-radio-group\" [(ngModel)]=\"field.value\">\r\n            <mat-radio-button\r\n                [id]=\"field.id + '-' + opt.id\"\r\n                [name]=\"field.id\"\r\n                [value]=\"opt.id\"\r\n                [disabled]=\"field.readOnly\"\r\n                [checked]=\"field.value === opt.id\"\r\n                (change)=\"onOptionClick(opt.id)\"\r\n                color=\"primary\"\r\n                class=\"adf-radio-button\" *ngFor=\"let opt of field.options\" >\r\n                {{opt.name}}\r\n            </mat-radio-button>\r\n        </mat-radio-group>\r\n    </div>\r\n    <error-widget [error]=\"field.validationSummary\" ></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n</div>\r\n\r\n\r\n",
                    host: baseHost,
                    encapsulation: ViewEncapsulation.None,
                    styles: [".adf-radio-button-container{margin-bottom:15px}.adf-radio-group{display:inline-flex;flex-direction:column}.adf-radio-button{margin:5px}"]
                }] }
    ];
    /** @nocollapse */
    RadioButtonsWidgetComponent.ctorParameters = function () { return [
        { type: FormService },
        { type: LogService }
    ]; };
    return RadioButtonsWidgetComponent;
}(WidgetComponent));
export { RadioButtonsWidgetComponent };
if (false) {
    /** @type {?} */
    RadioButtonsWidgetComponent.prototype.formService;
    /**
     * @type {?}
     * @private
     */
    RadioButtonsWidgetComponent.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmFkaW8tYnV0dG9ucy53aWRnZXQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy9yYWRpby1idXR0b25zL3JhZGlvLWJ1dHRvbnMud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQzlELE9BQU8sRUFBRSxTQUFTLEVBQVUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDckUsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBRTdELE9BQU8sRUFBRSxRQUFRLEVBQUcsZUFBZSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFFbkU7SUFPaUQsdURBQWU7SUFFNUQscUNBQW1CLFdBQXdCLEVBQ3ZCLFVBQXNCO1FBRDFDLFlBRUssa0JBQU0sV0FBVyxDQUFDLFNBQ3RCO1FBSGtCLGlCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3ZCLGdCQUFVLEdBQVYsVUFBVSxDQUFZOztJQUUxQyxDQUFDOzs7O0lBRUQsOENBQVE7OztJQUFSO1FBQ0ksSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxFQUFFO1lBQ2xDLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUN4QixJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQzthQUM3QjtpQkFBTTtnQkFDSCxJQUFJLENBQUMsK0JBQStCLEVBQUUsQ0FBQzthQUMxQztTQUNKO0lBQ0wsQ0FBQzs7OztJQUVELHdEQUFrQjs7O0lBQWxCO1FBQUEsaUJBYUM7UUFaRyxJQUFJLENBQUMsV0FBVzthQUNYLGtCQUFrQixDQUNmLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFDdEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQ2hCO2FBQ0EsU0FBUzs7OztRQUNOLFVBQUMsZUFBa0M7WUFDL0IsS0FBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsZUFBZSxJQUFJLEVBQUUsQ0FBQztZQUMzQyxLQUFJLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRSxDQUFDO1FBQzVCLENBQUM7Ozs7UUFDRCxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQ2pDLENBQUM7SUFDVixDQUFDOzs7O0lBRUQscUVBQStCOzs7SUFBL0I7UUFBQSxpQkFhQztRQVpHLElBQUksQ0FBQyxXQUFXO2FBQ1gsNkJBQTZCLENBQzFCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLG1CQUFtQixFQUNuQyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FDaEI7YUFDQSxTQUFTOzs7O1FBQ04sVUFBQyxlQUFrQztZQUMvQixLQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxlQUFlLElBQUksRUFBRSxDQUFDO1lBQzNDLEtBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDNUIsQ0FBQzs7OztRQUNELFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFDakMsQ0FBQztJQUNWLENBQUM7Ozs7O0lBRUQsbURBQWE7Ozs7SUFBYixVQUFjLGNBQW1CO1FBQzdCLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLGNBQWMsQ0FBQztRQUNsQyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdkMsQ0FBQzs7Ozs7SUFFRCxpREFBVzs7OztJQUFYLFVBQVksS0FBVTtRQUNsQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUNqQyxDQUFDOztnQkE3REosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxzQkFBc0I7b0JBQ2hDLG1zQ0FBMEM7b0JBRTFDLElBQUksRUFBRSxRQUFRO29CQUNkLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOztpQkFDeEM7Ozs7Z0JBVlEsV0FBVztnQkFGWCxVQUFVOztJQXFFbkIsa0NBQUM7Q0FBQSxBQS9ERCxDQU9pRCxlQUFlLEdBd0QvRDtTQXhEWSwyQkFBMkI7OztJQUV4QixrREFBK0I7Ozs7O0lBQy9CLGlEQUE4QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4gLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL3NlcnZpY2VzL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRm9ybUZpZWxkT3B0aW9uIH0gZnJvbSAnLi8uLi9jb3JlL2Zvcm0tZmllbGQtb3B0aW9uJztcclxuaW1wb3J0IHsgYmFzZUhvc3QgLCBXaWRnZXRDb21wb25lbnQgfSBmcm9tICcuLy4uL3dpZGdldC5jb21wb25lbnQnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ3JhZGlvLWJ1dHRvbnMtd2lkZ2V0JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9yYWRpby1idXR0b25zLndpZGdldC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL3JhZGlvLWJ1dHRvbnMud2lkZ2V0LnNjc3MnXSxcclxuICAgIGhvc3Q6IGJhc2VIb3N0LFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUmFkaW9CdXR0b25zV2lkZ2V0Q29tcG9uZW50IGV4dGVuZHMgV2lkZ2V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZm9ybVNlcnZpY2U6IEZvcm1TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlKSB7XHJcbiAgICAgICAgIHN1cGVyKGZvcm1TZXJ2aWNlKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBpZiAodGhpcy5maWVsZCAmJiB0aGlzLmZpZWxkLnJlc3RVcmwpIHtcclxuICAgICAgICAgICAgaWYgKHRoaXMuZmllbGQuZm9ybS50YXNrSWQpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZ2V0T3B0aW9uc0J5VGFza0lkKCk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmdldE9wdGlvbnNCeVByb2Nlc3NEZWZpbml0aW9uSWQoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRPcHRpb25zQnlUYXNrSWQoKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtU2VydmljZVxyXG4gICAgICAgICAgICAuZ2V0UmVzdEZpZWxkVmFsdWVzKFxyXG4gICAgICAgICAgICAgICAgdGhpcy5maWVsZC5mb3JtLnRhc2tJZCxcclxuICAgICAgICAgICAgICAgIHRoaXMuZmllbGQuaWRcclxuICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgKGZvcm1GaWVsZE9wdGlvbjogRm9ybUZpZWxkT3B0aW9uW10pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZpZWxkLm9wdGlvbnMgPSBmb3JtRmllbGRPcHRpb24gfHwgW107XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5maWVsZC51cGRhdGVGb3JtKCk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0T3B0aW9uc0J5UHJvY2Vzc0RlZmluaXRpb25JZCgpIHtcclxuICAgICAgICB0aGlzLmZvcm1TZXJ2aWNlXHJcbiAgICAgICAgICAgIC5nZXRSZXN0RmllbGRWYWx1ZXNCeVByb2Nlc3NJZChcclxuICAgICAgICAgICAgICAgIHRoaXMuZmllbGQuZm9ybS5wcm9jZXNzRGVmaW5pdGlvbklkLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5maWVsZC5pZFxyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAoZm9ybUZpZWxkT3B0aW9uOiBGb3JtRmllbGRPcHRpb25bXSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmllbGQub3B0aW9ucyA9IGZvcm1GaWVsZE9wdGlvbiB8fCBbXTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZpZWxkLnVwZGF0ZUZvcm0oKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycilcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBvbk9wdGlvbkNsaWNrKG9wdGlvblNlbGVjdGVkOiBhbnkpIHtcclxuICAgICAgICB0aGlzLmZpZWxkLnZhbHVlID0gb3B0aW9uU2VsZWN0ZWQ7XHJcbiAgICAgICAgdGhpcy5maWVsZENoYW5nZWQuZW1pdCh0aGlzLmZpZWxkKTtcclxuICAgIH1cclxuXHJcbiAgICBoYW5kbGVFcnJvcihlcnJvcjogYW55KSB7XHJcbiAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKGVycm9yKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19