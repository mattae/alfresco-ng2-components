/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable: component-selector no-use-before-declare no-input-rename  */
import { Directive, ElementRef, forwardRef, HostListener, Input, Renderer2 } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
/** @type {?} */
export var CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR = {
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef((/**
     * @return {?}
     */
    function () { return InputMaskDirective; })),
    multi: true
};
/**
 * Directive selectors without adf- prefix will be deprecated on 3.0.0
 */
var InputMaskDirective = /** @class */ (function () {
    function InputMaskDirective(el, render) {
        this.el = el;
        this.render = render;
        this.translationMask = {
            '0': { pattern: /\d/ },
            '9': { pattern: /\d/, optional: true },
            '#': { pattern: /\d/, recursive: true },
            'A': { pattern: /[a-zA-Z0-9]/ },
            'S': { pattern: /[a-zA-Z]/ }
        };
        this.byPassKeys = [9, 16, 17, 18, 36, 37, 38, 39, 40, 91];
        this.invalidCharacters = [];
        this._onChange = (/**
         * @param {?} _
         * @return {?}
         */
        function (_) {
        });
        this._onTouched = (/**
         * @return {?}
         */
        function () {
        });
    }
    /**
     * @param {?} event
     * @return {?}
     */
    InputMaskDirective.prototype.onTextInput = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.inputMask && this.inputMask.mask) {
            this.maskValue(this.el.nativeElement.value, this.el.nativeElement.selectionStart, this.inputMask.mask, this.inputMask.isReversed, event.keyCode);
        }
        else {
            this._onChange(this.el.nativeElement.value);
        }
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    InputMaskDirective.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes['inputMask'] && changes['inputMask'].currentValue['mask']) {
            this.inputMask = changes['inputMask'].currentValue;
        }
    };
    /**
     * @param {?} value
     * @return {?}
     */
    InputMaskDirective.prototype.writeValue = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.el.nativeElement.value = value;
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    InputMaskDirective.prototype.registerOnChange = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this._onChange = fn;
    };
    /**
     * @param {?} fn
     * @return {?}
     */
    InputMaskDirective.prototype.registerOnTouched = /**
     * @param {?} fn
     * @return {?}
     */
    function (fn) {
        this._onTouched = fn;
    };
    /**
     * @private
     * @param {?} actualValue
     * @param {?} startCaret
     * @param {?} maskToApply
     * @param {?} isMaskReversed
     * @param {?} keyCode
     * @return {?}
     */
    InputMaskDirective.prototype.maskValue = /**
     * @private
     * @param {?} actualValue
     * @param {?} startCaret
     * @param {?} maskToApply
     * @param {?} isMaskReversed
     * @param {?} keyCode
     * @return {?}
     */
    function (actualValue, startCaret, maskToApply, isMaskReversed, keyCode) {
        if (this.byPassKeys.indexOf(keyCode) === -1) {
            /** @type {?} */
            var value = this.getMasked(false, actualValue, maskToApply, isMaskReversed);
            /** @type {?} */
            var calculatedCaret = this.calculateCaretPosition(startCaret, actualValue, keyCode);
            this.render.setAttribute(this.el.nativeElement, 'value', value);
            this.el.nativeElement.value = value;
            this.setValue(value);
            this._onChange(value);
            this.setCaretPosition(calculatedCaret);
        }
    };
    /**
     * @private
     * @param {?} caretPosition
     * @return {?}
     */
    InputMaskDirective.prototype.setCaretPosition = /**
     * @private
     * @param {?} caretPosition
     * @return {?}
     */
    function (caretPosition) {
        this.el.nativeElement.moveStart = caretPosition;
        this.el.nativeElement.moveEnd = caretPosition;
    };
    /**
     * @param {?} caretPosition
     * @param {?} newValue
     * @param {?} keyCode
     * @return {?}
     */
    InputMaskDirective.prototype.calculateCaretPosition = /**
     * @param {?} caretPosition
     * @param {?} newValue
     * @param {?} keyCode
     * @return {?}
     */
    function (caretPosition, newValue, keyCode) {
        /** @type {?} */
        var newValueLength = newValue.length;
        /** @type {?} */
        var oldValue = this.getValue() || '';
        /** @type {?} */
        var oldValueLength = oldValue.length;
        if (keyCode === 8 && oldValue !== newValue) {
            caretPosition = caretPosition - (newValue.slice(0, caretPosition).length - oldValue.slice(0, caretPosition).length);
        }
        else if (oldValue !== newValue) {
            if (caretPosition >= oldValueLength) {
                caretPosition = newValueLength;
            }
            else {
                caretPosition = caretPosition + (newValue.slice(0, caretPosition).length - oldValue.slice(0, caretPosition).length);
            }
        }
        return caretPosition;
    };
    /**
     * @param {?} skipMaskChars
     * @param {?} val
     * @param {?} mask
     * @param {?=} isReversed
     * @return {?}
     */
    InputMaskDirective.prototype.getMasked = /**
     * @param {?} skipMaskChars
     * @param {?} val
     * @param {?} mask
     * @param {?=} isReversed
     * @return {?}
     */
    function (skipMaskChars, val, mask, isReversed) {
        if (isReversed === void 0) { isReversed = false; }
        /** @type {?} */
        var buf = [];
        /** @type {?} */
        var value = val;
        /** @type {?} */
        var maskIndex = 0;
        /** @type {?} */
        var maskLen = mask.length;
        /** @type {?} */
        var valueIndex = 0;
        /** @type {?} */
        var valueLength = value.length;
        /** @type {?} */
        var offset = 1;
        /** @type {?} */
        var addMethod = 'push';
        /** @type {?} */
        var resetPos = -1;
        /** @type {?} */
        var lastMaskChar;
        /** @type {?} */
        var lastUntranslatedMaskChar;
        /** @type {?} */
        var check;
        if (isReversed) {
            addMethod = 'unshift';
            offset = -1;
            lastMaskChar = 0;
            maskIndex = maskLen - 1;
            valueIndex = valueLength - 1;
        }
        else {
            lastMaskChar = maskLen - 1;
        }
        check = this.isToCheck(isReversed, maskIndex, maskLen, valueIndex, valueLength);
        while (check) {
            /** @type {?} */
            var maskDigit = mask.charAt(maskIndex);
            /** @type {?} */
            var valDigit = value.charAt(valueIndex);
            /** @type {?} */
            var translation = this.translationMask[maskDigit];
            if (translation) {
                if (valDigit.match(translation.pattern)) {
                    buf[addMethod](valDigit);
                    if (translation.recursive) {
                        if (resetPos === -1) {
                            resetPos = maskIndex;
                        }
                        else if (maskIndex === lastMaskChar) {
                            maskIndex = resetPos - offset;
                        }
                        if (lastMaskChar === resetPos) {
                            maskIndex -= offset;
                        }
                    }
                    maskIndex += offset;
                }
                else if (valDigit === lastUntranslatedMaskChar) {
                    lastUntranslatedMaskChar = undefined;
                }
                else if (translation.optional) {
                    maskIndex += offset;
                    valueIndex -= offset;
                }
                else {
                    this.invalidCharacters.push({
                        index: valueIndex,
                        digit: valDigit,
                        translated: translation.pattern
                    });
                }
                valueIndex += offset;
            }
            else {
                if (!skipMaskChars) {
                    buf[addMethod](maskDigit);
                }
                if (valDigit === maskDigit) {
                    valueIndex += offset;
                }
                else {
                    lastUntranslatedMaskChar = maskDigit;
                }
                maskIndex += offset;
            }
            check = this.isToCheck(isReversed, maskIndex, maskLen, valueIndex, valueLength);
        }
        /** @type {?} */
        var lastMaskCharDigit = mask.charAt(lastMaskChar);
        if (maskLen === valueLength + 1 && !this.translationMask[lastMaskCharDigit]) {
            buf.push(lastMaskCharDigit);
        }
        return buf.join('');
    };
    /**
     * @private
     * @param {?} isReversed
     * @param {?} maskIndex
     * @param {?} maskLen
     * @param {?} valueIndex
     * @param {?} valueLength
     * @return {?}
     */
    InputMaskDirective.prototype.isToCheck = /**
     * @private
     * @param {?} isReversed
     * @param {?} maskIndex
     * @param {?} maskLen
     * @param {?} valueIndex
     * @param {?} valueLength
     * @return {?}
     */
    function (isReversed, maskIndex, maskLen, valueIndex, valueLength) {
        /** @type {?} */
        var check = false;
        if (isReversed) {
            check = (maskIndex > -1) && (valueIndex > -1);
        }
        else {
            check = (maskIndex < maskLen) && (valueIndex < valueLength);
        }
        return check;
    };
    /**
     * @private
     * @param {?} value
     * @return {?}
     */
    InputMaskDirective.prototype.setValue = /**
     * @private
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.value = value;
    };
    /**
     * @private
     * @return {?}
     */
    InputMaskDirective.prototype.getValue = /**
     * @private
     * @return {?}
     */
    function () {
        return this.value;
    };
    InputMaskDirective.decorators = [
        { type: Directive, args: [{
                    selector: '[adf-text-mask], [textMask]',
                    providers: [CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR]
                },] }
    ];
    /** @nocollapse */
    InputMaskDirective.ctorParameters = function () { return [
        { type: ElementRef },
        { type: Renderer2 }
    ]; };
    InputMaskDirective.propDecorators = {
        inputMask: [{ type: Input, args: ['textMask',] }],
        onTextInput: [{ type: HostListener, args: ['input', ['$event'],] }, { type: HostListener, args: ['keyup', ['$event'],] }]
    };
    return InputMaskDirective;
}());
export { InputMaskDirective };
if (false) {
    /**
     * Object defining mask and "reversed" status.
     * @type {?}
     */
    InputMaskDirective.prototype.inputMask;
    /**
     * @type {?}
     * @private
     */
    InputMaskDirective.prototype.translationMask;
    /**
     * @type {?}
     * @private
     */
    InputMaskDirective.prototype.byPassKeys;
    /**
     * @type {?}
     * @private
     */
    InputMaskDirective.prototype.value;
    /**
     * @type {?}
     * @private
     */
    InputMaskDirective.prototype.invalidCharacters;
    /** @type {?} */
    InputMaskDirective.prototype._onChange;
    /** @type {?} */
    InputMaskDirective.prototype._onTouched;
    /**
     * @type {?}
     * @private
     */
    InputMaskDirective.prototype.el;
    /**
     * @type {?}
     * @private
     */
    InputMaskDirective.prototype.render;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGV4dC1tYXNrLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL3RleHQvdGV4dC1tYXNrLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUNILFNBQVMsRUFDVCxVQUFVLEVBQ1YsVUFBVSxFQUNWLFlBQVksRUFDWixLQUFLLEVBRUwsU0FBUyxFQUVaLE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBd0IsaUJBQWlCLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7QUFFekUsTUFBTSxLQUFPLG1DQUFtQyxHQUFRO0lBQ3BELE9BQU8sRUFBRSxpQkFBaUI7SUFDMUIsV0FBVyxFQUFFLFVBQVU7OztJQUFDLGNBQU0sT0FBQSxrQkFBa0IsRUFBbEIsQ0FBa0IsRUFBQztJQUNqRCxLQUFLLEVBQUUsSUFBSTtDQUNkOzs7O0FBS0Q7SUF3QkksNEJBQW9CLEVBQWMsRUFBVSxNQUFpQjtRQUF6QyxPQUFFLEdBQUYsRUFBRSxDQUFZO1FBQVUsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQVpyRCxvQkFBZSxHQUFHO1lBQ3RCLEdBQUcsRUFBRSxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUU7WUFDdEIsR0FBRyxFQUFFLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsSUFBSSxFQUFFO1lBQ3RDLEdBQUcsRUFBRSxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsU0FBUyxFQUFFLElBQUksRUFBRTtZQUN2QyxHQUFHLEVBQUUsRUFBRSxPQUFPLEVBQUUsYUFBYSxFQUFFO1lBQy9CLEdBQUcsRUFBRSxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUU7U0FDL0IsQ0FBQztRQUVNLGVBQVUsR0FBRyxDQUFDLENBQUMsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsQ0FBQyxDQUFDO1FBRXJELHNCQUFpQixHQUFHLEVBQUUsQ0FBQztRQUsvQixjQUFTOzs7O1FBQUcsVUFBQyxDQUFNO1FBQ25CLENBQUMsRUFBQTtRQUVELGVBQVU7OztRQUFHO1FBQ2IsQ0FBQyxFQUFBO0lBTkQsQ0FBQzs7Ozs7SUFTa0Msd0NBQVc7Ozs7SUFEOUMsVUFDK0MsS0FBb0I7UUFDL0QsSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxFQUFFO1lBQ3ZDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGNBQWMsRUFDNUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ3RFO2FBQU07WUFDSCxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssQ0FBQyxDQUFDO1NBQy9DO0lBQ0wsQ0FBQzs7Ozs7SUFFRCx3Q0FBVzs7OztJQUFYLFVBQVksT0FBc0I7UUFDOUIsSUFBSSxPQUFPLENBQUMsV0FBVyxDQUFDLElBQUksT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsRUFBRTtZQUNuRSxJQUFJLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxXQUFXLENBQUMsQ0FBQyxZQUFZLENBQUM7U0FDdEQ7SUFDTCxDQUFDOzs7OztJQUVELHVDQUFVOzs7O0lBQVYsVUFBVyxLQUFVO1FBQ2pCLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDeEMsQ0FBQzs7Ozs7SUFFRCw2Q0FBZ0I7Ozs7SUFBaEIsVUFBaUIsRUFBTztRQUNwQixJQUFJLENBQUMsU0FBUyxHQUFHLEVBQUUsQ0FBQztJQUN4QixDQUFDOzs7OztJQUVELDhDQUFpQjs7OztJQUFqQixVQUFrQixFQUFhO1FBQzNCLElBQUksQ0FBQyxVQUFVLEdBQUcsRUFBRSxDQUFDO0lBQ3pCLENBQUM7Ozs7Ozs7Ozs7SUFFTyxzQ0FBUzs7Ozs7Ozs7O0lBQWpCLFVBQWtCLFdBQVcsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLGNBQWMsRUFBRSxPQUFPO1FBQzNFLElBQUksSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7O2dCQUNuQyxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxjQUFjLENBQUM7O2dCQUN2RSxlQUFlLEdBQUcsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFVBQVUsRUFBRSxXQUFXLEVBQUUsT0FBTyxDQUFDO1lBQ3JGLElBQUksQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxFQUFFLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztZQUNoRSxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEdBQUcsS0FBSyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7WUFDckIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUN0QixJQUFJLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDMUM7SUFDTCxDQUFDOzs7Ozs7SUFFTyw2Q0FBZ0I7Ozs7O0lBQXhCLFVBQXlCLGFBQWE7UUFDbEMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxhQUFhLENBQUMsU0FBUyxHQUFHLGFBQWEsQ0FBQztRQUNoRCxJQUFJLENBQUMsRUFBRSxDQUFDLGFBQWEsQ0FBQyxPQUFPLEdBQUcsYUFBYSxDQUFDO0lBQ2xELENBQUM7Ozs7Ozs7SUFFRCxtREFBc0I7Ozs7OztJQUF0QixVQUF1QixhQUFhLEVBQUUsUUFBUSxFQUFFLE9BQU87O1lBQzdDLGNBQWMsR0FBRyxRQUFRLENBQUMsTUFBTTs7WUFDaEMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLEVBQUUsSUFBSSxFQUFFOztZQUNoQyxjQUFjLEdBQUcsUUFBUSxDQUFDLE1BQU07UUFFdEMsSUFBSSxPQUFPLEtBQUssQ0FBQyxJQUFJLFFBQVEsS0FBSyxRQUFRLEVBQUU7WUFDeEMsYUFBYSxHQUFHLGFBQWEsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLGFBQWEsQ0FBQyxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxhQUFhLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUN2SDthQUFNLElBQUksUUFBUSxLQUFLLFFBQVEsRUFBRTtZQUM5QixJQUFJLGFBQWEsSUFBSSxjQUFjLEVBQUU7Z0JBQ2pDLGFBQWEsR0FBRyxjQUFjLENBQUM7YUFDbEM7aUJBQU07Z0JBQ0gsYUFBYSxHQUFHLGFBQWEsR0FBRyxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUFFLGFBQWEsQ0FBQyxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRSxhQUFhLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUN2SDtTQUNKO1FBQ0QsT0FBTyxhQUFhLENBQUM7SUFDekIsQ0FBQzs7Ozs7Ozs7SUFFRCxzQ0FBUzs7Ozs7OztJQUFULFVBQVUsYUFBYSxFQUFFLEdBQUcsRUFBRSxJQUFJLEVBQUUsVUFBa0I7UUFBbEIsMkJBQUEsRUFBQSxrQkFBa0I7O1lBQzVDLEdBQUcsR0FBRyxFQUFFOztZQUNSLEtBQUssR0FBRyxHQUFHOztZQUNiLFNBQVMsR0FBRyxDQUFDOztZQUNYLE9BQU8sR0FBRyxJQUFJLENBQUMsTUFBTTs7WUFDdkIsVUFBVSxHQUFHLENBQUM7O1lBQ1osV0FBVyxHQUFHLEtBQUssQ0FBQyxNQUFNOztZQUM1QixNQUFNLEdBQUcsQ0FBQzs7WUFDVixTQUFTLEdBQUcsTUFBTTs7WUFDbEIsUUFBUSxHQUFHLENBQUMsQ0FBQzs7WUFDYixZQUFZOztZQUNaLHdCQUF3Qjs7WUFDeEIsS0FBSztRQUVULElBQUksVUFBVSxFQUFFO1lBQ1osU0FBUyxHQUFHLFNBQVMsQ0FBQztZQUN0QixNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUM7WUFDWixZQUFZLEdBQUcsQ0FBQyxDQUFDO1lBQ2pCLFNBQVMsR0FBRyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1lBQ3hCLFVBQVUsR0FBRyxXQUFXLEdBQUcsQ0FBQyxDQUFDO1NBQ2hDO2FBQU07WUFDSCxZQUFZLEdBQUcsT0FBTyxHQUFHLENBQUMsQ0FBQztTQUM5QjtRQUNELEtBQUssR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFVBQVUsRUFBRSxTQUFTLEVBQUUsT0FBTyxFQUFFLFVBQVUsRUFBRSxXQUFXLENBQUMsQ0FBQztRQUNoRixPQUFPLEtBQUssRUFBRTs7Z0JBQ0osU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxDQUFDOztnQkFDcEMsUUFBUSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUMsVUFBVSxDQUFDOztnQkFDbkMsV0FBVyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsU0FBUyxDQUFDO1lBRWpELElBQUksV0FBVyxFQUFFO2dCQUNiLElBQUksUUFBUSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUU7b0JBQ3JDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxRQUFRLENBQUMsQ0FBQztvQkFDekIsSUFBSSxXQUFXLENBQUMsU0FBUyxFQUFFO3dCQUN2QixJQUFJLFFBQVEsS0FBSyxDQUFDLENBQUMsRUFBRTs0QkFDakIsUUFBUSxHQUFHLFNBQVMsQ0FBQzt5QkFDeEI7NkJBQU0sSUFBSSxTQUFTLEtBQUssWUFBWSxFQUFFOzRCQUNuQyxTQUFTLEdBQUcsUUFBUSxHQUFHLE1BQU0sQ0FBQzt5QkFDakM7d0JBQ0QsSUFBSSxZQUFZLEtBQUssUUFBUSxFQUFFOzRCQUMzQixTQUFTLElBQUksTUFBTSxDQUFDO3lCQUN2QjtxQkFDSjtvQkFDRCxTQUFTLElBQUksTUFBTSxDQUFDO2lCQUN2QjtxQkFBTSxJQUFJLFFBQVEsS0FBSyx3QkFBd0IsRUFBRTtvQkFDOUMsd0JBQXdCLEdBQUcsU0FBUyxDQUFDO2lCQUN4QztxQkFBTSxJQUFJLFdBQVcsQ0FBQyxRQUFRLEVBQUU7b0JBQzdCLFNBQVMsSUFBSSxNQUFNLENBQUM7b0JBQ3BCLFVBQVUsSUFBSSxNQUFNLENBQUM7aUJBQ3hCO3FCQUFNO29CQUNILElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUM7d0JBQ3hCLEtBQUssRUFBRSxVQUFVO3dCQUNqQixLQUFLLEVBQUUsUUFBUTt3QkFDZixVQUFVLEVBQUUsV0FBVyxDQUFDLE9BQU87cUJBQ2xDLENBQUMsQ0FBQztpQkFDTjtnQkFDRCxVQUFVLElBQUksTUFBTSxDQUFDO2FBQ3hCO2lCQUFNO2dCQUNILElBQUksQ0FBQyxhQUFhLEVBQUU7b0JBQ2hCLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQyxTQUFTLENBQUMsQ0FBQztpQkFDN0I7Z0JBQ0QsSUFBSSxRQUFRLEtBQUssU0FBUyxFQUFFO29CQUN4QixVQUFVLElBQUksTUFBTSxDQUFDO2lCQUN4QjtxQkFBTTtvQkFDSCx3QkFBd0IsR0FBRyxTQUFTLENBQUM7aUJBQ3hDO2dCQUNELFNBQVMsSUFBSSxNQUFNLENBQUM7YUFDdkI7WUFDRCxLQUFLLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLEVBQUUsU0FBUyxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsV0FBVyxDQUFDLENBQUM7U0FDbkY7O1lBRUssaUJBQWlCLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUM7UUFDbkQsSUFBSSxPQUFPLEtBQUssV0FBVyxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsaUJBQWlCLENBQUMsRUFBRTtZQUN6RSxHQUFHLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUM7U0FDL0I7UUFFRCxPQUFPLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDeEIsQ0FBQzs7Ozs7Ozs7OztJQUVPLHNDQUFTOzs7Ozs7Ozs7SUFBakIsVUFBa0IsVUFBVSxFQUFFLFNBQVMsRUFBRSxPQUFPLEVBQUUsVUFBVSxFQUFFLFdBQVc7O1lBQ2pFLEtBQUssR0FBRyxLQUFLO1FBQ2pCLElBQUksVUFBVSxFQUFFO1lBQ1osS0FBSyxHQUFHLENBQUMsU0FBUyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNqRDthQUFNO1lBQ0gsS0FBSyxHQUFHLENBQUMsU0FBUyxHQUFHLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFHLFdBQVcsQ0FBQyxDQUFDO1NBQy9EO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7Ozs7O0lBRU8scUNBQVE7Ozs7O0lBQWhCLFVBQWlCLEtBQUs7UUFDbEIsSUFBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7SUFDdkIsQ0FBQzs7Ozs7SUFFTyxxQ0FBUTs7OztJQUFoQjtRQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDOztnQkE3TEosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSw2QkFBNkI7b0JBQ3ZDLFNBQVMsRUFBRSxDQUFDLG1DQUFtQyxDQUFDO2lCQUNuRDs7OztnQkF0QkcsVUFBVTtnQkFLVixTQUFTOzs7NEJBcUJSLEtBQUssU0FBQyxVQUFVOzhCQTBCaEIsWUFBWSxTQUFDLE9BQU8sRUFBRSxDQUFDLFFBQVEsQ0FBQyxjQUNoQyxZQUFZLFNBQUMsT0FBTyxFQUFFLENBQUMsUUFBUSxDQUFDOztJQTRKckMseUJBQUM7Q0FBQSxBQTlMRCxJQThMQztTQTFMWSxrQkFBa0I7Ozs7OztJQUczQix1Q0FHRTs7Ozs7SUFFRiw2Q0FNRTs7Ozs7SUFFRix3Q0FBNkQ7Ozs7O0lBQzdELG1DQUFjOzs7OztJQUNkLCtDQUErQjs7SUFLL0IsdUNBQ0M7O0lBRUQsd0NBQ0M7Ozs7O0lBUFcsZ0NBQXNCOzs7OztJQUFFLG9DQUF5QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4gLyogdHNsaW50OmRpc2FibGU6IGNvbXBvbmVudC1zZWxlY3RvciBuby11c2UtYmVmb3JlLWRlY2xhcmUgbm8taW5wdXQtcmVuYW1lICAqL1xyXG5cclxuaW1wb3J0IHtcclxuICAgIERpcmVjdGl2ZSxcclxuICAgIEVsZW1lbnRSZWYsXHJcbiAgICBmb3J3YXJkUmVmLFxyXG4gICAgSG9zdExpc3RlbmVyLFxyXG4gICAgSW5wdXQsXHJcbiAgICBPbkNoYW5nZXMsXHJcbiAgICBSZW5kZXJlcjIsXHJcbiAgICBTaW1wbGVDaGFuZ2VzXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbnRyb2xWYWx1ZUFjY2Vzc29yLCBOR19WQUxVRV9BQ0NFU1NPUiB9IGZyb20gJ0Bhbmd1bGFyL2Zvcm1zJztcclxuXHJcbmV4cG9ydCBjb25zdCBDVVNUT01fSU5QVVRfQ09OVFJPTF9WQUxVRV9BQ0NFU1NPUjogYW55ID0ge1xyXG4gICAgcHJvdmlkZTogTkdfVkFMVUVfQUNDRVNTT1IsXHJcbiAgICB1c2VFeGlzdGluZzogZm9yd2FyZFJlZigoKSA9PiBJbnB1dE1hc2tEaXJlY3RpdmUpLFxyXG4gICAgbXVsdGk6IHRydWVcclxufTtcclxuXHJcbi8qKlxyXG4gKiBEaXJlY3RpdmUgc2VsZWN0b3JzIHdpdGhvdXQgYWRmLSBwcmVmaXggd2lsbCBiZSBkZXByZWNhdGVkIG9uIDMuMC4wXHJcbiAqL1xyXG5ARGlyZWN0aXZlKHtcclxuICAgIHNlbGVjdG9yOiAnW2FkZi10ZXh0LW1hc2tdLCBbdGV4dE1hc2tdJyxcclxuICAgIHByb3ZpZGVyczogW0NVU1RPTV9JTlBVVF9DT05UUk9MX1ZBTFVFX0FDQ0VTU09SXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgSW5wdXRNYXNrRGlyZWN0aXZlIGltcGxlbWVudHMgT25DaGFuZ2VzLCBDb250cm9sVmFsdWVBY2Nlc3NvciB7XHJcblxyXG4gICAgLyoqIE9iamVjdCBkZWZpbmluZyBtYXNrIGFuZCBcInJldmVyc2VkXCIgc3RhdHVzLiAqL1xyXG4gICAgQElucHV0KCd0ZXh0TWFzaycpIGlucHV0TWFzazoge1xyXG4gICAgICAgIG1hc2s6ICcnLFxyXG4gICAgICAgIGlzUmV2ZXJzZWQ6IGZhbHNlXHJcbiAgICB9O1xyXG5cclxuICAgIHByaXZhdGUgdHJhbnNsYXRpb25NYXNrID0ge1xyXG4gICAgICAgICcwJzogeyBwYXR0ZXJuOiAvXFxkLyB9LFxyXG4gICAgICAgICc5JzogeyBwYXR0ZXJuOiAvXFxkLywgb3B0aW9uYWw6IHRydWUgfSxcclxuICAgICAgICAnIyc6IHsgcGF0dGVybjogL1xcZC8sIHJlY3Vyc2l2ZTogdHJ1ZSB9LFxyXG4gICAgICAgICdBJzogeyBwYXR0ZXJuOiAvW2EtekEtWjAtOV0vIH0sXHJcbiAgICAgICAgJ1MnOiB7IHBhdHRlcm46IC9bYS16QS1aXS8gfVxyXG4gICAgfTtcclxuXHJcbiAgICBwcml2YXRlIGJ5UGFzc0tleXMgPSBbOSwgMTYsIDE3LCAxOCwgMzYsIDM3LCAzOCwgMzksIDQwLCA5MV07XHJcbiAgICBwcml2YXRlIHZhbHVlO1xyXG4gICAgcHJpdmF0ZSBpbnZhbGlkQ2hhcmFjdGVycyA9IFtdO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgZWw6IEVsZW1lbnRSZWYsIHByaXZhdGUgcmVuZGVyOiBSZW5kZXJlcjIpIHtcclxuICAgIH1cclxuXHJcbiAgICBfb25DaGFuZ2UgPSAoXzogYW55KSA9PiB7XHJcbiAgICB9XHJcblxyXG4gICAgX29uVG91Y2hlZCA9ICgpID0+IHtcclxuICAgIH1cclxuXHJcbiAgICBASG9zdExpc3RlbmVyKCdpbnB1dCcsIFsnJGV2ZW50J10pXHJcbiAgICBASG9zdExpc3RlbmVyKCdrZXl1cCcsIFsnJGV2ZW50J10pIG9uVGV4dElucHV0KGV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaW5wdXRNYXNrICYmIHRoaXMuaW5wdXRNYXNrLm1hc2spIHtcclxuICAgICAgICAgICAgdGhpcy5tYXNrVmFsdWUodGhpcy5lbC5uYXRpdmVFbGVtZW50LnZhbHVlLCB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQuc2VsZWN0aW9uU3RhcnQsXHJcbiAgICAgICAgICAgICAgICB0aGlzLmlucHV0TWFzay5tYXNrLCB0aGlzLmlucHV0TWFzay5pc1JldmVyc2VkLCBldmVudC5rZXlDb2RlKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB0aGlzLl9vbkNoYW5nZSh0aGlzLmVsLm5hdGl2ZUVsZW1lbnQudmFsdWUpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKSB7XHJcbiAgICAgICAgaWYgKGNoYW5nZXNbJ2lucHV0TWFzayddICYmIGNoYW5nZXNbJ2lucHV0TWFzayddLmN1cnJlbnRWYWx1ZVsnbWFzayddKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaW5wdXRNYXNrID0gY2hhbmdlc1snaW5wdXRNYXNrJ10uY3VycmVudFZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB3cml0ZVZhbHVlKHZhbHVlOiBhbnkpIHtcclxuICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQudmFsdWUgPSB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICByZWdpc3Rlck9uQ2hhbmdlKGZuOiBhbnkpIHtcclxuICAgICAgICB0aGlzLl9vbkNoYW5nZSA9IGZuO1xyXG4gICAgfVxyXG5cclxuICAgIHJlZ2lzdGVyT25Ub3VjaGVkKGZuOiAoKSA9PiBhbnkpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLl9vblRvdWNoZWQgPSBmbjtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG1hc2tWYWx1ZShhY3R1YWxWYWx1ZSwgc3RhcnRDYXJldCwgbWFza1RvQXBwbHksIGlzTWFza1JldmVyc2VkLCBrZXlDb2RlKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuYnlQYXNzS2V5cy5pbmRleE9mKGtleUNvZGUpID09PSAtMSkge1xyXG4gICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHRoaXMuZ2V0TWFza2VkKGZhbHNlLCBhY3R1YWxWYWx1ZSwgbWFza1RvQXBwbHksIGlzTWFza1JldmVyc2VkKTtcclxuICAgICAgICAgICAgY29uc3QgY2FsY3VsYXRlZENhcmV0ID0gdGhpcy5jYWxjdWxhdGVDYXJldFBvc2l0aW9uKHN0YXJ0Q2FyZXQsIGFjdHVhbFZhbHVlLCBrZXlDb2RlKTtcclxuICAgICAgICAgICAgdGhpcy5yZW5kZXIuc2V0QXR0cmlidXRlKHRoaXMuZWwubmF0aXZlRWxlbWVudCwgJ3ZhbHVlJywgdmFsdWUpO1xyXG4gICAgICAgICAgICB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQudmFsdWUgPSB2YWx1ZTtcclxuICAgICAgICAgICAgdGhpcy5zZXRWYWx1ZSh2YWx1ZSk7XHJcbiAgICAgICAgICAgIHRoaXMuX29uQ2hhbmdlKHZhbHVlKTtcclxuICAgICAgICAgICAgdGhpcy5zZXRDYXJldFBvc2l0aW9uKGNhbGN1bGF0ZWRDYXJldCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgc2V0Q2FyZXRQb3NpdGlvbihjYXJldFBvc2l0aW9uKSB7XHJcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50Lm1vdmVTdGFydCA9IGNhcmV0UG9zaXRpb247XHJcbiAgICAgICAgdGhpcy5lbC5uYXRpdmVFbGVtZW50Lm1vdmVFbmQgPSBjYXJldFBvc2l0aW9uO1xyXG4gICAgfVxyXG5cclxuICAgIGNhbGN1bGF0ZUNhcmV0UG9zaXRpb24oY2FyZXRQb3NpdGlvbiwgbmV3VmFsdWUsIGtleUNvZGUpIHtcclxuICAgICAgICBjb25zdCBuZXdWYWx1ZUxlbmd0aCA9IG5ld1ZhbHVlLmxlbmd0aDtcclxuICAgICAgICBjb25zdCBvbGRWYWx1ZSA9IHRoaXMuZ2V0VmFsdWUoKSB8fCAnJztcclxuICAgICAgICBjb25zdCBvbGRWYWx1ZUxlbmd0aCA9IG9sZFZhbHVlLmxlbmd0aDtcclxuXHJcbiAgICAgICAgaWYgKGtleUNvZGUgPT09IDggJiYgb2xkVmFsdWUgIT09IG5ld1ZhbHVlKSB7XHJcbiAgICAgICAgICAgIGNhcmV0UG9zaXRpb24gPSBjYXJldFBvc2l0aW9uIC0gKG5ld1ZhbHVlLnNsaWNlKDAsIGNhcmV0UG9zaXRpb24pLmxlbmd0aCAtIG9sZFZhbHVlLnNsaWNlKDAsIGNhcmV0UG9zaXRpb24pLmxlbmd0aCk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChvbGRWYWx1ZSAhPT0gbmV3VmFsdWUpIHtcclxuICAgICAgICAgICAgaWYgKGNhcmV0UG9zaXRpb24gPj0gb2xkVmFsdWVMZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgIGNhcmV0UG9zaXRpb24gPSBuZXdWYWx1ZUxlbmd0aDtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGNhcmV0UG9zaXRpb24gPSBjYXJldFBvc2l0aW9uICsgKG5ld1ZhbHVlLnNsaWNlKDAsIGNhcmV0UG9zaXRpb24pLmxlbmd0aCAtIG9sZFZhbHVlLnNsaWNlKDAsIGNhcmV0UG9zaXRpb24pLmxlbmd0aCk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGNhcmV0UG9zaXRpb247XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0TWFza2VkKHNraXBNYXNrQ2hhcnMsIHZhbCwgbWFzaywgaXNSZXZlcnNlZCA9IGZhbHNlKSB7XHJcbiAgICAgICAgY29uc3QgYnVmID0gW107XHJcbiAgICAgICAgY29uc3QgdmFsdWUgPSB2YWw7XHJcbiAgICAgICAgbGV0IG1hc2tJbmRleCA9IDA7XHJcbiAgICAgICAgY29uc3QgbWFza0xlbiA9IG1hc2subGVuZ3RoO1xyXG4gICAgICAgIGxldCB2YWx1ZUluZGV4ID0gMDtcclxuICAgICAgICBjb25zdCB2YWx1ZUxlbmd0aCA9IHZhbHVlLmxlbmd0aDtcclxuICAgICAgICBsZXQgb2Zmc2V0ID0gMTtcclxuICAgICAgICBsZXQgYWRkTWV0aG9kID0gJ3B1c2gnO1xyXG4gICAgICAgIGxldCByZXNldFBvcyA9IC0xO1xyXG4gICAgICAgIGxldCBsYXN0TWFza0NoYXI7XHJcbiAgICAgICAgbGV0IGxhc3RVbnRyYW5zbGF0ZWRNYXNrQ2hhcjtcclxuICAgICAgICBsZXQgY2hlY2s7XHJcblxyXG4gICAgICAgIGlmIChpc1JldmVyc2VkKSB7XHJcbiAgICAgICAgICAgIGFkZE1ldGhvZCA9ICd1bnNoaWZ0JztcclxuICAgICAgICAgICAgb2Zmc2V0ID0gLTE7XHJcbiAgICAgICAgICAgIGxhc3RNYXNrQ2hhciA9IDA7XHJcbiAgICAgICAgICAgIG1hc2tJbmRleCA9IG1hc2tMZW4gLSAxO1xyXG4gICAgICAgICAgICB2YWx1ZUluZGV4ID0gdmFsdWVMZW5ndGggLSAxO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGxhc3RNYXNrQ2hhciA9IG1hc2tMZW4gLSAxO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjaGVjayA9IHRoaXMuaXNUb0NoZWNrKGlzUmV2ZXJzZWQsIG1hc2tJbmRleCwgbWFza0xlbiwgdmFsdWVJbmRleCwgdmFsdWVMZW5ndGgpO1xyXG4gICAgICAgIHdoaWxlIChjaGVjaykge1xyXG4gICAgICAgICAgICBjb25zdCBtYXNrRGlnaXQgPSBtYXNrLmNoYXJBdChtYXNrSW5kZXgpLFxyXG4gICAgICAgICAgICAgICAgdmFsRGlnaXQgPSB2YWx1ZS5jaGFyQXQodmFsdWVJbmRleCksXHJcbiAgICAgICAgICAgICAgICB0cmFuc2xhdGlvbiA9IHRoaXMudHJhbnNsYXRpb25NYXNrW21hc2tEaWdpdF07XHJcblxyXG4gICAgICAgICAgICBpZiAodHJhbnNsYXRpb24pIHtcclxuICAgICAgICAgICAgICAgIGlmICh2YWxEaWdpdC5tYXRjaCh0cmFuc2xhdGlvbi5wYXR0ZXJuKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGJ1ZlthZGRNZXRob2RdKHZhbERpZ2l0KTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodHJhbnNsYXRpb24ucmVjdXJzaXZlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyZXNldFBvcyA9PT0gLTEpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlc2V0UG9zID0gbWFza0luZGV4O1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKG1hc2tJbmRleCA9PT0gbGFzdE1hc2tDaGFyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXNrSW5kZXggPSByZXNldFBvcyAtIG9mZnNldDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAobGFzdE1hc2tDaGFyID09PSByZXNldFBvcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWFza0luZGV4IC09IG9mZnNldDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICBtYXNrSW5kZXggKz0gb2Zmc2V0O1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICh2YWxEaWdpdCA9PT0gbGFzdFVudHJhbnNsYXRlZE1hc2tDaGFyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGFzdFVudHJhbnNsYXRlZE1hc2tDaGFyID0gdW5kZWZpbmVkO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0cmFuc2xhdGlvbi5vcHRpb25hbCkge1xyXG4gICAgICAgICAgICAgICAgICAgIG1hc2tJbmRleCArPSBvZmZzZXQ7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWVJbmRleCAtPSBvZmZzZXQ7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaW52YWxpZENoYXJhY3RlcnMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGluZGV4OiB2YWx1ZUluZGV4LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaWdpdDogdmFsRGlnaXQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zbGF0ZWQ6IHRyYW5zbGF0aW9uLnBhdHRlcm5cclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHZhbHVlSW5kZXggKz0gb2Zmc2V0O1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaWYgKCFza2lwTWFza0NoYXJzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgYnVmW2FkZE1ldGhvZF0obWFza0RpZ2l0KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGlmICh2YWxEaWdpdCA9PT0gbWFza0RpZ2l0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWVJbmRleCArPSBvZmZzZXQ7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGxhc3RVbnRyYW5zbGF0ZWRNYXNrQ2hhciA9IG1hc2tEaWdpdDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIG1hc2tJbmRleCArPSBvZmZzZXQ7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2hlY2sgPSB0aGlzLmlzVG9DaGVjayhpc1JldmVyc2VkLCBtYXNrSW5kZXgsIG1hc2tMZW4sIHZhbHVlSW5kZXgsIHZhbHVlTGVuZ3RoKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGxhc3RNYXNrQ2hhckRpZ2l0ID0gbWFzay5jaGFyQXQobGFzdE1hc2tDaGFyKTtcclxuICAgICAgICBpZiAobWFza0xlbiA9PT0gdmFsdWVMZW5ndGggKyAxICYmICF0aGlzLnRyYW5zbGF0aW9uTWFza1tsYXN0TWFza0NoYXJEaWdpdF0pIHtcclxuICAgICAgICAgICAgYnVmLnB1c2gobGFzdE1hc2tDaGFyRGlnaXQpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGJ1Zi5qb2luKCcnKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGlzVG9DaGVjayhpc1JldmVyc2VkLCBtYXNrSW5kZXgsIG1hc2tMZW4sIHZhbHVlSW5kZXgsIHZhbHVlTGVuZ3RoKSB7XHJcbiAgICAgICAgbGV0IGNoZWNrID0gZmFsc2U7XHJcbiAgICAgICAgaWYgKGlzUmV2ZXJzZWQpIHtcclxuICAgICAgICAgICAgY2hlY2sgPSAobWFza0luZGV4ID4gLTEpICYmICh2YWx1ZUluZGV4ID4gLTEpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNoZWNrID0gKG1hc2tJbmRleCA8IG1hc2tMZW4pICYmICh2YWx1ZUluZGV4IDwgdmFsdWVMZW5ndGgpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gY2hlY2s7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBzZXRWYWx1ZSh2YWx1ZSkge1xyXG4gICAgICAgIHRoaXMudmFsdWUgPSB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldFZhbHVlKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnZhbHVlO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==