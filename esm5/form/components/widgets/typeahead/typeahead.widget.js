/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { LogService } from '../../../../services/log.service';
import { ENTER, ESCAPE } from '@angular/cdk/keycodes';
import { Component, ViewEncapsulation } from '@angular/core';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
var TypeaheadWidgetComponent = /** @class */ (function (_super) {
    tslib_1.__extends(TypeaheadWidgetComponent, _super);
    function TypeaheadWidgetComponent(formService, logService) {
        var _this = _super.call(this, formService) || this;
        _this.formService = formService;
        _this.logService = logService;
        _this.minTermLength = 1;
        _this.options = [];
        return _this;
    }
    /**
     * @return {?}
     */
    TypeaheadWidgetComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.field.form.taskId && this.field.restUrl) {
            this.getValuesByTaskId();
        }
        else if (this.field.form.processDefinitionId && this.field.restUrl) {
            this.getValuesByProcessDefinitionId();
        }
        if (this.isReadOnlyType()) {
            this.value = this.field.value;
        }
    };
    /**
     * @return {?}
     */
    TypeaheadWidgetComponent.prototype.getValuesByTaskId = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.formService
            .getRestFieldValues(this.field.form.taskId, this.field.id)
            .subscribe((/**
         * @param {?} formFieldOption
         * @return {?}
         */
        function (formFieldOption) {
            /** @type {?} */
            var options = formFieldOption || [];
            _this.field.options = options;
            /** @type {?} */
            var fieldValue = _this.field.value;
            if (fieldValue) {
                /** @type {?} */
                var toSelect = options.find((/**
                 * @param {?} item
                 * @return {?}
                 */
                function (item) { return item.id === fieldValue || item.name.toLocaleLowerCase() === fieldValue.toLocaleLowerCase(); }));
                if (toSelect) {
                    _this.value = toSelect.name;
                }
            }
            _this.onFieldChanged(_this.field);
            _this.field.updateForm();
        }), (/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); }));
    };
    /**
     * @return {?}
     */
    TypeaheadWidgetComponent.prototype.getValuesByProcessDefinitionId = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.formService
            .getRestFieldValuesByProcessId(this.field.form.processDefinitionId, this.field.id)
            .subscribe((/**
         * @param {?} formFieldOption
         * @return {?}
         */
        function (formFieldOption) {
            /** @type {?} */
            var options = formFieldOption || [];
            _this.field.options = options;
            /** @type {?} */
            var fieldValue = _this.field.value;
            if (fieldValue) {
                /** @type {?} */
                var toSelect = options.find((/**
                 * @param {?} item
                 * @return {?}
                 */
                function (item) { return item.id === fieldValue; }));
                if (toSelect) {
                    _this.value = toSelect.name;
                }
            }
            _this.onFieldChanged(_this.field);
            _this.field.updateForm();
        }), (/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); }));
    };
    /**
     * @return {?}
     */
    TypeaheadWidgetComponent.prototype.getOptions = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var val = this.value.trim().toLocaleLowerCase();
        return this.field.options.filter((/**
         * @param {?} item
         * @return {?}
         */
        function (item) {
            /** @type {?} */
            var name = item.name.toLocaleLowerCase();
            return name.indexOf(val) > -1;
        }));
    };
    /**
     * @param {?} optionName
     * @return {?}
     */
    TypeaheadWidgetComponent.prototype.isValidOptionName = /**
     * @param {?} optionName
     * @return {?}
     */
    function (optionName) {
        /** @type {?} */
        var option = this.field.options.find((/**
         * @param {?} item
         * @return {?}
         */
        function (item) { return item.name && item.name.toLocaleLowerCase() === optionName.toLocaleLowerCase(); }));
        return option ? true : false;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    TypeaheadWidgetComponent.prototype.onKeyUp = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.value && this.value.trim().length >= this.minTermLength && this.oldValue !== this.value) {
            if (event.keyCode !== ESCAPE && event.keyCode !== ENTER) {
                if (this.value.length >= this.minTermLength) {
                    this.options = this.getOptions();
                    this.oldValue = this.value;
                    if (this.isValidOptionName(this.value)) {
                        this.field.value = this.options[0].id;
                    }
                }
            }
        }
        if (this.isValueDefined() && this.value.trim().length === 0) {
            this.oldValue = this.value;
            this.options = [];
        }
    };
    /**
     * @param {?} item
     * @return {?}
     */
    TypeaheadWidgetComponent.prototype.onItemSelect = /**
     * @param {?} item
     * @return {?}
     */
    function (item) {
        if (item) {
            this.field.value = item.id;
            this.value = item.name;
            this.onFieldChanged(this.field);
        }
    };
    /**
     * @return {?}
     */
    TypeaheadWidgetComponent.prototype.validate = /**
     * @return {?}
     */
    function () {
        this.field.value = this.value;
    };
    /**
     * @return {?}
     */
    TypeaheadWidgetComponent.prototype.isValueDefined = /**
     * @return {?}
     */
    function () {
        return this.value !== null && this.value !== undefined;
    };
    /**
     * @param {?} error
     * @return {?}
     */
    TypeaheadWidgetComponent.prototype.handleError = /**
     * @param {?} error
     * @return {?}
     */
    function (error) {
        this.logService.error(error);
    };
    /**
     * @return {?}
     */
    TypeaheadWidgetComponent.prototype.isReadOnlyType = /**
     * @return {?}
     */
    function () {
        return this.field.type === 'readonly' ? true : false;
    };
    TypeaheadWidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'typeahead-widget',
                    template: "<div class=\"adf-typeahead-widget-container\">\r\n    <div class=\"adf-typeahead-widget {{field.className}}\"\r\n        [class.is-dirty]=\"value\"\r\n        [class.adf-invalid]=\"!field.isValid\"\r\n        [class.adf-readonly]=\"field.readOnly\"\r\n        id=\"typehead-div\">\r\n        <mat-form-field>\r\n            <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}</label>\r\n            <input matInput class=\"adf-input\"\r\n                   type=\"text\"\r\n                   [id]=\"field.id\"\r\n                   [(ngModel)]=\"value\"\r\n                   (ngModelChange)=\"validate()\"\r\n                   (keyup)=\"onKeyUp($event)\"\r\n                   [disabled]=\"field.readOnly\"\r\n                   placeholder=\"{{field.placeholder}}\"\r\n                   [matAutocomplete]=\"auto\">\r\n            <mat-autocomplete #auto=\"matAutocomplete\" (optionSelected)=\"onItemSelect($event.option.value)\">\r\n                <mat-option *ngFor=\"let item of options\" [value]=\"item\">\r\n                    <span [id]=\"field.name+'_option_'+item.id\">{{item.name}}</span>\r\n                </mat-option>\r\n            </mat-autocomplete>\r\n        </mat-form-field>\r\n\r\n        <error-widget [error]=\"field.validationSummary\"></error-widget>\r\n        <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n    </div>\r\n</div>\r\n",
                    host: baseHost,
                    encapsulation: ViewEncapsulation.None,
                    styles: [".adf-typeahead-widget-container{position:relative;display:block}.adf-typeahead-widget{width:100%}"]
                }] }
    ];
    /** @nocollapse */
    TypeaheadWidgetComponent.ctorParameters = function () { return [
        { type: FormService },
        { type: LogService }
    ]; };
    return TypeaheadWidgetComponent;
}(WidgetComponent));
export { TypeaheadWidgetComponent };
if (false) {
    /** @type {?} */
    TypeaheadWidgetComponent.prototype.minTermLength;
    /** @type {?} */
    TypeaheadWidgetComponent.prototype.value;
    /** @type {?} */
    TypeaheadWidgetComponent.prototype.oldValue;
    /** @type {?} */
    TypeaheadWidgetComponent.prototype.options;
    /** @type {?} */
    TypeaheadWidgetComponent.prototype.formService;
    /**
     * @type {?}
     * @private
     */
    TypeaheadWidgetComponent.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHlwZWFoZWFkLndpZGdldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL3R5cGVhaGVhZC90eXBlYWhlYWQud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQzlELE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDdEQsT0FBTyxFQUFFLFNBQVMsRUFBVSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUNyRSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFFL0QsT0FBTyxFQUFFLFFBQVEsRUFBRSxlQUFlLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUVsRTtJQU84QyxvREFBZTtJQU96RCxrQ0FBbUIsV0FBd0IsRUFDdkIsVUFBc0I7UUFEMUMsWUFFSSxrQkFBTSxXQUFXLENBQUMsU0FDckI7UUFIa0IsaUJBQVcsR0FBWCxXQUFXLENBQWE7UUFDdkIsZ0JBQVUsR0FBVixVQUFVLENBQVk7UUFOMUMsbUJBQWEsR0FBVyxDQUFDLENBQUM7UUFHMUIsYUFBTyxHQUFzQixFQUFFLENBQUM7O0lBS2hDLENBQUM7Ozs7SUFFRCwyQ0FBUTs7O0lBQVI7UUFDSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sRUFBRTtZQUM5QyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztTQUM1QjthQUFNLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsbUJBQW1CLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEVBQUU7WUFDbEUsSUFBSSxDQUFDLDhCQUE4QixFQUFFLENBQUM7U0FDekM7UUFDRCxJQUFJLElBQUksQ0FBQyxjQUFjLEVBQUUsRUFBRTtZQUN2QixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1NBQ2pDO0lBQ0wsQ0FBQzs7OztJQUVELG9EQUFpQjs7O0lBQWpCO1FBQUEsaUJBdUJDO1FBdEJHLElBQUksQ0FBQyxXQUFXO2FBQ1gsa0JBQWtCLENBQ2YsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUN0QixJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FDaEI7YUFDQSxTQUFTOzs7O1FBQ04sVUFBQyxlQUFrQzs7Z0JBQ3pCLE9BQU8sR0FBRyxlQUFlLElBQUksRUFBRTtZQUNyQyxLQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7O2dCQUV2QixVQUFVLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxLQUFLO1lBQ25DLElBQUksVUFBVSxFQUFFOztvQkFDTixRQUFRLEdBQUcsT0FBTyxDQUFDLElBQUk7Ozs7Z0JBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLENBQUMsRUFBRSxLQUFLLFVBQVUsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLEtBQUssVUFBVSxDQUFDLGlCQUFpQixFQUFFLEVBQTFGLENBQTBGLEVBQUM7Z0JBQ25JLElBQUksUUFBUSxFQUFFO29CQUNWLEtBQUksQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztpQkFDOUI7YUFDSjtZQUNELEtBQUksQ0FBQyxjQUFjLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hDLEtBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDNUIsQ0FBQzs7OztRQUNELFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFDakMsQ0FBQztJQUNWLENBQUM7Ozs7SUFFRCxpRUFBOEI7OztJQUE5QjtRQUFBLGlCQXVCQztRQXRCRyxJQUFJLENBQUMsV0FBVzthQUNYLDZCQUE2QixDQUMxQixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxtQkFBbUIsRUFDbkMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQ2hCO2FBQ0EsU0FBUzs7OztRQUNOLFVBQUMsZUFBa0M7O2dCQUN6QixPQUFPLEdBQUcsZUFBZSxJQUFJLEVBQUU7WUFDckMsS0FBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDOztnQkFFdkIsVUFBVSxHQUFHLEtBQUksQ0FBQyxLQUFLLENBQUMsS0FBSztZQUNuQyxJQUFJLFVBQVUsRUFBRTs7b0JBQ04sUUFBUSxHQUFHLE9BQU8sQ0FBQyxJQUFJOzs7O2dCQUFDLFVBQUMsSUFBSSxJQUFLLE9BQUEsSUFBSSxDQUFDLEVBQUUsS0FBSyxVQUFVLEVBQXRCLENBQXNCLEVBQUM7Z0JBQy9ELElBQUksUUFBUSxFQUFFO29CQUNWLEtBQUksQ0FBQyxLQUFLLEdBQUcsUUFBUSxDQUFDLElBQUksQ0FBQztpQkFDOUI7YUFDSjtZQUNELEtBQUksQ0FBQyxjQUFjLENBQUMsS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2hDLEtBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUM7UUFDNUIsQ0FBQzs7OztRQUNELFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFDakMsQ0FBQztJQUNWLENBQUM7Ozs7SUFFRCw2Q0FBVTs7O0lBQVY7O1lBQ1UsR0FBRyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsaUJBQWlCLEVBQUU7UUFDakQsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNOzs7O1FBQUMsVUFBQyxJQUFJOztnQkFDNUIsSUFBSSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDMUMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ2xDLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFRCxvREFBaUI7Ozs7SUFBakIsVUFBa0IsVUFBa0I7O1lBQzFCLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxJQUFJLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsS0FBSyxVQUFVLENBQUMsaUJBQWlCLEVBQUUsRUFBN0UsQ0FBNkUsRUFBQztRQUMvSCxPQUFPLE1BQU0sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFRCwwQ0FBTzs7OztJQUFQLFVBQVEsS0FBb0I7UUFDeEIsSUFBSSxJQUFJLENBQUMsS0FBSyxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxhQUFhLElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQzlGLElBQUksS0FBSyxDQUFDLE9BQU8sS0FBSyxNQUFNLElBQUksS0FBSyxDQUFDLE9BQU8sS0FBSyxLQUFLLEVBQUU7Z0JBQ3JELElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRTtvQkFDekMsSUFBSSxDQUFDLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7b0JBQ2pDLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztvQkFDM0IsSUFBSSxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFO3dCQUNwQyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztxQkFDekM7aUJBQ0o7YUFDSjtTQUNKO1FBQ0QsSUFBSSxJQUFJLENBQUMsY0FBYyxFQUFFLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ3pELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztZQUMzQixJQUFJLENBQUMsT0FBTyxHQUFHLEVBQUUsQ0FBQztTQUNyQjtJQUNMLENBQUM7Ozs7O0lBRUQsK0NBQVk7Ozs7SUFBWixVQUFhLElBQXFCO1FBQzlCLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEVBQUUsQ0FBQztZQUMzQixJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7WUFDdkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDbkM7SUFDTCxDQUFDOzs7O0lBRUQsMkNBQVE7OztJQUFSO1FBQ0ksSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQztJQUNsQyxDQUFDOzs7O0lBRUQsaURBQWM7OztJQUFkO1FBQ0ksT0FBTyxJQUFJLENBQUMsS0FBSyxLQUFLLElBQUksSUFBSSxJQUFJLENBQUMsS0FBSyxLQUFLLFNBQVMsQ0FBQztJQUMzRCxDQUFDOzs7OztJQUVELDhDQUFXOzs7O0lBQVgsVUFBWSxLQUFVO1FBQ2xCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ2pDLENBQUM7Ozs7SUFFRCxpREFBYzs7O0lBQWQ7UUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDekQsQ0FBQzs7Z0JBcklKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsa0JBQWtCO29CQUM1QixxOENBQXNDO29CQUV0QyxJQUFJLEVBQUUsUUFBUTtvQkFDZCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7aUJBQ3hDOzs7O2dCQVZRLFdBQVc7Z0JBSFgsVUFBVTs7SUE4SW5CLCtCQUFDO0NBQUEsQUF2SUQsQ0FPOEMsZUFBZSxHQWdJNUQ7U0FoSVksd0JBQXdCOzs7SUFFakMsaURBQTBCOztJQUMxQix5Q0FBYzs7SUFDZCw0Q0FBaUI7O0lBQ2pCLDJDQUFnQzs7SUFFcEIsK0NBQStCOzs7OztJQUMvQiw4Q0FBOEIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4uLy4uLy4uLy4uL3NlcnZpY2VzL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRU5URVIsIEVTQ0FQRSB9IGZyb20gJ0Bhbmd1bGFyL2Nkay9rZXljb2Rlcyc7XHJcbmltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBGb3JtU2VydmljZSB9IGZyb20gJy4vLi4vLi4vLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRm9ybUZpZWxkT3B0aW9uIH0gZnJvbSAnLi8uLi9jb3JlL2Zvcm0tZmllbGQtb3B0aW9uJztcclxuaW1wb3J0IHsgYmFzZUhvc3QsIFdpZGdldENvbXBvbmVudCB9IGZyb20gJy4vLi4vd2lkZ2V0LmNvbXBvbmVudCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAndHlwZWFoZWFkLXdpZGdldCcsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vdHlwZWFoZWFkLndpZGdldC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL3R5cGVhaGVhZC53aWRnZXQuc2NzcyddLFxyXG4gICAgaG9zdDogYmFzZUhvc3QsXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBUeXBlYWhlYWRXaWRnZXRDb21wb25lbnQgZXh0ZW5kcyBXaWRnZXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIG1pblRlcm1MZW5ndGg6IG51bWJlciA9IDE7XHJcbiAgICB2YWx1ZTogc3RyaW5nO1xyXG4gICAgb2xkVmFsdWU6IHN0cmluZztcclxuICAgIG9wdGlvbnM6IEZvcm1GaWVsZE9wdGlvbltdID0gW107XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgbG9nU2VydmljZTogTG9nU2VydmljZSkge1xyXG4gICAgICAgIHN1cGVyKGZvcm1TZXJ2aWNlKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICBpZiAodGhpcy5maWVsZC5mb3JtLnRhc2tJZCAmJiB0aGlzLmZpZWxkLnJlc3RVcmwpIHtcclxuICAgICAgICAgICAgdGhpcy5nZXRWYWx1ZXNCeVRhc2tJZCgpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodGhpcy5maWVsZC5mb3JtLnByb2Nlc3NEZWZpbml0aW9uSWQgJiYgdGhpcy5maWVsZC5yZXN0VXJsKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0VmFsdWVzQnlQcm9jZXNzRGVmaW5pdGlvbklkKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGlmICh0aGlzLmlzUmVhZE9ubHlUeXBlKCkpIHtcclxuICAgICAgICAgICAgdGhpcy52YWx1ZSA9IHRoaXMuZmllbGQudmFsdWU7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldFZhbHVlc0J5VGFza0lkKCkge1xyXG4gICAgICAgIHRoaXMuZm9ybVNlcnZpY2VcclxuICAgICAgICAgICAgLmdldFJlc3RGaWVsZFZhbHVlcyhcclxuICAgICAgICAgICAgICAgIHRoaXMuZmllbGQuZm9ybS50YXNrSWQsXHJcbiAgICAgICAgICAgICAgICB0aGlzLmZpZWxkLmlkXHJcbiAgICAgICAgICAgIClcclxuICAgICAgICAgICAgLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIChmb3JtRmllbGRPcHRpb246IEZvcm1GaWVsZE9wdGlvbltdKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3Qgb3B0aW9ucyA9IGZvcm1GaWVsZE9wdGlvbiB8fCBbXTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmZpZWxkLm9wdGlvbnMgPSBvcHRpb25zO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWVsZFZhbHVlID0gdGhpcy5maWVsZC52YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoZmllbGRWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB0b1NlbGVjdCA9IG9wdGlvbnMuZmluZCgoaXRlbSkgPT4gaXRlbS5pZCA9PT0gZmllbGRWYWx1ZSB8fCBpdGVtLm5hbWUudG9Mb2NhbGVMb3dlckNhc2UoKSA9PT0gZmllbGRWYWx1ZS50b0xvY2FsZUxvd2VyQ2FzZSgpKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHRvU2VsZWN0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnZhbHVlID0gdG9TZWxlY3QubmFtZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9uRmllbGRDaGFuZ2VkKHRoaXMuZmllbGQpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZmllbGQudXBkYXRlRm9ybSgpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIGdldFZhbHVlc0J5UHJvY2Vzc0RlZmluaXRpb25JZCgpIHtcclxuICAgICAgICB0aGlzLmZvcm1TZXJ2aWNlXHJcbiAgICAgICAgICAgIC5nZXRSZXN0RmllbGRWYWx1ZXNCeVByb2Nlc3NJZChcclxuICAgICAgICAgICAgICAgIHRoaXMuZmllbGQuZm9ybS5wcm9jZXNzRGVmaW5pdGlvbklkLFxyXG4gICAgICAgICAgICAgICAgdGhpcy5maWVsZC5pZFxyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAoZm9ybUZpZWxkT3B0aW9uOiBGb3JtRmllbGRPcHRpb25bXSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IG9wdGlvbnMgPSBmb3JtRmllbGRPcHRpb24gfHwgW107XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5maWVsZC5vcHRpb25zID0gb3B0aW9ucztcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgZmllbGRWYWx1ZSA9IHRoaXMuZmllbGQudmFsdWU7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGZpZWxkVmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdG9TZWxlY3QgPSBvcHRpb25zLmZpbmQoKGl0ZW0pID0+IGl0ZW0uaWQgPT09IGZpZWxkVmFsdWUpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodG9TZWxlY3QpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMudmFsdWUgPSB0b1NlbGVjdC5uYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub25GaWVsZENoYW5nZWQodGhpcy5maWVsZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5maWVsZC51cGRhdGVGb3JtKCk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0T3B0aW9ucygpOiBGb3JtRmllbGRPcHRpb25bXSB7XHJcbiAgICAgICAgY29uc3QgdmFsID0gdGhpcy52YWx1ZS50cmltKCkudG9Mb2NhbGVMb3dlckNhc2UoKTtcclxuICAgICAgICByZXR1cm4gdGhpcy5maWVsZC5vcHRpb25zLmZpbHRlcigoaXRlbSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBuYW1lID0gaXRlbS5uYW1lLnRvTG9jYWxlTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgICAgIHJldHVybiBuYW1lLmluZGV4T2YodmFsKSA+IC0xO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIGlzVmFsaWRPcHRpb25OYW1lKG9wdGlvbk5hbWU6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGNvbnN0IG9wdGlvbiA9IHRoaXMuZmllbGQub3B0aW9ucy5maW5kKChpdGVtKSA9PiBpdGVtLm5hbWUgJiYgaXRlbS5uYW1lLnRvTG9jYWxlTG93ZXJDYXNlKCkgPT09IG9wdGlvbk5hbWUudG9Mb2NhbGVMb3dlckNhc2UoKSk7XHJcbiAgICAgICAgcmV0dXJuIG9wdGlvbiA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBvbktleVVwKGV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XHJcbiAgICAgICAgaWYgKHRoaXMudmFsdWUgJiYgdGhpcy52YWx1ZS50cmltKCkubGVuZ3RoID49IHRoaXMubWluVGVybUxlbmd0aCAmJiB0aGlzLm9sZFZhbHVlICE9PSB0aGlzLnZhbHVlKSB7XHJcbiAgICAgICAgICAgIGlmIChldmVudC5rZXlDb2RlICE9PSBFU0NBUEUgJiYgZXZlbnQua2V5Q29kZSAhPT0gRU5URVIpIHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLnZhbHVlLmxlbmd0aCA+PSB0aGlzLm1pblRlcm1MZW5ndGgpIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9wdGlvbnMgPSB0aGlzLmdldE9wdGlvbnMoKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9sZFZhbHVlID0gdGhpcy52YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5pc1ZhbGlkT3B0aW9uTmFtZSh0aGlzLnZhbHVlKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmZpZWxkLnZhbHVlID0gdGhpcy5vcHRpb25zWzBdLmlkO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAodGhpcy5pc1ZhbHVlRGVmaW5lZCgpICYmIHRoaXMudmFsdWUudHJpbSgpLmxlbmd0aCA9PT0gMCkge1xyXG4gICAgICAgICAgICB0aGlzLm9sZFZhbHVlID0gdGhpcy52YWx1ZTtcclxuICAgICAgICAgICAgdGhpcy5vcHRpb25zID0gW107XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uSXRlbVNlbGVjdChpdGVtOiBGb3JtRmllbGRPcHRpb24pIHtcclxuICAgICAgICBpZiAoaXRlbSkge1xyXG4gICAgICAgICAgICB0aGlzLmZpZWxkLnZhbHVlID0gaXRlbS5pZDtcclxuICAgICAgICAgICAgdGhpcy52YWx1ZSA9IGl0ZW0ubmFtZTtcclxuICAgICAgICAgICAgdGhpcy5vbkZpZWxkQ2hhbmdlZCh0aGlzLmZpZWxkKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgdmFsaWRhdGUoKSB7XHJcbiAgICAgICAgdGhpcy5maWVsZC52YWx1ZSA9IHRoaXMudmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgaXNWYWx1ZURlZmluZWQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudmFsdWUgIT09IG51bGwgJiYgdGhpcy52YWx1ZSAhPT0gdW5kZWZpbmVkO1xyXG4gICAgfVxyXG5cclxuICAgIGhhbmRsZUVycm9yKGVycm9yOiBhbnkpIHtcclxuICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZXJyb3IoZXJyb3IpO1xyXG4gICAgfVxyXG5cclxuICAgIGlzUmVhZE9ubHlUeXBlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZpZWxkLnR5cGUgPT09ICdyZWFkb25seScgPyB0cnVlIDogZmFsc2U7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==