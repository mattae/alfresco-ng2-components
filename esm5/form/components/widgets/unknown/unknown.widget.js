/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, ViewEncapsulation } from '@angular/core';
import { FormService } from './../../../services/form.service';
import { baseHost, WidgetComponent } from './../widget.component';
var UnknownWidgetComponent = /** @class */ (function (_super) {
    tslib_1.__extends(UnknownWidgetComponent, _super);
    function UnknownWidgetComponent(formService) {
        var _this = _super.call(this, formService) || this;
        _this.formService = formService;
        return _this;
    }
    UnknownWidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'unknown-widget',
                    template: "\n            <mat-list class=\"adf-unknown-widget\">\n                <mat-list-item>\n                     <mat-icon class=\"mat-24\">error_outline</mat-icon>\n                     <span class=\"adf-unknown-text\">Unknown type: {{field.type}}</span>\n                </mat-list-item>\n            </mat-list>\n\n    ",
                    host: baseHost,
                    encapsulation: ViewEncapsulation.None,
                    styles: [".adf-unknown-text{margin-left:10px;color:red}.adf-unknown-widget{margin:42px}"]
                }] }
    ];
    /** @nocollapse */
    UnknownWidgetComponent.ctorParameters = function () { return [
        { type: FormService }
    ]; };
    return UnknownWidgetComponent;
}(WidgetComponent));
export { UnknownWidgetComponent };
if (false) {
    /** @type {?} */
    UnknownWidgetComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidW5rbm93bi53aWRnZXQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2NvbXBvbmVudHMvd2lkZ2V0cy91bmtub3duL3Vua25vd24ud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDL0QsT0FBTyxFQUFFLFFBQVEsRUFBRyxlQUFlLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUVuRTtJQWU0QyxrREFBZTtJQUV2RCxnQ0FBbUIsV0FBd0I7UUFBM0MsWUFDSyxrQkFBTSxXQUFXLENBQUMsU0FDdEI7UUFGa0IsaUJBQVcsR0FBWCxXQUFXLENBQWE7O0lBRTNDLENBQUM7O2dCQW5CSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGdCQUFnQjtvQkFDMUIsUUFBUSxFQUFFLGdVQVFUO29CQUVELElBQUksRUFBRSxRQUFRO29CQUNkLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOztpQkFDeEM7Ozs7Z0JBakJRLFdBQVc7O0lBdUJwQiw2QkFBQztDQUFBLEFBcEJELENBZTRDLGVBQWUsR0FLMUQ7U0FMWSxzQkFBc0I7OztJQUVuQiw2Q0FBK0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuIC8qIHRzbGludDpkaXNhYmxlOmNvbXBvbmVudC1zZWxlY3RvciAgKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgRm9ybVNlcnZpY2UgfSBmcm9tICcuLy4uLy4uLy4uL3NlcnZpY2VzL2Zvcm0uc2VydmljZSc7XHJcbmltcG9ydCB7IGJhc2VIb3N0ICwgV2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi8uLi93aWRnZXQuY29tcG9uZW50JztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICd1bmtub3duLXdpZGdldCcsXHJcbiAgICB0ZW1wbGF0ZTogYFxyXG4gICAgICAgICAgICA8bWF0LWxpc3QgY2xhc3M9XCJhZGYtdW5rbm93bi13aWRnZXRcIj5cclxuICAgICAgICAgICAgICAgIDxtYXQtbGlzdC1pdGVtPlxyXG4gICAgICAgICAgICAgICAgICAgICA8bWF0LWljb24gY2xhc3M9XCJtYXQtMjRcIj5lcnJvcl9vdXRsaW5lPC9tYXQtaWNvbj5cclxuICAgICAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3M9XCJhZGYtdW5rbm93bi10ZXh0XCI+VW5rbm93biB0eXBlOiB7e2ZpZWxkLnR5cGV9fTwvc3Bhbj5cclxuICAgICAgICAgICAgICAgIDwvbWF0LWxpc3QtaXRlbT5cclxuICAgICAgICAgICAgPC9tYXQtbGlzdD5cclxuXHJcbiAgICBgLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vdW5rbm93bi53aWRnZXQuc2NzcyddLFxyXG4gICAgaG9zdDogYmFzZUhvc3QsXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBVbmtub3duV2lkZ2V0Q29tcG9uZW50IGV4dGVuZHMgV2lkZ2V0Q29tcG9uZW50IHtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZm9ybVNlcnZpY2U6IEZvcm1TZXJ2aWNlKSB7XHJcbiAgICAgICAgIHN1cGVyKGZvcm1TZXJ2aWNlKTtcclxuICAgIH1cclxufVxyXG4iXX0=