/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { LogService } from '../../../../services/log.service';
import { ThumbnailService } from '../../../../services/thumbnail.service';
import { Component, ElementRef, ViewChild, ViewEncapsulation } from '@angular/core';
import { from } from 'rxjs';
import { FormService } from '../../../services/form.service';
import { ProcessContentService } from '../../../services/process-content.service';
import { ContentLinkModel } from '../core/content-link.model';
import { baseHost, WidgetComponent } from './../widget.component';
import { map, mergeMap } from 'rxjs/operators';
var UploadWidgetComponent = /** @class */ (function (_super) {
    tslib_1.__extends(UploadWidgetComponent, _super);
    function UploadWidgetComponent(formService, logService, thumbnailService, processContentService) {
        var _this = _super.call(this, formService) || this;
        _this.formService = formService;
        _this.logService = logService;
        _this.thumbnailService = thumbnailService;
        _this.processContentService = processContentService;
        _this.multipleOption = '';
        return _this;
    }
    /**
     * @return {?}
     */
    UploadWidgetComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (this.field &&
            this.field.value &&
            this.field.value.length > 0) {
            this.hasFile = true;
        }
        this.getMultipleFileParam();
    };
    /**
     * @param {?} file
     * @return {?}
     */
    UploadWidgetComponent.prototype.removeFile = /**
     * @param {?} file
     * @return {?}
     */
    function (file) {
        if (this.field) {
            this.removeElementFromList(file);
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    UploadWidgetComponent.prototype.onFileChanged = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        var _this = this;
        /** @type {?} */
        var files = event.target.files;
        /** @type {?} */
        var filesSaved = [];
        if (this.field.json.value) {
            filesSaved = tslib_1.__spread(this.field.json.value);
        }
        if (files && files.length > 0) {
            from(files)
                .pipe(mergeMap((/**
             * @param {?} file
             * @return {?}
             */
            function (file) { return _this.uploadRawContent(file); })))
                .subscribe((/**
             * @param {?} res
             * @return {?}
             */
            function (res) { return filesSaved.push(res); }), (/**
             * @return {?}
             */
            function () { return _this.logService.error('Error uploading file. See console output for more details.'); }), (/**
             * @return {?}
             */
            function () {
                _this.field.value = filesSaved;
                _this.field.json.value = filesSaved;
                _this.hasFile = true;
            }));
        }
    };
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    UploadWidgetComponent.prototype.uploadRawContent = /**
     * @private
     * @param {?} file
     * @return {?}
     */
    function (file) {
        var _this = this;
        return this.processContentService.createTemporaryRawRelatedContent(file)
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            _this.logService.info(response);
            response.contentBlob = file;
            return response;
        })));
    };
    /**
     * @return {?}
     */
    UploadWidgetComponent.prototype.getMultipleFileParam = /**
     * @return {?}
     */
    function () {
        if (this.field &&
            this.field.params &&
            this.field.params.multiple) {
            this.multipleOption = this.field.params.multiple ? 'multiple' : '';
        }
    };
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    UploadWidgetComponent.prototype.removeElementFromList = /**
     * @private
     * @param {?} file
     * @return {?}
     */
    function (file) {
        /** @type {?} */
        var index = this.field.value.indexOf(file);
        if (index !== -1) {
            this.field.value.splice(index, 1);
            this.field.json.value = this.field.value;
            this.field.updateForm();
        }
        this.hasFile = this.field.value.length > 0;
        this.resetFormValueWithNoFiles();
    };
    /**
     * @private
     * @return {?}
     */
    UploadWidgetComponent.prototype.resetFormValueWithNoFiles = /**
     * @private
     * @return {?}
     */
    function () {
        if (this.field.value.length === 0) {
            this.field.value = [];
            this.field.json.value = [];
        }
    };
    /**
     * @param {?} mimeType
     * @return {?}
     */
    UploadWidgetComponent.prototype.getIcon = /**
     * @param {?} mimeType
     * @return {?}
     */
    function (mimeType) {
        return this.thumbnailService.getMimeTypeIcon(mimeType);
    };
    /**
     * @param {?} contentLinkModel
     * @return {?}
     */
    UploadWidgetComponent.prototype.fileClicked = /**
     * @param {?} contentLinkModel
     * @return {?}
     */
    function (contentLinkModel) {
        var _this = this;
        /** @type {?} */
        var file = new ContentLinkModel(contentLinkModel);
        /** @type {?} */
        var fetch = this.processContentService.getContentPreview(file.id);
        if (file.isTypeImage() || file.isTypePdf()) {
            fetch = this.processContentService.getFileRawContent(file.id);
        }
        fetch.subscribe((/**
         * @param {?} blob
         * @return {?}
         */
        function (blob) {
            file.contentBlob = blob;
            _this.formService.formContentClicked.next(file);
        }), (/**
         * @return {?}
         */
        function () {
            _this.logService.error('Unable to send event for file ' + file.name);
        }));
    };
    UploadWidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'upload-widget',
                    template: "<div class=\"adf-upload-widget {{field.className}}\"\r\n     [class.adf-invalid]=\"!field.isValid\"\r\n     [class.adf-readonly]=\"field.readOnly\">\r\n    <label class=\"adf-label\" [attr.for]=\"field.id\">{{field.name | translate }}<span *ngIf=\"isRequired()\">*</span></label>\r\n    <div class=\"adf-upload-widget-container\">\r\n        <div>\r\n            <mat-list *ngIf=\"hasFile\">\r\n                <mat-list-item class=\"adf-upload-files-row\" *ngFor=\"let file of field.value\">\r\n                    <img mat-list-icon class=\"adf-upload-widget__icon\"\r\n                         [id]=\"'file-'+file.id+'-icon'\"\r\n                         [src]=\"getIcon(file.mimeType)\"\r\n                         [alt]=\"mimeTypeIcon\"\r\n                         (click)=\"fileClicked(file)\"\r\n                         (keyup.enter)=\"fileClicked(file)\"\r\n                         role=\"button\"\r\n                         tabindex=\"0\"/>\r\n                    <span matLine id=\"{{'file-'+file.id}}\" (click)=\"fileClicked(file)\" (keyup.enter)=\"fileClicked(file)\"\r\n                          role=\"button\" tabindex=\"0\" class=\"adf-file\">{{file.name}}</span>\r\n                    <button *ngIf=\"!field.readOnly\" mat-icon-button [id]=\"'file-'+file.id+'-remove'\"\r\n                            (click)=\"removeFile(file);\" (keyup.enter)=\"removeFile(file);\">\r\n                        <mat-icon class=\"mat-24\">highlight_off</mat-icon>\r\n                    </button>\r\n                </mat-list-item>\r\n            </mat-list>\r\n        </div>\r\n\r\n        <div *ngIf=\"(!hasFile || multipleOption) && !field.readOnly\">\r\n            <button mat-raised-button color=\"primary\" (click)=\"uploadFiles.click()\">\r\n                {{ 'FORM.FIELD.UPLOAD' | translate }}<mat-icon>file_upload</mat-icon>\r\n                <input #uploadFiles\r\n                       [multiple]=\"multipleOption\"\r\n                       type=\"file\"\r\n                       [id]=\"field.id\"\r\n                       (change)=\"onFileChanged($event)\"/>\r\n            </button>\r\n        </div>\r\n    </div>\r\n    <error-widget [error]=\"field.validationSummary\"></error-widget>\r\n    <error-widget *ngIf=\"isInvalidFieldRequired()\" required=\"{{ 'FORM.FIELD.REQUIRED' | translate }}\"></error-widget>\r\n</div>\r\n",
                    host: baseHost,
                    encapsulation: ViewEncapsulation.None,
                    styles: [".adf-upload-widget-container{margin-bottom:15px}.adf-upload-widget-container input{display:none}.adf-upload-widget{width:100%;word-break:break-all;padding:.4375em 0;border-top:.84375em solid transparent}.adf-upload-widget__icon{padding:6px;float:left;cursor:pointer}.adf-upload-widget__reset{margin-top:-2px}.adf-upload-files-row .mat-line{margin-bottom:0}"]
                }] }
    ];
    /** @nocollapse */
    UploadWidgetComponent.ctorParameters = function () { return [
        { type: FormService },
        { type: LogService },
        { type: ThumbnailService },
        { type: ProcessContentService }
    ]; };
    UploadWidgetComponent.propDecorators = {
        fileInput: [{ type: ViewChild, args: ['uploadFiles', { static: true },] }]
    };
    return UploadWidgetComponent;
}(WidgetComponent));
export { UploadWidgetComponent };
if (false) {
    /** @type {?} */
    UploadWidgetComponent.prototype.hasFile;
    /** @type {?} */
    UploadWidgetComponent.prototype.displayText;
    /** @type {?} */
    UploadWidgetComponent.prototype.multipleOption;
    /** @type {?} */
    UploadWidgetComponent.prototype.mimeTypeIcon;
    /** @type {?} */
    UploadWidgetComponent.prototype.fileInput;
    /** @type {?} */
    UploadWidgetComponent.prototype.formService;
    /**
     * @type {?}
     * @private
     */
    UploadWidgetComponent.prototype.logService;
    /**
     * @type {?}
     * @private
     */
    UploadWidgetComponent.prototype.thumbnailService;
    /** @type {?} */
    UploadWidgetComponent.prototype.processContentService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLndpZGdldC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL3VwbG9hZC91cGxvYWQud2lkZ2V0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBcUNBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUM5RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSx3Q0FBd0MsQ0FBQztBQUMxRSxPQUFPLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBVSxTQUFTLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDNUYsT0FBTyxFQUFFLElBQUksRUFBYyxNQUFNLE1BQU0sQ0FBQztBQUN4QyxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDN0QsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFDbEYsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDOUQsT0FBTyxFQUFFLFFBQVEsRUFBRSxlQUFlLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUNsRSxPQUFPLEVBQUUsR0FBRyxFQUFFLFFBQVEsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRS9DO0lBTzJDLGlEQUFlO0lBVXRELCtCQUFtQixXQUF3QixFQUN2QixVQUFzQixFQUN0QixnQkFBa0MsRUFDbkMscUJBQTRDO1FBSC9ELFlBSUksa0JBQU0sV0FBVyxDQUFDLFNBQ3JCO1FBTGtCLGlCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3ZCLGdCQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLHNCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbkMsMkJBQXFCLEdBQXJCLHFCQUFxQixDQUF1QjtRQVQvRCxvQkFBYyxHQUFXLEVBQUUsQ0FBQzs7SUFXNUIsQ0FBQzs7OztJQUVELHdDQUFROzs7SUFBUjtRQUNJLElBQUksSUFBSSxDQUFDLEtBQUs7WUFDVixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUs7WUFDaEIsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUM3QixJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQztTQUN2QjtRQUNELElBQUksQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0lBQ2hDLENBQUM7Ozs7O0lBRUQsMENBQVU7Ozs7SUFBVixVQUFXLElBQVM7UUFDaEIsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ1osSUFBSSxDQUFDLHFCQUFxQixDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3BDO0lBQ0wsQ0FBQzs7Ozs7SUFFRCw2Q0FBYTs7OztJQUFiLFVBQWMsS0FBVTtRQUF4QixpQkFxQkM7O1lBcEJTLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUs7O1lBQzVCLFVBQVUsR0FBRyxFQUFFO1FBRW5CLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQ3ZCLFVBQVUsb0JBQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7U0FDM0M7UUFFRCxJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUMzQixJQUFJLENBQUMsS0FBSyxDQUFDO2lCQUNOLElBQUksQ0FBQyxRQUFROzs7O1lBQUMsVUFBQyxJQUFJLElBQUssT0FBQSxLQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLEVBQTNCLENBQTJCLEVBQUMsQ0FBQztpQkFDckQsU0FBUzs7OztZQUNOLFVBQUMsR0FBRyxJQUFLLE9BQUEsVUFBVSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBcEIsQ0FBb0I7OztZQUM3QixjQUFNLE9BQUEsS0FBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsNERBQTRELENBQUMsRUFBbkYsQ0FBbUY7OztZQUN6RjtnQkFDSSxLQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUM7Z0JBQzlCLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxVQUFVLENBQUM7Z0JBQ25DLEtBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1lBQ3hCLENBQUMsRUFDSixDQUFDO1NBQ1Q7SUFDTCxDQUFDOzs7Ozs7SUFFTyxnREFBZ0I7Ozs7O0lBQXhCLFVBQXlCLElBQUk7UUFBN0IsaUJBU0M7UUFSRyxPQUFPLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxnQ0FBZ0MsQ0FBQyxJQUFJLENBQUM7YUFDbkUsSUFBSSxDQUNELEdBQUc7Ozs7UUFBQyxVQUFDLFFBQWE7WUFDZCxLQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUMvQixRQUFRLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztZQUM1QixPQUFPLFFBQVEsQ0FBQztRQUNwQixDQUFDLEVBQUMsQ0FDTCxDQUFDO0lBQ1YsQ0FBQzs7OztJQUVELG9EQUFvQjs7O0lBQXBCO1FBQ0ksSUFBSSxJQUFJLENBQUMsS0FBSztZQUNWLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTTtZQUNqQixJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7WUFDNUIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO1NBQ3RFO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8scURBQXFCOzs7OztJQUE3QixVQUE4QixJQUFJOztZQUN4QixLQUFLLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQztRQUU1QyxJQUFJLEtBQUssS0FBSyxDQUFDLENBQUMsRUFBRTtZQUNkLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxLQUFLLEVBQUUsQ0FBQyxDQUFDLENBQUM7WUFDbEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDO1lBQ3pDLElBQUksQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFLENBQUM7U0FDM0I7UUFFRCxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFFM0MsSUFBSSxDQUFDLHlCQUF5QixFQUFFLENBQUM7SUFDckMsQ0FBQzs7Ozs7SUFFTyx5REFBeUI7Ozs7SUFBakM7UUFDSSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDL0IsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxFQUFFLENBQUM7U0FDOUI7SUFDTCxDQUFDOzs7OztJQUVELHVDQUFPOzs7O0lBQVAsVUFBUSxRQUFRO1FBQ1osT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsZUFBZSxDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQzNELENBQUM7Ozs7O0lBRUQsMkNBQVc7Ozs7SUFBWCxVQUFZLGdCQUFxQjtRQUFqQyxpQkFlQzs7WUFkUyxJQUFJLEdBQUcsSUFBSSxnQkFBZ0IsQ0FBQyxnQkFBZ0IsQ0FBQzs7WUFDL0MsS0FBSyxHQUFHLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO1FBQ2pFLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRSxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUUsRUFBRTtZQUN4QyxLQUFLLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUNqRTtRQUNELEtBQUssQ0FBQyxTQUFTOzs7O1FBQ1gsVUFBQyxJQUFVO1lBQ1AsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7WUFDeEIsS0FBSSxDQUFDLFdBQVcsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDbkQsQ0FBQzs7O1FBQ0Q7WUFDSSxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxnQ0FBZ0MsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDeEUsQ0FBQyxFQUNKLENBQUM7SUFDTixDQUFDOztnQkF6SEosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxlQUFlO29CQUN6Qiw0ekVBQW1DO29CQUVuQyxJQUFJLEVBQUUsUUFBUTtvQkFDZCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7aUJBQ3hDOzs7O2dCQVpRLFdBQVc7Z0JBSlgsVUFBVTtnQkFDVixnQkFBZ0I7Z0JBSWhCLHFCQUFxQjs7OzRCQW1CekIsU0FBUyxTQUFDLGFBQWEsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7O0lBNEc1Qyw0QkFBQztDQUFBLEFBMUhELENBTzJDLGVBQWUsR0FtSHpEO1NBbkhZLHFCQUFxQjs7O0lBRTlCLHdDQUFpQjs7SUFDakIsNENBQW9COztJQUNwQiwrQ0FBNEI7O0lBQzVCLDZDQUFxQjs7SUFFckIsMENBQ3NCOztJQUVWLDRDQUErQjs7Ozs7SUFDL0IsMkNBQThCOzs7OztJQUM5QixpREFBMEM7O0lBQzFDLHNEQUFtRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxuXG4vKiFcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxuICpcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cbiAqL1xuXG4vKiB0c2xpbnQ6ZGlzYWJsZTpjb21wb25lbnQtc2VsZWN0b3IgICovXG5cbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi8uLi8uLi9zZXJ2aWNlcy9sb2cuc2VydmljZSc7XG5pbXBvcnQgeyBUaHVtYm5haWxTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vLi4vc2VydmljZXMvdGh1bWJuYWlsLnNlcnZpY2UnO1xuaW1wb3J0IHsgQ29tcG9uZW50LCBFbGVtZW50UmVmLCBPbkluaXQsIFZpZXdDaGlsZCwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IGZyb20sIE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi4vLi4vLi4vc2VydmljZXMvZm9ybS5zZXJ2aWNlJztcbmltcG9ydCB7IFByb2Nlc3NDb250ZW50U2VydmljZSB9IGZyb20gJy4uLy4uLy4uL3NlcnZpY2VzL3Byb2Nlc3MtY29udGVudC5zZXJ2aWNlJztcbmltcG9ydCB7IENvbnRlbnRMaW5rTW9kZWwgfSBmcm9tICcuLi9jb3JlL2NvbnRlbnQtbGluay5tb2RlbCc7XG5pbXBvcnQgeyBiYXNlSG9zdCwgV2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi8uLi93aWRnZXQuY29tcG9uZW50JztcbmltcG9ydCB7IG1hcCwgbWVyZ2VNYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAndXBsb2FkLXdpZGdldCcsXG4gICAgdGVtcGxhdGVVcmw6ICcuL3VwbG9hZC53aWRnZXQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vdXBsb2FkLndpZGdldC5zY3NzJ10sXG4gICAgaG9zdDogYmFzZUhvc3QsXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxufSlcbmV4cG9ydCBjbGFzcyBVcGxvYWRXaWRnZXRDb21wb25lbnQgZXh0ZW5kcyBXaWRnZXRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gICAgaGFzRmlsZTogYm9vbGVhbjtcbiAgICBkaXNwbGF5VGV4dDogc3RyaW5nO1xuICAgIG11bHRpcGxlT3B0aW9uOiBzdHJpbmcgPSAnJztcbiAgICBtaW1lVHlwZUljb246IHN0cmluZztcblxuICAgIEBWaWV3Q2hpbGQoJ3VwbG9hZEZpbGVzJywge3N0YXRpYzogdHJ1ZX0pXG4gICAgZmlsZUlucHV0OiBFbGVtZW50UmVmO1xuXG4gICAgY29uc3RydWN0b3IocHVibGljIGZvcm1TZXJ2aWNlOiBGb3JtU2VydmljZSxcbiAgICAgICAgICAgICAgICBwcml2YXRlIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UsXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSB0aHVtYm5haWxTZXJ2aWNlOiBUaHVtYm5haWxTZXJ2aWNlLFxuICAgICAgICAgICAgICAgIHB1YmxpYyBwcm9jZXNzQ29udGVudFNlcnZpY2U6IFByb2Nlc3NDb250ZW50U2VydmljZSkge1xuICAgICAgICBzdXBlcihmb3JtU2VydmljZSk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIGlmICh0aGlzLmZpZWxkICYmXG4gICAgICAgICAgICB0aGlzLmZpZWxkLnZhbHVlICYmXG4gICAgICAgICAgICB0aGlzLmZpZWxkLnZhbHVlLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIHRoaXMuaGFzRmlsZSA9IHRydWU7XG4gICAgICAgIH1cbiAgICAgICAgdGhpcy5nZXRNdWx0aXBsZUZpbGVQYXJhbSgpO1xuICAgIH1cblxuICAgIHJlbW92ZUZpbGUoZmlsZTogYW55KSB7XG4gICAgICAgIGlmICh0aGlzLmZpZWxkKSB7XG4gICAgICAgICAgICB0aGlzLnJlbW92ZUVsZW1lbnRGcm9tTGlzdChmaWxlKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG9uRmlsZUNoYW5nZWQoZXZlbnQ6IGFueSkge1xuICAgICAgICBjb25zdCBmaWxlcyA9IGV2ZW50LnRhcmdldC5maWxlcztcbiAgICAgICAgbGV0IGZpbGVzU2F2ZWQgPSBbXTtcblxuICAgICAgICBpZiAodGhpcy5maWVsZC5qc29uLnZhbHVlKSB7XG4gICAgICAgICAgICBmaWxlc1NhdmVkID0gWy4uLnRoaXMuZmllbGQuanNvbi52YWx1ZV07XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZmlsZXMgJiYgZmlsZXMubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgZnJvbShmaWxlcylcbiAgICAgICAgICAgICAgICAucGlwZShtZXJnZU1hcCgoZmlsZSkgPT4gdGhpcy51cGxvYWRSYXdDb250ZW50KGZpbGUpKSlcbiAgICAgICAgICAgICAgICAuc3Vic2NyaWJlKFxuICAgICAgICAgICAgICAgICAgICAocmVzKSA9PiBmaWxlc1NhdmVkLnB1c2gocmVzKSxcbiAgICAgICAgICAgICAgICAgICAgKCkgPT4gdGhpcy5sb2dTZXJ2aWNlLmVycm9yKCdFcnJvciB1cGxvYWRpbmcgZmlsZS4gU2VlIGNvbnNvbGUgb3V0cHV0IGZvciBtb3JlIGRldGFpbHMuJyksXG4gICAgICAgICAgICAgICAgICAgICgpID0+IHtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZmllbGQudmFsdWUgPSBmaWxlc1NhdmVkO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5maWVsZC5qc29uLnZhbHVlID0gZmlsZXNTYXZlZDtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaGFzRmlsZSA9IHRydWU7XG4gICAgICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICApO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSB1cGxvYWRSYXdDb250ZW50KGZpbGUpOiBPYnNlcnZhYmxlPGFueT4ge1xuICAgICAgICByZXR1cm4gdGhpcy5wcm9jZXNzQ29udGVudFNlcnZpY2UuY3JlYXRlVGVtcG9yYXJ5UmF3UmVsYXRlZENvbnRlbnQoZmlsZSlcbiAgICAgICAgICAgIC5waXBlKFxuICAgICAgICAgICAgICAgIG1hcCgocmVzcG9uc2U6IGFueSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuaW5mbyhyZXNwb25zZSk7XG4gICAgICAgICAgICAgICAgICAgIHJlc3BvbnNlLmNvbnRlbnRCbG9iID0gZmlsZTtcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xuICAgICAgICAgICAgICAgIH0pXG4gICAgICAgICAgICApO1xuICAgIH1cblxuICAgIGdldE11bHRpcGxlRmlsZVBhcmFtKCkge1xuICAgICAgICBpZiAodGhpcy5maWVsZCAmJlxuICAgICAgICAgICAgdGhpcy5maWVsZC5wYXJhbXMgJiZcbiAgICAgICAgICAgIHRoaXMuZmllbGQucGFyYW1zLm11bHRpcGxlKSB7XG4gICAgICAgICAgICB0aGlzLm11bHRpcGxlT3B0aW9uID0gdGhpcy5maWVsZC5wYXJhbXMubXVsdGlwbGUgPyAnbXVsdGlwbGUnIDogJyc7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIHJlbW92ZUVsZW1lbnRGcm9tTGlzdChmaWxlKSB7XG4gICAgICAgIGNvbnN0IGluZGV4ID0gdGhpcy5maWVsZC52YWx1ZS5pbmRleE9mKGZpbGUpO1xuXG4gICAgICAgIGlmIChpbmRleCAhPT0gLTEpIHtcbiAgICAgICAgICAgIHRoaXMuZmllbGQudmFsdWUuc3BsaWNlKGluZGV4LCAxKTtcbiAgICAgICAgICAgIHRoaXMuZmllbGQuanNvbi52YWx1ZSA9IHRoaXMuZmllbGQudmFsdWU7XG4gICAgICAgICAgICB0aGlzLmZpZWxkLnVwZGF0ZUZvcm0oKTtcbiAgICAgICAgfVxuXG4gICAgICAgIHRoaXMuaGFzRmlsZSA9IHRoaXMuZmllbGQudmFsdWUubGVuZ3RoID4gMDtcblxuICAgICAgICB0aGlzLnJlc2V0Rm9ybVZhbHVlV2l0aE5vRmlsZXMoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHJlc2V0Rm9ybVZhbHVlV2l0aE5vRmlsZXMoKSB7XG4gICAgICAgIGlmICh0aGlzLmZpZWxkLnZhbHVlLmxlbmd0aCA9PT0gMCkge1xuICAgICAgICAgICAgdGhpcy5maWVsZC52YWx1ZSA9IFtdO1xuICAgICAgICAgICAgdGhpcy5maWVsZC5qc29uLnZhbHVlID0gW107XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBnZXRJY29uKG1pbWVUeXBlKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnRodW1ibmFpbFNlcnZpY2UuZ2V0TWltZVR5cGVJY29uKG1pbWVUeXBlKTtcbiAgICB9XG5cbiAgICBmaWxlQ2xpY2tlZChjb250ZW50TGlua01vZGVsOiBhbnkpOiB2b2lkIHtcbiAgICAgICAgY29uc3QgZmlsZSA9IG5ldyBDb250ZW50TGlua01vZGVsKGNvbnRlbnRMaW5rTW9kZWwpO1xuICAgICAgICBsZXQgZmV0Y2ggPSB0aGlzLnByb2Nlc3NDb250ZW50U2VydmljZS5nZXRDb250ZW50UHJldmlldyhmaWxlLmlkKTtcbiAgICAgICAgaWYgKGZpbGUuaXNUeXBlSW1hZ2UoKSB8fCBmaWxlLmlzVHlwZVBkZigpKSB7XG4gICAgICAgICAgICBmZXRjaCA9IHRoaXMucHJvY2Vzc0NvbnRlbnRTZXJ2aWNlLmdldEZpbGVSYXdDb250ZW50KGZpbGUuaWQpO1xuICAgICAgICB9XG4gICAgICAgIGZldGNoLnN1YnNjcmliZShcbiAgICAgICAgICAgIChibG9iOiBCbG9iKSA9PiB7XG4gICAgICAgICAgICAgICAgZmlsZS5jb250ZW50QmxvYiA9IGJsb2I7XG4gICAgICAgICAgICAgICAgdGhpcy5mb3JtU2VydmljZS5mb3JtQ29udGVudENsaWNrZWQubmV4dChmaWxlKTtcbiAgICAgICAgICAgIH0sXG4gICAgICAgICAgICAoKSA9PiB7XG4gICAgICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKCdVbmFibGUgdG8gc2VuZCBldmVudCBmb3IgZmlsZSAnICsgZmlsZS5uYW1lKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgKTtcbiAgICB9XG59XG4iXX0=