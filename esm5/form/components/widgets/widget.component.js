/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:component-selector  */
import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { FormService } from './../../services/form.service';
import { FormFieldModel } from './core/index';
/** @type {?} */
export var baseHost = {
    '(click)': 'event($event)',
    '(blur)': 'event($event)',
    '(change)': 'event($event)',
    '(focus)': 'event($event)',
    '(focusin)': 'event($event)',
    '(focusout)': 'event($event)',
    '(input)': 'event($event)',
    '(invalid)': 'event($event)',
    '(select)': 'event($event)'
};
/**
 * Base widget component.
 */
var WidgetComponent = /** @class */ (function () {
    function WidgetComponent(formService) {
        this.formService = formService;
        /**
         * Does the widget show a read-only value? (ie, can't be edited)
         */
        this.readOnly = false;
        /**
         * Emitted when a field value changes.
         */
        this.fieldChanged = new EventEmitter();
    }
    /**
     * @return {?}
     */
    WidgetComponent.prototype.hasField = /**
     * @return {?}
     */
    function () {
        return this.field ? true : false;
    };
    // Note for developers:
    // returns <any> object to be able binding it to the <element required="required"> attribute
    // Note for developers:
    // returns <any> object to be able binding it to the <element required="required"> attribute
    /**
     * @return {?}
     */
    WidgetComponent.prototype.isRequired = 
    // Note for developers:
    // returns <any> object to be able binding it to the <element required="required"> attribute
    /**
     * @return {?}
     */
    function () {
        if (this.field && this.field.required) {
            return true;
        }
        return null;
    };
    /**
     * @return {?}
     */
    WidgetComponent.prototype.isValid = /**
     * @return {?}
     */
    function () {
        return this.field.validationSummary ? true : false;
    };
    /**
     * @return {?}
     */
    WidgetComponent.prototype.hasValue = /**
     * @return {?}
     */
    function () {
        return this.field &&
            this.field.value !== null &&
            this.field.value !== undefined;
    };
    /**
     * @return {?}
     */
    WidgetComponent.prototype.isInvalidFieldRequired = /**
     * @return {?}
     */
    function () {
        return !this.field.isValid && !this.field.validationSummary && this.isRequired();
    };
    /**
     * @return {?}
     */
    WidgetComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        this.fieldChanged.emit(this.field);
    };
    /**
     * @param {?} field
     * @return {?}
     */
    WidgetComponent.prototype.checkVisibility = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        this.fieldChanged.emit(field);
    };
    /**
     * @param {?} field
     * @return {?}
     */
    WidgetComponent.prototype.onFieldChanged = /**
     * @param {?} field
     * @return {?}
     */
    function (field) {
        this.fieldChanged.emit(field);
    };
    /**
     * @protected
     * @param {?} field
     * @return {?}
     */
    WidgetComponent.prototype.getHyperlinkUrl = /**
     * @protected
     * @param {?} field
     * @return {?}
     */
    function (field) {
        /** @type {?} */
        var url = WidgetComponent.DEFAULT_HYPERLINK_URL;
        if (field && field.hyperlinkUrl) {
            url = field.hyperlinkUrl;
            if (!/^https?:\/\//i.test(url)) {
                url = "" + WidgetComponent.DEFAULT_HYPERLINK_SCHEME + url;
            }
        }
        return url;
    };
    /**
     * @protected
     * @param {?} field
     * @return {?}
     */
    WidgetComponent.prototype.getHyperlinkText = /**
     * @protected
     * @param {?} field
     * @return {?}
     */
    function (field) {
        if (field) {
            return field.displayText || field.hyperlinkUrl;
        }
        return null;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    WidgetComponent.prototype.event = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.formService.formEvents.next(event);
    };
    WidgetComponent.DEFAULT_HYPERLINK_URL = '#';
    WidgetComponent.DEFAULT_HYPERLINK_SCHEME = 'http://';
    WidgetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'base-widget',
                    template: '',
                    host: baseHost,
                    encapsulation: ViewEncapsulation.None
                }] }
    ];
    /** @nocollapse */
    WidgetComponent.ctorParameters = function () { return [
        { type: FormService }
    ]; };
    WidgetComponent.propDecorators = {
        readOnly: [{ type: Input }],
        field: [{ type: Input }],
        fieldChanged: [{ type: Output }]
    };
    return WidgetComponent;
}());
export { WidgetComponent };
if (false) {
    /** @type {?} */
    WidgetComponent.DEFAULT_HYPERLINK_URL;
    /** @type {?} */
    WidgetComponent.DEFAULT_HYPERLINK_SCHEME;
    /**
     * Does the widget show a read-only value? (ie, can't be edited)
     * @type {?}
     */
    WidgetComponent.prototype.readOnly;
    /**
     * Data to be displayed in the field
     * @type {?}
     */
    WidgetComponent.prototype.field;
    /**
     * Emitted when a field value changes.
     * @type {?}
     */
    WidgetComponent.prototype.fieldChanged;
    /** @type {?} */
    WidgetComponent.prototype.formService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vY29tcG9uZW50cy93aWRnZXRzL3dpZGdldC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLE9BQU8sRUFBaUIsU0FBUyxFQUFFLFlBQVksRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pHLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sY0FBYyxDQUFDOztBQUU5QyxNQUFNLEtBQU8sUUFBUSxHQUFHO0lBQ3BCLFNBQVMsRUFBRSxlQUFlO0lBQzFCLFFBQVEsRUFBRSxlQUFlO0lBQ3pCLFVBQVUsRUFBRSxlQUFlO0lBQzNCLFNBQVMsRUFBRSxlQUFlO0lBQzFCLFdBQVcsRUFBRSxlQUFlO0lBQzVCLFlBQVksRUFBRSxlQUFlO0lBQzdCLFNBQVMsRUFBRSxlQUFlO0lBQzFCLFdBQVcsRUFBRSxlQUFlO0lBQzVCLFVBQVUsRUFBRSxlQUFlO0NBQzlCOzs7O0FBS0Q7SUF5QkkseUJBQW1CLFdBQXlCO1FBQXpCLGdCQUFXLEdBQVgsV0FBVyxDQUFjOzs7O1FBWjVDLGFBQVEsR0FBWSxLQUFLLENBQUM7Ozs7UUFVMUIsaUJBQVksR0FBaUMsSUFBSSxZQUFZLEVBQWtCLENBQUM7SUFHaEYsQ0FBQzs7OztJQUVELGtDQUFROzs7SUFBUjtRQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDckMsQ0FBQztJQUVELHVCQUF1QjtJQUN2Qiw0RkFBNEY7Ozs7OztJQUM1RixvQ0FBVTs7Ozs7O0lBQVY7UUFDSSxJQUFJLElBQUksQ0FBQyxLQUFLLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUU7WUFDbkMsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7SUFFRCxpQ0FBTzs7O0lBQVA7UUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQ3ZELENBQUM7Ozs7SUFFRCxrQ0FBUTs7O0lBQVI7UUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLO1lBQ2IsSUFBSSxDQUFDLEtBQUssQ0FBQyxLQUFLLEtBQUssSUFBSTtZQUN6QixJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssS0FBSyxTQUFTLENBQUM7SUFDdkMsQ0FBQzs7OztJQUVELGdEQUFzQjs7O0lBQXRCO1FBQ0ksT0FBTyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDckYsQ0FBQzs7OztJQUVELHlDQUFlOzs7SUFBZjtRQUNJLElBQUksQ0FBQyxZQUFZLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN2QyxDQUFDOzs7OztJQUVELHlDQUFlOzs7O0lBQWYsVUFBZ0IsS0FBcUI7UUFDakMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbEMsQ0FBQzs7Ozs7SUFFRCx3Q0FBYzs7OztJQUFkLFVBQWUsS0FBcUI7UUFDaEMsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbEMsQ0FBQzs7Ozs7O0lBRVMseUNBQWU7Ozs7O0lBQXpCLFVBQTBCLEtBQXFCOztZQUN2QyxHQUFHLEdBQUcsZUFBZSxDQUFDLHFCQUFxQjtRQUMvQyxJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsWUFBWSxFQUFFO1lBQzdCLEdBQUcsR0FBRyxLQUFLLENBQUMsWUFBWSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUM1QixHQUFHLEdBQUcsS0FBRyxlQUFlLENBQUMsd0JBQXdCLEdBQUcsR0FBSyxDQUFDO2FBQzdEO1NBQ0o7UUFDRCxPQUFPLEdBQUcsQ0FBQztJQUNmLENBQUM7Ozs7OztJQUVTLDBDQUFnQjs7Ozs7SUFBMUIsVUFBMkIsS0FBcUI7UUFDNUMsSUFBSSxLQUFLLEVBQUU7WUFDUCxPQUFPLEtBQUssQ0FBQyxXQUFXLElBQUksS0FBSyxDQUFDLFlBQVksQ0FBQztTQUNsRDtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7O0lBRUQsK0JBQUs7Ozs7SUFBTCxVQUFNLEtBQVk7UUFDZCxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDNUMsQ0FBQztJQS9FTSxxQ0FBcUIsR0FBVyxHQUFHLENBQUM7SUFDcEMsd0NBQXdCLEdBQVcsU0FBUyxDQUFDOztnQkFUdkQsU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxhQUFhO29CQUN2QixRQUFRLEVBQUUsRUFBRTtvQkFDWixJQUFJLEVBQUUsUUFBUTtvQkFDZCxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtpQkFDeEM7Ozs7Z0JBdkJRLFdBQVc7OzsyQkE4QmYsS0FBSzt3QkFJTCxLQUFLOytCQU1MLE1BQU07O0lBa0VYLHNCQUFDO0NBQUEsQUF4RkQsSUF3RkM7U0FsRlksZUFBZTs7O0lBRXhCLHNDQUEyQzs7SUFDM0MseUNBQW9EOzs7OztJQUdwRCxtQ0FDMEI7Ozs7O0lBRzFCLGdDQUNzQjs7Ozs7SUFLdEIsdUNBQ2dGOztJQUVwRSxzQ0FBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6Y29tcG9uZW50LXNlbGVjdG9yICAqL1xyXG5cclxuaW1wb3J0IHsgQWZ0ZXJWaWV3SW5pdCwgQ29tcG9uZW50LCBFdmVudEVtaXR0ZXIsIElucHV0LCBPdXRwdXQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1TZXJ2aWNlIH0gZnJvbSAnLi8uLi8uLi9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRNb2RlbCB9IGZyb20gJy4vY29yZS9pbmRleCc7XHJcblxyXG5leHBvcnQgY29uc3QgYmFzZUhvc3QgPSB7XHJcbiAgICAnKGNsaWNrKSc6ICdldmVudCgkZXZlbnQpJyxcclxuICAgICcoYmx1ciknOiAnZXZlbnQoJGV2ZW50KScsXHJcbiAgICAnKGNoYW5nZSknOiAnZXZlbnQoJGV2ZW50KScsXHJcbiAgICAnKGZvY3VzKSc6ICdldmVudCgkZXZlbnQpJyxcclxuICAgICcoZm9jdXNpbiknOiAnZXZlbnQoJGV2ZW50KScsXHJcbiAgICAnKGZvY3Vzb3V0KSc6ICdldmVudCgkZXZlbnQpJyxcclxuICAgICcoaW5wdXQpJzogJ2V2ZW50KCRldmVudCknLFxyXG4gICAgJyhpbnZhbGlkKSc6ICdldmVudCgkZXZlbnQpJyxcclxuICAgICcoc2VsZWN0KSc6ICdldmVudCgkZXZlbnQpJ1xyXG59O1xyXG5cclxuLyoqXHJcbiAqIEJhc2Ugd2lkZ2V0IGNvbXBvbmVudC5cclxuICovXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdiYXNlLXdpZGdldCcsXHJcbiAgICB0ZW1wbGF0ZTogJycsXHJcbiAgICBob3N0OiBiYXNlSG9zdCxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIFdpZGdldENvbXBvbmVudCBpbXBsZW1lbnRzIEFmdGVyVmlld0luaXQge1xyXG5cclxuICAgIHN0YXRpYyBERUZBVUxUX0hZUEVSTElOS19VUkw6IHN0cmluZyA9ICcjJztcclxuICAgIHN0YXRpYyBERUZBVUxUX0hZUEVSTElOS19TQ0hFTUU6IHN0cmluZyA9ICdodHRwOi8vJztcclxuXHJcbiAgICAvKiogRG9lcyB0aGUgd2lkZ2V0IHNob3cgYSByZWFkLW9ubHkgdmFsdWU/IChpZSwgY2FuJ3QgYmUgZWRpdGVkKSAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHJlYWRPbmx5OiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgLyoqIERhdGEgdG8gYmUgZGlzcGxheWVkIGluIHRoZSBmaWVsZCAqL1xyXG4gICAgQElucHV0KClcclxuICAgIGZpZWxkOiBGb3JtRmllbGRNb2RlbDtcclxuXHJcbiAgICAvKipcclxuICAgICAqIEVtaXR0ZWQgd2hlbiBhIGZpZWxkIHZhbHVlIGNoYW5nZXMuXHJcbiAgICAgKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgZmllbGRDaGFuZ2VkOiBFdmVudEVtaXR0ZXI8Rm9ybUZpZWxkTW9kZWw+ID0gbmV3IEV2ZW50RW1pdHRlcjxGb3JtRmllbGRNb2RlbD4oKTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgZm9ybVNlcnZpY2U/OiBGb3JtU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIGhhc0ZpZWxkKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZpZWxkID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIE5vdGUgZm9yIGRldmVsb3BlcnM6XHJcbiAgICAvLyByZXR1cm5zIDxhbnk+IG9iamVjdCB0byBiZSBhYmxlIGJpbmRpbmcgaXQgdG8gdGhlIDxlbGVtZW50IHJlcXVpcmVkPVwicmVxdWlyZWRcIj4gYXR0cmlidXRlXHJcbiAgICBpc1JlcXVpcmVkKCk6IGFueSB7XHJcbiAgICAgICAgaWYgKHRoaXMuZmllbGQgJiYgdGhpcy5maWVsZC5yZXF1aXJlZCkge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgaXNWYWxpZCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5maWVsZC52YWxpZGF0aW9uU3VtbWFyeSA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBoYXNWYWx1ZSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5maWVsZCAmJlxyXG4gICAgICAgICAgICB0aGlzLmZpZWxkLnZhbHVlICE9PSBudWxsICYmXHJcbiAgICAgICAgICAgIHRoaXMuZmllbGQudmFsdWUgIT09IHVuZGVmaW5lZDtcclxuICAgIH1cclxuXHJcbiAgICBpc0ludmFsaWRGaWVsZFJlcXVpcmVkKCkge1xyXG4gICAgICAgIHJldHVybiAhdGhpcy5maWVsZC5pc1ZhbGlkICYmICF0aGlzLmZpZWxkLnZhbGlkYXRpb25TdW1tYXJ5ICYmIHRoaXMuaXNSZXF1aXJlZCgpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nQWZ0ZXJWaWV3SW5pdCgpIHtcclxuICAgICAgICB0aGlzLmZpZWxkQ2hhbmdlZC5lbWl0KHRoaXMuZmllbGQpO1xyXG4gICAgfVxyXG5cclxuICAgIGNoZWNrVmlzaWJpbGl0eShmaWVsZDogRm9ybUZpZWxkTW9kZWwpIHtcclxuICAgICAgICB0aGlzLmZpZWxkQ2hhbmdlZC5lbWl0KGZpZWxkKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkZpZWxkQ2hhbmdlZChmaWVsZDogRm9ybUZpZWxkTW9kZWwpIHtcclxuICAgICAgICB0aGlzLmZpZWxkQ2hhbmdlZC5lbWl0KGZpZWxkKTtcclxuICAgIH1cclxuXHJcbiAgICBwcm90ZWN0ZWQgZ2V0SHlwZXJsaW5rVXJsKGZpZWxkOiBGb3JtRmllbGRNb2RlbCkge1xyXG4gICAgICAgIGxldCB1cmwgPSBXaWRnZXRDb21wb25lbnQuREVGQVVMVF9IWVBFUkxJTktfVVJMO1xyXG4gICAgICAgIGlmIChmaWVsZCAmJiBmaWVsZC5oeXBlcmxpbmtVcmwpIHtcclxuICAgICAgICAgICAgdXJsID0gZmllbGQuaHlwZXJsaW5rVXJsO1xyXG4gICAgICAgICAgICBpZiAoIS9eaHR0cHM/OlxcL1xcLy9pLnRlc3QodXJsKSkge1xyXG4gICAgICAgICAgICAgICAgdXJsID0gYCR7V2lkZ2V0Q29tcG9uZW50LkRFRkFVTFRfSFlQRVJMSU5LX1NDSEVNRX0ke3VybH1gO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB1cmw7XHJcbiAgICB9XHJcblxyXG4gICAgcHJvdGVjdGVkIGdldEh5cGVybGlua1RleHQoZmllbGQ6IEZvcm1GaWVsZE1vZGVsKSB7XHJcbiAgICAgICAgaWYgKGZpZWxkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmaWVsZC5kaXNwbGF5VGV4dCB8fCBmaWVsZC5oeXBlcmxpbmtVcmw7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIGV2ZW50KGV2ZW50OiBFdmVudCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZm9ybVNlcnZpY2UuZm9ybUV2ZW50cy5uZXh0KGV2ZW50KTtcclxuICAgIH1cclxufVxyXG4iXX0=