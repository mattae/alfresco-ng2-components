/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var FormEvent = /** @class */ (function () {
    function FormEvent(form) {
        this.isDefaultPrevented = false;
        this.form = form;
    }
    Object.defineProperty(FormEvent.prototype, "defaultPrevented", {
        get: /**
         * @return {?}
         */
        function () {
            return this.isDefaultPrevented;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    FormEvent.prototype.preventDefault = /**
     * @return {?}
     */
    function () {
        this.isDefaultPrevented = true;
    };
    return FormEvent;
}());
export { FormEvent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    FormEvent.prototype.isDefaultPrevented;
    /** @type {?} */
    FormEvent.prototype.form;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5ldmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vZXZlbnRzL2Zvcm0uZXZlbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7SUFNSSxtQkFBWSxJQUFlO1FBSm5CLHVCQUFrQixHQUFZLEtBQUssQ0FBQztRQUt4QyxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztJQUNyQixDQUFDO0lBRUQsc0JBQUksdUNBQWdCOzs7O1FBQXBCO1lBQ0ksT0FBTyxJQUFJLENBQUMsa0JBQWtCLENBQUM7UUFDbkMsQ0FBQzs7O09BQUE7Ozs7SUFFRCxrQ0FBYzs7O0lBQWQ7UUFDSSxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDO0lBQ25DLENBQUM7SUFDTCxnQkFBQztBQUFELENBQUMsQUFqQkQsSUFpQkM7Ozs7Ozs7SUFmRyx1Q0FBNEM7O0lBRTVDLHlCQUF5QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBGb3JtTW9kZWwgfSBmcm9tICcuLy4uL2NvbXBvbmVudHMvd2lkZ2V0cy9jb3JlL2luZGV4JztcclxuXHJcbmV4cG9ydCBjbGFzcyBGb3JtRXZlbnQge1xyXG5cclxuICAgIHByaXZhdGUgaXNEZWZhdWx0UHJldmVudGVkOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgcmVhZG9ubHkgZm9ybTogRm9ybU1vZGVsO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGZvcm06IEZvcm1Nb2RlbCkge1xyXG4gICAgICAgIHRoaXMuZm9ybSA9IGZvcm07XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGRlZmF1bHRQcmV2ZW50ZWQoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuaXNEZWZhdWx0UHJldmVudGVkO1xyXG4gICAgfVxyXG5cclxuICAgIHByZXZlbnREZWZhdWx0KCkge1xyXG4gICAgICAgIHRoaXMuaXNEZWZhdWx0UHJldmVudGVkID0gdHJ1ZTtcclxuICAgIH1cclxufVxyXG4iXX0=