/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FormFieldEvent } from './form-field.event';
var ValidateDynamicTableRowEvent = /** @class */ (function (_super) {
    tslib_1.__extends(ValidateDynamicTableRowEvent, _super);
    function ValidateDynamicTableRowEvent(form, field, row, summary) {
        var _this = _super.call(this, form, field) || this;
        _this.row = row;
        _this.summary = summary;
        _this.isValid = true;
        return _this;
    }
    return ValidateDynamicTableRowEvent;
}(FormFieldEvent));
export { ValidateDynamicTableRowEvent };
if (false) {
    /** @type {?} */
    ValidateDynamicTableRowEvent.prototype.isValid;
    /** @type {?} */
    ValidateDynamicTableRowEvent.prototype.row;
    /** @type {?} */
    ValidateDynamicTableRowEvent.prototype.summary;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmFsaWRhdGUtZHluYW1pYy10YWJsZS1yb3cuZXZlbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL2V2ZW50cy92YWxpZGF0ZS1keW5hbWljLXRhYmxlLXJvdy5ldmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFxQkEsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRXBEO0lBQWtELHdEQUFjO0lBSTVELHNDQUFZLElBQWUsRUFDZixLQUFxQixFQUNkLEdBQW9CLEVBQ3BCLE9BQW9DO1FBSHZELFlBSUksa0JBQU0sSUFBSSxFQUFFLEtBQUssQ0FBQyxTQUNyQjtRQUhrQixTQUFHLEdBQUgsR0FBRyxDQUFpQjtRQUNwQixhQUFPLEdBQVAsT0FBTyxDQUE2QjtRQUx2RCxhQUFPLEdBQUcsSUFBSSxDQUFDOztJQU9mLENBQUM7SUFFTCxtQ0FBQztBQUFELENBQUMsQUFYRCxDQUFrRCxjQUFjLEdBVy9EOzs7O0lBVEcsK0NBQWU7O0lBSUgsMkNBQTJCOztJQUMzQiwrQ0FBMkMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgRHluYW1pY1Jvd1ZhbGlkYXRpb25TdW1tYXJ5IH0gZnJvbSAnLi4vY29tcG9uZW50cy93aWRnZXRzL2R5bmFtaWMtdGFibGUvZHluYW1pYy1yb3ctdmFsaWRhdGlvbi1zdW1tYXJ5Lm1vZGVsJztcclxuaW1wb3J0IHsgRHluYW1pY1RhYmxlUm93IH0gZnJvbSAnLi4vY29tcG9uZW50cy93aWRnZXRzL2R5bmFtaWMtdGFibGUvZHluYW1pYy10YWJsZS1yb3cubW9kZWwnO1xyXG5cclxuaW1wb3J0IHsgRm9ybUZpZWxkTW9kZWwsIEZvcm1Nb2RlbCB9IGZyb20gJy4vLi4vY29tcG9uZW50cy93aWRnZXRzL2NvcmUvaW5kZXgnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRFdmVudCB9IGZyb20gJy4vZm9ybS1maWVsZC5ldmVudCc7XHJcblxyXG5leHBvcnQgY2xhc3MgVmFsaWRhdGVEeW5hbWljVGFibGVSb3dFdmVudCBleHRlbmRzIEZvcm1GaWVsZEV2ZW50IHtcclxuXHJcbiAgICBpc1ZhbGlkID0gdHJ1ZTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihmb3JtOiBGb3JtTW9kZWwsXHJcbiAgICAgICAgICAgICAgICBmaWVsZDogRm9ybUZpZWxkTW9kZWwsXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgcm93OiBEeW5hbWljVGFibGVSb3csXHJcbiAgICAgICAgICAgICAgICBwdWJsaWMgc3VtbWFyeTogRHluYW1pY1Jvd1ZhbGlkYXRpb25TdW1tYXJ5KSB7XHJcbiAgICAgICAgc3VwZXIoZm9ybSwgZmllbGQpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=