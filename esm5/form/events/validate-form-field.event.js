/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FormFieldEvent } from './form-field.event';
var ValidateFormFieldEvent = /** @class */ (function (_super) {
    tslib_1.__extends(ValidateFormFieldEvent, _super);
    function ValidateFormFieldEvent(form, field) {
        var _this = _super.call(this, form, field) || this;
        _this.isValid = true;
        return _this;
    }
    return ValidateFormFieldEvent;
}(FormFieldEvent));
export { ValidateFormFieldEvent };
if (false) {
    /** @type {?} */
    ValidateFormFieldEvent.prototype.isValid;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmFsaWRhdGUtZm9ybS1maWVsZC5ldmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vZXZlbnRzL3ZhbGlkYXRlLWZvcm0tZmllbGQuZXZlbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBa0JBLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUVwRDtJQUE0QyxrREFBYztJQUl0RCxnQ0FBWSxJQUFlLEVBQUUsS0FBcUI7UUFBbEQsWUFDSSxrQkFBTSxJQUFJLEVBQUUsS0FBSyxDQUFDLFNBQ3JCO1FBSkQsYUFBTyxHQUFHLElBQUksQ0FBQzs7SUFJZixDQUFDO0lBRUwsNkJBQUM7QUFBRCxDQUFDLEFBUkQsQ0FBNEMsY0FBYyxHQVF6RDs7OztJQU5HLHlDQUFlIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEZvcm1GaWVsZE1vZGVsLCBGb3JtTW9kZWwgfSBmcm9tICcuLy4uL2NvbXBvbmVudHMvd2lkZ2V0cy9jb3JlL2luZGV4JztcclxuaW1wb3J0IHsgRm9ybUZpZWxkRXZlbnQgfSBmcm9tICcuL2Zvcm0tZmllbGQuZXZlbnQnO1xyXG5cclxuZXhwb3J0IGNsYXNzIFZhbGlkYXRlRm9ybUZpZWxkRXZlbnQgZXh0ZW5kcyBGb3JtRmllbGRFdmVudCB7XHJcblxyXG4gICAgaXNWYWxpZCA9IHRydWU7XHJcblxyXG4gICAgY29uc3RydWN0b3IoZm9ybTogRm9ybU1vZGVsLCBmaWVsZDogRm9ybUZpZWxkTW9kZWwpIHtcclxuICAgICAgICBzdXBlcihmb3JtLCBmaWVsZCk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==