/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { DataTableModule } from '../datatable/datatable.module';
import { DataColumnModule } from '../data-column/data-column.module';
import { PipeModule } from '../pipes/pipe.module';
import { HttpClientModule } from '@angular/common/http';
import { MaterialModule } from '../material.module';
import { MASK_DIRECTIVE, WIDGET_DIRECTIVES } from './components/widgets/index';
import { StartFormCustomButtonDirective } from './components/form-custom-button.directive';
import { FormFieldComponent } from './components/form-field/form-field.component';
import { FormListComponent } from './components/form-list.component';
import { ContentWidgetComponent } from './components/widgets/content/content.widget';
import { WidgetComponent } from './components/widgets/widget.component';
import { MatDatetimepickerModule, MatNativeDatetimeModule } from '@mat-datetimepicker/core';
import { FormRendererComponent } from './components/form-renderer.component';
var FormBaseModule = /** @class */ (function () {
    function FormBaseModule() {
    }
    FormBaseModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        DataTableModule,
                        HttpClientModule,
                        MaterialModule,
                        TranslateModule.forChild(),
                        FormsModule,
                        ReactiveFormsModule,
                        DataColumnModule,
                        PipeModule,
                        MatDatetimepickerModule,
                        MatNativeDatetimeModule
                    ],
                    declarations: tslib_1.__spread([
                        ContentWidgetComponent,
                        FormFieldComponent,
                        FormListComponent,
                        FormRendererComponent,
                        StartFormCustomButtonDirective
                    ], WIDGET_DIRECTIVES, MASK_DIRECTIVE, [
                        WidgetComponent
                    ]),
                    entryComponents: tslib_1.__spread(WIDGET_DIRECTIVES),
                    exports: tslib_1.__spread([
                        ContentWidgetComponent,
                        FormFieldComponent,
                        FormListComponent,
                        FormRendererComponent,
                        StartFormCustomButtonDirective
                    ], WIDGET_DIRECTIVES)
                },] }
    ];
    return FormBaseModule;
}());
export { FormBaseModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1iYXNlLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vZm9ybS1iYXNlLm1vZHVsZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxXQUFXLEVBQUUsbUJBQW1CLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsRSxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RCxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDaEUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDckUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBRXhELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUVwRCxPQUFPLEVBQUUsY0FBYyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFFL0UsT0FBTyxFQUFFLDhCQUE4QixFQUFFLE1BQU0sMkNBQTJDLENBQUM7QUFFM0YsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDbEYsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDckUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNkNBQTZDLENBQUM7QUFDckYsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQ3hFLE9BQU8sRUFBRSx1QkFBdUIsRUFBRSx1QkFBdUIsRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQzVGLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBRTdFO0lBQUE7SUFxQ0EsQ0FBQzs7Z0JBckNBLFFBQVEsU0FBQztvQkFDTixPQUFPLEVBQUU7d0JBQ0wsWUFBWTt3QkFDWixlQUFlO3dCQUNmLGdCQUFnQjt3QkFDaEIsY0FBYzt3QkFDZCxlQUFlLENBQUMsUUFBUSxFQUFFO3dCQUMxQixXQUFXO3dCQUNYLG1CQUFtQjt3QkFDbkIsZ0JBQWdCO3dCQUNoQixVQUFVO3dCQUNWLHVCQUF1Qjt3QkFDdkIsdUJBQXVCO3FCQUMxQjtvQkFDRCxZQUFZO3dCQUNSLHNCQUFzQjt3QkFDdEIsa0JBQWtCO3dCQUNsQixpQkFBaUI7d0JBQ2pCLHFCQUFxQjt3QkFDckIsOEJBQThCO3VCQUMzQixpQkFBaUIsRUFDakIsY0FBYzt3QkFDakIsZUFBZTtzQkFDbEI7b0JBQ0QsZUFBZSxtQkFDUixpQkFBaUIsQ0FDdkI7b0JBQ0QsT0FBTzt3QkFDSCxzQkFBc0I7d0JBQ3RCLGtCQUFrQjt3QkFDbEIsaUJBQWlCO3dCQUNqQixxQkFBcUI7d0JBQ3JCLDhCQUE4Qjt1QkFDM0IsaUJBQWlCLENBQ3ZCO2lCQUNKOztJQUVELHFCQUFDO0NBQUEsQUFyQ0QsSUFxQ0M7U0FEWSxjQUFjIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IEZvcm1zTW9kdWxlLCBSZWFjdGl2ZUZvcm1zTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVNb2R1bGUgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuaW1wb3J0IHsgRGF0YVRhYmxlTW9kdWxlIH0gZnJvbSAnLi4vZGF0YXRhYmxlL2RhdGF0YWJsZS5tb2R1bGUnO1xyXG5pbXBvcnQgeyBEYXRhQ29sdW1uTW9kdWxlIH0gZnJvbSAnLi4vZGF0YS1jb2x1bW4vZGF0YS1jb2x1bW4ubW9kdWxlJztcclxuaW1wb3J0IHsgUGlwZU1vZHVsZSB9IGZyb20gJy4uL3BpcGVzL3BpcGUubW9kdWxlJztcclxuaW1wb3J0IHsgSHR0cENsaWVudE1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuXHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSAnLi4vbWF0ZXJpYWwubW9kdWxlJztcclxuXHJcbmltcG9ydCB7IE1BU0tfRElSRUNUSVZFLCBXSURHRVRfRElSRUNUSVZFUyB9IGZyb20gJy4vY29tcG9uZW50cy93aWRnZXRzL2luZGV4JztcclxuXHJcbmltcG9ydCB7IFN0YXJ0Rm9ybUN1c3RvbUJ1dHRvbkRpcmVjdGl2ZSB9IGZyb20gJy4vY29tcG9uZW50cy9mb3JtLWN1c3RvbS1idXR0b24uZGlyZWN0aXZlJztcclxuXHJcbmltcG9ydCB7IEZvcm1GaWVsZENvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mb3JtLWZpZWxkL2Zvcm0tZmllbGQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgRm9ybUxpc3RDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvZm9ybS1saXN0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IENvbnRlbnRXaWRnZXRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvd2lkZ2V0cy9jb250ZW50L2NvbnRlbnQud2lkZ2V0JztcclxuaW1wb3J0IHsgV2lkZ2V0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL3dpZGdldHMvd2lkZ2V0LmNvbXBvbmVudCc7XHJcbmltcG9ydCB7IE1hdERhdGV0aW1lcGlja2VyTW9kdWxlLCBNYXROYXRpdmVEYXRldGltZU1vZHVsZSB9IGZyb20gJ0BtYXQtZGF0ZXRpbWVwaWNrZXIvY29yZSc7XHJcbmltcG9ydCB7IEZvcm1SZW5kZXJlckNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9mb3JtLXJlbmRlcmVyLmNvbXBvbmVudCc7XHJcblxyXG5ATmdNb2R1bGUoe1xyXG4gICAgaW1wb3J0czogW1xyXG4gICAgICAgIENvbW1vbk1vZHVsZSxcclxuICAgICAgICBEYXRhVGFibGVNb2R1bGUsXHJcbiAgICAgICAgSHR0cENsaWVudE1vZHVsZSxcclxuICAgICAgICBNYXRlcmlhbE1vZHVsZSxcclxuICAgICAgICBUcmFuc2xhdGVNb2R1bGUuZm9yQ2hpbGQoKSxcclxuICAgICAgICBGb3Jtc01vZHVsZSxcclxuICAgICAgICBSZWFjdGl2ZUZvcm1zTW9kdWxlLFxyXG4gICAgICAgIERhdGFDb2x1bW5Nb2R1bGUsXHJcbiAgICAgICAgUGlwZU1vZHVsZSxcclxuICAgICAgICBNYXREYXRldGltZXBpY2tlck1vZHVsZSxcclxuICAgICAgICBNYXROYXRpdmVEYXRldGltZU1vZHVsZVxyXG4gICAgXSxcclxuICAgIGRlY2xhcmF0aW9uczogW1xyXG4gICAgICAgIENvbnRlbnRXaWRnZXRDb21wb25lbnQsXHJcbiAgICAgICAgRm9ybUZpZWxkQ29tcG9uZW50LFxyXG4gICAgICAgIEZvcm1MaXN0Q29tcG9uZW50LFxyXG4gICAgICAgIEZvcm1SZW5kZXJlckNvbXBvbmVudCxcclxuICAgICAgICBTdGFydEZvcm1DdXN0b21CdXR0b25EaXJlY3RpdmUsXHJcbiAgICAgICAgLi4uV0lER0VUX0RJUkVDVElWRVMsXHJcbiAgICAgICAgLi4uTUFTS19ESVJFQ1RJVkUsXHJcbiAgICAgICAgV2lkZ2V0Q29tcG9uZW50XHJcbiAgICBdLFxyXG4gICAgZW50cnlDb21wb25lbnRzOiBbXHJcbiAgICAgICAgLi4uV0lER0VUX0RJUkVDVElWRVNcclxuICAgIF0sXHJcbiAgICBleHBvcnRzOiBbXHJcbiAgICAgICAgQ29udGVudFdpZGdldENvbXBvbmVudCxcclxuICAgICAgICBGb3JtRmllbGRDb21wb25lbnQsXHJcbiAgICAgICAgRm9ybUxpc3RDb21wb25lbnQsXHJcbiAgICAgICAgRm9ybVJlbmRlcmVyQ29tcG9uZW50LFxyXG4gICAgICAgIFN0YXJ0Rm9ybUN1c3RvbUJ1dHRvbkRpcmVjdGl2ZSxcclxuICAgICAgICAuLi5XSURHRVRfRElSRUNUSVZFU1xyXG4gICAgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9ybUJhc2VNb2R1bGUge1xyXG59XHJcbiJdfQ==