/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FormSaveRepresentation } from '@alfresco/js-api';
var FormDefinitionModel = /** @class */ (function (_super) {
    tslib_1.__extends(FormDefinitionModel, _super);
    function FormDefinitionModel(id, name, lastUpdatedByFullName, lastUpdated, metadata) {
        var _this = _super.call(this) || this;
        _this.reusable = false;
        _this.newVersion = false;
        _this.formImageBase64 = '';
        _this.formRepresentation = {
            id: id,
            name: name,
            description: '',
            version: 1,
            lastUpdatedBy: 1,
            lastUpdatedByFullName: lastUpdatedByFullName,
            lastUpdated: lastUpdated,
            stencilSetId: 0,
            referenceId: null,
            formDefinition: {
                fields: [{
                        name: 'Label',
                        type: 'container',
                        fieldType: 'ContainerRepresentation',
                        numberOfColumns: 2,
                        required: false,
                        readOnly: false,
                        sizeX: 2,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        fields: { '1': _this.metadataToFields(metadata) }
                    }],
                gridsterForm: false,
                javascriptEvents: [],
                metadata: {},
                outcomes: [],
                className: '',
                style: '',
                tabs: [],
                variables: []
            }
        };
        return _this;
    }
    /**
     * @private
     * @param {?} metadata
     * @return {?}
     */
    FormDefinitionModel.prototype.metadataToFields = /**
     * @private
     * @param {?} metadata
     * @return {?}
     */
    function (metadata) {
        /** @type {?} */
        var fields = [];
        if (metadata) {
            metadata.forEach((/**
             * @param {?} property
             * @return {?}
             */
            function (property) {
                if (property) {
                    /** @type {?} */
                    var field = {
                        type: 'text',
                        id: property.name,
                        name: property.name,
                        required: false,
                        readOnly: false,
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        colspan: 1,
                        params: {
                            existingColspan: 1,
                            maxColspan: 2
                        },
                        layout: {
                            colspan: 1,
                            row: -1,
                            column: -1
                        }
                    };
                    fields.push(field);
                }
            }));
        }
        return fields;
    };
    return FormDefinitionModel;
}(FormSaveRepresentation));
export { FormDefinitionModel };
if (false) {
    /** @type {?} */
    FormDefinitionModel.prototype.reusable;
    /** @type {?} */
    FormDefinitionModel.prototype.newVersion;
    /** @type {?} */
    FormDefinitionModel.prototype.formRepresentation;
    /** @type {?} */
    FormDefinitionModel.prototype.formImageBase64;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS1kZWZpbml0aW9uLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9tb2RlbHMvZm9ybS1kZWZpbml0aW9uLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUUxRDtJQUF5QywrQ0FBc0I7SUFNM0QsNkJBQVksRUFBVSxFQUFFLElBQVMsRUFBRSxxQkFBNkIsRUFBRSxXQUFtQixFQUFFLFFBQWE7UUFBcEcsWUFDSSxpQkFBTyxTQW1DVjtRQXpDRCxjQUFRLEdBQVksS0FBSyxDQUFDO1FBQzFCLGdCQUFVLEdBQVksS0FBSyxDQUFDO1FBRTVCLHFCQUFlLEdBQVcsRUFBRSxDQUFDO1FBSXpCLEtBQUksQ0FBQyxrQkFBa0IsR0FBRztZQUN0QixFQUFFLEVBQUUsRUFBRTtZQUNOLElBQUksRUFBRSxJQUFJO1lBQ1YsV0FBVyxFQUFFLEVBQUU7WUFDZixPQUFPLEVBQUUsQ0FBQztZQUNWLGFBQWEsRUFBRSxDQUFDO1lBQ2hCLHFCQUFxQixFQUFFLHFCQUFxQjtZQUM1QyxXQUFXLEVBQUUsV0FBVztZQUN4QixZQUFZLEVBQUUsQ0FBQztZQUNmLFdBQVcsRUFBRSxJQUFJO1lBQ2pCLGNBQWMsRUFBRTtnQkFDWixNQUFNLEVBQUUsQ0FBQzt3QkFDTCxJQUFJLEVBQUUsT0FBTzt3QkFDYixJQUFJLEVBQUUsV0FBVzt3QkFDakIsU0FBUyxFQUFFLHlCQUF5Qjt3QkFDcEMsZUFBZSxFQUFFLENBQUM7d0JBQ2xCLFFBQVEsRUFBRSxLQUFLO3dCQUNmLFFBQVEsRUFBRSxLQUFLO3dCQUNmLEtBQUssRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxDQUFDO3dCQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxNQUFNLEVBQUUsRUFBQyxHQUFHLEVBQUUsS0FBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxFQUFDO3FCQUNqRCxDQUFDO2dCQUNGLFlBQVksRUFBRSxLQUFLO2dCQUNuQixnQkFBZ0IsRUFBRSxFQUFFO2dCQUNwQixRQUFRLEVBQUUsRUFBRTtnQkFDWixRQUFRLEVBQUUsRUFBRTtnQkFDWixTQUFTLEVBQUUsRUFBRTtnQkFDYixLQUFLLEVBQUUsRUFBRTtnQkFDVCxJQUFJLEVBQUUsRUFBRTtnQkFDUixTQUFTLEVBQUUsRUFBRTthQUNoQjtTQUNKLENBQUM7O0lBQ04sQ0FBQzs7Ozs7O0lBRU8sOENBQWdCOzs7OztJQUF4QixVQUF5QixRQUFhOztZQUM1QixNQUFNLEdBQUcsRUFBRTtRQUNqQixJQUFJLFFBQVEsRUFBRTtZQUNWLFFBQVEsQ0FBQyxPQUFPOzs7O1lBQUMsVUFBUyxRQUFRO2dCQUM5QixJQUFJLFFBQVEsRUFBRTs7d0JBQ0osS0FBSyxHQUFHO3dCQUNWLElBQUksRUFBRSxNQUFNO3dCQUNaLEVBQUUsRUFBRSxRQUFRLENBQUMsSUFBSTt3QkFDakIsSUFBSSxFQUFFLFFBQVEsQ0FBQyxJQUFJO3dCQUNuQixRQUFRLEVBQUUsS0FBSzt3QkFDZixRQUFRLEVBQUUsS0FBSzt3QkFDZixLQUFLLEVBQUUsQ0FBQzt3QkFDUixLQUFLLEVBQUUsQ0FBQzt3QkFDUixHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsT0FBTyxFQUFFLENBQUM7d0JBQ1YsTUFBTSxFQUFFOzRCQUNKLGVBQWUsRUFBRSxDQUFDOzRCQUNsQixVQUFVLEVBQUUsQ0FBQzt5QkFDaEI7d0JBQ0QsTUFBTSxFQUFFOzRCQUNKLE9BQU8sRUFBRSxDQUFDOzRCQUNWLEdBQUcsRUFBRSxDQUFDLENBQUM7NEJBQ1AsTUFBTSxFQUFFLENBQUMsQ0FBQzt5QkFDYjtxQkFDSjtvQkFDRCxNQUFNLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2lCQUN0QjtZQUNMLENBQUMsRUFBQyxDQUFDO1NBQ047UUFFRCxPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDO0lBQ0wsMEJBQUM7QUFBRCxDQUFDLEFBN0VELENBQXlDLHNCQUFzQixHQTZFOUQ7Ozs7SUE1RUcsdUNBQTBCOztJQUMxQix5Q0FBNEI7O0lBQzVCLGlEQUF3Qjs7SUFDeEIsOENBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEZvcm1TYXZlUmVwcmVzZW50YXRpb24gfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuXHJcbmV4cG9ydCBjbGFzcyBGb3JtRGVmaW5pdGlvbk1vZGVsIGV4dGVuZHMgRm9ybVNhdmVSZXByZXNlbnRhdGlvbiB7XHJcbiAgICByZXVzYWJsZTogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgbmV3VmVyc2lvbjogYm9vbGVhbiA9IGZhbHNlO1xyXG4gICAgZm9ybVJlcHJlc2VudGF0aW9uOiBhbnk7XHJcbiAgICBmb3JtSW1hZ2VCYXNlNjQ6IHN0cmluZyA9ICcnO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGlkOiBzdHJpbmcsIG5hbWU6IGFueSwgbGFzdFVwZGF0ZWRCeUZ1bGxOYW1lOiBzdHJpbmcsIGxhc3RVcGRhdGVkOiBzdHJpbmcsIG1ldGFkYXRhOiBhbnkpIHtcclxuICAgICAgICBzdXBlcigpO1xyXG4gICAgICAgIHRoaXMuZm9ybVJlcHJlc2VudGF0aW9uID0ge1xyXG4gICAgICAgICAgICBpZDogaWQsXHJcbiAgICAgICAgICAgIG5hbWU6IG5hbWUsXHJcbiAgICAgICAgICAgIGRlc2NyaXB0aW9uOiAnJyxcclxuICAgICAgICAgICAgdmVyc2lvbjogMSxcclxuICAgICAgICAgICAgbGFzdFVwZGF0ZWRCeTogMSxcclxuICAgICAgICAgICAgbGFzdFVwZGF0ZWRCeUZ1bGxOYW1lOiBsYXN0VXBkYXRlZEJ5RnVsbE5hbWUsXHJcbiAgICAgICAgICAgIGxhc3RVcGRhdGVkOiBsYXN0VXBkYXRlZCxcclxuICAgICAgICAgICAgc3RlbmNpbFNldElkOiAwLFxyXG4gICAgICAgICAgICByZWZlcmVuY2VJZDogbnVsbCxcclxuICAgICAgICAgICAgZm9ybURlZmluaXRpb246IHtcclxuICAgICAgICAgICAgICAgIGZpZWxkczogW3tcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiAnTGFiZWwnLFxyXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICdjb250YWluZXInLFxyXG4gICAgICAgICAgICAgICAgICAgIGZpZWxkVHlwZTogJ0NvbnRhaW5lclJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgICAgICAgICBudW1iZXJPZkNvbHVtbnM6IDIsXHJcbiAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICBzaXplWDogMixcclxuICAgICAgICAgICAgICAgICAgICBzaXplWTogMSxcclxuICAgICAgICAgICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgZmllbGRzOiB7JzEnOiB0aGlzLm1ldGFkYXRhVG9GaWVsZHMobWV0YWRhdGEpfVxyXG4gICAgICAgICAgICAgICAgfV0sXHJcbiAgICAgICAgICAgICAgICBncmlkc3RlckZvcm06IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgamF2YXNjcmlwdEV2ZW50czogW10sXHJcbiAgICAgICAgICAgICAgICBtZXRhZGF0YToge30sXHJcbiAgICAgICAgICAgICAgICBvdXRjb21lczogW10sXHJcbiAgICAgICAgICAgICAgICBjbGFzc05hbWU6ICcnLFxyXG4gICAgICAgICAgICAgICAgc3R5bGU6ICcnLFxyXG4gICAgICAgICAgICAgICAgdGFiczogW10sXHJcbiAgICAgICAgICAgICAgICB2YXJpYWJsZXM6IFtdXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgbWV0YWRhdGFUb0ZpZWxkcyhtZXRhZGF0YTogYW55KTogYW55W10ge1xyXG4gICAgICAgIGNvbnN0IGZpZWxkcyA9IFtdO1xyXG4gICAgICAgIGlmIChtZXRhZGF0YSkge1xyXG4gICAgICAgICAgICBtZXRhZGF0YS5mb3JFYWNoKGZ1bmN0aW9uKHByb3BlcnR5KSB7XHJcbiAgICAgICAgICAgICAgICBpZiAocHJvcGVydHkpIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBmaWVsZCA9IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3RleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogcHJvcGVydHkubmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogcHJvcGVydHkubmFtZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpemVYOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWTogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sOiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGlzdGluZ0NvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhDb2xzcGFuOiAyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxheW91dDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2x1bW46IC0xXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIGZpZWxkcy5wdXNoKGZpZWxkKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZmllbGRzO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==