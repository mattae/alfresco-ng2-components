/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var WidgetVisibilityModel = /** @class */ (function () {
    function WidgetVisibilityModel(json) {
        this.json = json;
        if (json) {
            this.operator = json.operator;
            this.nextCondition = new WidgetVisibilityModel(json.nextCondition);
            this.nextConditionOperator = json.nextConditionOperator;
            this.rightRestResponseId = json.rightRestResponseId;
            this.rightFormFieldId = json.rightFormFieldId;
            this.leftFormFieldId = json.leftFormFieldId;
            this.leftRestResponseId = json.leftRestResponseId;
        }
        else {
            this.json = {};
        }
    }
    Object.defineProperty(WidgetVisibilityModel.prototype, "leftType", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.leftFormFieldId) {
                return WidgetTypeEnum.field;
            }
            else if (this.leftRestResponseId) {
                return WidgetTypeEnum.variable;
            }
            else if (!!this.json.leftType) {
                return this.json.leftType;
            }
        },
        set: /**
         * @param {?} leftType
         * @return {?}
         */
        function (leftType) {
            this.json.leftType = leftType;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetVisibilityModel.prototype, "rightType", {
        get: /**
         * @return {?}
         */
        function () {
            if (!!this.json.rightType) {
                return this.json.rightType;
            }
            else if (this.json.rightValue) {
                return WidgetTypeEnum.value;
            }
            else if (this.rightRestResponseId) {
                return WidgetTypeEnum.variable;
            }
            else if (this.rightFormFieldId) {
                return WidgetTypeEnum.field;
            }
        },
        set: /**
         * @param {?} rightType
         * @return {?}
         */
        function (rightType) {
            this.json.rightType = rightType;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetVisibilityModel.prototype, "leftValue", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.json.leftValue) {
                return this.json.leftValue;
            }
            else if (this.leftFormFieldId) {
                return this.leftFormFieldId;
            }
            else {
                return this.leftRestResponseId;
            }
        },
        set: /**
         * @param {?} leftValue
         * @return {?}
         */
        function (leftValue) {
            this.json.leftValue = leftValue;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(WidgetVisibilityModel.prototype, "rightValue", {
        get: /**
         * @return {?}
         */
        function () {
            if (this.json.rightValue) {
                return this.json.rightValue;
            }
            else if (this.rightFormFieldId) {
                return this.rightFormFieldId;
            }
            else {
                return this.rightRestResponseId;
            }
        },
        set: /**
         * @param {?} rightValue
         * @return {?}
         */
        function (rightValue) {
            this.json.rightValue = rightValue;
        },
        enumerable: true,
        configurable: true
    });
    return WidgetVisibilityModel;
}());
export { WidgetVisibilityModel };
if (false) {
    /** @type {?} */
    WidgetVisibilityModel.prototype.rightRestResponseId;
    /** @type {?} */
    WidgetVisibilityModel.prototype.rightFormFieldId;
    /** @type {?} */
    WidgetVisibilityModel.prototype.leftRestResponseId;
    /** @type {?} */
    WidgetVisibilityModel.prototype.leftFormFieldId;
    /** @type {?} */
    WidgetVisibilityModel.prototype.operator;
    /** @type {?} */
    WidgetVisibilityModel.prototype.nextCondition;
    /** @type {?} */
    WidgetVisibilityModel.prototype.nextConditionOperator;
    /**
     * @type {?}
     * @private
     */
    WidgetVisibilityModel.prototype.json;
}
/** @enum {string} */
var WidgetTypeEnum = {
    field: 'field',
    variable: 'variable',
    value: 'value',
};
export { WidgetTypeEnum };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0LXZpc2liaWxpdHkubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL21vZGVscy93aWRnZXQtdmlzaWJpbGl0eS5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQTtJQVNRLCtCQUFvQixJQUFVO1FBQVYsU0FBSSxHQUFKLElBQUksQ0FBTTtRQUN0QixJQUFJLElBQUksRUFBRTtZQUNGLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUM5QixJQUFJLENBQUMsYUFBYSxHQUFHLElBQUkscUJBQXFCLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1lBQ25FLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMscUJBQXFCLENBQUM7WUFDeEQsSUFBSSxDQUFDLG1CQUFtQixHQUFHLElBQUksQ0FBQyxtQkFBbUIsQ0FBQztZQUNwRCxJQUFJLENBQUMsZ0JBQWdCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1lBQzlDLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQztZQUM1QyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDO1NBQ3pEO2FBQU07WUFDQyxJQUFJLENBQUMsSUFBSSxHQUFHLEVBQUUsQ0FBQztTQUN0QjtJQUNULENBQUM7SUFFRCxzQkFBSSwyQ0FBUTs7OztRQWdCWjtZQUNRLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtnQkFDbEIsT0FBTyxjQUFjLENBQUMsS0FBSyxDQUFDO2FBQ25DO2lCQUFNLElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO2dCQUM1QixPQUFPLGNBQWMsQ0FBQyxRQUFRLENBQUM7YUFDdEM7aUJBQU0sSUFBSyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLEVBQUU7Z0JBQzFCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7YUFDakM7UUFDVCxDQUFDOzs7OztRQXhCRCxVQUFhLFFBQWdCO1lBQ3JCLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLFFBQVEsQ0FBQztRQUN0QyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDRDQUFTOzs7O1FBZ0NiO1lBQ1EsSUFBSyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEVBQUc7Z0JBQ3JCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7YUFDbEM7aUJBQU0sSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRTtnQkFDekIsT0FBTyxjQUFjLENBQUMsS0FBSyxDQUFDO2FBQ25DO2lCQUFNLElBQUksSUFBSSxDQUFDLG1CQUFtQixFQUFFO2dCQUM3QixPQUFPLGNBQWMsQ0FBQyxRQUFRLENBQUM7YUFDdEM7aUJBQU0sSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQzFCLE9BQU8sY0FBYyxDQUFDLEtBQUssQ0FBQzthQUNuQztRQUNULENBQUM7Ozs7O1FBMUNELFVBQWMsU0FBaUI7WUFDdkIsSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO1FBQ3hDLENBQUM7OztPQUFBO0lBRUQsc0JBQUksNENBQVM7Ozs7UUFrQmI7WUFDUSxJQUFLLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFHO2dCQUNuQixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDO2FBQ2xDO2lCQUFNLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtnQkFDekIsT0FBTyxJQUFJLENBQUMsZUFBZSxDQUFDO2FBQ25DO2lCQUFNO2dCQUNDLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDO2FBQ3RDO1FBQ1QsQ0FBQzs7Ozs7UUExQkQsVUFBYyxTQUFpQjtZQUN2QixJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7UUFDeEMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSw2Q0FBVTs7OztRQW9DZDtZQUNRLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7Z0JBQ2xCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUM7YUFDbkM7aUJBQU0sSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQzFCLE9BQU8sSUFBSSxDQUFDLGdCQUFnQixDQUFDO2FBQ3BDO2lCQUFNO2dCQUNDLE9BQU8sSUFBSSxDQUFDLG1CQUFtQixDQUFDO2FBQ3ZDO1FBQ1QsQ0FBQzs7Ozs7UUE1Q0QsVUFBZSxVQUFrQjtZQUN6QixJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7UUFDMUMsQ0FBQzs7O09BQUE7SUE0Q1QsNEJBQUM7QUFBRCxDQUFDLEFBakZELElBaUZDOzs7O0lBaEZPLG9EQUE2Qjs7SUFDN0IsaURBQTBCOztJQUMxQixtREFBNEI7O0lBQzVCLGdEQUF5Qjs7SUFDekIseUNBQWlCOztJQUNqQiw4Q0FBcUM7O0lBQ3JDLHNEQUE4Qjs7Ozs7SUFFbEIscUNBQWtCOzs7O0lBMkU5QixPQUFRLE9BQU87SUFDZixVQUFXLFVBQVU7SUFDckIsT0FBUSxPQUFPIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmV4cG9ydCBjbGFzcyBXaWRnZXRWaXNpYmlsaXR5TW9kZWwge1xyXG4gICAgICAgIHJpZ2h0UmVzdFJlc3BvbnNlSWQ/OiBzdHJpbmc7XHJcbiAgICAgICAgcmlnaHRGb3JtRmllbGRJZD86IHN0cmluZztcclxuICAgICAgICBsZWZ0UmVzdFJlc3BvbnNlSWQ/OiBzdHJpbmc7XHJcbiAgICAgICAgbGVmdEZvcm1GaWVsZElkPzogc3RyaW5nO1xyXG4gICAgICAgIG9wZXJhdG9yOiBzdHJpbmc7XHJcbiAgICAgICAgbmV4dENvbmRpdGlvbjogV2lkZ2V0VmlzaWJpbGl0eU1vZGVsO1xyXG4gICAgICAgIG5leHRDb25kaXRpb25PcGVyYXRvcjogc3RyaW5nO1xyXG5cclxuICAgICAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGpzb24/OiBhbnkpIHtcclxuICAgICAgICAgICAgICAgIGlmIChqc29uKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMub3BlcmF0b3IgPSBqc29uLm9wZXJhdG9yO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLm5leHRDb25kaXRpb24gPSBuZXcgV2lkZ2V0VmlzaWJpbGl0eU1vZGVsKGpzb24ubmV4dENvbmRpdGlvbik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubmV4dENvbmRpdGlvbk9wZXJhdG9yID0ganNvbi5uZXh0Q29uZGl0aW9uT3BlcmF0b3I7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucmlnaHRSZXN0UmVzcG9uc2VJZCA9IGpzb24ucmlnaHRSZXN0UmVzcG9uc2VJZDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yaWdodEZvcm1GaWVsZElkID0ganNvbi5yaWdodEZvcm1GaWVsZElkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxlZnRGb3JtRmllbGRJZCA9IGpzb24ubGVmdEZvcm1GaWVsZElkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxlZnRSZXN0UmVzcG9uc2VJZCA9IGpzb24ubGVmdFJlc3RSZXNwb25zZUlkO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5qc29uID0ge307XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBzZXQgbGVmdFR5cGUobGVmdFR5cGU6IHN0cmluZykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5qc29uLmxlZnRUeXBlID0gbGVmdFR5cGU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBzZXQgcmlnaHRUeXBlKHJpZ2h0VHlwZTogc3RyaW5nKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmpzb24ucmlnaHRUeXBlID0gcmlnaHRUeXBlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgc2V0IGxlZnRWYWx1ZShsZWZ0VmFsdWU6IHN0cmluZykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5qc29uLmxlZnRWYWx1ZSA9IGxlZnRWYWx1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHNldCByaWdodFZhbHVlKHJpZ2h0VmFsdWU6IHN0cmluZykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5qc29uLnJpZ2h0VmFsdWUgPSByaWdodFZhbHVlO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgZ2V0IGxlZnRUeXBlKCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMubGVmdEZvcm1GaWVsZElkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBXaWRnZXRUeXBlRW51bS5maWVsZDtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5sZWZ0UmVzdFJlc3BvbnNlSWQpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIFdpZGdldFR5cGVFbnVtLnZhcmlhYmxlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICggISF0aGlzLmpzb24ubGVmdFR5cGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuanNvbi5sZWZ0VHlwZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGdldCBsZWZ0VmFsdWUoKSB7XHJcbiAgICAgICAgICAgICAgICBpZiAoIHRoaXMuanNvbi5sZWZ0VmFsdWUgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmpzb24ubGVmdFZhbHVlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLmxlZnRGb3JtRmllbGRJZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5sZWZ0Rm9ybUZpZWxkSWQ7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5sZWZ0UmVzdFJlc3BvbnNlSWQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBnZXQgcmlnaHRUeXBlKCkge1xyXG4gICAgICAgICAgICAgICAgaWYgKCAhIXRoaXMuanNvbi5yaWdodFR5cGUgKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmpzb24ucmlnaHRUeXBlO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICh0aGlzLmpzb24ucmlnaHRWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gV2lkZ2V0VHlwZUVudW0udmFsdWU7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMucmlnaHRSZXN0UmVzcG9uc2VJZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gV2lkZ2V0VHlwZUVudW0udmFyaWFibGU7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMucmlnaHRGb3JtRmllbGRJZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gV2lkZ2V0VHlwZUVudW0uZmllbGQ7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBnZXQgcmlnaHRWYWx1ZSgpIHtcclxuICAgICAgICAgICAgICAgIGlmICh0aGlzLmpzb24ucmlnaHRWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5qc29uLnJpZ2h0VmFsdWU7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMucmlnaHRGb3JtRmllbGRJZCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5yaWdodEZvcm1GaWVsZElkO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMucmlnaHRSZXN0UmVzcG9uc2VJZDtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG59XHJcblxyXG5leHBvcnQgZW51bSBXaWRnZXRUeXBlRW51bSB7XHJcbiAgICAgICAgZmllbGQgPSAnZmllbGQnLFxyXG4gICAgICAgIHZhcmlhYmxlID0gJ3ZhcmlhYmxlJyxcclxuICAgICAgICB2YWx1ZSA9ICd2YWx1ZSdcclxufVxyXG4iXX0=