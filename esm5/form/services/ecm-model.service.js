/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { LogService } from '../../services/log.service';
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { Injectable } from '@angular/core';
import { Observable, from } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "../../services/alfresco-api.service";
import * as i2 from "../../services/log.service";
var EcmModelService = /** @class */ (function () {
    function EcmModelService(apiService, logService) {
        this.apiService = apiService;
        this.logService = logService;
    }
    /**
     * @param {?} formName
     * @param {?} form
     * @return {?}
     */
    EcmModelService.prototype.createEcmTypeForActivitiForm = /**
     * @param {?} formName
     * @param {?} form
     * @return {?}
     */
    function (formName, form) {
        var _this = this;
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        function (observer) {
            _this.searchActivitiEcmModel().subscribe((/**
             * @param {?} model
             * @return {?}
             */
            function (model) {
                if (!model) {
                    _this.createActivitiEcmModel(formName, form).subscribe((/**
                     * @param {?} typeForm
                     * @return {?}
                     */
                    function (typeForm) {
                        observer.next(typeForm);
                        observer.complete();
                    }));
                }
                else {
                    _this.saveFomType(formName, form).subscribe((/**
                     * @param {?} typeForm
                     * @return {?}
                     */
                    function (typeForm) {
                        observer.next(typeForm);
                        observer.complete();
                    }));
                }
            }), (/**
             * @param {?} err
             * @return {?}
             */
            function (err) { return _this.handleError(err); }));
        }));
    };
    /**
     * @return {?}
     */
    EcmModelService.prototype.searchActivitiEcmModel = /**
     * @return {?}
     */
    function () {
        return this.getEcmModels().pipe(map((/**
         * @param {?} ecmModels
         * @return {?}
         */
        function (ecmModels) {
            return ecmModels.list.entries.find((/**
             * @param {?} model
             * @return {?}
             */
            function (model) { return model.entry.name === EcmModelService.MODEL_NAME; }));
        })));
    };
    /**
     * @param {?} formName
     * @param {?} form
     * @return {?}
     */
    EcmModelService.prototype.createActivitiEcmModel = /**
     * @param {?} formName
     * @param {?} form
     * @return {?}
     */
    function (formName, form) {
        var _this = this;
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        function (observer) {
            _this.createEcmModel(EcmModelService.MODEL_NAME, EcmModelService.MODEL_NAMESPACE).subscribe((/**
             * @param {?} model
             * @return {?}
             */
            function (model) {
                _this.logService.info('model created', model);
                _this.activeEcmModel(EcmModelService.MODEL_NAME).subscribe((/**
                 * @param {?} modelActive
                 * @return {?}
                 */
                function (modelActive) {
                    _this.logService.info('model active', modelActive);
                    _this.createEcmTypeWithProperties(formName, form).subscribe((/**
                     * @param {?} typeCreated
                     * @return {?}
                     */
                    function (typeCreated) {
                        observer.next(typeCreated);
                        observer.complete();
                    }));
                }), (/**
                 * @param {?} err
                 * @return {?}
                 */
                function (err) { return _this.handleError(err); }));
            }), (/**
             * @param {?} err
             * @return {?}
             */
            function (err) { return _this.handleError(err); }));
        }));
    };
    /**
     * @param {?} formName
     * @param {?} form
     * @return {?}
     */
    EcmModelService.prototype.saveFomType = /**
     * @param {?} formName
     * @param {?} form
     * @return {?}
     */
    function (formName, form) {
        var _this = this;
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        function (observer) {
            _this.searchEcmType(formName, EcmModelService.MODEL_NAME).subscribe((/**
             * @param {?} ecmType
             * @return {?}
             */
            function (ecmType) {
                _this.logService.info('custom types', ecmType);
                if (!ecmType) {
                    _this.createEcmTypeWithProperties(formName, form).subscribe((/**
                     * @param {?} typeCreated
                     * @return {?}
                     */
                    function (typeCreated) {
                        observer.next(typeCreated);
                        observer.complete();
                    }));
                }
                else {
                    observer.next(ecmType);
                    observer.complete();
                }
            }), (/**
             * @param {?} err
             * @return {?}
             */
            function (err) { return _this.handleError(err); }));
        }));
    };
    /**
     * @param {?} formName
     * @param {?} form
     * @return {?}
     */
    EcmModelService.prototype.createEcmTypeWithProperties = /**
     * @param {?} formName
     * @param {?} form
     * @return {?}
     */
    function (formName, form) {
        var _this = this;
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        function (observer) {
            _this.createEcmType(formName, EcmModelService.MODEL_NAME, EcmModelService.TYPE_MODEL).subscribe((/**
             * @param {?} typeCreated
             * @return {?}
             */
            function (typeCreated) {
                _this.logService.info('type Created', typeCreated);
                _this.addPropertyToAType(EcmModelService.MODEL_NAME, formName, form).subscribe((/**
                 * @param {?} propertyAdded
                 * @return {?}
                 */
                function (propertyAdded) {
                    _this.logService.info('property Added', propertyAdded);
                    observer.next(typeCreated);
                    observer.complete();
                }), (/**
                 * @param {?} err
                 * @return {?}
                 */
                function (err) { return _this.handleError(err); }));
            }), (/**
             * @param {?} err
             * @return {?}
             */
            function (err) { return _this.handleError(err); }));
        }));
    };
    /**
     * @param {?} typeName
     * @param {?} modelName
     * @return {?}
     */
    EcmModelService.prototype.searchEcmType = /**
     * @param {?} typeName
     * @param {?} modelName
     * @return {?}
     */
    function (typeName, modelName) {
        return this.getEcmType(modelName).pipe(map((/**
         * @param {?} customTypes
         * @return {?}
         */
        function (customTypes) {
            return customTypes.list.entries.find((/**
             * @param {?} type
             * @return {?}
             */
            function (type) { return type.entry.prefixedName === typeName || type.entry.title === typeName; }));
        })));
    };
    /**
     * @param {?} modelName
     * @return {?}
     */
    EcmModelService.prototype.activeEcmModel = /**
     * @param {?} modelName
     * @return {?}
     */
    function (modelName) {
        var _this = this;
        return from(this.apiService.getInstance().core.customModelApi.activateCustomModel(modelName))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * @param {?} modelName
     * @param {?} nameSpace
     * @return {?}
     */
    EcmModelService.prototype.createEcmModel = /**
     * @param {?} modelName
     * @param {?} nameSpace
     * @return {?}
     */
    function (modelName, nameSpace) {
        var _this = this;
        return from(this.apiService.getInstance().core.customModelApi.createCustomModel('DRAFT', '', modelName, modelName, nameSpace))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * @return {?}
     */
    EcmModelService.prototype.getEcmModels = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return from(this.apiService.getInstance().core.customModelApi.getAllCustomModel())
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * @param {?} modelName
     * @return {?}
     */
    EcmModelService.prototype.getEcmType = /**
     * @param {?} modelName
     * @return {?}
     */
    function (modelName) {
        var _this = this;
        return from(this.apiService.getInstance().core.customModelApi.getAllCustomType(modelName))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * @param {?} typeName
     * @param {?} modelName
     * @param {?} parentType
     * @return {?}
     */
    EcmModelService.prototype.createEcmType = /**
     * @param {?} typeName
     * @param {?} modelName
     * @param {?} parentType
     * @return {?}
     */
    function (typeName, modelName, parentType) {
        var _this = this;
        /** @type {?} */
        var name = this.cleanNameType(typeName);
        return from(this.apiService.getInstance().core.customModelApi.createCustomType(modelName, name, parentType, typeName, ''))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * @param {?} modelName
     * @param {?} typeName
     * @param {?} formFields
     * @return {?}
     */
    EcmModelService.prototype.addPropertyToAType = /**
     * @param {?} modelName
     * @param {?} typeName
     * @param {?} formFields
     * @return {?}
     */
    function (modelName, typeName, formFields) {
        var _this = this;
        /** @type {?} */
        var name = this.cleanNameType(typeName);
        /** @type {?} */
        var properties = [];
        if (formFields && formFields.values) {
            for (var key in formFields.values) {
                if (key) {
                    properties.push({
                        name: key,
                        title: key,
                        description: key,
                        dataType: 'd:text',
                        multiValued: false,
                        mandatory: false,
                        mandatoryEnforced: false
                    });
                }
            }
        }
        return from(this.apiService.getInstance().core.customModelApi.addPropertyToType(modelName, name, properties))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * @param {?} name
     * @return {?}
     */
    EcmModelService.prototype.cleanNameType = /**
     * @param {?} name
     * @return {?}
     */
    function (name) {
        /** @type {?} */
        var cleanName = name;
        if (name.indexOf(':') !== -1) {
            cleanName = name.split(':')[1];
        }
        return cleanName.replace(/[^a-zA-Z ]/g, '');
    };
    /**
     * @param {?} res
     * @return {?}
     */
    EcmModelService.prototype.toJson = /**
     * @param {?} res
     * @return {?}
     */
    function (res) {
        if (res) {
            return res || {};
        }
        return {};
    };
    /**
     * @param {?} err
     * @return {?}
     */
    EcmModelService.prototype.handleError = /**
     * @param {?} err
     * @return {?}
     */
    function (err) {
        this.logService.error(err);
    };
    EcmModelService.MODEL_NAMESPACE = 'activitiForms';
    EcmModelService.MODEL_NAME = 'activitiFormsModel';
    EcmModelService.TYPE_MODEL = 'cm:folder';
    EcmModelService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    EcmModelService.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: LogService }
    ]; };
    /** @nocollapse */ EcmModelService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function EcmModelService_Factory() { return new EcmModelService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: EcmModelService, providedIn: "root" });
    return EcmModelService;
}());
export { EcmModelService };
if (false) {
    /** @type {?} */
    EcmModelService.MODEL_NAMESPACE;
    /** @type {?} */
    EcmModelService.MODEL_NAME;
    /** @type {?} */
    EcmModelService.TYPE_MODEL;
    /**
     * @type {?}
     * @private
     */
    EcmModelService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    EcmModelService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWNtLW1vZGVsLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJmb3JtL3NlcnZpY2VzL2VjbS1tb2RlbC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUN6RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsSUFBSSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRXhDLE9BQU8sRUFBRSxHQUFHLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7QUFFakQ7SUFTSSx5QkFBb0IsVUFBOEIsRUFDOUIsVUFBc0I7UUFEdEIsZUFBVSxHQUFWLFVBQVUsQ0FBb0I7UUFDOUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUMxQyxDQUFDOzs7Ozs7SUFFTSxzREFBNEI7Ozs7O0lBQW5DLFVBQW9DLFFBQWdCLEVBQUUsSUFBZTtRQUFyRSxpQkFvQkM7UUFuQkcsT0FBTyxJQUFJLFVBQVU7Ozs7UUFBQyxVQUFDLFFBQVE7WUFDM0IsS0FBSSxDQUFDLHNCQUFzQixFQUFFLENBQUMsU0FBUzs7OztZQUNuQyxVQUFDLEtBQUs7Z0JBQ0YsSUFBSSxDQUFDLEtBQUssRUFBRTtvQkFDUixLQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVM7Ozs7b0JBQUMsVUFBQyxRQUFRO3dCQUMzRCxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3hCLENBQUMsRUFBQyxDQUFDO2lCQUNOO3FCQUFNO29CQUNILEtBQUksQ0FBQyxXQUFXLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUFDLFNBQVM7Ozs7b0JBQUMsVUFBQyxRQUFRO3dCQUNoRCxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3hCLENBQUMsRUFBQyxDQUFDO2lCQUNOO1lBQ0wsQ0FBQzs7OztZQUNELFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFDakMsQ0FBQztRQUNOLENBQUMsRUFBQyxDQUFDO0lBRVAsQ0FBQzs7OztJQUVELGdEQUFzQjs7O0lBQXRCO1FBQ0ksT0FBTyxJQUFJLENBQUMsWUFBWSxFQUFFLENBQUMsSUFBSSxDQUFDLEdBQUc7Ozs7UUFBQyxVQUFVLFNBQWM7WUFDeEQsT0FBTyxTQUFTLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJOzs7O1lBQUMsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFLLENBQUMsS0FBSyxDQUFDLElBQUksS0FBSyxlQUFlLENBQUMsVUFBVSxFQUEvQyxDQUErQyxFQUFDLENBQUM7UUFDbkcsQ0FBQyxFQUFDLENBQUMsQ0FBQztJQUNSLENBQUM7Ozs7OztJQUVELGdEQUFzQjs7Ozs7SUFBdEIsVUFBdUIsUUFBZ0IsRUFBRSxJQUFlO1FBQXhELGlCQW1CQztRQWxCRyxPQUFPLElBQUksVUFBVTs7OztRQUFDLFVBQUMsUUFBUTtZQUMzQixLQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxVQUFVLEVBQUUsZUFBZSxDQUFDLGVBQWUsQ0FBQyxDQUFDLFNBQVM7Ozs7WUFDdEYsVUFBQyxLQUFLO2dCQUNGLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxLQUFLLENBQUMsQ0FBQztnQkFDN0MsS0FBSSxDQUFDLGNBQWMsQ0FBQyxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUzs7OztnQkFDckQsVUFBQyxXQUFXO29CQUNSLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxXQUFXLENBQUMsQ0FBQztvQkFDbEQsS0FBSSxDQUFDLDJCQUEyQixDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsQ0FBQyxTQUFTOzs7O29CQUFDLFVBQUMsV0FBVzt3QkFDbkUsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQzt3QkFDM0IsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO29CQUN4QixDQUFDLEVBQUMsQ0FBQztnQkFDUCxDQUFDOzs7O2dCQUNELFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFDakMsQ0FBQztZQUNOLENBQUM7Ozs7WUFDRCxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQ2pDLENBQUM7UUFDTixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQUVELHFDQUFXOzs7OztJQUFYLFVBQVksUUFBZ0IsRUFBRSxJQUFlO1FBQTdDLGlCQWtCQztRQWpCRyxPQUFPLElBQUksVUFBVTs7OztRQUFDLFVBQUMsUUFBUTtZQUMzQixLQUFJLENBQUMsYUFBYSxDQUFDLFFBQVEsRUFBRSxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUzs7OztZQUM5RCxVQUFDLE9BQU87Z0JBQ0osS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQyxDQUFDO2dCQUM5QyxJQUFJLENBQUMsT0FBTyxFQUFFO29CQUNWLEtBQUksQ0FBQywyQkFBMkIsQ0FBQyxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUzs7OztvQkFBQyxVQUFDLFdBQVc7d0JBQ25FLFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7d0JBQzNCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztvQkFDeEIsQ0FBQyxFQUFDLENBQUM7aUJBQ047cUJBQU07b0JBQ0gsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztvQkFDdkIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO2lCQUN2QjtZQUNMLENBQUM7Ozs7WUFDRCxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQ2pDLENBQUM7UUFDTixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7Ozs7OztJQUVNLHFEQUEyQjs7Ozs7SUFBbEMsVUFBbUMsUUFBZ0IsRUFBRSxJQUFlO1FBQXBFLGlCQWVDO1FBZEcsT0FBTyxJQUFJLFVBQVU7Ozs7UUFBQyxVQUFDLFFBQVE7WUFDM0IsS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsZUFBZSxDQUFDLFVBQVUsRUFBRSxlQUFlLENBQUMsVUFBVSxDQUFDLENBQUMsU0FBUzs7OztZQUMxRixVQUFDLFdBQVc7Z0JBQ1IsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsY0FBYyxFQUFFLFdBQVcsQ0FBQyxDQUFDO2dCQUNsRCxLQUFJLENBQUMsa0JBQWtCLENBQUMsZUFBZSxDQUFDLFVBQVUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLENBQUMsU0FBUzs7OztnQkFDekUsVUFBQyxhQUFhO29CQUNWLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLGdCQUFnQixFQUFFLGFBQWEsQ0FBQyxDQUFDO29CQUN0RCxRQUFRLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO29CQUMzQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7Z0JBQ3hCLENBQUM7Ozs7Z0JBQ0QsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQUM7WUFDeEMsQ0FBQzs7OztZQUNELFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFBQyxDQUFDO1FBQ3hDLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7O0lBRU0sdUNBQWE7Ozs7O0lBQXBCLFVBQXFCLFFBQWdCLEVBQUUsU0FBaUI7UUFDcEQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsQ0FBQyxDQUFDLElBQUksQ0FBQyxHQUFHOzs7O1FBQUMsVUFBVSxXQUFnQjtZQUNqRSxPQUFPLFdBQVcsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUk7Ozs7WUFBQyxVQUFDLElBQUksSUFBSyxPQUFBLElBQUksQ0FBQyxLQUFLLENBQUMsWUFBWSxLQUFLLFFBQVEsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssS0FBSyxRQUFRLEVBQXJFLENBQXFFLEVBQUMsQ0FBQztRQUMxSCxDQUFDLEVBQUMsQ0FBQyxDQUFDO0lBQ1IsQ0FBQzs7Ozs7SUFFTSx3Q0FBYzs7OztJQUFyQixVQUFzQixTQUFpQjtRQUF2QyxpQkFNQztRQUxHLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUN4RixJQUFJLENBQ0QsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFDaEIsVUFBVTs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQzs7Ozs7O0lBRU0sd0NBQWM7Ozs7O0lBQXJCLFVBQXNCLFNBQWlCLEVBQUUsU0FBaUI7UUFBMUQsaUJBTUM7UUFMRyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsaUJBQWlCLENBQUMsT0FBTyxFQUFFLEVBQUUsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUFFLFNBQVMsQ0FBQyxDQUFDO2FBQ3pILElBQUksQ0FDRCxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUNoQixVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7O0lBRU0sc0NBQVk7OztJQUFuQjtRQUFBLGlCQU1DO1FBTEcsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGlCQUFpQixFQUFFLENBQUM7YUFDN0UsSUFBSSxDQUNELEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQ2hCLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7Ozs7O0lBRU0sb0NBQVU7Ozs7SUFBakIsVUFBa0IsU0FBaUI7UUFBbkMsaUJBTUM7UUFMRyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDckYsSUFBSSxDQUNELEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQ2hCLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7Ozs7Ozs7SUFFTSx1Q0FBYTs7Ozs7O0lBQXBCLFVBQXFCLFFBQWdCLEVBQUUsU0FBaUIsRUFBRSxVQUFrQjtRQUE1RSxpQkFRQzs7WUFQUyxJQUFJLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7UUFFekMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLFNBQVMsRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxFQUFFLENBQUMsQ0FBQzthQUNySCxJQUFJLENBQ0QsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFDaEIsVUFBVTs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQzs7Ozs7OztJQUVNLDRDQUFrQjs7Ozs7O0lBQXpCLFVBQTBCLFNBQWlCLEVBQUUsUUFBZ0IsRUFBRSxVQUFlO1FBQTlFLGlCQTBCQzs7WUF6QlMsSUFBSSxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxDQUFDOztZQUVuQyxVQUFVLEdBQUcsRUFBRTtRQUNyQixJQUFJLFVBQVUsSUFBSSxVQUFVLENBQUMsTUFBTSxFQUFFO1lBQ2pDLEtBQUssSUFBTSxHQUFHLElBQUksVUFBVSxDQUFDLE1BQU0sRUFBRTtnQkFDakMsSUFBSSxHQUFHLEVBQUU7b0JBQ0wsVUFBVSxDQUFDLElBQUksQ0FBQzt3QkFDWixJQUFJLEVBQUUsR0FBRzt3QkFDVCxLQUFLLEVBQUUsR0FBRzt3QkFDVixXQUFXLEVBQUUsR0FBRzt3QkFDaEIsUUFBUSxFQUFFLFFBQVE7d0JBQ2xCLFdBQVcsRUFBRSxLQUFLO3dCQUNsQixTQUFTLEVBQUUsS0FBSzt3QkFDaEIsaUJBQWlCLEVBQUUsS0FBSztxQkFDM0IsQ0FBQyxDQUFDO2lCQUNOO2FBQ0o7U0FDSjtRQUVELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxpQkFBaUIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxFQUFFLFVBQVUsQ0FBQyxDQUFDO2FBQ3hHLElBQUksQ0FDRCxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUNoQixVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFFVixDQUFDOzs7OztJQUVELHVDQUFhOzs7O0lBQWIsVUFBYyxJQUFZOztZQUNsQixTQUFTLEdBQUcsSUFBSTtRQUNwQixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7WUFDMUIsU0FBUyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7U0FDbEM7UUFDRCxPQUFPLFNBQVMsQ0FBQyxPQUFPLENBQUMsYUFBYSxFQUFFLEVBQUUsQ0FBQyxDQUFDO0lBQ2hELENBQUM7Ozs7O0lBRUQsZ0NBQU07Ozs7SUFBTixVQUFPLEdBQVE7UUFDWCxJQUFJLEdBQUcsRUFBRTtZQUNMLE9BQU8sR0FBRyxJQUFJLEVBQUUsQ0FBQztTQUNwQjtRQUNELE9BQU8sRUFBRSxDQUFDO0lBQ2QsQ0FBQzs7Ozs7SUFFRCxxQ0FBVzs7OztJQUFYLFVBQVksR0FBUTtRQUNoQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUMvQixDQUFDO0lBM0xhLCtCQUFlLEdBQVcsZUFBZSxDQUFDO0lBQzFDLDBCQUFVLEdBQVcsb0JBQW9CLENBQUM7SUFDMUMsMEJBQVUsR0FBVyxXQUFXLENBQUM7O2dCQVBsRCxVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7O2dCQVJRLGtCQUFrQjtnQkFEbEIsVUFBVTs7OzBCQWpCbkI7Q0F5TkMsQUFqTUQsSUFpTUM7U0E5TFksZUFBZTs7O0lBRXhCLGdDQUF3RDs7SUFDeEQsMkJBQXdEOztJQUN4RCwyQkFBK0M7Ozs7O0lBRW5DLHFDQUFzQzs7Ozs7SUFDdEMscUNBQThCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9sb2cuc2VydmljZSc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmcm9tIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEZvcm1Nb2RlbCB9IGZyb20gJy4uL2NvbXBvbmVudHMvd2lkZ2V0cy9jb3JlL2Zvcm0ubW9kZWwnO1xyXG5pbXBvcnQgeyBtYXAsIGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEVjbU1vZGVsU2VydmljZSB7XHJcblxyXG4gICAgcHVibGljIHN0YXRpYyBNT0RFTF9OQU1FU1BBQ0U6IHN0cmluZyA9ICdhY3Rpdml0aUZvcm1zJztcclxuICAgIHB1YmxpYyBzdGF0aWMgTU9ERUxfTkFNRTogc3RyaW5nID0gJ2FjdGl2aXRpRm9ybXNNb2RlbCc7XHJcbiAgICBwdWJsaWMgc3RhdGljIFRZUEVfTU9ERUw6IHN0cmluZyA9ICdjbTpmb2xkZXInO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGNyZWF0ZUVjbVR5cGVGb3JBY3Rpdml0aUZvcm0oZm9ybU5hbWU6IHN0cmluZywgZm9ybTogRm9ybU1vZGVsKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUoKG9ic2VydmVyKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuc2VhcmNoQWN0aXZpdGlFY21Nb2RlbCgpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgIChtb2RlbCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICghbW9kZWwpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVBY3Rpdml0aUVjbU1vZGVsKGZvcm1OYW1lLCBmb3JtKS5zdWJzY3JpYmUoKHR5cGVGb3JtKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5uZXh0KHR5cGVGb3JtKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuc2F2ZUZvbVR5cGUoZm9ybU5hbWUsIGZvcm0pLnN1YnNjcmliZSgodHlwZUZvcm0pID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHlwZUZvcm0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0pO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBzZWFyY2hBY3Rpdml0aUVjbU1vZGVsKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldEVjbU1vZGVscygpLnBpcGUobWFwKGZ1bmN0aW9uIChlY21Nb2RlbHM6IGFueSkge1xyXG4gICAgICAgICAgICByZXR1cm4gZWNtTW9kZWxzLmxpc3QuZW50cmllcy5maW5kKChtb2RlbCkgPT4gbW9kZWwuZW50cnkubmFtZSA9PT0gRWNtTW9kZWxTZXJ2aWNlLk1PREVMX05BTUUpO1xyXG4gICAgICAgIH0pKTtcclxuICAgIH1cclxuXHJcbiAgICBjcmVhdGVBY3Rpdml0aUVjbU1vZGVsKGZvcm1OYW1lOiBzdHJpbmcsIGZvcm06IEZvcm1Nb2RlbCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKChvYnNlcnZlcikgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmNyZWF0ZUVjbU1vZGVsKEVjbU1vZGVsU2VydmljZS5NT0RFTF9OQU1FLCBFY21Nb2RlbFNlcnZpY2UuTU9ERUxfTkFNRVNQQUNFKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAobW9kZWwpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuaW5mbygnbW9kZWwgY3JlYXRlZCcsIG1vZGVsKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFjdGl2ZUVjbU1vZGVsKEVjbU1vZGVsU2VydmljZS5NT0RFTF9OQU1FKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChtb2RlbEFjdGl2ZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmluZm8oJ21vZGVsIGFjdGl2ZScsIG1vZGVsQWN0aXZlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuY3JlYXRlRWNtVHlwZVdpdGhQcm9wZXJ0aWVzKGZvcm1OYW1lLCBmb3JtKS5zdWJzY3JpYmUoKHR5cGVDcmVhdGVkKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dCh0eXBlQ3JlYXRlZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycilcclxuICAgICAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHNhdmVGb21UeXBlKGZvcm1OYW1lOiBzdHJpbmcsIGZvcm06IEZvcm1Nb2RlbCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKChvYnNlcnZlcikgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnNlYXJjaEVjbVR5cGUoZm9ybU5hbWUsIEVjbU1vZGVsU2VydmljZS5NT0RFTF9OQU1FKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAoZWNtVHlwZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9nU2VydmljZS5pbmZvKCdjdXN0b20gdHlwZXMnLCBlY21UeXBlKTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIWVjbVR5cGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5jcmVhdGVFY21UeXBlV2l0aFByb3BlcnRpZXMoZm9ybU5hbWUsIGZvcm0pLnN1YnNjcmliZSgodHlwZUNyZWF0ZWQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHlwZUNyZWF0ZWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChlY21UeXBlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGNyZWF0ZUVjbVR5cGVXaXRoUHJvcGVydGllcyhmb3JtTmFtZTogc3RyaW5nLCBmb3JtOiBGb3JtTW9kZWwpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZSgob2JzZXJ2ZXIpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5jcmVhdGVFY21UeXBlKGZvcm1OYW1lLCBFY21Nb2RlbFNlcnZpY2UuTU9ERUxfTkFNRSwgRWNtTW9kZWxTZXJ2aWNlLlRZUEVfTU9ERUwpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgICh0eXBlQ3JlYXRlZCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9nU2VydmljZS5pbmZvKCd0eXBlIENyZWF0ZWQnLCB0eXBlQ3JlYXRlZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hZGRQcm9wZXJ0eVRvQVR5cGUoRWNtTW9kZWxTZXJ2aWNlLk1PREVMX05BTUUsIGZvcm1OYW1lLCBmb3JtKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChwcm9wZXJ0eUFkZGVkKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuaW5mbygncHJvcGVydHkgQWRkZWQnLCBwcm9wZXJ0eUFkZGVkKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHlwZUNyZWF0ZWQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyBzZWFyY2hFY21UeXBlKHR5cGVOYW1lOiBzdHJpbmcsIG1vZGVsTmFtZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRFY21UeXBlKG1vZGVsTmFtZSkucGlwZShtYXAoZnVuY3Rpb24gKGN1c3RvbVR5cGVzOiBhbnkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGN1c3RvbVR5cGVzLmxpc3QuZW50cmllcy5maW5kKCh0eXBlKSA9PiB0eXBlLmVudHJ5LnByZWZpeGVkTmFtZSA9PT0gdHlwZU5hbWUgfHwgdHlwZS5lbnRyeS50aXRsZSA9PT0gdHlwZU5hbWUpO1xyXG4gICAgICAgIH0pKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgYWN0aXZlRWNtTW9kZWwobW9kZWxOYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmNvcmUuY3VzdG9tTW9kZWxBcGkuYWN0aXZhdGVDdXN0b21Nb2RlbChtb2RlbE5hbWUpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCh0aGlzLnRvSnNvbiksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgY3JlYXRlRWNtTW9kZWwobW9kZWxOYW1lOiBzdHJpbmcsIG5hbWVTcGFjZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5jb3JlLmN1c3RvbU1vZGVsQXBpLmNyZWF0ZUN1c3RvbU1vZGVsKCdEUkFGVCcsICcnLCBtb2RlbE5hbWUsIG1vZGVsTmFtZSwgbmFtZVNwYWNlKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy50b0pzb24pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldEVjbU1vZGVscygpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmNvcmUuY3VzdG9tTW9kZWxBcGkuZ2V0QWxsQ3VzdG9tTW9kZWwoKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy50b0pzb24pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGdldEVjbVR5cGUobW9kZWxOYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmNvcmUuY3VzdG9tTW9kZWxBcGkuZ2V0QWxsQ3VzdG9tVHlwZShtb2RlbE5hbWUpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCh0aGlzLnRvSnNvbiksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgY3JlYXRlRWNtVHlwZSh0eXBlTmFtZTogc3RyaW5nLCBtb2RlbE5hbWU6IHN0cmluZywgcGFyZW50VHlwZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBuYW1lID0gdGhpcy5jbGVhbk5hbWVUeXBlKHR5cGVOYW1lKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuY29yZS5jdXN0b21Nb2RlbEFwaS5jcmVhdGVDdXN0b21UeXBlKG1vZGVsTmFtZSwgbmFtZSwgcGFyZW50VHlwZSwgdHlwZU5hbWUsICcnKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy50b0pzb24pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHVibGljIGFkZFByb3BlcnR5VG9BVHlwZShtb2RlbE5hbWU6IHN0cmluZywgdHlwZU5hbWU6IHN0cmluZywgZm9ybUZpZWxkczogYW55KSB7XHJcbiAgICAgICAgY29uc3QgbmFtZSA9IHRoaXMuY2xlYW5OYW1lVHlwZSh0eXBlTmFtZSk7XHJcblxyXG4gICAgICAgIGNvbnN0IHByb3BlcnRpZXMgPSBbXTtcclxuICAgICAgICBpZiAoZm9ybUZpZWxkcyAmJiBmb3JtRmllbGRzLnZhbHVlcykge1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBmb3JtRmllbGRzLnZhbHVlcykge1xyXG4gICAgICAgICAgICAgICAgaWYgKGtleSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHByb3BlcnRpZXMucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6IGtleSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGl0bGU6IGtleSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGVzY3JpcHRpb246IGtleSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0YVR5cGU6ICdkOnRleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtdWx0aVZhbHVlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hbmRhdG9yeTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1hbmRhdG9yeUVuZm9yY2VkOiBmYWxzZVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5jb3JlLmN1c3RvbU1vZGVsQXBpLmFkZFByb3BlcnR5VG9UeXBlKG1vZGVsTmFtZSwgbmFtZSwgcHJvcGVydGllcykpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKHRoaXMudG9Kc29uKSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICBjbGVhbk5hbWVUeXBlKG5hbWU6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgbGV0IGNsZWFuTmFtZSA9IG5hbWU7XHJcbiAgICAgICAgaWYgKG5hbWUuaW5kZXhPZignOicpICE9PSAtMSkge1xyXG4gICAgICAgICAgICBjbGVhbk5hbWUgPSBuYW1lLnNwbGl0KCc6JylbMV07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBjbGVhbk5hbWUucmVwbGFjZSgvW15hLXpBLVogXS9nLCAnJyk7XHJcbiAgICB9XHJcblxyXG4gICAgdG9Kc29uKHJlczogYW55KSB7XHJcbiAgICAgICAgaWYgKHJlcykge1xyXG4gICAgICAgICAgICByZXR1cm4gcmVzIHx8IHt9O1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4ge307XHJcbiAgICB9XHJcblxyXG4gICAgaGFuZGxlRXJyb3IoZXJyOiBhbnkpOiBhbnkge1xyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnIpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==