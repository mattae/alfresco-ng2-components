/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import { Injectable } from '@angular/core';
import { Observable, Subject, from, of, throwError } from 'rxjs';
import { FormDefinitionModel } from '../models/form-definition.model';
import { FormModel, FormOutcomeModel } from './../components/widgets/core/index';
import { EcmModelService } from './ecm-model.service';
import { map, catchError, switchMap, combineAll, defaultIfEmpty } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./ecm-model.service";
import * as i2 from "../../services/alfresco-api.service";
import * as i3 from "../../services/log.service";
var FormService = /** @class */ (function () {
    function FormService(ecmModelService, apiService, logService) {
        this.ecmModelService = ecmModelService;
        this.apiService = apiService;
        this.logService = logService;
        this.formLoaded = new Subject();
        this.formDataRefreshed = new Subject();
        this.formFieldValueChanged = new Subject();
        this.formEvents = new Subject();
        this.taskCompleted = new Subject();
        this.taskCompletedError = new Subject();
        this.taskSaved = new Subject();
        this.taskSavedError = new Subject();
        this.formContentClicked = new Subject();
        this.validateForm = new Subject();
        this.validateFormField = new Subject();
        this.validateDynamicTableRow = new Subject();
        this.executeOutcome = new Subject();
    }
    Object.defineProperty(FormService.prototype, "taskApi", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.apiService.getInstance().activiti.taskApi;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormService.prototype, "modelsApi", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.apiService.getInstance().activiti.modelsApi;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormService.prototype, "editorApi", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.apiService.getInstance().activiti.editorApi;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormService.prototype, "processApi", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.apiService.getInstance().activiti.processApi;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormService.prototype, "processInstanceVariablesApi", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.apiService.getInstance().activiti.processInstanceVariablesApi;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormService.prototype, "usersWorkflowApi", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.apiService.getInstance().activiti.usersWorkflowApi;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(FormService.prototype, "groupsApi", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.apiService.getInstance().activiti.groupsApi;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Parses JSON data to create a corresponding Form model.
     * @param json JSON to create the form
     * @param data Values for the form fields
     * @param readOnly Should the form fields be read-only?
     * @returns Form model created from input data
     */
    /**
     * Parses JSON data to create a corresponding Form model.
     * @param {?} json JSON to create the form
     * @param {?=} data Values for the form fields
     * @param {?=} readOnly Should the form fields be read-only?
     * @return {?} Form model created from input data
     */
    FormService.prototype.parseForm = /**
     * Parses JSON data to create a corresponding Form model.
     * @param {?} json JSON to create the form
     * @param {?=} data Values for the form fields
     * @param {?=} readOnly Should the form fields be read-only?
     * @return {?} Form model created from input data
     */
    function (json, data, readOnly) {
        if (readOnly === void 0) { readOnly = false; }
        if (json) {
            /** @type {?} */
            var form = new FormModel(json, data, readOnly, this);
            if (!json.fields) {
                form.outcomes = [
                    new FormOutcomeModel(form, {
                        id: '$save',
                        name: FormOutcomeModel.SAVE_ACTION,
                        isSystem: true
                    })
                ];
            }
            return form;
        }
        return null;
    };
    /**
     * Creates a Form with a field for each metadata property.
     * @param formName Name of the new form
     * @returns The new form
     */
    /**
     * Creates a Form with a field for each metadata property.
     * @param {?} formName Name of the new form
     * @return {?} The new form
     */
    FormService.prototype.createFormFromANode = /**
     * Creates a Form with a field for each metadata property.
     * @param {?} formName Name of the new form
     * @return {?} The new form
     */
    function (formName) {
        var _this = this;
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        function (observer) {
            _this.createForm(formName).subscribe((/**
             * @param {?} form
             * @return {?}
             */
            function (form) {
                _this.ecmModelService.searchEcmType(formName, EcmModelService.MODEL_NAME).subscribe((/**
                 * @param {?} customType
                 * @return {?}
                 */
                function (customType) {
                    /** @type {?} */
                    var formDefinitionModel = new FormDefinitionModel(form.id, form.name, form.lastUpdatedByFullName, form.lastUpdated, customType.entry.properties);
                    from(_this.editorApi.saveForm(form.id, formDefinitionModel)).subscribe((/**
                     * @param {?} formData
                     * @return {?}
                     */
                    function (formData) {
                        observer.next(formData);
                        observer.complete();
                    }), (/**
                     * @param {?} err
                     * @return {?}
                     */
                    function (err) { return _this.handleError(err); }));
                }), (/**
                 * @param {?} err
                 * @return {?}
                 */
                function (err) { return _this.handleError(err); }));
            }), (/**
             * @param {?} err
             * @return {?}
             */
            function (err) { return _this.handleError(err); }));
        }));
    };
    /**
     * Create a Form.
     * @param formName Name of the new form
     * @returns The new form
     */
    /**
     * Create a Form.
     * @param {?} formName Name of the new form
     * @return {?} The new form
     */
    FormService.prototype.createForm = /**
     * Create a Form.
     * @param {?} formName Name of the new form
     * @return {?} The new form
     */
    function (formName) {
        /** @type {?} */
        var dataModel = {
            name: formName,
            description: '',
            modelType: 2,
            stencilSet: 0
        };
        return from(this.modelsApi.createModel(dataModel));
    };
    /**
     * Saves a form.
     * @param formId ID of the form to save
     * @param formModel Model data for the form
     * @returns Data for the saved form
     */
    /**
     * Saves a form.
     * @param {?} formId ID of the form to save
     * @param {?} formModel Model data for the form
     * @return {?} Data for the saved form
     */
    FormService.prototype.saveForm = /**
     * Saves a form.
     * @param {?} formId ID of the form to save
     * @param {?} formModel Model data for the form
     * @return {?} Data for the saved form
     */
    function (formId, formModel) {
        return from(this.editorApi.saveForm(formId, formModel));
    };
    /**
     * Searches for a form by name.
     * @param name The form name to search for
     * @returns Form model(s) matching the search name
     */
    /**
     * Searches for a form by name.
     * @param {?} name The form name to search for
     * @return {?} Form model(s) matching the search name
     */
    FormService.prototype.searchFrom = /**
     * Searches for a form by name.
     * @param {?} name The form name to search for
     * @return {?} Form model(s) matching the search name
     */
    function (name) {
        var _this = this;
        /** @type {?} */
        var opts = {
            'modelType': 2
        };
        return from(this.modelsApi.getModels(opts))
            .pipe(map((/**
         * @param {?} forms
         * @return {?}
         */
        function (forms) {
            return forms.data.find((/**
             * @param {?} formData
             * @return {?}
             */
            function (formData) { return formData.name === name; }));
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets all the forms.
     * @returns List of form models
     */
    /**
     * Gets all the forms.
     * @return {?} List of form models
     */
    FormService.prototype.getForms = /**
     * Gets all the forms.
     * @return {?} List of form models
     */
    function () {
        var _this = this;
        /** @type {?} */
        var opts = {
            'modelType': 2
        };
        return from(this.modelsApi.getModels(opts))
            .pipe(map(this.toJsonArray), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets process definitions.
     * @returns List of process definitions
     */
    /**
     * Gets process definitions.
     * @return {?} List of process definitions
     */
    FormService.prototype.getProcessDefinitions = /**
     * Gets process definitions.
     * @return {?} List of process definitions
     */
    function () {
        var _this = this;
        return from(this.processApi.getProcessDefinitions({}))
            .pipe(map(this.toJsonArray), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets instance variables for a process.
     * @param processInstanceId ID of the target process
     * @returns List of instance variable information
     */
    /**
     * Gets instance variables for a process.
     * @param {?} processInstanceId ID of the target process
     * @return {?} List of instance variable information
     */
    FormService.prototype.getProcessVariablesById = /**
     * Gets instance variables for a process.
     * @param {?} processInstanceId ID of the target process
     * @return {?} List of instance variable information
     */
    function (processInstanceId) {
        var _this = this;
        return from(this.processInstanceVariablesApi.getProcessInstanceVariables(processInstanceId))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets all the tasks.
     * @returns List of tasks
     */
    /**
     * Gets all the tasks.
     * @return {?} List of tasks
     */
    FormService.prototype.getTasks = /**
     * Gets all the tasks.
     * @return {?} List of tasks
     */
    function () {
        var _this = this;
        return from(this.taskApi.listTasks({}))
            .pipe(map(this.toJsonArray), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets a task.
     * @param taskId Task Id
     * @returns Task info
     */
    /**
     * Gets a task.
     * @param {?} taskId Task Id
     * @return {?} Task info
     */
    FormService.prototype.getTask = /**
     * Gets a task.
     * @param {?} taskId Task Id
     * @return {?} Task info
     */
    function (taskId) {
        var _this = this;
        return from(this.taskApi.getTask(taskId))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Saves a task form.
     * @param taskId Task Id
     * @param formValues Form Values
     * @returns Null response when the operation is complete
     */
    /**
     * Saves a task form.
     * @param {?} taskId Task Id
     * @param {?} formValues Form Values
     * @return {?} Null response when the operation is complete
     */
    FormService.prototype.saveTaskForm = /**
     * Saves a task form.
     * @param {?} taskId Task Id
     * @param {?} formValues Form Values
     * @return {?} Null response when the operation is complete
     */
    function (taskId, formValues) {
        var _this = this;
        /** @type {?} */
        var saveFormRepresentation = (/** @type {?} */ ({ values: formValues }));
        return from(this.taskApi.saveTaskForm(taskId, saveFormRepresentation))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Completes a Task Form.
     * @param taskId Task Id
     * @param formValues Form Values
     * @param outcome Form Outcome
     * @returns Null response when the operation is complete
     */
    /**
     * Completes a Task Form.
     * @param {?} taskId Task Id
     * @param {?} formValues Form Values
     * @param {?=} outcome Form Outcome
     * @return {?} Null response when the operation is complete
     */
    FormService.prototype.completeTaskForm = /**
     * Completes a Task Form.
     * @param {?} taskId Task Id
     * @param {?} formValues Form Values
     * @param {?=} outcome Form Outcome
     * @return {?} Null response when the operation is complete
     */
    function (taskId, formValues, outcome) {
        var _this = this;
        /** @type {?} */
        var completeFormRepresentation = (/** @type {?} */ ({ values: formValues }));
        if (outcome) {
            completeFormRepresentation.outcome = outcome;
        }
        return from(this.taskApi.completeTaskForm(taskId, completeFormRepresentation))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets a form related to a task.
     * @param taskId ID of the target task
     * @returns Form definition
     */
    /**
     * Gets a form related to a task.
     * @param {?} taskId ID of the target task
     * @return {?} Form definition
     */
    FormService.prototype.getTaskForm = /**
     * Gets a form related to a task.
     * @param {?} taskId ID of the target task
     * @return {?} Form definition
     */
    function (taskId) {
        var _this = this;
        return from(this.taskApi.getTaskForm(taskId))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets a form definition.
     * @param formId ID of the target form
     * @returns Form definition
     */
    /**
     * Gets a form definition.
     * @param {?} formId ID of the target form
     * @return {?} Form definition
     */
    FormService.prototype.getFormDefinitionById = /**
     * Gets a form definition.
     * @param {?} formId ID of the target form
     * @return {?} Form definition
     */
    function (formId) {
        var _this = this;
        return from(this.editorApi.getForm(formId))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets the form definition with a given name.
     * @param name The form name
     * @returns Form definition
     */
    /**
     * Gets the form definition with a given name.
     * @param {?} name The form name
     * @return {?} Form definition
     */
    FormService.prototype.getFormDefinitionByName = /**
     * Gets the form definition with a given name.
     * @param {?} name The form name
     * @return {?} Form definition
     */
    function (name) {
        var _this = this;
        /** @type {?} */
        var opts = {
            'filter': 'myReusableForms',
            'filterText': name,
            'modelType': 2
        };
        return from(this.modelsApi.getModels(opts))
            .pipe(map(this.getFormId), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets the start form instance for a given process.
     * @param processId Process definition ID
     * @returns Form definition
     */
    /**
     * Gets the start form instance for a given process.
     * @param {?} processId Process definition ID
     * @return {?} Form definition
     */
    FormService.prototype.getStartFormInstance = /**
     * Gets the start form instance for a given process.
     * @param {?} processId Process definition ID
     * @return {?} Form definition
     */
    function (processId) {
        var _this = this;
        return from(this.processApi.getProcessInstanceStartForm(processId))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets a process instance.
     * @param processId ID of the process to get
     * @returns Process instance
     */
    /**
     * Gets a process instance.
     * @param {?} processId ID of the process to get
     * @return {?} Process instance
     */
    FormService.prototype.getProcessInstance = /**
     * Gets a process instance.
     * @param {?} processId ID of the process to get
     * @return {?} Process instance
     */
    function (processId) {
        var _this = this;
        return from(this.processApi.getProcessInstance(processId))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets the start form definition for a given process.
     * @param processId Process definition ID
     * @returns Form definition
     */
    /**
     * Gets the start form definition for a given process.
     * @param {?} processId Process definition ID
     * @return {?} Form definition
     */
    FormService.prototype.getStartFormDefinition = /**
     * Gets the start form definition for a given process.
     * @param {?} processId Process definition ID
     * @return {?} Form definition
     */
    function (processId) {
        var _this = this;
        return from(this.processApi.getProcessDefinitionStartForm(processId))
            .pipe(map(this.toJson), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets values of fields populated by a REST backend.
     * @param taskId Task identifier
     * @param field Field identifier
     * @returns Field values
     */
    /**
     * Gets values of fields populated by a REST backend.
     * @param {?} taskId Task identifier
     * @param {?} field Field identifier
     * @return {?} Field values
     */
    FormService.prototype.getRestFieldValues = /**
     * Gets values of fields populated by a REST backend.
     * @param {?} taskId Task identifier
     * @param {?} field Field identifier
     * @return {?} Field values
     */
    function (taskId, field) {
        var _this = this;
        return from(this.taskApi.getRestFieldValues(taskId, field))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets values of fields populated by a REST backend using a process ID.
     * @param processDefinitionId Process identifier
     * @param field Field identifier
     * @returns Field values
     */
    /**
     * Gets values of fields populated by a REST backend using a process ID.
     * @param {?} processDefinitionId Process identifier
     * @param {?} field Field identifier
     * @return {?} Field values
     */
    FormService.prototype.getRestFieldValuesByProcessId = /**
     * Gets values of fields populated by a REST backend using a process ID.
     * @param {?} processDefinitionId Process identifier
     * @param {?} field Field identifier
     * @return {?} Field values
     */
    function (processDefinitionId, field) {
        var _this = this;
        return from(this.processApi.getRestFieldValues(processDefinitionId, field))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets column values of fields populated by a REST backend using a process ID.
     * @param processDefinitionId Process identifier
     * @param field Field identifier
     * @param column Column identifier
     * @returns Field values
     */
    /**
     * Gets column values of fields populated by a REST backend using a process ID.
     * @param {?} processDefinitionId Process identifier
     * @param {?} field Field identifier
     * @param {?=} column Column identifier
     * @return {?} Field values
     */
    FormService.prototype.getRestFieldValuesColumnByProcessId = /**
     * Gets column values of fields populated by a REST backend using a process ID.
     * @param {?} processDefinitionId Process identifier
     * @param {?} field Field identifier
     * @param {?=} column Column identifier
     * @return {?} Field values
     */
    function (processDefinitionId, field, column) {
        var _this = this;
        return from(this.processApi.getRestTableFieldValues(processDefinitionId, field, column))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets column values of fields populated by a REST backend.
     * @param taskId Task identifier
     * @param field Field identifier
     * @param column Column identifier
     * @returns Field values
     */
    /**
     * Gets column values of fields populated by a REST backend.
     * @param {?} taskId Task identifier
     * @param {?} field Field identifier
     * @param {?=} column Column identifier
     * @return {?} Field values
     */
    FormService.prototype.getRestFieldValuesColumn = /**
     * Gets column values of fields populated by a REST backend.
     * @param {?} taskId Task identifier
     * @param {?} field Field identifier
     * @param {?=} column Column identifier
     * @return {?} Field values
     */
    function (taskId, field, column) {
        var _this = this;
        return from(this.taskApi.getRestFieldValuesColumn(taskId, field, column))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Returns a URL for the profile picture of a user.
     * @param userId ID of the target user
     * @returns URL string
     */
    /**
     * Returns a URL for the profile picture of a user.
     * @param {?} userId ID of the target user
     * @return {?} URL string
     */
    FormService.prototype.getUserProfileImageApi = /**
     * Returns a URL for the profile picture of a user.
     * @param {?} userId ID of the target user
     * @return {?} URL string
     */
    function (userId) {
        return this.apiService.getInstance().activiti.userApi.getUserProfilePictureUrl(userId);
    };
    /**
     * Gets a list of workflow users.
     * @param filter Filter to select specific users
     * @param groupId Group ID for the search
     * @returns Array of users
     */
    /**
     * Gets a list of workflow users.
     * @param {?} filter Filter to select specific users
     * @param {?=} groupId Group ID for the search
     * @return {?} Array of users
     */
    FormService.prototype.getWorkflowUsers = /**
     * Gets a list of workflow users.
     * @param {?} filter Filter to select specific users
     * @param {?=} groupId Group ID for the search
     * @return {?} Array of users
     */
    function (filter, groupId) {
        var _this = this;
        /** @type {?} */
        var option = { filter: filter };
        if (groupId) {
            option.groupId = groupId;
        }
        return from(this.usersWorkflowApi.getUsers(option))
            .pipe(switchMap((/**
         * @param {?} response
         * @return {?}
         */
        function (response) { return (/** @type {?} */ (response.data)) || []; })), map((/**
         * @param {?} user
         * @return {?}
         */
        function (user) {
            user.userImage = _this.getUserProfileImageApi(user.id);
            return of(user);
        })), combineAll(), defaultIfEmpty([]), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets a list of groups in a workflow.
     * @param filter Filter to select specific groups
     * @param groupId Group ID for the search
     * @returns Array of groups
     */
    /**
     * Gets a list of groups in a workflow.
     * @param {?} filter Filter to select specific groups
     * @param {?=} groupId Group ID for the search
     * @return {?} Array of groups
     */
    FormService.prototype.getWorkflowGroups = /**
     * Gets a list of groups in a workflow.
     * @param {?} filter Filter to select specific groups
     * @param {?=} groupId Group ID for the search
     * @return {?} Array of groups
     */
    function (filter, groupId) {
        var _this = this;
        /** @type {?} */
        var option = { filter: filter };
        if (groupId) {
            option.groupId = groupId;
        }
        return from(this.groupsApi.getGroups(option))
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) { return (/** @type {?} */ (response.data)) || []; })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets the ID of a form.
     * @param form Object representing a form
     * @returns ID string
     */
    /**
     * Gets the ID of a form.
     * @param {?} form Object representing a form
     * @return {?} ID string
     */
    FormService.prototype.getFormId = /**
     * Gets the ID of a form.
     * @param {?} form Object representing a form
     * @return {?} ID string
     */
    function (form) {
        /** @type {?} */
        var result = null;
        if (form && form.data && form.data.length > 0) {
            result = form.data[0].id;
        }
        return result;
    };
    /**
     * Creates a JSON representation of form data.
     * @param res Object representing form data
     * @returns JSON data
     */
    /**
     * Creates a JSON representation of form data.
     * @param {?} res Object representing form data
     * @return {?} JSON data
     */
    FormService.prototype.toJson = /**
     * Creates a JSON representation of form data.
     * @param {?} res Object representing form data
     * @return {?} JSON data
     */
    function (res) {
        if (res) {
            return res || {};
        }
        return {};
    };
    /**
     * Creates a JSON array representation of form data.
     * @param res Object representing form data
     * @returns JSON data
     */
    /**
     * Creates a JSON array representation of form data.
     * @param {?} res Object representing form data
     * @return {?} JSON data
     */
    FormService.prototype.toJsonArray = /**
     * Creates a JSON array representation of form data.
     * @param {?} res Object representing form data
     * @return {?} JSON data
     */
    function (res) {
        if (res) {
            return res.data || [];
        }
        return [];
    };
    /**
     * Reports an error message.
     * @param error Data object with optional `message` and `status` fields for the error
     * @returns Error message
     */
    /**
     * Reports an error message.
     * @param {?} error Data object with optional `message` and `status` fields for the error
     * @return {?} Error message
     */
    FormService.prototype.handleError = /**
     * Reports an error message.
     * @param {?} error Data object with optional `message` and `status` fields for the error
     * @return {?} Error message
     */
    function (error) {
        /** @type {?} */
        var errMsg = FormService.UNKNOWN_ERROR_MESSAGE;
        if (error) {
            errMsg = (error.message) ? error.message :
                error.status ? error.status + " - " + error.statusText : FormService.GENERIC_ERROR_MESSAGE;
        }
        this.logService.error(errMsg);
        return throwError(errMsg);
    };
    FormService.UNKNOWN_ERROR_MESSAGE = 'Unknown error';
    FormService.GENERIC_ERROR_MESSAGE = 'Server error';
    FormService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    FormService.ctorParameters = function () { return [
        { type: EcmModelService },
        { type: AlfrescoApiService },
        { type: LogService }
    ]; };
    /** @nocollapse */ FormService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function FormService_Factory() { return new FormService(i0.ɵɵinject(i1.EcmModelService), i0.ɵɵinject(i2.AlfrescoApiService), i0.ɵɵinject(i3.LogService)); }, token: FormService, providedIn: "root" });
    return FormService;
}());
export { FormService };
if (false) {
    /** @type {?} */
    FormService.UNKNOWN_ERROR_MESSAGE;
    /** @type {?} */
    FormService.GENERIC_ERROR_MESSAGE;
    /** @type {?} */
    FormService.prototype.formLoaded;
    /** @type {?} */
    FormService.prototype.formDataRefreshed;
    /** @type {?} */
    FormService.prototype.formFieldValueChanged;
    /** @type {?} */
    FormService.prototype.formEvents;
    /** @type {?} */
    FormService.prototype.taskCompleted;
    /** @type {?} */
    FormService.prototype.taskCompletedError;
    /** @type {?} */
    FormService.prototype.taskSaved;
    /** @type {?} */
    FormService.prototype.taskSavedError;
    /** @type {?} */
    FormService.prototype.formContentClicked;
    /** @type {?} */
    FormService.prototype.validateForm;
    /** @type {?} */
    FormService.prototype.validateFormField;
    /** @type {?} */
    FormService.prototype.validateDynamicTableRow;
    /** @type {?} */
    FormService.prototype.executeOutcome;
    /**
     * @type {?}
     * @private
     */
    FormService.prototype.ecmModelService;
    /**
     * @type {?}
     * @private
     */
    FormService.prototype.apiService;
    /**
     * @type {?}
     * @protected
     */
    FormService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9zZXJ2aWNlcy9mb3JtLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDekUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBRXhELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLEVBQUUsRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDakUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFHdEUsT0FBTyxFQUFFLFNBQVMsRUFBb0IsZ0JBQWdCLEVBQWMsTUFBTSxvQ0FBb0MsQ0FBQztBQUsvRyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdEQsT0FBTyxFQUFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFVBQVUsRUFBRSxjQUFjLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7Ozs7QUFPeEY7SUF3QkkscUJBQW9CLGVBQWdDLEVBQ2hDLFVBQThCLEVBQzVCLFVBQXNCO1FBRnhCLG9CQUFlLEdBQWYsZUFBZSxDQUFpQjtRQUNoQyxlQUFVLEdBQVYsVUFBVSxDQUFvQjtRQUM1QixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBbEI1QyxlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQWEsQ0FBQztRQUN0QyxzQkFBaUIsR0FBRyxJQUFJLE9BQU8sRUFBYSxDQUFDO1FBQzdDLDBCQUFxQixHQUFHLElBQUksT0FBTyxFQUFrQixDQUFDO1FBQ3RELGVBQVUsR0FBRyxJQUFJLE9BQU8sRUFBUyxDQUFDO1FBQ2xDLGtCQUFhLEdBQUcsSUFBSSxPQUFPLEVBQWEsQ0FBQztRQUN6Qyx1QkFBa0IsR0FBRyxJQUFJLE9BQU8sRUFBa0IsQ0FBQztRQUNuRCxjQUFTLEdBQUcsSUFBSSxPQUFPLEVBQWEsQ0FBQztRQUNyQyxtQkFBYyxHQUFHLElBQUksT0FBTyxFQUFrQixDQUFDO1FBQy9DLHVCQUFrQixHQUFHLElBQUksT0FBTyxFQUFvQixDQUFDO1FBRXJELGlCQUFZLEdBQUcsSUFBSSxPQUFPLEVBQXFCLENBQUM7UUFDaEQsc0JBQWlCLEdBQUcsSUFBSSxPQUFPLEVBQTBCLENBQUM7UUFDMUQsNEJBQXVCLEdBQUcsSUFBSSxPQUFPLEVBQWdDLENBQUM7UUFFdEUsbUJBQWMsR0FBRyxJQUFJLE9BQU8sRUFBb0IsQ0FBQztJQUtqRCxDQUFDO0lBRUQsc0JBQVksZ0NBQU87Ozs7O1FBQW5CO1lBQ0ksT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUM7UUFDMUQsQ0FBQzs7O09BQUE7SUFFRCxzQkFBWSxrQ0FBUzs7Ozs7UUFBckI7WUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFNBQVMsQ0FBQztRQUM1RCxDQUFDOzs7T0FBQTtJQUVELHNCQUFZLGtDQUFTOzs7OztRQUFyQjtZQUNJLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsU0FBUyxDQUFDO1FBQzVELENBQUM7OztPQUFBO0lBRUQsc0JBQVksbUNBQVU7Ozs7O1FBQXRCO1lBQ0ksT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUM7UUFDN0QsQ0FBQzs7O09BQUE7SUFFRCxzQkFBWSxvREFBMkI7Ozs7O1FBQXZDO1lBQ0ksT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQywyQkFBMkIsQ0FBQztRQUM5RSxDQUFDOzs7T0FBQTtJQUVELHNCQUFZLHlDQUFnQjs7Ozs7UUFBNUI7WUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLGdCQUFnQixDQUFDO1FBQ25FLENBQUM7OztPQUFBO0lBRUQsc0JBQVksa0NBQVM7Ozs7O1FBQXJCO1lBQ0ksT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxTQUFTLENBQUM7UUFDNUQsQ0FBQzs7O09BQUE7SUFFRDs7Ozs7O09BTUc7Ozs7Ozs7O0lBQ0gsK0JBQVM7Ozs7Ozs7SUFBVCxVQUFVLElBQVMsRUFBRSxJQUFpQixFQUFFLFFBQXlCO1FBQXpCLHlCQUFBLEVBQUEsZ0JBQXlCO1FBQzdELElBQUksSUFBSSxFQUFFOztnQkFDQSxJQUFJLEdBQUcsSUFBSSxTQUFTLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDO1lBQ3RELElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUNkLElBQUksQ0FBQyxRQUFRLEdBQUc7b0JBQ1osSUFBSSxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUU7d0JBQ3ZCLEVBQUUsRUFBRSxPQUFPO3dCQUNYLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxXQUFXO3dCQUNsQyxRQUFRLEVBQUUsSUFBSTtxQkFDakIsQ0FBQztpQkFDTCxDQUFDO2FBQ0w7WUFDRCxPQUFPLElBQUksQ0FBQztTQUNmO1FBQ0QsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVEOzs7O09BSUc7Ozs7OztJQUNILHlDQUFtQjs7Ozs7SUFBbkIsVUFBb0IsUUFBZ0I7UUFBcEMsaUJBa0JDO1FBakJHLE9BQU8sSUFBSSxVQUFVOzs7O1FBQUMsVUFBQyxRQUFRO1lBQzNCLEtBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxDQUFDLENBQUMsU0FBUzs7OztZQUMvQixVQUFDLElBQUk7Z0JBQ0QsS0FBSSxDQUFDLGVBQWUsQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLGVBQWUsQ0FBQyxVQUFVLENBQUMsQ0FBQyxTQUFTOzs7O2dCQUM5RSxVQUFDLFVBQVU7O3dCQUNELG1CQUFtQixHQUFHLElBQUksbUJBQW1CLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxJQUFJLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxJQUFJLENBQUMsV0FBVyxFQUFFLFVBQVUsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDO29CQUNsSixJQUFJLENBQ0EsS0FBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxtQkFBbUIsQ0FBQyxDQUN4RCxDQUFDLFNBQVM7Ozs7b0JBQUMsVUFBQyxRQUFRO3dCQUNqQixRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO3dCQUN4QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7b0JBQ3hCLENBQUM7Ozs7b0JBQUUsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQUM7Z0JBQ3ZDLENBQUM7Ozs7Z0JBQ0QsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQUM7WUFDeEMsQ0FBQzs7OztZQUNELFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFBQyxDQUFDO1FBQ3hDLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQztJQUVEOzs7O09BSUc7Ozs7OztJQUNILGdDQUFVOzs7OztJQUFWLFVBQVcsUUFBZ0I7O1lBQ2pCLFNBQVMsR0FBRztZQUNkLElBQUksRUFBRSxRQUFRO1lBQ2QsV0FBVyxFQUFFLEVBQUU7WUFDZixTQUFTLEVBQUUsQ0FBQztZQUNaLFVBQVUsRUFBRSxDQUFDO1NBQ2hCO1FBRUQsT0FBTyxJQUFJLENBQ1AsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsU0FBUyxDQUFDLENBQ3hDLENBQUM7SUFDTixDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCw4QkFBUTs7Ozs7O0lBQVIsVUFBUyxNQUFjLEVBQUUsU0FBOEI7UUFDbkQsT0FBTyxJQUFJLENBQ1AsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLFNBQVMsQ0FBQyxDQUM3QyxDQUFDO0lBQ04sQ0FBQztJQUVEOzs7O09BSUc7Ozs7OztJQUNILGdDQUFVOzs7OztJQUFWLFVBQVcsSUFBWTtRQUF2QixpQkFjQzs7WUFiUyxJQUFJLEdBQUc7WUFDVCxXQUFXLEVBQUUsQ0FBQztTQUNqQjtRQUVELE9BQU8sSUFBSSxDQUNQLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUNqQzthQUNJLElBQUksQ0FDRCxHQUFHOzs7O1FBQUMsVUFBVSxLQUFVO1lBQ3BCLE9BQU8sS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJOzs7O1lBQUMsVUFBQyxRQUFRLElBQUssT0FBQSxRQUFRLENBQUMsSUFBSSxLQUFLLElBQUksRUFBdEIsQ0FBc0IsRUFBQyxDQUFDO1FBQ2pFLENBQUMsRUFBQyxFQUNGLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7SUFFRDs7O09BR0c7Ozs7O0lBQ0gsOEJBQVE7Ozs7SUFBUjtRQUFBLGlCQVVDOztZQVRTLElBQUksR0FBRztZQUNULFdBQVcsRUFBRSxDQUFDO1NBQ2pCO1FBRUQsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7YUFDdEMsSUFBSSxDQUNELEdBQUcsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLEVBQ3JCLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7SUFFRDs7O09BR0c7Ozs7O0lBQ0gsMkNBQXFCOzs7O0lBQXJCO1FBQUEsaUJBTUM7UUFMRyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLHFCQUFxQixDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQ2pELElBQUksQ0FDRCxHQUFHLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxFQUNyQixVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsNkNBQXVCOzs7OztJQUF2QixVQUF3QixpQkFBeUI7UUFBakQsaUJBTUM7UUFMRyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsMkJBQTJCLENBQUMsMkJBQTJCLENBQUMsaUJBQWlCLENBQUMsQ0FBQzthQUN2RixJQUFJLENBQ0QsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFDaEIsVUFBVTs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7SUFDSCw4QkFBUTs7OztJQUFSO1FBQUEsaUJBTUM7UUFMRyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUNsQyxJQUFJLENBQ0QsR0FBRyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsRUFDckIsVUFBVTs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQztJQUVEOzs7O09BSUc7Ozs7OztJQUNILDZCQUFPOzs7OztJQUFQLFVBQVEsTUFBYztRQUF0QixpQkFNQztRQUxHLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ3BDLElBQUksQ0FDRCxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUNoQixVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCxrQ0FBWTs7Ozs7O0lBQVosVUFBYSxNQUFjLEVBQUUsVUFBc0I7UUFBbkQsaUJBT0M7O1lBTlMsc0JBQXNCLEdBQUcsbUJBQXlCLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRSxFQUFBO1FBRTlFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxzQkFBc0IsQ0FBQyxDQUFDO2FBQ2pFLElBQUksQ0FDRCxVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDO0lBRUQ7Ozs7OztPQU1HOzs7Ozs7OztJQUNILHNDQUFnQjs7Ozs7OztJQUFoQixVQUFpQixNQUFjLEVBQUUsVUFBc0IsRUFBRSxPQUFnQjtRQUF6RSxpQkFVQzs7WUFUUywwQkFBMEIsR0FBUSxtQkFBNkIsRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLEVBQUE7UUFDM0YsSUFBSSxPQUFPLEVBQUU7WUFDVCwwQkFBMEIsQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1NBQ2hEO1FBRUQsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLEVBQUUsMEJBQTBCLENBQUMsQ0FBQzthQUN6RSxJQUFJLENBQ0QsVUFBVTs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQztJQUVEOzs7O09BSUc7Ozs7OztJQUNILGlDQUFXOzs7OztJQUFYLFVBQVksTUFBYztRQUExQixpQkFNQztRQUxHLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQ3hDLElBQUksQ0FDRCxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUNoQixVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsMkNBQXFCOzs7OztJQUFyQixVQUFzQixNQUFjO1FBQXBDLGlCQU1DO1FBTEcsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxPQUFPLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDdEMsSUFBSSxDQUNELEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQ2hCLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCw2Q0FBdUI7Ozs7O0lBQXZCLFVBQXdCLElBQVk7UUFBcEMsaUJBWUM7O1lBWFMsSUFBSSxHQUFHO1lBQ1QsUUFBUSxFQUFFLGlCQUFpQjtZQUMzQixZQUFZLEVBQUUsSUFBSTtZQUNsQixXQUFXLEVBQUUsQ0FBQztTQUNqQjtRQUVELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO2FBQ3RDLElBQUksQ0FDRCxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxFQUNuQixVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsMENBQW9COzs7OztJQUFwQixVQUFxQixTQUFpQjtRQUF0QyxpQkFNQztRQUxHLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsMkJBQTJCLENBQUMsU0FBUyxDQUFDLENBQUM7YUFDOUQsSUFBSSxDQUNELEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQ2hCLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCx3Q0FBa0I7Ozs7O0lBQWxCLFVBQW1CLFNBQWlCO1FBQXBDLGlCQU1DO1FBTEcsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxTQUFTLENBQUMsQ0FBQzthQUNyRCxJQUFJLENBQ0QsR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsRUFDaEIsVUFBVTs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQztJQUVEOzs7O09BSUc7Ozs7OztJQUNILDRDQUFzQjs7Ozs7SUFBdEIsVUFBdUIsU0FBaUI7UUFBeEMsaUJBTUM7UUFMRyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLDZCQUE2QixDQUFDLFNBQVMsQ0FBQyxDQUFDO2FBQ2hFLElBQUksQ0FDRCxHQUFHLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUNoQixVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCx3Q0FBa0I7Ozs7OztJQUFsQixVQUFtQixNQUFjLEVBQUUsS0FBYTtRQUFoRCxpQkFLQztRQUpHLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsa0JBQWtCLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO2FBQ3RELElBQUksQ0FDRCxVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCxtREFBNkI7Ozs7OztJQUE3QixVQUE4QixtQkFBMkIsRUFBRSxLQUFhO1FBQXhFLGlCQUtDO1FBSkcsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxrQkFBa0IsQ0FBQyxtQkFBbUIsRUFBRSxLQUFLLENBQUMsQ0FBQzthQUN0RSxJQUFJLENBQ0QsVUFBVTs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQztJQUVEOzs7Ozs7T0FNRzs7Ozs7Ozs7SUFDSCx5REFBbUM7Ozs7Ozs7SUFBbkMsVUFBb0MsbUJBQTJCLEVBQUUsS0FBYSxFQUFFLE1BQWU7UUFBL0YsaUJBS0M7UUFKRyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLHVCQUF1QixDQUFDLG1CQUFtQixFQUFFLEtBQUssRUFBRSxNQUFNLENBQUMsQ0FBQzthQUNuRixJQUFJLENBQ0QsVUFBVTs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQztJQUVEOzs7Ozs7T0FNRzs7Ozs7Ozs7SUFDSCw4Q0FBd0I7Ozs7Ozs7SUFBeEIsVUFBeUIsTUFBYyxFQUFFLEtBQWEsRUFBRSxNQUFlO1FBQXZFLGlCQUtDO1FBSkcsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyx3QkFBd0IsQ0FBQyxNQUFNLEVBQUUsS0FBSyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQ3BFLElBQUksQ0FDRCxVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsNENBQXNCOzs7OztJQUF0QixVQUF1QixNQUFjO1FBQ2pDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLHdCQUF3QixDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQzNGLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILHNDQUFnQjs7Ozs7O0lBQWhCLFVBQWlCLE1BQWMsRUFBRSxPQUFnQjtRQUFqRCxpQkFnQkM7O1lBZlMsTUFBTSxHQUFRLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRTtRQUN0QyxJQUFJLE9BQU8sRUFBRTtZQUNULE1BQU0sQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO1NBQzVCO1FBQ0QsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUM5QyxJQUFJLENBQ0QsU0FBUzs7OztRQUFDLFVBQUMsUUFBYSxJQUFLLE9BQUEsbUJBQXFCLFFBQVEsQ0FBQyxJQUFJLEVBQUEsSUFBSSxFQUFFLEVBQXhDLENBQXdDLEVBQUMsRUFDdEUsR0FBRzs7OztRQUFDLFVBQUMsSUFBUztZQUNWLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztZQUN0RCxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQixDQUFDLEVBQUMsRUFDRixVQUFVLEVBQUUsRUFDWixjQUFjLENBQUMsRUFBRSxDQUFDLEVBQ2xCLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILHVDQUFpQjs7Ozs7O0lBQWpCLFVBQWtCLE1BQWMsRUFBRSxPQUFnQjtRQUFsRCxpQkFVQzs7WUFUUyxNQUFNLEdBQVEsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFO1FBQ3RDLElBQUksT0FBTyxFQUFFO1lBQ1QsTUFBTSxDQUFDLE9BQU8sR0FBRyxPQUFPLENBQUM7U0FDNUI7UUFDRCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUN4QyxJQUFJLENBQ0QsR0FBRzs7OztRQUFDLFVBQUMsUUFBYSxJQUFLLE9BQUEsbUJBQWUsUUFBUSxDQUFDLElBQUksRUFBQSxJQUFJLEVBQUUsRUFBbEMsQ0FBa0MsRUFBQyxFQUMxRCxVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsK0JBQVM7Ozs7O0lBQVQsVUFBVSxJQUFTOztZQUNYLE1BQU0sR0FBRyxJQUFJO1FBRWpCLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzNDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztTQUM1QjtRQUVELE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCw0QkFBTTs7Ozs7SUFBTixVQUFPLEdBQVE7UUFDWCxJQUFJLEdBQUcsRUFBRTtZQUNMLE9BQU8sR0FBRyxJQUFJLEVBQUUsQ0FBQztTQUNwQjtRQUNELE9BQU8sRUFBRSxDQUFDO0lBQ2QsQ0FBQztJQUVEOzs7O09BSUc7Ozs7OztJQUNILGlDQUFXOzs7OztJQUFYLFVBQVksR0FBUTtRQUNoQixJQUFJLEdBQUcsRUFBRTtZQUNMLE9BQU8sR0FBRyxDQUFDLElBQUksSUFBSSxFQUFFLENBQUM7U0FDekI7UUFDRCxPQUFPLEVBQUUsQ0FBQztJQUNkLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCxpQ0FBVzs7Ozs7SUFBWCxVQUFZLEtBQVU7O1lBQ2QsTUFBTSxHQUFHLFdBQVcsQ0FBQyxxQkFBcUI7UUFDOUMsSUFBSSxLQUFLLEVBQUU7WUFDUCxNQUFNLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztnQkFDdEMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUksS0FBSyxDQUFDLE1BQU0sV0FBTSxLQUFLLENBQUMsVUFBWSxDQUFDLENBQUMsQ0FBQyxXQUFXLENBQUMscUJBQXFCLENBQUM7U0FDbEc7UUFDRCxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUM5QixPQUFPLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUM5QixDQUFDO0lBN2VNLGlDQUFxQixHQUFXLGVBQWUsQ0FBQztJQUNoRCxpQ0FBcUIsR0FBVyxjQUFjLENBQUM7O2dCQU56RCxVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7O2dCQVZRLGVBQWU7Z0JBYmYsa0JBQWtCO2dCQUNsQixVQUFVOzs7c0JBbEJuQjtDQXloQkMsQUFuZkQsSUFtZkM7U0FoZlksV0FBVzs7O0lBRXBCLGtDQUF1RDs7SUFDdkQsa0NBQXNEOztJQUV0RCxpQ0FBc0M7O0lBQ3RDLHdDQUE2Qzs7SUFDN0MsNENBQXNEOztJQUN0RCxpQ0FBa0M7O0lBQ2xDLG9DQUF5Qzs7SUFDekMseUNBQW1EOztJQUNuRCxnQ0FBcUM7O0lBQ3JDLHFDQUErQzs7SUFDL0MseUNBQXFEOztJQUVyRCxtQ0FBZ0Q7O0lBQ2hELHdDQUEwRDs7SUFDMUQsOENBQXNFOztJQUV0RSxxQ0FBaUQ7Ozs7O0lBRXJDLHNDQUF3Qzs7Ozs7SUFDeEMsaUNBQXNDOzs7OztJQUN0QyxpQ0FBZ0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMb2dTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbG9nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBVc2VyUHJvY2Vzc01vZGVsIH0gZnJvbSAnLi4vLi4vbW9kZWxzJztcclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBTdWJqZWN0LCBmcm9tLCBvZiwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBGb3JtRGVmaW5pdGlvbk1vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL2Zvcm0tZGVmaW5pdGlvbi5tb2RlbCc7XHJcbmltcG9ydCB7IENvbnRlbnRMaW5rTW9kZWwgfSBmcm9tICcuLy4uL2NvbXBvbmVudHMvd2lkZ2V0cy9jb3JlL2NvbnRlbnQtbGluay5tb2RlbCc7XHJcbmltcG9ydCB7IEdyb3VwTW9kZWwgfSBmcm9tICcuLy4uL2NvbXBvbmVudHMvd2lkZ2V0cy9jb3JlL2dyb3VwLm1vZGVsJztcclxuaW1wb3J0IHsgRm9ybU1vZGVsLCBGb3JtT3V0Y29tZUV2ZW50LCBGb3JtT3V0Y29tZU1vZGVsLCBGb3JtVmFsdWVzIH0gZnJvbSAnLi8uLi9jb21wb25lbnRzL3dpZGdldHMvY29yZS9pbmRleCc7XHJcbmltcG9ydCB7XHJcbiAgICBGb3JtRXJyb3JFdmVudCwgRm9ybUV2ZW50LCBGb3JtRmllbGRFdmVudCxcclxuICAgIFZhbGlkYXRlRHluYW1pY1RhYmxlUm93RXZlbnQsIFZhbGlkYXRlRm9ybUV2ZW50LCBWYWxpZGF0ZUZvcm1GaWVsZEV2ZW50XHJcbn0gZnJvbSAnLi8uLi9ldmVudHMvaW5kZXgnO1xyXG5pbXBvcnQgeyBFY21Nb2RlbFNlcnZpY2UgfSBmcm9tICcuL2VjbS1tb2RlbC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgbWFwLCBjYXRjaEVycm9yLCBzd2l0Y2hNYXAsIGNvbWJpbmVBbGwsIGRlZmF1bHRJZkVtcHR5IH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQge1xyXG4gICAgQWN0aXZpdGksXHJcbiAgICBDb21wbGV0ZUZvcm1SZXByZXNlbnRhdGlvbixcclxuICAgIFNhdmVGb3JtUmVwcmVzZW50YXRpb25cclxufSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRm9ybVNlcnZpY2Uge1xyXG5cclxuICAgIHN0YXRpYyBVTktOT1dOX0VSUk9SX01FU1NBR0U6IHN0cmluZyA9ICdVbmtub3duIGVycm9yJztcclxuICAgIHN0YXRpYyBHRU5FUklDX0VSUk9SX01FU1NBR0U6IHN0cmluZyA9ICdTZXJ2ZXIgZXJyb3InO1xyXG5cclxuICAgIGZvcm1Mb2FkZWQgPSBuZXcgU3ViamVjdDxGb3JtRXZlbnQ+KCk7XHJcbiAgICBmb3JtRGF0YVJlZnJlc2hlZCA9IG5ldyBTdWJqZWN0PEZvcm1FdmVudD4oKTtcclxuICAgIGZvcm1GaWVsZFZhbHVlQ2hhbmdlZCA9IG5ldyBTdWJqZWN0PEZvcm1GaWVsZEV2ZW50PigpO1xyXG4gICAgZm9ybUV2ZW50cyA9IG5ldyBTdWJqZWN0PEV2ZW50PigpO1xyXG4gICAgdGFza0NvbXBsZXRlZCA9IG5ldyBTdWJqZWN0PEZvcm1FdmVudD4oKTtcclxuICAgIHRhc2tDb21wbGV0ZWRFcnJvciA9IG5ldyBTdWJqZWN0PEZvcm1FcnJvckV2ZW50PigpO1xyXG4gICAgdGFza1NhdmVkID0gbmV3IFN1YmplY3Q8Rm9ybUV2ZW50PigpO1xyXG4gICAgdGFza1NhdmVkRXJyb3IgPSBuZXcgU3ViamVjdDxGb3JtRXJyb3JFdmVudD4oKTtcclxuICAgIGZvcm1Db250ZW50Q2xpY2tlZCA9IG5ldyBTdWJqZWN0PENvbnRlbnRMaW5rTW9kZWw+KCk7XHJcblxyXG4gICAgdmFsaWRhdGVGb3JtID0gbmV3IFN1YmplY3Q8VmFsaWRhdGVGb3JtRXZlbnQ+KCk7XHJcbiAgICB2YWxpZGF0ZUZvcm1GaWVsZCA9IG5ldyBTdWJqZWN0PFZhbGlkYXRlRm9ybUZpZWxkRXZlbnQ+KCk7XHJcbiAgICB2YWxpZGF0ZUR5bmFtaWNUYWJsZVJvdyA9IG5ldyBTdWJqZWN0PFZhbGlkYXRlRHluYW1pY1RhYmxlUm93RXZlbnQ+KCk7XHJcblxyXG4gICAgZXhlY3V0ZU91dGNvbWUgPSBuZXcgU3ViamVjdDxGb3JtT3V0Y29tZUV2ZW50PigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgZWNtTW9kZWxTZXJ2aWNlOiBFY21Nb2RlbFNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByb3RlY3RlZCBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXQgdGFza0FwaSgpOiBBY3Rpdml0aS5UYXNrQXBpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuYWN0aXZpdGkudGFza0FwaTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldCBtb2RlbHNBcGkoKTogQWN0aXZpdGkuTW9kZWxzQXBpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuYWN0aXZpdGkubW9kZWxzQXBpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0IGVkaXRvckFwaSgpOiBBY3Rpdml0aS5FZGl0b3JBcGkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5hY3Rpdml0aS5lZGl0b3JBcGk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXQgcHJvY2Vzc0FwaSgpOiBBY3Rpdml0aS5Qcm9jZXNzQXBpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuYWN0aXZpdGkucHJvY2Vzc0FwaTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldCBwcm9jZXNzSW5zdGFuY2VWYXJpYWJsZXNBcGkoKTogQWN0aXZpdGkuUHJvY2Vzc0luc3RhbmNlVmFyaWFibGVzQXBpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuYWN0aXZpdGkucHJvY2Vzc0luc3RhbmNlVmFyaWFibGVzQXBpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0IHVzZXJzV29ya2Zsb3dBcGkoKTogQWN0aXZpdGkuVXNlcnNXb3JrZmxvd0FwaSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmFjdGl2aXRpLnVzZXJzV29ya2Zsb3dBcGk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXQgZ3JvdXBzQXBpKCk6IEFjdGl2aXRpLkdyb3Vwc0FwaSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmFjdGl2aXRpLmdyb3Vwc0FwaTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFBhcnNlcyBKU09OIGRhdGEgdG8gY3JlYXRlIGEgY29ycmVzcG9uZGluZyBGb3JtIG1vZGVsLlxyXG4gICAgICogQHBhcmFtIGpzb24gSlNPTiB0byBjcmVhdGUgdGhlIGZvcm1cclxuICAgICAqIEBwYXJhbSBkYXRhIFZhbHVlcyBmb3IgdGhlIGZvcm0gZmllbGRzXHJcbiAgICAgKiBAcGFyYW0gcmVhZE9ubHkgU2hvdWxkIHRoZSBmb3JtIGZpZWxkcyBiZSByZWFkLW9ubHk/XHJcbiAgICAgKiBAcmV0dXJucyBGb3JtIG1vZGVsIGNyZWF0ZWQgZnJvbSBpbnB1dCBkYXRhXHJcbiAgICAgKi9cclxuICAgIHBhcnNlRm9ybShqc29uOiBhbnksIGRhdGE/OiBGb3JtVmFsdWVzLCByZWFkT25seTogYm9vbGVhbiA9IGZhbHNlKTogRm9ybU1vZGVsIHtcclxuICAgICAgICBpZiAoanNvbikge1xyXG4gICAgICAgICAgICBjb25zdCBmb3JtID0gbmV3IEZvcm1Nb2RlbChqc29uLCBkYXRhLCByZWFkT25seSwgdGhpcyk7XHJcbiAgICAgICAgICAgIGlmICghanNvbi5maWVsZHMpIHtcclxuICAgICAgICAgICAgICAgIGZvcm0ub3V0Y29tZXMgPSBbXHJcbiAgICAgICAgICAgICAgICAgICAgbmV3IEZvcm1PdXRjb21lTW9kZWwoZm9ybSwge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogJyRzYXZlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogRm9ybU91dGNvbWVNb2RlbC5TQVZFX0FDVElPTixcclxuICAgICAgICAgICAgICAgICAgICAgICAgaXNTeXN0ZW06IHRydWVcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgXTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gZm9ybTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDcmVhdGVzIGEgRm9ybSB3aXRoIGEgZmllbGQgZm9yIGVhY2ggbWV0YWRhdGEgcHJvcGVydHkuXHJcbiAgICAgKiBAcGFyYW0gZm9ybU5hbWUgTmFtZSBvZiB0aGUgbmV3IGZvcm1cclxuICAgICAqIEByZXR1cm5zIFRoZSBuZXcgZm9ybVxyXG4gICAgICovXHJcbiAgICBjcmVhdGVGb3JtRnJvbUFOb2RlKGZvcm1OYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZSgob2JzZXJ2ZXIpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5jcmVhdGVGb3JtKGZvcm1OYW1lKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAoZm9ybSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZWNtTW9kZWxTZXJ2aWNlLnNlYXJjaEVjbVR5cGUoZm9ybU5hbWUsIEVjbU1vZGVsU2VydmljZS5NT0RFTF9OQU1FKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIChjdXN0b21UeXBlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBmb3JtRGVmaW5pdGlvbk1vZGVsID0gbmV3IEZvcm1EZWZpbml0aW9uTW9kZWwoZm9ybS5pZCwgZm9ybS5uYW1lLCBmb3JtLmxhc3RVcGRhdGVkQnlGdWxsTmFtZSwgZm9ybS5sYXN0VXBkYXRlZCwgY3VzdG9tVHlwZS5lbnRyeS5wcm9wZXJ0aWVzKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZyb20oXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5lZGl0b3JBcGkuc2F2ZUZvcm0oZm9ybS5pZCwgZm9ybURlZmluaXRpb25Nb2RlbClcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICkuc3Vic2NyaWJlKChmb3JtRGF0YSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZm9ybURhdGEpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LCAoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpO1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDcmVhdGUgYSBGb3JtLlxyXG4gICAgICogQHBhcmFtIGZvcm1OYW1lIE5hbWUgb2YgdGhlIG5ldyBmb3JtXHJcbiAgICAgKiBAcmV0dXJucyBUaGUgbmV3IGZvcm1cclxuICAgICAqL1xyXG4gICAgY3JlYXRlRm9ybShmb3JtTmFtZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBkYXRhTW9kZWwgPSB7XHJcbiAgICAgICAgICAgIG5hbWU6IGZvcm1OYW1lLFxyXG4gICAgICAgICAgICBkZXNjcmlwdGlvbjogJycsXHJcbiAgICAgICAgICAgIG1vZGVsVHlwZTogMixcclxuICAgICAgICAgICAgc3RlbmNpbFNldDogMFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKFxyXG4gICAgICAgICAgICB0aGlzLm1vZGVsc0FwaS5jcmVhdGVNb2RlbChkYXRhTW9kZWwpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNhdmVzIGEgZm9ybS5cclxuICAgICAqIEBwYXJhbSBmb3JtSWQgSUQgb2YgdGhlIGZvcm0gdG8gc2F2ZVxyXG4gICAgICogQHBhcmFtIGZvcm1Nb2RlbCBNb2RlbCBkYXRhIGZvciB0aGUgZm9ybVxyXG4gICAgICogQHJldHVybnMgRGF0YSBmb3IgdGhlIHNhdmVkIGZvcm1cclxuICAgICAqL1xyXG4gICAgc2F2ZUZvcm0oZm9ybUlkOiBudW1iZXIsIGZvcm1Nb2RlbDogRm9ybURlZmluaXRpb25Nb2RlbCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20oXHJcbiAgICAgICAgICAgIHRoaXMuZWRpdG9yQXBpLnNhdmVGb3JtKGZvcm1JZCwgZm9ybU1vZGVsKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZWFyY2hlcyBmb3IgYSBmb3JtIGJ5IG5hbWUuXHJcbiAgICAgKiBAcGFyYW0gbmFtZSBUaGUgZm9ybSBuYW1lIHRvIHNlYXJjaCBmb3JcclxuICAgICAqIEByZXR1cm5zIEZvcm0gbW9kZWwocykgbWF0Y2hpbmcgdGhlIHNlYXJjaCBuYW1lXHJcbiAgICAgKi9cclxuICAgIHNlYXJjaEZyb20obmFtZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBvcHRzID0ge1xyXG4gICAgICAgICAgICAnbW9kZWxUeXBlJzogMlxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKFxyXG4gICAgICAgICAgICB0aGlzLm1vZGVsc0FwaS5nZXRNb2RlbHMob3B0cylcclxuICAgICAgICApXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKGZ1bmN0aW9uIChmb3JtczogYW55KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGZvcm1zLmRhdGEuZmluZCgoZm9ybURhdGEpID0+IGZvcm1EYXRhLm5hbWUgPT09IG5hbWUpO1xyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYWxsIHRoZSBmb3Jtcy5cclxuICAgICAqIEByZXR1cm5zIExpc3Qgb2YgZm9ybSBtb2RlbHNcclxuICAgICAqL1xyXG4gICAgZ2V0Rm9ybXMoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBvcHRzID0ge1xyXG4gICAgICAgICAgICAnbW9kZWxUeXBlJzogMlxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMubW9kZWxzQXBpLmdldE1vZGVscyhvcHRzKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy50b0pzb25BcnJheSksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgcHJvY2VzcyBkZWZpbml0aW9ucy5cclxuICAgICAqIEByZXR1cm5zIExpc3Qgb2YgcHJvY2VzcyBkZWZpbml0aW9uc1xyXG4gICAgICovXHJcbiAgICBnZXRQcm9jZXNzRGVmaW5pdGlvbnMoKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLnByb2Nlc3NBcGkuZ2V0UHJvY2Vzc0RlZmluaXRpb25zKHt9KSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy50b0pzb25BcnJheSksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgaW5zdGFuY2UgdmFyaWFibGVzIGZvciBhIHByb2Nlc3MuXHJcbiAgICAgKiBAcGFyYW0gcHJvY2Vzc0luc3RhbmNlSWQgSUQgb2YgdGhlIHRhcmdldCBwcm9jZXNzXHJcbiAgICAgKiBAcmV0dXJucyBMaXN0IG9mIGluc3RhbmNlIHZhcmlhYmxlIGluZm9ybWF0aW9uXHJcbiAgICAgKi9cclxuICAgIGdldFByb2Nlc3NWYXJpYWJsZXNCeUlkKHByb2Nlc3NJbnN0YW5jZUlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueVtdPiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5wcm9jZXNzSW5zdGFuY2VWYXJpYWJsZXNBcGkuZ2V0UHJvY2Vzc0luc3RhbmNlVmFyaWFibGVzKHByb2Nlc3NJbnN0YW5jZUlkKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy50b0pzb24pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGFsbCB0aGUgdGFza3MuXHJcbiAgICAgKiBAcmV0dXJucyBMaXN0IG9mIHRhc2tzXHJcbiAgICAgKi9cclxuICAgIGdldFRhc2tzKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy50YXNrQXBpLmxpc3RUYXNrcyh7fSkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKHRoaXMudG9Kc29uQXJyYXkpLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgdGFzay5cclxuICAgICAqIEBwYXJhbSB0YXNrSWQgVGFzayBJZFxyXG4gICAgICogQHJldHVybnMgVGFzayBpbmZvXHJcbiAgICAgKi9cclxuICAgIGdldFRhc2sodGFza0lkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMudGFza0FwaS5nZXRUYXNrKHRhc2tJZCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKHRoaXMudG9Kc29uKSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2F2ZXMgYSB0YXNrIGZvcm0uXHJcbiAgICAgKiBAcGFyYW0gdGFza0lkIFRhc2sgSWRcclxuICAgICAqIEBwYXJhbSBmb3JtVmFsdWVzIEZvcm0gVmFsdWVzXHJcbiAgICAgKiBAcmV0dXJucyBOdWxsIHJlc3BvbnNlIHdoZW4gdGhlIG9wZXJhdGlvbiBpcyBjb21wbGV0ZVxyXG4gICAgICovXHJcbiAgICBzYXZlVGFza0Zvcm0odGFza0lkOiBzdHJpbmcsIGZvcm1WYWx1ZXM6IEZvcm1WYWx1ZXMpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IHNhdmVGb3JtUmVwcmVzZW50YXRpb24gPSA8U2F2ZUZvcm1SZXByZXNlbnRhdGlvbj4geyB2YWx1ZXM6IGZvcm1WYWx1ZXMgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy50YXNrQXBpLnNhdmVUYXNrRm9ybSh0YXNrSWQsIHNhdmVGb3JtUmVwcmVzZW50YXRpb24pKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ29tcGxldGVzIGEgVGFzayBGb3JtLlxyXG4gICAgICogQHBhcmFtIHRhc2tJZCBUYXNrIElkXHJcbiAgICAgKiBAcGFyYW0gZm9ybVZhbHVlcyBGb3JtIFZhbHVlc1xyXG4gICAgICogQHBhcmFtIG91dGNvbWUgRm9ybSBPdXRjb21lXHJcbiAgICAgKiBAcmV0dXJucyBOdWxsIHJlc3BvbnNlIHdoZW4gdGhlIG9wZXJhdGlvbiBpcyBjb21wbGV0ZVxyXG4gICAgICovXHJcbiAgICBjb21wbGV0ZVRhc2tGb3JtKHRhc2tJZDogc3RyaW5nLCBmb3JtVmFsdWVzOiBGb3JtVmFsdWVzLCBvdXRjb21lPzogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBjb21wbGV0ZUZvcm1SZXByZXNlbnRhdGlvbjogYW55ID0gPENvbXBsZXRlRm9ybVJlcHJlc2VudGF0aW9uPiB7IHZhbHVlczogZm9ybVZhbHVlcyB9O1xyXG4gICAgICAgIGlmIChvdXRjb21lKSB7XHJcbiAgICAgICAgICAgIGNvbXBsZXRlRm9ybVJlcHJlc2VudGF0aW9uLm91dGNvbWUgPSBvdXRjb21lO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy50YXNrQXBpLmNvbXBsZXRlVGFza0Zvcm0odGFza0lkLCBjb21wbGV0ZUZvcm1SZXByZXNlbnRhdGlvbikpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgZm9ybSByZWxhdGVkIHRvIGEgdGFzay5cclxuICAgICAqIEBwYXJhbSB0YXNrSWQgSUQgb2YgdGhlIHRhcmdldCB0YXNrXHJcbiAgICAgKiBAcmV0dXJucyBGb3JtIGRlZmluaXRpb25cclxuICAgICAqL1xyXG4gICAgZ2V0VGFza0Zvcm0odGFza0lkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMudGFza0FwaS5nZXRUYXNrRm9ybSh0YXNrSWQpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCh0aGlzLnRvSnNvbiksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBmb3JtIGRlZmluaXRpb24uXHJcbiAgICAgKiBAcGFyYW0gZm9ybUlkIElEIG9mIHRoZSB0YXJnZXQgZm9ybVxyXG4gICAgICogQHJldHVybnMgRm9ybSBkZWZpbml0aW9uXHJcbiAgICAgKi9cclxuICAgIGdldEZvcm1EZWZpbml0aW9uQnlJZChmb3JtSWQ6IG51bWJlcik6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5lZGl0b3JBcGkuZ2V0Rm9ybShmb3JtSWQpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCh0aGlzLnRvSnNvbiksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIGZvcm0gZGVmaW5pdGlvbiB3aXRoIGEgZ2l2ZW4gbmFtZS5cclxuICAgICAqIEBwYXJhbSBuYW1lIFRoZSBmb3JtIG5hbWVcclxuICAgICAqIEByZXR1cm5zIEZvcm0gZGVmaW5pdGlvblxyXG4gICAgICovXHJcbiAgICBnZXRGb3JtRGVmaW5pdGlvbkJ5TmFtZShuYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IG9wdHMgPSB7XHJcbiAgICAgICAgICAgICdmaWx0ZXInOiAnbXlSZXVzYWJsZUZvcm1zJyxcclxuICAgICAgICAgICAgJ2ZpbHRlclRleHQnOiBuYW1lLFxyXG4gICAgICAgICAgICAnbW9kZWxUeXBlJzogMlxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMubW9kZWxzQXBpLmdldE1vZGVscyhvcHRzKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy5nZXRGb3JtSWQpLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBzdGFydCBmb3JtIGluc3RhbmNlIGZvciBhIGdpdmVuIHByb2Nlc3MuXHJcbiAgICAgKiBAcGFyYW0gcHJvY2Vzc0lkIFByb2Nlc3MgZGVmaW5pdGlvbiBJRFxyXG4gICAgICogQHJldHVybnMgRm9ybSBkZWZpbml0aW9uXHJcbiAgICAgKi9cclxuICAgIGdldFN0YXJ0Rm9ybUluc3RhbmNlKHByb2Nlc3NJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLnByb2Nlc3NBcGkuZ2V0UHJvY2Vzc0luc3RhbmNlU3RhcnRGb3JtKHByb2Nlc3NJZCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKHRoaXMudG9Kc29uKSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhIHByb2Nlc3MgaW5zdGFuY2UuXHJcbiAgICAgKiBAcGFyYW0gcHJvY2Vzc0lkIElEIG9mIHRoZSBwcm9jZXNzIHRvIGdldFxyXG4gICAgICogQHJldHVybnMgUHJvY2VzcyBpbnN0YW5jZVxyXG4gICAgICovXHJcbiAgICBnZXRQcm9jZXNzSW5zdGFuY2UocHJvY2Vzc0lkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMucHJvY2Vzc0FwaS5nZXRQcm9jZXNzSW5zdGFuY2UocHJvY2Vzc0lkKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAodGhpcy50b0pzb24pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBzdGFydCBmb3JtIGRlZmluaXRpb24gZm9yIGEgZ2l2ZW4gcHJvY2Vzcy5cclxuICAgICAqIEBwYXJhbSBwcm9jZXNzSWQgUHJvY2VzcyBkZWZpbml0aW9uIElEXHJcbiAgICAgKiBAcmV0dXJucyBGb3JtIGRlZmluaXRpb25cclxuICAgICAqL1xyXG4gICAgZ2V0U3RhcnRGb3JtRGVmaW5pdGlvbihwcm9jZXNzSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5wcm9jZXNzQXBpLmdldFByb2Nlc3NEZWZpbml0aW9uU3RhcnRGb3JtKHByb2Nlc3NJZCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKHRoaXMudG9Kc29uKSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB2YWx1ZXMgb2YgZmllbGRzIHBvcHVsYXRlZCBieSBhIFJFU1QgYmFja2VuZC5cclxuICAgICAqIEBwYXJhbSB0YXNrSWQgVGFzayBpZGVudGlmaWVyXHJcbiAgICAgKiBAcGFyYW0gZmllbGQgRmllbGQgaWRlbnRpZmllclxyXG4gICAgICogQHJldHVybnMgRmllbGQgdmFsdWVzXHJcbiAgICAgKi9cclxuICAgIGdldFJlc3RGaWVsZFZhbHVlcyh0YXNrSWQ6IHN0cmluZywgZmllbGQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy50YXNrQXBpLmdldFJlc3RGaWVsZFZhbHVlcyh0YXNrSWQsIGZpZWxkKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdmFsdWVzIG9mIGZpZWxkcyBwb3B1bGF0ZWQgYnkgYSBSRVNUIGJhY2tlbmQgdXNpbmcgYSBwcm9jZXNzIElELlxyXG4gICAgICogQHBhcmFtIHByb2Nlc3NEZWZpbml0aW9uSWQgUHJvY2VzcyBpZGVudGlmaWVyXHJcbiAgICAgKiBAcGFyYW0gZmllbGQgRmllbGQgaWRlbnRpZmllclxyXG4gICAgICogQHJldHVybnMgRmllbGQgdmFsdWVzXHJcbiAgICAgKi9cclxuICAgIGdldFJlc3RGaWVsZFZhbHVlc0J5UHJvY2Vzc0lkKHByb2Nlc3NEZWZpbml0aW9uSWQ6IHN0cmluZywgZmllbGQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5wcm9jZXNzQXBpLmdldFJlc3RGaWVsZFZhbHVlcyhwcm9jZXNzRGVmaW5pdGlvbklkLCBmaWVsZCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGNvbHVtbiB2YWx1ZXMgb2YgZmllbGRzIHBvcHVsYXRlZCBieSBhIFJFU1QgYmFja2VuZCB1c2luZyBhIHByb2Nlc3MgSUQuXHJcbiAgICAgKiBAcGFyYW0gcHJvY2Vzc0RlZmluaXRpb25JZCBQcm9jZXNzIGlkZW50aWZpZXJcclxuICAgICAqIEBwYXJhbSBmaWVsZCBGaWVsZCBpZGVudGlmaWVyXHJcbiAgICAgKiBAcGFyYW0gY29sdW1uIENvbHVtbiBpZGVudGlmaWVyXHJcbiAgICAgKiBAcmV0dXJucyBGaWVsZCB2YWx1ZXNcclxuICAgICAqL1xyXG4gICAgZ2V0UmVzdEZpZWxkVmFsdWVzQ29sdW1uQnlQcm9jZXNzSWQocHJvY2Vzc0RlZmluaXRpb25JZDogc3RyaW5nLCBmaWVsZDogc3RyaW5nLCBjb2x1bW4/OiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMucHJvY2Vzc0FwaS5nZXRSZXN0VGFibGVGaWVsZFZhbHVlcyhwcm9jZXNzRGVmaW5pdGlvbklkLCBmaWVsZCwgY29sdW1uKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgY29sdW1uIHZhbHVlcyBvZiBmaWVsZHMgcG9wdWxhdGVkIGJ5IGEgUkVTVCBiYWNrZW5kLlxyXG4gICAgICogQHBhcmFtIHRhc2tJZCBUYXNrIGlkZW50aWZpZXJcclxuICAgICAqIEBwYXJhbSBmaWVsZCBGaWVsZCBpZGVudGlmaWVyXHJcbiAgICAgKiBAcGFyYW0gY29sdW1uIENvbHVtbiBpZGVudGlmaWVyXHJcbiAgICAgKiBAcmV0dXJucyBGaWVsZCB2YWx1ZXNcclxuICAgICAqL1xyXG4gICAgZ2V0UmVzdEZpZWxkVmFsdWVzQ29sdW1uKHRhc2tJZDogc3RyaW5nLCBmaWVsZDogc3RyaW5nLCBjb2x1bW4/OiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMudGFza0FwaS5nZXRSZXN0RmllbGRWYWx1ZXNDb2x1bW4odGFza0lkLCBmaWVsZCwgY29sdW1uKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybnMgYSBVUkwgZm9yIHRoZSBwcm9maWxlIHBpY3R1cmUgb2YgYSB1c2VyLlxyXG4gICAgICogQHBhcmFtIHVzZXJJZCBJRCBvZiB0aGUgdGFyZ2V0IHVzZXJcclxuICAgICAqIEByZXR1cm5zIFVSTCBzdHJpbmdcclxuICAgICAqL1xyXG4gICAgZ2V0VXNlclByb2ZpbGVJbWFnZUFwaSh1c2VySWQ6IG51bWJlcik6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmFjdGl2aXRpLnVzZXJBcGkuZ2V0VXNlclByb2ZpbGVQaWN0dXJlVXJsKHVzZXJJZCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgbGlzdCBvZiB3b3JrZmxvdyB1c2Vycy5cclxuICAgICAqIEBwYXJhbSBmaWx0ZXIgRmlsdGVyIHRvIHNlbGVjdCBzcGVjaWZpYyB1c2Vyc1xyXG4gICAgICogQHBhcmFtIGdyb3VwSWQgR3JvdXAgSUQgZm9yIHRoZSBzZWFyY2hcclxuICAgICAqIEByZXR1cm5zIEFycmF5IG9mIHVzZXJzXHJcbiAgICAgKi9cclxuICAgIGdldFdvcmtmbG93VXNlcnMoZmlsdGVyOiBzdHJpbmcsIGdyb3VwSWQ/OiBzdHJpbmcpOiBPYnNlcnZhYmxlPFVzZXJQcm9jZXNzTW9kZWxbXT4ge1xyXG4gICAgICAgIGNvbnN0IG9wdGlvbjogYW55ID0geyBmaWx0ZXI6IGZpbHRlciB9O1xyXG4gICAgICAgIGlmIChncm91cElkKSB7XHJcbiAgICAgICAgICAgIG9wdGlvbi5ncm91cElkID0gZ3JvdXBJZDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy51c2Vyc1dvcmtmbG93QXBpLmdldFVzZXJzKG9wdGlvbikpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgc3dpdGNoTWFwKChyZXNwb25zZTogYW55KSA9PiA8VXNlclByb2Nlc3NNb2RlbFtdPiByZXNwb25zZS5kYXRhIHx8IFtdKSxcclxuICAgICAgICAgICAgICAgIG1hcCgodXNlcjogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdXNlci51c2VySW1hZ2UgPSB0aGlzLmdldFVzZXJQcm9maWxlSW1hZ2VBcGkodXNlci5pZCk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG9mKHVzZXIpO1xyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBjb21iaW5lQWxsKCksXHJcbiAgICAgICAgICAgICAgICBkZWZhdWx0SWZFbXB0eShbXSksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBsaXN0IG9mIGdyb3VwcyBpbiBhIHdvcmtmbG93LlxyXG4gICAgICogQHBhcmFtIGZpbHRlciBGaWx0ZXIgdG8gc2VsZWN0IHNwZWNpZmljIGdyb3Vwc1xyXG4gICAgICogQHBhcmFtIGdyb3VwSWQgR3JvdXAgSUQgZm9yIHRoZSBzZWFyY2hcclxuICAgICAqIEByZXR1cm5zIEFycmF5IG9mIGdyb3Vwc1xyXG4gICAgICovXHJcbiAgICBnZXRXb3JrZmxvd0dyb3VwcyhmaWx0ZXI6IHN0cmluZywgZ3JvdXBJZD86IHN0cmluZyk6IE9ic2VydmFibGU8R3JvdXBNb2RlbFtdPiB7XHJcbiAgICAgICAgY29uc3Qgb3B0aW9uOiBhbnkgPSB7IGZpbHRlcjogZmlsdGVyIH07XHJcbiAgICAgICAgaWYgKGdyb3VwSWQpIHtcclxuICAgICAgICAgICAgb3B0aW9uLmdyb3VwSWQgPSBncm91cElkO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmdyb3Vwc0FwaS5nZXRHcm91cHMob3B0aW9uKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAoKHJlc3BvbnNlOiBhbnkpID0+IDxHcm91cE1vZGVsW10+IHJlc3BvbnNlLmRhdGEgfHwgW10pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBJRCBvZiBhIGZvcm0uXHJcbiAgICAgKiBAcGFyYW0gZm9ybSBPYmplY3QgcmVwcmVzZW50aW5nIGEgZm9ybVxyXG4gICAgICogQHJldHVybnMgSUQgc3RyaW5nXHJcbiAgICAgKi9cclxuICAgIGdldEZvcm1JZChmb3JtOiBhbnkpOiBzdHJpbmcge1xyXG4gICAgICAgIGxldCByZXN1bHQgPSBudWxsO1xyXG5cclxuICAgICAgICBpZiAoZm9ybSAmJiBmb3JtLmRhdGEgJiYgZm9ybS5kYXRhLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgcmVzdWx0ID0gZm9ybS5kYXRhWzBdLmlkO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZXMgYSBKU09OIHJlcHJlc2VudGF0aW9uIG9mIGZvcm0gZGF0YS5cclxuICAgICAqIEBwYXJhbSByZXMgT2JqZWN0IHJlcHJlc2VudGluZyBmb3JtIGRhdGFcclxuICAgICAqIEByZXR1cm5zIEpTT04gZGF0YVxyXG4gICAgICovXHJcbiAgICB0b0pzb24ocmVzOiBhbnkpIHtcclxuICAgICAgICBpZiAocmVzKSB7XHJcbiAgICAgICAgICAgIHJldHVybiByZXMgfHwge307XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB7fTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZXMgYSBKU09OIGFycmF5IHJlcHJlc2VudGF0aW9uIG9mIGZvcm0gZGF0YS5cclxuICAgICAqIEBwYXJhbSByZXMgT2JqZWN0IHJlcHJlc2VudGluZyBmb3JtIGRhdGFcclxuICAgICAqIEByZXR1cm5zIEpTT04gZGF0YVxyXG4gICAgICovXHJcbiAgICB0b0pzb25BcnJheShyZXM6IGFueSkge1xyXG4gICAgICAgIGlmIChyZXMpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHJlcy5kYXRhIHx8IFtdO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gW107XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXBvcnRzIGFuIGVycm9yIG1lc3NhZ2UuXHJcbiAgICAgKiBAcGFyYW0gZXJyb3IgRGF0YSBvYmplY3Qgd2l0aCBvcHRpb25hbCBgbWVzc2FnZWAgYW5kIGBzdGF0dXNgIGZpZWxkcyBmb3IgdGhlIGVycm9yXHJcbiAgICAgKiBAcmV0dXJucyBFcnJvciBtZXNzYWdlXHJcbiAgICAgKi9cclxuICAgIGhhbmRsZUVycm9yKGVycm9yOiBhbnkpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBlcnJNc2cgPSBGb3JtU2VydmljZS5VTktOT1dOX0VSUk9SX01FU1NBR0U7XHJcbiAgICAgICAgaWYgKGVycm9yKSB7XHJcbiAgICAgICAgICAgIGVyck1zZyA9IChlcnJvci5tZXNzYWdlKSA/IGVycm9yLm1lc3NhZ2UgOlxyXG4gICAgICAgICAgICAgICAgZXJyb3Iuc3RhdHVzID8gYCR7ZXJyb3Iuc3RhdHVzfSAtICR7ZXJyb3Iuc3RhdHVzVGV4dH1gIDogRm9ybVNlcnZpY2UuR0VORVJJQ19FUlJPUl9NRVNTQUdFO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZXJyb3IoZXJyTXNnKTtcclxuICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihlcnJNc2cpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==