/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { Injectable } from '@angular/core';
import { from } from 'rxjs';
import { NodeMetadata } from '../models/node-metadata.model';
import { map } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "../../services/alfresco-api.service";
var NodeService = /** @class */ (function () {
    function NodeService(apiService) {
        this.apiService = apiService;
    }
    /**
     * Get the metadata and the nodeType for a nodeId cleaned by the prefix.
     * @param nodeId ID of the target node
     * @returns Node metadata
     */
    /**
     * Get the metadata and the nodeType for a nodeId cleaned by the prefix.
     * @param {?} nodeId ID of the target node
     * @return {?} Node metadata
     */
    NodeService.prototype.getNodeMetadata = /**
     * Get the metadata and the nodeType for a nodeId cleaned by the prefix.
     * @param {?} nodeId ID of the target node
     * @return {?} Node metadata
     */
    function (nodeId) {
        return from(this.apiService.getInstance().nodes.getNode(nodeId))
            .pipe(map(this.cleanMetadataFromSemicolon));
    };
    /**
     * Create a new Node from form metadata.
     * @param path Path to the node
     * @param nodeType Node type
     * @param name Node name
     * @param nameSpace Namespace for properties
     * @param data Property data to store in the node under namespace
     * @returns The created node
     */
    /**
     * Create a new Node from form metadata.
     * @param {?} nodeType Node type
     * @param {?} nameSpace Namespace for properties
     * @param {?} data Property data to store in the node under namespace
     * @param {?} path Path to the node
     * @param {?=} name Node name
     * @return {?} The created node
     */
    NodeService.prototype.createNodeMetadata = /**
     * Create a new Node from form metadata.
     * @param {?} nodeType Node type
     * @param {?} nameSpace Namespace for properties
     * @param {?} data Property data to store in the node under namespace
     * @param {?} path Path to the node
     * @param {?=} name Node name
     * @return {?} The created node
     */
    function (nodeType, nameSpace, data, path, name) {
        /** @type {?} */
        var properties = {};
        for (var key in data) {
            if (data[key]) {
                properties[nameSpace + ':' + key] = data[key];
            }
        }
        return this.createNode(name || this.generateUuid(), nodeType, properties, path);
    };
    /**
     * Create a new Node from form metadata
     * @param name Node name
     * @param nodeType Node type
     * @param properties Node body properties
     * @param path Path to the node
     * @returns The created node
     */
    /**
     * Create a new Node from form metadata
     * @param {?} name Node name
     * @param {?} nodeType Node type
     * @param {?} properties Node body properties
     * @param {?} path Path to the node
     * @return {?} The created node
     */
    NodeService.prototype.createNode = /**
     * Create a new Node from form metadata
     * @param {?} name Node name
     * @param {?} nodeType Node type
     * @param {?} properties Node body properties
     * @param {?} path Path to the node
     * @return {?} The created node
     */
    function (name, nodeType, properties, path) {
        /** @type {?} */
        var body = {
            name: name,
            nodeType: nodeType,
            properties: properties,
            relativePath: path
        };
        /** @type {?} */
        var apiService = this.apiService.getInstance();
        return from(apiService.nodes.addNode('-root-', body, {}));
    };
    /**
     * @private
     * @return {?}
     */
    NodeService.prototype.generateUuid = /**
     * @private
     * @return {?}
     */
    function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (/**
         * @param {?} c
         * @return {?}
         */
        function (c) {
            /** @type {?} */
            var r = Math.random() * 16 | 0;
            /** @type {?} */
            var v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        }));
    };
    /**
     * @private
     * @param {?} nodeEntry
     * @return {?}
     */
    NodeService.prototype.cleanMetadataFromSemicolon = /**
     * @private
     * @param {?} nodeEntry
     * @return {?}
     */
    function (nodeEntry) {
        /** @type {?} */
        var metadata = {};
        if (nodeEntry && nodeEntry.entry.properties) {
            for (var key in nodeEntry.entry.properties) {
                if (key) {
                    if (key.indexOf(':') !== -1) {
                        metadata[key.split(':')[1]] = nodeEntry.entry.properties[key];
                    }
                    else {
                        metadata[key] = nodeEntry.entry.properties[key];
                    }
                }
            }
        }
        return new NodeMetadata(metadata, nodeEntry.entry.nodeType);
    };
    NodeService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    NodeService.ctorParameters = function () { return [
        { type: AlfrescoApiService }
    ]; };
    /** @nocollapse */ NodeService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function NodeService_Factory() { return new NodeService(i0.ɵɵinject(i1.AlfrescoApiService)); }, token: NodeService, providedIn: "root" });
    return NodeService;
}());
export { NodeService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NodeService.prototype.apiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9kZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiZm9ybS9zZXJ2aWNlcy9ub2RlLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDekUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQWMsSUFBSSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3hDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSwrQkFBK0IsQ0FBQztBQUM3RCxPQUFPLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7OztBQUdyQztJQUtJLHFCQUFvQixVQUE4QjtRQUE5QixlQUFVLEdBQVYsVUFBVSxDQUFvQjtJQUNsRCxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0kscUNBQWU7Ozs7O0lBQXRCLFVBQXVCLE1BQWM7UUFDakMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQzNELElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLENBQUMsQ0FBQztJQUNwRCxDQUFDO0lBRUQ7Ozs7Ozs7O09BUUc7Ozs7Ozs7Ozs7SUFDSSx3Q0FBa0I7Ozs7Ozs7OztJQUF6QixVQUEwQixRQUFnQixFQUFFLFNBQWMsRUFBRSxJQUFTLEVBQUUsSUFBWSxFQUFFLElBQWE7O1lBQ3hGLFVBQVUsR0FBRyxFQUFFO1FBQ3JCLEtBQUssSUFBTSxHQUFHLElBQUksSUFBSSxFQUFFO1lBQ3BCLElBQUksSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNYLFVBQVUsQ0FBQyxTQUFTLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQzthQUNqRDtTQUNKO1FBRUQsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsWUFBWSxFQUFFLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNwRixDQUFDO0lBRUQ7Ozs7Ozs7T0FPRzs7Ozs7Ozs7O0lBQ0ksZ0NBQVU7Ozs7Ozs7O0lBQWpCLFVBQWtCLElBQVksRUFBRSxRQUFnQixFQUFFLFVBQWUsRUFBRSxJQUFZOztZQUNyRSxJQUFJLEdBQUc7WUFDVCxJQUFJLEVBQUUsSUFBSTtZQUNWLFFBQVEsRUFBRSxRQUFRO1lBQ2xCLFVBQVUsRUFBRSxVQUFVO1lBQ3RCLFlBQVksRUFBRSxJQUFJO1NBQ3JCOztZQUVLLFVBQVUsR0FBNkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUU7UUFDMUUsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFLElBQUksRUFBRSxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQzlELENBQUM7Ozs7O0lBRU8sa0NBQVk7Ozs7SUFBcEI7UUFDSSxPQUFPLHNDQUFzQyxDQUFDLE9BQU8sQ0FBQyxPQUFPOzs7O1FBQUUsVUFBVSxDQUFDOztnQkFDaEUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQzs7Z0JBQUUsQ0FBQyxHQUFHLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQztZQUNyRSxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDMUIsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7Ozs7SUFFTyxnREFBMEI7Ozs7O0lBQWxDLFVBQW1DLFNBQW9COztZQUM3QyxRQUFRLEdBQUcsRUFBRTtRQUVuQixJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsS0FBSyxDQUFDLFVBQVUsRUFBRTtZQUN6QyxLQUFLLElBQU0sR0FBRyxJQUFJLFNBQVMsQ0FBQyxLQUFLLENBQUMsVUFBVSxFQUFFO2dCQUMxQyxJQUFJLEdBQUcsRUFBRTtvQkFDTCxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxDQUFDLEVBQUU7d0JBQ3pCLFFBQVEsQ0FBRSxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsU0FBUyxDQUFDLEtBQUssQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQ2xFO3lCQUFNO3dCQUNILFFBQVEsQ0FBRSxHQUFHLENBQUMsR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsQ0FBQztxQkFDcEQ7aUJBQ0o7YUFDSjtTQUNKO1FBRUQsT0FBTyxJQUFJLFlBQVksQ0FBQyxRQUFRLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUNoRSxDQUFDOztnQkFqRkosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFUUSxrQkFBa0I7OztzQkFqQjNCO0NBMEdDLEFBbEZELElBa0ZDO1NBL0VZLFdBQVc7Ozs7OztJQUVSLGlDQUFzQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgZnJvbSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBOb2RlTWV0YWRhdGEgfSBmcm9tICcuLi9tb2RlbHMvbm9kZS1tZXRhZGF0YS5tb2RlbCc7XHJcbmltcG9ydCB7IG1hcCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlDb21wYXRpYmlsaXR5LCBOb2RlRW50cnkgfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTm9kZVNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXQgdGhlIG1ldGFkYXRhIGFuZCB0aGUgbm9kZVR5cGUgZm9yIGEgbm9kZUlkIGNsZWFuZWQgYnkgdGhlIHByZWZpeC5cclxuICAgICAqIEBwYXJhbSBub2RlSWQgSUQgb2YgdGhlIHRhcmdldCBub2RlXHJcbiAgICAgKiBAcmV0dXJucyBOb2RlIG1ldGFkYXRhXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXROb2RlTWV0YWRhdGEobm9kZUlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPE5vZGVNZXRhZGF0YT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLm5vZGVzLmdldE5vZGUobm9kZUlkKSlcclxuICAgICAgICAgICAgLnBpcGUobWFwKHRoaXMuY2xlYW5NZXRhZGF0YUZyb21TZW1pY29sb24pKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZSBhIG5ldyBOb2RlIGZyb20gZm9ybSBtZXRhZGF0YS5cclxuICAgICAqIEBwYXJhbSBwYXRoIFBhdGggdG8gdGhlIG5vZGVcclxuICAgICAqIEBwYXJhbSBub2RlVHlwZSBOb2RlIHR5cGVcclxuICAgICAqIEBwYXJhbSBuYW1lIE5vZGUgbmFtZVxyXG4gICAgICogQHBhcmFtIG5hbWVTcGFjZSBOYW1lc3BhY2UgZm9yIHByb3BlcnRpZXNcclxuICAgICAqIEBwYXJhbSBkYXRhIFByb3BlcnR5IGRhdGEgdG8gc3RvcmUgaW4gdGhlIG5vZGUgdW5kZXIgbmFtZXNwYWNlXHJcbiAgICAgKiBAcmV0dXJucyBUaGUgY3JlYXRlZCBub2RlXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBjcmVhdGVOb2RlTWV0YWRhdGEobm9kZVR5cGU6IHN0cmluZywgbmFtZVNwYWNlOiBhbnksIGRhdGE6IGFueSwgcGF0aDogc3RyaW5nLCBuYW1lPzogc3RyaW5nKTogT2JzZXJ2YWJsZTxOb2RlRW50cnk+IHtcclxuICAgICAgICBjb25zdCBwcm9wZXJ0aWVzID0ge307XHJcbiAgICAgICAgZm9yIChjb25zdCBrZXkgaW4gZGF0YSkge1xyXG4gICAgICAgICAgICBpZiAoZGF0YVtrZXldKSB7XHJcbiAgICAgICAgICAgICAgICBwcm9wZXJ0aWVzW25hbWVTcGFjZSArICc6JyArIGtleV0gPSBkYXRhW2tleV07XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLmNyZWF0ZU5vZGUobmFtZSB8fCB0aGlzLmdlbmVyYXRlVXVpZCgpLCBub2RlVHlwZSwgcHJvcGVydGllcywgcGF0aCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDcmVhdGUgYSBuZXcgTm9kZSBmcm9tIGZvcm0gbWV0YWRhdGFcclxuICAgICAqIEBwYXJhbSBuYW1lIE5vZGUgbmFtZVxyXG4gICAgICogQHBhcmFtIG5vZGVUeXBlIE5vZGUgdHlwZVxyXG4gICAgICogQHBhcmFtIHByb3BlcnRpZXMgTm9kZSBib2R5IHByb3BlcnRpZXNcclxuICAgICAqIEBwYXJhbSBwYXRoIFBhdGggdG8gdGhlIG5vZGVcclxuICAgICAqIEByZXR1cm5zIFRoZSBjcmVhdGVkIG5vZGVcclxuICAgICAqL1xyXG4gICAgcHVibGljIGNyZWF0ZU5vZGUobmFtZTogc3RyaW5nLCBub2RlVHlwZTogc3RyaW5nLCBwcm9wZXJ0aWVzOiBhbnksIHBhdGg6IHN0cmluZyk6IE9ic2VydmFibGU8Tm9kZUVudHJ5PiB7XHJcbiAgICAgICAgY29uc3QgYm9keSA9IHtcclxuICAgICAgICAgICAgbmFtZTogbmFtZSxcclxuICAgICAgICAgICAgbm9kZVR5cGU6IG5vZGVUeXBlLFxyXG4gICAgICAgICAgICBwcm9wZXJ0aWVzOiBwcm9wZXJ0aWVzLFxyXG4gICAgICAgICAgICByZWxhdGl2ZVBhdGg6IHBhdGhcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBjb25zdCBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaUNvbXBhdGliaWxpdHkgPSB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKTtcclxuICAgICAgICByZXR1cm4gZnJvbShhcGlTZXJ2aWNlLm5vZGVzLmFkZE5vZGUoJy1yb290LScsIGJvZHksIHt9KSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZW5lcmF0ZVV1aWQoKSB7XHJcbiAgICAgICAgcmV0dXJuICd4eHh4eHh4eC14eHh4LTR4eHgteXh4eC14eHh4eHh4eHh4eHgnLnJlcGxhY2UoL1t4eV0vZywgZnVuY3Rpb24gKGMpIHtcclxuICAgICAgICAgICAgY29uc3QgciA9IE1hdGgucmFuZG9tKCkgKiAxNiB8IDAsIHYgPSBjID09PSAneCcgPyByIDogKHIgJiAweDMgfCAweDgpO1xyXG4gICAgICAgICAgICByZXR1cm4gdi50b1N0cmluZygxNik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBjbGVhbk1ldGFkYXRhRnJvbVNlbWljb2xvbihub2RlRW50cnk6IE5vZGVFbnRyeSk6IE5vZGVNZXRhZGF0YSB7XHJcbiAgICAgICAgY29uc3QgbWV0YWRhdGEgPSB7fTtcclxuXHJcbiAgICAgICAgaWYgKG5vZGVFbnRyeSAmJiBub2RlRW50cnkuZW50cnkucHJvcGVydGllcykge1xyXG4gICAgICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiBub2RlRW50cnkuZW50cnkucHJvcGVydGllcykge1xyXG4gICAgICAgICAgICAgICAgaWYgKGtleSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChrZXkuaW5kZXhPZignOicpICE9PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YSBba2V5LnNwbGl0KCc6JylbMV1dID0gbm9kZUVudHJ5LmVudHJ5LnByb3BlcnRpZXNba2V5XTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBtZXRhZGF0YSBba2V5XSA9IG5vZGVFbnRyeS5lbnRyeS5wcm9wZXJ0aWVzW2tleV07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gbmV3IE5vZGVNZXRhZGF0YShtZXRhZGF0YSwgbm9kZUVudHJ5LmVudHJ5Lm5vZGVUeXBlKTtcclxuICAgIH1cclxufVxyXG4iXX0=