/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import { Injectable } from '@angular/core';
import moment from 'moment-es6';
import { from, throwError } from 'rxjs';
import { WidgetTypeEnum } from '../models/widget-visibility.model';
import { map, catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "../../services/alfresco-api.service";
import * as i2 from "../../services/log.service";
var WidgetVisibilityService = /** @class */ (function () {
    function WidgetVisibilityService(apiService, logService) {
        this.apiService = apiService;
        this.logService = logService;
    }
    /**
     * @param {?} form
     * @return {?}
     */
    WidgetVisibilityService.prototype.refreshVisibility = /**
     * @param {?} form
     * @return {?}
     */
    function (form) {
        var _this = this;
        if (form && form.tabs && form.tabs.length > 0) {
            form.tabs.map((/**
             * @param {?} tabModel
             * @return {?}
             */
            function (tabModel) { return _this.refreshEntityVisibility(tabModel); }));
        }
        if (form) {
            form.getFormFields().map((/**
             * @param {?} field
             * @return {?}
             */
            function (field) { return _this.refreshEntityVisibility(field); }));
        }
    };
    /**
     * @param {?} element
     * @return {?}
     */
    WidgetVisibilityService.prototype.refreshEntityVisibility = /**
     * @param {?} element
     * @return {?}
     */
    function (element) {
        /** @type {?} */
        var visible = this.evaluateVisibility(element.form, element.visibilityCondition);
        element.isVisible = visible;
    };
    /**
     * @param {?} form
     * @param {?} visibilityObj
     * @return {?}
     */
    WidgetVisibilityService.prototype.evaluateVisibility = /**
     * @param {?} form
     * @param {?} visibilityObj
     * @return {?}
     */
    function (form, visibilityObj) {
        /** @type {?} */
        var isLeftFieldPresent = visibilityObj && (visibilityObj.leftType || visibilityObj.leftValue);
        if (!isLeftFieldPresent || isLeftFieldPresent === 'null') {
            return true;
        }
        else {
            return this.isFieldVisible(form, visibilityObj);
        }
    };
    /**
     * @param {?} form
     * @param {?} visibilityObj
     * @param {?=} accumulator
     * @param {?=} result
     * @return {?}
     */
    WidgetVisibilityService.prototype.isFieldVisible = /**
     * @param {?} form
     * @param {?} visibilityObj
     * @param {?=} accumulator
     * @param {?=} result
     * @return {?}
     */
    function (form, visibilityObj, accumulator, result) {
        if (accumulator === void 0) { accumulator = []; }
        if (result === void 0) { result = false; }
        /** @type {?} */
        var leftValue = this.getLeftValue(form, visibilityObj);
        /** @type {?} */
        var rightValue = this.getRightValue(form, visibilityObj);
        /** @type {?} */
        var actualResult = this.evaluateCondition(leftValue, rightValue, visibilityObj.operator);
        accumulator.push({ value: actualResult, operator: visibilityObj.nextConditionOperator });
        if (visibilityObj.nextCondition) {
            result = this.isFieldVisible(form, visibilityObj.nextCondition, accumulator);
        }
        else {
            result = accumulator[0].value ? accumulator[0].value : result;
            for (var i = 1; i < accumulator.length; i++) {
                if (accumulator[i - 1].operator && accumulator[i].value) {
                    result = this.evaluateLogicalOperation(accumulator[i - 1].operator, result, accumulator[i].value);
                }
            }
        }
        return !!result;
    };
    /**
     * @param {?} form
     * @param {?} visibilityObj
     * @return {?}
     */
    WidgetVisibilityService.prototype.getLeftValue = /**
     * @param {?} form
     * @param {?} visibilityObj
     * @return {?}
     */
    function (form, visibilityObj) {
        /** @type {?} */
        var leftValue = '';
        if (visibilityObj.leftType && visibilityObj.leftType === WidgetTypeEnum.variable) {
            leftValue = this.getVariableValue(form, visibilityObj.leftValue, this.processVarList);
        }
        else if (visibilityObj.leftType && visibilityObj.leftType === WidgetTypeEnum.field) {
            leftValue = this.getFormValue(form, visibilityObj.leftValue);
            leftValue = leftValue ? leftValue : this.getVariableValue(form, visibilityObj.leftValue, this.processVarList);
        }
        return leftValue;
    };
    /**
     * @param {?} form
     * @param {?} visibilityObj
     * @return {?}
     */
    WidgetVisibilityService.prototype.getRightValue = /**
     * @param {?} form
     * @param {?} visibilityObj
     * @return {?}
     */
    function (form, visibilityObj) {
        /** @type {?} */
        var valueFound = '';
        if (visibilityObj.rightType === WidgetTypeEnum.variable) {
            valueFound = this.getVariableValue(form, visibilityObj.rightValue, this.processVarList);
        }
        else if (visibilityObj.rightType === WidgetTypeEnum.field) {
            valueFound = this.getFormValue(form, visibilityObj.rightValue);
        }
        else {
            if (moment(visibilityObj.rightValue, 'YYYY-MM-DD', true).isValid()) {
                valueFound = visibilityObj.rightValue + 'T00:00:00.000Z';
            }
            else {
                valueFound = visibilityObj.rightValue;
            }
        }
        return valueFound;
    };
    /**
     * @param {?} form
     * @param {?} fieldId
     * @return {?}
     */
    WidgetVisibilityService.prototype.getFormValue = /**
     * @param {?} form
     * @param {?} fieldId
     * @return {?}
     */
    function (form, fieldId) {
        /** @type {?} */
        var value = this.getFieldValue(form.values, fieldId);
        if (!value) {
            value = this.searchValueInForm(form, fieldId);
        }
        return value;
    };
    /**
     * @param {?} valueList
     * @param {?} fieldId
     * @return {?}
     */
    WidgetVisibilityService.prototype.getFieldValue = /**
     * @param {?} valueList
     * @param {?} fieldId
     * @return {?}
     */
    function (valueList, fieldId) {
        /** @type {?} */
        var dropDownFilterByName;
        /** @type {?} */
        var valueFound;
        if (fieldId && fieldId.indexOf('_LABEL') > 0) {
            dropDownFilterByName = fieldId.substring(0, fieldId.length - 6);
            if (valueList[dropDownFilterByName]) {
                valueFound = valueList[dropDownFilterByName].name;
            }
        }
        else if (valueList[fieldId] && valueList[fieldId].id) {
            valueFound = valueList[fieldId].id;
        }
        else {
            valueFound = valueList[fieldId];
        }
        return valueFound;
    };
    /**
     * @param {?} form
     * @param {?} fieldId
     * @return {?}
     */
    WidgetVisibilityService.prototype.searchValueInForm = /**
     * @param {?} form
     * @param {?} fieldId
     * @return {?}
     */
    function (form, fieldId) {
        var _this = this;
        /** @type {?} */
        var fieldValue = '';
        form.getFormFields().forEach((/**
         * @param {?} formField
         * @return {?}
         */
        function (formField) {
            if (_this.isSearchedField(formField, fieldId)) {
                fieldValue = _this.getObjectValue(formField, fieldId);
                if (!fieldValue) {
                    if (formField.value && formField.value.id) {
                        fieldValue = formField.value.id;
                    }
                    else {
                        fieldValue = formField.value;
                    }
                }
            }
        }));
        return fieldValue;
    };
    /**
     * @private
     * @param {?} field
     * @param {?} fieldId
     * @return {?}
     */
    WidgetVisibilityService.prototype.getObjectValue = /**
     * @private
     * @param {?} field
     * @param {?} fieldId
     * @return {?}
     */
    function (field, fieldId) {
        /** @type {?} */
        var value = '';
        if (field.value && field.value.name) {
            value = field.value.name;
        }
        else if (field.options) {
            /** @type {?} */
            var option = field.options.find((/**
             * @param {?} opt
             * @return {?}
             */
            function (opt) { return opt.id === field.value; }));
            if (option) {
                value = this.getValueFromOption(fieldId, option);
            }
        }
        return value;
    };
    /**
     * @private
     * @param {?} fieldId
     * @param {?} option
     * @return {?}
     */
    WidgetVisibilityService.prototype.getValueFromOption = /**
     * @private
     * @param {?} fieldId
     * @param {?} option
     * @return {?}
     */
    function (fieldId, option) {
        /** @type {?} */
        var optionValue = '';
        if (fieldId && fieldId.indexOf('_LABEL') > 0) {
            optionValue = option.name;
        }
        else {
            optionValue = option.id;
        }
        return optionValue;
    };
    /**
     * @private
     * @param {?} field
     * @param {?} fieldToFind
     * @return {?}
     */
    WidgetVisibilityService.prototype.isSearchedField = /**
     * @private
     * @param {?} field
     * @param {?} fieldToFind
     * @return {?}
     */
    function (field, fieldToFind) {
        return (field.id && fieldToFind) ? field.id.toUpperCase() === fieldToFind.toUpperCase() : false;
    };
    /**
     * @param {?} form
     * @param {?} name
     * @param {?} processVarList
     * @return {?}
     */
    WidgetVisibilityService.prototype.getVariableValue = /**
     * @param {?} form
     * @param {?} name
     * @param {?} processVarList
     * @return {?}
     */
    function (form, name, processVarList) {
        return this.getFormVariableValue(form, name) ||
            this.getProcessVariableValue(name, processVarList);
    };
    /**
     * @private
     * @param {?} form
     * @param {?} identifier
     * @return {?}
     */
    WidgetVisibilityService.prototype.getFormVariableValue = /**
     * @private
     * @param {?} form
     * @param {?} identifier
     * @return {?}
     */
    function (form, identifier) {
        /** @type {?} */
        var variables = this.getFormVariables(form);
        if (variables) {
            /** @type {?} */
            var formVariable = variables.find((/**
             * @param {?} formVar
             * @return {?}
             */
            function (formVar) {
                return formVar.name === identifier || formVar.id === identifier;
            }));
            /** @type {?} */
            var value = void 0;
            if (formVariable) {
                value = formVariable.value;
                if (formVariable.type === 'date') {
                    value += 'T00:00:00.000Z';
                }
            }
            return value;
        }
    };
    /**
     * @private
     * @param {?} form
     * @return {?}
     */
    WidgetVisibilityService.prototype.getFormVariables = /**
     * @private
     * @param {?} form
     * @return {?}
     */
    function (form) {
        return form.json.variables;
    };
    /**
     * @private
     * @param {?} name
     * @param {?} processVarList
     * @return {?}
     */
    WidgetVisibilityService.prototype.getProcessVariableValue = /**
     * @private
     * @param {?} name
     * @param {?} processVarList
     * @return {?}
     */
    function (name, processVarList) {
        if (processVarList) {
            /** @type {?} */
            var processVariable = processVarList.find((/**
             * @param {?} variable
             * @return {?}
             */
            function (variable) { return variable.id === name; }));
            if (processVariable) {
                return processVariable.value;
            }
        }
    };
    /**
     * @param {?} logicOp
     * @param {?} previousValue
     * @param {?} newValue
     * @return {?}
     */
    WidgetVisibilityService.prototype.evaluateLogicalOperation = /**
     * @param {?} logicOp
     * @param {?} previousValue
     * @param {?} newValue
     * @return {?}
     */
    function (logicOp, previousValue, newValue) {
        switch (logicOp) {
            case 'and':
                return previousValue && newValue;
            case 'or':
                return previousValue || newValue;
            case 'and-not':
                return previousValue && !newValue;
            case 'or-not':
                return previousValue || !newValue;
            default:
                this.logService.error('NO valid operation! wrong op request : ' + logicOp);
                break;
        }
    };
    /**
     * @param {?} leftValue
     * @param {?} rightValue
     * @param {?} operator
     * @return {?}
     */
    WidgetVisibilityService.prototype.evaluateCondition = /**
     * @param {?} leftValue
     * @param {?} rightValue
     * @param {?} operator
     * @return {?}
     */
    function (leftValue, rightValue, operator) {
        switch (operator) {
            case '==':
                return leftValue + '' === rightValue + '';
            case '<':
                return leftValue < rightValue;
            case '!=':
                return leftValue + '' !== rightValue + '';
            case '>':
                return leftValue > rightValue;
            case '>=':
                return leftValue >= rightValue;
            case '<=':
                return leftValue <= rightValue;
            case 'empty':
                return leftValue ? leftValue === '' : true;
            case '!empty':
                return leftValue ? leftValue !== '' : false;
            default:
                this.logService.error('NO valid operation!');
                break;
        }
        return;
    };
    /**
     * @return {?}
     */
    WidgetVisibilityService.prototype.cleanProcessVariable = /**
     * @return {?}
     */
    function () {
        this.processVarList = [];
    };
    /**
     * @param {?} taskId
     * @return {?}
     */
    WidgetVisibilityService.prototype.getTaskProcessVariable = /**
     * @param {?} taskId
     * @return {?}
     */
    function (taskId) {
        var _this = this;
        return from(this.apiService.getInstance().activiti.taskFormsApi.getTaskFormVariables(taskId))
            .pipe(map((/**
         * @param {?} res
         * @return {?}
         */
        function (res) {
            /** @type {?} */
            var jsonRes = _this.toJson(res);
            _this.processVarList = (/** @type {?} */ (jsonRes));
            return jsonRes;
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * @param {?} res
     * @return {?}
     */
    WidgetVisibilityService.prototype.toJson = /**
     * @param {?} res
     * @return {?}
     */
    function (res) {
        return res || {};
    };
    /**
     * @private
     * @param {?} err
     * @return {?}
     */
    WidgetVisibilityService.prototype.handleError = /**
     * @private
     * @param {?} err
     * @return {?}
     */
    function (err) {
        this.logService.error('Error while performing a call');
        return throwError('Error while performing a call - Server error');
    };
    WidgetVisibilityService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    WidgetVisibilityService.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: LogService }
    ]; };
    /** @nocollapse */ WidgetVisibilityService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function WidgetVisibilityService_Factory() { return new WidgetVisibilityService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: WidgetVisibilityService, providedIn: "root" });
    return WidgetVisibilityService;
}());
export { WidgetVisibilityService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    WidgetVisibilityService.prototype.processVarList;
    /**
     * @type {?}
     * @private
     */
    WidgetVisibilityService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    WidgetVisibilityService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoid2lkZ2V0LXZpc2liaWxpdHkuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImZvcm0vc2VydmljZXMvd2lkZ2V0LXZpc2liaWxpdHkuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUN6RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDeEQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLE1BQU0sTUFBTSxZQUFZLENBQUM7QUFDaEMsT0FBTyxFQUFjLElBQUksRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFHcEQsT0FBTyxFQUF5QixjQUFjLEVBQUUsTUFBTSxtQ0FBbUMsQ0FBQztBQUMxRixPQUFPLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBRWpEO0lBT0ksaUNBQW9CLFVBQThCLEVBQzlCLFVBQXNCO1FBRHRCLGVBQVUsR0FBVixVQUFVLENBQW9CO1FBQzlCLGVBQVUsR0FBVixVQUFVLENBQVk7SUFDMUMsQ0FBQzs7Ozs7SUFFTSxtREFBaUI7Ozs7SUFBeEIsVUFBeUIsSUFBZTtRQUF4QyxpQkFRQztRQVBHLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzNDLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRzs7OztZQUFDLFVBQUMsUUFBUSxJQUFLLE9BQUEsS0FBSSxDQUFDLHVCQUF1QixDQUFDLFFBQVEsQ0FBQyxFQUF0QyxDQUFzQyxFQUFDLENBQUM7U0FDdkU7UUFFRCxJQUFJLElBQUksRUFBRTtZQUNOLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQyxHQUFHOzs7O1lBQUMsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsdUJBQXVCLENBQUMsS0FBSyxDQUFDLEVBQW5DLENBQW1DLEVBQUMsQ0FBQztTQUM1RTtJQUNMLENBQUM7Ozs7O0lBRUQseURBQXVCOzs7O0lBQXZCLFVBQXdCLE9BQWtDOztZQUNoRCxPQUFPLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsT0FBTyxDQUFDLG1CQUFtQixDQUFDO1FBQ2xGLE9BQU8sQ0FBQyxTQUFTLEdBQUcsT0FBTyxDQUFDO0lBQ2hDLENBQUM7Ozs7OztJQUVELG9EQUFrQjs7Ozs7SUFBbEIsVUFBbUIsSUFBZSxFQUFFLGFBQW9DOztZQUM5RCxrQkFBa0IsR0FBRyxhQUFhLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxJQUFJLGFBQWEsQ0FBQyxTQUFTLENBQUM7UUFDL0YsSUFBSSxDQUFDLGtCQUFrQixJQUFJLGtCQUFrQixLQUFLLE1BQU0sRUFBRTtZQUN0RCxPQUFPLElBQUksQ0FBQztTQUNmO2FBQU07WUFDSCxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxDQUFDO1NBQ25EO0lBQ0wsQ0FBQzs7Ozs7Ozs7SUFFRCxnREFBYzs7Ozs7OztJQUFkLFVBQWUsSUFBZSxFQUFFLGFBQW9DLEVBQUUsV0FBdUIsRUFBRSxNQUF1QjtRQUFoRCw0QkFBQSxFQUFBLGdCQUF1QjtRQUFFLHVCQUFBLEVBQUEsY0FBdUI7O1lBQzVHLFNBQVMsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxhQUFhLENBQUM7O1lBQ2xELFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxhQUFhLENBQUM7O1lBQ3BELFlBQVksR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsU0FBUyxFQUFFLFVBQVUsRUFBRSxhQUFhLENBQUMsUUFBUSxDQUFDO1FBQzFGLFdBQVcsQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUsWUFBWSxFQUFFLFFBQVEsRUFBRSxhQUFhLENBQUMscUJBQXFCLEVBQUUsQ0FBQyxDQUFDO1FBQ3pGLElBQUksYUFBYSxDQUFDLGFBQWEsRUFBRTtZQUM3QixNQUFNLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDLGFBQWEsRUFBRSxXQUFXLENBQUMsQ0FBQztTQUNoRjthQUFNO1lBQ0gsTUFBTSxHQUFHLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQztZQUU5RCxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsV0FBVyxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDekMsSUFBSSxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLFFBQVEsSUFBSSxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxFQUFFO29CQUNyRCxNQUFNLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUNsQyxXQUFXLENBQUMsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFDM0IsTUFBTSxFQUNOLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQ3ZCLENBQUM7aUJBQ0w7YUFDSjtTQUNKO1FBQ0QsT0FBTyxDQUFDLENBQUMsTUFBTSxDQUFDO0lBRXBCLENBQUM7Ozs7OztJQUVELDhDQUFZOzs7OztJQUFaLFVBQWEsSUFBZSxFQUFFLGFBQW9DOztZQUMxRCxTQUFTLEdBQUcsRUFBRTtRQUNsQixJQUFJLGFBQWEsQ0FBQyxRQUFRLElBQUksYUFBYSxDQUFDLFFBQVEsS0FBSyxjQUFjLENBQUMsUUFBUSxFQUFFO1lBQzlFLFNBQVMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxFQUFFLGFBQWEsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1NBQ3pGO2FBQU0sSUFBSSxhQUFhLENBQUMsUUFBUSxJQUFJLGFBQWEsQ0FBQyxRQUFRLEtBQUssY0FBYyxDQUFDLEtBQUssRUFBRTtZQUNsRixTQUFTLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzdELFNBQVMsR0FBRyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxhQUFhLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztTQUNqSDtRQUNELE9BQU8sU0FBUyxDQUFDO0lBQ3JCLENBQUM7Ozs7OztJQUVELCtDQUFhOzs7OztJQUFiLFVBQWMsSUFBZSxFQUFFLGFBQW9DOztZQUMzRCxVQUFVLEdBQUcsRUFBRTtRQUNuQixJQUFJLGFBQWEsQ0FBQyxTQUFTLEtBQUssY0FBYyxDQUFDLFFBQVEsRUFBRTtZQUNyRCxVQUFVLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxhQUFhLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxjQUFjLENBQUMsQ0FBQztTQUMzRjthQUFNLElBQUksYUFBYSxDQUFDLFNBQVMsS0FBSyxjQUFjLENBQUMsS0FBSyxFQUFFO1lBQ3pELFVBQVUsR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxhQUFhLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDbEU7YUFBTTtZQUNILElBQUksTUFBTSxDQUFDLGFBQWEsQ0FBQyxVQUFVLEVBQUUsWUFBWSxFQUFFLElBQUksQ0FBQyxDQUFDLE9BQU8sRUFBRSxFQUFFO2dCQUNoRSxVQUFVLEdBQUcsYUFBYSxDQUFDLFVBQVUsR0FBRyxnQkFBZ0IsQ0FBQzthQUM1RDtpQkFBTTtnQkFDSCxVQUFVLEdBQUcsYUFBYSxDQUFDLFVBQVUsQ0FBQzthQUN6QztTQUNKO1FBQ0QsT0FBTyxVQUFVLENBQUM7SUFDdEIsQ0FBQzs7Ozs7O0lBRUQsOENBQVk7Ozs7O0lBQVosVUFBYSxJQUFlLEVBQUUsT0FBZTs7WUFDckMsS0FBSyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxPQUFPLENBQUM7UUFFcEQsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUNSLEtBQUssR0FBRyxJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLE9BQU8sQ0FBQyxDQUFDO1NBQ2pEO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7Ozs7O0lBRUQsK0NBQWE7Ozs7O0lBQWIsVUFBYyxTQUFjLEVBQUUsT0FBZTs7WUFDckMsb0JBQW9COztZQUFFLFVBQVU7UUFDcEMsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDMUMsb0JBQW9CLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQyxDQUFDLEVBQUUsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUMsQ0FBQztZQUNoRSxJQUFJLFNBQVMsQ0FBQyxvQkFBb0IsQ0FBQyxFQUFFO2dCQUNqQyxVQUFVLEdBQUcsU0FBUyxDQUFDLG9CQUFvQixDQUFDLENBQUMsSUFBSSxDQUFDO2FBQ3JEO1NBQ0o7YUFBTSxJQUFJLFNBQVMsQ0FBQyxPQUFPLENBQUMsSUFBSSxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxFQUFFO1lBQ3BELFVBQVUsR0FBRyxTQUFTLENBQUMsT0FBTyxDQUFDLENBQUMsRUFBRSxDQUFDO1NBQ3RDO2FBQU07WUFDSCxVQUFVLEdBQUcsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1NBQ25DO1FBQ0QsT0FBTyxVQUFVLENBQUM7SUFDdEIsQ0FBQzs7Ozs7O0lBRUQsbURBQWlCOzs7OztJQUFqQixVQUFrQixJQUFlLEVBQUUsT0FBZTtRQUFsRCxpQkFnQkM7O1lBZk8sVUFBVSxHQUFHLEVBQUU7UUFDbkIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDLE9BQU87Ozs7UUFBQyxVQUFDLFNBQXlCO1lBQ25ELElBQUksS0FBSSxDQUFDLGVBQWUsQ0FBQyxTQUFTLEVBQUUsT0FBTyxDQUFDLEVBQUU7Z0JBQzFDLFVBQVUsR0FBRyxLQUFJLENBQUMsY0FBYyxDQUFDLFNBQVMsRUFBRSxPQUFPLENBQUMsQ0FBQztnQkFDckQsSUFBSSxDQUFDLFVBQVUsRUFBRTtvQkFDYixJQUFJLFNBQVMsQ0FBQyxLQUFLLElBQUksU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUU7d0JBQ3ZDLFVBQVUsR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQztxQkFDbkM7eUJBQU07d0JBQ0gsVUFBVSxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUM7cUJBQ2hDO2lCQUNKO2FBQ0o7UUFDTCxDQUFDLEVBQUMsQ0FBQztRQUVILE9BQU8sVUFBVSxDQUFDO0lBQ3RCLENBQUM7Ozs7Ozs7SUFFTyxnREFBYzs7Ozs7O0lBQXRCLFVBQXVCLEtBQXFCLEVBQUUsT0FBZTs7WUFDckQsS0FBSyxHQUFHLEVBQUU7UUFDZCxJQUFJLEtBQUssQ0FBQyxLQUFLLElBQUksS0FBSyxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUU7WUFDakMsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO1NBQzVCO2FBQU0sSUFBSSxLQUFLLENBQUMsT0FBTyxFQUFFOztnQkFDaEIsTUFBTSxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSTs7OztZQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsR0FBRyxDQUFDLEVBQUUsS0FBSyxLQUFLLENBQUMsS0FBSyxFQUF0QixDQUFzQixFQUFDO1lBQ2xFLElBQUksTUFBTSxFQUFFO2dCQUNSLEtBQUssR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxDQUFDO2FBQ3BEO1NBQ0o7UUFDRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDOzs7Ozs7O0lBRU8sb0RBQWtCOzs7Ozs7SUFBMUIsVUFBMkIsT0FBZSxFQUFFLE1BQU07O1lBQzFDLFdBQVcsR0FBRyxFQUFFO1FBQ3BCLElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQzFDLFdBQVcsR0FBRyxNQUFNLENBQUMsSUFBSSxDQUFDO1NBQzdCO2FBQU07WUFDSCxXQUFXLEdBQUcsTUFBTSxDQUFDLEVBQUUsQ0FBQztTQUMzQjtRQUNELE9BQU8sV0FBVyxDQUFDO0lBQ3ZCLENBQUM7Ozs7Ozs7SUFFTyxpREFBZTs7Ozs7O0lBQXZCLFVBQXdCLEtBQXFCLEVBQUUsV0FBbUI7UUFDOUQsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLElBQUksV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsV0FBVyxFQUFFLEtBQUssV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDcEcsQ0FBQzs7Ozs7OztJQUVELGtEQUFnQjs7Ozs7O0lBQWhCLFVBQWlCLElBQWUsRUFBRSxJQUFZLEVBQUUsY0FBMEM7UUFDdEYsT0FBTyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxFQUFFLElBQUksQ0FBQztZQUN4QyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxFQUFFLGNBQWMsQ0FBQyxDQUFDO0lBQzNELENBQUM7Ozs7Ozs7SUFFTyxzREFBb0I7Ozs7OztJQUE1QixVQUE2QixJQUFlLEVBQUUsVUFBa0I7O1lBQ3RELFNBQVMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDO1FBQzdDLElBQUksU0FBUyxFQUFFOztnQkFDTCxZQUFZLEdBQUcsU0FBUyxDQUFDLElBQUk7Ozs7WUFBQyxVQUFDLE9BQU87Z0JBQ3hDLE9BQU8sT0FBTyxDQUFDLElBQUksS0FBSyxVQUFVLElBQUksT0FBTyxDQUFDLEVBQUUsS0FBSyxVQUFVLENBQUM7WUFDcEUsQ0FBQyxFQUFDOztnQkFFRSxLQUFLLFNBQUE7WUFDVCxJQUFJLFlBQVksRUFBRTtnQkFDZCxLQUFLLEdBQUcsWUFBWSxDQUFDLEtBQUssQ0FBQztnQkFDM0IsSUFBSSxZQUFZLENBQUMsSUFBSSxLQUFLLE1BQU0sRUFBRTtvQkFDOUIsS0FBSyxJQUFJLGdCQUFnQixDQUFDO2lCQUM3QjthQUNKO1lBRUQsT0FBTyxLQUFLLENBQUM7U0FDaEI7SUFDTCxDQUFDOzs7Ozs7SUFFTyxrREFBZ0I7Ozs7O0lBQXhCLFVBQXlCLElBQWU7UUFDcEMsT0FBUSxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztJQUNoQyxDQUFDOzs7Ozs7O0lBRU8seURBQXVCOzs7Ozs7SUFBL0IsVUFBZ0MsSUFBWSxFQUFFLGNBQTBDO1FBQ3BGLElBQUksY0FBYyxFQUFFOztnQkFDVixlQUFlLEdBQUcsY0FBYyxDQUFDLElBQUk7Ozs7WUFBQyxVQUFDLFFBQVEsSUFBSyxPQUFBLFFBQVEsQ0FBQyxFQUFFLEtBQUssSUFBSSxFQUFwQixDQUFvQixFQUFDO1lBQy9FLElBQUksZUFBZSxFQUFFO2dCQUNqQixPQUFPLGVBQWUsQ0FBQyxLQUFLLENBQUM7YUFDaEM7U0FDSjtJQUNMLENBQUM7Ozs7Ozs7SUFFRCwwREFBd0I7Ozs7OztJQUF4QixVQUF5QixPQUFPLEVBQUUsYUFBYSxFQUFFLFFBQVE7UUFDckQsUUFBUSxPQUFPLEVBQUU7WUFDYixLQUFLLEtBQUs7Z0JBQ04sT0FBTyxhQUFhLElBQUksUUFBUSxDQUFDO1lBQ3JDLEtBQUssSUFBSTtnQkFDTCxPQUFPLGFBQWEsSUFBSSxRQUFRLENBQUM7WUFDckMsS0FBSyxTQUFTO2dCQUNWLE9BQU8sYUFBYSxJQUFJLENBQUMsUUFBUSxDQUFDO1lBQ3RDLEtBQUssUUFBUTtnQkFDVCxPQUFPLGFBQWEsSUFBSSxDQUFDLFFBQVEsQ0FBQztZQUN0QztnQkFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyx5Q0FBeUMsR0FBRyxPQUFPLENBQUMsQ0FBQztnQkFDM0UsTUFBTTtTQUNiO0lBQ0wsQ0FBQzs7Ozs7OztJQUVELG1EQUFpQjs7Ozs7O0lBQWpCLFVBQWtCLFNBQVMsRUFBRSxVQUFVLEVBQUUsUUFBUTtRQUM3QyxRQUFRLFFBQVEsRUFBRTtZQUNkLEtBQUssSUFBSTtnQkFDTCxPQUFPLFNBQVMsR0FBRyxFQUFFLEtBQUssVUFBVSxHQUFHLEVBQUUsQ0FBQztZQUM5QyxLQUFLLEdBQUc7Z0JBQ0osT0FBTyxTQUFTLEdBQUcsVUFBVSxDQUFDO1lBQ2xDLEtBQUssSUFBSTtnQkFDTCxPQUFPLFNBQVMsR0FBRyxFQUFFLEtBQUssVUFBVSxHQUFHLEVBQUUsQ0FBQztZQUM5QyxLQUFLLEdBQUc7Z0JBQ0osT0FBTyxTQUFTLEdBQUcsVUFBVSxDQUFDO1lBQ2xDLEtBQUssSUFBSTtnQkFDTCxPQUFPLFNBQVMsSUFBSSxVQUFVLENBQUM7WUFDbkMsS0FBSyxJQUFJO2dCQUNMLE9BQU8sU0FBUyxJQUFJLFVBQVUsQ0FBQztZQUNuQyxLQUFLLE9BQU87Z0JBQ1IsT0FBTyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztZQUMvQyxLQUFLLFFBQVE7Z0JBQ1QsT0FBTyxTQUFTLENBQUMsQ0FBQyxDQUFDLFNBQVMsS0FBSyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztZQUNoRDtnQkFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO2dCQUM3QyxNQUFNO1NBQ2I7UUFDRCxPQUFPO0lBQ1gsQ0FBQzs7OztJQUVELHNEQUFvQjs7O0lBQXBCO1FBQ0ksSUFBSSxDQUFDLGNBQWMsR0FBRyxFQUFFLENBQUM7SUFDN0IsQ0FBQzs7Ozs7SUFFRCx3REFBc0I7Ozs7SUFBdEIsVUFBdUIsTUFBYztRQUFyQyxpQkFVQztRQVRHLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUN4RixJQUFJLENBQ0QsR0FBRzs7OztRQUFDLFVBQUMsR0FBRzs7Z0JBQ0UsT0FBTyxHQUFHLEtBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDO1lBQ2hDLEtBQUksQ0FBQyxjQUFjLEdBQUcsbUJBQTZCLE9BQU8sRUFBQSxDQUFDO1lBQzNELE9BQU8sT0FBTyxDQUFDO1FBQ25CLENBQUMsRUFBQyxFQUNGLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7Ozs7O0lBRUQsd0NBQU07Ozs7SUFBTixVQUFPLEdBQVE7UUFDWCxPQUFPLEdBQUcsSUFBSSxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7O0lBRU8sNkNBQVc7Ozs7O0lBQW5CLFVBQW9CLEdBQUc7UUFDbkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsK0JBQStCLENBQUMsQ0FBQztRQUN2RCxPQUFPLFVBQVUsQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO0lBQ3RFLENBQUM7O2dCQWhRSixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7O2dCQVpRLGtCQUFrQjtnQkFDbEIsVUFBVTs7O2tDQWxCbkI7Q0E0UkMsQUFqUUQsSUFpUUM7U0E5UFksdUJBQXVCOzs7Ozs7SUFFaEMsaURBQW1EOzs7OztJQUV2Qyw2Q0FBc0M7Ozs7O0lBQ3RDLDZDQUE4QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9sb2cuc2VydmljZSc7XHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IG1vbWVudCBmcm9tICdtb21lbnQtZXM2JztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgZnJvbSwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBGb3JtRmllbGRNb2RlbCwgRm9ybU1vZGVsLCBUYWJNb2RlbCB9IGZyb20gJy4uL2NvbXBvbmVudHMvd2lkZ2V0cy9jb3JlL2luZGV4JztcclxuaW1wb3J0IHsgVGFza1Byb2Nlc3NWYXJpYWJsZU1vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL3Rhc2stcHJvY2Vzcy12YXJpYWJsZS5tb2RlbCc7XHJcbmltcG9ydCB7IFdpZGdldFZpc2liaWxpdHlNb2RlbCwgV2lkZ2V0VHlwZUVudW0gfSBmcm9tICcuLi9tb2RlbHMvd2lkZ2V0LXZpc2liaWxpdHkubW9kZWwnO1xyXG5pbXBvcnQgeyBtYXAsIGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFdpZGdldFZpc2liaWxpdHlTZXJ2aWNlIHtcclxuXHJcbiAgICBwcml2YXRlIHByb2Nlc3NWYXJMaXN0OiBUYXNrUHJvY2Vzc1ZhcmlhYmxlTW9kZWxbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgbG9nU2VydmljZTogTG9nU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIHB1YmxpYyByZWZyZXNoVmlzaWJpbGl0eShmb3JtOiBGb3JtTW9kZWwpIHtcclxuICAgICAgICBpZiAoZm9ybSAmJiBmb3JtLnRhYnMgJiYgZm9ybS50YWJzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgZm9ybS50YWJzLm1hcCgodGFiTW9kZWwpID0+IHRoaXMucmVmcmVzaEVudGl0eVZpc2liaWxpdHkodGFiTW9kZWwpKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChmb3JtKSB7XHJcbiAgICAgICAgICAgIGZvcm0uZ2V0Rm9ybUZpZWxkcygpLm1hcCgoZmllbGQpID0+IHRoaXMucmVmcmVzaEVudGl0eVZpc2liaWxpdHkoZmllbGQpKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcmVmcmVzaEVudGl0eVZpc2liaWxpdHkoZWxlbWVudDogRm9ybUZpZWxkTW9kZWwgfCBUYWJNb2RlbCkge1xyXG4gICAgICAgIGNvbnN0IHZpc2libGUgPSB0aGlzLmV2YWx1YXRlVmlzaWJpbGl0eShlbGVtZW50LmZvcm0sIGVsZW1lbnQudmlzaWJpbGl0eUNvbmRpdGlvbik7XHJcbiAgICAgICAgZWxlbWVudC5pc1Zpc2libGUgPSB2aXNpYmxlO1xyXG4gICAgfVxyXG5cclxuICAgIGV2YWx1YXRlVmlzaWJpbGl0eShmb3JtOiBGb3JtTW9kZWwsIHZpc2liaWxpdHlPYmo6IFdpZGdldFZpc2liaWxpdHlNb2RlbCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGNvbnN0IGlzTGVmdEZpZWxkUHJlc2VudCA9IHZpc2liaWxpdHlPYmogJiYgKHZpc2liaWxpdHlPYmoubGVmdFR5cGUgfHwgdmlzaWJpbGl0eU9iai5sZWZ0VmFsdWUpO1xyXG4gICAgICAgIGlmICghaXNMZWZ0RmllbGRQcmVzZW50IHx8IGlzTGVmdEZpZWxkUHJlc2VudCA9PT0gJ251bGwnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmlzRmllbGRWaXNpYmxlKGZvcm0sIHZpc2liaWxpdHlPYmopO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBpc0ZpZWxkVmlzaWJsZShmb3JtOiBGb3JtTW9kZWwsIHZpc2liaWxpdHlPYmo6IFdpZGdldFZpc2liaWxpdHlNb2RlbCwgYWNjdW11bGF0b3I6IGFueVtdID0gW10sIHJlc3VsdDogYm9vbGVhbiA9IGZhbHNlKTogYm9vbGVhbiB7XHJcbiAgICAgICAgY29uc3QgbGVmdFZhbHVlID0gdGhpcy5nZXRMZWZ0VmFsdWUoZm9ybSwgdmlzaWJpbGl0eU9iaik7XHJcbiAgICAgICAgY29uc3QgcmlnaHRWYWx1ZSA9IHRoaXMuZ2V0UmlnaHRWYWx1ZShmb3JtLCB2aXNpYmlsaXR5T2JqKTtcclxuICAgICAgICBjb25zdCBhY3R1YWxSZXN1bHQgPSB0aGlzLmV2YWx1YXRlQ29uZGl0aW9uKGxlZnRWYWx1ZSwgcmlnaHRWYWx1ZSwgdmlzaWJpbGl0eU9iai5vcGVyYXRvcik7XHJcbiAgICAgICAgYWNjdW11bGF0b3IucHVzaCh7IHZhbHVlOiBhY3R1YWxSZXN1bHQsIG9wZXJhdG9yOiB2aXNpYmlsaXR5T2JqLm5leHRDb25kaXRpb25PcGVyYXRvciB9KTtcclxuICAgICAgICBpZiAodmlzaWJpbGl0eU9iai5uZXh0Q29uZGl0aW9uKSB7XHJcbiAgICAgICAgICAgIHJlc3VsdCA9IHRoaXMuaXNGaWVsZFZpc2libGUoZm9ybSwgdmlzaWJpbGl0eU9iai5uZXh0Q29uZGl0aW9uLCBhY2N1bXVsYXRvcik7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmVzdWx0ID0gYWNjdW11bGF0b3JbMF0udmFsdWUgPyBhY2N1bXVsYXRvclswXS52YWx1ZSA6IHJlc3VsdDtcclxuXHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAxOyBpIDwgYWNjdW11bGF0b3IubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGlmIChhY2N1bXVsYXRvcltpIC0gMV0ub3BlcmF0b3IgJiYgYWNjdW11bGF0b3JbaV0udmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSB0aGlzLmV2YWx1YXRlTG9naWNhbE9wZXJhdGlvbihcclxuICAgICAgICAgICAgICAgICAgICAgICAgYWNjdW11bGF0b3JbaSAtIDFdLm9wZXJhdG9yLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN1bHQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGFjY3VtdWxhdG9yW2ldLnZhbHVlXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gISFyZXN1bHQ7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIGdldExlZnRWYWx1ZShmb3JtOiBGb3JtTW9kZWwsIHZpc2liaWxpdHlPYmo6IFdpZGdldFZpc2liaWxpdHlNb2RlbCk6IHN0cmluZyB7XHJcbiAgICAgICAgbGV0IGxlZnRWYWx1ZSA9ICcnO1xyXG4gICAgICAgIGlmICh2aXNpYmlsaXR5T2JqLmxlZnRUeXBlICYmIHZpc2liaWxpdHlPYmoubGVmdFR5cGUgPT09IFdpZGdldFR5cGVFbnVtLnZhcmlhYmxlKSB7XHJcbiAgICAgICAgICAgIGxlZnRWYWx1ZSA9IHRoaXMuZ2V0VmFyaWFibGVWYWx1ZShmb3JtLCB2aXNpYmlsaXR5T2JqLmxlZnRWYWx1ZSwgdGhpcy5wcm9jZXNzVmFyTGlzdCk7XHJcbiAgICAgICAgfSBlbHNlIGlmICh2aXNpYmlsaXR5T2JqLmxlZnRUeXBlICYmIHZpc2liaWxpdHlPYmoubGVmdFR5cGUgPT09IFdpZGdldFR5cGVFbnVtLmZpZWxkKSB7XHJcbiAgICAgICAgICAgIGxlZnRWYWx1ZSA9IHRoaXMuZ2V0Rm9ybVZhbHVlKGZvcm0sIHZpc2liaWxpdHlPYmoubGVmdFZhbHVlKTtcclxuICAgICAgICAgICAgbGVmdFZhbHVlID0gbGVmdFZhbHVlID8gbGVmdFZhbHVlIDogdGhpcy5nZXRWYXJpYWJsZVZhbHVlKGZvcm0sIHZpc2liaWxpdHlPYmoubGVmdFZhbHVlLCB0aGlzLnByb2Nlc3NWYXJMaXN0KTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGxlZnRWYWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRSaWdodFZhbHVlKGZvcm06IEZvcm1Nb2RlbCwgdmlzaWJpbGl0eU9iajogV2lkZ2V0VmlzaWJpbGl0eU1vZGVsKTogc3RyaW5nIHtcclxuICAgICAgICBsZXQgdmFsdWVGb3VuZCA9ICcnO1xyXG4gICAgICAgIGlmICh2aXNpYmlsaXR5T2JqLnJpZ2h0VHlwZSA9PT0gV2lkZ2V0VHlwZUVudW0udmFyaWFibGUpIHtcclxuICAgICAgICAgICAgdmFsdWVGb3VuZCA9IHRoaXMuZ2V0VmFyaWFibGVWYWx1ZShmb3JtLCB2aXNpYmlsaXR5T2JqLnJpZ2h0VmFsdWUsIHRoaXMucHJvY2Vzc1Zhckxpc3QpO1xyXG4gICAgICAgIH0gZWxzZSBpZiAodmlzaWJpbGl0eU9iai5yaWdodFR5cGUgPT09IFdpZGdldFR5cGVFbnVtLmZpZWxkKSB7XHJcbiAgICAgICAgICAgIHZhbHVlRm91bmQgPSB0aGlzLmdldEZvcm1WYWx1ZShmb3JtLCB2aXNpYmlsaXR5T2JqLnJpZ2h0VmFsdWUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGlmIChtb21lbnQodmlzaWJpbGl0eU9iai5yaWdodFZhbHVlLCAnWVlZWS1NTS1ERCcsIHRydWUpLmlzVmFsaWQoKSkge1xyXG4gICAgICAgICAgICAgICAgdmFsdWVGb3VuZCA9IHZpc2liaWxpdHlPYmoucmlnaHRWYWx1ZSArICdUMDA6MDA6MDAuMDAwWic7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB2YWx1ZUZvdW5kID0gdmlzaWJpbGl0eU9iai5yaWdodFZhbHVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB2YWx1ZUZvdW5kO1xyXG4gICAgfVxyXG5cclxuICAgIGdldEZvcm1WYWx1ZShmb3JtOiBGb3JtTW9kZWwsIGZpZWxkSWQ6IHN0cmluZyk6IGFueSB7XHJcbiAgICAgICAgbGV0IHZhbHVlID0gdGhpcy5nZXRGaWVsZFZhbHVlKGZvcm0udmFsdWVzLCBmaWVsZElkKTtcclxuXHJcbiAgICAgICAgaWYgKCF2YWx1ZSkge1xyXG4gICAgICAgICAgICB2YWx1ZSA9IHRoaXMuc2VhcmNoVmFsdWVJbkZvcm0oZm9ybSwgZmllbGRJZCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB2YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRGaWVsZFZhbHVlKHZhbHVlTGlzdDogYW55LCBmaWVsZElkOiBzdHJpbmcpOiBhbnkge1xyXG4gICAgICAgIGxldCBkcm9wRG93bkZpbHRlckJ5TmFtZSwgdmFsdWVGb3VuZDtcclxuICAgICAgICBpZiAoZmllbGRJZCAmJiBmaWVsZElkLmluZGV4T2YoJ19MQUJFTCcpID4gMCkge1xyXG4gICAgICAgICAgICBkcm9wRG93bkZpbHRlckJ5TmFtZSA9IGZpZWxkSWQuc3Vic3RyaW5nKDAsIGZpZWxkSWQubGVuZ3RoIC0gNik7XHJcbiAgICAgICAgICAgIGlmICh2YWx1ZUxpc3RbZHJvcERvd25GaWx0ZXJCeU5hbWVdKSB7XHJcbiAgICAgICAgICAgICAgICB2YWx1ZUZvdW5kID0gdmFsdWVMaXN0W2Ryb3BEb3duRmlsdGVyQnlOYW1lXS5uYW1lO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSBlbHNlIGlmICh2YWx1ZUxpc3RbZmllbGRJZF0gJiYgdmFsdWVMaXN0W2ZpZWxkSWRdLmlkKSB7XHJcbiAgICAgICAgICAgIHZhbHVlRm91bmQgPSB2YWx1ZUxpc3RbZmllbGRJZF0uaWQ7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdmFsdWVGb3VuZCA9IHZhbHVlTGlzdFtmaWVsZElkXTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHZhbHVlRm91bmQ7XHJcbiAgICB9XHJcblxyXG4gICAgc2VhcmNoVmFsdWVJbkZvcm0oZm9ybTogRm9ybU1vZGVsLCBmaWVsZElkOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIGxldCBmaWVsZFZhbHVlID0gJyc7XHJcbiAgICAgICAgZm9ybS5nZXRGb3JtRmllbGRzKCkuZm9yRWFjaCgoZm9ybUZpZWxkOiBGb3JtRmllbGRNb2RlbCkgPT4ge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5pc1NlYXJjaGVkRmllbGQoZm9ybUZpZWxkLCBmaWVsZElkKSkge1xyXG4gICAgICAgICAgICAgICAgZmllbGRWYWx1ZSA9IHRoaXMuZ2V0T2JqZWN0VmFsdWUoZm9ybUZpZWxkLCBmaWVsZElkKTtcclxuICAgICAgICAgICAgICAgIGlmICghZmllbGRWYWx1ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChmb3JtRmllbGQudmFsdWUgJiYgZm9ybUZpZWxkLnZhbHVlLmlkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkVmFsdWUgPSBmb3JtRmllbGQudmFsdWUuaWQ7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRWYWx1ZSA9IGZvcm1GaWVsZC52YWx1ZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZpZWxkVmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRPYmplY3RWYWx1ZShmaWVsZDogRm9ybUZpZWxkTW9kZWwsIGZpZWxkSWQ6IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgbGV0IHZhbHVlID0gJyc7XHJcbiAgICAgICAgaWYgKGZpZWxkLnZhbHVlICYmIGZpZWxkLnZhbHVlLm5hbWUpIHtcclxuICAgICAgICAgICAgdmFsdWUgPSBmaWVsZC52YWx1ZS5uYW1lO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoZmllbGQub3B0aW9ucykge1xyXG4gICAgICAgICAgICBjb25zdCBvcHRpb24gPSBmaWVsZC5vcHRpb25zLmZpbmQoKG9wdCkgPT4gb3B0LmlkID09PSBmaWVsZC52YWx1ZSk7XHJcbiAgICAgICAgICAgIGlmIChvcHRpb24pIHtcclxuICAgICAgICAgICAgICAgIHZhbHVlID0gdGhpcy5nZXRWYWx1ZUZyb21PcHRpb24oZmllbGRJZCwgb3B0aW9uKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRWYWx1ZUZyb21PcHRpb24oZmllbGRJZDogc3RyaW5nLCBvcHRpb24pOiBzdHJpbmcge1xyXG4gICAgICAgIGxldCBvcHRpb25WYWx1ZSA9ICcnO1xyXG4gICAgICAgIGlmIChmaWVsZElkICYmIGZpZWxkSWQuaW5kZXhPZignX0xBQkVMJykgPiAwKSB7XHJcbiAgICAgICAgICAgIG9wdGlvblZhbHVlID0gb3B0aW9uLm5hbWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgb3B0aW9uVmFsdWUgPSBvcHRpb24uaWQ7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBvcHRpb25WYWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGlzU2VhcmNoZWRGaWVsZChmaWVsZDogRm9ybUZpZWxkTW9kZWwsIGZpZWxkVG9GaW5kOiBzdHJpbmcpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gKGZpZWxkLmlkICYmIGZpZWxkVG9GaW5kKSA/IGZpZWxkLmlkLnRvVXBwZXJDYXNlKCkgPT09IGZpZWxkVG9GaW5kLnRvVXBwZXJDYXNlKCkgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRWYXJpYWJsZVZhbHVlKGZvcm06IEZvcm1Nb2RlbCwgbmFtZTogc3RyaW5nLCBwcm9jZXNzVmFyTGlzdDogVGFza1Byb2Nlc3NWYXJpYWJsZU1vZGVsW10pOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldEZvcm1WYXJpYWJsZVZhbHVlKGZvcm0sIG5hbWUpIHx8XHJcbiAgICAgICAgICAgIHRoaXMuZ2V0UHJvY2Vzc1ZhcmlhYmxlVmFsdWUobmFtZSwgcHJvY2Vzc1Zhckxpc3QpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0Rm9ybVZhcmlhYmxlVmFsdWUoZm9ybTogRm9ybU1vZGVsLCBpZGVudGlmaWVyOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIGNvbnN0IHZhcmlhYmxlcyA9IHRoaXMuZ2V0Rm9ybVZhcmlhYmxlcyhmb3JtKTtcclxuICAgICAgICBpZiAodmFyaWFibGVzKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGZvcm1WYXJpYWJsZSA9IHZhcmlhYmxlcy5maW5kKChmb3JtVmFyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZm9ybVZhci5uYW1lID09PSBpZGVudGlmaWVyIHx8IGZvcm1WYXIuaWQgPT09IGlkZW50aWZpZXI7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgbGV0IHZhbHVlO1xyXG4gICAgICAgICAgICBpZiAoZm9ybVZhcmlhYmxlKSB7XHJcbiAgICAgICAgICAgICAgICB2YWx1ZSA9IGZvcm1WYXJpYWJsZS52YWx1ZTtcclxuICAgICAgICAgICAgICAgIGlmIChmb3JtVmFyaWFibGUudHlwZSA9PT0gJ2RhdGUnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWUgKz0gJ1QwMDowMDowMC4wMDBaJztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcmV0dXJuIHZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldEZvcm1WYXJpYWJsZXMoZm9ybTogRm9ybU1vZGVsKTogYW55W10ge1xyXG4gICAgICAgIHJldHVybiAgZm9ybS5qc29uLnZhcmlhYmxlcztcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldFByb2Nlc3NWYXJpYWJsZVZhbHVlKG5hbWU6IHN0cmluZywgcHJvY2Vzc1Zhckxpc3Q6IFRhc2tQcm9jZXNzVmFyaWFibGVNb2RlbFtdKTogc3RyaW5nIHtcclxuICAgICAgICBpZiAocHJvY2Vzc1Zhckxpc3QpIHtcclxuICAgICAgICAgICAgY29uc3QgcHJvY2Vzc1ZhcmlhYmxlID0gcHJvY2Vzc1Zhckxpc3QuZmluZCgodmFyaWFibGUpID0+IHZhcmlhYmxlLmlkID09PSBuYW1lKTtcclxuICAgICAgICAgICAgaWYgKHByb2Nlc3NWYXJpYWJsZSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHByb2Nlc3NWYXJpYWJsZS52YWx1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBldmFsdWF0ZUxvZ2ljYWxPcGVyYXRpb24obG9naWNPcCwgcHJldmlvdXNWYWx1ZSwgbmV3VmFsdWUpOiBib29sZWFuIHtcclxuICAgICAgICBzd2l0Y2ggKGxvZ2ljT3ApIHtcclxuICAgICAgICAgICAgY2FzZSAnYW5kJzpcclxuICAgICAgICAgICAgICAgIHJldHVybiBwcmV2aW91c1ZhbHVlICYmIG5ld1ZhbHVlO1xyXG4gICAgICAgICAgICBjYXNlICdvcicgOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHByZXZpb3VzVmFsdWUgfHwgbmV3VmFsdWU7XHJcbiAgICAgICAgICAgIGNhc2UgJ2FuZC1ub3QnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHByZXZpb3VzVmFsdWUgJiYgIW5ld1ZhbHVlO1xyXG4gICAgICAgICAgICBjYXNlICdvci1ub3QnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHByZXZpb3VzVmFsdWUgfHwgIW5ld1ZhbHVlO1xyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKCdOTyB2YWxpZCBvcGVyYXRpb24hIHdyb25nIG9wIHJlcXVlc3QgOiAnICsgbG9naWNPcCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZXZhbHVhdGVDb25kaXRpb24obGVmdFZhbHVlLCByaWdodFZhbHVlLCBvcGVyYXRvcik6IGJvb2xlYW4ge1xyXG4gICAgICAgIHN3aXRjaCAob3BlcmF0b3IpIHtcclxuICAgICAgICAgICAgY2FzZSAnPT0nOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGxlZnRWYWx1ZSArICcnID09PSByaWdodFZhbHVlICsgJyc7XHJcbiAgICAgICAgICAgIGNhc2UgJzwnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGxlZnRWYWx1ZSA8IHJpZ2h0VmFsdWU7XHJcbiAgICAgICAgICAgIGNhc2UgJyE9JzpcclxuICAgICAgICAgICAgICAgIHJldHVybiBsZWZ0VmFsdWUgKyAnJyAhPT0gcmlnaHRWYWx1ZSArICcnO1xyXG4gICAgICAgICAgICBjYXNlICc+JzpcclxuICAgICAgICAgICAgICAgIHJldHVybiBsZWZ0VmFsdWUgPiByaWdodFZhbHVlO1xyXG4gICAgICAgICAgICBjYXNlICc+PSc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbGVmdFZhbHVlID49IHJpZ2h0VmFsdWU7XHJcbiAgICAgICAgICAgIGNhc2UgJzw9JzpcclxuICAgICAgICAgICAgICAgIHJldHVybiBsZWZ0VmFsdWUgPD0gcmlnaHRWYWx1ZTtcclxuICAgICAgICAgICAgY2FzZSAnZW1wdHknOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGxlZnRWYWx1ZSA/IGxlZnRWYWx1ZSA9PT0gJycgOiB0cnVlO1xyXG4gICAgICAgICAgICBjYXNlICchZW1wdHknOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGxlZnRWYWx1ZSA/IGxlZnRWYWx1ZSAhPT0gJycgOiBmYWxzZTtcclxuICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcignTk8gdmFsaWQgb3BlcmF0aW9uIScpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybjtcclxuICAgIH1cclxuXHJcbiAgICBjbGVhblByb2Nlc3NWYXJpYWJsZSgpIHtcclxuICAgICAgICB0aGlzLnByb2Nlc3NWYXJMaXN0ID0gW107XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VGFza1Byb2Nlc3NWYXJpYWJsZSh0YXNrSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8VGFza1Byb2Nlc3NWYXJpYWJsZU1vZGVsW10+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5hY3Rpdml0aS50YXNrRm9ybXNBcGkuZ2V0VGFza0Zvcm1WYXJpYWJsZXModGFza0lkKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAoKHJlcykgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGpzb25SZXMgPSB0aGlzLnRvSnNvbihyZXMpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucHJvY2Vzc1Zhckxpc3QgPSA8VGFza1Byb2Nlc3NWYXJpYWJsZU1vZGVsW10+IGpzb25SZXM7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGpzb25SZXM7XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIHRvSnNvbihyZXM6IGFueSk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIHJlcyB8fCB7fTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGhhbmRsZUVycm9yKGVycikge1xyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcignRXJyb3Igd2hpbGUgcGVyZm9ybWluZyBhIGNhbGwnKTtcclxuICAgICAgICByZXR1cm4gdGhyb3dFcnJvcignRXJyb3Igd2hpbGUgcGVyZm9ybWluZyBhIGNhbGwgLSBTZXJ2ZXIgZXJyb3InKTtcclxuICAgIH1cclxufVxyXG4iXX0=