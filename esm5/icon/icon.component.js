/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewEncapsulation, ChangeDetectionStrategy } from '@angular/core';
var IconComponent = /** @class */ (function () {
    function IconComponent() {
        this._value = '';
        this._isCustom = false;
    }
    Object.defineProperty(IconComponent.prototype, "value", {
        get: /**
         * @return {?}
         */
        function () {
            return this._value;
        },
        /** Icon value, which can be either a ligature name or a custom icon in the format `[namespace]:[name]`. */
        set: /**
         * Icon value, which can be either a ligature name or a custom icon in the format `[namespace]:[name]`.
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this._value = value || 'settings';
            this._isCustom = this._value.includes(':');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(IconComponent.prototype, "isCustom", {
        get: /**
         * @return {?}
         */
        function () {
            return this._isCustom;
        },
        enumerable: true,
        configurable: true
    });
    IconComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-icon',
                    template: "<ng-container *ngIf=\"isCustom; else: default\">\r\n  <mat-icon [color]=\"color\" [svgIcon]=\"value\"></mat-icon>\r\n</ng-container>\r\n\r\n<ng-template #default>\r\n  <mat-icon [color]=\"color\">{{ value }}</mat-icon>\r\n</ng-template>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    host: { class: 'adf-icon' },
                    styles: [".adf-icon{display:inline-flex;vertical-align:middle}"]
                }] }
    ];
    IconComponent.propDecorators = {
        color: [{ type: Input }],
        value: [{ type: Input }]
    };
    return IconComponent;
}());
export { IconComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    IconComponent.prototype._value;
    /**
     * @type {?}
     * @private
     */
    IconComponent.prototype._isCustom;
    /**
     * Theme color palette for the component.
     * @type {?}
     */
    IconComponent.prototype.color;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWNvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJpY29uL2ljb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFDSCxTQUFTLEVBQ1QsS0FBSyxFQUNMLGlCQUFpQixFQUNqQix1QkFBdUIsRUFDMUIsTUFBTSxlQUFlLENBQUM7QUFHdkI7SUFBQTtRQVNZLFdBQU0sR0FBRyxFQUFFLENBQUM7UUFDWixjQUFTLEdBQUcsS0FBSyxDQUFDO0lBb0I5QixDQUFDO0lBZEcsc0JBQUksZ0NBQUs7Ozs7UUFBVDtZQUNJLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQztRQUN2QixDQUFDO1FBRUQsMkdBQTJHOzs7Ozs7UUFDM0csVUFDVSxLQUFhO1lBQ25CLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxJQUFJLFVBQVUsQ0FBQztZQUNsQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQy9DLENBQUM7OztPQVBBO0lBU0Qsc0JBQUksbUNBQVE7Ozs7UUFBWjtZQUNJLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUMxQixDQUFDOzs7T0FBQTs7Z0JBN0JKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsVUFBVTtvQkFDcEIsNFBBQW9DO29CQUVwQyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtvQkFDckMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLElBQUksRUFBRSxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUU7O2lCQUM5Qjs7O3dCQU1JLEtBQUs7d0JBUUwsS0FBSzs7SUFTVixvQkFBQztDQUFBLEFBOUJELElBOEJDO1NBdEJZLGFBQWE7Ozs7OztJQUN0QiwrQkFBb0I7Ozs7O0lBQ3BCLGtDQUEwQjs7Ozs7SUFHMUIsOEJBQ29CIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7XHJcbiAgICBDb21wb25lbnQsXHJcbiAgICBJbnB1dCxcclxuICAgIFZpZXdFbmNhcHN1bGF0aW9uLFxyXG4gICAgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3lcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVGhlbWVQYWxldHRlIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1pY29uJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9pY29uLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2ljb24uY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXHJcbiAgICBjaGFuZ2VEZXRlY3Rpb246IENoYW5nZURldGVjdGlvblN0cmF0ZWd5Lk9uUHVzaCxcclxuICAgIGhvc3Q6IHsgY2xhc3M6ICdhZGYtaWNvbicgfVxyXG59KVxyXG5leHBvcnQgY2xhc3MgSWNvbkNvbXBvbmVudCB7XHJcbiAgICBwcml2YXRlIF92YWx1ZSA9ICcnO1xyXG4gICAgcHJpdmF0ZSBfaXNDdXN0b20gPSBmYWxzZTtcclxuXHJcbiAgICAvKiogVGhlbWUgY29sb3IgcGFsZXR0ZSBmb3IgdGhlIGNvbXBvbmVudC4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBjb2xvcjogVGhlbWVQYWxldHRlO1xyXG5cclxuICAgIGdldCB2YWx1ZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl92YWx1ZTtcclxuICAgIH1cclxuXHJcbiAgICAvKiogSWNvbiB2YWx1ZSwgd2hpY2ggY2FuIGJlIGVpdGhlciBhIGxpZ2F0dXJlIG5hbWUgb3IgYSBjdXN0b20gaWNvbiBpbiB0aGUgZm9ybWF0IGBbbmFtZXNwYWNlXTpbbmFtZV1gLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHNldCB2YWx1ZSh2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5fdmFsdWUgPSB2YWx1ZSB8fCAnc2V0dGluZ3MnO1xyXG4gICAgICAgIHRoaXMuX2lzQ3VzdG9tID0gdGhpcy5fdmFsdWUuaW5jbHVkZXMoJzonKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgaXNDdXN0b20oKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzQ3VzdG9tO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==