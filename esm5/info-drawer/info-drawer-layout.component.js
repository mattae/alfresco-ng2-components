/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Directive, ViewEncapsulation } from '@angular/core';
var InfoDrawerLayoutComponent = /** @class */ (function () {
    function InfoDrawerLayoutComponent() {
    }
    InfoDrawerLayoutComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-info-drawer-layout',
                    template: "<div class=\"adf-info-drawer-layout-header\">\r\n    <div class=\"adf-info-drawer-layout-header-title\">\r\n        <ng-content select=\"[info-drawer-title]\"></ng-content>\r\n    </div>\r\n    <div class=\"adf-info-drawer-layout-header-buttons\">\r\n        <ng-content select=\"[info-drawer-buttons]\"></ng-content>\r\n    </div>\r\n</div>\r\n<div class=\"adf-info-drawer-layout-content\">\r\n    <ng-content select=\"[info-drawer-content]\"></ng-content>\r\n</div>",
                    encapsulation: ViewEncapsulation.None,
                    host: { 'class': 'adf-info-drawer-layout' },
                    styles: [""]
                }] }
    ];
    return InfoDrawerLayoutComponent;
}());
export { InfoDrawerLayoutComponent };
/**
 * Directive selectors without adf- prefix will be deprecated on 3.0.0
 */
var InfoDrawerTitleDirective = /** @class */ (function () {
    function InfoDrawerTitleDirective() {
    }
    InfoDrawerTitleDirective.decorators = [
        { type: Directive, args: [{ selector: '[adf-info-drawer-title], [info-drawer-title]' },] }
    ];
    return InfoDrawerTitleDirective;
}());
export { InfoDrawerTitleDirective };
var InfoDrawerButtonsDirective = /** @class */ (function () {
    function InfoDrawerButtonsDirective() {
    }
    InfoDrawerButtonsDirective.decorators = [
        { type: Directive, args: [{ selector: '[adf-info-drawer-buttons], [info-drawer-buttons]' },] }
    ];
    return InfoDrawerButtonsDirective;
}());
export { InfoDrawerButtonsDirective };
var InfoDrawerContentDirective = /** @class */ (function () {
    function InfoDrawerContentDirective() {
    }
    InfoDrawerContentDirective.decorators = [
        { type: Directive, args: [{ selector: '[adf-info-drawer-content], [info-drawer-content]' },] }
    ];
    return InfoDrawerContentDirective;
}());
export { InfoDrawerContentDirective };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5mby1kcmF3ZXItbGF5b3V0LmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImluZm8tZHJhd2VyL2luZm8tZHJhd2VyLWxheW91dC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFeEU7SUFBQTtJQU93QyxDQUFDOztnQkFQeEMsU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSx3QkFBd0I7b0JBQ2xDLCtkQUFrRDtvQkFFbEQsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7b0JBQ3JDLElBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSx3QkFBd0IsRUFBRTs7aUJBQzlDOztJQUN1QyxnQ0FBQztDQUFBLEFBUHpDLElBT3lDO1NBQTVCLHlCQUF5Qjs7OztBQUt0QztJQUFBO0lBQWdILENBQUM7O2dCQUFoSCxTQUFTLFNBQUMsRUFBRSxRQUFRLEVBQUUsOENBQThDLEVBQUU7O0lBQXlDLCtCQUFDO0NBQUEsQUFBakgsSUFBaUg7U0FBM0Isd0JBQXdCO0FBQzlHO0lBQUE7SUFBc0gsQ0FBQzs7Z0JBQXRILFNBQVMsU0FBQyxFQUFFLFFBQVEsRUFBRSxrREFBa0QsRUFBRTs7SUFBMkMsaUNBQUM7Q0FBQSxBQUF2SCxJQUF1SDtTQUE3QiwwQkFBMEI7QUFDcEg7SUFBQTtJQUFzSCxDQUFDOztnQkFBdEgsU0FBUyxTQUFDLEVBQUUsUUFBUSxFQUFFLGtEQUFrRCxFQUFFOztJQUEyQyxpQ0FBQztDQUFBLEFBQXZILElBQXVIO1NBQTdCLDBCQUEwQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIERpcmVjdGl2ZSwgVmlld0VuY2Fwc3VsYXRpb24gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtaW5mby1kcmF3ZXItbGF5b3V0JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9pbmZvLWRyYXdlci1sYXlvdXQuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vaW5mby1kcmF3ZXItbGF5b3V0LmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gICAgaG9zdDogeyAnY2xhc3MnOiAnYWRmLWluZm8tZHJhd2VyLWxheW91dCcgfVxyXG59KVxyXG5leHBvcnQgY2xhc3MgSW5mb0RyYXdlckxheW91dENvbXBvbmVudCB7fVxyXG5cclxuLyoqXHJcbiAqIERpcmVjdGl2ZSBzZWxlY3RvcnMgd2l0aG91dCBhZGYtIHByZWZpeCB3aWxsIGJlIGRlcHJlY2F0ZWQgb24gMy4wLjBcclxuICovXHJcbkBEaXJlY3RpdmUoeyBzZWxlY3RvcjogJ1thZGYtaW5mby1kcmF3ZXItdGl0bGVdLCBbaW5mby1kcmF3ZXItdGl0bGVdJyB9KSBleHBvcnQgY2xhc3MgSW5mb0RyYXdlclRpdGxlRGlyZWN0aXZlIHt9XHJcbkBEaXJlY3RpdmUoeyBzZWxlY3RvcjogJ1thZGYtaW5mby1kcmF3ZXItYnV0dG9uc10sIFtpbmZvLWRyYXdlci1idXR0b25zXScgfSkgZXhwb3J0IGNsYXNzIEluZm9EcmF3ZXJCdXR0b25zRGlyZWN0aXZlIHt9XHJcbkBEaXJlY3RpdmUoeyBzZWxlY3RvcjogJ1thZGYtaW5mby1kcmF3ZXItY29udGVudF0sIFtpbmZvLWRyYXdlci1jb250ZW50XScgfSkgZXhwb3J0IGNsYXNzIEluZm9EcmF3ZXJDb250ZW50RGlyZWN0aXZlIHt9XHJcbiJdfQ==