/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ContentChildren, EventEmitter, Input, Output, QueryList, TemplateRef, ViewChild, ViewEncapsulation } from '@angular/core';
var InfoDrawerTabComponent = /** @class */ (function () {
    function InfoDrawerTabComponent() {
        /**
         * The title of the tab (string or translation key).
         */
        this.label = '';
        /**
         * Icon to render for the tab.
         */
        this.icon = null;
    }
    InfoDrawerTabComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-info-drawer-tab',
                    template: '<ng-template><ng-content></ng-content></ng-template>'
                }] }
    ];
    InfoDrawerTabComponent.propDecorators = {
        label: [{ type: Input }],
        icon: [{ type: Input }],
        content: [{ type: ViewChild, args: [TemplateRef, { static: true },] }]
    };
    return InfoDrawerTabComponent;
}());
export { InfoDrawerTabComponent };
if (false) {
    /**
     * The title of the tab (string or translation key).
     * @type {?}
     */
    InfoDrawerTabComponent.prototype.label;
    /**
     * Icon to render for the tab.
     * @type {?}
     */
    InfoDrawerTabComponent.prototype.icon;
    /** @type {?} */
    InfoDrawerTabComponent.prototype.content;
}
var InfoDrawerComponent = /** @class */ (function () {
    function InfoDrawerComponent() {
        /**
         * The title of the info drawer (string or translation key).
         */
        this.title = null;
        /**
         * The selected index tab.
         */
        this.selectedIndex = 0;
        /**
         * Emitted when the currently active tab changes.
         */
        this.currentTab = new EventEmitter();
    }
    /**
     * @return {?}
     */
    InfoDrawerComponent.prototype.showTabLayout = /**
     * @return {?}
     */
    function () {
        return this.contentBlocks.length > 0;
    };
    /**
     * @param {?} event
     * @return {?}
     */
    InfoDrawerComponent.prototype.onTabChange = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.currentTab.emit(event.index);
    };
    InfoDrawerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-info-drawer',
                    template: "<adf-info-drawer-layout>\r\n    <div *ngIf=\"title\" info-drawer-title>{{ title | translate }}</div>\r\n    <ng-content *ngIf=\"!title\" info-drawer-title select=\"[info-drawer-title]\"></ng-content>\r\n\r\n    <ng-content info-drawer-buttons select=\"[info-drawer-buttons]\"></ng-content>\r\n\r\n    <ng-container info-drawer-content *ngIf=\"showTabLayout(); then tabLayout else singleLayout\"></ng-container>\r\n\r\n    <ng-template #tabLayout>\r\n        <mat-tab-group [(selectedIndex)]=\"selectedIndex\" class=\"adf-info-drawer-tabs\" (selectedTabChange)=\"onTabChange($event)\">\r\n            <mat-tab *ngFor=\"let contentBlock of contentBlocks\"\r\n                [label]=\"contentBlock.label | translate\"\r\n                class=\"adf-info-drawer-tab\">\r\n\r\n                <ng-template mat-tab-label>\r\n                    <mat-icon *ngIf=\"contentBlock.icon\">{{ contentBlock.icon }}</mat-icon>\r\n                    <span *ngIf=\"contentBlock.label\">{{ contentBlock.label | translate }}</span>\r\n                </ng-template>\r\n\r\n                <ng-container *ngTemplateOutlet=\"contentBlock.content\"></ng-container>\r\n            </mat-tab>\r\n        </mat-tab-group>\r\n    </ng-template>\r\n\r\n    <ng-template #singleLayout>\r\n        <ng-content select=\"[info-drawer-content]\"></ng-content>\r\n    </ng-template>\r\n</adf-info-drawer-layout>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    host: { 'class': 'adf-info-drawer' },
                    styles: [".adf-info-drawer{display:block}.adf-info-drawer .mat-tab-label{min-width:0}.adf-info-drawer .adf-info-drawer-layout-content{padding:0}.adf-info-drawer .adf-info-drawer-layout-content>:not(.adf-info-drawer-tabs){padding:10px}.adf-info-drawer .adf-info-drawer-layout-content .adf-info-drawer-tabs .mat-tab-body-content>*,.adf-info-drawer .adf-info-drawer-layout-content>:not(.adf-info-drawer-tabs)>*{margin-bottom:20px;display:block}.adf-info-drawer .adf-info-drawer-layout-content .adf-info-drawer-tabs .mat-tab-body-content>:last-child{margin-bottom:0}.adf-info-drawer .adf-info-drawer-layout-content .adf-info-drawer-tabs .mat-tab-label{flex-grow:1}.adf-info-drawer .adf-info-drawer-layout-content .adf-info-drawer-tabs .mat-tab-label .mat-icon+span{padding-left:5px}.adf-info-drawer .adf-info-drawer-layout-content .adf-info-drawer-tabs .mat-ink-bar{height:4px}.adf-info-drawer .adf-info-drawer-layout-content .adf-info-drawer-tabs .mat-tab-body{padding:10px}.adf-info-drawer .adf-info-drawer-layout-content .adf-info-drawer-tabs .mat-tab-body-content{overflow:initial}"]
                }] }
    ];
    InfoDrawerComponent.propDecorators = {
        title: [{ type: Input }],
        selectedIndex: [{ type: Input }],
        currentTab: [{ type: Output }],
        contentBlocks: [{ type: ContentChildren, args: [InfoDrawerTabComponent,] }]
    };
    return InfoDrawerComponent;
}());
export { InfoDrawerComponent };
if (false) {
    /**
     * The title of the info drawer (string or translation key).
     * @type {?}
     */
    InfoDrawerComponent.prototype.title;
    /**
     * The selected index tab.
     * @type {?}
     */
    InfoDrawerComponent.prototype.selectedIndex;
    /**
     * Emitted when the currently active tab changes.
     * @type {?}
     */
    InfoDrawerComponent.prototype.currentTab;
    /** @type {?} */
    InfoDrawerComponent.prototype.contentBlocks;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5mby1kcmF3ZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiaW5mby1kcmF3ZXIvaW5mby1kcmF3ZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxPQUFPLEVBQUUsU0FBUyxFQUFFLGVBQWUsRUFBRSxZQUFZLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxTQUFTLEVBQUUsV0FBVyxFQUFFLFNBQVMsRUFBRSxpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUU5STtJQUFBOzs7O1FBT0ksVUFBSyxHQUFXLEVBQUUsQ0FBQzs7OztRQUluQixTQUFJLEdBQVcsSUFBSSxDQUFDO0lBSXhCLENBQUM7O2dCQWZBLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUscUJBQXFCO29CQUMvQixRQUFRLEVBQUUsc0RBQXNEO2lCQUNuRTs7O3dCQUdJLEtBQUs7dUJBSUwsS0FBSzswQkFHTCxTQUFTLFNBQUMsV0FBVyxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQzs7SUFFMUMsNkJBQUM7Q0FBQSxBQWZELElBZUM7U0FYWSxzQkFBc0I7Ozs7OztJQUUvQix1Q0FDbUI7Ozs7O0lBR25CLHNDQUNvQjs7SUFFcEIseUNBQzBCOztBQUc5QjtJQUFBOzs7O1FBVUksVUFBSyxHQUFnQixJQUFJLENBQUM7Ozs7UUFJMUIsa0JBQWEsR0FBVyxDQUFDLENBQUM7Ozs7UUFJMUIsZUFBVSxHQUF5QixJQUFJLFlBQVksRUFBVSxDQUFDO0lBWWxFLENBQUM7Ozs7SUFQRywyQ0FBYTs7O0lBQWI7UUFDSSxPQUFPLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7OztJQUVELHlDQUFXOzs7O0lBQVgsVUFBWSxLQUF3QjtRQUNoQyxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7Z0JBN0JKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsaUJBQWlCO29CQUMzQixvM0NBQTJDO29CQUUzQyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtvQkFDckMsSUFBSSxFQUFFLEVBQUUsT0FBTyxFQUFFLGlCQUFpQixFQUFFOztpQkFDdkM7Ozt3QkFHSSxLQUFLO2dDQUlMLEtBQUs7NkJBSUwsTUFBTTtnQ0FHTixlQUFlLFNBQUMsc0JBQXNCOztJQVUzQywwQkFBQztDQUFBLEFBOUJELElBOEJDO1NBdkJZLG1CQUFtQjs7Ozs7O0lBRTVCLG9DQUMwQjs7Ozs7SUFHMUIsNENBQzBCOzs7OztJQUcxQix5Q0FDOEQ7O0lBRTlELDRDQUNpRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxuXG4vKiFcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxuICpcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cbiAqL1xuXG5pbXBvcnQgeyBDb21wb25lbnQsIENvbnRlbnRDaGlsZHJlbiwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT3V0cHV0LCBRdWVyeUxpc3QsIFRlbXBsYXRlUmVmLCBWaWV3Q2hpbGQsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBNYXRUYWJDaGFuZ2VFdmVudCB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYWRmLWluZm8tZHJhd2VyLXRhYicsXG4gICAgdGVtcGxhdGU6ICc8bmctdGVtcGxhdGU+PG5nLWNvbnRlbnQ+PC9uZy1jb250ZW50PjwvbmctdGVtcGxhdGU+J1xufSlcbmV4cG9ydCBjbGFzcyBJbmZvRHJhd2VyVGFiQ29tcG9uZW50IHtcbiAgICAvKiogVGhlIHRpdGxlIG9mIHRoZSB0YWIgKHN0cmluZyBvciB0cmFuc2xhdGlvbiBrZXkpLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgbGFiZWw6IHN0cmluZyA9ICcnO1xuXG4gICAgLyoqIEljb24gdG8gcmVuZGVyIGZvciB0aGUgdGFiLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgaWNvbjogc3RyaW5nID0gbnVsbDtcblxuICAgIEBWaWV3Q2hpbGQoVGVtcGxhdGVSZWYsIHtzdGF0aWM6IHRydWV9KVxuICAgIGNvbnRlbnQ6IFRlbXBsYXRlUmVmPGFueT47XG59XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYWRmLWluZm8tZHJhd2VyJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vaW5mby1kcmF3ZXIuY29tcG9uZW50Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL2luZm8tZHJhd2VyLmNvbXBvbmVudC5zY3NzJ10sXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcbiAgICBob3N0OiB7ICdjbGFzcyc6ICdhZGYtaW5mby1kcmF3ZXInIH1cbn0pXG5leHBvcnQgY2xhc3MgSW5mb0RyYXdlckNvbXBvbmVudCB7XG4gICAgLyoqIFRoZSB0aXRsZSBvZiB0aGUgaW5mbyBkcmF3ZXIgKHN0cmluZyBvciB0cmFuc2xhdGlvbiBrZXkpLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgdGl0bGU6IHN0cmluZ3xudWxsID0gbnVsbDtcblxuICAgIC8qKiBUaGUgc2VsZWN0ZWQgaW5kZXggdGFiLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgc2VsZWN0ZWRJbmRleDogbnVtYmVyID0gMDtcblxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIGN1cnJlbnRseSBhY3RpdmUgdGFiIGNoYW5nZXMuICovXG4gICAgQE91dHB1dCgpXG4gICAgY3VycmVudFRhYjogRXZlbnRFbWl0dGVyPG51bWJlcj4gPSBuZXcgRXZlbnRFbWl0dGVyPG51bWJlcj4oKTtcblxuICAgIEBDb250ZW50Q2hpbGRyZW4oSW5mb0RyYXdlclRhYkNvbXBvbmVudClcbiAgICBjb250ZW50QmxvY2tzOiBRdWVyeUxpc3Q8SW5mb0RyYXdlclRhYkNvbXBvbmVudD47XG5cbiAgICBzaG93VGFiTGF5b3V0KCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gdGhpcy5jb250ZW50QmxvY2tzLmxlbmd0aCA+IDA7XG4gICAgfVxuXG4gICAgb25UYWJDaGFuZ2UoZXZlbnQ6IE1hdFRhYkNoYW5nZUV2ZW50KSB7XG4gICAgICAgIHRoaXMuY3VycmVudFRhYi5lbWl0KGV2ZW50LmluZGV4KTtcbiAgICB9XG59XG4iXX0=