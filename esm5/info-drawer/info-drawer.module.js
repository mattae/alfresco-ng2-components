/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MaterialModule } from '../material.module';
import { InfoDrawerLayoutComponent, InfoDrawerTitleDirective, InfoDrawerButtonsDirective, InfoDrawerContentDirective } from './info-drawer-layout.component';
import { InfoDrawerComponent, InfoDrawerTabComponent } from './info-drawer.component';
import { TranslateModule } from '@ngx-translate/core';
/**
 * @return {?}
 */
export function declarations() {
    return [
        InfoDrawerLayoutComponent,
        InfoDrawerTabComponent,
        InfoDrawerComponent,
        InfoDrawerTitleDirective,
        InfoDrawerButtonsDirective,
        InfoDrawerContentDirective
    ];
}
var InfoDrawerModule = /** @class */ (function () {
    function InfoDrawerModule() {
    }
    InfoDrawerModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        MaterialModule,
                        TranslateModule.forChild()
                    ],
                    declarations: declarations(),
                    exports: declarations()
                },] }
    ];
    return InfoDrawerModule;
}());
export { InfoDrawerModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5mby1kcmF3ZXIubW9kdWxlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsiaW5mby1kcmF3ZXIvaW5mby1kcmF3ZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUVwRCxPQUFPLEVBQUUseUJBQXlCLEVBQUUsd0JBQXdCLEVBQUUsMEJBQTBCLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUM3SixPQUFPLEVBQUUsbUJBQW1CLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN0RixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0scUJBQXFCLENBQUM7Ozs7QUFFdEQsTUFBTSxVQUFVLFlBQVk7SUFDeEIsT0FBTztRQUNILHlCQUF5QjtRQUN6QixzQkFBc0I7UUFDdEIsbUJBQW1CO1FBQ25CLHdCQUF3QjtRQUN4QiwwQkFBMEI7UUFDMUIsMEJBQTBCO0tBQzdCLENBQUM7QUFDTixDQUFDO0FBRUQ7SUFBQTtJQVMrQixDQUFDOztnQkFUL0IsUUFBUSxTQUFDO29CQUNOLE9BQU8sRUFBRTt3QkFDTCxZQUFZO3dCQUNaLGNBQWM7d0JBQ2QsZUFBZSxDQUFDLFFBQVEsRUFBRTtxQkFDN0I7b0JBQ0QsWUFBWSxFQUFFLFlBQVksRUFBRTtvQkFDNUIsT0FBTyxFQUFFLFlBQVksRUFBRTtpQkFDMUI7O0lBQzhCLHVCQUFDO0NBQUEsQUFUaEMsSUFTZ0M7U0FBbkIsZ0JBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSAnLi4vbWF0ZXJpYWwubW9kdWxlJztcclxuXHJcbmltcG9ydCB7IEluZm9EcmF3ZXJMYXlvdXRDb21wb25lbnQsIEluZm9EcmF3ZXJUaXRsZURpcmVjdGl2ZSwgSW5mb0RyYXdlckJ1dHRvbnNEaXJlY3RpdmUsIEluZm9EcmF3ZXJDb250ZW50RGlyZWN0aXZlIH0gZnJvbSAnLi9pbmZvLWRyYXdlci1sYXlvdXQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgSW5mb0RyYXdlckNvbXBvbmVudCwgSW5mb0RyYXdlclRhYkNvbXBvbmVudCB9IGZyb20gJy4vaW5mby1kcmF3ZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XHJcblxyXG5leHBvcnQgZnVuY3Rpb24gZGVjbGFyYXRpb25zKCkge1xyXG4gICAgcmV0dXJuIFtcclxuICAgICAgICBJbmZvRHJhd2VyTGF5b3V0Q29tcG9uZW50LFxyXG4gICAgICAgIEluZm9EcmF3ZXJUYWJDb21wb25lbnQsXHJcbiAgICAgICAgSW5mb0RyYXdlckNvbXBvbmVudCxcclxuICAgICAgICBJbmZvRHJhd2VyVGl0bGVEaXJlY3RpdmUsXHJcbiAgICAgICAgSW5mb0RyYXdlckJ1dHRvbnNEaXJlY3RpdmUsXHJcbiAgICAgICAgSW5mb0RyYXdlckNvbnRlbnREaXJlY3RpdmVcclxuICAgIF07XHJcbn1cclxuXHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgICAgIFRyYW5zbGF0ZU1vZHVsZS5mb3JDaGlsZCgpXHJcbiAgICBdLFxyXG4gICAgZGVjbGFyYXRpb25zOiBkZWNsYXJhdGlvbnMoKSxcclxuICAgIGV4cG9ydHM6IGRlY2xhcmF0aW9ucygpXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBJbmZvRHJhd2VyTW9kdWxlIHt9XHJcbiJdfQ==