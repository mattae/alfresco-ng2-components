/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
var HeaderLayoutComponent = /** @class */ (function () {
    function HeaderLayoutComponent() {
        /**
         * The router link for the application logo, when clicked.
         */
        this.redirectUrl = '/';
        /**
         * Toggles whether the sidenav button will be displayed in the header
         * or not.
         */
        this.showSidenavToggle = true;
        /**
         * Emitted when the sidenav button is clicked.
         */
        this.clicked = new EventEmitter();
        /**
         * The side of the page that the drawer is attached to (can be 'start' or 'end')
         */
        this.position = 'start';
    }
    /**
     * @return {?}
     */
    HeaderLayoutComponent.prototype.toggleMenu = /**
     * @return {?}
     */
    function () {
        this.clicked.emit(true);
    };
    /**
     * @return {?}
     */
    HeaderLayoutComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        if (!this.logo) {
            this.logo = './assets/images/logo.png';
        }
    };
    HeaderLayoutComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-layout-header',
                    template: "<mat-toolbar\r\n    [color]=\"color\"\r\n    [style.background-color]=\"color\"\r\n    role=\"heading\"\r\n    aria-level=\"1\">\r\n    <button\r\n        *ngIf=\"showSidenavToggle && position === 'start'\"\r\n        id=\"adf-sidebar-toggle-start\"\r\n        data-automation-id=\"adf-menu-icon\"\r\n        class=\"mat-icon-button adf-menu-icon\"\r\n        mat-icon-button\r\n        (click)=\"toggleMenu()\"\r\n        aria-label=\"Toggle Menu\">\r\n        <mat-icon\r\n            class=\"mat-icon material-icon\"\r\n            role=\"img\"\r\n            aria-hidden=\"true\">menu</mat-icon>\r\n    </button>\r\n\r\n    <a [routerLink]=\"redirectUrl\" title=\"{{ tooltip }}\">\r\n        <img\r\n            src=\"{{ logo }}\"\r\n            class=\"adf-app-logo\"\r\n            alt=\"{{ 'CORE.HEADER.LOGO_ARIA' | translate }}\"\r\n        />\r\n    </a>\r\n\r\n    <span\r\n        [routerLink]=\"redirectUrl\"\r\n        fxFlex=\"1 1 auto\"\r\n        fxShow\r\n        fxHide.lt-sm=\"true\"\r\n        class=\"adf-app-title\"\r\n        >{{ title }}</span>\r\n    <ng-content></ng-content>\r\n\r\n    <button\r\n        *ngIf=\"showSidenavToggle && position === 'end'\"\r\n        id=\"adf-sidebar-toggle-end\"\r\n        data-automation-id=\"adf-menu-icon\"\r\n        class=\"mat-icon-button adf-menu-icon\"\r\n        mat-icon-button\r\n        (click)=\"toggleMenu()\"\r\n        aria-label=\"Toggle Menu\">\r\n        <mat-icon\r\n            class=\"mat-icon material-icon\"\r\n            role=\"img\"\r\n            aria-hidden=\"true\">menu</mat-icon>\r\n    </button>\r\n</mat-toolbar>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    host: { class: 'adf-layout-header' }
                }] }
    ];
    HeaderLayoutComponent.propDecorators = {
        title: [{ type: Input }],
        logo: [{ type: Input }],
        redirectUrl: [{ type: Input }],
        tooltip: [{ type: Input }],
        color: [{ type: Input }],
        showSidenavToggle: [{ type: Input }],
        clicked: [{ type: Output }],
        position: [{ type: Input }]
    };
    return HeaderLayoutComponent;
}());
export { HeaderLayoutComponent };
if (false) {
    /**
     * Title of the application.
     * @type {?}
     */
    HeaderLayoutComponent.prototype.title;
    /**
     * Path to an image file for the application logo.
     * @type {?}
     */
    HeaderLayoutComponent.prototype.logo;
    /**
     * The router link for the application logo, when clicked.
     * @type {?}
     */
    HeaderLayoutComponent.prototype.redirectUrl;
    /**
     * The tooltip text for the application logo.
     * @type {?}
     */
    HeaderLayoutComponent.prototype.tooltip;
    /**
     * Background color for the header. It can be any hex color code or one
     * of the Material theme colors: 'primary', 'accent' or 'warn'.
     * @type {?}
     */
    HeaderLayoutComponent.prototype.color;
    /**
     * Toggles whether the sidenav button will be displayed in the header
     * or not.
     * @type {?}
     */
    HeaderLayoutComponent.prototype.showSidenavToggle;
    /**
     * Emitted when the sidenav button is clicked.
     * @type {?}
     */
    HeaderLayoutComponent.prototype.clicked;
    /**
     * The side of the page that the drawer is attached to (can be 'start' or 'end')
     * @type {?}
     */
    HeaderLayoutComponent.prototype.position;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaGVhZGVyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImxheW91dC9jb21wb25lbnRzL2hlYWRlci9oZWFkZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUsaUJBQWlCLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFFbEc7SUFBQTs7OztRQWNhLGdCQUFXLEdBQW1CLEdBQUcsQ0FBQzs7Ozs7UUFlbEMsc0JBQWlCLEdBQVksSUFBSSxDQUFDOzs7O1FBR2pDLFlBQU8sR0FBRyxJQUFJLFlBQVksRUFBTyxDQUFDOzs7O1FBR25DLGFBQVEsR0FBRyxPQUFPLENBQUM7SUFXaEMsQ0FBQzs7OztJQVRHLDBDQUFVOzs7SUFBVjtRQUNJLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzVCLENBQUM7Ozs7SUFFRCx3Q0FBUTs7O0lBQVI7UUFDSSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNaLElBQUksQ0FBQyxJQUFJLEdBQUcsMEJBQTBCLENBQUM7U0FDMUM7SUFDTCxDQUFDOztnQkE3Q0osU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxtQkFBbUI7b0JBQzdCLHVsREFBc0M7b0JBQ3RDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO29CQUNyQyxJQUFJLEVBQUUsRUFBRSxLQUFLLEVBQUUsbUJBQW1CLEVBQUU7aUJBQ3ZDOzs7d0JBR0ksS0FBSzt1QkFHTCxLQUFLOzhCQUdMLEtBQUs7MEJBR0wsS0FBSzt3QkFNTCxLQUFLO29DQU1MLEtBQUs7MEJBR0wsTUFBTTsyQkFHTixLQUFLOztJQVdWLDRCQUFDO0NBQUEsQUE5Q0QsSUE4Q0M7U0F4Q1kscUJBQXFCOzs7Ozs7SUFFOUIsc0NBQXVCOzs7OztJQUd2QixxQ0FBc0I7Ozs7O0lBR3RCLDRDQUEyQzs7Ozs7SUFHM0Msd0NBQXlCOzs7Ozs7SUFNekIsc0NBQXVCOzs7Ozs7SUFNdkIsa0RBQTJDOzs7OztJQUczQyx3Q0FBNEM7Ozs7O0lBRzVDLHlDQUE0QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPdXRwdXQsIEV2ZW50RW1pdHRlciwgVmlld0VuY2Fwc3VsYXRpb24sIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1sYXlvdXQtaGVhZGVyJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9oZWFkZXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICAgIGhvc3Q6IHsgY2xhc3M6ICdhZGYtbGF5b3V0LWhlYWRlcicgfVxyXG59KVxyXG5leHBvcnQgY2xhc3MgSGVhZGVyTGF5b3V0Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcclxuICAgIC8qKiBUaXRsZSBvZiB0aGUgYXBwbGljYXRpb24uICovXHJcbiAgICBASW5wdXQoKSB0aXRsZTogc3RyaW5nO1xyXG5cclxuICAgIC8qKiBQYXRoIHRvIGFuIGltYWdlIGZpbGUgZm9yIHRoZSBhcHBsaWNhdGlvbiBsb2dvLiAqL1xyXG4gICAgQElucHV0KCkgbG9nbzogc3RyaW5nO1xyXG5cclxuICAgIC8qKiBUaGUgcm91dGVyIGxpbmsgZm9yIHRoZSBhcHBsaWNhdGlvbiBsb2dvLCB3aGVuIGNsaWNrZWQuICovXHJcbiAgICBASW5wdXQoKSByZWRpcmVjdFVybDogc3RyaW5nIHwgYW55W10gPSAnLyc7XHJcblxyXG4gICAgLyoqIFRoZSB0b29sdGlwIHRleHQgZm9yIHRoZSBhcHBsaWNhdGlvbiBsb2dvLiAqL1xyXG4gICAgQElucHV0KCkgdG9vbHRpcDogc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQmFja2dyb3VuZCBjb2xvciBmb3IgdGhlIGhlYWRlci4gSXQgY2FuIGJlIGFueSBoZXggY29sb3IgY29kZSBvciBvbmVcclxuICAgICAqIG9mIHRoZSBNYXRlcmlhbCB0aGVtZSBjb2xvcnM6ICdwcmltYXJ5JywgJ2FjY2VudCcgb3IgJ3dhcm4nLlxyXG4gICAgICovXHJcbiAgICBASW5wdXQoKSBjb2xvcjogc3RyaW5nO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogVG9nZ2xlcyB3aGV0aGVyIHRoZSBzaWRlbmF2IGJ1dHRvbiB3aWxsIGJlIGRpc3BsYXllZCBpbiB0aGUgaGVhZGVyXHJcbiAgICAgKiBvciBub3QuXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpIHNob3dTaWRlbmF2VG9nZ2xlOiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSBzaWRlbmF2IGJ1dHRvbiBpcyBjbGlja2VkLiAqL1xyXG4gICAgQE91dHB1dCgpIGNsaWNrZWQgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICAvKiogVGhlIHNpZGUgb2YgdGhlIHBhZ2UgdGhhdCB0aGUgZHJhd2VyIGlzIGF0dGFjaGVkIHRvIChjYW4gYmUgJ3N0YXJ0JyBvciAnZW5kJykgKi9cclxuICAgIEBJbnB1dCgpIHBvc2l0aW9uID0gJ3N0YXJ0JztcclxuXHJcbiAgICB0b2dnbGVNZW51KCkge1xyXG4gICAgICAgIHRoaXMuY2xpY2tlZC5lbWl0KHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIGlmICghdGhpcy5sb2dvKSB7XHJcbiAgICAgICAgICAgIHRoaXMubG9nbyA9ICcuL2Fzc2V0cy9pbWFnZXMvbG9nby5wbmcnO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=