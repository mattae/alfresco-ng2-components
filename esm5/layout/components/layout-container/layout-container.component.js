/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewChild, ViewEncapsulation } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { sidenavAnimation, contentAnimation } from '../../helpers/animations';
var LayoutContainerComponent = /** @class */ (function () {
    function LayoutContainerComponent() {
        this.hideSidenav = false;
        this.expandedSidenav = true;
        /**
         * The side that the drawer is attached to 'start' | 'end' page
         */
        this.position = 'start';
        /**
         * Layout text orientation 'ltr' | 'rtl'
         */
        this.direction = 'ltr';
        this.SIDENAV_STATES = { MOBILE: {}, EXPANDED: {}, COMPACT: {} };
        this.CONTENT_STATES = { MOBILE: {}, EXPANDED: {}, COMPACT: {} };
        this.onMediaQueryChange = this.onMediaQueryChange.bind(this);
    }
    /**
     * @return {?}
     */
    LayoutContainerComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.SIDENAV_STATES.MOBILE = { value: 'expanded', params: { width: this.sidenavMax } };
        this.SIDENAV_STATES.EXPANDED = { value: 'expanded', params: { width: this.sidenavMax } };
        this.SIDENAV_STATES.COMPACT = { value: 'compact', params: { width: this.sidenavMin } };
        this.CONTENT_STATES.MOBILE = { value: 'expanded' };
        this.mediaQueryList.addListener(this.onMediaQueryChange);
        if (this.isMobileScreenSize) {
            this.sidenavAnimationState = this.SIDENAV_STATES.MOBILE;
            this.contentAnimationState = this.CONTENT_STATES.MOBILE;
        }
        else if (this.expandedSidenav) {
            this.sidenavAnimationState = this.SIDENAV_STATES.EXPANDED;
            this.contentAnimationState = this.toggledContentAnimation;
        }
        else {
            this.sidenavAnimationState = this.SIDENAV_STATES.COMPACT;
            this.contentAnimationState = this.toggledContentAnimation;
        }
    };
    /**
     * @return {?}
     */
    LayoutContainerComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.mediaQueryList.removeListener(this.onMediaQueryChange);
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    LayoutContainerComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        if (changes && changes.direction) {
            this.contentAnimationState = this.toggledContentAnimation;
        }
    };
    /**
     * @return {?}
     */
    LayoutContainerComponent.prototype.toggleMenu = /**
     * @return {?}
     */
    function () {
        if (this.isMobileScreenSize) {
            this.sidenav.toggle();
        }
        else {
            this.sidenavAnimationState = this.toggledSidenavAnimation;
            this.contentAnimationState = this.toggledContentAnimation;
        }
    };
    Object.defineProperty(LayoutContainerComponent.prototype, "isMobileScreenSize", {
        get: /**
         * @return {?}
         */
        function () {
            return this.mediaQueryList.matches;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    LayoutContainerComponent.prototype.getContentAnimationState = /**
     * @return {?}
     */
    function () {
        return this.contentAnimationState;
    };
    Object.defineProperty(LayoutContainerComponent.prototype, "toggledSidenavAnimation", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.sidenavAnimationState === this.SIDENAV_STATES.EXPANDED
                ? this.SIDENAV_STATES.COMPACT
                : this.SIDENAV_STATES.EXPANDED;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LayoutContainerComponent.prototype, "toggledContentAnimation", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            if (this.isMobileScreenSize) {
                return this.CONTENT_STATES.MOBILE;
            }
            if (this.sidenavAnimationState === this.SIDENAV_STATES.EXPANDED) {
                if (this.position === 'start' && this.direction === 'ltr') {
                    return { value: 'compact', params: { 'margin-left': this.sidenavMax } };
                }
                if (this.position === 'start' && this.direction === 'rtl') {
                    return { value: 'compact', params: { 'margin-right': this.sidenavMax } };
                }
                if (this.position === 'end' && this.direction === 'ltr') {
                    return { value: 'compact', params: { 'margin-right': this.sidenavMax } };
                }
                if (this.position === 'end' && this.direction === 'rtl') {
                    return { value: 'compact', params: { 'margin-left': this.sidenavMax } };
                }
            }
            else {
                if (this.position === 'start' && this.direction === 'ltr') {
                    return { value: 'expanded', params: { 'margin-left': this.sidenavMin } };
                }
                if (this.position === 'start' && this.direction === 'rtl') {
                    return { value: 'expanded', params: { 'margin-right': this.sidenavMin } };
                }
                if (this.position === 'end' && this.direction === 'ltr') {
                    return { value: 'expanded', params: { 'margin-right': this.sidenavMin } };
                }
                if (this.position === 'end' && this.direction === 'rtl') {
                    return { value: 'expanded', params: { 'margin-left': this.sidenavMin } };
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @private
     * @return {?}
     */
    LayoutContainerComponent.prototype.onMediaQueryChange = /**
     * @private
     * @return {?}
     */
    function () {
        this.sidenavAnimationState = this.SIDENAV_STATES.EXPANDED;
        this.contentAnimationState = this.toggledContentAnimation;
    };
    LayoutContainerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-layout-container',
                    template: "<mat-sidenav-container>\r\n    <mat-sidenav\r\n        [position]=\"position\"\r\n        [disableClose]=\"!isMobileScreenSize\"\r\n        [ngClass]=\"{ 'adf-sidenav--hidden': hideSidenav }\"\r\n        [@sidenavAnimation]=\"sidenavAnimationState\"\r\n        [opened]=\"!isMobileScreenSize\"\r\n        [mode]=\"isMobileScreenSize ? 'over' : 'side'\">\r\n        <ng-content sidenav select=\"[app-layout-navigation]\"></ng-content>\r\n    </mat-sidenav>\r\n\r\n    <div>\r\n        <div class=\"adf-container-full-width\" [@contentAnimationLeft]=\"getContentAnimationState()\">\r\n            <ng-content select=\"[app-layout-content]\"></ng-content>\r\n        </div>\r\n    </div>\r\n</mat-sidenav-container>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    animations: [sidenavAnimation, contentAnimation],
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    LayoutContainerComponent.ctorParameters = function () { return []; };
    LayoutContainerComponent.propDecorators = {
        sidenavMin: [{ type: Input }],
        sidenavMax: [{ type: Input }],
        mediaQueryList: [{ type: Input }],
        hideSidenav: [{ type: Input }],
        expandedSidenav: [{ type: Input }],
        position: [{ type: Input }],
        direction: [{ type: Input }],
        sidenav: [{ type: ViewChild, args: [MatSidenav, { static: true },] }]
    };
    return LayoutContainerComponent;
}());
export { LayoutContainerComponent };
if (false) {
    /** @type {?} */
    LayoutContainerComponent.prototype.sidenavMin;
    /** @type {?} */
    LayoutContainerComponent.prototype.sidenavMax;
    /** @type {?} */
    LayoutContainerComponent.prototype.mediaQueryList;
    /** @type {?} */
    LayoutContainerComponent.prototype.hideSidenav;
    /** @type {?} */
    LayoutContainerComponent.prototype.expandedSidenav;
    /**
     * The side that the drawer is attached to 'start' | 'end' page
     * @type {?}
     */
    LayoutContainerComponent.prototype.position;
    /**
     * Layout text orientation 'ltr' | 'rtl'
     * @type {?}
     */
    LayoutContainerComponent.prototype.direction;
    /** @type {?} */
    LayoutContainerComponent.prototype.sidenav;
    /** @type {?} */
    LayoutContainerComponent.prototype.sidenavAnimationState;
    /** @type {?} */
    LayoutContainerComponent.prototype.contentAnimationState;
    /** @type {?} */
    LayoutContainerComponent.prototype.SIDENAV_STATES;
    /** @type {?} */
    LayoutContainerComponent.prototype.CONTENT_STATES;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGF5b3V0LWNvbnRhaW5lci5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJsYXlvdXQvY29tcG9uZW50cy9sYXlvdXQtY29udGFpbmVyL2xheW91dC1jb250YWluZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQXFCLGlCQUFpQixFQUFhLE1BQU0sZUFBZSxDQUFDO0FBQzdHLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUc5RTtJQStCSTtRQWpCUyxnQkFBVyxHQUFHLEtBQUssQ0FBQztRQUNwQixvQkFBZSxHQUFHLElBQUksQ0FBQzs7OztRQUd2QixhQUFRLEdBQUcsT0FBTyxDQUFDOzs7O1FBR25CLGNBQVMsR0FBYyxLQUFLLENBQUM7UUFPdEMsbUJBQWMsR0FBRyxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUM7UUFDM0QsbUJBQWMsR0FBRyxFQUFFLE1BQU0sRUFBRSxFQUFFLEVBQUUsUUFBUSxFQUFFLEVBQUUsRUFBRSxPQUFPLEVBQUUsRUFBRSxFQUFFLENBQUM7UUFHdkQsSUFBSSxDQUFDLGtCQUFrQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7SUFDakUsQ0FBQzs7OztJQUVELDJDQUFROzs7SUFBUjtRQUNJLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsRUFBRSxLQUFLLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUM7UUFDdkYsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLEdBQUcsRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQztRQUN6RixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sR0FBRyxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDO1FBRXZGLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxHQUFHLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxDQUFDO1FBRW5ELElBQUksQ0FBQyxjQUFjLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO1FBRXpELElBQUksSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQ3pCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE1BQU0sQ0FBQztZQUN4RCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7U0FDM0Q7YUFBTSxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDN0IsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsUUFBUSxDQUFDO1lBQzFELElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUM7U0FDN0Q7YUFBTTtZQUNILElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQztZQUN6RCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDO1NBQzdEO0lBQ0wsQ0FBQzs7OztJQUVELDhDQUFXOzs7SUFBWDtRQUNJLElBQUksQ0FBQyxjQUFjLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDO0lBQ2hFLENBQUM7Ozs7O0lBRUQsOENBQVc7Ozs7SUFBWCxVQUFZLE9BQU87UUFDZixJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMsU0FBUyxFQUFFO1lBQzlCLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUM7U0FDN0Q7SUFDTCxDQUFDOzs7O0lBRUQsNkNBQVU7OztJQUFWO1FBQ0ksSUFBSSxJQUFJLENBQUMsa0JBQWtCLEVBQUU7WUFDekIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQztTQUN6QjthQUFNO1lBQ0gsSUFBSSxDQUFDLHFCQUFxQixHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQztZQUMxRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDO1NBQzdEO0lBQ0wsQ0FBQztJQUVELHNCQUFJLHdEQUFrQjs7OztRQUF0QjtZQUNJLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUM7UUFDdkMsQ0FBQzs7O09BQUE7Ozs7SUFFRCwyREFBd0I7OztJQUF4QjtRQUNJLE9BQU8sSUFBSSxDQUFDLHFCQUFxQixDQUFDO0lBQ3RDLENBQUM7SUFFRCxzQkFBWSw2REFBdUI7Ozs7O1FBQW5DO1lBQ0ksT0FBTyxJQUFJLENBQUMscUJBQXFCLEtBQUssSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRO2dCQUM5RCxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPO2dCQUM3QixDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUM7UUFDdkMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBWSw2REFBdUI7Ozs7O1FBQW5DO1lBQ0ksSUFBSSxJQUFJLENBQUMsa0JBQWtCLEVBQUU7Z0JBQ3pCLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUM7YUFDckM7WUFFRCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsS0FBSyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsRUFBRTtnQkFDN0QsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLE9BQU8sSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLEtBQUssRUFBRTtvQkFDdkQsT0FBTyxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLEVBQUUsYUFBYSxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDO2lCQUMzRTtnQkFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssT0FBTyxJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssS0FBSyxFQUFFO29CQUN2RCxPQUFPLEVBQUUsS0FBSyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsRUFBRSxjQUFjLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUM7aUJBQzVFO2dCQUVELElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxLQUFLLEVBQUU7b0JBQ3JELE9BQU8sRUFBRSxLQUFLLEVBQUUsU0FBUyxFQUFFLE1BQU0sRUFBRSxFQUFFLGNBQWMsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQztpQkFDNUU7Z0JBRUQsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLEtBQUssSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLEtBQUssRUFBRTtvQkFDckQsT0FBTyxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLEVBQUUsYUFBYSxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDO2lCQUMzRTthQUVKO2lCQUFNO2dCQUNILElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxPQUFPLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxLQUFLLEVBQUU7b0JBQ3ZELE9BQU8sRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxFQUFFLGFBQWEsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQztpQkFDNUU7Z0JBRUQsSUFBSSxJQUFJLENBQUMsUUFBUSxLQUFLLE9BQU8sSUFBSSxJQUFJLENBQUMsU0FBUyxLQUFLLEtBQUssRUFBRTtvQkFDdkQsT0FBTyxFQUFFLEtBQUssRUFBRSxVQUFVLEVBQUUsTUFBTSxFQUFFLEVBQUUsY0FBYyxFQUFFLElBQUksQ0FBQyxVQUFVLEVBQUUsRUFBRSxDQUFDO2lCQUM3RTtnQkFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLEtBQUssS0FBSyxJQUFJLElBQUksQ0FBQyxTQUFTLEtBQUssS0FBSyxFQUFFO29CQUNyRCxPQUFPLEVBQUUsS0FBSyxFQUFFLFVBQVUsRUFBRSxNQUFNLEVBQUUsRUFBRSxjQUFjLEVBQUUsSUFBSSxDQUFDLFVBQVUsRUFBRSxFQUFFLENBQUM7aUJBQzdFO2dCQUVELElBQUksSUFBSSxDQUFDLFFBQVEsS0FBSyxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsS0FBSyxLQUFLLEVBQUU7b0JBQ3JELE9BQU8sRUFBRSxLQUFLLEVBQUUsVUFBVSxFQUFFLE1BQU0sRUFBRSxFQUFFLGFBQWEsRUFBRSxJQUFJLENBQUMsVUFBVSxFQUFFLEVBQUUsQ0FBQztpQkFDNUU7YUFDSjtRQUNMLENBQUM7OztPQUFBOzs7OztJQUVPLHFEQUFrQjs7OztJQUExQjtRQUNJLElBQUksQ0FBQyxxQkFBcUIsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQztRQUMxRCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLHVCQUF1QixDQUFDO0lBQzlELENBQUM7O2dCQXJJSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLHNCQUFzQjtvQkFDaEMsd3RCQUFnRDtvQkFFaEQsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7b0JBQ3JDLFVBQVUsRUFBRSxDQUFDLGdCQUFnQixFQUFFLGdCQUFnQixDQUFDOztpQkFDbkQ7Ozs7OzZCQUVJLEtBQUs7NkJBQ0wsS0FBSztpQ0FHTCxLQUFLOzhCQUVMLEtBQUs7a0NBQ0wsS0FBSzsyQkFHTCxLQUFLOzRCQUdMLEtBQUs7MEJBRUwsU0FBUyxTQUFDLFVBQVUsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7O0lBK0d6QywrQkFBQztDQUFBLEFBdElELElBc0lDO1NBL0hZLHdCQUF3Qjs7O0lBQ2pDLDhDQUE0Qjs7SUFDNUIsOENBQTRCOztJQUc1QixrREFBOEM7O0lBRTlDLCtDQUE2Qjs7SUFDN0IsbURBQWdDOzs7OztJQUdoQyw0Q0FBNEI7Ozs7O0lBRzVCLDZDQUFzQzs7SUFFdEMsMkNBQTJEOztJQUUzRCx5REFBMkI7O0lBQzNCLHlEQUEyQjs7SUFFM0Isa0RBQTJEOztJQUMzRCxrREFBMkQiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cblxuLyohXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cbiAqXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4gKlxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuICpcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXG4gKi9cblxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgVmlld0NoaWxkLCBPbkluaXQsIE9uRGVzdHJveSwgVmlld0VuY2Fwc3VsYXRpb24sIE9uQ2hhbmdlcyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgTWF0U2lkZW5hdiB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7IHNpZGVuYXZBbmltYXRpb24sIGNvbnRlbnRBbmltYXRpb24gfSBmcm9tICcuLi8uLi9oZWxwZXJzL2FuaW1hdGlvbnMnO1xuaW1wb3J0IHsgRGlyZWN0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY2RrL2JpZGknO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2FkZi1sYXlvdXQtY29udGFpbmVyJyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vbGF5b3V0LWNvbnRhaW5lci5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vbGF5b3V0LWNvbnRhaW5lci5jb21wb25lbnQuc2NzcyddLFxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXG4gICAgYW5pbWF0aW9uczogW3NpZGVuYXZBbmltYXRpb24sIGNvbnRlbnRBbmltYXRpb25dXG59KVxuZXhwb3J0IGNsYXNzIExheW91dENvbnRhaW5lckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95LCBPbkNoYW5nZXMge1xuICAgIEBJbnB1dCgpIHNpZGVuYXZNaW46IG51bWJlcjtcbiAgICBASW5wdXQoKSBzaWRlbmF2TWF4OiBudW1iZXI7XG5cbiAgICAvLyBcIiB8IGFueVwiLCBiZWNhdXNlIFNhZmFyaSB0aHJvd3MgYW4gZXJyb3Igb3RoZXJ3aXNlLi4uXG4gICAgQElucHV0KCkgbWVkaWFRdWVyeUxpc3Q6IE1lZGlhUXVlcnlMaXN0IHwgYW55O1xuXG4gICAgQElucHV0KCkgaGlkZVNpZGVuYXYgPSBmYWxzZTtcbiAgICBASW5wdXQoKSBleHBhbmRlZFNpZGVuYXYgPSB0cnVlO1xuXG4gICAgLyoqIFRoZSBzaWRlIHRoYXQgdGhlIGRyYXdlciBpcyBhdHRhY2hlZCB0byAnc3RhcnQnIHwgJ2VuZCcgcGFnZSAqL1xuICAgIEBJbnB1dCgpIHBvc2l0aW9uID0gJ3N0YXJ0JztcblxuICAgIC8qKiBMYXlvdXQgdGV4dCBvcmllbnRhdGlvbiAnbHRyJyB8ICdydGwnICovXG4gICAgQElucHV0KCkgZGlyZWN0aW9uOiBEaXJlY3Rpb24gPSAnbHRyJztcblxuICAgIEBWaWV3Q2hpbGQoTWF0U2lkZW5hdiwge3N0YXRpYzogdHJ1ZX0pIHNpZGVuYXY6IE1hdFNpZGVuYXY7XG5cbiAgICBzaWRlbmF2QW5pbWF0aW9uU3RhdGU6IGFueTtcbiAgICBjb250ZW50QW5pbWF0aW9uU3RhdGU6IGFueTtcblxuICAgIFNJREVOQVZfU1RBVEVTID0geyBNT0JJTEU6IHt9LCBFWFBBTkRFRDoge30sIENPTVBBQ1Q6IHt9IH07XG4gICAgQ09OVEVOVF9TVEFURVMgPSB7IE1PQklMRToge30sIEVYUEFOREVEOiB7fSwgQ09NUEFDVDoge30gfTtcblxuICAgIGNvbnN0cnVjdG9yKCkge1xuICAgICAgICB0aGlzLm9uTWVkaWFRdWVyeUNoYW5nZSA9IHRoaXMub25NZWRpYVF1ZXJ5Q2hhbmdlLmJpbmQodGhpcyk7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMuU0lERU5BVl9TVEFURVMuTU9CSUxFID0geyB2YWx1ZTogJ2V4cGFuZGVkJywgcGFyYW1zOiB7IHdpZHRoOiB0aGlzLnNpZGVuYXZNYXggfSB9O1xuICAgICAgICB0aGlzLlNJREVOQVZfU1RBVEVTLkVYUEFOREVEID0geyB2YWx1ZTogJ2V4cGFuZGVkJywgcGFyYW1zOiB7IHdpZHRoOiB0aGlzLnNpZGVuYXZNYXggfSB9O1xuICAgICAgICB0aGlzLlNJREVOQVZfU1RBVEVTLkNPTVBBQ1QgPSB7IHZhbHVlOiAnY29tcGFjdCcsIHBhcmFtczogeyB3aWR0aDogdGhpcy5zaWRlbmF2TWluIH0gfTtcblxuICAgICAgICB0aGlzLkNPTlRFTlRfU1RBVEVTLk1PQklMRSA9IHsgdmFsdWU6ICdleHBhbmRlZCcgfTtcblxuICAgICAgICB0aGlzLm1lZGlhUXVlcnlMaXN0LmFkZExpc3RlbmVyKHRoaXMub25NZWRpYVF1ZXJ5Q2hhbmdlKTtcblxuICAgICAgICBpZiAodGhpcy5pc01vYmlsZVNjcmVlblNpemUpIHtcbiAgICAgICAgICAgIHRoaXMuc2lkZW5hdkFuaW1hdGlvblN0YXRlID0gdGhpcy5TSURFTkFWX1NUQVRFUy5NT0JJTEU7XG4gICAgICAgICAgICB0aGlzLmNvbnRlbnRBbmltYXRpb25TdGF0ZSA9IHRoaXMuQ09OVEVOVF9TVEFURVMuTU9CSUxFO1xuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuZXhwYW5kZWRTaWRlbmF2KSB7XG4gICAgICAgICAgICB0aGlzLnNpZGVuYXZBbmltYXRpb25TdGF0ZSA9IHRoaXMuU0lERU5BVl9TVEFURVMuRVhQQU5ERUQ7XG4gICAgICAgICAgICB0aGlzLmNvbnRlbnRBbmltYXRpb25TdGF0ZSA9IHRoaXMudG9nZ2xlZENvbnRlbnRBbmltYXRpb247XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICB0aGlzLnNpZGVuYXZBbmltYXRpb25TdGF0ZSA9IHRoaXMuU0lERU5BVl9TVEFURVMuQ09NUEFDVDtcbiAgICAgICAgICAgIHRoaXMuY29udGVudEFuaW1hdGlvblN0YXRlID0gdGhpcy50b2dnbGVkQ29udGVudEFuaW1hdGlvbjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG5nT25EZXN0cm95KCk6IHZvaWQge1xuICAgICAgICB0aGlzLm1lZGlhUXVlcnlMaXN0LnJlbW92ZUxpc3RlbmVyKHRoaXMub25NZWRpYVF1ZXJ5Q2hhbmdlKTtcbiAgICB9XG5cbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzKSB7XG4gICAgICAgIGlmIChjaGFuZ2VzICYmIGNoYW5nZXMuZGlyZWN0aW9uKSB7XG4gICAgICAgICAgICB0aGlzLmNvbnRlbnRBbmltYXRpb25TdGF0ZSA9IHRoaXMudG9nZ2xlZENvbnRlbnRBbmltYXRpb247XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICB0b2dnbGVNZW51KCk6IHZvaWQge1xuICAgICAgICBpZiAodGhpcy5pc01vYmlsZVNjcmVlblNpemUpIHtcbiAgICAgICAgICAgIHRoaXMuc2lkZW5hdi50b2dnbGUoKTtcbiAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgIHRoaXMuc2lkZW5hdkFuaW1hdGlvblN0YXRlID0gdGhpcy50b2dnbGVkU2lkZW5hdkFuaW1hdGlvbjtcbiAgICAgICAgICAgIHRoaXMuY29udGVudEFuaW1hdGlvblN0YXRlID0gdGhpcy50b2dnbGVkQ29udGVudEFuaW1hdGlvbjtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIGdldCBpc01vYmlsZVNjcmVlblNpemUoKTogYm9vbGVhbiB7XG4gICAgICAgIHJldHVybiB0aGlzLm1lZGlhUXVlcnlMaXN0Lm1hdGNoZXM7XG4gICAgfVxuXG4gICAgZ2V0Q29udGVudEFuaW1hdGlvblN0YXRlKCkge1xuICAgICAgICByZXR1cm4gdGhpcy5jb250ZW50QW5pbWF0aW9uU3RhdGU7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZXQgdG9nZ2xlZFNpZGVuYXZBbmltYXRpb24oKSB7XG4gICAgICAgIHJldHVybiB0aGlzLnNpZGVuYXZBbmltYXRpb25TdGF0ZSA9PT0gdGhpcy5TSURFTkFWX1NUQVRFUy5FWFBBTkRFRFxuICAgICAgICAgICAgPyB0aGlzLlNJREVOQVZfU1RBVEVTLkNPTVBBQ1RcbiAgICAgICAgICAgIDogdGhpcy5TSURFTkFWX1NUQVRFUy5FWFBBTkRFRDtcbiAgICB9XG5cbiAgICBwcml2YXRlIGdldCB0b2dnbGVkQ29udGVudEFuaW1hdGlvbigpIHtcbiAgICAgICAgaWYgKHRoaXMuaXNNb2JpbGVTY3JlZW5TaXplKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5DT05URU5UX1NUQVRFUy5NT0JJTEU7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5zaWRlbmF2QW5pbWF0aW9uU3RhdGUgPT09IHRoaXMuU0lERU5BVl9TVEFURVMuRVhQQU5ERUQpIHtcbiAgICAgICAgICAgIGlmICh0aGlzLnBvc2l0aW9uID09PSAnc3RhcnQnICYmIHRoaXMuZGlyZWN0aW9uID09PSAnbHRyJykge1xuICAgICAgICAgICAgICAgIHJldHVybiB7IHZhbHVlOiAnY29tcGFjdCcsIHBhcmFtczogeyAnbWFyZ2luLWxlZnQnOiB0aGlzLnNpZGVuYXZNYXggfSB9O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5wb3NpdGlvbiA9PT0gJ3N0YXJ0JyAmJiB0aGlzLmRpcmVjdGlvbiA9PT0gJ3J0bCcpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4geyB2YWx1ZTogJ2NvbXBhY3QnLCBwYXJhbXM6IHsgJ21hcmdpbi1yaWdodCc6IHRoaXMuc2lkZW5hdk1heCB9IH07XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLnBvc2l0aW9uID09PSAnZW5kJyAmJiB0aGlzLmRpcmVjdGlvbiA9PT0gJ2x0cicpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4geyB2YWx1ZTogJ2NvbXBhY3QnLCBwYXJhbXM6IHsgJ21hcmdpbi1yaWdodCc6IHRoaXMuc2lkZW5hdk1heCB9IH07XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIGlmICh0aGlzLnBvc2l0aW9uID09PSAnZW5kJyAmJiB0aGlzLmRpcmVjdGlvbiA9PT0gJ3J0bCcpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4geyB2YWx1ZTogJ2NvbXBhY3QnLCBwYXJhbXM6IHsgJ21hcmdpbi1sZWZ0JzogdGhpcy5zaWRlbmF2TWF4IH0gfTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgaWYgKHRoaXMucG9zaXRpb24gPT09ICdzdGFydCcgJiYgdGhpcy5kaXJlY3Rpb24gPT09ICdsdHInKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHsgdmFsdWU6ICdleHBhbmRlZCcsIHBhcmFtczogeyAnbWFyZ2luLWxlZnQnOiB0aGlzLnNpZGVuYXZNaW4gfSB9O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5wb3NpdGlvbiA9PT0gJ3N0YXJ0JyAmJiB0aGlzLmRpcmVjdGlvbiA9PT0gJ3J0bCcpIHtcbiAgICAgICAgICAgICAgICByZXR1cm4geyB2YWx1ZTogJ2V4cGFuZGVkJywgcGFyYW1zOiB7ICdtYXJnaW4tcmlnaHQnOiB0aGlzLnNpZGVuYXZNaW4gfSB9O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBpZiAodGhpcy5wb3NpdGlvbiA9PT0gJ2VuZCcgJiYgdGhpcy5kaXJlY3Rpb24gPT09ICdsdHInKSB7XG4gICAgICAgICAgICAgICAgcmV0dXJuIHsgdmFsdWU6ICdleHBhbmRlZCcsIHBhcmFtczogeyAnbWFyZ2luLXJpZ2h0JzogdGhpcy5zaWRlbmF2TWluIH0gfTtcbiAgICAgICAgICAgIH1cblxuICAgICAgICAgICAgaWYgKHRoaXMucG9zaXRpb24gPT09ICdlbmQnICYmIHRoaXMuZGlyZWN0aW9uID09PSAncnRsJykge1xuICAgICAgICAgICAgICAgIHJldHVybiB7IHZhbHVlOiAnZXhwYW5kZWQnLCBwYXJhbXM6IHsgJ21hcmdpbi1sZWZ0JzogdGhpcy5zaWRlbmF2TWluIH0gfTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgb25NZWRpYVF1ZXJ5Q2hhbmdlKCkge1xuICAgICAgICB0aGlzLnNpZGVuYXZBbmltYXRpb25TdGF0ZSA9IHRoaXMuU0lERU5BVl9TVEFURVMuRVhQQU5ERUQ7XG4gICAgICAgIHRoaXMuY29udGVudEFuaW1hdGlvblN0YXRlID0gdGhpcy50b2dnbGVkQ29udGVudEFuaW1hdGlvbjtcbiAgICB9XG59XG4iXX0=