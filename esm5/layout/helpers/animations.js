/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { trigger, transition, animate, style, state } from '@angular/animations';
/** @type {?} */
export var sidenavAnimation = trigger('sidenavAnimation', [
    state('expanded', style({ width: '{{ width }}px' }), { params: { width: 0 } }),
    state('compact', style({ width: '{{ width }}px' }), { params: { width: 0 } }),
    transition('compact <=> expanded', animate('0.4s cubic-bezier(0.25, 0.8, 0.25, 1)'))
]);
/** @type {?} */
export var contentAnimation = trigger('contentAnimationLeft', [
    state('expanded', style({
        'margin-left': '{{ margin-left }}px',
        'margin-right': '{{ margin-right }}px'
    }), { params: { 'margin-left': 0, 'margin-right': 0 } }),
    state('compact', style({
        'margin-left': '{{ margin-left }}px',
        'margin-right': '{{ margin-right }}px'
    }), { params: { 'margin-left': 0, 'margin-right': 0 } }),
    transition('expanded <=> compact', animate('400ms cubic-bezier(0.25, 0.8, 0.25, 1)'))
]);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYW5pbWF0aW9ucy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImxheW91dC9oZWxwZXJzL2FuaW1hdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLE9BQU8sRUFBRSxVQUFVLEVBQUUsT0FBTyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQTRCLE1BQU0scUJBQXFCLENBQUM7O0FBRTNHLE1BQU0sS0FBTyxnQkFBZ0IsR0FBNkIsT0FBTyxDQUFDLGtCQUFrQixFQUFFO0lBQ2xGLEtBQUssQ0FBQyxVQUFVLEVBQUUsS0FBSyxDQUFDLEVBQUUsS0FBSyxFQUFFLGVBQWUsRUFBRSxDQUFDLEVBQUUsRUFBRSxNQUFNLEVBQUcsRUFBRSxLQUFLLEVBQUUsQ0FBQyxFQUFFLEVBQUUsQ0FBQztJQUMvRSxLQUFLLENBQUMsU0FBUyxFQUFHLEtBQUssQ0FBQyxFQUFFLEtBQUssRUFBRSxlQUFlLEVBQUUsQ0FBQyxFQUFFLEVBQUUsTUFBTSxFQUFHLEVBQUUsS0FBSyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7SUFDL0UsVUFBVSxDQUFDLHNCQUFzQixFQUFFLE9BQU8sQ0FBQyx1Q0FBdUMsQ0FBQyxDQUFDO0NBQ3ZGLENBQUM7O0FBRUYsTUFBTSxLQUFPLGdCQUFnQixHQUE2QixPQUFPLENBQUMsc0JBQXNCLEVBQUU7SUFDdEYsS0FBSyxDQUFDLFVBQVUsRUFBRSxLQUFLLENBQUM7UUFDcEIsYUFBYSxFQUFFLHFCQUFxQjtRQUNwQyxjQUFjLEVBQUUsc0JBQXNCO0tBQ3pDLENBQUMsRUFBRSxFQUFFLE1BQU0sRUFBRSxFQUFFLGFBQWEsRUFBRSxDQUFDLEVBQUUsY0FBYyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7SUFDeEQsS0FBSyxDQUFDLFNBQVMsRUFBRSxLQUFLLENBQUM7UUFDbkIsYUFBYSxFQUFFLHFCQUFxQjtRQUNwQyxjQUFjLEVBQUUsc0JBQXNCO0tBQ3pDLENBQUMsRUFBRSxFQUFFLE1BQU0sRUFBRSxFQUFFLGFBQWEsRUFBRSxDQUFDLEVBQUUsY0FBYyxFQUFFLENBQUMsRUFBRSxFQUFFLENBQUM7SUFDeEQsVUFBVSxDQUFDLHNCQUFzQixFQUFFLE9BQU8sQ0FBQyx3Q0FBd0MsQ0FBQyxDQUFDO0NBQ3hGLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgdHJpZ2dlciwgdHJhbnNpdGlvbiwgYW5pbWF0ZSwgc3R5bGUsIHN0YXRlLCBBbmltYXRpb25UcmlnZ2VyTWV0YWRhdGEgfSBmcm9tICdAYW5ndWxhci9hbmltYXRpb25zJztcclxuXHJcbmV4cG9ydCBjb25zdCBzaWRlbmF2QW5pbWF0aW9uOiBBbmltYXRpb25UcmlnZ2VyTWV0YWRhdGEgPSB0cmlnZ2VyKCdzaWRlbmF2QW5pbWF0aW9uJywgW1xyXG4gICAgc3RhdGUoJ2V4cGFuZGVkJywgc3R5bGUoeyB3aWR0aDogJ3t7IHdpZHRoIH19cHgnIH0pLCB7IHBhcmFtcyA6IHsgd2lkdGg6IDAgfSB9KSxcclxuICAgIHN0YXRlKCdjb21wYWN0JywgIHN0eWxlKHsgd2lkdGg6ICd7eyB3aWR0aCB9fXB4JyB9KSwgeyBwYXJhbXMgOiB7IHdpZHRoOiAwIH0gfSksXHJcbiAgICB0cmFuc2l0aW9uKCdjb21wYWN0IDw9PiBleHBhbmRlZCcsIGFuaW1hdGUoJzAuNHMgY3ViaWMtYmV6aWVyKDAuMjUsIDAuOCwgMC4yNSwgMSknKSlcclxuXSk7XHJcblxyXG5leHBvcnQgY29uc3QgY29udGVudEFuaW1hdGlvbjogQW5pbWF0aW9uVHJpZ2dlck1ldGFkYXRhID0gdHJpZ2dlcignY29udGVudEFuaW1hdGlvbkxlZnQnLCBbXHJcbiAgICBzdGF0ZSgnZXhwYW5kZWQnLCBzdHlsZSh7XHJcbiAgICAgICAgJ21hcmdpbi1sZWZ0JzogJ3t7IG1hcmdpbi1sZWZ0IH19cHgnLFxyXG4gICAgICAgICdtYXJnaW4tcmlnaHQnOiAne3sgbWFyZ2luLXJpZ2h0IH19cHgnXHJcbiAgICB9KSwgeyBwYXJhbXM6IHsgJ21hcmdpbi1sZWZ0JzogMCwgJ21hcmdpbi1yaWdodCc6IDAgfSB9KSxcclxuICAgIHN0YXRlKCdjb21wYWN0Jywgc3R5bGUoe1xyXG4gICAgICAgICdtYXJnaW4tbGVmdCc6ICd7eyBtYXJnaW4tbGVmdCB9fXB4JyxcclxuICAgICAgICAnbWFyZ2luLXJpZ2h0JzogJ3t7IG1hcmdpbi1yaWdodCB9fXB4J1xyXG4gICAgfSksIHsgcGFyYW1zOiB7ICdtYXJnaW4tbGVmdCc6IDAsICdtYXJnaW4tcmlnaHQnOiAwIH0gfSksXHJcbiAgICB0cmFuc2l0aW9uKCdleHBhbmRlZCA8PT4gY29tcGFjdCcsIGFuaW1hdGUoJzQwMG1zIGN1YmljLWJlemllcigwLjI1LCAwLjgsIDAuMjUsIDEpJykpXHJcbl0pO1xyXG4iXX0=