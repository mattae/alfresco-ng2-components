/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MaterialModule } from '../material.module';
import { SidenavLayoutContentDirective } from './directives/sidenav-layout-content.directive';
import { SidenavLayoutHeaderDirective } from './directives/sidenav-layout-header.directive';
import { SidenavLayoutNavigationDirective } from './directives/sidenav-layout-navigation.directive';
import { SidenavLayoutComponent } from './components/sidenav-layout/sidenav-layout.component';
import { LayoutContainerComponent } from './components/layout-container/layout-container.component';
import { SidebarActionMenuComponent, SidebarMenuDirective, SidebarMenuExpandIconDirective, SidebarMenuTitleIconDirective } from './components/sidebar-action/sidebar-action-menu.component';
import { HeaderLayoutComponent } from './components/header/header.component';
import { TranslateModule } from '@ngx-translate/core';
var LayoutModule = /** @class */ (function () {
    function LayoutModule() {
    }
    LayoutModule.decorators = [
        { type: NgModule, args: [{
                    imports: [
                        CommonModule,
                        MaterialModule,
                        RouterModule,
                        TranslateModule.forChild()
                    ],
                    exports: [
                        SidenavLayoutHeaderDirective,
                        SidenavLayoutContentDirective,
                        SidenavLayoutNavigationDirective,
                        SidenavLayoutComponent,
                        LayoutContainerComponent,
                        SidebarActionMenuComponent,
                        SidebarMenuDirective,
                        SidebarMenuExpandIconDirective,
                        SidebarMenuTitleIconDirective,
                        HeaderLayoutComponent
                    ],
                    declarations: [
                        SidenavLayoutHeaderDirective,
                        SidenavLayoutContentDirective,
                        SidenavLayoutNavigationDirective,
                        SidenavLayoutComponent,
                        LayoutContainerComponent,
                        SidebarActionMenuComponent,
                        SidebarMenuDirective,
                        SidebarMenuExpandIconDirective,
                        SidebarMenuTitleIconDirective,
                        HeaderLayoutComponent
                    ]
                },] }
    ];
    return LayoutModule;
}());
export { LayoutModule };
export { LayoutModule as SidenavLayoutModule };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibGF5b3V0Lm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImxheW91dC9sYXlvdXQubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsUUFBUSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ3pDLE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMvQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sb0JBQW9CLENBQUM7QUFDcEQsT0FBTyxFQUFFLDZCQUE2QixFQUFFLE1BQU0sK0NBQStDLENBQUM7QUFDOUYsT0FBTyxFQUFFLDRCQUE0QixFQUFFLE1BQU0sOENBQThDLENBQUM7QUFDNUYsT0FBTyxFQUFFLGdDQUFnQyxFQUFFLE1BQU0sa0RBQWtELENBQUM7QUFDcEcsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sc0RBQXNELENBQUM7QUFDOUYsT0FBTyxFQUFFLHdCQUF3QixFQUFFLE1BQU0sMERBQTBELENBQUM7QUFDcEcsT0FBTyxFQUFFLDBCQUEwQixFQUFFLG9CQUFvQixFQUNyRCw4QkFBOEIsRUFBRSw2QkFBNkIsRUFBRSxNQUFNLDJEQUEyRCxDQUFDO0FBQ3JJLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQzdFLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxxQkFBcUIsQ0FBQztBQUN0RDtJQUFBO0lBZ0MyQixDQUFDOztnQkFoQzNCLFFBQVEsU0FBQztvQkFDTixPQUFPLEVBQUU7d0JBQ0wsWUFBWTt3QkFDWixjQUFjO3dCQUNkLFlBQVk7d0JBQ1osZUFBZSxDQUFDLFFBQVEsRUFBRTtxQkFDN0I7b0JBQ0QsT0FBTyxFQUFFO3dCQUNMLDRCQUE0Qjt3QkFDNUIsNkJBQTZCO3dCQUM3QixnQ0FBZ0M7d0JBQ2hDLHNCQUFzQjt3QkFDdEIsd0JBQXdCO3dCQUN4QiwwQkFBMEI7d0JBQzFCLG9CQUFvQjt3QkFDcEIsOEJBQThCO3dCQUM5Qiw2QkFBNkI7d0JBQzdCLHFCQUFxQjtxQkFDeEI7b0JBQ0QsWUFBWSxFQUFFO3dCQUNWLDRCQUE0Qjt3QkFDNUIsNkJBQTZCO3dCQUM3QixnQ0FBZ0M7d0JBQ2hDLHNCQUFzQjt3QkFDdEIsd0JBQXdCO3dCQUN4QiwwQkFBMEI7d0JBQzFCLG9CQUFvQjt3QkFDcEIsOEJBQThCO3dCQUM5Qiw2QkFBNkI7d0JBQzdCLHFCQUFxQjtxQkFDeEI7aUJBQ0o7O0lBQzBCLG1CQUFDO0NBQUEsQUFoQzVCLElBZ0M0QjtTQUFmLFlBQVk7QUFDekIsT0FBTyxFQUFFLFlBQVksSUFBSSxtQkFBbUIsRUFBRSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IE5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJvdXRlck1vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IE1hdGVyaWFsTW9kdWxlIH0gZnJvbSAnLi4vbWF0ZXJpYWwubW9kdWxlJztcclxuaW1wb3J0IHsgU2lkZW5hdkxheW91dENvbnRlbnREaXJlY3RpdmUgfSBmcm9tICcuL2RpcmVjdGl2ZXMvc2lkZW5hdi1sYXlvdXQtY29udGVudC5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBTaWRlbmF2TGF5b3V0SGVhZGVyRGlyZWN0aXZlIH0gZnJvbSAnLi9kaXJlY3RpdmVzL3NpZGVuYXYtbGF5b3V0LWhlYWRlci5kaXJlY3RpdmUnO1xyXG5pbXBvcnQgeyBTaWRlbmF2TGF5b3V0TmF2aWdhdGlvbkRpcmVjdGl2ZSB9IGZyb20gJy4vZGlyZWN0aXZlcy9zaWRlbmF2LWxheW91dC1uYXZpZ2F0aW9uLmRpcmVjdGl2ZSc7XHJcbmltcG9ydCB7IFNpZGVuYXZMYXlvdXRDb21wb25lbnQgfSBmcm9tICcuL2NvbXBvbmVudHMvc2lkZW5hdi1sYXlvdXQvc2lkZW5hdi1sYXlvdXQuY29tcG9uZW50JztcclxuaW1wb3J0IHsgTGF5b3V0Q29udGFpbmVyQ29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2xheW91dC1jb250YWluZXIvbGF5b3V0LWNvbnRhaW5lci5jb21wb25lbnQnO1xyXG5pbXBvcnQgeyBTaWRlYmFyQWN0aW9uTWVudUNvbXBvbmVudCwgU2lkZWJhck1lbnVEaXJlY3RpdmUsXHJcbiAgICBTaWRlYmFyTWVudUV4cGFuZEljb25EaXJlY3RpdmUsIFNpZGViYXJNZW51VGl0bGVJY29uRGlyZWN0aXZlIH0gZnJvbSAnLi9jb21wb25lbnRzL3NpZGViYXItYWN0aW9uL3NpZGViYXItYWN0aW9uLW1lbnUuY29tcG9uZW50JztcclxuaW1wb3J0IHsgSGVhZGVyTGF5b3V0Q29tcG9uZW50IH0gZnJvbSAnLi9jb21wb25lbnRzL2hlYWRlci9oZWFkZXIuY29tcG9uZW50JztcclxuaW1wb3J0IHsgVHJhbnNsYXRlTW9kdWxlIH0gZnJvbSAnQG5neC10cmFuc2xhdGUvY29yZSc7XHJcbkBOZ01vZHVsZSh7XHJcbiAgICBpbXBvcnRzOiBbXHJcbiAgICAgICAgQ29tbW9uTW9kdWxlLFxyXG4gICAgICAgIE1hdGVyaWFsTW9kdWxlLFxyXG4gICAgICAgIFJvdXRlck1vZHVsZSxcclxuICAgICAgICBUcmFuc2xhdGVNb2R1bGUuZm9yQ2hpbGQoKVxyXG4gICAgXSxcclxuICAgIGV4cG9ydHM6IFtcclxuICAgICAgICBTaWRlbmF2TGF5b3V0SGVhZGVyRGlyZWN0aXZlLFxyXG4gICAgICAgIFNpZGVuYXZMYXlvdXRDb250ZW50RGlyZWN0aXZlLFxyXG4gICAgICAgIFNpZGVuYXZMYXlvdXROYXZpZ2F0aW9uRGlyZWN0aXZlLFxyXG4gICAgICAgIFNpZGVuYXZMYXlvdXRDb21wb25lbnQsXHJcbiAgICAgICAgTGF5b3V0Q29udGFpbmVyQ29tcG9uZW50LFxyXG4gICAgICAgIFNpZGViYXJBY3Rpb25NZW51Q29tcG9uZW50LFxyXG4gICAgICAgIFNpZGViYXJNZW51RGlyZWN0aXZlLFxyXG4gICAgICAgIFNpZGViYXJNZW51RXhwYW5kSWNvbkRpcmVjdGl2ZSxcclxuICAgICAgICBTaWRlYmFyTWVudVRpdGxlSWNvbkRpcmVjdGl2ZSxcclxuICAgICAgICBIZWFkZXJMYXlvdXRDb21wb25lbnRcclxuICAgIF0sXHJcbiAgICBkZWNsYXJhdGlvbnM6IFtcclxuICAgICAgICBTaWRlbmF2TGF5b3V0SGVhZGVyRGlyZWN0aXZlLFxyXG4gICAgICAgIFNpZGVuYXZMYXlvdXRDb250ZW50RGlyZWN0aXZlLFxyXG4gICAgICAgIFNpZGVuYXZMYXlvdXROYXZpZ2F0aW9uRGlyZWN0aXZlLFxyXG4gICAgICAgIFNpZGVuYXZMYXlvdXRDb21wb25lbnQsXHJcbiAgICAgICAgTGF5b3V0Q29udGFpbmVyQ29tcG9uZW50LFxyXG4gICAgICAgIFNpZGViYXJBY3Rpb25NZW51Q29tcG9uZW50LFxyXG4gICAgICAgIFNpZGViYXJNZW51RGlyZWN0aXZlLFxyXG4gICAgICAgIFNpZGViYXJNZW51RXhwYW5kSWNvbkRpcmVjdGl2ZSxcclxuICAgICAgICBTaWRlYmFyTWVudVRpdGxlSWNvbkRpcmVjdGl2ZSxcclxuICAgICAgICBIZWFkZXJMYXlvdXRDb21wb25lbnRcclxuICAgIF1cclxufSlcclxuZXhwb3J0IGNsYXNzIExheW91dE1vZHVsZSB7fVxyXG5leHBvcnQgeyBMYXlvdXRNb2R1bGUgYXMgU2lkZW5hdkxheW91dE1vZHVsZSB9O1xyXG4iXX0=