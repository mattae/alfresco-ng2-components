/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ViewEncapsulation, ViewChild, Output, EventEmitter } from '@angular/core';
import { LoginComponent } from './login.component';
var LoginDialogPanelComponent = /** @class */ (function () {
    function LoginDialogPanelComponent() {
        /**
         * Emitted when the login succeeds.
         */
        this.success = new EventEmitter();
    }
    /**
     * @return {?}
     */
    LoginDialogPanelComponent.prototype.submitForm = /**
     * @return {?}
     */
    function () {
        this.login.submit();
    };
    /**
     * @param {?} event
     * @return {?}
     */
    LoginDialogPanelComponent.prototype.onLoginSuccess = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.success.emit(event);
    };
    /**
     * @return {?}
     */
    LoginDialogPanelComponent.prototype.isValid = /**
     * @return {?}
     */
    function () {
        return this.login && this.login.form ? this.login.form.valid : false;
    };
    LoginDialogPanelComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-login-dialog-panel',
                    template: "<div>\r\n    <adf-login #adfLogin\r\n               class=\"adf-panel-login-dialog-component\"\r\n               [showRememberMe]=\"false\"\r\n               [showLoginActions]=\"false\"\r\n               [backgroundImageUrl]=\"''\"\r\n               (success)=\"onLoginSuccess($event)\">\r\n        <adf-login-header><ng-template></ng-template></adf-login-header>\r\n        <adf-login-footer><ng-template></ng-template></adf-login-footer>\r\n    </adf-login>\r\n</div>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    styles: [""]
                }] }
    ];
    LoginDialogPanelComponent.propDecorators = {
        success: [{ type: Output }],
        login: [{ type: ViewChild, args: ['adfLogin', { static: true },] }]
    };
    return LoginDialogPanelComponent;
}());
export { LoginDialogPanelComponent };
if (false) {
    /**
     * Emitted when the login succeeds.
     * @type {?}
     */
    LoginDialogPanelComponent.prototype.success;
    /** @type {?} */
    LoginDialogPanelComponent.prototype.login;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tZGlhbG9nLXBhbmVsLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImxvZ2luL2NvbXBvbmVudHMvbG9naW4tZGlhbG9nLXBhbmVsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQ0EsT0FBTyxFQUFFLFNBQVMsRUFBRSxpQkFBaUIsRUFBRSxTQUFTLEVBQUUsTUFBTSxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5RixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFHbkQ7SUFBQTs7OztRQVNJLFlBQU8sR0FBRyxJQUFJLFlBQVksRUFBcUIsQ0FBQztJQWdCcEQsQ0FBQzs7OztJQVhHLDhDQUFVOzs7SUFBVjtRQUNJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLENBQUM7SUFDeEIsQ0FBQzs7Ozs7SUFFRCxrREFBYzs7OztJQUFkLFVBQWUsS0FBd0I7UUFDbkMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDN0IsQ0FBQzs7OztJQUVELDJDQUFPOzs7SUFBUDtRQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDekUsQ0FBQzs7Z0JBeEJKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsd0JBQXdCO29CQUNsQyxzZUFBa0Q7b0JBRWxELGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOztpQkFDeEM7OzswQkFHSSxNQUFNO3dCQUdOLFNBQVMsU0FBQyxVQUFVLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDOztJQWN6QyxnQ0FBQztDQUFBLEFBekJELElBeUJDO1NBbkJZLHlCQUF5Qjs7Ozs7O0lBRWxDLDRDQUNnRDs7SUFFaEQsMENBQ3NCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXG5cbi8qIVxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXG4gKlxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuICpcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbiAqXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxuICovXG5cbmltcG9ydCB7IENvbXBvbmVudCwgVmlld0VuY2Fwc3VsYXRpb24sIFZpZXdDaGlsZCwgT3V0cHV0LCBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IExvZ2luQ29tcG9uZW50IH0gZnJvbSAnLi9sb2dpbi5jb21wb25lbnQnO1xuaW1wb3J0IHsgTG9naW5TdWNjZXNzRXZlbnQgfSBmcm9tICcuLi9tb2RlbHMvbG9naW4tc3VjY2Vzcy5ldmVudCc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYWRmLWxvZ2luLWRpYWxvZy1wYW5lbCcsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2xvZ2luLWRpYWxvZy1wYW5lbC5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vbG9naW4tZGlhbG9nLXBhbmVsLmNvbXBvbmVudC5zY3NzJ10sXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxufSlcbmV4cG9ydCBjbGFzcyBMb2dpbkRpYWxvZ1BhbmVsQ29tcG9uZW50IHtcbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSBsb2dpbiBzdWNjZWVkcy4gKi9cbiAgICBAT3V0cHV0KClcbiAgICBzdWNjZXNzID0gbmV3IEV2ZW50RW1pdHRlcjxMb2dpblN1Y2Nlc3NFdmVudD4oKTtcblxuICAgIEBWaWV3Q2hpbGQoJ2FkZkxvZ2luJywge3N0YXRpYzogdHJ1ZX0pXG4gICAgbG9naW46IExvZ2luQ29tcG9uZW50O1xuXG4gICAgc3VibWl0Rm9ybSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5sb2dpbi5zdWJtaXQoKTtcbiAgICB9XG5cbiAgICBvbkxvZ2luU3VjY2VzcyhldmVudDogTG9naW5TdWNjZXNzRXZlbnQpIHtcbiAgICAgICAgdGhpcy5zdWNjZXNzLmVtaXQoZXZlbnQpO1xuICAgIH1cblxuICAgIGlzVmFsaWQoKSB7XG4gICAgICAgIHJldHVybiB0aGlzLmxvZ2luICYmIHRoaXMubG9naW4uZm9ybSA/IHRoaXMubG9naW4uZm9ybS52YWxpZCA6IGZhbHNlO1xuICAgIH1cbn1cbiJdfQ==