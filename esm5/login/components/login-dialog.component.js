/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Inject, ViewEncapsulation, ViewChild } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material';
import { LoginDialogPanelComponent } from './login-dialog-panel.component';
var LoginDialogComponent = /** @class */ (function () {
    function LoginDialogComponent(data) {
        this.data = data;
        this.buttonActionName = '';
        this.buttonActionName = data.actionName ? "LOGIN.DIALOG." + data.actionName.toUpperCase() : 'LOGIN.DIALOG.CHOOSE';
    }
    /**
     * @return {?}
     */
    LoginDialogComponent.prototype.close = /**
     * @return {?}
     */
    function () {
        this.data.logged.complete();
    };
    /**
     * @return {?}
     */
    LoginDialogComponent.prototype.submitForm = /**
     * @return {?}
     */
    function () {
        this.loginPanel.submitForm();
    };
    /**
     * @param {?} event
     * @return {?}
     */
    LoginDialogComponent.prototype.onLoginSuccess = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.data.logged.next(event);
        this.close();
    };
    /**
     * @return {?}
     */
    LoginDialogComponent.prototype.isFormValid = /**
     * @return {?}
     */
    function () {
        return this.loginPanel ? this.loginPanel.isValid() : false;
    };
    LoginDialogComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-login-dialog',
                    template: "<header\r\n    mat-dialog-title\r\n    data-automation-id=\"login-dialog-title\">{{data?.title}}\r\n</header>\r\n\r\n<mat-dialog-content class=\"adf-login-dialog-content\">\r\n    <adf-login-dialog-panel #adfLoginPanel\r\n                            (success)=\"onLoginSuccess($event)\">\r\n    </adf-login-dialog-panel>\r\n</mat-dialog-content>\r\n\r\n<mat-dialog-actions align=\"end\">\r\n    <button\r\n        mat-button\r\n        (click)=\"close()\"\r\n        data-automation-id=\"login-dialog-actions-cancel\">{{ 'LOGIN.DIALOG.CANCEL' | translate }}\r\n    </button>\r\n\r\n    <button mat-button\r\n        class=\"choose-action\"\r\n        data-automation-id=\"login-dialog-actions-perform\"\r\n        [disabled]=\"!isFormValid()\"\r\n        (click)=\"submitForm()\">{{ buttonActionName | translate}}\r\n    </button>\r\n</mat-dialog-actions>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    LoginDialogComponent.ctorParameters = function () { return [
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
    ]; };
    LoginDialogComponent.propDecorators = {
        loginPanel: [{ type: ViewChild, args: ['adfLoginPanel', { static: true },] }]
    };
    return LoginDialogComponent;
}());
export { LoginDialogComponent };
if (false) {
    /** @type {?} */
    LoginDialogComponent.prototype.loginPanel;
    /** @type {?} */
    LoginDialogComponent.prototype.buttonActionName;
    /** @type {?} */
    LoginDialogComponent.prototype.data;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tZGlhbG9nLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbImxvZ2luL2NvbXBvbmVudHMvbG9naW4tZGlhbG9nLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQ0EsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQUUsaUJBQWlCLEVBQUUsU0FBUyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2hGLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUVwRCxPQUFPLEVBQUUseUJBQXlCLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUMzRTtJQWFJLDhCQUE0QyxJQUE4QjtRQUE5QixTQUFJLEdBQUosSUFBSSxDQUEwQjtRQUYxRSxxQkFBZ0IsR0FBRyxFQUFFLENBQUM7UUFHbEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLGtCQUFnQixJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBSSxDQUFDLENBQUMsQ0FBQyxxQkFBcUIsQ0FBQztJQUN0SCxDQUFDOzs7O0lBRUQsb0NBQUs7OztJQUFMO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLENBQUM7SUFDaEMsQ0FBQzs7OztJQUVELHlDQUFVOzs7SUFBVjtRQUNJLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDakMsQ0FBQzs7Ozs7SUFFRCw2Q0FBYzs7OztJQUFkLFVBQWUsS0FBVTtRQUNyQixJQUFJLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ2pCLENBQUM7Ozs7SUFFRCwwQ0FBVzs7O0lBQVg7UUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUMvRCxDQUFDOztnQkFoQ0osU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxrQkFBa0I7b0JBQzVCLHUyQkFBNEM7b0JBRTVDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOztpQkFDeEM7Ozs7Z0RBUWdCLE1BQU0sU0FBQyxlQUFlOzs7NkJBTGxDLFNBQVMsU0FBQyxlQUFlLEVBQUUsRUFBQyxNQUFNLEVBQUUsSUFBSSxFQUFDOztJQXlCOUMsMkJBQUM7Q0FBQSxBQWpDRCxJQWlDQztTQTNCWSxvQkFBb0I7OztJQUU3QiwwQ0FDc0M7O0lBRXRDLGdEQUFzQjs7SUFFVixvQ0FBOEQiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cblxuLyohXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cbiAqXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4gKlxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuICpcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXG4gKi9cblxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbmplY3QsIFZpZXdFbmNhcHN1bGF0aW9uLCBWaWV3Q2hpbGQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE1BVF9ESUFMT0dfREFUQSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcbmltcG9ydCB7IExvZ2luRGlhbG9nQ29tcG9uZW50RGF0YSB9IGZyb20gJy4vbG9naW4tZGlhbG9nLWNvbXBvbmVudC1kYXRhLmludGVyZmFjZSc7XG5pbXBvcnQgeyBMb2dpbkRpYWxvZ1BhbmVsQ29tcG9uZW50IH0gZnJvbSAnLi9sb2dpbi1kaWFsb2ctcGFuZWwuY29tcG9uZW50JztcbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYWRmLWxvZ2luLWRpYWxvZycsXG4gICAgdGVtcGxhdGVVcmw6ICcuL2xvZ2luLWRpYWxvZy5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vbG9naW4tZGlhbG9nLmNvbXBvbmVudC5zY3NzJ10sXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxufSlcbmV4cG9ydCBjbGFzcyBMb2dpbkRpYWxvZ0NvbXBvbmVudCB7XG5cbiAgICBAVmlld0NoaWxkKCdhZGZMb2dpblBhbmVsJywge3N0YXRpYzogdHJ1ZX0pXG4gICAgbG9naW5QYW5lbDogTG9naW5EaWFsb2dQYW5lbENvbXBvbmVudDtcblxuICAgIGJ1dHRvbkFjdGlvbk5hbWUgPSAnJztcblxuICAgIGNvbnN0cnVjdG9yKEBJbmplY3QoTUFUX0RJQUxPR19EQVRBKSBwdWJsaWMgZGF0YTogTG9naW5EaWFsb2dDb21wb25lbnREYXRhKSB7XG4gICAgICAgIHRoaXMuYnV0dG9uQWN0aW9uTmFtZSA9IGRhdGEuYWN0aW9uTmFtZSA/IGBMT0dJTi5ESUFMT0cuJHtkYXRhLmFjdGlvbk5hbWUudG9VcHBlckNhc2UoKX1gIDogJ0xPR0lOLkRJQUxPRy5DSE9PU0UnO1xuICAgIH1cblxuICAgIGNsb3NlKCkge1xuICAgICAgICB0aGlzLmRhdGEubG9nZ2VkLmNvbXBsZXRlKCk7XG4gICAgfVxuXG4gICAgc3VibWl0Rm9ybSgpOiB2b2lkIHtcbiAgICAgICAgdGhpcy5sb2dpblBhbmVsLnN1Ym1pdEZvcm0oKTtcbiAgICB9XG5cbiAgICBvbkxvZ2luU3VjY2VzcyhldmVudDogYW55KSB7XG4gICAgICAgIHRoaXMuZGF0YS5sb2dnZWQubmV4dChldmVudCk7XG4gICAgICAgIHRoaXMuY2xvc2UoKTtcbiAgICB9XG5cbiAgICBpc0Zvcm1WYWxpZCgpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMubG9naW5QYW5lbCA/IHRoaXMubG9naW5QYW5lbC5pc1ZhbGlkKCkgOiBmYWxzZTtcbiAgICB9XG59XG4iXX0=