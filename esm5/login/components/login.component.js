/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { AuthenticationService } from '../../services/authentication.service';
import { LogService } from '../../services/log.service';
import { TranslationService } from '../../services/translation.service';
import { UserPreferencesService } from '../../services/user-preferences.service';
import { LoginErrorEvent } from '../models/login-error.event';
import { LoginSubmitEvent } from '../models/login-submit.event';
import { LoginSuccessEvent } from '../models/login-success.event';
import { AppConfigService, AppConfigValues } from '../../app-config/app-config.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
/** @enum {number} */
var LoginSteps = {
    Landing: 0,
    Checking: 1,
    Welcome: 2,
};
LoginSteps[LoginSteps.Landing] = 'Landing';
LoginSteps[LoginSteps.Checking] = 'Checking';
LoginSteps[LoginSteps.Welcome] = 'Welcome';
/**
 * @record
 */
function ValidationMessage() { }
if (false) {
    /** @type {?} */
    ValidationMessage.prototype.value;
    /** @type {?|undefined} */
    ValidationMessage.prototype.params;
}
var LoginComponent = /** @class */ (function () {
    /**
     * Constructor
     * @param _fb
     * @param authService
     * @param translate
     */
    function LoginComponent(_fb, authService, translateService, logService, router, appConfig, userPreferences, location, route, sanitizer) {
        this._fb = _fb;
        this.authService = authService;
        this.translateService = translateService;
        this.logService = logService;
        this.router = router;
        this.appConfig = appConfig;
        this.userPreferences = userPreferences;
        this.location = location;
        this.route = route;
        this.sanitizer = sanitizer;
        this.isPasswordShow = false;
        /**
         * Should the `Remember me` checkbox be shown? When selected, this
         * option will remember the logged-in user after the browser is closed
         * to avoid logging in repeatedly.
         */
        this.showRememberMe = true;
        /**
         * Should the extra actions (`Need Help`, `Register`, etc) be shown?
         */
        this.showLoginActions = true;
        /**
         * Sets the URL of the NEED HELP link in the footer.
         */
        this.needHelpLink = '';
        /**
         * Sets the URL of the REGISTER link in the footer.
         */
        this.registerLink = '';
        /**
         * Path to a custom logo image.
         */
        this.logoImageUrl = './assets/images/alfresco-logo.svg';
        /**
         * Path to a custom background image.
         */
        this.backgroundImageUrl = './assets/images/background.svg';
        /**
         * The copyright text below the login box.
         */
        this.copyrightText = '\u00A9 2016 Alfresco Software, Inc. All Rights Reserved.';
        /**
         * Route to redirect to on successful login.
         */
        this.successRoute = null;
        /**
         * Emitted when the login is successful.
         */
        this.success = new EventEmitter();
        /**
         * Emitted when the login fails.
         */
        this.error = new EventEmitter();
        /**
         * Emitted when the login form is submitted.
         */
        this.executeSubmit = new EventEmitter();
        this.implicitFlow = false;
        this.isError = false;
        this.actualLoginStep = LoginSteps.Landing;
        this.LoginSteps = LoginSteps;
        this.rememberMe = true;
        this.minLength = 2;
        this.onDestroy$ = new Subject();
        this.initFormError();
        this.initFormFieldsMessages();
    }
    /**
     * @return {?}
     */
    LoginComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.authService.isOauth()) {
            /** @type {?} */
            var oauth = this.appConfig.get(AppConfigValues.OAUTHCONFIG, null);
            if (oauth && oauth.implicitFlow) {
                this.implicitFlow = true;
            }
        }
        if (this.authService.isEcmLoggedIn() || this.authService.isBpmLoggedIn()) {
            this.location.forward();
        }
        else {
            this.route.queryParams.subscribe((/**
             * @param {?} params
             * @return {?}
             */
            function (params) {
                /** @type {?} */
                var url = params['redirectUrl'];
                /** @type {?} */
                var provider = _this.appConfig.get(AppConfigValues.PROVIDERS);
                _this.authService.setRedirect({ provider: provider, url: url });
            }));
        }
        if (this.hasCustomFieldsValidation()) {
            this.form = this._fb.group(this.fieldsValidation);
        }
        else {
            this.initFormFieldsDefault();
            this.initFormFieldsMessagesDefault();
        }
        this.form.valueChanges
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} data
         * @return {?}
         */
        function (data) { return _this.onValueChanged(data); }));
    };
    /**
     * @return {?}
     */
    LoginComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    };
    /**
     * @return {?}
     */
    LoginComponent.prototype.submit = /**
     * @return {?}
     */
    function () {
        this.onSubmit(this.form.value);
    };
    /**
     * Method called on submit form
     * @param values
     * @param event
     */
    /**
     * Method called on submit form
     * @param {?} values
     * @return {?}
     */
    LoginComponent.prototype.onSubmit = /**
     * Method called on submit form
     * @param {?} values
     * @return {?}
     */
    function (values) {
        this.disableError();
        if (this.authService.isOauth() && !this.authService.isSSODiscoveryConfigured()) {
            this.errorMsg = 'LOGIN.MESSAGES.SSO-WRONG-CONFIGURATION';
            this.isError = true;
        }
        else {
            /** @type {?} */
            var args = new LoginSubmitEvent({
                controls: { username: this.form.controls.username }
            });
            this.executeSubmit.emit(args);
            if (args.defaultPrevented) {
                return false;
            }
            else {
                this.performLogin(values);
            }
        }
    };
    /**
     * @return {?}
     */
    LoginComponent.prototype.implicitLogin = /**
     * @return {?}
     */
    function () {
        if (this.authService.isOauth() && !this.authService.isSSODiscoveryConfigured()) {
            this.errorMsg = 'LOGIN.MESSAGES.SSO-WRONG-CONFIGURATION';
            this.isError = true;
        }
        else {
            this.authService.ssoImplicitLogin();
        }
    };
    /**
     * The method check the error in the form and push the error in the formError object
     * @param data
     */
    /**
     * The method check the error in the form and push the error in the formError object
     * @param {?} data
     * @return {?}
     */
    LoginComponent.prototype.onValueChanged = /**
     * The method check the error in the form and push the error in the formError object
     * @param {?} data
     * @return {?}
     */
    function (data) {
        this.disableError();
        for (var field in this.formError) {
            if (field) {
                this.formError[field] = '';
                /** @type {?} */
                var hasError = (this.form.controls[field].errors && data[field] !== '') ||
                    (this.form.controls[field].dirty &&
                        !this.form.controls[field].valid);
                if (hasError) {
                    for (var key in this.form.controls[field].errors) {
                        if (key) {
                            /** @type {?} */
                            var message = this._message[field][key];
                            if (message && message.value) {
                                /** @type {?} */
                                var translated = this.translateService.instant(message.value, message.params);
                                this.formError[field] += translated;
                            }
                        }
                    }
                }
            }
        }
    };
    /**
     * @private
     * @param {?} values
     * @return {?}
     */
    LoginComponent.prototype.performLogin = /**
     * @private
     * @param {?} values
     * @return {?}
     */
    function (values) {
        var _this = this;
        this.actualLoginStep = LoginSteps.Checking;
        this.authService
            .login(values.username, values.password, this.rememberMe)
            .subscribe((/**
         * @param {?} token
         * @return {?}
         */
        function (token) {
            /** @type {?} */
            var redirectUrl = _this.authService.getRedirect();
            _this.actualLoginStep = LoginSteps.Welcome;
            _this.userPreferences.setStoragePrefix(values.username);
            values.password = null;
            _this.success.emit(new LoginSuccessEvent(token, values.username, null));
            if (redirectUrl) {
                _this.authService.setRedirect(null);
                _this.router.navigateByUrl(redirectUrl);
            }
            else if (_this.successRoute) {
                _this.router.navigate([_this.successRoute]);
            }
        }), (/**
         * @param {?} err
         * @return {?}
         */
        function (err) {
            _this.actualLoginStep = LoginSteps.Landing;
            _this.displayErrorMessage(err);
            _this.isError = true;
            _this.error.emit(new LoginErrorEvent(err));
        }), (/**
         * @return {?}
         */
        function () { return _this.logService.info('Login done'); }));
    };
    /**
     * Check and display the right error message in the UI
     */
    /**
     * Check and display the right error message in the UI
     * @private
     * @param {?} err
     * @return {?}
     */
    LoginComponent.prototype.displayErrorMessage = /**
     * Check and display the right error message in the UI
     * @private
     * @param {?} err
     * @return {?}
     */
    function (err) {
        if (err.error &&
            err.error.crossDomain &&
            err.error.message.indexOf('Access-Control-Allow-Origin') !== -1) {
            this.errorMsg = err.error.message;
        }
        else if (err.status === 403 &&
            err.message.indexOf('Invalid CSRF-token') !== -1) {
            this.errorMsg = 'LOGIN.MESSAGES.LOGIN-ERROR-CSRF';
        }
        else if (err.status === 403 &&
            err.message.indexOf('The system is currently in read-only mode') !==
                -1) {
            this.errorMsg = 'LOGIN.MESSAGES.LOGIN-ECM-LICENSE';
        }
        else {
            this.errorMsg = 'LOGIN.MESSAGES.LOGIN-ERROR-CREDENTIALS';
        }
    };
    /**
     * Add a custom form error for a field
     * @param field
     * @param msg
     */
    /**
     * Add a custom form error for a field
     * @param {?} field
     * @param {?} msg
     * @return {?}
     */
    LoginComponent.prototype.addCustomFormError = /**
     * Add a custom form error for a field
     * @param {?} field
     * @param {?} msg
     * @return {?}
     */
    function (field, msg) {
        this.formError[field] += msg;
    };
    /**
     * Add a custom validation rule error for a field
     * @param field
     * @param ruleId - i.e. required | minlength | maxlength
     * @param msg
     */
    /**
     * Add a custom validation rule error for a field
     * @param {?} field
     * @param {?} ruleId - i.e. required | minlength | maxlength
     * @param {?} msg
     * @param {?=} params
     * @return {?}
     */
    LoginComponent.prototype.addCustomValidationError = /**
     * Add a custom validation rule error for a field
     * @param {?} field
     * @param {?} ruleId - i.e. required | minlength | maxlength
     * @param {?} msg
     * @param {?=} params
     * @return {?}
     */
    function (field, ruleId, msg, params) {
        this._message[field][ruleId] = {
            value: msg,
            params: params
        };
    };
    /**
     * Display and hide the password value.
     */
    /**
     * Display and hide the password value.
     * @return {?}
     */
    LoginComponent.prototype.toggleShowPassword = /**
     * Display and hide the password value.
     * @return {?}
     */
    function () {
        this.isPasswordShow = !this.isPasswordShow;
    };
    /**
     * The method return if a field is valid or not
     * @param field
     */
    /**
     * The method return if a field is valid or not
     * @param {?} field
     * @return {?}
     */
    LoginComponent.prototype.isErrorStyle = /**
     * The method return if a field is valid or not
     * @param {?} field
     * @return {?}
     */
    function (field) {
        return !field.valid && field.dirty && !field.pristine;
    };
    /**
     * Trim username
     */
    /**
     * Trim username
     * @param {?} event
     * @return {?}
     */
    LoginComponent.prototype.trimUsername = /**
     * Trim username
     * @param {?} event
     * @return {?}
     */
    function (event) {
        event.target.value = event.target.value.trim();
    };
    /**
     * @return {?}
     */
    LoginComponent.prototype.getBackgroundUrlImageUrl = /**
     * @return {?}
     */
    function () {
        return this.sanitizer.bypassSecurityTrustStyle("url(" + this.backgroundImageUrl + ")");
    };
    /**
     * Default formError values
     */
    /**
     * Default formError values
     * @private
     * @return {?}
     */
    LoginComponent.prototype.initFormError = /**
     * Default formError values
     * @private
     * @return {?}
     */
    function () {
        this.formError = {
            username: '',
            password: ''
        };
    };
    /**
     * Init form fields messages
     */
    /**
     * Init form fields messages
     * @private
     * @return {?}
     */
    LoginComponent.prototype.initFormFieldsMessages = /**
     * Init form fields messages
     * @private
     * @return {?}
     */
    function () {
        this._message = {
            username: {},
            password: {}
        };
    };
    /**
     * Default form fields messages
     */
    /**
     * Default form fields messages
     * @private
     * @return {?}
     */
    LoginComponent.prototype.initFormFieldsMessagesDefault = /**
     * Default form fields messages
     * @private
     * @return {?}
     */
    function () {
        this._message = {
            username: {
                required: {
                    value: 'LOGIN.MESSAGES.USERNAME-REQUIRED'
                },
                minLength: {
                    value: 'LOGIN.MESSAGES.USERNAME-MIN',
                    params: {
                        /**
                         * @return {?}
                         */
                        get minLength() {
                            return this.minLength;
                        }
                    }
                }
            },
            password: {
                required: {
                    value: 'LOGIN.MESSAGES.PASSWORD-REQUIRED'
                }
            }
        };
    };
    /**
     * @private
     * @return {?}
     */
    LoginComponent.prototype.initFormFieldsDefault = /**
     * @private
     * @return {?}
     */
    function () {
        this.form = this._fb.group({
            username: ['', Validators.required],
            password: ['', Validators.required]
        });
    };
    /**
     * Disable the error flag
     */
    /**
     * Disable the error flag
     * @private
     * @return {?}
     */
    LoginComponent.prototype.disableError = /**
     * Disable the error flag
     * @private
     * @return {?}
     */
    function () {
        this.isError = false;
        this.initFormError();
    };
    /**
     * @private
     * @return {?}
     */
    LoginComponent.prototype.hasCustomFieldsValidation = /**
     * @private
     * @return {?}
     */
    function () {
        return this.fieldsValidation !== undefined;
    };
    LoginComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-login',
                    template: "<div class=\"adf-login-content\" [style.background-image]=\"getBackgroundUrlImageUrl()\">\r\n    <div class=\"adf-ie11FixerParent\">\r\n        <div class=\"adf-ie11FixerChild\">\r\n\r\n            <mat-card class=\"adf-login-card-wide\">\r\n                <form id=\"adf-login-form\" [formGroup]=\"form\" (submit)=\"onSubmit(form.value)\" autocomplete=\"off\">\r\n                    <mat-card-header>\r\n                        <mat-card-title>\r\n                            <div class=\"adf-alfresco-logo\">\r\n                                <!--HEADER TEMPLATE-->\r\n                                <ng-template *ngIf=\"headerTemplate\"\r\n                                             ngFor [ngForOf]=\"[data]\"\r\n                                             [ngForTemplate]=\"headerTemplate\">\r\n                                </ng-template>\r\n                                <img *ngIf=\"!headerTemplate\" id=\"adf-login-img-logo\" class=\"adf-img-logo\" [src]=\"logoImageUrl\"\r\n                                     alt=\"{{'LOGIN.LOGO' | translate }}\">\r\n                            </div>\r\n                        </mat-card-title>\r\n                    </mat-card-header>\r\n\r\n                    <mat-card-content class=\"adf-login-controls\">\r\n\r\n                        <!--ERRORS AREA-->\r\n                        <div class=\"adf-error-container\">\r\n                            <div *ngIf=\"isError\" id=\"login-error\" data-automation-id=\"login-error\"\r\n                                 class=\"adf-error  adf-error-message\">\r\n                                <mat-icon class=\"adf-error-icon\">warning</mat-icon>\r\n                                <span class=\"adf-login-error-message\">{{errorMsg | translate }}</span>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div *ngIf=\"!implicitFlow\">\r\n\r\n                            <!--USERNAME FIELD-->\r\n                            <div class=\"adf-login__field\"\r\n                                 [ngClass]=\"{'adf-is-invalid': isErrorStyle(form.controls.username)}\">\r\n                                <mat-form-field class=\"adf-full-width\" floatPlaceholder=\"never\" color=\"primary\">\r\n                                    <input matInput placeholder=\"{{'LOGIN.LABEL.USERNAME' | translate }}\"\r\n                                           type=\"text\"\r\n                                           class=\"adf-full-width\"\r\n                                           [formControl]=\"form.controls['username']\"\r\n                                           autocapitalize=\"none\"\r\n                                           id=\"username\"\r\n                                           data-automation-id=\"username\"\r\n                                           (blur)=\"trimUsername($event)\">\r\n                                </mat-form-field>\r\n\r\n                                <span class=\"adf-login-validation\" for=\"username\" *ngIf=\"formError['username']\">\r\n                                <span id=\"username-error\" class=\"adf-login-error\" data-automation-id=\"username-error\">{{formError['username'] | translate }}</span>\r\n                            </span>\r\n                            </div>\r\n\r\n                            <!--PASSWORD FIELD-->\r\n                            <div class=\"adf-login__field\">\r\n                                <mat-form-field class=\"adf-full-width\" floatPlaceholder=\"never\" color=\"primary\">\r\n                                    <input matInput placeholder=\"{{'LOGIN.LABEL.PASSWORD' | translate }}\"\r\n                                           [type]=\"isPasswordShow ? 'text' : 'password'\"\r\n                                           [formControl]=\"form.controls['password']\"\r\n                                           id=\"password\"\r\n                                           data-automation-id=\"password\">\r\n                                    <mat-icon *ngIf=\"isPasswordShow\" matSuffix class=\"adf-login-password-icon\"\r\n                                              data-automation-id=\"hide_password\" (click)=\"toggleShowPassword()\" (keyup.enter)=\"toggleShowPassword()\">\r\n                                        visibility\r\n                                    </mat-icon>\r\n                                    <mat-icon *ngIf=\"!isPasswordShow\" matSuffix class=\"adf-login-password-icon\"\r\n                                              data-automation-id=\"show_password\" (click)=\"toggleShowPassword()\" (keyup.enter)=\"toggleShowPassword()\">\r\n                                        visibility_off\r\n                                    </mat-icon>\r\n                                </mat-form-field>\r\n                                <span class=\"adf-login-validation\" for=\"password\" *ngIf=\"formError['password']\">\r\n                                <span id=\"password-required\" class=\"adf-login-error\"\r\n                                      data-automation-id=\"password-required\">{{formError['password'] | translate }}</span>\r\n                            </span>\r\n                            </div>\r\n\r\n                            <!--CUSTOM CONTENT-->\r\n                            <ng-content></ng-content>\r\n\r\n                            <br>\r\n                            <button type=\"submit\" id=\"login-button\"\r\n                                    class=\"adf-login-button\"\r\n                                    mat-raised-button color=\"primary\"\r\n                                    [class.adf-isChecking]=\"actualLoginStep === LoginSteps.Checking\"\r\n                                    [class.adf-isWelcome]=\"actualLoginStep === LoginSteps.Welcome\"\r\n                                    data-automation-id=\"login-button\" [disabled]=\"!form.valid\">\r\n\r\n                                <span *ngIf=\"actualLoginStep === LoginSteps.Landing\" class=\"adf-login-button-label\">{{ 'LOGIN.BUTTON.LOGIN' | translate }}</span>\r\n\r\n                                <div *ngIf=\"actualLoginStep === LoginSteps.Checking\"\r\n                                     class=\"adf-interactive-login-label\">\r\n                                    <span\r\n                                        class=\"adf-login-button-label\">{{ 'LOGIN.BUTTON.CHECKING' | translate }}</span>\r\n                                    <div class=\"adf-login-spinner-container\">\r\n                                        <mat-spinner id=\"checking-spinner\" class=\"adf-login-checking-spinner\"\r\n                                                     [diameter]=\"25\"></mat-spinner>\r\n                                    </div>\r\n                                </div>\r\n\r\n\r\n                                <div *ngIf=\"actualLoginStep === LoginSteps.Welcome\" class=\"adf-interactive-login-label\">\r\n                                    <span class=\"adf-login-button-label\">{{ 'LOGIN.BUTTON.WELCOME' | translate }}</span>\r\n                                    <mat-icon class=\"adf-welcome-icon\">done</mat-icon>\r\n                                </div>\r\n\r\n                            </button>\r\n                            <div *ngIf=\"showRememberMe\" class=\"adf-login__remember-me\">\r\n                                <mat-checkbox id=\"adf-login-remember\" color=\"primary\" class=\"adf-login-remember-me\"\r\n                                              [checked]=\"rememberMe\"\r\n                                              (change)=\"rememberMe = !rememberMe\">{{ 'LOGIN.LABEL.REMEMBER' | translate }}\r\n                                </mat-checkbox>\r\n                            </div>\r\n                        </div>\r\n\r\n                        <div *ngIf=\"implicitFlow\">\r\n                            <button type=\"button\" (click)=\"implicitLogin()\" id=\"login-button-sso\"\r\n                                    class=\"adf-login-button\"\r\n                                    mat-raised-button color=\"primary\"\r\n                                    data-automation-id=\"login-button-sso\">\r\n                                <span  class=\"adf-login-button-label\">{{ 'LOGIN.BUTTON.SSO' | translate }}</span>\r\n                            </button>\r\n                        </div>\r\n\r\n                    </mat-card-content>\r\n\r\n                    <mat-card-actions *ngIf=\"footerTemplate || showLoginActions\">\r\n\r\n                        <div class=\"adf-login-action-container\">\r\n                            <!--FOOTER TEMPLATE-->\r\n                            <ng-template *ngIf=\"footerTemplate\"\r\n                                         ngFor [ngForOf]=\"[data]\"\r\n                                         [ngForTemplate]=\"footerTemplate\">\r\n                            </ng-template>\r\n                            <div class=\"adf-login-action\" *ngIf=\"!footerTemplate && showLoginActions\">\r\n                                <div id=\"adf-login-action-left\" class=\"adf-login-action-left\">\r\n                                    <a href=\"{{needHelpLink}}\">{{'LOGIN.ACTION.HELP' | translate }}</a>\r\n                                </div>\r\n                                <div id=\"adf-login-action-right\" class=\"adf-login-action-right\">\r\n                                    <a href=\"{{registerLink}}\">{{'LOGIN.ACTION.REGISTER' | translate }}</a>\r\n                                </div>\r\n                            </div>\r\n                        </div>\r\n                    </mat-card-actions>\r\n\r\n                </form>\r\n            </mat-card>\r\n\r\n            <div class=\"adf-copyright\" data-automation-id=\"login-copyright\">\r\n                {{ copyrightText }}\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    host: {
                        class: 'adf-login'
                    },
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    LoginComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: AuthenticationService },
        { type: TranslationService },
        { type: LogService },
        { type: Router },
        { type: AppConfigService },
        { type: UserPreferencesService },
        { type: Location },
        { type: ActivatedRoute },
        { type: DomSanitizer }
    ]; };
    LoginComponent.propDecorators = {
        showRememberMe: [{ type: Input }],
        showLoginActions: [{ type: Input }],
        needHelpLink: [{ type: Input }],
        registerLink: [{ type: Input }],
        logoImageUrl: [{ type: Input }],
        backgroundImageUrl: [{ type: Input }],
        copyrightText: [{ type: Input }],
        fieldsValidation: [{ type: Input }],
        successRoute: [{ type: Input }],
        success: [{ type: Output }],
        error: [{ type: Output }],
        executeSubmit: [{ type: Output }]
    };
    return LoginComponent;
}());
export { LoginComponent };
if (false) {
    /** @type {?} */
    LoginComponent.prototype.isPasswordShow;
    /**
     * Should the `Remember me` checkbox be shown? When selected, this
     * option will remember the logged-in user after the browser is closed
     * to avoid logging in repeatedly.
     * @type {?}
     */
    LoginComponent.prototype.showRememberMe;
    /**
     * Should the extra actions (`Need Help`, `Register`, etc) be shown?
     * @type {?}
     */
    LoginComponent.prototype.showLoginActions;
    /**
     * Sets the URL of the NEED HELP link in the footer.
     * @type {?}
     */
    LoginComponent.prototype.needHelpLink;
    /**
     * Sets the URL of the REGISTER link in the footer.
     * @type {?}
     */
    LoginComponent.prototype.registerLink;
    /**
     * Path to a custom logo image.
     * @type {?}
     */
    LoginComponent.prototype.logoImageUrl;
    /**
     * Path to a custom background image.
     * @type {?}
     */
    LoginComponent.prototype.backgroundImageUrl;
    /**
     * The copyright text below the login box.
     * @type {?}
     */
    LoginComponent.prototype.copyrightText;
    /**
     * Custom validation rules for the login form.
     * @type {?}
     */
    LoginComponent.prototype.fieldsValidation;
    /**
     * Route to redirect to on successful login.
     * @type {?}
     */
    LoginComponent.prototype.successRoute;
    /**
     * Emitted when the login is successful.
     * @type {?}
     */
    LoginComponent.prototype.success;
    /**
     * Emitted when the login fails.
     * @type {?}
     */
    LoginComponent.prototype.error;
    /**
     * Emitted when the login form is submitted.
     * @type {?}
     */
    LoginComponent.prototype.executeSubmit;
    /** @type {?} */
    LoginComponent.prototype.implicitFlow;
    /** @type {?} */
    LoginComponent.prototype.form;
    /** @type {?} */
    LoginComponent.prototype.isError;
    /** @type {?} */
    LoginComponent.prototype.errorMsg;
    /** @type {?} */
    LoginComponent.prototype.actualLoginStep;
    /** @type {?} */
    LoginComponent.prototype.LoginSteps;
    /** @type {?} */
    LoginComponent.prototype.rememberMe;
    /** @type {?} */
    LoginComponent.prototype.formError;
    /** @type {?} */
    LoginComponent.prototype.minLength;
    /** @type {?} */
    LoginComponent.prototype.footerTemplate;
    /** @type {?} */
    LoginComponent.prototype.headerTemplate;
    /** @type {?} */
    LoginComponent.prototype.data;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype._message;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.onDestroy$;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype._fb;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.authService;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.translateService;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.logService;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.router;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.appConfig;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.userPreferences;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.location;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.route;
    /**
     * @type {?}
     * @private
     */
    LoginComponent.prototype.sanitizer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsibG9naW4vY29tcG9uZW50cy9sb2dpbi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUNILFNBQVMsRUFBRSxZQUFZLEVBQ3ZCLEtBQUssRUFBVSxNQUFNLEVBQWUsaUJBQWlCLEVBQ3hELE1BQU0sZUFBZSxDQUFDO0FBQ3ZCLE9BQU8sRUFBbUIsV0FBVyxFQUFhLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ3JGLE9BQU8sRUFBRSxNQUFNLEVBQUUsY0FBYyxFQUFVLE1BQU0saUJBQWlCLENBQUM7QUFDakUsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxxQkFBcUIsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBQzlFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUN4RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxvQ0FBb0MsQ0FBQztBQUN4RSxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSx5Q0FBeUMsQ0FBQztBQUVqRixPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sNkJBQTZCLENBQUM7QUFDOUQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDaEUsT0FBTyxFQUFFLGlCQUFpQixFQUFFLE1BQU0sK0JBQStCLENBQUM7QUFDbEUsT0FBTyxFQUNILGdCQUFnQixFQUNoQixlQUFlLEVBQ2xCLE1BQU0scUNBQXFDLENBQUM7QUFFN0MsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7SUFHdkMsVUFBVztJQUNYLFdBQVk7SUFDWixVQUFXOzs7Ozs7OztBQUdmLGdDQUdDOzs7SUFGRyxrQ0FBYzs7SUFDZCxtQ0FBYTs7QUFHakI7SUFpRkk7Ozs7O09BS0c7SUFDSCx3QkFDWSxHQUFnQixFQUNoQixXQUFrQyxFQUNsQyxnQkFBb0MsRUFDcEMsVUFBc0IsRUFDdEIsTUFBYyxFQUNkLFNBQTJCLEVBQzNCLGVBQXVDLEVBQ3ZDLFFBQWtCLEVBQ2xCLEtBQXFCLEVBQ3JCLFNBQXVCO1FBVHZCLFFBQUcsR0FBSCxHQUFHLENBQWE7UUFDaEIsZ0JBQVcsR0FBWCxXQUFXLENBQXVCO1FBQ2xDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBb0I7UUFDcEMsZUFBVSxHQUFWLFVBQVUsQ0FBWTtRQUN0QixXQUFNLEdBQU4sTUFBTSxDQUFRO1FBQ2QsY0FBUyxHQUFULFNBQVMsQ0FBa0I7UUFDM0Isb0JBQWUsR0FBZixlQUFlLENBQXdCO1FBQ3ZDLGFBQVEsR0FBUixRQUFRLENBQVU7UUFDbEIsVUFBSyxHQUFMLEtBQUssQ0FBZ0I7UUFDckIsY0FBUyxHQUFULFNBQVMsQ0FBYztRQXZGbkMsbUJBQWMsR0FBWSxLQUFLLENBQUM7Ozs7OztRQVFoQyxtQkFBYyxHQUFZLElBQUksQ0FBQzs7OztRQUkvQixxQkFBZ0IsR0FBWSxJQUFJLENBQUM7Ozs7UUFJakMsaUJBQVksR0FBVyxFQUFFLENBQUM7Ozs7UUFJMUIsaUJBQVksR0FBVyxFQUFFLENBQUM7Ozs7UUFJMUIsaUJBQVksR0FBVyxtQ0FBbUMsQ0FBQzs7OztRQUkzRCx1QkFBa0IsR0FBVyxnQ0FBZ0MsQ0FBQzs7OztRQUk5RCxrQkFBYSxHQUFXLDBEQUEwRCxDQUFDOzs7O1FBUW5GLGlCQUFZLEdBQVcsSUFBSSxDQUFDOzs7O1FBSTVCLFlBQU8sR0FBRyxJQUFJLFlBQVksRUFBcUIsQ0FBQzs7OztRQUloRCxVQUFLLEdBQUcsSUFBSSxZQUFZLEVBQW1CLENBQUM7Ozs7UUFJNUMsa0JBQWEsR0FBRyxJQUFJLFlBQVksRUFBb0IsQ0FBQztRQUVyRCxpQkFBWSxHQUFZLEtBQUssQ0FBQztRQUc5QixZQUFPLEdBQVksS0FBSyxDQUFDO1FBRXpCLG9CQUFlLEdBQVEsVUFBVSxDQUFDLE9BQU8sQ0FBQztRQUMxQyxlQUFVLEdBQVEsVUFBVSxDQUFDO1FBQzdCLGVBQVUsR0FBWSxJQUFJLENBQUM7UUFFM0IsY0FBUyxHQUFXLENBQUMsQ0FBQztRQU1kLGVBQVUsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDO1FBb0J4QyxJQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDckIsSUFBSSxDQUFDLHNCQUFzQixFQUFFLENBQUM7SUFDbEMsQ0FBQzs7OztJQUVELGlDQUFROzs7SUFBUjtRQUFBLGlCQTRCQztRQTNCRyxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLEVBQUU7O2dCQUN0QixLQUFLLEdBQXFCLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFtQixlQUFlLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQztZQUN2RyxJQUFJLEtBQUssSUFBSSxLQUFLLENBQUMsWUFBWSxFQUFFO2dCQUM3QixJQUFJLENBQUMsWUFBWSxHQUFHLElBQUksQ0FBQzthQUM1QjtTQUNKO1FBRUQsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLGFBQWEsRUFBRSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsYUFBYSxFQUFFLEVBQUU7WUFDdEUsSUFBSSxDQUFDLFFBQVEsQ0FBQyxPQUFPLEVBQUUsQ0FBQztTQUMzQjthQUFNO1lBQ0gsSUFBSSxDQUFDLEtBQUssQ0FBQyxXQUFXLENBQUMsU0FBUzs7OztZQUFDLFVBQUMsTUFBYzs7b0JBQ3RDLEdBQUcsR0FBRyxNQUFNLENBQUMsYUFBYSxDQUFDOztvQkFDM0IsUUFBUSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFTLGVBQWUsQ0FBQyxTQUFTLENBQUM7Z0JBRXRFLEtBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLEVBQUUsUUFBUSxVQUFBLEVBQUUsR0FBRyxLQUFBLEVBQUUsQ0FBQyxDQUFDO1lBQ2xELENBQUMsRUFBQyxDQUFDO1NBQ1I7UUFFRCxJQUFJLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxFQUFFO1lBQ2xDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7U0FDckQ7YUFBTTtZQUNILElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1lBQzdCLElBQUksQ0FBQyw2QkFBNkIsRUFBRSxDQUFDO1NBQ3hDO1FBQ0QsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZO2FBQ2pCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2hDLFNBQVM7Ozs7UUFBQyxVQUFBLElBQUksSUFBSSxPQUFBLEtBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLEVBQXpCLENBQXlCLEVBQUMsQ0FBQztJQUN0RCxDQUFDOzs7O0lBRUQsb0NBQVc7OztJQUFYO1FBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDOzs7O0lBRUQsK0JBQU07OztJQUFOO1FBQ0ksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCxpQ0FBUTs7Ozs7SUFBUixVQUFTLE1BQVc7UUFDaEIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBRXBCLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsd0JBQXdCLEVBQUUsRUFBRTtZQUM1RSxJQUFJLENBQUMsUUFBUSxHQUFHLHdDQUF3QyxDQUFDO1lBQ3pELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1NBQ3ZCO2FBQU07O2dCQUNHLElBQUksR0FBRyxJQUFJLGdCQUFnQixDQUFDO2dCQUM5QixRQUFRLEVBQUUsRUFBRSxRQUFRLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxFQUFFO2FBQ3RELENBQUM7WUFDRixJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUU5QixJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtnQkFDdkIsT0FBTyxLQUFLLENBQUM7YUFDaEI7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUM3QjtTQUNKO0lBQ0wsQ0FBQzs7OztJQUVELHNDQUFhOzs7SUFBYjtRQUNJLElBQUksSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsd0JBQXdCLEVBQUUsRUFBRTtZQUM1RSxJQUFJLENBQUMsUUFBUSxHQUFHLHdDQUF3QyxDQUFDO1lBQ3pELElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1NBQ3ZCO2FBQU07WUFDSCxJQUFJLENBQUMsV0FBVyxDQUFDLGdCQUFnQixFQUFFLENBQUM7U0FDdkM7SUFDTCxDQUFDO0lBRUQ7OztPQUdHOzs7Ozs7SUFDSCx1Q0FBYzs7Ozs7SUFBZCxVQUFlLElBQVM7UUFDcEIsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3BCLEtBQUssSUFBTSxLQUFLLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQyxJQUFJLEtBQUssRUFBRTtnQkFDUCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxHQUFHLEVBQUUsQ0FBQzs7b0JBQ3JCLFFBQVEsR0FDVixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO29CQUN4RCxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLEtBQUs7d0JBQzVCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsS0FBSyxDQUFDO2dCQUN6QyxJQUFJLFFBQVEsRUFBRTtvQkFDVixLQUFLLElBQU0sR0FBRyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLE1BQU0sRUFBRTt3QkFDaEQsSUFBSSxHQUFHLEVBQUU7O2dDQUNDLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsQ0FBQzs0QkFDekMsSUFBSSxPQUFPLElBQUksT0FBTyxDQUFDLEtBQUssRUFBRTs7b0NBQ3BCLFVBQVUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsT0FBTyxDQUFDLE1BQU0sQ0FBQztnQ0FDL0UsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxVQUFVLENBQUM7NkJBQ3ZDO3lCQUNKO3FCQUNKO2lCQUNKO2FBQ0o7U0FDSjtJQUNMLENBQUM7Ozs7OztJQUVPLHFDQUFZOzs7OztJQUFwQixVQUFxQixNQUFXO1FBQWhDLGlCQThCQztRQTdCRyxJQUFJLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQyxRQUFRLENBQUM7UUFDM0MsSUFBSSxDQUFDLFdBQVc7YUFDWCxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxNQUFNLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUM7YUFDeEQsU0FBUzs7OztRQUNOLFVBQUMsS0FBVTs7Z0JBQ0QsV0FBVyxHQUFHLEtBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFO1lBRWxELEtBQUksQ0FBQyxlQUFlLEdBQUcsVUFBVSxDQUFDLE9BQU8sQ0FBQztZQUMxQyxLQUFJLENBQUMsZUFBZSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQztZQUN2RCxNQUFNLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQztZQUN2QixLQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FDYixJQUFJLGlCQUFpQixDQUFDLEtBQUssRUFBRSxNQUFNLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxDQUN0RCxDQUFDO1lBRUYsSUFBSSxXQUFXLEVBQUU7Z0JBQ2IsS0FBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBQ25DLEtBQUksQ0FBQyxNQUFNLENBQUMsYUFBYSxDQUFDLFdBQVcsQ0FBQyxDQUFDO2FBQzFDO2lCQUFNLElBQUksS0FBSSxDQUFDLFlBQVksRUFBRTtnQkFDMUIsS0FBSSxDQUFDLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxLQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQzthQUM3QztRQUNMLENBQUM7Ozs7UUFDRCxVQUFDLEdBQVE7WUFDTCxLQUFJLENBQUMsZUFBZSxHQUFHLFVBQVUsQ0FBQyxPQUFPLENBQUM7WUFDMUMsS0FBSSxDQUFDLG1CQUFtQixDQUFDLEdBQUcsQ0FBQyxDQUFDO1lBQzlCLEtBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxDQUFDO1lBQ3BCLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksZUFBZSxDQUFDLEdBQUcsQ0FBQyxDQUFDLENBQUM7UUFDOUMsQ0FBQzs7O1FBQ0QsY0FBTSxPQUFBLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxFQUFsQyxDQUFrQyxFQUMzQyxDQUFDO0lBQ1YsQ0FBQztJQUVEOztPQUVHOzs7Ozs7O0lBQ0ssNENBQW1COzs7Ozs7SUFBM0IsVUFBNEIsR0FBUTtRQUNoQyxJQUNJLEdBQUcsQ0FBQyxLQUFLO1lBQ1QsR0FBRyxDQUFDLEtBQUssQ0FBQyxXQUFXO1lBQ3JCLEdBQUcsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyw2QkFBNkIsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUNqRTtZQUNFLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUM7U0FDckM7YUFBTSxJQUNILEdBQUcsQ0FBQyxNQUFNLEtBQUssR0FBRztZQUNsQixHQUFHLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxvQkFBb0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxFQUNsRDtZQUNFLElBQUksQ0FBQyxRQUFRLEdBQUcsaUNBQWlDLENBQUM7U0FDckQ7YUFBTSxJQUNILEdBQUcsQ0FBQyxNQUFNLEtBQUssR0FBRztZQUNsQixHQUFHLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQywyQ0FBMkMsQ0FBQztnQkFDaEUsQ0FBQyxDQUFDLEVBQ0o7WUFDRSxJQUFJLENBQUMsUUFBUSxHQUFHLGtDQUFrQyxDQUFDO1NBQ3REO2FBQU07WUFDSCxJQUFJLENBQUMsUUFBUSxHQUFHLHdDQUF3QyxDQUFDO1NBQzVEO0lBQ0wsQ0FBQztJQUVEOzs7O09BSUc7Ozs7Ozs7SUFDSSwyQ0FBa0I7Ozs7OztJQUF6QixVQUEwQixLQUFhLEVBQUUsR0FBVztRQUNoRCxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxJQUFJLEdBQUcsQ0FBQztJQUNqQyxDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7OztJQUNILGlEQUF3Qjs7Ozs7Ozs7SUFBeEIsVUFDSSxLQUFhLEVBQ2IsTUFBYyxFQUNkLEdBQVcsRUFDWCxNQUFZO1FBRVosSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsR0FBRztZQUMzQixLQUFLLEVBQUUsR0FBRztZQUNWLE1BQU0sUUFBQTtTQUNULENBQUM7SUFDTixDQUFDO0lBRUQ7O09BRUc7Ozs7O0lBQ0gsMkNBQWtCOzs7O0lBQWxCO1FBQ0ksSUFBSSxDQUFDLGNBQWMsR0FBRyxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUM7SUFDL0MsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7O0lBQ0gscUNBQVk7Ozs7O0lBQVosVUFBYSxLQUFzQjtRQUMvQixPQUFPLENBQUMsS0FBSyxDQUFDLEtBQUssSUFBSSxLQUFLLENBQUMsS0FBSyxJQUFJLENBQUMsS0FBSyxDQUFDLFFBQVEsQ0FBQztJQUMxRCxDQUFDO0lBRUQ7O09BRUc7Ozs7OztJQUNILHFDQUFZOzs7OztJQUFaLFVBQWEsS0FBVTtRQUNuQixLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNuRCxDQUFDOzs7O0lBRUQsaURBQXdCOzs7SUFBeEI7UUFDSSxPQUFRLElBQUksQ0FBQyxTQUFTLENBQUMsd0JBQXdCLENBQUMsU0FBTyxJQUFJLENBQUMsa0JBQWtCLE1BQUcsQ0FBQyxDQUFDO0lBQ3ZGLENBQUM7SUFFRDs7T0FFRzs7Ozs7O0lBQ0ssc0NBQWE7Ozs7O0lBQXJCO1FBQ0ksSUFBSSxDQUFDLFNBQVMsR0FBRztZQUNiLFFBQVEsRUFBRSxFQUFFO1lBQ1osUUFBUSxFQUFFLEVBQUU7U0FDZixDQUFDO0lBQ04sQ0FBQztJQUVEOztPQUVHOzs7Ozs7SUFDSywrQ0FBc0I7Ozs7O0lBQTlCO1FBQ0ksSUFBSSxDQUFDLFFBQVEsR0FBRztZQUNaLFFBQVEsRUFBRSxFQUFFO1lBQ1osUUFBUSxFQUFFLEVBQUU7U0FDZixDQUFDO0lBQ04sQ0FBQztJQUVEOztPQUVHOzs7Ozs7SUFDSyxzREFBNkI7Ozs7O0lBQXJDO1FBQ0ksSUFBSSxDQUFDLFFBQVEsR0FBRztZQUNaLFFBQVEsRUFBRTtnQkFDTixRQUFRLEVBQUU7b0JBQ04sS0FBSyxFQUFFLGtDQUFrQztpQkFDNUM7Z0JBQ0QsU0FBUyxFQUFFO29CQUNQLEtBQUssRUFBRSw2QkFBNkI7b0JBQ3BDLE1BQU0sRUFBRTs7Ozt3QkFDSixJQUFJLFNBQVM7NEJBQ1QsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO3dCQUMxQixDQUFDO3FCQUNKO2lCQUNKO2FBRUo7WUFDRCxRQUFRLEVBQUU7Z0JBQ04sUUFBUSxFQUFFO29CQUNOLEtBQUssRUFBRSxrQ0FBa0M7aUJBQzVDO2FBQ0o7U0FDSixDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFFTyw4Q0FBcUI7Ozs7SUFBN0I7UUFDSSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDO1lBQ3ZCLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQ25DLFFBQVEsRUFBRSxDQUFDLEVBQUUsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1NBQ3RDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7T0FFRzs7Ozs7O0lBQ0sscUNBQVk7Ozs7O0lBQXBCO1FBQ0ksSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUM7UUFDckIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ3pCLENBQUM7Ozs7O0lBRU8sa0RBQXlCOzs7O0lBQWpDO1FBQ0ksT0FBTyxJQUFJLENBQUMsZ0JBQWdCLEtBQUssU0FBUyxDQUFDO0lBQy9DLENBQUM7O2dCQTNYSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLFdBQVc7b0JBQ3JCLDBxVEFBcUM7b0JBRXJDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJO29CQUNyQyxJQUFJLEVBQUU7d0JBQ0YsS0FBSyxFQUFFLFdBQVc7cUJBQ3JCOztpQkFDSjs7OztnQkF2Q3lCLFdBQVc7Z0JBRzVCLHFCQUFxQjtnQkFFckIsa0JBQWtCO2dCQURsQixVQUFVO2dCQUhWLE1BQU07Z0JBV1gsZ0JBQWdCO2dCQU5YLHNCQUFzQjtnQkFKdEIsUUFBUTtnQkFEQSxjQUFjO2dCQWV0QixZQUFZOzs7aUNBZ0NoQixLQUFLO21DQUlMLEtBQUs7K0JBSUwsS0FBSzsrQkFJTCxLQUFLOytCQUlMLEtBQUs7cUNBSUwsS0FBSztnQ0FJTCxLQUFLO21DQUlMLEtBQUs7K0JBSUwsS0FBSzswQkFJTCxNQUFNO3dCQUlOLE1BQU07Z0NBSU4sTUFBTTs7SUErVFgscUJBQUM7Q0FBQSxBQTVYRCxJQTRYQztTQW5YWSxjQUFjOzs7SUFDdkIsd0NBQWdDOzs7Ozs7O0lBT2hDLHdDQUMrQjs7Ozs7SUFHL0IsMENBQ2lDOzs7OztJQUdqQyxzQ0FDMEI7Ozs7O0lBRzFCLHNDQUMwQjs7Ozs7SUFHMUIsc0NBQzJEOzs7OztJQUczRCw0Q0FDOEQ7Ozs7O0lBRzlELHVDQUNtRjs7Ozs7SUFHbkYsMENBQ3NCOzs7OztJQUd0QixzQ0FDNEI7Ozs7O0lBRzVCLGlDQUNnRDs7Ozs7SUFHaEQsK0JBQzRDOzs7OztJQUc1Qyx1Q0FDcUQ7O0lBRXJELHNDQUE4Qjs7SUFFOUIsOEJBQWdCOztJQUNoQixpQ0FBeUI7O0lBQ3pCLGtDQUFpQjs7SUFDakIseUNBQTBDOztJQUMxQyxvQ0FBNkI7O0lBQzdCLG9DQUEyQjs7SUFDM0IsbUNBQW9DOztJQUNwQyxtQ0FBc0I7O0lBQ3RCLHdDQUFpQzs7SUFDakMsd0NBQWlDOztJQUNqQyw4QkFBVTs7Ozs7SUFFVixrQ0FBd0U7Ozs7O0lBQ3hFLG9DQUE0Qzs7Ozs7SUFTeEMsNkJBQXdCOzs7OztJQUN4QixxQ0FBMEM7Ozs7O0lBQzFDLDBDQUE0Qzs7Ozs7SUFDNUMsb0NBQThCOzs7OztJQUM5QixnQ0FBc0I7Ozs7O0lBQ3RCLG1DQUFtQzs7Ozs7SUFDbkMseUNBQStDOzs7OztJQUMvQyxrQ0FBMEI7Ozs7O0lBQzFCLCtCQUE2Qjs7Ozs7SUFDN0IsbUNBQStCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7XHJcbiAgICBDb21wb25lbnQsIEV2ZW50RW1pdHRlcixcclxuICAgIElucHV0LCBPbkluaXQsIE91dHB1dCwgVGVtcGxhdGVSZWYsIFZpZXdFbmNhcHN1bGF0aW9uLCBPbkRlc3Ryb3lcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQWJzdHJhY3RDb250cm9sLCBGb3JtQnVpbGRlciwgRm9ybUdyb3VwLCBWYWxpZGF0b3JzIH0gZnJvbSAnQGFuZ3VsYXIvZm9ybXMnO1xyXG5pbXBvcnQgeyBSb3V0ZXIsIEFjdGl2YXRlZFJvdXRlLCBQYXJhbXMgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBMb2NhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IEF1dGhlbnRpY2F0aW9uU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2F1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMb2dTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbG9nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGlvblNlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy90cmFuc2xhdGlvbi5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXNlclByZWZlcmVuY2VzU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL3VzZXItcHJlZmVyZW5jZXMuc2VydmljZSc7XHJcblxyXG5pbXBvcnQgeyBMb2dpbkVycm9yRXZlbnQgfSBmcm9tICcuLi9tb2RlbHMvbG9naW4tZXJyb3IuZXZlbnQnO1xyXG5pbXBvcnQgeyBMb2dpblN1Ym1pdEV2ZW50IH0gZnJvbSAnLi4vbW9kZWxzL2xvZ2luLXN1Ym1pdC5ldmVudCc7XHJcbmltcG9ydCB7IExvZ2luU3VjY2Vzc0V2ZW50IH0gZnJvbSAnLi4vbW9kZWxzL2xvZ2luLXN1Y2Nlc3MuZXZlbnQnO1xyXG5pbXBvcnQge1xyXG4gICAgQXBwQ29uZmlnU2VydmljZSxcclxuICAgIEFwcENvbmZpZ1ZhbHVlc1xyXG59IGZyb20gJy4uLy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgT2F1dGhDb25maWdNb2RlbCB9IGZyb20gJy4uLy4uL21vZGVscy9vYXV0aC1jb25maWcubW9kZWwnO1xyXG5pbXBvcnQgeyBEb21TYW5pdGl6ZXIgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5lbnVtIExvZ2luU3RlcHMge1xyXG4gICAgTGFuZGluZyA9IDAsXHJcbiAgICBDaGVja2luZyA9IDEsXHJcbiAgICBXZWxjb21lID0gMlxyXG59XHJcblxyXG5pbnRlcmZhY2UgVmFsaWRhdGlvbk1lc3NhZ2Uge1xyXG4gICAgdmFsdWU6IHN0cmluZztcclxuICAgIHBhcmFtcz86IGFueTtcclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1sb2dpbicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vbG9naW4uY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vbG9naW4uY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmUsXHJcbiAgICBob3N0OiB7XHJcbiAgICAgICAgY2xhc3M6ICdhZGYtbG9naW4nXHJcbiAgICB9XHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBMb2dpbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95IHtcclxuICAgIGlzUGFzc3dvcmRTaG93OiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTaG91bGQgdGhlIGBSZW1lbWJlciBtZWAgY2hlY2tib3ggYmUgc2hvd24/IFdoZW4gc2VsZWN0ZWQsIHRoaXNcclxuICAgICAqIG9wdGlvbiB3aWxsIHJlbWVtYmVyIHRoZSBsb2dnZWQtaW4gdXNlciBhZnRlciB0aGUgYnJvd3NlciBpcyBjbG9zZWRcclxuICAgICAqIHRvIGF2b2lkIGxvZ2dpbmcgaW4gcmVwZWF0ZWRseS5cclxuICAgICAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHNob3dSZW1lbWJlck1lOiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgICAvKiogU2hvdWxkIHRoZSBleHRyYSBhY3Rpb25zIChgTmVlZCBIZWxwYCwgYFJlZ2lzdGVyYCwgZXRjKSBiZSBzaG93bj8gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzaG93TG9naW5BY3Rpb25zOiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgICAvKiogU2V0cyB0aGUgVVJMIG9mIHRoZSBORUVEIEhFTFAgbGluayBpbiB0aGUgZm9vdGVyLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIG5lZWRIZWxwTGluazogc3RyaW5nID0gJyc7XHJcblxyXG4gICAgLyoqIFNldHMgdGhlIFVSTCBvZiB0aGUgUkVHSVNURVIgbGluayBpbiB0aGUgZm9vdGVyLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHJlZ2lzdGVyTGluazogc3RyaW5nID0gJyc7XHJcblxyXG4gICAgLyoqIFBhdGggdG8gYSBjdXN0b20gbG9nbyBpbWFnZS4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBsb2dvSW1hZ2VVcmw6IHN0cmluZyA9ICcuL2Fzc2V0cy9pbWFnZXMvYWxmcmVzY28tbG9nby5zdmcnO1xyXG5cclxuICAgIC8qKiBQYXRoIHRvIGEgY3VzdG9tIGJhY2tncm91bmQgaW1hZ2UuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgYmFja2dyb3VuZEltYWdlVXJsOiBzdHJpbmcgPSAnLi9hc3NldHMvaW1hZ2VzL2JhY2tncm91bmQuc3ZnJztcclxuXHJcbiAgICAvKiogVGhlIGNvcHlyaWdodCB0ZXh0IGJlbG93IHRoZSBsb2dpbiBib3guICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgY29weXJpZ2h0VGV4dDogc3RyaW5nID0gJ1xcdTAwQTkgMjAxNiBBbGZyZXNjbyBTb2Z0d2FyZSwgSW5jLiBBbGwgUmlnaHRzIFJlc2VydmVkLic7XHJcblxyXG4gICAgLyoqIEN1c3RvbSB2YWxpZGF0aW9uIHJ1bGVzIGZvciB0aGUgbG9naW4gZm9ybS4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBmaWVsZHNWYWxpZGF0aW9uOiBhbnk7XHJcblxyXG4gICAgLyoqIFJvdXRlIHRvIHJlZGlyZWN0IHRvIG9uIHN1Y2Nlc3NmdWwgbG9naW4uICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgc3VjY2Vzc1JvdXRlOiBzdHJpbmcgPSBudWxsO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIGxvZ2luIGlzIHN1Y2Nlc3NmdWwuICovXHJcbiAgICBAT3V0cHV0KClcclxuICAgIHN1Y2Nlc3MgPSBuZXcgRXZlbnRFbWl0dGVyPExvZ2luU3VjY2Vzc0V2ZW50PigpO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIGxvZ2luIGZhaWxzLiAqL1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICBlcnJvciA9IG5ldyBFdmVudEVtaXR0ZXI8TG9naW5FcnJvckV2ZW50PigpO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIGxvZ2luIGZvcm0gaXMgc3VibWl0dGVkLiAqL1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICBleGVjdXRlU3VibWl0ID0gbmV3IEV2ZW50RW1pdHRlcjxMb2dpblN1Ym1pdEV2ZW50PigpO1xyXG5cclxuICAgIGltcGxpY2l0RmxvdzogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIGZvcm06IEZvcm1Hcm91cDtcclxuICAgIGlzRXJyb3I6IGJvb2xlYW4gPSBmYWxzZTtcclxuICAgIGVycm9yTXNnOiBzdHJpbmc7XHJcbiAgICBhY3R1YWxMb2dpblN0ZXA6IGFueSA9IExvZ2luU3RlcHMuTGFuZGluZztcclxuICAgIExvZ2luU3RlcHM6IGFueSA9IExvZ2luU3RlcHM7XHJcbiAgICByZW1lbWJlck1lOiBib29sZWFuID0gdHJ1ZTtcclxuICAgIGZvcm1FcnJvcjogeyBbaWQ6IHN0cmluZ106IHN0cmluZyB9O1xyXG4gICAgbWluTGVuZ3RoOiBudW1iZXIgPSAyO1xyXG4gICAgZm9vdGVyVGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT47XHJcbiAgICBoZWFkZXJUZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PjtcclxuICAgIGRhdGE6IGFueTtcclxuXHJcbiAgICBwcml2YXRlIF9tZXNzYWdlOiB7IFtpZDogc3RyaW5nXTogeyBbaWQ6IHN0cmluZ106IFZhbGlkYXRpb25NZXNzYWdlIH0gfTtcclxuICAgIHByaXZhdGUgb25EZXN0cm95JCA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb25zdHJ1Y3RvclxyXG4gICAgICogQHBhcmFtIF9mYlxyXG4gICAgICogQHBhcmFtIGF1dGhTZXJ2aWNlXHJcbiAgICAgKiBAcGFyYW0gdHJhbnNsYXRlXHJcbiAgICAgKi9cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgX2ZiOiBGb3JtQnVpbGRlcixcclxuICAgICAgICBwcml2YXRlIGF1dGhTZXJ2aWNlOiBBdXRoZW50aWNhdGlvblNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSB0cmFuc2xhdGVTZXJ2aWNlOiBUcmFuc2xhdGlvblNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgcHJpdmF0ZSBhcHBDb25maWc6IEFwcENvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSB1c2VyUHJlZmVyZW5jZXM6IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBsb2NhdGlvbjogTG9jYXRpb24sXHJcbiAgICAgICAgcHJpdmF0ZSByb3V0ZTogQWN0aXZhdGVkUm91dGUsXHJcbiAgICAgICAgcHJpdmF0ZSBzYW5pdGl6ZXI6IERvbVNhbml0aXplclxyXG4gICAgKSB7XHJcbiAgICAgICAgdGhpcy5pbml0Rm9ybUVycm9yKCk7XHJcbiAgICAgICAgdGhpcy5pbml0Rm9ybUZpZWxkc01lc3NhZ2VzKCk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuYXV0aFNlcnZpY2UuaXNPYXV0aCgpKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IG9hdXRoOiBPYXV0aENvbmZpZ01vZGVsID0gdGhpcy5hcHBDb25maWcuZ2V0PE9hdXRoQ29uZmlnTW9kZWw+KEFwcENvbmZpZ1ZhbHVlcy5PQVVUSENPTkZJRywgbnVsbCk7XHJcbiAgICAgICAgICAgIGlmIChvYXV0aCAmJiBvYXV0aC5pbXBsaWNpdEZsb3cpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuaW1wbGljaXRGbG93ID0gdHJ1ZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHRoaXMuYXV0aFNlcnZpY2UuaXNFY21Mb2dnZWRJbigpIHx8IHRoaXMuYXV0aFNlcnZpY2UuaXNCcG1Mb2dnZWRJbigpKSB7XHJcbiAgICAgICAgICAgIHRoaXMubG9jYXRpb24uZm9yd2FyZCgpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMucm91dGUucXVlcnlQYXJhbXMuc3Vic2NyaWJlKChwYXJhbXM6IFBhcmFtcykgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdXJsID0gcGFyYW1zWydyZWRpcmVjdFVybCddO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcHJvdmlkZXIgPSB0aGlzLmFwcENvbmZpZy5nZXQ8c3RyaW5nPihBcHBDb25maWdWYWx1ZXMuUFJPVklERVJTKTtcclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLmF1dGhTZXJ2aWNlLnNldFJlZGlyZWN0KHsgcHJvdmlkZXIsIHVybCB9KTtcclxuICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmhhc0N1c3RvbUZpZWxkc1ZhbGlkYXRpb24oKSkge1xyXG4gICAgICAgICAgICB0aGlzLmZvcm0gPSB0aGlzLl9mYi5ncm91cCh0aGlzLmZpZWxkc1ZhbGlkYXRpb24pO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuaW5pdEZvcm1GaWVsZHNEZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIHRoaXMuaW5pdEZvcm1GaWVsZHNNZXNzYWdlc0RlZmF1bHQoKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy5mb3JtLnZhbHVlQ2hhbmdlc1xyXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5vbkRlc3Ryb3kkKSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZShkYXRhID0+IHRoaXMub25WYWx1ZUNoYW5nZWQoZGF0YSkpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIHRoaXMub25EZXN0cm95JC5uZXh0KHRydWUpO1xyXG4gICAgICAgIHRoaXMub25EZXN0cm95JC5jb21wbGV0ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHN1Ym1pdCgpIHtcclxuICAgICAgICB0aGlzLm9uU3VibWl0KHRoaXMuZm9ybS52YWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBNZXRob2QgY2FsbGVkIG9uIHN1Ym1pdCBmb3JtXHJcbiAgICAgKiBAcGFyYW0gdmFsdWVzXHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqL1xyXG4gICAgb25TdWJtaXQodmFsdWVzOiBhbnkpIHtcclxuICAgICAgICB0aGlzLmRpc2FibGVFcnJvcigpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5hdXRoU2VydmljZS5pc09hdXRoKCkgJiYgIXRoaXMuYXV0aFNlcnZpY2UuaXNTU09EaXNjb3ZlcnlDb25maWd1cmVkKCkpIHtcclxuICAgICAgICAgICAgdGhpcy5lcnJvck1zZyA9ICdMT0dJTi5NRVNTQUdFUy5TU08tV1JPTkctQ09ORklHVVJBVElPTic7XHJcbiAgICAgICAgICAgIHRoaXMuaXNFcnJvciA9IHRydWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgYXJncyA9IG5ldyBMb2dpblN1Ym1pdEV2ZW50KHtcclxuICAgICAgICAgICAgICAgIGNvbnRyb2xzOiB7IHVzZXJuYW1lOiB0aGlzLmZvcm0uY29udHJvbHMudXNlcm5hbWUgfVxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgdGhpcy5leGVjdXRlU3VibWl0LmVtaXQoYXJncyk7XHJcblxyXG4gICAgICAgICAgICBpZiAoYXJncy5kZWZhdWx0UHJldmVudGVkKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnBlcmZvcm1Mb2dpbih2YWx1ZXMpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGltcGxpY2l0TG9naW4oKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuYXV0aFNlcnZpY2UuaXNPYXV0aCgpICYmICF0aGlzLmF1dGhTZXJ2aWNlLmlzU1NPRGlzY292ZXJ5Q29uZmlndXJlZCgpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuZXJyb3JNc2cgPSAnTE9HSU4uTUVTU0FHRVMuU1NPLVdST05HLUNPTkZJR1VSQVRJT04nO1xyXG4gICAgICAgICAgICB0aGlzLmlzRXJyb3IgPSB0cnVlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuYXV0aFNlcnZpY2Uuc3NvSW1wbGljaXRMb2dpbigpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFRoZSBtZXRob2QgY2hlY2sgdGhlIGVycm9yIGluIHRoZSBmb3JtIGFuZCBwdXNoIHRoZSBlcnJvciBpbiB0aGUgZm9ybUVycm9yIG9iamVjdFxyXG4gICAgICogQHBhcmFtIGRhdGFcclxuICAgICAqL1xyXG4gICAgb25WYWx1ZUNoYW5nZWQoZGF0YTogYW55KSB7XHJcbiAgICAgICAgdGhpcy5kaXNhYmxlRXJyb3IoKTtcclxuICAgICAgICBmb3IgKGNvbnN0IGZpZWxkIGluIHRoaXMuZm9ybUVycm9yKSB7XHJcbiAgICAgICAgICAgIGlmIChmaWVsZCkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5mb3JtRXJyb3JbZmllbGRdID0gJyc7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBoYXNFcnJvciA9XHJcbiAgICAgICAgICAgICAgICAgICAgKHRoaXMuZm9ybS5jb250cm9sc1tmaWVsZF0uZXJyb3JzICYmIGRhdGFbZmllbGRdICE9PSAnJykgfHxcclxuICAgICAgICAgICAgICAgICAgICAodGhpcy5mb3JtLmNvbnRyb2xzW2ZpZWxkXS5kaXJ0eSAmJlxyXG4gICAgICAgICAgICAgICAgICAgICAgICAhdGhpcy5mb3JtLmNvbnRyb2xzW2ZpZWxkXS52YWxpZCk7XHJcbiAgICAgICAgICAgICAgICBpZiAoaGFzRXJyb3IpIHtcclxuICAgICAgICAgICAgICAgICAgICBmb3IgKGNvbnN0IGtleSBpbiB0aGlzLmZvcm0uY29udHJvbHNbZmllbGRdLmVycm9ycykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoa2V5KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBtZXNzYWdlID0gdGhpcy5fbWVzc2FnZVtmaWVsZF1ba2V5XTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChtZXNzYWdlICYmIG1lc3NhZ2UudmFsdWUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb25zdCB0cmFuc2xhdGVkID0gdGhpcy50cmFuc2xhdGVTZXJ2aWNlLmluc3RhbnQobWVzc2FnZS52YWx1ZSwgbWVzc2FnZS5wYXJhbXMpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZm9ybUVycm9yW2ZpZWxkXSArPSB0cmFuc2xhdGVkO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgcGVyZm9ybUxvZ2luKHZhbHVlczogYW55KSB7XHJcbiAgICAgICAgdGhpcy5hY3R1YWxMb2dpblN0ZXAgPSBMb2dpblN0ZXBzLkNoZWNraW5nO1xyXG4gICAgICAgIHRoaXMuYXV0aFNlcnZpY2VcclxuICAgICAgICAgICAgLmxvZ2luKHZhbHVlcy51c2VybmFtZSwgdmFsdWVzLnBhc3N3b3JkLCB0aGlzLnJlbWVtYmVyTWUpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAodG9rZW46IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IHJlZGlyZWN0VXJsID0gdGhpcy5hdXRoU2VydmljZS5nZXRSZWRpcmVjdCgpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmFjdHVhbExvZ2luU3RlcCA9IExvZ2luU3RlcHMuV2VsY29tZTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlcy5zZXRTdG9yYWdlUHJlZml4KHZhbHVlcy51c2VybmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWVzLnBhc3N3b3JkID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnN1Y2Nlc3MuZW1pdChcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmV3IExvZ2luU3VjY2Vzc0V2ZW50KHRva2VuLCB2YWx1ZXMudXNlcm5hbWUsIG51bGwpXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHJlZGlyZWN0VXJsKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuYXV0aFNlcnZpY2Uuc2V0UmVkaXJlY3QobnVsbCk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwocmVkaXJlY3RVcmwpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5zdWNjZXNzUm91dGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5yb3V0ZXIubmF2aWdhdGUoW3RoaXMuc3VjY2Vzc1JvdXRlXSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIChlcnI6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYWN0dWFsTG9naW5TdGVwID0gTG9naW5TdGVwcy5MYW5kaW5nO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuZGlzcGxheUVycm9yTWVzc2FnZShlcnIpO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuaXNFcnJvciA9IHRydWU7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5lcnJvci5lbWl0KG5ldyBMb2dpbkVycm9yRXZlbnQoZXJyKSk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKCkgPT4gdGhpcy5sb2dTZXJ2aWNlLmluZm8oJ0xvZ2luIGRvbmUnKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2sgYW5kIGRpc3BsYXkgdGhlIHJpZ2h0IGVycm9yIG1lc3NhZ2UgaW4gdGhlIFVJXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgZGlzcGxheUVycm9yTWVzc2FnZShlcnI6IGFueSk6IHZvaWQge1xyXG4gICAgICAgIGlmIChcclxuICAgICAgICAgICAgZXJyLmVycm9yICYmXHJcbiAgICAgICAgICAgIGVyci5lcnJvci5jcm9zc0RvbWFpbiAmJlxyXG4gICAgICAgICAgICBlcnIuZXJyb3IubWVzc2FnZS5pbmRleE9mKCdBY2Nlc3MtQ29udHJvbC1BbGxvdy1PcmlnaW4nKSAhPT0gLTFcclxuICAgICAgICApIHtcclxuICAgICAgICAgICAgdGhpcy5lcnJvck1zZyA9IGVyci5lcnJvci5tZXNzYWdlO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoXHJcbiAgICAgICAgICAgIGVyci5zdGF0dXMgPT09IDQwMyAmJlxyXG4gICAgICAgICAgICBlcnIubWVzc2FnZS5pbmRleE9mKCdJbnZhbGlkIENTUkYtdG9rZW4nKSAhPT0gLTFcclxuICAgICAgICApIHtcclxuICAgICAgICAgICAgdGhpcy5lcnJvck1zZyA9ICdMT0dJTi5NRVNTQUdFUy5MT0dJTi1FUlJPUi1DU1JGJztcclxuICAgICAgICB9IGVsc2UgaWYgKFxyXG4gICAgICAgICAgICBlcnIuc3RhdHVzID09PSA0MDMgJiZcclxuICAgICAgICAgICAgZXJyLm1lc3NhZ2UuaW5kZXhPZignVGhlIHN5c3RlbSBpcyBjdXJyZW50bHkgaW4gcmVhZC1vbmx5IG1vZGUnKSAhPT1cclxuICAgICAgICAgICAgLTFcclxuICAgICAgICApIHtcclxuICAgICAgICAgICAgdGhpcy5lcnJvck1zZyA9ICdMT0dJTi5NRVNTQUdFUy5MT0dJTi1FQ00tTElDRU5TRSc7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5lcnJvck1zZyA9ICdMT0dJTi5NRVNTQUdFUy5MT0dJTi1FUlJPUi1DUkVERU5USUFMUyc7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQWRkIGEgY3VzdG9tIGZvcm0gZXJyb3IgZm9yIGEgZmllbGRcclxuICAgICAqIEBwYXJhbSBmaWVsZFxyXG4gICAgICogQHBhcmFtIG1zZ1xyXG4gICAgICovXHJcbiAgICBwdWJsaWMgYWRkQ3VzdG9tRm9ybUVycm9yKGZpZWxkOiBzdHJpbmcsIG1zZzogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtRXJyb3JbZmllbGRdICs9IG1zZztcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEFkZCBhIGN1c3RvbSB2YWxpZGF0aW9uIHJ1bGUgZXJyb3IgZm9yIGEgZmllbGRcclxuICAgICAqIEBwYXJhbSBmaWVsZFxyXG4gICAgICogQHBhcmFtIHJ1bGVJZCAtIGkuZS4gcmVxdWlyZWQgfCBtaW5sZW5ndGggfCBtYXhsZW5ndGhcclxuICAgICAqIEBwYXJhbSBtc2dcclxuICAgICAqL1xyXG4gICAgYWRkQ3VzdG9tVmFsaWRhdGlvbkVycm9yKFxyXG4gICAgICAgIGZpZWxkOiBzdHJpbmcsXHJcbiAgICAgICAgcnVsZUlkOiBzdHJpbmcsXHJcbiAgICAgICAgbXNnOiBzdHJpbmcsXHJcbiAgICAgICAgcGFyYW1zPzogYW55XHJcbiAgICApIHtcclxuICAgICAgICB0aGlzLl9tZXNzYWdlW2ZpZWxkXVtydWxlSWRdID0ge1xyXG4gICAgICAgICAgICB2YWx1ZTogbXNnLFxyXG4gICAgICAgICAgICBwYXJhbXNcclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGlzcGxheSBhbmQgaGlkZSB0aGUgcGFzc3dvcmQgdmFsdWUuXHJcbiAgICAgKi9cclxuICAgIHRvZ2dsZVNob3dQYXNzd29yZCgpIHtcclxuICAgICAgICB0aGlzLmlzUGFzc3dvcmRTaG93ID0gIXRoaXMuaXNQYXNzd29yZFNob3c7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUaGUgbWV0aG9kIHJldHVybiBpZiBhIGZpZWxkIGlzIHZhbGlkIG9yIG5vdFxyXG4gICAgICogQHBhcmFtIGZpZWxkXHJcbiAgICAgKi9cclxuICAgIGlzRXJyb3JTdHlsZShmaWVsZDogQWJzdHJhY3RDb250cm9sKSB7XHJcbiAgICAgICAgcmV0dXJuICFmaWVsZC52YWxpZCAmJiBmaWVsZC5kaXJ0eSAmJiAhZmllbGQucHJpc3RpbmU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUcmltIHVzZXJuYW1lXHJcbiAgICAgKi9cclxuICAgIHRyaW1Vc2VybmFtZShldmVudDogYW55KSB7XHJcbiAgICAgICAgZXZlbnQudGFyZ2V0LnZhbHVlID0gZXZlbnQudGFyZ2V0LnZhbHVlLnRyaW0oKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRCYWNrZ3JvdW5kVXJsSW1hZ2VVcmwoKSB7XHJcbiAgICAgICAgcmV0dXJuICB0aGlzLnNhbml0aXplci5ieXBhc3NTZWN1cml0eVRydXN0U3R5bGUoYHVybCgke3RoaXMuYmFja2dyb3VuZEltYWdlVXJsfSlgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERlZmF1bHQgZm9ybUVycm9yIHZhbHVlc1xyXG4gICAgICovXHJcbiAgICBwcml2YXRlIGluaXRGb3JtRXJyb3IoKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtRXJyb3IgPSB7XHJcbiAgICAgICAgICAgIHVzZXJuYW1lOiAnJyxcclxuICAgICAgICAgICAgcGFzc3dvcmQ6ICcnXHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEluaXQgZm9ybSBmaWVsZHMgbWVzc2FnZXNcclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBpbml0Rm9ybUZpZWxkc01lc3NhZ2VzKCkge1xyXG4gICAgICAgIHRoaXMuX21lc3NhZ2UgPSB7XHJcbiAgICAgICAgICAgIHVzZXJuYW1lOiB7fSxcclxuICAgICAgICAgICAgcGFzc3dvcmQ6IHt9XHJcbiAgICAgICAgfTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERlZmF1bHQgZm9ybSBmaWVsZHMgbWVzc2FnZXNcclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBpbml0Rm9ybUZpZWxkc01lc3NhZ2VzRGVmYXVsdCgpIHtcclxuICAgICAgICB0aGlzLl9tZXNzYWdlID0ge1xyXG4gICAgICAgICAgICB1c2VybmFtZToge1xyXG4gICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHtcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZTogJ0xPR0lOLk1FU1NBR0VTLlVTRVJOQU1FLVJFUVVJUkVEJ1xyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIG1pbkxlbmd0aDoge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlOiAnTE9HSU4uTUVTU0FHRVMuVVNFUk5BTUUtTUlOJyxcclxuICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZ2V0IG1pbkxlbmd0aCgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLm1pbkxlbmd0aDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHBhc3N3b3JkOiB7XHJcbiAgICAgICAgICAgICAgICByZXF1aXJlZDoge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhbHVlOiAnTE9HSU4uTUVTU0FHRVMuUEFTU1dPUkQtUkVRVUlSRUQnXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9O1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaW5pdEZvcm1GaWVsZHNEZWZhdWx0KCkge1xyXG4gICAgICAgIHRoaXMuZm9ybSA9IHRoaXMuX2ZiLmdyb3VwKHtcclxuICAgICAgICAgICAgdXNlcm5hbWU6IFsnJywgVmFsaWRhdG9ycy5yZXF1aXJlZF0sXHJcbiAgICAgICAgICAgIHBhc3N3b3JkOiBbJycsIFZhbGlkYXRvcnMucmVxdWlyZWRdXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEaXNhYmxlIHRoZSBlcnJvciBmbGFnXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgZGlzYWJsZUVycm9yKCkge1xyXG4gICAgICAgIHRoaXMuaXNFcnJvciA9IGZhbHNlO1xyXG4gICAgICAgIHRoaXMuaW5pdEZvcm1FcnJvcigpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaGFzQ3VzdG9tRmllbGRzVmFsaWRhdGlvbigpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5maWVsZHNWYWxpZGF0aW9uICE9PSB1bmRlZmluZWQ7XHJcbiAgICB9XHJcbn1cclxuIl19