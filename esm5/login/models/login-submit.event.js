/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var LoginSubmitEvent = /** @class */ (function () {
    function LoginSubmitEvent(_values) {
        this._defaultPrevented = false;
        this._values = _values;
    }
    Object.defineProperty(LoginSubmitEvent.prototype, "values", {
        get: /**
         * @return {?}
         */
        function () {
            return this._values;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(LoginSubmitEvent.prototype, "defaultPrevented", {
        get: /**
         * @return {?}
         */
        function () {
            return this._defaultPrevented;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    LoginSubmitEvent.prototype.preventDefault = /**
     * @return {?}
     */
    function () {
        this._defaultPrevented = true;
    };
    return LoginSubmitEvent;
}());
export { LoginSubmitEvent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    LoginSubmitEvent.prototype._values;
    /**
     * @type {?}
     * @private
     */
    LoginSubmitEvent.prototype._defaultPrevented;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9naW4tc3VibWl0LmV2ZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsibG9naW4vbW9kZWxzL2xvZ2luLXN1Ym1pdC5ldmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQTtJQWFJLDBCQUFZLE9BQVk7UUFWaEIsc0JBQWlCLEdBQVksS0FBSyxDQUFDO1FBV3ZDLElBQUksQ0FBQyxPQUFPLEdBQUcsT0FBTyxDQUFDO0lBQzNCLENBQUM7SUFWRCxzQkFBSSxvQ0FBTTs7OztRQUFWO1lBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3hCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksOENBQWdCOzs7O1FBQXBCO1lBQ0ksT0FBTyxJQUFJLENBQUMsaUJBQWlCLENBQUM7UUFDbEMsQ0FBQzs7O09BQUE7Ozs7SUFNRCx5Q0FBYzs7O0lBQWQ7UUFDSSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDO0lBQ2xDLENBQUM7SUFFTCx1QkFBQztBQUFELENBQUMsQUFyQkQsSUFxQkM7Ozs7Ozs7SUFuQkcsbUNBQXFCOzs7OztJQUNyQiw2Q0FBMkMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuZXhwb3J0IGNsYXNzIExvZ2luU3VibWl0RXZlbnQge1xyXG5cclxuICAgIHByaXZhdGUgX3ZhbHVlczogYW55O1xyXG4gICAgcHJpdmF0ZSBfZGVmYXVsdFByZXZlbnRlZDogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIGdldCB2YWx1ZXMoKTogYW55IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fdmFsdWVzO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBkZWZhdWx0UHJldmVudGVkKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLl9kZWZhdWx0UHJldmVudGVkO1xyXG4gICAgfVxyXG5cclxuICAgIGNvbnN0cnVjdG9yKF92YWx1ZXM6IGFueSkge1xyXG4gICAgICAgIHRoaXMuX3ZhbHVlcyA9IF92YWx1ZXM7XHJcbiAgICB9XHJcblxyXG4gICAgcHJldmVudERlZmF1bHQoKSB7XHJcbiAgICAgICAgdGhpcy5fZGVmYXVsdFByZXZlbnRlZCA9IHRydWU7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==