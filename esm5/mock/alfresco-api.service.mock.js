/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { AppConfigService } from '../app-config/app-config.service';
import { AlfrescoApiService } from '../services/alfresco-api.service';
import { StorageService } from '../services/storage.service';
/* tslint:disable:adf-file-name */
var AlfrescoApiServiceMock = /** @class */ (function (_super) {
    tslib_1.__extends(AlfrescoApiServiceMock, _super);
    function AlfrescoApiServiceMock(appConfig, storageService) {
        var _this = _super.call(this, appConfig, storageService) || this;
        _this.appConfig = appConfig;
        _this.storageService = storageService;
        if (!_this.alfrescoApi) {
            _this.initAlfrescoApi();
        }
        return _this;
    }
    /**
     * @return {?}
     */
    AlfrescoApiServiceMock.prototype.initialize = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        function (resolve) {
            _this.alfrescoApiInitializedSubject.next();
            resolve();
        }));
    };
    AlfrescoApiServiceMock.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    AlfrescoApiServiceMock.ctorParameters = function () { return [
        { type: AppConfigService },
        { type: StorageService }
    ]; };
    return AlfrescoApiServiceMock;
}(AlfrescoApiService));
export { AlfrescoApiServiceMock };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    AlfrescoApiServiceMock.prototype.appConfig;
    /**
     * @type {?}
     * @protected
     */
    AlfrescoApiServiceMock.prototype.storageService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxmcmVzY28tYXBpLnNlcnZpY2UubW9jay5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vY2svYWxmcmVzY28tYXBpLnNlcnZpY2UubW9jay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUNwRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUN0RSxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sNkJBQTZCLENBQUM7O0FBRzdEO0lBQzRDLGtEQUFrQjtJQUUxRCxnQ0FBc0IsU0FBMkIsRUFDM0IsY0FBOEI7UUFEcEQsWUFFSSxrQkFBTSxTQUFTLEVBQUUsY0FBYyxDQUFDLFNBSW5DO1FBTnFCLGVBQVMsR0FBVCxTQUFTLENBQWtCO1FBQzNCLG9CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUVoRCxJQUFJLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRTtZQUNuQixLQUFJLENBQUMsZUFBZSxFQUFFLENBQUM7U0FDMUI7O0lBQ0wsQ0FBQzs7OztJQUVELDJDQUFVOzs7SUFBVjtRQUFBLGlCQUtDO1FBSkcsT0FBTyxJQUFJLE9BQU87Ozs7UUFBQyxVQUFDLE9BQU87WUFDdkIsS0FBSSxDQUFDLDZCQUE2QixDQUFDLElBQUksRUFBRSxDQUFDO1lBQzFDLE9BQU8sRUFBRSxDQUFDO1FBQ2QsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOztnQkFoQkosVUFBVTs7OztnQkFMRixnQkFBZ0I7Z0JBRWhCLGNBQWM7O0lBb0J2Qiw2QkFBQztDQUFBLEFBakJELENBQzRDLGtCQUFrQixHQWdCN0Q7U0FoQlksc0JBQXNCOzs7Ozs7SUFFbkIsMkNBQXFDOzs7OztJQUNyQyxnREFBd0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBcHBDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vYXBwLWNvbmZpZy9hcHAtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvc3RvcmFnZS5zZXJ2aWNlJztcclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOmFkZi1maWxlLW5hbWUgKi9cclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQWxmcmVzY29BcGlTZXJ2aWNlTW9jayBleHRlbmRzIEFsZnJlc2NvQXBpU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJvdGVjdGVkIGFwcENvbmZpZzogQXBwQ29uZmlnU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByb3RlY3RlZCBzdG9yYWdlU2VydmljZTogU3RvcmFnZVNlcnZpY2UpIHtcclxuICAgICAgICBzdXBlcihhcHBDb25maWcsIHN0b3JhZ2VTZXJ2aWNlKTtcclxuICAgICAgICBpZiAoIXRoaXMuYWxmcmVzY29BcGkpIHtcclxuICAgICAgICAgICAgdGhpcy5pbml0QWxmcmVzY29BcGkoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdGlhbGl6ZSgpOiBQcm9taXNlPGFueT4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZSgocmVzb2x2ZSkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFsZnJlc2NvQXBpSW5pdGlhbGl6ZWRTdWJqZWN0Lm5leHQoKTtcclxuICAgICAgICAgICAgcmVzb2x2ZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==