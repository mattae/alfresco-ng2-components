/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { AppConfigService } from '../app-config/app-config.service';
import { HttpClient } from '@angular/common/http';
var AppConfigServiceMock = /** @class */ (function (_super) {
    tslib_1.__extends(AppConfigServiceMock, _super);
    function AppConfigServiceMock(http) {
        var _this = _super.call(this, http) || this;
        _this.config = {
            application: {
                name: 'Alfresco ADF Application'
            },
            ecmHost: 'http://{hostname}{:port}/ecm',
            bpmHost: 'http://{hostname}{:port}/bpm',
            logLevel: 'silent'
        };
        return _this;
    }
    /**
     * @return {?}
     */
    AppConfigServiceMock.prototype.load = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        function (resolve) {
            _this.onLoadSubject.next(_this.config);
            resolve(_this.config);
        }));
    };
    AppConfigServiceMock.decorators = [
        { type: Injectable }
    ];
    /** @nocollapse */
    AppConfigServiceMock.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    return AppConfigServiceMock;
}(AppConfigService));
export { AppConfigServiceMock };
if (false) {
    /** @type {?} */
    AppConfigServiceMock.prototype.config;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwLWNvbmZpZy5zZXJ2aWNlLm1vY2suanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJtb2NrL2FwcC1jb25maWcuc2VydmljZS5tb2NrLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUNsRDtJQUMwQyxnREFBZ0I7SUFXdEQsOEJBQVksSUFBZ0I7UUFBNUIsWUFDSSxrQkFBTSxJQUFJLENBQUMsU0FDZDtRQVhELFlBQU0sR0FBUTtZQUNWLFdBQVcsRUFBRTtnQkFDVCxJQUFJLEVBQUUsMEJBQTBCO2FBQ25DO1lBQ0QsT0FBTyxFQUFFLDhCQUE4QjtZQUN2QyxPQUFPLEVBQUUsOEJBQThCO1lBQ3ZDLFFBQVEsRUFBRSxRQUFRO1NBQ3JCLENBQUM7O0lBSUYsQ0FBQzs7OztJQUVELG1DQUFJOzs7SUFBSjtRQUFBLGlCQUtDO1FBSkcsT0FBTyxJQUFJLE9BQU87Ozs7UUFBQyxVQUFDLE9BQU87WUFDdkIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3JDLE9BQU8sQ0FBQyxLQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDekIsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOztnQkFyQkosVUFBVTs7OztnQkFERixVQUFVOztJQXVCbkIsMkJBQUM7Q0FBQSxBQXRCRCxDQUMwQyxnQkFBZ0IsR0FxQnpEO1NBckJZLG9CQUFvQjs7O0lBRTdCLHNDQU9FIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQXBwQ29uZmlnU2VydmljZSB9IGZyb20gJy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSHR0cENsaWVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuQEluamVjdGFibGUoKVxyXG5leHBvcnQgY2xhc3MgQXBwQ29uZmlnU2VydmljZU1vY2sgZXh0ZW5kcyBBcHBDb25maWdTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25maWc6IGFueSA9IHtcclxuICAgICAgICBhcHBsaWNhdGlvbjoge1xyXG4gICAgICAgICAgICBuYW1lOiAnQWxmcmVzY28gQURGIEFwcGxpY2F0aW9uJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZWNtSG9zdDogJ2h0dHA6Ly97aG9zdG5hbWV9ezpwb3J0fS9lY20nLFxyXG4gICAgICAgIGJwbUhvc3Q6ICdodHRwOi8ve2hvc3RuYW1lfXs6cG9ydH0vYnBtJyxcclxuICAgICAgICBsb2dMZXZlbDogJ3NpbGVudCdcclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IoaHR0cDogSHR0cENsaWVudCkge1xyXG4gICAgICAgIHN1cGVyKGh0dHApO1xyXG4gICAgfVxyXG5cclxuICAgIGxvYWQoKTogUHJvbWlzZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5vbkxvYWRTdWJqZWN0Lm5leHQodGhpcy5jb25maWcpO1xyXG4gICAgICAgICAgICByZXNvbHZlKHRoaXMuY29uZmlnKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=