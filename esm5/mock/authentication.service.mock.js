/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { of, throwError } from 'rxjs';
// TODO: should be extending AuthenticationService
var 
// TODO: should be extending AuthenticationService
AuthenticationMock /*extends AuthenticationService*/ = /** @class */ (function () {
    function AuthenticationMock() {
        this.redirectUrl = null;
    }
    /**
     * @param {?} url
     * @return {?}
     */
    AuthenticationMock.prototype.setRedirectUrl = /**
     * @param {?} url
     * @return {?}
     */
    function (url) {
        this.redirectUrl = url;
    };
    /**
     * @return {?}
     */
    AuthenticationMock.prototype.getRedirectUrl = /**
     * @return {?}
     */
    function () {
        return this.redirectUrl ? this.redirectUrl.url : null;
    };
    // TODO: real auth service returns Observable<string>
    // TODO: real auth service returns Observable<string>
    /**
     * @param {?} username
     * @param {?} password
     * @return {?}
     */
    AuthenticationMock.prototype.login = 
    // TODO: real auth service returns Observable<string>
    /**
     * @param {?} username
     * @param {?} password
     * @return {?}
     */
    function (username, password) {
        if (username === 'fake-username' && password === 'fake-password') {
            return of({ type: 'type', ticket: 'ticket' });
        }
        if (username === 'fake-username-CORS-error' && password === 'fake-password') {
            return throwError({
                error: {
                    crossDomain: true,
                    message: 'ERROR: the network is offline, Origin is not allowed by Access-Control-Allow-Origin'
                }
            });
        }
        if (username === 'fake-username-CSRF-error' && password === 'fake-password') {
            return throwError({ message: 'ERROR: Invalid CSRF-token', status: 403 });
        }
        if (username === 'fake-username-ECM-access-error' && password === 'fake-password') {
            return throwError({ message: 'ERROR: 00170728 Access Denied.  The system is currently in read-only mode', status: 403 });
        }
        return throwError('Fake server error');
    };
    return AuthenticationMock;
}());
// TODO: should be extending AuthenticationService
export { AuthenticationMock };
if (false) {
    /**
     * @type {?}
     * @private
     */
    AuthenticationMock.prototype.redirectUrl;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24uc2VydmljZS5tb2NrLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsibW9jay9hdXRoZW50aWNhdGlvbi5zZXJ2aWNlLm1vY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFjLEVBQUUsRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7O0FBSWxEOzttQkFBZ0MsaUNBQWlDO0lBQWpFO1FBQ1ksZ0JBQVcsR0FBcUIsSUFBSSxDQUFDO0lBbUNqRCxDQUFDOzs7OztJQWpDRywyQ0FBYzs7OztJQUFkLFVBQWUsR0FBcUI7UUFDaEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7SUFDM0IsQ0FBQzs7OztJQUVELDJDQUFjOzs7SUFBZDtRQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUMxRCxDQUFDO0lBRUQscURBQXFEOzs7Ozs7O0lBQ3JELGtDQUFLOzs7Ozs7O0lBQUwsVUFBTSxRQUFnQixFQUFFLFFBQWdCO1FBQ3BDLElBQUksUUFBUSxLQUFLLGVBQWUsSUFBSSxRQUFRLEtBQUssZUFBZSxFQUFFO1lBQzlELE9BQU8sRUFBRSxDQUFDLEVBQUUsSUFBSSxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsUUFBUSxFQUFDLENBQUMsQ0FBQztTQUNoRDtRQUVELElBQUksUUFBUSxLQUFLLDBCQUEwQixJQUFJLFFBQVEsS0FBSyxlQUFlLEVBQUU7WUFDekUsT0FBTyxVQUFVLENBQUM7Z0JBQ2QsS0FBSyxFQUFFO29CQUNILFdBQVcsRUFBRSxJQUFJO29CQUNqQixPQUFPLEVBQUUscUZBQXFGO2lCQUNqRzthQUNKLENBQUMsQ0FBQztTQUNOO1FBRUQsSUFBSSxRQUFRLEtBQUssMEJBQTBCLElBQUksUUFBUSxLQUFLLGVBQWUsRUFBRTtZQUN6RSxPQUFPLFVBQVUsQ0FBQyxFQUFDLE9BQU8sRUFBRSwyQkFBMkIsRUFBRSxNQUFNLEVBQUUsR0FBRyxFQUFDLENBQUMsQ0FBQztTQUMxRTtRQUVELElBQUksUUFBUSxLQUFLLGdDQUFnQyxJQUFJLFFBQVEsS0FBSyxlQUFlLEVBQUU7WUFDL0UsT0FBTyxVQUFVLENBQUMsRUFBQyxPQUFPLEVBQUUsMkVBQTJFLEVBQUUsTUFBTSxFQUFFLEdBQUcsRUFBQyxDQUFDLENBQUM7U0FDMUg7UUFFRCxPQUFPLFVBQVUsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO0lBQzNDLENBQUM7SUFDTCx5QkFBQztBQUFELENBQUMsQUFwQ0QsSUFvQ0M7Ozs7Ozs7O0lBbkNHLHlDQUE2QyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBvZiwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBSZWRpcmVjdGlvbk1vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL3JlZGlyZWN0aW9uLm1vZGVsJztcclxuXHJcbi8vIFRPRE86IHNob3VsZCBiZSBleHRlbmRpbmcgQXV0aGVudGljYXRpb25TZXJ2aWNlXHJcbmV4cG9ydCBjbGFzcyBBdXRoZW50aWNhdGlvbk1vY2sgLypleHRlbmRzIEF1dGhlbnRpY2F0aW9uU2VydmljZSovIHtcclxuICAgIHByaXZhdGUgcmVkaXJlY3RVcmw6IFJlZGlyZWN0aW9uTW9kZWwgPSBudWxsO1xyXG5cclxuICAgIHNldFJlZGlyZWN0VXJsKHVybDogUmVkaXJlY3Rpb25Nb2RlbCkge1xyXG4gICAgICAgIHRoaXMucmVkaXJlY3RVcmwgPSB1cmw7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UmVkaXJlY3RVcmwoKTogc3RyaW5nfG51bGwge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnJlZGlyZWN0VXJsID8gdGhpcy5yZWRpcmVjdFVybC51cmwgOiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8vIFRPRE86IHJlYWwgYXV0aCBzZXJ2aWNlIHJldHVybnMgT2JzZXJ2YWJsZTxzdHJpbmc+XHJcbiAgICBsb2dpbih1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nKTogT2JzZXJ2YWJsZTx7IHR5cGU6IHN0cmluZywgdGlja2V0OiBhbnkgfT4ge1xyXG4gICAgICAgIGlmICh1c2VybmFtZSA9PT0gJ2Zha2UtdXNlcm5hbWUnICYmIHBhc3N3b3JkID09PSAnZmFrZS1wYXNzd29yZCcpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG9mKHsgdHlwZTogJ3R5cGUnLCB0aWNrZXQ6ICd0aWNrZXQnfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodXNlcm5hbWUgPT09ICdmYWtlLXVzZXJuYW1lLUNPUlMtZXJyb3InICYmIHBhc3N3b3JkID09PSAnZmFrZS1wYXNzd29yZCcpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRocm93RXJyb3Ioe1xyXG4gICAgICAgICAgICAgICAgZXJyb3I6IHtcclxuICAgICAgICAgICAgICAgICAgICBjcm9zc0RvbWFpbjogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICBtZXNzYWdlOiAnRVJST1I6IHRoZSBuZXR3b3JrIGlzIG9mZmxpbmUsIE9yaWdpbiBpcyBub3QgYWxsb3dlZCBieSBBY2Nlc3MtQ29udHJvbC1BbGxvdy1PcmlnaW4nXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHVzZXJuYW1lID09PSAnZmFrZS11c2VybmFtZS1DU1JGLWVycm9yJyAmJiBwYXNzd29yZCA9PT0gJ2Zha2UtcGFzc3dvcmQnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aHJvd0Vycm9yKHttZXNzYWdlOiAnRVJST1I6IEludmFsaWQgQ1NSRi10b2tlbicsIHN0YXR1czogNDAzfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodXNlcm5hbWUgPT09ICdmYWtlLXVzZXJuYW1lLUVDTS1hY2Nlc3MtZXJyb3InICYmIHBhc3N3b3JkID09PSAnZmFrZS1wYXNzd29yZCcpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRocm93RXJyb3Ioe21lc3NhZ2U6ICdFUlJPUjogMDAxNzA3MjggQWNjZXNzIERlbmllZC4gIFRoZSBzeXN0ZW0gaXMgY3VycmVudGx5IGluIHJlYWQtb25seSBtb2RlJywgc3RhdHVzOiA0MDN9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB0aHJvd0Vycm9yKCdGYWtlIHNlcnZlciBlcnJvcicpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==