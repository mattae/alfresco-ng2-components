/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/** @type {?} */
export var fakeBpmUserNoImage = {
    apps: [],
    capabilities: 'fake-capability',
    company: 'fake-company',
    created: 'fake-create-date',
    email: 'fakeBpm@fake.com',
    externalId: 'fake-external-id',
    firstName: 'fake-first-name',
    lastName: 'fake-last-name',
    groups: [],
    id: 'fake-id',
    lastUpdate: 'fake-update-date',
    latestSyncTimeStamp: 'fake-timestamp',
    password: 'fake-password',
    pictureId: undefined,
    status: 'fake-status',
    tenantId: 'fake-tenant-id',
    tenantName: 'fake-tenant-name',
    tenantPictureId: 'fake-tenant-picture-id',
    type: 'fake-type'
};
/** @type {?} */
export var fakeBpmUser = {
    apps: [],
    capabilities: null,
    company: 'fake-company',
    created: 'fake-create-date',
    email: 'fakeBpm@fake.com',
    externalId: 'fake-external-id',
    firstName: 'fake-bpm-first-name',
    lastName: 'fake-bpm-last-name',
    groups: [],
    id: 'fake-id',
    lastUpdate: 'fake-update-date',
    latestSyncTimeStamp: 'fake-timestamp',
    password: 'fake-password',
    pictureId: 12,
    status: 'fake-status',
    tenantId: 'fake-tenant-id',
    tenantName: 'fake-tenant-name',
    tenantPictureId: 'fake-tenant-picture-id',
    type: 'fake-type'
};
/** @type {?} */
export var fakeBpmEditedUser = {
    apps: [],
    capabilities: 'fake-capability',
    company: 'fake-company',
    created: 'fake-create-date',
    email: 'fakeBpm@fake.com',
    externalId: 'fake-external-id',
    firstName: 'fake-first-name',
    lastName: 'fake-last-name',
    groups: [],
    id: 'fake-id',
    lastUpdate: 'fake-update-date',
    latestSyncTimeStamp: 'fake-timestamp',
    password: 'fake-password',
    pictureId: 'src/assets/images/bpmImg.gif',
    status: 'fake-status',
    tenantId: 'fake-tenant-id',
    tenantName: 'fake-tenant-name',
    tenantPictureId: 'fake-tenant-picture-id',
    type: 'fake-type'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnBtLXVzZXIuc2VydmljZS5tb2NrLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsibW9jay9icG0tdXNlci5zZXJ2aWNlLm1vY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE1BQU0sS0FBSyxrQkFBa0IsR0FBRztJQUM1QixJQUFJLEVBQUUsRUFBRTtJQUNSLFlBQVksRUFBRSxpQkFBaUI7SUFDL0IsT0FBTyxFQUFFLGNBQWM7SUFDdkIsT0FBTyxFQUFFLGtCQUFrQjtJQUMzQixLQUFLLEVBQUUsa0JBQWtCO0lBQ3pCLFVBQVUsRUFBRSxrQkFBa0I7SUFDOUIsU0FBUyxFQUFFLGlCQUFpQjtJQUM1QixRQUFRLEVBQUUsZ0JBQWdCO0lBQzFCLE1BQU0sRUFBRSxFQUFFO0lBQ1YsRUFBRSxFQUFFLFNBQVM7SUFDYixVQUFVLEVBQUUsa0JBQWtCO0lBQzlCLG1CQUFtQixFQUFFLGdCQUFnQjtJQUNyQyxRQUFRLEVBQUUsZUFBZTtJQUN6QixTQUFTLEVBQUUsU0FBUztJQUNwQixNQUFNLEVBQUUsYUFBYTtJQUNyQixRQUFRLEVBQUUsZ0JBQWdCO0lBQzFCLFVBQVUsRUFBRSxrQkFBa0I7SUFDOUIsZUFBZSxFQUFFLHdCQUF3QjtJQUN6QyxJQUFJLEVBQUUsV0FBVztDQUNwQjs7QUFFRCxNQUFNLEtBQUssV0FBVyxHQUFHO0lBQ3JCLElBQUksRUFBRSxFQUFFO0lBQ1IsWUFBWSxFQUFFLElBQUk7SUFDbEIsT0FBTyxFQUFFLGNBQWM7SUFDdkIsT0FBTyxFQUFFLGtCQUFrQjtJQUMzQixLQUFLLEVBQUUsa0JBQWtCO0lBQ3pCLFVBQVUsRUFBRSxrQkFBa0I7SUFDOUIsU0FBUyxFQUFFLHFCQUFxQjtJQUNoQyxRQUFRLEVBQUUsb0JBQW9CO0lBQzlCLE1BQU0sRUFBRSxFQUFFO0lBQ1YsRUFBRSxFQUFFLFNBQVM7SUFDYixVQUFVLEVBQUUsa0JBQWtCO0lBQzlCLG1CQUFtQixFQUFFLGdCQUFnQjtJQUNyQyxRQUFRLEVBQUUsZUFBZTtJQUN6QixTQUFTLEVBQUUsRUFBRTtJQUNiLE1BQU0sRUFBRSxhQUFhO0lBQ3JCLFFBQVEsRUFBRSxnQkFBZ0I7SUFDMUIsVUFBVSxFQUFFLGtCQUFrQjtJQUM5QixlQUFlLEVBQUUsd0JBQXdCO0lBQ3pDLElBQUksRUFBRSxXQUFXO0NBQ3BCOztBQUVELE1BQU0sS0FBSyxpQkFBaUIsR0FBRztJQUMzQixJQUFJLEVBQUUsRUFBRTtJQUNSLFlBQVksRUFBRSxpQkFBaUI7SUFDL0IsT0FBTyxFQUFFLGNBQWM7SUFDdkIsT0FBTyxFQUFFLGtCQUFrQjtJQUMzQixLQUFLLEVBQUUsa0JBQWtCO0lBQ3pCLFVBQVUsRUFBRSxrQkFBa0I7SUFDOUIsU0FBUyxFQUFFLGlCQUFpQjtJQUM1QixRQUFRLEVBQUUsZ0JBQWdCO0lBQzFCLE1BQU0sRUFBRSxFQUFFO0lBQ1YsRUFBRSxFQUFFLFNBQVM7SUFDYixVQUFVLEVBQUUsa0JBQWtCO0lBQzlCLG1CQUFtQixFQUFFLGdCQUFnQjtJQUNyQyxRQUFRLEVBQUUsZUFBZTtJQUN6QixTQUFTLEVBQUUsOEJBQThCO0lBQ3pDLE1BQU0sRUFBRSxhQUFhO0lBQ3JCLFFBQVEsRUFBRSxnQkFBZ0I7SUFDMUIsVUFBVSxFQUFFLGtCQUFrQjtJQUM5QixlQUFlLEVBQUUsd0JBQXdCO0lBQ3pDLElBQUksRUFBRSxXQUFXO0NBQ3BCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmV4cG9ydCBsZXQgZmFrZUJwbVVzZXJOb0ltYWdlID0ge1xyXG4gICAgYXBwczogW10sXHJcbiAgICBjYXBhYmlsaXRpZXM6ICdmYWtlLWNhcGFiaWxpdHknLFxyXG4gICAgY29tcGFueTogJ2Zha2UtY29tcGFueScsXHJcbiAgICBjcmVhdGVkOiAnZmFrZS1jcmVhdGUtZGF0ZScsXHJcbiAgICBlbWFpbDogJ2Zha2VCcG1AZmFrZS5jb20nLFxyXG4gICAgZXh0ZXJuYWxJZDogJ2Zha2UtZXh0ZXJuYWwtaWQnLFxyXG4gICAgZmlyc3ROYW1lOiAnZmFrZS1maXJzdC1uYW1lJyxcclxuICAgIGxhc3ROYW1lOiAnZmFrZS1sYXN0LW5hbWUnLFxyXG4gICAgZ3JvdXBzOiBbXSxcclxuICAgIGlkOiAnZmFrZS1pZCcsXHJcbiAgICBsYXN0VXBkYXRlOiAnZmFrZS11cGRhdGUtZGF0ZScsXHJcbiAgICBsYXRlc3RTeW5jVGltZVN0YW1wOiAnZmFrZS10aW1lc3RhbXAnLFxyXG4gICAgcGFzc3dvcmQ6ICdmYWtlLXBhc3N3b3JkJyxcclxuICAgIHBpY3R1cmVJZDogdW5kZWZpbmVkLFxyXG4gICAgc3RhdHVzOiAnZmFrZS1zdGF0dXMnLFxyXG4gICAgdGVuYW50SWQ6ICdmYWtlLXRlbmFudC1pZCcsXHJcbiAgICB0ZW5hbnROYW1lOiAnZmFrZS10ZW5hbnQtbmFtZScsXHJcbiAgICB0ZW5hbnRQaWN0dXJlSWQ6ICdmYWtlLXRlbmFudC1waWN0dXJlLWlkJyxcclxuICAgIHR5cGU6ICdmYWtlLXR5cGUnXHJcbn07XHJcblxyXG5leHBvcnQgbGV0IGZha2VCcG1Vc2VyID0ge1xyXG4gICAgYXBwczogW10sXHJcbiAgICBjYXBhYmlsaXRpZXM6IG51bGwsXHJcbiAgICBjb21wYW55OiAnZmFrZS1jb21wYW55JyxcclxuICAgIGNyZWF0ZWQ6ICdmYWtlLWNyZWF0ZS1kYXRlJyxcclxuICAgIGVtYWlsOiAnZmFrZUJwbUBmYWtlLmNvbScsXHJcbiAgICBleHRlcm5hbElkOiAnZmFrZS1leHRlcm5hbC1pZCcsXHJcbiAgICBmaXJzdE5hbWU6ICdmYWtlLWJwbS1maXJzdC1uYW1lJyxcclxuICAgIGxhc3ROYW1lOiAnZmFrZS1icG0tbGFzdC1uYW1lJyxcclxuICAgIGdyb3VwczogW10sXHJcbiAgICBpZDogJ2Zha2UtaWQnLFxyXG4gICAgbGFzdFVwZGF0ZTogJ2Zha2UtdXBkYXRlLWRhdGUnLFxyXG4gICAgbGF0ZXN0U3luY1RpbWVTdGFtcDogJ2Zha2UtdGltZXN0YW1wJyxcclxuICAgIHBhc3N3b3JkOiAnZmFrZS1wYXNzd29yZCcsXHJcbiAgICBwaWN0dXJlSWQ6IDEyLFxyXG4gICAgc3RhdHVzOiAnZmFrZS1zdGF0dXMnLFxyXG4gICAgdGVuYW50SWQ6ICdmYWtlLXRlbmFudC1pZCcsXHJcbiAgICB0ZW5hbnROYW1lOiAnZmFrZS10ZW5hbnQtbmFtZScsXHJcbiAgICB0ZW5hbnRQaWN0dXJlSWQ6ICdmYWtlLXRlbmFudC1waWN0dXJlLWlkJyxcclxuICAgIHR5cGU6ICdmYWtlLXR5cGUnXHJcbn07XHJcblxyXG5leHBvcnQgbGV0IGZha2VCcG1FZGl0ZWRVc2VyID0ge1xyXG4gICAgYXBwczogW10sXHJcbiAgICBjYXBhYmlsaXRpZXM6ICdmYWtlLWNhcGFiaWxpdHknLFxyXG4gICAgY29tcGFueTogJ2Zha2UtY29tcGFueScsXHJcbiAgICBjcmVhdGVkOiAnZmFrZS1jcmVhdGUtZGF0ZScsXHJcbiAgICBlbWFpbDogJ2Zha2VCcG1AZmFrZS5jb20nLFxyXG4gICAgZXh0ZXJuYWxJZDogJ2Zha2UtZXh0ZXJuYWwtaWQnLFxyXG4gICAgZmlyc3ROYW1lOiAnZmFrZS1maXJzdC1uYW1lJyxcclxuICAgIGxhc3ROYW1lOiAnZmFrZS1sYXN0LW5hbWUnLFxyXG4gICAgZ3JvdXBzOiBbXSxcclxuICAgIGlkOiAnZmFrZS1pZCcsXHJcbiAgICBsYXN0VXBkYXRlOiAnZmFrZS11cGRhdGUtZGF0ZScsXHJcbiAgICBsYXRlc3RTeW5jVGltZVN0YW1wOiAnZmFrZS10aW1lc3RhbXAnLFxyXG4gICAgcGFzc3dvcmQ6ICdmYWtlLXBhc3N3b3JkJyxcclxuICAgIHBpY3R1cmVJZDogJ3NyYy9hc3NldHMvaW1hZ2VzL2JwbUltZy5naWYnLFxyXG4gICAgc3RhdHVzOiAnZmFrZS1zdGF0dXMnLFxyXG4gICAgdGVuYW50SWQ6ICdmYWtlLXRlbmFudC1pZCcsXHJcbiAgICB0ZW5hbnROYW1lOiAnZmFrZS10ZW5hbnQtbmFtZScsXHJcbiAgICB0ZW5hbnRQaWN0dXJlSWQ6ICdmYWtlLXRlbmFudC1waWN0dXJlLWlkJyxcclxuICAgIHR5cGU6ICdmYWtlLXR5cGUnXHJcbn07XHJcbiJdfQ==