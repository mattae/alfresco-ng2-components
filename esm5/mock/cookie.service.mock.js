/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { CookieService } from '../services/cookie.service';
var CookieServiceMock = /** @class */ (function (_super) {
    tslib_1.__extends(CookieServiceMock, _super);
    function CookieServiceMock() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    /** @override */
    /**
     * @override
     * @return {?}
     */
    CookieServiceMock.prototype.isEnabled = /**
     * @override
     * @return {?}
     */
    function () {
        return true;
    };
    /** @override */
    /**
     * @override
     * @param {?} key
     * @return {?}
     */
    CookieServiceMock.prototype.getItem = /**
     * @override
     * @param {?} key
     * @return {?}
     */
    function (key) {
        return this[key] && this[key].data || null;
    };
    /** @override */
    /**
     * @override
     * @param {?} key
     * @param {?} data
     * @param {?} expiration
     * @param {?} path
     * @return {?}
     */
    CookieServiceMock.prototype.setItem = /**
     * @override
     * @param {?} key
     * @param {?} data
     * @param {?} expiration
     * @param {?} path
     * @return {?}
     */
    function (key, data, expiration, path) {
        this[key] = { data: data, expiration: expiration, path: path };
    };
    /** @override */
    /**
     * @override
     * @return {?}
     */
    CookieServiceMock.prototype.clear = /**
     * @override
     * @return {?}
     */
    function () {
        var _this = this;
        Object.keys(this).forEach((/**
         * @param {?} key
         * @return {?}
         */
        function (key) {
            if (_this.hasOwnProperty(key) && typeof (_this[key]) !== 'function') {
                _this[key] = undefined;
            }
        }));
    };
    return CookieServiceMock;
}(CookieService));
export { CookieServiceMock };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29va2llLnNlcnZpY2UubW9jay5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vY2svY29va2llLnNlcnZpY2UubW9jay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBRTNEO0lBQXVDLDZDQUFhO0lBQXBEOztJQXlCQSxDQUFDO0lBdkJHLGdCQUFnQjs7Ozs7SUFDaEIscUNBQVM7Ozs7SUFBVDtRQUNJLE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRCxnQkFBZ0I7Ozs7OztJQUNoQixtQ0FBTzs7Ozs7SUFBUCxVQUFRLEdBQVc7UUFDZixPQUFPLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQztJQUMvQyxDQUFDO0lBRUQsZ0JBQWdCOzs7Ozs7Ozs7SUFDaEIsbUNBQU87Ozs7Ozs7O0lBQVAsVUFBUSxHQUFXLEVBQUUsSUFBWSxFQUFFLFVBQXVCLEVBQUUsSUFBbUI7UUFDM0UsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEVBQUMsSUFBSSxNQUFBLEVBQUUsVUFBVSxZQUFBLEVBQUUsSUFBSSxNQUFBLEVBQUMsQ0FBQztJQUN6QyxDQUFDO0lBRUQsZ0JBQWdCOzs7OztJQUNoQixpQ0FBSzs7OztJQUFMO1FBQUEsaUJBTUM7UUFMRyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU87Ozs7UUFBQyxVQUFDLEdBQUc7WUFDMUIsSUFBSSxLQUFJLENBQUMsY0FBYyxDQUFDLEdBQUcsQ0FBQyxJQUFJLE9BQU0sQ0FBQyxLQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsS0FBSyxVQUFVLEVBQUU7Z0JBQzlELEtBQUksQ0FBQyxHQUFHLENBQUMsR0FBRyxTQUFTLENBQUM7YUFDekI7UUFDTCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7SUFDTCx3QkFBQztBQUFELENBQUMsQUF6QkQsQ0FBdUMsYUFBYSxHQXlCbkQiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ29va2llU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL2Nvb2tpZS5zZXJ2aWNlJztcclxuXHJcbmV4cG9ydCBjbGFzcyBDb29raWVTZXJ2aWNlTW9jayBleHRlbmRzIENvb2tpZVNlcnZpY2Uge1xyXG5cclxuICAgIC8qKiBAb3ZlcnJpZGUgKi9cclxuICAgIGlzRW5hYmxlZCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgIH1cclxuXHJcbiAgICAvKiogQG92ZXJyaWRlICovXHJcbiAgICBnZXRJdGVtKGtleTogc3RyaW5nKTogc3RyaW5nIHwgbnVsbCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXNba2V5XSAmJiB0aGlzW2tleV0uZGF0YSB8fCBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKiBAb3ZlcnJpZGUgKi9cclxuICAgIHNldEl0ZW0oa2V5OiBzdHJpbmcsIGRhdGE6IHN0cmluZywgZXhwaXJhdGlvbjogRGF0ZSB8IG51bGwsIHBhdGg6IHN0cmluZyB8IG51bGwpOiB2b2lkIHtcclxuICAgICAgICB0aGlzW2tleV0gPSB7ZGF0YSwgZXhwaXJhdGlvbiwgcGF0aH07XHJcbiAgICB9XHJcblxyXG4gICAgLyoqIEBvdmVycmlkZSAqL1xyXG4gICAgY2xlYXIoKSB7XHJcbiAgICAgICAgT2JqZWN0LmtleXModGhpcykuZm9yRWFjaCgoa2V5KSA9PiB7XHJcbiAgICAgICAgICAgIGlmICh0aGlzLmhhc093blByb3BlcnR5KGtleSkgJiYgdHlwZW9mKHRoaXNba2V5XSkgIT09ICdmdW5jdGlvbicpIHtcclxuICAgICAgICAgICAgICAgIHRoaXNba2V5XSA9IHVuZGVmaW5lZDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==