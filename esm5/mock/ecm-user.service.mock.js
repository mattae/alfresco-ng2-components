/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/** @type {?} */
export var fakeEcmCompany = {
    organization: 'company-fake-name',
    address1: 'fake-address-1',
    address2: 'fake-address-2',
    address3: 'fake-address-3',
    postcode: 'fAk1',
    telephone: '00000000',
    fax: '11111111',
    email: 'fakeCompany@fake.com'
};
/** @type {?} */
export var fakeEcmUser = {
    id: 'fake-id',
    firstName: 'fake-ecm-first-name',
    lastName: 'fake-ecm-last-name',
    description: 'i am a fake user for test',
    avatarId: 'fake-avatar-id',
    email: 'fakeEcm@ecmUser.com',
    skypeId: 'fake-skype-id',
    googleId: 'fake-googleId-id',
    instantMessageId: 'fake-instantMessageId-id',
    company: null,
    jobTitle: 'job-ecm-test',
    location: 'fake location',
    mobile: '000000000',
    telephone: '11111111',
    statusUpdatedAt: 'fake-date',
    userStatus: 'active',
    enabled: true,
    emailNotificationsEnabled: true
};
/** @type {?} */
export var fakeEcmUserNoImage = {
    id: 'fake-id',
    firstName: 'fake-first-name',
    lastName: 'fake-last-name',
    description: 'i am a fake user for test',
    avatarId: null,
    email: 'fakeEcm@ecmUser.com',
    skypeId: 'fake-skype-id',
    googleId: 'fake-googleId-id',
    instantMessageId: 'fake-instantMessageId-id',
    company: null,
    jobTitle: null,
    location: 'fake location',
    mobile: '000000000',
    telephone: '11111111',
    statusUpdatedAt: 'fake-date',
    userStatus: 'active',
    enabled: true,
    emailNotificationsEnabled: true
};
/** @type {?} */
export var fakeEcmEditedUser = {
    id: 'fake-id',
    firstName: null,
    lastName: 'fake-last-name',
    description: 'i am a fake user for test',
    avatarId: 'fake-avatar-id',
    email: 'fakeEcm@ecmUser.com',
    skypeId: 'fake-skype-id',
    googleId: 'fake-googleId-id',
    instantMessageId: 'fake-instantMessageId-id',
    company: null,
    jobTitle: 'test job',
    location: 'fake location',
    mobile: '000000000',
    telephone: '11111111',
    statusUpdatedAt: 'fake-date',
    userStatus: 'active',
    enabled: true,
    emailNotificationsEnabled: true
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWNtLXVzZXIuc2VydmljZS5tb2NrLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsibW9jay9lY20tdXNlci5zZXJ2aWNlLm1vY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLE1BQU0sS0FBSyxjQUFjLEdBQW9CO0lBQ3pDLFlBQVksRUFBRSxtQkFBbUI7SUFDakMsUUFBUSxFQUFFLGdCQUFnQjtJQUMxQixRQUFRLEVBQUUsZ0JBQWdCO0lBQzFCLFFBQVEsRUFBRSxnQkFBZ0I7SUFDMUIsUUFBUSxFQUFFLE1BQU07SUFDaEIsU0FBUyxFQUFFLFVBQVU7SUFDckIsR0FBRyxFQUFFLFVBQVU7SUFDZixLQUFLLEVBQUUsc0JBQXNCO0NBQ2hDOztBQUVELE1BQU0sS0FBSyxXQUFXLEdBQUc7SUFDckIsRUFBRSxFQUFFLFNBQVM7SUFDYixTQUFTLEVBQUUscUJBQXFCO0lBQ2hDLFFBQVEsRUFBRSxvQkFBb0I7SUFDOUIsV0FBVyxFQUFFLDJCQUEyQjtJQUN4QyxRQUFRLEVBQUUsZ0JBQWdCO0lBQzFCLEtBQUssRUFBRSxxQkFBcUI7SUFDNUIsT0FBTyxFQUFFLGVBQWU7SUFDeEIsUUFBUSxFQUFFLGtCQUFrQjtJQUM1QixnQkFBZ0IsRUFBRSwwQkFBMEI7SUFDNUMsT0FBTyxFQUFFLElBQUk7SUFDYixRQUFRLEVBQUUsY0FBYztJQUN4QixRQUFRLEVBQUUsZUFBZTtJQUN6QixNQUFNLEVBQUUsV0FBVztJQUNuQixTQUFTLEVBQUUsVUFBVTtJQUNyQixlQUFlLEVBQUUsV0FBVztJQUM1QixVQUFVLEVBQUUsUUFBUTtJQUNwQixPQUFPLEVBQUUsSUFBSTtJQUNiLHlCQUF5QixFQUFFLElBQUk7Q0FDbEM7O0FBRUQsTUFBTSxLQUFLLGtCQUFrQixHQUFHO0lBQzVCLEVBQUUsRUFBRSxTQUFTO0lBQ2IsU0FBUyxFQUFFLGlCQUFpQjtJQUM1QixRQUFRLEVBQUUsZ0JBQWdCO0lBQzFCLFdBQVcsRUFBRSwyQkFBMkI7SUFDeEMsUUFBUSxFQUFFLElBQUk7SUFDZCxLQUFLLEVBQUUscUJBQXFCO0lBQzVCLE9BQU8sRUFBRSxlQUFlO0lBQ3hCLFFBQVEsRUFBRSxrQkFBa0I7SUFDNUIsZ0JBQWdCLEVBQUUsMEJBQTBCO0lBQzVDLE9BQU8sRUFBRSxJQUFJO0lBQ2IsUUFBUSxFQUFFLElBQUk7SUFDZCxRQUFRLEVBQUUsZUFBZTtJQUN6QixNQUFNLEVBQUUsV0FBVztJQUNuQixTQUFTLEVBQUUsVUFBVTtJQUNyQixlQUFlLEVBQUUsV0FBVztJQUM1QixVQUFVLEVBQUUsUUFBUTtJQUNwQixPQUFPLEVBQUUsSUFBSTtJQUNiLHlCQUF5QixFQUFFLElBQUk7Q0FDbEM7O0FBRUQsTUFBTSxLQUFLLGlCQUFpQixHQUFHO0lBQzNCLEVBQUUsRUFBRSxTQUFTO0lBQ2IsU0FBUyxFQUFFLElBQUk7SUFDZixRQUFRLEVBQUUsZ0JBQWdCO0lBQzFCLFdBQVcsRUFBRSwyQkFBMkI7SUFDeEMsUUFBUSxFQUFFLGdCQUFnQjtJQUMxQixLQUFLLEVBQUUscUJBQXFCO0lBQzVCLE9BQU8sRUFBRSxlQUFlO0lBQ3hCLFFBQVEsRUFBRSxrQkFBa0I7SUFDNUIsZ0JBQWdCLEVBQUUsMEJBQTBCO0lBQzVDLE9BQU8sRUFBRSxJQUFJO0lBQ2IsUUFBUSxFQUFFLFVBQVU7SUFDcEIsUUFBUSxFQUFFLGVBQWU7SUFDekIsTUFBTSxFQUFFLFdBQVc7SUFDbkIsU0FBUyxFQUFFLFVBQVU7SUFDckIsZUFBZSxFQUFFLFdBQVc7SUFDNUIsVUFBVSxFQUFFLFFBQVE7SUFDcEIsT0FBTyxFQUFFLElBQUk7SUFDYix5QkFBeUIsRUFBRSxJQUFJO0NBQ2xDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEVjbUNvbXBhbnlNb2RlbCB9IGZyb20gJy4uL21vZGVscy9lY20tY29tcGFueS5tb2RlbCc7XHJcblxyXG5leHBvcnQgbGV0IGZha2VFY21Db21wYW55OiBFY21Db21wYW55TW9kZWwgPSB7XHJcbiAgICBvcmdhbml6YXRpb246ICdjb21wYW55LWZha2UtbmFtZScsXHJcbiAgICBhZGRyZXNzMTogJ2Zha2UtYWRkcmVzcy0xJyxcclxuICAgIGFkZHJlc3MyOiAnZmFrZS1hZGRyZXNzLTInLFxyXG4gICAgYWRkcmVzczM6ICdmYWtlLWFkZHJlc3MtMycsXHJcbiAgICBwb3N0Y29kZTogJ2ZBazEnLFxyXG4gICAgdGVsZXBob25lOiAnMDAwMDAwMDAnLFxyXG4gICAgZmF4OiAnMTExMTExMTEnLFxyXG4gICAgZW1haWw6ICdmYWtlQ29tcGFueUBmYWtlLmNvbSdcclxufTtcclxuXHJcbmV4cG9ydCBsZXQgZmFrZUVjbVVzZXIgPSB7XHJcbiAgICBpZDogJ2Zha2UtaWQnLFxyXG4gICAgZmlyc3ROYW1lOiAnZmFrZS1lY20tZmlyc3QtbmFtZScsXHJcbiAgICBsYXN0TmFtZTogJ2Zha2UtZWNtLWxhc3QtbmFtZScsXHJcbiAgICBkZXNjcmlwdGlvbjogJ2kgYW0gYSBmYWtlIHVzZXIgZm9yIHRlc3QnLFxyXG4gICAgYXZhdGFySWQ6ICdmYWtlLWF2YXRhci1pZCcsXHJcbiAgICBlbWFpbDogJ2Zha2VFY21AZWNtVXNlci5jb20nLFxyXG4gICAgc2t5cGVJZDogJ2Zha2Utc2t5cGUtaWQnLFxyXG4gICAgZ29vZ2xlSWQ6ICdmYWtlLWdvb2dsZUlkLWlkJyxcclxuICAgIGluc3RhbnRNZXNzYWdlSWQ6ICdmYWtlLWluc3RhbnRNZXNzYWdlSWQtaWQnLFxyXG4gICAgY29tcGFueTogbnVsbCxcclxuICAgIGpvYlRpdGxlOiAnam9iLWVjbS10ZXN0JyxcclxuICAgIGxvY2F0aW9uOiAnZmFrZSBsb2NhdGlvbicsXHJcbiAgICBtb2JpbGU6ICcwMDAwMDAwMDAnLFxyXG4gICAgdGVsZXBob25lOiAnMTExMTExMTEnLFxyXG4gICAgc3RhdHVzVXBkYXRlZEF0OiAnZmFrZS1kYXRlJyxcclxuICAgIHVzZXJTdGF0dXM6ICdhY3RpdmUnLFxyXG4gICAgZW5hYmxlZDogdHJ1ZSxcclxuICAgIGVtYWlsTm90aWZpY2F0aW9uc0VuYWJsZWQ6IHRydWVcclxufTtcclxuXHJcbmV4cG9ydCBsZXQgZmFrZUVjbVVzZXJOb0ltYWdlID0ge1xyXG4gICAgaWQ6ICdmYWtlLWlkJyxcclxuICAgIGZpcnN0TmFtZTogJ2Zha2UtZmlyc3QtbmFtZScsXHJcbiAgICBsYXN0TmFtZTogJ2Zha2UtbGFzdC1uYW1lJyxcclxuICAgIGRlc2NyaXB0aW9uOiAnaSBhbSBhIGZha2UgdXNlciBmb3IgdGVzdCcsXHJcbiAgICBhdmF0YXJJZDogbnVsbCxcclxuICAgIGVtYWlsOiAnZmFrZUVjbUBlY21Vc2VyLmNvbScsXHJcbiAgICBza3lwZUlkOiAnZmFrZS1za3lwZS1pZCcsXHJcbiAgICBnb29nbGVJZDogJ2Zha2UtZ29vZ2xlSWQtaWQnLFxyXG4gICAgaW5zdGFudE1lc3NhZ2VJZDogJ2Zha2UtaW5zdGFudE1lc3NhZ2VJZC1pZCcsXHJcbiAgICBjb21wYW55OiBudWxsLFxyXG4gICAgam9iVGl0bGU6IG51bGwsXHJcbiAgICBsb2NhdGlvbjogJ2Zha2UgbG9jYXRpb24nLFxyXG4gICAgbW9iaWxlOiAnMDAwMDAwMDAwJyxcclxuICAgIHRlbGVwaG9uZTogJzExMTExMTExJyxcclxuICAgIHN0YXR1c1VwZGF0ZWRBdDogJ2Zha2UtZGF0ZScsXHJcbiAgICB1c2VyU3RhdHVzOiAnYWN0aXZlJyxcclxuICAgIGVuYWJsZWQ6IHRydWUsXHJcbiAgICBlbWFpbE5vdGlmaWNhdGlvbnNFbmFibGVkOiB0cnVlXHJcbn07XHJcblxyXG5leHBvcnQgbGV0IGZha2VFY21FZGl0ZWRVc2VyID0ge1xyXG4gICAgaWQ6ICdmYWtlLWlkJyxcclxuICAgIGZpcnN0TmFtZTogbnVsbCxcclxuICAgIGxhc3ROYW1lOiAnZmFrZS1sYXN0LW5hbWUnLFxyXG4gICAgZGVzY3JpcHRpb246ICdpIGFtIGEgZmFrZSB1c2VyIGZvciB0ZXN0JyxcclxuICAgIGF2YXRhcklkOiAnZmFrZS1hdmF0YXItaWQnLFxyXG4gICAgZW1haWw6ICdmYWtlRWNtQGVjbVVzZXIuY29tJyxcclxuICAgIHNreXBlSWQ6ICdmYWtlLXNreXBlLWlkJyxcclxuICAgIGdvb2dsZUlkOiAnZmFrZS1nb29nbGVJZC1pZCcsXHJcbiAgICBpbnN0YW50TWVzc2FnZUlkOiAnZmFrZS1pbnN0YW50TWVzc2FnZUlkLWlkJyxcclxuICAgIGNvbXBhbnk6IG51bGwsXHJcbiAgICBqb2JUaXRsZTogJ3Rlc3Qgam9iJyxcclxuICAgIGxvY2F0aW9uOiAnZmFrZSBsb2NhdGlvbicsXHJcbiAgICBtb2JpbGU6ICcwMDAwMDAwMDAnLFxyXG4gICAgdGVsZXBob25lOiAnMTExMTExMTEnLFxyXG4gICAgc3RhdHVzVXBkYXRlZEF0OiAnZmFrZS1kYXRlJyxcclxuICAgIHVzZXJTdGF0dXM6ICdhY3RpdmUnLFxyXG4gICAgZW5hYmxlZDogdHJ1ZSxcclxuICAgIGVtYWlsTm90aWZpY2F0aW9uc0VuYWJsZWQ6IHRydWVcclxufTtcclxuIl19