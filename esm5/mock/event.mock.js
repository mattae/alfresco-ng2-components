/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var EventMock = /** @class */ (function () {
    function EventMock() {
    }
    /**
     * @param {?} key
     * @return {?}
     */
    EventMock.keyDown = /**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        /** @type {?} */
        var event = document.createEvent('Event');
        event.keyCode = key;
        event.initEvent('keydown');
        document.dispatchEvent(event);
    };
    /**
     * @param {?} key
     * @return {?}
     */
    EventMock.keyUp = /**
     * @param {?} key
     * @return {?}
     */
    function (key) {
        /** @type {?} */
        var event = document.createEvent('Event');
        event.keyCode = key;
        event.initEvent('keyup');
        document.dispatchEvent(event);
    };
    /**
     * @return {?}
     */
    EventMock.resizeMobileView = /**
     * @return {?}
     */
    function () {
        // todo: no longer compiles with TS 2.0.2 as innerWidth/innerHeight are readonly fields
        /*
        window.innerWidth = 320;
        window.innerHeight = 568;
        */
        window.dispatchEvent(new Event('resize'));
    };
    return EventMock;
}());
export { EventMock };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnQubW9jay5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vY2svZXZlbnQubW9jay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQTtJQUFBO0lBd0JBLENBQUM7Ozs7O0lBdEJVLGlCQUFPOzs7O0lBQWQsVUFBZSxHQUFROztZQUNiLEtBQUssR0FBUSxRQUFRLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQztRQUNoRCxLQUFLLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUNwQixLQUFLLENBQUMsU0FBUyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQzNCLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbEMsQ0FBQzs7Ozs7SUFFTSxlQUFLOzs7O0lBQVosVUFBYSxHQUFROztZQUNYLEtBQUssR0FBUSxRQUFRLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQztRQUNoRCxLQUFLLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztRQUNwQixLQUFLLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3pCLFFBQVEsQ0FBQyxhQUFhLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDbEMsQ0FBQzs7OztJQUVNLDBCQUFnQjs7O0lBQXZCO1FBQ0ksdUZBQXVGO1FBQ3ZGOzs7VUFHRTtRQUNGLE1BQU0sQ0FBQyxhQUFhLENBQUMsSUFBSSxLQUFLLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQztJQUM5QyxDQUFDO0lBQ0wsZ0JBQUM7QUFBRCxDQUFDLEFBeEJELElBd0JDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmV4cG9ydCBjbGFzcyBFdmVudE1vY2sge1xyXG5cclxuICAgIHN0YXRpYyBrZXlEb3duKGtleTogYW55KSB7XHJcbiAgICAgICAgY29uc3QgZXZlbnQ6IGFueSA9IGRvY3VtZW50LmNyZWF0ZUV2ZW50KCdFdmVudCcpO1xyXG4gICAgICAgIGV2ZW50LmtleUNvZGUgPSBrZXk7XHJcbiAgICAgICAgZXZlbnQuaW5pdEV2ZW50KCdrZXlkb3duJyk7XHJcbiAgICAgICAgZG9jdW1lbnQuZGlzcGF0Y2hFdmVudChldmVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIGtleVVwKGtleTogYW55KSB7XHJcbiAgICAgICAgY29uc3QgZXZlbnQ6IGFueSA9IGRvY3VtZW50LmNyZWF0ZUV2ZW50KCdFdmVudCcpO1xyXG4gICAgICAgIGV2ZW50LmtleUNvZGUgPSBrZXk7XHJcbiAgICAgICAgZXZlbnQuaW5pdEV2ZW50KCdrZXl1cCcpO1xyXG4gICAgICAgIGRvY3VtZW50LmRpc3BhdGNoRXZlbnQoZXZlbnQpO1xyXG4gICAgfVxyXG5cclxuICAgIHN0YXRpYyByZXNpemVNb2JpbGVWaWV3KCkge1xyXG4gICAgICAgIC8vIHRvZG86IG5vIGxvbmdlciBjb21waWxlcyB3aXRoIFRTIDIuMC4yIGFzIGlubmVyV2lkdGgvaW5uZXJIZWlnaHQgYXJlIHJlYWRvbmx5IGZpZWxkc1xyXG4gICAgICAgIC8qXHJcbiAgICAgICAgd2luZG93LmlubmVyV2lkdGggPSAzMjA7XHJcbiAgICAgICAgd2luZG93LmlubmVySGVpZ2h0ID0gNTY4O1xyXG4gICAgICAgICovXHJcbiAgICAgICAgd2luZG93LmRpc3BhdGNoRXZlbnQobmV3IEV2ZW50KCdyZXNpemUnKSk7XHJcbiAgICB9XHJcbn1cclxuIl19