/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/** @type {?} */
export var fakeForm = {
    id: 1001,
    name: 'ISSUE_FORM',
    processDefinitionId: 'ISSUE_APP:1:2504',
    processDefinitionName: 'ISSUE_APP',
    processDefinitionKey: 'ISSUE_APP',
    taskId: '7506',
    taskDefinitionKey: 'sid-F67A2996-1684-4774-855A-4591490081FD',
    tabs: [],
    fields: [
        {
            fieldType: 'ContainerRepresentation',
            id: '1498212398417',
            name: 'Label',
            type: 'container',
            value: null,
            required: false,
            readOnly: false,
            overrideId: false,
            colspan: 1,
            placeholder: null,
            minLength: 0,
            maxLength: 0,
            minValue: null,
            maxValue: null,
            regexPattern: null,
            optionType: null,
            hasEmptyValue: null,
            options: null,
            restUrl: null,
            restResponsePath: null,
            restIdProperty: null,
            restLabelProperty: null,
            tab: null,
            className: null,
            dateDisplayFormat: null,
            layout: null,
            sizeX: 2,
            sizeY: 1,
            row: -1,
            col: -1,
            visibilityCondition: null,
            numberOfColumns: 2,
            fields: {
                1: [
                    {
                        fieldType: 'RestFieldRepresentation',
                        id: 'label',
                        name: 'Label',
                        type: 'dropdown',
                        value: 'Choose one...',
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: true,
                        options: [
                            {
                                id: 'empty',
                                name: 'Choose one...'
                            },
                            {
                                id: 'option_1',
                                name: 'test1'
                            },
                            {
                                id: 'option_2',
                                name: 'test2'
                            },
                            {
                                id: 'option_3',
                                name: 'test3'
                            }
                        ],
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 2
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null,
                        endpoint: null,
                        requestHeaders: null
                    }
                ],
                2: [
                    {
                        fieldType: 'RestFieldRepresentation',
                        id: 'radio',
                        name: 'radio',
                        type: 'radio-buttons',
                        value: null,
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: [
                            {
                                id: 'option_1',
                                name: 'Option 1'
                            },
                            {
                                id: 'option_2',
                                name: 'Option 2'
                            },
                            {
                                id: 'option_3',
                                name: 'Option 3'
                            }
                        ],
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 1
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 2,
                        row: -1,
                        col: -1,
                        visibilityCondition: null,
                        endpoint: null,
                        requestHeaders: null
                    }
                ]
            }
        },
        {
            fieldType: 'ContainerRepresentation',
            id: '1498212413062',
            name: 'Label',
            type: 'container',
            value: null,
            required: false,
            readOnly: false,
            overrideId: false,
            colspan: 1,
            placeholder: null,
            minLength: 0,
            maxLength: 0,
            minValue: null,
            maxValue: null,
            regexPattern: null,
            optionType: null,
            hasEmptyValue: null,
            options: null,
            restUrl: null,
            restResponsePath: null,
            restIdProperty: null,
            restLabelProperty: null,
            tab: null,
            className: null,
            dateDisplayFormat: null,
            layout: null,
            sizeX: 2,
            sizeY: 1,
            row: -1,
            col: -1,
            visibilityCondition: null,
            numberOfColumns: 2,
            fields: {
                1: [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'date',
                        name: 'date',
                        type: 'date',
                        value: null,
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 2
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ],
                2: []
            }
        }
    ],
    outcomes: [],
    javascriptEvents: [],
    className: '',
    style: '',
    customFieldTemplates: {},
    metadata: {},
    variables: [],
    customFieldsValueInfo: {},
    gridsterForm: false,
    globalDateFormat: 'D-M-YYYY'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5jb21wb25lbnQubW9jay5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vY2svZm9ybS9mb3JtLmNvbXBvbmVudC5tb2NrLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxNQUFNLEtBQUssUUFBUSxHQUFHO0lBQ2xCLEVBQUUsRUFBRSxJQUFJO0lBQ1IsSUFBSSxFQUFFLFlBQVk7SUFDbEIsbUJBQW1CLEVBQUUsa0JBQWtCO0lBQ3ZDLHFCQUFxQixFQUFFLFdBQVc7SUFDbEMsb0JBQW9CLEVBQUUsV0FBVztJQUNqQyxNQUFNLEVBQUUsTUFBTTtJQUNkLGlCQUFpQixFQUFFLDBDQUEwQztJQUM3RCxJQUFJLEVBQUUsRUFBRTtJQUNSLE1BQU0sRUFBRTtRQUNKO1lBQ0ksU0FBUyxFQUFFLHlCQUF5QjtZQUNwQyxFQUFFLEVBQUUsZUFBZTtZQUNuQixJQUFJLEVBQUUsT0FBTztZQUNiLElBQUksRUFBRSxXQUFXO1lBQ2pCLEtBQUssRUFBRSxJQUFJO1lBQ1gsUUFBUSxFQUFFLEtBQUs7WUFDZixRQUFRLEVBQUUsS0FBSztZQUNmLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLE9BQU8sRUFBRSxDQUFDO1lBQ1YsV0FBVyxFQUFFLElBQUk7WUFDakIsU0FBUyxFQUFFLENBQUM7WUFDWixTQUFTLEVBQUUsQ0FBQztZQUNaLFFBQVEsRUFBRSxJQUFJO1lBQ2QsUUFBUSxFQUFFLElBQUk7WUFDZCxZQUFZLEVBQUUsSUFBSTtZQUNsQixVQUFVLEVBQUUsSUFBSTtZQUNoQixhQUFhLEVBQUUsSUFBSTtZQUNuQixPQUFPLEVBQUUsSUFBSTtZQUNiLE9BQU8sRUFBRSxJQUFJO1lBQ2IsZ0JBQWdCLEVBQUUsSUFBSTtZQUN0QixjQUFjLEVBQUUsSUFBSTtZQUNwQixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLEdBQUcsRUFBRSxJQUFJO1lBQ1QsU0FBUyxFQUFFLElBQUk7WUFDZixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLE1BQU0sRUFBRSxJQUFJO1lBQ1osS0FBSyxFQUFFLENBQUM7WUFDUixLQUFLLEVBQUUsQ0FBQztZQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBQ1AsbUJBQW1CLEVBQUUsSUFBSTtZQUN6QixlQUFlLEVBQUUsQ0FBQztZQUNsQixNQUFNLEVBQUU7Z0JBQ0osQ0FBQyxFQUFFO29CQUNDO3dCQUNJLFNBQVMsRUFBRSx5QkFBeUI7d0JBQ3BDLEVBQUUsRUFBRSxPQUFPO3dCQUNYLElBQUksRUFBRSxPQUFPO3dCQUNiLElBQUksRUFBRSxVQUFVO3dCQUNoQixLQUFLLEVBQUUsZUFBZTt3QkFDdEIsUUFBUSxFQUFFLEtBQUs7d0JBQ2YsUUFBUSxFQUFFLEtBQUs7d0JBQ2YsVUFBVSxFQUFFLEtBQUs7d0JBQ2pCLE9BQU8sRUFBRSxDQUFDO3dCQUNWLFdBQVcsRUFBRSxJQUFJO3dCQUNqQixTQUFTLEVBQUUsQ0FBQzt3QkFDWixTQUFTLEVBQUUsQ0FBQzt3QkFDWixRQUFRLEVBQUUsSUFBSTt3QkFDZCxRQUFRLEVBQUUsSUFBSTt3QkFDZCxZQUFZLEVBQUUsSUFBSTt3QkFDbEIsVUFBVSxFQUFFLElBQUk7d0JBQ2hCLGFBQWEsRUFBRSxJQUFJO3dCQUNuQixPQUFPLEVBQUU7NEJBQ0w7Z0NBQ0ksRUFBRSxFQUFFLE9BQU87Z0NBQ1gsSUFBSSxFQUFFLGVBQWU7NkJBQ3hCOzRCQUNEO2dDQUNJLEVBQUUsRUFBRSxVQUFVO2dDQUNkLElBQUksRUFBRSxPQUFPOzZCQUNoQjs0QkFDRDtnQ0FDSSxFQUFFLEVBQUUsVUFBVTtnQ0FDZCxJQUFJLEVBQUUsT0FBTzs2QkFDaEI7NEJBQ0Q7Z0NBQ0ksRUFBRSxFQUFFLFVBQVU7Z0NBQ2QsSUFBSSxFQUFFLE9BQU87NkJBQ2hCO3lCQUNKO3dCQUNELE9BQU8sRUFBRSxJQUFJO3dCQUNiLGdCQUFnQixFQUFFLElBQUk7d0JBQ3RCLGNBQWMsRUFBRSxJQUFJO3dCQUNwQixpQkFBaUIsRUFBRSxJQUFJO3dCQUN2QixHQUFHLEVBQUUsSUFBSTt3QkFDVCxTQUFTLEVBQUUsSUFBSTt3QkFDZixNQUFNLEVBQUU7NEJBQ0osZUFBZSxFQUFFLENBQUM7NEJBQ2xCLFVBQVUsRUFBRSxDQUFDO3lCQUNoQjt3QkFDRCxpQkFBaUIsRUFBRSxJQUFJO3dCQUN2QixNQUFNLEVBQUU7NEJBQ0osR0FBRyxFQUFFLENBQUMsQ0FBQzs0QkFDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDOzRCQUNWLE9BQU8sRUFBRSxDQUFDO3lCQUNiO3dCQUNELEtBQUssRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxDQUFDO3dCQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxtQkFBbUIsRUFBRSxJQUFJO3dCQUN6QixRQUFRLEVBQUUsSUFBSTt3QkFDZCxjQUFjLEVBQUUsSUFBSTtxQkFDdkI7aUJBQ0o7Z0JBQ0QsQ0FBQyxFQUFFO29CQUNDO3dCQUNJLFNBQVMsRUFBRSx5QkFBeUI7d0JBQ3BDLEVBQUUsRUFBRSxPQUFPO3dCQUNYLElBQUksRUFBRSxPQUFPO3dCQUNiLElBQUksRUFBRSxlQUFlO3dCQUNyQixLQUFLLEVBQUUsSUFBSTt3QkFDWCxRQUFRLEVBQUUsS0FBSzt3QkFDZixRQUFRLEVBQUUsS0FBSzt3QkFDZixVQUFVLEVBQUUsS0FBSzt3QkFDakIsT0FBTyxFQUFFLENBQUM7d0JBQ1YsV0FBVyxFQUFFLElBQUk7d0JBQ2pCLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFlBQVksRUFBRSxJQUFJO3dCQUNsQixVQUFVLEVBQUUsSUFBSTt3QkFDaEIsYUFBYSxFQUFFLElBQUk7d0JBQ25CLE9BQU8sRUFBRTs0QkFDTDtnQ0FDSSxFQUFFLEVBQUUsVUFBVTtnQ0FDZCxJQUFJLEVBQUUsVUFBVTs2QkFDbkI7NEJBQ0Q7Z0NBQ0ksRUFBRSxFQUFFLFVBQVU7Z0NBQ2QsSUFBSSxFQUFFLFVBQVU7NkJBQ25COzRCQUNEO2dDQUNJLEVBQUUsRUFBRSxVQUFVO2dDQUNkLElBQUksRUFBRSxVQUFVOzZCQUNuQjt5QkFDSjt3QkFDRCxPQUFPLEVBQUUsSUFBSTt3QkFDYixnQkFBZ0IsRUFBRSxJQUFJO3dCQUN0QixjQUFjLEVBQUUsSUFBSTt3QkFDcEIsaUJBQWlCLEVBQUUsSUFBSTt3QkFDdkIsR0FBRyxFQUFFLElBQUk7d0JBQ1QsU0FBUyxFQUFFLElBQUk7d0JBQ2YsTUFBTSxFQUFFOzRCQUNKLGVBQWUsRUFBRSxDQUFDOzRCQUNsQixVQUFVLEVBQUUsQ0FBQzt5QkFDaEI7d0JBQ0QsaUJBQWlCLEVBQUUsSUFBSTt3QkFDdkIsTUFBTSxFQUFFOzRCQUNKLEdBQUcsRUFBRSxDQUFDLENBQUM7NEJBQ1AsTUFBTSxFQUFFLENBQUMsQ0FBQzs0QkFDVixPQUFPLEVBQUUsQ0FBQzt5QkFDYjt3QkFDRCxLQUFLLEVBQUUsQ0FBQzt3QkFDUixLQUFLLEVBQUUsQ0FBQzt3QkFDUixHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsbUJBQW1CLEVBQUUsSUFBSTt3QkFDekIsUUFBUSxFQUFFLElBQUk7d0JBQ2QsY0FBYyxFQUFFLElBQUk7cUJBQ3ZCO2lCQUNKO2FBQ0o7U0FDSjtRQUNEO1lBQ0ksU0FBUyxFQUFFLHlCQUF5QjtZQUNwQyxFQUFFLEVBQUUsZUFBZTtZQUNuQixJQUFJLEVBQUUsT0FBTztZQUNiLElBQUksRUFBRSxXQUFXO1lBQ2pCLEtBQUssRUFBRSxJQUFJO1lBQ1gsUUFBUSxFQUFFLEtBQUs7WUFDZixRQUFRLEVBQUUsS0FBSztZQUNmLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLE9BQU8sRUFBRSxDQUFDO1lBQ1YsV0FBVyxFQUFFLElBQUk7WUFDakIsU0FBUyxFQUFFLENBQUM7WUFDWixTQUFTLEVBQUUsQ0FBQztZQUNaLFFBQVEsRUFBRSxJQUFJO1lBQ2QsUUFBUSxFQUFFLElBQUk7WUFDZCxZQUFZLEVBQUUsSUFBSTtZQUNsQixVQUFVLEVBQUUsSUFBSTtZQUNoQixhQUFhLEVBQUUsSUFBSTtZQUNuQixPQUFPLEVBQUUsSUFBSTtZQUNiLE9BQU8sRUFBRSxJQUFJO1lBQ2IsZ0JBQWdCLEVBQUUsSUFBSTtZQUN0QixjQUFjLEVBQUUsSUFBSTtZQUNwQixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLEdBQUcsRUFBRSxJQUFJO1lBQ1QsU0FBUyxFQUFFLElBQUk7WUFDZixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLE1BQU0sRUFBRSxJQUFJO1lBQ1osS0FBSyxFQUFFLENBQUM7WUFDUixLQUFLLEVBQUUsQ0FBQztZQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBQ1AsbUJBQW1CLEVBQUUsSUFBSTtZQUN6QixlQUFlLEVBQUUsQ0FBQztZQUNsQixNQUFNLEVBQUU7Z0JBQ0osQ0FBQyxFQUFFO29CQUNDO3dCQUNJLFNBQVMsRUFBRSx5QkFBeUI7d0JBQ3BDLEVBQUUsRUFBRSxNQUFNO3dCQUNWLElBQUksRUFBRSxNQUFNO3dCQUNaLElBQUksRUFBRSxNQUFNO3dCQUNaLEtBQUssRUFBRSxJQUFJO3dCQUNYLFFBQVEsRUFBRSxLQUFLO3dCQUNmLFFBQVEsRUFBRSxLQUFLO3dCQUNmLFVBQVUsRUFBRSxLQUFLO3dCQUNqQixPQUFPLEVBQUUsQ0FBQzt3QkFDVixXQUFXLEVBQUUsSUFBSTt3QkFDakIsU0FBUyxFQUFFLENBQUM7d0JBQ1osU0FBUyxFQUFFLENBQUM7d0JBQ1osUUFBUSxFQUFFLElBQUk7d0JBQ2QsUUFBUSxFQUFFLElBQUk7d0JBQ2QsWUFBWSxFQUFFLElBQUk7d0JBQ2xCLFVBQVUsRUFBRSxJQUFJO3dCQUNoQixhQUFhLEVBQUUsSUFBSTt3QkFDbkIsT0FBTyxFQUFFLElBQUk7d0JBQ2IsT0FBTyxFQUFFLElBQUk7d0JBQ2IsZ0JBQWdCLEVBQUUsSUFBSTt3QkFDdEIsY0FBYyxFQUFFLElBQUk7d0JBQ3BCLGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLEdBQUcsRUFBRSxJQUFJO3dCQUNULFNBQVMsRUFBRSxJQUFJO3dCQUNmLE1BQU0sRUFBRTs0QkFDSixlQUFlLEVBQUUsQ0FBQzs0QkFDbEIsVUFBVSxFQUFFLENBQUM7eUJBQ2hCO3dCQUNELGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLE1BQU0sRUFBRTs0QkFDSixHQUFHLEVBQUUsQ0FBQyxDQUFDOzRCQUNQLE1BQU0sRUFBRSxDQUFDLENBQUM7NEJBQ1YsT0FBTyxFQUFFLENBQUM7eUJBQ2I7d0JBQ0QsS0FBSyxFQUFFLENBQUM7d0JBQ1IsS0FBSyxFQUFFLENBQUM7d0JBQ1IsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLG1CQUFtQixFQUFFLElBQUk7cUJBQzVCO2lCQUNKO2dCQUNELENBQUMsRUFBRSxFQUFFO2FBQ1I7U0FDSjtLQUNKO0lBQ0QsUUFBUSxFQUFFLEVBQUU7SUFDWixnQkFBZ0IsRUFBRSxFQUFFO0lBQ3BCLFNBQVMsRUFBRSxFQUFFO0lBQ2IsS0FBSyxFQUFFLEVBQUU7SUFDVCxvQkFBb0IsRUFBRSxFQUFFO0lBQ3hCLFFBQVEsRUFBRSxFQUFFO0lBQ1osU0FBUyxFQUFFLEVBQUU7SUFDYixxQkFBcUIsRUFBRSxFQUFFO0lBQ3pCLFlBQVksRUFBRSxLQUFLO0lBQ25CLGdCQUFnQixFQUFFLFVBQVU7Q0FDL0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuZXhwb3J0IGxldCBmYWtlRm9ybSA9IHtcclxuICAgIGlkOiAxMDAxLFxyXG4gICAgbmFtZTogJ0lTU1VFX0ZPUk0nLFxyXG4gICAgcHJvY2Vzc0RlZmluaXRpb25JZDogJ0lTU1VFX0FQUDoxOjI1MDQnLFxyXG4gICAgcHJvY2Vzc0RlZmluaXRpb25OYW1lOiAnSVNTVUVfQVBQJyxcclxuICAgIHByb2Nlc3NEZWZpbml0aW9uS2V5OiAnSVNTVUVfQVBQJyxcclxuICAgIHRhc2tJZDogJzc1MDYnLFxyXG4gICAgdGFza0RlZmluaXRpb25LZXk6ICdzaWQtRjY3QTI5OTYtMTY4NC00Nzc0LTg1NUEtNDU5MTQ5MDA4MUZEJyxcclxuICAgIHRhYnM6IFtdLFxyXG4gICAgZmllbGRzOiBbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBmaWVsZFR5cGU6ICdDb250YWluZXJSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgIGlkOiAnMTQ5ODIxMjM5ODQxNycsXHJcbiAgICAgICAgICAgIG5hbWU6ICdMYWJlbCcsXHJcbiAgICAgICAgICAgIHR5cGU6ICdjb250YWluZXInLFxyXG4gICAgICAgICAgICB2YWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgICAgIG92ZXJyaWRlSWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBjb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICBwbGFjZWhvbGRlcjogbnVsbCxcclxuICAgICAgICAgICAgbWluTGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICBtYXhMZW5ndGg6IDAsXHJcbiAgICAgICAgICAgIG1pblZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICBtYXhWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgcmVnZXhQYXR0ZXJuOiBudWxsLFxyXG4gICAgICAgICAgICBvcHRpb25UeXBlOiBudWxsLFxyXG4gICAgICAgICAgICBoYXNFbXB0eVZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICBvcHRpb25zOiBudWxsLFxyXG4gICAgICAgICAgICByZXN0VXJsOiBudWxsLFxyXG4gICAgICAgICAgICByZXN0UmVzcG9uc2VQYXRoOiBudWxsLFxyXG4gICAgICAgICAgICByZXN0SWRQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgcmVzdExhYmVsUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgIHRhYjogbnVsbCxcclxuICAgICAgICAgICAgY2xhc3NOYW1lOiBudWxsLFxyXG4gICAgICAgICAgICBkYXRlRGlzcGxheUZvcm1hdDogbnVsbCxcclxuICAgICAgICAgICAgbGF5b3V0OiBudWxsLFxyXG4gICAgICAgICAgICBzaXplWDogMixcclxuICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGwsXHJcbiAgICAgICAgICAgIG51bWJlck9mQ29sdW1uczogMixcclxuICAgICAgICAgICAgZmllbGRzOiB7XHJcbiAgICAgICAgICAgICAgICAxOiBbXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWVsZFR5cGU6ICdSZXN0RmllbGRSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnbGFiZWwnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnTGFiZWwnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnZHJvcGRvd24nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogJ0Nob29zZSBvbmUuLi4nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnM6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogJ2VtcHR5JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnQ2hvb3NlIG9uZS4uLidcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdvcHRpb25fMScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ3Rlc3QxJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogJ29wdGlvbl8yJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAndGVzdDInXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnb3B0aW9uXzMnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICd0ZXN0MydcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGlzdGluZ0NvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhDb2xzcGFuOiAyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYXlvdXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2x1bW46IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVuZHBvaW50OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1ZXN0SGVhZGVyczogbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICAyOiBbXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWVsZFR5cGU6ICdSZXN0RmllbGRSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAncmFkaW8nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAncmFkaW8nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAncmFkaW8tYnV0dG9ucycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnM6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogJ29wdGlvbl8xJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnT3B0aW9uIDEnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnb3B0aW9uXzInLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdPcHRpb24gMidcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdvcHRpb25fMycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ09wdGlvbiAzJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0VXJsOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0UmVzcG9uc2VQYXRoOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0SWRQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdExhYmVsUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhYjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4aXN0aW5nQ29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heENvbHNwYW46IDFcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0ZURpc3BsYXlGb3JtYXQ6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxheW91dDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbHVtbjogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xzcGFuOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpemVYOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWTogMixcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sOiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmlzaWJpbGl0eUNvbmRpdGlvbjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZW5kcG9pbnQ6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVlc3RIZWFkZXJzOiBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGZpZWxkVHlwZTogJ0NvbnRhaW5lclJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgaWQ6ICcxNDk4MjEyNDEzMDYyJyxcclxuICAgICAgICAgICAgbmFtZTogJ0xhYmVsJyxcclxuICAgICAgICAgICAgdHlwZTogJ2NvbnRhaW5lcicsXHJcbiAgICAgICAgICAgIHZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBudWxsLFxyXG4gICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIG9wdGlvbnM6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RVcmw6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RSZXNwb25zZVBhdGg6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RJZFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICByZXN0TGFiZWxQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgdGFiOiBudWxsLFxyXG4gICAgICAgICAgICBjbGFzc05hbWU6IG51bGwsXHJcbiAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICBsYXlvdXQ6IG51bGwsXHJcbiAgICAgICAgICAgIHNpemVYOiAyLFxyXG4gICAgICAgICAgICBzaXplWTogMSxcclxuICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgY29sOiAtMSxcclxuICAgICAgICAgICAgdmlzaWJpbGl0eUNvbmRpdGlvbjogbnVsbCxcclxuICAgICAgICAgICAgbnVtYmVyT2ZDb2x1bW5zOiAyLFxyXG4gICAgICAgICAgICBmaWVsZHM6IHtcclxuICAgICAgICAgICAgICAgIDE6IFtcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkVHlwZTogJ0Zvcm1GaWVsZFJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdkYXRlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ2RhdGUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnZGF0ZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnM6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RVcmw6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RSZXNwb25zZVBhdGg6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RJZFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0TGFiZWxQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGFiOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmFtczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhpc3RpbmdDb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF4Q29sc3BhbjogMlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRlRGlzcGxheUZvcm1hdDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGF5b3V0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sdW1uOiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDFcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVg6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpemVZOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2w6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2aXNpYmlsaXR5Q29uZGl0aW9uOiBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgIDI6IFtdXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICBdLFxyXG4gICAgb3V0Y29tZXM6IFtdLFxyXG4gICAgamF2YXNjcmlwdEV2ZW50czogW10sXHJcbiAgICBjbGFzc05hbWU6ICcnLFxyXG4gICAgc3R5bGU6ICcnLFxyXG4gICAgY3VzdG9tRmllbGRUZW1wbGF0ZXM6IHt9LFxyXG4gICAgbWV0YWRhdGE6IHt9LFxyXG4gICAgdmFyaWFibGVzOiBbXSxcclxuICAgIGN1c3RvbUZpZWxkc1ZhbHVlSW5mbzoge30sXHJcbiAgICBncmlkc3RlckZvcm06IGZhbHNlLFxyXG4gICAgZ2xvYmFsRGF0ZUZvcm1hdDogJ0QtTS1ZWVlZJ1xyXG59O1xyXG4iXX0=