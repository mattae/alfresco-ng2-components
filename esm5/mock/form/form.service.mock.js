/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/** @type {?} */
export var formModelTabs = {
    id: 16,
    name: 'start event',
    description: '',
    version: 2,
    lastUpdatedBy: 4,
    lastUpdatedByFullName: 'User Test',
    lastUpdated: '2017-10-04T13:00:03.030+0000',
    stencilSetId: null,
    referenceId: null,
    formDefinition: {
        tabs: [],
        fields: [
            {
                fieldType: 'ContainerRepresentation',
                id: '1507037668653',
                name: 'Label',
                type: 'container',
                value: null,
                required: false,
                readOnly: false,
                overrideId: false,
                colspan: 1,
                placeholder: null,
                minLength: 0,
                maxLength: 0,
                minValue: null,
                maxValue: null,
                regexPattern: null,
                optionType: null,
                hasEmptyValue: null,
                options: null,
                restUrl: null,
                restResponsePath: null,
                restIdProperty: null,
                restLabelProperty: null,
                tab: null,
                className: null,
                dateDisplayFormat: null,
                layout: null,
                sizeX: 2,
                sizeY: 1,
                row: -1,
                col: -1,
                visibilityCondition: null,
                numberOfColumns: 2,
                fields: {
                    '1': [
                        {
                            fieldType: 'AmountFieldRepresentation',
                            id: 'label',
                            name: 'Label',
                            type: 'amount',
                            value: null,
                            required: false,
                            readOnly: false,
                            overrideId: false,
                            colspan: 1,
                            placeholder: null,
                            minLength: 0,
                            maxLength: 0,
                            minValue: null,
                            maxValue: null,
                            regexPattern: null,
                            optionType: null,
                            hasEmptyValue: null,
                            options: null,
                            restUrl: null,
                            restResponsePath: null,
                            restIdProperty: null,
                            restLabelProperty: null,
                            tab: null,
                            className: null,
                            params: {
                                existingColspan: 1,
                                maxColspan: 2
                            },
                            dateDisplayFormat: null,
                            layout: {
                                row: -1,
                                column: -1,
                                colspan: 1
                            },
                            sizeX: 1,
                            sizeY: 1,
                            row: -1,
                            col: -1,
                            visibilityCondition: null,
                            enableFractions: false,
                            currency: null
                        }
                    ],
                    '2': [
                        {
                            fieldType: 'FormFieldRepresentation',
                            id: 'label1',
                            name: 'Label1',
                            type: 'date',
                            value: null,
                            required: false,
                            readOnly: false,
                            overrideId: false,
                            colspan: 1,
                            placeholder: null,
                            minLength: 0,
                            maxLength: 0,
                            minValue: null,
                            maxValue: null,
                            regexPattern: null,
                            optionType: null,
                            hasEmptyValue: null,
                            options: null,
                            restUrl: null,
                            restResponsePath: null,
                            restIdProperty: null,
                            restLabelProperty: null,
                            tab: null,
                            className: null,
                            params: {
                                existingColspan: 1,
                                maxColspan: 1
                            },
                            dateDisplayFormat: null,
                            layout: {
                                row: -1,
                                column: -1,
                                colspan: 1
                            },
                            sizeX: 1,
                            sizeY: 1,
                            row: -1,
                            col: -1,
                            visibilityCondition: null
                        }
                    ]
                }
            },
            {
                fieldType: 'ContainerRepresentation',
                id: '1507037670167',
                name: 'Label',
                type: 'container',
                value: null,
                required: false,
                readOnly: false,
                overrideId: false,
                colspan: 1,
                placeholder: null,
                minLength: 0,
                maxLength: 0,
                minValue: null,
                maxValue: null,
                regexPattern: null,
                optionType: null,
                hasEmptyValue: null,
                options: null,
                restUrl: null,
                restResponsePath: null,
                restIdProperty: null,
                restLabelProperty: null,
                tab: null,
                className: null,
                dateDisplayFormat: null,
                layout: null,
                sizeX: 2,
                sizeY: 1,
                row: -1,
                col: -1,
                visibilityCondition: null,
                numberOfColumns: 2,
                fields: {
                    '1': [
                        {
                            fieldType: 'FormFieldRepresentation',
                            id: 'label2',
                            name: 'Label2',
                            type: 'boolean',
                            value: null,
                            required: false,
                            readOnly: false,
                            overrideId: false,
                            colspan: 1,
                            placeholder: null,
                            minLength: 0,
                            maxLength: 0,
                            minValue: null,
                            maxValue: null,
                            regexPattern: null,
                            optionType: null,
                            hasEmptyValue: null,
                            options: null,
                            restUrl: null,
                            restResponsePath: null,
                            restIdProperty: null,
                            restLabelProperty: null,
                            tab: null,
                            className: null,
                            params: {
                                existingColspan: 1,
                                maxColspan: 2
                            },
                            dateDisplayFormat: null,
                            layout: {
                                row: -1,
                                column: -1,
                                colspan: 1
                            },
                            sizeX: 1,
                            sizeY: 1,
                            row: -1,
                            col: -1,
                            visibilityCondition: null
                        }
                    ],
                    '2': []
                }
            }
        ],
        outcomes: [],
        javascriptEvents: [],
        className: '',
        style: '',
        customFieldTemplates: {},
        metadata: {},
        variables: [],
        customFieldsValueInfo: {},
        gridsterForm: false
    }
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybS5zZXJ2aWNlLm1vY2suanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJtb2NrL2Zvcm0vZm9ybS5zZXJ2aWNlLm1vY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE1BQU0sS0FBSyxhQUFhLEdBQUc7SUFDdkIsRUFBRSxFQUFFLEVBQUU7SUFDTixJQUFJLEVBQUUsYUFBYTtJQUNuQixXQUFXLEVBQUUsRUFBRTtJQUNmLE9BQU8sRUFBRSxDQUFDO0lBQ1YsYUFBYSxFQUFFLENBQUM7SUFDaEIscUJBQXFCLEVBQUUsV0FBVztJQUNsQyxXQUFXLEVBQUUsOEJBQThCO0lBQzNDLFlBQVksRUFBRSxJQUFJO0lBQ2xCLFdBQVcsRUFBRSxJQUFJO0lBQ2pCLGNBQWMsRUFBRTtRQUNkLElBQUksRUFBRSxFQUFFO1FBQ1IsTUFBTSxFQUFFO1lBQ047Z0JBQ0UsU0FBUyxFQUFFLHlCQUF5QjtnQkFDcEMsRUFBRSxFQUFFLGVBQWU7Z0JBQ25CLElBQUksRUFBRSxPQUFPO2dCQUNiLElBQUksRUFBRSxXQUFXO2dCQUNqQixLQUFLLEVBQUUsSUFBSTtnQkFDWCxRQUFRLEVBQUUsS0FBSztnQkFDZixRQUFRLEVBQUUsS0FBSztnQkFDZixVQUFVLEVBQUUsS0FBSztnQkFDakIsT0FBTyxFQUFFLENBQUM7Z0JBQ1YsV0FBVyxFQUFFLElBQUk7Z0JBQ2pCLFNBQVMsRUFBRSxDQUFDO2dCQUNaLFNBQVMsRUFBRSxDQUFDO2dCQUNaLFFBQVEsRUFBRSxJQUFJO2dCQUNkLFFBQVEsRUFBRSxJQUFJO2dCQUNkLFlBQVksRUFBRSxJQUFJO2dCQUNsQixVQUFVLEVBQUUsSUFBSTtnQkFDaEIsYUFBYSxFQUFFLElBQUk7Z0JBQ25CLE9BQU8sRUFBRSxJQUFJO2dCQUNiLE9BQU8sRUFBRSxJQUFJO2dCQUNiLGdCQUFnQixFQUFFLElBQUk7Z0JBQ3RCLGNBQWMsRUFBRSxJQUFJO2dCQUNwQixpQkFBaUIsRUFBRSxJQUFJO2dCQUN2QixHQUFHLEVBQUUsSUFBSTtnQkFDVCxTQUFTLEVBQUUsSUFBSTtnQkFDZixpQkFBaUIsRUFBRSxJQUFJO2dCQUN2QixNQUFNLEVBQUUsSUFBSTtnQkFDWixLQUFLLEVBQUUsQ0FBQztnQkFDUixLQUFLLEVBQUUsQ0FBQztnQkFDUixHQUFHLEVBQUUsQ0FBQyxDQUFDO2dCQUNQLEdBQUcsRUFBRSxDQUFDLENBQUM7Z0JBQ1AsbUJBQW1CLEVBQUUsSUFBSTtnQkFDekIsZUFBZSxFQUFFLENBQUM7Z0JBQ2xCLE1BQU0sRUFBRTtvQkFDTixHQUFHLEVBQUU7d0JBQ0g7NEJBQ0UsU0FBUyxFQUFFLDJCQUEyQjs0QkFDdEMsRUFBRSxFQUFFLE9BQU87NEJBQ1gsSUFBSSxFQUFFLE9BQU87NEJBQ2IsSUFBSSxFQUFFLFFBQVE7NEJBQ2QsS0FBSyxFQUFFLElBQUk7NEJBQ1gsUUFBUSxFQUFFLEtBQUs7NEJBQ2YsUUFBUSxFQUFFLEtBQUs7NEJBQ2YsVUFBVSxFQUFFLEtBQUs7NEJBQ2pCLE9BQU8sRUFBRSxDQUFDOzRCQUNWLFdBQVcsRUFBRSxJQUFJOzRCQUNqQixTQUFTLEVBQUUsQ0FBQzs0QkFDWixTQUFTLEVBQUUsQ0FBQzs0QkFDWixRQUFRLEVBQUUsSUFBSTs0QkFDZCxRQUFRLEVBQUUsSUFBSTs0QkFDZCxZQUFZLEVBQUUsSUFBSTs0QkFDbEIsVUFBVSxFQUFFLElBQUk7NEJBQ2hCLGFBQWEsRUFBRSxJQUFJOzRCQUNuQixPQUFPLEVBQUUsSUFBSTs0QkFDYixPQUFPLEVBQUUsSUFBSTs0QkFDYixnQkFBZ0IsRUFBRSxJQUFJOzRCQUN0QixjQUFjLEVBQUUsSUFBSTs0QkFDcEIsaUJBQWlCLEVBQUUsSUFBSTs0QkFDdkIsR0FBRyxFQUFFLElBQUk7NEJBQ1QsU0FBUyxFQUFFLElBQUk7NEJBQ2YsTUFBTSxFQUFFO2dDQUNOLGVBQWUsRUFBRSxDQUFDO2dDQUNsQixVQUFVLEVBQUUsQ0FBQzs2QkFDZDs0QkFDRCxpQkFBaUIsRUFBRSxJQUFJOzRCQUN2QixNQUFNLEVBQUU7Z0NBQ04sR0FBRyxFQUFFLENBQUMsQ0FBQztnQ0FDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDO2dDQUNWLE9BQU8sRUFBRSxDQUFDOzZCQUNYOzRCQUNELEtBQUssRUFBRSxDQUFDOzRCQUNSLEtBQUssRUFBRSxDQUFDOzRCQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7NEJBQ1AsR0FBRyxFQUFFLENBQUMsQ0FBQzs0QkFDUCxtQkFBbUIsRUFBRSxJQUFJOzRCQUN6QixlQUFlLEVBQUUsS0FBSzs0QkFDdEIsUUFBUSxFQUFFLElBQUk7eUJBQ2Y7cUJBQ0Y7b0JBQ0QsR0FBRyxFQUFFO3dCQUNIOzRCQUNFLFNBQVMsRUFBRSx5QkFBeUI7NEJBQ3BDLEVBQUUsRUFBRSxRQUFROzRCQUNaLElBQUksRUFBRSxRQUFROzRCQUNkLElBQUksRUFBRSxNQUFNOzRCQUNaLEtBQUssRUFBRSxJQUFJOzRCQUNYLFFBQVEsRUFBRSxLQUFLOzRCQUNmLFFBQVEsRUFBRSxLQUFLOzRCQUNmLFVBQVUsRUFBRSxLQUFLOzRCQUNqQixPQUFPLEVBQUUsQ0FBQzs0QkFDVixXQUFXLEVBQUUsSUFBSTs0QkFDakIsU0FBUyxFQUFFLENBQUM7NEJBQ1osU0FBUyxFQUFFLENBQUM7NEJBQ1osUUFBUSxFQUFFLElBQUk7NEJBQ2QsUUFBUSxFQUFFLElBQUk7NEJBQ2QsWUFBWSxFQUFFLElBQUk7NEJBQ2xCLFVBQVUsRUFBRSxJQUFJOzRCQUNoQixhQUFhLEVBQUUsSUFBSTs0QkFDbkIsT0FBTyxFQUFFLElBQUk7NEJBQ2IsT0FBTyxFQUFFLElBQUk7NEJBQ2IsZ0JBQWdCLEVBQUUsSUFBSTs0QkFDdEIsY0FBYyxFQUFFLElBQUk7NEJBQ3BCLGlCQUFpQixFQUFFLElBQUk7NEJBQ3ZCLEdBQUcsRUFBRSxJQUFJOzRCQUNULFNBQVMsRUFBRSxJQUFJOzRCQUNmLE1BQU0sRUFBRTtnQ0FDTixlQUFlLEVBQUUsQ0FBQztnQ0FDbEIsVUFBVSxFQUFFLENBQUM7NkJBQ2Q7NEJBQ0QsaUJBQWlCLEVBQUUsSUFBSTs0QkFDdkIsTUFBTSxFQUFFO2dDQUNOLEdBQUcsRUFBRSxDQUFDLENBQUM7Z0NBQ1AsTUFBTSxFQUFFLENBQUMsQ0FBQztnQ0FDVixPQUFPLEVBQUUsQ0FBQzs2QkFDWDs0QkFDRCxLQUFLLEVBQUUsQ0FBQzs0QkFDUixLQUFLLEVBQUUsQ0FBQzs0QkFDUixHQUFHLEVBQUUsQ0FBQyxDQUFDOzRCQUNQLEdBQUcsRUFBRSxDQUFDLENBQUM7NEJBQ1AsbUJBQW1CLEVBQUUsSUFBSTt5QkFDMUI7cUJBQ0Y7aUJBQ0Y7YUFDRjtZQUNEO2dCQUNFLFNBQVMsRUFBRSx5QkFBeUI7Z0JBQ3BDLEVBQUUsRUFBRSxlQUFlO2dCQUNuQixJQUFJLEVBQUUsT0FBTztnQkFDYixJQUFJLEVBQUUsV0FBVztnQkFDakIsS0FBSyxFQUFFLElBQUk7Z0JBQ1gsUUFBUSxFQUFFLEtBQUs7Z0JBQ2YsUUFBUSxFQUFFLEtBQUs7Z0JBQ2YsVUFBVSxFQUFFLEtBQUs7Z0JBQ2pCLE9BQU8sRUFBRSxDQUFDO2dCQUNWLFdBQVcsRUFBRSxJQUFJO2dCQUNqQixTQUFTLEVBQUUsQ0FBQztnQkFDWixTQUFTLEVBQUUsQ0FBQztnQkFDWixRQUFRLEVBQUUsSUFBSTtnQkFDZCxRQUFRLEVBQUUsSUFBSTtnQkFDZCxZQUFZLEVBQUUsSUFBSTtnQkFDbEIsVUFBVSxFQUFFLElBQUk7Z0JBQ2hCLGFBQWEsRUFBRSxJQUFJO2dCQUNuQixPQUFPLEVBQUUsSUFBSTtnQkFDYixPQUFPLEVBQUUsSUFBSTtnQkFDYixnQkFBZ0IsRUFBRSxJQUFJO2dCQUN0QixjQUFjLEVBQUUsSUFBSTtnQkFDcEIsaUJBQWlCLEVBQUUsSUFBSTtnQkFDdkIsR0FBRyxFQUFFLElBQUk7Z0JBQ1QsU0FBUyxFQUFFLElBQUk7Z0JBQ2YsaUJBQWlCLEVBQUUsSUFBSTtnQkFDdkIsTUFBTSxFQUFFLElBQUk7Z0JBQ1osS0FBSyxFQUFFLENBQUM7Z0JBQ1IsS0FBSyxFQUFFLENBQUM7Z0JBQ1IsR0FBRyxFQUFFLENBQUMsQ0FBQztnQkFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO2dCQUNQLG1CQUFtQixFQUFFLElBQUk7Z0JBQ3pCLGVBQWUsRUFBRSxDQUFDO2dCQUNsQixNQUFNLEVBQUU7b0JBQ04sR0FBRyxFQUFFO3dCQUNIOzRCQUNFLFNBQVMsRUFBRSx5QkFBeUI7NEJBQ3BDLEVBQUUsRUFBRSxRQUFROzRCQUNaLElBQUksRUFBRSxRQUFROzRCQUNkLElBQUksRUFBRSxTQUFTOzRCQUNmLEtBQUssRUFBRSxJQUFJOzRCQUNYLFFBQVEsRUFBRSxLQUFLOzRCQUNmLFFBQVEsRUFBRSxLQUFLOzRCQUNmLFVBQVUsRUFBRSxLQUFLOzRCQUNqQixPQUFPLEVBQUUsQ0FBQzs0QkFDVixXQUFXLEVBQUUsSUFBSTs0QkFDakIsU0FBUyxFQUFFLENBQUM7NEJBQ1osU0FBUyxFQUFFLENBQUM7NEJBQ1osUUFBUSxFQUFFLElBQUk7NEJBQ2QsUUFBUSxFQUFFLElBQUk7NEJBQ2QsWUFBWSxFQUFFLElBQUk7NEJBQ2xCLFVBQVUsRUFBRSxJQUFJOzRCQUNoQixhQUFhLEVBQUUsSUFBSTs0QkFDbkIsT0FBTyxFQUFFLElBQUk7NEJBQ2IsT0FBTyxFQUFFLElBQUk7NEJBQ2IsZ0JBQWdCLEVBQUUsSUFBSTs0QkFDdEIsY0FBYyxFQUFFLElBQUk7NEJBQ3BCLGlCQUFpQixFQUFFLElBQUk7NEJBQ3ZCLEdBQUcsRUFBRSxJQUFJOzRCQUNULFNBQVMsRUFBRSxJQUFJOzRCQUNmLE1BQU0sRUFBRTtnQ0FDTixlQUFlLEVBQUUsQ0FBQztnQ0FDbEIsVUFBVSxFQUFFLENBQUM7NkJBQ2Q7NEJBQ0QsaUJBQWlCLEVBQUUsSUFBSTs0QkFDdkIsTUFBTSxFQUFFO2dDQUNOLEdBQUcsRUFBRSxDQUFDLENBQUM7Z0NBQ1AsTUFBTSxFQUFFLENBQUMsQ0FBQztnQ0FDVixPQUFPLEVBQUUsQ0FBQzs2QkFDWDs0QkFDRCxLQUFLLEVBQUUsQ0FBQzs0QkFDUixLQUFLLEVBQUUsQ0FBQzs0QkFDUixHQUFHLEVBQUUsQ0FBQyxDQUFDOzRCQUNQLEdBQUcsRUFBRSxDQUFDLENBQUM7NEJBQ1AsbUJBQW1CLEVBQUUsSUFBSTt5QkFDMUI7cUJBQ0Y7b0JBQ0QsR0FBRyxFQUFFLEVBQUU7aUJBQ1I7YUFDRjtTQUNGO1FBQ0QsUUFBUSxFQUFFLEVBQUU7UUFDWixnQkFBZ0IsRUFBRSxFQUFFO1FBQ3BCLFNBQVMsRUFBRSxFQUFFO1FBQ2IsS0FBSyxFQUFFLEVBQUU7UUFDVCxvQkFBb0IsRUFBRSxFQUFFO1FBQ3hCLFFBQVEsRUFBRSxFQUFFO1FBQ1osU0FBUyxFQUFFLEVBQUU7UUFDYixxQkFBcUIsRUFBRSxFQUFFO1FBQ3pCLFlBQVksRUFBRSxLQUFLO0tBQ3BCO0NBQ0YiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuZXhwb3J0IGxldCBmb3JtTW9kZWxUYWJzID0ge1xyXG4gICAgaWQ6IDE2LFxyXG4gICAgbmFtZTogJ3N0YXJ0IGV2ZW50JyxcclxuICAgIGRlc2NyaXB0aW9uOiAnJyxcclxuICAgIHZlcnNpb246IDIsXHJcbiAgICBsYXN0VXBkYXRlZEJ5OiA0LFxyXG4gICAgbGFzdFVwZGF0ZWRCeUZ1bGxOYW1lOiAnVXNlciBUZXN0JyxcclxuICAgIGxhc3RVcGRhdGVkOiAnMjAxNy0xMC0wNFQxMzowMDowMy4wMzArMDAwMCcsXHJcbiAgICBzdGVuY2lsU2V0SWQ6IG51bGwsXHJcbiAgICByZWZlcmVuY2VJZDogbnVsbCxcclxuICAgIGZvcm1EZWZpbml0aW9uOiB7XHJcbiAgICAgIHRhYnM6IFtdLFxyXG4gICAgICBmaWVsZHM6IFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICBmaWVsZFR5cGU6ICdDb250YWluZXJSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICBpZDogJzE1MDcwMzc2Njg2NTMnLFxyXG4gICAgICAgICAgbmFtZTogJ0xhYmVsJyxcclxuICAgICAgICAgIHR5cGU6ICdjb250YWluZXInLFxyXG4gICAgICAgICAgdmFsdWU6IG51bGwsXHJcbiAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgICBvdmVycmlkZUlkOiBmYWxzZSxcclxuICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICBwbGFjZWhvbGRlcjogbnVsbCxcclxuICAgICAgICAgIG1pbkxlbmd0aDogMCxcclxuICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgIG1pblZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgbWF4VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICBvcHRpb25UeXBlOiBudWxsLFxyXG4gICAgICAgICAgaGFzRW1wdHlWYWx1ZTogbnVsbCxcclxuICAgICAgICAgIG9wdGlvbnM6IG51bGwsXHJcbiAgICAgICAgICByZXN0VXJsOiBudWxsLFxyXG4gICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgIHJlc3RJZFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgcmVzdExhYmVsUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICBjbGFzc05hbWU6IG51bGwsXHJcbiAgICAgICAgICBkYXRlRGlzcGxheUZvcm1hdDogbnVsbCxcclxuICAgICAgICAgIGxheW91dDogbnVsbCxcclxuICAgICAgICAgIHNpemVYOiAyLFxyXG4gICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgY29sOiAtMSxcclxuICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGwsXHJcbiAgICAgICAgICBudW1iZXJPZkNvbHVtbnM6IDIsXHJcbiAgICAgICAgICBmaWVsZHM6IHtcclxuICAgICAgICAgICAgJzEnOiBbXHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgZmllbGRUeXBlOiAnQW1vdW50RmllbGRSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgICAgICBpZDogJ2xhYmVsJyxcclxuICAgICAgICAgICAgICAgIG5hbWU6ICdMYWJlbCcsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnYW1vdW50JyxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBjb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICBtYXhMZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICBtaW5WYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgcmVnZXhQYXR0ZXJuOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgb3B0aW9uVHlwZTogbnVsbCxcclxuICAgICAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBvcHRpb25zOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgICAgIHJlc3RSZXNwb25zZVBhdGg6IG51bGwsXHJcbiAgICAgICAgICAgICAgICByZXN0SWRQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgdGFiOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgIGV4aXN0aW5nQ29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgbWF4Q29sc3BhbjogMlxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgbGF5b3V0OiB7XHJcbiAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgIGNvbHVtbjogLTEsXHJcbiAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDFcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBzaXplWDogMSxcclxuICAgICAgICAgICAgICAgIHNpemVZOiAxLFxyXG4gICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgICAgICB2aXNpYmlsaXR5Q29uZGl0aW9uOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgZW5hYmxlRnJhY3Rpb25zOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIGN1cnJlbmN5OiBudWxsXHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAnMic6IFtcclxuICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBmaWVsZFR5cGU6ICdGb3JtRmllbGRSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgICAgICBpZDogJ2xhYmVsMScsXHJcbiAgICAgICAgICAgICAgICBuYW1lOiAnTGFiZWwxJyxcclxuICAgICAgICAgICAgICAgIHR5cGU6ICdkYXRlJyxcclxuICAgICAgICAgICAgICAgIHZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICBjb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICBtYXhMZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICBtaW5WYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgcmVnZXhQYXR0ZXJuOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgb3B0aW9uVHlwZTogbnVsbCxcclxuICAgICAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBvcHRpb25zOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgICAgIHJlc3RSZXNwb25zZVBhdGg6IG51bGwsXHJcbiAgICAgICAgICAgICAgICByZXN0SWRQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgdGFiOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgY2xhc3NOYW1lOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgIGV4aXN0aW5nQ29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgbWF4Q29sc3BhbjogMVxyXG4gICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgbGF5b3V0OiB7XHJcbiAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgIGNvbHVtbjogLTEsXHJcbiAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDFcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBzaXplWDogMSxcclxuICAgICAgICAgICAgICAgIHNpemVZOiAxLFxyXG4gICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgICAgICB2aXNpYmlsaXR5Q29uZGl0aW9uOiBudWxsXHJcbiAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBdXHJcbiAgICAgICAgICB9XHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICBmaWVsZFR5cGU6ICdDb250YWluZXJSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICBpZDogJzE1MDcwMzc2NzAxNjcnLFxyXG4gICAgICAgICAgbmFtZTogJ0xhYmVsJyxcclxuICAgICAgICAgIHR5cGU6ICdjb250YWluZXInLFxyXG4gICAgICAgICAgdmFsdWU6IG51bGwsXHJcbiAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgICBvdmVycmlkZUlkOiBmYWxzZSxcclxuICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICBwbGFjZWhvbGRlcjogbnVsbCxcclxuICAgICAgICAgIG1pbkxlbmd0aDogMCxcclxuICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgIG1pblZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgbWF4VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICBvcHRpb25UeXBlOiBudWxsLFxyXG4gICAgICAgICAgaGFzRW1wdHlWYWx1ZTogbnVsbCxcclxuICAgICAgICAgIG9wdGlvbnM6IG51bGwsXHJcbiAgICAgICAgICByZXN0VXJsOiBudWxsLFxyXG4gICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgIHJlc3RJZFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgcmVzdExhYmVsUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICBjbGFzc05hbWU6IG51bGwsXHJcbiAgICAgICAgICBkYXRlRGlzcGxheUZvcm1hdDogbnVsbCxcclxuICAgICAgICAgIGxheW91dDogbnVsbCxcclxuICAgICAgICAgIHNpemVYOiAyLFxyXG4gICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgY29sOiAtMSxcclxuICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGwsXHJcbiAgICAgICAgICBudW1iZXJPZkNvbHVtbnM6IDIsXHJcbiAgICAgICAgICBmaWVsZHM6IHtcclxuICAgICAgICAgICAgJzEnOiBbXHJcbiAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgZmllbGRUeXBlOiAnRm9ybUZpZWxkUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICAgICAgaWQ6ICdsYWJlbDInLFxyXG4gICAgICAgICAgICAgICAgbmFtZTogJ0xhYmVsMicsXHJcbiAgICAgICAgICAgICAgICB0eXBlOiAnYm9vbGVhbicsXHJcbiAgICAgICAgICAgICAgICB2YWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIG92ZXJyaWRlSWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgY29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgbWluTGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgbWF4TGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBtYXhWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgIHJlZ2V4UGF0dGVybjogbnVsbCxcclxuICAgICAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICBoYXNFbXB0eVZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgb3B0aW9uczogbnVsbCxcclxuICAgICAgICAgICAgICAgIHJlc3RVcmw6IG51bGwsXHJcbiAgICAgICAgICAgICAgICByZXN0UmVzcG9uc2VQYXRoOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICByZXN0TGFiZWxQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgIHRhYjogbnVsbCxcclxuICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogbnVsbCxcclxuICAgICAgICAgICAgICAgIHBhcmFtczoge1xyXG4gICAgICAgICAgICAgICAgICBleGlzdGluZ0NvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgIG1heENvbHNwYW46IDJcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICBkYXRlRGlzcGxheUZvcm1hdDogbnVsbCxcclxuICAgICAgICAgICAgICAgIGxheW91dDoge1xyXG4gICAgICAgICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICAgICAgICBjb2x1bW46IC0xLFxyXG4gICAgICAgICAgICAgICAgICBjb2xzcGFuOiAxXHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgc2l6ZVg6IDEsXHJcbiAgICAgICAgICAgICAgICBzaXplWTogMSxcclxuICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICBjb2w6IC0xLFxyXG4gICAgICAgICAgICAgICAgdmlzaWJpbGl0eUNvbmRpdGlvbjogbnVsbFxyXG4gICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgJzInOiBbXVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgXSxcclxuICAgICAgb3V0Y29tZXM6IFtdLFxyXG4gICAgICBqYXZhc2NyaXB0RXZlbnRzOiBbXSxcclxuICAgICAgY2xhc3NOYW1lOiAnJyxcclxuICAgICAgc3R5bGU6ICcnLFxyXG4gICAgICBjdXN0b21GaWVsZFRlbXBsYXRlczoge30sXHJcbiAgICAgIG1ldGFkYXRhOiB7fSxcclxuICAgICAgdmFyaWFibGVzOiBbXSxcclxuICAgICAgY3VzdG9tRmllbGRzVmFsdWVJbmZvOiB7fSxcclxuICAgICAgZ3JpZHN0ZXJGb3JtOiBmYWxzZVxyXG4gICAgfVxyXG4gIH07XHJcbiJdfQ==