/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/** @type {?} */
export var formReadonlyTwoTextFields = {
    id: 22,
    name: 'formTextDefinition',
    processDefinitionId: 'textDefinition:3:182',
    processDefinitionName: 'textDefinition',
    processDefinitionKey: 'textDefinition',
    taskId: '188',
    taskDefinitionKey: 'sid-D941F49F-2B04-4FBB-9B49-9E95991993E8',
    tabs: [],
    fields: [
        {
            fieldType: 'ContainerRepresentation',
            id: '1507044399260',
            name: 'Label',
            type: 'container',
            value: null,
            required: false,
            readOnly: false,
            overrideId: false,
            colspan: 1,
            placeholder: null,
            minLength: 0,
            maxLength: 0,
            minValue: null,
            maxValue: null,
            regexPattern: null,
            optionType: null,
            hasEmptyValue: null,
            options: null,
            restUrl: null,
            restResponsePath: null,
            restIdProperty: null,
            restLabelProperty: null,
            tab: null,
            className: null,
            dateDisplayFormat: null,
            layout: null,
            sizeX: 2,
            sizeY: 1,
            row: -1,
            col: -1,
            visibilityCondition: null,
            numberOfColumns: 2,
            fields: {
                '1': [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'firstname',
                        name: 'firstName',
                        type: 'readonly',
                        value: 'fakeFirstName',
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 2,
                            field: {
                                id: 'firstname',
                                name: 'firstName',
                                type: 'text',
                                value: null,
                                required: false,
                                readOnly: false,
                                overrideId: false,
                                colspan: 1,
                                placeholder: null,
                                minLength: 0,
                                maxLength: 0,
                                minValue: null,
                                maxValue: null,
                                regexPattern: null,
                                optionType: null,
                                hasEmptyValue: null,
                                options: null,
                                restUrl: null,
                                restResponsePath: null,
                                restIdProperty: null,
                                restLabelProperty: null,
                                tab: null,
                                className: null,
                                dateDisplayFormat: null,
                                layout: null,
                                sizeX: 0,
                                sizeY: 0,
                                row: 0,
                                col: 0,
                                visibilityCondition: null
                            }
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ],
                '2': [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'lastname',
                        name: 'lastName',
                        type: 'readonly',
                        value: 'fakeLastName',
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 1,
                            field: {
                                id: 'lastname',
                                name: 'lastName',
                                type: 'text',
                                value: null,
                                required: false,
                                readOnly: false,
                                overrideId: false,
                                colspan: 1,
                                placeholder: null,
                                minLength: 0,
                                maxLength: 0,
                                minValue: null,
                                maxValue: null,
                                regexPattern: null,
                                optionType: null,
                                hasEmptyValue: null,
                                options: null,
                                restUrl: null,
                                restResponsePath: null,
                                restIdProperty: null,
                                restLabelProperty: null,
                                tab: null,
                                className: null,
                                dateDisplayFormat: null,
                                layout: null,
                                sizeX: 0,
                                sizeY: 0,
                                row: 0,
                                col: 0,
                                visibilityCondition: null
                            }
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ]
            }
        }
    ],
    outcomes: [],
    javascriptEvents: [],
    className: '',
    style: '',
    customFieldTemplates: {},
    metadata: {},
    variables: [],
    customFieldsValueInfo: {},
    gridsterForm: false,
    globalDateFormat: 'D-M-YYYY'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybURlZmluaXRpb25SZWFkb25seS5tb2NrLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsibW9jay9mb3JtL2Zvcm1EZWZpbml0aW9uUmVhZG9ubHkubW9jay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsTUFBTSxLQUFLLHlCQUF5QixHQUFHO0lBQ25DLEVBQUUsRUFBRSxFQUFFO0lBQ04sSUFBSSxFQUFFLG9CQUFvQjtJQUMxQixtQkFBbUIsRUFBRSxzQkFBc0I7SUFDM0MscUJBQXFCLEVBQUUsZ0JBQWdCO0lBQ3ZDLG9CQUFvQixFQUFFLGdCQUFnQjtJQUN0QyxNQUFNLEVBQUUsS0FBSztJQUNiLGlCQUFpQixFQUFFLDBDQUEwQztJQUM3RCxJQUFJLEVBQUUsRUFBRTtJQUNSLE1BQU0sRUFBRTtRQUNOO1lBQ0UsU0FBUyxFQUFFLHlCQUF5QjtZQUNwQyxFQUFFLEVBQUUsZUFBZTtZQUNuQixJQUFJLEVBQUUsT0FBTztZQUNiLElBQUksRUFBRSxXQUFXO1lBQ2pCLEtBQUssRUFBRSxJQUFJO1lBQ1gsUUFBUSxFQUFFLEtBQUs7WUFDZixRQUFRLEVBQUUsS0FBSztZQUNmLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLE9BQU8sRUFBRSxDQUFDO1lBQ1YsV0FBVyxFQUFFLElBQUk7WUFDakIsU0FBUyxFQUFFLENBQUM7WUFDWixTQUFTLEVBQUUsQ0FBQztZQUNaLFFBQVEsRUFBRSxJQUFJO1lBQ2QsUUFBUSxFQUFFLElBQUk7WUFDZCxZQUFZLEVBQUUsSUFBSTtZQUNsQixVQUFVLEVBQUUsSUFBSTtZQUNoQixhQUFhLEVBQUUsSUFBSTtZQUNuQixPQUFPLEVBQUUsSUFBSTtZQUNiLE9BQU8sRUFBRSxJQUFJO1lBQ2IsZ0JBQWdCLEVBQUUsSUFBSTtZQUN0QixjQUFjLEVBQUUsSUFBSTtZQUNwQixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLEdBQUcsRUFBRSxJQUFJO1lBQ1QsU0FBUyxFQUFFLElBQUk7WUFDZixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLE1BQU0sRUFBRSxJQUFJO1lBQ1osS0FBSyxFQUFFLENBQUM7WUFDUixLQUFLLEVBQUUsQ0FBQztZQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBQ1AsbUJBQW1CLEVBQUUsSUFBSTtZQUN6QixlQUFlLEVBQUUsQ0FBQztZQUNsQixNQUFNLEVBQUU7Z0JBQ04sR0FBRyxFQUFFO29CQUNIO3dCQUNFLFNBQVMsRUFBRSx5QkFBeUI7d0JBQ3BDLEVBQUUsRUFBRSxXQUFXO3dCQUNmLElBQUksRUFBRSxXQUFXO3dCQUNqQixJQUFJLEVBQUUsVUFBVTt3QkFDaEIsS0FBSyxFQUFFLGVBQWU7d0JBQ3RCLFFBQVEsRUFBRSxLQUFLO3dCQUNmLFFBQVEsRUFBRSxLQUFLO3dCQUNmLFVBQVUsRUFBRSxLQUFLO3dCQUNqQixPQUFPLEVBQUUsQ0FBQzt3QkFDVixXQUFXLEVBQUUsSUFBSTt3QkFDakIsU0FBUyxFQUFFLENBQUM7d0JBQ1osU0FBUyxFQUFFLENBQUM7d0JBQ1osUUFBUSxFQUFFLElBQUk7d0JBQ2QsUUFBUSxFQUFFLElBQUk7d0JBQ2QsWUFBWSxFQUFFLElBQUk7d0JBQ2xCLFVBQVUsRUFBRSxJQUFJO3dCQUNoQixhQUFhLEVBQUUsSUFBSTt3QkFDbkIsT0FBTyxFQUFFLElBQUk7d0JBQ2IsT0FBTyxFQUFFLElBQUk7d0JBQ2IsZ0JBQWdCLEVBQUUsSUFBSTt3QkFDdEIsY0FBYyxFQUFFLElBQUk7d0JBQ3BCLGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLEdBQUcsRUFBRSxJQUFJO3dCQUNULFNBQVMsRUFBRSxJQUFJO3dCQUNmLE1BQU0sRUFBRTs0QkFDTixlQUFlLEVBQUUsQ0FBQzs0QkFDbEIsVUFBVSxFQUFFLENBQUM7NEJBQ2IsS0FBSyxFQUFFO2dDQUNMLEVBQUUsRUFBRSxXQUFXO2dDQUNmLElBQUksRUFBRSxXQUFXO2dDQUNqQixJQUFJLEVBQUUsTUFBTTtnQ0FDWixLQUFLLEVBQUUsSUFBSTtnQ0FDWCxRQUFRLEVBQUUsS0FBSztnQ0FDZixRQUFRLEVBQUUsS0FBSztnQ0FDZixVQUFVLEVBQUUsS0FBSztnQ0FDakIsT0FBTyxFQUFFLENBQUM7Z0NBQ1YsV0FBVyxFQUFFLElBQUk7Z0NBQ2pCLFNBQVMsRUFBRSxDQUFDO2dDQUNaLFNBQVMsRUFBRSxDQUFDO2dDQUNaLFFBQVEsRUFBRSxJQUFJO2dDQUNkLFFBQVEsRUFBRSxJQUFJO2dDQUNkLFlBQVksRUFBRSxJQUFJO2dDQUNsQixVQUFVLEVBQUUsSUFBSTtnQ0FDaEIsYUFBYSxFQUFFLElBQUk7Z0NBQ25CLE9BQU8sRUFBRSxJQUFJO2dDQUNiLE9BQU8sRUFBRSxJQUFJO2dDQUNiLGdCQUFnQixFQUFFLElBQUk7Z0NBQ3RCLGNBQWMsRUFBRSxJQUFJO2dDQUNwQixpQkFBaUIsRUFBRSxJQUFJO2dDQUN2QixHQUFHLEVBQUUsSUFBSTtnQ0FDVCxTQUFTLEVBQUUsSUFBSTtnQ0FDZixpQkFBaUIsRUFBRSxJQUFJO2dDQUN2QixNQUFNLEVBQUUsSUFBSTtnQ0FDWixLQUFLLEVBQUUsQ0FBQztnQ0FDUixLQUFLLEVBQUUsQ0FBQztnQ0FDUixHQUFHLEVBQUUsQ0FBQztnQ0FDTixHQUFHLEVBQUUsQ0FBQztnQ0FDTixtQkFBbUIsRUFBRSxJQUFJOzZCQUMxQjt5QkFDRjt3QkFDRCxpQkFBaUIsRUFBRSxJQUFJO3dCQUN2QixNQUFNLEVBQUU7NEJBQ04sR0FBRyxFQUFFLENBQUMsQ0FBQzs0QkFDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDOzRCQUNWLE9BQU8sRUFBRSxDQUFDO3lCQUNYO3dCQUNELEtBQUssRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxDQUFDO3dCQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxtQkFBbUIsRUFBRSxJQUFJO3FCQUMxQjtpQkFDRjtnQkFDRCxHQUFHLEVBQUU7b0JBQ0g7d0JBQ0UsU0FBUyxFQUFFLHlCQUF5Qjt3QkFDcEMsRUFBRSxFQUFFLFVBQVU7d0JBQ2QsSUFBSSxFQUFFLFVBQVU7d0JBQ2hCLElBQUksRUFBRSxVQUFVO3dCQUNoQixLQUFLLEVBQUUsY0FBYzt3QkFDckIsUUFBUSxFQUFFLEtBQUs7d0JBQ2YsUUFBUSxFQUFFLEtBQUs7d0JBQ2YsVUFBVSxFQUFFLEtBQUs7d0JBQ2pCLE9BQU8sRUFBRSxDQUFDO3dCQUNWLFdBQVcsRUFBRSxJQUFJO3dCQUNqQixTQUFTLEVBQUUsQ0FBQzt3QkFDWixTQUFTLEVBQUUsQ0FBQzt3QkFDWixRQUFRLEVBQUUsSUFBSTt3QkFDZCxRQUFRLEVBQUUsSUFBSTt3QkFDZCxZQUFZLEVBQUUsSUFBSTt3QkFDbEIsVUFBVSxFQUFFLElBQUk7d0JBQ2hCLGFBQWEsRUFBRSxJQUFJO3dCQUNuQixPQUFPLEVBQUUsSUFBSTt3QkFDYixPQUFPLEVBQUUsSUFBSTt3QkFDYixnQkFBZ0IsRUFBRSxJQUFJO3dCQUN0QixjQUFjLEVBQUUsSUFBSTt3QkFDcEIsaUJBQWlCLEVBQUUsSUFBSTt3QkFDdkIsR0FBRyxFQUFFLElBQUk7d0JBQ1QsU0FBUyxFQUFFLElBQUk7d0JBQ2YsTUFBTSxFQUFFOzRCQUNOLGVBQWUsRUFBRSxDQUFDOzRCQUNsQixVQUFVLEVBQUUsQ0FBQzs0QkFDYixLQUFLLEVBQUU7Z0NBQ0wsRUFBRSxFQUFFLFVBQVU7Z0NBQ2QsSUFBSSxFQUFFLFVBQVU7Z0NBQ2hCLElBQUksRUFBRSxNQUFNO2dDQUNaLEtBQUssRUFBRSxJQUFJO2dDQUNYLFFBQVEsRUFBRSxLQUFLO2dDQUNmLFFBQVEsRUFBRSxLQUFLO2dDQUNmLFVBQVUsRUFBRSxLQUFLO2dDQUNqQixPQUFPLEVBQUUsQ0FBQztnQ0FDVixXQUFXLEVBQUUsSUFBSTtnQ0FDakIsU0FBUyxFQUFFLENBQUM7Z0NBQ1osU0FBUyxFQUFFLENBQUM7Z0NBQ1osUUFBUSxFQUFFLElBQUk7Z0NBQ2QsUUFBUSxFQUFFLElBQUk7Z0NBQ2QsWUFBWSxFQUFFLElBQUk7Z0NBQ2xCLFVBQVUsRUFBRSxJQUFJO2dDQUNoQixhQUFhLEVBQUUsSUFBSTtnQ0FDbkIsT0FBTyxFQUFFLElBQUk7Z0NBQ2IsT0FBTyxFQUFFLElBQUk7Z0NBQ2IsZ0JBQWdCLEVBQUUsSUFBSTtnQ0FDdEIsY0FBYyxFQUFFLElBQUk7Z0NBQ3BCLGlCQUFpQixFQUFFLElBQUk7Z0NBQ3ZCLEdBQUcsRUFBRSxJQUFJO2dDQUNULFNBQVMsRUFBRSxJQUFJO2dDQUNmLGlCQUFpQixFQUFFLElBQUk7Z0NBQ3ZCLE1BQU0sRUFBRSxJQUFJO2dDQUNaLEtBQUssRUFBRSxDQUFDO2dDQUNSLEtBQUssRUFBRSxDQUFDO2dDQUNSLEdBQUcsRUFBRSxDQUFDO2dDQUNOLEdBQUcsRUFBRSxDQUFDO2dDQUNOLG1CQUFtQixFQUFFLElBQUk7NkJBQzFCO3lCQUNGO3dCQUNELGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLE1BQU0sRUFBRTs0QkFDTixHQUFHLEVBQUUsQ0FBQyxDQUFDOzRCQUNQLE1BQU0sRUFBRSxDQUFDLENBQUM7NEJBQ1YsT0FBTyxFQUFFLENBQUM7eUJBQ1g7d0JBQ0QsS0FBSyxFQUFFLENBQUM7d0JBQ1IsS0FBSyxFQUFFLENBQUM7d0JBQ1IsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLG1CQUFtQixFQUFFLElBQUk7cUJBQzFCO2lCQUNGO2FBQ0Y7U0FDRjtLQUNGO0lBQ0QsUUFBUSxFQUFFLEVBQUU7SUFDWixnQkFBZ0IsRUFBRSxFQUFFO0lBQ3BCLFNBQVMsRUFBRSxFQUFFO0lBQ2IsS0FBSyxFQUFFLEVBQUU7SUFDVCxvQkFBb0IsRUFBRSxFQUFFO0lBQ3hCLFFBQVEsRUFBRSxFQUFFO0lBQ1osU0FBUyxFQUFFLEVBQUU7SUFDYixxQkFBcUIsRUFBRSxFQUFFO0lBQ3pCLFlBQVksRUFBRSxLQUFLO0lBQ25CLGdCQUFnQixFQUFFLFVBQVU7Q0FDN0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuZXhwb3J0IGxldCBmb3JtUmVhZG9ubHlUd29UZXh0RmllbGRzID0ge1xyXG4gICAgaWQ6IDIyLFxyXG4gICAgbmFtZTogJ2Zvcm1UZXh0RGVmaW5pdGlvbicsXHJcbiAgICBwcm9jZXNzRGVmaW5pdGlvbklkOiAndGV4dERlZmluaXRpb246MzoxODInLFxyXG4gICAgcHJvY2Vzc0RlZmluaXRpb25OYW1lOiAndGV4dERlZmluaXRpb24nLFxyXG4gICAgcHJvY2Vzc0RlZmluaXRpb25LZXk6ICd0ZXh0RGVmaW5pdGlvbicsXHJcbiAgICB0YXNrSWQ6ICcxODgnLFxyXG4gICAgdGFza0RlZmluaXRpb25LZXk6ICdzaWQtRDk0MUY0OUYtMkIwNC00RkJCLTlCNDktOUU5NTk5MTk5M0U4JyxcclxuICAgIHRhYnM6IFtdLFxyXG4gICAgZmllbGRzOiBbXHJcbiAgICAgIHtcclxuICAgICAgICBmaWVsZFR5cGU6ICdDb250YWluZXJSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgaWQ6ICcxNTA3MDQ0Mzk5MjYwJyxcclxuICAgICAgICBuYW1lOiAnTGFiZWwnLFxyXG4gICAgICAgIHR5cGU6ICdjb250YWluZXInLFxyXG4gICAgICAgIHZhbHVlOiBudWxsLFxyXG4gICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgY29sc3BhbjogMSxcclxuICAgICAgICBwbGFjZWhvbGRlcjogbnVsbCxcclxuICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgbWF4TGVuZ3RoOiAwLFxyXG4gICAgICAgIG1pblZhbHVlOiBudWxsLFxyXG4gICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgIHJlZ2V4UGF0dGVybjogbnVsbCxcclxuICAgICAgICBvcHRpb25UeXBlOiBudWxsLFxyXG4gICAgICAgIGhhc0VtcHR5VmFsdWU6IG51bGwsXHJcbiAgICAgICAgb3B0aW9uczogbnVsbCxcclxuICAgICAgICByZXN0VXJsOiBudWxsLFxyXG4gICAgICAgIHJlc3RSZXNwb25zZVBhdGg6IG51bGwsXHJcbiAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgcmVzdExhYmVsUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgdGFiOiBudWxsLFxyXG4gICAgICAgIGNsYXNzTmFtZTogbnVsbCxcclxuICAgICAgICBkYXRlRGlzcGxheUZvcm1hdDogbnVsbCxcclxuICAgICAgICBsYXlvdXQ6IG51bGwsXHJcbiAgICAgICAgc2l6ZVg6IDIsXHJcbiAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgcm93OiAtMSxcclxuICAgICAgICBjb2w6IC0xLFxyXG4gICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGwsXHJcbiAgICAgICAgbnVtYmVyT2ZDb2x1bW5zOiAyLFxyXG4gICAgICAgIGZpZWxkczoge1xyXG4gICAgICAgICAgJzEnOiBbXHJcbiAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICBmaWVsZFR5cGU6ICdGb3JtRmllbGRSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgICAgaWQ6ICdmaXJzdG5hbWUnLFxyXG4gICAgICAgICAgICAgIG5hbWU6ICdmaXJzdE5hbWUnLFxyXG4gICAgICAgICAgICAgIHR5cGU6ICdyZWFkb25seScsXHJcbiAgICAgICAgICAgICAgdmFsdWU6ICdmYWtlRmlyc3ROYW1lJyxcclxuICAgICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICAgIG92ZXJyaWRlSWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IG51bGwsXHJcbiAgICAgICAgICAgICAgbWluTGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgICAgICBtaW5WYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICBtYXhWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICAgICAgb3B0aW9uVHlwZTogbnVsbCxcclxuICAgICAgICAgICAgICBoYXNFbXB0eVZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgIG9wdGlvbnM6IG51bGwsXHJcbiAgICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgICByZXN0UmVzcG9uc2VQYXRoOiBudWxsLFxyXG4gICAgICAgICAgICAgIHJlc3RJZFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgIHRhYjogbnVsbCxcclxuICAgICAgICAgICAgICBjbGFzc05hbWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICBleGlzdGluZ0NvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICBtYXhDb2xzcGFuOiAyLFxyXG4gICAgICAgICAgICAgICAgZmllbGQ6IHtcclxuICAgICAgICAgICAgICAgICAgaWQ6ICdmaXJzdG5hbWUnLFxyXG4gICAgICAgICAgICAgICAgICBuYW1lOiAnZmlyc3ROYW1lJyxcclxuICAgICAgICAgICAgICAgICAgdHlwZTogJ3RleHQnLFxyXG4gICAgICAgICAgICAgICAgICB2YWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgIG92ZXJyaWRlSWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICBjb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgbWluTGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgICBtYXhMZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICAgIG1pblZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICBtYXhWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgcmVnZXhQYXR0ZXJuOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICBvcHRpb25UeXBlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICBoYXNFbXB0eVZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICBvcHRpb25zOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICByZXN0VXJsOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICByZXN0UmVzcG9uc2VQYXRoOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICByZXN0SWRQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgcmVzdExhYmVsUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgIHRhYjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICBkYXRlRGlzcGxheUZvcm1hdDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgbGF5b3V0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICBzaXplWDogMCxcclxuICAgICAgICAgICAgICAgICAgc2l6ZVk6IDAsXHJcbiAgICAgICAgICAgICAgICAgIHJvdzogMCxcclxuICAgICAgICAgICAgICAgICAgY29sOiAwLFxyXG4gICAgICAgICAgICAgICAgICB2aXNpYmlsaXR5Q29uZGl0aW9uOiBudWxsXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICBkYXRlRGlzcGxheUZvcm1hdDogbnVsbCxcclxuICAgICAgICAgICAgICBsYXlvdXQ6IHtcclxuICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICBjb2x1bW46IC0xLFxyXG4gICAgICAgICAgICAgICAgY29sc3BhbjogMVxyXG4gICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgc2l6ZVg6IDEsXHJcbiAgICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICBjb2w6IC0xLFxyXG4gICAgICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGxcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgXSxcclxuICAgICAgICAgICcyJzogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgZmllbGRUeXBlOiAnRm9ybUZpZWxkUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICAgIGlkOiAnbGFzdG5hbWUnLFxyXG4gICAgICAgICAgICAgIG5hbWU6ICdsYXN0TmFtZScsXHJcbiAgICAgICAgICAgICAgdHlwZTogJ3JlYWRvbmx5JyxcclxuICAgICAgICAgICAgICB2YWx1ZTogJ2Zha2VMYXN0TmFtZScsXHJcbiAgICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgICBvdmVycmlkZUlkOiBmYWxzZSxcclxuICAgICAgICAgICAgICBjb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBudWxsLFxyXG4gICAgICAgICAgICAgIG1pbkxlbmd0aDogMCxcclxuICAgICAgICAgICAgICBtYXhMZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgbWF4VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgcmVnZXhQYXR0ZXJuOiBudWxsLFxyXG4gICAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgICAgaGFzRW1wdHlWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICBvcHRpb25zOiBudWxsLFxyXG4gICAgICAgICAgICAgIHJlc3RVcmw6IG51bGwsXHJcbiAgICAgICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgICAgICByZXN0SWRQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICByZXN0TGFiZWxQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICAgICAgY2xhc3NOYW1lOiBudWxsLFxyXG4gICAgICAgICAgICAgIHBhcmFtczoge1xyXG4gICAgICAgICAgICAgICAgZXhpc3RpbmdDb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgbWF4Q29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgIGZpZWxkOiB7XHJcbiAgICAgICAgICAgICAgICAgIGlkOiAnbGFzdG5hbWUnLFxyXG4gICAgICAgICAgICAgICAgICBuYW1lOiAnbGFzdE5hbWUnLFxyXG4gICAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgIHZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgIG9wdGlvbnM6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgIHJlc3RVcmw6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgIHJlc3RSZXNwb25zZVBhdGg6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgIHJlc3RJZFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICByZXN0TGFiZWxQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgdGFiOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICBjbGFzc05hbWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICBsYXlvdXQ6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgIHNpemVYOiAwLFxyXG4gICAgICAgICAgICAgICAgICBzaXplWTogMCxcclxuICAgICAgICAgICAgICAgICAgcm93OiAwLFxyXG4gICAgICAgICAgICAgICAgICBjb2w6IDAsXHJcbiAgICAgICAgICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGxcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICAgIGxheW91dDoge1xyXG4gICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgIGNvbHVtbjogLTEsXHJcbiAgICAgICAgICAgICAgICBjb2xzcGFuOiAxXHJcbiAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICBzaXplWDogMSxcclxuICAgICAgICAgICAgICBzaXplWTogMSxcclxuICAgICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgICAgdmlzaWJpbGl0eUNvbmRpdGlvbjogbnVsbFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICBdXHJcbiAgICAgICAgfVxyXG4gICAgICB9XHJcbiAgICBdLFxyXG4gICAgb3V0Y29tZXM6IFtdLFxyXG4gICAgamF2YXNjcmlwdEV2ZW50czogW10sXHJcbiAgICBjbGFzc05hbWU6ICcnLFxyXG4gICAgc3R5bGU6ICcnLFxyXG4gICAgY3VzdG9tRmllbGRUZW1wbGF0ZXM6IHt9LFxyXG4gICAgbWV0YWRhdGE6IHt9LFxyXG4gICAgdmFyaWFibGVzOiBbXSxcclxuICAgIGN1c3RvbUZpZWxkc1ZhbHVlSW5mbzoge30sXHJcbiAgICBncmlkc3RlckZvcm06IGZhbHNlLFxyXG4gICAgZ2xvYmFsRGF0ZUZvcm1hdDogJ0QtTS1ZWVlZJ1xyXG4gIH07XHJcbiJdfQ==