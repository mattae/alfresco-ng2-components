/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/** @type {?} */
export var formDefVisibilitiFieldDependsOnNextOne = {
    id: 19,
    processDefinitionId: 'visibility:1:148',
    processDefinitionName: 'visibility',
    processDefinitionKey: 'visibility',
    tabs: [],
    fields: [
        {
            fieldType: 'ContainerRepresentation',
            id: '1506960847579',
            name: 'Label',
            type: 'container',
            value: null,
            required: false,
            readOnly: false,
            overrideId: false,
            colspan: 1,
            placeholder: null,
            minLength: 0,
            maxLength: 0,
            minValue: null,
            maxValue: null,
            regexPattern: null,
            optionType: null,
            hasEmptyValue: null,
            options: null,
            restUrl: null,
            restResponsePath: null,
            restIdProperty: null,
            restLabelProperty: null,
            tab: null,
            className: null,
            dateDisplayFormat: null,
            layout: null,
            sizeX: 2,
            sizeY: 1,
            row: -1,
            col: -1,
            visibilityCondition: null,
            numberOfColumns: 2,
            fields: {
                '1': [
                    {
                        fieldType: 'RestFieldRepresentation',
                        id: 'country',
                        name: 'country',
                        type: 'dropdown',
                        value: 'Choose one...',
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: true,
                        options: [
                            {
                                id: 'empty',
                                name: 'Choose one...'
                            },
                            {
                                id: 'option_1',
                                name: 'france'
                            },
                            {
                                id: 'option_2',
                                name: 'uk'
                            }
                        ],
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 2
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: {
                            leftType: 'field',
                            leftValue: 'name',
                            operator: '==',
                            rightValue: 'italy',
                            rightFormFieldId: '',
                            rightRestResponseId: '',
                            nextConditionOperator: '',
                            nextCondition: null
                        },
                        endpoint: null,
                        requestHeaders: null
                    }
                ],
                '2': [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'name',
                        name: 'name',
                        type: 'text',
                        value: null,
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 1
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ]
            }
        }
    ],
    outcomes: [],
    javascriptEvents: [],
    className: '',
    style: '',
    customFieldTemplates: {},
    metadata: {},
    variables: [],
    customFieldsValueInfo: {},
    gridsterForm: false,
    globalDateFormat: 'D-M-YYYY'
};
/** @type {?} */
export var formDefVisibilitiFieldDependsOnPreviousOne = {
    id: 19,
    processDefinitionId: 'visibility:1:148',
    processDefinitionName: 'visibility',
    processDefinitionKey: 'visibility',
    tabs: [],
    fields: [
        {
            fieldType: 'ContainerRepresentation',
            id: '1506960847579',
            name: 'Label',
            type: 'container',
            value: null,
            required: false,
            readOnly: false,
            overrideId: false,
            colspan: 1,
            placeholder: null,
            minLength: 0,
            maxLength: 0,
            minValue: null,
            maxValue: null,
            regexPattern: null,
            optionType: null,
            hasEmptyValue: null,
            options: null,
            restUrl: null,
            restResponsePath: null,
            restIdProperty: null,
            restLabelProperty: null,
            tab: null,
            className: null,
            dateDisplayFormat: null,
            layout: null,
            sizeX: 2,
            sizeY: 1,
            row: -1,
            col: -1,
            visibilityCondition: null,
            numberOfColumns: 2,
            fields: {
                '1': [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'name',
                        name: 'name',
                        type: 'text',
                        value: null,
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 1
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ],
                '2': [
                    {
                        fieldType: 'RestFieldRepresentation',
                        id: 'country',
                        name: 'country',
                        type: 'dropdown',
                        value: 'Choose one...',
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: true,
                        options: [
                            {
                                id: 'empty',
                                name: 'Choose one...'
                            },
                            {
                                id: 'option_1',
                                name: 'france'
                            },
                            {
                                id: 'option_2',
                                name: 'uk'
                            }
                        ],
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 2
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: {
                            leftType: 'field',
                            leftValue: 'name',
                            operator: '==',
                            rightValue: 'italy',
                            rightFormFieldId: '',
                            rightRestResponseId: '',
                            nextConditionOperator: '',
                            nextCondition: null
                        },
                        endpoint: null,
                        requestHeaders: null
                    }
                ]
            }
        }
    ],
    outcomes: [],
    javascriptEvents: [],
    className: '',
    style: '',
    customFieldTemplates: {},
    metadata: {},
    variables: [],
    customFieldsValueInfo: {},
    gridsterForm: false,
    globalDateFormat: 'D-M-YYYY'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybURlZmluaXRpb25WaXNpYmlsaXR5Lm1vY2suanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJtb2NrL2Zvcm0vZm9ybURlZmluaXRpb25WaXNpYmlsaXR5Lm1vY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE1BQU0sS0FBSyxzQ0FBc0MsR0FBRztJQUNoRCxFQUFFLEVBQUUsRUFBRTtJQUNOLG1CQUFtQixFQUFFLGtCQUFrQjtJQUN2QyxxQkFBcUIsRUFBRSxZQUFZO0lBQ25DLG9CQUFvQixFQUFFLFlBQVk7SUFDbEMsSUFBSSxFQUFFLEVBQUU7SUFDUixNQUFNLEVBQUU7UUFDSjtZQUNJLFNBQVMsRUFBRSx5QkFBeUI7WUFDcEMsRUFBRSxFQUFFLGVBQWU7WUFDbkIsSUFBSSxFQUFFLE9BQU87WUFDYixJQUFJLEVBQUUsV0FBVztZQUNqQixLQUFLLEVBQUUsSUFBSTtZQUNYLFFBQVEsRUFBRSxLQUFLO1lBQ2YsUUFBUSxFQUFFLEtBQUs7WUFDZixVQUFVLEVBQUUsS0FBSztZQUNqQixPQUFPLEVBQUUsQ0FBQztZQUNWLFdBQVcsRUFBRSxJQUFJO1lBQ2pCLFNBQVMsRUFBRSxDQUFDO1lBQ1osU0FBUyxFQUFFLENBQUM7WUFDWixRQUFRLEVBQUUsSUFBSTtZQUNkLFFBQVEsRUFBRSxJQUFJO1lBQ2QsWUFBWSxFQUFFLElBQUk7WUFDbEIsVUFBVSxFQUFFLElBQUk7WUFDaEIsYUFBYSxFQUFFLElBQUk7WUFDbkIsT0FBTyxFQUFFLElBQUk7WUFDYixPQUFPLEVBQUUsSUFBSTtZQUNiLGdCQUFnQixFQUFFLElBQUk7WUFDdEIsY0FBYyxFQUFFLElBQUk7WUFDcEIsaUJBQWlCLEVBQUUsSUFBSTtZQUN2QixHQUFHLEVBQUUsSUFBSTtZQUNULFNBQVMsRUFBRSxJQUFJO1lBQ2YsaUJBQWlCLEVBQUUsSUFBSTtZQUN2QixNQUFNLEVBQUUsSUFBSTtZQUNaLEtBQUssRUFBRSxDQUFDO1lBQ1IsS0FBSyxFQUFFLENBQUM7WUFDUixHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBQ1AsR0FBRyxFQUFFLENBQUMsQ0FBQztZQUNQLG1CQUFtQixFQUFFLElBQUk7WUFDekIsZUFBZSxFQUFFLENBQUM7WUFDbEIsTUFBTSxFQUFFO2dCQUNKLEdBQUcsRUFBRTtvQkFDRDt3QkFDSSxTQUFTLEVBQUUseUJBQXlCO3dCQUNwQyxFQUFFLEVBQUUsU0FBUzt3QkFDYixJQUFJLEVBQUUsU0FBUzt3QkFDZixJQUFJLEVBQUUsVUFBVTt3QkFDaEIsS0FBSyxFQUFFLGVBQWU7d0JBQ3RCLFFBQVEsRUFBRSxLQUFLO3dCQUNmLFFBQVEsRUFBRSxLQUFLO3dCQUNmLFVBQVUsRUFBRSxLQUFLO3dCQUNqQixPQUFPLEVBQUUsQ0FBQzt3QkFDVixXQUFXLEVBQUUsSUFBSTt3QkFDakIsU0FBUyxFQUFFLENBQUM7d0JBQ1osU0FBUyxFQUFFLENBQUM7d0JBQ1osUUFBUSxFQUFFLElBQUk7d0JBQ2QsUUFBUSxFQUFFLElBQUk7d0JBQ2QsWUFBWSxFQUFFLElBQUk7d0JBQ2xCLFVBQVUsRUFBRSxJQUFJO3dCQUNoQixhQUFhLEVBQUUsSUFBSTt3QkFDbkIsT0FBTyxFQUFFOzRCQUNMO2dDQUNJLEVBQUUsRUFBRSxPQUFPO2dDQUNYLElBQUksRUFBRSxlQUFlOzZCQUN4Qjs0QkFDRDtnQ0FDSSxFQUFFLEVBQUUsVUFBVTtnQ0FDZCxJQUFJLEVBQUUsUUFBUTs2QkFDakI7NEJBQ0Q7Z0NBQ0ksRUFBRSxFQUFFLFVBQVU7Z0NBQ2QsSUFBSSxFQUFFLElBQUk7NkJBQ2I7eUJBQ0o7d0JBQ0QsT0FBTyxFQUFFLElBQUk7d0JBQ2IsZ0JBQWdCLEVBQUUsSUFBSTt3QkFDdEIsY0FBYyxFQUFFLElBQUk7d0JBQ3BCLGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLEdBQUcsRUFBRSxJQUFJO3dCQUNULFNBQVMsRUFBRSxJQUFJO3dCQUNmLE1BQU0sRUFBRTs0QkFDSixlQUFlLEVBQUUsQ0FBQzs0QkFDbEIsVUFBVSxFQUFFLENBQUM7eUJBQ2hCO3dCQUNELGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLE1BQU0sRUFBRTs0QkFDSixHQUFHLEVBQUUsQ0FBQyxDQUFDOzRCQUNQLE1BQU0sRUFBRSxDQUFDLENBQUM7NEJBQ1YsT0FBTyxFQUFFLENBQUM7eUJBQ2I7d0JBQ0QsS0FBSyxFQUFFLENBQUM7d0JBQ1IsS0FBSyxFQUFFLENBQUM7d0JBQ1IsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLG1CQUFtQixFQUFFOzRCQUNqQixRQUFRLEVBQUUsT0FBTzs0QkFDakIsU0FBUyxFQUFFLE1BQU07NEJBQ2pCLFFBQVEsRUFBRSxJQUFJOzRCQUNkLFVBQVUsRUFBRSxPQUFPOzRCQUNuQixnQkFBZ0IsRUFBRSxFQUFFOzRCQUNwQixtQkFBbUIsRUFBRSxFQUFFOzRCQUN2QixxQkFBcUIsRUFBRSxFQUFFOzRCQUN6QixhQUFhLEVBQUUsSUFBSTt5QkFDdEI7d0JBQ0QsUUFBUSxFQUFFLElBQUk7d0JBQ2QsY0FBYyxFQUFFLElBQUk7cUJBQ3ZCO2lCQUNKO2dCQUNELEdBQUcsRUFBRTtvQkFDRDt3QkFDSSxTQUFTLEVBQUUseUJBQXlCO3dCQUNwQyxFQUFFLEVBQUUsTUFBTTt3QkFDVixJQUFJLEVBQUUsTUFBTTt3QkFDWixJQUFJLEVBQUUsTUFBTTt3QkFDWixLQUFLLEVBQUUsSUFBSTt3QkFDWCxRQUFRLEVBQUUsS0FBSzt3QkFDZixRQUFRLEVBQUUsS0FBSzt3QkFDZixVQUFVLEVBQUUsS0FBSzt3QkFDakIsT0FBTyxFQUFFLENBQUM7d0JBQ1YsV0FBVyxFQUFFLElBQUk7d0JBQ2pCLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFlBQVksRUFBRSxJQUFJO3dCQUNsQixVQUFVLEVBQUUsSUFBSTt3QkFDaEIsYUFBYSxFQUFFLElBQUk7d0JBQ25CLE9BQU8sRUFBRSxJQUFJO3dCQUNiLE9BQU8sRUFBRSxJQUFJO3dCQUNiLGdCQUFnQixFQUFFLElBQUk7d0JBQ3RCLGNBQWMsRUFBRSxJQUFJO3dCQUNwQixpQkFBaUIsRUFBRSxJQUFJO3dCQUN2QixHQUFHLEVBQUUsSUFBSTt3QkFDVCxTQUFTLEVBQUUsSUFBSTt3QkFDZixNQUFNLEVBQUU7NEJBQ0osZUFBZSxFQUFFLENBQUM7NEJBQ2xCLFVBQVUsRUFBRSxDQUFDO3lCQUNoQjt3QkFDRCxpQkFBaUIsRUFBRSxJQUFJO3dCQUN2QixNQUFNLEVBQUU7NEJBQ0osR0FBRyxFQUFFLENBQUMsQ0FBQzs0QkFDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDOzRCQUNWLE9BQU8sRUFBRSxDQUFDO3lCQUNiO3dCQUNELEtBQUssRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxDQUFDO3dCQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxtQkFBbUIsRUFBRSxJQUFJO3FCQUM1QjtpQkFDSjthQUNKO1NBQ0o7S0FDSjtJQUNELFFBQVEsRUFBRSxFQUFFO0lBQ1osZ0JBQWdCLEVBQUUsRUFBRTtJQUNwQixTQUFTLEVBQUUsRUFBRTtJQUNiLEtBQUssRUFBRSxFQUFFO0lBQ1Qsb0JBQW9CLEVBQUUsRUFBRTtJQUN4QixRQUFRLEVBQUUsRUFBRTtJQUNaLFNBQVMsRUFBRSxFQUFFO0lBQ2IscUJBQXFCLEVBQUUsRUFBRTtJQUN6QixZQUFZLEVBQUUsS0FBSztJQUNuQixnQkFBZ0IsRUFBRSxVQUFVO0NBQy9COztBQUVELE1BQU0sS0FBSywwQ0FBMEMsR0FBRztJQUNwRCxFQUFFLEVBQUUsRUFBRTtJQUNOLG1CQUFtQixFQUFFLGtCQUFrQjtJQUN2QyxxQkFBcUIsRUFBRSxZQUFZO0lBQ25DLG9CQUFvQixFQUFFLFlBQVk7SUFDbEMsSUFBSSxFQUFFLEVBQUU7SUFDUixNQUFNLEVBQUU7UUFDSjtZQUNJLFNBQVMsRUFBRSx5QkFBeUI7WUFDcEMsRUFBRSxFQUFFLGVBQWU7WUFDbkIsSUFBSSxFQUFFLE9BQU87WUFDYixJQUFJLEVBQUUsV0FBVztZQUNqQixLQUFLLEVBQUUsSUFBSTtZQUNYLFFBQVEsRUFBRSxLQUFLO1lBQ2YsUUFBUSxFQUFFLEtBQUs7WUFDZixVQUFVLEVBQUUsS0FBSztZQUNqQixPQUFPLEVBQUUsQ0FBQztZQUNWLFdBQVcsRUFBRSxJQUFJO1lBQ2pCLFNBQVMsRUFBRSxDQUFDO1lBQ1osU0FBUyxFQUFFLENBQUM7WUFDWixRQUFRLEVBQUUsSUFBSTtZQUNkLFFBQVEsRUFBRSxJQUFJO1lBQ2QsWUFBWSxFQUFFLElBQUk7WUFDbEIsVUFBVSxFQUFFLElBQUk7WUFDaEIsYUFBYSxFQUFFLElBQUk7WUFDbkIsT0FBTyxFQUFFLElBQUk7WUFDYixPQUFPLEVBQUUsSUFBSTtZQUNiLGdCQUFnQixFQUFFLElBQUk7WUFDdEIsY0FBYyxFQUFFLElBQUk7WUFDcEIsaUJBQWlCLEVBQUUsSUFBSTtZQUN2QixHQUFHLEVBQUUsSUFBSTtZQUNULFNBQVMsRUFBRSxJQUFJO1lBQ2YsaUJBQWlCLEVBQUUsSUFBSTtZQUN2QixNQUFNLEVBQUUsSUFBSTtZQUNaLEtBQUssRUFBRSxDQUFDO1lBQ1IsS0FBSyxFQUFFLENBQUM7WUFDUixHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBQ1AsR0FBRyxFQUFFLENBQUMsQ0FBQztZQUNQLG1CQUFtQixFQUFFLElBQUk7WUFDekIsZUFBZSxFQUFFLENBQUM7WUFDbEIsTUFBTSxFQUFFO2dCQUNKLEdBQUcsRUFBRTtvQkFDRDt3QkFDSSxTQUFTLEVBQUUseUJBQXlCO3dCQUNwQyxFQUFFLEVBQUUsTUFBTTt3QkFDVixJQUFJLEVBQUUsTUFBTTt3QkFDWixJQUFJLEVBQUUsTUFBTTt3QkFDWixLQUFLLEVBQUUsSUFBSTt3QkFDWCxRQUFRLEVBQUUsS0FBSzt3QkFDZixRQUFRLEVBQUUsS0FBSzt3QkFDZixVQUFVLEVBQUUsS0FBSzt3QkFDakIsT0FBTyxFQUFFLENBQUM7d0JBQ1YsV0FBVyxFQUFFLElBQUk7d0JBQ2pCLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFlBQVksRUFBRSxJQUFJO3dCQUNsQixVQUFVLEVBQUUsSUFBSTt3QkFDaEIsYUFBYSxFQUFFLElBQUk7d0JBQ25CLE9BQU8sRUFBRSxJQUFJO3dCQUNiLE9BQU8sRUFBRSxJQUFJO3dCQUNiLGdCQUFnQixFQUFFLElBQUk7d0JBQ3RCLGNBQWMsRUFBRSxJQUFJO3dCQUNwQixpQkFBaUIsRUFBRSxJQUFJO3dCQUN2QixHQUFHLEVBQUUsSUFBSTt3QkFDVCxTQUFTLEVBQUUsSUFBSTt3QkFDZixNQUFNLEVBQUU7NEJBQ0osZUFBZSxFQUFFLENBQUM7NEJBQ2xCLFVBQVUsRUFBRSxDQUFDO3lCQUNoQjt3QkFDRCxpQkFBaUIsRUFBRSxJQUFJO3dCQUN2QixNQUFNLEVBQUU7NEJBQ0osR0FBRyxFQUFFLENBQUMsQ0FBQzs0QkFDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDOzRCQUNWLE9BQU8sRUFBRSxDQUFDO3lCQUNiO3dCQUNELEtBQUssRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxDQUFDO3dCQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxtQkFBbUIsRUFBRSxJQUFJO3FCQUM1QjtpQkFDSjtnQkFDRCxHQUFHLEVBQUU7b0JBQ0Q7d0JBQ0ksU0FBUyxFQUFFLHlCQUF5Qjt3QkFDcEMsRUFBRSxFQUFFLFNBQVM7d0JBQ2IsSUFBSSxFQUFFLFNBQVM7d0JBQ2YsSUFBSSxFQUFFLFVBQVU7d0JBQ2hCLEtBQUssRUFBRSxlQUFlO3dCQUN0QixRQUFRLEVBQUUsS0FBSzt3QkFDZixRQUFRLEVBQUUsS0FBSzt3QkFDZixVQUFVLEVBQUUsS0FBSzt3QkFDakIsT0FBTyxFQUFFLENBQUM7d0JBQ1YsV0FBVyxFQUFFLElBQUk7d0JBQ2pCLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFlBQVksRUFBRSxJQUFJO3dCQUNsQixVQUFVLEVBQUUsSUFBSTt3QkFDaEIsYUFBYSxFQUFFLElBQUk7d0JBQ25CLE9BQU8sRUFBRTs0QkFDTDtnQ0FDSSxFQUFFLEVBQUUsT0FBTztnQ0FDWCxJQUFJLEVBQUUsZUFBZTs2QkFDeEI7NEJBQ0Q7Z0NBQ0ksRUFBRSxFQUFFLFVBQVU7Z0NBQ2QsSUFBSSxFQUFFLFFBQVE7NkJBQ2pCOzRCQUNEO2dDQUNJLEVBQUUsRUFBRSxVQUFVO2dDQUNkLElBQUksRUFBRSxJQUFJOzZCQUNiO3lCQUNKO3dCQUNELE9BQU8sRUFBRSxJQUFJO3dCQUNiLGdCQUFnQixFQUFFLElBQUk7d0JBQ3RCLGNBQWMsRUFBRSxJQUFJO3dCQUNwQixpQkFBaUIsRUFBRSxJQUFJO3dCQUN2QixHQUFHLEVBQUUsSUFBSTt3QkFDVCxTQUFTLEVBQUUsSUFBSTt3QkFDZixNQUFNLEVBQUU7NEJBQ0osZUFBZSxFQUFFLENBQUM7NEJBQ2xCLFVBQVUsRUFBRSxDQUFDO3lCQUNoQjt3QkFDRCxpQkFBaUIsRUFBRSxJQUFJO3dCQUN2QixNQUFNLEVBQUU7NEJBQ0osR0FBRyxFQUFFLENBQUMsQ0FBQzs0QkFDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDOzRCQUNWLE9BQU8sRUFBRSxDQUFDO3lCQUNiO3dCQUNELEtBQUssRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxDQUFDO3dCQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxtQkFBbUIsRUFBRTs0QkFDakIsUUFBUSxFQUFFLE9BQU87NEJBQ2pCLFNBQVMsRUFBRSxNQUFNOzRCQUNqQixRQUFRLEVBQUUsSUFBSTs0QkFDZCxVQUFVLEVBQUUsT0FBTzs0QkFDbkIsZ0JBQWdCLEVBQUUsRUFBRTs0QkFDcEIsbUJBQW1CLEVBQUUsRUFBRTs0QkFDdkIscUJBQXFCLEVBQUUsRUFBRTs0QkFDekIsYUFBYSxFQUFFLElBQUk7eUJBQ3RCO3dCQUNELFFBQVEsRUFBRSxJQUFJO3dCQUNkLGNBQWMsRUFBRSxJQUFJO3FCQUN2QjtpQkFDSjthQUNKO1NBQ0o7S0FDSjtJQUNELFFBQVEsRUFBRSxFQUFFO0lBQ1osZ0JBQWdCLEVBQUUsRUFBRTtJQUNwQixTQUFTLEVBQUUsRUFBRTtJQUNiLEtBQUssRUFBRSxFQUFFO0lBQ1Qsb0JBQW9CLEVBQUUsRUFBRTtJQUN4QixRQUFRLEVBQUUsRUFBRTtJQUNaLFNBQVMsRUFBRSxFQUFFO0lBQ2IscUJBQXFCLEVBQUUsRUFBRTtJQUN6QixZQUFZLEVBQUUsS0FBSztJQUNuQixnQkFBZ0IsRUFBRSxVQUFVO0NBQy9CIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmV4cG9ydCBsZXQgZm9ybURlZlZpc2liaWxpdGlGaWVsZERlcGVuZHNPbk5leHRPbmUgPSB7XHJcbiAgICBpZDogMTksXHJcbiAgICBwcm9jZXNzRGVmaW5pdGlvbklkOiAndmlzaWJpbGl0eToxOjE0OCcsXHJcbiAgICBwcm9jZXNzRGVmaW5pdGlvbk5hbWU6ICd2aXNpYmlsaXR5JyxcclxuICAgIHByb2Nlc3NEZWZpbml0aW9uS2V5OiAndmlzaWJpbGl0eScsXHJcbiAgICB0YWJzOiBbXSxcclxuICAgIGZpZWxkczogW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgZmllbGRUeXBlOiAnQ29udGFpbmVyUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICBpZDogJzE1MDY5NjA4NDc1NzknLFxyXG4gICAgICAgICAgICBuYW1lOiAnTGFiZWwnLFxyXG4gICAgICAgICAgICB0eXBlOiAnY29udGFpbmVyJyxcclxuICAgICAgICAgICAgdmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICBvdmVycmlkZUlkOiBmYWxzZSxcclxuICAgICAgICAgICAgY29sc3BhbjogMSxcclxuICAgICAgICAgICAgcGxhY2Vob2xkZXI6IG51bGwsXHJcbiAgICAgICAgICAgIG1pbkxlbmd0aDogMCxcclxuICAgICAgICAgICAgbWF4TGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICBtaW5WYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgbWF4VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIHJlZ2V4UGF0dGVybjogbnVsbCxcclxuICAgICAgICAgICAgb3B0aW9uVHlwZTogbnVsbCxcclxuICAgICAgICAgICAgaGFzRW1wdHlWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgb3B0aW9uczogbnVsbCxcclxuICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICAgIGNsYXNzTmFtZTogbnVsbCxcclxuICAgICAgICAgICAgZGF0ZURpc3BsYXlGb3JtYXQ6IG51bGwsXHJcbiAgICAgICAgICAgIGxheW91dDogbnVsbCxcclxuICAgICAgICAgICAgc2l6ZVg6IDIsXHJcbiAgICAgICAgICAgIHNpemVZOiAxLFxyXG4gICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICBjb2w6IC0xLFxyXG4gICAgICAgICAgICB2aXNpYmlsaXR5Q29uZGl0aW9uOiBudWxsLFxyXG4gICAgICAgICAgICBudW1iZXJPZkNvbHVtbnM6IDIsXHJcbiAgICAgICAgICAgIGZpZWxkczoge1xyXG4gICAgICAgICAgICAgICAgJzEnOiBbXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWVsZFR5cGU6ICdSZXN0RmllbGRSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnY291bnRyeScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdjb3VudHJ5JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2Ryb3Bkb3duJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6ICdDaG9vc2Ugb25lLi4uJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG92ZXJyaWRlSWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWluTGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXhMZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pblZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXhWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVnZXhQYXR0ZXJuOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25UeXBlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBoYXNFbXB0eVZhbHVlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zOiBbXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdlbXB0eScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ0Nob29zZSBvbmUuLi4nXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnb3B0aW9uXzEnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdmcmFuY2UnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnb3B0aW9uXzInLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICd1aydcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGlzdGluZ0NvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhDb2xzcGFuOiAyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYXlvdXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2x1bW46IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGxlZnRUeXBlOiAnZmllbGQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGVmdFZhbHVlOiAnbmFtZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvcGVyYXRvcjogJz09JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJpZ2h0VmFsdWU6ICdpdGFseScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByaWdodEZvcm1GaWVsZElkOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJpZ2h0UmVzdFJlc3BvbnNlSWQ6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbmV4dENvbmRpdGlvbk9wZXJhdG9yOiAnJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5leHRDb25kaXRpb246IG51bGxcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZW5kcG9pbnQ6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVlc3RIZWFkZXJzOiBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgICcyJzogW1xyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRUeXBlOiAnRm9ybUZpZWxkUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogJ25hbWUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnbmFtZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICd0ZXh0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvdmVycmlkZUlkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbkxlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4TGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5WYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlZ2V4UGF0dGVybjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uVHlwZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGFzRW1wdHlWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uczogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGlzdGluZ0NvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhDb2xzcGFuOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYXlvdXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2x1bW46IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGxcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICBdLFxyXG4gICAgb3V0Y29tZXM6IFtdLFxyXG4gICAgamF2YXNjcmlwdEV2ZW50czogW10sXHJcbiAgICBjbGFzc05hbWU6ICcnLFxyXG4gICAgc3R5bGU6ICcnLFxyXG4gICAgY3VzdG9tRmllbGRUZW1wbGF0ZXM6IHt9LFxyXG4gICAgbWV0YWRhdGE6IHt9LFxyXG4gICAgdmFyaWFibGVzOiBbXSxcclxuICAgIGN1c3RvbUZpZWxkc1ZhbHVlSW5mbzoge30sXHJcbiAgICBncmlkc3RlckZvcm06IGZhbHNlLFxyXG4gICAgZ2xvYmFsRGF0ZUZvcm1hdDogJ0QtTS1ZWVlZJ1xyXG59O1xyXG5cclxuZXhwb3J0IGxldCBmb3JtRGVmVmlzaWJpbGl0aUZpZWxkRGVwZW5kc09uUHJldmlvdXNPbmUgPSB7XHJcbiAgICBpZDogMTksXHJcbiAgICBwcm9jZXNzRGVmaW5pdGlvbklkOiAndmlzaWJpbGl0eToxOjE0OCcsXHJcbiAgICBwcm9jZXNzRGVmaW5pdGlvbk5hbWU6ICd2aXNpYmlsaXR5JyxcclxuICAgIHByb2Nlc3NEZWZpbml0aW9uS2V5OiAndmlzaWJpbGl0eScsXHJcbiAgICB0YWJzOiBbXSxcclxuICAgIGZpZWxkczogW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgZmllbGRUeXBlOiAnQ29udGFpbmVyUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICBpZDogJzE1MDY5NjA4NDc1NzknLFxyXG4gICAgICAgICAgICBuYW1lOiAnTGFiZWwnLFxyXG4gICAgICAgICAgICB0eXBlOiAnY29udGFpbmVyJyxcclxuICAgICAgICAgICAgdmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICBvdmVycmlkZUlkOiBmYWxzZSxcclxuICAgICAgICAgICAgY29sc3BhbjogMSxcclxuICAgICAgICAgICAgcGxhY2Vob2xkZXI6IG51bGwsXHJcbiAgICAgICAgICAgIG1pbkxlbmd0aDogMCxcclxuICAgICAgICAgICAgbWF4TGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICBtaW5WYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgbWF4VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIHJlZ2V4UGF0dGVybjogbnVsbCxcclxuICAgICAgICAgICAgb3B0aW9uVHlwZTogbnVsbCxcclxuICAgICAgICAgICAgaGFzRW1wdHlWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgb3B0aW9uczogbnVsbCxcclxuICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICAgIGNsYXNzTmFtZTogbnVsbCxcclxuICAgICAgICAgICAgZGF0ZURpc3BsYXlGb3JtYXQ6IG51bGwsXHJcbiAgICAgICAgICAgIGxheW91dDogbnVsbCxcclxuICAgICAgICAgICAgc2l6ZVg6IDIsXHJcbiAgICAgICAgICAgIHNpemVZOiAxLFxyXG4gICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICBjb2w6IC0xLFxyXG4gICAgICAgICAgICB2aXNpYmlsaXR5Q29uZGl0aW9uOiBudWxsLFxyXG4gICAgICAgICAgICBudW1iZXJPZkNvbHVtbnM6IDIsXHJcbiAgICAgICAgICAgIGZpZWxkczoge1xyXG4gICAgICAgICAgICAgICAgJzEnOiBbXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWVsZFR5cGU6ICdGb3JtRmllbGRSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnbmFtZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICduYW1lJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3RleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG92ZXJyaWRlSWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWluTGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXhMZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pblZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXhWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVnZXhQYXR0ZXJuOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25UeXBlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBoYXNFbXB0eVZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0VXJsOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0UmVzcG9uc2VQYXRoOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0SWRQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdExhYmVsUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhYjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4aXN0aW5nQ29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heENvbHNwYW46IDFcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0ZURpc3BsYXlGb3JtYXQ6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxheW91dDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbHVtbjogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xzcGFuOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpemVYOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWTogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sOiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmlzaWJpbGl0eUNvbmRpdGlvbjogbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICAnMic6IFtcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkVHlwZTogJ1Jlc3RGaWVsZFJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdjb3VudHJ5JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ2NvdW50cnknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnZHJvcGRvd24nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogJ0Nob29zZSBvbmUuLi4nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnM6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogJ2VtcHR5JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnQ2hvb3NlIG9uZS4uLidcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdvcHRpb25fMScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ2ZyYW5jZSdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdvcHRpb25fMicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ3VrJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0VXJsOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0UmVzcG9uc2VQYXRoOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0SWRQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdExhYmVsUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhYjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4aXN0aW5nQ29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heENvbHNwYW46IDJcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0ZURpc3BsYXlGb3JtYXQ6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxheW91dDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbHVtbjogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xzcGFuOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpemVYOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWTogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sOiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmlzaWJpbGl0eUNvbmRpdGlvbjoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbGVmdFR5cGU6ICdmaWVsZCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBsZWZ0VmFsdWU6ICduYW1lJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG9wZXJhdG9yOiAnPT0nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmlnaHRWYWx1ZTogJ2l0YWx5JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJpZ2h0Rm9ybUZpZWxkSWQ6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcmlnaHRSZXN0UmVzcG9uc2VJZDogJycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBuZXh0Q29uZGl0aW9uT3BlcmF0b3I6ICcnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbmV4dENvbmRpdGlvbjogbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbmRwb2ludDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWVzdEhlYWRlcnM6IG51bGxcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICBdLFxyXG4gICAgb3V0Y29tZXM6IFtdLFxyXG4gICAgamF2YXNjcmlwdEV2ZW50czogW10sXHJcbiAgICBjbGFzc05hbWU6ICcnLFxyXG4gICAgc3R5bGU6ICcnLFxyXG4gICAgY3VzdG9tRmllbGRUZW1wbGF0ZXM6IHt9LFxyXG4gICAgbWV0YWRhdGE6IHt9LFxyXG4gICAgdmFyaWFibGVzOiBbXSxcclxuICAgIGN1c3RvbUZpZWxkc1ZhbHVlSW5mbzoge30sXHJcbiAgICBncmlkc3RlckZvcm06IGZhbHNlLFxyXG4gICAgZ2xvYmFsRGF0ZUZvcm1hdDogJ0QtTS1ZWVlZJ1xyXG59O1xyXG4iXX0=