/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/** @type {?} */
export var startFormDateWidgetMock = {
    id: 4,
    name: 'Claim Review Process',
    processDefinitionId: 'ClaimReviewProcess:2: 93',
    fields: [
        {
            fieldType: 'ContainerRepresentation',
            id: 1497953253784,
            name: 'Label',
            type: 'container',
            value: null,
            readOnly: false,
            fields: {
                1: [{
                        fieldType: 'FormFieldRepresentation',
                        id: 'date',
                        name: 'date',
                        type: 'date',
                        value: null
                    }]
            }
        }
    ]
};
/** @type {?} */
export var startFormNumberWidgetMock = {
    id: 4,
    name: 'Claim Review Process',
    processDefinitionId: 'ClaimReviewProcess:2: 93',
    fields: [
        {
            fieldType: 'ContainerRepresentation',
            id: 1497953253784,
            name: 'Label',
            type: 'container',
            value: null,
            readOnly: false,
            fields: {
                1: [{
                        fieldType: 'FormFieldRepresentation',
                        id: 'number',
                        name: 'number widget',
                        type: 'integer',
                        value: null
                    }]
            }
        }
    ]
};
/** @type {?} */
export var startFormAmountWidgetMock = {
    id: 4,
    name: 'Claim Review Process',
    processDefinitionId: 'ClaimReviewProcess:2: 93',
    fields: [
        {
            fieldType: 'ContainerRepresentation',
            id: 1497953253784,
            name: 'Label',
            type: 'container',
            value: null,
            readOnly: false,
            fields: {
                1: [{
                        fieldType: 'FormFieldRepresentation',
                        id: 'amount',
                        name: 'amount widget',
                        type: 'amount',
                        value: null
                    }]
            }
        }
    ]
};
/** @type {?} */
export var startFormRadioButtonWidgetMock = {
    id: 4,
    name: 'Claim Review Process',
    processDefinitionId: 'ClaimReviewProcess:2: 93',
    fields: [
        {
            fieldType: 'ContainerRepresentation',
            id: 1497953253784,
            name: 'Label',
            type: 'container',
            value: null,
            readOnly: false,
            fields: {
                1: [{
                        fieldType: 'RestFieldRepresentation',
                        id: 'radio-but',
                        name: 'radio-buttons',
                        type: 'radio-buttons',
                        value: null
                    }]
            }
        }
    ]
};
/** @type {?} */
export var startFormTextDefinitionMock = {
    id: 4,
    name: 'Claim Review Process',
    processDefinitionId: 'ClaimReviewProcess:2: 93',
    fields: [
        {
            fieldType: 'ContainerRepresentation',
            id: 1497953253784,
            name: 'Label',
            type: 'container',
            value: null,
            readOnly: false,
            fields: {
                1: [{
                        fieldType: 'FormFieldRepresentation',
                        id: 'mocktext',
                        name: 'mockText',
                        type: 'text',
                        value: null
                    }]
            }
        }
    ]
};
/** @type {?} */
export var startFormDropdownDefinitionMock = {
    id: 4,
    name: 'Claim Review Process',
    processDefinitionId: 'ClaimReviewProcess:2: 93',
    fields: [
        {
            fieldType: 'ContainerRepresentation',
            id: 1497953253784,
            name: 'Label',
            type: 'container',
            value: null,
            readOnly: false,
            fields: {
                1: [{
                        fieldType: 'RestFieldRepresentation',
                        id: 'mockTypeDropDown',
                        name: 'mock DropDown',
                        type: 'dropdown',
                        value: 'Chooseone...',
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        options: [
                            {
                                id: 'empty',
                                name: 'Chooseone...'
                            },
                            {
                                id: 'opt1',
                                name: 'Option-1'
                            },
                            {
                                id: 'opt2',
                                name: 'Option-2'
                            },
                            {
                                id: 'opt3',
                                name: 'Option-3'
                            },
                            {
                                id: 'opt2',
                                name: 'Option-3'
                            }
                        ]
                    }]
            }
        }
    ]
};
/** @type {?} */
export var startMockForm = {
    id: 4,
    name: 'Claim Review Process',
    processDefinitionId: 'ClaimReviewProcess:2: 93',
    processDefinitionName: 'ClaimReviewProcess',
    processDefinitionKey: 'ClaimReviewProcess',
    tabs: [],
    fields: [
        {
            fieldType: 'ContainerRepresentation',
            id: 1497953253784,
            name: 'Label',
            type: 'container',
            value: null,
            required: false,
            readOnly: false,
            overrideId: false,
            colspan: 1,
            placeholder: null,
            minLength: 0,
            maxLength: 0,
            minValue: null,
            maxValue: null,
            regexPattern: null,
            optionType: null,
            hasEmptyValue: null,
            options: null,
            restUrl: null,
            restResponsePath: null,
            restIdProperty: null,
            restLabelProperty: null,
            tab: null,
            className: null,
            dateDisplayFormat: null,
            layout: null,
            sizeX: 2,
            sizeY: 1,
            row: -1,
            col: -1,
            visibilityCondition: null,
            numberOfColumns: 2,
            fields: {
                1: [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'clientname',
                        name: 'ClientName',
                        type: 'text',
                        value: null,
                        required: true,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 2
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ],
                2: [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'policyno',
                        name: 'PolicyNo',
                        type: 'integer',
                        value: null,
                        required: true,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: 'EnterPolicyName',
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 1
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ]
            }
        },
        {
            fieldType: 'ContainerRepresentation',
            id: 1497953270269,
            name: 'Label',
            type: 'container',
            value: null,
            required: false,
            readOnly: false,
            overrideId: false,
            colspan: 1,
            placeholder: null,
            minLength: 0,
            maxLength: 0,
            minValue: null,
            maxValue: null,
            regexPattern: null,
            optionType: null,
            hasEmptyValue: null,
            options: null,
            restUrl: null,
            restResponsePath: null,
            restIdProperty: null,
            restLabelProperty: null,
            tab: null,
            className: null,
            dateDisplayFormat: null,
            layout: null,
            sizeX: 2,
            sizeY: 1,
            row: -1,
            col: -1,
            visibilityCondition: null,
            numberOfColumns: 2,
            fields: {
                1: [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'billAmount',
                        name: 'BillAmount',
                        type: 'integer',
                        value: null,
                        required: true,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: 'EnterBillAmount',
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 2
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ],
                2: [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'billdate',
                        name: 'BillDate',
                        type: 'date',
                        value: null,
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: 'billdate',
                        params: {
                            existingColspan: 1,
                            maxColspan: 1
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ]
            }
        },
        {
            fieldType: 'ContainerRepresentation',
            id: 1497953280930,
            name: 'Label',
            type: 'container',
            value: null,
            required: false,
            readOnly: false,
            overrideId: false,
            colspan: 1,
            placeholder: null,
            minLength: 0,
            maxLength: 0,
            minValue: null,
            maxValue: null,
            regexPattern: null,
            optionType: null,
            hasEmptyValue: null,
            options: null,
            restUrl: null,
            restResponsePath: null,
            restIdProperty: null,
            restLabelProperty: null,
            tab: null,
            className: null,
            dateDisplayFormat: null,
            layout: null,
            sizeX: 2,
            sizeY: 1,
            row: -1,
            col: -1,
            visibilityCondition: null,
            numberOfColumns: 2,
            fields: {
                1: [
                    {
                        fieldType: 'RestFieldRepresentation',
                        id: 'claimtype',
                        name: 'ClaimType',
                        type: 'dropdown',
                        value: 'Chooseone...',
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: true,
                        options: [
                            {
                                id: 'empty',
                                name: 'Chooseone...'
                            },
                            {
                                id: 'cashless',
                                name: 'Cashless'
                            },
                            {
                                id: 'reimbursement',
                                name: 'Reimbursement'
                            }
                        ],
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 2
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null,
                        endpoint: null,
                        requestHeaders: null
                    }
                ],
                2: [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'hospitalName',
                        name: 'HospitalName',
                        type: 'text',
                        value: null,
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: 'EnterHospitalName',
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 1
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ]
            }
        }
    ],
    outcomes: [
        {
            id: 'approve',
            name: 'Approve'
        },
        {
            id: 'complete',
            name: 'Complete'
        },
        {
            id: 'start_process',
            name: 'Start Process'
        }
    ],
    javascriptEvents: [],
    className: '',
    style: '',
    metadata: {},
    variables: [],
    customFieldsValueInfo: {},
    gridsterForm: false,
    globalDateFormat: 'D - M - YYYY'
};
/** @type {?} */
export var startMockFormWithTab = {
    id: 4,
    taskName: 'Mock Title',
    processDefinitionId: 'ClaimReviewProcess:2: 93',
    processDefinitionName: 'ClaimReviewProcess',
    processDefinitionKey: 'ClaimReviewProcess',
    tabs: [
        {
            id: 'form1',
            name: 'Tab 1'
        },
        {
            id: 'form2',
            name: 'Tab 2'
        }
    ],
    fields: [
        {
            fieldType: 'ContainerRepresentation',
            id: 1497953253784,
            name: 'Label',
            type: 'container',
            value: null,
            required: false,
            readOnly: false,
            overrideId: false,
            colspan: 1,
            placeholder: null,
            minLength: 0,
            maxLength: 0,
            minValue: null,
            maxValue: null,
            regexPattern: null,
            optionType: null,
            hasEmptyValue: null,
            options: null,
            restUrl: null,
            restResponsePath: null,
            restIdProperty: null,
            restLabelProperty: null,
            tab: null,
            className: null,
            dateDisplayFormat: null,
            layout: null,
            sizeX: 2,
            sizeY: 1,
            row: -1,
            col: -1,
            visibilityCondition: null,
            numberOfColumns: 2,
            fields: {
                1: [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'clientname',
                        name: 'ClientName',
                        type: 'text',
                        value: null,
                        required: true,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 2
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ],
                2: [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'policyno',
                        name: 'PolicyNo',
                        type: 'integer',
                        value: null,
                        required: true,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: 'EnterPolicyName',
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 1
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ]
            }
        },
        {
            fieldType: 'ContainerRepresentation',
            id: 1497953270269,
            name: 'Label',
            type: 'container',
            value: null,
            required: false,
            readOnly: false,
            overrideId: false,
            colspan: 1,
            placeholder: null,
            minLength: 0,
            maxLength: 0,
            minValue: null,
            maxValue: null,
            regexPattern: null,
            optionType: null,
            hasEmptyValue: null,
            options: null,
            restUrl: null,
            restResponsePath: null,
            restIdProperty: null,
            restLabelProperty: null,
            tab: null,
            className: null,
            dateDisplayFormat: null,
            layout: null,
            sizeX: 2,
            sizeY: 1,
            row: -1,
            col: -1,
            visibilityCondition: null,
            numberOfColumns: 2,
            fields: {
                1: [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'billAmount',
                        name: 'BillAmount',
                        type: 'integer',
                        value: null,
                        required: true,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: 'EnterBillAmount',
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 2
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ],
                2: [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'billdate',
                        name: 'BillDate',
                        type: 'date',
                        value: null,
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: 'billdate',
                        params: {
                            existingColspan: 1,
                            maxColspan: 1
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ]
            }
        },
        {
            fieldType: 'ContainerRepresentation',
            id: 1497953280930,
            name: 'Label',
            type: 'container',
            value: null,
            required: false,
            readOnly: false,
            overrideId: false,
            colspan: 1,
            placeholder: null,
            minLength: 0,
            maxLength: 0,
            minValue: null,
            maxValue: null,
            regexPattern: null,
            optionType: null,
            hasEmptyValue: null,
            options: null,
            restUrl: null,
            restResponsePath: null,
            restIdProperty: null,
            restLabelProperty: null,
            tab: null,
            className: null,
            dateDisplayFormat: null,
            layout: null,
            sizeX: 2,
            sizeY: 1,
            row: -1,
            col: -1,
            visibilityCondition: null,
            numberOfColumns: 2,
            fields: {
                1: [
                    {
                        fieldType: 'RestFieldRepresentation',
                        id: 'claimtype',
                        name: 'ClaimType',
                        type: 'dropdown',
                        value: 'Chooseone...',
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: null,
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: true,
                        options: [
                            {
                                id: 'empty',
                                name: 'Chooseone...'
                            },
                            {
                                id: 'cashless',
                                name: 'Cashless'
                            },
                            {
                                id: 'reimbursement',
                                name: 'Reimbursement'
                            }
                        ],
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 2
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null,
                        endpoint: null,
                        requestHeaders: null
                    }
                ],
                2: [
                    {
                        fieldType: 'FormFieldRepresentation',
                        id: 'hospitalName',
                        name: 'HospitalName',
                        type: 'text',
                        value: null,
                        required: false,
                        readOnly: false,
                        overrideId: false,
                        colspan: 1,
                        placeholder: 'EnterHospitalName',
                        minLength: 0,
                        maxLength: 0,
                        minValue: null,
                        maxValue: null,
                        regexPattern: null,
                        optionType: null,
                        hasEmptyValue: null,
                        options: null,
                        restUrl: null,
                        restResponsePath: null,
                        restIdProperty: null,
                        restLabelProperty: null,
                        tab: null,
                        className: null,
                        params: {
                            existingColspan: 1,
                            maxColspan: 1
                        },
                        dateDisplayFormat: null,
                        layout: {
                            row: -1,
                            column: -1,
                            colspan: 1
                        },
                        sizeX: 1,
                        sizeY: 1,
                        row: -1,
                        col: -1,
                        visibilityCondition: null
                    }
                ]
            }
        }
    ],
    outcomes: [
        {
            id: 'approve',
            name: 'Approve'
        },
        {
            id: 'complete',
            name: 'Complete'
        }
    ],
    javascriptEvents: [],
    className: '',
    style: '',
    metadata: {},
    variables: [],
    customFieldsValueInfo: {},
    gridsterForm: false,
    globalDateFormat: 'D - M - YYYY'
};
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RhcnQtZm9ybS5jb21wb25lbnQubW9jay5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vY2svZm9ybS9zdGFydC1mb3JtLmNvbXBvbmVudC5tb2NrLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxNQUFNLEtBQUssdUJBQXVCLEdBQUc7SUFDakMsRUFBRSxFQUFFLENBQUM7SUFDTCxJQUFJLEVBQUUsc0JBQXNCO0lBQzVCLG1CQUFtQixFQUFFLDBCQUEwQjtJQUMvQyxNQUFNLEVBQUU7UUFDSjtZQUNJLFNBQVMsRUFBRSx5QkFBeUI7WUFDcEMsRUFBRSxFQUFFLGFBQWE7WUFDakIsSUFBSSxFQUFFLE9BQU87WUFDYixJQUFJLEVBQUUsV0FBVztZQUNqQixLQUFLLEVBQUUsSUFBSTtZQUNYLFFBQVEsRUFBRSxLQUFLO1lBQ2YsTUFBTSxFQUFFO2dCQUNKLENBQUMsRUFBRSxDQUFDO3dCQUNJLFNBQVMsRUFBRSx5QkFBeUI7d0JBQ3BDLEVBQUUsRUFBRSxNQUFNO3dCQUNWLElBQUksRUFBRSxNQUFNO3dCQUNaLElBQUksRUFBRSxNQUFNO3dCQUNaLEtBQUssRUFBRSxJQUFJO3FCQUNkLENBQUM7YUFBQztTQUNkO0tBQUM7Q0FDVDs7QUFFRCxNQUFNLEtBQUsseUJBQXlCLEdBQUc7SUFDbkMsRUFBRSxFQUFFLENBQUM7SUFDTCxJQUFJLEVBQUUsc0JBQXNCO0lBQzVCLG1CQUFtQixFQUFFLDBCQUEwQjtJQUMvQyxNQUFNLEVBQUU7UUFDSjtZQUNJLFNBQVMsRUFBRSx5QkFBeUI7WUFDcEMsRUFBRSxFQUFFLGFBQWE7WUFDakIsSUFBSSxFQUFFLE9BQU87WUFDYixJQUFJLEVBQUUsV0FBVztZQUNqQixLQUFLLEVBQUUsSUFBSTtZQUNYLFFBQVEsRUFBRSxLQUFLO1lBQ2YsTUFBTSxFQUFFO2dCQUNKLENBQUMsRUFBRSxDQUFDO3dCQUNJLFNBQVMsRUFBRSx5QkFBeUI7d0JBQ3BDLEVBQUUsRUFBRSxRQUFRO3dCQUNaLElBQUksRUFBRSxlQUFlO3dCQUNyQixJQUFJLEVBQUUsU0FBUzt3QkFDZixLQUFLLEVBQUUsSUFBSTtxQkFDZCxDQUFDO2FBQUM7U0FDZDtLQUFDO0NBQ1Q7O0FBRUQsTUFBTSxLQUFLLHlCQUF5QixHQUFHO0lBQ25DLEVBQUUsRUFBRSxDQUFDO0lBQ0wsSUFBSSxFQUFFLHNCQUFzQjtJQUM1QixtQkFBbUIsRUFBRSwwQkFBMEI7SUFDL0MsTUFBTSxFQUFFO1FBQ0o7WUFDSSxTQUFTLEVBQUUseUJBQXlCO1lBQ3BDLEVBQUUsRUFBRSxhQUFhO1lBQ2pCLElBQUksRUFBRSxPQUFPO1lBQ2IsSUFBSSxFQUFFLFdBQVc7WUFDakIsS0FBSyxFQUFFLElBQUk7WUFDWCxRQUFRLEVBQUUsS0FBSztZQUNmLE1BQU0sRUFBRTtnQkFDSixDQUFDLEVBQUUsQ0FBQzt3QkFDSSxTQUFTLEVBQUUseUJBQXlCO3dCQUNwQyxFQUFFLEVBQUUsUUFBUTt3QkFDWixJQUFJLEVBQUUsZUFBZTt3QkFDckIsSUFBSSxFQUFFLFFBQVE7d0JBQ2QsS0FBSyxFQUFFLElBQUk7cUJBQ2QsQ0FBQzthQUFDO1NBQ2Q7S0FBQztDQUNUOztBQUVELE1BQU0sS0FBSyw4QkFBOEIsR0FBRztJQUN4QyxFQUFFLEVBQUUsQ0FBQztJQUNMLElBQUksRUFBRSxzQkFBc0I7SUFDNUIsbUJBQW1CLEVBQUUsMEJBQTBCO0lBQy9DLE1BQU0sRUFBRTtRQUNKO1lBQ0ksU0FBUyxFQUFFLHlCQUF5QjtZQUNwQyxFQUFFLEVBQUUsYUFBYTtZQUNqQixJQUFJLEVBQUUsT0FBTztZQUNiLElBQUksRUFBRSxXQUFXO1lBQ2pCLEtBQUssRUFBRSxJQUFJO1lBQ1gsUUFBUSxFQUFFLEtBQUs7WUFDZixNQUFNLEVBQUU7Z0JBQ0osQ0FBQyxFQUFFLENBQUM7d0JBQ0ksU0FBUyxFQUFFLHlCQUF5Qjt3QkFDcEMsRUFBRSxFQUFFLFdBQVc7d0JBQ2YsSUFBSSxFQUFFLGVBQWU7d0JBQ3JCLElBQUksRUFBRSxlQUFlO3dCQUNyQixLQUFLLEVBQUUsSUFBSTtxQkFDZCxDQUFDO2FBQUM7U0FDZDtLQUFDO0NBQ1Q7O0FBRUQsTUFBTSxLQUFLLDJCQUEyQixHQUFHO0lBQ3JDLEVBQUUsRUFBRSxDQUFDO0lBQ0wsSUFBSSxFQUFFLHNCQUFzQjtJQUM1QixtQkFBbUIsRUFBRSwwQkFBMEI7SUFDL0MsTUFBTSxFQUFFO1FBQ0o7WUFDSSxTQUFTLEVBQUUseUJBQXlCO1lBQ3BDLEVBQUUsRUFBRSxhQUFhO1lBQ2pCLElBQUksRUFBRSxPQUFPO1lBQ2IsSUFBSSxFQUFFLFdBQVc7WUFDakIsS0FBSyxFQUFFLElBQUk7WUFDWCxRQUFRLEVBQUUsS0FBSztZQUNmLE1BQU0sRUFBRTtnQkFDSixDQUFDLEVBQUUsQ0FBQzt3QkFDSSxTQUFTLEVBQUUseUJBQXlCO3dCQUNwQyxFQUFFLEVBQUUsVUFBVTt3QkFDZCxJQUFJLEVBQUUsVUFBVTt3QkFDaEIsSUFBSSxFQUFFLE1BQU07d0JBQ1osS0FBSyxFQUFFLElBQUk7cUJBQ2QsQ0FBQzthQUFDO1NBQ2Q7S0FBQztDQUNUOztBQUVELE1BQU0sS0FBSywrQkFBK0IsR0FBRztJQUN6QyxFQUFFLEVBQUUsQ0FBQztJQUNMLElBQUksRUFBRSxzQkFBc0I7SUFDNUIsbUJBQW1CLEVBQUUsMEJBQTBCO0lBQy9DLE1BQU0sRUFBRTtRQUNKO1lBQ0ksU0FBUyxFQUFFLHlCQUF5QjtZQUNwQyxFQUFFLEVBQUUsYUFBYTtZQUNqQixJQUFJLEVBQUUsT0FBTztZQUNiLElBQUksRUFBRSxXQUFXO1lBQ2pCLEtBQUssRUFBRSxJQUFJO1lBQ1gsUUFBUSxFQUFFLEtBQUs7WUFDZixNQUFNLEVBQUU7Z0JBQ0osQ0FBQyxFQUFFLENBQUM7d0JBRUksU0FBUyxFQUFFLHlCQUF5Qjt3QkFDcEMsRUFBRSxFQUFFLGtCQUFrQjt3QkFDdEIsSUFBSSxFQUFFLGVBQWU7d0JBQ3JCLElBQUksRUFBRSxVQUFVO3dCQUNoQixLQUFLLEVBQUUsY0FBYzt3QkFDckIsUUFBUSxFQUFFLEtBQUs7d0JBQ2YsUUFBUSxFQUFFLEtBQUs7d0JBQ2YsVUFBVSxFQUFFLEtBQUs7d0JBQ2pCLE9BQU8sRUFBRTs0QkFDTDtnQ0FDSSxFQUFFLEVBQUUsT0FBTztnQ0FDWCxJQUFJLEVBQUUsY0FBYzs2QkFDdkI7NEJBQ0Q7Z0NBQ0ksRUFBRSxFQUFFLE1BQU07Z0NBQ1YsSUFBSSxFQUFFLFVBQVU7NkJBQ25COzRCQUNEO2dDQUNJLEVBQUUsRUFBRSxNQUFNO2dDQUNWLElBQUksRUFBRSxVQUFVOzZCQUNuQjs0QkFDRDtnQ0FDSSxFQUFFLEVBQUUsTUFBTTtnQ0FDVixJQUFJLEVBQUUsVUFBVTs2QkFDbkI7NEJBQ0Q7Z0NBQ0ksRUFBRSxFQUFFLE1BQU07Z0NBQ1YsSUFBSSxFQUFFLFVBQVU7NkJBQ25CO3lCQUNKO3FCQUNKLENBQUM7YUFBQztTQUNkO0tBQUM7Q0FDVDs7QUFFRCxNQUFNLEtBQUssYUFBYSxHQUFHO0lBQ3ZCLEVBQUUsRUFBRSxDQUFDO0lBQ0wsSUFBSSxFQUFFLHNCQUFzQjtJQUM1QixtQkFBbUIsRUFBRSwwQkFBMEI7SUFDL0MscUJBQXFCLEVBQUUsb0JBQW9CO0lBQzNDLG9CQUFvQixFQUFFLG9CQUFvQjtJQUMxQyxJQUFJLEVBQUUsRUFBRTtJQUNSLE1BQU0sRUFBRTtRQUNKO1lBQ0ksU0FBUyxFQUFFLHlCQUF5QjtZQUNwQyxFQUFFLEVBQUUsYUFBYTtZQUNqQixJQUFJLEVBQUUsT0FBTztZQUNiLElBQUksRUFBRSxXQUFXO1lBQ2pCLEtBQUssRUFBRSxJQUFJO1lBQ1gsUUFBUSxFQUFFLEtBQUs7WUFDZixRQUFRLEVBQUUsS0FBSztZQUNmLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLE9BQU8sRUFBRSxDQUFDO1lBQ1YsV0FBVyxFQUFFLElBQUk7WUFDakIsU0FBUyxFQUFFLENBQUM7WUFDWixTQUFTLEVBQUUsQ0FBQztZQUNaLFFBQVEsRUFBRSxJQUFJO1lBQ2QsUUFBUSxFQUFFLElBQUk7WUFDZCxZQUFZLEVBQUUsSUFBSTtZQUNsQixVQUFVLEVBQUUsSUFBSTtZQUNoQixhQUFhLEVBQUUsSUFBSTtZQUNuQixPQUFPLEVBQUUsSUFBSTtZQUNiLE9BQU8sRUFBRSxJQUFJO1lBQ2IsZ0JBQWdCLEVBQUUsSUFBSTtZQUN0QixjQUFjLEVBQUUsSUFBSTtZQUNwQixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLEdBQUcsRUFBRSxJQUFJO1lBQ1QsU0FBUyxFQUFFLElBQUk7WUFDZixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLE1BQU0sRUFBRSxJQUFJO1lBQ1osS0FBSyxFQUFFLENBQUM7WUFDUixLQUFLLEVBQUUsQ0FBQztZQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBQ1AsbUJBQW1CLEVBQUUsSUFBSTtZQUN6QixlQUFlLEVBQUUsQ0FBQztZQUNsQixNQUFNLEVBQUU7Z0JBQ0osQ0FBQyxFQUFFO29CQUNDO3dCQUNJLFNBQVMsRUFBRSx5QkFBeUI7d0JBQ3BDLEVBQUUsRUFBRSxZQUFZO3dCQUNoQixJQUFJLEVBQUUsWUFBWTt3QkFDbEIsSUFBSSxFQUFFLE1BQU07d0JBQ1osS0FBSyxFQUFFLElBQUk7d0JBQ1gsUUFBUSxFQUFFLElBQUk7d0JBQ2QsUUFBUSxFQUFFLEtBQUs7d0JBQ2YsVUFBVSxFQUFFLEtBQUs7d0JBQ2pCLE9BQU8sRUFBRSxDQUFDO3dCQUNWLFdBQVcsRUFBRSxJQUFJO3dCQUNqQixTQUFTLEVBQUUsQ0FBQzt3QkFDWixTQUFTLEVBQUUsQ0FBQzt3QkFDWixRQUFRLEVBQUUsSUFBSTt3QkFDZCxRQUFRLEVBQUUsSUFBSTt3QkFDZCxZQUFZLEVBQUUsSUFBSTt3QkFDbEIsVUFBVSxFQUFFLElBQUk7d0JBQ2hCLGFBQWEsRUFBRSxJQUFJO3dCQUNuQixPQUFPLEVBQUUsSUFBSTt3QkFDYixPQUFPLEVBQUUsSUFBSTt3QkFDYixnQkFBZ0IsRUFBRSxJQUFJO3dCQUN0QixjQUFjLEVBQUUsSUFBSTt3QkFDcEIsaUJBQWlCLEVBQUUsSUFBSTt3QkFDdkIsR0FBRyxFQUFFLElBQUk7d0JBQ1QsU0FBUyxFQUFFLElBQUk7d0JBQ2YsTUFBTSxFQUFFOzRCQUNKLGVBQWUsRUFBRSxDQUFDOzRCQUNsQixVQUFVLEVBQUUsQ0FBQzt5QkFDaEI7d0JBQ0QsaUJBQWlCLEVBQUUsSUFBSTt3QkFDdkIsTUFBTSxFQUFFOzRCQUNKLEdBQUcsRUFBRSxDQUFDLENBQUM7NEJBQ1AsTUFBTSxFQUFFLENBQUMsQ0FBQzs0QkFDVixPQUFPLEVBQUUsQ0FBQzt5QkFDYjt3QkFDRCxLQUFLLEVBQUUsQ0FBQzt3QkFDUixLQUFLLEVBQUUsQ0FBQzt3QkFDUixHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsbUJBQW1CLEVBQUUsSUFBSTtxQkFDNUI7aUJBQ0o7Z0JBQ0QsQ0FBQyxFQUFFO29CQUNDO3dCQUNJLFNBQVMsRUFBRSx5QkFBeUI7d0JBQ3BDLEVBQUUsRUFBRSxVQUFVO3dCQUNkLElBQUksRUFBRSxVQUFVO3dCQUNoQixJQUFJLEVBQUUsU0FBUzt3QkFDZixLQUFLLEVBQUUsSUFBSTt3QkFDWCxRQUFRLEVBQUUsSUFBSTt3QkFDZCxRQUFRLEVBQUUsS0FBSzt3QkFDZixVQUFVLEVBQUUsS0FBSzt3QkFDakIsT0FBTyxFQUFFLENBQUM7d0JBQ1YsV0FBVyxFQUFFLGlCQUFpQjt3QkFDOUIsU0FBUyxFQUFFLENBQUM7d0JBQ1osU0FBUyxFQUFFLENBQUM7d0JBQ1osUUFBUSxFQUFFLElBQUk7d0JBQ2QsUUFBUSxFQUFFLElBQUk7d0JBQ2QsWUFBWSxFQUFFLElBQUk7d0JBQ2xCLFVBQVUsRUFBRSxJQUFJO3dCQUNoQixhQUFhLEVBQUUsSUFBSTt3QkFDbkIsT0FBTyxFQUFFLElBQUk7d0JBQ2IsT0FBTyxFQUFFLElBQUk7d0JBQ2IsZ0JBQWdCLEVBQUUsSUFBSTt3QkFDdEIsY0FBYyxFQUFFLElBQUk7d0JBQ3BCLGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLEdBQUcsRUFBRSxJQUFJO3dCQUNULFNBQVMsRUFBRSxJQUFJO3dCQUNmLE1BQU0sRUFBRTs0QkFDSixlQUFlLEVBQUUsQ0FBQzs0QkFDbEIsVUFBVSxFQUFFLENBQUM7eUJBQ2hCO3dCQUNELGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLE1BQU0sRUFBRTs0QkFDSixHQUFHLEVBQUUsQ0FBQyxDQUFDOzRCQUNQLE1BQU0sRUFBRSxDQUFDLENBQUM7NEJBQ1YsT0FBTyxFQUFFLENBQUM7eUJBQ2I7d0JBQ0QsS0FBSyxFQUFFLENBQUM7d0JBQ1IsS0FBSyxFQUFFLENBQUM7d0JBQ1IsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLG1CQUFtQixFQUFFLElBQUk7cUJBQzVCO2lCQUNKO2FBQ0o7U0FDSjtRQUNEO1lBQ0ksU0FBUyxFQUFFLHlCQUF5QjtZQUNwQyxFQUFFLEVBQUUsYUFBYTtZQUNqQixJQUFJLEVBQUUsT0FBTztZQUNiLElBQUksRUFBRSxXQUFXO1lBQ2pCLEtBQUssRUFBRSxJQUFJO1lBQ1gsUUFBUSxFQUFFLEtBQUs7WUFDZixRQUFRLEVBQUUsS0FBSztZQUNmLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLE9BQU8sRUFBRSxDQUFDO1lBQ1YsV0FBVyxFQUFFLElBQUk7WUFDakIsU0FBUyxFQUFFLENBQUM7WUFDWixTQUFTLEVBQUUsQ0FBQztZQUNaLFFBQVEsRUFBRSxJQUFJO1lBQ2QsUUFBUSxFQUFFLElBQUk7WUFDZCxZQUFZLEVBQUUsSUFBSTtZQUNsQixVQUFVLEVBQUUsSUFBSTtZQUNoQixhQUFhLEVBQUUsSUFBSTtZQUNuQixPQUFPLEVBQUUsSUFBSTtZQUNiLE9BQU8sRUFBRSxJQUFJO1lBQ2IsZ0JBQWdCLEVBQUUsSUFBSTtZQUN0QixjQUFjLEVBQUUsSUFBSTtZQUNwQixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLEdBQUcsRUFBRSxJQUFJO1lBQ1QsU0FBUyxFQUFFLElBQUk7WUFDZixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLE1BQU0sRUFBRSxJQUFJO1lBQ1osS0FBSyxFQUFFLENBQUM7WUFDUixLQUFLLEVBQUUsQ0FBQztZQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBQ1AsbUJBQW1CLEVBQUUsSUFBSTtZQUN6QixlQUFlLEVBQUUsQ0FBQztZQUNsQixNQUFNLEVBQUU7Z0JBQ0osQ0FBQyxFQUFFO29CQUNDO3dCQUNJLFNBQVMsRUFBRSx5QkFBeUI7d0JBQ3BDLEVBQUUsRUFBRSxZQUFZO3dCQUNoQixJQUFJLEVBQUUsWUFBWTt3QkFDbEIsSUFBSSxFQUFFLFNBQVM7d0JBQ2YsS0FBSyxFQUFFLElBQUk7d0JBQ1gsUUFBUSxFQUFFLElBQUk7d0JBQ2QsUUFBUSxFQUFFLEtBQUs7d0JBQ2YsVUFBVSxFQUFFLEtBQUs7d0JBQ2pCLE9BQU8sRUFBRSxDQUFDO3dCQUNWLFdBQVcsRUFBRSxpQkFBaUI7d0JBQzlCLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFlBQVksRUFBRSxJQUFJO3dCQUNsQixVQUFVLEVBQUUsSUFBSTt3QkFDaEIsYUFBYSxFQUFFLElBQUk7d0JBQ25CLE9BQU8sRUFBRSxJQUFJO3dCQUNiLE9BQU8sRUFBRSxJQUFJO3dCQUNiLGdCQUFnQixFQUFFLElBQUk7d0JBQ3RCLGNBQWMsRUFBRSxJQUFJO3dCQUNwQixpQkFBaUIsRUFBRSxJQUFJO3dCQUN2QixHQUFHLEVBQUUsSUFBSTt3QkFDVCxTQUFTLEVBQUUsSUFBSTt3QkFDZixNQUFNLEVBQUU7NEJBQ0osZUFBZSxFQUFFLENBQUM7NEJBQ2xCLFVBQVUsRUFBRSxDQUFDO3lCQUNoQjt3QkFDRCxpQkFBaUIsRUFBRSxJQUFJO3dCQUN2QixNQUFNLEVBQUU7NEJBQ0osR0FBRyxFQUFFLENBQUMsQ0FBQzs0QkFDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDOzRCQUNWLE9BQU8sRUFBRSxDQUFDO3lCQUNiO3dCQUNELEtBQUssRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxDQUFDO3dCQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxtQkFBbUIsRUFBRSxJQUFJO3FCQUM1QjtpQkFDSjtnQkFDRCxDQUFDLEVBQUU7b0JBQ0M7d0JBQ0ksU0FBUyxFQUFFLHlCQUF5Qjt3QkFDcEMsRUFBRSxFQUFFLFVBQVU7d0JBQ2QsSUFBSSxFQUFFLFVBQVU7d0JBQ2hCLElBQUksRUFBRSxNQUFNO3dCQUNaLEtBQUssRUFBRSxJQUFJO3dCQUNYLFFBQVEsRUFBRSxLQUFLO3dCQUNmLFFBQVEsRUFBRSxLQUFLO3dCQUNmLFVBQVUsRUFBRSxLQUFLO3dCQUNqQixPQUFPLEVBQUUsQ0FBQzt3QkFDVixXQUFXLEVBQUUsSUFBSTt3QkFDakIsU0FBUyxFQUFFLENBQUM7d0JBQ1osU0FBUyxFQUFFLENBQUM7d0JBQ1osUUFBUSxFQUFFLElBQUk7d0JBQ2QsUUFBUSxFQUFFLElBQUk7d0JBQ2QsWUFBWSxFQUFFLElBQUk7d0JBQ2xCLFVBQVUsRUFBRSxJQUFJO3dCQUNoQixhQUFhLEVBQUUsSUFBSTt3QkFDbkIsT0FBTyxFQUFFLElBQUk7d0JBQ2IsT0FBTyxFQUFFLElBQUk7d0JBQ2IsZ0JBQWdCLEVBQUUsSUFBSTt3QkFDdEIsY0FBYyxFQUFFLElBQUk7d0JBQ3BCLGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLEdBQUcsRUFBRSxJQUFJO3dCQUNULFNBQVMsRUFBRSxVQUFVO3dCQUNyQixNQUFNLEVBQUU7NEJBQ0osZUFBZSxFQUFFLENBQUM7NEJBQ2xCLFVBQVUsRUFBRSxDQUFDO3lCQUNoQjt3QkFDRCxpQkFBaUIsRUFBRSxJQUFJO3dCQUN2QixNQUFNLEVBQUU7NEJBQ0osR0FBRyxFQUFFLENBQUMsQ0FBQzs0QkFDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDOzRCQUNWLE9BQU8sRUFBRSxDQUFDO3lCQUNiO3dCQUNELEtBQUssRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxDQUFDO3dCQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxtQkFBbUIsRUFBRSxJQUFJO3FCQUM1QjtpQkFDSjthQUNKO1NBQ0o7UUFDRDtZQUNJLFNBQVMsRUFBRSx5QkFBeUI7WUFDcEMsRUFBRSxFQUFFLGFBQWE7WUFDakIsSUFBSSxFQUFFLE9BQU87WUFDYixJQUFJLEVBQUUsV0FBVztZQUNqQixLQUFLLEVBQUUsSUFBSTtZQUNYLFFBQVEsRUFBRSxLQUFLO1lBQ2YsUUFBUSxFQUFFLEtBQUs7WUFDZixVQUFVLEVBQUUsS0FBSztZQUNqQixPQUFPLEVBQUUsQ0FBQztZQUNWLFdBQVcsRUFBRSxJQUFJO1lBQ2pCLFNBQVMsRUFBRSxDQUFDO1lBQ1osU0FBUyxFQUFFLENBQUM7WUFDWixRQUFRLEVBQUUsSUFBSTtZQUNkLFFBQVEsRUFBRSxJQUFJO1lBQ2QsWUFBWSxFQUFFLElBQUk7WUFDbEIsVUFBVSxFQUFFLElBQUk7WUFDaEIsYUFBYSxFQUFFLElBQUk7WUFDbkIsT0FBTyxFQUFFLElBQUk7WUFDYixPQUFPLEVBQUUsSUFBSTtZQUNiLGdCQUFnQixFQUFFLElBQUk7WUFDdEIsY0FBYyxFQUFFLElBQUk7WUFDcEIsaUJBQWlCLEVBQUUsSUFBSTtZQUN2QixHQUFHLEVBQUUsSUFBSTtZQUNULFNBQVMsRUFBRSxJQUFJO1lBQ2YsaUJBQWlCLEVBQUUsSUFBSTtZQUN2QixNQUFNLEVBQUUsSUFBSTtZQUNaLEtBQUssRUFBRSxDQUFDO1lBQ1IsS0FBSyxFQUFFLENBQUM7WUFDUixHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBQ1AsR0FBRyxFQUFFLENBQUMsQ0FBQztZQUNQLG1CQUFtQixFQUFFLElBQUk7WUFDekIsZUFBZSxFQUFFLENBQUM7WUFDbEIsTUFBTSxFQUFFO2dCQUNKLENBQUMsRUFBRTtvQkFDQzt3QkFDSSxTQUFTLEVBQUUseUJBQXlCO3dCQUNwQyxFQUFFLEVBQUUsV0FBVzt3QkFDZixJQUFJLEVBQUUsV0FBVzt3QkFDakIsSUFBSSxFQUFFLFVBQVU7d0JBQ2hCLEtBQUssRUFBRSxjQUFjO3dCQUNyQixRQUFRLEVBQUUsS0FBSzt3QkFDZixRQUFRLEVBQUUsS0FBSzt3QkFDZixVQUFVLEVBQUUsS0FBSzt3QkFDakIsT0FBTyxFQUFFLENBQUM7d0JBQ1YsV0FBVyxFQUFFLElBQUk7d0JBQ2pCLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFlBQVksRUFBRSxJQUFJO3dCQUNsQixVQUFVLEVBQUUsSUFBSTt3QkFDaEIsYUFBYSxFQUFFLElBQUk7d0JBQ25CLE9BQU8sRUFBRTs0QkFDTDtnQ0FDSSxFQUFFLEVBQUUsT0FBTztnQ0FDWCxJQUFJLEVBQUUsY0FBYzs2QkFDdkI7NEJBQ0Q7Z0NBQ0ksRUFBRSxFQUFFLFVBQVU7Z0NBQ2QsSUFBSSxFQUFFLFVBQVU7NkJBQ25COzRCQUNEO2dDQUNJLEVBQUUsRUFBRSxlQUFlO2dDQUNuQixJQUFJLEVBQUUsZUFBZTs2QkFDeEI7eUJBQ0o7d0JBQ0QsT0FBTyxFQUFFLElBQUk7d0JBQ2IsZ0JBQWdCLEVBQUUsSUFBSTt3QkFDdEIsY0FBYyxFQUFFLElBQUk7d0JBQ3BCLGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLEdBQUcsRUFBRSxJQUFJO3dCQUNULFNBQVMsRUFBRSxJQUFJO3dCQUNmLE1BQU0sRUFBRTs0QkFDSixlQUFlLEVBQUUsQ0FBQzs0QkFDbEIsVUFBVSxFQUFFLENBQUM7eUJBQ2hCO3dCQUNELGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLE1BQU0sRUFBRTs0QkFDSixHQUFHLEVBQUUsQ0FBQyxDQUFDOzRCQUNQLE1BQU0sRUFBRSxDQUFDLENBQUM7NEJBQ1YsT0FBTyxFQUFFLENBQUM7eUJBQ2I7d0JBQ0QsS0FBSyxFQUFFLENBQUM7d0JBQ1IsS0FBSyxFQUFFLENBQUM7d0JBQ1IsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLG1CQUFtQixFQUFFLElBQUk7d0JBQ3pCLFFBQVEsRUFBRSxJQUFJO3dCQUNkLGNBQWMsRUFBRSxJQUFJO3FCQUN2QjtpQkFDSjtnQkFDRCxDQUFDLEVBQUU7b0JBQ0M7d0JBQ0ksU0FBUyxFQUFFLHlCQUF5Qjt3QkFDcEMsRUFBRSxFQUFFLGNBQWM7d0JBQ2xCLElBQUksRUFBRSxjQUFjO3dCQUNwQixJQUFJLEVBQUUsTUFBTTt3QkFDWixLQUFLLEVBQUUsSUFBSTt3QkFDWCxRQUFRLEVBQUUsS0FBSzt3QkFDZixRQUFRLEVBQUUsS0FBSzt3QkFDZixVQUFVLEVBQUUsS0FBSzt3QkFDakIsT0FBTyxFQUFFLENBQUM7d0JBQ1YsV0FBVyxFQUFFLG1CQUFtQjt3QkFDaEMsU0FBUyxFQUFFLENBQUM7d0JBQ1osU0FBUyxFQUFFLENBQUM7d0JBQ1osUUFBUSxFQUFFLElBQUk7d0JBQ2QsUUFBUSxFQUFFLElBQUk7d0JBQ2QsWUFBWSxFQUFFLElBQUk7d0JBQ2xCLFVBQVUsRUFBRSxJQUFJO3dCQUNoQixhQUFhLEVBQUUsSUFBSTt3QkFDbkIsT0FBTyxFQUFFLElBQUk7d0JBQ2IsT0FBTyxFQUFFLElBQUk7d0JBQ2IsZ0JBQWdCLEVBQUUsSUFBSTt3QkFDdEIsY0FBYyxFQUFFLElBQUk7d0JBQ3BCLGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLEdBQUcsRUFBRSxJQUFJO3dCQUNULFNBQVMsRUFBRSxJQUFJO3dCQUNmLE1BQU0sRUFBRTs0QkFDSixlQUFlLEVBQUUsQ0FBQzs0QkFDbEIsVUFBVSxFQUFFLENBQUM7eUJBQ2hCO3dCQUNELGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLE1BQU0sRUFBRTs0QkFDSixHQUFHLEVBQUUsQ0FBQyxDQUFDOzRCQUNQLE1BQU0sRUFBRSxDQUFDLENBQUM7NEJBQ1YsT0FBTyxFQUFFLENBQUM7eUJBQ2I7d0JBQ0QsS0FBSyxFQUFFLENBQUM7d0JBQ1IsS0FBSyxFQUFFLENBQUM7d0JBQ1IsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLG1CQUFtQixFQUFFLElBQUk7cUJBQzVCO2lCQUNKO2FBQ0o7U0FDSjtLQUNKO0lBQ0QsUUFBUSxFQUFFO1FBQ047WUFDSSxFQUFFLEVBQUUsU0FBUztZQUNiLElBQUksRUFBRSxTQUFTO1NBQ2xCO1FBQ0Q7WUFDSSxFQUFFLEVBQUUsVUFBVTtZQUNkLElBQUksRUFBRSxVQUFVO1NBQ25CO1FBQ0Q7WUFDSSxFQUFFLEVBQUUsZUFBZTtZQUNuQixJQUFJLEVBQUUsZUFBZTtTQUN4QjtLQUNKO0lBQ0QsZ0JBQWdCLEVBQUUsRUFBRTtJQUNwQixTQUFTLEVBQUUsRUFBRTtJQUNiLEtBQUssRUFBRSxFQUFFO0lBQ1QsUUFBUSxFQUFFLEVBQUU7SUFDWixTQUFTLEVBQUUsRUFBRTtJQUNiLHFCQUFxQixFQUFFLEVBQUU7SUFDekIsWUFBWSxFQUFFLEtBQUs7SUFDbkIsZ0JBQWdCLEVBQUUsY0FBYztDQUNuQzs7QUFFRCxNQUFNLEtBQUssb0JBQW9CLEdBQUc7SUFDOUIsRUFBRSxFQUFFLENBQUM7SUFDTCxRQUFRLEVBQUUsWUFBWTtJQUN0QixtQkFBbUIsRUFBRSwwQkFBMEI7SUFDL0MscUJBQXFCLEVBQUUsb0JBQW9CO0lBQzNDLG9CQUFvQixFQUFFLG9CQUFvQjtJQUMxQyxJQUFJLEVBQUU7UUFDRjtZQUNJLEVBQUUsRUFBRSxPQUFPO1lBQ1gsSUFBSSxFQUFFLE9BQU87U0FDaEI7UUFDRDtZQUNJLEVBQUUsRUFBRSxPQUFPO1lBQ1gsSUFBSSxFQUFFLE9BQU87U0FDaEI7S0FDSjtJQUNELE1BQU0sRUFBRTtRQUNKO1lBQ0ksU0FBUyxFQUFFLHlCQUF5QjtZQUNwQyxFQUFFLEVBQUUsYUFBYTtZQUNqQixJQUFJLEVBQUUsT0FBTztZQUNiLElBQUksRUFBRSxXQUFXO1lBQ2pCLEtBQUssRUFBRSxJQUFJO1lBQ1gsUUFBUSxFQUFFLEtBQUs7WUFDZixRQUFRLEVBQUUsS0FBSztZQUNmLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLE9BQU8sRUFBRSxDQUFDO1lBQ1YsV0FBVyxFQUFFLElBQUk7WUFDakIsU0FBUyxFQUFFLENBQUM7WUFDWixTQUFTLEVBQUUsQ0FBQztZQUNaLFFBQVEsRUFBRSxJQUFJO1lBQ2QsUUFBUSxFQUFFLElBQUk7WUFDZCxZQUFZLEVBQUUsSUFBSTtZQUNsQixVQUFVLEVBQUUsSUFBSTtZQUNoQixhQUFhLEVBQUUsSUFBSTtZQUNuQixPQUFPLEVBQUUsSUFBSTtZQUNiLE9BQU8sRUFBRSxJQUFJO1lBQ2IsZ0JBQWdCLEVBQUUsSUFBSTtZQUN0QixjQUFjLEVBQUUsSUFBSTtZQUNwQixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLEdBQUcsRUFBRSxJQUFJO1lBQ1QsU0FBUyxFQUFFLElBQUk7WUFDZixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLE1BQU0sRUFBRSxJQUFJO1lBQ1osS0FBSyxFQUFFLENBQUM7WUFDUixLQUFLLEVBQUUsQ0FBQztZQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBQ1AsbUJBQW1CLEVBQUUsSUFBSTtZQUN6QixlQUFlLEVBQUUsQ0FBQztZQUNsQixNQUFNLEVBQUU7Z0JBQ0osQ0FBQyxFQUFFO29CQUNDO3dCQUNJLFNBQVMsRUFBRSx5QkFBeUI7d0JBQ3BDLEVBQUUsRUFBRSxZQUFZO3dCQUNoQixJQUFJLEVBQUUsWUFBWTt3QkFDbEIsSUFBSSxFQUFFLE1BQU07d0JBQ1osS0FBSyxFQUFFLElBQUk7d0JBQ1gsUUFBUSxFQUFFLElBQUk7d0JBQ2QsUUFBUSxFQUFFLEtBQUs7d0JBQ2YsVUFBVSxFQUFFLEtBQUs7d0JBQ2pCLE9BQU8sRUFBRSxDQUFDO3dCQUNWLFdBQVcsRUFBRSxJQUFJO3dCQUNqQixTQUFTLEVBQUUsQ0FBQzt3QkFDWixTQUFTLEVBQUUsQ0FBQzt3QkFDWixRQUFRLEVBQUUsSUFBSTt3QkFDZCxRQUFRLEVBQUUsSUFBSTt3QkFDZCxZQUFZLEVBQUUsSUFBSTt3QkFDbEIsVUFBVSxFQUFFLElBQUk7d0JBQ2hCLGFBQWEsRUFBRSxJQUFJO3dCQUNuQixPQUFPLEVBQUUsSUFBSTt3QkFDYixPQUFPLEVBQUUsSUFBSTt3QkFDYixnQkFBZ0IsRUFBRSxJQUFJO3dCQUN0QixjQUFjLEVBQUUsSUFBSTt3QkFDcEIsaUJBQWlCLEVBQUUsSUFBSTt3QkFDdkIsR0FBRyxFQUFFLElBQUk7d0JBQ1QsU0FBUyxFQUFFLElBQUk7d0JBQ2YsTUFBTSxFQUFFOzRCQUNKLGVBQWUsRUFBRSxDQUFDOzRCQUNsQixVQUFVLEVBQUUsQ0FBQzt5QkFDaEI7d0JBQ0QsaUJBQWlCLEVBQUUsSUFBSTt3QkFDdkIsTUFBTSxFQUFFOzRCQUNKLEdBQUcsRUFBRSxDQUFDLENBQUM7NEJBQ1AsTUFBTSxFQUFFLENBQUMsQ0FBQzs0QkFDVixPQUFPLEVBQUUsQ0FBQzt5QkFDYjt3QkFDRCxLQUFLLEVBQUUsQ0FBQzt3QkFDUixLQUFLLEVBQUUsQ0FBQzt3QkFDUixHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsbUJBQW1CLEVBQUUsSUFBSTtxQkFDNUI7aUJBQ0o7Z0JBQ0QsQ0FBQyxFQUFFO29CQUNDO3dCQUNJLFNBQVMsRUFBRSx5QkFBeUI7d0JBQ3BDLEVBQUUsRUFBRSxVQUFVO3dCQUNkLElBQUksRUFBRSxVQUFVO3dCQUNoQixJQUFJLEVBQUUsU0FBUzt3QkFDZixLQUFLLEVBQUUsSUFBSTt3QkFDWCxRQUFRLEVBQUUsSUFBSTt3QkFDZCxRQUFRLEVBQUUsS0FBSzt3QkFDZixVQUFVLEVBQUUsS0FBSzt3QkFDakIsT0FBTyxFQUFFLENBQUM7d0JBQ1YsV0FBVyxFQUFFLGlCQUFpQjt3QkFDOUIsU0FBUyxFQUFFLENBQUM7d0JBQ1osU0FBUyxFQUFFLENBQUM7d0JBQ1osUUFBUSxFQUFFLElBQUk7d0JBQ2QsUUFBUSxFQUFFLElBQUk7d0JBQ2QsWUFBWSxFQUFFLElBQUk7d0JBQ2xCLFVBQVUsRUFBRSxJQUFJO3dCQUNoQixhQUFhLEVBQUUsSUFBSTt3QkFDbkIsT0FBTyxFQUFFLElBQUk7d0JBQ2IsT0FBTyxFQUFFLElBQUk7d0JBQ2IsZ0JBQWdCLEVBQUUsSUFBSTt3QkFDdEIsY0FBYyxFQUFFLElBQUk7d0JBQ3BCLGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLEdBQUcsRUFBRSxJQUFJO3dCQUNULFNBQVMsRUFBRSxJQUFJO3dCQUNmLE1BQU0sRUFBRTs0QkFDSixlQUFlLEVBQUUsQ0FBQzs0QkFDbEIsVUFBVSxFQUFFLENBQUM7eUJBQ2hCO3dCQUNELGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLE1BQU0sRUFBRTs0QkFDSixHQUFHLEVBQUUsQ0FBQyxDQUFDOzRCQUNQLE1BQU0sRUFBRSxDQUFDLENBQUM7NEJBQ1YsT0FBTyxFQUFFLENBQUM7eUJBQ2I7d0JBQ0QsS0FBSyxFQUFFLENBQUM7d0JBQ1IsS0FBSyxFQUFFLENBQUM7d0JBQ1IsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLG1CQUFtQixFQUFFLElBQUk7cUJBQzVCO2lCQUNKO2FBQ0o7U0FDSjtRQUNEO1lBQ0ksU0FBUyxFQUFFLHlCQUF5QjtZQUNwQyxFQUFFLEVBQUUsYUFBYTtZQUNqQixJQUFJLEVBQUUsT0FBTztZQUNiLElBQUksRUFBRSxXQUFXO1lBQ2pCLEtBQUssRUFBRSxJQUFJO1lBQ1gsUUFBUSxFQUFFLEtBQUs7WUFDZixRQUFRLEVBQUUsS0FBSztZQUNmLFVBQVUsRUFBRSxLQUFLO1lBQ2pCLE9BQU8sRUFBRSxDQUFDO1lBQ1YsV0FBVyxFQUFFLElBQUk7WUFDakIsU0FBUyxFQUFFLENBQUM7WUFDWixTQUFTLEVBQUUsQ0FBQztZQUNaLFFBQVEsRUFBRSxJQUFJO1lBQ2QsUUFBUSxFQUFFLElBQUk7WUFDZCxZQUFZLEVBQUUsSUFBSTtZQUNsQixVQUFVLEVBQUUsSUFBSTtZQUNoQixhQUFhLEVBQUUsSUFBSTtZQUNuQixPQUFPLEVBQUUsSUFBSTtZQUNiLE9BQU8sRUFBRSxJQUFJO1lBQ2IsZ0JBQWdCLEVBQUUsSUFBSTtZQUN0QixjQUFjLEVBQUUsSUFBSTtZQUNwQixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLEdBQUcsRUFBRSxJQUFJO1lBQ1QsU0FBUyxFQUFFLElBQUk7WUFDZixpQkFBaUIsRUFBRSxJQUFJO1lBQ3ZCLE1BQU0sRUFBRSxJQUFJO1lBQ1osS0FBSyxFQUFFLENBQUM7WUFDUixLQUFLLEVBQUUsQ0FBQztZQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7WUFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBQ1AsbUJBQW1CLEVBQUUsSUFBSTtZQUN6QixlQUFlLEVBQUUsQ0FBQztZQUNsQixNQUFNLEVBQUU7Z0JBQ0osQ0FBQyxFQUFFO29CQUNDO3dCQUNJLFNBQVMsRUFBRSx5QkFBeUI7d0JBQ3BDLEVBQUUsRUFBRSxZQUFZO3dCQUNoQixJQUFJLEVBQUUsWUFBWTt3QkFDbEIsSUFBSSxFQUFFLFNBQVM7d0JBQ2YsS0FBSyxFQUFFLElBQUk7d0JBQ1gsUUFBUSxFQUFFLElBQUk7d0JBQ2QsUUFBUSxFQUFFLEtBQUs7d0JBQ2YsVUFBVSxFQUFFLEtBQUs7d0JBQ2pCLE9BQU8sRUFBRSxDQUFDO3dCQUNWLFdBQVcsRUFBRSxpQkFBaUI7d0JBQzlCLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFlBQVksRUFBRSxJQUFJO3dCQUNsQixVQUFVLEVBQUUsSUFBSTt3QkFDaEIsYUFBYSxFQUFFLElBQUk7d0JBQ25CLE9BQU8sRUFBRSxJQUFJO3dCQUNiLE9BQU8sRUFBRSxJQUFJO3dCQUNiLGdCQUFnQixFQUFFLElBQUk7d0JBQ3RCLGNBQWMsRUFBRSxJQUFJO3dCQUNwQixpQkFBaUIsRUFBRSxJQUFJO3dCQUN2QixHQUFHLEVBQUUsSUFBSTt3QkFDVCxTQUFTLEVBQUUsSUFBSTt3QkFDZixNQUFNLEVBQUU7NEJBQ0osZUFBZSxFQUFFLENBQUM7NEJBQ2xCLFVBQVUsRUFBRSxDQUFDO3lCQUNoQjt3QkFDRCxpQkFBaUIsRUFBRSxJQUFJO3dCQUN2QixNQUFNLEVBQUU7NEJBQ0osR0FBRyxFQUFFLENBQUMsQ0FBQzs0QkFDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDOzRCQUNWLE9BQU8sRUFBRSxDQUFDO3lCQUNiO3dCQUNELEtBQUssRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxDQUFDO3dCQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxtQkFBbUIsRUFBRSxJQUFJO3FCQUM1QjtpQkFDSjtnQkFDRCxDQUFDLEVBQUU7b0JBQ0M7d0JBQ0ksU0FBUyxFQUFFLHlCQUF5Qjt3QkFDcEMsRUFBRSxFQUFFLFVBQVU7d0JBQ2QsSUFBSSxFQUFFLFVBQVU7d0JBQ2hCLElBQUksRUFBRSxNQUFNO3dCQUNaLEtBQUssRUFBRSxJQUFJO3dCQUNYLFFBQVEsRUFBRSxLQUFLO3dCQUNmLFFBQVEsRUFBRSxLQUFLO3dCQUNmLFVBQVUsRUFBRSxLQUFLO3dCQUNqQixPQUFPLEVBQUUsQ0FBQzt3QkFDVixXQUFXLEVBQUUsSUFBSTt3QkFDakIsU0FBUyxFQUFFLENBQUM7d0JBQ1osU0FBUyxFQUFFLENBQUM7d0JBQ1osUUFBUSxFQUFFLElBQUk7d0JBQ2QsUUFBUSxFQUFFLElBQUk7d0JBQ2QsWUFBWSxFQUFFLElBQUk7d0JBQ2xCLFVBQVUsRUFBRSxJQUFJO3dCQUNoQixhQUFhLEVBQUUsSUFBSTt3QkFDbkIsT0FBTyxFQUFFLElBQUk7d0JBQ2IsT0FBTyxFQUFFLElBQUk7d0JBQ2IsZ0JBQWdCLEVBQUUsSUFBSTt3QkFDdEIsY0FBYyxFQUFFLElBQUk7d0JBQ3BCLGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLEdBQUcsRUFBRSxJQUFJO3dCQUNULFNBQVMsRUFBRSxVQUFVO3dCQUNyQixNQUFNLEVBQUU7NEJBQ0osZUFBZSxFQUFFLENBQUM7NEJBQ2xCLFVBQVUsRUFBRSxDQUFDO3lCQUNoQjt3QkFDRCxpQkFBaUIsRUFBRSxJQUFJO3dCQUN2QixNQUFNLEVBQUU7NEJBQ0osR0FBRyxFQUFFLENBQUMsQ0FBQzs0QkFDUCxNQUFNLEVBQUUsQ0FBQyxDQUFDOzRCQUNWLE9BQU8sRUFBRSxDQUFDO3lCQUNiO3dCQUNELEtBQUssRUFBRSxDQUFDO3dCQUNSLEtBQUssRUFBRSxDQUFDO3dCQUNSLEdBQUcsRUFBRSxDQUFDLENBQUM7d0JBQ1AsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxtQkFBbUIsRUFBRSxJQUFJO3FCQUM1QjtpQkFDSjthQUNKO1NBQ0o7UUFDRDtZQUNJLFNBQVMsRUFBRSx5QkFBeUI7WUFDcEMsRUFBRSxFQUFFLGFBQWE7WUFDakIsSUFBSSxFQUFFLE9BQU87WUFDYixJQUFJLEVBQUUsV0FBVztZQUNqQixLQUFLLEVBQUUsSUFBSTtZQUNYLFFBQVEsRUFBRSxLQUFLO1lBQ2YsUUFBUSxFQUFFLEtBQUs7WUFDZixVQUFVLEVBQUUsS0FBSztZQUNqQixPQUFPLEVBQUUsQ0FBQztZQUNWLFdBQVcsRUFBRSxJQUFJO1lBQ2pCLFNBQVMsRUFBRSxDQUFDO1lBQ1osU0FBUyxFQUFFLENBQUM7WUFDWixRQUFRLEVBQUUsSUFBSTtZQUNkLFFBQVEsRUFBRSxJQUFJO1lBQ2QsWUFBWSxFQUFFLElBQUk7WUFDbEIsVUFBVSxFQUFFLElBQUk7WUFDaEIsYUFBYSxFQUFFLElBQUk7WUFDbkIsT0FBTyxFQUFFLElBQUk7WUFDYixPQUFPLEVBQUUsSUFBSTtZQUNiLGdCQUFnQixFQUFFLElBQUk7WUFDdEIsY0FBYyxFQUFFLElBQUk7WUFDcEIsaUJBQWlCLEVBQUUsSUFBSTtZQUN2QixHQUFHLEVBQUUsSUFBSTtZQUNULFNBQVMsRUFBRSxJQUFJO1lBQ2YsaUJBQWlCLEVBQUUsSUFBSTtZQUN2QixNQUFNLEVBQUUsSUFBSTtZQUNaLEtBQUssRUFBRSxDQUFDO1lBQ1IsS0FBSyxFQUFFLENBQUM7WUFDUixHQUFHLEVBQUUsQ0FBQyxDQUFDO1lBQ1AsR0FBRyxFQUFFLENBQUMsQ0FBQztZQUNQLG1CQUFtQixFQUFFLElBQUk7WUFDekIsZUFBZSxFQUFFLENBQUM7WUFDbEIsTUFBTSxFQUFFO2dCQUNKLENBQUMsRUFBRTtvQkFDQzt3QkFDSSxTQUFTLEVBQUUseUJBQXlCO3dCQUNwQyxFQUFFLEVBQUUsV0FBVzt3QkFDZixJQUFJLEVBQUUsV0FBVzt3QkFDakIsSUFBSSxFQUFFLFVBQVU7d0JBQ2hCLEtBQUssRUFBRSxjQUFjO3dCQUNyQixRQUFRLEVBQUUsS0FBSzt3QkFDZixRQUFRLEVBQUUsS0FBSzt3QkFDZixVQUFVLEVBQUUsS0FBSzt3QkFDakIsT0FBTyxFQUFFLENBQUM7d0JBQ1YsV0FBVyxFQUFFLElBQUk7d0JBQ2pCLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFNBQVMsRUFBRSxDQUFDO3dCQUNaLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFFBQVEsRUFBRSxJQUFJO3dCQUNkLFlBQVksRUFBRSxJQUFJO3dCQUNsQixVQUFVLEVBQUUsSUFBSTt3QkFDaEIsYUFBYSxFQUFFLElBQUk7d0JBQ25CLE9BQU8sRUFBRTs0QkFDTDtnQ0FDSSxFQUFFLEVBQUUsT0FBTztnQ0FDWCxJQUFJLEVBQUUsY0FBYzs2QkFDdkI7NEJBQ0Q7Z0NBQ0ksRUFBRSxFQUFFLFVBQVU7Z0NBQ2QsSUFBSSxFQUFFLFVBQVU7NkJBQ25COzRCQUNEO2dDQUNJLEVBQUUsRUFBRSxlQUFlO2dDQUNuQixJQUFJLEVBQUUsZUFBZTs2QkFDeEI7eUJBQ0o7d0JBQ0QsT0FBTyxFQUFFLElBQUk7d0JBQ2IsZ0JBQWdCLEVBQUUsSUFBSTt3QkFDdEIsY0FBYyxFQUFFLElBQUk7d0JBQ3BCLGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLEdBQUcsRUFBRSxJQUFJO3dCQUNULFNBQVMsRUFBRSxJQUFJO3dCQUNmLE1BQU0sRUFBRTs0QkFDSixlQUFlLEVBQUUsQ0FBQzs0QkFDbEIsVUFBVSxFQUFFLENBQUM7eUJBQ2hCO3dCQUNELGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLE1BQU0sRUFBRTs0QkFDSixHQUFHLEVBQUUsQ0FBQyxDQUFDOzRCQUNQLE1BQU0sRUFBRSxDQUFDLENBQUM7NEJBQ1YsT0FBTyxFQUFFLENBQUM7eUJBQ2I7d0JBQ0QsS0FBSyxFQUFFLENBQUM7d0JBQ1IsS0FBSyxFQUFFLENBQUM7d0JBQ1IsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLG1CQUFtQixFQUFFLElBQUk7d0JBQ3pCLFFBQVEsRUFBRSxJQUFJO3dCQUNkLGNBQWMsRUFBRSxJQUFJO3FCQUN2QjtpQkFDSjtnQkFDRCxDQUFDLEVBQUU7b0JBQ0M7d0JBQ0ksU0FBUyxFQUFFLHlCQUF5Qjt3QkFDcEMsRUFBRSxFQUFFLGNBQWM7d0JBQ2xCLElBQUksRUFBRSxjQUFjO3dCQUNwQixJQUFJLEVBQUUsTUFBTTt3QkFDWixLQUFLLEVBQUUsSUFBSTt3QkFDWCxRQUFRLEVBQUUsS0FBSzt3QkFDZixRQUFRLEVBQUUsS0FBSzt3QkFDZixVQUFVLEVBQUUsS0FBSzt3QkFDakIsT0FBTyxFQUFFLENBQUM7d0JBQ1YsV0FBVyxFQUFFLG1CQUFtQjt3QkFDaEMsU0FBUyxFQUFFLENBQUM7d0JBQ1osU0FBUyxFQUFFLENBQUM7d0JBQ1osUUFBUSxFQUFFLElBQUk7d0JBQ2QsUUFBUSxFQUFFLElBQUk7d0JBQ2QsWUFBWSxFQUFFLElBQUk7d0JBQ2xCLFVBQVUsRUFBRSxJQUFJO3dCQUNoQixhQUFhLEVBQUUsSUFBSTt3QkFDbkIsT0FBTyxFQUFFLElBQUk7d0JBQ2IsT0FBTyxFQUFFLElBQUk7d0JBQ2IsZ0JBQWdCLEVBQUUsSUFBSTt3QkFDdEIsY0FBYyxFQUFFLElBQUk7d0JBQ3BCLGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLEdBQUcsRUFBRSxJQUFJO3dCQUNULFNBQVMsRUFBRSxJQUFJO3dCQUNmLE1BQU0sRUFBRTs0QkFDSixlQUFlLEVBQUUsQ0FBQzs0QkFDbEIsVUFBVSxFQUFFLENBQUM7eUJBQ2hCO3dCQUNELGlCQUFpQixFQUFFLElBQUk7d0JBQ3ZCLE1BQU0sRUFBRTs0QkFDSixHQUFHLEVBQUUsQ0FBQyxDQUFDOzRCQUNQLE1BQU0sRUFBRSxDQUFDLENBQUM7NEJBQ1YsT0FBTyxFQUFFLENBQUM7eUJBQ2I7d0JBQ0QsS0FBSyxFQUFFLENBQUM7d0JBQ1IsS0FBSyxFQUFFLENBQUM7d0JBQ1IsR0FBRyxFQUFFLENBQUMsQ0FBQzt3QkFDUCxHQUFHLEVBQUUsQ0FBQyxDQUFDO3dCQUNQLG1CQUFtQixFQUFFLElBQUk7cUJBQzVCO2lCQUNKO2FBQ0o7U0FDSjtLQUNKO0lBQ0QsUUFBUSxFQUFFO1FBQ047WUFDSSxFQUFFLEVBQUUsU0FBUztZQUNiLElBQUksRUFBRSxTQUFTO1NBQ2xCO1FBQ0Q7WUFDSSxFQUFFLEVBQUUsVUFBVTtZQUNkLElBQUksRUFBRSxVQUFVO1NBQ25CO0tBQ0o7SUFDRCxnQkFBZ0IsRUFBRSxFQUFFO0lBQ3BCLFNBQVMsRUFBRSxFQUFFO0lBQ2IsS0FBSyxFQUFFLEVBQUU7SUFDVCxRQUFRLEVBQUUsRUFBRTtJQUNaLFNBQVMsRUFBRSxFQUFFO0lBQ2IscUJBQXFCLEVBQUUsRUFBRTtJQUN6QixZQUFZLEVBQUUsS0FBSztJQUNuQixnQkFBZ0IsRUFBRSxjQUFjO0NBQ25DIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmV4cG9ydCBsZXQgc3RhcnRGb3JtRGF0ZVdpZGdldE1vY2sgPSB7XHJcbiAgICBpZDogNCxcclxuICAgIG5hbWU6ICdDbGFpbSBSZXZpZXcgUHJvY2VzcycsXHJcbiAgICBwcm9jZXNzRGVmaW5pdGlvbklkOiAnQ2xhaW1SZXZpZXdQcm9jZXNzOjI6IDkzJyxcclxuICAgIGZpZWxkczogW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgZmllbGRUeXBlOiAnQ29udGFpbmVyUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICBpZDogMTQ5Nzk1MzI1Mzc4NCxcclxuICAgICAgICAgICAgbmFtZTogJ0xhYmVsJyxcclxuICAgICAgICAgICAgdHlwZTogJ2NvbnRhaW5lcicsXHJcbiAgICAgICAgICAgIHZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgICAgIGZpZWxkczoge1xyXG4gICAgICAgICAgICAgICAgMTogW3tcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRUeXBlOiAnRm9ybUZpZWxkUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogJ2RhdGUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnZGF0ZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdkYXRlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IG51bGxcclxuICAgICAgICAgICAgICAgICAgICB9XX1cclxuICAgICAgICB9XVxyXG59O1xyXG5cclxuZXhwb3J0IGxldCBzdGFydEZvcm1OdW1iZXJXaWRnZXRNb2NrID0ge1xyXG4gICAgaWQ6IDQsXHJcbiAgICBuYW1lOiAnQ2xhaW0gUmV2aWV3IFByb2Nlc3MnLFxyXG4gICAgcHJvY2Vzc0RlZmluaXRpb25JZDogJ0NsYWltUmV2aWV3UHJvY2VzczoyOiA5MycsXHJcbiAgICBmaWVsZHM6IFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGZpZWxkVHlwZTogJ0NvbnRhaW5lclJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgaWQ6IDE0OTc5NTMyNTM3ODQsXHJcbiAgICAgICAgICAgIG5hbWU6ICdMYWJlbCcsXHJcbiAgICAgICAgICAgIHR5cGU6ICdjb250YWluZXInLFxyXG4gICAgICAgICAgICB2YWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICBmaWVsZHM6IHtcclxuICAgICAgICAgICAgICAgIDE6IFt7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkVHlwZTogJ0Zvcm1GaWVsZFJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdudW1iZXInLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnbnVtYmVyIHdpZGdldCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdpbnRlZ2VyJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IG51bGxcclxuICAgICAgICAgICAgICAgICAgICB9XX1cclxuICAgICAgICB9XVxyXG59O1xyXG5cclxuZXhwb3J0IGxldCBzdGFydEZvcm1BbW91bnRXaWRnZXRNb2NrID0ge1xyXG4gICAgaWQ6IDQsXHJcbiAgICBuYW1lOiAnQ2xhaW0gUmV2aWV3IFByb2Nlc3MnLFxyXG4gICAgcHJvY2Vzc0RlZmluaXRpb25JZDogJ0NsYWltUmV2aWV3UHJvY2VzczoyOiA5MycsXHJcbiAgICBmaWVsZHM6IFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGZpZWxkVHlwZTogJ0NvbnRhaW5lclJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgaWQ6IDE0OTc5NTMyNTM3ODQsXHJcbiAgICAgICAgICAgIG5hbWU6ICdMYWJlbCcsXHJcbiAgICAgICAgICAgIHR5cGU6ICdjb250YWluZXInLFxyXG4gICAgICAgICAgICB2YWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICBmaWVsZHM6IHtcclxuICAgICAgICAgICAgICAgIDE6IFt7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkVHlwZTogJ0Zvcm1GaWVsZFJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdhbW91bnQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnYW1vdW50IHdpZGdldCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdhbW91bnQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgIH1dfVxyXG4gICAgICAgIH1dXHJcbn07XHJcblxyXG5leHBvcnQgbGV0IHN0YXJ0Rm9ybVJhZGlvQnV0dG9uV2lkZ2V0TW9jayA9IHtcclxuICAgIGlkOiA0LFxyXG4gICAgbmFtZTogJ0NsYWltIFJldmlldyBQcm9jZXNzJyxcclxuICAgIHByb2Nlc3NEZWZpbml0aW9uSWQ6ICdDbGFpbVJldmlld1Byb2Nlc3M6MjogOTMnLFxyXG4gICAgZmllbGRzOiBbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBmaWVsZFR5cGU6ICdDb250YWluZXJSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgIGlkOiAxNDk3OTUzMjUzNzg0LFxyXG4gICAgICAgICAgICBuYW1lOiAnTGFiZWwnLFxyXG4gICAgICAgICAgICB0eXBlOiAnY29udGFpbmVyJyxcclxuICAgICAgICAgICAgdmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgZmllbGRzOiB7XHJcbiAgICAgICAgICAgICAgICAxOiBbe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWVsZFR5cGU6ICdSZXN0RmllbGRSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAncmFkaW8tYnV0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ3JhZGlvLWJ1dHRvbnMnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAncmFkaW8tYnV0dG9ucycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgfV19XHJcbiAgICAgICAgfV1cclxufTtcclxuXHJcbmV4cG9ydCBsZXQgc3RhcnRGb3JtVGV4dERlZmluaXRpb25Nb2NrID0ge1xyXG4gICAgaWQ6IDQsXHJcbiAgICBuYW1lOiAnQ2xhaW0gUmV2aWV3IFByb2Nlc3MnLFxyXG4gICAgcHJvY2Vzc0RlZmluaXRpb25JZDogJ0NsYWltUmV2aWV3UHJvY2VzczoyOiA5MycsXHJcbiAgICBmaWVsZHM6IFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGZpZWxkVHlwZTogJ0NvbnRhaW5lclJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgaWQ6IDE0OTc5NTMyNTM3ODQsXHJcbiAgICAgICAgICAgIG5hbWU6ICdMYWJlbCcsXHJcbiAgICAgICAgICAgIHR5cGU6ICdjb250YWluZXInLFxyXG4gICAgICAgICAgICB2YWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICBmaWVsZHM6IHtcclxuICAgICAgICAgICAgICAgIDE6IFt7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkVHlwZTogJ0Zvcm1GaWVsZFJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdtb2NrdGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdtb2NrVGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICd0ZXh0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IG51bGxcclxuICAgICAgICAgICAgICAgICAgICB9XX1cclxuICAgICAgICB9XVxyXG59O1xyXG5cclxuZXhwb3J0IGxldCBzdGFydEZvcm1Ecm9wZG93bkRlZmluaXRpb25Nb2NrID0ge1xyXG4gICAgaWQ6IDQsXHJcbiAgICBuYW1lOiAnQ2xhaW0gUmV2aWV3IFByb2Nlc3MnLFxyXG4gICAgcHJvY2Vzc0RlZmluaXRpb25JZDogJ0NsYWltUmV2aWV3UHJvY2VzczoyOiA5MycsXHJcbiAgICBmaWVsZHM6IFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGZpZWxkVHlwZTogJ0NvbnRhaW5lclJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgaWQ6IDE0OTc5NTMyNTM3ODQsXHJcbiAgICAgICAgICAgIG5hbWU6ICdMYWJlbCcsXHJcbiAgICAgICAgICAgIHR5cGU6ICdjb250YWluZXInLFxyXG4gICAgICAgICAgICB2YWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICBmaWVsZHM6IHtcclxuICAgICAgICAgICAgICAgIDE6IFt7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWVsZFR5cGU6ICdSZXN0RmllbGRSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnbW9ja1R5cGVEcm9wRG93bicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdtb2NrIERyb3BEb3duJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2Ryb3Bkb3duJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6ICdDaG9vc2VvbmUuLi4nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnM6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogJ2VtcHR5JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnQ2hvb3Nlb25lLi4uJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogJ29wdDEnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdPcHRpb24tMSdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdvcHQyJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnT3B0aW9uLTInXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnb3B0MycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ09wdGlvbi0zJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogJ29wdDInLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdPcHRpb24tMydcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICAgICAgICAgIH1dfVxyXG4gICAgICAgIH1dXHJcbn07XHJcblxyXG5leHBvcnQgbGV0IHN0YXJ0TW9ja0Zvcm0gPSB7XHJcbiAgICBpZDogNCxcclxuICAgIG5hbWU6ICdDbGFpbSBSZXZpZXcgUHJvY2VzcycsXHJcbiAgICBwcm9jZXNzRGVmaW5pdGlvbklkOiAnQ2xhaW1SZXZpZXdQcm9jZXNzOjI6IDkzJyxcclxuICAgIHByb2Nlc3NEZWZpbml0aW9uTmFtZTogJ0NsYWltUmV2aWV3UHJvY2VzcycsXHJcbiAgICBwcm9jZXNzRGVmaW5pdGlvbktleTogJ0NsYWltUmV2aWV3UHJvY2VzcycsXHJcbiAgICB0YWJzOiBbXSxcclxuICAgIGZpZWxkczogW1xyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgZmllbGRUeXBlOiAnQ29udGFpbmVyUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICBpZDogMTQ5Nzk1MzI1Mzc4NCxcclxuICAgICAgICAgICAgbmFtZTogJ0xhYmVsJyxcclxuICAgICAgICAgICAgdHlwZTogJ2NvbnRhaW5lcicsXHJcbiAgICAgICAgICAgIHZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBudWxsLFxyXG4gICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIG9wdGlvbnM6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RVcmw6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RSZXNwb25zZVBhdGg6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RJZFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICByZXN0TGFiZWxQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgdGFiOiBudWxsLFxyXG4gICAgICAgICAgICBjbGFzc05hbWU6IG51bGwsXHJcbiAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICBsYXlvdXQ6IG51bGwsXHJcbiAgICAgICAgICAgIHNpemVYOiAyLFxyXG4gICAgICAgICAgICBzaXplWTogMSxcclxuICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgY29sOiAtMSxcclxuICAgICAgICAgICAgdmlzaWJpbGl0eUNvbmRpdGlvbjogbnVsbCxcclxuICAgICAgICAgICAgbnVtYmVyT2ZDb2x1bW5zOiAyLFxyXG4gICAgICAgICAgICBmaWVsZHM6IHtcclxuICAgICAgICAgICAgICAgIDE6IFtcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkVHlwZTogJ0Zvcm1GaWVsZFJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdjbGllbnRuYW1lJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ0NsaWVudE5hbWUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvdmVycmlkZUlkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbkxlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4TGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5WYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlZ2V4UGF0dGVybjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uVHlwZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGFzRW1wdHlWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uczogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGlzdGluZ0NvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhDb2xzcGFuOiAyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYXlvdXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2x1bW46IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGxcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAgICAgMjogW1xyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRUeXBlOiAnRm9ybUZpZWxkUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogJ3BvbGljeW5vJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ1BvbGljeU5vJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2ludGVnZXInLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiAnRW50ZXJQb2xpY3lOYW1lJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWluTGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXhMZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pblZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXhWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVnZXhQYXR0ZXJuOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25UeXBlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBoYXNFbXB0eVZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0VXJsOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0UmVzcG9uc2VQYXRoOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0SWRQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdExhYmVsUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhYjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4aXN0aW5nQ29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heENvbHNwYW46IDFcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0ZURpc3BsYXlGb3JtYXQ6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxheW91dDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbHVtbjogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xzcGFuOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpemVYOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWTogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sOiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmlzaWJpbGl0eUNvbmRpdGlvbjogbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBmaWVsZFR5cGU6ICdDb250YWluZXJSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgIGlkOiAxNDk3OTUzMjcwMjY5LFxyXG4gICAgICAgICAgICBuYW1lOiAnTGFiZWwnLFxyXG4gICAgICAgICAgICB0eXBlOiAnY29udGFpbmVyJyxcclxuICAgICAgICAgICAgdmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICBvdmVycmlkZUlkOiBmYWxzZSxcclxuICAgICAgICAgICAgY29sc3BhbjogMSxcclxuICAgICAgICAgICAgcGxhY2Vob2xkZXI6IG51bGwsXHJcbiAgICAgICAgICAgIG1pbkxlbmd0aDogMCxcclxuICAgICAgICAgICAgbWF4TGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICBtaW5WYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgbWF4VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIHJlZ2V4UGF0dGVybjogbnVsbCxcclxuICAgICAgICAgICAgb3B0aW9uVHlwZTogbnVsbCxcclxuICAgICAgICAgICAgaGFzRW1wdHlWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgb3B0aW9uczogbnVsbCxcclxuICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICAgIGNsYXNzTmFtZTogbnVsbCxcclxuICAgICAgICAgICAgZGF0ZURpc3BsYXlGb3JtYXQ6IG51bGwsXHJcbiAgICAgICAgICAgIGxheW91dDogbnVsbCxcclxuICAgICAgICAgICAgc2l6ZVg6IDIsXHJcbiAgICAgICAgICAgIHNpemVZOiAxLFxyXG4gICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICBjb2w6IC0xLFxyXG4gICAgICAgICAgICB2aXNpYmlsaXR5Q29uZGl0aW9uOiBudWxsLFxyXG4gICAgICAgICAgICBudW1iZXJPZkNvbHVtbnM6IDIsXHJcbiAgICAgICAgICAgIGZpZWxkczoge1xyXG4gICAgICAgICAgICAgICAgMTogW1xyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRUeXBlOiAnRm9ybUZpZWxkUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogJ2JpbGxBbW91bnQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnQmlsbEFtb3VudCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdpbnRlZ2VyJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG92ZXJyaWRlSWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcjogJ0VudGVyQmlsbEFtb3VudCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbkxlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4TGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5WYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlZ2V4UGF0dGVybjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uVHlwZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGFzRW1wdHlWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uczogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGlzdGluZ0NvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhDb2xzcGFuOiAyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYXlvdXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2x1bW46IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGxcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAgICAgMjogW1xyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRUeXBlOiAnRm9ybUZpZWxkUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogJ2JpbGxkYXRlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ0JpbGxEYXRlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2RhdGUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG92ZXJyaWRlSWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWluTGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXhMZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pblZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtYXhWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVnZXhQYXR0ZXJuOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25UeXBlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBoYXNFbXB0eVZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvcHRpb25zOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0VXJsOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0UmVzcG9uc2VQYXRoOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0SWRQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdExhYmVsUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRhYjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lOiAnYmlsbGRhdGUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwYXJhbXM6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGV4aXN0aW5nQ29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIG1heENvbHNwYW46IDFcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGF0ZURpc3BsYXlGb3JtYXQ6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGxheW91dDoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbHVtbjogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2xzcGFuOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpemVYOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWTogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sOiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmlzaWJpbGl0eUNvbmRpdGlvbjogbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIF1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBmaWVsZFR5cGU6ICdDb250YWluZXJSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgIGlkOiAxNDk3OTUzMjgwOTMwLFxyXG4gICAgICAgICAgICBuYW1lOiAnTGFiZWwnLFxyXG4gICAgICAgICAgICB0eXBlOiAnY29udGFpbmVyJyxcclxuICAgICAgICAgICAgdmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICBvdmVycmlkZUlkOiBmYWxzZSxcclxuICAgICAgICAgICAgY29sc3BhbjogMSxcclxuICAgICAgICAgICAgcGxhY2Vob2xkZXI6IG51bGwsXHJcbiAgICAgICAgICAgIG1pbkxlbmd0aDogMCxcclxuICAgICAgICAgICAgbWF4TGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICBtaW5WYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgbWF4VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIHJlZ2V4UGF0dGVybjogbnVsbCxcclxuICAgICAgICAgICAgb3B0aW9uVHlwZTogbnVsbCxcclxuICAgICAgICAgICAgaGFzRW1wdHlWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgb3B0aW9uczogbnVsbCxcclxuICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICAgIGNsYXNzTmFtZTogbnVsbCxcclxuICAgICAgICAgICAgZGF0ZURpc3BsYXlGb3JtYXQ6IG51bGwsXHJcbiAgICAgICAgICAgIGxheW91dDogbnVsbCxcclxuICAgICAgICAgICAgc2l6ZVg6IDIsXHJcbiAgICAgICAgICAgIHNpemVZOiAxLFxyXG4gICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICBjb2w6IC0xLFxyXG4gICAgICAgICAgICB2aXNpYmlsaXR5Q29uZGl0aW9uOiBudWxsLFxyXG4gICAgICAgICAgICBudW1iZXJPZkNvbHVtbnM6IDIsXHJcbiAgICAgICAgICAgIGZpZWxkczoge1xyXG4gICAgICAgICAgICAgICAgMTogW1xyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRUeXBlOiAnUmVzdEZpZWxkUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogJ2NsYWltdHlwZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdDbGFpbVR5cGUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnZHJvcGRvd24nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogJ0Nob29zZW9uZS4uLicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvdmVycmlkZUlkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbkxlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4TGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5WYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlZ2V4UGF0dGVybjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uVHlwZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGFzRW1wdHlWYWx1ZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uczogW1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnZW1wdHknLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdDaG9vc2VvbmUuLi4nXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnY2FzaGxlc3MnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdDYXNobGVzcydcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdyZWltYnVyc2VtZW50JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnUmVpbWJ1cnNlbWVudCdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGlzdGluZ0NvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhDb2xzcGFuOiAyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYXlvdXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2x1bW46IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVuZHBvaW50OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1ZXN0SGVhZGVyczogbnVsbFxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICAyOiBbXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWVsZFR5cGU6ICdGb3JtRmllbGRSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnaG9zcGl0YWxOYW1lJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ0hvc3BpdGFsTmFtZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICd0ZXh0JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvdmVycmlkZUlkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6ICdFbnRlckhvc3BpdGFsTmFtZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbkxlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4TGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5WYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlZ2V4UGF0dGVybjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uVHlwZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGFzRW1wdHlWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uczogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGlzdGluZ0NvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhDb2xzcGFuOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYXlvdXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2x1bW46IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGxcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICBdLFxyXG4gICAgb3V0Y29tZXM6IFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGlkOiAnYXBwcm92ZScsXHJcbiAgICAgICAgICAgIG5hbWU6ICdBcHByb3ZlJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBpZDogJ2NvbXBsZXRlJyxcclxuICAgICAgICAgICAgbmFtZTogJ0NvbXBsZXRlJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBpZDogJ3N0YXJ0X3Byb2Nlc3MnLFxyXG4gICAgICAgICAgICBuYW1lOiAnU3RhcnQgUHJvY2VzcydcclxuICAgICAgICB9XHJcbiAgICBdLFxyXG4gICAgamF2YXNjcmlwdEV2ZW50czogW10sXHJcbiAgICBjbGFzc05hbWU6ICcnLFxyXG4gICAgc3R5bGU6ICcnLFxyXG4gICAgbWV0YWRhdGE6IHt9LFxyXG4gICAgdmFyaWFibGVzOiBbXSxcclxuICAgIGN1c3RvbUZpZWxkc1ZhbHVlSW5mbzoge30sXHJcbiAgICBncmlkc3RlckZvcm06IGZhbHNlLFxyXG4gICAgZ2xvYmFsRGF0ZUZvcm1hdDogJ0QgLSBNIC0gWVlZWSdcclxufTtcclxuXHJcbmV4cG9ydCBsZXQgc3RhcnRNb2NrRm9ybVdpdGhUYWIgPSB7XHJcbiAgICBpZDogNCxcclxuICAgIHRhc2tOYW1lOiAnTW9jayBUaXRsZScsXHJcbiAgICBwcm9jZXNzRGVmaW5pdGlvbklkOiAnQ2xhaW1SZXZpZXdQcm9jZXNzOjI6IDkzJyxcclxuICAgIHByb2Nlc3NEZWZpbml0aW9uTmFtZTogJ0NsYWltUmV2aWV3UHJvY2VzcycsXHJcbiAgICBwcm9jZXNzRGVmaW5pdGlvbktleTogJ0NsYWltUmV2aWV3UHJvY2VzcycsXHJcbiAgICB0YWJzOiBbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBpZDogJ2Zvcm0xJyxcclxuICAgICAgICAgICAgbmFtZTogJ1RhYiAxJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBpZDogJ2Zvcm0yJyxcclxuICAgICAgICAgICAgbmFtZTogJ1RhYiAyJ1xyXG4gICAgICAgIH1cclxuICAgIF0sXHJcbiAgICBmaWVsZHM6IFtcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIGZpZWxkVHlwZTogJ0NvbnRhaW5lclJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgaWQ6IDE0OTc5NTMyNTM3ODQsXHJcbiAgICAgICAgICAgIG5hbWU6ICdMYWJlbCcsXHJcbiAgICAgICAgICAgIHR5cGU6ICdjb250YWluZXInLFxyXG4gICAgICAgICAgICB2YWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgcmVxdWlyZWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgICAgIG92ZXJyaWRlSWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICBjb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICBwbGFjZWhvbGRlcjogbnVsbCxcclxuICAgICAgICAgICAgbWluTGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICBtYXhMZW5ndGg6IDAsXHJcbiAgICAgICAgICAgIG1pblZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICBtYXhWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgcmVnZXhQYXR0ZXJuOiBudWxsLFxyXG4gICAgICAgICAgICBvcHRpb25UeXBlOiBudWxsLFxyXG4gICAgICAgICAgICBoYXNFbXB0eVZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICBvcHRpb25zOiBudWxsLFxyXG4gICAgICAgICAgICByZXN0VXJsOiBudWxsLFxyXG4gICAgICAgICAgICByZXN0UmVzcG9uc2VQYXRoOiBudWxsLFxyXG4gICAgICAgICAgICByZXN0SWRQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgcmVzdExhYmVsUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgIHRhYjogbnVsbCxcclxuICAgICAgICAgICAgY2xhc3NOYW1lOiBudWxsLFxyXG4gICAgICAgICAgICBkYXRlRGlzcGxheUZvcm1hdDogbnVsbCxcclxuICAgICAgICAgICAgbGF5b3V0OiBudWxsLFxyXG4gICAgICAgICAgICBzaXplWDogMixcclxuICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGwsXHJcbiAgICAgICAgICAgIG51bWJlck9mQ29sdW1uczogMixcclxuICAgICAgICAgICAgZmllbGRzOiB7XHJcbiAgICAgICAgICAgICAgICAxOiBbXHJcbiAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBmaWVsZFR5cGU6ICdGb3JtRmllbGRSZXByZXNlbnRhdGlvbicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAnY2xpZW50bmFtZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdDbGllbnROYW1lJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3RleHQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWlyZWQ6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnM6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RVcmw6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RSZXNwb25zZVBhdGg6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RJZFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0TGFiZWxQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGFiOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmFtczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhpc3RpbmdDb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF4Q29sc3BhbjogMlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRlRGlzcGxheUZvcm1hdDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGF5b3V0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sdW1uOiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDFcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVg6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpemVZOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2w6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2aXNpYmlsaXR5Q29uZGl0aW9uOiBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgIDI6IFtcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkVHlwZTogJ0Zvcm1GaWVsZFJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdwb2xpY3lubycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdQb2xpY3lObycsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdpbnRlZ2VyJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWFkT25seTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG92ZXJyaWRlSWQ6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcjogJ0VudGVyUG9saWN5TmFtZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbkxlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4TGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5WYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlZ2V4UGF0dGVybjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uVHlwZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGFzRW1wdHlWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uczogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGlzdGluZ0NvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhDb2xzcGFuOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYXlvdXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2x1bW46IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGxcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgZmllbGRUeXBlOiAnQ29udGFpbmVyUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICBpZDogMTQ5Nzk1MzI3MDI2OSxcclxuICAgICAgICAgICAgbmFtZTogJ0xhYmVsJyxcclxuICAgICAgICAgICAgdHlwZTogJ2NvbnRhaW5lcicsXHJcbiAgICAgICAgICAgIHZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBudWxsLFxyXG4gICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIG9wdGlvbnM6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RVcmw6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RSZXNwb25zZVBhdGg6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RJZFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICByZXN0TGFiZWxQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgdGFiOiBudWxsLFxyXG4gICAgICAgICAgICBjbGFzc05hbWU6IG51bGwsXHJcbiAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICBsYXlvdXQ6IG51bGwsXHJcbiAgICAgICAgICAgIHNpemVYOiAyLFxyXG4gICAgICAgICAgICBzaXplWTogMSxcclxuICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgY29sOiAtMSxcclxuICAgICAgICAgICAgdmlzaWJpbGl0eUNvbmRpdGlvbjogbnVsbCxcclxuICAgICAgICAgICAgbnVtYmVyT2ZDb2x1bW5zOiAyLFxyXG4gICAgICAgICAgICBmaWVsZHM6IHtcclxuICAgICAgICAgICAgICAgIDE6IFtcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkVHlwZTogJ0Zvcm1GaWVsZFJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdiaWxsQW1vdW50JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ0JpbGxBbW91bnQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnaW50ZWdlcicsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvdmVycmlkZUlkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6ICdFbnRlckJpbGxBbW91bnQnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnM6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RVcmw6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RSZXNwb25zZVBhdGg6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RJZFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0TGFiZWxQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGFiOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmFtczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhpc3RpbmdDb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF4Q29sc3BhbjogMlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRlRGlzcGxheUZvcm1hdDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGF5b3V0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sdW1uOiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDFcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVg6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpemVZOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2w6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2aXNpYmlsaXR5Q29uZGl0aW9uOiBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgXSxcclxuICAgICAgICAgICAgICAgIDI6IFtcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkVHlwZTogJ0Zvcm1GaWVsZFJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdiaWxsZGF0ZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdCaWxsRGF0ZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdkYXRlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlcXVpcmVkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVhZE9ubHk6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBvdmVycmlkZUlkOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1pbkxlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4TGVuZ3RoOiAwLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5WYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWF4VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlZ2V4UGF0dGVybjogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uVHlwZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaGFzRW1wdHlWYWx1ZTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3B0aW9uczogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFVybDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdFJlc3BvbnNlUGF0aDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVzdElkUHJvcGVydHk6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RMYWJlbFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0YWI6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZTogJ2JpbGxkYXRlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcGFyYW1zOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBleGlzdGluZ0NvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhDb2xzcGFuOiAxXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsYXlvdXQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBjb2x1bW46IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sc3BhbjogMVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzaXplWDogMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVk6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJvdzogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbDogLTEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZpc2liaWxpdHlDb25kaXRpb246IG51bGxcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBdXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgZmllbGRUeXBlOiAnQ29udGFpbmVyUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICBpZDogMTQ5Nzk1MzI4MDkzMCxcclxuICAgICAgICAgICAgbmFtZTogJ0xhYmVsJyxcclxuICAgICAgICAgICAgdHlwZTogJ2NvbnRhaW5lcicsXHJcbiAgICAgICAgICAgIHZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBudWxsLFxyXG4gICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgIG9wdGlvbnM6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RVcmw6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RSZXNwb25zZVBhdGg6IG51bGwsXHJcbiAgICAgICAgICAgIHJlc3RJZFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICByZXN0TGFiZWxQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgdGFiOiBudWxsLFxyXG4gICAgICAgICAgICBjbGFzc05hbWU6IG51bGwsXHJcbiAgICAgICAgICAgIGRhdGVEaXNwbGF5Rm9ybWF0OiBudWxsLFxyXG4gICAgICAgICAgICBsYXlvdXQ6IG51bGwsXHJcbiAgICAgICAgICAgIHNpemVYOiAyLFxyXG4gICAgICAgICAgICBzaXplWTogMSxcclxuICAgICAgICAgICAgcm93OiAtMSxcclxuICAgICAgICAgICAgY29sOiAtMSxcclxuICAgICAgICAgICAgdmlzaWJpbGl0eUNvbmRpdGlvbjogbnVsbCxcclxuICAgICAgICAgICAgbnVtYmVyT2ZDb2x1bW5zOiAyLFxyXG4gICAgICAgICAgICBmaWVsZHM6IHtcclxuICAgICAgICAgICAgICAgIDE6IFtcclxuICAgICAgICAgICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGZpZWxkVHlwZTogJ1Jlc3RGaWVsZFJlcHJlc2VudGF0aW9uJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWQ6ICdjbGFpbXR5cGUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnQ2xhaW1UeXBlJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2Ryb3Bkb3duJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWU6ICdDaG9vc2VvbmUuLi4nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IHRydWUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnM6IFtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogJ2VtcHR5JyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnQ2hvb3Nlb25lLi4uJ1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpZDogJ2Nhc2hsZXNzJyxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBuYW1lOiAnQ2FzaGxlc3MnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiAncmVpbWJ1cnNlbWVudCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgbmFtZTogJ1JlaW1idXJzZW1lbnQnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIF0sXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RVcmw6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RSZXNwb25zZVBhdGg6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RJZFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0TGFiZWxQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGFiOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmFtczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhpc3RpbmdDb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF4Q29sc3BhbjogMlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRlRGlzcGxheUZvcm1hdDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGF5b3V0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sdW1uOiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDFcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVg6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpemVZOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2w6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2aXNpYmlsaXR5Q29uZGl0aW9uOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBlbmRwb2ludDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmVxdWVzdEhlYWRlcnM6IG51bGxcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBdLFxyXG4gICAgICAgICAgICAgICAgMjogW1xyXG4gICAgICAgICAgICAgICAgICAgIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZmllbGRUeXBlOiAnRm9ybUZpZWxkUmVwcmVzZW50YXRpb24nLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZDogJ2hvc3BpdGFsTmFtZScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG5hbWU6ICdIb3NwaXRhbE5hbWUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAndGV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXF1aXJlZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlYWRPbmx5OiBmYWxzZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgb3ZlcnJpZGVJZDogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyOiAnRW50ZXJIb3NwaXRhbE5hbWUnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBtaW5MZW5ndGg6IDAsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heExlbmd0aDogMCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWluVmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG1heFZhbHVlOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZWdleFBhdHRlcm46IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvblR5cGU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc0VtcHR5VmFsdWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wdGlvbnM6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RVcmw6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RSZXNwb25zZVBhdGg6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJlc3RJZFByb3BlcnR5OiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByZXN0TGFiZWxQcm9wZXJ0eTogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGFiOiBudWxsLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU6IG51bGwsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHBhcmFtczoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZXhpc3RpbmdDb2xzcGFuOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWF4Q29sc3BhbjogMVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBkYXRlRGlzcGxheUZvcm1hdDogbnVsbCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGF5b3V0OiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY29sdW1uOiAtMSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNvbHNwYW46IDFcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgc2l6ZVg6IDEsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNpemVZOiAxLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICByb3c6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb2w6IC0xLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2aXNpYmlsaXR5Q29uZGl0aW9uOiBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgXVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgXSxcclxuICAgIG91dGNvbWVzOiBbXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICBpZDogJ2FwcHJvdmUnLFxyXG4gICAgICAgICAgICBuYW1lOiAnQXBwcm92ZSdcclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgaWQ6ICdjb21wbGV0ZScsXHJcbiAgICAgICAgICAgIG5hbWU6ICdDb21wbGV0ZSdcclxuICAgICAgICB9XHJcbiAgICBdLFxyXG4gICAgamF2YXNjcmlwdEV2ZW50czogW10sXHJcbiAgICBjbGFzc05hbWU6ICcnLFxyXG4gICAgc3R5bGU6ICcnLFxyXG4gICAgbWV0YWRhdGE6IHt9LFxyXG4gICAgdmFyaWFibGVzOiBbXSxcclxuICAgIGN1c3RvbUZpZWxkc1ZhbHVlSW5mbzoge30sXHJcbiAgICBncmlkc3RlckZvcm06IGZhbHNlLFxyXG4gICAgZ2xvYmFsRGF0ZUZvcm1hdDogJ0QgLSBNIC0gWVlZWSdcclxufTtcclxuIl19