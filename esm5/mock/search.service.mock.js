/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/** @type {?} */
export var fakeSearch = {
    list: {
        pagination: {
            count: 1,
            hasMoreItems: false,
            totalItems: 1,
            skipCount: 0,
            maxItems: 100
        },
        entries: [
            {
                entry: {
                    id: '123',
                    name: 'MyDoc',
                    content: {
                        mimetype: 'text/plain'
                    },
                    createdByUser: {
                        displayName: 'John Doe'
                    },
                    modifiedByUser: {
                        displayName: 'John Doe'
                    }
                }
            }
        ]
    }
};
/** @type {?} */
export var mockError = {
    error: {
        errorKey: 'Search failed',
        statusCode: 400,
        briefSummary: '08220082 search failed',
        stackTrace: 'For security reasons the stack trace is no longer displayed, but the property is kept for previous versions.',
        descriptionURL: 'https://api-explorer.alfresco.com'
    }
};
var ɵ0 = /**
 * @param {?} term
 * @param {?} opts
 * @return {?}
 */
function (term, opts) { return Promise.resolve(fakeSearch); };
/** @type {?} */
export var searchMockApi = {
    core: {
        queriesApi: {
            findNodes: (ɵ0)
        }
    }
};
export { ɵ0 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnNlcnZpY2UubW9jay5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vY2svc2VhcmNoLnNlcnZpY2UubW9jay50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsTUFBTSxLQUFLLFVBQVUsR0FBRztJQUNwQixJQUFJLEVBQUU7UUFDRixVQUFVLEVBQUU7WUFDUixLQUFLLEVBQUUsQ0FBQztZQUNSLFlBQVksRUFBRSxLQUFLO1lBQ25CLFVBQVUsRUFBRSxDQUFDO1lBQ2IsU0FBUyxFQUFFLENBQUM7WUFDWixRQUFRLEVBQUUsR0FBRztTQUNoQjtRQUNELE9BQU8sRUFBRTtZQUNMO2dCQUNJLEtBQUssRUFBRTtvQkFDSCxFQUFFLEVBQUUsS0FBSztvQkFDVCxJQUFJLEVBQUUsT0FBTztvQkFDYixPQUFPLEVBQUU7d0JBQ0wsUUFBUSxFQUFFLFlBQVk7cUJBQ3pCO29CQUNELGFBQWEsRUFBRTt3QkFDWCxXQUFXLEVBQUUsVUFBVTtxQkFDMUI7b0JBQ0QsY0FBYyxFQUFFO3dCQUNaLFdBQVcsRUFBRSxVQUFVO3FCQUMxQjtpQkFDSjthQUNKO1NBQ0o7S0FDSjtDQUNKOztBQUVELE1BQU0sS0FBSyxTQUFTLEdBQUc7SUFDbkIsS0FBSyxFQUFFO1FBQ0gsUUFBUSxFQUFFLGVBQWU7UUFDekIsVUFBVSxFQUFFLEdBQUc7UUFDZixZQUFZLEVBQUUsd0JBQXdCO1FBQ3RDLFVBQVUsRUFBRSw4R0FBOEc7UUFDMUgsY0FBYyxFQUFFLG1DQUFtQztLQUN0RDtDQUNKOzs7Ozs7QUFLc0IsVUFBQyxJQUFJLEVBQUUsSUFBSSxJQUFLLE9BQUEsT0FBTyxDQUFDLE9BQU8sQ0FBQyxVQUFVLENBQUMsRUFBM0IsQ0FBMkI7O0FBSGxFLE1BQU0sS0FBSyxhQUFhLEdBQUc7SUFDdkIsSUFBSSxFQUFFO1FBQ0YsVUFBVSxFQUFFO1lBQ1IsU0FBUyxNQUE2QztTQUN6RDtLQUNKO0NBQ0oiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuZXhwb3J0IGxldCBmYWtlU2VhcmNoID0ge1xyXG4gICAgbGlzdDoge1xyXG4gICAgICAgIHBhZ2luYXRpb246IHtcclxuICAgICAgICAgICAgY291bnQ6IDEsXHJcbiAgICAgICAgICAgIGhhc01vcmVJdGVtczogZmFsc2UsXHJcbiAgICAgICAgICAgIHRvdGFsSXRlbXM6IDEsXHJcbiAgICAgICAgICAgIHNraXBDb3VudDogMCxcclxuICAgICAgICAgICAgbWF4SXRlbXM6IDEwMFxyXG4gICAgICAgIH0sXHJcbiAgICAgICAgZW50cmllczogW1xyXG4gICAgICAgICAgICB7XHJcbiAgICAgICAgICAgICAgICBlbnRyeToge1xyXG4gICAgICAgICAgICAgICAgICAgIGlkOiAnMTIzJyxcclxuICAgICAgICAgICAgICAgICAgICBuYW1lOiAnTXlEb2MnLFxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnQ6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWltZXR5cGU6ICd0ZXh0L3BsYWluJ1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgY3JlYXRlZEJ5VXNlcjoge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkaXNwbGF5TmFtZTogJ0pvaG4gRG9lJ1xyXG4gICAgICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAgICAgbW9kaWZpZWRCeVVzZXI6IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZGlzcGxheU5hbWU6ICdKb2huIERvZSdcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICBdXHJcbiAgICB9XHJcbn07XHJcblxyXG5leHBvcnQgbGV0IG1vY2tFcnJvciA9IHtcclxuICAgIGVycm9yOiB7XHJcbiAgICAgICAgZXJyb3JLZXk6ICdTZWFyY2ggZmFpbGVkJyxcclxuICAgICAgICBzdGF0dXNDb2RlOiA0MDAsXHJcbiAgICAgICAgYnJpZWZTdW1tYXJ5OiAnMDgyMjAwODIgc2VhcmNoIGZhaWxlZCcsXHJcbiAgICAgICAgc3RhY2tUcmFjZTogJ0ZvciBzZWN1cml0eSByZWFzb25zIHRoZSBzdGFjayB0cmFjZSBpcyBubyBsb25nZXIgZGlzcGxheWVkLCBidXQgdGhlIHByb3BlcnR5IGlzIGtlcHQgZm9yIHByZXZpb3VzIHZlcnNpb25zLicsXHJcbiAgICAgICAgZGVzY3JpcHRpb25VUkw6ICdodHRwczovL2FwaS1leHBsb3Jlci5hbGZyZXNjby5jb20nXHJcbiAgICB9XHJcbn07XHJcblxyXG5leHBvcnQgbGV0IHNlYXJjaE1vY2tBcGkgPSB7XHJcbiAgICBjb3JlOiB7XHJcbiAgICAgICAgcXVlcmllc0FwaToge1xyXG4gICAgICAgICAgICBmaW5kTm9kZXM6ICh0ZXJtLCBvcHRzKSA9PiBQcm9taXNlLnJlc29sdmUoZmFrZVNlYXJjaClcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn07XHJcbiJdfQ==