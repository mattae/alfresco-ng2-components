/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { EventEmitter } from '@angular/core';
import { of } from 'rxjs';
/**
 * @record
 */
export function LangChangeEvent() { }
if (false) {
    /** @type {?} */
    LangChangeEvent.prototype.lang;
    /** @type {?} */
    LangChangeEvent.prototype.translations;
}
var TranslationMock = /** @class */ (function () {
    function TranslationMock() {
        this.defaultLang = 'en';
        this.translate = {
            onLangChange: new EventEmitter()
        };
    }
    /**
     * @return {?}
     */
    TranslationMock.prototype.addTranslationFolder = /**
     * @return {?}
     */
    function () { };
    /**
     * @return {?}
     */
    TranslationMock.prototype.onTranslationChanged = /**
     * @return {?}
     */
    function () { };
    /**
     * @return {?}
     */
    TranslationMock.prototype.use = /**
     * @return {?}
     */
    function () { };
    /**
     * @return {?}
     */
    TranslationMock.prototype.loadTranslation = /**
     * @return {?}
     */
    function () { };
    /**
     * @param {?} key
     * @param {?=} interpolateParams
     * @return {?}
     */
    TranslationMock.prototype.get = /**
     * @param {?} key
     * @param {?=} interpolateParams
     * @return {?}
     */
    function (key, interpolateParams) {
        return of(key);
    };
    /**
     * @param {?} key
     * @param {?=} interpolateParams
     * @return {?}
     */
    TranslationMock.prototype.instant = /**
     * @param {?} key
     * @param {?=} interpolateParams
     * @return {?}
     */
    function (key, interpolateParams) {
        return key;
    };
    return TranslationMock;
}());
export { TranslationMock };
if (false) {
    /** @type {?} */
    TranslationMock.prototype.defaultLang;
    /** @type {?} */
    TranslationMock.prototype.userLang;
    /** @type {?} */
    TranslationMock.prototype.customLoader;
    /** @type {?} */
    TranslationMock.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhbnNsYXRpb24uc2VydmljZS5tb2NrLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsibW9jay90cmFuc2xhdGlvbi5zZXJ2aWNlLm1vY2sudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM3QyxPQUFPLEVBQWMsRUFBRSxFQUFFLE1BQU0sTUFBTSxDQUFDOzs7O0FBR3RDLHFDQUdDOzs7SUFGRywrQkFBYTs7SUFDYix1Q0FBa0I7O0FBR3RCO0lBQUE7UUFFSSxnQkFBVyxHQUFXLElBQUksQ0FBQztRQUkzQixjQUFTLEdBQVE7WUFDYixZQUFZLEVBQUUsSUFBSSxZQUFZLEVBQW1CO1NBQ3BELENBQUM7SUFrQk4sQ0FBQzs7OztJQWhCRyw4Q0FBb0I7OztJQUFwQixjQUF3QixDQUFDOzs7O0lBRXpCLDhDQUFvQjs7O0lBQXBCLGNBQXdCLENBQUM7Ozs7SUFFekIsNkJBQUc7OztJQUFILGNBQVksQ0FBQzs7OztJQUViLHlDQUFlOzs7SUFBZixjQUFtQixDQUFDOzs7Ozs7SUFFcEIsNkJBQUc7Ozs7O0lBQUgsVUFBSSxHQUEyQixFQUFFLGlCQUEwQjtRQUN2RCxPQUFPLEVBQUUsQ0FBQyxHQUFHLENBQUMsQ0FBQztJQUNuQixDQUFDOzs7Ozs7SUFFRCxpQ0FBTzs7Ozs7SUFBUCxVQUFRLEdBQTJCLEVBQUUsaUJBQTBCO1FBQzNELE9BQU8sR0FBRyxDQUFDO0lBQ2YsQ0FBQztJQUVMLHNCQUFDO0FBQUQsQ0FBQyxBQTFCRCxJQTBCQzs7OztJQXhCRyxzQ0FBMkI7O0lBQzNCLG1DQUFpQjs7SUFDakIsdUNBQWtCOztJQUVsQixvQ0FFRSIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBFdmVudEVtaXR0ZXIgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgb2YgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgVHJhbnNsYXRpb25TZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvdHJhbnNsYXRpb24uc2VydmljZSc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIExhbmdDaGFuZ2VFdmVudCB7XHJcbiAgICBsYW5nOiBzdHJpbmc7XHJcbiAgICB0cmFuc2xhdGlvbnM6IGFueTtcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIFRyYW5zbGF0aW9uTW9jayBpbXBsZW1lbnRzIFRyYW5zbGF0aW9uU2VydmljZSB7XHJcblxyXG4gICAgZGVmYXVsdExhbmc6IHN0cmluZyA9ICdlbic7XHJcbiAgICB1c2VyTGFuZzogc3RyaW5nO1xyXG4gICAgY3VzdG9tTG9hZGVyOiBhbnk7XHJcblxyXG4gICAgdHJhbnNsYXRlOiBhbnkgPSB7XHJcbiAgICAgICAgb25MYW5nQ2hhbmdlOiBuZXcgRXZlbnRFbWl0dGVyPExhbmdDaGFuZ2VFdmVudD4oKVxyXG4gICAgfTtcclxuXHJcbiAgICBhZGRUcmFuc2xhdGlvbkZvbGRlcigpIHt9XHJcblxyXG4gICAgb25UcmFuc2xhdGlvbkNoYW5nZWQoKSB7fVxyXG5cclxuICAgIHVzZSgpOiBhbnkge31cclxuXHJcbiAgICBsb2FkVHJhbnNsYXRpb24oKSB7fVxyXG5cclxuICAgIGdldChrZXk6IHN0cmluZyB8IEFycmF5PHN0cmluZz4sIGludGVycG9sYXRlUGFyYW1zPzogT2JqZWN0KTogT2JzZXJ2YWJsZTxzdHJpbmcgfCBhbnk+IHtcclxuICAgICAgICByZXR1cm4gb2Yoa2V5KTtcclxuICAgIH1cclxuXHJcbiAgICBpbnN0YW50KGtleTogc3RyaW5nIHwgQXJyYXk8c3RyaW5nPiwgaW50ZXJwb2xhdGVQYXJhbXM/OiBPYmplY3QpOiBzdHJpbmcgfCBhbnkge1xyXG4gICAgICAgIHJldHVybiBrZXk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==