/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* spellchecker: disable */
var AllowableOperationsEnum = /** @class */ (function (_super) {
    tslib_1.__extends(AllowableOperationsEnum, _super);
    function AllowableOperationsEnum() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    AllowableOperationsEnum.DELETE = 'delete';
    AllowableOperationsEnum.UPDATE = 'update';
    AllowableOperationsEnum.CREATE = 'create';
    AllowableOperationsEnum.COPY = 'copy';
    AllowableOperationsEnum.LOCK = 'lock';
    AllowableOperationsEnum.UPDATEPERMISSIONS = 'updatePermissions';
    AllowableOperationsEnum.NOT_DELETE = '!delete';
    AllowableOperationsEnum.NOT_UPDATE = '!update';
    AllowableOperationsEnum.NOT_CREATE = '!create';
    AllowableOperationsEnum.NOT_UPDATEPERMISSIONS = '!updatePermissions';
    return AllowableOperationsEnum;
}(String));
export { AllowableOperationsEnum };
if (false) {
    /** @type {?} */
    AllowableOperationsEnum.DELETE;
    /** @type {?} */
    AllowableOperationsEnum.UPDATE;
    /** @type {?} */
    AllowableOperationsEnum.CREATE;
    /** @type {?} */
    AllowableOperationsEnum.COPY;
    /** @type {?} */
    AllowableOperationsEnum.LOCK;
    /** @type {?} */
    AllowableOperationsEnum.UPDATEPERMISSIONS;
    /** @type {?} */
    AllowableOperationsEnum.NOT_DELETE;
    /** @type {?} */
    AllowableOperationsEnum.NOT_UPDATE;
    /** @type {?} */
    AllowableOperationsEnum.NOT_CREATE;
    /** @type {?} */
    AllowableOperationsEnum.NOT_UPDATEPERMISSIONS;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYWxsb3dhYmxlLW9wZXJhdGlvbnMuZW51bS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vZGVscy9hbGxvd2FibGUtb3BlcmF0aW9ucy5lbnVtLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkE7SUFBNkMsbURBQU07SUFBbkQ7O0lBV0EsQ0FBQztJQVZVLDhCQUFNLEdBQVcsUUFBUSxDQUFDO0lBQzFCLDhCQUFNLEdBQVcsUUFBUSxDQUFDO0lBQzFCLDhCQUFNLEdBQVcsUUFBUSxDQUFDO0lBQzFCLDRCQUFJLEdBQVcsTUFBTSxDQUFDO0lBQ3RCLDRCQUFJLEdBQVcsTUFBTSxDQUFDO0lBQ3RCLHlDQUFpQixHQUFXLG1CQUFtQixDQUFDO0lBQ2hELGtDQUFVLEdBQVcsU0FBUyxDQUFDO0lBQy9CLGtDQUFVLEdBQVcsU0FBUyxDQUFDO0lBQy9CLGtDQUFVLEdBQVcsU0FBUyxDQUFDO0lBQy9CLDZDQUFxQixHQUFXLG9CQUFvQixDQUFDO0lBQ2hFLDhCQUFDO0NBQUEsQUFYRCxDQUE2QyxNQUFNLEdBV2xEO1NBWFksdUJBQXVCOzs7SUFDaEMsK0JBQWlDOztJQUNqQywrQkFBaUM7O0lBQ2pDLCtCQUFpQzs7SUFDakMsNkJBQTZCOztJQUM3Qiw2QkFBNkI7O0lBQzdCLDBDQUF1RDs7SUFDdkQsbUNBQXNDOztJQUN0QyxtQ0FBc0M7O0lBQ3RDLG1DQUFzQzs7SUFDdEMsOENBQTREIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbiAvKiBzcGVsbGNoZWNrZXI6IGRpc2FibGUgKi9cclxuZXhwb3J0IGNsYXNzIEFsbG93YWJsZU9wZXJhdGlvbnNFbnVtIGV4dGVuZHMgU3RyaW5nIHtcclxuICAgIHN0YXRpYyBERUxFVEU6IHN0cmluZyA9ICdkZWxldGUnO1xyXG4gICAgc3RhdGljIFVQREFURTogc3RyaW5nID0gJ3VwZGF0ZSc7XHJcbiAgICBzdGF0aWMgQ1JFQVRFOiBzdHJpbmcgPSAnY3JlYXRlJztcclxuICAgIHN0YXRpYyBDT1BZOiBzdHJpbmcgPSAnY29weSc7XHJcbiAgICBzdGF0aWMgTE9DSzogc3RyaW5nID0gJ2xvY2snO1xyXG4gICAgc3RhdGljIFVQREFURVBFUk1JU1NJT05TOiBzdHJpbmcgPSAndXBkYXRlUGVybWlzc2lvbnMnO1xyXG4gICAgc3RhdGljIE5PVF9ERUxFVEU6IHN0cmluZyA9ICchZGVsZXRlJztcclxuICAgIHN0YXRpYyBOT1RfVVBEQVRFOiBzdHJpbmcgPSAnIXVwZGF0ZSc7XHJcbiAgICBzdGF0aWMgTk9UX0NSRUFURTogc3RyaW5nID0gJyFjcmVhdGUnO1xyXG4gICAgc3RhdGljIE5PVF9VUERBVEVQRVJNSVNTSU9OUzogc3RyaW5nID0gJyF1cGRhdGVQZXJtaXNzaW9ucyc7XHJcbn1cclxuIl19