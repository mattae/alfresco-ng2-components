/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var EcmCompanyModel = /** @class */ (function () {
    function EcmCompanyModel() {
    }
    return EcmCompanyModel;
}());
export { EcmCompanyModel };
if (false) {
    /** @type {?} */
    EcmCompanyModel.prototype.organization;
    /** @type {?} */
    EcmCompanyModel.prototype.address1;
    /** @type {?} */
    EcmCompanyModel.prototype.address2;
    /** @type {?} */
    EcmCompanyModel.prototype.address3;
    /** @type {?} */
    EcmCompanyModel.prototype.postcode;
    /** @type {?} */
    EcmCompanyModel.prototype.telephone;
    /** @type {?} */
    EcmCompanyModel.prototype.fax;
    /** @type {?} */
    EcmCompanyModel.prototype.email;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWNtLWNvbXBhbnkubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJtb2RlbHMvZWNtLWNvbXBhbnkubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkE7SUFBQTtJQVNBLENBQUM7SUFBRCxzQkFBQztBQUFELENBQUMsQUFURCxJQVNDOzs7O0lBUkssdUNBQXFCOztJQUNyQixtQ0FBaUI7O0lBQ2pCLG1DQUFpQjs7SUFDakIsbUNBQWlCOztJQUNqQixtQ0FBaUI7O0lBQ2pCLG9DQUFrQjs7SUFDbEIsOEJBQVk7O0lBQ1osZ0NBQWMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuZXhwb3J0IGNsYXNzIEVjbUNvbXBhbnlNb2RlbCB7XHJcbiAgICAgIG9yZ2FuaXphdGlvbjogc3RyaW5nO1xyXG4gICAgICBhZGRyZXNzMTogc3RyaW5nO1xyXG4gICAgICBhZGRyZXNzMjogc3RyaW5nO1xyXG4gICAgICBhZGRyZXNzMzogc3RyaW5nO1xyXG4gICAgICBwb3N0Y29kZTogc3RyaW5nO1xyXG4gICAgICB0ZWxlcGhvbmU6IHN0cmluZztcclxuICAgICAgZmF4OiBzdHJpbmc7XHJcbiAgICAgIGVtYWlsOiBzdHJpbmc7XHJcbn1cclxuIl19