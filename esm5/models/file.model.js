/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @record
 */
export function FileUploadProgress() { }
if (false) {
    /** @type {?} */
    FileUploadProgress.prototype.loaded;
    /** @type {?} */
    FileUploadProgress.prototype.total;
    /** @type {?} */
    FileUploadProgress.prototype.percent;
}
var FileUploadOptions = /** @class */ (function () {
    function FileUploadOptions() {
    }
    return FileUploadOptions;
}());
export { FileUploadOptions };
if (false) {
    /** @type {?} */
    FileUploadOptions.prototype.comment;
    /** @type {?} */
    FileUploadOptions.prototype.newVersion;
    /** @type {?} */
    FileUploadOptions.prototype.majorVersion;
    /** @type {?} */
    FileUploadOptions.prototype.parentId;
    /** @type {?} */
    FileUploadOptions.prototype.path;
    /** @type {?} */
    FileUploadOptions.prototype.nodeType;
    /** @type {?} */
    FileUploadOptions.prototype.properties;
    /** @type {?} */
    FileUploadOptions.prototype.association;
    /** @type {?} */
    FileUploadOptions.prototype.secondaryChildren;
    /** @type {?} */
    FileUploadOptions.prototype.targets;
}
/** @enum {number} */
var FileUploadStatus = {
    Pending: 0,
    Complete: 1,
    Starting: 2,
    Progress: 3,
    Cancelled: 4,
    Aborted: 5,
    Error: 6,
    Deleted: 7,
};
export { FileUploadStatus };
FileUploadStatus[FileUploadStatus.Pending] = 'Pending';
FileUploadStatus[FileUploadStatus.Complete] = 'Complete';
FileUploadStatus[FileUploadStatus.Starting] = 'Starting';
FileUploadStatus[FileUploadStatus.Progress] = 'Progress';
FileUploadStatus[FileUploadStatus.Cancelled] = 'Cancelled';
FileUploadStatus[FileUploadStatus.Aborted] = 'Aborted';
FileUploadStatus[FileUploadStatus.Error] = 'Error';
FileUploadStatus[FileUploadStatus.Deleted] = 'Deleted';
var FileModel = /** @class */ (function () {
    function FileModel(file, options, id) {
        this.status = FileUploadStatus.Pending;
        this.file = file;
        this.id = id;
        this.name = file.name;
        this.size = file.size;
        this.data = null;
        this.errorCode = null;
        this.progress = {
            loaded: 0,
            total: 0,
            percent: 0
        };
        this.options = Object.assign({}, {
            newVersion: false
        }, options);
    }
    Object.defineProperty(FileModel.prototype, "extension", {
        get: /**
         * @return {?}
         */
        function () {
            return this.name.slice((Math.max(0, this.name.lastIndexOf('.')) || Infinity) + 1);
        },
        enumerable: true,
        configurable: true
    });
    return FileModel;
}());
export { FileModel };
if (false) {
    /** @type {?} */
    FileModel.prototype.name;
    /** @type {?} */
    FileModel.prototype.size;
    /** @type {?} */
    FileModel.prototype.file;
    /** @type {?} */
    FileModel.prototype.id;
    /** @type {?} */
    FileModel.prototype.status;
    /** @type {?} */
    FileModel.prototype.errorCode;
    /** @type {?} */
    FileModel.prototype.progress;
    /** @type {?} */
    FileModel.prototype.options;
    /** @type {?} */
    FileModel.prototype.data;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vZGVscy9maWxlLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLHdDQUlDOzs7SUFIRyxvQ0FBZTs7SUFDZixtQ0FBYzs7SUFDZCxxQ0FBZ0I7O0FBR3BCO0lBQUE7SUFXQSxDQUFDO0lBQUQsd0JBQUM7QUFBRCxDQUFDLEFBWEQsSUFXQzs7OztJQVZHLG9DQUFpQjs7SUFDakIsdUNBQXFCOztJQUNyQix5Q0FBdUI7O0lBQ3ZCLHFDQUFrQjs7SUFDbEIsaUNBQWM7O0lBQ2QscUNBQWtCOztJQUNsQix1Q0FBaUI7O0lBQ2pCLHdDQUFrQjs7SUFDbEIsOENBQXFDOztJQUNyQyxvQ0FBNEI7Ozs7SUFJNUIsVUFBVztJQUNYLFdBQVk7SUFDWixXQUFZO0lBQ1osV0FBWTtJQUNaLFlBQWE7SUFDYixVQUFXO0lBQ1gsUUFBUztJQUNULFVBQVc7Ozs7Ozs7Ozs7O0FBR2Y7SUFZSSxtQkFBWSxJQUFVLEVBQUUsT0FBMkIsRUFBRSxFQUFXO1FBTmhFLFdBQU0sR0FBcUIsZ0JBQWdCLENBQUMsT0FBTyxDQUFDO1FBT2hELElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO1FBQ2pCLElBQUksQ0FBQyxFQUFFLEdBQUcsRUFBRSxDQUFDO1FBQ2IsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztRQUN0QixJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztRQUNqQixJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQztRQUV0QixJQUFJLENBQUMsUUFBUSxHQUFHO1lBQ1osTUFBTSxFQUFFLENBQUM7WUFDVCxLQUFLLEVBQUUsQ0FBQztZQUNSLE9BQU8sRUFBRSxDQUFDO1NBQ2IsQ0FBQztRQUVGLElBQUksQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEVBQUU7WUFDN0IsVUFBVSxFQUFFLEtBQUs7U0FDcEIsRUFBRSxPQUFPLENBQUMsQ0FBQztJQUNoQixDQUFDO0lBRUQsc0JBQUksZ0NBQVM7Ozs7UUFBYjtZQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLFFBQVEsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDO1FBQ3RGLENBQUM7OztPQUFBO0lBQ0wsZ0JBQUM7QUFBRCxDQUFDLEFBbENELElBa0NDOzs7O0lBakNHLHlCQUFzQjs7SUFDdEIseUJBQXNCOztJQUN0Qix5QkFBb0I7O0lBRXBCLHVCQUFXOztJQUNYLDJCQUFvRDs7SUFDcEQsOEJBQWtCOztJQUNsQiw2QkFBNkI7O0lBQzdCLDRCQUEyQjs7SUFDM0IseUJBQVUiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQXNzb2NDaGlsZEJvZHksIEFzc29jaWF0aW9uQm9keSB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5cclxuZXhwb3J0IGludGVyZmFjZSBGaWxlVXBsb2FkUHJvZ3Jlc3Mge1xyXG4gICAgbG9hZGVkOiBudW1iZXI7XHJcbiAgICB0b3RhbDogbnVtYmVyO1xyXG4gICAgcGVyY2VudDogbnVtYmVyO1xyXG59XHJcblxyXG5leHBvcnQgY2xhc3MgRmlsZVVwbG9hZE9wdGlvbnMge1xyXG4gICAgY29tbWVudD86IHN0cmluZztcclxuICAgIG5ld1ZlcnNpb24/OiBib29sZWFuO1xyXG4gICAgbWFqb3JWZXJzaW9uPzogYm9vbGVhbjtcclxuICAgIHBhcmVudElkPzogc3RyaW5nO1xyXG4gICAgcGF0aD86IHN0cmluZztcclxuICAgIG5vZGVUeXBlPzogc3RyaW5nO1xyXG4gICAgcHJvcGVydGllcz86IGFueTtcclxuICAgIGFzc29jaWF0aW9uPzogYW55O1xyXG4gICAgc2Vjb25kYXJ5Q2hpbGRyZW4/OiBBc3NvY0NoaWxkQm9keVtdO1xyXG4gICAgdGFyZ2V0cz86IEFzc29jaWF0aW9uQm9keVtdO1xyXG59XHJcblxyXG5leHBvcnQgZW51bSBGaWxlVXBsb2FkU3RhdHVzIHtcclxuICAgIFBlbmRpbmcgPSAwLFxyXG4gICAgQ29tcGxldGUgPSAxLFxyXG4gICAgU3RhcnRpbmcgPSAyLFxyXG4gICAgUHJvZ3Jlc3MgPSAzLFxyXG4gICAgQ2FuY2VsbGVkID0gNCxcclxuICAgIEFib3J0ZWQgPSA1LFxyXG4gICAgRXJyb3IgPSA2LFxyXG4gICAgRGVsZXRlZCA9IDdcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIEZpbGVNb2RlbCB7XHJcbiAgICByZWFkb25seSBuYW1lOiBzdHJpbmc7XHJcbiAgICByZWFkb25seSBzaXplOiBudW1iZXI7XHJcbiAgICByZWFkb25seSBmaWxlOiBGaWxlO1xyXG5cclxuICAgIGlkOiBzdHJpbmc7XHJcbiAgICBzdGF0dXM6IEZpbGVVcGxvYWRTdGF0dXMgPSBGaWxlVXBsb2FkU3RhdHVzLlBlbmRpbmc7XHJcbiAgICBlcnJvckNvZGU6IG51bWJlcjtcclxuICAgIHByb2dyZXNzOiBGaWxlVXBsb2FkUHJvZ3Jlc3M7XHJcbiAgICBvcHRpb25zOiBGaWxlVXBsb2FkT3B0aW9ucztcclxuICAgIGRhdGE6IGFueTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihmaWxlOiBGaWxlLCBvcHRpb25zPzogRmlsZVVwbG9hZE9wdGlvbnMsIGlkPzogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5maWxlID0gZmlsZTtcclxuICAgICAgICB0aGlzLmlkID0gaWQ7XHJcbiAgICAgICAgdGhpcy5uYW1lID0gZmlsZS5uYW1lO1xyXG4gICAgICAgIHRoaXMuc2l6ZSA9IGZpbGUuc2l6ZTtcclxuICAgICAgICB0aGlzLmRhdGEgPSBudWxsO1xyXG4gICAgICAgIHRoaXMuZXJyb3JDb2RlID0gbnVsbDtcclxuXHJcbiAgICAgICAgdGhpcy5wcm9ncmVzcyA9IHtcclxuICAgICAgICAgICAgbG9hZGVkOiAwLFxyXG4gICAgICAgICAgICB0b3RhbDogMCxcclxuICAgICAgICAgICAgcGVyY2VudDogMFxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHRoaXMub3B0aW9ucyA9IE9iamVjdC5hc3NpZ24oe30sIHtcclxuICAgICAgICAgICAgbmV3VmVyc2lvbjogZmFsc2VcclxuICAgICAgICB9LCBvcHRpb25zKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgZXh0ZW5zaW9uKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMubmFtZS5zbGljZSgoTWF0aC5tYXgoMCwgdGhpcy5uYW1lLmxhc3RJbmRleE9mKCcuJykpIHx8IEluZmluaXR5KSArIDEpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==