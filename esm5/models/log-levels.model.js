/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var LogLevelsEnum = /** @class */ (function (_super) {
    tslib_1.__extends(LogLevelsEnum, _super);
    function LogLevelsEnum() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    LogLevelsEnum.TRACE = 5;
    LogLevelsEnum.DEBUG = 4;
    LogLevelsEnum.INFO = 3;
    LogLevelsEnum.WARN = 2;
    LogLevelsEnum.ERROR = 1;
    LogLevelsEnum.SILENT = 0;
    return LogLevelsEnum;
}(Number));
export { LogLevelsEnum };
if (false) {
    /** @type {?} */
    LogLevelsEnum.TRACE;
    /** @type {?} */
    LogLevelsEnum.DEBUG;
    /** @type {?} */
    LogLevelsEnum.INFO;
    /** @type {?} */
    LogLevelsEnum.WARN;
    /** @type {?} */
    LogLevelsEnum.ERROR;
    /** @type {?} */
    LogLevelsEnum.SILENT;
}
/** @type {?} */
export var logLevels = [
    { level: LogLevelsEnum.TRACE, name: 'TRACE' },
    { level: LogLevelsEnum.DEBUG, name: 'DEBUG' },
    { level: LogLevelsEnum.INFO, name: 'INFO' },
    { level: LogLevelsEnum.WARN, name: 'WARN' },
    { level: LogLevelsEnum.ERROR, name: 'ERROR' },
    { level: LogLevelsEnum.SILENT, name: 'SILENT' }
];
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLWxldmVscy5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vZGVscy9sb2ctbGV2ZWxzLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQTtJQUFtQyx5Q0FBTTtJQUF6Qzs7SUFPQSxDQUFDO0lBTlUsbUJBQUssR0FBVyxDQUFDLENBQUM7SUFDbEIsbUJBQUssR0FBVyxDQUFDLENBQUM7SUFDbEIsa0JBQUksR0FBVyxDQUFDLENBQUM7SUFDakIsa0JBQUksR0FBVyxDQUFDLENBQUM7SUFDakIsbUJBQUssR0FBVyxDQUFDLENBQUM7SUFDbEIsb0JBQU0sR0FBVyxDQUFDLENBQUM7SUFDOUIsb0JBQUM7Q0FBQSxBQVBELENBQW1DLE1BQU0sR0FPeEM7U0FQWSxhQUFhOzs7SUFDdEIsb0JBQXlCOztJQUN6QixvQkFBeUI7O0lBQ3pCLG1CQUF3Qjs7SUFDeEIsbUJBQXdCOztJQUN4QixvQkFBeUI7O0lBQ3pCLHFCQUEwQjs7O0FBRzlCLE1BQU0sS0FBSyxTQUFTLEdBQVU7SUFDMUIsRUFBQyxLQUFLLEVBQUUsYUFBYSxDQUFDLEtBQUssRUFBRSxJQUFJLEVBQUUsT0FBTyxFQUFDO0lBQzNDLEVBQUMsS0FBSyxFQUFFLGFBQWEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBQztJQUMzQyxFQUFDLEtBQUssRUFBRSxhQUFhLENBQUMsSUFBSSxFQUFFLElBQUksRUFBRSxNQUFNLEVBQUM7SUFDekMsRUFBQyxLQUFLLEVBQUUsYUFBYSxDQUFDLElBQUksRUFBRSxJQUFJLEVBQUUsTUFBTSxFQUFDO0lBQ3pDLEVBQUMsS0FBSyxFQUFFLGFBQWEsQ0FBQyxLQUFLLEVBQUUsSUFBSSxFQUFFLE9BQU8sRUFBQztJQUMzQyxFQUFDLEtBQUssRUFBRSxhQUFhLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxRQUFRLEVBQUM7Q0FDaEQiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuZXhwb3J0IGNsYXNzIExvZ0xldmVsc0VudW0gZXh0ZW5kcyBOdW1iZXIge1xyXG4gICAgc3RhdGljIFRSQUNFOiBudW1iZXIgPSA1O1xyXG4gICAgc3RhdGljIERFQlVHOiBudW1iZXIgPSA0O1xyXG4gICAgc3RhdGljIElORk86IG51bWJlciA9IDM7XHJcbiAgICBzdGF0aWMgV0FSTjogbnVtYmVyID0gMjtcclxuICAgIHN0YXRpYyBFUlJPUjogbnVtYmVyID0gMTtcclxuICAgIHN0YXRpYyBTSUxFTlQ6IG51bWJlciA9IDA7XHJcbn1cclxuXHJcbmV4cG9ydCBsZXQgbG9nTGV2ZWxzOiBhbnlbXSA9IFtcclxuICAgIHtsZXZlbDogTG9nTGV2ZWxzRW51bS5UUkFDRSwgbmFtZTogJ1RSQUNFJ30sXHJcbiAgICB7bGV2ZWw6IExvZ0xldmVsc0VudW0uREVCVUcsIG5hbWU6ICdERUJVRyd9LFxyXG4gICAge2xldmVsOiBMb2dMZXZlbHNFbnVtLklORk8sIG5hbWU6ICdJTkZPJ30sXHJcbiAgICB7bGV2ZWw6IExvZ0xldmVsc0VudW0uV0FSTiwgbmFtZTogJ1dBUk4nfSxcclxuICAgIHtsZXZlbDogTG9nTGV2ZWxzRW51bS5FUlJPUiwgbmFtZTogJ0VSUk9SJ30sXHJcbiAgICB7bGV2ZWw6IExvZ0xldmVsc0VudW0uU0lMRU5ULCBuYW1lOiAnU0lMRU5UJ31cclxuXTtcclxuIl19