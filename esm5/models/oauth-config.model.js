/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @record
 */
export function OauthConfigModel() { }
if (false) {
    /** @type {?} */
    OauthConfigModel.prototype.host;
    /** @type {?} */
    OauthConfigModel.prototype.clientId;
    /** @type {?} */
    OauthConfigModel.prototype.scope;
    /** @type {?} */
    OauthConfigModel.prototype.implicitFlow;
    /** @type {?} */
    OauthConfigModel.prototype.redirectUri;
    /** @type {?|undefined} */
    OauthConfigModel.prototype.silentLogin;
    /** @type {?|undefined} */
    OauthConfigModel.prototype.secret;
    /** @type {?|undefined} */
    OauthConfigModel.prototype.redirectUriLogout;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2F1dGgtY29uZmlnLm1vZGVsLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsibW9kZWxzL29hdXRoLWNvbmZpZy5tb2RlbC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxzQ0FTQzs7O0lBUkcsZ0NBQWE7O0lBQ2Isb0NBQWlCOztJQUNqQixpQ0FBYzs7SUFDZCx3Q0FBc0I7O0lBQ3RCLHVDQUFvQjs7SUFDcEIsdUNBQXNCOztJQUN0QixrQ0FBZ0I7O0lBQ2hCLDZDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIE9hdXRoQ29uZmlnTW9kZWwge1xyXG4gICAgaG9zdDogc3RyaW5nO1xyXG4gICAgY2xpZW50SWQ6IHN0cmluZztcclxuICAgIHNjb3BlOiBzdHJpbmc7XHJcbiAgICBpbXBsaWNpdEZsb3c6IGJvb2xlYW47XHJcbiAgICByZWRpcmVjdFVyaTogc3RyaW5nO1xyXG4gICAgc2lsZW50TG9naW4/OiBib29sZWFuO1xyXG4gICAgc2VjcmV0Pzogc3RyaW5nO1xyXG4gICAgcmVkaXJlY3RVcmlMb2dvdXQ/OiBzdHJpbmc7XHJcbn1cclxuIl19