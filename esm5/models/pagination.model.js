/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Pagination } from '@alfresco/js-api';
var PaginationModel = /** @class */ (function (_super) {
    tslib_1.__extends(PaginationModel, _super);
    function PaginationModel(input) {
        var _this = _super.call(this, input) || this;
        if (input) {
            _this.count = input.count;
            _this.hasMoreItems = input.hasMoreItems ? input.hasMoreItems : false;
            _this.merge = input.merge ? input.merge : false;
            _this.totalItems = input.totalItems;
            _this.skipCount = input.skipCount;
            _this.maxItems = input.maxItems;
        }
        return _this;
    }
    return PaginationModel;
}(Pagination));
export { PaginationModel };
if (false) {
    /** @type {?} */
    PaginationModel.prototype.merge;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnaW5hdGlvbi5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbIm1vZGVscy9wYWdpbmF0aW9uLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sa0JBQWtCLENBQUM7QUFFOUM7SUFBcUMsMkNBQVU7SUFHM0MseUJBQVksS0FBVztRQUF2QixZQUNJLGtCQUFNLEtBQUssQ0FBQyxTQVNmO1FBUkcsSUFBSSxLQUFLLEVBQUU7WUFDUCxLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7WUFDekIsS0FBSSxDQUFDLFlBQVksR0FBRyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsWUFBWSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDcEUsS0FBSSxDQUFDLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7WUFDL0MsS0FBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDO1lBQ25DLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQztZQUNqQyxLQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUM7U0FDbEM7O0lBQ0wsQ0FBQztJQUNMLHNCQUFDO0FBQUQsQ0FBQyxBQWRELENBQXFDLFVBQVUsR0FjOUM7Ozs7SUFiRyxnQ0FBZ0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgUGFnaW5hdGlvbiB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5cclxuZXhwb3J0IGNsYXNzIFBhZ2luYXRpb25Nb2RlbCBleHRlbmRzIFBhZ2luYXRpb24ge1xyXG4gICAgbWVyZ2U/OiBib29sZWFuO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGlucHV0PzogYW55KSB7XHJcbiAgICAgICAgc3VwZXIoaW5wdXQpO1xyXG4gICAgICAgIGlmIChpbnB1dCkge1xyXG4gICAgICAgICAgICB0aGlzLmNvdW50ID0gaW5wdXQuY291bnQ7XHJcbiAgICAgICAgICAgIHRoaXMuaGFzTW9yZUl0ZW1zID0gaW5wdXQuaGFzTW9yZUl0ZW1zID8gaW5wdXQuaGFzTW9yZUl0ZW1zIDogZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMubWVyZ2UgPSBpbnB1dC5tZXJnZSA/IGlucHV0Lm1lcmdlIDogZmFsc2U7XHJcbiAgICAgICAgICAgIHRoaXMudG90YWxJdGVtcyA9IGlucHV0LnRvdGFsSXRlbXM7XHJcbiAgICAgICAgICAgIHRoaXMuc2tpcENvdW50ID0gaW5wdXQuc2tpcENvdW50O1xyXG4gICAgICAgICAgICB0aGlzLm1heEl0ZW1zID0gaW5wdXQubWF4SXRlbXM7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==