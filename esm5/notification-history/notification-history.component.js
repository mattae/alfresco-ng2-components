/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewChild } from '@angular/core';
import { NotificationService } from '../services/notification.service';
import { MatMenuTrigger } from '@angular/material';
import { takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
var NotificationHistoryComponent = /** @class */ (function () {
    function NotificationHistoryComponent(notificationService) {
        var _this = this;
        this.notificationService = notificationService;
        this.onDestroy$ = new Subject();
        this.notifications = [];
        /**
         * Custom choice for opening the menu at the bottom. Can be `before` or `after`.
         */
        this.menuPositionX = 'after';
        /**
         * Custom choice for opening the menu at the bottom. Can be `above` or `below`.
         */
        this.menuPositionY = 'below';
        this.notificationService.messages
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} message
         * @return {?}
         */
        function (message) {
            _this.notifications.push(message);
        }));
    }
    /**
     * @return {?}
     */
    NotificationHistoryComponent.prototype.isEmptyNotification = /**
     * @return {?}
     */
    function () {
        return (!this.notifications || this.notifications.length === 0);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    NotificationHistoryComponent.prototype.onKeyPress = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.closeUserModal(event);
    };
    /**
     * @return {?}
     */
    NotificationHistoryComponent.prototype.markAsRead = /**
     * @return {?}
     */
    function () {
        this.notifications = [];
    };
    /**
     * @return {?}
     */
    NotificationHistoryComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    };
    /**
     * @private
     * @param {?} $event
     * @return {?}
     */
    NotificationHistoryComponent.prototype.closeUserModal = /**
     * @private
     * @param {?} $event
     * @return {?}
     */
    function ($event) {
        if ($event.keyCode === 27) {
            this.trigger.closeMenu();
        }
    };
    NotificationHistoryComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-notification-history',
                    template: "<div (keyup)=\"onKeyPress($event)\">\r\n    <button mat-button [matMenuTriggerFor]=\"menu\" class=\"adf-notification-history-menu_button\"\r\n            id=\"adf-notification-history-open-button\">\r\n        <mat-icon>mail_outline</mat-icon>\r\n    </button>\r\n    <mat-menu #menu=\"matMenu\" [xPosition]=\"menuPositionX\" [yPosition]=\"menuPositionY\"\r\n              [overlapTrigger]=\"false\" id=\"adf-notification-history-menu\" class=\"adf-notification-history-menu\">\r\n\r\n        <div id=\"adf-notification-history-list\">\r\n            <mat-list>\r\n                <mat-list-item>\r\n                    <h6 mat-line>{{ 'NOTIFICATION_HISTORY.NOTIFICATIONS' | translate }}</h6>\r\n                </mat-list-item>\r\n            </mat-list>\r\n            <mat-divider></mat-divider>\r\n\r\n            <mat-list>\r\n                <mat-list-item *ngFor=\"let notification of notifications\">\r\n                    <mat-icon mat-list-icon>{{notification.info? notification.info: 'info'}}</mat-icon>\r\n                    <h4 mat-line>{{notification.message}}</h4>\r\n                    <p mat-line> {{notification.dateTime | date}} </p>\r\n                </mat-list-item>\r\n                <mat-list-item *ngIf=\"isEmptyNotification()\" id=\"adf-notification-history-component-no-message\">\r\n                    <h4 mat-line>{{ 'NOTIFICATION_HISTORY.NO_MESSAGE' | translate }}</h4>\r\n                </mat-list-item>\r\n                <mat-action-list *ngIf=\"!isEmptyNotification()\" id=\"adf-notification-history-mark-as-read\">\r\n                    <button mat-list-item (click)=\"markAsRead()\">{{ 'NOTIFICATION_HISTORY.MARK_AS_READ' | translate }}\r\n                    </button>\r\n                </mat-action-list>\r\n            </mat-list>\r\n        </div>\r\n    </mat-menu>\r\n</div>\r\n",
                    styles: [".adf-notification-history-menu_button.mat-button{margin-right:0;border-radius:90%;padding:0;min-width:40px;height:40px}@media only screen and (min-device-width:480px){.mat-menu-panel.adf-notification-history-menu{max-height:450px;min-width:450px;overflow:auto;padding:0}}.mat-menu-panel.adf-notification-history-menu .mat-menu-content{padding:0}"]
                }] }
    ];
    /** @nocollapse */
    NotificationHistoryComponent.ctorParameters = function () { return [
        { type: NotificationService }
    ]; };
    NotificationHistoryComponent.propDecorators = {
        trigger: [{ type: ViewChild, args: [MatMenuTrigger, { static: true },] }],
        menuPositionX: [{ type: Input }],
        menuPositionY: [{ type: Input }]
    };
    return NotificationHistoryComponent;
}());
export { NotificationHistoryComponent };
if (false) {
    /** @type {?} */
    NotificationHistoryComponent.prototype.onDestroy$;
    /** @type {?} */
    NotificationHistoryComponent.prototype.notifications;
    /** @type {?} */
    NotificationHistoryComponent.prototype.trigger;
    /**
     * Custom choice for opening the menu at the bottom. Can be `before` or `after`.
     * @type {?}
     */
    NotificationHistoryComponent.prototype.menuPositionX;
    /**
     * Custom choice for opening the menu at the bottom. Can be `above` or `below`.
     * @type {?}
     */
    NotificationHistoryComponent.prototype.menuPositionY;
    /**
     * @type {?}
     * @private
     */
    NotificationHistoryComponent.prototype.notificationService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLWhpc3RvcnkuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsibm90aWZpY2F0aW9uLWhpc3Rvcnkvbm90aWZpY2F0aW9uLWhpc3RvcnkuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxPQUFPLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxTQUFTLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFDdkUsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFFdkUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBRS9CO0lBc0JJLHNDQUNZLG1CQUF3QztRQURwRCxpQkFPQztRQU5XLHdCQUFtQixHQUFuQixtQkFBbUIsQ0FBcUI7UUFoQnBELGVBQVUsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDO1FBRXBDLGtCQUFhLEdBQXdCLEVBQUUsQ0FBQzs7OztRQU94QyxrQkFBYSxHQUFXLE9BQU8sQ0FBQzs7OztRQUloQyxrQkFBYSxHQUFXLE9BQU8sQ0FBQztRQUk1QixJQUFJLENBQUMsbUJBQW1CLENBQUMsUUFBUTthQUM1QixJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNoQyxTQUFTOzs7O1FBQUMsVUFBQyxPQUFPO1lBQ25CLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3JDLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7OztJQUVELDBEQUFtQjs7O0lBQW5CO1FBQ0ksT0FBTyxDQUFDLENBQUMsSUFBSSxDQUFDLGFBQWEsSUFBSSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sS0FBSyxDQUFDLENBQUMsQ0FBQztJQUNwRSxDQUFDOzs7OztJQUVELGlEQUFVOzs7O0lBQVYsVUFBVyxLQUFvQjtRQUMzQixJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQy9CLENBQUM7Ozs7SUFFRCxpREFBVTs7O0lBQVY7UUFDSSxJQUFJLENBQUMsYUFBYSxHQUFHLEVBQUUsQ0FBQztJQUM1QixDQUFDOzs7O0lBRUQsa0RBQVc7OztJQUFYO1FBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDOzs7Ozs7SUFFTyxxREFBYzs7Ozs7SUFBdEIsVUFBdUIsTUFBcUI7UUFDeEMsSUFBSSxNQUFNLENBQUMsT0FBTyxLQUFLLEVBQUUsRUFBRTtZQUN2QixJQUFJLENBQUMsT0FBTyxDQUFDLFNBQVMsRUFBRSxDQUFDO1NBQzVCO0lBQ0wsQ0FBQzs7Z0JBcERKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsMEJBQTBCO29CQUVwQyxnekRBQWtEOztpQkFDckQ7Ozs7Z0JBVlEsbUJBQW1COzs7MEJBaUJ2QixTQUFTLFNBQUMsY0FBYyxFQUFFLEVBQUMsTUFBTSxFQUFFLElBQUksRUFBQztnQ0FJeEMsS0FBSztnQ0FJTCxLQUFLOztJQWtDVixtQ0FBQztDQUFBLEFBckRELElBcURDO1NBaERZLDRCQUE0Qjs7O0lBRXJDLGtEQUFvQzs7SUFFcEMscURBQXdDOztJQUV4QywrQ0FDd0I7Ozs7O0lBR3hCLHFEQUNnQzs7Ozs7SUFHaEMscURBQ2dDOzs7OztJQUc1QiwyREFBZ0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cblxuLyohXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cbiAqXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4gKlxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuICpcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXG4gKi9cblxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgVmlld0NoaWxkLCBPbkRlc3Ryb3kgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IE5vdGlmaWNhdGlvblNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9ub3RpZmljYXRpb24uc2VydmljZSc7XG5pbXBvcnQgeyBOb3RpZmljYXRpb25Nb2RlbCB9IGZyb20gJy4uL21vZGVscy9ub3RpZmljYXRpb24ubW9kZWwnO1xuaW1wb3J0IHsgTWF0TWVudVRyaWdnZXIgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XG5cbkBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYWRmLW5vdGlmaWNhdGlvbi1oaXN0b3J5JyxcbiAgICBzdHlsZVVybHM6IFsnbm90aWZpY2F0aW9uLWhpc3RvcnkuY29tcG9uZW50LnNjc3MnXSxcbiAgICB0ZW1wbGF0ZVVybDogJ25vdGlmaWNhdGlvbi1oaXN0b3J5LmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBOb3RpZmljYXRpb25IaXN0b3J5Q29tcG9uZW50IGltcGxlbWVudHMgT25EZXN0cm95IHtcblxuICAgIG9uRGVzdHJveSQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xuXG4gICAgbm90aWZpY2F0aW9uczogTm90aWZpY2F0aW9uTW9kZWxbXSA9IFtdO1xuXG4gICAgQFZpZXdDaGlsZChNYXRNZW51VHJpZ2dlciwge3N0YXRpYzogdHJ1ZX0pXG4gICAgdHJpZ2dlcjogTWF0TWVudVRyaWdnZXI7XG5cbiAgICAvKiogQ3VzdG9tIGNob2ljZSBmb3Igb3BlbmluZyB0aGUgbWVudSBhdCB0aGUgYm90dG9tLiBDYW4gYmUgYGJlZm9yZWAgb3IgYGFmdGVyYC4gKi9cbiAgICBASW5wdXQoKVxuICAgIG1lbnVQb3NpdGlvblg6IHN0cmluZyA9ICdhZnRlcic7XG5cbiAgICAvKiogQ3VzdG9tIGNob2ljZSBmb3Igb3BlbmluZyB0aGUgbWVudSBhdCB0aGUgYm90dG9tLiBDYW4gYmUgYGFib3ZlYCBvciBgYmVsb3dgLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgbWVudVBvc2l0aW9uWTogc3RyaW5nID0gJ2JlbG93JztcblxuICAgIGNvbnN0cnVjdG9yKFxuICAgICAgICBwcml2YXRlIG5vdGlmaWNhdGlvblNlcnZpY2U6IE5vdGlmaWNhdGlvblNlcnZpY2UpIHtcbiAgICAgICAgdGhpcy5ub3RpZmljYXRpb25TZXJ2aWNlLm1lc3NhZ2VzXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5vbkRlc3Ryb3kkKSlcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKG1lc3NhZ2UpID0+IHtcbiAgICAgICAgICAgIHRoaXMubm90aWZpY2F0aW9ucy5wdXNoKG1lc3NhZ2UpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBpc0VtcHR5Tm90aWZpY2F0aW9uKCk6IGJvb2xlYW4ge1xuICAgICAgICByZXR1cm4gKCF0aGlzLm5vdGlmaWNhdGlvbnMgfHwgdGhpcy5ub3RpZmljYXRpb25zLmxlbmd0aCA9PT0gMCk7XG4gICAgfVxuXG4gICAgb25LZXlQcmVzcyhldmVudDogS2V5Ym9hcmRFdmVudCkge1xuICAgICAgICB0aGlzLmNsb3NlVXNlck1vZGFsKGV2ZW50KTtcbiAgICB9XG5cbiAgICBtYXJrQXNSZWFkKCkge1xuICAgICAgICB0aGlzLm5vdGlmaWNhdGlvbnMgPSBbXTtcbiAgICB9XG5cbiAgICBuZ09uRGVzdHJveSgpIHtcbiAgICAgICAgdGhpcy5vbkRlc3Ryb3kkLm5leHQodHJ1ZSk7XG4gICAgICAgIHRoaXMub25EZXN0cm95JC5jb21wbGV0ZSgpO1xuICAgIH1cblxuICAgIHByaXZhdGUgY2xvc2VVc2VyTW9kYWwoJGV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XG4gICAgICAgIGlmICgkZXZlbnQua2V5Q29kZSA9PT0gMjcpIHtcbiAgICAgICAgICAgIHRoaXMudHJpZ2dlci5jbG9zZU1lbnUoKTtcbiAgICAgICAgfVxuICAgIH1cbn1cbiJdfQ==