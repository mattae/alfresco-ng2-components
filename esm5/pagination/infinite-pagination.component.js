/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:no-input-rename  */
/* tslint:disable:rxjs-no-subject-value */
import { ChangeDetectionStrategy, ChangeDetectorRef, Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { UserPreferencesService, UserPreferenceValues } from '../services/user-preferences.service';
import { Pagination } from '@alfresco/js-api';
import { takeUntil } from 'rxjs/operators';
var InfinitePaginationComponent = /** @class */ (function () {
    function InfinitePaginationComponent(cdr, userPreferencesService) {
        this.cdr = cdr;
        this.userPreferencesService = userPreferencesService;
        this.onDestroy$ = new Subject();
        /**
         * Is a new page loading?
         */
        this.isLoading = false;
        /**
         * Emitted when the "Load More" button is clicked.
         */
        this.loadMore = new EventEmitter();
        this.pagination = InfinitePaginationComponent.DEFAULT_PAGINATION;
        this.requestPaginationModel = {
            skipCount: 0,
            merge: true
        };
    }
    Object.defineProperty(InfinitePaginationComponent.prototype, "target", {
        get: /**
         * @return {?}
         */
        function () {
            return this._target;
        },
        /** Component that provides custom pagination support. */
        set: /**
         * Component that provides custom pagination support.
         * @param {?} target
         * @return {?}
         */
        function (target) {
            var _this = this;
            if (target) {
                this._target = target;
                target.pagination
                    .pipe(takeUntil(this.onDestroy$))
                    .subscribe((/**
                 * @param {?} pagination
                 * @return {?}
                 */
                function (pagination) {
                    _this.isLoading = false;
                    _this.pagination = pagination;
                    if (!_this.pagination.hasMoreItems) {
                        _this.pagination.hasMoreItems = false;
                    }
                    _this.cdr.detectChanges();
                }));
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    InfinitePaginationComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.userPreferencesService
            .select(UserPreferenceValues.PaginationSize)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} pageSize
         * @return {?}
         */
        function (pageSize) {
            _this.pageSize = _this.pageSize || pageSize;
            _this.requestPaginationModel.maxItems = _this.pageSize;
        }));
    };
    /**
     * @return {?}
     */
    InfinitePaginationComponent.prototype.onLoadMore = /**
     * @return {?}
     */
    function () {
        this.requestPaginationModel.skipCount = 0;
        this.requestPaginationModel.merge = false;
        this.requestPaginationModel.maxItems += this.pageSize;
        this.loadMore.next(this.requestPaginationModel);
        if (this._target) {
            this.isLoading = true;
            this._target.updatePagination((/** @type {?} */ (this.requestPaginationModel)));
        }
    };
    /**
     * @return {?}
     */
    InfinitePaginationComponent.prototype.reset = /**
     * @return {?}
     */
    function () {
        this.pagination.skipCount = 0;
        this.pagination.maxItems = this.pageSize;
        if (this._target) {
            this._target.updatePagination(this.pagination);
        }
    };
    /**
     * @return {?}
     */
    InfinitePaginationComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    };
    InfinitePaginationComponent.DEFAULT_PAGINATION = new Pagination({
        skipCount: 0,
        maxItems: 25,
        totalItems: 0
    });
    InfinitePaginationComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-infinite-pagination',
                    host: { 'class': 'infinite-adf-pagination' },
                    template: "<div *ngIf=\"pagination?.hasMoreItems || isLoading\" class=\"adf-infinite-pagination\">\r\n\r\n    <button mat-button\r\n        *ngIf=\"!isLoading\"\r\n        class=\"adf-infinite-pagination-load-more\"\r\n        (click)=\"onLoadMore()\"\r\n        data-automation-id=\"adf-infinite-pagination-button\">\r\n            <ng-content></ng-content>\r\n    </button>\r\n\r\n    <mat-progress-bar *ngIf=\"isLoading\"\r\n        mode=\"indeterminate\"\r\n        class=\"adf-infinite-pagination-spinner\"\r\n        data-automation-id=\"adf-infinite-pagination-spinner\"></mat-progress-bar>\r\n</div>\r\n",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    encapsulation: ViewEncapsulation.None,
                    styles: [".adf-infinite-pagination{display:flex;justify-content:space-around;min-height:56px}.adf-infinite-pagination-load-more{margin-bottom:10px;margin-top:10px}"]
                }] }
    ];
    /** @nocollapse */
    InfinitePaginationComponent.ctorParameters = function () { return [
        { type: ChangeDetectorRef },
        { type: UserPreferencesService }
    ]; };
    InfinitePaginationComponent.propDecorators = {
        target: [{ type: Input }],
        pageSize: [{ type: Input }],
        isLoading: [{ type: Input, args: ['loading',] }],
        loadMore: [{ type: Output }]
    };
    return InfinitePaginationComponent;
}());
export { InfinitePaginationComponent };
if (false) {
    /** @type {?} */
    InfinitePaginationComponent.DEFAULT_PAGINATION;
    /** @type {?} */
    InfinitePaginationComponent.prototype._target;
    /**
     * @type {?}
     * @private
     */
    InfinitePaginationComponent.prototype.onDestroy$;
    /**
     * Number of items that are added with each "load more" event.
     * @type {?}
     */
    InfinitePaginationComponent.prototype.pageSize;
    /**
     * Is a new page loading?
     * @type {?}
     */
    InfinitePaginationComponent.prototype.isLoading;
    /**
     * Emitted when the "Load More" button is clicked.
     * @type {?}
     */
    InfinitePaginationComponent.prototype.loadMore;
    /** @type {?} */
    InfinitePaginationComponent.prototype.pagination;
    /** @type {?} */
    InfinitePaginationComponent.prototype.requestPaginationModel;
    /**
     * @type {?}
     * @private
     */
    InfinitePaginationComponent.prototype.cdr;
    /**
     * @type {?}
     * @private
     */
    InfinitePaginationComponent.prototype.userPreferencesService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5maW5pdGUtcGFnaW5hdGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJwYWdpbmF0aW9uL2luZmluaXRlLXBhZ2luYXRpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQkEsT0FBTyxFQUNILHVCQUF1QixFQUFFLGlCQUFpQixFQUFFLFNBQVMsRUFBRSxZQUFZLEVBQ25FLEtBQUssRUFBVSxNQUFNLEVBQWEsaUJBQWlCLEVBQ3RELE1BQU0sZUFBZSxDQUFDO0FBR3ZCLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFJL0IsT0FBTyxFQUFFLHNCQUFzQixFQUFFLG9CQUFvQixFQUFFLE1BQU0sc0NBQXNDLENBQUM7QUFDcEcsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUUzQztJQThESSxxQ0FBb0IsR0FBc0IsRUFDdEIsc0JBQThDO1FBRDlDLFFBQUcsR0FBSCxHQUFHLENBQW1CO1FBQ3RCLDJCQUFzQixHQUF0QixzQkFBc0IsQ0FBd0I7UUE5QzFELGVBQVUsR0FBRyxJQUFJLE9BQU8sRUFBVyxDQUFDOzs7O1FBZ0M1QyxjQUFTLEdBQVksS0FBSyxDQUFDOzs7O1FBSTNCLGFBQVEsR0FBeUMsSUFBSSxZQUFZLEVBQTBCLENBQUM7UUFFNUYsZUFBVSxHQUFvQiwyQkFBMkIsQ0FBQyxrQkFBa0IsQ0FBQztRQUU3RSwyQkFBc0IsR0FBMkI7WUFDN0MsU0FBUyxFQUFFLENBQUM7WUFDWixLQUFLLEVBQUUsSUFBSTtTQUNkLENBQUM7SUFJRixDQUFDO0lBNUNELHNCQUNJLCtDQUFNOzs7O1FBa0JWO1lBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDO1FBQ3hCLENBQUM7UUF0QkQseURBQXlEOzs7Ozs7UUFDekQsVUFDVyxNQUEwQjtZQURyQyxpQkFpQkM7WUFmRyxJQUFJLE1BQU0sRUFBRTtnQkFDUixJQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQztnQkFDdEIsTUFBTSxDQUFDLFVBQVU7cUJBQ1osSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7cUJBQ2hDLFNBQVM7Ozs7Z0JBQUMsVUFBQSxVQUFVO29CQUNqQixLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztvQkFDdkIsS0FBSSxDQUFDLFVBQVUsR0FBRyxVQUFVLENBQUM7b0JBRTdCLElBQUksQ0FBQyxLQUFJLENBQUMsVUFBVSxDQUFDLFlBQVksRUFBRTt3QkFDL0IsS0FBSSxDQUFDLFVBQVUsQ0FBQyxZQUFZLEdBQUcsS0FBSyxDQUFDO3FCQUN4QztvQkFFRCxLQUFJLENBQUMsR0FBRyxDQUFDLGFBQWEsRUFBRSxDQUFDO2dCQUM3QixDQUFDLEVBQUMsQ0FBQzthQUNWO1FBQ0wsQ0FBQzs7O09BQUE7Ozs7SUE2QkQsOENBQVE7OztJQUFSO1FBQUEsaUJBUUM7UUFQRyxJQUFJLENBQUMsc0JBQXNCO2FBQ3RCLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxjQUFjLENBQUM7YUFDM0MsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDaEMsU0FBUzs7OztRQUFDLFVBQUMsUUFBZ0I7WUFDeEIsS0FBSSxDQUFDLFFBQVEsR0FBRyxLQUFJLENBQUMsUUFBUSxJQUFJLFFBQVEsQ0FBQztZQUMxQyxLQUFJLENBQUMsc0JBQXNCLENBQUMsUUFBUSxHQUFHLEtBQUksQ0FBQyxRQUFRLENBQUM7UUFDekQsQ0FBQyxFQUFDLENBQUM7SUFDWCxDQUFDOzs7O0lBRUQsZ0RBQVU7OztJQUFWO1FBQ0ksSUFBSSxDQUFDLHNCQUFzQixDQUFDLFNBQVMsR0FBRyxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLEtBQUssR0FBRyxLQUFLLENBQUM7UUFFMUMsSUFBSSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDO1FBRXRELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxDQUFDO1FBRWhELElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNkLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1lBQ3RCLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsbUJBQXlCLElBQUksQ0FBQyxzQkFBc0IsRUFBQSxDQUFDLENBQUM7U0FDdkY7SUFDTCxDQUFDOzs7O0lBRUQsMkNBQUs7OztJQUFMO1FBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxRQUFRLENBQUM7UUFFekMsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO1lBQ2QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDbEQ7SUFDTCxDQUFDOzs7O0lBRUQsaURBQVc7OztJQUFYO1FBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBNUZNLDhDQUFrQixHQUFlLElBQUksVUFBVSxDQUFDO1FBQ25ELFNBQVMsRUFBRSxDQUFDO1FBQ1osUUFBUSxFQUFFLEVBQUU7UUFDWixVQUFVLEVBQUUsQ0FBQztLQUNoQixDQUFDLENBQUM7O2dCQWROLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUseUJBQXlCO29CQUNuQyxJQUFJLEVBQUUsRUFBRSxPQUFPLEVBQUUseUJBQXlCLEVBQUU7b0JBQzVDLG9tQkFBbUQ7b0JBRW5ELGVBQWUsRUFBRSx1QkFBdUIsQ0FBQyxNQUFNO29CQUMvQyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7aUJBQ3hDOzs7O2dCQXBCNEIsaUJBQWlCO2dCQVNyQyxzQkFBc0I7Ozt5QkF3QjFCLEtBQUs7MkJBd0JMLEtBQUs7NEJBSUwsS0FBSyxTQUFDLFNBQVM7MkJBSWYsTUFBTTs7SUFtRFgsa0NBQUM7Q0FBQSxBQXZHRCxJQXVHQztTQS9GWSwyQkFBMkI7OztJQUVwQywrQ0FJRzs7SUFFSCw4Q0FBNEI7Ozs7O0lBQzVCLGlEQUE0Qzs7Ozs7SUEyQjVDLCtDQUNpQjs7Ozs7SUFHakIsZ0RBQzJCOzs7OztJQUczQiwrQ0FDNEY7O0lBRTVGLGlEQUE2RTs7SUFFN0UsNkRBR0U7Ozs7O0lBRVUsMENBQThCOzs7OztJQUM5Qiw2REFBc0QiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuLyogdHNsaW50OmRpc2FibGU6bm8taW5wdXQtcmVuYW1lICAqL1xyXG4vKiB0c2xpbnQ6ZGlzYWJsZTpyeGpzLW5vLXN1YmplY3QtdmFsdWUgKi9cclxuXHJcbmltcG9ydCB7XHJcbiAgICBDaGFuZ2VEZXRlY3Rpb25TdHJhdGVneSwgQ2hhbmdlRGV0ZWN0b3JSZWYsIENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLFxyXG4gICAgSW5wdXQsIE9uSW5pdCwgT3V0cHV0LCBPbkRlc3Ryb3ksIFZpZXdFbmNhcHN1bGF0aW9uXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5pbXBvcnQgeyBQYWdpbmF0ZWRDb21wb25lbnQgfSBmcm9tICcuL3BhZ2luYXRlZC1jb21wb25lbnQuaW50ZXJmYWNlJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBQYWdpbmF0aW9uQ29tcG9uZW50SW50ZXJmYWNlIH0gZnJvbSAnLi9wYWdpbmF0aW9uLWNvbXBvbmVudC5pbnRlcmZhY2UnO1xyXG5pbXBvcnQgeyBQYWdpbmF0aW9uTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvcGFnaW5hdGlvbi5tb2RlbCc7XHJcbmltcG9ydCB7IFJlcXVlc3RQYWdpbmF0aW9uTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvcmVxdWVzdC1wYWdpbmF0aW9uLm1vZGVsJztcclxuaW1wb3J0IHsgVXNlclByZWZlcmVuY2VzU2VydmljZSwgVXNlclByZWZlcmVuY2VWYWx1ZXMgfSBmcm9tICcuLi9zZXJ2aWNlcy91c2VyLXByZWZlcmVuY2VzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBQYWdpbmF0aW9uIH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtaW5maW5pdGUtcGFnaW5hdGlvbicsXHJcbiAgICBob3N0OiB7ICdjbGFzcyc6ICdpbmZpbml0ZS1hZGYtcGFnaW5hdGlvbicgfSxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9pbmZpbml0ZS1wYWdpbmF0aW9uLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2luZmluaXRlLXBhZ2luYXRpb24uY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgSW5maW5pdGVQYWdpbmF0aW9uQ29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0LCBPbkRlc3Ryb3ksIFBhZ2luYXRpb25Db21wb25lbnRJbnRlcmZhY2Uge1xyXG5cclxuICAgIHN0YXRpYyBERUZBVUxUX1BBR0lOQVRJT046IFBhZ2luYXRpb24gPSBuZXcgUGFnaW5hdGlvbih7XHJcbiAgICAgICAgc2tpcENvdW50OiAwLFxyXG4gICAgICAgIG1heEl0ZW1zOiAyNSxcclxuICAgICAgICB0b3RhbEl0ZW1zOiAwXHJcbiAgICB9KTtcclxuXHJcbiAgICBfdGFyZ2V0OiBQYWdpbmF0ZWRDb21wb25lbnQ7XHJcbiAgICBwcml2YXRlIG9uRGVzdHJveSQgPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xyXG5cclxuICAgIC8qKiBDb21wb25lbnQgdGhhdCBwcm92aWRlcyBjdXN0b20gcGFnaW5hdGlvbiBzdXBwb3J0LiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHNldCB0YXJnZXQodGFyZ2V0OiBQYWdpbmF0ZWRDb21wb25lbnQpIHtcclxuICAgICAgICBpZiAodGFyZ2V0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuX3RhcmdldCA9IHRhcmdldDtcclxuICAgICAgICAgICAgdGFyZ2V0LnBhZ2luYXRpb25cclxuICAgICAgICAgICAgICAgIC5waXBlKHRha2VVbnRpbCh0aGlzLm9uRGVzdHJveSQpKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZShwYWdpbmF0aW9uID0+IHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzTG9hZGluZyA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMucGFnaW5hdGlvbiA9IHBhZ2luYXRpb247XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICghdGhpcy5wYWdpbmF0aW9uLmhhc01vcmVJdGVtcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnBhZ2luYXRpb24uaGFzTW9yZUl0ZW1zID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNkci5kZXRlY3RDaGFuZ2VzKCk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHRhcmdldCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5fdGFyZ2V0O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKiBOdW1iZXIgb2YgaXRlbXMgdGhhdCBhcmUgYWRkZWQgd2l0aCBlYWNoIFwibG9hZCBtb3JlXCIgZXZlbnQuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgcGFnZVNpemU6IG51bWJlcjtcclxuXHJcbiAgICAvKiogSXMgYSBuZXcgcGFnZSBsb2FkaW5nPyAqL1xyXG4gICAgQElucHV0KCdsb2FkaW5nJylcclxuICAgIGlzTG9hZGluZzogYm9vbGVhbiA9IGZhbHNlO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIFwiTG9hZCBNb3JlXCIgYnV0dG9uIGlzIGNsaWNrZWQuICovXHJcbiAgICBAT3V0cHV0KClcclxuICAgIGxvYWRNb3JlOiBFdmVudEVtaXR0ZXI8UmVxdWVzdFBhZ2luYXRpb25Nb2RlbD4gPSBuZXcgRXZlbnRFbWl0dGVyPFJlcXVlc3RQYWdpbmF0aW9uTW9kZWw+KCk7XHJcblxyXG4gICAgcGFnaW5hdGlvbjogUGFnaW5hdGlvbk1vZGVsID0gSW5maW5pdGVQYWdpbmF0aW9uQ29tcG9uZW50LkRFRkFVTFRfUEFHSU5BVElPTjtcclxuXHJcbiAgICByZXF1ZXN0UGFnaW5hdGlvbk1vZGVsOiBSZXF1ZXN0UGFnaW5hdGlvbk1vZGVsID0ge1xyXG4gICAgICAgIHNraXBDb3VudDogMCxcclxuICAgICAgICBtZXJnZTogdHJ1ZVxyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNkcjogQ2hhbmdlRGV0ZWN0b3JSZWYsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHVzZXJQcmVmZXJlbmNlc1NlcnZpY2U6IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlc1NlcnZpY2VcclxuICAgICAgICAgICAgLnNlbGVjdChVc2VyUHJlZmVyZW5jZVZhbHVlcy5QYWdpbmF0aW9uU2l6ZSlcclxuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMub25EZXN0cm95JCkpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUoKHBhZ2VTaXplOiBudW1iZXIpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMucGFnZVNpemUgPSB0aGlzLnBhZ2VTaXplIHx8IHBhZ2VTaXplO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5yZXF1ZXN0UGFnaW5hdGlvbk1vZGVsLm1heEl0ZW1zID0gdGhpcy5wYWdlU2l6ZTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgb25Mb2FkTW9yZSgpIHtcclxuICAgICAgICB0aGlzLnJlcXVlc3RQYWdpbmF0aW9uTW9kZWwuc2tpcENvdW50ID0gMDtcclxuICAgICAgICB0aGlzLnJlcXVlc3RQYWdpbmF0aW9uTW9kZWwubWVyZ2UgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgdGhpcy5yZXF1ZXN0UGFnaW5hdGlvbk1vZGVsLm1heEl0ZW1zICs9IHRoaXMucGFnZVNpemU7XHJcblxyXG4gICAgICAgIHRoaXMubG9hZE1vcmUubmV4dCh0aGlzLnJlcXVlc3RQYWdpbmF0aW9uTW9kZWwpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5fdGFyZ2V0KSB7XHJcbiAgICAgICAgICAgIHRoaXMuaXNMb2FkaW5nID0gdHJ1ZTtcclxuICAgICAgICAgICAgdGhpcy5fdGFyZ2V0LnVwZGF0ZVBhZ2luYXRpb24oPFJlcXVlc3RQYWdpbmF0aW9uTW9kZWw+IHRoaXMucmVxdWVzdFBhZ2luYXRpb25Nb2RlbCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0KCkge1xyXG4gICAgICAgIHRoaXMucGFnaW5hdGlvbi5za2lwQ291bnQgPSAwO1xyXG4gICAgICAgIHRoaXMucGFnaW5hdGlvbi5tYXhJdGVtcyA9IHRoaXMucGFnZVNpemU7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLl90YXJnZXQpIHtcclxuICAgICAgICAgICAgdGhpcy5fdGFyZ2V0LnVwZGF0ZVBhZ2luYXRpb24odGhpcy5wYWdpbmF0aW9uKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAgICAgdGhpcy5vbkRlc3Ryb3kkLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5vbkRlc3Ryb3kkLmNvbXBsZXRlKCk7XHJcbiAgICB9XHJcbn1cclxuIl19