/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, ViewEncapsulation, ChangeDetectorRef, HostBinding } from '@angular/core';
import { Pagination } from '@alfresco/js-api';
import { Subject } from 'rxjs';
import { PaginationModel } from '../models/pagination.model';
import { UserPreferencesService, UserPreferenceValues } from '../services/user-preferences.service';
import { takeUntil } from 'rxjs/operators';
var PaginationComponent = /** @class */ (function () {
    function PaginationComponent(cdr, userPreferencesService) {
        this.cdr = cdr;
        this.userPreferencesService = userPreferencesService;
        /**
         * Pagination object.
         */
        this.pagination = PaginationComponent.DEFAULT_PAGINATION;
        /**
         * Emitted when pagination changes in any way.
         */
        this.change = new EventEmitter();
        /**
         * Emitted when the page number changes.
         */
        this.changePageNumber = new EventEmitter();
        /**
         * Emitted when the page size changes.
         */
        this.changePageSize = new EventEmitter();
        /**
         * Emitted when the next page is requested.
         */
        this.nextPage = new EventEmitter();
        /**
         * Emitted when the previous page is requested.
         */
        this.prevPage = new EventEmitter();
        this.onDestroy$ = new Subject();
    }
    /**
     * @return {?}
     */
    PaginationComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.userPreferencesService
            .select(UserPreferenceValues.PaginationSize)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} pagSize
         * @return {?}
         */
        function (pagSize) { return _this.pagination.maxItems = pagSize; }));
        if (!this.supportedPageSizes) {
            this.supportedPageSizes = this.userPreferencesService.supportedPageSizes;
        }
        if (this.target) {
            this.target.pagination
                .pipe(takeUntil(this.onDestroy$))
                .subscribe((/**
             * @param {?} pagination
             * @return {?}
             */
            function (pagination) {
                if (pagination.count === 0 && !_this.isFirstPage) {
                    _this.goPrevious();
                }
                _this.pagination = pagination;
                _this.cdr.detectChanges();
            }));
        }
        if (!this.pagination) {
            this.pagination = PaginationComponent.DEFAULT_PAGINATION;
        }
    };
    Object.defineProperty(PaginationComponent.prototype, "lastPage", {
        get: /**
         * @return {?}
         */
        function () {
            var _a = this.pagination, maxItems = _a.maxItems, totalItems = _a.totalItems;
            return (totalItems && maxItems)
                ? Math.ceil(totalItems / maxItems)
                : 1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationComponent.prototype, "current", {
        get: /**
         * @return {?}
         */
        function () {
            var _a = this.pagination, maxItems = _a.maxItems, skipCount = _a.skipCount;
            return (skipCount && maxItems)
                ? Math.floor(skipCount / maxItems) + 1
                : 1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationComponent.prototype, "isLastPage", {
        get: /**
         * @return {?}
         */
        function () {
            return this.current === this.lastPage;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationComponent.prototype, "isFirstPage", {
        get: /**
         * @return {?}
         */
        function () {
            return this.current === 1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationComponent.prototype, "next", {
        get: /**
         * @return {?}
         */
        function () {
            return this.isLastPage ? this.current : this.current + 1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationComponent.prototype, "previous", {
        get: /**
         * @return {?}
         */
        function () {
            return this.isFirstPage ? 1 : this.current - 1;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationComponent.prototype, "hasItems", {
        get: /**
         * @return {?}
         */
        function () {
            return this.pagination && this.pagination.count > 0;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationComponent.prototype, "isEmpty", {
        get: /**
         * @return {?}
         */
        function () {
            return !this.hasItems;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationComponent.prototype, "range", {
        get: /**
         * @return {?}
         */
        function () {
            var _a = this.pagination, skipCount = _a.skipCount, maxItems = _a.maxItems, totalItems = _a.totalItems;
            var isLastPage = this.isLastPage;
            /** @type {?} */
            var start = totalItems ? skipCount + 1 : 0;
            /** @type {?} */
            var end = isLastPage ? totalItems : skipCount + maxItems;
            return [start, end];
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(PaginationComponent.prototype, "pages", {
        get: /**
         * @return {?}
         */
        function () {
            return Array(this.lastPage)
                .fill('n')
                .map((/**
             * @param {?} item
             * @param {?} index
             * @return {?}
             */
            function (item, index) { return (index + 1); }));
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    PaginationComponent.prototype.goNext = /**
     * @return {?}
     */
    function () {
        if (this.hasItems) {
            /** @type {?} */
            var maxItems = this.pagination.maxItems;
            /** @type {?} */
            var skipCount = (this.next - 1) * maxItems;
            this.pagination.skipCount = skipCount;
            this.handlePaginationEvent(PaginationComponent.ACTIONS.NEXT_PAGE, {
                skipCount: skipCount,
                maxItems: maxItems
            });
        }
    };
    /**
     * @return {?}
     */
    PaginationComponent.prototype.goPrevious = /**
     * @return {?}
     */
    function () {
        if (this.hasItems) {
            /** @type {?} */
            var maxItems = this.pagination.maxItems;
            /** @type {?} */
            var skipCount = (this.previous - 1) * maxItems;
            this.pagination.skipCount = skipCount;
            this.handlePaginationEvent(PaginationComponent.ACTIONS.PREV_PAGE, {
                skipCount: skipCount,
                maxItems: maxItems
            });
        }
    };
    /**
     * @param {?} pageNumber
     * @return {?}
     */
    PaginationComponent.prototype.onChangePageNumber = /**
     * @param {?} pageNumber
     * @return {?}
     */
    function (pageNumber) {
        if (this.hasItems) {
            /** @type {?} */
            var maxItems = this.pagination.maxItems;
            /** @type {?} */
            var skipCount = (pageNumber - 1) * maxItems;
            this.pagination.skipCount = skipCount;
            this.handlePaginationEvent(PaginationComponent.ACTIONS.CHANGE_PAGE_NUMBER, {
                skipCount: skipCount,
                maxItems: maxItems
            });
        }
    };
    /**
     * @param {?} maxItems
     * @return {?}
     */
    PaginationComponent.prototype.onChangePageSize = /**
     * @param {?} maxItems
     * @return {?}
     */
    function (maxItems) {
        this.pagination.skipCount = 0;
        this.userPreferencesService.paginationSize = maxItems;
        this.handlePaginationEvent(PaginationComponent.ACTIONS.CHANGE_PAGE_SIZE, {
            skipCount: 0,
            maxItems: maxItems
        });
    };
    /**
     * @return {?}
     */
    PaginationComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    };
    /**
     * @param {?} action
     * @param {?} params
     * @return {?}
     */
    PaginationComponent.prototype.handlePaginationEvent = /**
     * @param {?} action
     * @param {?} params
     * @return {?}
     */
    function (action, params) {
        var _a = PaginationComponent.ACTIONS, NEXT_PAGE = _a.NEXT_PAGE, PREV_PAGE = _a.PREV_PAGE, CHANGE_PAGE_NUMBER = _a.CHANGE_PAGE_NUMBER, CHANGE_PAGE_SIZE = _a.CHANGE_PAGE_SIZE;
        var _b = this, change = _b.change, changePageNumber = _b.changePageNumber, changePageSize = _b.changePageSize, nextPage = _b.nextPage, prevPage = _b.prevPage, pagination = _b.pagination;
        /** @type {?} */
        var paginationModel = Object.assign({}, pagination, params);
        if (action === NEXT_PAGE) {
            nextPage.emit(paginationModel);
        }
        if (action === PREV_PAGE) {
            prevPage.emit(paginationModel);
        }
        if (action === CHANGE_PAGE_NUMBER) {
            changePageNumber.emit(paginationModel);
        }
        if (action === CHANGE_PAGE_SIZE) {
            changePageSize.emit(paginationModel);
        }
        change.emit(params);
        if (this.target) {
            this.target.updatePagination(params);
        }
    };
    PaginationComponent.DEFAULT_PAGINATION = new Pagination({
        skipCount: 0,
        maxItems: 25,
        totalItems: 0
    });
    PaginationComponent.ACTIONS = {
        NEXT_PAGE: 'NEXT_PAGE',
        PREV_PAGE: 'PREV_PAGE',
        CHANGE_PAGE_SIZE: 'CHANGE_PAGE_SIZE',
        CHANGE_PAGE_NUMBER: 'CHANGE_PAGE_NUMBER'
    };
    PaginationComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-pagination',
                    host: { 'class': 'adf-pagination' },
                    template: "<ng-container *ngIf=\"hasItems\">\r\n    <div class=\"adf-pagination__block adf-pagination__range-block\">\r\n        <span class=\"adf-pagination__range\">\r\n            {{\r\n                'CORE.PAGINATION.ITEMS_RANGE' | translate: {\r\n                    range: range.join('-'),\r\n                    total: pagination.totalItems\r\n                }\r\n            }}\r\n        </span>\r\n    </div>\r\n\r\n    <div class=\"adf-pagination__block adf-pagination__perpage-block\">\r\n        <span>\r\n            {{ 'CORE.PAGINATION.ITEMS_PER_PAGE' | translate }}\r\n        </span>\r\n\r\n        <span class=\"adf-pagination__max-items\">\r\n            {{ pagination.maxItems }}\r\n        </span>\r\n\r\n        <button\r\n            mat-icon-button\r\n            [attr.aria-label]=\"'CORE.PAGINATION.ARIA.ITEMS_PER_PAGE' | translate\"\r\n            [matMenuTriggerFor]=\"pageSizeMenu\">\r\n            <mat-icon>arrow_drop_down</mat-icon>\r\n        </button>\r\n\r\n        <mat-menu #pageSizeMenu=\"matMenu\" class=\"adf-pagination__page-selector\">\r\n            <button\r\n                mat-menu-item\r\n                *ngFor=\"let pageSize of supportedPageSizes\"\r\n                (click)=\"onChangePageSize(pageSize)\">\r\n                {{ pageSize }}\r\n            </button>\r\n        </mat-menu>\r\n    </div>\r\n\r\n    <div class=\"adf-pagination__block adf-pagination__actualinfo-block\">\r\n        <span class=\"adf-pagination__current-page\">\r\n            {{ 'CORE.PAGINATION.CURRENT_PAGE' | translate: { number: current } }}\r\n        </span>\r\n\r\n        <button\r\n            mat-icon-button\r\n            data-automation-id=\"page-selector\"\r\n            [attr.aria-label]=\"'CORE.PAGINATION.ARIA.CURRENT_PAGE' | translate\"\r\n            [matMenuTriggerFor]=\"pagesMenu\"\r\n            *ngIf=\"pages.length > 1\">\r\n            <mat-icon>arrow_drop_down</mat-icon>\r\n        </button>\r\n\r\n        <span class=\"adf-pagination__total-pages\">\r\n            {{ 'CORE.PAGINATION.TOTAL_PAGES' | translate: { total: pages.length } }}\r\n        </span>\r\n\r\n        <mat-menu #pagesMenu=\"matMenu\" class=\"adf-pagination__page-selector\">\r\n            <button\r\n                mat-menu-item\r\n                *ngFor=\"let pageNumber of pages\"\r\n                (click)=\"onChangePageNumber(pageNumber)\">\r\n                {{ pageNumber }}\r\n            </button>\r\n        </mat-menu>\r\n    </div>\r\n\r\n    <div class=\"adf-pagination__block adf-pagination__controls-block\">\r\n        <button\r\n            class=\"adf-pagination__previous-button\"\r\n            mat-icon-button\r\n            [attr.aria-label]=\"'CORE.PAGINATION.ARIA.PREVIOUS_PAGE' | translate\"\r\n            [disabled]=\"isFirstPage\"\r\n            (click)=\"goPrevious()\">\r\n            <mat-icon>keyboard_arrow_left</mat-icon>\r\n        </button>\r\n\r\n        <button\r\n            class=\"adf-pagination__next-button\"\r\n            mat-icon-button\r\n            [attr.aria-label]=\"'CORE.PAGINATION.ARIA.NEXT_PAGE' | translate\"\r\n            [disabled]=\"isLastPage\"\r\n            (click)=\"goNext()\">\r\n            <mat-icon>keyboard_arrow_right</mat-icon>\r\n        </button>\r\n    </div>\r\n</ng-container>\r\n",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    encapsulation: ViewEncapsulation.None,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    PaginationComponent.ctorParameters = function () { return [
        { type: ChangeDetectorRef },
        { type: UserPreferencesService }
    ]; };
    PaginationComponent.propDecorators = {
        target: [{ type: Input }],
        supportedPageSizes: [{ type: Input }],
        pagination: [{ type: Input }],
        change: [{ type: Output }],
        changePageNumber: [{ type: Output }],
        changePageSize: [{ type: Output }],
        nextPage: [{ type: Output }],
        prevPage: [{ type: Output }],
        isEmpty: [{ type: HostBinding, args: ['class.adf-pagination__empty',] }]
    };
    return PaginationComponent;
}());
export { PaginationComponent };
if (false) {
    /** @type {?} */
    PaginationComponent.DEFAULT_PAGINATION;
    /** @type {?} */
    PaginationComponent.ACTIONS;
    /**
     * Component that provides custom pagination support.
     * @type {?}
     */
    PaginationComponent.prototype.target;
    /**
     * An array of page sizes.
     * @type {?}
     */
    PaginationComponent.prototype.supportedPageSizes;
    /**
     * Pagination object.
     * @type {?}
     */
    PaginationComponent.prototype.pagination;
    /**
     * Emitted when pagination changes in any way.
     * @type {?}
     */
    PaginationComponent.prototype.change;
    /**
     * Emitted when the page number changes.
     * @type {?}
     */
    PaginationComponent.prototype.changePageNumber;
    /**
     * Emitted when the page size changes.
     * @type {?}
     */
    PaginationComponent.prototype.changePageSize;
    /**
     * Emitted when the next page is requested.
     * @type {?}
     */
    PaginationComponent.prototype.nextPage;
    /**
     * Emitted when the previous page is requested.
     * @type {?}
     */
    PaginationComponent.prototype.prevPage;
    /**
     * @type {?}
     * @private
     */
    PaginationComponent.prototype.onDestroy$;
    /**
     * @type {?}
     * @private
     */
    PaginationComponent.prototype.cdr;
    /**
     * @type {?}
     * @private
     */
    PaginationComponent.prototype.userPreferencesService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnaW5hdGlvbi5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJwYWdpbmF0aW9uL3BhZ2luYXRpb24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFDSCx1QkFBdUIsRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUFFLEtBQUssRUFBVSxNQUFNLEVBQUUsaUJBQWlCLEVBQzFGLGlCQUFpQixFQUFhLFdBQVcsRUFDNUMsTUFBTSxlQUFlLENBQUM7QUFFdkIsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGtCQUFrQixDQUFDO0FBRzlDLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQzdELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ3BHLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUUzQztJQXlESSw2QkFBb0IsR0FBc0IsRUFBVSxzQkFBOEM7UUFBOUUsUUFBRyxHQUFILEdBQUcsQ0FBbUI7UUFBVSwyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCOzs7O1FBeEJsRyxlQUFVLEdBQW9CLG1CQUFtQixDQUFDLGtCQUFrQixDQUFDOzs7O1FBSXJFLFdBQU0sR0FBa0MsSUFBSSxZQUFZLEVBQW1CLENBQUM7Ozs7UUFJNUUscUJBQWdCLEdBQWtDLElBQUksWUFBWSxFQUFtQixDQUFDOzs7O1FBSXRGLG1CQUFjLEdBQWtDLElBQUksWUFBWSxFQUFtQixDQUFDOzs7O1FBSXBGLGFBQVEsR0FBa0MsSUFBSSxZQUFZLEVBQW1CLENBQUM7Ozs7UUFJOUUsYUFBUSxHQUFrQyxJQUFJLFlBQVksRUFBbUIsQ0FBQztRQUV0RSxlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztJQUc1QyxDQUFDOzs7O0lBRUQsc0NBQVE7OztJQUFSO1FBQUEsaUJBMEJDO1FBekJHLElBQUksQ0FBQyxzQkFBc0I7YUFDdEIsTUFBTSxDQUFDLG9CQUFvQixDQUFDLGNBQWMsQ0FBQzthQUMzQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQzthQUNoQyxTQUFTOzs7O1FBQUMsVUFBQSxPQUFPLElBQUksT0FBQSxLQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsR0FBRyxPQUFPLEVBQWxDLENBQWtDLEVBQUMsQ0FBQztRQUU5RCxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixFQUFFO1lBQzFCLElBQUksQ0FBQyxrQkFBa0IsR0FBRyxJQUFJLENBQUMsc0JBQXNCLENBQUMsa0JBQWtCLENBQUM7U0FDNUU7UUFFRCxJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDYixJQUFJLENBQUMsTUFBTSxDQUFDLFVBQVU7aUJBQ2pCLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2lCQUNoQyxTQUFTOzs7O1lBQUMsVUFBQSxVQUFVO2dCQUNqQixJQUFJLFVBQVUsQ0FBQyxLQUFLLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSSxDQUFDLFdBQVcsRUFBRTtvQkFDN0MsS0FBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO2lCQUNyQjtnQkFFRCxLQUFJLENBQUMsVUFBVSxHQUFHLFVBQVUsQ0FBQztnQkFDN0IsS0FBSSxDQUFDLEdBQUcsQ0FBQyxhQUFhLEVBQUUsQ0FBQztZQUM3QixDQUFDLEVBQUMsQ0FBQztTQUNWO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFVBQVUsR0FBRyxtQkFBbUIsQ0FBQyxrQkFBa0IsQ0FBQztTQUM1RDtJQUNMLENBQUM7SUFFRCxzQkFBSSx5Q0FBUTs7OztRQUFaO1lBQ1UsSUFBQSxvQkFBMEMsRUFBeEMsc0JBQVEsRUFBRSwwQkFBOEI7WUFFaEQsT0FBTyxDQUFDLFVBQVUsSUFBSSxRQUFRLENBQUM7Z0JBQzNCLENBQUMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsR0FBRyxRQUFRLENBQUM7Z0JBQ2xDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDWixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHdDQUFPOzs7O1FBQVg7WUFDVSxJQUFBLG9CQUF5QyxFQUF2QyxzQkFBUSxFQUFFLHdCQUE2QjtZQUUvQyxPQUFPLENBQUMsU0FBUyxJQUFJLFFBQVEsQ0FBQztnQkFDMUIsQ0FBQyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUyxHQUFHLFFBQVEsQ0FBQyxHQUFHLENBQUM7Z0JBQ3RDLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDWixDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDJDQUFVOzs7O1FBQWQ7WUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLEtBQUssSUFBSSxDQUFDLFFBQVEsQ0FBQztRQUMxQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDRDQUFXOzs7O1FBQWY7WUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLEtBQUssQ0FBQyxDQUFDO1FBQzlCLENBQUM7OztPQUFBO0lBRUQsc0JBQUkscUNBQUk7Ozs7UUFBUjtZQUNJLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDN0QsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSx5Q0FBUTs7OztRQUFaO1lBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxPQUFPLEdBQUcsQ0FBQyxDQUFDO1FBQ25ELENBQUM7OztPQUFBO0lBRUQsc0JBQUkseUNBQVE7Ozs7UUFBWjtZQUNJLE9BQU8sSUFBSSxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUM7UUFDeEQsQ0FBQzs7O09BQUE7SUFFRCxzQkFDSSx3Q0FBTzs7OztRQURYO1lBRUksT0FBTyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDMUIsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSxzQ0FBSzs7OztRQUFUO1lBQ1UsSUFBQSxvQkFBcUQsRUFBbkQsd0JBQVMsRUFBRSxzQkFBUSxFQUFFLDBCQUE4QjtZQUNuRCxJQUFBLDRCQUFVOztnQkFFWixLQUFLLEdBQUcsVUFBVSxDQUFDLENBQUMsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDOztnQkFDdEMsR0FBRyxHQUFHLFVBQVUsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxTQUFTLEdBQUcsUUFBUTtZQUUxRCxPQUFPLENBQUMsS0FBSyxFQUFFLEdBQUcsQ0FBQyxDQUFDO1FBQ3hCLENBQUM7OztPQUFBO0lBRUQsc0JBQUksc0NBQUs7Ozs7UUFBVDtZQUNJLE9BQU8sS0FBSyxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7aUJBQ3RCLElBQUksQ0FBQyxHQUFHLENBQUM7aUJBQ1QsR0FBRzs7Ozs7WUFBQyxVQUFDLElBQUksRUFBRSxLQUFLLElBQUssT0FBQSxDQUFDLEtBQUssR0FBRyxDQUFDLENBQUMsRUFBWCxDQUFXLEVBQUMsQ0FBQztRQUMzQyxDQUFDOzs7T0FBQTs7OztJQUVELG9DQUFNOzs7SUFBTjtRQUNJLElBQUksSUFBSSxDQUFDLFFBQVEsRUFBRTs7Z0JBQ1QsUUFBUSxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsUUFBUTs7Z0JBQ25DLFNBQVMsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDLEdBQUcsUUFBUTtZQUM1QyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7WUFFdEMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxTQUFTLEVBQUU7Z0JBQzlELFNBQVMsV0FBQTtnQkFDVCxRQUFRLFVBQUE7YUFDWCxDQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7Ozs7SUFFRCx3Q0FBVTs7O0lBQVY7UUFDSSxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7O2dCQUNULFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVE7O2dCQUNuQyxTQUFTLEdBQUcsQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLENBQUMsQ0FBQyxHQUFHLFFBQVE7WUFDaEQsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsU0FBUyxDQUFDO1lBRXRDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsU0FBUyxFQUFFO2dCQUM5RCxTQUFTLFdBQUE7Z0JBQ1QsUUFBUSxVQUFBO2FBQ1gsQ0FBQyxDQUFDO1NBQ047SUFDTCxDQUFDOzs7OztJQUVELGdEQUFrQjs7OztJQUFsQixVQUFtQixVQUFrQjtRQUNqQyxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7O2dCQUNULFFBQVEsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVE7O2dCQUNuQyxTQUFTLEdBQUcsQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDLEdBQUcsUUFBUTtZQUM3QyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7WUFFdEMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsRUFBRTtnQkFDdkUsU0FBUyxXQUFBO2dCQUNULFFBQVEsVUFBQTthQUNYLENBQUMsQ0FBQztTQUNOO0lBQ0wsQ0FBQzs7Ozs7SUFFRCw4Q0FBZ0I7Ozs7SUFBaEIsVUFBaUIsUUFBZ0I7UUFDN0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxTQUFTLEdBQUcsQ0FBQyxDQUFDO1FBQzlCLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxjQUFjLEdBQUcsUUFBUSxDQUFDO1FBQ3RELElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxtQkFBbUIsQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLEVBQUU7WUFDckUsU0FBUyxFQUFFLENBQUM7WUFDWixRQUFRLFVBQUE7U0FDWCxDQUFDLENBQUM7SUFDUCxDQUFDOzs7O0lBRUQseUNBQVc7OztJQUFYO1FBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDOzs7Ozs7SUFFRCxtREFBcUI7Ozs7O0lBQXJCLFVBQXNCLE1BQWMsRUFBRSxNQUF1QjtRQUNuRCxJQUFBLGdDQUt5QixFQUozQix3QkFBUyxFQUNULHdCQUFTLEVBQ1QsMENBQWtCLEVBQ2xCLHNDQUMyQjtRQUV6QixJQUFBLFNBT0UsRUFOSixrQkFBTSxFQUNOLHNDQUFnQixFQUNoQixrQ0FBYyxFQUNkLHNCQUFRLEVBQ1Isc0JBQVEsRUFDUiwwQkFDSTs7WUFFRixlQUFlLEdBQW9CLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLFVBQVUsRUFBRSxNQUFNLENBQUM7UUFFOUUsSUFBSSxNQUFNLEtBQUssU0FBUyxFQUFFO1lBQ3RCLFFBQVEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDbEM7UUFFRCxJQUFJLE1BQU0sS0FBSyxTQUFTLEVBQUU7WUFDdEIsUUFBUSxDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUNsQztRQUVELElBQUksTUFBTSxLQUFLLGtCQUFrQixFQUFFO1lBQy9CLGdCQUFnQixDQUFDLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQztTQUMxQztRQUVELElBQUksTUFBTSxLQUFLLGdCQUFnQixFQUFFO1lBQzdCLGNBQWMsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7U0FDeEM7UUFFRCxNQUFNLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRXBCLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtZQUNiLElBQUksQ0FBQyxNQUFNLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDeEM7SUFDTCxDQUFDO0lBcE9NLHNDQUFrQixHQUFlLElBQUksVUFBVSxDQUFDO1FBQ25ELFNBQVMsRUFBRSxDQUFDO1FBQ1osUUFBUSxFQUFFLEVBQUU7UUFDWixVQUFVLEVBQUUsQ0FBQztLQUNoQixDQUFDLENBQUM7SUFFSSwyQkFBTyxHQUFHO1FBQ2IsU0FBUyxFQUFFLFdBQVc7UUFDdEIsU0FBUyxFQUFFLFdBQVc7UUFDdEIsZ0JBQWdCLEVBQUUsa0JBQWtCO1FBQ3BDLGtCQUFrQixFQUFFLG9CQUFvQjtLQUMzQyxDQUFDOztnQkFyQkwsU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxnQkFBZ0I7b0JBQzFCLElBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRTtvQkFDbkMsaXVHQUEwQztvQkFFMUMsZUFBZSxFQUFFLHVCQUF1QixDQUFDLE1BQU07b0JBQy9DLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOztpQkFDeEM7Ozs7Z0JBbEJHLGlCQUFpQjtnQkFRWixzQkFBc0I7Ozt5QkEyQjFCLEtBQUs7cUNBSUwsS0FBSzs2QkFJTCxLQUFLO3lCQUlMLE1BQU07bUNBSU4sTUFBTTtpQ0FJTixNQUFNOzJCQUlOLE1BQU07MkJBSU4sTUFBTTswQkF3RU4sV0FBVyxTQUFDLDZCQUE2Qjs7SUFtSDlDLDBCQUFDO0NBQUEsQUEvT0QsSUErT0M7U0F2T1ksbUJBQW1COzs7SUFFNUIsdUNBSUc7O0lBRUgsNEJBS0U7Ozs7O0lBR0YscUNBQzJCOzs7OztJQUczQixpREFDNkI7Ozs7O0lBRzdCLHlDQUNxRTs7Ozs7SUFHckUscUNBQzRFOzs7OztJQUc1RSwrQ0FDc0Y7Ozs7O0lBR3RGLDZDQUNvRjs7Ozs7SUFHcEYsdUNBQzhFOzs7OztJQUc5RSx1Q0FDOEU7Ozs7O0lBRTlFLHlDQUE0Qzs7Ozs7SUFFaEMsa0NBQThCOzs7OztJQUFFLHFEQUFzRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQge1xyXG4gICAgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksIENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBJbnB1dCwgT25Jbml0LCBPdXRwdXQsIFZpZXdFbmNhcHN1bGF0aW9uLFxyXG4gICAgQ2hhbmdlRGV0ZWN0b3JSZWYsIE9uRGVzdHJveSwgSG9zdEJpbmRpbmdcclxufSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbmltcG9ydCB7IFBhZ2luYXRpb24gfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuaW1wb3J0IHsgUGFnaW5hdGVkQ29tcG9uZW50IH0gZnJvbSAnLi9wYWdpbmF0ZWQtY29tcG9uZW50LmludGVyZmFjZSc7XHJcbmltcG9ydCB7IFBhZ2luYXRpb25Db21wb25lbnRJbnRlcmZhY2UgfSBmcm9tICcuL3BhZ2luYXRpb24tY29tcG9uZW50LmludGVyZmFjZSc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgUGFnaW5hdGlvbk1vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL3BhZ2luYXRpb24ubW9kZWwnO1xyXG5pbXBvcnQgeyBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlLCBVc2VyUHJlZmVyZW5jZVZhbHVlcyB9IGZyb20gJy4uL3NlcnZpY2VzL3VzZXItcHJlZmVyZW5jZXMuc2VydmljZSc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtcGFnaW5hdGlvbicsXHJcbiAgICBob3N0OiB7ICdjbGFzcyc6ICdhZGYtcGFnaW5hdGlvbicgfSxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9wYWdpbmF0aW9uLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL3BhZ2luYXRpb24uY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUGFnaW5hdGlvbkNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25EZXN0cm95LCBQYWdpbmF0aW9uQ29tcG9uZW50SW50ZXJmYWNlIHtcclxuXHJcbiAgICBzdGF0aWMgREVGQVVMVF9QQUdJTkFUSU9OOiBQYWdpbmF0aW9uID0gbmV3IFBhZ2luYXRpb24oe1xyXG4gICAgICAgIHNraXBDb3VudDogMCxcclxuICAgICAgICBtYXhJdGVtczogMjUsXHJcbiAgICAgICAgdG90YWxJdGVtczogMFxyXG4gICAgfSk7XHJcblxyXG4gICAgc3RhdGljIEFDVElPTlMgPSB7XHJcbiAgICAgICAgTkVYVF9QQUdFOiAnTkVYVF9QQUdFJyxcclxuICAgICAgICBQUkVWX1BBR0U6ICdQUkVWX1BBR0UnLFxyXG4gICAgICAgIENIQU5HRV9QQUdFX1NJWkU6ICdDSEFOR0VfUEFHRV9TSVpFJyxcclxuICAgICAgICBDSEFOR0VfUEFHRV9OVU1CRVI6ICdDSEFOR0VfUEFHRV9OVU1CRVInXHJcbiAgICB9O1xyXG5cclxuICAgIC8qKiBDb21wb25lbnQgdGhhdCBwcm92aWRlcyBjdXN0b20gcGFnaW5hdGlvbiBzdXBwb3J0LiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHRhcmdldDogUGFnaW5hdGVkQ29tcG9uZW50O1xyXG5cclxuICAgIC8qKiBBbiBhcnJheSBvZiBwYWdlIHNpemVzLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHN1cHBvcnRlZFBhZ2VTaXplczogbnVtYmVyW107XHJcblxyXG4gICAgLyoqIFBhZ2luYXRpb24gb2JqZWN0LiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHBhZ2luYXRpb246IFBhZ2luYXRpb25Nb2RlbCA9IFBhZ2luYXRpb25Db21wb25lbnQuREVGQVVMVF9QQUdJTkFUSU9OO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gcGFnaW5hdGlvbiBjaGFuZ2VzIGluIGFueSB3YXkuICovXHJcbiAgICBAT3V0cHV0KClcclxuICAgIGNoYW5nZTogRXZlbnRFbWl0dGVyPFBhZ2luYXRpb25Nb2RlbD4gPSBuZXcgRXZlbnRFbWl0dGVyPFBhZ2luYXRpb25Nb2RlbD4oKTtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSBwYWdlIG51bWJlciBjaGFuZ2VzLiAqL1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICBjaGFuZ2VQYWdlTnVtYmVyOiBFdmVudEVtaXR0ZXI8UGFnaW5hdGlvbk1vZGVsPiA9IG5ldyBFdmVudEVtaXR0ZXI8UGFnaW5hdGlvbk1vZGVsPigpO1xyXG5cclxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIHBhZ2Ugc2l6ZSBjaGFuZ2VzLiAqL1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICBjaGFuZ2VQYWdlU2l6ZTogRXZlbnRFbWl0dGVyPFBhZ2luYXRpb25Nb2RlbD4gPSBuZXcgRXZlbnRFbWl0dGVyPFBhZ2luYXRpb25Nb2RlbD4oKTtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSBuZXh0IHBhZ2UgaXMgcmVxdWVzdGVkLiAqL1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICBuZXh0UGFnZTogRXZlbnRFbWl0dGVyPFBhZ2luYXRpb25Nb2RlbD4gPSBuZXcgRXZlbnRFbWl0dGVyPFBhZ2luYXRpb25Nb2RlbD4oKTtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSBwcmV2aW91cyBwYWdlIGlzIHJlcXVlc3RlZC4gKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgcHJldlBhZ2U6IEV2ZW50RW1pdHRlcjxQYWdpbmF0aW9uTW9kZWw+ID0gbmV3IEV2ZW50RW1pdHRlcjxQYWdpbmF0aW9uTW9kZWw+KCk7XHJcblxyXG4gICAgcHJpdmF0ZSBvbkRlc3Ryb3kkID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNkcjogQ2hhbmdlRGV0ZWN0b3JSZWYsIHByaXZhdGUgdXNlclByZWZlcmVuY2VzU2VydmljZTogVXNlclByZWZlcmVuY2VzU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25Jbml0KCkge1xyXG4gICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VzU2VydmljZVxyXG4gICAgICAgICAgICAuc2VsZWN0KFVzZXJQcmVmZXJlbmNlVmFsdWVzLlBhZ2luYXRpb25TaXplKVxyXG4gICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5vbkRlc3Ryb3kkKSlcclxuICAgICAgICAgICAgLnN1YnNjcmliZShwYWdTaXplID0+IHRoaXMucGFnaW5hdGlvbi5tYXhJdGVtcyA9IHBhZ1NpemUpO1xyXG5cclxuICAgICAgICBpZiAoIXRoaXMuc3VwcG9ydGVkUGFnZVNpemVzKSB7XHJcbiAgICAgICAgICAgIHRoaXMuc3VwcG9ydGVkUGFnZVNpemVzID0gdGhpcy51c2VyUHJlZmVyZW5jZXNTZXJ2aWNlLnN1cHBvcnRlZFBhZ2VTaXplcztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnRhcmdldCkge1xyXG4gICAgICAgICAgICB0aGlzLnRhcmdldC5wYWdpbmF0aW9uXHJcbiAgICAgICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5vbkRlc3Ryb3kkKSlcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUocGFnaW5hdGlvbiA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKHBhZ2luYXRpb24uY291bnQgPT09IDAgJiYgIXRoaXMuaXNGaXJzdFBhZ2UpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5nb1ByZXZpb3VzKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgICAgICB0aGlzLnBhZ2luYXRpb24gPSBwYWdpbmF0aW9uO1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMuY2RyLmRldGVjdENoYW5nZXMoKTtcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLnBhZ2luYXRpb24pIHtcclxuICAgICAgICAgICAgdGhpcy5wYWdpbmF0aW9uID0gUGFnaW5hdGlvbkNvbXBvbmVudC5ERUZBVUxUX1BBR0lOQVRJT047XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldCBsYXN0UGFnZSgpOiBudW1iZXIge1xyXG4gICAgICAgIGNvbnN0IHsgbWF4SXRlbXMsIHRvdGFsSXRlbXMgfSA9IHRoaXMucGFnaW5hdGlvbjtcclxuXHJcbiAgICAgICAgcmV0dXJuICh0b3RhbEl0ZW1zICYmIG1heEl0ZW1zKVxyXG4gICAgICAgICAgICA/IE1hdGguY2VpbCh0b3RhbEl0ZW1zIC8gbWF4SXRlbXMpXHJcbiAgICAgICAgICAgIDogMTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgY3VycmVudCgpOiBudW1iZXIge1xyXG4gICAgICAgIGNvbnN0IHsgbWF4SXRlbXMsIHNraXBDb3VudCB9ID0gdGhpcy5wYWdpbmF0aW9uO1xyXG5cclxuICAgICAgICByZXR1cm4gKHNraXBDb3VudCAmJiBtYXhJdGVtcylcclxuICAgICAgICAgICAgPyBNYXRoLmZsb29yKHNraXBDb3VudCAvIG1heEl0ZW1zKSArIDFcclxuICAgICAgICAgICAgOiAxO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBpc0xhc3RQYWdlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnQgPT09IHRoaXMubGFzdFBhZ2U7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGlzRmlyc3RQYWdlKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmN1cnJlbnQgPT09IDE7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IG5leHQoKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pc0xhc3RQYWdlID8gdGhpcy5jdXJyZW50IDogdGhpcy5jdXJyZW50ICsgMTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgcHJldmlvdXMoKTogbnVtYmVyIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5pc0ZpcnN0UGFnZSA/IDEgOiB0aGlzLmN1cnJlbnQgLSAxO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBoYXNJdGVtcygpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wYWdpbmF0aW9uICYmIHRoaXMucGFnaW5hdGlvbi5jb3VudCA+IDA7XHJcbiAgICB9XHJcblxyXG4gICAgQEhvc3RCaW5kaW5nKCdjbGFzcy5hZGYtcGFnaW5hdGlvbl9fZW1wdHknKVxyXG4gICAgZ2V0IGlzRW1wdHkoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuICF0aGlzLmhhc0l0ZW1zO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCByYW5nZSgpOiBudW1iZXJbXSB7XHJcbiAgICAgICAgY29uc3QgeyBza2lwQ291bnQsIG1heEl0ZW1zLCB0b3RhbEl0ZW1zIH0gPSB0aGlzLnBhZ2luYXRpb247XHJcbiAgICAgICAgY29uc3QgeyBpc0xhc3RQYWdlIH0gPSB0aGlzO1xyXG5cclxuICAgICAgICBjb25zdCBzdGFydCA9IHRvdGFsSXRlbXMgPyBza2lwQ291bnQgKyAxIDogMDtcclxuICAgICAgICBjb25zdCBlbmQgPSBpc0xhc3RQYWdlID8gdG90YWxJdGVtcyA6IHNraXBDb3VudCArIG1heEl0ZW1zO1xyXG5cclxuICAgICAgICByZXR1cm4gW3N0YXJ0LCBlbmRdO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBwYWdlcygpOiBudW1iZXJbXSB7XHJcbiAgICAgICAgcmV0dXJuIEFycmF5KHRoaXMubGFzdFBhZ2UpXHJcbiAgICAgICAgICAgIC5maWxsKCduJylcclxuICAgICAgICAgICAgLm1hcCgoaXRlbSwgaW5kZXgpID0+IChpbmRleCArIDEpKTtcclxuICAgIH1cclxuXHJcbiAgICBnb05leHQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaGFzSXRlbXMpIHtcclxuICAgICAgICAgICAgY29uc3QgbWF4SXRlbXMgPSB0aGlzLnBhZ2luYXRpb24ubWF4SXRlbXM7XHJcbiAgICAgICAgICAgIGNvbnN0IHNraXBDb3VudCA9ICh0aGlzLm5leHQgLSAxKSAqIG1heEl0ZW1zO1xyXG4gICAgICAgICAgICB0aGlzLnBhZ2luYXRpb24uc2tpcENvdW50ID0gc2tpcENvdW50O1xyXG5cclxuICAgICAgICAgICAgdGhpcy5oYW5kbGVQYWdpbmF0aW9uRXZlbnQoUGFnaW5hdGlvbkNvbXBvbmVudC5BQ1RJT05TLk5FWFRfUEFHRSwge1xyXG4gICAgICAgICAgICAgICAgc2tpcENvdW50LFxyXG4gICAgICAgICAgICAgICAgbWF4SXRlbXNcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdvUHJldmlvdXMoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaGFzSXRlbXMpIHtcclxuICAgICAgICAgICAgY29uc3QgbWF4SXRlbXMgPSB0aGlzLnBhZ2luYXRpb24ubWF4SXRlbXM7XHJcbiAgICAgICAgICAgIGNvbnN0IHNraXBDb3VudCA9ICh0aGlzLnByZXZpb3VzIC0gMSkgKiBtYXhJdGVtcztcclxuICAgICAgICAgICAgdGhpcy5wYWdpbmF0aW9uLnNraXBDb3VudCA9IHNraXBDb3VudDtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuaGFuZGxlUGFnaW5hdGlvbkV2ZW50KFBhZ2luYXRpb25Db21wb25lbnQuQUNUSU9OUy5QUkVWX1BBR0UsIHtcclxuICAgICAgICAgICAgICAgIHNraXBDb3VudCxcclxuICAgICAgICAgICAgICAgIG1heEl0ZW1zXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvbkNoYW5nZVBhZ2VOdW1iZXIocGFnZU51bWJlcjogbnVtYmVyKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaGFzSXRlbXMpIHtcclxuICAgICAgICAgICAgY29uc3QgbWF4SXRlbXMgPSB0aGlzLnBhZ2luYXRpb24ubWF4SXRlbXM7XHJcbiAgICAgICAgICAgIGNvbnN0IHNraXBDb3VudCA9IChwYWdlTnVtYmVyIC0gMSkgKiBtYXhJdGVtcztcclxuICAgICAgICAgICAgdGhpcy5wYWdpbmF0aW9uLnNraXBDb3VudCA9IHNraXBDb3VudDtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuaGFuZGxlUGFnaW5hdGlvbkV2ZW50KFBhZ2luYXRpb25Db21wb25lbnQuQUNUSU9OUy5DSEFOR0VfUEFHRV9OVU1CRVIsIHtcclxuICAgICAgICAgICAgICAgIHNraXBDb3VudCxcclxuICAgICAgICAgICAgICAgIG1heEl0ZW1zXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvbkNoYW5nZVBhZ2VTaXplKG1heEl0ZW1zOiBudW1iZXIpIHtcclxuICAgICAgICB0aGlzLnBhZ2luYXRpb24uc2tpcENvdW50ID0gMDtcclxuICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlc1NlcnZpY2UucGFnaW5hdGlvblNpemUgPSBtYXhJdGVtcztcclxuICAgICAgICB0aGlzLmhhbmRsZVBhZ2luYXRpb25FdmVudChQYWdpbmF0aW9uQ29tcG9uZW50LkFDVElPTlMuQ0hBTkdFX1BBR0VfU0laRSwge1xyXG4gICAgICAgICAgICBza2lwQ291bnQ6IDAsXHJcbiAgICAgICAgICAgIG1heEl0ZW1zXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAgICAgdGhpcy5vbkRlc3Ryb3kkLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5vbkRlc3Ryb3kkLmNvbXBsZXRlKCk7XHJcbiAgICB9XHJcblxyXG4gICAgaGFuZGxlUGFnaW5hdGlvbkV2ZW50KGFjdGlvbjogc3RyaW5nLCBwYXJhbXM6IFBhZ2luYXRpb25Nb2RlbCkge1xyXG4gICAgICAgIGNvbnN0IHtcclxuICAgICAgICAgICAgTkVYVF9QQUdFLFxyXG4gICAgICAgICAgICBQUkVWX1BBR0UsXHJcbiAgICAgICAgICAgIENIQU5HRV9QQUdFX05VTUJFUixcclxuICAgICAgICAgICAgQ0hBTkdFX1BBR0VfU0laRVxyXG4gICAgICAgIH0gPSBQYWdpbmF0aW9uQ29tcG9uZW50LkFDVElPTlM7XHJcblxyXG4gICAgICAgIGNvbnN0IHtcclxuICAgICAgICAgICAgY2hhbmdlLFxyXG4gICAgICAgICAgICBjaGFuZ2VQYWdlTnVtYmVyLFxyXG4gICAgICAgICAgICBjaGFuZ2VQYWdlU2l6ZSxcclxuICAgICAgICAgICAgbmV4dFBhZ2UsXHJcbiAgICAgICAgICAgIHByZXZQYWdlLFxyXG4gICAgICAgICAgICBwYWdpbmF0aW9uXHJcbiAgICAgICAgfSA9IHRoaXM7XHJcblxyXG4gICAgICAgIGNvbnN0IHBhZ2luYXRpb25Nb2RlbDogUGFnaW5hdGlvbk1vZGVsID0gT2JqZWN0LmFzc2lnbih7fSwgcGFnaW5hdGlvbiwgcGFyYW1zKTtcclxuXHJcbiAgICAgICAgaWYgKGFjdGlvbiA9PT0gTkVYVF9QQUdFKSB7XHJcbiAgICAgICAgICAgIG5leHRQYWdlLmVtaXQocGFnaW5hdGlvbk1vZGVsKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChhY3Rpb24gPT09IFBSRVZfUEFHRSkge1xyXG4gICAgICAgICAgICBwcmV2UGFnZS5lbWl0KHBhZ2luYXRpb25Nb2RlbCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoYWN0aW9uID09PSBDSEFOR0VfUEFHRV9OVU1CRVIpIHtcclxuICAgICAgICAgICAgY2hhbmdlUGFnZU51bWJlci5lbWl0KHBhZ2luYXRpb25Nb2RlbCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoYWN0aW9uID09PSBDSEFOR0VfUEFHRV9TSVpFKSB7XHJcbiAgICAgICAgICAgIGNoYW5nZVBhZ2VTaXplLmVtaXQocGFnaW5hdGlvbk1vZGVsKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNoYW5nZS5lbWl0KHBhcmFtcyk7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnRhcmdldCkge1xyXG4gICAgICAgICAgICB0aGlzLnRhcmdldC51cGRhdGVQYWdpbmF0aW9uKHBhcmFtcyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG59XHJcbiJdfQ==