/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DecimalPipe } from '@angular/common';
import { Pipe } from '@angular/core';
import { AppConfigService } from '../app-config/app-config.service';
import { UserPreferencesService, UserPreferenceValues } from '../services/user-preferences.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
var DecimalNumberPipe = /** @class */ (function () {
    function DecimalNumberPipe(userPreferenceService, appConfig) {
        var _this = this;
        this.userPreferenceService = userPreferenceService;
        this.appConfig = appConfig;
        this.defaultLocale = DecimalNumberPipe.DEFAULT_LOCALE;
        this.defaultMinIntegerDigits = DecimalNumberPipe.DEFAULT_MIN_INTEGER_DIGITS;
        this.defaultMinFractionDigits = DecimalNumberPipe.DEFAULT_MIN_FRACTION_DIGITS;
        this.defaultMaxFractionDigits = DecimalNumberPipe.DEFAULT_MAX_FRACTION_DIGITS;
        this.onDestroy$ = new Subject();
        if (this.userPreferenceService) {
            this.userPreferenceService.select(UserPreferenceValues.Locale)
                .pipe(takeUntil(this.onDestroy$))
                .subscribe((/**
             * @param {?} locale
             * @return {?}
             */
            function (locale) {
                if (locale) {
                    _this.defaultLocale = locale;
                }
            }));
        }
        if (this.appConfig) {
            this.defaultMinIntegerDigits = this.appConfig.get('decimalValues.minIntegerDigits', DecimalNumberPipe.DEFAULT_MIN_INTEGER_DIGITS);
            this.defaultMinFractionDigits = this.appConfig.get('decimalValues.minFractionDigits', DecimalNumberPipe.DEFAULT_MIN_FRACTION_DIGITS);
            this.defaultMaxFractionDigits = this.appConfig.get('decimalValues.maxFractionDigits', DecimalNumberPipe.DEFAULT_MAX_FRACTION_DIGITS);
        }
    }
    /**
     * @param {?} value
     * @param {?=} digitsInfo
     * @param {?=} locale
     * @return {?}
     */
    DecimalNumberPipe.prototype.transform = /**
     * @param {?} value
     * @param {?=} digitsInfo
     * @param {?=} locale
     * @return {?}
     */
    function (value, digitsInfo, locale) {
        /** @type {?} */
        var actualMinIntegerDigits = digitsInfo && digitsInfo.minIntegerDigits ? digitsInfo.minIntegerDigits : this.defaultMinIntegerDigits;
        /** @type {?} */
        var actualMinFractionDigits = digitsInfo && digitsInfo.minFractionDigits ? digitsInfo.minFractionDigits : this.defaultMinFractionDigits;
        /** @type {?} */
        var actualMaxFractionDigits = digitsInfo && digitsInfo.maxFractionDigits ? digitsInfo.maxFractionDigits : this.defaultMaxFractionDigits;
        /** @type {?} */
        var actualDigitsInfo = actualMinIntegerDigits + "." + actualMinFractionDigits + "-" + actualMaxFractionDigits;
        /** @type {?} */
        var actualLocale = locale || this.defaultLocale;
        /** @type {?} */
        var datePipe = new DecimalPipe(actualLocale);
        return datePipe.transform(value, actualDigitsInfo);
    };
    /**
     * @return {?}
     */
    DecimalNumberPipe.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    };
    DecimalNumberPipe.DEFAULT_LOCALE = 'en-US';
    DecimalNumberPipe.DEFAULT_MIN_INTEGER_DIGITS = 1;
    DecimalNumberPipe.DEFAULT_MIN_FRACTION_DIGITS = 0;
    DecimalNumberPipe.DEFAULT_MAX_FRACTION_DIGITS = 2;
    DecimalNumberPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'adfDecimalNumber',
                    pure: false
                },] }
    ];
    /** @nocollapse */
    DecimalNumberPipe.ctorParameters = function () { return [
        { type: UserPreferencesService },
        { type: AppConfigService }
    ]; };
    return DecimalNumberPipe;
}());
export { DecimalNumberPipe };
if (false) {
    /** @type {?} */
    DecimalNumberPipe.DEFAULT_LOCALE;
    /** @type {?} */
    DecimalNumberPipe.DEFAULT_MIN_INTEGER_DIGITS;
    /** @type {?} */
    DecimalNumberPipe.DEFAULT_MIN_FRACTION_DIGITS;
    /** @type {?} */
    DecimalNumberPipe.DEFAULT_MAX_FRACTION_DIGITS;
    /** @type {?} */
    DecimalNumberPipe.prototype.defaultLocale;
    /** @type {?} */
    DecimalNumberPipe.prototype.defaultMinIntegerDigits;
    /** @type {?} */
    DecimalNumberPipe.prototype.defaultMinFractionDigits;
    /** @type {?} */
    DecimalNumberPipe.prototype.defaultMaxFractionDigits;
    /** @type {?} */
    DecimalNumberPipe.prototype.onDestroy$;
    /** @type {?} */
    DecimalNumberPipe.prototype.userPreferenceService;
    /** @type {?} */
    DecimalNumberPipe.prototype.appConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVjaW1hbC1udW1iZXIucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInBpcGVzL2RlY2ltYWwtbnVtYmVyLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzlDLE9BQU8sRUFBRSxJQUFJLEVBQTRCLE1BQU0sZUFBZSxDQUFDO0FBQy9ELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBRXBHLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTNDO0lBa0JJLDJCQUFtQixxQkFBOEMsRUFDOUMsU0FBNEI7UUFEL0MsaUJBb0JDO1FBcEJrQiwwQkFBcUIsR0FBckIscUJBQXFCLENBQXlCO1FBQzlDLGNBQVMsR0FBVCxTQUFTLENBQW1CO1FBUi9DLGtCQUFhLEdBQVcsaUJBQWlCLENBQUMsY0FBYyxDQUFDO1FBQ3pELDRCQUF1QixHQUFXLGlCQUFpQixDQUFDLDBCQUEwQixDQUFDO1FBQy9FLDZCQUF3QixHQUFXLGlCQUFpQixDQUFDLDJCQUEyQixDQUFDO1FBQ2pGLDZCQUF3QixHQUFXLGlCQUFpQixDQUFDLDJCQUEyQixDQUFDO1FBRWpGLGVBQVUsR0FBcUIsSUFBSSxPQUFPLEVBQVcsQ0FBQztRQUtsRCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUM1QixJQUFJLENBQUMscUJBQXFCLENBQUMsTUFBTSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQztpQkFDekQsSUFBSSxDQUNELFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQzdCO2lCQUNBLFNBQVM7Ozs7WUFBQyxVQUFDLE1BQU07Z0JBQ2QsSUFBSSxNQUFNLEVBQUU7b0JBQ1IsS0FBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUM7aUJBQy9CO1lBQ0wsQ0FBQyxFQUFDLENBQUM7U0FDVjtRQUVELElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQixJQUFJLENBQUMsdUJBQXVCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQVMsZ0NBQWdDLEVBQUUsaUJBQWlCLENBQUMsMEJBQTBCLENBQUMsQ0FBQztZQUMxSSxJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQVMsaUNBQWlDLEVBQUUsaUJBQWlCLENBQUMsMkJBQTJCLENBQUMsQ0FBQztZQUM3SSxJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQVMsaUNBQWlDLEVBQUUsaUJBQWlCLENBQUMsMkJBQTJCLENBQUMsQ0FBQztTQUNoSjtJQUNMLENBQUM7Ozs7Ozs7SUFFRCxxQ0FBUzs7Ozs7O0lBQVQsVUFBVSxLQUFVLEVBQUUsVUFBK0IsRUFBRSxNQUFlOztZQUM1RCxzQkFBc0IsR0FBVyxVQUFVLElBQUksVUFBVSxDQUFDLGdCQUFnQixDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsZ0JBQWdCLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyx1QkFBdUI7O1lBQ3ZJLHVCQUF1QixHQUFXLFVBQVUsSUFBSSxVQUFVLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLHdCQUF3Qjs7WUFDM0ksdUJBQXVCLEdBQVcsVUFBVSxJQUFJLFVBQVUsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsVUFBVSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsd0JBQXdCOztZQUUzSSxnQkFBZ0IsR0FBTSxzQkFBc0IsU0FBSSx1QkFBdUIsU0FBSSx1QkFBeUI7O1lBQ3BHLFlBQVksR0FBRyxNQUFNLElBQUksSUFBSSxDQUFDLGFBQWE7O1lBRTNDLFFBQVEsR0FBZ0IsSUFBSSxXQUFXLENBQUMsWUFBWSxDQUFDO1FBQzNELE9BQU8sUUFBUSxDQUFDLFNBQVMsQ0FBQyxLQUFLLEVBQUUsZ0JBQWdCLENBQUMsQ0FBQztJQUN2RCxDQUFDOzs7O0lBRUQsdUNBQVc7OztJQUFYO1FBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBakRNLGdDQUFjLEdBQUcsT0FBTyxDQUFDO0lBQ3pCLDRDQUEwQixHQUFHLENBQUMsQ0FBQztJQUMvQiw2Q0FBMkIsR0FBRyxDQUFDLENBQUM7SUFDaEMsNkNBQTJCLEdBQUcsQ0FBQyxDQUFDOztnQkFUMUMsSUFBSSxTQUFDO29CQUNGLElBQUksRUFBRSxrQkFBa0I7b0JBQ3hCLElBQUksRUFBRSxLQUFLO2lCQUNkOzs7O2dCQVJRLHNCQUFzQjtnQkFEdEIsZ0JBQWdCOztJQThEekIsd0JBQUM7Q0FBQSxBQXhERCxJQXdEQztTQXBEWSxpQkFBaUI7OztJQUUxQixpQ0FBZ0M7O0lBQ2hDLDZDQUFzQzs7SUFDdEMsOENBQXVDOztJQUN2Qyw4Q0FBdUM7O0lBRXZDLDBDQUF5RDs7SUFDekQsb0RBQStFOztJQUMvRSxxREFBaUY7O0lBQ2pGLHFEQUFpRjs7SUFFakYsdUNBQXNEOztJQUUxQyxrREFBcUQ7O0lBQ3JELHNDQUFtQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBEZWNpbWFsUGlwZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbic7XHJcbmltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0sIE9uRGVzdHJveSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBcHBDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vYXBwLWNvbmZpZy9hcHAtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlLCBVc2VyUHJlZmVyZW5jZVZhbHVlcyB9IGZyb20gJy4uL3NlcnZpY2VzL3VzZXItcHJlZmVyZW5jZXMuc2VydmljZSc7XHJcbmltcG9ydCB7IERlY2ltYWxOdW1iZXJNb2RlbCB9IGZyb20gJy4uL21vZGVscy9kZWNpbWFsLW51bWJlci5tb2RlbCc7XHJcbmltcG9ydCB7IFN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQFBpcGUoe1xyXG4gICAgbmFtZTogJ2FkZkRlY2ltYWxOdW1iZXInLFxyXG4gICAgcHVyZTogZmFsc2VcclxufSlcclxuZXhwb3J0IGNsYXNzIERlY2ltYWxOdW1iZXJQaXBlIGltcGxlbWVudHMgUGlwZVRyYW5zZm9ybSwgT25EZXN0cm95IHtcclxuXHJcbiAgICBzdGF0aWMgREVGQVVMVF9MT0NBTEUgPSAnZW4tVVMnO1xyXG4gICAgc3RhdGljIERFRkFVTFRfTUlOX0lOVEVHRVJfRElHSVRTID0gMTtcclxuICAgIHN0YXRpYyBERUZBVUxUX01JTl9GUkFDVElPTl9ESUdJVFMgPSAwO1xyXG4gICAgc3RhdGljIERFRkFVTFRfTUFYX0ZSQUNUSU9OX0RJR0lUUyA9IDI7XHJcblxyXG4gICAgZGVmYXVsdExvY2FsZTogc3RyaW5nID0gRGVjaW1hbE51bWJlclBpcGUuREVGQVVMVF9MT0NBTEU7XHJcbiAgICBkZWZhdWx0TWluSW50ZWdlckRpZ2l0czogbnVtYmVyID0gRGVjaW1hbE51bWJlclBpcGUuREVGQVVMVF9NSU5fSU5URUdFUl9ESUdJVFM7XHJcbiAgICBkZWZhdWx0TWluRnJhY3Rpb25EaWdpdHM6IG51bWJlciA9IERlY2ltYWxOdW1iZXJQaXBlLkRFRkFVTFRfTUlOX0ZSQUNUSU9OX0RJR0lUUztcclxuICAgIGRlZmF1bHRNYXhGcmFjdGlvbkRpZ2l0czogbnVtYmVyID0gRGVjaW1hbE51bWJlclBpcGUuREVGQVVMVF9NQVhfRlJBQ1RJT05fRElHSVRTO1xyXG5cclxuICAgIG9uRGVzdHJveSQ6IFN1YmplY3Q8Ym9vbGVhbj4gPSBuZXcgU3ViamVjdDxib29sZWFuPigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyB1c2VyUHJlZmVyZW5jZVNlcnZpY2U/OiBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHVibGljIGFwcENvbmZpZz86IEFwcENvbmZpZ1NlcnZpY2UpIHtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMudXNlclByZWZlcmVuY2VTZXJ2aWNlKSB7XHJcbiAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VTZXJ2aWNlLnNlbGVjdChVc2VyUHJlZmVyZW5jZVZhbHVlcy5Mb2NhbGUpXHJcbiAgICAgICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgICAgICB0YWtlVW50aWwodGhpcy5vbkRlc3Ryb3kkKVxyXG4gICAgICAgICAgICAgICAgKVxyXG4gICAgICAgICAgICAgICAgLnN1YnNjcmliZSgobG9jYWxlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKGxvY2FsZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmRlZmF1bHRMb2NhbGUgPSBsb2NhbGU7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5hcHBDb25maWcpIHtcclxuICAgICAgICAgICAgdGhpcy5kZWZhdWx0TWluSW50ZWdlckRpZ2l0cyA9IHRoaXMuYXBwQ29uZmlnLmdldDxudW1iZXI+KCdkZWNpbWFsVmFsdWVzLm1pbkludGVnZXJEaWdpdHMnLCBEZWNpbWFsTnVtYmVyUGlwZS5ERUZBVUxUX01JTl9JTlRFR0VSX0RJR0lUUyk7XHJcbiAgICAgICAgICAgIHRoaXMuZGVmYXVsdE1pbkZyYWN0aW9uRGlnaXRzID0gdGhpcy5hcHBDb25maWcuZ2V0PG51bWJlcj4oJ2RlY2ltYWxWYWx1ZXMubWluRnJhY3Rpb25EaWdpdHMnLCBEZWNpbWFsTnVtYmVyUGlwZS5ERUZBVUxUX01JTl9GUkFDVElPTl9ESUdJVFMpO1xyXG4gICAgICAgICAgICB0aGlzLmRlZmF1bHRNYXhGcmFjdGlvbkRpZ2l0cyA9IHRoaXMuYXBwQ29uZmlnLmdldDxudW1iZXI+KCdkZWNpbWFsVmFsdWVzLm1heEZyYWN0aW9uRGlnaXRzJywgRGVjaW1hbE51bWJlclBpcGUuREVGQVVMVF9NQVhfRlJBQ1RJT05fRElHSVRTKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgdHJhbnNmb3JtKHZhbHVlOiBhbnksIGRpZ2l0c0luZm8/OiBEZWNpbWFsTnVtYmVyTW9kZWwsIGxvY2FsZT86IHN0cmluZyk6IGFueSB7XHJcbiAgICAgICAgY29uc3QgYWN0dWFsTWluSW50ZWdlckRpZ2l0czogbnVtYmVyID0gZGlnaXRzSW5mbyAmJiBkaWdpdHNJbmZvLm1pbkludGVnZXJEaWdpdHMgPyBkaWdpdHNJbmZvLm1pbkludGVnZXJEaWdpdHMgOiB0aGlzLmRlZmF1bHRNaW5JbnRlZ2VyRGlnaXRzO1xyXG4gICAgICAgIGNvbnN0IGFjdHVhbE1pbkZyYWN0aW9uRGlnaXRzOiBudW1iZXIgPSBkaWdpdHNJbmZvICYmIGRpZ2l0c0luZm8ubWluRnJhY3Rpb25EaWdpdHMgPyBkaWdpdHNJbmZvLm1pbkZyYWN0aW9uRGlnaXRzIDogdGhpcy5kZWZhdWx0TWluRnJhY3Rpb25EaWdpdHM7XHJcbiAgICAgICAgY29uc3QgYWN0dWFsTWF4RnJhY3Rpb25EaWdpdHM6IG51bWJlciA9IGRpZ2l0c0luZm8gJiYgZGlnaXRzSW5mby5tYXhGcmFjdGlvbkRpZ2l0cyA/IGRpZ2l0c0luZm8ubWF4RnJhY3Rpb25EaWdpdHMgOiB0aGlzLmRlZmF1bHRNYXhGcmFjdGlvbkRpZ2l0cztcclxuXHJcbiAgICAgICAgY29uc3QgYWN0dWFsRGlnaXRzSW5mbyA9IGAke2FjdHVhbE1pbkludGVnZXJEaWdpdHN9LiR7YWN0dWFsTWluRnJhY3Rpb25EaWdpdHN9LSR7YWN0dWFsTWF4RnJhY3Rpb25EaWdpdHN9YDtcclxuICAgICAgICBjb25zdCBhY3R1YWxMb2NhbGUgPSBsb2NhbGUgfHwgdGhpcy5kZWZhdWx0TG9jYWxlO1xyXG5cclxuICAgICAgICBjb25zdCBkYXRlUGlwZTogRGVjaW1hbFBpcGUgPSBuZXcgRGVjaW1hbFBpcGUoYWN0dWFsTG9jYWxlKTtcclxuICAgICAgICByZXR1cm4gZGF0ZVBpcGUudHJhbnNmb3JtKHZhbHVlLCBhY3R1YWxEaWdpdHNJbmZvKTtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQubmV4dCh0cnVlKTtcclxuICAgICAgICB0aGlzLm9uRGVzdHJveSQuY29tcGxldGUoKTtcclxuICAgIH1cclxufVxyXG4iXX0=