/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Pipe } from '@angular/core';
import { TranslationService } from '../services/translation.service';
var FileSizePipe = /** @class */ (function () {
    function FileSizePipe(translation) {
        this.translation = translation;
    }
    /**
     * @param {?} bytes
     * @param {?=} decimals
     * @return {?}
     */
    FileSizePipe.prototype.transform = /**
     * @param {?} bytes
     * @param {?=} decimals
     * @return {?}
     */
    function (bytes, decimals) {
        if (decimals === void 0) { decimals = 2; }
        if (bytes == null || bytes === undefined) {
            return '';
        }
        if (bytes === 0) {
            return '0 ' + this.translation.instant('CORE.FILE_SIZE.BYTES');
        }
        /** @type {?} */
        var k = 1024;
        /** @type {?} */
        var dm = decimals || 2;
        /** @type {?} */
        var sizes = ['BYTES', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        /** @type {?} */
        var i = Math.floor(Math.log(bytes) / Math.log(k));
        /** @type {?} */
        var i18nSize = this.translation.instant("CORE.FILE_SIZE." + sizes[i]);
        return parseFloat((bytes / Math.pow(k, i)).toFixed(dm)) + ' ' + i18nSize;
    };
    FileSizePipe.decorators = [
        { type: Pipe, args: [{
                    name: 'adfFileSize',
                    pure: false
                },] }
    ];
    /** @nocollapse */
    FileSizePipe.ctorParameters = function () { return [
        { type: TranslationService }
    ]; };
    return FileSizePipe;
}());
export { FileSizePipe };
if (false) {
    /**
     * @type {?}
     * @private
     */
    FileSizePipe.prototype.translation;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZS1zaXplLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJwaXBlcy9maWxlLXNpemUucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUNwRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxpQ0FBaUMsQ0FBQztBQUVyRTtJQU1JLHNCQUFvQixXQUErQjtRQUEvQixnQkFBVyxHQUFYLFdBQVcsQ0FBb0I7SUFDbkQsQ0FBQzs7Ozs7O0lBRUQsZ0NBQVM7Ozs7O0lBQVQsVUFBVSxLQUFhLEVBQUUsUUFBb0I7UUFBcEIseUJBQUEsRUFBQSxZQUFvQjtRQUN6QyxJQUFJLEtBQUssSUFBSSxJQUFJLElBQUksS0FBSyxLQUFLLFNBQVMsRUFBRTtZQUN0QyxPQUFPLEVBQUUsQ0FBQztTQUNiO1FBRUQsSUFBSSxLQUFLLEtBQUssQ0FBQyxFQUFFO1lBQ2IsT0FBTyxJQUFJLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsc0JBQXNCLENBQUMsQ0FBQztTQUNsRTs7WUFFSyxDQUFDLEdBQUcsSUFBSTs7WUFDVixFQUFFLEdBQUcsUUFBUSxJQUFJLENBQUM7O1lBQ2xCLEtBQUssR0FBRyxDQUFDLE9BQU8sRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDOztZQUNqRSxDQUFDLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUM7O1lBRTNDLFFBQVEsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sQ0FBQyxvQkFBa0IsS0FBSyxDQUFDLENBQUMsQ0FBRyxDQUFDO1FBRXZFLE9BQU8sVUFBVSxDQUFDLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDLENBQUMsT0FBTyxDQUFDLEVBQUUsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLFFBQVEsQ0FBQztJQUM3RSxDQUFDOztnQkExQkosSUFBSSxTQUFDO29CQUNGLElBQUksRUFBRSxhQUFhO29CQUNuQixJQUFJLEVBQUUsS0FBSztpQkFDZDs7OztnQkFMUSxrQkFBa0I7O0lBOEIzQixtQkFBQztDQUFBLEFBNUJELElBNEJDO1NBeEJZLFlBQVk7Ozs7OztJQUVULG1DQUF1QyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFRyYW5zbGF0aW9uU2VydmljZSB9IGZyb20gJy4uL3NlcnZpY2VzL3RyYW5zbGF0aW9uLnNlcnZpY2UnO1xyXG5cclxuQFBpcGUoe1xyXG4gICAgbmFtZTogJ2FkZkZpbGVTaXplJyxcclxuICAgIHB1cmU6IGZhbHNlXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGaWxlU2l6ZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHRyYW5zbGF0aW9uOiBUcmFuc2xhdGlvblNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICB0cmFuc2Zvcm0oYnl0ZXM6IG51bWJlciwgZGVjaW1hbHM6IG51bWJlciA9IDIpOiBzdHJpbmcge1xyXG4gICAgICAgIGlmIChieXRlcyA9PSBudWxsIHx8IGJ5dGVzID09PSB1bmRlZmluZWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGJ5dGVzID09PSAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAnMCAnICsgdGhpcy50cmFuc2xhdGlvbi5pbnN0YW50KCdDT1JFLkZJTEVfU0laRS5CWVRFUycpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgayA9IDEwMjQsXHJcbiAgICAgICAgICAgIGRtID0gZGVjaW1hbHMgfHwgMixcclxuICAgICAgICAgICAgc2l6ZXMgPSBbJ0JZVEVTJywgJ0tCJywgJ01CJywgJ0dCJywgJ1RCJywgJ1BCJywgJ0VCJywgJ1pCJywgJ1lCJ10sXHJcbiAgICAgICAgICAgIGkgPSBNYXRoLmZsb29yKE1hdGgubG9nKGJ5dGVzKSAvIE1hdGgubG9nKGspKTtcclxuXHJcbiAgICAgICAgY29uc3QgaTE4blNpemUgPSB0aGlzLnRyYW5zbGF0aW9uLmluc3RhbnQoYENPUkUuRklMRV9TSVpFLiR7c2l6ZXNbaV19YCk7XHJcblxyXG4gICAgICAgIHJldHVybiBwYXJzZUZsb2F0KChieXRlcyAvIE1hdGgucG93KGssIGkpKS50b0ZpeGVkKGRtKSkgKyAnICcgKyBpMThuU2l6ZTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19