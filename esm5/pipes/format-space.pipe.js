/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Pipe } from '@angular/core';
var FormatSpacePipe = /** @class */ (function () {
    function FormatSpacePipe() {
    }
    /**
     * @param {?} inputValue
     * @param {?=} replaceChar
     * @param {?=} lowerCase
     * @return {?}
     */
    FormatSpacePipe.prototype.transform = /**
     * @param {?} inputValue
     * @param {?=} replaceChar
     * @param {?=} lowerCase
     * @return {?}
     */
    function (inputValue, replaceChar, lowerCase) {
        if (replaceChar === void 0) { replaceChar = '_'; }
        if (lowerCase === void 0) { lowerCase = true; }
        /** @type {?} */
        var transformedString = '';
        if (inputValue) {
            transformedString = lowerCase ? inputValue.trim().split(' ').join(replaceChar).toLocaleLowerCase() :
                inputValue.trim().split(' ').join(replaceChar);
        }
        return transformedString;
    };
    FormatSpacePipe.decorators = [
        { type: Pipe, args: [{
                    name: 'formatSpace'
                },] }
    ];
    return FormatSpacePipe;
}());
export { FormatSpacePipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZm9ybWF0LXNwYWNlLnBpcGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJwaXBlcy9mb3JtYXQtc3BhY2UucGlwZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsSUFBSSxFQUFpQixNQUFNLGVBQWUsQ0FBQztBQUVwRDtJQUFBO0lBY0EsQ0FBQzs7Ozs7OztJQVRHLG1DQUFTOzs7Ozs7SUFBVCxVQUFVLFVBQWtCLEVBQUUsV0FBeUIsRUFBRSxTQUF5QjtRQUFwRCw0QkFBQSxFQUFBLGlCQUF5QjtRQUFFLDBCQUFBLEVBQUEsZ0JBQXlCOztZQUMxRSxpQkFBaUIsR0FBRyxFQUFFO1FBQzFCLElBQUksVUFBVSxFQUFFO1lBQ1osaUJBQWlCLEdBQUcsU0FBUyxDQUFDLENBQUMsQ0FBQyxVQUFVLENBQUMsSUFBSSxFQUFFLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDLENBQUM7Z0JBQ2hHLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxLQUFLLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQ3REO1FBQ0QsT0FBTyxpQkFBaUIsQ0FBQztJQUM3QixDQUFDOztnQkFaSixJQUFJLFNBQUM7b0JBQ0YsSUFBSSxFQUFFLGFBQWE7aUJBQ3RCOztJQVlELHNCQUFDO0NBQUEsQUFkRCxJQWNDO1NBWFksZUFBZSIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBQaXBlLCBQaXBlVHJhbnNmb3JtIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5AUGlwZSh7XHJcbiAgICBuYW1lOiAnZm9ybWF0U3BhY2UnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBGb3JtYXRTcGFjZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtIHtcclxuXHJcbiAgICB0cmFuc2Zvcm0oaW5wdXRWYWx1ZTogc3RyaW5nLCByZXBsYWNlQ2hhcjogc3RyaW5nID0gJ18nLCBsb3dlckNhc2U6IGJvb2xlYW4gPSB0cnVlKTogc3RyaW5nIHtcclxuICAgICAgICBsZXQgdHJhbnNmb3JtZWRTdHJpbmcgPSAnJztcclxuICAgICAgICBpZiAoaW5wdXRWYWx1ZSkge1xyXG4gICAgICAgICAgICB0cmFuc2Zvcm1lZFN0cmluZyA9IGxvd2VyQ2FzZSA/IGlucHV0VmFsdWUudHJpbSgpLnNwbGl0KCcgJykuam9pbihyZXBsYWNlQ2hhcikudG9Mb2NhbGVMb3dlckNhc2UoKSA6XHJcbiAgICAgICAgICAgICAgICBpbnB1dFZhbHVlLnRyaW0oKS5zcGxpdCgnICcpLmpvaW4ocmVwbGFjZUNoYXIpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdHJhbnNmb3JtZWRTdHJpbmc7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==