/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DatePipe } from '@angular/common';
import { Pipe } from '@angular/core';
import { AppConfigService } from '../app-config/app-config.service';
import { UserPreferencesService, UserPreferenceValues } from '../services/user-preferences.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
var LocalizedDatePipe = /** @class */ (function () {
    function LocalizedDatePipe(userPreferenceService, appConfig) {
        var _this = this;
        this.userPreferenceService = userPreferenceService;
        this.appConfig = appConfig;
        this.defaultLocale = LocalizedDatePipe.DEFAULT_LOCALE;
        this.defaultFormat = LocalizedDatePipe.DEFAULT_DATE_FORMAT;
        this.onDestroy$ = new Subject();
        if (this.userPreferenceService) {
            this.userPreferenceService
                .select(UserPreferenceValues.Locale)
                .pipe(takeUntil(this.onDestroy$))
                .subscribe((/**
             * @param {?} locale
             * @return {?}
             */
            function (locale) {
                if (locale) {
                    _this.defaultLocale = locale;
                }
            }));
        }
        if (this.appConfig) {
            this.defaultFormat = this.appConfig.get('dateValues.defaultDateFormat', LocalizedDatePipe.DEFAULT_DATE_FORMAT);
        }
    }
    /**
     * @param {?} value
     * @param {?=} format
     * @param {?=} locale
     * @return {?}
     */
    LocalizedDatePipe.prototype.transform = /**
     * @param {?} value
     * @param {?=} format
     * @param {?=} locale
     * @return {?}
     */
    function (value, format, locale) {
        /** @type {?} */
        var actualFormat = format || this.defaultFormat;
        /** @type {?} */
        var actualLocale = locale || this.defaultLocale;
        /** @type {?} */
        var datePipe = new DatePipe(actualLocale);
        return datePipe.transform(value, actualFormat);
    };
    /**
     * @return {?}
     */
    LocalizedDatePipe.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    };
    LocalizedDatePipe.DEFAULT_LOCALE = 'en-US';
    LocalizedDatePipe.DEFAULT_DATE_FORMAT = 'mediumDate';
    LocalizedDatePipe.decorators = [
        { type: Pipe, args: [{
                    name: 'adfLocalizedDate',
                    pure: false
                },] }
    ];
    /** @nocollapse */
    LocalizedDatePipe.ctorParameters = function () { return [
        { type: UserPreferencesService },
        { type: AppConfigService }
    ]; };
    return LocalizedDatePipe;
}());
export { LocalizedDatePipe };
if (false) {
    /** @type {?} */
    LocalizedDatePipe.DEFAULT_LOCALE;
    /** @type {?} */
    LocalizedDatePipe.DEFAULT_DATE_FORMAT;
    /** @type {?} */
    LocalizedDatePipe.prototype.defaultLocale;
    /** @type {?} */
    LocalizedDatePipe.prototype.defaultFormat;
    /**
     * @type {?}
     * @private
     */
    LocalizedDatePipe.prototype.onDestroy$;
    /** @type {?} */
    LocalizedDatePipe.prototype.userPreferenceService;
    /** @type {?} */
    LocalizedDatePipe.prototype.appConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9jYWxpemVkLWRhdGUucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInBpcGVzL2xvY2FsaXplZC1kYXRlLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFFBQVEsRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQzNDLE9BQU8sRUFBRSxJQUFJLEVBQTRCLE1BQU0sZUFBZSxDQUFDO0FBQy9ELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ3BHLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTNDO0lBY0ksMkJBQW1CLHFCQUE4QyxFQUM5QyxTQUE0QjtRQUQvQyxpQkFpQkM7UUFqQmtCLDBCQUFxQixHQUFyQixxQkFBcUIsQ0FBeUI7UUFDOUMsY0FBUyxHQUFULFNBQVMsQ0FBbUI7UUFOL0Msa0JBQWEsR0FBVyxpQkFBaUIsQ0FBQyxjQUFjLENBQUM7UUFDekQsa0JBQWEsR0FBVyxpQkFBaUIsQ0FBQyxtQkFBbUIsQ0FBQztRQUV0RCxlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztRQUt4QyxJQUFJLElBQUksQ0FBQyxxQkFBcUIsRUFBRTtZQUM1QixJQUFJLENBQUMscUJBQXFCO2lCQUNyQixNQUFNLENBQUMsb0JBQW9CLENBQUMsTUFBTSxDQUFDO2lCQUNuQyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQztpQkFDaEMsU0FBUzs7OztZQUFDLFVBQUEsTUFBTTtnQkFDYixJQUFJLE1BQU0sRUFBRTtvQkFDUixLQUFJLENBQUMsYUFBYSxHQUFHLE1BQU0sQ0FBQztpQkFDL0I7WUFDTCxDQUFDLEVBQUMsQ0FBQztTQUNWO1FBRUQsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxhQUFhLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQVMsOEJBQThCLEVBQUUsaUJBQWlCLENBQUMsbUJBQW1CLENBQUMsQ0FBQztTQUMxSDtJQUNMLENBQUM7Ozs7Ozs7SUFFRCxxQ0FBUzs7Ozs7O0lBQVQsVUFBVSxLQUFVLEVBQUUsTUFBZSxFQUFFLE1BQWU7O1lBQzVDLFlBQVksR0FBRyxNQUFNLElBQUksSUFBSSxDQUFDLGFBQWE7O1lBQzNDLFlBQVksR0FBRyxNQUFNLElBQUksSUFBSSxDQUFDLGFBQWE7O1lBQzNDLFFBQVEsR0FBYSxJQUFJLFFBQVEsQ0FBQyxZQUFZLENBQUM7UUFDckQsT0FBTyxRQUFRLENBQUMsU0FBUyxDQUFDLEtBQUssRUFBRSxZQUFZLENBQUMsQ0FBQztJQUNuRCxDQUFDOzs7O0lBRUQsdUNBQVc7OztJQUFYO1FBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBckNNLGdDQUFjLEdBQUcsT0FBTyxDQUFDO0lBQ3pCLHFDQUFtQixHQUFHLFlBQVksQ0FBQzs7Z0JBUDdDLElBQUksU0FBQztvQkFDRixJQUFJLEVBQUUsa0JBQWtCO29CQUN4QixJQUFJLEVBQUUsS0FBSztpQkFDZDs7OztnQkFQUSxzQkFBc0I7Z0JBRHRCLGdCQUFnQjs7SUFrRHpCLHdCQUFDO0NBQUEsQUE3Q0QsSUE2Q0M7U0F6Q1ksaUJBQWlCOzs7SUFFMUIsaUNBQWdDOztJQUNoQyxzQ0FBMEM7O0lBRTFDLDBDQUF5RDs7SUFDekQsMENBQThEOzs7OztJQUU5RCx1Q0FBNEM7O0lBRWhDLGtEQUFxRDs7SUFDckQsc0NBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IERhdGVQaXBlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uJztcclxuaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UsIFVzZXJQcmVmZXJlbmNlVmFsdWVzIH0gZnJvbSAnLi4vc2VydmljZXMvdXNlci1wcmVmZXJlbmNlcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyB0YWtlVW50aWwgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5AUGlwZSh7XHJcbiAgICBuYW1lOiAnYWRmTG9jYWxpemVkRGF0ZScsXHJcbiAgICBwdXJlOiBmYWxzZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgTG9jYWxpemVkRGF0ZVBpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtLCBPbkRlc3Ryb3kge1xyXG5cclxuICAgIHN0YXRpYyBERUZBVUxUX0xPQ0FMRSA9ICdlbi1VUyc7XHJcbiAgICBzdGF0aWMgREVGQVVMVF9EQVRFX0ZPUk1BVCA9ICdtZWRpdW1EYXRlJztcclxuXHJcbiAgICBkZWZhdWx0TG9jYWxlOiBzdHJpbmcgPSBMb2NhbGl6ZWREYXRlUGlwZS5ERUZBVUxUX0xPQ0FMRTtcclxuICAgIGRlZmF1bHRGb3JtYXQ6IHN0cmluZyA9IExvY2FsaXplZERhdGVQaXBlLkRFRkFVTFRfREFURV9GT1JNQVQ7XHJcblxyXG4gICAgcHJpdmF0ZSBvbkRlc3Ryb3kkID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgdXNlclByZWZlcmVuY2VTZXJ2aWNlPzogVXNlclByZWZlcmVuY2VzU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBhcHBDb25maWc/OiBBcHBDb25maWdTZXJ2aWNlKSB7XHJcblxyXG4gICAgICAgIGlmICh0aGlzLnVzZXJQcmVmZXJlbmNlU2VydmljZSkge1xyXG4gICAgICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlU2VydmljZVxyXG4gICAgICAgICAgICAgICAgLnNlbGVjdChVc2VyUHJlZmVyZW5jZVZhbHVlcy5Mb2NhbGUpXHJcbiAgICAgICAgICAgICAgICAucGlwZSh0YWtlVW50aWwodGhpcy5vbkRlc3Ryb3kkKSlcclxuICAgICAgICAgICAgICAgIC5zdWJzY3JpYmUobG9jYWxlID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAobG9jYWxlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuZGVmYXVsdExvY2FsZSA9IGxvY2FsZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmFwcENvbmZpZykge1xyXG4gICAgICAgICAgICB0aGlzLmRlZmF1bHRGb3JtYXQgPSB0aGlzLmFwcENvbmZpZy5nZXQ8c3RyaW5nPignZGF0ZVZhbHVlcy5kZWZhdWx0RGF0ZUZvcm1hdCcsIExvY2FsaXplZERhdGVQaXBlLkRFRkFVTFRfREFURV9GT1JNQVQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0cmFuc2Zvcm0odmFsdWU6IGFueSwgZm9ybWF0Pzogc3RyaW5nLCBsb2NhbGU/OiBzdHJpbmcpOiBhbnkge1xyXG4gICAgICAgIGNvbnN0IGFjdHVhbEZvcm1hdCA9IGZvcm1hdCB8fCB0aGlzLmRlZmF1bHRGb3JtYXQ7XHJcbiAgICAgICAgY29uc3QgYWN0dWFsTG9jYWxlID0gbG9jYWxlIHx8IHRoaXMuZGVmYXVsdExvY2FsZTtcclxuICAgICAgICBjb25zdCBkYXRlUGlwZTogRGF0ZVBpcGUgPSBuZXcgRGF0ZVBpcGUoYWN0dWFsTG9jYWxlKTtcclxuICAgICAgICByZXR1cm4gZGF0ZVBpcGUudHJhbnNmb3JtKHZhbHVlLCBhY3R1YWxGb3JtYXQpO1xyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIHRoaXMub25EZXN0cm95JC5uZXh0KHRydWUpO1xyXG4gICAgICAgIHRoaXMub25EZXN0cm95JC5jb21wbGV0ZSgpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=