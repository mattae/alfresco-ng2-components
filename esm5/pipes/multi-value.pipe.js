/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Pipe } from '@angular/core';
var MultiValuePipe = /** @class */ (function () {
    function MultiValuePipe() {
    }
    /**
     * @param {?} values
     * @param {?=} valueSeparator
     * @return {?}
     */
    MultiValuePipe.prototype.transform = /**
     * @param {?} values
     * @param {?=} valueSeparator
     * @return {?}
     */
    function (values, valueSeparator) {
        if (valueSeparator === void 0) { valueSeparator = MultiValuePipe.DEFAULT_SEPARATOR; }
        if (values && values instanceof Array) {
            /** @type {?} */
            var valueList = values.map((/**
             * @param {?} value
             * @return {?}
             */
            function (value) { return value.trim(); }));
            return valueList.join(valueSeparator);
        }
        return (/** @type {?} */ (values));
    };
    MultiValuePipe.DEFAULT_SEPARATOR = ', ';
    MultiValuePipe.decorators = [
        { type: Pipe, args: [{ name: 'multiValue' },] }
    ];
    return MultiValuePipe;
}());
export { MultiValuePipe };
if (false) {
    /** @type {?} */
    MultiValuePipe.DEFAULT_SEPARATOR;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXVsdGktdmFsdWUucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInBpcGVzL211bHRpLXZhbHVlLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFFcEQ7SUFBQTtJQWNBLENBQUM7Ozs7OztJQVRHLGtDQUFTOzs7OztJQUFULFVBQVUsTUFBMEIsRUFBRSxjQUF5RDtRQUF6RCwrQkFBQSxFQUFBLGlCQUF5QixjQUFjLENBQUMsaUJBQWlCO1FBRTNGLElBQUksTUFBTSxJQUFJLE1BQU0sWUFBWSxLQUFLLEVBQUU7O2dCQUM3QixTQUFTLEdBQUcsTUFBTSxDQUFDLEdBQUc7Ozs7WUFBQyxVQUFDLEtBQUssSUFBSyxPQUFBLEtBQUssQ0FBQyxJQUFJLEVBQUUsRUFBWixDQUFZLEVBQUM7WUFDckQsT0FBTyxTQUFTLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxDQUFDO1NBQ3pDO1FBRUQsT0FBTyxtQkFBUyxNQUFNLEVBQUEsQ0FBQztJQUMzQixDQUFDO0lBVk0sZ0NBQWlCLEdBQUcsSUFBSSxDQUFDOztnQkFIbkMsSUFBSSxTQUFDLEVBQUUsSUFBSSxFQUFFLFlBQVksRUFBRTs7SUFjNUIscUJBQUM7Q0FBQSxBQWRELElBY0M7U0FiWSxjQUFjOzs7SUFFdkIsaUNBQWdDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBQaXBlKHsgbmFtZTogJ211bHRpVmFsdWUnIH0pXHJcbmV4cG9ydCBjbGFzcyBNdWx0aVZhbHVlUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xyXG5cclxuICAgIHN0YXRpYyBERUZBVUxUX1NFUEFSQVRPUiA9ICcsICc7XHJcblxyXG4gICAgdHJhbnNmb3JtKHZhbHVlczogc3RyaW5nIHwgc3RyaW5nIFtdLCB2YWx1ZVNlcGFyYXRvcjogc3RyaW5nID0gTXVsdGlWYWx1ZVBpcGUuREVGQVVMVF9TRVBBUkFUT1IpOiBzdHJpbmcge1xyXG5cclxuICAgICAgICBpZiAodmFsdWVzICYmIHZhbHVlcyBpbnN0YW5jZW9mIEFycmF5KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHZhbHVlTGlzdCA9IHZhbHVlcy5tYXAoKHZhbHVlKSA9PiB2YWx1ZS50cmltKCkpO1xyXG4gICAgICAgICAgICByZXR1cm4gdmFsdWVMaXN0LmpvaW4odmFsdWVTZXBhcmF0b3IpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIDxzdHJpbmc+IHZhbHVlcztcclxuICAgIH1cclxufVxyXG4iXX0=