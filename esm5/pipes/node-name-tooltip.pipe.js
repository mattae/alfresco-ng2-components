/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Pipe } from '@angular/core';
var NodeNameTooltipPipe = /** @class */ (function () {
    function NodeNameTooltipPipe() {
    }
    /**
     * @param {?} node
     * @return {?}
     */
    NodeNameTooltipPipe.prototype.transform = /**
     * @param {?} node
     * @return {?}
     */
    function (node) {
        if (node) {
            return this.getNodeTooltip(node);
        }
        return null;
    };
    /**
     * @private
     * @param {?} lines
     * @param {?} line
     * @return {?}
     */
    NodeNameTooltipPipe.prototype.containsLine = /**
     * @private
     * @param {?} lines
     * @param {?} line
     * @return {?}
     */
    function (lines, line) {
        return lines.some((/**
         * @param {?} item
         * @return {?}
         */
        function (item) {
            return item.toLowerCase() === line.toLowerCase();
        }));
    };
    /**
     * @private
     * @param {?} lines
     * @return {?}
     */
    NodeNameTooltipPipe.prototype.removeDuplicateLines = /**
     * @private
     * @param {?} lines
     * @return {?}
     */
    function (lines) {
        var _this = this;
        /** @type {?} */
        var reducer = (/**
         * @param {?} acc
         * @param {?} line
         * @return {?}
         */
        function (acc, line) {
            if (!_this.containsLine(acc, line)) {
                acc.push(line);
            }
            return acc;
        });
        return lines.reduce(reducer, []);
    };
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    NodeNameTooltipPipe.prototype.getNodeTooltip = /**
     * @private
     * @param {?} node
     * @return {?}
     */
    function (node) {
        if (!node || !node.entry) {
            return null;
        }
        var _a = node.entry, properties = _a.properties, name = _a.name;
        /** @type {?} */
        var lines = [name];
        if (properties) {
            var title = properties["cm:title"], description = properties["cm:description"];
            if (title && description) {
                lines[0] = title;
                lines[1] = description;
            }
            if (title) {
                lines[1] = title;
            }
            if (description) {
                lines[1] = description;
            }
        }
        return this.removeDuplicateLines(lines).join("\n");
    };
    NodeNameTooltipPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'adfNodeNameTooltip'
                },] }
    ];
    return NodeNameTooltipPipe;
}());
export { NodeNameTooltipPipe };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9kZS1uYW1lLXRvb2x0aXAucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInBpcGVzL25vZGUtbmFtZS10b29sdGlwLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLElBQUksRUFBaUIsTUFBTSxlQUFlLENBQUM7QUFHcEQ7SUFBQTtJQXlEQSxDQUFDOzs7OztJQXBERyx1Q0FBUzs7OztJQUFULFVBQVUsSUFBZTtRQUNyQixJQUFJLElBQUksRUFBRTtZQUNOLE9BQU8sSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsQ0FBQztTQUNwQztRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7Ozs7SUFFTywwQ0FBWTs7Ozs7O0lBQXBCLFVBQXFCLEtBQWUsRUFBRSxJQUFZO1FBQzlDLE9BQU8sS0FBSyxDQUFDLElBQUk7Ozs7UUFBQyxVQUFDLElBQVk7WUFDM0IsT0FBTyxJQUFJLENBQUMsV0FBVyxFQUFFLEtBQUssSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO1FBQ3JELENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7O0lBRU8sa0RBQW9COzs7OztJQUE1QixVQUE2QixLQUFlO1FBQTVDLGlCQU9DOztZQU5TLE9BQU87Ozs7O1FBQUcsVUFBQyxHQUFhLEVBQUUsSUFBWTtZQUN4QyxJQUFJLENBQUMsS0FBSSxDQUFDLFlBQVksQ0FBQyxHQUFHLEVBQUUsSUFBSSxDQUFDLEVBQUU7Z0JBQUUsR0FBRyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQzthQUFFO1lBQ3RELE9BQU8sR0FBRyxDQUFDO1FBQ2YsQ0FBQyxDQUFBO1FBRUQsT0FBTyxLQUFLLENBQUMsTUFBTSxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsQ0FBQztJQUNyQyxDQUFDOzs7Ozs7SUFFTyw0Q0FBYzs7Ozs7SUFBdEIsVUFBdUIsSUFBZTtRQUNsQyxJQUFJLENBQUMsSUFBSSxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssRUFBRTtZQUN0QixPQUFPLElBQUksQ0FBQztTQUNmO1FBRU8sSUFBQSxlQUEyQixFQUFsQiwwQkFBVSxFQUFFLGNBQU07O1lBQzdCLEtBQUssR0FBRyxDQUFFLElBQUksQ0FBRTtRQUV0QixJQUFJLFVBQVUsRUFBRTtZQUVSLElBQUEsOEJBQWlCLEVBQ2pCLDBDQUE2QjtZQUdqQyxJQUFJLEtBQUssSUFBSSxXQUFXLEVBQUU7Z0JBQ3RCLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUM7Z0JBQ2pCLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxXQUFXLENBQUM7YUFDMUI7WUFFRCxJQUFJLEtBQUssRUFBRTtnQkFDUCxLQUFLLENBQUMsQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDO2FBQ3BCO1lBRUQsSUFBSSxXQUFXLEVBQUU7Z0JBQ2IsS0FBSyxDQUFDLENBQUMsQ0FBQyxHQUFHLFdBQVcsQ0FBQzthQUMxQjtTQUNKO1FBRUQsT0FBTyxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3ZELENBQUM7O2dCQXhESixJQUFJLFNBQUM7b0JBQ0YsSUFBSSxFQUFFLG9CQUFvQjtpQkFDN0I7O0lBdURELDBCQUFDO0NBQUEsQUF6REQsSUF5REM7U0F0RFksbUJBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IFBpcGUsIFBpcGVUcmFuc2Zvcm0gfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTm9kZUVudHJ5IH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcblxyXG5AUGlwZSh7XHJcbiAgICBuYW1lOiAnYWRmTm9kZU5hbWVUb29sdGlwJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgTm9kZU5hbWVUb29sdGlwUGlwZSBpbXBsZW1lbnRzIFBpcGVUcmFuc2Zvcm0ge1xyXG5cclxuICAgIHRyYW5zZm9ybShub2RlOiBOb2RlRW50cnkpOiBzdHJpbmcge1xyXG4gICAgICAgIGlmIChub2RlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmdldE5vZGVUb29sdGlwKG5vZGUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNvbnRhaW5zTGluZShsaW5lczogc3RyaW5nW10sIGxpbmU6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiBsaW5lcy5zb21lKChpdGVtOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgcmV0dXJuIGl0ZW0udG9Mb3dlckNhc2UoKSA9PT0gbGluZS50b0xvd2VyQ2FzZSgpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgcmVtb3ZlRHVwbGljYXRlTGluZXMobGluZXM6IHN0cmluZ1tdKTogc3RyaW5nW10ge1xyXG4gICAgICAgIGNvbnN0IHJlZHVjZXIgPSAoYWNjOiBzdHJpbmdbXSwgbGluZTogc3RyaW5nKTogc3RyaW5nW10gPT4ge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuY29udGFpbnNMaW5lKGFjYywgbGluZSkpIHsgYWNjLnB1c2gobGluZSk7IH1cclxuICAgICAgICAgICAgcmV0dXJuIGFjYztcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICByZXR1cm4gbGluZXMucmVkdWNlKHJlZHVjZXIsIFtdKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldE5vZGVUb29sdGlwKG5vZGU6IE5vZGVFbnRyeSk6IHN0cmluZyB7XHJcbiAgICAgICAgaWYgKCFub2RlIHx8ICFub2RlLmVudHJ5KSB7XHJcbiAgICAgICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgeyBlbnRyeTogeyBwcm9wZXJ0aWVzLCBuYW1lIH0gfSA9IG5vZGU7XHJcbiAgICAgICAgY29uc3QgbGluZXMgPSBbIG5hbWUgXTtcclxuXHJcbiAgICAgICAgaWYgKHByb3BlcnRpZXMpIHtcclxuICAgICAgICAgICAgY29uc3Qge1xyXG4gICAgICAgICAgICAgICAgJ2NtOnRpdGxlJzogdGl0bGUsXHJcbiAgICAgICAgICAgICAgICAnY206ZGVzY3JpcHRpb24nOiBkZXNjcmlwdGlvblxyXG4gICAgICAgICAgICB9ID0gcHJvcGVydGllcztcclxuXHJcbiAgICAgICAgICAgIGlmICh0aXRsZSAmJiBkZXNjcmlwdGlvbikge1xyXG4gICAgICAgICAgICAgICAgbGluZXNbMF0gPSB0aXRsZTtcclxuICAgICAgICAgICAgICAgIGxpbmVzWzFdID0gZGVzY3JpcHRpb247XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmICh0aXRsZSkge1xyXG4gICAgICAgICAgICAgICAgbGluZXNbMV0gPSB0aXRsZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgaWYgKGRlc2NyaXB0aW9uKSB7XHJcbiAgICAgICAgICAgICAgICBsaW5lc1sxXSA9IGRlc2NyaXB0aW9uO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5yZW1vdmVEdXBsaWNhdGVMaW5lcyhsaW5lcykuam9pbihgXFxuYCk7XHJcbiAgICB9XHJcbn1cclxuIl19