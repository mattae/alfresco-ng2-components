/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export { FileSizePipe } from './file-size.pipe';
export { MimeTypeIconPipe } from './mime-type-icon.pipe';
export { NodeNameTooltipPipe } from './node-name-tooltip.pipe';
export { HighlightPipe } from './text-highlight.pipe';
export { TimeAgoPipe } from './time-ago.pipe';
export { InitialUsernamePipe } from './user-initial.pipe';
export { FullNamePipe } from './full-name.pipe';
export { MultiValuePipe } from './multi-value.pipe';
export { LocalizedDatePipe } from './localized-date.pipe';
export { DecimalNumberPipe } from './decimal-number.pipe';
export { PipeModule } from './pipe.module';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInBpcGVzL3B1YmxpYy1hcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsNkJBQWMsa0JBQWtCLENBQUM7QUFDakMsaUNBQWMsdUJBQXVCLENBQUM7QUFDdEMsb0NBQWMsMEJBQTBCLENBQUM7QUFDekMsOEJBQWMsdUJBQXVCLENBQUM7QUFDdEMsNEJBQWMsaUJBQWlCLENBQUM7QUFDaEMsb0NBQWMscUJBQXFCLENBQUM7QUFDcEMsNkJBQWMsa0JBQWtCLENBQUM7QUFDakMsK0JBQWMsb0JBQW9CLENBQUM7QUFDbkMsa0NBQWMsdUJBQXVCLENBQUM7QUFDdEMsa0NBQWMsdUJBQXVCLENBQUM7QUFFdEMsMkJBQWMsZUFBZSxDQUFDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmV4cG9ydCAqIGZyb20gJy4vZmlsZS1zaXplLnBpcGUnO1xyXG5leHBvcnQgKiBmcm9tICcuL21pbWUtdHlwZS1pY29uLnBpcGUnO1xyXG5leHBvcnQgKiBmcm9tICcuL25vZGUtbmFtZS10b29sdGlwLnBpcGUnO1xyXG5leHBvcnQgKiBmcm9tICcuL3RleHQtaGlnaGxpZ2h0LnBpcGUnO1xyXG5leHBvcnQgKiBmcm9tICcuL3RpbWUtYWdvLnBpcGUnO1xyXG5leHBvcnQgKiBmcm9tICcuL3VzZXItaW5pdGlhbC5waXBlJztcclxuZXhwb3J0ICogZnJvbSAnLi9mdWxsLW5hbWUucGlwZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vbXVsdGktdmFsdWUucGlwZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vbG9jYWxpemVkLWRhdGUucGlwZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vZGVjaW1hbC1udW1iZXIucGlwZSc7XHJcblxyXG5leHBvcnQgKiBmcm9tICcuL3BpcGUubW9kdWxlJztcclxuIl19