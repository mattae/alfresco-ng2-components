/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import moment from 'moment-es6';
import { Pipe } from '@angular/core';
import { AppConfigService } from '../app-config/app-config.service';
import { UserPreferenceValues, UserPreferencesService } from '../services/user-preferences.service';
import { DatePipe } from '@angular/common';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
var TimeAgoPipe = /** @class */ (function () {
    function TimeAgoPipe(userPreferenceService, appConfig) {
        var _this = this;
        this.userPreferenceService = userPreferenceService;
        this.appConfig = appConfig;
        this.onDestroy$ = new Subject();
        this.userPreferenceService
            .select(UserPreferenceValues.Locale)
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} locale
         * @return {?}
         */
        function (locale) {
            _this.defaultLocale = locale || TimeAgoPipe.DEFAULT_LOCALE;
        }));
        this.defaultDateTimeFormat = this.appConfig.get('dateValues.defaultDateTimeFormat', TimeAgoPipe.DEFAULT_DATE_TIME_FORMAT);
    }
    /**
     * @param {?} value
     * @param {?=} locale
     * @return {?}
     */
    TimeAgoPipe.prototype.transform = /**
     * @param {?} value
     * @param {?=} locale
     * @return {?}
     */
    function (value, locale) {
        if (value !== null && value !== undefined) {
            /** @type {?} */
            var actualLocale = locale || this.defaultLocale;
            /** @type {?} */
            var then = moment(value);
            /** @type {?} */
            var diff = moment().locale(actualLocale).diff(then, 'days');
            if (diff > 7) {
                /** @type {?} */
                var datePipe = new DatePipe(actualLocale);
                return datePipe.transform(value, this.defaultDateTimeFormat);
            }
            else {
                return then.locale(actualLocale).fromNow();
            }
        }
        return '';
    };
    /**
     * @return {?}
     */
    TimeAgoPipe.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    };
    TimeAgoPipe.DEFAULT_LOCALE = 'en-US';
    TimeAgoPipe.DEFAULT_DATE_TIME_FORMAT = 'dd/MM/yyyy HH:mm';
    TimeAgoPipe.decorators = [
        { type: Pipe, args: [{
                    name: 'adfTimeAgo'
                },] }
    ];
    /** @nocollapse */
    TimeAgoPipe.ctorParameters = function () { return [
        { type: UserPreferencesService },
        { type: AppConfigService }
    ]; };
    return TimeAgoPipe;
}());
export { TimeAgoPipe };
if (false) {
    /** @type {?} */
    TimeAgoPipe.DEFAULT_LOCALE;
    /** @type {?} */
    TimeAgoPipe.DEFAULT_DATE_TIME_FORMAT;
    /** @type {?} */
    TimeAgoPipe.prototype.defaultLocale;
    /** @type {?} */
    TimeAgoPipe.prototype.defaultDateTimeFormat;
    /**
     * @type {?}
     * @private
     */
    TimeAgoPipe.prototype.onDestroy$;
    /** @type {?} */
    TimeAgoPipe.prototype.userPreferenceService;
    /** @type {?} */
    TimeAgoPipe.prototype.appConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGltZS1hZ28ucGlwZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInBpcGVzL3RpbWUtYWdvLnBpcGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxNQUFNLE1BQU0sWUFBWSxDQUFDO0FBQ2hDLE9BQU8sRUFBRSxJQUFJLEVBQTRCLE1BQU0sZUFBZSxDQUFDO0FBQy9ELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQ3BHLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUMzQyxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQy9CLE9BQU8sRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUUzQztJQWFJLHFCQUFtQixxQkFBNkMsRUFDN0MsU0FBMkI7UUFEOUMsaUJBU0M7UUFUa0IsMEJBQXFCLEdBQXJCLHFCQUFxQixDQUF3QjtRQUM3QyxjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQUh0QyxlQUFVLEdBQUcsSUFBSSxPQUFPLEVBQVcsQ0FBQztRQUl4QyxJQUFJLENBQUMscUJBQXFCO2FBQ3JCLE1BQU0sQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUM7YUFDbkMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7YUFDaEMsU0FBUzs7OztRQUFDLFVBQUEsTUFBTTtZQUNiLEtBQUksQ0FBQyxhQUFhLEdBQUcsTUFBTSxJQUFJLFdBQVcsQ0FBQyxjQUFjLENBQUM7UUFDOUQsQ0FBQyxFQUFDLENBQUM7UUFDUCxJQUFJLENBQUMscUJBQXFCLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQVMsa0NBQWtDLEVBQUUsV0FBVyxDQUFDLHdCQUF3QixDQUFDLENBQUM7SUFDdEksQ0FBQzs7Ozs7O0lBRUQsK0JBQVM7Ozs7O0lBQVQsVUFBVSxLQUFXLEVBQUUsTUFBZTtRQUNsQyxJQUFJLEtBQUssS0FBSyxJQUFJLElBQUksS0FBSyxLQUFLLFNBQVMsRUFBRzs7Z0JBQ2xDLFlBQVksR0FBRyxNQUFNLElBQUksSUFBSSxDQUFDLGFBQWE7O2dCQUMzQyxJQUFJLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQzs7Z0JBQ3BCLElBQUksR0FBRyxNQUFNLEVBQUUsQ0FBQyxNQUFNLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSSxDQUFDLElBQUksRUFBRSxNQUFNLENBQUM7WUFDN0QsSUFBSyxJQUFJLEdBQUcsQ0FBQyxFQUFFOztvQkFDTCxRQUFRLEdBQWEsSUFBSSxRQUFRLENBQUMsWUFBWSxDQUFDO2dCQUNyRCxPQUFPLFFBQVEsQ0FBQyxTQUFTLENBQUMsS0FBSyxFQUFFLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO2FBQ2hFO2lCQUFNO2dCQUNILE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQyxPQUFPLEVBQUUsQ0FBQzthQUM5QztTQUNKO1FBQ0QsT0FBTyxFQUFFLENBQUM7SUFDZCxDQUFDOzs7O0lBRUQsaUNBQVc7OztJQUFYO1FBQ0ksSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztJQUMvQixDQUFDO0lBckNNLDBCQUFjLEdBQUcsT0FBTyxDQUFDO0lBQ3pCLG9DQUF3QixHQUFHLGtCQUFrQixDQUFDOztnQkFOeEQsSUFBSSxTQUFDO29CQUNGLElBQUksRUFBRSxZQUFZO2lCQUNyQjs7OztnQkFQOEIsc0JBQXNCO2dCQUQ1QyxnQkFBZ0I7O0lBaUR6QixrQkFBQztDQUFBLEFBM0NELElBMkNDO1NBeENZLFdBQVc7OztJQUVwQiwyQkFBZ0M7O0lBQ2hDLHFDQUFxRDs7SUFFckQsb0NBQXNCOztJQUN0Qiw0Q0FBOEI7Ozs7O0lBRTlCLGlDQUE0Qzs7SUFFaEMsNENBQW9EOztJQUNwRCxnQ0FBa0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IG1vbWVudCBmcm9tICdtb21lbnQtZXM2JztcclxuaW1wb3J0IHsgUGlwZSwgUGlwZVRyYW5zZm9ybSwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IFVzZXJQcmVmZXJlbmNlVmFsdWVzLCBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlIH0gZnJvbSAnLi4vc2VydmljZXMvdXNlci1wcmVmZXJlbmNlcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRGF0ZVBpcGUgfSBmcm9tICdAYW5ndWxhci9jb21tb24nO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IHRha2VVbnRpbCB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBQaXBlKHtcclxuICAgIG5hbWU6ICdhZGZUaW1lQWdvJ1xyXG59KVxyXG5leHBvcnQgY2xhc3MgVGltZUFnb1BpcGUgaW1wbGVtZW50cyBQaXBlVHJhbnNmb3JtLCBPbkRlc3Ryb3kge1xyXG5cclxuICAgIHN0YXRpYyBERUZBVUxUX0xPQ0FMRSA9ICdlbi1VUyc7XHJcbiAgICBzdGF0aWMgREVGQVVMVF9EQVRFX1RJTUVfRk9STUFUID0gJ2RkL01NL3l5eXkgSEg6bW0nO1xyXG5cclxuICAgIGRlZmF1bHRMb2NhbGU6IHN0cmluZztcclxuICAgIGRlZmF1bHREYXRlVGltZUZvcm1hdDogc3RyaW5nO1xyXG5cclxuICAgIHByaXZhdGUgb25EZXN0cm95JCA9IG5ldyBTdWJqZWN0PGJvb2xlYW4+KCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHVibGljIHVzZXJQcmVmZXJlbmNlU2VydmljZTogVXNlclByZWZlcmVuY2VzU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBhcHBDb25maWc6IEFwcENvbmZpZ1NlcnZpY2UpIHtcclxuICAgICAgICB0aGlzLnVzZXJQcmVmZXJlbmNlU2VydmljZVxyXG4gICAgICAgICAgICAuc2VsZWN0KFVzZXJQcmVmZXJlbmNlVmFsdWVzLkxvY2FsZSlcclxuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMub25EZXN0cm95JCkpXHJcbiAgICAgICAgICAgIC5zdWJzY3JpYmUobG9jYWxlID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZGVmYXVsdExvY2FsZSA9IGxvY2FsZSB8fCBUaW1lQWdvUGlwZS5ERUZBVUxUX0xPQ0FMRTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgdGhpcy5kZWZhdWx0RGF0ZVRpbWVGb3JtYXQgPSB0aGlzLmFwcENvbmZpZy5nZXQ8c3RyaW5nPignZGF0ZVZhbHVlcy5kZWZhdWx0RGF0ZVRpbWVGb3JtYXQnLCBUaW1lQWdvUGlwZS5ERUZBVUxUX0RBVEVfVElNRV9GT1JNQVQpO1xyXG4gICAgfVxyXG5cclxuICAgIHRyYW5zZm9ybSh2YWx1ZTogRGF0ZSwgbG9jYWxlPzogc3RyaW5nKSB7XHJcbiAgICAgICAgaWYgKHZhbHVlICE9PSBudWxsICYmIHZhbHVlICE9PSB1bmRlZmluZWQgKSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGFjdHVhbExvY2FsZSA9IGxvY2FsZSB8fCB0aGlzLmRlZmF1bHRMb2NhbGU7XHJcbiAgICAgICAgICAgIGNvbnN0IHRoZW4gPSBtb21lbnQodmFsdWUpO1xyXG4gICAgICAgICAgICBjb25zdCBkaWZmID0gbW9tZW50KCkubG9jYWxlKGFjdHVhbExvY2FsZSkuZGlmZih0aGVuLCAnZGF5cycpO1xyXG4gICAgICAgICAgICBpZiAoIGRpZmYgPiA3KSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBkYXRlUGlwZTogRGF0ZVBpcGUgPSBuZXcgRGF0ZVBpcGUoYWN0dWFsTG9jYWxlKTtcclxuICAgICAgICAgICAgICAgIHJldHVybiBkYXRlUGlwZS50cmFuc2Zvcm0odmFsdWUsIHRoaXMuZGVmYXVsdERhdGVUaW1lRm9ybWF0KTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGVuLmxvY2FsZShhY3R1YWxMb2NhbGUpLmZyb21Ob3coKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gJyc7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkRlc3Ryb3koKSB7XHJcbiAgICAgICAgdGhpcy5vbkRlc3Ryb3kkLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgdGhpcy5vbkRlc3Ryb3kkLmNvbXBsZXRlKCk7XHJcbiAgICB9XHJcbn1cclxuIl19