/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { LogService } from './log.service';
import { map, catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./log.service";
var AppsProcessService = /** @class */ (function () {
    function AppsProcessService(apiService, logService) {
        this.apiService = apiService;
        this.logService = logService;
    }
    /**
     * Gets a list of deployed apps for this user.
     * @returns The list of deployed apps
     */
    /**
     * Gets a list of deployed apps for this user.
     * @return {?} The list of deployed apps
     */
    AppsProcessService.prototype.getDeployedApplications = /**
     * Gets a list of deployed apps for this user.
     * @return {?} The list of deployed apps
     */
    function () {
        var _this = this;
        return from(this.apiService.getInstance().activiti.appsApi.getAppDefinitions())
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) { return (/** @type {?} */ (response.data)); })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets a list of deployed apps for this user, where the app name is `name`.
     * @param name Name of the app
     * @returns The list of deployed apps
     */
    /**
     * Gets a list of deployed apps for this user, where the app name is `name`.
     * @param {?} name Name of the app
     * @return {?} The list of deployed apps
     */
    AppsProcessService.prototype.getDeployedApplicationsByName = /**
     * Gets a list of deployed apps for this user, where the app name is `name`.
     * @param {?} name Name of the app
     * @return {?} The list of deployed apps
     */
    function (name) {
        var _this = this;
        return from(this.apiService.getInstance().activiti.appsApi.getAppDefinitions())
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) { return (/** @type {?} */ (response.data.find((/**
         * @param {?} app
         * @return {?}
         */
        function (app) { return app.name === name; })))); })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets the details for a specific app ID number.
     * @param appId ID of the target app
     * @returns Details of the app
     */
    /**
     * Gets the details for a specific app ID number.
     * @param {?} appId ID of the target app
     * @return {?} Details of the app
     */
    AppsProcessService.prototype.getApplicationDetailsById = /**
     * Gets the details for a specific app ID number.
     * @param {?} appId ID of the target app
     * @return {?} Details of the app
     */
    function (appId) {
        var _this = this;
        return from(this.apiService.getInstance().activiti.appsApi.getAppDefinitions())
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) { return response.data.find((/**
         * @param {?} app
         * @return {?}
         */
        function (app) { return app.id === appId; })); })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * @private
     * @param {?} error
     * @return {?}
     */
    AppsProcessService.prototype.handleError = /**
     * @private
     * @param {?} error
     * @return {?}
     */
    function (error) {
        this.logService.error(error);
        return throwError(error || 'Server error');
    };
    AppsProcessService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    AppsProcessService.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: LogService }
    ]; };
    /** @nocollapse */ AppsProcessService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AppsProcessService_Factory() { return new AppsProcessService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: AppsProcessService, providedIn: "root" });
    return AppsProcessService;
}());
export { AppsProcessService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    AppsProcessService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    AppsProcessService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXBwcy1wcm9jZXNzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9hcHBzLXByb2Nlc3Muc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTNDLE9BQU8sRUFBYyxJQUFJLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3BELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7OztBQUVqRDtJQUtJLDRCQUFvQixVQUE4QixFQUM5QixVQUFzQjtRQUR0QixlQUFVLEdBQVYsVUFBVSxDQUFvQjtRQUM5QixlQUFVLEdBQVYsVUFBVSxDQUFZO0lBQzFDLENBQUM7SUFFRDs7O09BR0c7Ozs7O0lBQ0gsb0RBQXVCOzs7O0lBQXZCO1FBQUEsaUJBTUM7UUFMRyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsaUJBQWlCLEVBQUUsQ0FBQzthQUMxRSxJQUFJLENBQ0QsR0FBRzs7OztRQUFDLFVBQUMsUUFBYSxXQUFLLG1CQUFnQyxRQUFRLENBQUMsSUFBSSxFQUFBLEdBQUEsRUFBQyxFQUNyRSxVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsMERBQTZCOzs7OztJQUE3QixVQUE4QixJQUFZO1FBQTFDLGlCQU1DO1FBTEcsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLGlCQUFpQixFQUFFLENBQUM7YUFDMUUsSUFBSSxDQUNELEdBQUc7Ozs7UUFBQyxVQUFDLFFBQWEsV0FBSyxtQkFBOEIsUUFBUSxDQUFDLElBQUksQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxHQUFHLENBQUMsSUFBSSxLQUFLLElBQUksRUFBakIsQ0FBaUIsRUFBQyxFQUFBLEdBQUEsRUFBQyxFQUNwRyxVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsc0RBQXlCOzs7OztJQUF6QixVQUEwQixLQUFhO1FBQXZDLGlCQU1DO1FBTEcsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLGlCQUFpQixFQUFFLENBQUM7YUFDMUUsSUFBSSxDQUNELEdBQUc7Ozs7UUFBQyxVQUFDLFFBQWEsSUFBSyxPQUFBLFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSTs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsR0FBRyxDQUFDLEVBQUUsS0FBSyxLQUFLLEVBQWhCLENBQWdCLEVBQUMsRUFBN0MsQ0FBNkMsRUFBQyxFQUNyRSxVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7Ozs7SUFFTyx3Q0FBVzs7Ozs7SUFBbkIsVUFBb0IsS0FBVTtRQUMxQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixPQUFPLFVBQVUsQ0FBQyxLQUFLLElBQUksY0FBYyxDQUFDLENBQUM7SUFDL0MsQ0FBQzs7Z0JBbERKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7Z0JBTlEsa0JBQWtCO2dCQUNsQixVQUFVOzs7NkJBckJuQjtDQTRFQyxBQXBERCxJQW9EQztTQWpEWSxrQkFBa0I7Ozs7OztJQUVmLHdDQUFzQzs7Ozs7SUFDdEMsd0NBQThCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQXBwRGVmaW5pdGlvblJlcHJlc2VudGF0aW9uIH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZyb20sIHRocm93RXJyb3IgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgbWFwLCBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBBcHBzUHJvY2Vzc1NlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgbGlzdCBvZiBkZXBsb3llZCBhcHBzIGZvciB0aGlzIHVzZXIuXHJcbiAgICAgKiBAcmV0dXJucyBUaGUgbGlzdCBvZiBkZXBsb3llZCBhcHBzXHJcbiAgICAgKi9cclxuICAgIGdldERlcGxveWVkQXBwbGljYXRpb25zKCk6IE9ic2VydmFibGU8QXBwRGVmaW5pdGlvblJlcHJlc2VudGF0aW9uW10+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5hY3Rpdml0aS5hcHBzQXBpLmdldEFwcERlZmluaXRpb25zKCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKChyZXNwb25zZTogYW55KSA9PiA8QXBwRGVmaW5pdGlvblJlcHJlc2VudGF0aW9uW10+IHJlc3BvbnNlLmRhdGEpLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgbGlzdCBvZiBkZXBsb3llZCBhcHBzIGZvciB0aGlzIHVzZXIsIHdoZXJlIHRoZSBhcHAgbmFtZSBpcyBgbmFtZWAuXHJcbiAgICAgKiBAcGFyYW0gbmFtZSBOYW1lIG9mIHRoZSBhcHBcclxuICAgICAqIEByZXR1cm5zIFRoZSBsaXN0IG9mIGRlcGxveWVkIGFwcHNcclxuICAgICAqL1xyXG4gICAgZ2V0RGVwbG95ZWRBcHBsaWNhdGlvbnNCeU5hbWUobmFtZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxBcHBEZWZpbml0aW9uUmVwcmVzZW50YXRpb24+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5hY3Rpdml0aS5hcHBzQXBpLmdldEFwcERlZmluaXRpb25zKCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKChyZXNwb25zZTogYW55KSA9PiA8QXBwRGVmaW5pdGlvblJlcHJlc2VudGF0aW9uPiByZXNwb25zZS5kYXRhLmZpbmQoKGFwcCkgPT4gYXBwLm5hbWUgPT09IG5hbWUpKSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgZGV0YWlscyBmb3IgYSBzcGVjaWZpYyBhcHAgSUQgbnVtYmVyLlxyXG4gICAgICogQHBhcmFtIGFwcElkIElEIG9mIHRoZSB0YXJnZXQgYXBwXHJcbiAgICAgKiBAcmV0dXJucyBEZXRhaWxzIG9mIHRoZSBhcHBcclxuICAgICAqL1xyXG4gICAgZ2V0QXBwbGljYXRpb25EZXRhaWxzQnlJZChhcHBJZDogbnVtYmVyKTogT2JzZXJ2YWJsZTxBcHBEZWZpbml0aW9uUmVwcmVzZW50YXRpb24+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5hY3Rpdml0aS5hcHBzQXBpLmdldEFwcERlZmluaXRpb25zKCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKChyZXNwb25zZTogYW55KSA9PiByZXNwb25zZS5kYXRhLmZpbmQoKGFwcCkgPT4gYXBwLmlkID09PSBhcHBJZCkpLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBoYW5kbGVFcnJvcihlcnJvcjogYW55KSB7XHJcbiAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKGVycm9yKTtcclxuICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihlcnJvciB8fCAnU2VydmVyIGVycm9yJyk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==