/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AppConfigValues } from '../app-config/app-config.service';
/**
 * @abstract
 */
var /**
 * @abstract
 */
AuthGuardBase = /** @class */ (function () {
    function AuthGuardBase(authenticationService, router, appConfigService) {
        this.authenticationService = authenticationService;
        this.router = router;
        this.appConfigService = appConfigService;
    }
    Object.defineProperty(AuthGuardBase.prototype, "withCredentials", {
        get: /**
         * @protected
         * @return {?}
         */
        function () {
            return this.appConfigService.get('auth.withCredentials', false);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    AuthGuardBase.prototype.canActivate = /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    function (route, state) {
        return this.checkLogin(route, state.url);
    };
    /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    AuthGuardBase.prototype.canActivateChild = /**
     * @param {?} route
     * @param {?} state
     * @return {?}
     */
    function (route, state) {
        return this.canActivate(route, state);
    };
    /**
     * @protected
     * @param {?} provider
     * @param {?} url
     * @return {?}
     */
    AuthGuardBase.prototype.redirectToUrl = /**
     * @protected
     * @param {?} provider
     * @param {?} url
     * @return {?}
     */
    function (provider, url) {
        this.authenticationService.setRedirect({ provider: provider, url: url });
        /** @type {?} */
        var pathToLogin = this.getLoginRoute();
        /** @type {?} */
        var urlToRedirect = "/" + pathToLogin + "?redirectUrl=" + url;
        this.router.navigateByUrl(urlToRedirect);
    };
    /**
     * @protected
     * @return {?}
     */
    AuthGuardBase.prototype.getLoginRoute = /**
     * @protected
     * @return {?}
     */
    function () {
        return (this.appConfigService &&
            this.appConfigService.get(AppConfigValues.LOGIN_ROUTE, 'login'));
    };
    /**
     * @protected
     * @return {?}
     */
    AuthGuardBase.prototype.isOAuthWithoutSilentLogin = /**
     * @protected
     * @return {?}
     */
    function () {
        /** @type {?} */
        var oauth = this.appConfigService.get(AppConfigValues.OAUTHCONFIG, null);
        return (this.authenticationService.isOauth() && oauth.silentLogin === false);
    };
    return AuthGuardBase;
}());
/**
 * @abstract
 */
export { AuthGuardBase };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    AuthGuardBase.prototype.authenticationService;
    /**
     * @type {?}
     * @protected
     */
    AuthGuardBase.prototype.router;
    /**
     * @type {?}
     * @protected
     */
    AuthGuardBase.prototype.appConfigService;
    /**
     * @abstract
     * @param {?} activeRoute
     * @param {?} redirectUrl
     * @return {?}
     */
    AuthGuardBase.prototype.checkLogin = function (activeRoute, redirectUrl) { };
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1ndWFyZC1iYXNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvYXV0aC1ndWFyZC1iYXNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBMEJBLE9BQU8sRUFFSCxlQUFlLEVBQ2xCLE1BQU0sa0NBQWtDLENBQUM7Ozs7QUFHMUM7Ozs7SUFhSSx1QkFDYyxxQkFBNEMsRUFDNUMsTUFBYyxFQUNkLGdCQUFrQztRQUZsQywwQkFBcUIsR0FBckIscUJBQXFCLENBQXVCO1FBQzVDLFdBQU0sR0FBTixNQUFNLENBQVE7UUFDZCxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO0lBQzdDLENBQUM7SUFYSixzQkFBYywwQ0FBZTs7Ozs7UUFBN0I7WUFDSSxPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQzVCLHNCQUFzQixFQUN0QixLQUFLLENBQ1IsQ0FBQztRQUNOLENBQUM7OztPQUFBOzs7Ozs7SUFRRCxtQ0FBVzs7Ozs7SUFBWCxVQUNJLEtBQTZCLEVBQzdCLEtBQTBCO1FBRTFCLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLEVBQUUsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQzdDLENBQUM7Ozs7OztJQUVELHdDQUFnQjs7Ozs7SUFBaEIsVUFDSSxLQUE2QixFQUM3QixLQUEwQjtRQUUxQixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQzFDLENBQUM7Ozs7Ozs7SUFFUyxxQ0FBYTs7Ozs7O0lBQXZCLFVBQXdCLFFBQWdCLEVBQUUsR0FBVztRQUNqRCxJQUFJLENBQUMscUJBQXFCLENBQUMsV0FBVyxDQUFDLEVBQUUsUUFBUSxVQUFBLEVBQUUsR0FBRyxLQUFBLEVBQUUsQ0FBQyxDQUFDOztZQUVwRCxXQUFXLEdBQUcsSUFBSSxDQUFDLGFBQWEsRUFBRTs7WUFDbEMsYUFBYSxHQUFHLE1BQUksV0FBVyxxQkFBZ0IsR0FBSztRQUUxRCxJQUFJLENBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxhQUFhLENBQUMsQ0FBQztJQUM3QyxDQUFDOzs7OztJQUVTLHFDQUFhOzs7O0lBQXZCO1FBQ0ksT0FBTyxDQUNILElBQUksQ0FBQyxnQkFBZ0I7WUFDckIsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FDckIsZUFBZSxDQUFDLFdBQVcsRUFDM0IsT0FBTyxDQUNWLENBQ0osQ0FBQztJQUNOLENBQUM7Ozs7O0lBRVMsaURBQXlCOzs7O0lBQW5DOztZQUNVLEtBQUssR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUNuQyxlQUFlLENBQUMsV0FBVyxFQUMzQixJQUFJLENBQ1A7UUFDRCxPQUFPLENBQ0gsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sRUFBRSxJQUFJLEtBQUssQ0FBQyxXQUFXLEtBQUssS0FBSyxDQUN0RSxDQUFDO0lBQ04sQ0FBQztJQUNMLG9CQUFDO0FBQUQsQ0FBQyxBQTdERCxJQTZEQzs7Ozs7Ozs7OztJQS9DTyw4Q0FBc0Q7Ozs7O0lBQ3RELCtCQUF3Qjs7Ozs7SUFDeEIseUNBQTRDOzs7Ozs7O0lBZmhELDZFQUdvRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQge1xyXG4gICAgUm91dGVyLFxyXG4gICAgQ2FuQWN0aXZhdGUsXHJcbiAgICBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LFxyXG4gICAgUm91dGVyU3RhdGVTbmFwc2hvdCxcclxuICAgIENhbkFjdGl2YXRlQ2hpbGRcclxufSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEF1dGhlbnRpY2F0aW9uU2VydmljZSB9IGZyb20gJy4vYXV0aGVudGljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7XHJcbiAgICBBcHBDb25maWdTZXJ2aWNlLFxyXG4gICAgQXBwQ29uZmlnVmFsdWVzXHJcbn0gZnJvbSAnLi4vYXBwLWNvbmZpZy9hcHAtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBPYXV0aENvbmZpZ01vZGVsIH0gZnJvbSAnLi4vbW9kZWxzL29hdXRoLWNvbmZpZy5tb2RlbCc7XHJcblxyXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgQXV0aEd1YXJkQmFzZSBpbXBsZW1lbnRzIENhbkFjdGl2YXRlLCBDYW5BY3RpdmF0ZUNoaWxkIHtcclxuICAgIGFic3RyYWN0IGNoZWNrTG9naW4oXHJcbiAgICAgICAgYWN0aXZlUm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsXHJcbiAgICAgICAgcmVkaXJlY3RVcmw6IHN0cmluZ1xyXG4gICAgKTogT2JzZXJ2YWJsZTxib29sZWFuPiB8IFByb21pc2U8Ym9vbGVhbj4gfCBib29sZWFuO1xyXG5cclxuICAgIHByb3RlY3RlZCBnZXQgd2l0aENyZWRlbnRpYWxzKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwcENvbmZpZ1NlcnZpY2UuZ2V0PGJvb2xlYW4+KFxyXG4gICAgICAgICAgICAnYXV0aC53aXRoQ3JlZGVudGlhbHMnLFxyXG4gICAgICAgICAgICBmYWxzZVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJvdGVjdGVkIGF1dGhlbnRpY2F0aW9uU2VydmljZTogQXV0aGVudGljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgIHByb3RlY3RlZCByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICBwcm90ZWN0ZWQgYXBwQ29uZmlnU2VydmljZTogQXBwQ29uZmlnU2VydmljZVxyXG4gICAgKSB7fVxyXG5cclxuICAgIGNhbkFjdGl2YXRlKFxyXG4gICAgICAgIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LFxyXG4gICAgICAgIHN0YXRlOiBSb3V0ZXJTdGF0ZVNuYXBzaG90XHJcbiAgICApOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHwgUHJvbWlzZTxib29sZWFuPiB8IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNoZWNrTG9naW4ocm91dGUsIHN0YXRlLnVybCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2FuQWN0aXZhdGVDaGlsZChcclxuICAgICAgICByb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCxcclxuICAgICAgICBzdGF0ZTogUm91dGVyU3RhdGVTbmFwc2hvdFxyXG4gICAgKTogT2JzZXJ2YWJsZTxib29sZWFuPiB8IFByb21pc2U8Ym9vbGVhbj4gfCBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jYW5BY3RpdmF0ZShyb3V0ZSwgc3RhdGUpO1xyXG4gICAgfVxyXG5cclxuICAgIHByb3RlY3RlZCByZWRpcmVjdFRvVXJsKHByb3ZpZGVyOiBzdHJpbmcsIHVybDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5hdXRoZW50aWNhdGlvblNlcnZpY2Uuc2V0UmVkaXJlY3QoeyBwcm92aWRlciwgdXJsIH0pO1xyXG5cclxuICAgICAgICBjb25zdCBwYXRoVG9Mb2dpbiA9IHRoaXMuZ2V0TG9naW5Sb3V0ZSgpO1xyXG4gICAgICAgIGNvbnN0IHVybFRvUmVkaXJlY3QgPSBgLyR7cGF0aFRvTG9naW59P3JlZGlyZWN0VXJsPSR7dXJsfWA7XHJcblxyXG4gICAgICAgIHRoaXMucm91dGVyLm5hdmlnYXRlQnlVcmwodXJsVG9SZWRpcmVjdCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJvdGVjdGVkIGdldExvZ2luUm91dGUoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gKFxyXG4gICAgICAgICAgICB0aGlzLmFwcENvbmZpZ1NlcnZpY2UgJiZcclxuICAgICAgICAgICAgdGhpcy5hcHBDb25maWdTZXJ2aWNlLmdldDxzdHJpbmc+KFxyXG4gICAgICAgICAgICAgICAgQXBwQ29uZmlnVmFsdWVzLkxPR0lOX1JPVVRFLFxyXG4gICAgICAgICAgICAgICAgJ2xvZ2luJ1xyXG4gICAgICAgICAgICApXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBwcm90ZWN0ZWQgaXNPQXV0aFdpdGhvdXRTaWxlbnRMb2dpbigpIHtcclxuICAgICAgICBjb25zdCBvYXV0aCA9IHRoaXMuYXBwQ29uZmlnU2VydmljZS5nZXQ8T2F1dGhDb25maWdNb2RlbD4oXHJcbiAgICAgICAgICAgIEFwcENvbmZpZ1ZhbHVlcy5PQVVUSENPTkZJRyxcclxuICAgICAgICAgICAgbnVsbFxyXG4gICAgICAgICk7XHJcbiAgICAgICAgcmV0dXJuIChcclxuICAgICAgICAgICAgdGhpcy5hdXRoZW50aWNhdGlvblNlcnZpY2UuaXNPYXV0aCgpICYmIG9hdXRoLnNpbGVudExvZ2luID09PSBmYWxzZVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19