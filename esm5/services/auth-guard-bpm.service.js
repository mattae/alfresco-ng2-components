/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AppConfigService } from '../app-config/app-config.service';
import { AuthenticationService } from './authentication.service';
import { AuthGuardBase } from './auth-guard-base';
import * as i0 from "@angular/core";
import * as i1 from "./authentication.service";
import * as i2 from "@angular/router";
import * as i3 from "../app-config/app-config.service";
var AuthGuardBpm = /** @class */ (function (_super) {
    tslib_1.__extends(AuthGuardBpm, _super);
    function AuthGuardBpm(authenticationService, router, appConfigService) {
        return _super.call(this, authenticationService, router, appConfigService) || this;
    }
    /**
     * @param {?} activeRoute
     * @param {?} redirectUrl
     * @return {?}
     */
    AuthGuardBpm.prototype.checkLogin = /**
     * @param {?} activeRoute
     * @param {?} redirectUrl
     * @return {?}
     */
    function (activeRoute, redirectUrl) {
        if (this.authenticationService.isBpmLoggedIn() || this.withCredentials) {
            return true;
        }
        if (!this.authenticationService.isOauth() || this.isOAuthWithoutSilentLogin()) {
            this.redirectToUrl('BPM', redirectUrl);
        }
        return false;
    };
    AuthGuardBpm.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    AuthGuardBpm.ctorParameters = function () { return [
        { type: AuthenticationService },
        { type: Router },
        { type: AppConfigService }
    ]; };
    /** @nocollapse */ AuthGuardBpm.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AuthGuardBpm_Factory() { return new AuthGuardBpm(i0.ɵɵinject(i1.AuthenticationService), i0.ɵɵinject(i2.Router), i0.ɵɵinject(i3.AppConfigService)); }, token: AuthGuardBpm, providedIn: "root" });
    return AuthGuardBpm;
}(AuthGuardBase));
export { AuthGuardBpm };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1ndWFyZC1icG0uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2F1dGgtZ3VhcmQtYnBtLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUEwQixNQUFNLEVBQUUsTUFBTSxpQkFBaUIsQ0FBQztBQUNqRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUNwRSxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNqRSxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7Ozs7O0FBR2xEO0lBR2tDLHdDQUFhO0lBRTNDLHNCQUFZLHFCQUE0QyxFQUM1QyxNQUFjLEVBQ2QsZ0JBQWtDO2VBQzFDLGtCQUFNLHFCQUFxQixFQUFFLE1BQU0sRUFBRSxnQkFBZ0IsQ0FBQztJQUMxRCxDQUFDOzs7Ozs7SUFFRCxpQ0FBVTs7Ozs7SUFBVixVQUFXLFdBQW1DLEVBQUUsV0FBbUI7UUFDL0QsSUFBSSxJQUFJLENBQUMscUJBQXFCLENBQUMsYUFBYSxFQUFFLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUNwRSxPQUFPLElBQUksQ0FBQztTQUNmO1FBRUQsSUFBSSxDQUFDLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMseUJBQXlCLEVBQUUsRUFBRTtZQUMzRSxJQUFJLENBQUMsYUFBYSxDQUFDLEtBQUssRUFBRSxXQUFXLENBQUMsQ0FBQztTQUMxQztRQUVELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7O2dCQXJCSixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7O2dCQU5RLHFCQUFxQjtnQkFGRyxNQUFNO2dCQUM5QixnQkFBZ0I7Ozt1QkFuQnpCO0NBOENDLEFBdEJELENBR2tDLGFBQWEsR0FtQjlDO1NBbkJZLFlBQVkiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZVNuYXBzaG90LCBSb3V0ZXIgfSBmcm9tICdAYW5ndWxhci9yb3V0ZXInO1xyXG5pbXBvcnQgeyBBcHBDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vYXBwLWNvbmZpZy9hcHAtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBdXRoZW50aWNhdGlvblNlcnZpY2UgfSBmcm9tICcuL2F1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBdXRoR3VhcmRCYXNlIH0gZnJvbSAnLi9hdXRoLWd1YXJkLWJhc2UnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlIH0gZnJvbSAncnhqcyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEF1dGhHdWFyZEJwbSBleHRlbmRzIEF1dGhHdWFyZEJhc2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKGF1dGhlbnRpY2F0aW9uU2VydmljZTogQXV0aGVudGljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgICAgICAgICBhcHBDb25maWdTZXJ2aWNlOiBBcHBDb25maWdTZXJ2aWNlKSB7XHJcbiAgICAgICAgc3VwZXIoYXV0aGVudGljYXRpb25TZXJ2aWNlLCByb3V0ZXIsIGFwcENvbmZpZ1NlcnZpY2UpO1xyXG4gICAgfVxyXG5cclxuICAgIGNoZWNrTG9naW4oYWN0aXZlUm91dGU6IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIHJlZGlyZWN0VXJsOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHwgUHJvbWlzZTxib29sZWFuPiB8IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLmF1dGhlbnRpY2F0aW9uU2VydmljZS5pc0JwbUxvZ2dlZEluKCkgfHwgdGhpcy53aXRoQ3JlZGVudGlhbHMpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXRoaXMuYXV0aGVudGljYXRpb25TZXJ2aWNlLmlzT2F1dGgoKSB8fCB0aGlzLmlzT0F1dGhXaXRob3V0U2lsZW50TG9naW4oKSkge1xyXG4gICAgICAgICAgICB0aGlzLnJlZGlyZWN0VG9VcmwoJ0JQTScsIHJlZGlyZWN0VXJsKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxufVxyXG4iXX0=