/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { AppConfigService } from '../app-config/app-config.service';
import { AuthGuardBase } from './auth-guard-base';
import * as i0 from "@angular/core";
import * as i1 from "./authentication.service";
import * as i2 from "@angular/router";
import * as i3 from "../app-config/app-config.service";
var AuthGuardEcm = /** @class */ (function (_super) {
    tslib_1.__extends(AuthGuardEcm, _super);
    function AuthGuardEcm(authenticationService, router, appConfigService) {
        return _super.call(this, authenticationService, router, appConfigService) || this;
    }
    /**
     * @param {?} activeRoute
     * @param {?} redirectUrl
     * @return {?}
     */
    AuthGuardEcm.prototype.checkLogin = /**
     * @param {?} activeRoute
     * @param {?} redirectUrl
     * @return {?}
     */
    function (activeRoute, redirectUrl) {
        if (this.authenticationService.isEcmLoggedIn() || this.withCredentials) {
            return true;
        }
        if (!this.authenticationService.isOauth() || this.isOAuthWithoutSilentLogin()) {
            this.redirectToUrl('ECM', redirectUrl);
        }
        return false;
    };
    AuthGuardEcm.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    AuthGuardEcm.ctorParameters = function () { return [
        { type: AuthenticationService },
        { type: Router },
        { type: AppConfigService }
    ]; };
    /** @nocollapse */ AuthGuardEcm.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AuthGuardEcm_Factory() { return new AuthGuardEcm(i0.ɵɵinject(i1.AuthenticationService), i0.ɵɵinject(i2.Router), i0.ɵɵinject(i3.AppConfigService)); }, token: AuthGuardEcm, providedIn: "root" });
    return AuthGuardEcm;
}(AuthGuardBase));
export { AuthGuardEcm };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1ndWFyZC1lY20uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2F1dGgtZ3VhcmQtZWNtLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUNxQixNQUFNLEVBQ2pDLE1BQU0saUJBQWlCLENBQUM7QUFDekIsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFDakUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDcEUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLG1CQUFtQixDQUFDOzs7OztBQUdsRDtJQUdrQyx3Q0FBYTtJQUUzQyxzQkFBWSxxQkFBNEMsRUFDNUMsTUFBYyxFQUNkLGdCQUFrQztlQUMxQyxrQkFBTSxxQkFBcUIsRUFBRSxNQUFNLEVBQUUsZ0JBQWdCLENBQUM7SUFDMUQsQ0FBQzs7Ozs7O0lBRUQsaUNBQVU7Ozs7O0lBQVYsVUFBVyxXQUFtQyxFQUFFLFdBQW1CO1FBQy9ELElBQUksSUFBSSxDQUFDLHFCQUFxQixDQUFDLGFBQWEsRUFBRSxJQUFJLElBQUksQ0FBQyxlQUFlLEVBQUU7WUFDcEUsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsT0FBTyxFQUFFLElBQUksSUFBSSxDQUFDLHlCQUF5QixFQUFFLEVBQUU7WUFDM0UsSUFBSSxDQUFDLGFBQWEsQ0FBQyxLQUFLLEVBQUUsV0FBVyxDQUFDLENBQUM7U0FDMUM7UUFFRCxPQUFPLEtBQUssQ0FBQztJQUNqQixDQUFDOztnQkFyQkosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFQUSxxQkFBcUI7Z0JBRkYsTUFBTTtnQkFHekIsZ0JBQWdCOzs7dUJBdEJ6QjtDQWdEQyxBQXRCRCxDQUdrQyxhQUFhLEdBbUI5QztTQW5CWSxZQUFZIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHtcclxuICAgIEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIFJvdXRlclxyXG59IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEF1dGhlbnRpY2F0aW9uU2VydmljZSB9IGZyb20gJy4vYXV0aGVudGljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IEF1dGhHdWFyZEJhc2UgfSBmcm9tICcuL2F1dGgtZ3VhcmQtYmFzZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQXV0aEd1YXJkRWNtIGV4dGVuZHMgQXV0aEd1YXJkQmFzZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoYXV0aGVudGljYXRpb25TZXJ2aWNlOiBBdXRoZW50aWNhdGlvblNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICByb3V0ZXI6IFJvdXRlcixcclxuICAgICAgICAgICAgICAgIGFwcENvbmZpZ1NlcnZpY2U6IEFwcENvbmZpZ1NlcnZpY2UpIHtcclxuICAgICAgICBzdXBlcihhdXRoZW50aWNhdGlvblNlcnZpY2UsIHJvdXRlciwgYXBwQ29uZmlnU2VydmljZSk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hlY2tMb2dpbihhY3RpdmVSb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgcmVkaXJlY3RVcmw6IHN0cmluZyk6IE9ic2VydmFibGU8Ym9vbGVhbj4gfCBQcm9taXNlPGJvb2xlYW4+IHwgYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHRoaXMuYXV0aGVudGljYXRpb25TZXJ2aWNlLmlzRWNtTG9nZ2VkSW4oKSB8fCB0aGlzLndpdGhDcmVkZW50aWFscykge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICghdGhpcy5hdXRoZW50aWNhdGlvblNlcnZpY2UuaXNPYXV0aCgpIHx8IHRoaXMuaXNPQXV0aFdpdGhvdXRTaWxlbnRMb2dpbigpKSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVkaXJlY3RUb1VybCgnRUNNJywgcmVkaXJlY3RVcmwpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==