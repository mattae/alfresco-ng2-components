/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { AppConfigService } from '../app-config/app-config.service';
import { AuthGuardBase } from './auth-guard-base';
import { JwtHelperService } from './jwt-helper.service';
import * as i0 from "@angular/core";
import * as i1 from "./jwt-helper.service";
import * as i2 from "./authentication.service";
import * as i3 from "@angular/router";
import * as i4 from "../app-config/app-config.service";
var AuthGuard = /** @class */ (function (_super) {
    tslib_1.__extends(AuthGuard, _super);
    function AuthGuard(jwtHelperService, authenticationService, router, appConfigService) {
        var _this = _super.call(this, authenticationService, router, appConfigService) || this;
        _this.jwtHelperService = jwtHelperService;
        _this.ticketChangeBind = _this.ticketChange.bind(_this);
        window.addEventListener('storage', _this.ticketChangeBind);
        return _this;
    }
    /**
     * @param {?} event
     * @return {?}
     */
    AuthGuard.prototype.ticketChange = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (event.key === 'ticket-ECM' && event.newValue !== event.oldValue) {
            this.ticketChangeRedirect(event, 'ECM');
        }
        if (event.key === 'ticket-BPM' && event.newValue !== event.oldValue) {
            this.ticketChangeRedirect(event, 'BPM');
        }
        if (event.key === JwtHelperService.USER_ACCESS_TOKEN &&
            this.jwtHelperService.getValueFromToken(event.newValue, JwtHelperService.USER_PREFERRED_USERNAME) !==
                this.jwtHelperService.getValueFromToken(event.oldValue, JwtHelperService.USER_PREFERRED_USERNAME)) {
            this.ticketChangeRedirect(event, 'ALL');
        }
    };
    /**
     * @private
     * @param {?} event
     * @param {?} provider
     * @return {?}
     */
    AuthGuard.prototype.ticketChangeRedirect = /**
     * @private
     * @param {?} event
     * @param {?} provider
     * @return {?}
     */
    function (event, provider) {
        if (!event.newValue) {
            this.redirectToUrl(provider, this.router.url);
        }
        else {
            window.location.reload();
        }
        window.removeEventListener('storage', this.ticketChangeBind);
    };
    /**
     * @param {?} activeRoute
     * @param {?} redirectUrl
     * @return {?}
     */
    AuthGuard.prototype.checkLogin = /**
     * @param {?} activeRoute
     * @param {?} redirectUrl
     * @return {?}
     */
    function (activeRoute, redirectUrl) {
        if (this.authenticationService.isLoggedIn() || this.withCredentials) {
            return true;
        }
        if (!this.authenticationService.isOauth() || this.isOAuthWithoutSilentLogin()) {
            this.redirectToUrl('ALL', redirectUrl);
        }
        return false;
    };
    AuthGuard.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    AuthGuard.ctorParameters = function () { return [
        { type: JwtHelperService },
        { type: AuthenticationService },
        { type: Router },
        { type: AppConfigService }
    ]; };
    /** @nocollapse */ AuthGuard.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AuthGuard_Factory() { return new AuthGuard(i0.ɵɵinject(i1.JwtHelperService), i0.ɵɵinject(i2.AuthenticationService), i0.ɵɵinject(i3.Router), i0.ɵɵinject(i4.AppConfigService)); }, token: AuthGuard, providedIn: "root" });
    return AuthGuard;
}(AuthGuardBase));
export { AuthGuard };
if (false) {
    /** @type {?} */
    AuthGuard.prototype.ticketChangeBind;
    /**
     * @type {?}
     * @private
     */
    AuthGuard.prototype.jwtHelperService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aC1ndWFyZC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvYXV0aC1ndWFyZC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBMEIsTUFBTSxFQUFFLE1BQU0saUJBQWlCLENBQUM7QUFDakUsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0sMEJBQTBCLENBQUM7QUFFakUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDcEUsT0FBTyxFQUFFLGFBQWEsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHNCQUFzQixDQUFDOzs7Ozs7QUFFeEQ7SUFHK0IscUNBQWE7SUFJeEMsbUJBQW9CLGdCQUFrQyxFQUMxQyxxQkFBNEMsRUFDNUMsTUFBYyxFQUNkLGdCQUFrQztRQUg5QyxZQUlJLGtCQUFNLHFCQUFxQixFQUFFLE1BQU0sRUFBRSxnQkFBZ0IsQ0FBQyxTQUl6RDtRQVJtQixzQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBS2xELEtBQUksQ0FBQyxnQkFBZ0IsR0FBRyxLQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxLQUFJLENBQUMsQ0FBQztRQUVyRCxNQUFNLENBQUMsZ0JBQWdCLENBQUMsU0FBUyxFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDOztJQUM5RCxDQUFDOzs7OztJQUVELGdDQUFZOzs7O0lBQVosVUFBYSxLQUFtQjtRQUM1QixJQUFJLEtBQUssQ0FBQyxHQUFHLEtBQUssWUFBWSxJQUFJLEtBQUssQ0FBQyxRQUFRLEtBQUssS0FBSyxDQUFDLFFBQVEsRUFBRTtZQUNqRSxJQUFJLENBQUMsb0JBQW9CLENBQUMsS0FBSyxFQUFFLEtBQUssQ0FBQyxDQUFDO1NBQzNDO1FBRUQsSUFBSSxLQUFLLENBQUMsR0FBRyxLQUFLLFlBQVksSUFBSSxLQUFLLENBQUMsUUFBUSxLQUFLLEtBQUssQ0FBQyxRQUFRLEVBQUU7WUFDakUsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztTQUMzQztRQUVELElBQUksS0FBSyxDQUFDLEdBQUcsS0FBSyxnQkFBZ0IsQ0FBQyxpQkFBaUI7WUFDaEQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGlCQUFpQixDQUFDLEtBQUssQ0FBQyxRQUFRLEVBQUUsZ0JBQWdCLENBQUMsdUJBQXVCLENBQUM7Z0JBQ2pHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxpQkFBaUIsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLGdCQUFnQixDQUFDLHVCQUF1QixDQUFDLEVBQUU7WUFDbkcsSUFBSSxDQUFDLG9CQUFvQixDQUFDLEtBQUssRUFBRSxLQUFLLENBQUMsQ0FBQztTQUMzQztJQUNMLENBQUM7Ozs7Ozs7SUFFTyx3Q0FBb0I7Ozs7OztJQUE1QixVQUE2QixLQUFtQixFQUFFLFFBQWdCO1FBQzlELElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFO1lBQ2pCLElBQUksQ0FBQyxhQUFhLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7U0FDakQ7YUFBTTtZQUNILE1BQU0sQ0FBQyxRQUFRLENBQUMsTUFBTSxFQUFFLENBQUM7U0FDNUI7UUFFRCxNQUFNLENBQUMsbUJBQW1CLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxDQUFDO0lBQ2pFLENBQUM7Ozs7OztJQUVELDhCQUFVOzs7OztJQUFWLFVBQVcsV0FBbUMsRUFBRSxXQUFtQjtRQUMvRCxJQUFJLElBQUksQ0FBQyxxQkFBcUIsQ0FBQyxVQUFVLEVBQUUsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ2pFLE9BQU8sSUFBSSxDQUFDO1NBQ2Y7UUFDRCxJQUFJLENBQUMsSUFBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sRUFBRSxJQUFJLElBQUksQ0FBQyx5QkFBeUIsRUFBRSxFQUFFO1lBQzNFLElBQUksQ0FBQyxhQUFhLENBQUMsS0FBSyxFQUFFLFdBQVcsQ0FBQyxDQUFDO1NBQzFDO1FBRUQsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQzs7Z0JBcERKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7Z0JBSlEsZ0JBQWdCO2dCQUpoQixxQkFBcUI7Z0JBREcsTUFBTTtnQkFHOUIsZ0JBQWdCOzs7b0JBckJ6QjtDQThFQyxBQXJERCxDQUcrQixhQUFhLEdBa0QzQztTQWxEWSxTQUFTOzs7SUFFbEIscUNBQXNCOzs7OztJQUVWLHFDQUEwQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFjdGl2YXRlZFJvdXRlU25hcHNob3QsIFJvdXRlciB9IGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XHJcbmltcG9ydCB7IEF1dGhlbnRpY2F0aW9uU2VydmljZSB9IGZyb20gJy4vYXV0aGVudGljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQXBwQ29uZmlnU2VydmljZSB9IGZyb20gJy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQXV0aEd1YXJkQmFzZSB9IGZyb20gJy4vYXV0aC1ndWFyZC1iYXNlJztcclxuaW1wb3J0IHsgSnd0SGVscGVyU2VydmljZSB9IGZyb20gJy4vand0LWhlbHBlci5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQXV0aEd1YXJkIGV4dGVuZHMgQXV0aEd1YXJkQmFzZSB7XHJcblxyXG4gICAgdGlja2V0Q2hhbmdlQmluZDogYW55O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgand0SGVscGVyU2VydmljZTogSnd0SGVscGVyU2VydmljZSxcclxuICAgICAgICAgICAgICAgIGF1dGhlbnRpY2F0aW9uU2VydmljZTogQXV0aGVudGljYXRpb25TZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcm91dGVyOiBSb3V0ZXIsXHJcbiAgICAgICAgICAgICAgICBhcHBDb25maWdTZXJ2aWNlOiBBcHBDb25maWdTZXJ2aWNlKSB7XHJcbiAgICAgICAgc3VwZXIoYXV0aGVudGljYXRpb25TZXJ2aWNlLCByb3V0ZXIsIGFwcENvbmZpZ1NlcnZpY2UpO1xyXG4gICAgICAgIHRoaXMudGlja2V0Q2hhbmdlQmluZCA9IHRoaXMudGlja2V0Q2hhbmdlLmJpbmQodGhpcyk7XHJcblxyXG4gICAgICAgIHdpbmRvdy5hZGRFdmVudExpc3RlbmVyKCdzdG9yYWdlJywgdGhpcy50aWNrZXRDaGFuZ2VCaW5kKTtcclxuICAgIH1cclxuXHJcbiAgICB0aWNrZXRDaGFuZ2UoZXZlbnQ6IFN0b3JhZ2VFdmVudCkge1xyXG4gICAgICAgIGlmIChldmVudC5rZXkgPT09ICd0aWNrZXQtRUNNJyAmJiBldmVudC5uZXdWYWx1ZSAhPT0gZXZlbnQub2xkVmFsdWUpIHtcclxuICAgICAgICAgICAgdGhpcy50aWNrZXRDaGFuZ2VSZWRpcmVjdChldmVudCwgJ0VDTScpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKGV2ZW50LmtleSA9PT0gJ3RpY2tldC1CUE0nICYmIGV2ZW50Lm5ld1ZhbHVlICE9PSBldmVudC5vbGRWYWx1ZSkge1xyXG4gICAgICAgICAgICB0aGlzLnRpY2tldENoYW5nZVJlZGlyZWN0KGV2ZW50LCAnQlBNJyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoZXZlbnQua2V5ID09PSBKd3RIZWxwZXJTZXJ2aWNlLlVTRVJfQUNDRVNTX1RPS0VOICYmXHJcbiAgICAgICAgICAgIHRoaXMuand0SGVscGVyU2VydmljZS5nZXRWYWx1ZUZyb21Ub2tlbihldmVudC5uZXdWYWx1ZSwgSnd0SGVscGVyU2VydmljZS5VU0VSX1BSRUZFUlJFRF9VU0VSTkFNRSkgIT09XHJcbiAgICAgICAgICAgIHRoaXMuand0SGVscGVyU2VydmljZS5nZXRWYWx1ZUZyb21Ub2tlbihldmVudC5vbGRWYWx1ZSwgSnd0SGVscGVyU2VydmljZS5VU0VSX1BSRUZFUlJFRF9VU0VSTkFNRSkpIHtcclxuICAgICAgICAgICAgdGhpcy50aWNrZXRDaGFuZ2VSZWRpcmVjdChldmVudCwgJ0FMTCcpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHRpY2tldENoYW5nZVJlZGlyZWN0KGV2ZW50OiBTdG9yYWdlRXZlbnQsIHByb3ZpZGVyOiBzdHJpbmcpIHtcclxuICAgICAgICBpZiAoIWV2ZW50Lm5ld1ZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMucmVkaXJlY3RUb1VybChwcm92aWRlciwgdGhpcy5yb3V0ZXIudXJsKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICB3aW5kb3cubG9jYXRpb24ucmVsb2FkKCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB3aW5kb3cucmVtb3ZlRXZlbnRMaXN0ZW5lcignc3RvcmFnZScsIHRoaXMudGlja2V0Q2hhbmdlQmluZCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2hlY2tMb2dpbihhY3RpdmVSb3V0ZTogQWN0aXZhdGVkUm91dGVTbmFwc2hvdCwgcmVkaXJlY3RVcmw6IHN0cmluZyk6IE9ic2VydmFibGU8Ym9vbGVhbj4gfCBQcm9taXNlPGJvb2xlYW4+IHwgYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHRoaXMuYXV0aGVudGljYXRpb25TZXJ2aWNlLmlzTG9nZ2VkSW4oKSB8fCB0aGlzLndpdGhDcmVkZW50aWFscykge1xyXG4gICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKCF0aGlzLmF1dGhlbnRpY2F0aW9uU2VydmljZS5pc09hdXRoKCkgfHwgdGhpcy5pc09BdXRoV2l0aG91dFNpbGVudExvZ2luKCkpIHtcclxuICAgICAgICAgICAgdGhpcy5yZWRpcmVjdFRvVXJsKCdBTEwnLCByZWRpcmVjdFVybCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICB9XHJcbn1cclxuIl19