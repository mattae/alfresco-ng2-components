/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { Observable, Subject, from, throwError } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { CookieService } from './cookie.service';
import { LogService } from './log.service';
import { AppConfigService, AppConfigValues } from '../app-config/app-config.service';
import { map, catchError, tap } from 'rxjs/operators';
import { HttpHeaders } from '@angular/common/http';
import { JwtHelperService } from './jwt-helper.service';
import * as i0 from "@angular/core";
import * as i1 from "../app-config/app-config.service";
import * as i2 from "./alfresco-api.service";
import * as i3 from "./cookie.service";
import * as i4 from "./log.service";
/** @type {?} */
var REMEMBER_ME_COOKIE_KEY = 'ALFRESCO_REMEMBER_ME';
/** @type {?} */
var REMEMBER_ME_UNTIL = 1000 * 60 * 60 * 24 * 30;
var AuthenticationService = /** @class */ (function () {
    function AuthenticationService(appConfig, alfrescoApi, cookie, logService) {
        this.appConfig = appConfig;
        this.alfrescoApi = alfrescoApi;
        this.cookie = cookie;
        this.logService = logService;
        this.redirectUrl = null;
        this.bearerExcludedUrls = ['auth/realms', 'resources/', 'assets/'];
        this.onLogin = new Subject();
        this.onLogout = new Subject();
    }
    /**
     * Checks if the user logged in.
     * @returns True if logged in, false otherwise
     */
    /**
     * Checks if the user logged in.
     * @return {?} True if logged in, false otherwise
     */
    AuthenticationService.prototype.isLoggedIn = /**
     * Checks if the user logged in.
     * @return {?} True if logged in, false otherwise
     */
    function () {
        if (!this.isOauth() && this.cookie.isEnabled() && !this.isRememberMeSet()) {
            return false;
        }
        return this.alfrescoApi.getInstance().isLoggedIn();
    };
    /**
     * Does the provider support OAuth?
     * @returns True if supported, false otherwise
     */
    /**
     * Does the provider support OAuth?
     * @return {?} True if supported, false otherwise
     */
    AuthenticationService.prototype.isOauth = /**
     * Does the provider support OAuth?
     * @return {?} True if supported, false otherwise
     */
    function () {
        return this.alfrescoApi.getInstance().isOauthConfiguration();
    };
    /**
     * Does the provider support ECM?
     * @returns True if supported, false otherwise
     */
    /**
     * Does the provider support ECM?
     * @return {?} True if supported, false otherwise
     */
    AuthenticationService.prototype.isECMProvider = /**
     * Does the provider support ECM?
     * @return {?} True if supported, false otherwise
     */
    function () {
        return this.alfrescoApi.getInstance().isEcmConfiguration();
    };
    /**
     * Does the provider support BPM?
     * @returns True if supported, false otherwise
     */
    /**
     * Does the provider support BPM?
     * @return {?} True if supported, false otherwise
     */
    AuthenticationService.prototype.isBPMProvider = /**
     * Does the provider support BPM?
     * @return {?} True if supported, false otherwise
     */
    function () {
        return this.alfrescoApi.getInstance().isBpmConfiguration();
    };
    /**
     * Does the provider support both ECM and BPM?
     * @returns True if both are supported, false otherwise
     */
    /**
     * Does the provider support both ECM and BPM?
     * @return {?} True if both are supported, false otherwise
     */
    AuthenticationService.prototype.isALLProvider = /**
     * Does the provider support both ECM and BPM?
     * @return {?} True if both are supported, false otherwise
     */
    function () {
        return this.alfrescoApi.getInstance().isEcmBpmConfiguration();
    };
    /**
     * Logs the user in.
     * @param username Username for the login
     * @param password Password for the login
     * @param rememberMe Stores the user's login details if true
     * @returns Object with auth type ("ECM", "BPM" or "ALL") and auth ticket
     */
    /**
     * Logs the user in.
     * @param {?} username Username for the login
     * @param {?} password Password for the login
     * @param {?=} rememberMe Stores the user's login details if true
     * @return {?} Object with auth type ("ECM", "BPM" or "ALL") and auth ticket
     */
    AuthenticationService.prototype.login = /**
     * Logs the user in.
     * @param {?} username Username for the login
     * @param {?} password Password for the login
     * @param {?=} rememberMe Stores the user's login details if true
     * @return {?} Object with auth type ("ECM", "BPM" or "ALL") and auth ticket
     */
    function (username, password, rememberMe) {
        var _this = this;
        if (rememberMe === void 0) { rememberMe = false; }
        return from(this.alfrescoApi.getInstance().login(username, password))
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            _this.saveRememberMeCookie(rememberMe);
            _this.onLogin.next(response);
            return {
                type: _this.appConfig.get(AppConfigValues.PROVIDERS),
                ticket: response
            };
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Logs the user in with SSO
     */
    /**
     * Logs the user in with SSO
     * @return {?}
     */
    AuthenticationService.prototype.ssoImplicitLogin = /**
     * Logs the user in with SSO
     * @return {?}
     */
    function () {
        this.alfrescoApi.getInstance().implicitLogin();
    };
    /**
     * Saves the "remember me" cookie as either a long-life cookie or a session cookie.
     * @param rememberMe Enables a long-life cookie
     */
    /**
     * Saves the "remember me" cookie as either a long-life cookie or a session cookie.
     * @private
     * @param {?} rememberMe Enables a long-life cookie
     * @return {?}
     */
    AuthenticationService.prototype.saveRememberMeCookie = /**
     * Saves the "remember me" cookie as either a long-life cookie or a session cookie.
     * @private
     * @param {?} rememberMe Enables a long-life cookie
     * @return {?}
     */
    function (rememberMe) {
        /** @type {?} */
        var expiration = null;
        if (rememberMe) {
            expiration = new Date();
            /** @type {?} */
            var time = expiration.getTime();
            /** @type {?} */
            var expireTime = time + REMEMBER_ME_UNTIL;
            expiration.setTime(expireTime);
        }
        this.cookie.setItem(REMEMBER_ME_COOKIE_KEY, '1', expiration, null);
    };
    /**
     * Checks whether the "remember me" cookie was set or not.
     * @returns True if set, false otherwise
     */
    /**
     * Checks whether the "remember me" cookie was set or not.
     * @return {?} True if set, false otherwise
     */
    AuthenticationService.prototype.isRememberMeSet = /**
     * Checks whether the "remember me" cookie was set or not.
     * @return {?} True if set, false otherwise
     */
    function () {
        return (this.cookie.getItem(REMEMBER_ME_COOKIE_KEY) === null) ? false : true;
    };
    /**
     * Logs the user out.
     * @returns Response event called when logout is complete
     */
    /**
     * Logs the user out.
     * @return {?} Response event called when logout is complete
     */
    AuthenticationService.prototype.logout = /**
     * Logs the user out.
     * @return {?} Response event called when logout is complete
     */
    function () {
        var _this = this;
        return from(this.callApiLogout())
            .pipe(tap((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            _this.onLogout.next(response);
            return response;
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * @private
     * @return {?}
     */
    AuthenticationService.prototype.callApiLogout = /**
     * @private
     * @return {?}
     */
    function () {
        if (this.alfrescoApi.getInstance()) {
            return this.alfrescoApi.getInstance().logout();
        }
    };
    /**
     * Gets the ECM ticket stored in the Storage.
     * @returns The ticket or `null` if none was found
     */
    /**
     * Gets the ECM ticket stored in the Storage.
     * @return {?} The ticket or `null` if none was found
     */
    AuthenticationService.prototype.getTicketEcm = /**
     * Gets the ECM ticket stored in the Storage.
     * @return {?} The ticket or `null` if none was found
     */
    function () {
        return this.alfrescoApi.getInstance().getTicketEcm();
    };
    /**
     * Gets the BPM ticket stored in the Storage.
     * @returns The ticket or `null` if none was found
     */
    /**
     * Gets the BPM ticket stored in the Storage.
     * @return {?} The ticket or `null` if none was found
     */
    AuthenticationService.prototype.getTicketBpm = /**
     * Gets the BPM ticket stored in the Storage.
     * @return {?} The ticket or `null` if none was found
     */
    function () {
        return this.alfrescoApi.getInstance().getTicketBpm();
    };
    /**
     * Gets the BPM ticket from the Storage in Base 64 format.
     * @returns The ticket or `null` if none was found
     */
    /**
     * Gets the BPM ticket from the Storage in Base 64 format.
     * @return {?} The ticket or `null` if none was found
     */
    AuthenticationService.prototype.getTicketEcmBase64 = /**
     * Gets the BPM ticket from the Storage in Base 64 format.
     * @return {?} The ticket or `null` if none was found
     */
    function () {
        /** @type {?} */
        var ticket = this.alfrescoApi.getInstance().getTicketEcm();
        if (ticket) {
            return 'Basic ' + btoa(ticket);
        }
        return null;
    };
    /**
     * Checks if the user is logged in on an ECM provider.
     * @returns True if logged in, false otherwise
     */
    /**
     * Checks if the user is logged in on an ECM provider.
     * @return {?} True if logged in, false otherwise
     */
    AuthenticationService.prototype.isEcmLoggedIn = /**
     * Checks if the user is logged in on an ECM provider.
     * @return {?} True if logged in, false otherwise
     */
    function () {
        if (this.isECMProvider() || this.isALLProvider()) {
            if (!this.isOauth() && this.cookie.isEnabled() && !this.isRememberMeSet()) {
                return false;
            }
            return this.alfrescoApi.getInstance().isEcmLoggedIn();
        }
        return false;
    };
    /**
     * Checks if the user is logged in on a BPM provider.
     * @returns True if logged in, false otherwise
     */
    /**
     * Checks if the user is logged in on a BPM provider.
     * @return {?} True if logged in, false otherwise
     */
    AuthenticationService.prototype.isBpmLoggedIn = /**
     * Checks if the user is logged in on a BPM provider.
     * @return {?} True if logged in, false otherwise
     */
    function () {
        if (this.isBPMProvider() || this.isALLProvider()) {
            if (!this.isOauth() && this.cookie.isEnabled() && !this.isRememberMeSet()) {
                return false;
            }
            return this.alfrescoApi.getInstance().isBpmLoggedIn();
        }
        return false;
    };
    /**
     * Gets the ECM username.
     * @returns The ECM username
     */
    /**
     * Gets the ECM username.
     * @return {?} The ECM username
     */
    AuthenticationService.prototype.getEcmUsername = /**
     * Gets the ECM username.
     * @return {?} The ECM username
     */
    function () {
        return this.alfrescoApi.getInstance().getEcmUsername();
    };
    /**
     * Gets the BPM username
     * @returns The BPM username
     */
    /**
     * Gets the BPM username
     * @return {?} The BPM username
     */
    AuthenticationService.prototype.getBpmUsername = /**
     * Gets the BPM username
     * @return {?} The BPM username
     */
    function () {
        return this.alfrescoApi.getInstance().getBpmUsername();
    };
    /** Sets the URL to redirect to after login.
     * @param url URL to redirect to
     */
    /**
     * Sets the URL to redirect to after login.
     * @param {?} url URL to redirect to
     * @return {?}
     */
    AuthenticationService.prototype.setRedirect = /**
     * Sets the URL to redirect to after login.
     * @param {?} url URL to redirect to
     * @return {?}
     */
    function (url) {
        this.redirectUrl = url;
    };
    /** Gets the URL to redirect to after login.
     * @returns The redirect URL
     */
    /**
     * Gets the URL to redirect to after login.
     * @return {?} The redirect URL
     */
    AuthenticationService.prototype.getRedirect = /**
     * Gets the URL to redirect to after login.
     * @return {?} The redirect URL
     */
    function () {
        /** @type {?} */
        var provider = (/** @type {?} */ (this.appConfig.get(AppConfigValues.PROVIDERS)));
        return this.hasValidRedirection(provider) ? this.redirectUrl.url : null;
    };
    /**
     * Gets information about the user currently logged into APS.
     * @returns User information
     */
    /**
     * Gets information about the user currently logged into APS.
     * @return {?} User information
     */
    AuthenticationService.prototype.getBpmLoggedUser = /**
     * Gets information about the user currently logged into APS.
     * @return {?} User information
     */
    function () {
        return from(this.alfrescoApi.getInstance().activiti.profileApi.getProfile());
    };
    /**
     * @private
     * @param {?} provider
     * @return {?}
     */
    AuthenticationService.prototype.hasValidRedirection = /**
     * @private
     * @param {?} provider
     * @return {?}
     */
    function (provider) {
        return this.redirectUrl && (this.redirectUrl.provider === provider || this.hasSelectedProviderAll(provider));
    };
    /**
     * @private
     * @param {?} provider
     * @return {?}
     */
    AuthenticationService.prototype.hasSelectedProviderAll = /**
     * @private
     * @param {?} provider
     * @return {?}
     */
    function (provider) {
        return this.redirectUrl && (this.redirectUrl.provider === 'ALL' || provider === 'ALL');
    };
    /**
     * Prints an error message in the console browser
     * @param error Error message
     * @returns Object representing the error message
     */
    /**
     * Prints an error message in the console browser
     * @param {?} error Error message
     * @return {?} Object representing the error message
     */
    AuthenticationService.prototype.handleError = /**
     * Prints an error message in the console browser
     * @param {?} error Error message
     * @return {?} Object representing the error message
     */
    function (error) {
        this.logService.error('Error when logging in', error);
        return throwError(error || 'Server error');
    };
    /**
     * Gets the set of URLs that the token bearer is excluded from.
     * @returns Array of URL strings
     */
    /**
     * Gets the set of URLs that the token bearer is excluded from.
     * @return {?} Array of URL strings
     */
    AuthenticationService.prototype.getBearerExcludedUrls = /**
     * Gets the set of URLs that the token bearer is excluded from.
     * @return {?} Array of URL strings
     */
    function () {
        return this.bearerExcludedUrls;
    };
    /**
     * Gets the auth token.
     * @returns Auth token string
     */
    /**
     * Gets the auth token.
     * @return {?} Auth token string
     */
    AuthenticationService.prototype.getToken = /**
     * Gets the auth token.
     * @return {?} Auth token string
     */
    function () {
        return localStorage.getItem(JwtHelperService.USER_ACCESS_TOKEN);
    };
    /**
     * Adds the auth token to an HTTP header using the 'bearer' scheme.
     * @param headersArg Header that will receive the token
     * @returns The new header with the token added
     */
    /**
     * Adds the auth token to an HTTP header using the 'bearer' scheme.
     * @param {?=} headersArg Header that will receive the token
     * @return {?} The new header with the token added
     */
    AuthenticationService.prototype.addTokenToHeader = /**
     * Adds the auth token to an HTTP header using the 'bearer' scheme.
     * @param {?=} headersArg Header that will receive the token
     * @return {?} The new header with the token added
     */
    function (headersArg) {
        var _this = this;
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        function (observer) {
            /** @type {?} */
            var headers = headersArg;
            if (!headers) {
                headers = new HttpHeaders();
            }
            try {
                /** @type {?} */
                var token = _this.getToken();
                headers = headers.set('Authorization', 'bearer ' + token);
                observer.next(headers);
                observer.complete();
            }
            catch (error) {
                observer.error(error);
            }
        }));
    };
    /**
     * Checks if SSO is configured correctly.
     * @returns True if configured correctly, false otherwise
     */
    /**
     * Checks if SSO is configured correctly.
     * @return {?} True if configured correctly, false otherwise
     */
    AuthenticationService.prototype.isSSODiscoveryConfigured = /**
     * Checks if SSO is configured correctly.
     * @return {?} True if configured correctly, false otherwise
     */
    function () {
        return this.alfrescoApi.getInstance().storage.getItem('discovery') ? true : false;
    };
    AuthenticationService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    AuthenticationService.ctorParameters = function () { return [
        { type: AppConfigService },
        { type: AlfrescoApiService },
        { type: CookieService },
        { type: LogService }
    ]; };
    /** @nocollapse */ AuthenticationService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function AuthenticationService_Factory() { return new AuthenticationService(i0.ɵɵinject(i1.AppConfigService), i0.ɵɵinject(i2.AlfrescoApiService), i0.ɵɵinject(i3.CookieService), i0.ɵɵinject(i4.LogService)); }, token: AuthenticationService, providedIn: "root" });
    return AuthenticationService;
}());
export { AuthenticationService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.redirectUrl;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.bearerExcludedUrls;
    /** @type {?} */
    AuthenticationService.prototype.onLogin;
    /** @type {?} */
    AuthenticationService.prototype.onLogout;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.appConfig;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.alfrescoApi;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.cookie;
    /**
     * @type {?}
     * @private
     */
    AuthenticationService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0aGVudGljYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2F1dGhlbnRpY2F0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsVUFBVSxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsVUFBVSxFQUFZLE1BQU0sTUFBTSxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxrQkFBa0IsQ0FBQztBQUNqRCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTNDLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxlQUFlLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUVyRixPQUFPLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUN0RCxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbkQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sc0JBQXNCLENBQUM7Ozs7Ozs7SUFFbEQsc0JBQXNCLEdBQUcsc0JBQXNCOztJQUMvQyxpQkFBaUIsR0FBRyxJQUFJLEdBQUcsRUFBRSxHQUFHLEVBQUUsR0FBRyxFQUFFLEdBQUcsRUFBRTtBQUVsRDtJQVdJLCtCQUNZLFNBQTJCLEVBQzNCLFdBQStCLEVBQy9CLE1BQXFCLEVBQ3JCLFVBQXNCO1FBSHRCLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBQzNCLGdCQUFXLEdBQVgsV0FBVyxDQUFvQjtRQUMvQixXQUFNLEdBQU4sTUFBTSxDQUFlO1FBQ3JCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFYMUIsZ0JBQVcsR0FBcUIsSUFBSSxDQUFDO1FBRXJDLHVCQUFrQixHQUFhLENBQUMsYUFBYSxFQUFFLFlBQVksRUFBRSxTQUFTLENBQUMsQ0FBQztRQUVoRixZQUFPLEdBQWlCLElBQUksT0FBTyxFQUFPLENBQUM7UUFDM0MsYUFBUSxHQUFpQixJQUFJLE9BQU8sRUFBTyxDQUFDO0lBTzVDLENBQUM7SUFFRDs7O09BR0c7Ozs7O0lBQ0gsMENBQVU7Ozs7SUFBVjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLElBQUksSUFBSSxDQUFDLE1BQU0sQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsRUFBRTtZQUN2RSxPQUFPLEtBQUssQ0FBQztTQUNoQjtRQUNELE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUN2RCxDQUFDO0lBRUQ7OztPQUdHOzs7OztJQUNILHVDQUFPOzs7O0lBQVA7UUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztJQUNqRSxDQUFDO0lBRUQ7OztPQUdHOzs7OztJQUNILDZDQUFhOzs7O0lBQWI7UUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUMvRCxDQUFDO0lBRUQ7OztPQUdHOzs7OztJQUNILDZDQUFhOzs7O0lBQWI7UUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUMsa0JBQWtCLEVBQUUsQ0FBQztJQUMvRCxDQUFDO0lBRUQ7OztPQUdHOzs7OztJQUNILDZDQUFhOzs7O0lBQWI7UUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUMscUJBQXFCLEVBQUUsQ0FBQztJQUNsRSxDQUFDO0lBRUQ7Ozs7OztPQU1HOzs7Ozs7OztJQUNILHFDQUFLOzs7Ozs7O0lBQUwsVUFBTSxRQUFnQixFQUFFLFFBQWdCLEVBQUUsVUFBMkI7UUFBckUsaUJBYUM7UUFieUMsMkJBQUEsRUFBQSxrQkFBMkI7UUFDakUsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxLQUFLLENBQUMsUUFBUSxFQUFFLFFBQVEsQ0FBQyxDQUFDO2FBQ2hFLElBQUksQ0FDRCxHQUFHOzs7O1FBQUMsVUFBQyxRQUFhO1lBQ2QsS0FBSSxDQUFDLG9CQUFvQixDQUFDLFVBQVUsQ0FBQyxDQUFDO1lBQ3RDLEtBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzVCLE9BQU87Z0JBQ0gsSUFBSSxFQUFFLEtBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUM7Z0JBQ25ELE1BQU0sRUFBRSxRQUFRO2FBQ25CLENBQUM7UUFDTixDQUFDLEVBQUMsRUFDRixVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDO0lBRUQ7O09BRUc7Ozs7O0lBQ0gsZ0RBQWdCOzs7O0lBQWhCO1FBQ0ksSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxhQUFhLEVBQUUsQ0FBQztJQUNuRCxDQUFDO0lBRUQ7OztPQUdHOzs7Ozs7O0lBQ0ssb0RBQW9COzs7Ozs7SUFBNUIsVUFBNkIsVUFBbUI7O1lBQ3hDLFVBQVUsR0FBRyxJQUFJO1FBRXJCLElBQUksVUFBVSxFQUFFO1lBQ1osVUFBVSxHQUFHLElBQUksSUFBSSxFQUFFLENBQUM7O2dCQUNsQixJQUFJLEdBQUcsVUFBVSxDQUFDLE9BQU8sRUFBRTs7Z0JBQzNCLFVBQVUsR0FBRyxJQUFJLEdBQUcsaUJBQWlCO1lBQzNDLFVBQVUsQ0FBQyxPQUFPLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDbEM7UUFDRCxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsRUFBRSxHQUFHLEVBQUUsVUFBVSxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQ3ZFLENBQUM7SUFFRDs7O09BR0c7Ozs7O0lBQ0gsK0NBQWU7Ozs7SUFBZjtRQUNJLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLE9BQU8sQ0FBQyxzQkFBc0IsQ0FBQyxLQUFLLElBQUksQ0FBQyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUNqRixDQUFDO0lBRUQ7OztPQUdHOzs7OztJQUNILHNDQUFNOzs7O0lBQU47UUFBQSxpQkFTQztRQVJHLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQzthQUM1QixJQUFJLENBQ0QsR0FBRzs7OztRQUFDLFVBQUMsUUFBUTtZQUNULEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQzdCLE9BQU8sUUFBUSxDQUFDO1FBQ3BCLENBQUMsRUFBQyxFQUNGLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7Ozs7O0lBRU8sNkNBQWE7Ozs7SUFBckI7UUFDSSxJQUFJLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLEVBQUU7WUFDaEMsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sRUFBRSxDQUFDO1NBQ2xEO0lBQ0wsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7SUFDSCw0Q0FBWTs7OztJQUFaO1FBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDLFlBQVksRUFBRSxDQUFDO0lBQ3pELENBQUM7SUFFRDs7O09BR0c7Ozs7O0lBQ0gsNENBQVk7Ozs7SUFBWjtRQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxZQUFZLEVBQUUsQ0FBQztJQUN6RCxDQUFDO0lBRUQ7OztPQUdHOzs7OztJQUNILGtEQUFrQjs7OztJQUFsQjs7WUFDVSxNQUFNLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxZQUFZLEVBQUU7UUFDNUQsSUFBSSxNQUFNLEVBQUU7WUFDUixPQUFPLFFBQVEsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDbEM7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQ7OztPQUdHOzs7OztJQUNILDZDQUFhOzs7O0lBQWI7UUFDSSxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUUsSUFBSSxJQUFJLENBQUMsYUFBYSxFQUFFLEVBQUU7WUFDOUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUUsSUFBSSxJQUFJLENBQUMsTUFBTSxDQUFDLFNBQVMsRUFBRSxJQUFJLENBQUMsSUFBSSxDQUFDLGVBQWUsRUFBRSxFQUFFO2dCQUN2RSxPQUFPLEtBQUssQ0FBQzthQUNoQjtZQUNELE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxhQUFhLEVBQUUsQ0FBQztTQUN6RDtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRDs7O09BR0c7Ozs7O0lBQ0gsNkNBQWE7Ozs7SUFBYjtRQUNJLElBQUksSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLElBQUksQ0FBQyxhQUFhLEVBQUUsRUFBRTtZQUM5QyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxJQUFJLElBQUksQ0FBQyxNQUFNLENBQUMsU0FBUyxFQUFFLElBQUksQ0FBQyxJQUFJLENBQUMsZUFBZSxFQUFFLEVBQUU7Z0JBQ3ZFLE9BQU8sS0FBSyxDQUFDO2FBQ2hCO1lBQ0QsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDLGFBQWEsRUFBRSxDQUFDO1NBQ3pEO1FBQ0QsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7SUFDSCw4Q0FBYzs7OztJQUFkO1FBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQzNELENBQUM7SUFFRDs7O09BR0c7Ozs7O0lBQ0gsOENBQWM7Ozs7SUFBZDtRQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxjQUFjLEVBQUUsQ0FBQztJQUMzRCxDQUFDO0lBRUQ7O09BRUc7Ozs7OztJQUNILDJDQUFXOzs7OztJQUFYLFVBQVksR0FBcUI7UUFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUM7SUFDM0IsQ0FBQztJQUVEOztPQUVHOzs7OztJQUNILDJDQUFXOzs7O0lBQVg7O1lBQ1UsUUFBUSxHQUFHLG1CQUFTLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxTQUFTLENBQUMsRUFBQTtRQUN2RSxPQUFPLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUM1RSxDQUFDO0lBRUQ7OztPQUdHOzs7OztJQUNILGdEQUFnQjs7OztJQUFoQjtRQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxVQUFVLEVBQUUsQ0FBQyxDQUFDO0lBQ2pGLENBQUM7Ozs7OztJQUVPLG1EQUFtQjs7Ozs7SUFBM0IsVUFBNEIsUUFBZ0I7UUFDeEMsT0FBTyxJQUFJLENBQUMsV0FBVyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEtBQUssUUFBUSxJQUFJLElBQUksQ0FBQyxzQkFBc0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQ2pILENBQUM7Ozs7OztJQUVPLHNEQUFzQjs7Ozs7SUFBOUIsVUFBK0IsUUFBZ0I7UUFDM0MsT0FBTyxJQUFJLENBQUMsV0FBVyxJQUFJLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxRQUFRLEtBQUssS0FBSyxJQUFJLFFBQVEsS0FBSyxLQUFLLENBQUMsQ0FBQztJQUMzRixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsMkNBQVc7Ozs7O0lBQVgsVUFBWSxLQUFVO1FBQ2xCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLHVCQUF1QixFQUFFLEtBQUssQ0FBQyxDQUFDO1FBQ3RELE9BQU8sVUFBVSxDQUFDLEtBQUssSUFBSSxjQUFjLENBQUMsQ0FBQztJQUMvQyxDQUFDO0lBRUQ7OztPQUdHOzs7OztJQUNILHFEQUFxQjs7OztJQUFyQjtRQUNJLE9BQU8sSUFBSSxDQUFDLGtCQUFrQixDQUFDO0lBQ25DLENBQUM7SUFFRDs7O09BR0c7Ozs7O0lBQ0gsd0NBQVE7Ozs7SUFBUjtRQUNJLE9BQU8sWUFBWSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO0lBQ3BFLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCxnREFBZ0I7Ozs7O0lBQWhCLFVBQWlCLFVBQXdCO1FBQXpDLGlCQWVDO1FBZEcsT0FBTyxJQUFJLFVBQVU7Ozs7UUFBQyxVQUFDLFFBQXVCOztnQkFDdEMsT0FBTyxHQUFHLFVBQVU7WUFDeEIsSUFBSSxDQUFDLE9BQU8sRUFBRTtnQkFDVixPQUFPLEdBQUcsSUFBSSxXQUFXLEVBQUUsQ0FBQzthQUMvQjtZQUNELElBQUk7O29CQUNNLEtBQUssR0FBVyxLQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNyQyxPQUFPLEdBQUcsT0FBTyxDQUFDLEdBQUcsQ0FBQyxlQUFlLEVBQUUsU0FBUyxHQUFHLEtBQUssQ0FBQyxDQUFDO2dCQUMxRCxRQUFRLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO2dCQUN2QixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7YUFDdkI7WUFBQyxPQUFPLEtBQUssRUFBRTtnQkFDWixRQUFRLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO2FBQ3pCO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7OztPQUdHOzs7OztJQUNILHdEQUF3Qjs7OztJQUF4QjtRQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUN0RixDQUFDOztnQkFwU0osVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFYUSxnQkFBZ0I7Z0JBSmhCLGtCQUFrQjtnQkFDbEIsYUFBYTtnQkFDYixVQUFVOzs7Z0NBckJuQjtDQXFVQyxBQXJTRCxJQXFTQztTQWxTWSxxQkFBcUI7Ozs7OztJQUM5Qiw0Q0FBNkM7Ozs7O0lBRTdDLG1EQUFnRjs7SUFFaEYsd0NBQTJDOztJQUMzQyx5Q0FBNEM7Ozs7O0lBR3hDLDBDQUFtQzs7Ozs7SUFDbkMsNENBQXVDOzs7OztJQUN2Qyx1Q0FBNkI7Ozs7O0lBQzdCLDJDQUE4QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIFN1YmplY3QsIGZyb20sIHRocm93RXJyb3IsIE9ic2VydmVyIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4vYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBDb29raWVTZXJ2aWNlIH0gZnJvbSAnLi9jb29raWUuc2VydmljZSc7XHJcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUmVkaXJlY3Rpb25Nb2RlbCB9IGZyb20gJy4uL21vZGVscy9yZWRpcmVjdGlvbi5tb2RlbCc7XHJcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UsIEFwcENvbmZpZ1ZhbHVlcyB9IGZyb20gJy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXNlclJlcHJlc2VudGF0aW9uIH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IG1hcCwgY2F0Y2hFcnJvciwgdGFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBIdHRwSGVhZGVycyB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwJztcclxuaW1wb3J0IHsgSnd0SGVscGVyU2VydmljZSB9IGZyb20gJy4vand0LWhlbHBlci5zZXJ2aWNlJztcclxuXHJcbmNvbnN0IFJFTUVNQkVSX01FX0NPT0tJRV9LRVkgPSAnQUxGUkVTQ09fUkVNRU1CRVJfTUUnO1xyXG5jb25zdCBSRU1FTUJFUl9NRV9VTlRJTCA9IDEwMDAgKiA2MCAqIDYwICogMjQgKiAzMDtcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgQXV0aGVudGljYXRpb25TZXJ2aWNlIHtcclxuICAgIHByaXZhdGUgcmVkaXJlY3RVcmw6IFJlZGlyZWN0aW9uTW9kZWwgPSBudWxsO1xyXG5cclxuICAgIHByaXZhdGUgYmVhcmVyRXhjbHVkZWRVcmxzOiBzdHJpbmdbXSA9IFsnYXV0aC9yZWFsbXMnLCAncmVzb3VyY2VzLycsICdhc3NldHMvJ107XHJcblxyXG4gICAgb25Mb2dpbjogU3ViamVjdDxhbnk+ID0gbmV3IFN1YmplY3Q8YW55PigpO1xyXG4gICAgb25Mb2dvdXQ6IFN1YmplY3Q8YW55PiA9IG5ldyBTdWJqZWN0PGFueT4oKTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihcclxuICAgICAgICBwcml2YXRlIGFwcENvbmZpZzogQXBwQ29uZmlnU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIGFsZnJlc2NvQXBpOiBBbGZyZXNjb0FwaVNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBjb29raWU6IENvb2tpZVNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVja3MgaWYgdGhlIHVzZXIgbG9nZ2VkIGluLlxyXG4gICAgICogQHJldHVybnMgVHJ1ZSBpZiBsb2dnZWQgaW4sIGZhbHNlIG90aGVyd2lzZVxyXG4gICAgICovXHJcbiAgICBpc0xvZ2dlZEluKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICghdGhpcy5pc09hdXRoKCkgJiYgdGhpcy5jb29raWUuaXNFbmFibGVkKCkgJiYgIXRoaXMuaXNSZW1lbWJlck1lU2V0KCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5hbGZyZXNjb0FwaS5nZXRJbnN0YW5jZSgpLmlzTG9nZ2VkSW4oKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERvZXMgdGhlIHByb3ZpZGVyIHN1cHBvcnQgT0F1dGg/XHJcbiAgICAgKiBAcmV0dXJucyBUcnVlIGlmIHN1cHBvcnRlZCwgZmFsc2Ugb3RoZXJ3aXNlXHJcbiAgICAgKi9cclxuICAgIGlzT2F1dGgoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYWxmcmVzY29BcGkuZ2V0SW5zdGFuY2UoKS5pc09hdXRoQ29uZmlndXJhdGlvbigpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRG9lcyB0aGUgcHJvdmlkZXIgc3VwcG9ydCBFQ00/XHJcbiAgICAgKiBAcmV0dXJucyBUcnVlIGlmIHN1cHBvcnRlZCwgZmFsc2Ugb3RoZXJ3aXNlXHJcbiAgICAgKi9cclxuICAgIGlzRUNNUHJvdmlkZXIoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYWxmcmVzY29BcGkuZ2V0SW5zdGFuY2UoKS5pc0VjbUNvbmZpZ3VyYXRpb24oKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERvZXMgdGhlIHByb3ZpZGVyIHN1cHBvcnQgQlBNP1xyXG4gICAgICogQHJldHVybnMgVHJ1ZSBpZiBzdXBwb3J0ZWQsIGZhbHNlIG90aGVyd2lzZVxyXG4gICAgICovXHJcbiAgICBpc0JQTVByb3ZpZGVyKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFsZnJlc2NvQXBpLmdldEluc3RhbmNlKCkuaXNCcG1Db25maWd1cmF0aW9uKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEb2VzIHRoZSBwcm92aWRlciBzdXBwb3J0IGJvdGggRUNNIGFuZCBCUE0/XHJcbiAgICAgKiBAcmV0dXJucyBUcnVlIGlmIGJvdGggYXJlIHN1cHBvcnRlZCwgZmFsc2Ugb3RoZXJ3aXNlXHJcbiAgICAgKi9cclxuICAgIGlzQUxMUHJvdmlkZXIoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYWxmcmVzY29BcGkuZ2V0SW5zdGFuY2UoKS5pc0VjbUJwbUNvbmZpZ3VyYXRpb24oKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExvZ3MgdGhlIHVzZXIgaW4uXHJcbiAgICAgKiBAcGFyYW0gdXNlcm5hbWUgVXNlcm5hbWUgZm9yIHRoZSBsb2dpblxyXG4gICAgICogQHBhcmFtIHBhc3N3b3JkIFBhc3N3b3JkIGZvciB0aGUgbG9naW5cclxuICAgICAqIEBwYXJhbSByZW1lbWJlck1lIFN0b3JlcyB0aGUgdXNlcidzIGxvZ2luIGRldGFpbHMgaWYgdHJ1ZVxyXG4gICAgICogQHJldHVybnMgT2JqZWN0IHdpdGggYXV0aCB0eXBlIChcIkVDTVwiLCBcIkJQTVwiIG9yIFwiQUxMXCIpIGFuZCBhdXRoIHRpY2tldFxyXG4gICAgICovXHJcbiAgICBsb2dpbih1c2VybmFtZTogc3RyaW5nLCBwYXNzd29yZDogc3RyaW5nLCByZW1lbWJlck1lOiBib29sZWFuID0gZmFsc2UpOiBPYnNlcnZhYmxlPHsgdHlwZTogc3RyaW5nLCB0aWNrZXQ6IGFueSB9PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaS5nZXRJbnN0YW5jZSgpLmxvZ2luKHVzZXJuYW1lLCBwYXNzd29yZCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5zYXZlUmVtZW1iZXJNZUNvb2tpZShyZW1lbWJlck1lKTtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLm9uTG9naW4ubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogdGhpcy5hcHBDb25maWcuZ2V0KEFwcENvbmZpZ1ZhbHVlcy5QUk9WSURFUlMpLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0aWNrZXQ6IHJlc3BvbnNlXHJcbiAgICAgICAgICAgICAgICAgICAgfTtcclxuICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBMb2dzIHRoZSB1c2VyIGluIHdpdGggU1NPXHJcbiAgICAgKi9cclxuICAgIHNzb0ltcGxpY2l0TG9naW4oKSB7XHJcbiAgICAgICAgdGhpcy5hbGZyZXNjb0FwaS5nZXRJbnN0YW5jZSgpLmltcGxpY2l0TG9naW4oKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNhdmVzIHRoZSBcInJlbWVtYmVyIG1lXCIgY29va2llIGFzIGVpdGhlciBhIGxvbmctbGlmZSBjb29raWUgb3IgYSBzZXNzaW9uIGNvb2tpZS5cclxuICAgICAqIEBwYXJhbSByZW1lbWJlck1lIEVuYWJsZXMgYSBsb25nLWxpZmUgY29va2llXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgc2F2ZVJlbWVtYmVyTWVDb29raWUocmVtZW1iZXJNZTogYm9vbGVhbik6IHZvaWQge1xyXG4gICAgICAgIGxldCBleHBpcmF0aW9uID0gbnVsbDtcclxuXHJcbiAgICAgICAgaWYgKHJlbWVtYmVyTWUpIHtcclxuICAgICAgICAgICAgZXhwaXJhdGlvbiA9IG5ldyBEYXRlKCk7XHJcbiAgICAgICAgICAgIGNvbnN0IHRpbWUgPSBleHBpcmF0aW9uLmdldFRpbWUoKTtcclxuICAgICAgICAgICAgY29uc3QgZXhwaXJlVGltZSA9IHRpbWUgKyBSRU1FTUJFUl9NRV9VTlRJTDtcclxuICAgICAgICAgICAgZXhwaXJhdGlvbi5zZXRUaW1lKGV4cGlyZVRpbWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLmNvb2tpZS5zZXRJdGVtKFJFTUVNQkVSX01FX0NPT0tJRV9LRVksICcxJywgZXhwaXJhdGlvbiwgbnVsbCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVja3Mgd2hldGhlciB0aGUgXCJyZW1lbWJlciBtZVwiIGNvb2tpZSB3YXMgc2V0IG9yIG5vdC5cclxuICAgICAqIEByZXR1cm5zIFRydWUgaWYgc2V0LCBmYWxzZSBvdGhlcndpc2VcclxuICAgICAqL1xyXG4gICAgaXNSZW1lbWJlck1lU2V0KCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiAodGhpcy5jb29raWUuZ2V0SXRlbShSRU1FTUJFUl9NRV9DT09LSUVfS0VZKSA9PT0gbnVsbCkgPyBmYWxzZSA6IHRydWU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBMb2dzIHRoZSB1c2VyIG91dC5cclxuICAgICAqIEByZXR1cm5zIFJlc3BvbnNlIGV2ZW50IGNhbGxlZCB3aGVuIGxvZ291dCBpcyBjb21wbGV0ZVxyXG4gICAgICovXHJcbiAgICBsb2dvdXQoKSB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5jYWxsQXBpTG9nb3V0KCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgdGFwKChyZXNwb25zZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMub25Mb2dvdXQubmV4dChyZXNwb25zZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlc3BvbnNlO1xyXG4gICAgICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNhbGxBcGlMb2dvdXQoKTogUHJvbWlzZTxhbnk+IHtcclxuICAgICAgICBpZiAodGhpcy5hbGZyZXNjb0FwaS5nZXRJbnN0YW5jZSgpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFsZnJlc2NvQXBpLmdldEluc3RhbmNlKCkubG9nb3V0KCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgRUNNIHRpY2tldCBzdG9yZWQgaW4gdGhlIFN0b3JhZ2UuXHJcbiAgICAgKiBAcmV0dXJucyBUaGUgdGlja2V0IG9yIGBudWxsYCBpZiBub25lIHdhcyBmb3VuZFxyXG4gICAgICovXHJcbiAgICBnZXRUaWNrZXRFY20oKTogc3RyaW5nIHwgbnVsbCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYWxmcmVzY29BcGkuZ2V0SW5zdGFuY2UoKS5nZXRUaWNrZXRFY20oKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIEJQTSB0aWNrZXQgc3RvcmVkIGluIHRoZSBTdG9yYWdlLlxyXG4gICAgICogQHJldHVybnMgVGhlIHRpY2tldCBvciBgbnVsbGAgaWYgbm9uZSB3YXMgZm91bmRcclxuICAgICAqL1xyXG4gICAgZ2V0VGlja2V0QnBtKCk6IHN0cmluZyB8IG51bGwge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFsZnJlc2NvQXBpLmdldEluc3RhbmNlKCkuZ2V0VGlja2V0QnBtKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBCUE0gdGlja2V0IGZyb20gdGhlIFN0b3JhZ2UgaW4gQmFzZSA2NCBmb3JtYXQuXHJcbiAgICAgKiBAcmV0dXJucyBUaGUgdGlja2V0IG9yIGBudWxsYCBpZiBub25lIHdhcyBmb3VuZFxyXG4gICAgICovXHJcbiAgICBnZXRUaWNrZXRFY21CYXNlNjQoKTogc3RyaW5nIHwgbnVsbCB7XHJcbiAgICAgICAgY29uc3QgdGlja2V0ID0gdGhpcy5hbGZyZXNjb0FwaS5nZXRJbnN0YW5jZSgpLmdldFRpY2tldEVjbSgpO1xyXG4gICAgICAgIGlmICh0aWNrZXQpIHtcclxuICAgICAgICAgICAgcmV0dXJuICdCYXNpYyAnICsgYnRvYSh0aWNrZXQpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrcyBpZiB0aGUgdXNlciBpcyBsb2dnZWQgaW4gb24gYW4gRUNNIHByb3ZpZGVyLlxyXG4gICAgICogQHJldHVybnMgVHJ1ZSBpZiBsb2dnZWQgaW4sIGZhbHNlIG90aGVyd2lzZVxyXG4gICAgICovXHJcbiAgICBpc0VjbUxvZ2dlZEluKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLmlzRUNNUHJvdmlkZXIoKSB8fCB0aGlzLmlzQUxMUHJvdmlkZXIoKSkge1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuaXNPYXV0aCgpICYmIHRoaXMuY29va2llLmlzRW5hYmxlZCgpICYmICF0aGlzLmlzUmVtZW1iZXJNZVNldCgpKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuYWxmcmVzY29BcGkuZ2V0SW5zdGFuY2UoKS5pc0VjbUxvZ2dlZEluKCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrcyBpZiB0aGUgdXNlciBpcyBsb2dnZWQgaW4gb24gYSBCUE0gcHJvdmlkZXIuXHJcbiAgICAgKiBAcmV0dXJucyBUcnVlIGlmIGxvZ2dlZCBpbiwgZmFsc2Ugb3RoZXJ3aXNlXHJcbiAgICAgKi9cclxuICAgIGlzQnBtTG9nZ2VkSW4oKTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNCUE1Qcm92aWRlcigpIHx8IHRoaXMuaXNBTExQcm92aWRlcigpKSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5pc09hdXRoKCkgJiYgdGhpcy5jb29raWUuaXNFbmFibGVkKCkgJiYgIXRoaXMuaXNSZW1lbWJlck1lU2V0KCkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5hbGZyZXNjb0FwaS5nZXRJbnN0YW5jZSgpLmlzQnBtTG9nZ2VkSW4oKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgRUNNIHVzZXJuYW1lLlxyXG4gICAgICogQHJldHVybnMgVGhlIEVDTSB1c2VybmFtZVxyXG4gICAgICovXHJcbiAgICBnZXRFY21Vc2VybmFtZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFsZnJlc2NvQXBpLmdldEluc3RhbmNlKCkuZ2V0RWNtVXNlcm5hbWUoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIEJQTSB1c2VybmFtZVxyXG4gICAgICogQHJldHVybnMgVGhlIEJQTSB1c2VybmFtZVxyXG4gICAgICovXHJcbiAgICBnZXRCcG1Vc2VybmFtZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFsZnJlc2NvQXBpLmdldEluc3RhbmNlKCkuZ2V0QnBtVXNlcm5hbWUoKTtcclxuICAgIH1cclxuXHJcbiAgICAvKiogU2V0cyB0aGUgVVJMIHRvIHJlZGlyZWN0IHRvIGFmdGVyIGxvZ2luLlxyXG4gICAgICogQHBhcmFtIHVybCBVUkwgdG8gcmVkaXJlY3QgdG9cclxuICAgICAqL1xyXG4gICAgc2V0UmVkaXJlY3QodXJsOiBSZWRpcmVjdGlvbk1vZGVsKSB7XHJcbiAgICAgICAgdGhpcy5yZWRpcmVjdFVybCA9IHVybDtcclxuICAgIH1cclxuXHJcbiAgICAvKiogR2V0cyB0aGUgVVJMIHRvIHJlZGlyZWN0IHRvIGFmdGVyIGxvZ2luLlxyXG4gICAgICogQHJldHVybnMgVGhlIHJlZGlyZWN0IFVSTFxyXG4gICAgICovXHJcbiAgICBnZXRSZWRpcmVjdCgpOiBzdHJpbmcge1xyXG4gICAgICAgIGNvbnN0IHByb3ZpZGVyID0gPHN0cmluZz4gdGhpcy5hcHBDb25maWcuZ2V0KEFwcENvbmZpZ1ZhbHVlcy5QUk9WSURFUlMpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLmhhc1ZhbGlkUmVkaXJlY3Rpb24ocHJvdmlkZXIpID8gdGhpcy5yZWRpcmVjdFVybC51cmwgOiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBpbmZvcm1hdGlvbiBhYm91dCB0aGUgdXNlciBjdXJyZW50bHkgbG9nZ2VkIGludG8gQVBTLlxyXG4gICAgICogQHJldHVybnMgVXNlciBpbmZvcm1hdGlvblxyXG4gICAgICovXHJcbiAgICBnZXRCcG1Mb2dnZWRVc2VyKCk6IE9ic2VydmFibGU8VXNlclJlcHJlc2VudGF0aW9uPiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaS5nZXRJbnN0YW5jZSgpLmFjdGl2aXRpLnByb2ZpbGVBcGkuZ2V0UHJvZmlsZSgpKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGhhc1ZhbGlkUmVkaXJlY3Rpb24ocHJvdmlkZXI6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnJlZGlyZWN0VXJsICYmICh0aGlzLnJlZGlyZWN0VXJsLnByb3ZpZGVyID09PSBwcm92aWRlciB8fCB0aGlzLmhhc1NlbGVjdGVkUHJvdmlkZXJBbGwocHJvdmlkZXIpKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGhhc1NlbGVjdGVkUHJvdmlkZXJBbGwocHJvdmlkZXI6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnJlZGlyZWN0VXJsICYmICh0aGlzLnJlZGlyZWN0VXJsLnByb3ZpZGVyID09PSAnQUxMJyB8fCBwcm92aWRlciA9PT0gJ0FMTCcpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUHJpbnRzIGFuIGVycm9yIG1lc3NhZ2UgaW4gdGhlIGNvbnNvbGUgYnJvd3NlclxyXG4gICAgICogQHBhcmFtIGVycm9yIEVycm9yIG1lc3NhZ2VcclxuICAgICAqIEByZXR1cm5zIE9iamVjdCByZXByZXNlbnRpbmcgdGhlIGVycm9yIG1lc3NhZ2VcclxuICAgICAqL1xyXG4gICAgaGFuZGxlRXJyb3IoZXJyb3I6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKCdFcnJvciB3aGVuIGxvZ2dpbmcgaW4nLCBlcnJvcik7XHJcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IgfHwgJ1NlcnZlciBlcnJvcicpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgc2V0IG9mIFVSTHMgdGhhdCB0aGUgdG9rZW4gYmVhcmVyIGlzIGV4Y2x1ZGVkIGZyb20uXHJcbiAgICAgKiBAcmV0dXJucyBBcnJheSBvZiBVUkwgc3RyaW5nc1xyXG4gICAgICovXHJcbiAgICBnZXRCZWFyZXJFeGNsdWRlZFVybHMoKTogc3RyaW5nW10ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmJlYXJlckV4Y2x1ZGVkVXJscztcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIGF1dGggdG9rZW4uXHJcbiAgICAgKiBAcmV0dXJucyBBdXRoIHRva2VuIHN0cmluZ1xyXG4gICAgICovXHJcbiAgICBnZXRUb2tlbigpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShKd3RIZWxwZXJTZXJ2aWNlLlVTRVJfQUNDRVNTX1RPS0VOKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEFkZHMgdGhlIGF1dGggdG9rZW4gdG8gYW4gSFRUUCBoZWFkZXIgdXNpbmcgdGhlICdiZWFyZXInIHNjaGVtZS5cclxuICAgICAqIEBwYXJhbSBoZWFkZXJzQXJnIEhlYWRlciB0aGF0IHdpbGwgcmVjZWl2ZSB0aGUgdG9rZW5cclxuICAgICAqIEByZXR1cm5zIFRoZSBuZXcgaGVhZGVyIHdpdGggdGhlIHRva2VuIGFkZGVkXHJcbiAgICAgKi9cclxuICAgIGFkZFRva2VuVG9IZWFkZXIoaGVhZGVyc0FyZz86IEh0dHBIZWFkZXJzKTogT2JzZXJ2YWJsZTxIdHRwSGVhZGVycz4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZSgob2JzZXJ2ZXI6IE9ic2VydmVyPGFueT4pID0+IHtcclxuICAgICAgICAgICAgbGV0IGhlYWRlcnMgPSBoZWFkZXJzQXJnO1xyXG4gICAgICAgICAgICBpZiAoIWhlYWRlcnMpIHtcclxuICAgICAgICAgICAgICAgIGhlYWRlcnMgPSBuZXcgSHR0cEhlYWRlcnMoKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdG9rZW46IHN0cmluZyA9IHRoaXMuZ2V0VG9rZW4oKTtcclxuICAgICAgICAgICAgICAgIGhlYWRlcnMgPSBoZWFkZXJzLnNldCgnQXV0aG9yaXphdGlvbicsICdiZWFyZXIgJyArIHRva2VuKTtcclxuICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoaGVhZGVycyk7XHJcbiAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICB9IGNhdGNoIChlcnJvcikge1xyXG4gICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVja3MgaWYgU1NPIGlzIGNvbmZpZ3VyZWQgY29ycmVjdGx5LlxyXG4gICAgICogQHJldHVybnMgVHJ1ZSBpZiBjb25maWd1cmVkIGNvcnJlY3RseSwgZmFsc2Ugb3RoZXJ3aXNlXHJcbiAgICAgKi9cclxuICAgIGlzU1NPRGlzY292ZXJ5Q29uZmlndXJlZCgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hbGZyZXNjb0FwaS5nZXRJbnN0YW5jZSgpLnN0b3JhZ2UuZ2V0SXRlbSgnZGlzY292ZXJ5JykgPyB0cnVlIDogZmFsc2U7XHJcbiAgICB9XHJcbn1cclxuIl19