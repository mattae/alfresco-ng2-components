/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { AppConfigService } from '../app-config/app-config.service';
import { AlfrescoApiService } from '../services/alfresco-api.service';
import { StorageService } from './storage.service';
import { UserPreferencesService } from './user-preferences.service';
import { DemoForm } from '../mock/form/demo-form.mock';
import * as i0 from "@angular/core";
import * as i1 from "../app-config/app-config.service";
import * as i2 from "./alfresco-api.service";
import * as i3 from "./user-preferences.service";
import * as i4 from "./storage.service";
var CoreAutomationService = /** @class */ (function () {
    function CoreAutomationService(appConfigService, alfrescoApiService, userPreferencesService, storageService) {
        this.appConfigService = appConfigService;
        this.alfrescoApiService = alfrescoApiService;
        this.userPreferencesService = userPreferencesService;
        this.storageService = storageService;
        this.forms = new DemoForm();
    }
    /**
     * @return {?}
     */
    CoreAutomationService.prototype.setup = /**
     * @return {?}
     */
    function () {
        var _this = this;
        /** @type {?} */
        var adfProxy = window['adf'] || {};
        adfProxy.setConfigField = (/**
         * @param {?} field
         * @param {?} value
         * @return {?}
         */
        function (field, value) {
            _this.appConfigService.config[field] = JSON.parse(value);
        });
        adfProxy.setStorageItem = (/**
         * @param {?} key
         * @param {?} data
         * @return {?}
         */
        function (key, data) {
            _this.storageService.setItem(key, data);
        });
        adfProxy.setUserPreference = (/**
         * @param {?} key
         * @param {?} data
         * @return {?}
         */
        function (key, data) {
            _this.userPreferencesService.set(key, data);
        });
        adfProxy.setFormInEditor = (/**
         * @param {?} json
         * @return {?}
         */
        function (json) {
            _this.forms.formDefinition = JSON.parse(json);
        });
        adfProxy.setCloudFormInEditor = (/**
         * @param {?} json
         * @return {?}
         */
        function (json) {
            _this.forms.cloudFormDefinition = JSON.parse(json);
        });
        adfProxy.clearStorage = (/**
         * @return {?}
         */
        function () {
            _this.storageService.clear();
        });
        adfProxy.apiReset = (/**
         * @return {?}
         */
        function () {
            _this.alfrescoApiService.reset();
        });
        window['adf'] = adfProxy;
    };
    CoreAutomationService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    CoreAutomationService.ctorParameters = function () { return [
        { type: AppConfigService },
        { type: AlfrescoApiService },
        { type: UserPreferencesService },
        { type: StorageService }
    ]; };
    /** @nocollapse */ CoreAutomationService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function CoreAutomationService_Factory() { return new CoreAutomationService(i0.ɵɵinject(i1.AppConfigService), i0.ɵɵinject(i2.AlfrescoApiService), i0.ɵɵinject(i3.UserPreferencesService), i0.ɵɵinject(i4.StorageService)); }, token: CoreAutomationService, providedIn: "root" });
    return CoreAutomationService;
}());
export { CoreAutomationService };
if (false) {
    /** @type {?} */
    CoreAutomationService.prototype.forms;
    /**
     * @type {?}
     * @private
     */
    CoreAutomationService.prototype.appConfigService;
    /**
     * @type {?}
     * @private
     */
    CoreAutomationService.prototype.alfrescoApiService;
    /**
     * @type {?}
     * @private
     */
    CoreAutomationService.prototype.userPreferencesService;
    /**
     * @type {?}
     * @private
     */
    CoreAutomationService.prototype.storageService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYXV0b21hdGlvbi5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvYXV0b21hdGlvbi5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDcEUsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDdEUsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxRQUFRLEVBQUUsTUFBTSw2QkFBNkIsQ0FBQzs7Ozs7O0FBRXZEO0lBT0ksK0JBQW9CLGdCQUFrQyxFQUNsQyxrQkFBc0MsRUFDdEMsc0JBQThDLEVBQzlDLGNBQThCO1FBSDlCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFDbEMsdUJBQWtCLEdBQWxCLGtCQUFrQixDQUFvQjtRQUN0QywyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO1FBQzlDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUwzQyxVQUFLLEdBQUcsSUFBSSxRQUFRLEVBQUUsQ0FBQztJQU05QixDQUFDOzs7O0lBRUQscUNBQUs7OztJQUFMO1FBQUEsaUJBZ0NDOztZQS9CUyxRQUFRLEdBQUcsTUFBTSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUU7UUFFcEMsUUFBUSxDQUFDLGNBQWM7Ozs7O1FBQUcsVUFBQyxLQUFhLEVBQUUsS0FBYTtZQUNuRCxLQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDNUQsQ0FBQyxDQUFBLENBQUM7UUFFRixRQUFRLENBQUMsY0FBYzs7Ozs7UUFBRyxVQUFDLEdBQVcsRUFBRSxJQUFZO1lBQ2hELEtBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMzQyxDQUFDLENBQUEsQ0FBQztRQUVGLFFBQVEsQ0FBQyxpQkFBaUI7Ozs7O1FBQUcsVUFBQyxHQUFXLEVBQUUsSUFBUztZQUNoRCxLQUFJLENBQUMsc0JBQXNCLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxJQUFJLENBQUMsQ0FBQztRQUMvQyxDQUFDLENBQUEsQ0FBQztRQUVGLFFBQVEsQ0FBQyxlQUFlOzs7O1FBQUcsVUFBQyxJQUFZO1lBQ3BDLEtBQUksQ0FBQyxLQUFLLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakQsQ0FBQyxDQUFBLENBQUM7UUFFRixRQUFRLENBQUMsb0JBQW9COzs7O1FBQUcsVUFBQyxJQUFZO1lBQ3pDLEtBQUksQ0FBQyxLQUFLLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN0RCxDQUFDLENBQUEsQ0FBQztRQUVGLFFBQVEsQ0FBQyxZQUFZOzs7UUFBRztZQUNwQixLQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssRUFBRSxDQUFDO1FBQ2hDLENBQUMsQ0FBQSxDQUFDO1FBRUYsUUFBUSxDQUFDLFFBQVE7OztRQUFHO1lBQ2hCLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNwQyxDQUFDLENBQUEsQ0FBQztRQUVGLE1BQU0sQ0FBQyxLQUFLLENBQUMsR0FBRyxRQUFRLENBQUM7SUFDN0IsQ0FBQzs7Z0JBN0NKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7Z0JBUlEsZ0JBQWdCO2dCQUNoQixrQkFBa0I7Z0JBRWxCLHNCQUFzQjtnQkFEdEIsY0FBYzs7O2dDQXBCdkI7Q0FzRUMsQUE5Q0QsSUE4Q0M7U0EzQ1kscUJBQXFCOzs7SUFFOUIsc0NBQThCOzs7OztJQUVsQixpREFBMEM7Ozs7O0lBQzFDLG1EQUE4Qzs7Ozs7SUFDOUMsdURBQXNEOzs7OztJQUN0RCwrQ0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBBcHBDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vYXBwLWNvbmZpZy9hcHAtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnLi9zdG9yYWdlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlIH0gZnJvbSAnLi91c2VyLXByZWZlcmVuY2VzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBEZW1vRm9ybSB9IGZyb20gJy4uL21vY2svZm9ybS9kZW1vLWZvcm0ubW9jayc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIENvcmVBdXRvbWF0aW9uU2VydmljZSB7XHJcblxyXG4gICAgcHVibGljIGZvcm1zID0gbmV3IERlbW9Gb3JtKCk7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhcHBDb25maWdTZXJ2aWNlOiBBcHBDb25maWdTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBhbGZyZXNjb0FwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgdXNlclByZWZlcmVuY2VzU2VydmljZTogVXNlclByZWZlcmVuY2VzU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgc3RvcmFnZVNlcnZpY2U6IFN0b3JhZ2VTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgc2V0dXAoKSB7XHJcbiAgICAgICAgY29uc3QgYWRmUHJveHkgPSB3aW5kb3dbJ2FkZiddIHx8IHt9O1xyXG5cclxuICAgICAgICBhZGZQcm94eS5zZXRDb25maWdGaWVsZCA9IChmaWVsZDogc3RyaW5nLCB2YWx1ZTogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuYXBwQ29uZmlnU2VydmljZS5jb25maWdbZmllbGRdID0gSlNPTi5wYXJzZSh2YWx1ZSk7XHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgYWRmUHJveHkuc2V0U3RvcmFnZUl0ZW0gPSAoa2V5OiBzdHJpbmcsIGRhdGE6IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLnN0b3JhZ2VTZXJ2aWNlLnNldEl0ZW0oa2V5LCBkYXRhKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBhZGZQcm94eS5zZXRVc2VyUHJlZmVyZW5jZSA9IChrZXk6IHN0cmluZywgZGF0YTogYW55KSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VzU2VydmljZS5zZXQoa2V5LCBkYXRhKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBhZGZQcm94eS5zZXRGb3JtSW5FZGl0b3IgPSAoanNvbjogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZm9ybXMuZm9ybURlZmluaXRpb24gPSBKU09OLnBhcnNlKGpzb24pO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGFkZlByb3h5LnNldENsb3VkRm9ybUluRWRpdG9yID0gKGpzb246IHN0cmluZykgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmZvcm1zLmNsb3VkRm9ybURlZmluaXRpb24gPSBKU09OLnBhcnNlKGpzb24pO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGFkZlByb3h5LmNsZWFyU3RvcmFnZSA9ICgpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5zdG9yYWdlU2VydmljZS5jbGVhcigpO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIGFkZlByb3h5LmFwaVJlc2V0ID0gKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5yZXNldCgpO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHdpbmRvd1snYWRmJ10gPSBhZGZQcm94eTtcclxuICAgIH1cclxufVxyXG4iXX0=