/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { CommentModel } from '../models/comment.model';
import { UserProcessModel } from '../models/user-process.model';
import { AlfrescoApiService } from './alfresco-api.service';
import { LogService } from './log.service';
import { map, catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./log.service";
var CommentProcessService = /** @class */ (function () {
    function CommentProcessService(apiService, logService) {
        this.apiService = apiService;
        this.logService = logService;
    }
    /**
     * Adds a comment to a task.
     * @param taskId ID of the target task
     * @param message Text for the comment
     * @returns Details about the comment
     */
    /**
     * Adds a comment to a task.
     * @param {?} taskId ID of the target task
     * @param {?} message Text for the comment
     * @return {?} Details about the comment
     */
    CommentProcessService.prototype.addTaskComment = /**
     * Adds a comment to a task.
     * @param {?} taskId ID of the target task
     * @param {?} message Text for the comment
     * @return {?} Details about the comment
     */
    function (taskId, message) {
        var _this = this;
        return from(this.apiService.getInstance().activiti.taskApi.addTaskComment({ message: message }, taskId))
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return new CommentModel({
                id: response.id,
                message: response.message,
                created: response.created,
                createdBy: response.createdBy
            });
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets all comments that have been added to a task.
     * @param taskId ID of the target task
     * @returns Details for each comment
     */
    /**
     * Gets all comments that have been added to a task.
     * @param {?} taskId ID of the target task
     * @return {?} Details for each comment
     */
    CommentProcessService.prototype.getTaskComments = /**
     * Gets all comments that have been added to a task.
     * @param {?} taskId ID of the target task
     * @return {?} Details for each comment
     */
    function (taskId) {
        var _this = this;
        return from(this.apiService.getInstance().activiti.taskApi.getTaskComments(taskId))
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            /** @type {?} */
            var comments = [];
            response.data.forEach((/**
             * @param {?} comment
             * @return {?}
             */
            function (comment) {
                /** @type {?} */
                var user = new UserProcessModel(comment.createdBy);
                comments.push(new CommentModel({
                    id: comment.id,
                    message: comment.message,
                    created: comment.created,
                    createdBy: user
                }));
            }));
            return comments;
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets all comments that have been added to a process instance.
     * @param processInstanceId ID of the target process instance
     * @returns Details for each comment
     */
    /**
     * Gets all comments that have been added to a process instance.
     * @param {?} processInstanceId ID of the target process instance
     * @return {?} Details for each comment
     */
    CommentProcessService.prototype.getProcessInstanceComments = /**
     * Gets all comments that have been added to a process instance.
     * @param {?} processInstanceId ID of the target process instance
     * @return {?} Details for each comment
     */
    function (processInstanceId) {
        var _this = this;
        return from(this.apiService.getInstance().activiti.commentsApi.getProcessInstanceComments(processInstanceId))
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            /** @type {?} */
            var comments = [];
            response.data.forEach((/**
             * @param {?} comment
             * @return {?}
             */
            function (comment) {
                /** @type {?} */
                var user = new UserProcessModel(comment.createdBy);
                comments.push(new CommentModel({
                    id: comment.id,
                    message: comment.message,
                    created: comment.created,
                    createdBy: user
                }));
            }));
            return comments;
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Adds a comment to a process instance.
     * @param processInstanceId ID of the target process instance
     * @param message Text for the comment
     * @returns Details of the comment added
     */
    /**
     * Adds a comment to a process instance.
     * @param {?} processInstanceId ID of the target process instance
     * @param {?} message Text for the comment
     * @return {?} Details of the comment added
     */
    CommentProcessService.prototype.addProcessInstanceComment = /**
     * Adds a comment to a process instance.
     * @param {?} processInstanceId ID of the target process instance
     * @param {?} message Text for the comment
     * @return {?} Details of the comment added
     */
    function (processInstanceId, message) {
        var _this = this;
        return from(this.apiService.getInstance().activiti.commentsApi.addProcessInstanceComment({ message: message }, processInstanceId)).pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return new CommentModel({
                id: response.id,
                message: response.message,
                created: response.created,
                createdBy: response.createdBy
            });
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * @private
     * @param {?} error
     * @return {?}
     */
    CommentProcessService.prototype.handleError = /**
     * @private
     * @param {?} error
     * @return {?}
     */
    function (error) {
        this.logService.error(error);
        return throwError(error || 'Server error');
    };
    CommentProcessService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    CommentProcessService.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: LogService }
    ]; };
    /** @nocollapse */ CommentProcessService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function CommentProcessService_Factory() { return new CommentProcessService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: CommentProcessService, providedIn: "root" });
    return CommentProcessService;
}());
export { CommentProcessService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    CommentProcessService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    CommentProcessService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29tbWVudC1wcm9jZXNzLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9jb21tZW50LXByb2Nlc3Muc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBYyxJQUFJLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3BELE9BQU8sRUFBRSxZQUFZLEVBQUUsTUFBTSx5QkFBeUIsQ0FBQztBQUN2RCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSw4QkFBOEIsQ0FBQztBQUNoRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxHQUFHLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7QUFFakQ7SUFLSSwrQkFBb0IsVUFBOEIsRUFDOUIsVUFBc0I7UUFEdEIsZUFBVSxHQUFWLFVBQVUsQ0FBb0I7UUFDOUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUMxQyxDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCw4Q0FBYzs7Ozs7O0lBQWQsVUFBZSxNQUFjLEVBQUUsT0FBZTtRQUE5QyxpQkFhQztRQVpHLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsRUFBRSxPQUFPLEVBQUUsT0FBTyxFQUFFLEVBQUUsTUFBTSxDQUFDLENBQUM7YUFDbkcsSUFBSSxDQUNELEdBQUc7Ozs7UUFBQyxVQUFDLFFBQXNCO1lBQ3ZCLE9BQU8sSUFBSSxZQUFZLENBQUM7Z0JBQ3BCLEVBQUUsRUFBRSxRQUFRLENBQUMsRUFBRTtnQkFDZixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQ3pCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTztnQkFDekIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxTQUFTO2FBQ2hDLENBQUMsQ0FBQztRQUNQLENBQUMsRUFBQyxFQUNGLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQVEsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FDbEQsQ0FBQztJQUNWLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCwrQ0FBZTs7Ozs7SUFBZixVQUFnQixNQUFjO1FBQTlCLGlCQWtCQztRQWpCRyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLE1BQU0sQ0FBQyxDQUFDO2FBQzlFLElBQUksQ0FDRCxHQUFHOzs7O1FBQUMsVUFBQyxRQUFhOztnQkFDUixRQUFRLEdBQW1CLEVBQUU7WUFDbkMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPOzs7O1lBQUMsVUFBQyxPQUFxQjs7b0JBQ2xDLElBQUksR0FBRyxJQUFJLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUM7Z0JBQ3BELFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxZQUFZLENBQUM7b0JBQzNCLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRTtvQkFDZCxPQUFPLEVBQUUsT0FBTyxDQUFDLE9BQU87b0JBQ3hCLE9BQU8sRUFBRSxPQUFPLENBQUMsT0FBTztvQkFDeEIsU0FBUyxFQUFFLElBQUk7aUJBQ2xCLENBQUMsQ0FBQyxDQUFDO1lBQ1IsQ0FBQyxFQUFDLENBQUM7WUFDSCxPQUFPLFFBQVEsQ0FBQztRQUNwQixDQUFDLEVBQUMsRUFDRixVQUFVOzs7O1FBQUMsVUFBQyxHQUFRLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQ2xELENBQUM7SUFDVixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsMERBQTBCOzs7OztJQUExQixVQUEyQixpQkFBeUI7UUFBcEQsaUJBa0JDO1FBakJHLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFdBQVcsQ0FBQywwQkFBMEIsQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2FBQ3hHLElBQUksQ0FDRCxHQUFHOzs7O1FBQUMsVUFBQyxRQUFhOztnQkFDUixRQUFRLEdBQW1CLEVBQUU7WUFDbkMsUUFBUSxDQUFDLElBQUksQ0FBQyxPQUFPOzs7O1lBQUMsVUFBQyxPQUFxQjs7b0JBQ2xDLElBQUksR0FBRyxJQUFJLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUM7Z0JBQ3BELFFBQVEsQ0FBQyxJQUFJLENBQUMsSUFBSSxZQUFZLENBQUM7b0JBQzNCLEVBQUUsRUFBRSxPQUFPLENBQUMsRUFBRTtvQkFDZCxPQUFPLEVBQUUsT0FBTyxDQUFDLE9BQU87b0JBQ3hCLE9BQU8sRUFBRSxPQUFPLENBQUMsT0FBTztvQkFDeEIsU0FBUyxFQUFFLElBQUk7aUJBQ2xCLENBQUMsQ0FBQyxDQUFDO1lBQ1IsQ0FBQyxFQUFDLENBQUM7WUFDSCxPQUFPLFFBQVEsQ0FBQztRQUNwQixDQUFDLEVBQUMsRUFDRixVQUFVOzs7O1FBQUMsVUFBQyxHQUFRLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQ2xELENBQUM7SUFDVixDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCx5REFBeUI7Ozs7OztJQUF6QixVQUEwQixpQkFBeUIsRUFBRSxPQUFlO1FBQXBFLGlCQWNDO1FBYkcsT0FBTyxJQUFJLENBQ1AsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsV0FBVyxDQUFDLHlCQUF5QixDQUFDLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxFQUFFLGlCQUFpQixDQUFDLENBQ3hILENBQUMsSUFBSSxDQUNGLEdBQUc7Ozs7UUFBQyxVQUFDLFFBQXNCO1lBQ3ZCLE9BQU8sSUFBSSxZQUFZLENBQUM7Z0JBQ3BCLEVBQUUsRUFBRSxRQUFRLENBQUMsRUFBRTtnQkFDZixPQUFPLEVBQUUsUUFBUSxDQUFDLE9BQU87Z0JBQ3pCLE9BQU8sRUFBRSxRQUFRLENBQUMsT0FBTztnQkFDekIsU0FBUyxFQUFFLFFBQVEsQ0FBQyxTQUFTO2FBQ2hDLENBQUMsQ0FBQztRQUNQLENBQUMsRUFBQyxFQUNGLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQVEsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FDbEQsQ0FBQztJQUNOLENBQUM7Ozs7OztJQUVPLDJDQUFXOzs7OztJQUFuQixVQUFvQixLQUFVO1FBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLE9BQU8sVUFBVSxDQUFDLEtBQUssSUFBSSxjQUFjLENBQUMsQ0FBQztJQUMvQyxDQUFDOztnQkF6R0osVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFOUSxrQkFBa0I7Z0JBQ2xCLFVBQVU7OztnQ0F0Qm5CO0NBb0lDLEFBM0dELElBMkdDO1NBeEdZLHFCQUFxQjs7Ozs7O0lBRWxCLDJDQUFzQzs7Ozs7SUFDdEMsMkNBQThCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgZnJvbSwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBDb21tZW50TW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvY29tbWVudC5tb2RlbCc7XHJcbmltcG9ydCB7IFVzZXJQcm9jZXNzTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvdXNlci1wcm9jZXNzLm1vZGVsJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgbWFwLCBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb21tZW50UHJvY2Vzc1NlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBZGRzIGEgY29tbWVudCB0byBhIHRhc2suXHJcbiAgICAgKiBAcGFyYW0gdGFza0lkIElEIG9mIHRoZSB0YXJnZXQgdGFza1xyXG4gICAgICogQHBhcmFtIG1lc3NhZ2UgVGV4dCBmb3IgdGhlIGNvbW1lbnRcclxuICAgICAqIEByZXR1cm5zIERldGFpbHMgYWJvdXQgdGhlIGNvbW1lbnRcclxuICAgICAqL1xyXG4gICAgYWRkVGFza0NvbW1lbnQodGFza0lkOiBzdHJpbmcsIG1lc3NhZ2U6IHN0cmluZyk6IE9ic2VydmFibGU8Q29tbWVudE1vZGVsPiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuYWN0aXZpdGkudGFza0FwaS5hZGRUYXNrQ29tbWVudCh7IG1lc3NhZ2U6IG1lc3NhZ2UgfSwgdGFza0lkKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAoKHJlc3BvbnNlOiBDb21tZW50TW9kZWwpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmV3IENvbW1lbnRNb2RlbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlkOiByZXNwb25zZS5pZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogcmVzcG9uc2UubWVzc2FnZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3JlYXRlZDogcmVzcG9uc2UuY3JlYXRlZCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgY3JlYXRlZEJ5OiByZXNwb25zZS5jcmVhdGVkQnlcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyOiBhbnkpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYWxsIGNvbW1lbnRzIHRoYXQgaGF2ZSBiZWVuIGFkZGVkIHRvIGEgdGFzay5cclxuICAgICAqIEBwYXJhbSB0YXNrSWQgSUQgb2YgdGhlIHRhcmdldCB0YXNrXHJcbiAgICAgKiBAcmV0dXJucyBEZXRhaWxzIGZvciBlYWNoIGNvbW1lbnRcclxuICAgICAqL1xyXG4gICAgZ2V0VGFza0NvbW1lbnRzKHRhc2tJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxDb21tZW50TW9kZWxbXT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmFjdGl2aXRpLnRhc2tBcGkuZ2V0VGFza0NvbW1lbnRzKHRhc2tJZCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgY29tbWVudHM6IENvbW1lbnRNb2RlbFtdID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UuZGF0YS5mb3JFYWNoKChjb21tZW50OiBDb21tZW50TW9kZWwpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlciA9IG5ldyBVc2VyUHJvY2Vzc01vZGVsKGNvbW1lbnQuY3JlYXRlZEJ5KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29tbWVudHMucHVzaChuZXcgQ29tbWVudE1vZGVsKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBjb21tZW50LmlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogY29tbWVudC5tZXNzYWdlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY3JlYXRlZDogY29tbWVudC5jcmVhdGVkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY3JlYXRlZEJ5OiB1c2VyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gY29tbWVudHM7XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycjogYW55KSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGFsbCBjb21tZW50cyB0aGF0IGhhdmUgYmVlbiBhZGRlZCB0byBhIHByb2Nlc3MgaW5zdGFuY2UuXHJcbiAgICAgKiBAcGFyYW0gcHJvY2Vzc0luc3RhbmNlSWQgSUQgb2YgdGhlIHRhcmdldCBwcm9jZXNzIGluc3RhbmNlXHJcbiAgICAgKiBAcmV0dXJucyBEZXRhaWxzIGZvciBlYWNoIGNvbW1lbnRcclxuICAgICAqL1xyXG4gICAgZ2V0UHJvY2Vzc0luc3RhbmNlQ29tbWVudHMocHJvY2Vzc0luc3RhbmNlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8Q29tbWVudE1vZGVsW10+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5hY3Rpdml0aS5jb21tZW50c0FwaS5nZXRQcm9jZXNzSW5zdGFuY2VDb21tZW50cyhwcm9jZXNzSW5zdGFuY2VJZCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKChyZXNwb25zZTogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgY29tbWVudHM6IENvbW1lbnRNb2RlbFtdID0gW107XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzcG9uc2UuZGF0YS5mb3JFYWNoKChjb21tZW50OiBDb21tZW50TW9kZWwpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdXNlciA9IG5ldyBVc2VyUHJvY2Vzc01vZGVsKGNvbW1lbnQuY3JlYXRlZEJ5KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29tbWVudHMucHVzaChuZXcgQ29tbWVudE1vZGVsKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlkOiBjb21tZW50LmlkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgbWVzc2FnZTogY29tbWVudC5tZXNzYWdlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY3JlYXRlZDogY29tbWVudC5jcmVhdGVkLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY3JlYXRlZEJ5OiB1c2VyXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pKTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gY29tbWVudHM7XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycjogYW55KSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBZGRzIGEgY29tbWVudCB0byBhIHByb2Nlc3MgaW5zdGFuY2UuXHJcbiAgICAgKiBAcGFyYW0gcHJvY2Vzc0luc3RhbmNlSWQgSUQgb2YgdGhlIHRhcmdldCBwcm9jZXNzIGluc3RhbmNlXHJcbiAgICAgKiBAcGFyYW0gbWVzc2FnZSBUZXh0IGZvciB0aGUgY29tbWVudFxyXG4gICAgICogQHJldHVybnMgRGV0YWlscyBvZiB0aGUgY29tbWVudCBhZGRlZFxyXG4gICAgICovXHJcbiAgICBhZGRQcm9jZXNzSW5zdGFuY2VDb21tZW50KHByb2Nlc3NJbnN0YW5jZUlkOiBzdHJpbmcsIG1lc3NhZ2U6IHN0cmluZyk6IE9ic2VydmFibGU8Q29tbWVudE1vZGVsPiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20oXHJcbiAgICAgICAgICAgIHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmFjdGl2aXRpLmNvbW1lbnRzQXBpLmFkZFByb2Nlc3NJbnN0YW5jZUNvbW1lbnQoeyBtZXNzYWdlOiBtZXNzYWdlIH0sIHByb2Nlc3NJbnN0YW5jZUlkKVxyXG4gICAgICAgICkucGlwZShcclxuICAgICAgICAgICAgbWFwKChyZXNwb25zZTogQ29tbWVudE1vZGVsKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbmV3IENvbW1lbnRNb2RlbCh7XHJcbiAgICAgICAgICAgICAgICAgICAgaWQ6IHJlc3BvbnNlLmlkLFxyXG4gICAgICAgICAgICAgICAgICAgIG1lc3NhZ2U6IHJlc3BvbnNlLm1lc3NhZ2UsXHJcbiAgICAgICAgICAgICAgICAgICAgY3JlYXRlZDogcmVzcG9uc2UuY3JlYXRlZCxcclxuICAgICAgICAgICAgICAgICAgICBjcmVhdGVkQnk6IHJlc3BvbnNlLmNyZWF0ZWRCeVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnI6IGFueSkgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBoYW5kbGVFcnJvcihlcnJvcjogYW55KSB7XHJcbiAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKGVycm9yKTtcclxuICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihlcnJvciB8fCAnU2VydmVyIGVycm9yJyk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==