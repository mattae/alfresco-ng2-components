/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Subject, from, throwError } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { AuthenticationService } from './authentication.service';
import { LogService } from './log.service';
import { catchError } from 'rxjs/operators';
import { PermissionsEnum } from '../models/permissions.enum';
import { AllowableOperationsEnum } from '../models/allowable-operations.enum';
import { DownloadService } from './download.service';
import { ThumbnailService } from './thumbnail.service';
import * as i0 from "@angular/core";
import * as i1 from "./authentication.service";
import * as i2 from "./alfresco-api.service";
import * as i3 from "./log.service";
import * as i4 from "@angular/platform-browser";
import * as i5 from "./download.service";
import * as i6 from "./thumbnail.service";
var ContentService = /** @class */ (function () {
    function ContentService(authService, apiService, logService, sanitizer, downloadService, thumbnailService) {
        this.authService = authService;
        this.apiService = apiService;
        this.logService = logService;
        this.sanitizer = sanitizer;
        this.downloadService = downloadService;
        this.thumbnailService = thumbnailService;
        this.folderCreated = new Subject();
        this.folderCreate = new Subject();
        this.folderEdit = new Subject();
    }
    /**
     * @deprecated in 3.2.0, use DownloadService instead.
     * Invokes content download for a Blob with a file name.
     * @param blob Content to download.
     * @param fileName Name of the resulting file.
     */
    /**
     * @deprecated in 3.2.0, use DownloadService instead.
     * Invokes content download for a Blob with a file name.
     * @param {?} blob Content to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    ContentService.prototype.downloadBlob = /**
     * @deprecated in 3.2.0, use DownloadService instead.
     * Invokes content download for a Blob with a file name.
     * @param {?} blob Content to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    function (blob, fileName) {
        this.downloadService.downloadBlob(blob, fileName);
    };
    /**
     * @deprecated in 3.2.0, use DownloadService instead.
     * Invokes content download for a data array with a file name.
     * @param data Data to download.
     * @param fileName Name of the resulting file.
     */
    /**
     * @deprecated in 3.2.0, use DownloadService instead.
     * Invokes content download for a data array with a file name.
     * @param {?} data Data to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    ContentService.prototype.downloadData = /**
     * @deprecated in 3.2.0, use DownloadService instead.
     * Invokes content download for a data array with a file name.
     * @param {?} data Data to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    function (data, fileName) {
        this.downloadService.downloadData(data, fileName);
    };
    /**
     * @deprecated in 3.2.0, use DownloadService instead.
     * Invokes content download for a JSON object with a file name.
     * @param json JSON object to download.
     * @param fileName Name of the resulting file.
     */
    /**
     * @deprecated in 3.2.0, use DownloadService instead.
     * Invokes content download for a JSON object with a file name.
     * @param {?} json JSON object to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    ContentService.prototype.downloadJSON = /**
     * @deprecated in 3.2.0, use DownloadService instead.
     * Invokes content download for a JSON object with a file name.
     * @param {?} json JSON object to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    function (json, fileName) {
        this.downloadService.downloadJSON(json, fileName);
    };
    /**
     * Creates a trusted object URL from the Blob.
     * WARNING: calling this method with untrusted user data exposes your application to XSS security risks!
     * @param  blob Data to wrap into object URL
     * @returns URL string
     */
    /**
     * Creates a trusted object URL from the Blob.
     * WARNING: calling this method with untrusted user data exposes your application to XSS security risks!
     * @param {?} blob Data to wrap into object URL
     * @return {?} URL string
     */
    ContentService.prototype.createTrustedUrl = /**
     * Creates a trusted object URL from the Blob.
     * WARNING: calling this method with untrusted user data exposes your application to XSS security risks!
     * @param {?} blob Data to wrap into object URL
     * @return {?} URL string
     */
    function (blob) {
        /** @type {?} */
        var url = window.URL.createObjectURL(blob);
        return (/** @type {?} */ (this.sanitizer.bypassSecurityTrustUrl(url)));
    };
    Object.defineProperty(ContentService.prototype, "contentApi", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.apiService.getInstance().content;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @deprecated in 3.2.0, use ThumbnailService instead.
     * Gets a thumbnail URL for the given document node.
     * @param node Node or Node ID to get URL for.
     * @param attachment Toggles whether to retrieve content as an attachment for download
     * @param ticket Custom ticket to use for authentication
     * @returns URL string
     */
    /**
     * @deprecated in 3.2.0, use ThumbnailService instead.
     * Gets a thumbnail URL for the given document node.
     * @param {?} node Node or Node ID to get URL for.
     * @param {?=} attachment Toggles whether to retrieve content as an attachment for download
     * @param {?=} ticket Custom ticket to use for authentication
     * @return {?} URL string
     */
    ContentService.prototype.getDocumentThumbnailUrl = /**
     * @deprecated in 3.2.0, use ThumbnailService instead.
     * Gets a thumbnail URL for the given document node.
     * @param {?} node Node or Node ID to get URL for.
     * @param {?=} attachment Toggles whether to retrieve content as an attachment for download
     * @param {?=} ticket Custom ticket to use for authentication
     * @return {?} URL string
     */
    function (node, attachment, ticket) {
        return this.thumbnailService.getDocumentThumbnailUrl(node, attachment, ticket);
    };
    /**
     * Gets a content URL for the given node.
     * @param node Node or Node ID to get URL for.
     * @param attachment Toggles whether to retrieve content as an attachment for download
     * @param ticket Custom ticket to use for authentication
     * @returns URL string or `null`
     */
    /**
     * Gets a content URL for the given node.
     * @param {?} node Node or Node ID to get URL for.
     * @param {?=} attachment Toggles whether to retrieve content as an attachment for download
     * @param {?=} ticket Custom ticket to use for authentication
     * @return {?} URL string or `null`
     */
    ContentService.prototype.getContentUrl = /**
     * Gets a content URL for the given node.
     * @param {?} node Node or Node ID to get URL for.
     * @param {?=} attachment Toggles whether to retrieve content as an attachment for download
     * @param {?=} ticket Custom ticket to use for authentication
     * @return {?} URL string or `null`
     */
    function (node, attachment, ticket) {
        if (node) {
            /** @type {?} */
            var nodeId = void 0;
            if (typeof node === 'string') {
                nodeId = node;
            }
            else if (node.entry) {
                nodeId = node.entry.id;
            }
            return this.contentApi.getContentUrl(nodeId, attachment, ticket);
        }
        return null;
    };
    /**
     * Gets content for the given node.
     * @param nodeId ID of the target node
     * @returns Content data
     */
    /**
     * Gets content for the given node.
     * @param {?} nodeId ID of the target node
     * @return {?} Content data
     */
    ContentService.prototype.getNodeContent = /**
     * Gets content for the given node.
     * @param {?} nodeId ID of the target node
     * @return {?} Content data
     */
    function (nodeId) {
        var _this = this;
        return from(this.apiService.getInstance().core.nodesApi.getFileContent(nodeId))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets a Node via its node ID.
     * @param nodeId ID of the target node
     * @param opts Options supported by JS-API
     * @returns Details of the folder
     */
    /**
     * Gets a Node via its node ID.
     * @param {?} nodeId ID of the target node
     * @param {?=} opts Options supported by JS-API
     * @return {?} Details of the folder
     */
    ContentService.prototype.getNode = /**
     * Gets a Node via its node ID.
     * @param {?} nodeId ID of the target node
     * @param {?=} opts Options supported by JS-API
     * @return {?} Details of the folder
     */
    function (nodeId, opts) {
        return from(this.apiService.getInstance().nodes.getNode(nodeId, opts));
    };
    /**
     * Checks if the user has permission on that node
     * @param node Node to check permissions
     * @param permission Required permission type
     * @returns True if the user has the required permissions, false otherwise
     */
    /**
     * Checks if the user has permission on that node
     * @param {?} node Node to check permissions
     * @param {?} permission Required permission type
     * @return {?} True if the user has the required permissions, false otherwise
     */
    ContentService.prototype.hasPermissions = /**
     * Checks if the user has permission on that node
     * @param {?} node Node to check permissions
     * @param {?} permission Required permission type
     * @return {?} True if the user has the required permissions, false otherwise
     */
    function (node, permission) {
        /** @type {?} */
        var hasPermissions = false;
        if (node && node.permissions && node.permissions.locallySet) {
            if (permission && permission.startsWith('!')) {
                hasPermissions = node.permissions.locallySet.find((/**
                 * @param {?} currentPermission
                 * @return {?}
                 */
                function (currentPermission) { return currentPermission.name === permission.replace('!', ''); })) ? false : true;
            }
            else {
                hasPermissions = node.permissions.locallySet.find((/**
                 * @param {?} currentPermission
                 * @return {?}
                 */
                function (currentPermission) { return currentPermission.name === permission; })) ? true : false;
            }
        }
        else {
            if (permission === PermissionsEnum.CONSUMER) {
                hasPermissions = true;
            }
            else if (permission === PermissionsEnum.NOT_CONSUMER) {
                hasPermissions = false;
            }
            else if (permission && permission.startsWith('!')) {
                hasPermissions = true;
            }
        }
        return hasPermissions;
    };
    /**
     * Checks if the user has permissions on that node
     * @param node Node to check allowableOperations
     * @param allowableOperation Create, delete, update, updatePermissions, !create, !delete, !update, !updatePermissions
     * @returns True if the user has the required permissions, false otherwise
     */
    /**
     * Checks if the user has permissions on that node
     * @param {?} node Node to check allowableOperations
     * @param {?} allowableOperation Create, delete, update, updatePermissions, !create, !delete, !update, !updatePermissions
     * @return {?} True if the user has the required permissions, false otherwise
     */
    ContentService.prototype.hasAllowableOperations = /**
     * Checks if the user has permissions on that node
     * @param {?} node Node to check allowableOperations
     * @param {?} allowableOperation Create, delete, update, updatePermissions, !create, !delete, !update, !updatePermissions
     * @return {?} True if the user has the required permissions, false otherwise
     */
    function (node, allowableOperation) {
        /** @type {?} */
        var hasAllowableOperations = false;
        if (node && node.allowableOperations) {
            if (allowableOperation && allowableOperation.startsWith('!')) {
                hasAllowableOperations = node.allowableOperations.find((/**
                 * @param {?} currentOperation
                 * @return {?}
                 */
                function (currentOperation) { return currentOperation === allowableOperation.replace('!', ''); })) ? false : true;
            }
            else {
                hasAllowableOperations = node.allowableOperations.find((/**
                 * @param {?} currentOperation
                 * @return {?}
                 */
                function (currentOperation) { return currentOperation === allowableOperation; })) ? true : false;
            }
        }
        else {
            if (allowableOperation && allowableOperation.startsWith('!')) {
                hasAllowableOperations = true;
            }
        }
        if (allowableOperation === AllowableOperationsEnum.COPY) {
            hasAllowableOperations = true;
        }
        if (allowableOperation === AllowableOperationsEnum.LOCK) {
            hasAllowableOperations = node.isFile;
            if (node.isLocked && node.allowableOperations) {
                hasAllowableOperations = !!~node.allowableOperations.indexOf('updatePermissions');
            }
        }
        return hasAllowableOperations;
    };
    /**
     * @private
     * @param {?} error
     * @return {?}
     */
    ContentService.prototype.handleError = /**
     * @private
     * @param {?} error
     * @return {?}
     */
    function (error) {
        this.logService.error(error);
        return throwError(error || 'Server error');
    };
    ContentService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    ContentService.ctorParameters = function () { return [
        { type: AuthenticationService },
        { type: AlfrescoApiService },
        { type: LogService },
        { type: DomSanitizer },
        { type: DownloadService },
        { type: ThumbnailService }
    ]; };
    /** @nocollapse */ ContentService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ContentService_Factory() { return new ContentService(i0.ɵɵinject(i1.AuthenticationService), i0.ɵɵinject(i2.AlfrescoApiService), i0.ɵɵinject(i3.LogService), i0.ɵɵinject(i4.DomSanitizer), i0.ɵɵinject(i5.DownloadService), i0.ɵɵinject(i6.ThumbnailService)); }, token: ContentService, providedIn: "root" });
    return ContentService;
}());
export { ContentService };
if (false) {
    /** @type {?} */
    ContentService.prototype.folderCreated;
    /** @type {?} */
    ContentService.prototype.folderCreate;
    /** @type {?} */
    ContentService.prototype.folderEdit;
    /** @type {?} */
    ContentService.prototype.authService;
    /** @type {?} */
    ContentService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    ContentService.prototype.logService;
    /**
     * @type {?}
     * @private
     */
    ContentService.prototype.sanitizer;
    /**
     * @type {?}
     * @private
     */
    ContentService.prototype.downloadService;
    /**
     * @type {?}
     * @private
     */
    ContentService.prototype.thumbnailService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY29udGVudC5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvY29udGVudC5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRXpELE9BQU8sRUFBYyxPQUFPLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUU3RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUscUJBQXFCLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUNqRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUM1QyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDN0QsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDOUUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBQ3JELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFCQUFxQixDQUFDOzs7Ozs7OztBQUV2RDtJQVNJLHdCQUFtQixXQUFrQyxFQUNsQyxVQUE4QixFQUM3QixVQUFzQixFQUN0QixTQUF1QixFQUN2QixlQUFnQyxFQUNoQyxnQkFBa0M7UUFMbkMsZ0JBQVcsR0FBWCxXQUFXLENBQXVCO1FBQ2xDLGVBQVUsR0FBVixVQUFVLENBQW9CO1FBQzdCLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIsY0FBUyxHQUFULFNBQVMsQ0FBYztRQUN2QixvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFDaEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQVR0RCxrQkFBYSxHQUFnQyxJQUFJLE9BQU8sRUFBc0IsQ0FBQztRQUMvRSxpQkFBWSxHQUF5QixJQUFJLE9BQU8sRUFBZSxDQUFDO1FBQ2hFLGVBQVUsR0FBeUIsSUFBSSxPQUFPLEVBQWUsQ0FBQztJQVE5RCxDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7O0lBQ0gscUNBQVk7Ozs7Ozs7SUFBWixVQUFhLElBQVUsRUFBRSxRQUFnQjtRQUNyQyxJQUFJLENBQUMsZUFBZSxDQUFDLFlBQVksQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDdEQsQ0FBQztJQUVEOzs7OztPQUtHOzs7Ozs7OztJQUNILHFDQUFZOzs7Ozs7O0lBQVosVUFBYSxJQUFTLEVBQUUsUUFBZ0I7UUFDcEMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7Ozs7SUFDSCxxQ0FBWTs7Ozs7OztJQUFaLFVBQWEsSUFBUyxFQUFFLFFBQWdCO1FBQ3BDLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztJQUN0RCxDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCx5Q0FBZ0I7Ozs7OztJQUFoQixVQUFpQixJQUFVOztZQUNqQixHQUFHLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDO1FBQzVDLE9BQU8sbUJBQVMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxzQkFBc0IsQ0FBQyxHQUFHLENBQUMsRUFBQSxDQUFDO0lBQy9ELENBQUM7SUFFRCxzQkFBWSxzQ0FBVTs7Ozs7UUFBdEI7WUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDO1FBQ2pELENBQUM7OztPQUFBO0lBRUQ7Ozs7Ozs7T0FPRzs7Ozs7Ozs7O0lBQ0gsZ0RBQXVCOzs7Ozs7OztJQUF2QixVQUF3QixJQUF3QixFQUFFLFVBQW9CLEVBQUUsTUFBZTtRQUNuRixPQUFPLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ25GLENBQUM7SUFFRDs7Ozs7O09BTUc7Ozs7Ozs7O0lBQ0gsc0NBQWE7Ozs7Ozs7SUFBYixVQUFjLElBQXdCLEVBQUUsVUFBb0IsRUFBRSxNQUFlO1FBQ3pFLElBQUksSUFBSSxFQUFFOztnQkFDRixNQUFNLFNBQVE7WUFFbEIsSUFBSSxPQUFPLElBQUksS0FBSyxRQUFRLEVBQUU7Z0JBQzFCLE1BQU0sR0FBRyxJQUFJLENBQUM7YUFDakI7aUJBQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFO2dCQUNuQixNQUFNLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUM7YUFDMUI7WUFFRCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxVQUFVLEVBQUUsTUFBTSxDQUFDLENBQUM7U0FDcEU7UUFFRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsdUNBQWM7Ozs7O0lBQWQsVUFBZSxNQUFjO1FBQTdCLGlCQUtDO1FBSkcsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxNQUFNLENBQUMsQ0FBQzthQUMxRSxJQUFJLENBQ0QsVUFBVTs7OztRQUFDLFVBQUMsR0FBUSxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFBQyxDQUNsRCxDQUFDO0lBQ1YsQ0FBQztJQUVEOzs7OztPQUtHOzs7Ozs7O0lBQ0gsZ0NBQU87Ozs7OztJQUFQLFVBQVEsTUFBYyxFQUFFLElBQVU7UUFDOUIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQyxDQUFDO0lBQzNFLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILHVDQUFjOzs7Ozs7SUFBZCxVQUFlLElBQVUsRUFBRSxVQUFvQzs7WUFDdkQsY0FBYyxHQUFHLEtBQUs7UUFFMUIsSUFBSSxJQUFJLElBQUksSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsRUFBRTtZQUN6RCxJQUFJLFVBQVUsSUFBSSxVQUFVLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUMxQyxjQUFjLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVLENBQUMsSUFBSTs7OztnQkFBQyxVQUFDLGlCQUFpQixJQUFLLE9BQUEsaUJBQWlCLENBQUMsSUFBSSxLQUFLLFVBQVUsQ0FBQyxPQUFPLENBQUMsR0FBRyxFQUFFLEVBQUUsQ0FBQyxFQUF0RCxDQUFzRCxFQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO2FBQ25KO2lCQUFNO2dCQUNILGNBQWMsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxJQUFJOzs7O2dCQUFDLFVBQUMsaUJBQWlCLElBQUssT0FBQSxpQkFBaUIsQ0FBQyxJQUFJLEtBQUssVUFBVSxFQUFyQyxDQUFxQyxFQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2FBQ2xJO1NBRUo7YUFBTTtZQUVILElBQUksVUFBVSxLQUFLLGVBQWUsQ0FBQyxRQUFRLEVBQUU7Z0JBQ3pDLGNBQWMsR0FBRyxJQUFJLENBQUM7YUFDekI7aUJBQU0sSUFBSSxVQUFVLEtBQUssZUFBZSxDQUFDLFlBQVksRUFBRTtnQkFDcEQsY0FBYyxHQUFHLEtBQUssQ0FBQzthQUMxQjtpQkFBTSxJQUFJLFVBQVUsSUFBSSxVQUFVLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNqRCxjQUFjLEdBQUcsSUFBSSxDQUFDO2FBQ3pCO1NBQ0o7UUFFRCxPQUFPLGNBQWMsQ0FBQztJQUMxQixDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCwrQ0FBc0I7Ozs7OztJQUF0QixVQUF1QixJQUFVLEVBQUUsa0JBQW9EOztZQUMvRSxzQkFBc0IsR0FBRyxLQUFLO1FBRWxDLElBQUksSUFBSSxJQUFJLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtZQUNsQyxJQUFJLGtCQUFrQixJQUFJLGtCQUFrQixDQUFDLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDMUQsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUk7Ozs7Z0JBQUMsVUFBQyxnQkFBZ0IsSUFBSyxPQUFBLGdCQUFnQixLQUFLLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsRUFBRSxDQUFDLEVBQXhELENBQXdELEVBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUM7YUFDeko7aUJBQU07Z0JBQ0gsc0JBQXNCLEdBQUcsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUk7Ozs7Z0JBQUMsVUFBQyxnQkFBZ0IsSUFBSyxPQUFBLGdCQUFnQixLQUFLLGtCQUFrQixFQUF2QyxDQUF1QyxFQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO2FBQ3hJO1NBRUo7YUFBTTtZQUNILElBQUksa0JBQWtCLElBQUksa0JBQWtCLENBQUMsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUMxRCxzQkFBc0IsR0FBRyxJQUFJLENBQUM7YUFDakM7U0FDSjtRQUVELElBQUksa0JBQWtCLEtBQUssdUJBQXVCLENBQUMsSUFBSSxFQUFFO1lBQ3JELHNCQUFzQixHQUFHLElBQUksQ0FBQztTQUNqQztRQUVELElBQUksa0JBQWtCLEtBQUssdUJBQXVCLENBQUMsSUFBSSxFQUFFO1lBQ3JELHNCQUFzQixHQUFHLElBQUksQ0FBQyxNQUFNLENBQUM7WUFFckMsSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxtQkFBbUIsRUFBRTtnQkFDM0Msc0JBQXNCLEdBQUcsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO2FBQ3JGO1NBQ0o7UUFFRCxPQUFPLHNCQUFzQixDQUFDO0lBQ2xDLENBQUM7Ozs7OztJQUVPLG9DQUFXOzs7OztJQUFuQixVQUFvQixLQUFVO1FBQzFCLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQzdCLE9BQU8sVUFBVSxDQUFDLEtBQUssSUFBSSxjQUFjLENBQUMsQ0FBQztJQUMvQyxDQUFDOztnQkE3TEosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFWUSxxQkFBcUI7Z0JBRHJCLGtCQUFrQjtnQkFFbEIsVUFBVTtnQkFOVixZQUFZO2dCQVVaLGVBQWU7Z0JBQ2YsZ0JBQWdCOzs7eUJBN0J6QjtDQTZOQyxBQTlMRCxJQThMQztTQTNMWSxjQUFjOzs7SUFFdkIsdUNBQStFOztJQUMvRSxzQ0FBZ0U7O0lBQ2hFLG9DQUE4RDs7SUFFbEQscUNBQXlDOztJQUN6QyxvQ0FBcUM7Ozs7O0lBQ3JDLG9DQUE4Qjs7Ozs7SUFDOUIsbUNBQStCOzs7OztJQUMvQix5Q0FBd0M7Ozs7O0lBQ3hDLDBDQUEwQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IERvbVNhbml0aXplciB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xyXG5pbXBvcnQgeyBDb250ZW50QXBpLCBNaW5pbWFsTm9kZSwgTm9kZSwgTm9kZUVudHJ5IH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIFN1YmplY3QsIGZyb20sIHRocm93RXJyb3IgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgRm9sZGVyQ3JlYXRlZEV2ZW50IH0gZnJvbSAnLi4vZXZlbnRzL2ZvbGRlci1jcmVhdGVkLmV2ZW50JztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IEF1dGhlbnRpY2F0aW9uU2VydmljZSB9IGZyb20gJy4vYXV0aGVudGljYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgY2F0Y2hFcnJvciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgUGVybWlzc2lvbnNFbnVtIH0gZnJvbSAnLi4vbW9kZWxzL3Blcm1pc3Npb25zLmVudW0nO1xyXG5pbXBvcnQgeyBBbGxvd2FibGVPcGVyYXRpb25zRW51bSB9IGZyb20gJy4uL21vZGVscy9hbGxvd2FibGUtb3BlcmF0aW9ucy5lbnVtJztcclxuaW1wb3J0IHsgRG93bmxvYWRTZXJ2aWNlIH0gZnJvbSAnLi9kb3dubG9hZC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVGh1bWJuYWlsU2VydmljZSB9IGZyb20gJy4vdGh1bWJuYWlsLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBDb250ZW50U2VydmljZSB7XHJcblxyXG4gICAgZm9sZGVyQ3JlYXRlZDogU3ViamVjdDxGb2xkZXJDcmVhdGVkRXZlbnQ+ID0gbmV3IFN1YmplY3Q8Rm9sZGVyQ3JlYXRlZEV2ZW50PigpO1xyXG4gICAgZm9sZGVyQ3JlYXRlOiBTdWJqZWN0PE1pbmltYWxOb2RlPiA9IG5ldyBTdWJqZWN0PE1pbmltYWxOb2RlPigpO1xyXG4gICAgZm9sZGVyRWRpdDogU3ViamVjdDxNaW5pbWFsTm9kZT4gPSBuZXcgU3ViamVjdDxNaW5pbWFsTm9kZT4oKTtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihwdWJsaWMgYXV0aFNlcnZpY2U6IEF1dGhlbnRpY2F0aW9uU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHB1YmxpYyBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIHNhbml0aXplcjogRG9tU2FuaXRpemVyLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBkb3dubG9hZFNlcnZpY2U6IERvd25sb2FkU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgdGh1bWJuYWlsU2VydmljZTogVGh1bWJuYWlsU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQGRlcHJlY2F0ZWQgaW4gMy4yLjAsIHVzZSBEb3dubG9hZFNlcnZpY2UgaW5zdGVhZC5cclxuICAgICAqIEludm9rZXMgY29udGVudCBkb3dubG9hZCBmb3IgYSBCbG9iIHdpdGggYSBmaWxlIG5hbWUuXHJcbiAgICAgKiBAcGFyYW0gYmxvYiBDb250ZW50IHRvIGRvd25sb2FkLlxyXG4gICAgICogQHBhcmFtIGZpbGVOYW1lIE5hbWUgb2YgdGhlIHJlc3VsdGluZyBmaWxlLlxyXG4gICAgICovXHJcbiAgICBkb3dubG9hZEJsb2IoYmxvYjogQmxvYiwgZmlsZU5hbWU6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZG93bmxvYWRTZXJ2aWNlLmRvd25sb2FkQmxvYihibG9iLCBmaWxlTmFtZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAZGVwcmVjYXRlZCBpbiAzLjIuMCwgdXNlIERvd25sb2FkU2VydmljZSBpbnN0ZWFkLlxyXG4gICAgICogSW52b2tlcyBjb250ZW50IGRvd25sb2FkIGZvciBhIGRhdGEgYXJyYXkgd2l0aCBhIGZpbGUgbmFtZS5cclxuICAgICAqIEBwYXJhbSBkYXRhIERhdGEgdG8gZG93bmxvYWQuXHJcbiAgICAgKiBAcGFyYW0gZmlsZU5hbWUgTmFtZSBvZiB0aGUgcmVzdWx0aW5nIGZpbGUuXHJcbiAgICAgKi9cclxuICAgIGRvd25sb2FkRGF0YShkYXRhOiBhbnksIGZpbGVOYW1lOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmRvd25sb2FkU2VydmljZS5kb3dubG9hZERhdGEoZGF0YSwgZmlsZU5hbWUpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQGRlcHJlY2F0ZWQgaW4gMy4yLjAsIHVzZSBEb3dubG9hZFNlcnZpY2UgaW5zdGVhZC5cclxuICAgICAqIEludm9rZXMgY29udGVudCBkb3dubG9hZCBmb3IgYSBKU09OIG9iamVjdCB3aXRoIGEgZmlsZSBuYW1lLlxyXG4gICAgICogQHBhcmFtIGpzb24gSlNPTiBvYmplY3QgdG8gZG93bmxvYWQuXHJcbiAgICAgKiBAcGFyYW0gZmlsZU5hbWUgTmFtZSBvZiB0aGUgcmVzdWx0aW5nIGZpbGUuXHJcbiAgICAgKi9cclxuICAgIGRvd25sb2FkSlNPTihqc29uOiBhbnksIGZpbGVOYW1lOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLmRvd25sb2FkU2VydmljZS5kb3dubG9hZEpTT04oanNvbiwgZmlsZU5hbWUpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBhIHRydXN0ZWQgb2JqZWN0IFVSTCBmcm9tIHRoZSBCbG9iLlxyXG4gICAgICogV0FSTklORzogY2FsbGluZyB0aGlzIG1ldGhvZCB3aXRoIHVudHJ1c3RlZCB1c2VyIGRhdGEgZXhwb3NlcyB5b3VyIGFwcGxpY2F0aW9uIHRvIFhTUyBzZWN1cml0eSByaXNrcyFcclxuICAgICAqIEBwYXJhbSAgYmxvYiBEYXRhIHRvIHdyYXAgaW50byBvYmplY3QgVVJMXHJcbiAgICAgKiBAcmV0dXJucyBVUkwgc3RyaW5nXHJcbiAgICAgKi9cclxuICAgIGNyZWF0ZVRydXN0ZWRVcmwoYmxvYjogQmxvYik6IHN0cmluZyB7XHJcbiAgICAgICAgY29uc3QgdXJsID0gd2luZG93LlVSTC5jcmVhdGVPYmplY3RVUkwoYmxvYik7XHJcbiAgICAgICAgcmV0dXJuIDxzdHJpbmc+IHRoaXMuc2FuaXRpemVyLmJ5cGFzc1NlY3VyaXR5VHJ1c3RVcmwodXJsKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldCBjb250ZW50QXBpKCk6IENvbnRlbnRBcGkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5jb250ZW50O1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQGRlcHJlY2F0ZWQgaW4gMy4yLjAsIHVzZSBUaHVtYm5haWxTZXJ2aWNlIGluc3RlYWQuXHJcbiAgICAgKiBHZXRzIGEgdGh1bWJuYWlsIFVSTCBmb3IgdGhlIGdpdmVuIGRvY3VtZW50IG5vZGUuXHJcbiAgICAgKiBAcGFyYW0gbm9kZSBOb2RlIG9yIE5vZGUgSUQgdG8gZ2V0IFVSTCBmb3IuXHJcbiAgICAgKiBAcGFyYW0gYXR0YWNobWVudCBUb2dnbGVzIHdoZXRoZXIgdG8gcmV0cmlldmUgY29udGVudCBhcyBhbiBhdHRhY2htZW50IGZvciBkb3dubG9hZFxyXG4gICAgICogQHBhcmFtIHRpY2tldCBDdXN0b20gdGlja2V0IHRvIHVzZSBmb3IgYXV0aGVudGljYXRpb25cclxuICAgICAqIEByZXR1cm5zIFVSTCBzdHJpbmdcclxuICAgICAqL1xyXG4gICAgZ2V0RG9jdW1lbnRUaHVtYm5haWxVcmwobm9kZTogTm9kZUVudHJ5IHwgc3RyaW5nLCBhdHRhY2htZW50PzogYm9vbGVhbiwgdGlja2V0Pzogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy50aHVtYm5haWxTZXJ2aWNlLmdldERvY3VtZW50VGh1bWJuYWlsVXJsKG5vZGUsIGF0dGFjaG1lbnQsIHRpY2tldCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgY29udGVudCBVUkwgZm9yIHRoZSBnaXZlbiBub2RlLlxyXG4gICAgICogQHBhcmFtIG5vZGUgTm9kZSBvciBOb2RlIElEIHRvIGdldCBVUkwgZm9yLlxyXG4gICAgICogQHBhcmFtIGF0dGFjaG1lbnQgVG9nZ2xlcyB3aGV0aGVyIHRvIHJldHJpZXZlIGNvbnRlbnQgYXMgYW4gYXR0YWNobWVudCBmb3IgZG93bmxvYWRcclxuICAgICAqIEBwYXJhbSB0aWNrZXQgQ3VzdG9tIHRpY2tldCB0byB1c2UgZm9yIGF1dGhlbnRpY2F0aW9uXHJcbiAgICAgKiBAcmV0dXJucyBVUkwgc3RyaW5nIG9yIGBudWxsYFxyXG4gICAgICovXHJcbiAgICBnZXRDb250ZW50VXJsKG5vZGU6IE5vZGVFbnRyeSB8IHN0cmluZywgYXR0YWNobWVudD86IGJvb2xlYW4sIHRpY2tldD86IHN0cmluZyk6IHN0cmluZyB7XHJcbiAgICAgICAgaWYgKG5vZGUpIHtcclxuICAgICAgICAgICAgbGV0IG5vZGVJZDogc3RyaW5nO1xyXG5cclxuICAgICAgICAgICAgaWYgKHR5cGVvZiBub2RlID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgbm9kZUlkID0gbm9kZTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChub2RlLmVudHJ5KSB7XHJcbiAgICAgICAgICAgICAgICBub2RlSWQgPSBub2RlLmVudHJ5LmlkO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jb250ZW50QXBpLmdldENvbnRlbnRVcmwobm9kZUlkLCBhdHRhY2htZW50LCB0aWNrZXQpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGNvbnRlbnQgZm9yIHRoZSBnaXZlbiBub2RlLlxyXG4gICAgICogQHBhcmFtIG5vZGVJZCBJRCBvZiB0aGUgdGFyZ2V0IG5vZGVcclxuICAgICAqIEByZXR1cm5zIENvbnRlbnQgZGF0YVxyXG4gICAgICovXHJcbiAgICBnZXROb2RlQ29udGVudChub2RlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuY29yZS5ub2Rlc0FwaS5nZXRGaWxlQ29udGVudChub2RlSWQpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycjogYW55KSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgTm9kZSB2aWEgaXRzIG5vZGUgSUQuXHJcbiAgICAgKiBAcGFyYW0gbm9kZUlkIElEIG9mIHRoZSB0YXJnZXQgbm9kZVxyXG4gICAgICogQHBhcmFtIG9wdHMgT3B0aW9ucyBzdXBwb3J0ZWQgYnkgSlMtQVBJXHJcbiAgICAgKiBAcmV0dXJucyBEZXRhaWxzIG9mIHRoZSBmb2xkZXJcclxuICAgICAqL1xyXG4gICAgZ2V0Tm9kZShub2RlSWQ6IHN0cmluZywgb3B0cz86IGFueSk6IE9ic2VydmFibGU8Tm9kZUVudHJ5PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkubm9kZXMuZ2V0Tm9kZShub2RlSWQsIG9wdHMpKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrcyBpZiB0aGUgdXNlciBoYXMgcGVybWlzc2lvbiBvbiB0aGF0IG5vZGVcclxuICAgICAqIEBwYXJhbSBub2RlIE5vZGUgdG8gY2hlY2sgcGVybWlzc2lvbnNcclxuICAgICAqIEBwYXJhbSBwZXJtaXNzaW9uIFJlcXVpcmVkIHBlcm1pc3Npb24gdHlwZVxyXG4gICAgICogQHJldHVybnMgVHJ1ZSBpZiB0aGUgdXNlciBoYXMgdGhlIHJlcXVpcmVkIHBlcm1pc3Npb25zLCBmYWxzZSBvdGhlcndpc2VcclxuICAgICAqL1xyXG4gICAgaGFzUGVybWlzc2lvbnMobm9kZTogTm9kZSwgcGVybWlzc2lvbjogUGVybWlzc2lvbnNFbnVtIHwgc3RyaW5nKTogYm9vbGVhbiB7XHJcbiAgICAgICAgbGV0IGhhc1Blcm1pc3Npb25zID0gZmFsc2U7XHJcblxyXG4gICAgICAgIGlmIChub2RlICYmIG5vZGUucGVybWlzc2lvbnMgJiYgbm9kZS5wZXJtaXNzaW9ucy5sb2NhbGx5U2V0KSB7XHJcbiAgICAgICAgICAgIGlmIChwZXJtaXNzaW9uICYmIHBlcm1pc3Npb24uc3RhcnRzV2l0aCgnIScpKSB7XHJcbiAgICAgICAgICAgICAgICBoYXNQZXJtaXNzaW9ucyA9IG5vZGUucGVybWlzc2lvbnMubG9jYWxseVNldC5maW5kKChjdXJyZW50UGVybWlzc2lvbikgPT4gY3VycmVudFBlcm1pc3Npb24ubmFtZSA9PT0gcGVybWlzc2lvbi5yZXBsYWNlKCchJywgJycpKSA/IGZhbHNlIDogdHJ1ZTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGhhc1Blcm1pc3Npb25zID0gbm9kZS5wZXJtaXNzaW9ucy5sb2NhbGx5U2V0LmZpbmQoKGN1cnJlbnRQZXJtaXNzaW9uKSA9PiBjdXJyZW50UGVybWlzc2lvbi5uYW1lID09PSBwZXJtaXNzaW9uKSA/IHRydWUgOiBmYWxzZTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgaWYgKHBlcm1pc3Npb24gPT09IFBlcm1pc3Npb25zRW51bS5DT05TVU1FUikge1xyXG4gICAgICAgICAgICAgICAgaGFzUGVybWlzc2lvbnMgPSB0cnVlO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHBlcm1pc3Npb24gPT09IFBlcm1pc3Npb25zRW51bS5OT1RfQ09OU1VNRVIpIHtcclxuICAgICAgICAgICAgICAgIGhhc1Blcm1pc3Npb25zID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAocGVybWlzc2lvbiAmJiBwZXJtaXNzaW9uLnN0YXJ0c1dpdGgoJyEnKSkge1xyXG4gICAgICAgICAgICAgICAgaGFzUGVybWlzc2lvbnMgPSB0cnVlO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gaGFzUGVybWlzc2lvbnM7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVja3MgaWYgdGhlIHVzZXIgaGFzIHBlcm1pc3Npb25zIG9uIHRoYXQgbm9kZVxyXG4gICAgICogQHBhcmFtIG5vZGUgTm9kZSB0byBjaGVjayBhbGxvd2FibGVPcGVyYXRpb25zXHJcbiAgICAgKiBAcGFyYW0gYWxsb3dhYmxlT3BlcmF0aW9uIENyZWF0ZSwgZGVsZXRlLCB1cGRhdGUsIHVwZGF0ZVBlcm1pc3Npb25zLCAhY3JlYXRlLCAhZGVsZXRlLCAhdXBkYXRlLCAhdXBkYXRlUGVybWlzc2lvbnNcclxuICAgICAqIEByZXR1cm5zIFRydWUgaWYgdGhlIHVzZXIgaGFzIHRoZSByZXF1aXJlZCBwZXJtaXNzaW9ucywgZmFsc2Ugb3RoZXJ3aXNlXHJcbiAgICAgKi9cclxuICAgIGhhc0FsbG93YWJsZU9wZXJhdGlvbnMobm9kZTogTm9kZSwgYWxsb3dhYmxlT3BlcmF0aW9uOiBBbGxvd2FibGVPcGVyYXRpb25zRW51bSB8IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGxldCBoYXNBbGxvd2FibGVPcGVyYXRpb25zID0gZmFsc2U7XHJcblxyXG4gICAgICAgIGlmIChub2RlICYmIG5vZGUuYWxsb3dhYmxlT3BlcmF0aW9ucykge1xyXG4gICAgICAgICAgICBpZiAoYWxsb3dhYmxlT3BlcmF0aW9uICYmIGFsbG93YWJsZU9wZXJhdGlvbi5zdGFydHNXaXRoKCchJykpIHtcclxuICAgICAgICAgICAgICAgIGhhc0FsbG93YWJsZU9wZXJhdGlvbnMgPSBub2RlLmFsbG93YWJsZU9wZXJhdGlvbnMuZmluZCgoY3VycmVudE9wZXJhdGlvbikgPT4gY3VycmVudE9wZXJhdGlvbiA9PT0gYWxsb3dhYmxlT3BlcmF0aW9uLnJlcGxhY2UoJyEnLCAnJykpID8gZmFsc2UgOiB0cnVlO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgaGFzQWxsb3dhYmxlT3BlcmF0aW9ucyA9IG5vZGUuYWxsb3dhYmxlT3BlcmF0aW9ucy5maW5kKChjdXJyZW50T3BlcmF0aW9uKSA9PiBjdXJyZW50T3BlcmF0aW9uID09PSBhbGxvd2FibGVPcGVyYXRpb24pID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGlmIChhbGxvd2FibGVPcGVyYXRpb24gJiYgYWxsb3dhYmxlT3BlcmF0aW9uLnN0YXJ0c1dpdGgoJyEnKSkge1xyXG4gICAgICAgICAgICAgICAgaGFzQWxsb3dhYmxlT3BlcmF0aW9ucyA9IHRydWU7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChhbGxvd2FibGVPcGVyYXRpb24gPT09IEFsbG93YWJsZU9wZXJhdGlvbnNFbnVtLkNPUFkpIHtcclxuICAgICAgICAgICAgaGFzQWxsb3dhYmxlT3BlcmF0aW9ucyA9IHRydWU7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoYWxsb3dhYmxlT3BlcmF0aW9uID09PSBBbGxvd2FibGVPcGVyYXRpb25zRW51bS5MT0NLKSB7XHJcbiAgICAgICAgICAgIGhhc0FsbG93YWJsZU9wZXJhdGlvbnMgPSBub2RlLmlzRmlsZTtcclxuXHJcbiAgICAgICAgICAgIGlmIChub2RlLmlzTG9ja2VkICYmIG5vZGUuYWxsb3dhYmxlT3BlcmF0aW9ucykge1xyXG4gICAgICAgICAgICAgICAgaGFzQWxsb3dhYmxlT3BlcmF0aW9ucyA9ICEhfm5vZGUuYWxsb3dhYmxlT3BlcmF0aW9ucy5pbmRleE9mKCd1cGRhdGVQZXJtaXNzaW9ucycpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gaGFzQWxsb3dhYmxlT3BlcmF0aW9ucztcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGhhbmRsZUVycm9yKGVycm9yOiBhbnkpIHtcclxuICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZXJyb3IoZXJyb3IpO1xyXG4gICAgICAgIHJldHVybiB0aHJvd0Vycm9yKGVycm9yIHx8ICdTZXJ2ZXIgZXJyb3InKTtcclxuICAgIH1cclxufVxyXG4iXX0=