/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, of } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { UserPreferencesService } from './user-preferences.service';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./user-preferences.service";
var DeletedNodesApiService = /** @class */ (function () {
    function DeletedNodesApiService(apiService, preferences) {
        this.apiService = apiService;
        this.preferences = preferences;
    }
    Object.defineProperty(DeletedNodesApiService.prototype, "nodesApi", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.apiService.getInstance().core.nodesApi;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Gets a list of nodes in the trash.
     * @param options Options for JS-API call
     * @returns List of nodes in the trash
     */
    /**
     * Gets a list of nodes in the trash.
     * @param {?=} options Options for JS-API call
     * @return {?} List of nodes in the trash
     */
    DeletedNodesApiService.prototype.getDeletedNodes = /**
     * Gets a list of nodes in the trash.
     * @param {?=} options Options for JS-API call
     * @return {?} List of nodes in the trash
     */
    function (options) {
        /** @type {?} */
        var defaultOptions = {
            include: ['path', 'properties'],
            maxItems: this.preferences.paginationSize,
            skipCount: 0
        };
        /** @type {?} */
        var queryOptions = Object.assign(defaultOptions, options);
        /** @type {?} */
        var promise = this.nodesApi.getDeletedNodes(queryOptions);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return of(err); })));
    };
    DeletedNodesApiService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    DeletedNodesApiService.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: UserPreferencesService }
    ]; };
    /** @nocollapse */ DeletedNodesApiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function DeletedNodesApiService_Factory() { return new DeletedNodesApiService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.UserPreferencesService)); }, token: DeletedNodesApiService, providedIn: "root" });
    return DeletedNodesApiService;
}());
export { DeletedNodesApiService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    DeletedNodesApiService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    DeletedNodesApiService.prototype.preferences;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGVsZXRlZC1ub2Rlcy1hcGkuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2RlbGV0ZWQtbm9kZXMtYXBpLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQWMsSUFBSSxFQUFFLEVBQUUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUc1QyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsc0JBQXNCLEVBQUUsTUFBTSw0QkFBNEIsQ0FBQztBQUNwRSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7QUFFNUM7SUFJSSxnQ0FDWSxVQUE4QixFQUM5QixXQUFtQztRQURuQyxlQUFVLEdBQVYsVUFBVSxDQUFvQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBd0I7SUFDNUMsQ0FBQztJQUVKLHNCQUFZLDRDQUFROzs7OztRQUFwQjtZQUNHLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDO1FBQ3RELENBQUM7OztPQUFBO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsZ0RBQWU7Ozs7O0lBQWYsVUFBZ0IsT0FBZ0I7O1lBQ3RCLGNBQWMsR0FBRztZQUNuQixPQUFPLEVBQUUsQ0FBRSxNQUFNLEVBQUUsWUFBWSxDQUFFO1lBQ2pDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWM7WUFDekMsU0FBUyxFQUFFLENBQUM7U0FDZjs7WUFDSyxZQUFZLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxjQUFjLEVBQUUsT0FBTyxDQUFDOztZQUNyRCxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxlQUFlLENBQUMsWUFBWSxDQUFDO1FBRTNELE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FDckIsVUFBVTs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFQLENBQU8sRUFBQyxDQUMvQixDQUFDO0lBQ04sQ0FBQzs7Z0JBOUJKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7Z0JBTlEsa0JBQWtCO2dCQUNsQixzQkFBc0I7OztpQ0F0Qi9CO0NBd0RDLEFBL0JELElBK0JDO1NBNUJZLHNCQUFzQjs7Ozs7O0lBRTNCLDRDQUFzQzs7Ozs7SUFDdEMsNkNBQTJDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgZnJvbSwgb2YgfSBmcm9tICdyeGpzJztcclxuXHJcbmltcG9ydCB7IE5vZGVQYWdpbmcgfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IFVzZXJQcmVmZXJlbmNlc1NlcnZpY2UgfSBmcm9tICcuL3VzZXItcHJlZmVyZW5jZXMuc2VydmljZSc7XHJcbmltcG9ydCB7IGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIERlbGV0ZWROb2Rlc0FwaVNlcnZpY2Uge1xyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBwcmVmZXJlbmNlczogVXNlclByZWZlcmVuY2VzU2VydmljZVxyXG4gICAgKSB7fVxyXG5cclxuICAgIHByaXZhdGUgZ2V0IG5vZGVzQXBpKCkge1xyXG4gICAgICAgcmV0dXJuIHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmNvcmUubm9kZXNBcGk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgbGlzdCBvZiBub2RlcyBpbiB0aGUgdHJhc2guXHJcbiAgICAgKiBAcGFyYW0gb3B0aW9ucyBPcHRpb25zIGZvciBKUy1BUEkgY2FsbFxyXG4gICAgICogQHJldHVybnMgTGlzdCBvZiBub2RlcyBpbiB0aGUgdHJhc2hcclxuICAgICAqL1xyXG4gICAgZ2V0RGVsZXRlZE5vZGVzKG9wdGlvbnM/OiBPYmplY3QpOiBPYnNlcnZhYmxlPE5vZGVQYWdpbmc+IHtcclxuICAgICAgICBjb25zdCBkZWZhdWx0T3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgaW5jbHVkZTogWyAncGF0aCcsICdwcm9wZXJ0aWVzJyBdLFxyXG4gICAgICAgICAgICBtYXhJdGVtczogdGhpcy5wcmVmZXJlbmNlcy5wYWdpbmF0aW9uU2l6ZSxcclxuICAgICAgICAgICAgc2tpcENvdW50OiAwXHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zdCBxdWVyeU9wdGlvbnMgPSBPYmplY3QuYXNzaWduKGRlZmF1bHRPcHRpb25zLCBvcHRpb25zKTtcclxuICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5ub2Rlc0FwaS5nZXREZWxldGVkTm9kZXMocXVlcnlPcHRpb25zKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20ocHJvbWlzZSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiBvZihlcnIpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19