/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { BpmProductVersionModel, EcmProductVersionModel } from '../models/product-version.model';
import { AlfrescoApiService } from './alfresco-api.service';
import { map, catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
var DiscoveryApiService = /** @class */ (function () {
    function DiscoveryApiService(apiService) {
        this.apiService = apiService;
    }
    /**
     * Gets product information for Content Services.
     * @returns ProductVersionModel containing product details
     */
    /**
     * Gets product information for Content Services.
     * @return {?} ProductVersionModel containing product details
     */
    DiscoveryApiService.prototype.getEcmProductInfo = /**
     * Gets product information for Content Services.
     * @return {?} ProductVersionModel containing product details
     */
    function () {
        return from(this.apiService.getInstance().discovery.discoveryApi.getRepositoryInformation())
            .pipe(map((/**
         * @param {?} res
         * @return {?}
         */
        function (res) { return new EcmProductVersionModel(res); })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return throwError(err); })));
    };
    /**
     * Gets product information for Process Services.
     * @returns ProductVersionModel containing product details
     */
    /**
     * Gets product information for Process Services.
     * @return {?} ProductVersionModel containing product details
     */
    DiscoveryApiService.prototype.getBpmProductInfo = /**
     * Gets product information for Process Services.
     * @return {?} ProductVersionModel containing product details
     */
    function () {
        return from(this.apiService.getInstance().activiti.aboutApi.getAppVersion())
            .pipe(map((/**
         * @param {?} res
         * @return {?}
         */
        function (res) { return new BpmProductVersionModel(res); })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return throwError(err); })));
    };
    DiscoveryApiService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    DiscoveryApiService.ctorParameters = function () { return [
        { type: AlfrescoApiService }
    ]; };
    /** @nocollapse */ DiscoveryApiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function DiscoveryApiService_Factory() { return new DiscoveryApiService(i0.ɵɵinject(i1.AlfrescoApiService)); }, token: DiscoveryApiService, providedIn: "root" });
    return DiscoveryApiService;
}());
export { DiscoveryApiService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    DiscoveryApiService.prototype.apiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZGlzY292ZXJ5LWFwaS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvZGlzY292ZXJ5LWFwaS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLElBQUksRUFBRSxVQUFVLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDeEMsT0FBTyxFQUFFLHNCQUFzQixFQUFFLHNCQUFzQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDakcsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDNUQsT0FBTyxFQUFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7O0FBR2pEO0lBS0ksNkJBQW9CLFVBQThCO1FBQTlCLGVBQVUsR0FBVixVQUFVLENBQW9CO0lBQUksQ0FBQztJQUV2RDs7O09BR0c7Ozs7O0lBQ0ksK0NBQWlCOzs7O0lBQXhCO1FBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDLHdCQUF3QixFQUFFLENBQUM7YUFDdkYsSUFBSSxDQUNELEdBQUc7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLElBQUksc0JBQXNCLENBQUMsR0FBRyxDQUFDLEVBQS9CLENBQStCLEVBQUMsRUFDN0MsVUFBVTs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFmLENBQWUsRUFBQyxDQUN2QyxDQUFDO0lBQ1YsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7SUFDSSwrQ0FBaUI7Ozs7SUFBeEI7UUFDSSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsYUFBYSxFQUFFLENBQUM7YUFDdkUsSUFBSSxDQUNELEdBQUc7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLElBQUksc0JBQXNCLENBQUMsR0FBRyxDQUFDLEVBQS9CLENBQStCLEVBQUMsRUFDN0MsVUFBVTs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsVUFBVSxDQUFDLEdBQUcsQ0FBQyxFQUFmLENBQWUsRUFBQyxDQUN2QyxDQUFDO0lBQ1YsQ0FBQzs7Z0JBN0JKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7Z0JBTlEsa0JBQWtCOzs7OEJBcEIzQjtDQXNEQyxBQTlCRCxJQThCQztTQTNCWSxtQkFBbUI7Ozs7OztJQUVoQix5Q0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBmcm9tLCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEJwbVByb2R1Y3RWZXJzaW9uTW9kZWwsIEVjbVByb2R1Y3RWZXJzaW9uTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvcHJvZHVjdC12ZXJzaW9uLm1vZGVsJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IG1hcCwgY2F0Y2hFcnJvciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBEaXNjb3ZlcnlBcGlTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSkgeyB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHByb2R1Y3QgaW5mb3JtYXRpb24gZm9yIENvbnRlbnQgU2VydmljZXMuXHJcbiAgICAgKiBAcmV0dXJucyBQcm9kdWN0VmVyc2lvbk1vZGVsIGNvbnRhaW5pbmcgcHJvZHVjdCBkZXRhaWxzXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXRFY21Qcm9kdWN0SW5mbygpOiBPYnNlcnZhYmxlPEVjbVByb2R1Y3RWZXJzaW9uTW9kZWw+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5kaXNjb3ZlcnkuZGlzY292ZXJ5QXBpLmdldFJlcG9zaXRvcnlJbmZvcm1hdGlvbigpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCgocmVzKSA9PiBuZXcgRWNtUHJvZHVjdFZlcnNpb25Nb2RlbChyZXMpKSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhyb3dFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBwcm9kdWN0IGluZm9ybWF0aW9uIGZvciBQcm9jZXNzIFNlcnZpY2VzLlxyXG4gICAgICogQHJldHVybnMgUHJvZHVjdFZlcnNpb25Nb2RlbCBjb250YWluaW5nIHByb2R1Y3QgZGV0YWlsc1xyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0QnBtUHJvZHVjdEluZm8oKTogT2JzZXJ2YWJsZTxCcG1Qcm9kdWN0VmVyc2lvbk1vZGVsPiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuYWN0aXZpdGkuYWJvdXRBcGkuZ2V0QXBwVmVyc2lvbigpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCgocmVzKSA9PiBuZXcgQnBtUHJvZHVjdFZlcnNpb25Nb2RlbChyZXMpKSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhyb3dFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==