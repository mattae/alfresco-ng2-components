/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { LogService } from './log.service';
import { AlfrescoApiService } from './alfresco-api.service';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./log.service";
var DownloadZipService = /** @class */ (function () {
    function DownloadZipService(apiService, logService) {
        this.apiService = apiService;
        this.logService = logService;
    }
    /**
     * Creates a new download.
     * @param payload Object containing the node IDs of the items to add to the ZIP file
     * @returns Status object for the download
     */
    /**
     * Creates a new download.
     * @param {?} payload Object containing the node IDs of the items to add to the ZIP file
     * @return {?} Status object for the download
     */
    DownloadZipService.prototype.createDownload = /**
     * Creates a new download.
     * @param {?} payload Object containing the node IDs of the items to add to the ZIP file
     * @return {?} Status object for the download
     */
    function (payload) {
        var _this = this;
        return from(this.apiService.getInstance().core.downloadsApi.createDownload(payload)).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets a content URL for the given node.
     * @param nodeId Node to get URL for.
     * @param attachment Toggles whether to retrieve content as an attachment for download
     * @returns URL string
     */
    /**
     * Gets a content URL for the given node.
     * @param {?} nodeId Node to get URL for.
     * @param {?=} attachment Toggles whether to retrieve content as an attachment for download
     * @return {?} URL string
     */
    DownloadZipService.prototype.getContentUrl = /**
     * Gets a content URL for the given node.
     * @param {?} nodeId Node to get URL for.
     * @param {?=} attachment Toggles whether to retrieve content as an attachment for download
     * @return {?} URL string
     */
    function (nodeId, attachment) {
        return this.apiService.getInstance().content.getContentUrl(nodeId, attachment);
    };
    /**
     * Gets a Node via its node ID.
     * @param nodeId ID of the target node
     * @returns Details of the node
     */
    /**
     * Gets a Node via its node ID.
     * @param {?} nodeId ID of the target node
     * @return {?} Details of the node
     */
    DownloadZipService.prototype.getNode = /**
     * Gets a Node via its node ID.
     * @param {?} nodeId ID of the target node
     * @return {?} Details of the node
     */
    function (nodeId) {
        return from(this.apiService.getInstance().core.nodesApi.getNode(nodeId));
    };
    /**
     * Gets status information for a download node.
     * @param downloadId ID of the download node
     * @returns Status object for the download
     */
    /**
     * Gets status information for a download node.
     * @param {?} downloadId ID of the download node
     * @return {?} Status object for the download
     */
    DownloadZipService.prototype.getDownload = /**
     * Gets status information for a download node.
     * @param {?} downloadId ID of the download node
     * @return {?} Status object for the download
     */
    function (downloadId) {
        return from(this.apiService.getInstance().core.downloadsApi.getDownload(downloadId));
    };
    /**
     * Cancels a download.
     * @param downloadId ID of the target download node
     */
    /**
     * Cancels a download.
     * @param {?} downloadId ID of the target download node
     * @return {?}
     */
    DownloadZipService.prototype.cancelDownload = /**
     * Cancels a download.
     * @param {?} downloadId ID of the target download node
     * @return {?}
     */
    function (downloadId) {
        this.apiService.getInstance().core.downloadsApi.cancelDownload(downloadId);
    };
    /**
     * @private
     * @param {?} error
     * @return {?}
     */
    DownloadZipService.prototype.handleError = /**
     * @private
     * @param {?} error
     * @return {?}
     */
    function (error) {
        this.logService.error(error);
        return throwError(error || 'Server error');
    };
    DownloadZipService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    DownloadZipService.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: LogService }
    ]; };
    /** @nocollapse */ DownloadZipService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function DownloadZipService_Factory() { return new DownloadZipService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: DownloadZipService, providedIn: "root" });
    return DownloadZipService;
}());
export { DownloadZipService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    DownloadZipService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    DownloadZipService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG93bmxvYWQtemlwLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9kb3dubG9hZC16aXAuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBYyxJQUFJLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3BELE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDNUQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBRTVDO0lBS0ksNEJBQW9CLFVBQThCLEVBQzlCLFVBQXNCO1FBRHRCLGVBQVUsR0FBVixVQUFVLENBQW9CO1FBQzlCLGVBQVUsR0FBVixVQUFVLENBQVk7SUFDMUMsQ0FBQztJQUVEOzs7O09BSUc7Ozs7OztJQUNILDJDQUFjOzs7OztJQUFkLFVBQWUsT0FBMkI7UUFBMUMsaUJBSUM7UUFIRyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUNyRixVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDTixDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCwwQ0FBYTs7Ozs7O0lBQWIsVUFBYyxNQUFjLEVBQUUsVUFBb0I7UUFDOUMsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLFVBQVUsQ0FBQyxDQUFDO0lBQ25GLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCxvQ0FBTzs7Ozs7SUFBUCxVQUFRLE1BQWM7UUFDbEIsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDO0lBQzdFLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCx3Q0FBVzs7Ozs7SUFBWCxVQUFZLFVBQWtCO1FBQzFCLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQztJQUN6RixDQUFDO0lBRUQ7OztPQUdHOzs7Ozs7SUFDSCwyQ0FBYzs7Ozs7SUFBZCxVQUFlLFVBQWtCO1FBQzdCLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDL0UsQ0FBQzs7Ozs7O0lBRU8sd0NBQVc7Ozs7O0lBQW5CLFVBQW9CLEtBQVU7UUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsT0FBTyxVQUFVLENBQUMsS0FBSyxJQUFJLGNBQWMsQ0FBQyxDQUFDO0lBQy9DLENBQUM7O2dCQTNESixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7O2dCQUxRLGtCQUFrQjtnQkFEbEIsVUFBVTs7OzZCQXBCbkI7Q0FvRkMsQUE1REQsSUE0REM7U0F6RFksa0JBQWtCOzs7Ozs7SUFFZix3Q0FBc0M7Ozs7O0lBQ3RDLHdDQUE4QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBOb2RlRW50cnksIERvd25sb2FkRW50cnksIERvd25sb2FkQm9keUNyZWF0ZSB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZyb20sIHRocm93RXJyb3IgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4vbG9nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgY2F0Y2hFcnJvciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRG93bmxvYWRaaXBTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgbG9nU2VydmljZTogTG9nU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBhIG5ldyBkb3dubG9hZC5cclxuICAgICAqIEBwYXJhbSBwYXlsb2FkIE9iamVjdCBjb250YWluaW5nIHRoZSBub2RlIElEcyBvZiB0aGUgaXRlbXMgdG8gYWRkIHRvIHRoZSBaSVAgZmlsZVxyXG4gICAgICogQHJldHVybnMgU3RhdHVzIG9iamVjdCBmb3IgdGhlIGRvd25sb2FkXHJcbiAgICAgKi9cclxuICAgIGNyZWF0ZURvd25sb2FkKHBheWxvYWQ6IERvd25sb2FkQm9keUNyZWF0ZSk6IE9ic2VydmFibGU8RG93bmxvYWRFbnRyeT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmNvcmUuZG93bmxvYWRzQXBpLmNyZWF0ZURvd25sb2FkKHBheWxvYWQpKS5waXBlKFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhIGNvbnRlbnQgVVJMIGZvciB0aGUgZ2l2ZW4gbm9kZS5cclxuICAgICAqIEBwYXJhbSBub2RlSWQgTm9kZSB0byBnZXQgVVJMIGZvci5cclxuICAgICAqIEBwYXJhbSBhdHRhY2htZW50IFRvZ2dsZXMgd2hldGhlciB0byByZXRyaWV2ZSBjb250ZW50IGFzIGFuIGF0dGFjaG1lbnQgZm9yIGRvd25sb2FkXHJcbiAgICAgKiBAcmV0dXJucyBVUkwgc3RyaW5nXHJcbiAgICAgKi9cclxuICAgIGdldENvbnRlbnRVcmwobm9kZUlkOiBzdHJpbmcsIGF0dGFjaG1lbnQ/OiBib29sZWFuKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuY29udGVudC5nZXRDb250ZW50VXJsKG5vZGVJZCwgYXR0YWNobWVudCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgTm9kZSB2aWEgaXRzIG5vZGUgSUQuXHJcbiAgICAgKiBAcGFyYW0gbm9kZUlkIElEIG9mIHRoZSB0YXJnZXQgbm9kZVxyXG4gICAgICogQHJldHVybnMgRGV0YWlscyBvZiB0aGUgbm9kZVxyXG4gICAgICovXHJcbiAgICBnZXROb2RlKG5vZGVJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxOb2RlRW50cnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5jb3JlLm5vZGVzQXBpLmdldE5vZGUobm9kZUlkKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHN0YXR1cyBpbmZvcm1hdGlvbiBmb3IgYSBkb3dubG9hZCBub2RlLlxyXG4gICAgICogQHBhcmFtIGRvd25sb2FkSWQgSUQgb2YgdGhlIGRvd25sb2FkIG5vZGVcclxuICAgICAqIEByZXR1cm5zIFN0YXR1cyBvYmplY3QgZm9yIHRoZSBkb3dubG9hZFxyXG4gICAgICovXHJcbiAgICBnZXREb3dubG9hZChkb3dubG9hZElkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPERvd25sb2FkRW50cnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5jb3JlLmRvd25sb2Fkc0FwaS5nZXREb3dubG9hZChkb3dubG9hZElkKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDYW5jZWxzIGEgZG93bmxvYWQuXHJcbiAgICAgKiBAcGFyYW0gZG93bmxvYWRJZCBJRCBvZiB0aGUgdGFyZ2V0IGRvd25sb2FkIG5vZGVcclxuICAgICAqL1xyXG4gICAgY2FuY2VsRG93bmxvYWQoZG93bmxvYWRJZDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuY29yZS5kb3dubG9hZHNBcGkuY2FuY2VsRG93bmxvYWQoZG93bmxvYWRJZCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBoYW5kbGVFcnJvcihlcnJvcjogYW55KSB7XHJcbiAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKGVycm9yKTtcclxuICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihlcnJvciB8fCAnU2VydmVyIGVycm9yJyk7XHJcbiAgICB9XHJcbn1cclxuIl19