/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var DownloadService = /** @class */ (function () {
    function DownloadService() {
        this.saveData = ((/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var a = document.createElement('a');
            document.body.appendChild(a);
            a.style.display = 'none';
            return (/**
             * @param {?} fileData
             * @param {?} format
             * @param {?} fileName
             * @return {?}
             */
            function (fileData, format, fileName) {
                /** @type {?} */
                var blob = null;
                if (format === 'blob' || format === 'data') {
                    blob = new Blob([fileData], { type: 'octet/stream' });
                }
                if (format === 'object' || format === 'json') {
                    /** @type {?} */
                    var json = JSON.stringify(fileData);
                    blob = new Blob([json], { type: 'octet/stream' });
                }
                if (blob) {
                    if (typeof window.navigator !== 'undefined' &&
                        window.navigator.msSaveOrOpenBlob) {
                        navigator.msSaveOrOpenBlob(blob, fileName);
                    }
                    else {
                        /** @type {?} */
                        var url = window.URL.createObjectURL(blob);
                        a.href = url;
                        a.download = fileName;
                        a.click();
                        window.URL.revokeObjectURL(url);
                    }
                }
            });
        }))();
    }
    /**
     * Invokes content download for a Blob with a file name.
     * @param blob Content to download.
     * @param fileName Name of the resulting file.
     */
    /**
     * Invokes content download for a Blob with a file name.
     * @param {?} blob Content to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    DownloadService.prototype.downloadBlob = /**
     * Invokes content download for a Blob with a file name.
     * @param {?} blob Content to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    function (blob, fileName) {
        this.saveData(blob, 'blob', fileName);
    };
    /**
     * Invokes content download for a data array with a file name.
     * @param data Data to download.
     * @param fileName Name of the resulting file.
     */
    /**
     * Invokes content download for a data array with a file name.
     * @param {?} data Data to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    DownloadService.prototype.downloadData = /**
     * Invokes content download for a data array with a file name.
     * @param {?} data Data to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    function (data, fileName) {
        this.saveData(data, 'data', fileName);
    };
    /**
     * Invokes content download for a JSON object with a file name.
     * @param json JSON object to download.
     * @param fileName Name of the resulting file.
     */
    /**
     * Invokes content download for a JSON object with a file name.
     * @param {?} json JSON object to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    DownloadService.prototype.downloadJSON = /**
     * Invokes content download for a JSON object with a file name.
     * @param {?} json JSON object to download.
     * @param {?} fileName Name of the resulting file.
     * @return {?}
     */
    function (json, fileName) {
        this.saveData(json, 'json', fileName);
    };
    DownloadService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    DownloadService.ctorParameters = function () { return []; };
    /** @nocollapse */ DownloadService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function DownloadService_Factory() { return new DownloadService(); }, token: DownloadService, providedIn: "root" });
    return DownloadService;
}());
export { DownloadService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    DownloadService.prototype.saveData;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZG93bmxvYWQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL2Rvd25sb2FkLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFFM0M7SUFNSTtRQUNJLElBQUksQ0FBQyxRQUFRLEdBQUc7OztRQUFDOztnQkFDUCxDQUFDLEdBQUcsUUFBUSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUM7WUFDckMsUUFBUSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDN0IsQ0FBQyxDQUFDLEtBQUssQ0FBQyxPQUFPLEdBQUcsTUFBTSxDQUFDO1lBRXpCOzs7Ozs7WUFBTyxVQUFTLFFBQVEsRUFBRSxNQUFNLEVBQUUsUUFBUTs7b0JBQ2xDLElBQUksR0FBRyxJQUFJO2dCQUVmLElBQUksTUFBTSxLQUFLLE1BQU0sSUFBSSxNQUFNLEtBQUssTUFBTSxFQUFFO29CQUN4QyxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxFQUFFLElBQUksRUFBRSxjQUFjLEVBQUUsQ0FBQyxDQUFDO2lCQUN6RDtnQkFFRCxJQUFJLE1BQU0sS0FBSyxRQUFRLElBQUksTUFBTSxLQUFLLE1BQU0sRUFBRTs7d0JBQ3BDLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFFBQVEsQ0FBQztvQkFDckMsSUFBSSxHQUFHLElBQUksSUFBSSxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsRUFBRSxJQUFJLEVBQUUsY0FBYyxFQUFFLENBQUMsQ0FBQztpQkFDckQ7Z0JBRUQsSUFBSSxJQUFJLEVBQUU7b0JBQ04sSUFDSSxPQUFPLE1BQU0sQ0FBQyxTQUFTLEtBQUssV0FBVzt3QkFDdkMsTUFBTSxDQUFDLFNBQVMsQ0FBQyxnQkFBZ0IsRUFDbkM7d0JBQ0UsU0FBUyxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxRQUFRLENBQUMsQ0FBQztxQkFDOUM7eUJBQU07OzRCQUNHLEdBQUcsR0FBRyxNQUFNLENBQUMsR0FBRyxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUM7d0JBQzVDLENBQUMsQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDO3dCQUNiLENBQUMsQ0FBQyxRQUFRLEdBQUcsUUFBUSxDQUFDO3dCQUN0QixDQUFDLENBQUMsS0FBSyxFQUFFLENBQUM7d0JBRVYsTUFBTSxDQUFDLEdBQUcsQ0FBQyxlQUFlLENBQUMsR0FBRyxDQUFDLENBQUM7cUJBQ25DO2lCQUNKO1lBQ0wsQ0FBQyxFQUFDO1FBQ04sQ0FBQyxFQUFDLEVBQUUsQ0FBQztJQUNULENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsc0NBQVk7Ozs7OztJQUFaLFVBQWEsSUFBVSxFQUFFLFFBQWdCO1FBQ3JDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQztJQUMxQyxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7OztJQUNILHNDQUFZOzs7Ozs7SUFBWixVQUFhLElBQVMsRUFBRSxRQUFnQjtRQUNwQyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksRUFBRSxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVEOzs7O09BSUc7Ozs7Ozs7SUFDSCxzQ0FBWTs7Ozs7O0lBQVosVUFBYSxJQUFTLEVBQUUsUUFBZ0I7UUFDcEMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDO0lBQzFDLENBQUM7O2dCQXBFSixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7OzswQkFyQkQ7Q0F3RkMsQUFyRUQsSUFxRUM7U0FsRVksZUFBZTs7Ozs7O0lBQ3hCLG1DQUEyQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIERvd25sb2FkU2VydmljZSB7XHJcbiAgICBwcml2YXRlIHNhdmVEYXRhOiBGdW5jdGlvbjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcigpIHtcclxuICAgICAgICB0aGlzLnNhdmVEYXRhID0gKGZ1bmN0aW9uKCkge1xyXG4gICAgICAgICAgICBjb25zdCBhID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnYScpO1xyXG4gICAgICAgICAgICBkb2N1bWVudC5ib2R5LmFwcGVuZENoaWxkKGEpO1xyXG4gICAgICAgICAgICBhLnN0eWxlLmRpc3BsYXkgPSAnbm9uZSc7XHJcblxyXG4gICAgICAgICAgICByZXR1cm4gZnVuY3Rpb24oZmlsZURhdGEsIGZvcm1hdCwgZmlsZU5hbWUpIHtcclxuICAgICAgICAgICAgICAgIGxldCBibG9iID0gbnVsbDtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoZm9ybWF0ID09PSAnYmxvYicgfHwgZm9ybWF0ID09PSAnZGF0YScpIHtcclxuICAgICAgICAgICAgICAgICAgICBibG9iID0gbmV3IEJsb2IoW2ZpbGVEYXRhXSwgeyB0eXBlOiAnb2N0ZXQvc3RyZWFtJyB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoZm9ybWF0ID09PSAnb2JqZWN0JyB8fCBmb3JtYXQgPT09ICdqc29uJykge1xyXG4gICAgICAgICAgICAgICAgICAgIGNvbnN0IGpzb24gPSBKU09OLnN0cmluZ2lmeShmaWxlRGF0YSk7XHJcbiAgICAgICAgICAgICAgICAgICAgYmxvYiA9IG5ldyBCbG9iKFtqc29uXSwgeyB0eXBlOiAnb2N0ZXQvc3RyZWFtJyB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoYmxvYikge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZW9mIHdpbmRvdy5uYXZpZ2F0b3IgIT09ICd1bmRlZmluZWQnICYmXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHdpbmRvdy5uYXZpZ2F0b3IubXNTYXZlT3JPcGVuQmxvYlxyXG4gICAgICAgICAgICAgICAgICAgICkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBuYXZpZ2F0b3IubXNTYXZlT3JPcGVuQmxvYihibG9iLCBmaWxlTmFtZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3QgdXJsID0gd2luZG93LlVSTC5jcmVhdGVPYmplY3RVUkwoYmxvYik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGEuaHJlZiA9IHVybDtcclxuICAgICAgICAgICAgICAgICAgICAgICAgYS5kb3dubG9hZCA9IGZpbGVOYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBhLmNsaWNrKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cuVVJMLnJldm9rZU9iamVjdFVSTCh1cmwpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9KSgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogSW52b2tlcyBjb250ZW50IGRvd25sb2FkIGZvciBhIEJsb2Igd2l0aCBhIGZpbGUgbmFtZS5cclxuICAgICAqIEBwYXJhbSBibG9iIENvbnRlbnQgdG8gZG93bmxvYWQuXHJcbiAgICAgKiBAcGFyYW0gZmlsZU5hbWUgTmFtZSBvZiB0aGUgcmVzdWx0aW5nIGZpbGUuXHJcbiAgICAgKi9cclxuICAgIGRvd25sb2FkQmxvYihibG9iOiBCbG9iLCBmaWxlTmFtZTogc3RyaW5nKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5zYXZlRGF0YShibG9iLCAnYmxvYicsIGZpbGVOYW1lKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEludm9rZXMgY29udGVudCBkb3dubG9hZCBmb3IgYSBkYXRhIGFycmF5IHdpdGggYSBmaWxlIG5hbWUuXHJcbiAgICAgKiBAcGFyYW0gZGF0YSBEYXRhIHRvIGRvd25sb2FkLlxyXG4gICAgICogQHBhcmFtIGZpbGVOYW1lIE5hbWUgb2YgdGhlIHJlc3VsdGluZyBmaWxlLlxyXG4gICAgICovXHJcbiAgICBkb3dubG9hZERhdGEoZGF0YTogYW55LCBmaWxlTmFtZTogc3RyaW5nKTogdm9pZCB7XHJcbiAgICAgICAgdGhpcy5zYXZlRGF0YShkYXRhLCAnZGF0YScsIGZpbGVOYW1lKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEludm9rZXMgY29udGVudCBkb3dubG9hZCBmb3IgYSBKU09OIG9iamVjdCB3aXRoIGEgZmlsZSBuYW1lLlxyXG4gICAgICogQHBhcmFtIGpzb24gSlNPTiBvYmplY3QgdG8gZG93bmxvYWQuXHJcbiAgICAgKiBAcGFyYW0gZmlsZU5hbWUgTmFtZSBvZiB0aGUgcmVzdWx0aW5nIGZpbGUuXHJcbiAgICAgKi9cclxuICAgIGRvd25sb2FkSlNPTihqc29uOiBhbnksIGZpbGVOYW1lOiBzdHJpbmcpOiB2b2lkIHtcclxuICAgICAgICB0aGlzLnNhdmVEYXRhKGpzb24sICdqc29uJywgZmlsZU5hbWUpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==