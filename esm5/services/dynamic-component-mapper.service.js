/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { getType } from './get-type';
/**
 * @record
 */
export function DynamicComponentModel() { }
if (false) {
    /** @type {?} */
    DynamicComponentModel.prototype.type;
}
var DynamicComponentResolver = /** @class */ (function () {
    function DynamicComponentResolver() {
    }
    /**
     * @param {?} type
     * @return {?}
     */
    DynamicComponentResolver.fromType = /**
     * @param {?} type
     * @return {?}
     */
    function (type) {
        return getType(type);
    };
    return DynamicComponentResolver;
}());
export { DynamicComponentResolver };
/**
 * @abstract
 */
var /**
 * @abstract
 */
DynamicComponentMapper = /** @class */ (function () {
    function DynamicComponentMapper() {
        this.defaultValue = undefined;
        this.types = {};
    }
    /**
     * Gets the currently active DynamicComponentResolveFunction for a field type.
     * @param type The type whose resolver you want
     * @param defaultValue Default type returned for types that are not yet mapped
     * @returns Resolver function
     */
    /**
     * Gets the currently active DynamicComponentResolveFunction for a field type.
     * @param {?} type The type whose resolver you want
     * @param {?=} defaultValue Default type returned for types that are not yet mapped
     * @return {?} Resolver function
     */
    DynamicComponentMapper.prototype.getComponentTypeResolver = /**
     * Gets the currently active DynamicComponentResolveFunction for a field type.
     * @param {?} type The type whose resolver you want
     * @param {?=} defaultValue Default type returned for types that are not yet mapped
     * @return {?} Resolver function
     */
    function (type, defaultValue) {
        if (defaultValue === void 0) { defaultValue = this.defaultValue; }
        if (type) {
            return this.types[type] || DynamicComponentResolver.fromType(defaultValue);
        }
        return DynamicComponentResolver.fromType(defaultValue);
    };
    /**
     * Sets or optionally replaces a DynamicComponentResolveFunction for a field type.
     * @param type The type whose resolver you want to set
     * @param resolver The new resolver function
     * @param override The new resolver will only replace an existing one if this parameter is true
     */
    /**
     * Sets or optionally replaces a DynamicComponentResolveFunction for a field type.
     * @param {?} type The type whose resolver you want to set
     * @param {?} resolver The new resolver function
     * @param {?=} override The new resolver will only replace an existing one if this parameter is true
     * @return {?}
     */
    DynamicComponentMapper.prototype.setComponentTypeResolver = /**
     * Sets or optionally replaces a DynamicComponentResolveFunction for a field type.
     * @param {?} type The type whose resolver you want to set
     * @param {?} resolver The new resolver function
     * @param {?=} override The new resolver will only replace an existing one if this parameter is true
     * @return {?}
     */
    function (type, resolver, override) {
        if (override === void 0) { override = true; }
        if (!type) {
            throw new Error("type is null or not defined");
        }
        if (!resolver) {
            throw new Error("resolver is null or not defined");
        }
        /** @type {?} */
        var existing = this.types[type];
        if (existing && !override) {
            throw new Error("already mapped, use override option if you intend replacing existing mapping.");
        }
        this.types[type] = resolver;
    };
    /**
     * Finds the component type that is needed to render a form field.
     * @param model Form field model for the field to render
     * @param defaultValue Default type returned for field types that are not yet mapped.
     * @returns Component type
     */
    /**
     * Finds the component type that is needed to render a form field.
     * @param {?} model Form field model for the field to render
     * @param {?=} defaultValue Default type returned for field types that are not yet mapped.
     * @return {?} Component type
     */
    DynamicComponentMapper.prototype.resolveComponentType = /**
     * Finds the component type that is needed to render a form field.
     * @param {?} model Form field model for the field to render
     * @param {?=} defaultValue Default type returned for field types that are not yet mapped.
     * @return {?} Component type
     */
    function (model, defaultValue) {
        if (defaultValue === void 0) { defaultValue = this.defaultValue; }
        if (model) {
            /** @type {?} */
            var resolver = this.getComponentTypeResolver(model.type, defaultValue);
            return resolver(model);
        }
        return defaultValue;
    };
    return DynamicComponentMapper;
}());
/**
 * @abstract
 */
export { DynamicComponentMapper };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    DynamicComponentMapper.prototype.defaultValue;
    /**
     * @type {?}
     * @protected
     */
    DynamicComponentMapper.prototype.types;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZHluYW1pYy1jb21wb25lbnQtbWFwcGVyLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9keW5hbWljLWNvbXBvbmVudC1tYXBwZXIuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWtCQSxPQUFPLEVBQUUsT0FBTyxFQUFFLE1BQU0sWUFBWSxDQUFDOzs7O0FBRXJDLDJDQUF3RDs7O0lBQWYscUNBQWE7O0FBRXREO0lBQUE7SUFJQSxDQUFDOzs7OztJQUhVLGlDQUFROzs7O0lBQWYsVUFBZ0IsSUFBYztRQUMxQixPQUFPLE9BQU8sQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBQ0wsK0JBQUM7QUFBRCxDQUFDLEFBSkQsSUFJQzs7Ozs7QUFFRDs7OztJQUFBO1FBRWMsaUJBQVksR0FBYSxTQUFTLENBQUM7UUFDbkMsVUFBSyxHQUF1RCxFQUFFLENBQUM7SUFtRDdFLENBQUM7SUFqREc7Ozs7O09BS0c7Ozs7Ozs7SUFDSCx5REFBd0I7Ozs7OztJQUF4QixVQUF5QixJQUFZLEVBQUUsWUFBMEM7UUFBMUMsNkJBQUEsRUFBQSxlQUF5QixJQUFJLENBQUMsWUFBWTtRQUM3RSxJQUFJLElBQUksRUFBRTtZQUNOLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSx3QkFBd0IsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7U0FDOUU7UUFDRCxPQUFPLHdCQUF3QixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUMzRCxDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7O0lBQ0gseURBQXdCOzs7Ozs7O0lBQXhCLFVBQXlCLElBQVksRUFBRSxRQUF5QyxFQUFFLFFBQXdCO1FBQXhCLHlCQUFBLEVBQUEsZUFBd0I7UUFDdEcsSUFBSSxDQUFDLElBQUksRUFBRTtZQUNQLE1BQU0sSUFBSSxLQUFLLENBQUMsNkJBQTZCLENBQUMsQ0FBQztTQUNsRDtRQUVELElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDWCxNQUFNLElBQUksS0FBSyxDQUFDLGlDQUFpQyxDQUFDLENBQUM7U0FDdEQ7O1lBRUssUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO1FBQ2pDLElBQUksUUFBUSxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ3ZCLE1BQU0sSUFBSSxLQUFLLENBQUMsK0VBQStFLENBQUMsQ0FBQztTQUNwRztRQUVELElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEdBQUcsUUFBUSxDQUFDO0lBQ2hDLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILHFEQUFvQjs7Ozs7O0lBQXBCLFVBQXFCLEtBQTRCLEVBQUUsWUFBMEM7UUFBMUMsNkJBQUEsRUFBQSxlQUF5QixJQUFJLENBQUMsWUFBWTtRQUN6RixJQUFJLEtBQUssRUFBRTs7Z0JBQ0QsUUFBUSxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLFlBQVksQ0FBQztZQUN4RSxPQUFPLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUMxQjtRQUNELE9BQU8sWUFBWSxDQUFDO0lBQ3hCLENBQUM7SUFDTCw2QkFBQztBQUFELENBQUMsQUF0REQsSUFzREM7Ozs7Ozs7Ozs7SUFwREcsOENBQTZDOzs7OztJQUM3Qyx1Q0FBeUUiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgVHlwZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBnZXRUeXBlIH0gZnJvbSAnLi9nZXQtdHlwZSc7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIER5bmFtaWNDb21wb25lbnRNb2RlbCB7IHR5cGU6IHN0cmluZzsgfVxyXG5leHBvcnQgdHlwZSBEeW5hbWljQ29tcG9uZW50UmVzb2x2ZUZ1bmN0aW9uID0gKG1vZGVsOiBEeW5hbWljQ29tcG9uZW50TW9kZWwpID0+IFR5cGU8e30+O1xyXG5leHBvcnQgY2xhc3MgRHluYW1pY0NvbXBvbmVudFJlc29sdmVyIHtcclxuICAgIHN0YXRpYyBmcm9tVHlwZSh0eXBlOiBUeXBlPHt9Pik6IER5bmFtaWNDb21wb25lbnRSZXNvbHZlRnVuY3Rpb24ge1xyXG4gICAgICAgIHJldHVybiBnZXRUeXBlKHR5cGUpO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgYWJzdHJhY3QgY2xhc3MgRHluYW1pY0NvbXBvbmVudE1hcHBlciB7XHJcblxyXG4gICAgcHJvdGVjdGVkIGRlZmF1bHRWYWx1ZTogVHlwZTx7fT4gPSB1bmRlZmluZWQ7XHJcbiAgICBwcm90ZWN0ZWQgdHlwZXM6IHsgW2tleTogc3RyaW5nXTogRHluYW1pY0NvbXBvbmVudFJlc29sdmVGdW5jdGlvbiB9ID0ge307XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBjdXJyZW50bHkgYWN0aXZlIER5bmFtaWNDb21wb25lbnRSZXNvbHZlRnVuY3Rpb24gZm9yIGEgZmllbGQgdHlwZS5cclxuICAgICAqIEBwYXJhbSB0eXBlIFRoZSB0eXBlIHdob3NlIHJlc29sdmVyIHlvdSB3YW50XHJcbiAgICAgKiBAcGFyYW0gZGVmYXVsdFZhbHVlIERlZmF1bHQgdHlwZSByZXR1cm5lZCBmb3IgdHlwZXMgdGhhdCBhcmUgbm90IHlldCBtYXBwZWRcclxuICAgICAqIEByZXR1cm5zIFJlc29sdmVyIGZ1bmN0aW9uXHJcbiAgICAgKi9cclxuICAgIGdldENvbXBvbmVudFR5cGVSZXNvbHZlcih0eXBlOiBzdHJpbmcsIGRlZmF1bHRWYWx1ZTogVHlwZTx7fT4gPSB0aGlzLmRlZmF1bHRWYWx1ZSk6IER5bmFtaWNDb21wb25lbnRSZXNvbHZlRnVuY3Rpb24ge1xyXG4gICAgICAgIGlmICh0eXBlKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLnR5cGVzW3R5cGVdIHx8IER5bmFtaWNDb21wb25lbnRSZXNvbHZlci5mcm9tVHlwZShkZWZhdWx0VmFsdWUpO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gRHluYW1pY0NvbXBvbmVudFJlc29sdmVyLmZyb21UeXBlKGRlZmF1bHRWYWx1ZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXRzIG9yIG9wdGlvbmFsbHkgcmVwbGFjZXMgYSBEeW5hbWljQ29tcG9uZW50UmVzb2x2ZUZ1bmN0aW9uIGZvciBhIGZpZWxkIHR5cGUuXHJcbiAgICAgKiBAcGFyYW0gdHlwZSBUaGUgdHlwZSB3aG9zZSByZXNvbHZlciB5b3Ugd2FudCB0byBzZXRcclxuICAgICAqIEBwYXJhbSByZXNvbHZlciBUaGUgbmV3IHJlc29sdmVyIGZ1bmN0aW9uXHJcbiAgICAgKiBAcGFyYW0gb3ZlcnJpZGUgVGhlIG5ldyByZXNvbHZlciB3aWxsIG9ubHkgcmVwbGFjZSBhbiBleGlzdGluZyBvbmUgaWYgdGhpcyBwYXJhbWV0ZXIgaXMgdHJ1ZVxyXG4gICAgICovXHJcbiAgICBzZXRDb21wb25lbnRUeXBlUmVzb2x2ZXIodHlwZTogc3RyaW5nLCByZXNvbHZlcjogRHluYW1pY0NvbXBvbmVudFJlc29sdmVGdW5jdGlvbiwgb3ZlcnJpZGU6IGJvb2xlYW4gPSB0cnVlKSB7XHJcbiAgICAgICAgaWYgKCF0eXBlKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgdHlwZSBpcyBudWxsIG9yIG5vdCBkZWZpbmVkYCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXJlc29sdmVyKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcihgcmVzb2x2ZXIgaXMgbnVsbCBvciBub3QgZGVmaW5lZGApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgY29uc3QgZXhpc3RpbmcgPSB0aGlzLnR5cGVzW3R5cGVdO1xyXG4gICAgICAgIGlmIChleGlzdGluZyAmJiAhb3ZlcnJpZGUpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKGBhbHJlYWR5IG1hcHBlZCwgdXNlIG92ZXJyaWRlIG9wdGlvbiBpZiB5b3UgaW50ZW5kIHJlcGxhY2luZyBleGlzdGluZyBtYXBwaW5nLmApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdGhpcy50eXBlc1t0eXBlXSA9IHJlc29sdmVyO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRmluZHMgdGhlIGNvbXBvbmVudCB0eXBlIHRoYXQgaXMgbmVlZGVkIHRvIHJlbmRlciBhIGZvcm0gZmllbGQuXHJcbiAgICAgKiBAcGFyYW0gbW9kZWwgRm9ybSBmaWVsZCBtb2RlbCBmb3IgdGhlIGZpZWxkIHRvIHJlbmRlclxyXG4gICAgICogQHBhcmFtIGRlZmF1bHRWYWx1ZSBEZWZhdWx0IHR5cGUgcmV0dXJuZWQgZm9yIGZpZWxkIHR5cGVzIHRoYXQgYXJlIG5vdCB5ZXQgbWFwcGVkLlxyXG4gICAgICogQHJldHVybnMgQ29tcG9uZW50IHR5cGVcclxuICAgICAqL1xyXG4gICAgcmVzb2x2ZUNvbXBvbmVudFR5cGUobW9kZWw6IER5bmFtaWNDb21wb25lbnRNb2RlbCwgZGVmYXVsdFZhbHVlOiBUeXBlPHt9PiA9IHRoaXMuZGVmYXVsdFZhbHVlKTogVHlwZTx7fT4ge1xyXG4gICAgICAgIGlmIChtb2RlbCkge1xyXG4gICAgICAgICAgICBjb25zdCByZXNvbHZlciA9IHRoaXMuZ2V0Q29tcG9uZW50VHlwZVJlc29sdmVyKG1vZGVsLnR5cGUsIGRlZmF1bHRWYWx1ZSk7XHJcbiAgICAgICAgICAgIHJldHVybiByZXNvbHZlcihtb2RlbCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBkZWZhdWx0VmFsdWU7XHJcbiAgICB9XHJcbn1cclxuIl19