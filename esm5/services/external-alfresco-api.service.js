/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { AlfrescoApiCompatibility } from '@alfresco/js-api';
import * as i0 from "@angular/core";
/* tslint:disable:adf-file-name */
var ExternalAlfrescoApiService = /** @class */ (function () {
    function ExternalAlfrescoApiService() {
    }
    /**
     * @return {?}
     */
    ExternalAlfrescoApiService.prototype.getInstance = /**
     * @return {?}
     */
    function () {
        return this.alfrescoApi;
    };
    Object.defineProperty(ExternalAlfrescoApiService.prototype, "contentApi", {
        get: /**
         * @return {?}
         */
        function () {
            return this.getInstance().content;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ExternalAlfrescoApiService.prototype, "nodesApi", {
        get: /**
         * @return {?}
         */
        function () {
            return this.getInstance().nodes;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @param {?} ecmHost
     * @param {?} contextRoot
     * @return {?}
     */
    ExternalAlfrescoApiService.prototype.init = /**
     * @param {?} ecmHost
     * @param {?} contextRoot
     * @return {?}
     */
    function (ecmHost, contextRoot) {
        /** @type {?} */
        var domainPrefix = this.createPrefixFromHost(ecmHost);
        /** @type {?} */
        var config = {
            provider: 'ECM',
            hostEcm: ecmHost,
            authType: 'BASIC',
            contextRoot: contextRoot,
            domainPrefix: domainPrefix
        };
        this.initAlfrescoApi(config);
    };
    /**
     * @protected
     * @param {?} config
     * @return {?}
     */
    ExternalAlfrescoApiService.prototype.initAlfrescoApi = /**
     * @protected
     * @param {?} config
     * @return {?}
     */
    function (config) {
        if (this.alfrescoApi) {
            this.alfrescoApi.configureJsApi(config);
        }
        else {
            this.alfrescoApi = new AlfrescoApiCompatibility(config);
        }
    };
    /**
     * @private
     * @param {?} url
     * @return {?}
     */
    ExternalAlfrescoApiService.prototype.createPrefixFromHost = /**
     * @private
     * @param {?} url
     * @return {?}
     */
    function (url) {
        /** @type {?} */
        var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
        /** @type {?} */
        var result = null;
        if (match != null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
            result = match[2];
        }
        return result;
    };
    ExternalAlfrescoApiService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */ ExternalAlfrescoApiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ExternalAlfrescoApiService_Factory() { return new ExternalAlfrescoApiService(); }, token: ExternalAlfrescoApiService, providedIn: "root" });
    return ExternalAlfrescoApiService;
}());
export { ExternalAlfrescoApiService };
if (false) {
    /**
     * @type {?}
     * @protected
     */
    ExternalAlfrescoApiService.prototype.alfrescoApi;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXh0ZXJuYWwtYWxmcmVzY28tYXBpLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9leHRlcm5hbC1hbGZyZXNjby1hcGkuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFDSCx3QkFBd0IsRUFHM0IsTUFBTSxrQkFBa0IsQ0FBQzs7O0FBRzFCO0lBQUE7S0FpREM7Ozs7SUExQ0csZ0RBQVc7OztJQUFYO1FBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7SUFFRCxzQkFBSSxrREFBVTs7OztRQUFkO1lBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUMsT0FBTyxDQUFDO1FBQ3RDLENBQUM7OztPQUFBO0lBRUQsc0JBQUksZ0RBQVE7Ozs7UUFBWjtZQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQztRQUNwQyxDQUFDOzs7T0FBQTs7Ozs7O0lBRUQseUNBQUk7Ozs7O0lBQUosVUFBSyxPQUFlLEVBQUUsV0FBbUI7O1lBRS9CLFlBQVksR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsT0FBTyxDQUFDOztZQUVqRCxNQUFNLEdBQUc7WUFDWCxRQUFRLEVBQUUsS0FBSztZQUNmLE9BQU8sRUFBRSxPQUFPO1lBQ2hCLFFBQVEsRUFBRSxPQUFPO1lBQ2pCLFdBQVcsRUFBRSxXQUFXO1lBQ3hCLFlBQVksY0FBQTtTQUNmO1FBQ0QsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNqQyxDQUFDOzs7Ozs7SUFFUyxvREFBZTs7Ozs7SUFBekIsVUFBMEIsTUFBTTtRQUM1QixJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDbEIsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDM0M7YUFBTTtZQUNILElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSx3QkFBd0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUMzRDtJQUNMLENBQUM7Ozs7OztJQUVPLHlEQUFvQjs7Ozs7SUFBNUIsVUFBNkIsR0FBVzs7WUFDOUIsS0FBSyxHQUFHLEdBQUcsQ0FBQyxLQUFLLENBQUMsK0JBQStCLENBQUM7O1lBQ3BELE1BQU0sR0FBRyxJQUFJO1FBQ2pCLElBQUksS0FBSyxJQUFJLElBQUksSUFBSSxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsSUFBSSxPQUFPLEtBQUssQ0FBQyxDQUFDLENBQUMsS0FBSyxRQUFRLElBQUksS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDMUYsTUFBTSxHQUFHLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQztTQUNyQjtRQUNELE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7O2dCQWhESixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7cUNBM0JEO0NBMEVDLEFBakRELElBaURDO1NBOUNZLDBCQUEwQjs7Ozs7O0lBRW5DLGlEQUFnRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7XHJcbiAgICBBbGZyZXNjb0FwaUNvbXBhdGliaWxpdHksXHJcbiAgICBDb250ZW50QXBpLFxyXG4gICAgQ29yZVxyXG59IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG4vKiB0c2xpbnQ6ZGlzYWJsZTphZGYtZmlsZS1uYW1lICovXHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEV4dGVybmFsQWxmcmVzY29BcGlTZXJ2aWNlIHtcclxuXHJcbiAgICBwcm90ZWN0ZWQgYWxmcmVzY29BcGk6IEFsZnJlc2NvQXBpQ29tcGF0aWJpbGl0eTtcclxuXHJcbiAgICBnZXRJbnN0YW5jZSgpOiBBbGZyZXNjb0FwaUNvbXBhdGliaWxpdHkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFsZnJlc2NvQXBpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBjb250ZW50QXBpKCk6IENvbnRlbnRBcGkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldEluc3RhbmNlKCkuY29udGVudDtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgbm9kZXNBcGkoKTogQ29yZS5Ob2Rlc0FwaSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0SW5zdGFuY2UoKS5ub2RlcztcclxuICAgIH1cclxuXHJcbiAgICBpbml0KGVjbUhvc3Q6IHN0cmluZywgY29udGV4dFJvb3Q6IHN0cmluZykge1xyXG5cclxuICAgICAgICBjb25zdCBkb21haW5QcmVmaXggPSB0aGlzLmNyZWF0ZVByZWZpeEZyb21Ib3N0KGVjbUhvc3QpO1xyXG5cclxuICAgICAgICBjb25zdCBjb25maWcgPSB7XHJcbiAgICAgICAgICAgIHByb3ZpZGVyOiAnRUNNJyxcclxuICAgICAgICAgICAgaG9zdEVjbTogZWNtSG9zdCxcclxuICAgICAgICAgICAgYXV0aFR5cGU6ICdCQVNJQycsXHJcbiAgICAgICAgICAgIGNvbnRleHRSb290OiBjb250ZXh0Um9vdCxcclxuICAgICAgICAgICAgZG9tYWluUHJlZml4XHJcbiAgICAgICAgfTtcclxuICAgICAgICB0aGlzLmluaXRBbGZyZXNjb0FwaShjb25maWcpO1xyXG4gICAgfVxyXG5cclxuICAgIHByb3RlY3RlZCBpbml0QWxmcmVzY29BcGkoY29uZmlnKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuYWxmcmVzY29BcGkpIHtcclxuICAgICAgICAgICAgdGhpcy5hbGZyZXNjb0FwaS5jb25maWd1cmVKc0FwaShjb25maWcpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMuYWxmcmVzY29BcGkgPSBuZXcgQWxmcmVzY29BcGlDb21wYXRpYmlsaXR5KGNvbmZpZyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgY3JlYXRlUHJlZml4RnJvbUhvc3QodXJsOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIGNvbnN0IG1hdGNoID0gdXJsLm1hdGNoKC86XFwvXFwvKHd3d1swLTldP1xcLik/KC5bXi86XSspL2kpO1xyXG4gICAgICAgIGxldCByZXN1bHQgPSBudWxsO1xyXG4gICAgICAgIGlmIChtYXRjaCAhPSBudWxsICYmIG1hdGNoLmxlbmd0aCA+IDIgJiYgdHlwZW9mIG1hdGNoWzJdID09PSAnc3RyaW5nJyAmJiBtYXRjaFsyXS5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIHJlc3VsdCA9IG1hdGNoWzJdO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcmVzdWx0O1xyXG4gICAgfVxyXG59XHJcbiJdfQ==