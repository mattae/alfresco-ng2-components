/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, of } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { UserPreferencesService } from './user-preferences.service';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./user-preferences.service";
var FavoritesApiService = /** @class */ (function () {
    function FavoritesApiService(apiService, preferences) {
        this.apiService = apiService;
        this.preferences = preferences;
    }
    /**
     * @param {?} __0
     * @return {?}
     */
    FavoritesApiService.remapEntry = /**
     * @param {?} __0
     * @return {?}
     */
    function (_a) {
        var entry = _a.entry;
        entry.properties = {
            'cm:title': entry.title,
            'cm:description': entry.description
        };
        return { entry: entry };
    };
    /**
     * @param {?=} data
     * @return {?}
     */
    FavoritesApiService.prototype.remapFavoritesData = /**
     * @param {?=} data
     * @return {?}
     */
    function (data) {
        if (data === void 0) { data = {}; }
        /** @type {?} */
        var list = (data.list || {});
        /** @type {?} */
        var pagination = (list.pagination || {});
        /** @type {?} */
        var entries = this
            .remapFavoriteEntries(list.entries || []);
        return (/** @type {?} */ ({
            list: { entries: entries, pagination: pagination }
        }));
    };
    /**
     * @param {?} entries
     * @return {?}
     */
    FavoritesApiService.prototype.remapFavoriteEntries = /**
     * @param {?} entries
     * @return {?}
     */
    function (entries) {
        return entries
            .map((/**
         * @param {?} __0
         * @return {?}
         */
        function (_a) {
            var target = _a.entry.target;
            return ({
                entry: target.file || target.folder
            });
        }))
            .filter((/**
         * @param {?} __0
         * @return {?}
         */
        function (_a) {
            var entry = _a.entry;
            return (!!entry);
        }))
            .map(FavoritesApiService.remapEntry);
    };
    Object.defineProperty(FavoritesApiService.prototype, "favoritesApi", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.apiService.getInstance().core.favoritesApi;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Gets the favorites for a user.
     * @param personId ID of the user
     * @param options Options supported by JS-API
     * @returns List of favorites
     */
    /**
     * Gets the favorites for a user.
     * @param {?} personId ID of the user
     * @param {?=} options Options supported by JS-API
     * @return {?} List of favorites
     */
    FavoritesApiService.prototype.getFavorites = /**
     * Gets the favorites for a user.
     * @param {?} personId ID of the user
     * @param {?=} options Options supported by JS-API
     * @return {?} List of favorites
     */
    function (personId, options) {
        /** @type {?} */
        var defaultOptions = {
            maxItems: this.preferences.paginationSize,
            skipCount: 0,
            where: '(EXISTS(target/file) OR EXISTS(target/folder))',
            include: ['properties', 'allowableOperations']
        };
        /** @type {?} */
        var queryOptions = Object.assign(defaultOptions, options);
        /** @type {?} */
        var promise = this.favoritesApi
            .getFavorites(personId, queryOptions)
            .then(this.remapFavoritesData);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return of(err); })));
    };
    FavoritesApiService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    FavoritesApiService.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: UserPreferencesService }
    ]; };
    /** @nocollapse */ FavoritesApiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function FavoritesApiService_Factory() { return new FavoritesApiService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.UserPreferencesService)); }, token: FavoritesApiService, providedIn: "root" });
    return FavoritesApiService;
}());
export { FavoritesApiService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    FavoritesApiService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    FavoritesApiService.prototype.preferences;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmF2b3JpdGVzLWFwaS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvZmF2b3JpdGVzLWFwaS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFjLElBQUksRUFBRSxFQUFFLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDNUMsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDNUQsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDcEUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDOzs7O0FBRTVDO0lBa0NJLDZCQUNZLFVBQThCLEVBQzlCLFdBQW1DO1FBRG5DLGVBQVUsR0FBVixVQUFVLENBQW9CO1FBQzlCLGdCQUFXLEdBQVgsV0FBVyxDQUF3QjtJQUM1QyxDQUFDOzs7OztJQWhDRyw4QkFBVTs7OztJQUFqQixVQUFrQixFQUFjO1lBQVosZ0JBQUs7UUFDckIsS0FBSyxDQUFDLFVBQVUsR0FBRztZQUNmLFVBQVUsRUFBRSxLQUFLLENBQUMsS0FBSztZQUN2QixnQkFBZ0IsRUFBRSxLQUFLLENBQUMsV0FBVztTQUN0QyxDQUFDO1FBRUYsT0FBTyxFQUFFLEtBQUssT0FBQSxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCxnREFBa0I7Ozs7SUFBbEIsVUFBbUIsSUFBYztRQUFkLHFCQUFBLEVBQUEsU0FBYzs7WUFDdkIsSUFBSSxHQUFHLENBQUMsSUFBSSxDQUFDLElBQUksSUFBSSxFQUFFLENBQUM7O1lBQ3hCLFVBQVUsR0FBRyxDQUFDLElBQUksQ0FBQyxVQUFVLElBQUksRUFBRSxDQUFDOztZQUNwQyxPQUFPLEdBQVUsSUFBSTthQUN0QixvQkFBb0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLEVBQUUsQ0FBQztRQUU3QyxPQUFPLG1CQUFhO1lBQ2hCLElBQUksRUFBRSxFQUFFLE9BQU8sU0FBQSxFQUFFLFVBQVUsWUFBQSxFQUFFO1NBQ2hDLEVBQUEsQ0FBQztJQUNOLENBQUM7Ozs7O0lBRUQsa0RBQW9COzs7O0lBQXBCLFVBQXFCLE9BQWM7UUFDL0IsT0FBTyxPQUFPO2FBQ1QsR0FBRzs7OztRQUFDLFVBQUMsRUFBeUI7Z0JBQWQsd0JBQU07WUFBYSxPQUFBLENBQUM7Z0JBQ2pDLEtBQUssRUFBRSxNQUFNLENBQUMsSUFBSSxJQUFJLE1BQU0sQ0FBQyxNQUFNO2FBQ3RDLENBQUM7UUFGa0MsQ0FFbEMsRUFBQzthQUNGLE1BQU07Ozs7UUFBQyxVQUFDLEVBQVM7Z0JBQVAsZ0JBQUs7WUFBTyxPQUFBLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztRQUFULENBQVMsRUFBQzthQUNoQyxHQUFHLENBQUMsbUJBQW1CLENBQUMsVUFBVSxDQUFDLENBQUM7SUFDN0MsQ0FBQztJQU9ELHNCQUFZLDZDQUFZOzs7OztRQUF4QjtZQUNHLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDO1FBQzFELENBQUM7OztPQUFBO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCwwQ0FBWTs7Ozs7O0lBQVosVUFBYSxRQUFnQixFQUFFLE9BQWE7O1lBQ2xDLGNBQWMsR0FBRztZQUNuQixRQUFRLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxjQUFjO1lBQ3pDLFNBQVMsRUFBRSxDQUFDO1lBQ1osS0FBSyxFQUFFLGdEQUFnRDtZQUN2RCxPQUFPLEVBQUUsQ0FBRSxZQUFZLEVBQUUscUJBQXFCLENBQUU7U0FDbkQ7O1lBQ0ssWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLE9BQU8sQ0FBQzs7WUFDckQsT0FBTyxHQUFHLElBQUksQ0FBQyxZQUFZO2FBQzVCLFlBQVksQ0FBQyxRQUFRLEVBQUUsWUFBWSxDQUFDO2FBQ3BDLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUM7UUFFbEMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUNyQixVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQVAsQ0FBTyxFQUFDLENBQy9CLENBQUM7SUFDTixDQUFDOztnQkFoRUosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFOUSxrQkFBa0I7Z0JBQ2xCLHNCQUFzQjs7OzhCQXJCL0I7Q0F5RkMsQUFqRUQsSUFpRUM7U0E5RFksbUJBQW1COzs7Ozs7SUFnQ3hCLHlDQUFzQzs7Ozs7SUFDdEMsMENBQTJDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTm9kZVBhZ2luZyB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmcm9tLCBvZiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgVXNlclByZWZlcmVuY2VzU2VydmljZSB9IGZyb20gJy4vdXNlci1wcmVmZXJlbmNlcy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgY2F0Y2hFcnJvciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgRmF2b3JpdGVzQXBpU2VydmljZSB7XHJcblxyXG4gICAgc3RhdGljIHJlbWFwRW50cnkoeyBlbnRyeSB9OiBhbnkpOiBhbnkge1xyXG4gICAgICAgIGVudHJ5LnByb3BlcnRpZXMgPSB7XHJcbiAgICAgICAgICAgICdjbTp0aXRsZSc6IGVudHJ5LnRpdGxlLFxyXG4gICAgICAgICAgICAnY206ZGVzY3JpcHRpb24nOiBlbnRyeS5kZXNjcmlwdGlvblxyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHJldHVybiB7IGVudHJ5IH07XHJcbiAgICB9XHJcblxyXG4gICAgcmVtYXBGYXZvcml0ZXNEYXRhKGRhdGE6IGFueSA9IHt9KTogTm9kZVBhZ2luZyB7XHJcbiAgICAgICAgY29uc3QgbGlzdCA9IChkYXRhLmxpc3QgfHwge30pO1xyXG4gICAgICAgIGNvbnN0IHBhZ2luYXRpb24gPSAobGlzdC5wYWdpbmF0aW9uIHx8IHt9KTtcclxuICAgICAgICBjb25zdCBlbnRyaWVzOiBhbnlbXSA9IHRoaXNcclxuICAgICAgICAgICAgLnJlbWFwRmF2b3JpdGVFbnRyaWVzKGxpc3QuZW50cmllcyB8fCBbXSk7XHJcblxyXG4gICAgICAgIHJldHVybiA8Tm9kZVBhZ2luZz4ge1xyXG4gICAgICAgICAgICBsaXN0OiB7IGVudHJpZXMsIHBhZ2luYXRpb24gfVxyXG4gICAgICAgIH07XHJcbiAgICB9XHJcblxyXG4gICAgcmVtYXBGYXZvcml0ZUVudHJpZXMoZW50cmllczogYW55W10pIHtcclxuICAgICAgICByZXR1cm4gZW50cmllc1xyXG4gICAgICAgICAgICAubWFwKCh7IGVudHJ5OiB7IHRhcmdldCB9fTogYW55KSA9PiAoe1xyXG4gICAgICAgICAgICAgICAgZW50cnk6IHRhcmdldC5maWxlIHx8IHRhcmdldC5mb2xkZXJcclxuICAgICAgICAgICAgfSkpXHJcbiAgICAgICAgICAgIC5maWx0ZXIoKHsgZW50cnkgfSkgPT4gKCEhZW50cnkpKVxyXG4gICAgICAgICAgICAubWFwKEZhdm9yaXRlc0FwaVNlcnZpY2UucmVtYXBFbnRyeSk7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBwcmVmZXJlbmNlczogVXNlclByZWZlcmVuY2VzU2VydmljZVxyXG4gICAgKSB7fVxyXG5cclxuICAgIHByaXZhdGUgZ2V0IGZhdm9yaXRlc0FwaSgpIHtcclxuICAgICAgIHJldHVybiB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5jb3JlLmZhdm9yaXRlc0FwaTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIGZhdm9yaXRlcyBmb3IgYSB1c2VyLlxyXG4gICAgICogQHBhcmFtIHBlcnNvbklkIElEIG9mIHRoZSB1c2VyXHJcbiAgICAgKiBAcGFyYW0gb3B0aW9ucyBPcHRpb25zIHN1cHBvcnRlZCBieSBKUy1BUElcclxuICAgICAqIEByZXR1cm5zIExpc3Qgb2YgZmF2b3JpdGVzXHJcbiAgICAgKi9cclxuICAgIGdldEZhdm9yaXRlcyhwZXJzb25JZDogc3RyaW5nLCBvcHRpb25zPzogYW55KTogT2JzZXJ2YWJsZTxOb2RlUGFnaW5nPiB7XHJcbiAgICAgICAgY29uc3QgZGVmYXVsdE9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIG1heEl0ZW1zOiB0aGlzLnByZWZlcmVuY2VzLnBhZ2luYXRpb25TaXplLFxyXG4gICAgICAgICAgICBza2lwQ291bnQ6IDAsXHJcbiAgICAgICAgICAgIHdoZXJlOiAnKEVYSVNUUyh0YXJnZXQvZmlsZSkgT1IgRVhJU1RTKHRhcmdldC9mb2xkZXIpKScsXHJcbiAgICAgICAgICAgIGluY2x1ZGU6IFsgJ3Byb3BlcnRpZXMnLCAnYWxsb3dhYmxlT3BlcmF0aW9ucycgXVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgY29uc3QgcXVlcnlPcHRpb25zID0gT2JqZWN0LmFzc2lnbihkZWZhdWx0T3B0aW9ucywgb3B0aW9ucyk7XHJcbiAgICAgICAgY29uc3QgcHJvbWlzZSA9IHRoaXMuZmF2b3JpdGVzQXBpXHJcbiAgICAgICAgICAgIC5nZXRGYXZvcml0ZXMocGVyc29uSWQsIHF1ZXJ5T3B0aW9ucylcclxuICAgICAgICAgICAgLnRoZW4odGhpcy5yZW1hcEZhdm9yaXRlc0RhdGEpO1xyXG5cclxuICAgICAgICByZXR1cm4gZnJvbShwcm9taXNlKS5waXBlKFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IG9mKGVycikpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxufVxyXG4iXX0=