/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var JwtHelperService = /** @class */ (function () {
    function JwtHelperService() {
    }
    /**
     * Decodes a JSON web token into a JS object.
     * @param token Token in encoded form
     * @returns Decoded token data object
     */
    /**
     * Decodes a JSON web token into a JS object.
     * @param {?} token Token in encoded form
     * @return {?} Decoded token data object
     */
    JwtHelperService.prototype.decodeToken = /**
     * Decodes a JSON web token into a JS object.
     * @param {?} token Token in encoded form
     * @return {?} Decoded token data object
     */
    function (token) {
        /** @type {?} */
        var parts = token.split('.');
        if (parts.length !== 3) {
            throw new Error('JWT must have 3 parts');
        }
        /** @type {?} */
        var decoded = this.urlBase64Decode(parts[1]);
        if (!decoded) {
            throw new Error('Cannot decode the token');
        }
        return JSON.parse(decoded);
    };
    /**
     * @private
     * @param {?} token
     * @return {?}
     */
    JwtHelperService.prototype.urlBase64Decode = /**
     * @private
     * @param {?} token
     * @return {?}
     */
    function (token) {
        /** @type {?} */
        var output = token.replace(/-/g, '+').replace(/_/g, '/');
        switch (output.length % 4) {
            case 0: {
                break;
            }
            case 2: {
                output += '==';
                break;
            }
            case 3: {
                output += '=';
                break;
            }
            default: {
                throw new Error('Illegal base64url string!');
            }
        }
        return decodeURIComponent(escape(window.atob(output)));
    };
    /**
     * Gets a named value from the user access token.
     * @param key Key name of the field to retrieve
     * @returns Value from the token
     */
    /**
     * Gets a named value from the user access token.
     * @template T
     * @param {?} key Key name of the field to retrieve
     * @return {?} Value from the token
     */
    JwtHelperService.prototype.getValueFromLocalAccessToken = /**
     * Gets a named value from the user access token.
     * @template T
     * @param {?} key Key name of the field to retrieve
     * @return {?} Value from the token
     */
    function (key) {
        return this.getValueFromToken(this.getAccessToken(), key);
    };
    /**
     * Gets access token
     * @returns access token
     */
    /**
     * Gets access token
     * @return {?} access token
     */
    JwtHelperService.prototype.getAccessToken = /**
     * Gets access token
     * @return {?} access token
     */
    function () {
        return localStorage.getItem(JwtHelperService.USER_ACCESS_TOKEN);
    };
    /**
     * Gets a named value from the user access token.
     * @param key accessToken
     * @param key Key name of the field to retrieve
     * @returns Value from the token
     */
    /**
     * Gets a named value from the user access token.
     * @template T
     * @param {?} accessToken
     * @param {?} key accessToken
     * @return {?} Value from the token
     */
    JwtHelperService.prototype.getValueFromToken = /**
     * Gets a named value from the user access token.
     * @template T
     * @param {?} accessToken
     * @param {?} key accessToken
     * @return {?} Value from the token
     */
    function (accessToken, key) {
        /** @type {?} */
        var value;
        if (accessToken) {
            /** @type {?} */
            var tokenPayload = this.decodeToken(accessToken);
            value = tokenPayload[key];
        }
        return (/** @type {?} */ (value));
    };
    JwtHelperService.USER_NAME = 'name';
    JwtHelperService.FAMILY_NAME = 'family_name';
    JwtHelperService.GIVEN_NAME = 'given_name';
    JwtHelperService.USER_EMAIL = 'email';
    JwtHelperService.USER_ACCESS_TOKEN = 'access_token';
    JwtHelperService.USER_PREFERRED_USERNAME = 'preferred_username';
    JwtHelperService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    JwtHelperService.ctorParameters = function () { return []; };
    /** @nocollapse */ JwtHelperService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function JwtHelperService_Factory() { return new JwtHelperService(); }, token: JwtHelperService, providedIn: "root" });
    return JwtHelperService;
}());
export { JwtHelperService };
if (false) {
    /** @type {?} */
    JwtHelperService.USER_NAME;
    /** @type {?} */
    JwtHelperService.FAMILY_NAME;
    /** @type {?} */
    JwtHelperService.GIVEN_NAME;
    /** @type {?} */
    JwtHelperService.USER_EMAIL;
    /** @type {?} */
    JwtHelperService.USER_ACCESS_TOKEN;
    /** @type {?} */
    JwtHelperService.USER_PREFERRED_USERNAME;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiand0LWhlbHBlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvand0LWhlbHBlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBRTNDO0lBWUk7SUFDQSxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsc0NBQVc7Ozs7O0lBQVgsVUFBWSxLQUFLOztZQUNQLEtBQUssR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQztRQUU5QixJQUFJLEtBQUssQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1lBQ3BCLE1BQU0sSUFBSSxLQUFLLENBQUMsdUJBQXVCLENBQUMsQ0FBQztTQUM1Qzs7WUFFSyxPQUFPLEdBQUcsSUFBSSxDQUFDLGVBQWUsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUM7UUFDOUMsSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNWLE1BQU0sSUFBSSxLQUFLLENBQUMseUJBQXlCLENBQUMsQ0FBQztTQUM5QztRQUVELE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUMvQixDQUFDOzs7Ozs7SUFFTywwQ0FBZTs7Ozs7SUFBdkIsVUFBd0IsS0FBSzs7WUFDckIsTUFBTSxHQUFHLEtBQUssQ0FBQyxPQUFPLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQUUsR0FBRyxDQUFDO1FBQ3hELFFBQVEsTUFBTSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDdkIsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDSixNQUFNO2FBQ1Q7WUFDRCxLQUFLLENBQUMsQ0FBQyxDQUFDO2dCQUNKLE1BQU0sSUFBSSxJQUFJLENBQUM7Z0JBQ2YsTUFBTTthQUNUO1lBQ0QsS0FBSyxDQUFDLENBQUMsQ0FBQztnQkFDSixNQUFNLElBQUksR0FBRyxDQUFDO2dCQUNkLE1BQU07YUFDVDtZQUNELE9BQU8sQ0FBQyxDQUFDO2dCQUNMLE1BQU0sSUFBSSxLQUFLLENBQUMsMkJBQTJCLENBQUMsQ0FBQzthQUNoRDtTQUNKO1FBQ0QsT0FBTyxrQkFBa0IsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDM0QsQ0FBQztJQUVEOzs7O09BSUc7Ozs7Ozs7SUFDSCx1REFBNEI7Ozs7OztJQUE1QixVQUFnQyxHQUFXO1FBQ3ZDLE9BQU8sSUFBSSxDQUFDLGlCQUFpQixDQUFDLElBQUksQ0FBQyxjQUFjLEVBQUUsRUFBRSxHQUFHLENBQUMsQ0FBQztJQUM5RCxDQUFDO0lBRUQ7OztPQUdHOzs7OztJQUNILHlDQUFjOzs7O0lBQWQ7UUFDSSxPQUFPLFlBQVksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsaUJBQWlCLENBQUMsQ0FBQztJQUNwRSxDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7O0lBQ0gsNENBQWlCOzs7Ozs7O0lBQWpCLFVBQXFCLFdBQW1CLEVBQUUsR0FBVzs7WUFDN0MsS0FBSztRQUNULElBQUksV0FBVyxFQUFFOztnQkFDUCxZQUFZLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUM7WUFDbEQsS0FBSyxHQUFHLFlBQVksQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUM3QjtRQUNELE9BQU8sbUJBQUksS0FBSyxFQUFBLENBQUM7SUFDckIsQ0FBQztJQWpGTSwwQkFBUyxHQUFHLE1BQU0sQ0FBQztJQUNuQiw0QkFBVyxHQUFHLGFBQWEsQ0FBQztJQUM1QiwyQkFBVSxHQUFHLFlBQVksQ0FBQztJQUMxQiwyQkFBVSxHQUFHLE9BQU8sQ0FBQztJQUNyQixrQ0FBaUIsR0FBRyxjQUFjLENBQUM7SUFDbkMsd0NBQXVCLEdBQUcsb0JBQW9CLENBQUM7O2dCQVZ6RCxVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7OzsyQkFyQkQ7Q0EwR0MsQUF2RkQsSUF1RkM7U0FwRlksZ0JBQWdCOzs7SUFFekIsMkJBQTBCOztJQUMxQiw2QkFBbUM7O0lBQ25DLDRCQUFpQzs7SUFDakMsNEJBQTRCOztJQUM1QixtQ0FBMEM7O0lBQzFDLHlDQUFzRCIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEp3dEhlbHBlclNlcnZpY2Uge1xyXG5cclxuICAgIHN0YXRpYyBVU0VSX05BTUUgPSAnbmFtZSc7XHJcbiAgICBzdGF0aWMgRkFNSUxZX05BTUUgPSAnZmFtaWx5X25hbWUnO1xyXG4gICAgc3RhdGljIEdJVkVOX05BTUUgPSAnZ2l2ZW5fbmFtZSc7XHJcbiAgICBzdGF0aWMgVVNFUl9FTUFJTCA9ICdlbWFpbCc7XHJcbiAgICBzdGF0aWMgVVNFUl9BQ0NFU1NfVE9LRU4gPSAnYWNjZXNzX3Rva2VuJztcclxuICAgIHN0YXRpYyBVU0VSX1BSRUZFUlJFRF9VU0VSTkFNRSA9ICdwcmVmZXJyZWRfdXNlcm5hbWUnO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogRGVjb2RlcyBhIEpTT04gd2ViIHRva2VuIGludG8gYSBKUyBvYmplY3QuXHJcbiAgICAgKiBAcGFyYW0gdG9rZW4gVG9rZW4gaW4gZW5jb2RlZCBmb3JtXHJcbiAgICAgKiBAcmV0dXJucyBEZWNvZGVkIHRva2VuIGRhdGEgb2JqZWN0XHJcbiAgICAgKi9cclxuICAgIGRlY29kZVRva2VuKHRva2VuKTogT2JqZWN0IHtcclxuICAgICAgICBjb25zdCBwYXJ0cyA9IHRva2VuLnNwbGl0KCcuJyk7XHJcblxyXG4gICAgICAgIGlmIChwYXJ0cy5sZW5ndGggIT09IDMpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdKV1QgbXVzdCBoYXZlIDMgcGFydHMnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGRlY29kZWQgPSB0aGlzLnVybEJhc2U2NERlY29kZShwYXJ0c1sxXSk7XHJcbiAgICAgICAgaWYgKCFkZWNvZGVkKSB7XHJcbiAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignQ2Fubm90IGRlY29kZSB0aGUgdG9rZW4nKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBKU09OLnBhcnNlKGRlY29kZWQpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgdXJsQmFzZTY0RGVjb2RlKHRva2VuKTogc3RyaW5nIHtcclxuICAgICAgICBsZXQgb3V0cHV0ID0gdG9rZW4ucmVwbGFjZSgvLS9nLCAnKycpLnJlcGxhY2UoL18vZywgJy8nKTtcclxuICAgICAgICBzd2l0Y2ggKG91dHB1dC5sZW5ndGggJSA0KSB7XHJcbiAgICAgICAgICAgIGNhc2UgMDoge1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgY2FzZSAyOiB7XHJcbiAgICAgICAgICAgICAgICBvdXRwdXQgKz0gJz09JztcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGNhc2UgMzoge1xyXG4gICAgICAgICAgICAgICAgb3V0cHV0ICs9ICc9JztcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGRlZmF1bHQ6IHtcclxuICAgICAgICAgICAgICAgIHRocm93IG5ldyBFcnJvcignSWxsZWdhbCBiYXNlNjR1cmwgc3RyaW5nIScpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBkZWNvZGVVUklDb21wb25lbnQoZXNjYXBlKHdpbmRvdy5hdG9iKG91dHB1dCkpKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBuYW1lZCB2YWx1ZSBmcm9tIHRoZSB1c2VyIGFjY2VzcyB0b2tlbi5cclxuICAgICAqIEBwYXJhbSBrZXkgS2V5IG5hbWUgb2YgdGhlIGZpZWxkIHRvIHJldHJpZXZlXHJcbiAgICAgKiBAcmV0dXJucyBWYWx1ZSBmcm9tIHRoZSB0b2tlblxyXG4gICAgICovXHJcbiAgICBnZXRWYWx1ZUZyb21Mb2NhbEFjY2Vzc1Rva2VuPFQ+KGtleTogc3RyaW5nKTogVCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0VmFsdWVGcm9tVG9rZW4odGhpcy5nZXRBY2Nlc3NUb2tlbigpLCBrZXkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhY2Nlc3MgdG9rZW5cclxuICAgICAqIEByZXR1cm5zIGFjY2VzcyB0b2tlblxyXG4gICAgICovXHJcbiAgICBnZXRBY2Nlc3NUb2tlbigpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShKd3RIZWxwZXJTZXJ2aWNlLlVTRVJfQUNDRVNTX1RPS0VOKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBuYW1lZCB2YWx1ZSBmcm9tIHRoZSB1c2VyIGFjY2VzcyB0b2tlbi5cclxuICAgICAqIEBwYXJhbSBrZXkgYWNjZXNzVG9rZW5cclxuICAgICAqIEBwYXJhbSBrZXkgS2V5IG5hbWUgb2YgdGhlIGZpZWxkIHRvIHJldHJpZXZlXHJcbiAgICAgKiBAcmV0dXJucyBWYWx1ZSBmcm9tIHRoZSB0b2tlblxyXG4gICAgICovXHJcbiAgICBnZXRWYWx1ZUZyb21Ub2tlbjxUPihhY2Nlc3NUb2tlbjogc3RyaW5nLCBrZXk6IHN0cmluZyk6IFQge1xyXG4gICAgICAgIGxldCB2YWx1ZTtcclxuICAgICAgICBpZiAoYWNjZXNzVG9rZW4pIHtcclxuICAgICAgICAgICAgY29uc3QgdG9rZW5QYXlsb2FkID0gdGhpcy5kZWNvZGVUb2tlbihhY2Nlc3NUb2tlbik7XHJcbiAgICAgICAgICAgIHZhbHVlID0gdG9rZW5QYXlsb2FkW2tleV07XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiA8VD4gdmFsdWU7XHJcbiAgICB9XHJcbn1cclxuIl19