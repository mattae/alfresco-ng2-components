/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* tslint:disable:no-console  */
import { Injectable } from '@angular/core';
import { AppConfigService, AppConfigValues } from '../app-config/app-config.service';
import { logLevels, LogLevelsEnum } from '../models/log-levels.model';
import { Subject } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "../app-config/app-config.service";
var LogService = /** @class */ (function () {
    function LogService(appConfig) {
        this.appConfig = appConfig;
        this.onMessage = new Subject();
    }
    Object.defineProperty(LogService.prototype, "currentLogLevel", {
        get: /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var configLevel = this.appConfig.get(AppConfigValues.LOG_LEVEL);
            if (configLevel) {
                return this.getLogLevel(configLevel);
            }
            return LogLevelsEnum.TRACE;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Logs a message at the "ERROR" level.
     * @param message Message to log
     * @param optionalParams Interpolation values for the message in "printf" format
     */
    /**
     * Logs a message at the "ERROR" level.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    LogService.prototype.error = /**
     * Logs a message at the "ERROR" level.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    function (message) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        if (this.currentLogLevel >= LogLevelsEnum.ERROR) {
            this.messageBus(message, 'ERROR');
            console.error.apply(console, tslib_1.__spread([message], optionalParams));
        }
    };
    /**
     * Logs a message at the "DEBUG" level.
     * @param message Message to log
     * @param optionalParams Interpolation values for the message in "printf" format
     */
    /**
     * Logs a message at the "DEBUG" level.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    LogService.prototype.debug = /**
     * Logs a message at the "DEBUG" level.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    function (message) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        if (this.currentLogLevel >= LogLevelsEnum.DEBUG) {
            this.messageBus(message, 'DEBUG');
            console.debug.apply(console, tslib_1.__spread([message], optionalParams));
        }
    };
    /**
     * Logs a message at the "INFO" level.
     * @param message Message to log
     * @param optionalParams Interpolation values for the message in "printf" format
     */
    /**
     * Logs a message at the "INFO" level.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    LogService.prototype.info = /**
     * Logs a message at the "INFO" level.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    function (message) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        if (this.currentLogLevel >= LogLevelsEnum.INFO) {
            this.messageBus(message, 'INFO');
            console.info.apply(console, tslib_1.__spread([message], optionalParams));
        }
    };
    /**
     * Logs a message at any level from "TRACE" upwards.
     * @param message Message to log
     * @param optionalParams Interpolation values for the message in "printf" format
     */
    /**
     * Logs a message at any level from "TRACE" upwards.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    LogService.prototype.log = /**
     * Logs a message at any level from "TRACE" upwards.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    function (message) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        if (this.currentLogLevel >= LogLevelsEnum.TRACE) {
            this.messageBus(message, 'LOG');
            console.log.apply(console, tslib_1.__spread([message], optionalParams));
        }
    };
    /**
     * Logs a message at the "TRACE" level.
     * @param message Message to log
     * @param optionalParams Interpolation values for the message in "printf" format
     */
    /**
     * Logs a message at the "TRACE" level.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    LogService.prototype.trace = /**
     * Logs a message at the "TRACE" level.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    function (message) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        if (this.currentLogLevel >= LogLevelsEnum.TRACE) {
            this.messageBus(message, 'TRACE');
            console.trace.apply(console, tslib_1.__spread([message], optionalParams));
        }
    };
    /**
     * Logs a message at the "WARN" level.
     * @param message Message to log
     * @param optionalParams Interpolation values for the message in "printf" format
     */
    /**
     * Logs a message at the "WARN" level.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    LogService.prototype.warn = /**
     * Logs a message at the "WARN" level.
     * @param {?=} message Message to log
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    function (message) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        if (this.currentLogLevel >= LogLevelsEnum.WARN) {
            this.messageBus(message, 'WARN');
            console.warn.apply(console, tslib_1.__spread([message], optionalParams));
        }
    };
    /**
     * Logs a message if a boolean test fails.
     * @param test Test value (typically a boolean expression)
     * @param message Message to show if test is false
     * @param optionalParams Interpolation values for the message in "printf" format
     */
    /**
     * Logs a message if a boolean test fails.
     * @param {?=} test Test value (typically a boolean expression)
     * @param {?=} message Message to show if test is false
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    LogService.prototype.assert = /**
     * Logs a message if a boolean test fails.
     * @param {?=} test Test value (typically a boolean expression)
     * @param {?=} message Message to show if test is false
     * @param {...?} optionalParams Interpolation values for the message in "printf" format
     * @return {?}
     */
    function (test, message) {
        var optionalParams = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            optionalParams[_i - 2] = arguments[_i];
        }
        if (this.currentLogLevel !== LogLevelsEnum.SILENT) {
            this.messageBus(message, 'ASSERT');
            console.assert.apply(console, tslib_1.__spread([test, message], optionalParams));
        }
    };
    /**
     * Starts an indented group of log messages.
     * @param groupTitle Title shown at the start of the group
     * @param optionalParams Interpolation values for the title in "printf" format
     */
    /**
     * Starts an indented group of log messages.
     * @param {?=} groupTitle Title shown at the start of the group
     * @param {...?} optionalParams Interpolation values for the title in "printf" format
     * @return {?}
     */
    LogService.prototype.group = /**
     * Starts an indented group of log messages.
     * @param {?=} groupTitle Title shown at the start of the group
     * @param {...?} optionalParams Interpolation values for the title in "printf" format
     * @return {?}
     */
    function (groupTitle) {
        var optionalParams = [];
        for (var _i = 1; _i < arguments.length; _i++) {
            optionalParams[_i - 1] = arguments[_i];
        }
        if (this.currentLogLevel !== LogLevelsEnum.SILENT) {
            console.group.apply(console, tslib_1.__spread([groupTitle], optionalParams));
        }
    };
    /**
     * Ends a indented group of log messages.
     */
    /**
     * Ends a indented group of log messages.
     * @return {?}
     */
    LogService.prototype.groupEnd = /**
     * Ends a indented group of log messages.
     * @return {?}
     */
    function () {
        if (this.currentLogLevel !== LogLevelsEnum.SILENT) {
            console.groupEnd();
        }
    };
    /**
     * Converts a log level name string into its numeric equivalent.
     * @param level Level name
     * @returns Numeric log level
     */
    /**
     * Converts a log level name string into its numeric equivalent.
     * @param {?} level Level name
     * @return {?} Numeric log level
     */
    LogService.prototype.getLogLevel = /**
     * Converts a log level name string into its numeric equivalent.
     * @param {?} level Level name
     * @return {?} Numeric log level
     */
    function (level) {
        /** @type {?} */
        var referencedLevel = logLevels.find((/**
         * @param {?} currentLevel
         * @return {?}
         */
        function (currentLevel) {
            return currentLevel.name.toLocaleLowerCase() === level.toLocaleLowerCase();
        }));
        return referencedLevel ? referencedLevel.level : 5;
    };
    /**
     * Triggers notification callback for log messages.
     * @param text Message text
     * @param logLevel Log level for the message
     */
    /**
     * Triggers notification callback for log messages.
     * @param {?} text Message text
     * @param {?} logLevel Log level for the message
     * @return {?}
     */
    LogService.prototype.messageBus = /**
     * Triggers notification callback for log messages.
     * @param {?} text Message text
     * @param {?} logLevel Log level for the message
     * @return {?}
     */
    function (text, logLevel) {
        this.onMessage.next({ text: text, type: logLevel });
    };
    LogService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    LogService.ctorParameters = function () { return [
        { type: AppConfigService }
    ]; };
    /** @nocollapse */ LogService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function LogService_Factory() { return new LogService(i0.ɵɵinject(i1.AppConfigService)); }, token: LogService, providedIn: "root" });
    return LogService;
}());
export { LogService };
if (false) {
    /** @type {?} */
    LogService.prototype.onMessage;
    /**
     * @type {?}
     * @private
     */
    LogService.prototype.appConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibG9nLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9sb2cuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLGVBQWUsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JGLE9BQU8sRUFBRSxTQUFTLEVBQUUsYUFBYSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDdEUsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQzs7O0FBRS9CO0lBaUJJLG9CQUFvQixTQUEyQjtRQUEzQixjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQUMzQyxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksT0FBTyxFQUFFLENBQUM7SUFDbkMsQ0FBQztJQWRELHNCQUFJLHVDQUFlOzs7O1FBQW5COztnQkFDVSxXQUFXLEdBQVcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQVMsZUFBZSxDQUFDLFNBQVMsQ0FBQztZQUVqRixJQUFJLFdBQVcsRUFBRTtnQkFDYixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDeEM7WUFFRCxPQUFPLGFBQWEsQ0FBQyxLQUFLLENBQUM7UUFDL0IsQ0FBQzs7O09BQUE7SUFRRDs7OztPQUlHOzs7Ozs7O0lBQ0gsMEJBQUs7Ozs7OztJQUFMLFVBQU0sT0FBYTtRQUFFLHdCQUF3QjthQUF4QixVQUF3QixFQUF4QixxQkFBd0IsRUFBeEIsSUFBd0I7WUFBeEIsdUNBQXdCOztRQUN6QyxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksYUFBYSxDQUFDLEtBQUssRUFBRTtZQUU3QyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQztZQUVsQyxPQUFPLENBQUMsS0FBSyxPQUFiLE9BQU8sb0JBQU8sT0FBTyxHQUFLLGNBQWMsR0FBRTtTQUM3QztJQUNMLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsMEJBQUs7Ozs7OztJQUFMLFVBQU0sT0FBYTtRQUFFLHdCQUF3QjthQUF4QixVQUF3QixFQUF4QixxQkFBd0IsRUFBeEIsSUFBd0I7WUFBeEIsdUNBQXdCOztRQUN6QyxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksYUFBYSxDQUFDLEtBQUssRUFBRTtZQUU3QyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQztZQUVsQyxPQUFPLENBQUMsS0FBSyxPQUFiLE9BQU8sb0JBQU8sT0FBTyxHQUFLLGNBQWMsR0FBRTtTQUM3QztJQUNMLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gseUJBQUk7Ozs7OztJQUFKLFVBQUssT0FBYTtRQUFFLHdCQUF3QjthQUF4QixVQUF3QixFQUF4QixxQkFBd0IsRUFBeEIsSUFBd0I7WUFBeEIsdUNBQXdCOztRQUN4QyxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksYUFBYSxDQUFDLElBQUksRUFBRTtZQUU1QyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztZQUVqQyxPQUFPLENBQUMsSUFBSSxPQUFaLE9BQU8sb0JBQU0sT0FBTyxHQUFLLGNBQWMsR0FBRTtTQUM1QztJQUNMLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsd0JBQUc7Ozs7OztJQUFILFVBQUksT0FBYTtRQUFFLHdCQUF3QjthQUF4QixVQUF3QixFQUF4QixxQkFBd0IsRUFBeEIsSUFBd0I7WUFBeEIsdUNBQXdCOztRQUN2QyxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksYUFBYSxDQUFDLEtBQUssRUFBRTtZQUU3QyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxLQUFLLENBQUMsQ0FBQztZQUVoQyxPQUFPLENBQUMsR0FBRyxPQUFYLE9BQU8sb0JBQUssT0FBTyxHQUFLLGNBQWMsR0FBRTtTQUMzQztJQUNMLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsMEJBQUs7Ozs7OztJQUFMLFVBQU0sT0FBYTtRQUFFLHdCQUF3QjthQUF4QixVQUF3QixFQUF4QixxQkFBd0IsRUFBeEIsSUFBd0I7WUFBeEIsdUNBQXdCOztRQUN6QyxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksYUFBYSxDQUFDLEtBQUssRUFBRTtZQUU3QyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxPQUFPLENBQUMsQ0FBQztZQUVsQyxPQUFPLENBQUMsS0FBSyxPQUFiLE9BQU8sb0JBQU8sT0FBTyxHQUFLLGNBQWMsR0FBRTtTQUM3QztJQUNMLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gseUJBQUk7Ozs7OztJQUFKLFVBQUssT0FBYTtRQUFFLHdCQUF3QjthQUF4QixVQUF3QixFQUF4QixxQkFBd0IsRUFBeEIsSUFBd0I7WUFBeEIsdUNBQXdCOztRQUN4QyxJQUFJLElBQUksQ0FBQyxlQUFlLElBQUksYUFBYSxDQUFDLElBQUksRUFBRTtZQUU1QyxJQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sRUFBRSxNQUFNLENBQUMsQ0FBQztZQUVqQyxPQUFPLENBQUMsSUFBSSxPQUFaLE9BQU8sb0JBQU0sT0FBTyxHQUFLLGNBQWMsR0FBRTtTQUM1QztJQUNMLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7Ozs7SUFDSCwyQkFBTTs7Ozs7OztJQUFOLFVBQU8sSUFBYyxFQUFFLE9BQWdCO1FBQUUsd0JBQXdCO2FBQXhCLFVBQXdCLEVBQXhCLHFCQUF3QixFQUF4QixJQUF3QjtZQUF4Qix1Q0FBd0I7O1FBQzdELElBQUksSUFBSSxDQUFDLGVBQWUsS0FBSyxhQUFhLENBQUMsTUFBTSxFQUFFO1lBRS9DLElBQUksQ0FBQyxVQUFVLENBQUMsT0FBTyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1lBRW5DLE9BQU8sQ0FBQyxNQUFNLE9BQWQsT0FBTyxvQkFBUSxJQUFJLEVBQUUsT0FBTyxHQUFLLGNBQWMsR0FBRTtTQUNwRDtJQUNMLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsMEJBQUs7Ozs7OztJQUFMLFVBQU0sVUFBbUI7UUFBRSx3QkFBd0I7YUFBeEIsVUFBd0IsRUFBeEIscUJBQXdCLEVBQXhCLElBQXdCO1lBQXhCLHVDQUF3Qjs7UUFDL0MsSUFBSSxJQUFJLENBQUMsZUFBZSxLQUFLLGFBQWEsQ0FBQyxNQUFNLEVBQUU7WUFDL0MsT0FBTyxDQUFDLEtBQUssT0FBYixPQUFPLG9CQUFPLFVBQVUsR0FBSyxjQUFjLEdBQUU7U0FDaEQ7SUFDTCxDQUFDO0lBRUQ7O09BRUc7Ozs7O0lBQ0gsNkJBQVE7Ozs7SUFBUjtRQUNJLElBQUksSUFBSSxDQUFDLGVBQWUsS0FBSyxhQUFhLENBQUMsTUFBTSxFQUFFO1lBQy9DLE9BQU8sQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUN0QjtJQUNMLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCxnQ0FBVzs7Ozs7SUFBWCxVQUFZLEtBQWE7O1lBQ2YsZUFBZSxHQUFHLFNBQVMsQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQyxZQUFpQjtZQUNyRCxPQUFPLFlBQVksQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsS0FBSyxLQUFLLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUMvRSxDQUFDLEVBQUM7UUFFRixPQUFPLGVBQWUsQ0FBQyxDQUFDLENBQUMsZUFBZSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3ZELENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsK0JBQVU7Ozs7OztJQUFWLFVBQVcsSUFBWSxFQUFFLFFBQWdCO1FBQ3JDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLENBQUMsQ0FBQztJQUN4RCxDQUFDOztnQkFoS0osVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFOUSxnQkFBZ0I7OztxQkFwQnpCO0NBeUxDLEFBaktELElBaUtDO1NBOUpZLFVBQVU7OztJQVluQiwrQkFBd0I7Ozs7O0lBRVosK0JBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbi8qIHRzbGludDpkaXNhYmxlOm5vLWNvbnNvbGUgICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UsIEFwcENvbmZpZ1ZhbHVlcyB9IGZyb20gJy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgbG9nTGV2ZWxzLCBMb2dMZXZlbHNFbnVtIH0gZnJvbSAnLi4vbW9kZWxzL2xvZy1sZXZlbHMubW9kZWwnO1xyXG5pbXBvcnQgeyBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIExvZ1NlcnZpY2Uge1xyXG5cclxuICAgIGdldCBjdXJyZW50TG9nTGV2ZWwoKSB7XHJcbiAgICAgICAgY29uc3QgY29uZmlnTGV2ZWw6IHN0cmluZyA9IHRoaXMuYXBwQ29uZmlnLmdldDxzdHJpbmc+KEFwcENvbmZpZ1ZhbHVlcy5MT0dfTEVWRUwpO1xyXG5cclxuICAgICAgICBpZiAoY29uZmlnTGV2ZWwpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0TG9nTGV2ZWwoY29uZmlnTGV2ZWwpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIExvZ0xldmVsc0VudW0uVFJBQ0U7XHJcbiAgICB9XHJcblxyXG4gICAgb25NZXNzYWdlOiBTdWJqZWN0PGFueT47XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhcHBDb25maWc6IEFwcENvbmZpZ1NlcnZpY2UpIHtcclxuICAgICAgICB0aGlzLm9uTWVzc2FnZSA9IG5ldyBTdWJqZWN0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBMb2dzIGEgbWVzc2FnZSBhdCB0aGUgXCJFUlJPUlwiIGxldmVsLlxyXG4gICAgICogQHBhcmFtIG1lc3NhZ2UgTWVzc2FnZSB0byBsb2dcclxuICAgICAqIEBwYXJhbSBvcHRpb25hbFBhcmFtcyBJbnRlcnBvbGF0aW9uIHZhbHVlcyBmb3IgdGhlIG1lc3NhZ2UgaW4gXCJwcmludGZcIiBmb3JtYXRcclxuICAgICAqL1xyXG4gICAgZXJyb3IobWVzc2FnZT86IGFueSwgLi4ub3B0aW9uYWxQYXJhbXM6IGFueVtdKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY3VycmVudExvZ0xldmVsID49IExvZ0xldmVsc0VudW0uRVJST1IpIHtcclxuXHJcbiAgICAgICAgICAgIHRoaXMubWVzc2FnZUJ1cyhtZXNzYWdlLCAnRVJST1InKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnNvbGUuZXJyb3IobWVzc2FnZSwgLi4ub3B0aW9uYWxQYXJhbXMpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExvZ3MgYSBtZXNzYWdlIGF0IHRoZSBcIkRFQlVHXCIgbGV2ZWwuXHJcbiAgICAgKiBAcGFyYW0gbWVzc2FnZSBNZXNzYWdlIHRvIGxvZ1xyXG4gICAgICogQHBhcmFtIG9wdGlvbmFsUGFyYW1zIEludGVycG9sYXRpb24gdmFsdWVzIGZvciB0aGUgbWVzc2FnZSBpbiBcInByaW50ZlwiIGZvcm1hdFxyXG4gICAgICovXHJcbiAgICBkZWJ1ZyhtZXNzYWdlPzogYW55LCAuLi5vcHRpb25hbFBhcmFtczogYW55W10pIHtcclxuICAgICAgICBpZiAodGhpcy5jdXJyZW50TG9nTGV2ZWwgPj0gTG9nTGV2ZWxzRW51bS5ERUJVRykge1xyXG5cclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlQnVzKG1lc3NhZ2UsICdERUJVRycpO1xyXG5cclxuICAgICAgICAgICAgY29uc29sZS5kZWJ1ZyhtZXNzYWdlLCAuLi5vcHRpb25hbFBhcmFtcyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTG9ncyBhIG1lc3NhZ2UgYXQgdGhlIFwiSU5GT1wiIGxldmVsLlxyXG4gICAgICogQHBhcmFtIG1lc3NhZ2UgTWVzc2FnZSB0byBsb2dcclxuICAgICAqIEBwYXJhbSBvcHRpb25hbFBhcmFtcyBJbnRlcnBvbGF0aW9uIHZhbHVlcyBmb3IgdGhlIG1lc3NhZ2UgaW4gXCJwcmludGZcIiBmb3JtYXRcclxuICAgICAqL1xyXG4gICAgaW5mbyhtZXNzYWdlPzogYW55LCAuLi5vcHRpb25hbFBhcmFtczogYW55W10pIHtcclxuICAgICAgICBpZiAodGhpcy5jdXJyZW50TG9nTGV2ZWwgPj0gTG9nTGV2ZWxzRW51bS5JTkZPKSB7XHJcblxyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2VCdXMobWVzc2FnZSwgJ0lORk8nKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnNvbGUuaW5mbyhtZXNzYWdlLCAuLi5vcHRpb25hbFBhcmFtcyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTG9ncyBhIG1lc3NhZ2UgYXQgYW55IGxldmVsIGZyb20gXCJUUkFDRVwiIHVwd2FyZHMuXHJcbiAgICAgKiBAcGFyYW0gbWVzc2FnZSBNZXNzYWdlIHRvIGxvZ1xyXG4gICAgICogQHBhcmFtIG9wdGlvbmFsUGFyYW1zIEludGVycG9sYXRpb24gdmFsdWVzIGZvciB0aGUgbWVzc2FnZSBpbiBcInByaW50ZlwiIGZvcm1hdFxyXG4gICAgICovXHJcbiAgICBsb2cobWVzc2FnZT86IGFueSwgLi4ub3B0aW9uYWxQYXJhbXM6IGFueVtdKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY3VycmVudExvZ0xldmVsID49IExvZ0xldmVsc0VudW0uVFJBQ0UpIHtcclxuXHJcbiAgICAgICAgICAgIHRoaXMubWVzc2FnZUJ1cyhtZXNzYWdlLCAnTE9HJyk7XHJcblxyXG4gICAgICAgICAgICBjb25zb2xlLmxvZyhtZXNzYWdlLCAuLi5vcHRpb25hbFBhcmFtcyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTG9ncyBhIG1lc3NhZ2UgYXQgdGhlIFwiVFJBQ0VcIiBsZXZlbC5cclxuICAgICAqIEBwYXJhbSBtZXNzYWdlIE1lc3NhZ2UgdG8gbG9nXHJcbiAgICAgKiBAcGFyYW0gb3B0aW9uYWxQYXJhbXMgSW50ZXJwb2xhdGlvbiB2YWx1ZXMgZm9yIHRoZSBtZXNzYWdlIGluIFwicHJpbnRmXCIgZm9ybWF0XHJcbiAgICAgKi9cclxuICAgIHRyYWNlKG1lc3NhZ2U/OiBhbnksIC4uLm9wdGlvbmFsUGFyYW1zOiBhbnlbXSkge1xyXG4gICAgICAgIGlmICh0aGlzLmN1cnJlbnRMb2dMZXZlbCA+PSBMb2dMZXZlbHNFbnVtLlRSQUNFKSB7XHJcblxyXG4gICAgICAgICAgICB0aGlzLm1lc3NhZ2VCdXMobWVzc2FnZSwgJ1RSQUNFJyk7XHJcblxyXG4gICAgICAgICAgICBjb25zb2xlLnRyYWNlKG1lc3NhZ2UsIC4uLm9wdGlvbmFsUGFyYW1zKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBMb2dzIGEgbWVzc2FnZSBhdCB0aGUgXCJXQVJOXCIgbGV2ZWwuXHJcbiAgICAgKiBAcGFyYW0gbWVzc2FnZSBNZXNzYWdlIHRvIGxvZ1xyXG4gICAgICogQHBhcmFtIG9wdGlvbmFsUGFyYW1zIEludGVycG9sYXRpb24gdmFsdWVzIGZvciB0aGUgbWVzc2FnZSBpbiBcInByaW50ZlwiIGZvcm1hdFxyXG4gICAgICovXHJcbiAgICB3YXJuKG1lc3NhZ2U/OiBhbnksIC4uLm9wdGlvbmFsUGFyYW1zOiBhbnlbXSkge1xyXG4gICAgICAgIGlmICh0aGlzLmN1cnJlbnRMb2dMZXZlbCA+PSBMb2dMZXZlbHNFbnVtLldBUk4pIHtcclxuXHJcbiAgICAgICAgICAgIHRoaXMubWVzc2FnZUJ1cyhtZXNzYWdlLCAnV0FSTicpO1xyXG5cclxuICAgICAgICAgICAgY29uc29sZS53YXJuKG1lc3NhZ2UsIC4uLm9wdGlvbmFsUGFyYW1zKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBMb2dzIGEgbWVzc2FnZSBpZiBhIGJvb2xlYW4gdGVzdCBmYWlscy5cclxuICAgICAqIEBwYXJhbSB0ZXN0IFRlc3QgdmFsdWUgKHR5cGljYWxseSBhIGJvb2xlYW4gZXhwcmVzc2lvbilcclxuICAgICAqIEBwYXJhbSBtZXNzYWdlIE1lc3NhZ2UgdG8gc2hvdyBpZiB0ZXN0IGlzIGZhbHNlXHJcbiAgICAgKiBAcGFyYW0gb3B0aW9uYWxQYXJhbXMgSW50ZXJwb2xhdGlvbiB2YWx1ZXMgZm9yIHRoZSBtZXNzYWdlIGluIFwicHJpbnRmXCIgZm9ybWF0XHJcbiAgICAgKi9cclxuICAgIGFzc2VydCh0ZXN0PzogYm9vbGVhbiwgbWVzc2FnZT86IHN0cmluZywgLi4ub3B0aW9uYWxQYXJhbXM6IGFueVtdKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY3VycmVudExvZ0xldmVsICE9PSBMb2dMZXZlbHNFbnVtLlNJTEVOVCkge1xyXG5cclxuICAgICAgICAgICAgdGhpcy5tZXNzYWdlQnVzKG1lc3NhZ2UsICdBU1NFUlQnKTtcclxuXHJcbiAgICAgICAgICAgIGNvbnNvbGUuYXNzZXJ0KHRlc3QsIG1lc3NhZ2UsIC4uLm9wdGlvbmFsUGFyYW1zKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTdGFydHMgYW4gaW5kZW50ZWQgZ3JvdXAgb2YgbG9nIG1lc3NhZ2VzLlxyXG4gICAgICogQHBhcmFtIGdyb3VwVGl0bGUgVGl0bGUgc2hvd24gYXQgdGhlIHN0YXJ0IG9mIHRoZSBncm91cFxyXG4gICAgICogQHBhcmFtIG9wdGlvbmFsUGFyYW1zIEludGVycG9sYXRpb24gdmFsdWVzIGZvciB0aGUgdGl0bGUgaW4gXCJwcmludGZcIiBmb3JtYXRcclxuICAgICAqL1xyXG4gICAgZ3JvdXAoZ3JvdXBUaXRsZT86IHN0cmluZywgLi4ub3B0aW9uYWxQYXJhbXM6IGFueVtdKSB7XHJcbiAgICAgICAgaWYgKHRoaXMuY3VycmVudExvZ0xldmVsICE9PSBMb2dMZXZlbHNFbnVtLlNJTEVOVCkge1xyXG4gICAgICAgICAgICBjb25zb2xlLmdyb3VwKGdyb3VwVGl0bGUsIC4uLm9wdGlvbmFsUGFyYW1zKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBFbmRzIGEgaW5kZW50ZWQgZ3JvdXAgb2YgbG9nIG1lc3NhZ2VzLlxyXG4gICAgICovXHJcbiAgICBncm91cEVuZCgpIHtcclxuICAgICAgICBpZiAodGhpcy5jdXJyZW50TG9nTGV2ZWwgIT09IExvZ0xldmVsc0VudW0uU0lMRU5UKSB7XHJcbiAgICAgICAgICAgIGNvbnNvbGUuZ3JvdXBFbmQoKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb252ZXJ0cyBhIGxvZyBsZXZlbCBuYW1lIHN0cmluZyBpbnRvIGl0cyBudW1lcmljIGVxdWl2YWxlbnQuXHJcbiAgICAgKiBAcGFyYW0gbGV2ZWwgTGV2ZWwgbmFtZVxyXG4gICAgICogQHJldHVybnMgTnVtZXJpYyBsb2cgbGV2ZWxcclxuICAgICAqL1xyXG4gICAgZ2V0TG9nTGV2ZWwobGV2ZWw6IHN0cmluZyk6IExvZ0xldmVsc0VudW0ge1xyXG4gICAgICAgIGNvbnN0IHJlZmVyZW5jZWRMZXZlbCA9IGxvZ0xldmVscy5maW5kKChjdXJyZW50TGV2ZWw6IGFueSkgPT4ge1xyXG4gICAgICAgICAgICByZXR1cm4gY3VycmVudExldmVsLm5hbWUudG9Mb2NhbGVMb3dlckNhc2UoKSA9PT0gbGV2ZWwudG9Mb2NhbGVMb3dlckNhc2UoKTtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHJlZmVyZW5jZWRMZXZlbCA/IHJlZmVyZW5jZWRMZXZlbC5sZXZlbCA6IDU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUcmlnZ2VycyBub3RpZmljYXRpb24gY2FsbGJhY2sgZm9yIGxvZyBtZXNzYWdlcy5cclxuICAgICAqIEBwYXJhbSB0ZXh0IE1lc3NhZ2UgdGV4dFxyXG4gICAgICogQHBhcmFtIGxvZ0xldmVsIExvZyBsZXZlbCBmb3IgdGhlIG1lc3NhZ2VcclxuICAgICAqL1xyXG4gICAgbWVzc2FnZUJ1cyh0ZXh0OiBzdHJpbmcsIGxvZ0xldmVsOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLm9uTWVzc2FnZS5uZXh0KHsgdGV4dDogdGV4dCwgdHlwZTogbG9nTGV2ZWwgfSk7XHJcbiAgICB9XHJcbn1cclxuIl19