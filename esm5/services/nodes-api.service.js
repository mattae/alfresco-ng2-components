/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { UserPreferencesService } from './user-preferences.service';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./user-preferences.service";
var NodesApiService = /** @class */ (function () {
    function NodesApiService(api, preferences) {
        this.api = api;
        this.preferences = preferences;
    }
    Object.defineProperty(NodesApiService.prototype, "nodesApi", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.api.getInstance().core.nodesApi;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @private
     * @param {?} entity
     * @return {?}
     */
    NodesApiService.prototype.getEntryFromEntity = /**
     * @private
     * @param {?} entity
     * @return {?}
     */
    function (entity) {
        return entity.entry;
    };
    /**
     * Gets the stored information about a node.
     * @param nodeId ID of the target node
     * @param options Optional parameters supported by JS-API
     * @returns Node information
     */
    /**
     * Gets the stored information about a node.
     * @param {?} nodeId ID of the target node
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} Node information
     */
    NodesApiService.prototype.getNode = /**
     * Gets the stored information about a node.
     * @param {?} nodeId ID of the target node
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} Node information
     */
    function (nodeId, options) {
        if (options === void 0) { options = {}; }
        /** @type {?} */
        var defaults = {
            include: ['path', 'properties', 'allowableOperations', 'permissions']
        };
        /** @type {?} */
        var queryOptions = Object.assign(defaults, options);
        /** @type {?} */
        var promise = this.nodesApi
            .getNode(nodeId, queryOptions)
            .then(this.getEntryFromEntity);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return throwError(err); })));
    };
    /**
     * Gets the items contained in a folder node.
     * @param nodeId ID of the target node
     * @param options Optional parameters supported by JS-API
     * @returns List of child items from the folder
     */
    /**
     * Gets the items contained in a folder node.
     * @param {?} nodeId ID of the target node
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} List of child items from the folder
     */
    NodesApiService.prototype.getNodeChildren = /**
     * Gets the items contained in a folder node.
     * @param {?} nodeId ID of the target node
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} List of child items from the folder
     */
    function (nodeId, options) {
        if (options === void 0) { options = {}; }
        /** @type {?} */
        var defaults = {
            maxItems: this.preferences.paginationSize,
            skipCount: 0,
            include: ['path', 'properties', 'allowableOperations', 'permissions']
        };
        /** @type {?} */
        var queryOptions = Object.assign(defaults, options);
        /** @type {?} */
        var promise = this.nodesApi
            .getNodeChildren(nodeId, queryOptions);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return throwError(err); })));
    };
    /**
     * Creates a new document node inside a folder.
     * @param parentNodeId ID of the parent folder node
     * @param nodeBody Data for the new node
     * @param options Optional parameters supported by JS-API
     * @returns Details of the new node
     */
    /**
     * Creates a new document node inside a folder.
     * @param {?} parentNodeId ID of the parent folder node
     * @param {?} nodeBody Data for the new node
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} Details of the new node
     */
    NodesApiService.prototype.createNode = /**
     * Creates a new document node inside a folder.
     * @param {?} parentNodeId ID of the parent folder node
     * @param {?} nodeBody Data for the new node
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} Details of the new node
     */
    function (parentNodeId, nodeBody, options) {
        if (options === void 0) { options = {}; }
        /** @type {?} */
        var promise = this.nodesApi
            .addNode(parentNodeId, nodeBody, options)
            .then(this.getEntryFromEntity);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return throwError(err); })));
    };
    /**
     * Creates a new folder node inside a parent folder.
     * @param parentNodeId ID of the parent folder node
     * @param nodeBody Data for the new folder
     * @param options Optional parameters supported by JS-API
     * @returns Details of the new folder
     */
    /**
     * Creates a new folder node inside a parent folder.
     * @param {?} parentNodeId ID of the parent folder node
     * @param {?} nodeBody Data for the new folder
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} Details of the new folder
     */
    NodesApiService.prototype.createFolder = /**
     * Creates a new folder node inside a parent folder.
     * @param {?} parentNodeId ID of the parent folder node
     * @param {?} nodeBody Data for the new folder
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} Details of the new folder
     */
    function (parentNodeId, nodeBody, options) {
        if (options === void 0) { options = {}; }
        /** @type {?} */
        var body = Object.assign({ nodeType: 'cm:folder' }, nodeBody);
        return this.createNode(parentNodeId, body, options);
    };
    /**
     * Updates the information about a node.
     * @param nodeId ID of the target node
     * @param nodeBody New data for the node
     * @param options Optional parameters supported by JS-API
     * @returns Updated node information
     */
    /**
     * Updates the information about a node.
     * @param {?} nodeId ID of the target node
     * @param {?} nodeBody New data for the node
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} Updated node information
     */
    NodesApiService.prototype.updateNode = /**
     * Updates the information about a node.
     * @param {?} nodeId ID of the target node
     * @param {?} nodeBody New data for the node
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} Updated node information
     */
    function (nodeId, nodeBody, options) {
        if (options === void 0) { options = {}; }
        /** @type {?} */
        var defaults = {
            include: ['path', 'properties', 'allowableOperations', 'permissions']
        };
        /** @type {?} */
        var queryOptions = Object.assign(defaults, options);
        /** @type {?} */
        var promise = this.nodesApi
            .updateNode(nodeId, nodeBody, queryOptions)
            .then(this.getEntryFromEntity);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return throwError(err); })));
    };
    /**
     * Moves a node to the trashcan.
     * @param nodeId ID of the target node
     * @param options Optional parameters supported by JS-API
     * @returns Empty result that notifies when the deletion is complete
     */
    /**
     * Moves a node to the trashcan.
     * @param {?} nodeId ID of the target node
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} Empty result that notifies when the deletion is complete
     */
    NodesApiService.prototype.deleteNode = /**
     * Moves a node to the trashcan.
     * @param {?} nodeId ID of the target node
     * @param {?=} options Optional parameters supported by JS-API
     * @return {?} Empty result that notifies when the deletion is complete
     */
    function (nodeId, options) {
        if (options === void 0) { options = {}; }
        /** @type {?} */
        var promise = this.nodesApi.deleteNode(nodeId, options);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return throwError(err); })));
    };
    /**
     * Restores a node previously moved to the trashcan.
     * @param nodeId ID of the node to restore
     * @returns Details of the restored node
     */
    /**
     * Restores a node previously moved to the trashcan.
     * @param {?} nodeId ID of the node to restore
     * @return {?} Details of the restored node
     */
    NodesApiService.prototype.restoreNode = /**
     * Restores a node previously moved to the trashcan.
     * @param {?} nodeId ID of the node to restore
     * @return {?} Details of the restored node
     */
    function (nodeId) {
        /** @type {?} */
        var promise = this.nodesApi
            .restoreNode(nodeId)
            .then(this.getEntryFromEntity);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return throwError(err); })));
    };
    NodesApiService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    NodesApiService.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: UserPreferencesService }
    ]; };
    /** @nocollapse */ NodesApiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function NodesApiService_Factory() { return new NodesApiService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.UserPreferencesService)); }, token: NodesApiService, providedIn: "root" });
    return NodesApiService;
}());
export { NodesApiService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    NodesApiService.prototype.api;
    /**
     * @type {?}
     * @private
     */
    NodesApiService.prototype.preferences;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm9kZXMtYXBpLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9ub2Rlcy1hcGkuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTNDLE9BQU8sRUFBYyxJQUFJLEVBQUUsVUFBVSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3BELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7OztBQUU1QztJQUtJLHlCQUNZLEdBQXVCLEVBQ3ZCLFdBQW1DO1FBRG5DLFFBQUcsR0FBSCxHQUFHLENBQW9CO1FBQ3ZCLGdCQUFXLEdBQVgsV0FBVyxDQUF3QjtJQUFHLENBQUM7SUFFbkQsc0JBQVkscUNBQVE7Ozs7O1FBQXBCO1lBQ0ksT0FBTyxJQUFJLENBQUMsR0FBRyxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUM7UUFDaEQsQ0FBQzs7O09BQUE7Ozs7OztJQUVPLDRDQUFrQjs7Ozs7SUFBMUIsVUFBMkIsTUFBaUI7UUFDeEMsT0FBTyxNQUFNLENBQUMsS0FBSyxDQUFDO0lBQ3hCLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILGlDQUFPOzs7Ozs7SUFBUCxVQUFRLE1BQWMsRUFBRSxPQUFpQjtRQUFqQix3QkFBQSxFQUFBLFlBQWlCOztZQUMvQixRQUFRLEdBQUc7WUFDYixPQUFPLEVBQUUsQ0FBRSxNQUFNLEVBQUUsWUFBWSxFQUFFLHFCQUFxQixFQUFFLGFBQWEsQ0FBRTtTQUMxRTs7WUFDSyxZQUFZLEdBQUcsTUFBTSxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsT0FBTyxDQUFDOztZQUMvQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVE7YUFDeEIsT0FBTyxDQUFDLE1BQU0sRUFBRSxZQUFZLENBQUM7YUFDN0IsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztRQUVsQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQ3JCLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBZixDQUFlLEVBQUMsQ0FDdkMsQ0FBQztJQUNOLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILHlDQUFlOzs7Ozs7SUFBZixVQUFnQixNQUFjLEVBQUUsT0FBaUI7UUFBakIsd0JBQUEsRUFBQSxZQUFpQjs7WUFDdkMsUUFBUSxHQUFHO1lBQ2IsUUFBUSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsY0FBYztZQUN6QyxTQUFTLEVBQUUsQ0FBQztZQUNaLE9BQU8sRUFBRSxDQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUscUJBQXFCLEVBQUUsYUFBYSxDQUFFO1NBQzFFOztZQUNLLFlBQVksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUM7O1lBQy9DLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUTthQUN4QixlQUFlLENBQUMsTUFBTSxFQUFFLFlBQVksQ0FBQztRQUUxQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQ3JCLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBZixDQUFlLEVBQUMsQ0FDdkMsQ0FBQztJQUNOLENBQUM7SUFFRDs7Ozs7O09BTUc7Ozs7Ozs7O0lBQ0gsb0NBQVU7Ozs7Ozs7SUFBVixVQUFXLFlBQW9CLEVBQUUsUUFBYSxFQUFFLE9BQWlCO1FBQWpCLHdCQUFBLEVBQUEsWUFBaUI7O1lBQ3ZELE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUTthQUN4QixPQUFPLENBQUMsWUFBWSxFQUFFLFFBQVEsRUFBRSxPQUFPLENBQUM7YUFDeEMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztRQUVsQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQ3JCLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBZixDQUFlLEVBQUMsQ0FDdkMsQ0FBQztJQUNOLENBQUM7SUFFRDs7Ozs7O09BTUc7Ozs7Ozs7O0lBQ0gsc0NBQVk7Ozs7Ozs7SUFBWixVQUFhLFlBQW9CLEVBQUUsUUFBYSxFQUFFLE9BQWlCO1FBQWpCLHdCQUFBLEVBQUEsWUFBaUI7O1lBQ3pELElBQUksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsUUFBUSxFQUFFLFdBQVcsRUFBRSxFQUFFLFFBQVEsQ0FBQztRQUMvRCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsWUFBWSxFQUFFLElBQUksRUFBRSxPQUFPLENBQUMsQ0FBQztJQUN4RCxDQUFDO0lBRUQ7Ozs7OztPQU1HOzs7Ozs7OztJQUNILG9DQUFVOzs7Ozs7O0lBQVYsVUFBVyxNQUFjLEVBQUUsUUFBYSxFQUFFLE9BQWlCO1FBQWpCLHdCQUFBLEVBQUEsWUFBaUI7O1lBQ2pELFFBQVEsR0FBRztZQUNiLE9BQU8sRUFBRSxDQUFFLE1BQU0sRUFBRSxZQUFZLEVBQUUscUJBQXFCLEVBQUUsYUFBYSxDQUFFO1NBQzFFOztZQUNLLFlBQVksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUM7O1lBRS9DLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUTthQUN4QixVQUFVLENBQUMsTUFBTSxFQUFFLFFBQVEsRUFBRSxZQUFZLENBQUM7YUFDMUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQztRQUVsQyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQ3JCLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBZixDQUFlLEVBQUMsQ0FDdkMsQ0FBQztJQUNOLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILG9DQUFVOzs7Ozs7SUFBVixVQUFXLE1BQWMsRUFBRSxPQUFpQjtRQUFqQix3QkFBQSxFQUFBLFlBQWlCOztZQUNsQyxPQUFPLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsTUFBTSxFQUFFLE9BQU8sQ0FBQztRQUV6RCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQ3JCLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLFVBQVUsQ0FBQyxHQUFHLENBQUMsRUFBZixDQUFlLEVBQUMsQ0FDdkMsQ0FBQztJQUNOLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCxxQ0FBVzs7Ozs7SUFBWCxVQUFZLE1BQWM7O1lBQ2hCLE9BQU8sR0FBRyxJQUFJLENBQUMsUUFBUTthQUN4QixXQUFXLENBQUMsTUFBTSxDQUFDO2FBQ25CLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUM7UUFFbEMsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUNyQixVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxVQUFVLENBQUMsR0FBRyxDQUFDLEVBQWYsQ0FBZSxFQUFDLENBQ3ZDLENBQUM7SUFDTixDQUFDOztnQkF4SUosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFOUSxrQkFBa0I7Z0JBQ2xCLHNCQUFzQjs7OzBCQXJCL0I7Q0FpS0MsQUF6SUQsSUF5SUM7U0F0SVksZUFBZTs7Ozs7O0lBR3BCLDhCQUErQjs7Ozs7SUFDL0Isc0NBQTJDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTm9kZUVudHJ5LCBNaW5pbWFsTm9kZSwgTm9kZVBhZ2luZyB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmcm9tLCB0aHJvd0Vycm9yIH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4vYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlIH0gZnJvbSAnLi91c2VyLXByZWZlcmVuY2VzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOb2Rlc0FwaVNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgYXBpOiBBbGZyZXNjb0FwaVNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBwcmVmZXJlbmNlczogVXNlclByZWZlcmVuY2VzU2VydmljZSkge31cclxuXHJcbiAgICBwcml2YXRlIGdldCBub2Rlc0FwaSgpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcGkuZ2V0SW5zdGFuY2UoKS5jb3JlLm5vZGVzQXBpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0RW50cnlGcm9tRW50aXR5KGVudGl0eTogTm9kZUVudHJ5KSB7XHJcbiAgICAgICAgcmV0dXJuIGVudGl0eS5lbnRyeTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIHN0b3JlZCBpbmZvcm1hdGlvbiBhYm91dCBhIG5vZGUuXHJcbiAgICAgKiBAcGFyYW0gbm9kZUlkIElEIG9mIHRoZSB0YXJnZXQgbm9kZVxyXG4gICAgICogQHBhcmFtIG9wdGlvbnMgT3B0aW9uYWwgcGFyYW1ldGVycyBzdXBwb3J0ZWQgYnkgSlMtQVBJXHJcbiAgICAgKiBAcmV0dXJucyBOb2RlIGluZm9ybWF0aW9uXHJcbiAgICAgKi9cclxuICAgIGdldE5vZGUobm9kZUlkOiBzdHJpbmcsIG9wdGlvbnM6IGFueSA9IHt9KTogT2JzZXJ2YWJsZTxNaW5pbWFsTm9kZT4ge1xyXG4gICAgICAgIGNvbnN0IGRlZmF1bHRzID0ge1xyXG4gICAgICAgICAgICBpbmNsdWRlOiBbICdwYXRoJywgJ3Byb3BlcnRpZXMnLCAnYWxsb3dhYmxlT3BlcmF0aW9ucycsICdwZXJtaXNzaW9ucycgXVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgY29uc3QgcXVlcnlPcHRpb25zID0gT2JqZWN0LmFzc2lnbihkZWZhdWx0cywgb3B0aW9ucyk7XHJcbiAgICAgICAgY29uc3QgcHJvbWlzZSA9IHRoaXMubm9kZXNBcGlcclxuICAgICAgICAgICAgLmdldE5vZGUobm9kZUlkLCBxdWVyeU9wdGlvbnMpXHJcbiAgICAgICAgICAgIC50aGVuKHRoaXMuZ2V0RW50cnlGcm9tRW50aXR5KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20ocHJvbWlzZSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aHJvd0Vycm9yKGVycikpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIGl0ZW1zIGNvbnRhaW5lZCBpbiBhIGZvbGRlciBub2RlLlxyXG4gICAgICogQHBhcmFtIG5vZGVJZCBJRCBvZiB0aGUgdGFyZ2V0IG5vZGVcclxuICAgICAqIEBwYXJhbSBvcHRpb25zIE9wdGlvbmFsIHBhcmFtZXRlcnMgc3VwcG9ydGVkIGJ5IEpTLUFQSVxyXG4gICAgICogQHJldHVybnMgTGlzdCBvZiBjaGlsZCBpdGVtcyBmcm9tIHRoZSBmb2xkZXJcclxuICAgICAqL1xyXG4gICAgZ2V0Tm9kZUNoaWxkcmVuKG5vZGVJZDogc3RyaW5nLCBvcHRpb25zOiBhbnkgPSB7fSk6IE9ic2VydmFibGU8Tm9kZVBhZ2luZz4ge1xyXG4gICAgICAgIGNvbnN0IGRlZmF1bHRzID0ge1xyXG4gICAgICAgICAgICBtYXhJdGVtczogdGhpcy5wcmVmZXJlbmNlcy5wYWdpbmF0aW9uU2l6ZSxcclxuICAgICAgICAgICAgc2tpcENvdW50OiAwLFxyXG4gICAgICAgICAgICBpbmNsdWRlOiBbICdwYXRoJywgJ3Byb3BlcnRpZXMnLCAnYWxsb3dhYmxlT3BlcmF0aW9ucycsICdwZXJtaXNzaW9ucycgXVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgY29uc3QgcXVlcnlPcHRpb25zID0gT2JqZWN0LmFzc2lnbihkZWZhdWx0cywgb3B0aW9ucyk7XHJcbiAgICAgICAgY29uc3QgcHJvbWlzZSA9IHRoaXMubm9kZXNBcGlcclxuICAgICAgICAgICAgLmdldE5vZGVDaGlsZHJlbihub2RlSWQsIHF1ZXJ5T3B0aW9ucyk7XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHByb21pc2UpLnBpcGUoXHJcbiAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhyb3dFcnJvcihlcnIpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDcmVhdGVzIGEgbmV3IGRvY3VtZW50IG5vZGUgaW5zaWRlIGEgZm9sZGVyLlxyXG4gICAgICogQHBhcmFtIHBhcmVudE5vZGVJZCBJRCBvZiB0aGUgcGFyZW50IGZvbGRlciBub2RlXHJcbiAgICAgKiBAcGFyYW0gbm9kZUJvZHkgRGF0YSBmb3IgdGhlIG5ldyBub2RlXHJcbiAgICAgKiBAcGFyYW0gb3B0aW9ucyBPcHRpb25hbCBwYXJhbWV0ZXJzIHN1cHBvcnRlZCBieSBKUy1BUElcclxuICAgICAqIEByZXR1cm5zIERldGFpbHMgb2YgdGhlIG5ldyBub2RlXHJcbiAgICAgKi9cclxuICAgIGNyZWF0ZU5vZGUocGFyZW50Tm9kZUlkOiBzdHJpbmcsIG5vZGVCb2R5OiBhbnksIG9wdGlvbnM6IGFueSA9IHt9KTogT2JzZXJ2YWJsZTxNaW5pbWFsTm9kZT4ge1xyXG4gICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLm5vZGVzQXBpXHJcbiAgICAgICAgICAgIC5hZGROb2RlKHBhcmVudE5vZGVJZCwgbm9kZUJvZHksIG9wdGlvbnMpXHJcbiAgICAgICAgICAgIC50aGVuKHRoaXMuZ2V0RW50cnlGcm9tRW50aXR5KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20ocHJvbWlzZSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aHJvd0Vycm9yKGVycikpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZXMgYSBuZXcgZm9sZGVyIG5vZGUgaW5zaWRlIGEgcGFyZW50IGZvbGRlci5cclxuICAgICAqIEBwYXJhbSBwYXJlbnROb2RlSWQgSUQgb2YgdGhlIHBhcmVudCBmb2xkZXIgbm9kZVxyXG4gICAgICogQHBhcmFtIG5vZGVCb2R5IERhdGEgZm9yIHRoZSBuZXcgZm9sZGVyXHJcbiAgICAgKiBAcGFyYW0gb3B0aW9ucyBPcHRpb25hbCBwYXJhbWV0ZXJzIHN1cHBvcnRlZCBieSBKUy1BUElcclxuICAgICAqIEByZXR1cm5zIERldGFpbHMgb2YgdGhlIG5ldyBmb2xkZXJcclxuICAgICAqL1xyXG4gICAgY3JlYXRlRm9sZGVyKHBhcmVudE5vZGVJZDogc3RyaW5nLCBub2RlQm9keTogYW55LCBvcHRpb25zOiBhbnkgPSB7fSk6IE9ic2VydmFibGU8TWluaW1hbE5vZGU+IHtcclxuICAgICAgICBjb25zdCBib2R5ID0gT2JqZWN0LmFzc2lnbih7IG5vZGVUeXBlOiAnY206Zm9sZGVyJyB9LCBub2RlQm9keSk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuY3JlYXRlTm9kZShwYXJlbnROb2RlSWQsIGJvZHksIG9wdGlvbnMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVXBkYXRlcyB0aGUgaW5mb3JtYXRpb24gYWJvdXQgYSBub2RlLlxyXG4gICAgICogQHBhcmFtIG5vZGVJZCBJRCBvZiB0aGUgdGFyZ2V0IG5vZGVcclxuICAgICAqIEBwYXJhbSBub2RlQm9keSBOZXcgZGF0YSBmb3IgdGhlIG5vZGVcclxuICAgICAqIEBwYXJhbSBvcHRpb25zIE9wdGlvbmFsIHBhcmFtZXRlcnMgc3VwcG9ydGVkIGJ5IEpTLUFQSVxyXG4gICAgICogQHJldHVybnMgVXBkYXRlZCBub2RlIGluZm9ybWF0aW9uXHJcbiAgICAgKi9cclxuICAgIHVwZGF0ZU5vZGUobm9kZUlkOiBzdHJpbmcsIG5vZGVCb2R5OiBhbnksIG9wdGlvbnM6IGFueSA9IHt9KTogT2JzZXJ2YWJsZTxNaW5pbWFsTm9kZT4ge1xyXG4gICAgICAgIGNvbnN0IGRlZmF1bHRzID0ge1xyXG4gICAgICAgICAgICBpbmNsdWRlOiBbICdwYXRoJywgJ3Byb3BlcnRpZXMnLCAnYWxsb3dhYmxlT3BlcmF0aW9ucycsICdwZXJtaXNzaW9ucycgXVxyXG4gICAgICAgIH07XHJcbiAgICAgICAgY29uc3QgcXVlcnlPcHRpb25zID0gT2JqZWN0LmFzc2lnbihkZWZhdWx0cywgb3B0aW9ucyk7XHJcblxyXG4gICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLm5vZGVzQXBpXHJcbiAgICAgICAgICAgIC51cGRhdGVOb2RlKG5vZGVJZCwgbm9kZUJvZHksIHF1ZXJ5T3B0aW9ucylcclxuICAgICAgICAgICAgLnRoZW4odGhpcy5nZXRFbnRyeUZyb21FbnRpdHkpO1xyXG5cclxuICAgICAgICByZXR1cm4gZnJvbShwcm9taXNlKS5waXBlKFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRocm93RXJyb3IoZXJyKSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogTW92ZXMgYSBub2RlIHRvIHRoZSB0cmFzaGNhbi5cclxuICAgICAqIEBwYXJhbSBub2RlSWQgSUQgb2YgdGhlIHRhcmdldCBub2RlXHJcbiAgICAgKiBAcGFyYW0gb3B0aW9ucyBPcHRpb25hbCBwYXJhbWV0ZXJzIHN1cHBvcnRlZCBieSBKUy1BUElcclxuICAgICAqIEByZXR1cm5zIEVtcHR5IHJlc3VsdCB0aGF0IG5vdGlmaWVzIHdoZW4gdGhlIGRlbGV0aW9uIGlzIGNvbXBsZXRlXHJcbiAgICAgKi9cclxuICAgIGRlbGV0ZU5vZGUobm9kZUlkOiBzdHJpbmcsIG9wdGlvbnM6IGFueSA9IHt9KTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5ub2Rlc0FwaS5kZWxldGVOb2RlKG5vZGVJZCwgb3B0aW9ucyk7XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHByb21pc2UpLnBpcGUoXHJcbiAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhyb3dFcnJvcihlcnIpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXN0b3JlcyBhIG5vZGUgcHJldmlvdXNseSBtb3ZlZCB0byB0aGUgdHJhc2hjYW4uXHJcbiAgICAgKiBAcGFyYW0gbm9kZUlkIElEIG9mIHRoZSBub2RlIHRvIHJlc3RvcmVcclxuICAgICAqIEByZXR1cm5zIERldGFpbHMgb2YgdGhlIHJlc3RvcmVkIG5vZGVcclxuICAgICAqL1xyXG4gICAgcmVzdG9yZU5vZGUobm9kZUlkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPE1pbmltYWxOb2RlPiB7XHJcbiAgICAgICAgY29uc3QgcHJvbWlzZSA9IHRoaXMubm9kZXNBcGlcclxuICAgICAgICAgICAgLnJlc3RvcmVOb2RlKG5vZGVJZClcclxuICAgICAgICAgICAgLnRoZW4odGhpcy5nZXRFbnRyeUZyb21FbnRpdHkpO1xyXG5cclxuICAgICAgICByZXR1cm4gZnJvbShwcm9taXNlKS5waXBlKFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRocm93RXJyb3IoZXJyKSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==