/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { TranslationService } from './translation.service';
import { AppConfigService, AppConfigValues } from '../app-config/app-config.service';
import { Subject } from 'rxjs';
import * as i0 from "@angular/core";
import * as i1 from "@angular/material/snack-bar";
import * as i2 from "./translation.service";
import * as i3 from "../app-config/app-config.service";
var NotificationService = /** @class */ (function () {
    function NotificationService(snackBar, translationService, appConfigService) {
        this.snackBar = snackBar;
        this.translationService = translationService;
        this.appConfigService = appConfigService;
        this.DEFAULT_DURATION_MESSAGE = 5000;
        this.messages = new Subject();
        this.DEFAULT_DURATION_MESSAGE = this.appConfigService.get(AppConfigValues.NOTIFY_DURATION) || this.DEFAULT_DURATION_MESSAGE;
    }
    /**
     * Opens a SnackBar notification to show a message.
     * @param message The message (or resource key) to show.
     * @param config Time before notification disappears after being shown or MatSnackBarConfig object
     * @returns Information/control object for the SnackBar
     */
    /**
     * Opens a SnackBar notification to show a message.
     * @param {?} message The message (or resource key) to show.
     * @param {?=} config Time before notification disappears after being shown or MatSnackBarConfig object
     * @return {?} Information/control object for the SnackBar
     */
    NotificationService.prototype.openSnackMessage = /**
     * Opens a SnackBar notification to show a message.
     * @param {?} message The message (or resource key) to show.
     * @param {?=} config Time before notification disappears after being shown or MatSnackBarConfig object
     * @return {?} Information/control object for the SnackBar
     */
    function (message, config) {
        if (!config) {
            config = this.DEFAULT_DURATION_MESSAGE;
        }
        /** @type {?} */
        var translatedMessage = this.translationService.instant(message);
        if (typeof config === 'number') {
            config = {
                duration: config
            };
        }
        this.messages.next({ message: translatedMessage, dateTime: new Date });
        return this.snackBar.open(translatedMessage, null, config);
    };
    /**
     * Opens a SnackBar notification with a message and a response button.
     * @param message The message (or resource key) to show.
     * @param action Caption for the response button
     * @param config Time before notification disappears after being shown or MatSnackBarConfig object
     * @returns Information/control object for the SnackBar
     */
    /**
     * Opens a SnackBar notification with a message and a response button.
     * @param {?} message The message (or resource key) to show.
     * @param {?} action Caption for the response button
     * @param {?=} config Time before notification disappears after being shown or MatSnackBarConfig object
     * @return {?} Information/control object for the SnackBar
     */
    NotificationService.prototype.openSnackMessageAction = /**
     * Opens a SnackBar notification with a message and a response button.
     * @param {?} message The message (or resource key) to show.
     * @param {?} action Caption for the response button
     * @param {?=} config Time before notification disappears after being shown or MatSnackBarConfig object
     * @return {?} Information/control object for the SnackBar
     */
    function (message, action, config) {
        if (!config) {
            config = this.DEFAULT_DURATION_MESSAGE;
        }
        /** @type {?} */
        var translatedMessage = this.translationService.instant(message);
        if (typeof config === 'number') {
            config = {
                duration: config
            };
        }
        this.messages.next({ message: translatedMessage, dateTime: new Date });
        return this.snackBar.open(translatedMessage, action, config);
    };
    /**
     *  dismiss the notification snackbar
     */
    /**
     *  dismiss the notification snackbar
     * @return {?}
     */
    NotificationService.prototype.dismissSnackMessageAction = /**
     *  dismiss the notification snackbar
     * @return {?}
     */
    function () {
        return this.snackBar.dismiss();
    };
    /**
     * @protected
     * @param {?} message
     * @param {?} panelClass
     * @param {?=} action
     * @return {?}
     */
    NotificationService.prototype.showMessage = /**
     * @protected
     * @param {?} message
     * @param {?} panelClass
     * @param {?=} action
     * @return {?}
     */
    function (message, panelClass, action) {
        message = this.translationService.instant(message);
        this.messages.next({ message: message, dateTime: new Date });
        return this.snackBar.open(message, action, {
            duration: this.DEFAULT_DURATION_MESSAGE,
            panelClass: panelClass
        });
    };
    /**
     * Rase error message
     * @param message Text message or translation key for the message.
     * @param action Action name
     */
    /**
     * Rase error message
     * @param {?} message Text message or translation key for the message.
     * @param {?=} action Action name
     * @return {?}
     */
    NotificationService.prototype.showError = /**
     * Rase error message
     * @param {?} message Text message or translation key for the message.
     * @param {?=} action Action name
     * @return {?}
     */
    function (message, action) {
        return this.showMessage(message, 'adf-error-snackbar', action);
    };
    /**
     * Rase info message
     * @param message Text message or translation key for the message.
     * @param action Action name
     */
    /**
     * Rase info message
     * @param {?} message Text message or translation key for the message.
     * @param {?=} action Action name
     * @return {?}
     */
    NotificationService.prototype.showInfo = /**
     * Rase info message
     * @param {?} message Text message or translation key for the message.
     * @param {?=} action Action name
     * @return {?}
     */
    function (message, action) {
        return this.showMessage(message, 'adf-info-snackbar', action);
    };
    /**
     * Rase warning message
     * @param message Text message or translation key for the message.
     * @param action Action name
     */
    /**
     * Rase warning message
     * @param {?} message Text message or translation key for the message.
     * @param {?=} action Action name
     * @return {?}
     */
    NotificationService.prototype.showWarning = /**
     * Rase warning message
     * @param {?} message Text message or translation key for the message.
     * @param {?=} action Action name
     * @return {?}
     */
    function (message, action) {
        return this.showMessage(message, 'adf-warning-snackbar', action);
    };
    NotificationService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    NotificationService.ctorParameters = function () { return [
        { type: MatSnackBar },
        { type: TranslationService },
        { type: AppConfigService }
    ]; };
    /** @nocollapse */ NotificationService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function NotificationService_Factory() { return new NotificationService(i0.ɵɵinject(i1.MatSnackBar), i0.ɵɵinject(i2.TranslationService), i0.ɵɵinject(i3.AppConfigService)); }, token: NotificationService, providedIn: "root" });
    return NotificationService;
}());
export { NotificationService };
if (false) {
    /** @type {?} */
    NotificationService.prototype.DEFAULT_DURATION_MESSAGE;
    /** @type {?} */
    NotificationService.prototype.messages;
    /**
     * @type {?}
     * @private
     */
    NotificationService.prototype.snackBar;
    /**
     * @type {?}
     * @private
     */
    NotificationService.prototype.translationService;
    /**
     * @type {?}
     * @private
     */
    NotificationService.prototype.appConfigService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibm90aWZpY2F0aW9uLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9ub3RpZmljYXRpb24uc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxXQUFXLEVBQXFDLE1BQU0sbUJBQW1CLENBQUM7QUFDbkYsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0sdUJBQXVCLENBQUM7QUFDM0QsT0FBTyxFQUFFLGdCQUFnQixFQUFFLGVBQWUsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3JGLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7Ozs7O0FBRy9CO0lBU0ksNkJBQW9CLFFBQXFCLEVBQ3JCLGtCQUFzQyxFQUN0QyxnQkFBa0M7UUFGbEMsYUFBUSxHQUFSLFFBQVEsQ0FBYTtRQUNyQix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7UUFOdEQsNkJBQXdCLEdBQVcsSUFBSSxDQUFDO1FBRXhDLGFBQVEsR0FBK0IsSUFBSSxPQUFPLEVBQUUsQ0FBQztRQUtqRCxJQUFJLENBQUMsd0JBQXdCLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBUyxlQUFlLENBQUMsZUFBZSxDQUFDLElBQUksSUFBSSxDQUFDLHdCQUF3QixDQUFDO0lBRXhJLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILDhDQUFnQjs7Ozs7O0lBQWhCLFVBQWlCLE9BQWUsRUFBRSxNQUFtQztRQUNqRSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ1QsTUFBTSxHQUFHLElBQUksQ0FBQyx3QkFBd0IsQ0FBQztTQUMxQzs7WUFFSyxpQkFBaUIsR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQztRQUVsRSxJQUFJLE9BQU8sTUFBTSxLQUFLLFFBQVEsRUFBRTtZQUM1QixNQUFNLEdBQUc7Z0JBQ0wsUUFBUSxFQUFFLE1BQU07YUFDbkIsQ0FBQztTQUNMO1FBRUQsSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRSxPQUFPLEVBQUUsaUJBQWlCLEVBQUUsUUFBUSxFQUFFLElBQUksSUFBSSxFQUFFLENBQUMsQ0FBQztRQUV2RSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLElBQUksRUFBRSxNQUFNLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRUQ7Ozs7OztPQU1HOzs7Ozs7OztJQUNILG9EQUFzQjs7Ozs7OztJQUF0QixVQUF1QixPQUFlLEVBQUUsTUFBYyxFQUFFLE1BQW1DO1FBQ3ZGLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDVCxNQUFNLEdBQUcsSUFBSSxDQUFDLHdCQUF3QixDQUFDO1NBQzFDOztZQUVLLGlCQUFpQixHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDO1FBRWxFLElBQUksT0FBTyxNQUFNLEtBQUssUUFBUSxFQUFFO1lBQzVCLE1BQU0sR0FBRztnQkFDTCxRQUFRLEVBQUUsTUFBTTthQUNuQixDQUFDO1NBQ0w7UUFFRCxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQyxFQUFFLE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxRQUFRLEVBQUUsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBRXZFLE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsaUJBQWlCLEVBQUUsTUFBTSxFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2pFLENBQUM7SUFFRDs7T0FFRzs7Ozs7SUFDSCx1REFBeUI7Ozs7SUFBekI7UUFDSSxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsT0FBTyxFQUFFLENBQUM7SUFDbkMsQ0FBQzs7Ozs7Ozs7SUFFUyx5Q0FBVzs7Ozs7OztJQUFyQixVQUFzQixPQUFlLEVBQUUsVUFBa0IsRUFBRSxNQUFlO1FBQ3RFLE9BQU8sR0FBRyxJQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBRW5ELElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUUsT0FBTyxFQUFFLE9BQU8sRUFBRSxRQUFRLEVBQUUsSUFBSSxJQUFJLEVBQUUsQ0FBQyxDQUFDO1FBRTdELE9BQU8sSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsT0FBTyxFQUFFLE1BQU0sRUFBRTtZQUN2QyxRQUFRLEVBQUUsSUFBSSxDQUFDLHdCQUF3QjtZQUN2QyxVQUFVLFlBQUE7U0FDYixDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7OztJQUNILHVDQUFTOzs7Ozs7SUFBVCxVQUFVLE9BQWUsRUFBRSxNQUFlO1FBQ3RDLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxPQUFPLEVBQUUsb0JBQW9CLEVBQUUsTUFBTSxDQUFDLENBQUM7SUFDbkUsQ0FBQztJQUVEOzs7O09BSUc7Ozs7Ozs7SUFDSCxzQ0FBUTs7Ozs7O0lBQVIsVUFBUyxPQUFlLEVBQUUsTUFBZTtRQUNyQyxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLG1CQUFtQixFQUFFLE1BQU0sQ0FBQyxDQUFDO0lBQ2xFLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gseUNBQVc7Ozs7OztJQUFYLFVBQVksT0FBZSxFQUFFLE1BQWU7UUFDeEMsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLENBQUMsQ0FBQztJQUNyRSxDQUFDOztnQkE1R0osVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFSUSxXQUFXO2dCQUNYLGtCQUFrQjtnQkFDbEIsZ0JBQWdCOzs7OEJBcEJ6QjtDQXFJQyxBQTdHRCxJQTZHQztTQTFHWSxtQkFBbUI7OztJQUU1Qix1REFBd0M7O0lBRXhDLHVDQUFxRDs7Ozs7SUFFekMsdUNBQTZCOzs7OztJQUM3QixpREFBOEM7Ozs7O0lBQzlDLCtDQUEwQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdFNuYWNrQmFyLCBNYXRTbmFja0JhclJlZiwgTWF0U25hY2tCYXJDb25maWcgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IFRyYW5zbGF0aW9uU2VydmljZSB9IGZyb20gJy4vdHJhbnNsYXRpb24uc2VydmljZSc7XHJcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UsIEFwcENvbmZpZ1ZhbHVlcyB9IGZyb20gJy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBOb3RpZmljYXRpb25Nb2RlbCB9IGZyb20gJy4uL21vZGVscy9ub3RpZmljYXRpb24ubW9kZWwnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBOb3RpZmljYXRpb25TZXJ2aWNlIHtcclxuXHJcbiAgICBERUZBVUxUX0RVUkFUSU9OX01FU1NBR0U6IG51bWJlciA9IDUwMDA7XHJcblxyXG4gICAgbWVzc2FnZXM6IFN1YmplY3Q8Tm90aWZpY2F0aW9uTW9kZWw+ID0gbmV3IFN1YmplY3QoKTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHNuYWNrQmFyOiBNYXRTbmFja0JhcixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgdHJhbnNsYXRpb25TZXJ2aWNlOiBUcmFuc2xhdGlvblNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGFwcENvbmZpZ1NlcnZpY2U6IEFwcENvbmZpZ1NlcnZpY2UpIHtcclxuICAgICAgICB0aGlzLkRFRkFVTFRfRFVSQVRJT05fTUVTU0FHRSA9IHRoaXMuYXBwQ29uZmlnU2VydmljZS5nZXQ8bnVtYmVyPihBcHBDb25maWdWYWx1ZXMuTk9USUZZX0RVUkFUSU9OKSB8fCB0aGlzLkRFRkFVTFRfRFVSQVRJT05fTUVTU0FHRTtcclxuXHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBPcGVucyBhIFNuYWNrQmFyIG5vdGlmaWNhdGlvbiB0byBzaG93IGEgbWVzc2FnZS5cclxuICAgICAqIEBwYXJhbSBtZXNzYWdlIFRoZSBtZXNzYWdlIChvciByZXNvdXJjZSBrZXkpIHRvIHNob3cuXHJcbiAgICAgKiBAcGFyYW0gY29uZmlnIFRpbWUgYmVmb3JlIG5vdGlmaWNhdGlvbiBkaXNhcHBlYXJzIGFmdGVyIGJlaW5nIHNob3duIG9yIE1hdFNuYWNrQmFyQ29uZmlnIG9iamVjdFxyXG4gICAgICogQHJldHVybnMgSW5mb3JtYXRpb24vY29udHJvbCBvYmplY3QgZm9yIHRoZSBTbmFja0JhclxyXG4gICAgICovXHJcbiAgICBvcGVuU25hY2tNZXNzYWdlKG1lc3NhZ2U6IHN0cmluZywgY29uZmlnPzogbnVtYmVyIHwgTWF0U25hY2tCYXJDb25maWcpOiBNYXRTbmFja0JhclJlZjxhbnk+IHtcclxuICAgICAgICBpZiAoIWNvbmZpZykge1xyXG4gICAgICAgICAgICBjb25maWcgPSB0aGlzLkRFRkFVTFRfRFVSQVRJT05fTUVTU0FHRTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHRyYW5zbGF0ZWRNZXNzYWdlID0gdGhpcy50cmFuc2xhdGlvblNlcnZpY2UuaW5zdGFudChtZXNzYWdlKTtcclxuXHJcbiAgICAgICAgaWYgKHR5cGVvZiBjb25maWcgPT09ICdudW1iZXInKSB7XHJcbiAgICAgICAgICAgIGNvbmZpZyA9IHtcclxuICAgICAgICAgICAgICAgIGR1cmF0aW9uOiBjb25maWdcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMubWVzc2FnZXMubmV4dCh7IG1lc3NhZ2U6IHRyYW5zbGF0ZWRNZXNzYWdlLCBkYXRlVGltZTogbmV3IERhdGUgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLnNuYWNrQmFyLm9wZW4odHJhbnNsYXRlZE1lc3NhZ2UsIG51bGwsIGNvbmZpZyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBPcGVucyBhIFNuYWNrQmFyIG5vdGlmaWNhdGlvbiB3aXRoIGEgbWVzc2FnZSBhbmQgYSByZXNwb25zZSBidXR0b24uXHJcbiAgICAgKiBAcGFyYW0gbWVzc2FnZSBUaGUgbWVzc2FnZSAob3IgcmVzb3VyY2Uga2V5KSB0byBzaG93LlxyXG4gICAgICogQHBhcmFtIGFjdGlvbiBDYXB0aW9uIGZvciB0aGUgcmVzcG9uc2UgYnV0dG9uXHJcbiAgICAgKiBAcGFyYW0gY29uZmlnIFRpbWUgYmVmb3JlIG5vdGlmaWNhdGlvbiBkaXNhcHBlYXJzIGFmdGVyIGJlaW5nIHNob3duIG9yIE1hdFNuYWNrQmFyQ29uZmlnIG9iamVjdFxyXG4gICAgICogQHJldHVybnMgSW5mb3JtYXRpb24vY29udHJvbCBvYmplY3QgZm9yIHRoZSBTbmFja0JhclxyXG4gICAgICovXHJcbiAgICBvcGVuU25hY2tNZXNzYWdlQWN0aW9uKG1lc3NhZ2U6IHN0cmluZywgYWN0aW9uOiBzdHJpbmcsIGNvbmZpZz86IG51bWJlciB8IE1hdFNuYWNrQmFyQ29uZmlnKTogTWF0U25hY2tCYXJSZWY8YW55PiB7XHJcbiAgICAgICAgaWYgKCFjb25maWcpIHtcclxuICAgICAgICAgICAgY29uZmlnID0gdGhpcy5ERUZBVUxUX0RVUkFUSU9OX01FU1NBR0U7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBjb25zdCB0cmFuc2xhdGVkTWVzc2FnZSA9IHRoaXMudHJhbnNsYXRpb25TZXJ2aWNlLmluc3RhbnQobWVzc2FnZSk7XHJcblxyXG4gICAgICAgIGlmICh0eXBlb2YgY29uZmlnID09PSAnbnVtYmVyJykge1xyXG4gICAgICAgICAgICBjb25maWcgPSB7XHJcbiAgICAgICAgICAgICAgICBkdXJhdGlvbjogY29uZmlnXHJcbiAgICAgICAgICAgIH07XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICB0aGlzLm1lc3NhZ2VzLm5leHQoeyBtZXNzYWdlOiB0cmFuc2xhdGVkTWVzc2FnZSwgZGF0ZVRpbWU6IG5ldyBEYXRlIH0pO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5zbmFja0Jhci5vcGVuKHRyYW5zbGF0ZWRNZXNzYWdlLCBhY3Rpb24sIGNvbmZpZyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiAgZGlzbWlzcyB0aGUgbm90aWZpY2F0aW9uIHNuYWNrYmFyXHJcbiAgICAgKi9cclxuICAgIGRpc21pc3NTbmFja01lc3NhZ2VBY3Rpb24oKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc25hY2tCYXIuZGlzbWlzcygpO1xyXG4gICAgfVxyXG5cclxuICAgIHByb3RlY3RlZCBzaG93TWVzc2FnZShtZXNzYWdlOiBzdHJpbmcsIHBhbmVsQ2xhc3M6IHN0cmluZywgYWN0aW9uPzogc3RyaW5nKTogTWF0U25hY2tCYXJSZWY8YW55PiB7XHJcbiAgICAgICAgbWVzc2FnZSA9IHRoaXMudHJhbnNsYXRpb25TZXJ2aWNlLmluc3RhbnQobWVzc2FnZSk7XHJcblxyXG4gICAgICAgIHRoaXMubWVzc2FnZXMubmV4dCh7IG1lc3NhZ2U6IG1lc3NhZ2UsIGRhdGVUaW1lOiBuZXcgRGF0ZSB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc25hY2tCYXIub3BlbihtZXNzYWdlLCBhY3Rpb24sIHtcclxuICAgICAgICAgICAgZHVyYXRpb246IHRoaXMuREVGQVVMVF9EVVJBVElPTl9NRVNTQUdFLFxyXG4gICAgICAgICAgICBwYW5lbENsYXNzXHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSYXNlIGVycm9yIG1lc3NhZ2VcclxuICAgICAqIEBwYXJhbSBtZXNzYWdlIFRleHQgbWVzc2FnZSBvciB0cmFuc2xhdGlvbiBrZXkgZm9yIHRoZSBtZXNzYWdlLlxyXG4gICAgICogQHBhcmFtIGFjdGlvbiBBY3Rpb24gbmFtZVxyXG4gICAgICovXHJcbiAgICBzaG93RXJyb3IobWVzc2FnZTogc3RyaW5nLCBhY3Rpb24/OiBzdHJpbmcpOiBNYXRTbmFja0JhclJlZjxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zaG93TWVzc2FnZShtZXNzYWdlLCAnYWRmLWVycm9yLXNuYWNrYmFyJywgYWN0aW9uKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJhc2UgaW5mbyBtZXNzYWdlXHJcbiAgICAgKiBAcGFyYW0gbWVzc2FnZSBUZXh0IG1lc3NhZ2Ugb3IgdHJhbnNsYXRpb24ga2V5IGZvciB0aGUgbWVzc2FnZS5cclxuICAgICAqIEBwYXJhbSBhY3Rpb24gQWN0aW9uIG5hbWVcclxuICAgICAqL1xyXG4gICAgc2hvd0luZm8obWVzc2FnZTogc3RyaW5nLCBhY3Rpb24/OiBzdHJpbmcpOiBNYXRTbmFja0JhclJlZjxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5zaG93TWVzc2FnZShtZXNzYWdlLCAnYWRmLWluZm8tc25hY2tiYXInLCBhY3Rpb24pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmFzZSB3YXJuaW5nIG1lc3NhZ2VcclxuICAgICAqIEBwYXJhbSBtZXNzYWdlIFRleHQgbWVzc2FnZSBvciB0cmFuc2xhdGlvbiBrZXkgZm9yIHRoZSBtZXNzYWdlLlxyXG4gICAgICogQHBhcmFtIGFjdGlvbiBBY3Rpb24gbmFtZVxyXG4gICAgICovXHJcbiAgICBzaG93V2FybmluZyhtZXNzYWdlOiBzdHJpbmcsIGFjdGlvbj86IHN0cmluZyk6IE1hdFNuYWNrQmFyUmVmPGFueT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNob3dNZXNzYWdlKG1lc3NhZ2UsICdhZGYtd2FybmluZy1zbmFja2JhcicsIGFjdGlvbik7XHJcbiAgICB9XHJcbn1cclxuIl19