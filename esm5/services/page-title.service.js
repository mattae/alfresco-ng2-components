/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AppConfigService } from '../app-config/app-config.service';
import { TranslationService } from './translation.service';
import * as i0 from "@angular/core";
import * as i1 from "@angular/platform-browser";
import * as i2 from "../app-config/app-config.service";
import * as i3 from "./translation.service";
var PageTitleService = /** @class */ (function () {
    function PageTitleService(titleService, appConfig, translationService) {
        var _this = this;
        this.titleService = titleService;
        this.appConfig = appConfig;
        this.translationService = translationService;
        this.originalTitle = '';
        this.translatedTitle = '';
        translationService.translate.onLangChange.subscribe((/**
         * @return {?}
         */
        function () { return _this.onLanguageChanged(); }));
        translationService.translate.onTranslationChange.subscribe((/**
         * @return {?}
         */
        function () { return _this.onLanguageChanged(); }));
    }
    /**
     * Sets the page title.
     * @param value The new title
     */
    /**
     * Sets the page title.
     * @param {?=} value The new title
     * @return {?}
     */
    PageTitleService.prototype.setTitle = /**
     * Sets the page title.
     * @param {?=} value The new title
     * @return {?}
     */
    function (value) {
        if (value === void 0) { value = ''; }
        this.originalTitle = value;
        this.translatedTitle = this.translationService.instant(value);
        this.updateTitle();
    };
    /**
     * @private
     * @return {?}
     */
    PageTitleService.prototype.onLanguageChanged = /**
     * @private
     * @return {?}
     */
    function () {
        this.translatedTitle = this.translationService.instant(this.originalTitle);
        this.updateTitle();
    };
    /**
     * @private
     * @return {?}
     */
    PageTitleService.prototype.updateTitle = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var name = this.appConfig.get('application.name') || 'Alfresco ADF Application';
        /** @type {?} */
        var title = this.translatedTitle ? this.translatedTitle + " - " + name : "" + name;
        this.titleService.setTitle(title);
    };
    PageTitleService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    PageTitleService.ctorParameters = function () { return [
        { type: Title },
        { type: AppConfigService },
        { type: TranslationService }
    ]; };
    /** @nocollapse */ PageTitleService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function PageTitleService_Factory() { return new PageTitleService(i0.ɵɵinject(i1.Title), i0.ɵɵinject(i2.AppConfigService), i0.ɵɵinject(i3.TranslationService)); }, token: PageTitleService, providedIn: "root" });
    return PageTitleService;
}());
export { PageTitleService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    PageTitleService.prototype.originalTitle;
    /**
     * @type {?}
     * @private
     */
    PageTitleService.prototype.translatedTitle;
    /**
     * @type {?}
     * @private
     */
    PageTitleService.prototype.titleService;
    /**
     * @type {?}
     * @private
     */
    PageTitleService.prototype.appConfig;
    /**
     * @type {?}
     * @private
     */
    PageTitleService.prototype.translationService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGFnZS10aXRsZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvcGFnZS10aXRsZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBQ3BFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHVCQUF1QixDQUFDOzs7OztBQUUzRDtJQVFJLDBCQUNZLFlBQW1CLEVBQ25CLFNBQTJCLEVBQzNCLGtCQUFzQztRQUhsRCxpQkFNQztRQUxXLGlCQUFZLEdBQVosWUFBWSxDQUFPO1FBQ25CLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBQzNCLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFOMUMsa0JBQWEsR0FBVyxFQUFFLENBQUM7UUFDM0Isb0JBQWUsR0FBVyxFQUFFLENBQUM7UUFNakMsa0JBQWtCLENBQUMsU0FBUyxDQUFDLFlBQVksQ0FBQyxTQUFTOzs7UUFBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLGlCQUFpQixFQUFFLEVBQXhCLENBQXdCLEVBQUMsQ0FBQztRQUNwRixrQkFBa0IsQ0FBQyxTQUFTLENBQUMsbUJBQW1CLENBQUMsU0FBUzs7O1FBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxFQUF4QixDQUF3QixFQUFDLENBQUM7SUFDL0YsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7O0lBQ0gsbUNBQVE7Ozs7O0lBQVIsVUFBUyxLQUFrQjtRQUFsQixzQkFBQSxFQUFBLFVBQWtCO1FBQ3ZCLElBQUksQ0FBQyxhQUFhLEdBQUcsS0FBSyxDQUFDO1FBQzNCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUU5RCxJQUFJLENBQUMsV0FBVyxFQUFFLENBQUM7SUFDdkIsQ0FBQzs7Ozs7SUFFTyw0Q0FBaUI7Ozs7SUFBekI7UUFDSSxJQUFJLENBQUMsZUFBZSxHQUFHLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzNFLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUN2QixDQUFDOzs7OztJQUVPLHNDQUFXOzs7O0lBQW5COztZQUNVLElBQUksR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLDBCQUEwQjs7WUFFM0UsS0FBSyxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsQ0FBQyxDQUFJLElBQUksQ0FBQyxlQUFlLFdBQU0sSUFBTSxDQUFDLENBQUMsQ0FBQyxLQUFHLElBQU07UUFDcEYsSUFBSSxDQUFDLFlBQVksQ0FBQyxRQUFRLENBQUMsS0FBSyxDQUFDLENBQUM7SUFDdEMsQ0FBQzs7Z0JBckNKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7Z0JBTlEsS0FBSztnQkFDTCxnQkFBZ0I7Z0JBQ2hCLGtCQUFrQjs7OzJCQXBCM0I7Q0E0REMsQUF0Q0QsSUFzQ0M7U0FuQ1ksZ0JBQWdCOzs7Ozs7SUFFekIseUNBQW1DOzs7OztJQUNuQywyQ0FBcUM7Ozs7O0lBR2pDLHdDQUEyQjs7Ozs7SUFDM0IscUNBQW1DOzs7OztJQUNuQyw4Q0FBOEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBUaXRsZSB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xyXG5pbXBvcnQgeyBBcHBDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vYXBwLWNvbmZpZy9hcHAtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGlvblNlcnZpY2UgfSBmcm9tICcuL3RyYW5zbGF0aW9uLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBQYWdlVGl0bGVTZXJ2aWNlIHtcclxuXHJcbiAgICBwcml2YXRlIG9yaWdpbmFsVGl0bGU6IHN0cmluZyA9ICcnO1xyXG4gICAgcHJpdmF0ZSB0cmFuc2xhdGVkVGl0bGU6IHN0cmluZyA9ICcnO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgdGl0bGVTZXJ2aWNlOiBUaXRsZSxcclxuICAgICAgICBwcml2YXRlIGFwcENvbmZpZzogQXBwQ29uZmlnU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIHRyYW5zbGF0aW9uU2VydmljZTogVHJhbnNsYXRpb25TZXJ2aWNlKSB7XHJcbiAgICAgICAgdHJhbnNsYXRpb25TZXJ2aWNlLnRyYW5zbGF0ZS5vbkxhbmdDaGFuZ2Uuc3Vic2NyaWJlKCgpID0+IHRoaXMub25MYW5ndWFnZUNoYW5nZWQoKSk7XHJcbiAgICAgICAgdHJhbnNsYXRpb25TZXJ2aWNlLnRyYW5zbGF0ZS5vblRyYW5zbGF0aW9uQ2hhbmdlLnN1YnNjcmliZSgoKSA9PiB0aGlzLm9uTGFuZ3VhZ2VDaGFuZ2VkKCkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0cyB0aGUgcGFnZSB0aXRsZS5cclxuICAgICAqIEBwYXJhbSB2YWx1ZSBUaGUgbmV3IHRpdGxlXHJcbiAgICAgKi9cclxuICAgIHNldFRpdGxlKHZhbHVlOiBzdHJpbmcgPSAnJykge1xyXG4gICAgICAgIHRoaXMub3JpZ2luYWxUaXRsZSA9IHZhbHVlO1xyXG4gICAgICAgIHRoaXMudHJhbnNsYXRlZFRpdGxlID0gdGhpcy50cmFuc2xhdGlvblNlcnZpY2UuaW5zdGFudCh2YWx1ZSk7XHJcblxyXG4gICAgICAgIHRoaXMudXBkYXRlVGl0bGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG9uTGFuZ3VhZ2VDaGFuZ2VkKCkge1xyXG4gICAgICAgIHRoaXMudHJhbnNsYXRlZFRpdGxlID0gdGhpcy50cmFuc2xhdGlvblNlcnZpY2UuaW5zdGFudCh0aGlzLm9yaWdpbmFsVGl0bGUpO1xyXG4gICAgICAgIHRoaXMudXBkYXRlVGl0bGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHVwZGF0ZVRpdGxlKCkge1xyXG4gICAgICAgIGNvbnN0IG5hbWUgPSB0aGlzLmFwcENvbmZpZy5nZXQoJ2FwcGxpY2F0aW9uLm5hbWUnKSB8fCAnQWxmcmVzY28gQURGIEFwcGxpY2F0aW9uJztcclxuXHJcbiAgICAgICAgY29uc3QgdGl0bGUgPSB0aGlzLnRyYW5zbGF0ZWRUaXRsZSA/IGAke3RoaXMudHJhbnNsYXRlZFRpdGxlfSAtICR7bmFtZX1gIDogYCR7bmFtZX1gO1xyXG4gICAgICAgIHRoaXMudGl0bGVTZXJ2aWNlLnNldFRpdGxlKHRpdGxlKTtcclxuICAgIH1cclxufVxyXG4iXX0=