/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, of } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
var PeopleContentService = /** @class */ (function () {
    function PeopleContentService(apiService) {
        this.apiService = apiService;
    }
    Object.defineProperty(PeopleContentService.prototype, "peopleApi", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.apiService.getInstance().core.peopleApi;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Gets information about a user identified by their username.
     * @param personId ID of the target user
     * @returns User information
     */
    /**
     * Gets information about a user identified by their username.
     * @param {?} personId ID of the target user
     * @return {?} User information
     */
    PeopleContentService.prototype.getPerson = /**
     * Gets information about a user identified by their username.
     * @param {?} personId ID of the target user
     * @return {?} User information
     */
    function (personId) {
        /** @type {?} */
        var promise = this.peopleApi.getPerson(personId);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return of(err); })));
    };
    /**
     * Gets information about the user who is currently logged in.
     * @returns User information
     */
    /**
     * Gets information about the user who is currently logged in.
     * @return {?} User information
     */
    PeopleContentService.prototype.getCurrentPerson = /**
     * Gets information about the user who is currently logged in.
     * @return {?} User information
     */
    function () {
        return this.getPerson('-me-');
    };
    PeopleContentService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    PeopleContentService.ctorParameters = function () { return [
        { type: AlfrescoApiService }
    ]; };
    /** @nocollapse */ PeopleContentService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function PeopleContentService_Factory() { return new PeopleContentService(i0.ɵɵinject(i1.AlfrescoApiService)); }, token: PeopleContentService, providedIn: "root" });
    return PeopleContentService;
}());
export { PeopleContentService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    PeopleContentService.prototype.apiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVvcGxlLWNvbnRlbnQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL3Blb3BsZS1jb250ZW50LnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQWMsSUFBSSxFQUFFLEVBQUUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUM1QyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7OztBQUU1QztJQUtJLDhCQUFvQixVQUE4QjtRQUE5QixlQUFVLEdBQVYsVUFBVSxDQUFvQjtJQUFHLENBQUM7SUFFdEQsc0JBQVksMkNBQVM7Ozs7O1FBQXJCO1lBQ0csT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUM7UUFDdkQsQ0FBQzs7O09BQUE7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCx3Q0FBUzs7Ozs7SUFBVCxVQUFVLFFBQWdCOztZQUNoQixPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDO1FBRWxELE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FDckIsVUFBVTs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFQLENBQU8sRUFBQyxDQUMvQixDQUFDO0lBQ04sQ0FBQztJQUVEOzs7T0FHRzs7Ozs7SUFDSCwrQ0FBZ0I7Ozs7SUFBaEI7UUFDSSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDbEMsQ0FBQzs7Z0JBOUJKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7Z0JBTFEsa0JBQWtCOzs7K0JBbkIzQjtDQXFEQyxBQS9CRCxJQStCQztTQTVCWSxvQkFBb0I7Ozs7OztJQUVqQiwwQ0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmcm9tLCBvZiB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgY2F0Y2hFcnJvciB9IGZyb20gJ3J4anMvb3BlcmF0b3JzJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgUGVvcGxlQ29udGVudFNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlKSB7fVxyXG5cclxuICAgIHByaXZhdGUgZ2V0IHBlb3BsZUFwaSgpIHtcclxuICAgICAgIHJldHVybiB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5jb3JlLnBlb3BsZUFwaTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgaW5mb3JtYXRpb24gYWJvdXQgYSB1c2VyIGlkZW50aWZpZWQgYnkgdGhlaXIgdXNlcm5hbWUuXHJcbiAgICAgKiBAcGFyYW0gcGVyc29uSWQgSUQgb2YgdGhlIHRhcmdldCB1c2VyXHJcbiAgICAgKiBAcmV0dXJucyBVc2VyIGluZm9ybWF0aW9uXHJcbiAgICAgKi9cclxuICAgIGdldFBlcnNvbihwZXJzb25JZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5wZW9wbGVBcGkuZ2V0UGVyc29uKHBlcnNvbklkKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20ocHJvbWlzZSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiBvZihlcnIpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGluZm9ybWF0aW9uIGFib3V0IHRoZSB1c2VyIHdobyBpcyBjdXJyZW50bHkgbG9nZ2VkIGluLlxyXG4gICAgICogQHJldHVybnMgVXNlciBpbmZvcm1hdGlvblxyXG4gICAgICovXHJcbiAgICBnZXRDdXJyZW50UGVyc29uKCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0UGVyc29uKCctbWUtJyk7XHJcbiAgICB9XHJcbn1cclxuIl19