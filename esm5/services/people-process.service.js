/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { LogService } from './log.service';
import { catchError, map } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./log.service";
var PeopleProcessService = /** @class */ (function () {
    function PeopleProcessService(alfrescoJsApi, logService) {
        this.alfrescoJsApi = alfrescoJsApi;
        this.logService = logService;
    }
    /**
     * Gets information about users across all tasks.
     * @param taskId ID of the task
     * @param searchWord Filter text to search for
     * @returns Array of user information objects
     */
    /**
     * Gets information about users across all tasks.
     * @param {?=} taskId ID of the task
     * @param {?=} searchWord Filter text to search for
     * @return {?} Array of user information objects
     */
    PeopleProcessService.prototype.getWorkflowUsers = /**
     * Gets information about users across all tasks.
     * @param {?=} taskId ID of the task
     * @param {?=} searchWord Filter text to search for
     * @return {?} Array of user information objects
     */
    function (taskId, searchWord) {
        var _this = this;
        /** @type {?} */
        var option = { excludeTaskId: taskId, filter: searchWord };
        return from(this.getWorkflowUserApi(option))
            .pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) { return (/** @type {?} */ (response.data)) || []; })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets the profile picture URL for the specified user.
     * @param user The target user
     * @returns Profile picture URL
     */
    /**
     * Gets the profile picture URL for the specified user.
     * @param {?} user The target user
     * @return {?} Profile picture URL
     */
    PeopleProcessService.prototype.getUserImage = /**
     * Gets the profile picture URL for the specified user.
     * @param {?} user The target user
     * @return {?} Profile picture URL
     */
    function (user) {
        return this.getUserProfileImageApi(user.id);
    };
    /**
     * Sets a user to be involved with a task.
     * @param taskId ID of the target task
     * @param idToInvolve ID of the user to involve
     * @returns Empty response when the update completes
     */
    /**
     * Sets a user to be involved with a task.
     * @param {?} taskId ID of the target task
     * @param {?} idToInvolve ID of the user to involve
     * @return {?} Empty response when the update completes
     */
    PeopleProcessService.prototype.involveUserWithTask = /**
     * Sets a user to be involved with a task.
     * @param {?} taskId ID of the target task
     * @param {?} idToInvolve ID of the user to involve
     * @return {?} Empty response when the update completes
     */
    function (taskId, idToInvolve) {
        var _this = this;
        /** @type {?} */
        var node = { userId: idToInvolve };
        return from(this.involveUserToTaskApi(taskId, node))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Removes a user who is currently involved with a task.
     * @param taskId ID of the target task
     * @param idToRemove ID of the user to remove
     * @returns Empty response when the update completes
     */
    /**
     * Removes a user who is currently involved with a task.
     * @param {?} taskId ID of the target task
     * @param {?} idToRemove ID of the user to remove
     * @return {?} Empty response when the update completes
     */
    PeopleProcessService.prototype.removeInvolvedUser = /**
     * Removes a user who is currently involved with a task.
     * @param {?} taskId ID of the target task
     * @param {?} idToRemove ID of the user to remove
     * @return {?} Empty response when the update completes
     */
    function (taskId, idToRemove) {
        var _this = this;
        /** @type {?} */
        var node = { userId: idToRemove };
        return from(this.removeInvolvedUserFromTaskApi(taskId, node))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * @private
     * @param {?} options
     * @return {?}
     */
    PeopleProcessService.prototype.getWorkflowUserApi = /**
     * @private
     * @param {?} options
     * @return {?}
     */
    function (options) {
        return this.alfrescoJsApi.getInstance().activiti.usersWorkflowApi.getUsers(options);
    };
    /**
     * @private
     * @param {?} taskId
     * @param {?} node
     * @return {?}
     */
    PeopleProcessService.prototype.involveUserToTaskApi = /**
     * @private
     * @param {?} taskId
     * @param {?} node
     * @return {?}
     */
    function (taskId, node) {
        return this.alfrescoJsApi.getInstance().activiti.taskActionsApi.involveUser(taskId, node);
    };
    /**
     * @private
     * @param {?} taskId
     * @param {?} node
     * @return {?}
     */
    PeopleProcessService.prototype.removeInvolvedUserFromTaskApi = /**
     * @private
     * @param {?} taskId
     * @param {?} node
     * @return {?}
     */
    function (taskId, node) {
        return this.alfrescoJsApi.getInstance().activiti.taskActionsApi.removeInvolvedUser(taskId, node);
    };
    /**
     * @private
     * @param {?} userId
     * @return {?}
     */
    PeopleProcessService.prototype.getUserProfileImageApi = /**
     * @private
     * @param {?} userId
     * @return {?}
     */
    function (userId) {
        return this.alfrescoJsApi.getInstance().activiti.userApi.getUserProfilePictureUrl(userId);
    };
    /**
     * Throw the error
     * @param error
     */
    /**
     * Throw the error
     * @private
     * @param {?} error
     * @return {?}
     */
    PeopleProcessService.prototype.handleError = /**
     * Throw the error
     * @private
     * @param {?} error
     * @return {?}
     */
    function (error) {
        this.logService.error(error);
        return throwError(error || 'Server error');
    };
    PeopleProcessService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    PeopleProcessService.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: LogService }
    ]; };
    /** @nocollapse */ PeopleProcessService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function PeopleProcessService_Factory() { return new PeopleProcessService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: PeopleProcessService, providedIn: "root" });
    return PeopleProcessService;
}());
export { PeopleProcessService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    PeopleProcessService.prototype.alfrescoJsApi;
    /**
     * @type {?}
     * @private
     */
    PeopleProcessService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGVvcGxlLXByb2Nlc3Muc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL3Blb3BsZS1wcm9jZXNzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQWMsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUVwRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBRSxVQUFVLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7QUFFakQ7SUFLSSw4QkFBb0IsYUFBaUMsRUFDakMsVUFBc0I7UUFEdEIsa0JBQWEsR0FBYixhQUFhLENBQW9CO1FBQ2pDLGVBQVUsR0FBVixVQUFVLENBQVk7SUFDMUMsQ0FBQztJQUVEOzs7OztPQUtHOzs7Ozs7O0lBQ0gsK0NBQWdCOzs7Ozs7SUFBaEIsVUFBaUIsTUFBZSxFQUFFLFVBQW1CO1FBQXJELGlCQU9DOztZQU5TLE1BQU0sR0FBRyxFQUFFLGFBQWEsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFLFVBQVUsRUFBRTtRQUM1RCxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsTUFBTSxDQUFDLENBQUM7YUFDdkMsSUFBSSxDQUNELEdBQUc7Ozs7UUFBQyxVQUFDLFFBQWEsSUFBSyxPQUFBLG1CQUFxQixRQUFRLENBQUMsSUFBSSxFQUFBLElBQUksRUFBRSxFQUF4QyxDQUF3QyxFQUFDLEVBQ2hFLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCwyQ0FBWTs7Ozs7SUFBWixVQUFhLElBQXNCO1FBQy9CLE9BQU8sSUFBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUNoRCxDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCxrREFBbUI7Ozs7OztJQUFuQixVQUFvQixNQUFjLEVBQUUsV0FBbUI7UUFBdkQsaUJBTUM7O1lBTFMsSUFBSSxHQUFHLEVBQUMsTUFBTSxFQUFFLFdBQVcsRUFBQztRQUNsQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQy9DLElBQUksQ0FDRCxVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCxpREFBa0I7Ozs7OztJQUFsQixVQUFtQixNQUFjLEVBQUUsVUFBa0I7UUFBckQsaUJBTUM7O1lBTFMsSUFBSSxHQUFHLEVBQUMsTUFBTSxFQUFFLFVBQVUsRUFBQztRQUNqQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsNkJBQTZCLENBQUMsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQ3hELElBQUksQ0FDRCxVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxFQUFyQixDQUFxQixFQUFDLENBQzdDLENBQUM7SUFDVixDQUFDOzs7Ozs7SUFFTyxpREFBa0I7Ozs7O0lBQTFCLFVBQTJCLE9BQVk7UUFDbkMsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLENBQUM7SUFDeEYsQ0FBQzs7Ozs7OztJQUVPLG1EQUFvQjs7Ozs7O0lBQTVCLFVBQTZCLE1BQWMsRUFBRSxJQUFTO1FBQ2xELE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLFdBQVcsQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7SUFDOUYsQ0FBQzs7Ozs7OztJQUVPLDREQUE2Qjs7Ozs7O0lBQXJDLFVBQXNDLE1BQWMsRUFBRSxJQUFTO1FBQzNELE9BQU8sSUFBSSxDQUFDLGFBQWEsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxRQUFRLENBQUMsY0FBYyxDQUFDLGtCQUFrQixDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsQ0FBQztJQUNyRyxDQUFDOzs7Ozs7SUFFTyxxREFBc0I7Ozs7O0lBQTlCLFVBQStCLE1BQWM7UUFDekMsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxPQUFPLENBQUMsd0JBQXdCLENBQUMsTUFBTSxDQUFDLENBQUM7SUFDOUYsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7OztJQUNLLDBDQUFXOzs7Ozs7SUFBbkIsVUFBb0IsS0FBVTtRQUMxQixJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixPQUFPLFVBQVUsQ0FBQyxLQUFLLElBQUksY0FBYyxDQUFDLENBQUM7SUFDL0MsQ0FBQzs7Z0JBcEZKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7Z0JBTlEsa0JBQWtCO2dCQUNsQixVQUFVOzs7K0JBckJuQjtDQTZHQyxBQXJGRCxJQXFGQztTQWxGWSxvQkFBb0I7Ozs7OztJQUVqQiw2Q0FBeUM7Ozs7O0lBQ3pDLDBDQUE4QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZyb20sIHRocm93RXJyb3IgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgVXNlclByb2Nlc3NNb2RlbCB9IGZyb20gJy4uL21vZGVscy91c2VyLXByb2Nlc3MubW9kZWwnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4vbG9nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBjYXRjaEVycm9yLCBtYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFBlb3BsZVByb2Nlc3NTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFsZnJlc2NvSnNBcGk6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgbG9nU2VydmljZTogTG9nU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBpbmZvcm1hdGlvbiBhYm91dCB1c2VycyBhY3Jvc3MgYWxsIHRhc2tzLlxyXG4gICAgICogQHBhcmFtIHRhc2tJZCBJRCBvZiB0aGUgdGFza1xyXG4gICAgICogQHBhcmFtIHNlYXJjaFdvcmQgRmlsdGVyIHRleHQgdG8gc2VhcmNoIGZvclxyXG4gICAgICogQHJldHVybnMgQXJyYXkgb2YgdXNlciBpbmZvcm1hdGlvbiBvYmplY3RzXHJcbiAgICAgKi9cclxuICAgIGdldFdvcmtmbG93VXNlcnModGFza0lkPzogc3RyaW5nLCBzZWFyY2hXb3JkPzogc3RyaW5nKTogT2JzZXJ2YWJsZTxVc2VyUHJvY2Vzc01vZGVsW10+IHtcclxuICAgICAgICBjb25zdCBvcHRpb24gPSB7IGV4Y2x1ZGVUYXNrSWQ6IHRhc2tJZCwgZmlsdGVyOiBzZWFyY2hXb3JkIH07XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5nZXRXb3JrZmxvd1VzZXJBcGkob3B0aW9uKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAoKHJlc3BvbnNlOiBhbnkpID0+IDxVc2VyUHJvY2Vzc01vZGVsW10+IHJlc3BvbnNlLmRhdGEgfHwgW10pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBwcm9maWxlIHBpY3R1cmUgVVJMIGZvciB0aGUgc3BlY2lmaWVkIHVzZXIuXHJcbiAgICAgKiBAcGFyYW0gdXNlciBUaGUgdGFyZ2V0IHVzZXJcclxuICAgICAqIEByZXR1cm5zIFByb2ZpbGUgcGljdHVyZSBVUkxcclxuICAgICAqL1xyXG4gICAgZ2V0VXNlckltYWdlKHVzZXI6IFVzZXJQcm9jZXNzTW9kZWwpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldFVzZXJQcm9maWxlSW1hZ2VBcGkodXNlci5pZCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXRzIGEgdXNlciB0byBiZSBpbnZvbHZlZCB3aXRoIGEgdGFzay5cclxuICAgICAqIEBwYXJhbSB0YXNrSWQgSUQgb2YgdGhlIHRhcmdldCB0YXNrXHJcbiAgICAgKiBAcGFyYW0gaWRUb0ludm9sdmUgSUQgb2YgdGhlIHVzZXIgdG8gaW52b2x2ZVxyXG4gICAgICogQHJldHVybnMgRW1wdHkgcmVzcG9uc2Ugd2hlbiB0aGUgdXBkYXRlIGNvbXBsZXRlc1xyXG4gICAgICovXHJcbiAgICBpbnZvbHZlVXNlcldpdGhUYXNrKHRhc2tJZDogc3RyaW5nLCBpZFRvSW52b2x2ZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxVc2VyUHJvY2Vzc01vZGVsW10+IHtcclxuICAgICAgICBjb25zdCBub2RlID0ge3VzZXJJZDogaWRUb0ludm9sdmV9O1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuaW52b2x2ZVVzZXJUb1Rhc2tBcGkodGFza0lkLCBub2RlKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFJlbW92ZXMgYSB1c2VyIHdobyBpcyBjdXJyZW50bHkgaW52b2x2ZWQgd2l0aCBhIHRhc2suXHJcbiAgICAgKiBAcGFyYW0gdGFza0lkIElEIG9mIHRoZSB0YXJnZXQgdGFza1xyXG4gICAgICogQHBhcmFtIGlkVG9SZW1vdmUgSUQgb2YgdGhlIHVzZXIgdG8gcmVtb3ZlXHJcbiAgICAgKiBAcmV0dXJucyBFbXB0eSByZXNwb25zZSB3aGVuIHRoZSB1cGRhdGUgY29tcGxldGVzXHJcbiAgICAgKi9cclxuICAgIHJlbW92ZUludm9sdmVkVXNlcih0YXNrSWQ6IHN0cmluZywgaWRUb1JlbW92ZTogc3RyaW5nKTogT2JzZXJ2YWJsZTxVc2VyUHJvY2Vzc01vZGVsW10+IHtcclxuICAgICAgICBjb25zdCBub2RlID0ge3VzZXJJZDogaWRUb1JlbW92ZX07XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5yZW1vdmVJbnZvbHZlZFVzZXJGcm9tVGFza0FwaSh0YXNrSWQsIG5vZGUpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0V29ya2Zsb3dVc2VyQXBpKG9wdGlvbnM6IGFueSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFsZnJlc2NvSnNBcGkuZ2V0SW5zdGFuY2UoKS5hY3Rpdml0aS51c2Vyc1dvcmtmbG93QXBpLmdldFVzZXJzKG9wdGlvbnMpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaW52b2x2ZVVzZXJUb1Rhc2tBcGkodGFza0lkOiBzdHJpbmcsIG5vZGU6IGFueSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFsZnJlc2NvSnNBcGkuZ2V0SW5zdGFuY2UoKS5hY3Rpdml0aS50YXNrQWN0aW9uc0FwaS5pbnZvbHZlVXNlcih0YXNrSWQsIG5vZGUpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgcmVtb3ZlSW52b2x2ZWRVc2VyRnJvbVRhc2tBcGkodGFza0lkOiBzdHJpbmcsIG5vZGU6IGFueSkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFsZnJlc2NvSnNBcGkuZ2V0SW5zdGFuY2UoKS5hY3Rpdml0aS50YXNrQWN0aW9uc0FwaS5yZW1vdmVJbnZvbHZlZFVzZXIodGFza0lkLCBub2RlKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldFVzZXJQcm9maWxlSW1hZ2VBcGkodXNlcklkOiBudW1iZXIpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hbGZyZXNjb0pzQXBpLmdldEluc3RhbmNlKCkuYWN0aXZpdGkudXNlckFwaS5nZXRVc2VyUHJvZmlsZVBpY3R1cmVVcmwodXNlcklkKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFRocm93IHRoZSBlcnJvclxyXG4gICAgICogQHBhcmFtIGVycm9yXHJcbiAgICAgKi9cclxuICAgIHByaXZhdGUgaGFuZGxlRXJyb3IoZXJyb3I6IGFueSkge1xyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IgfHwgJ1NlcnZlciBlcnJvcicpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==