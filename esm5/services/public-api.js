/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export { AuthGuardBase } from './auth-guard-base';
export { AuthenticationService } from './authentication.service';
export { AlfrescoApiService } from './alfresco-api.service';
export { ContentService } from './content.service';
export { AuthGuard } from './auth-guard.service';
export { AuthGuardEcm } from './auth-guard-ecm.service';
export { AuthGuardBpm } from './auth-guard-bpm.service';
export { AuthGuardSsoRoleService } from './auth-guard-sso-role.service';
export { AppsProcessService } from './apps-process.service';
export { PageTitleService } from './page-title.service';
export { StorageService } from './storage.service';
export { CookieService } from './cookie.service';
export { RenditionsService } from './renditions.service';
export { NotificationService } from './notification.service';
export { LogService } from './log.service';
export { TRANSLATION_PROVIDER, TranslationService } from './translation.service';
export { TranslateLoaderService } from './translate-loader.service';
export { ThumbnailService } from './thumbnail.service';
export { UploadService } from './upload.service';
export { DynamicComponentResolver, DynamicComponentMapper } from './dynamic-component-mapper.service';
export { UserPreferenceValues, UserPreferencesService } from './user-preferences.service';
export { HighlightTransformService } from './highlight-transform.service';
export { DeletedNodesApiService } from './deleted-nodes-api.service';
export { FavoritesApiService } from './favorites-api.service';
export { NodesApiService } from './nodes-api.service';
export { PeopleContentService } from './people-content.service';
export { PeopleProcessService } from './people-process.service';
export { SearchService } from './search.service';
export { SharedLinksApiService } from './shared-links-api.service';
export { SitesService } from './sites.service';
export { DiscoveryApiService } from './discovery-api.service';
export { CommentProcessService } from './comment-process.service';
export { SearchConfigurationService } from './search-configuration.service';
export { CommentContentService } from './comment-content.service';
export { LoginDialogService } from './login-dialog.service';
export { ExternalAlfrescoApiService } from './external-alfresco-api.service';
export { JwtHelperService } from './jwt-helper.service';
export { DownloadZipService } from './download-zip.service';
export { LockService } from './lock.service';
export { CoreAutomationService } from './automation.service';
export {} from './automation.service';
export { DownloadService } from './download.service';
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicHVibGljLWFwaS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL3B1YmxpYy1hcGkudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsOEJBQWMsbUJBQW1CLENBQUM7QUFDbEMsc0NBQWMsMEJBQTBCLENBQUM7QUFDekMsbUNBQWMsd0JBQXdCLENBQUM7QUFDdkMsK0JBQWMsbUJBQW1CLENBQUM7QUFDbEMsMEJBQWMsc0JBQXNCLENBQUM7QUFDckMsNkJBQWMsMEJBQTBCLENBQUM7QUFDekMsNkJBQWMsMEJBQTBCLENBQUM7QUFDekMsd0NBQWMsK0JBQStCLENBQUM7QUFDOUMsbUNBQWMsd0JBQXdCLENBQUM7QUFDdkMsaUNBQWMsc0JBQXNCLENBQUM7QUFDckMsK0JBQWMsbUJBQW1CLENBQUM7QUFDbEMsOEJBQWMsa0JBQWtCLENBQUM7QUFDakMsa0NBQWMsc0JBQXNCLENBQUM7QUFDckMsb0NBQWMsd0JBQXdCLENBQUM7QUFDdkMsMkJBQWMsZUFBZSxDQUFDO0FBQzlCLHlEQUFjLHVCQUF1QixDQUFDO0FBQ3RDLHVDQUFjLDRCQUE0QixDQUFDO0FBQzNDLGlDQUFjLHFCQUFxQixDQUFDO0FBQ3BDLDhCQUFjLGtCQUFrQixDQUFDO0FBQ2pDLGlFQUFjLG9DQUFvQyxDQUFDO0FBQ25ELDZEQUFjLDRCQUE0QixDQUFDO0FBQzNDLDBDQUFjLCtCQUErQixDQUFDO0FBQzlDLHVDQUFjLDZCQUE2QixDQUFDO0FBQzVDLG9DQUFjLHlCQUF5QixDQUFDO0FBQ3hDLGdDQUFjLHFCQUFxQixDQUFDO0FBQ3BDLHFDQUFjLDBCQUEwQixDQUFDO0FBQ3pDLHFDQUFjLDBCQUEwQixDQUFDO0FBQ3pDLDhCQUFjLGtCQUFrQixDQUFDO0FBQ2pDLHNDQUFjLDRCQUE0QixDQUFDO0FBQzNDLDZCQUFjLGlCQUFpQixDQUFDO0FBQ2hDLG9DQUFjLHlCQUF5QixDQUFDO0FBQ3hDLHNDQUFjLDJCQUEyQixDQUFDO0FBQzFDLDJDQUFjLGdDQUFnQyxDQUFDO0FBQy9DLHNDQUFjLDJCQUEyQixDQUFDO0FBQzFDLG1DQUFjLHdCQUF3QixDQUFDO0FBQ3ZDLDJDQUFjLGlDQUFpQyxDQUFDO0FBQ2hELGlDQUFjLHNCQUFzQixDQUFDO0FBQ3JDLG1DQUFjLHdCQUF3QixDQUFDO0FBQ3ZDLDRCQUFjLGdCQUFnQixDQUFDO0FBQy9CLHNDQUFjLHNCQUFzQixDQUFDO0FBQ3JDLGVBQWMsc0JBQXNCLENBQUM7QUFDckMsZ0NBQWMsb0JBQW9CLENBQUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuZXhwb3J0ICogZnJvbSAnLi9hdXRoLWd1YXJkLWJhc2UnO1xyXG5leHBvcnQgKiBmcm9tICcuL2F1dGhlbnRpY2F0aW9uLnNlcnZpY2UnO1xyXG5leHBvcnQgKiBmcm9tICcuL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuZXhwb3J0ICogZnJvbSAnLi9jb250ZW50LnNlcnZpY2UnO1xyXG5leHBvcnQgKiBmcm9tICcuL2F1dGgtZ3VhcmQuc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vYXV0aC1ndWFyZC1lY20uc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vYXV0aC1ndWFyZC1icG0uc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vYXV0aC1ndWFyZC1zc28tcm9sZS5zZXJ2aWNlJztcclxuZXhwb3J0ICogZnJvbSAnLi9hcHBzLXByb2Nlc3Muc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vcGFnZS10aXRsZS5zZXJ2aWNlJztcclxuZXhwb3J0ICogZnJvbSAnLi9zdG9yYWdlLnNlcnZpY2UnO1xyXG5leHBvcnQgKiBmcm9tICcuL2Nvb2tpZS5zZXJ2aWNlJztcclxuZXhwb3J0ICogZnJvbSAnLi9yZW5kaXRpb25zLnNlcnZpY2UnO1xyXG5leHBvcnQgKiBmcm9tICcuL25vdGlmaWNhdGlvbi5zZXJ2aWNlJztcclxuZXhwb3J0ICogZnJvbSAnLi9sb2cuc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vdHJhbnNsYXRpb24uc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vdHJhbnNsYXRlLWxvYWRlci5zZXJ2aWNlJztcclxuZXhwb3J0ICogZnJvbSAnLi90aHVtYm5haWwuc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vdXBsb2FkLnNlcnZpY2UnO1xyXG5leHBvcnQgKiBmcm9tICcuL2R5bmFtaWMtY29tcG9uZW50LW1hcHBlci5zZXJ2aWNlJztcclxuZXhwb3J0ICogZnJvbSAnLi91c2VyLXByZWZlcmVuY2VzLnNlcnZpY2UnO1xyXG5leHBvcnQgKiBmcm9tICcuL2hpZ2hsaWdodC10cmFuc2Zvcm0uc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vZGVsZXRlZC1ub2Rlcy1hcGkuc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vZmF2b3JpdGVzLWFwaS5zZXJ2aWNlJztcclxuZXhwb3J0ICogZnJvbSAnLi9ub2Rlcy1hcGkuc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vcGVvcGxlLWNvbnRlbnQuc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vcGVvcGxlLXByb2Nlc3Muc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vc2VhcmNoLnNlcnZpY2UnO1xyXG5leHBvcnQgKiBmcm9tICcuL3NoYXJlZC1saW5rcy1hcGkuc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vc2l0ZXMuc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vZGlzY292ZXJ5LWFwaS5zZXJ2aWNlJztcclxuZXhwb3J0ICogZnJvbSAnLi9jb21tZW50LXByb2Nlc3Muc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vc2VhcmNoLWNvbmZpZ3VyYXRpb24uc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vY29tbWVudC1jb250ZW50LnNlcnZpY2UnO1xyXG5leHBvcnQgKiBmcm9tICcuL2xvZ2luLWRpYWxvZy5zZXJ2aWNlJztcclxuZXhwb3J0ICogZnJvbSAnLi9leHRlcm5hbC1hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vand0LWhlbHBlci5zZXJ2aWNlJztcclxuZXhwb3J0ICogZnJvbSAnLi9kb3dubG9hZC16aXAuc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vbG9jay5zZXJ2aWNlJztcclxuZXhwb3J0ICogZnJvbSAnLi9hdXRvbWF0aW9uLnNlcnZpY2UnO1xyXG5leHBvcnQgKiBmcm9tICcuL2F1dG9tYXRpb24uc2VydmljZSc7XHJcbmV4cG9ydCAqIGZyb20gJy4vZG93bmxvYWQuc2VydmljZSc7XHJcbiJdfQ==