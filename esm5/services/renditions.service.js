/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { Observable, from, interval, empty } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { concatMap, switchMap, takeWhile, map } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
var RenditionsService = /** @class */ (function () {
    function RenditionsService(apiService) {
        this.apiService = apiService;
    }
    /**
     * Gets the first available rendition found for a node.
     * @param nodeId ID of the target node
     * @returns Information object for the rendition
     */
    /**
     * Gets the first available rendition found for a node.
     * @param {?} nodeId ID of the target node
     * @return {?} Information object for the rendition
     */
    RenditionsService.prototype.getAvailableRenditionForNode = /**
     * Gets the first available rendition found for a node.
     * @param {?} nodeId ID of the target node
     * @return {?} Information object for the rendition
     */
    function (nodeId) {
        return from(this.apiService.renditionsApi.getRenditions(nodeId)).pipe(map((/**
         * @param {?} availableRenditions
         * @return {?}
         */
        function (availableRenditions) {
            /** @type {?} */
            var renditionsAvailable = availableRenditions.list.entries.filter((/**
             * @param {?} rendition
             * @return {?}
             */
            function (rendition) { return (rendition.entry.id === 'pdf' || rendition.entry.id === 'imgpreview'); }));
            /** @type {?} */
            var existingRendition = renditionsAvailable.find((/**
             * @param {?} rend
             * @return {?}
             */
            function (rend) { return rend.entry.status === 'CREATED'; }));
            return existingRendition ? existingRendition : renditionsAvailable[0];
        })));
    };
    /**
     * Generates a rendition for a node using the first available encoding.
     * @param nodeId ID of the target node
     * @returns Null response to indicate completion
     */
    /**
     * Generates a rendition for a node using the first available encoding.
     * @param {?} nodeId ID of the target node
     * @return {?} Null response to indicate completion
     */
    RenditionsService.prototype.generateRenditionForNode = /**
     * Generates a rendition for a node using the first available encoding.
     * @param {?} nodeId ID of the target node
     * @return {?} Null response to indicate completion
     */
    function (nodeId) {
        var _this = this;
        return this.getAvailableRenditionForNode(nodeId).pipe(map((/**
         * @param {?} rendition
         * @return {?}
         */
        function (rendition) {
            if (rendition.entry.status !== 'CREATED') {
                return from(_this.apiService.renditionsApi.createRendition(nodeId, { id: rendition.entry.id }));
            }
            else {
                return empty();
            }
        })));
    };
    /**
     * Checks if the specified rendition is available for a node.
     * @param nodeId ID of the target node
     * @param encoding Name of the rendition encoding
     * @returns True if the rendition is available, false otherwise
     */
    /**
     * Checks if the specified rendition is available for a node.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @return {?} True if the rendition is available, false otherwise
     */
    RenditionsService.prototype.isRenditionAvailable = /**
     * Checks if the specified rendition is available for a node.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @return {?} True if the rendition is available, false otherwise
     */
    function (nodeId, encoding) {
        var _this = this;
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        function (observer) {
            _this.getRendition(nodeId, encoding).subscribe((/**
             * @param {?} res
             * @return {?}
             */
            function (res) {
                /** @type {?} */
                var isAvailable = true;
                if (res.entry.status.toString() === 'NOT_CREATED') {
                    isAvailable = false;
                }
                observer.next(isAvailable);
                observer.complete();
            }), (/**
             * @return {?}
             */
            function () {
                observer.next(false);
                observer.complete();
            }));
        }));
    };
    /**
     * Checks if the node can be converted using the specified rendition.
     * @param nodeId ID of the target node
     * @param encoding Name of the rendition encoding
     * @returns True if the node can be converted, false otherwise
     */
    /**
     * Checks if the node can be converted using the specified rendition.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @return {?} True if the node can be converted, false otherwise
     */
    RenditionsService.prototype.isConversionPossible = /**
     * Checks if the node can be converted using the specified rendition.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @return {?} True if the node can be converted, false otherwise
     */
    function (nodeId, encoding) {
        var _this = this;
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        function (observer) {
            _this.getRendition(nodeId, encoding).subscribe((/**
             * @return {?}
             */
            function () {
                observer.next(true);
                observer.complete();
            }), (/**
             * @return {?}
             */
            function () {
                observer.next(false);
                observer.complete();
            }));
        }));
    };
    /**
     * Gets a URL linking to the specified rendition of a node.
     * @param nodeId ID of the target node
     * @param encoding Name of the rendition encoding
     * @returns URL string
     */
    /**
     * Gets a URL linking to the specified rendition of a node.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @return {?} URL string
     */
    RenditionsService.prototype.getRenditionUrl = /**
     * Gets a URL linking to the specified rendition of a node.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @return {?} URL string
     */
    function (nodeId, encoding) {
        return this.apiService.contentApi.getRenditionUrl(nodeId, encoding);
    };
    /**
     * Gets information about a rendition of a node.
     * @param nodeId ID of the target node
     * @param encoding Name of the rendition encoding
     * @returns Information object about the rendition
     */
    /**
     * Gets information about a rendition of a node.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @return {?} Information object about the rendition
     */
    RenditionsService.prototype.getRendition = /**
     * Gets information about a rendition of a node.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @return {?} Information object about the rendition
     */
    function (nodeId, encoding) {
        return from(this.apiService.renditionsApi.getRendition(nodeId, encoding));
    };
    /**
     * Gets a list of all renditions for a node.
     * @param nodeId ID of the target node
     * @returns Paged list of rendition details
     */
    /**
     * Gets a list of all renditions for a node.
     * @param {?} nodeId ID of the target node
     * @return {?} Paged list of rendition details
     */
    RenditionsService.prototype.getRenditionsListByNodeId = /**
     * Gets a list of all renditions for a node.
     * @param {?} nodeId ID of the target node
     * @return {?} Paged list of rendition details
     */
    function (nodeId) {
        return from(this.apiService.renditionsApi.getRenditions(nodeId));
    };
    /**
     * Creates a rendition for a node.
     * @param nodeId ID of the target node
     * @param encoding Name of the rendition encoding
     * @returns Null response to indicate completion
     */
    /**
     * Creates a rendition for a node.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @return {?} Null response to indicate completion
     */
    RenditionsService.prototype.createRendition = /**
     * Creates a rendition for a node.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @return {?} Null response to indicate completion
     */
    function (nodeId, encoding) {
        return from(this.apiService.renditionsApi.createRendition(nodeId, { id: encoding }));
    };
    /**
     * Repeatedly attempts to create a rendition, through to success or failure.
     * @param nodeId ID of the target node
     * @param encoding Name of the rendition encoding
     * @param pollingInterval Time interval (in milliseconds) between checks for completion
     * @param retries Number of attempts to make before declaring failure
     * @returns True if the rendition was created, false otherwise
     */
    /**
     * Repeatedly attempts to create a rendition, through to success or failure.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @param {?=} pollingInterval Time interval (in milliseconds) between checks for completion
     * @param {?=} retries Number of attempts to make before declaring failure
     * @return {?} True if the rendition was created, false otherwise
     */
    RenditionsService.prototype.convert = /**
     * Repeatedly attempts to create a rendition, through to success or failure.
     * @param {?} nodeId ID of the target node
     * @param {?} encoding Name of the rendition encoding
     * @param {?=} pollingInterval Time interval (in milliseconds) between checks for completion
     * @param {?=} retries Number of attempts to make before declaring failure
     * @return {?} True if the rendition was created, false otherwise
     */
    function (nodeId, encoding, pollingInterval, retries) {
        var _this = this;
        if (pollingInterval === void 0) { pollingInterval = 1000; }
        if (retries === void 0) { retries = 5; }
        return this.createRendition(nodeId, encoding)
            .pipe(concatMap((/**
         * @return {?}
         */
        function () { return _this.pollRendition(nodeId, encoding, pollingInterval, retries); })));
    };
    /**
     * @private
     * @param {?} nodeId
     * @param {?} encoding
     * @param {?=} intervalSize
     * @param {?=} retries
     * @return {?}
     */
    RenditionsService.prototype.pollRendition = /**
     * @private
     * @param {?} nodeId
     * @param {?} encoding
     * @param {?=} intervalSize
     * @param {?=} retries
     * @return {?}
     */
    function (nodeId, encoding, intervalSize, retries) {
        var _this = this;
        if (intervalSize === void 0) { intervalSize = 1000; }
        if (retries === void 0) { retries = 5; }
        /** @type {?} */
        var attempts = 0;
        return interval(intervalSize)
            .pipe(switchMap((/**
         * @return {?}
         */
        function () { return _this.getRendition(nodeId, encoding); })), takeWhile((/**
         * @param {?} renditionEntry
         * @return {?}
         */
        function (renditionEntry) {
            attempts += 1;
            if (attempts > retries) {
                return false;
            }
            return (renditionEntry.entry.status.toString() !== 'CREATED');
        })));
    };
    RenditionsService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    RenditionsService.ctorParameters = function () { return [
        { type: AlfrescoApiService }
    ]; };
    /** @nocollapse */ RenditionsService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function RenditionsService_Factory() { return new RenditionsService(i0.ɵɵinject(i1.AlfrescoApiService)); }, token: RenditionsService, providedIn: "root" });
    return RenditionsService;
}());
export { RenditionsService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    RenditionsService.prototype.apiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVuZGl0aW9ucy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvcmVuZGl0aW9ucy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFFLFVBQVUsRUFBRSxJQUFJLEVBQUUsUUFBUSxFQUFFLEtBQUssRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN6RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsR0FBRyxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7OztBQUV0RTtJQUtJLDJCQUFvQixVQUE4QjtRQUE5QixlQUFVLEdBQVYsVUFBVSxDQUFvQjtJQUNsRCxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsd0RBQTRCOzs7OztJQUE1QixVQUE2QixNQUFjO1FBQ3ZDLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FDakUsR0FBRzs7OztRQUFDLFVBQUMsbUJBQW9DOztnQkFDL0IsbUJBQW1CLEdBQXFCLG1CQUFtQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTTs7OztZQUNqRixVQUFDLFNBQVMsSUFBSyxPQUFBLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFLEtBQUssS0FBSyxJQUFJLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRSxLQUFLLFlBQVksQ0FBQyxFQUFyRSxDQUFxRSxFQUFDOztnQkFDbkYsaUJBQWlCLEdBQUcsbUJBQW1CLENBQUMsSUFBSTs7OztZQUFDLFVBQUMsSUFBSSxJQUFLLE9BQUEsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEtBQUssU0FBUyxFQUEvQixDQUErQixFQUFDO1lBQzdGLE9BQU8saUJBQWlCLENBQUMsQ0FBQyxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMxRSxDQUFDLEVBQUMsQ0FBQyxDQUFDO0lBQ1osQ0FBQztJQUVEOzs7O09BSUc7Ozs7OztJQUNILG9EQUF3Qjs7Ozs7SUFBeEIsVUFBeUIsTUFBYztRQUF2QyxpQkFVQztRQVRHLE9BQU8sSUFBSSxDQUFDLDRCQUE0QixDQUFDLE1BQU0sQ0FBQyxDQUFDLElBQUksQ0FDakQsR0FBRzs7OztRQUFDLFVBQUMsU0FBeUI7WUFDMUIsSUFBSSxTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sS0FBSyxTQUFTLEVBQUU7Z0JBQ3RDLE9BQU8sSUFBSSxDQUFDLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsRUFBRSxFQUFFLEVBQUUsU0FBUyxDQUFDLEtBQUssQ0FBQyxFQUFFLEVBQUUsQ0FBQyxDQUFDLENBQUM7YUFDbEc7aUJBQU07Z0JBQ0gsT0FBTyxLQUFLLEVBQUUsQ0FBQzthQUNsQjtRQUNMLENBQUMsRUFBQyxDQUNMLENBQUM7SUFDTixDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCxnREFBb0I7Ozs7OztJQUFwQixVQUFxQixNQUFjLEVBQUUsUUFBZ0I7UUFBckQsaUJBaUJDO1FBaEJHLE9BQU8sSUFBSSxVQUFVOzs7O1FBQUMsVUFBQyxRQUFRO1lBQzNCLEtBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDLFNBQVM7Ozs7WUFDekMsVUFBQyxHQUFHOztvQkFDSSxXQUFXLEdBQUcsSUFBSTtnQkFDdEIsSUFBSSxHQUFHLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUUsS0FBSyxhQUFhLEVBQUU7b0JBQy9DLFdBQVcsR0FBRyxLQUFLLENBQUM7aUJBQ3ZCO2dCQUNELFFBQVEsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7Z0JBQzNCLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN4QixDQUFDOzs7WUFDRDtnQkFDSSxRQUFRLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNyQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQyxFQUNKLENBQUM7UUFDTixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILGdEQUFvQjs7Ozs7O0lBQXBCLFVBQXFCLE1BQWMsRUFBRSxRQUFnQjtRQUFyRCxpQkFhQztRQVpHLE9BQU8sSUFBSSxVQUFVOzs7O1FBQUMsVUFBQyxRQUFRO1lBQzNCLEtBQUksQ0FBQyxZQUFZLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDLFNBQVM7OztZQUN6QztnQkFDSSxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO2dCQUNwQixRQUFRLENBQUMsUUFBUSxFQUFFLENBQUM7WUFDeEIsQ0FBQzs7O1lBQ0Q7Z0JBQ0ksUUFBUSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztnQkFDckIsUUFBUSxDQUFDLFFBQVEsRUFBRSxDQUFDO1lBQ3hCLENBQUMsRUFDSixDQUFDO1FBQ04sQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCwyQ0FBZTs7Ozs7O0lBQWYsVUFBZ0IsTUFBYyxFQUFFLFFBQWdCO1FBQzVDLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxVQUFVLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQztJQUN4RSxDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCx3Q0FBWTs7Ozs7O0lBQVosVUFBYSxNQUFjLEVBQUUsUUFBZ0I7UUFDekMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsWUFBWSxDQUFDLE1BQU0sRUFBRSxRQUFRLENBQUMsQ0FBQyxDQUFDO0lBQzlFLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCxxREFBeUI7Ozs7O0lBQXpCLFVBQTBCLE1BQWM7UUFDcEMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUM7SUFDckUsQ0FBQztJQUVEOzs7OztPQUtHOzs7Ozs7O0lBQ0gsMkNBQWU7Ozs7OztJQUFmLFVBQWdCLE1BQWMsRUFBRSxRQUFnQjtRQUM1QyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLEVBQUUsRUFBRSxFQUFFLFFBQVEsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUN6RixDQUFDO0lBRUQ7Ozs7Ozs7T0FPRzs7Ozs7Ozs7O0lBQ0gsbUNBQU87Ozs7Ozs7O0lBQVAsVUFBUSxNQUFjLEVBQUUsUUFBZ0IsRUFBRSxlQUE4QixFQUFFLE9BQW1CO1FBQTdGLGlCQUtDO1FBTHlDLGdDQUFBLEVBQUEsc0JBQThCO1FBQUUsd0JBQUEsRUFBQSxXQUFtQjtRQUN6RixPQUFPLElBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQzthQUN4QyxJQUFJLENBQ0QsU0FBUzs7O1FBQUMsY0FBTSxPQUFBLEtBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLFFBQVEsRUFBRSxlQUFlLEVBQUUsT0FBTyxDQUFDLEVBQTlELENBQThELEVBQUMsQ0FDbEYsQ0FBQztJQUNWLENBQUM7Ozs7Ozs7OztJQUVPLHlDQUFhOzs7Ozs7OztJQUFyQixVQUFzQixNQUFjLEVBQUUsUUFBZ0IsRUFBRSxZQUEyQixFQUFFLE9BQW1CO1FBQXhHLGlCQWFDO1FBYnVELDZCQUFBLEVBQUEsbUJBQTJCO1FBQUUsd0JBQUEsRUFBQSxXQUFtQjs7WUFDaEcsUUFBUSxHQUFHLENBQUM7UUFDaEIsT0FBTyxRQUFRLENBQUMsWUFBWSxDQUFDO2FBQ3hCLElBQUksQ0FDRCxTQUFTOzs7UUFBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLEVBQW5DLENBQW1DLEVBQUMsRUFDcEQsU0FBUzs7OztRQUFDLFVBQUMsY0FBOEI7WUFDckMsUUFBUSxJQUFJLENBQUMsQ0FBQztZQUNkLElBQUksUUFBUSxHQUFHLE9BQU8sRUFBRTtnQkFDcEIsT0FBTyxLQUFLLENBQUM7YUFDaEI7WUFDRCxPQUFPLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLEtBQUssU0FBUyxDQUFDLENBQUM7UUFDbEUsQ0FBQyxFQUFDLENBQ0wsQ0FBQztJQUNWLENBQUM7O2dCQXpKSixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7O2dCQUxRLGtCQUFrQjs7OzRCQXBCM0I7Q0FpTEMsQUExSkQsSUEwSkM7U0F2SlksaUJBQWlCOzs7Ozs7SUFFZCx1Q0FBc0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSZW5kaXRpb25FbnRyeSwgUmVuZGl0aW9uUGFnaW5nIH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZyb20sIGludGVydmFsLCBlbXB0eSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgY29uY2F0TWFwLCBzd2l0Y2hNYXAsIHRha2VXaGlsZSwgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBSZW5kaXRpb25zU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIGZpcnN0IGF2YWlsYWJsZSByZW5kaXRpb24gZm91bmQgZm9yIGEgbm9kZS5cclxuICAgICAqIEBwYXJhbSBub2RlSWQgSUQgb2YgdGhlIHRhcmdldCBub2RlXHJcbiAgICAgKiBAcmV0dXJucyBJbmZvcm1hdGlvbiBvYmplY3QgZm9yIHRoZSByZW5kaXRpb25cclxuICAgICAqL1xyXG4gICAgZ2V0QXZhaWxhYmxlUmVuZGl0aW9uRm9yTm9kZShub2RlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8UmVuZGl0aW9uRW50cnk+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UucmVuZGl0aW9uc0FwaS5nZXRSZW5kaXRpb25zKG5vZGVJZCkpLnBpcGUoXHJcbiAgICAgICAgICAgIG1hcCgoYXZhaWxhYmxlUmVuZGl0aW9uczogUmVuZGl0aW9uUGFnaW5nKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCByZW5kaXRpb25zQXZhaWxhYmxlOiBSZW5kaXRpb25FbnRyeVtdID0gYXZhaWxhYmxlUmVuZGl0aW9ucy5saXN0LmVudHJpZXMuZmlsdGVyKFxyXG4gICAgICAgICAgICAgICAgICAgIChyZW5kaXRpb24pID0+IChyZW5kaXRpb24uZW50cnkuaWQgPT09ICdwZGYnIHx8IHJlbmRpdGlvbi5lbnRyeS5pZCA9PT0gJ2ltZ3ByZXZpZXcnKSk7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBleGlzdGluZ1JlbmRpdGlvbiA9IHJlbmRpdGlvbnNBdmFpbGFibGUuZmluZCgocmVuZCkgPT4gcmVuZC5lbnRyeS5zdGF0dXMgPT09ICdDUkVBVEVEJyk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZXhpc3RpbmdSZW5kaXRpb24gPyBleGlzdGluZ1JlbmRpdGlvbiA6IHJlbmRpdGlvbnNBdmFpbGFibGVbMF07XHJcbiAgICAgICAgICAgIH0pKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdlbmVyYXRlcyBhIHJlbmRpdGlvbiBmb3IgYSBub2RlIHVzaW5nIHRoZSBmaXJzdCBhdmFpbGFibGUgZW5jb2RpbmcuXHJcbiAgICAgKiBAcGFyYW0gbm9kZUlkIElEIG9mIHRoZSB0YXJnZXQgbm9kZVxyXG4gICAgICogQHJldHVybnMgTnVsbCByZXNwb25zZSB0byBpbmRpY2F0ZSBjb21wbGV0aW9uXHJcbiAgICAgKi9cclxuICAgIGdlbmVyYXRlUmVuZGl0aW9uRm9yTm9kZShub2RlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZ2V0QXZhaWxhYmxlUmVuZGl0aW9uRm9yTm9kZShub2RlSWQpLnBpcGUoXHJcbiAgICAgICAgICAgIG1hcCgocmVuZGl0aW9uOiBSZW5kaXRpb25FbnRyeSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHJlbmRpdGlvbi5lbnRyeS5zdGF0dXMgIT09ICdDUkVBVEVEJykge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5yZW5kaXRpb25zQXBpLmNyZWF0ZVJlbmRpdGlvbihub2RlSWQsIHsgaWQ6IHJlbmRpdGlvbi5lbnRyeS5pZCB9KSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBlbXB0eSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVja3MgaWYgdGhlIHNwZWNpZmllZCByZW5kaXRpb24gaXMgYXZhaWxhYmxlIGZvciBhIG5vZGUuXHJcbiAgICAgKiBAcGFyYW0gbm9kZUlkIElEIG9mIHRoZSB0YXJnZXQgbm9kZVxyXG4gICAgICogQHBhcmFtIGVuY29kaW5nIE5hbWUgb2YgdGhlIHJlbmRpdGlvbiBlbmNvZGluZ1xyXG4gICAgICogQHJldHVybnMgVHJ1ZSBpZiB0aGUgcmVuZGl0aW9uIGlzIGF2YWlsYWJsZSwgZmFsc2Ugb3RoZXJ3aXNlXHJcbiAgICAgKi9cclxuICAgIGlzUmVuZGl0aW9uQXZhaWxhYmxlKG5vZGVJZDogc3RyaW5nLCBlbmNvZGluZzogc3RyaW5nKTogT2JzZXJ2YWJsZTxib29sZWFuPiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBPYnNlcnZhYmxlKChvYnNlcnZlcikgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmdldFJlbmRpdGlvbihub2RlSWQsIGVuY29kaW5nKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICAgICAocmVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgbGV0IGlzQXZhaWxhYmxlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocmVzLmVudHJ5LnN0YXR1cy50b1N0cmluZygpID09PSAnTk9UX0NSRUFURUQnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlzQXZhaWxhYmxlID0gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoaXNBdmFpbGFibGUpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZmFsc2UpO1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmNvbXBsZXRlKCk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVja3MgaWYgdGhlIG5vZGUgY2FuIGJlIGNvbnZlcnRlZCB1c2luZyB0aGUgc3BlY2lmaWVkIHJlbmRpdGlvbi5cclxuICAgICAqIEBwYXJhbSBub2RlSWQgSUQgb2YgdGhlIHRhcmdldCBub2RlXHJcbiAgICAgKiBAcGFyYW0gZW5jb2RpbmcgTmFtZSBvZiB0aGUgcmVuZGl0aW9uIGVuY29kaW5nXHJcbiAgICAgKiBAcmV0dXJucyBUcnVlIGlmIHRoZSBub2RlIGNhbiBiZSBjb252ZXJ0ZWQsIGZhbHNlIG90aGVyd2lzZVxyXG4gICAgICovXHJcbiAgICBpc0NvbnZlcnNpb25Qb3NzaWJsZShub2RlSWQ6IHN0cmluZywgZW5jb2Rpbmc6IHN0cmluZyk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG4gICAgICAgIHJldHVybiBuZXcgT2JzZXJ2YWJsZSgob2JzZXJ2ZXIpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5nZXRSZW5kaXRpb24obm9kZUlkLCBlbmNvZGluZykuc3Vic2NyaWJlKFxyXG4gICAgICAgICAgICAgICAgKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQodHJ1ZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICAgICAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmYWxzZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuY29tcGxldGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBVUkwgbGlua2luZyB0byB0aGUgc3BlY2lmaWVkIHJlbmRpdGlvbiBvZiBhIG5vZGUuXHJcbiAgICAgKiBAcGFyYW0gbm9kZUlkIElEIG9mIHRoZSB0YXJnZXQgbm9kZVxyXG4gICAgICogQHBhcmFtIGVuY29kaW5nIE5hbWUgb2YgdGhlIHJlbmRpdGlvbiBlbmNvZGluZ1xyXG4gICAgICogQHJldHVybnMgVVJMIHN0cmluZ1xyXG4gICAgICovXHJcbiAgICBnZXRSZW5kaXRpb25Vcmwobm9kZUlkOiBzdHJpbmcsIGVuY29kaW5nOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwaVNlcnZpY2UuY29udGVudEFwaS5nZXRSZW5kaXRpb25Vcmwobm9kZUlkLCBlbmNvZGluZyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGluZm9ybWF0aW9uIGFib3V0IGEgcmVuZGl0aW9uIG9mIGEgbm9kZS5cclxuICAgICAqIEBwYXJhbSBub2RlSWQgSUQgb2YgdGhlIHRhcmdldCBub2RlXHJcbiAgICAgKiBAcGFyYW0gZW5jb2RpbmcgTmFtZSBvZiB0aGUgcmVuZGl0aW9uIGVuY29kaW5nXHJcbiAgICAgKiBAcmV0dXJucyBJbmZvcm1hdGlvbiBvYmplY3QgYWJvdXQgdGhlIHJlbmRpdGlvblxyXG4gICAgICovXHJcbiAgICBnZXRSZW5kaXRpb24obm9kZUlkOiBzdHJpbmcsIGVuY29kaW5nOiBzdHJpbmcpOiBPYnNlcnZhYmxlPFJlbmRpdGlvbkVudHJ5PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLnJlbmRpdGlvbnNBcGkuZ2V0UmVuZGl0aW9uKG5vZGVJZCwgZW5jb2RpbmcpKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBsaXN0IG9mIGFsbCByZW5kaXRpb25zIGZvciBhIG5vZGUuXHJcbiAgICAgKiBAcGFyYW0gbm9kZUlkIElEIG9mIHRoZSB0YXJnZXQgbm9kZVxyXG4gICAgICogQHJldHVybnMgUGFnZWQgbGlzdCBvZiByZW5kaXRpb24gZGV0YWlsc1xyXG4gICAgICovXHJcbiAgICBnZXRSZW5kaXRpb25zTGlzdEJ5Tm9kZUlkKG5vZGVJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxSZW5kaXRpb25QYWdpbmc+IHtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UucmVuZGl0aW9uc0FwaS5nZXRSZW5kaXRpb25zKG5vZGVJZCkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ3JlYXRlcyBhIHJlbmRpdGlvbiBmb3IgYSBub2RlLlxyXG4gICAgICogQHBhcmFtIG5vZGVJZCBJRCBvZiB0aGUgdGFyZ2V0IG5vZGVcclxuICAgICAqIEBwYXJhbSBlbmNvZGluZyBOYW1lIG9mIHRoZSByZW5kaXRpb24gZW5jb2RpbmdcclxuICAgICAqIEByZXR1cm5zIE51bGwgcmVzcG9uc2UgdG8gaW5kaWNhdGUgY29tcGxldGlvblxyXG4gICAgICovXHJcbiAgICBjcmVhdGVSZW5kaXRpb24obm9kZUlkOiBzdHJpbmcsIGVuY29kaW5nOiBzdHJpbmcpOiBPYnNlcnZhYmxlPHt9PiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLnJlbmRpdGlvbnNBcGkuY3JlYXRlUmVuZGl0aW9uKG5vZGVJZCwgeyBpZDogZW5jb2RpbmcgfSkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVwZWF0ZWRseSBhdHRlbXB0cyB0byBjcmVhdGUgYSByZW5kaXRpb24sIHRocm91Z2ggdG8gc3VjY2VzcyBvciBmYWlsdXJlLlxyXG4gICAgICogQHBhcmFtIG5vZGVJZCBJRCBvZiB0aGUgdGFyZ2V0IG5vZGVcclxuICAgICAqIEBwYXJhbSBlbmNvZGluZyBOYW1lIG9mIHRoZSByZW5kaXRpb24gZW5jb2RpbmdcclxuICAgICAqIEBwYXJhbSBwb2xsaW5nSW50ZXJ2YWwgVGltZSBpbnRlcnZhbCAoaW4gbWlsbGlzZWNvbmRzKSBiZXR3ZWVuIGNoZWNrcyBmb3IgY29tcGxldGlvblxyXG4gICAgICogQHBhcmFtIHJldHJpZXMgTnVtYmVyIG9mIGF0dGVtcHRzIHRvIG1ha2UgYmVmb3JlIGRlY2xhcmluZyBmYWlsdXJlXHJcbiAgICAgKiBAcmV0dXJucyBUcnVlIGlmIHRoZSByZW5kaXRpb24gd2FzIGNyZWF0ZWQsIGZhbHNlIG90aGVyd2lzZVxyXG4gICAgICovXHJcbiAgICBjb252ZXJ0KG5vZGVJZDogc3RyaW5nLCBlbmNvZGluZzogc3RyaW5nLCBwb2xsaW5nSW50ZXJ2YWw6IG51bWJlciA9IDEwMDAsIHJldHJpZXM6IG51bWJlciA9IDUpIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5jcmVhdGVSZW5kaXRpb24obm9kZUlkLCBlbmNvZGluZylcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBjb25jYXRNYXAoKCkgPT4gdGhpcy5wb2xsUmVuZGl0aW9uKG5vZGVJZCwgZW5jb2RpbmcsIHBvbGxpbmdJbnRlcnZhbCwgcmV0cmllcykpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBwb2xsUmVuZGl0aW9uKG5vZGVJZDogc3RyaW5nLCBlbmNvZGluZzogc3RyaW5nLCBpbnRlcnZhbFNpemU6IG51bWJlciA9IDEwMDAsIHJldHJpZXM6IG51bWJlciA9IDUpIHtcclxuICAgICAgICBsZXQgYXR0ZW1wdHMgPSAwO1xyXG4gICAgICAgIHJldHVybiBpbnRlcnZhbChpbnRlcnZhbFNpemUpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgc3dpdGNoTWFwKCgpID0+IHRoaXMuZ2V0UmVuZGl0aW9uKG5vZGVJZCwgZW5jb2RpbmcpKSxcclxuICAgICAgICAgICAgICAgIHRha2VXaGlsZSgocmVuZGl0aW9uRW50cnk6IFJlbmRpdGlvbkVudHJ5KSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgYXR0ZW1wdHMgKz0gMTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoYXR0ZW1wdHMgPiByZXRyaWVzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIChyZW5kaXRpb25FbnRyeS5lbnRyeS5zdGF0dXMudG9TdHJpbmcoKSAhPT0gJ0NSRUFURUQnKTtcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19