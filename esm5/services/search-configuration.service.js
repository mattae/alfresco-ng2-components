/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var SearchConfigurationService = /** @class */ (function () {
    function SearchConfigurationService() {
    }
    /**
     * Generates a QueryBody object with custom search parameters.
     * @param searchTerm Term text to search for
     * @param maxResults Maximum number of search results to show in a page
     * @param skipCount The offset of the start of the page within the results list
     * @returns Query body defined by the parameters
     */
    /**
     * Generates a QueryBody object with custom search parameters.
     * @param {?} searchTerm Term text to search for
     * @param {?} maxResults Maximum number of search results to show in a page
     * @param {?} skipCount The offset of the start of the page within the results list
     * @return {?} Query body defined by the parameters
     */
    SearchConfigurationService.prototype.generateQueryBody = /**
     * Generates a QueryBody object with custom search parameters.
     * @param {?} searchTerm Term text to search for
     * @param {?} maxResults Maximum number of search results to show in a page
     * @param {?} skipCount The offset of the start of the page within the results list
     * @return {?} Query body defined by the parameters
     */
    function (searchTerm, maxResults, skipCount) {
        /** @type {?} */
        var defaultQueryBody = {
            query: {
                query: searchTerm ? "'" + searchTerm + "*' OR name:'" + searchTerm + "*'" : searchTerm
            },
            include: ['path', 'allowableOperations'],
            paging: {
                maxItems: maxResults,
                skipCount: skipCount
            },
            filterQueries: [
                { query: "TYPE:'cm:folder' OR TYPE:'cm:content'" },
                { query: 'NOT cm:creator:System' }
            ]
        };
        return defaultQueryBody;
    };
    SearchConfigurationService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    SearchConfigurationService.ctorParameters = function () { return []; };
    /** @nocollapse */ SearchConfigurationService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function SearchConfigurationService_Factory() { return new SearchConfigurationService(); }, token: SearchConfigurationService, providedIn: "root" });
    return SearchConfigurationService;
}());
export { SearchConfigurationService };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLWNvbmZpZ3VyYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL3NlYXJjaC1jb25maWd1cmF0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQzs7QUFJM0M7SUFLSTtJQUNBLENBQUM7SUFFRDs7Ozs7O09BTUc7Ozs7Ozs7O0lBQ0ksc0RBQWlCOzs7Ozs7O0lBQXhCLFVBQXlCLFVBQWtCLEVBQUUsVUFBa0IsRUFBRSxTQUFpQjs7WUFDeEUsZ0JBQWdCLEdBQWM7WUFDaEMsS0FBSyxFQUFFO2dCQUNILEtBQUssRUFBRSxVQUFVLENBQUMsQ0FBQyxDQUFDLE1BQUksVUFBVSxvQkFBZSxVQUFVLE9BQUksQ0FBQyxDQUFDLENBQUMsVUFBVTthQUMvRTtZQUNELE9BQU8sRUFBRSxDQUFDLE1BQU0sRUFBRSxxQkFBcUIsQ0FBQztZQUN4QyxNQUFNLEVBQUU7Z0JBQ0osUUFBUSxFQUFFLFVBQVU7Z0JBQ3BCLFNBQVMsRUFBRSxTQUFTO2FBQ3ZCO1lBQ0QsYUFBYSxFQUFFO2dCQUNYLEVBQUUsS0FBSyxFQUFFLHVDQUF1QyxFQUFFO2dCQUNsRCxFQUFFLEtBQUssRUFBRSx1QkFBdUIsRUFBRTthQUFDO1NBQzFDO1FBRUQsT0FBTyxnQkFBZ0IsQ0FBQztJQUM1QixDQUFDOztnQkEvQkosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7Ozs7cUNBdkJEO0NBcURDLEFBaENELElBZ0NDO1NBN0JZLDBCQUEwQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFF1ZXJ5Qm9keSB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5pbXBvcnQgeyBTZWFyY2hDb25maWd1cmF0aW9uSW50ZXJmYWNlIH0gZnJvbSAnLi4vaW50ZXJmYWNlL3NlYXJjaC1jb25maWd1cmF0aW9uLmludGVyZmFjZSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFNlYXJjaENvbmZpZ3VyYXRpb25TZXJ2aWNlIGltcGxlbWVudHMgU2VhcmNoQ29uZmlndXJhdGlvbkludGVyZmFjZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZW5lcmF0ZXMgYSBRdWVyeUJvZHkgb2JqZWN0IHdpdGggY3VzdG9tIHNlYXJjaCBwYXJhbWV0ZXJzLlxyXG4gICAgICogQHBhcmFtIHNlYXJjaFRlcm0gVGVybSB0ZXh0IHRvIHNlYXJjaCBmb3JcclxuICAgICAqIEBwYXJhbSBtYXhSZXN1bHRzIE1heGltdW0gbnVtYmVyIG9mIHNlYXJjaCByZXN1bHRzIHRvIHNob3cgaW4gYSBwYWdlXHJcbiAgICAgKiBAcGFyYW0gc2tpcENvdW50IFRoZSBvZmZzZXQgb2YgdGhlIHN0YXJ0IG9mIHRoZSBwYWdlIHdpdGhpbiB0aGUgcmVzdWx0cyBsaXN0XHJcbiAgICAgKiBAcmV0dXJucyBRdWVyeSBib2R5IGRlZmluZWQgYnkgdGhlIHBhcmFtZXRlcnNcclxuICAgICAqL1xyXG4gICAgcHVibGljIGdlbmVyYXRlUXVlcnlCb2R5KHNlYXJjaFRlcm06IHN0cmluZywgbWF4UmVzdWx0czogbnVtYmVyLCBza2lwQ291bnQ6IG51bWJlcik6IFF1ZXJ5Qm9keSB7XHJcbiAgICAgICAgY29uc3QgZGVmYXVsdFF1ZXJ5Qm9keTogUXVlcnlCb2R5ID0ge1xyXG4gICAgICAgICAgICBxdWVyeToge1xyXG4gICAgICAgICAgICAgICAgcXVlcnk6IHNlYXJjaFRlcm0gPyBgJyR7c2VhcmNoVGVybX0qJyBPUiBuYW1lOicke3NlYXJjaFRlcm19KidgIDogc2VhcmNoVGVybVxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBpbmNsdWRlOiBbJ3BhdGgnLCAnYWxsb3dhYmxlT3BlcmF0aW9ucyddLFxyXG4gICAgICAgICAgICBwYWdpbmc6IHtcclxuICAgICAgICAgICAgICAgIG1heEl0ZW1zOiBtYXhSZXN1bHRzLFxyXG4gICAgICAgICAgICAgICAgc2tpcENvdW50OiBza2lwQ291bnRcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgZmlsdGVyUXVlcmllczogW1xyXG4gICAgICAgICAgICAgICAgeyBxdWVyeTogXCJUWVBFOidjbTpmb2xkZXInIE9SIFRZUEU6J2NtOmNvbnRlbnQnXCIgfSxcclxuICAgICAgICAgICAgICAgIHsgcXVlcnk6ICdOT1QgY206Y3JlYXRvcjpTeXN0ZW0nIH1dXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGRlZmF1bHRRdWVyeUJvZHk7XHJcbiAgICB9XHJcbn1cclxuIl19