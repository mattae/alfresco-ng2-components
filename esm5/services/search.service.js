/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { Subject, from, throwError } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { SearchConfigurationService } from './search-configuration.service';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./search-configuration.service";
var SearchService = /** @class */ (function () {
    function SearchService(apiService, searchConfigurationService) {
        this.apiService = apiService;
        this.searchConfigurationService = searchConfigurationService;
        this.dataLoaded = new Subject();
    }
    /**
     * Gets a list of nodes that match the given search criteria.
     * @param term Term to search for
     * @param options Options for delivery of the search results
     * @returns List of nodes resulting from the search
     */
    /**
     * Gets a list of nodes that match the given search criteria.
     * @param {?} term Term to search for
     * @param {?=} options Options for delivery of the search results
     * @return {?} List of nodes resulting from the search
     */
    SearchService.prototype.getNodeQueryResults = /**
     * Gets a list of nodes that match the given search criteria.
     * @param {?} term Term to search for
     * @param {?=} options Options for delivery of the search results
     * @return {?} List of nodes resulting from the search
     */
    function (term, options) {
        var _this = this;
        /** @type {?} */
        var promise = this.apiService.getInstance().core.queriesApi.findNodes(term, options);
        promise.then((/**
         * @param {?} nodePaging
         * @return {?}
         */
        function (nodePaging) {
            _this.dataLoaded.next(nodePaging);
        })).catch((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); }));
        return from(promise);
    };
    /**
     * Performs a search.
     * @param searchTerm Term to search for
     * @param maxResults Maximum number of items in the list of results
     * @param skipCount Number of higher-ranked items to skip over in the list
     * @returns List of search results
     */
    /**
     * Performs a search.
     * @param {?} searchTerm Term to search for
     * @param {?} maxResults Maximum number of items in the list of results
     * @param {?} skipCount Number of higher-ranked items to skip over in the list
     * @return {?} List of search results
     */
    SearchService.prototype.search = /**
     * Performs a search.
     * @param {?} searchTerm Term to search for
     * @param {?} maxResults Maximum number of items in the list of results
     * @param {?} skipCount Number of higher-ranked items to skip over in the list
     * @return {?} List of search results
     */
    function (searchTerm, maxResults, skipCount) {
        var _this = this;
        /** @type {?} */
        var searchQuery = Object.assign(this.searchConfigurationService.generateQueryBody(searchTerm, maxResults, skipCount));
        /** @type {?} */
        var promise = this.apiService.getInstance().search.searchApi.search(searchQuery);
        promise.then((/**
         * @param {?} nodePaging
         * @return {?}
         */
        function (nodePaging) {
            _this.dataLoaded.next(nodePaging);
        })).catch((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); }));
        return from(promise);
    };
    /**
     * Performs a search with its parameters supplied by a QueryBody object.
     * @param queryBody Object containing the search parameters
     * @returns List of search results
     */
    /**
     * Performs a search with its parameters supplied by a QueryBody object.
     * @param {?} queryBody Object containing the search parameters
     * @return {?} List of search results
     */
    SearchService.prototype.searchByQueryBody = /**
     * Performs a search with its parameters supplied by a QueryBody object.
     * @param {?} queryBody Object containing the search parameters
     * @return {?} List of search results
     */
    function (queryBody) {
        var _this = this;
        /** @type {?} */
        var promise = this.apiService.getInstance().search.searchApi.search(queryBody);
        promise.then((/**
         * @param {?} nodePaging
         * @return {?}
         */
        function (nodePaging) {
            _this.dataLoaded.next(nodePaging);
        })).catch((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); }));
        return from(promise);
    };
    /**
     * @private
     * @param {?} error
     * @return {?}
     */
    SearchService.prototype.handleError = /**
     * @private
     * @param {?} error
     * @return {?}
     */
    function (error) {
        return throwError(error || 'Server error');
    };
    SearchService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    SearchService.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: SearchConfigurationService }
    ]; };
    /** @nocollapse */ SearchService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function SearchService_Factory() { return new SearchService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.SearchConfigurationService)); }, token: SearchService, providedIn: "root" });
    return SearchService;
}());
export { SearchService };
if (false) {
    /** @type {?} */
    SearchService.prototype.dataLoaded;
    /**
     * @type {?}
     * @private
     */
    SearchService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    SearchService.prototype.searchConfigurationService;
}
/**
 * @record
 */
export function SearchOptions() { }
if (false) {
    /**
     * The number of entities that exist in the collection before those included in this list.
     * @type {?|undefined}
     */
    SearchOptions.prototype.skipCount;
    /**
     * The maximum number of items to return in the list.
     * @type {?|undefined}
     */
    SearchOptions.prototype.maxItems;
    /**
     * The id of the node to start the search from. Supports the aliases -my-, -root- and -shared-.
     * @type {?|undefined}
     */
    SearchOptions.prototype.rootNodeId;
    /**
     * Restrict the returned results to only those of the given node type and its sub-types.
     * @type {?|undefined}
     */
    SearchOptions.prototype.nodeType;
    /**
     * Return additional information about the node. The available optional fields are:
     * `allowableOperations`, `aspectNames`, `isLink`, `isLocked`, `path` and `properties`.
     * @type {?|undefined}
     */
    SearchOptions.prototype.include;
    /**
     * String to control the order of the entities returned in a list. You can use this
     * parameter to sort the list by one or more fields. Each field has a default sort order,
     * which is normally ascending order (but see the JS-API docs to check if any fields used
     * in a method have a descending default search order). To sort the entities in a specific
     * order, you can use the "ASC" and "DESC" keywords for any field.
     * @type {?|undefined}
     */
    SearchOptions.prototype.orderBy;
    /**
     * List of field names. You can use this parameter to restrict the fields returned within
     * a response if, for example, you want to save on overall bandwidth. The list applies to a
     * returned individual entity or entries within a collection. If the API method also supports
     * the `include` parameter, then the fields specified in the include parameter are returned in
     * addition to those specified in the fields parameter.
     * @type {?|undefined}
     */
    SearchOptions.prototype.fields;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2VhcmNoLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy9zZWFyY2guc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRTNDLE9BQU8sRUFBYyxPQUFPLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUM3RCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUM1RCxPQUFPLEVBQUUsMEJBQTBCLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQzs7OztBQUU1RTtJQU9JLHVCQUFvQixVQUE4QixFQUM5QiwwQkFBc0Q7UUFEdEQsZUFBVSxHQUFWLFVBQVUsQ0FBb0I7UUFDOUIsK0JBQTBCLEdBQTFCLDBCQUEwQixDQUE0QjtRQUgxRSxlQUFVLEdBQXdCLElBQUksT0FBTyxFQUFFLENBQUM7SUFJaEQsQ0FBQztJQUVEOzs7OztPQUtHOzs7Ozs7O0lBQ0gsMkNBQW1COzs7Ozs7SUFBbkIsVUFBb0IsSUFBWSxFQUFFLE9BQXVCO1FBQXpELGlCQVFDOztZQVBTLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUM7UUFFdEYsT0FBTyxDQUFDLElBQUk7Ozs7UUFBQyxVQUFDLFVBQXNCO1lBQ2hDLEtBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO1FBQ3JDLENBQUMsRUFBQyxDQUFDLEtBQUs7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FBQztRQUV6QyxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBRUQ7Ozs7OztPQU1HOzs7Ozs7OztJQUNILDhCQUFNOzs7Ozs7O0lBQU4sVUFBTyxVQUFrQixFQUFFLFVBQWtCLEVBQUUsU0FBaUI7UUFBaEUsaUJBU0M7O1lBUlMsV0FBVyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLDBCQUEwQixDQUFDLGlCQUFpQixDQUFDLFVBQVUsRUFBRSxVQUFVLEVBQUUsU0FBUyxDQUFDLENBQUM7O1lBQ2pILE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFdBQVcsQ0FBQztRQUVsRixPQUFPLENBQUMsSUFBSTs7OztRQUFDLFVBQUMsVUFBc0I7WUFDaEMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDckMsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFBQyxDQUFDO1FBRXpDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3pCLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCx5Q0FBaUI7Ozs7O0lBQWpCLFVBQWtCLFNBQW9CO1FBQXRDLGlCQVFDOztZQVBTLE9BQU8sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLFNBQVMsQ0FBQztRQUVoRixPQUFPLENBQUMsSUFBSTs7OztRQUFDLFVBQUMsVUFBc0I7WUFDaEMsS0FBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDckMsQ0FBQyxFQUFDLENBQUMsS0FBSzs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFBQyxDQUFDO1FBRXpDLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDO0lBQ3pCLENBQUM7Ozs7OztJQUVPLG1DQUFXOzs7OztJQUFuQixVQUFvQixLQUFVO1FBQzFCLE9BQU8sVUFBVSxDQUFDLEtBQUssSUFBSSxjQUFjLENBQUMsQ0FBQztJQUMvQyxDQUFDOztnQkE5REosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFMUSxrQkFBa0I7Z0JBQ2xCLDBCQUEwQjs7O3dCQXJCbkM7Q0FzRkMsQUEvREQsSUErREM7U0E1RFksYUFBYTs7O0lBRXRCLG1DQUFnRDs7Ozs7SUFFcEMsbUNBQXNDOzs7OztJQUN0QyxtREFBOEQ7Ozs7O0FBeUQ5RSxtQ0FvQ0M7Ozs7OztJQWxDRyxrQ0FBbUI7Ozs7O0lBR25CLGlDQUFrQjs7Ozs7SUFHbEIsbUNBQW9COzs7OztJQUdwQixpQ0FBa0I7Ozs7OztJQU1sQixnQ0FBbUI7Ozs7Ozs7OztJQVNuQixnQ0FBaUI7Ozs7Ozs7OztJQVNqQiwrQkFBa0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBOb2RlUGFnaW5nLCBRdWVyeUJvZHkgfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgU3ViamVjdCwgZnJvbSwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2VhcmNoQ29uZmlndXJhdGlvblNlcnZpY2UgfSBmcm9tICcuL3NlYXJjaC1jb25maWd1cmF0aW9uLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTZWFyY2hTZXJ2aWNlIHtcclxuXHJcbiAgICBkYXRhTG9hZGVkOiBTdWJqZWN0PE5vZGVQYWdpbmc+ID0gbmV3IFN1YmplY3QoKTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgc2VhcmNoQ29uZmlndXJhdGlvblNlcnZpY2U6IFNlYXJjaENvbmZpZ3VyYXRpb25TZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgbGlzdCBvZiBub2RlcyB0aGF0IG1hdGNoIHRoZSBnaXZlbiBzZWFyY2ggY3JpdGVyaWEuXHJcbiAgICAgKiBAcGFyYW0gdGVybSBUZXJtIHRvIHNlYXJjaCBmb3JcclxuICAgICAqIEBwYXJhbSBvcHRpb25zIE9wdGlvbnMgZm9yIGRlbGl2ZXJ5IG9mIHRoZSBzZWFyY2ggcmVzdWx0c1xyXG4gICAgICogQHJldHVybnMgTGlzdCBvZiBub2RlcyByZXN1bHRpbmcgZnJvbSB0aGUgc2VhcmNoXHJcbiAgICAgKi9cclxuICAgIGdldE5vZGVRdWVyeVJlc3VsdHModGVybTogc3RyaW5nLCBvcHRpb25zPzogU2VhcmNoT3B0aW9ucyk6IE9ic2VydmFibGU8Tm9kZVBhZ2luZz4ge1xyXG4gICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5jb3JlLnF1ZXJpZXNBcGkuZmluZE5vZGVzKHRlcm0sIG9wdGlvbnMpO1xyXG5cclxuICAgICAgICBwcm9taXNlLnRoZW4oKG5vZGVQYWdpbmc6IE5vZGVQYWdpbmcpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5kYXRhTG9hZGVkLm5leHQobm9kZVBhZ2luZyk7XHJcbiAgICAgICAgfSkuY2F0Y2goKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20ocHJvbWlzZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBQZXJmb3JtcyBhIHNlYXJjaC5cclxuICAgICAqIEBwYXJhbSBzZWFyY2hUZXJtIFRlcm0gdG8gc2VhcmNoIGZvclxyXG4gICAgICogQHBhcmFtIG1heFJlc3VsdHMgTWF4aW11bSBudW1iZXIgb2YgaXRlbXMgaW4gdGhlIGxpc3Qgb2YgcmVzdWx0c1xyXG4gICAgICogQHBhcmFtIHNraXBDb3VudCBOdW1iZXIgb2YgaGlnaGVyLXJhbmtlZCBpdGVtcyB0byBza2lwIG92ZXIgaW4gdGhlIGxpc3RcclxuICAgICAqIEByZXR1cm5zIExpc3Qgb2Ygc2VhcmNoIHJlc3VsdHNcclxuICAgICAqL1xyXG4gICAgc2VhcmNoKHNlYXJjaFRlcm06IHN0cmluZywgbWF4UmVzdWx0czogbnVtYmVyLCBza2lwQ291bnQ6IG51bWJlcik6IE9ic2VydmFibGU8Tm9kZVBhZ2luZz4ge1xyXG4gICAgICAgIGNvbnN0IHNlYXJjaFF1ZXJ5ID0gT2JqZWN0LmFzc2lnbih0aGlzLnNlYXJjaENvbmZpZ3VyYXRpb25TZXJ2aWNlLmdlbmVyYXRlUXVlcnlCb2R5KHNlYXJjaFRlcm0sIG1heFJlc3VsdHMsIHNraXBDb3VudCkpO1xyXG4gICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5zZWFyY2guc2VhcmNoQXBpLnNlYXJjaChzZWFyY2hRdWVyeSk7XHJcblxyXG4gICAgICAgIHByb21pc2UudGhlbigobm9kZVBhZ2luZzogTm9kZVBhZ2luZykgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmRhdGFMb2FkZWQubmV4dChub2RlUGFnaW5nKTtcclxuICAgICAgICB9KS5jYXRjaCgoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpO1xyXG5cclxuICAgICAgICByZXR1cm4gZnJvbShwcm9taXNlKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFBlcmZvcm1zIGEgc2VhcmNoIHdpdGggaXRzIHBhcmFtZXRlcnMgc3VwcGxpZWQgYnkgYSBRdWVyeUJvZHkgb2JqZWN0LlxyXG4gICAgICogQHBhcmFtIHF1ZXJ5Qm9keSBPYmplY3QgY29udGFpbmluZyB0aGUgc2VhcmNoIHBhcmFtZXRlcnNcclxuICAgICAqIEByZXR1cm5zIExpc3Qgb2Ygc2VhcmNoIHJlc3VsdHNcclxuICAgICAqL1xyXG4gICAgc2VhcmNoQnlRdWVyeUJvZHkocXVlcnlCb2R5OiBRdWVyeUJvZHkpOiBPYnNlcnZhYmxlPE5vZGVQYWdpbmc+IHtcclxuICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuc2VhcmNoLnNlYXJjaEFwaS5zZWFyY2gocXVlcnlCb2R5KTtcclxuXHJcbiAgICAgICAgcHJvbWlzZS50aGVuKChub2RlUGFnaW5nOiBOb2RlUGFnaW5nKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMuZGF0YUxvYWRlZC5uZXh0KG5vZGVQYWdpbmcpO1xyXG4gICAgICAgIH0pLmNhdGNoKChlcnIpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyKSk7XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHByb21pc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaGFuZGxlRXJyb3IoZXJyb3I6IGFueSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IgfHwgJ1NlcnZlciBlcnJvcicpO1xyXG4gICAgfVxyXG59XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIFNlYXJjaE9wdGlvbnMge1xyXG4gICAgLyoqIFRoZSBudW1iZXIgb2YgZW50aXRpZXMgdGhhdCBleGlzdCBpbiB0aGUgY29sbGVjdGlvbiBiZWZvcmUgdGhvc2UgaW5jbHVkZWQgaW4gdGhpcyBsaXN0LiAqL1xyXG4gICAgc2tpcENvdW50PzogbnVtYmVyO1xyXG5cclxuICAgIC8qKiBUaGUgbWF4aW11bSBudW1iZXIgb2YgaXRlbXMgdG8gcmV0dXJuIGluIHRoZSBsaXN0LiAqL1xyXG4gICAgbWF4SXRlbXM/OiBudW1iZXI7XHJcblxyXG4gICAgLyoqIFRoZSBpZCBvZiB0aGUgbm9kZSB0byBzdGFydCB0aGUgc2VhcmNoIGZyb20uIFN1cHBvcnRzIHRoZSBhbGlhc2VzIC1teS0sIC1yb290LSBhbmQgLXNoYXJlZC0uICovXHJcbiAgICByb290Tm9kZUlkPzogc3RyaW5nO1xyXG5cclxuICAgIC8qKiBSZXN0cmljdCB0aGUgcmV0dXJuZWQgcmVzdWx0cyB0byBvbmx5IHRob3NlIG9mIHRoZSBnaXZlbiBub2RlIHR5cGUgYW5kIGl0cyBzdWItdHlwZXMuICovXHJcbiAgICBub2RlVHlwZT86IHN0cmluZztcclxuXHJcbiAgICAvKipcclxuICAgICAqIFJldHVybiBhZGRpdGlvbmFsIGluZm9ybWF0aW9uIGFib3V0IHRoZSBub2RlLiBUaGUgYXZhaWxhYmxlIG9wdGlvbmFsIGZpZWxkcyBhcmU6XHJcbiAgICAgKiBgYWxsb3dhYmxlT3BlcmF0aW9uc2AsIGBhc3BlY3ROYW1lc2AsIGBpc0xpbmtgLCBgaXNMb2NrZWRgLCBgcGF0aGAgYW5kIGBwcm9wZXJ0aWVzYC5cclxuICAgICAqL1xyXG4gICAgaW5jbHVkZT86IHN0cmluZ1tdO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogU3RyaW5nIHRvIGNvbnRyb2wgdGhlIG9yZGVyIG9mIHRoZSBlbnRpdGllcyByZXR1cm5lZCBpbiBhIGxpc3QuIFlvdSBjYW4gdXNlIHRoaXNcclxuICAgICAqIHBhcmFtZXRlciB0byBzb3J0IHRoZSBsaXN0IGJ5IG9uZSBvciBtb3JlIGZpZWxkcy4gRWFjaCBmaWVsZCBoYXMgYSBkZWZhdWx0IHNvcnQgb3JkZXIsXHJcbiAgICAgKiB3aGljaCBpcyBub3JtYWxseSBhc2NlbmRpbmcgb3JkZXIgKGJ1dCBzZWUgdGhlIEpTLUFQSSBkb2NzIHRvIGNoZWNrIGlmIGFueSBmaWVsZHMgdXNlZFxyXG4gICAgICogaW4gYSBtZXRob2QgaGF2ZSBhIGRlc2NlbmRpbmcgZGVmYXVsdCBzZWFyY2ggb3JkZXIpLiBUbyBzb3J0IHRoZSBlbnRpdGllcyBpbiBhIHNwZWNpZmljXHJcbiAgICAgKiBvcmRlciwgeW91IGNhbiB1c2UgdGhlIFwiQVNDXCIgYW5kIFwiREVTQ1wiIGtleXdvcmRzIGZvciBhbnkgZmllbGQuXHJcbiAgICAgKi9cclxuICAgIG9yZGVyQnk/OiBzdHJpbmc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBMaXN0IG9mIGZpZWxkIG5hbWVzLiBZb3UgY2FuIHVzZSB0aGlzIHBhcmFtZXRlciB0byByZXN0cmljdCB0aGUgZmllbGRzIHJldHVybmVkIHdpdGhpblxyXG4gICAgICogYSByZXNwb25zZSBpZiwgZm9yIGV4YW1wbGUsIHlvdSB3YW50IHRvIHNhdmUgb24gb3ZlcmFsbCBiYW5kd2lkdGguIFRoZSBsaXN0IGFwcGxpZXMgdG8gYVxyXG4gICAgICogcmV0dXJuZWQgaW5kaXZpZHVhbCBlbnRpdHkgb3IgZW50cmllcyB3aXRoaW4gYSBjb2xsZWN0aW9uLiBJZiB0aGUgQVBJIG1ldGhvZCBhbHNvIHN1cHBvcnRzXHJcbiAgICAgKiB0aGUgYGluY2x1ZGVgIHBhcmFtZXRlciwgdGhlbiB0aGUgZmllbGRzIHNwZWNpZmllZCBpbiB0aGUgaW5jbHVkZSBwYXJhbWV0ZXIgYXJlIHJldHVybmVkIGluXHJcbiAgICAgKiBhZGRpdGlvbiB0byB0aG9zZSBzcGVjaWZpZWQgaW4gdGhlIGZpZWxkcyBwYXJhbWV0ZXIuXHJcbiAgICAgKi9cclxuICAgIGZpZWxkcz86IHN0cmluZ1tdO1xyXG59XHJcbiJdfQ==