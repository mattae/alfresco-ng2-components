/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, of, Subject } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { UserPreferencesService } from './user-preferences.service';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "./user-preferences.service";
var SharedLinksApiService = /** @class */ (function () {
    function SharedLinksApiService(apiService, preferences) {
        this.apiService = apiService;
        this.preferences = preferences;
        this.error = new Subject();
    }
    Object.defineProperty(SharedLinksApiService.prototype, "sharedLinksApi", {
        get: /**
         * @private
         * @return {?}
         */
        function () {
            return this.apiService.getInstance().core.sharedlinksApi;
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Gets shared links available to the current user.
     * @param options Options supported by JS-API
     * @returns List of shared links
     */
    /**
     * Gets shared links available to the current user.
     * @param {?=} options Options supported by JS-API
     * @return {?} List of shared links
     */
    SharedLinksApiService.prototype.getSharedLinks = /**
     * Gets shared links available to the current user.
     * @param {?=} options Options supported by JS-API
     * @return {?} List of shared links
     */
    function (options) {
        if (options === void 0) { options = {}; }
        /** @type {?} */
        var defaultOptions = {
            maxItems: this.preferences.paginationSize,
            skipCount: 0,
            include: ['properties', 'allowableOperations']
        };
        /** @type {?} */
        var queryOptions = Object.assign({}, defaultOptions, options);
        /** @type {?} */
        var promise = this.sharedLinksApi.findSharedLinks(queryOptions);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return of(err); })));
    };
    /**
     * Creates a shared link available to the current user.
     * @param nodeId ID of the node to link to
     * @param options Options supported by JS-API
     * @returns The shared link just created
     */
    /**
     * Creates a shared link available to the current user.
     * @param {?} nodeId ID of the node to link to
     * @param {?=} options Options supported by JS-API
     * @return {?} The shared link just created
     */
    SharedLinksApiService.prototype.createSharedLinks = /**
     * Creates a shared link available to the current user.
     * @param {?} nodeId ID of the node to link to
     * @param {?=} options Options supported by JS-API
     * @return {?} The shared link just created
     */
    function (nodeId, options) {
        if (options === void 0) { options = {}; }
        /** @type {?} */
        var promise = this.sharedLinksApi.addSharedLink({ nodeId: nodeId });
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return of(err); })));
    };
    /**
     * Deletes a shared link.
     * @param sharedId ID of the link to delete
     * @returns Null response notifying when the operation is complete
     */
    /**
     * Deletes a shared link.
     * @param {?} sharedId ID of the link to delete
     * @return {?} Null response notifying when the operation is complete
     */
    SharedLinksApiService.prototype.deleteSharedLink = /**
     * Deletes a shared link.
     * @param {?} sharedId ID of the link to delete
     * @return {?} Null response notifying when the operation is complete
     */
    function (sharedId) {
        /** @type {?} */
        var promise = this.sharedLinksApi.deleteSharedLink(sharedId);
        return from(promise).pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return of(err); })));
    };
    SharedLinksApiService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    SharedLinksApiService.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: UserPreferencesService }
    ]; };
    /** @nocollapse */ SharedLinksApiService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function SharedLinksApiService_Factory() { return new SharedLinksApiService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.UserPreferencesService)); }, token: SharedLinksApiService, providedIn: "root" });
    return SharedLinksApiService;
}());
export { SharedLinksApiService };
if (false) {
    /** @type {?} */
    SharedLinksApiService.prototype.error;
    /**
     * @type {?}
     * @private
     */
    SharedLinksApiService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    SharedLinksApiService.prototype.preferences;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLWxpbmtzLWFwaS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvc2hhcmVkLWxpbmtzLWFwaS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFjLElBQUksRUFBRSxFQUFFLEVBQUUsT0FBTyxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQ3JELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDO0FBQzVELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3BFLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7OztBQUU1QztJQU9JLCtCQUFvQixVQUE4QixFQUM5QixXQUFtQztRQURuQyxlQUFVLEdBQVYsVUFBVSxDQUFvQjtRQUM5QixnQkFBVyxHQUFYLFdBQVcsQ0FBd0I7UUFIdkQsVUFBSyxHQUFHLElBQUksT0FBTyxFQUEyQyxDQUFDO0lBSS9ELENBQUM7SUFFRCxzQkFBWSxpREFBYzs7Ozs7UUFBMUI7WUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQztRQUM3RCxDQUFDOzs7T0FBQTtJQUVEOzs7O09BSUc7Ozs7OztJQUNILDhDQUFjOzs7OztJQUFkLFVBQWUsT0FBaUI7UUFBakIsd0JBQUEsRUFBQSxZQUFpQjs7WUFDdEIsY0FBYyxHQUFHO1lBQ25CLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWM7WUFDekMsU0FBUyxFQUFFLENBQUM7WUFDWixPQUFPLEVBQUUsQ0FBQyxZQUFZLEVBQUUscUJBQXFCLENBQUM7U0FDakQ7O1lBQ0ssWUFBWSxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUMsRUFBRSxFQUFFLGNBQWMsRUFBRSxPQUFPLENBQUM7O1lBQ3pELE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUM7UUFFakUsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLENBQUMsSUFBSSxDQUNyQixVQUFVOzs7O1FBQUMsVUFBQyxHQUFHLElBQUssT0FBQSxFQUFFLENBQUMsR0FBRyxDQUFDLEVBQVAsQ0FBTyxFQUFDLENBQy9CLENBQUM7SUFDTixDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCxpREFBaUI7Ozs7OztJQUFqQixVQUFrQixNQUFjLEVBQUUsT0FBaUI7UUFBakIsd0JBQUEsRUFBQSxZQUFpQjs7WUFDekMsT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsYUFBYSxDQUFDLEVBQUUsTUFBTSxFQUFFLE1BQU0sRUFBRSxDQUFDO1FBRXJFLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FDckIsVUFBVTs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsRUFBRSxDQUFDLEdBQUcsQ0FBQyxFQUFQLENBQU8sRUFBQyxDQUMvQixDQUFDO0lBQ04sQ0FBQztJQUVEOzs7O09BSUc7Ozs7OztJQUNILGdEQUFnQjs7Ozs7SUFBaEIsVUFBaUIsUUFBZ0I7O1lBQ3ZCLE9BQU8sR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQztRQUU5RCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxJQUFJLENBQ3JCLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQVUsSUFBSyxPQUFBLEVBQUUsQ0FBQyxHQUFHLENBQUMsRUFBUCxDQUFPLEVBQUMsQ0FDdEMsQ0FBQztJQUNOLENBQUM7O2dCQTNESixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7O2dCQU5RLGtCQUFrQjtnQkFDbEIsc0JBQXNCOzs7Z0NBckIvQjtDQW9GQyxBQTVERCxJQTREQztTQXpEWSxxQkFBcUI7OztJQUU5QixzQ0FBK0Q7Ozs7O0lBRW5ELDJDQUFzQzs7Ozs7SUFDdEMsNENBQTJDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgTm9kZVBhZ2luZywgU2hhcmVkTGlua0VudHJ5IH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZyb20sIG9mLCBTdWJqZWN0IH0gZnJvbSAncnhqcyc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4vYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlIH0gZnJvbSAnLi91c2VyLXByZWZlcmVuY2VzLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBjYXRjaEVycm9yIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBTaGFyZWRMaW5rc0FwaVNlcnZpY2Uge1xyXG5cclxuICAgIGVycm9yID0gbmV3IFN1YmplY3Q8eyBzdGF0dXNDb2RlOiBudW1iZXIsIG1lc3NhZ2U6IHN0cmluZyB9PigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgYXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBwcmVmZXJlbmNlczogVXNlclByZWZlcmVuY2VzU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0IHNoYXJlZExpbmtzQXBpKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5jb3JlLnNoYXJlZGxpbmtzQXBpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBzaGFyZWQgbGlua3MgYXZhaWxhYmxlIHRvIHRoZSBjdXJyZW50IHVzZXIuXHJcbiAgICAgKiBAcGFyYW0gb3B0aW9ucyBPcHRpb25zIHN1cHBvcnRlZCBieSBKUy1BUElcclxuICAgICAqIEByZXR1cm5zIExpc3Qgb2Ygc2hhcmVkIGxpbmtzXHJcbiAgICAgKi9cclxuICAgIGdldFNoYXJlZExpbmtzKG9wdGlvbnM6IGFueSA9IHt9KTogT2JzZXJ2YWJsZTxOb2RlUGFnaW5nPiB7XHJcbiAgICAgICAgY29uc3QgZGVmYXVsdE9wdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIG1heEl0ZW1zOiB0aGlzLnByZWZlcmVuY2VzLnBhZ2luYXRpb25TaXplLFxyXG4gICAgICAgICAgICBza2lwQ291bnQ6IDAsXHJcbiAgICAgICAgICAgIGluY2x1ZGU6IFsncHJvcGVydGllcycsICdhbGxvd2FibGVPcGVyYXRpb25zJ11cclxuICAgICAgICB9O1xyXG4gICAgICAgIGNvbnN0IHF1ZXJ5T3B0aW9ucyA9IE9iamVjdC5hc3NpZ24oe30sIGRlZmF1bHRPcHRpb25zLCBvcHRpb25zKTtcclxuICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5zaGFyZWRMaW5rc0FwaS5maW5kU2hhcmVkTGlua3MocXVlcnlPcHRpb25zKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20ocHJvbWlzZSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiBvZihlcnIpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDcmVhdGVzIGEgc2hhcmVkIGxpbmsgYXZhaWxhYmxlIHRvIHRoZSBjdXJyZW50IHVzZXIuXHJcbiAgICAgKiBAcGFyYW0gbm9kZUlkIElEIG9mIHRoZSBub2RlIHRvIGxpbmsgdG9cclxuICAgICAqIEBwYXJhbSBvcHRpb25zIE9wdGlvbnMgc3VwcG9ydGVkIGJ5IEpTLUFQSVxyXG4gICAgICogQHJldHVybnMgVGhlIHNoYXJlZCBsaW5rIGp1c3QgY3JlYXRlZFxyXG4gICAgICovXHJcbiAgICBjcmVhdGVTaGFyZWRMaW5rcyhub2RlSWQ6IHN0cmluZywgb3B0aW9uczogYW55ID0ge30pOiBPYnNlcnZhYmxlPFNoYXJlZExpbmtFbnRyeT4ge1xyXG4gICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLnNoYXJlZExpbmtzQXBpLmFkZFNoYXJlZExpbmsoeyBub2RlSWQ6IG5vZGVJZCB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20ocHJvbWlzZSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiBvZihlcnIpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZWxldGVzIGEgc2hhcmVkIGxpbmsuXHJcbiAgICAgKiBAcGFyYW0gc2hhcmVkSWQgSUQgb2YgdGhlIGxpbmsgdG8gZGVsZXRlXHJcbiAgICAgKiBAcmV0dXJucyBOdWxsIHJlc3BvbnNlIG5vdGlmeWluZyB3aGVuIHRoZSBvcGVyYXRpb24gaXMgY29tcGxldGVcclxuICAgICAqL1xyXG4gICAgZGVsZXRlU2hhcmVkTGluayhzaGFyZWRJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnkgfCBFcnJvcj4ge1xyXG4gICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLnNoYXJlZExpbmtzQXBpLmRlbGV0ZVNoYXJlZExpbmsoc2hhcmVkSWQpO1xyXG5cclxuICAgICAgICByZXR1cm4gZnJvbShwcm9taXNlKS5waXBlKFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnI6IEVycm9yKSA9PiBvZihlcnIpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcbn1cclxuIl19