/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
var SitesService = /** @class */ (function () {
    function SitesService(apiService) {
        this.apiService = apiService;
    }
    /**
     * Gets a list of all sites in the repository.
     * @param opts Options supported by JS-API
     * @returns List of sites
     */
    /**
     * Gets a list of all sites in the repository.
     * @param {?=} opts Options supported by JS-API
     * @return {?} List of sites
     */
    SitesService.prototype.getSites = /**
     * Gets a list of all sites in the repository.
     * @param {?=} opts Options supported by JS-API
     * @return {?} List of sites
     */
    function (opts) {
        var _this = this;
        if (opts === void 0) { opts = {}; }
        /** @type {?} */
        var defaultOptions = {
            skipCount: 0,
            include: ['properties']
        };
        /** @type {?} */
        var queryOptions = Object.assign({}, defaultOptions, opts);
        return from(this.apiService.getInstance().core.sitesApi.getSites(queryOptions))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets the details for a site.
     * @param siteId ID of the target site
     * @param opts Options supported by JS-API
     * @returns Information about the site
     */
    /**
     * Gets the details for a site.
     * @param {?} siteId ID of the target site
     * @param {?=} opts Options supported by JS-API
     * @return {?} Information about the site
     */
    SitesService.prototype.getSite = /**
     * Gets the details for a site.
     * @param {?} siteId ID of the target site
     * @param {?=} opts Options supported by JS-API
     * @return {?} Information about the site
     */
    function (siteId, opts) {
        var _this = this;
        return from(this.apiService.getInstance().core.sitesApi.getSite(siteId, opts))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Deletes a site.
     * @param siteId Site to delete
     * @param permanentFlag True: deletion is permanent; False: site is moved to the trash
     * @returns Null response notifying when the operation is complete
     */
    /**
     * Deletes a site.
     * @param {?} siteId Site to delete
     * @param {?=} permanentFlag True: deletion is permanent; False: site is moved to the trash
     * @return {?} Null response notifying when the operation is complete
     */
    SitesService.prototype.deleteSite = /**
     * Deletes a site.
     * @param {?} siteId Site to delete
     * @param {?=} permanentFlag True: deletion is permanent; False: site is moved to the trash
     * @return {?} Null response notifying when the operation is complete
     */
    function (siteId, permanentFlag) {
        var _this = this;
        if (permanentFlag === void 0) { permanentFlag = true; }
        /** @type {?} */
        var options = {};
        options.permanent = permanentFlag;
        return from(this.apiService.getInstance().core.sitesApi.deleteSite(siteId, options))
            .pipe(catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets a site's content.
     * @param siteId ID of the target site
     * @returns Site content
     */
    /**
     * Gets a site's content.
     * @param {?} siteId ID of the target site
     * @return {?} Site content
     */
    SitesService.prototype.getSiteContent = /**
     * Gets a site's content.
     * @param {?} siteId ID of the target site
     * @return {?} Site content
     */
    function (siteId) {
        return this.getSite(siteId, { relations: ['containers'] });
    };
    /**
     * Gets a list of all a site's members.
     * @param siteId ID of the target site
     * @returns Site members
     */
    /**
     * Gets a list of all a site's members.
     * @param {?} siteId ID of the target site
     * @return {?} Site members
     */
    SitesService.prototype.getSiteMembers = /**
     * Gets a list of all a site's members.
     * @param {?} siteId ID of the target site
     * @return {?} Site members
     */
    function (siteId) {
        return this.getSite(siteId, { relations: ['members'] });
    };
    /**
     * Gets the username of the user currently logged into ACS.
     * @returns Username string
     */
    /**
     * Gets the username of the user currently logged into ACS.
     * @return {?} Username string
     */
    SitesService.prototype.getEcmCurrentLoggedUserName = /**
     * Gets the username of the user currently logged into ACS.
     * @return {?} Username string
     */
    function () {
        return this.apiService.getInstance().getEcmUsername();
    };
    /**
     * @private
     * @param {?} error
     * @return {?}
     */
    SitesService.prototype.handleError = /**
     * @private
     * @param {?} error
     * @return {?}
     */
    function (error) {
        console.error(error);
        return throwError(error || 'Server error');
    };
    SitesService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    SitesService.ctorParameters = function () { return [
        { type: AlfrescoApiService }
    ]; };
    /** @nocollapse */ SitesService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function SitesService_Factory() { return new SitesService(i0.ɵɵinject(i1.AlfrescoApiService)); }, token: SitesService, providedIn: "root" });
    return SitesService;
}());
export { SitesService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    SitesService.prototype.apiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2l0ZXMuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL3NpdGVzLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQWMsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNwRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQztBQUU1RCxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7OztBQUU1QztJQUtJLHNCQUNZLFVBQThCO1FBQTlCLGVBQVUsR0FBVixVQUFVLENBQW9CO0lBQzFDLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCwrQkFBUTs7Ozs7SUFBUixVQUFTLElBQWM7UUFBdkIsaUJBVUM7UUFWUSxxQkFBQSxFQUFBLFNBQWM7O1lBQ2IsY0FBYyxHQUFHO1lBQ25CLFNBQVMsRUFBRSxDQUFDO1lBQ1osT0FBTyxFQUFFLENBQUMsWUFBWSxDQUFDO1NBQzFCOztZQUNLLFlBQVksR0FBRyxNQUFNLENBQUMsTUFBTSxDQUFDLEVBQUUsRUFBRSxjQUFjLEVBQUUsSUFBSSxDQUFDO1FBQzVELE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxRQUFRLENBQUMsWUFBWSxDQUFDLENBQUM7YUFDMUUsSUFBSSxDQUNELFVBQVU7Ozs7UUFBQyxVQUFDLEdBQVEsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FDbEQsQ0FBQztJQUNWLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILDhCQUFPOzs7Ozs7SUFBUCxVQUFRLE1BQWMsRUFBRSxJQUFVO1FBQWxDLGlCQUtDO1FBSkcsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsSUFBSSxDQUFDLENBQUM7YUFDekUsSUFBSSxDQUNELFVBQVU7Ozs7UUFBQyxVQUFDLEdBQVEsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FDbEQsQ0FBQztJQUNWLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILGlDQUFVOzs7Ozs7SUFBVixVQUFXLE1BQWMsRUFBRSxhQUE2QjtRQUF4RCxpQkFPQztRQVAwQiw4QkFBQSxFQUFBLG9CQUE2Qjs7WUFDOUMsT0FBTyxHQUFRLEVBQUU7UUFDdkIsT0FBTyxDQUFDLFNBQVMsR0FBRyxhQUFhLENBQUM7UUFDbEMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsT0FBTyxDQUFDLENBQUM7YUFDL0UsSUFBSSxDQUNELFVBQVU7Ozs7UUFBQyxVQUFDLEdBQVEsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FDbEQsQ0FBQztJQUNWLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCxxQ0FBYzs7Ozs7SUFBZCxVQUFlLE1BQWM7UUFDekIsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRSxFQUFFLFNBQVMsRUFBRSxDQUFDLFlBQVksQ0FBQyxFQUFFLENBQUMsQ0FBQztJQUMvRCxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gscUNBQWM7Ozs7O0lBQWQsVUFBZSxNQUFjO1FBQ3pCLE9BQU8sSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsRUFBRSxTQUFTLEVBQUUsQ0FBQyxTQUFTLENBQUMsRUFBRSxDQUFDLENBQUM7SUFDNUQsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7SUFDSCxrREFBMkI7Ozs7SUFBM0I7UUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDMUQsQ0FBQzs7Ozs7O0lBRU8sa0NBQVc7Ozs7O0lBQW5CLFVBQW9CLEtBQVU7UUFDMUIsT0FBTyxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUNyQixPQUFPLFVBQVUsQ0FBQyxLQUFLLElBQUksY0FBYyxDQUFDLENBQUM7SUFDL0MsQ0FBQzs7Z0JBbkZKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7Z0JBTlEsa0JBQWtCOzs7dUJBbkIzQjtDQTJHQyxBQXBGRCxJQW9GQztTQWpGWSxZQUFZOzs7Ozs7SUFHakIsa0NBQXNDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgZnJvbSwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU2l0ZVBhZ2luZywgU2l0ZUVudHJ5IH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFNpdGVzU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBsaXN0IG9mIGFsbCBzaXRlcyBpbiB0aGUgcmVwb3NpdG9yeS5cclxuICAgICAqIEBwYXJhbSBvcHRzIE9wdGlvbnMgc3VwcG9ydGVkIGJ5IEpTLUFQSVxyXG4gICAgICogQHJldHVybnMgTGlzdCBvZiBzaXRlc1xyXG4gICAgICovXHJcbiAgICBnZXRTaXRlcyhvcHRzOiBhbnkgPSB7fSk6IE9ic2VydmFibGU8U2l0ZVBhZ2luZz4ge1xyXG4gICAgICAgIGNvbnN0IGRlZmF1bHRPcHRpb25zID0ge1xyXG4gICAgICAgICAgICBza2lwQ291bnQ6IDAsXHJcbiAgICAgICAgICAgIGluY2x1ZGU6IFsncHJvcGVydGllcyddXHJcbiAgICAgICAgfTtcclxuICAgICAgICBjb25zdCBxdWVyeU9wdGlvbnMgPSBPYmplY3QuYXNzaWduKHt9LCBkZWZhdWx0T3B0aW9ucywgb3B0cyk7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuY29yZS5zaXRlc0FwaS5nZXRTaXRlcyhxdWVyeU9wdGlvbnMpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycjogYW55KSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBkZXRhaWxzIGZvciBhIHNpdGUuXHJcbiAgICAgKiBAcGFyYW0gc2l0ZUlkIElEIG9mIHRoZSB0YXJnZXQgc2l0ZVxyXG4gICAgICogQHBhcmFtIG9wdHMgT3B0aW9ucyBzdXBwb3J0ZWQgYnkgSlMtQVBJXHJcbiAgICAgKiBAcmV0dXJucyBJbmZvcm1hdGlvbiBhYm91dCB0aGUgc2l0ZVxyXG4gICAgICovXHJcbiAgICBnZXRTaXRlKHNpdGVJZDogc3RyaW5nLCBvcHRzPzogYW55KTogT2JzZXJ2YWJsZTxTaXRlRW50cnkgfCB7fT4ge1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLmNvcmUuc2l0ZXNBcGkuZ2V0U2l0ZShzaXRlSWQsIG9wdHMpKVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycjogYW55KSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZWxldGVzIGEgc2l0ZS5cclxuICAgICAqIEBwYXJhbSBzaXRlSWQgU2l0ZSB0byBkZWxldGVcclxuICAgICAqIEBwYXJhbSBwZXJtYW5lbnRGbGFnIFRydWU6IGRlbGV0aW9uIGlzIHBlcm1hbmVudDsgRmFsc2U6IHNpdGUgaXMgbW92ZWQgdG8gdGhlIHRyYXNoXHJcbiAgICAgKiBAcmV0dXJucyBOdWxsIHJlc3BvbnNlIG5vdGlmeWluZyB3aGVuIHRoZSBvcGVyYXRpb24gaXMgY29tcGxldGVcclxuICAgICAqL1xyXG4gICAgZGVsZXRlU2l0ZShzaXRlSWQ6IHN0cmluZywgcGVybWFuZW50RmxhZzogYm9vbGVhbiA9IHRydWUpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGNvbnN0IG9wdGlvbnM6IGFueSA9IHt9O1xyXG4gICAgICAgIG9wdGlvbnMucGVybWFuZW50ID0gcGVybWFuZW50RmxhZztcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5jb3JlLnNpdGVzQXBpLmRlbGV0ZVNpdGUoc2l0ZUlkLCBvcHRpb25zKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnI6IGFueSkgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhIHNpdGUncyBjb250ZW50LlxyXG4gICAgICogQHBhcmFtIHNpdGVJZCBJRCBvZiB0aGUgdGFyZ2V0IHNpdGVcclxuICAgICAqIEByZXR1cm5zIFNpdGUgY29udGVudFxyXG4gICAgICovXHJcbiAgICBnZXRTaXRlQ29udGVudChzaXRlSWQ6IHN0cmluZyk6IE9ic2VydmFibGU8U2l0ZUVudHJ5IHwge30+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRTaXRlKHNpdGVJZCwgeyByZWxhdGlvbnM6IFsnY29udGFpbmVycyddIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhIGxpc3Qgb2YgYWxsIGEgc2l0ZSdzIG1lbWJlcnMuXHJcbiAgICAgKiBAcGFyYW0gc2l0ZUlkIElEIG9mIHRoZSB0YXJnZXQgc2l0ZVxyXG4gICAgICogQHJldHVybnMgU2l0ZSBtZW1iZXJzXHJcbiAgICAgKi9cclxuICAgIGdldFNpdGVNZW1iZXJzKHNpdGVJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxTaXRlRW50cnkgfCB7fT4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldFNpdGUoc2l0ZUlkLCB7IHJlbGF0aW9uczogWydtZW1iZXJzJ10gfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSB1c2VybmFtZSBvZiB0aGUgdXNlciBjdXJyZW50bHkgbG9nZ2VkIGludG8gQUNTLlxyXG4gICAgICogQHJldHVybnMgVXNlcm5hbWUgc3RyaW5nXHJcbiAgICAgKi9cclxuICAgIGdldEVjbUN1cnJlbnRMb2dnZWRVc2VyTmFtZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5nZXRFY21Vc2VybmFtZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaGFuZGxlRXJyb3IoZXJyb3I6IGFueSk6IGFueSB7XHJcbiAgICAgICAgY29uc29sZS5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IgfHwgJ1NlcnZlciBlcnJvcicpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==