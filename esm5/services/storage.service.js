/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import * as i0 from "@angular/core";
var StorageService = /** @class */ (function () {
    function StorageService() {
        this.memoryStore = {};
        this.useLocalStorage = false;
        this._prefix = '';
        this.useLocalStorage = this.storageAvailable('localStorage');
    }
    Object.defineProperty(StorageService.prototype, "prefix", {
        get: /**
         * @return {?}
         */
        function () {
            return this._prefix;
        },
        set: /**
         * @param {?} prefix
         * @return {?}
         */
        function (prefix) {
            this._prefix = prefix ? prefix + '_' : '';
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Gets an item.
     * @param key Key to identify the item
     * @returns The item (if any) retrieved by the key
     */
    /**
     * Gets an item.
     * @param {?} key Key to identify the item
     * @return {?} The item (if any) retrieved by the key
     */
    StorageService.prototype.getItem = /**
     * Gets an item.
     * @param {?} key Key to identify the item
     * @return {?} The item (if any) retrieved by the key
     */
    function (key) {
        if (this.useLocalStorage) {
            return localStorage.getItem(this.prefix + key);
        }
        else {
            return this.memoryStore.hasOwnProperty(this.prefix + key) ? this.memoryStore[this.prefix + key] : null;
        }
    };
    /**
     * Stores an item
     * @param key Key to identify the item
     * @param data Data to store
     */
    /**
     * Stores an item
     * @param {?} key Key to identify the item
     * @param {?} data Data to store
     * @return {?}
     */
    StorageService.prototype.setItem = /**
     * Stores an item
     * @param {?} key Key to identify the item
     * @param {?} data Data to store
     * @return {?}
     */
    function (key, data) {
        if (this.useLocalStorage) {
            localStorage.setItem(this.prefix + key, data);
        }
        else {
            this.memoryStore[this.prefix + key] = data.toString();
        }
    };
    /** Removes all currently stored items. */
    /**
     * Removes all currently stored items.
     * @return {?}
     */
    StorageService.prototype.clear = /**
     * Removes all currently stored items.
     * @return {?}
     */
    function () {
        if (this.useLocalStorage) {
            localStorage.clear();
        }
        else {
            this.memoryStore = {};
        }
    };
    /**
     * Removes a single item.
     * @param key Key to identify the item
     */
    /**
     * Removes a single item.
     * @param {?} key Key to identify the item
     * @return {?}
     */
    StorageService.prototype.removeItem = /**
     * Removes a single item.
     * @param {?} key Key to identify the item
     * @return {?}
     */
    function (key) {
        if (this.useLocalStorage) {
            localStorage.removeItem(this.prefix + key);
        }
        else {
            delete this.memoryStore[this.prefix + key];
        }
    };
    /**
     * Is any item currently stored under `key`?
     * @param key Key identifying item to check
     * @returns True if key retrieves an item, false otherwise
     */
    /**
     * Is any item currently stored under `key`?
     * @param {?} key Key identifying item to check
     * @return {?} True if key retrieves an item, false otherwise
     */
    StorageService.prototype.hasItem = /**
     * Is any item currently stored under `key`?
     * @param {?} key Key identifying item to check
     * @return {?} True if key retrieves an item, false otherwise
     */
    function (key) {
        if (this.useLocalStorage) {
            return localStorage.getItem(this.prefix + key) ? true : false;
        }
        else {
            return this.memoryStore.hasOwnProperty(key);
        }
    };
    /**
     * @private
     * @param {?} type
     * @return {?}
     */
    StorageService.prototype.storageAvailable = /**
     * @private
     * @param {?} type
     * @return {?}
     */
    function (type) {
        try {
            /** @type {?} */
            var storage = window[type];
            /** @type {?} */
            var key = '__storage_test__';
            storage.setItem(key, key);
            storage.removeItem(key, key);
            return true;
        }
        catch (e) {
            return false;
        }
    };
    StorageService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    StorageService.ctorParameters = function () { return []; };
    /** @nocollapse */ StorageService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function StorageService_Factory() { return new StorageService(); }, token: StorageService, providedIn: "root" });
    return StorageService;
}());
export { StorageService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    StorageService.prototype.memoryStore;
    /**
     * @type {?}
     * @private
     */
    StorageService.prototype.useLocalStorage;
    /**
     * @type {?}
     * @private
     */
    StorageService.prototype._prefix;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic3RvcmFnZS5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvc3RvcmFnZS5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7O0FBRTNDO0lBaUJJO1FBWlEsZ0JBQVcsR0FBMkIsRUFBRSxDQUFDO1FBQ3pDLG9CQUFlLEdBQVksS0FBSyxDQUFDO1FBQ2pDLFlBQU8sR0FBVyxFQUFFLENBQUM7UUFXekIsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsY0FBYyxDQUFDLENBQUM7SUFDakUsQ0FBQztJQVZELHNCQUFJLGtDQUFNOzs7O1FBQVY7WUFDSSxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDeEIsQ0FBQzs7Ozs7UUFFRCxVQUFXLE1BQWM7WUFDckIsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUMsQ0FBQyxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQztRQUM5QyxDQUFDOzs7T0FKQTtJQVVEOzs7O09BSUc7Ozs7OztJQUNILGdDQUFPOzs7OztJQUFQLFVBQVEsR0FBVztRQUNmLElBQUksSUFBSSxDQUFDLGVBQWUsRUFBRTtZQUN0QixPQUFPLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsQ0FBQztTQUNsRDthQUFNO1lBQ0gsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztTQUMxRztJQUNMLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsZ0NBQU87Ozs7OztJQUFQLFVBQVEsR0FBVyxFQUFFLElBQVk7UUFDN0IsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3RCLFlBQVksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLEVBQUUsSUFBSSxDQUFDLENBQUM7U0FDakQ7YUFBTTtZQUNILElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDekQ7SUFDTCxDQUFDO0lBRUQsMENBQTBDOzs7OztJQUMxQyw4QkFBSzs7OztJQUFMO1FBQ0ksSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3RCLFlBQVksQ0FBQyxLQUFLLEVBQUUsQ0FBQztTQUN4QjthQUFNO1lBQ0gsSUFBSSxDQUFDLFdBQVcsR0FBRyxFQUFFLENBQUM7U0FDekI7SUFDTCxDQUFDO0lBRUQ7OztPQUdHOzs7Ozs7SUFDSCxtQ0FBVTs7Ozs7SUFBVixVQUFXLEdBQVc7UUFDbEIsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3RCLFlBQVksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUMsQ0FBQztTQUM5QzthQUFNO1lBQ0gsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUM7U0FDOUM7SUFDTCxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsZ0NBQU87Ozs7O0lBQVAsVUFBUSxHQUFXO1FBQ2YsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3RCLE9BQU8sWUFBWSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztTQUNqRTthQUFNO1lBQ0gsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLGNBQWMsQ0FBQyxHQUFHLENBQUMsQ0FBQztTQUMvQztJQUNMLENBQUM7Ozs7OztJQUVPLHlDQUFnQjs7Ozs7SUFBeEIsVUFBeUIsSUFBWTtRQUNqQyxJQUFJOztnQkFDTSxPQUFPLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQzs7Z0JBQ3RCLEdBQUcsR0FBRyxrQkFBa0I7WUFDOUIsT0FBTyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDMUIsT0FBTyxDQUFDLFVBQVUsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLENBQUM7WUFDN0IsT0FBTyxJQUFJLENBQUM7U0FDZjtRQUFDLE9BQU8sQ0FBQyxFQUFFO1lBQ1IsT0FBTyxLQUFLLENBQUM7U0FDaEI7SUFDTCxDQUFDOztnQkEzRkosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7Ozs7eUJBckJEO0NBK0dDLEFBNUZELElBNEZDO1NBekZZLGNBQWM7Ozs7OztJQUV2QixxQ0FBaUQ7Ozs7O0lBQ2pELHlDQUF5Qzs7Ozs7SUFDekMsaUNBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgU3RvcmFnZVNlcnZpY2Uge1xyXG5cclxuICAgIHByaXZhdGUgbWVtb3J5U3RvcmU6IHsgW2tleTogc3RyaW5nXTogYW55IH0gPSB7fTtcclxuICAgIHByaXZhdGUgdXNlTG9jYWxTdG9yYWdlOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBwcml2YXRlIF9wcmVmaXg6IHN0cmluZyA9ICcnO1xyXG5cclxuICAgIGdldCBwcmVmaXgoKSB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX3ByZWZpeDtcclxuICAgIH1cclxuXHJcbiAgICBzZXQgcHJlZml4KHByZWZpeDogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5fcHJlZml4ID0gcHJlZml4ID8gcHJlZml4ICsgJ18nIDogJyc7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoKSB7XHJcbiAgICAgICAgdGhpcy51c2VMb2NhbFN0b3JhZ2UgPSB0aGlzLnN0b3JhZ2VBdmFpbGFibGUoJ2xvY2FsU3RvcmFnZScpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhbiBpdGVtLlxyXG4gICAgICogQHBhcmFtIGtleSBLZXkgdG8gaWRlbnRpZnkgdGhlIGl0ZW1cclxuICAgICAqIEByZXR1cm5zIFRoZSBpdGVtIChpZiBhbnkpIHJldHJpZXZlZCBieSB0aGUga2V5XHJcbiAgICAgKi9cclxuICAgIGdldEl0ZW0oa2V5OiBzdHJpbmcpOiBzdHJpbmcgfCBudWxsIHtcclxuICAgICAgICBpZiAodGhpcy51c2VMb2NhbFN0b3JhZ2UpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGxvY2FsU3RvcmFnZS5nZXRJdGVtKHRoaXMucHJlZml4ICsga2V5KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5tZW1vcnlTdG9yZS5oYXNPd25Qcm9wZXJ0eSh0aGlzLnByZWZpeCArIGtleSkgPyB0aGlzLm1lbW9yeVN0b3JlW3RoaXMucHJlZml4ICsga2V5XSA6IG51bGw7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU3RvcmVzIGFuIGl0ZW1cclxuICAgICAqIEBwYXJhbSBrZXkgS2V5IHRvIGlkZW50aWZ5IHRoZSBpdGVtXHJcbiAgICAgKiBAcGFyYW0gZGF0YSBEYXRhIHRvIHN0b3JlXHJcbiAgICAgKi9cclxuICAgIHNldEl0ZW0oa2V5OiBzdHJpbmcsIGRhdGE6IHN0cmluZykge1xyXG4gICAgICAgIGlmICh0aGlzLnVzZUxvY2FsU3RvcmFnZSkge1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2Uuc2V0SXRlbSh0aGlzLnByZWZpeCArIGtleSwgZGF0YSk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5tZW1vcnlTdG9yZVt0aGlzLnByZWZpeCArIGtleV0gPSBkYXRhLnRvU3RyaW5nKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKiBSZW1vdmVzIGFsbCBjdXJyZW50bHkgc3RvcmVkIGl0ZW1zLiAqL1xyXG4gICAgY2xlYXIoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudXNlTG9jYWxTdG9yYWdlKSB7XHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5jbGVhcigpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMubWVtb3J5U3RvcmUgPSB7fTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZW1vdmVzIGEgc2luZ2xlIGl0ZW0uXHJcbiAgICAgKiBAcGFyYW0ga2V5IEtleSB0byBpZGVudGlmeSB0aGUgaXRlbVxyXG4gICAgICovXHJcbiAgICByZW1vdmVJdGVtKGtleTogc3RyaW5nKSB7XHJcbiAgICAgICAgaWYgKHRoaXMudXNlTG9jYWxTdG9yYWdlKSB7XHJcbiAgICAgICAgICAgIGxvY2FsU3RvcmFnZS5yZW1vdmVJdGVtKHRoaXMucHJlZml4ICsga2V5KTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBkZWxldGUgdGhpcy5tZW1vcnlTdG9yZVt0aGlzLnByZWZpeCArIGtleV07XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogSXMgYW55IGl0ZW0gY3VycmVudGx5IHN0b3JlZCB1bmRlciBga2V5YD9cclxuICAgICAqIEBwYXJhbSBrZXkgS2V5IGlkZW50aWZ5aW5nIGl0ZW0gdG8gY2hlY2tcclxuICAgICAqIEByZXR1cm5zIFRydWUgaWYga2V5IHJldHJpZXZlcyBhbiBpdGVtLCBmYWxzZSBvdGhlcndpc2VcclxuICAgICAqL1xyXG4gICAgaGFzSXRlbShrZXk6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIGlmICh0aGlzLnVzZUxvY2FsU3RvcmFnZSkge1xyXG4gICAgICAgICAgICByZXR1cm4gbG9jYWxTdG9yYWdlLmdldEl0ZW0odGhpcy5wcmVmaXggKyBrZXkpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLm1lbW9yeVN0b3JlLmhhc093blByb3BlcnR5KGtleSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgc3RvcmFnZUF2YWlsYWJsZSh0eXBlOiBzdHJpbmcpOiBib29sZWFuIHtcclxuICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICBjb25zdCBzdG9yYWdlID0gd2luZG93W3R5cGVdO1xyXG4gICAgICAgICAgICBjb25zdCBrZXkgPSAnX19zdG9yYWdlX3Rlc3RfXyc7XHJcbiAgICAgICAgICAgIHN0b3JhZ2Uuc2V0SXRlbShrZXksIGtleSk7XHJcbiAgICAgICAgICAgIHN0b3JhZ2UucmVtb3ZlSXRlbShrZXksIGtleSk7XHJcbiAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgIH0gY2F0Y2ggKGUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=