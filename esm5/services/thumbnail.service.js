/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/* spellchecker: disable */
import { Injectable } from '@angular/core';
import { MatIconRegistry } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { AlfrescoApiService } from './alfresco-api.service';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "@angular/material/icon";
import * as i3 from "@angular/platform-browser";
var ThumbnailService = /** @class */ (function () {
    function ThumbnailService(apiService, matIconRegistry, sanitizer) {
        var _this = this;
        this.apiService = apiService;
        this.DEFAULT_ICON = './assets/images/ft_ic_miscellaneous.svg';
        this.mimeTypeIcons = {
            'image/png': './assets/images/ft_ic_raster_image.svg',
            'image/jpeg': './assets/images/ft_ic_raster_image.svg',
            'image/gif': './assets/images/ft_ic_raster_image.svg',
            'image/bmp': './assets/images/ft_ic_raster_image.svg',
            'image/cgm': './assets/images/ft_ic_raster_image.svg',
            'image/ief': './assets/images/ft_ic_raster_image.svg',
            'image/jp2': './assets/images/ft_ic_raster_image.svg',
            'image/tiff': './assets/images/ft_ic_raster_image.svg',
            'image/vnd.adobe.photoshop': './assets/images/ft_ic_raster_image.svg',
            'image/vnd.adobe.premiere': './assets/images/ft_ic_raster_image.svg',
            'image/x-cmu-raster': './assets/images/ft_ic_raster_image.svg',
            'image/x-dwt': './assets/images/ft_ic_raster_image.svg',
            'image/x-portable-anymap': './assets/images/ft_ic_raster_image.svg',
            'image/x-portable-bitmap': './assets/images/ft_ic_raster_image.svg',
            'image/x-portable-graymap': './assets/images/ft_ic_raster_image.svg',
            'image/x-portable-pixmap': './assets/images/ft_ic_raster_image.svg',
            'image/x-raw-adobe': './assets/images/ft_ic_raster_image.svg',
            'image/x-raw-canon': './assets/images/ft_ic_raster_image.svg',
            'image/x-raw-fuji': './assets/images/ft_ic_raster_image.svg',
            'image/x-raw-hasselblad': './assets/images/ft_ic_raster_image.svg',
            'image/x-raw-kodak': './assets/images/ft_ic_raster_image.svg',
            'image/x-raw-leica': './assets/images/ft_ic_raster_image.svg',
            'image/x-raw-minolta': './assets/images/ft_ic_raster_image.svg',
            'image/x-raw-nikon': './assets/images/ft_ic_raster_image.svg',
            'image/x-raw-olympus': './assets/images/ft_ic_raster_image.svg',
            'image/x-raw-panasonic': './assets/images/ft_ic_raster_image.svg',
            'image/x-raw-pentax': './assets/images/ft_ic_raster_image.svg',
            'image/x-raw-red': './assets/images/ft_ic_raster_image.svg',
            'image/x-raw-sigma': './assets/images/ft_ic_raster_image.svg',
            'image/x-raw-sony': './assets/images/ft_ic_raster_image.svg',
            'image/x-xbitmap': './assets/images/ft_ic_raster_image.svg',
            'image/x-xpixmap': './assets/images/ft_ic_raster_image.svg',
            'image/x-xwindowdump': './assets/images/ft_ic_raster_image.svg',
            'image/svg+xml': './assets/images/ft_ic_vector_image.svg',
            'application/eps': './assets/images/ft_ic_raster_image.svg',
            'application/illustrator': './assets/images/ft_ic_raster_image.svg',
            'application/pdf': './assets/images/ft_ic_pdf.svg',
            'application/vnd.ms-excel': './assets/images/ft_ic_ms_excel.svg',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': './assets/images/ft_ic_ms_excel.svg',
            'application/vnd.openxmlformats-officedocument.spreadsheetml.template': './assets/images/ft_ic_ms_excel.svg',
            'application/vnd.ms-excel.addin.macroenabled.12': './assets/images/ft_ic_ms_excel.svg',
            'application/vnd.ms-excel.sheet.binary.macroenabled.12': './assets/images/ft_ic_ms_excel.svg',
            'application/vnd.ms-excel.sheet.macroenabled.12': './assets/images/ft_ic_ms_excel.svg',
            'application/vnd.ms-excel.template.macroenabled.12': './assets/images/ft_ic_ms_excel.svg',
            'application/vnd.sun.xml.calc': './assets/images/ft_ic_ms_excel.svg',
            'application/vnd.sun.xml.calc.template': './assets/images/ft_ic_ms_excel.svg',
            'application/vnd.ms-outlook': './assets/images/ft_ic_document.svg',
            'application/msword': './assets/images/ft_ic_ms_word.svg',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document': './assets/images/ft_ic_ms_word.svg',
            'application/vnd.openxmlformats-officedocument.wordprocessingml.template': './assets/images/ft_ic_ms_word.svg',
            'application/vnd.ms-word.document.macroenabled.12': './assets/images/ft_ic_ms_word.svg',
            'application/vnd.ms-word.template.macroenabled.12': './assets/images/ft_ic_ms_word.svg',
            'application/vnd.sun.xml.writer': './assets/images/ft_ic_ms_word.svg',
            'application/vnd.sun.xml.writer.template': './assets/images/ft_ic_ms_word.svg',
            'application/rtf': './assets/images/ft_ic_ms_word.svg',
            'text/rtf': './assets/images/ft_ic_ms_word.svg',
            'application/vnd.ms-powerpoint': './assets/images/ft_ic_ms_powerpoint.svg',
            'application/vnd.openxmlformats-officedocument.presentationml.presentation': './assets/images/ft_ic_ms_powerpoint.svg',
            'application/vnd.openxmlformats-officedocument.presentationml.template': './assets/images/ft_ic_ms_powerpoint.svg',
            'application/vnd.openxmlformats-officedocument.presentationml.slideshow': './assets/images/ft_ic_ms_powerpoint.svg',
            'application/vnd.oasis.opendocument.presentation': './assets/images/ft_ic_ms_powerpoint.svg',
            'application/vnd.oasis.opendocument.presentation-template': './assets/images/ft_ic_ms_powerpoint.svg',
            'application/vnd.openxmlformats-officedocument.presentationml.slide': './assets/images/ft_ic_ms_powerpoint.svg',
            'application/vnd.sun.xml.impress': './assets/images/ft_ic_ms_powerpoint.svg',
            'application/vnd.sun.xml.impress.template': './assets/images/ft_ic_ms_powerpoint.svg',
            'application/vnd.oasis.opendocument.spreadsheet': './assets/images/ft_ic_ms_excel.svg',
            'application/vnd.oasis.opendocument.spreadsheet-template': './assets/images/ft_ic_ms_excel.svg',
            'application/vnd.ms-powerpoint.addin.macroenabled.12': './assets/images/ft_ic_ms_powerpoint.svg',
            'application/vnd.ms-powerpoint.presentation.macroenabled.12': './assets/images/ft_ic_ms_powerpoint.svg',
            'application/vnd.ms-powerpoint.slide.macroenabled.12': './assets/images/ft_ic_ms_powerpoint.svg',
            'application/vnd.ms-powerpoint.slideshow.macroenabled.12': './assets/images/ft_ic_ms_powerpoint.svg',
            'application/vnd.ms-powerpoint.template.macroenabled.12': './assets/images/ft_ic_ms_powerpoint.svg',
            'video/mp4': './assets/images/ft_ic_video.svg',
            'video/3gpp': './assets/images/ft_ic_video.svg',
            'video/3gpp2': './assets/images/ft_ic_video.svg',
            'video/mp2t': './assets/images/ft_ic_video.svg',
            'video/mpeg': './assets/images/ft_ic_video.svg',
            'video/mpeg2': './assets/images/ft_ic_video.svg',
            'video/ogg': './assets/images/ft_ic_video.svg',
            'video/quicktime': './assets/images/ft_ic_video.svg',
            'video/webm': './assets/images/ft_ic_video.svg',
            'video/x-flv': './assets/images/ft_ic_video.svg',
            'video/x-m4v': './assets/images/ft_ic_video.svg',
            'video/x-ms-asf': './assets/images/ft_ic_video.svg',
            'video/x-ms-wmv': './assets/images/ft_ic_video.svg',
            'video/x-msvideo': './assets/images/ft_ic_video.svg',
            'video/x-rad-screenplay': './assets/images/ft_ic_video.svg',
            'video/x-sgi-movie': './assets/images/ft_ic_video.svg',
            'video/x-matroska': './assets/images/ft_ic_video.svg',
            'audio/mpeg': './assets/images/ft_ic_audio.svg',
            'audio/ogg': './assets/images/ft_ic_audio.svg',
            'audio/wav': './assets/images/ft_ic_audio.svg',
            'audio/basic': './assets/images/ft_ic_audio.svg',
            'audio/mp4': './assets/images/ft_ic_audio.svg',
            'audio/vnd.adobe.soundbooth': './assets/images/ft_ic_audio.svg',
            'audio/vorbis': './assets/images/ft_ic_audio.svg',
            'audio/x-aiff': './assets/images/ft_ic_audio.svg',
            'audio/x-flac': './assets/images/ft_ic_audio.svg',
            'audio/x-ms-wma': './assets/images/ft_ic_audio.svg',
            'audio/x-wav': './assets/images/ft_ic_audio.svg',
            'x-world/x-vrml': './assets/images/ft_ic_video.svg',
            'text/plain': './assets/images/ft_ic_document.svg',
            'application/vnd.oasis.opendocument.text': './assets/images/ft_ic_document.svg',
            'application/vnd.oasis.opendocument.text-template': './assets/images/ft_ic_document.svg',
            'application/x-javascript': './assets/images/ft_ic_document.svg',
            'application/json': './assets/images/ft_ic_document.svg',
            'text/csv': './assets/images/ft_ic_document.svg',
            'text/xml': './assets/images/ft_ic_document.svg',
            'text/html': './assets/images/ft_ic_website.svg',
            'application/x-compressed': './assets/images/ft_ic_archive.svg',
            'application/x-zip-compressed': './assets/images/ft_ic_archive.svg',
            'application/zip': './assets/images/ft_ic_archive.svg',
            'application/x-tar': './assets/images/ft_ic_archive.svg',
            'application/vnd.apple.keynote': './assets/images/ft_ic_presentation.svg',
            'application/vnd.apple.pages': './assets/images/ft_ic_document.svg',
            'application/vnd.apple.numbers': './assets/images/ft_ic_spreadsheet.svg',
            'application/vnd.visio': './assets/images/ft_ic_document.svg',
            'application/wordperfect': './assets/images/ft_ic_document.svg',
            'application/x-cpio': './assets/images/ft_ic_document.svg',
            'folder': './assets/images/ft_ic_folder.svg',
            'smartFolder': './assets/images/ft_ic_smart_folder.svg',
            'ruleFolder': './assets/images/ft_ic_folder_rule.svg',
            'linkFolder': './assets/images/ft_ic_folder_shortcut_link.svg',
            'disable/folder': './assets/images/ft_ic_folder_disable.svg',
            'selected': './assets/images/ft_ic_selected.svg'
        };
        Object.keys(this.mimeTypeIcons).forEach((/**
         * @param {?} key
         * @return {?}
         */
        function (key) {
            /** @type {?} */
            var url = sanitizer.bypassSecurityTrustResourceUrl(_this.mimeTypeIcons[key]);
            matIconRegistry.addSvgIcon(key, url);
            matIconRegistry.addSvgIconInNamespace('adf', key, url);
        }));
    }
    /**
     * Gets a thumbnail URL for the given document node.
     * @param node Node or Node ID to get URL for.
     * @returns URL string
     */
    /**
     * Gets a thumbnail URL for the given document node.
     * @param {?} node Node or Node ID to get URL for.
     * @param {?=} attachment
     * @param {?=} ticket
     * @return {?} URL string
     */
    ThumbnailService.prototype.getDocumentThumbnailUrl = /**
     * Gets a thumbnail URL for the given document node.
     * @param {?} node Node or Node ID to get URL for.
     * @param {?=} attachment
     * @param {?=} ticket
     * @return {?} URL string
     */
    function (node, attachment, ticket) {
        /** @type {?} */
        var resultUrl;
        if (node) {
            /** @type {?} */
            var nodeId = void 0;
            if (typeof node === 'string') {
                nodeId = node;
            }
            else if (node.entry) {
                nodeId = node.entry.id;
            }
            resultUrl = this.apiService.contentApi.getDocumentThumbnailUrl(nodeId, attachment, ticket);
        }
        return resultUrl || this.DEFAULT_ICON;
    };
    /**
     * Gets a thumbnail URL for a MIME type.
     * @param mimeType MIME type for the thumbnail
     * @returns URL string
     */
    /**
     * Gets a thumbnail URL for a MIME type.
     * @param {?} mimeType MIME type for the thumbnail
     * @return {?} URL string
     */
    ThumbnailService.prototype.getMimeTypeIcon = /**
     * Gets a thumbnail URL for a MIME type.
     * @param {?} mimeType MIME type for the thumbnail
     * @return {?} URL string
     */
    function (mimeType) {
        /** @type {?} */
        var icon = this.mimeTypeIcons[mimeType];
        return (icon || this.DEFAULT_ICON);
    };
    /**
     * Gets a "miscellaneous" thumbnail URL for types with no other icon defined.
     * @returns URL string
     */
    /**
     * Gets a "miscellaneous" thumbnail URL for types with no other icon defined.
     * @return {?} URL string
     */
    ThumbnailService.prototype.getDefaultMimeTypeIcon = /**
     * Gets a "miscellaneous" thumbnail URL for types with no other icon defined.
     * @return {?} URL string
     */
    function () {
        return this.DEFAULT_ICON;
    };
    ThumbnailService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    ThumbnailService.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: MatIconRegistry },
        { type: DomSanitizer }
    ]; };
    /** @nocollapse */ ThumbnailService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ThumbnailService_Factory() { return new ThumbnailService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.MatIconRegistry), i0.ɵɵinject(i3.DomSanitizer)); }, token: ThumbnailService, providedIn: "root" });
    return ThumbnailService;
}());
export { ThumbnailService };
if (false) {
    /** @type {?} */
    ThumbnailService.prototype.DEFAULT_ICON;
    /** @type {?} */
    ThumbnailService.prototype.mimeTypeIcons;
    /**
     * @type {?}
     * @protected
     */
    ThumbnailService.prototype.apiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidGh1bWJuYWlsLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy90aHVtYm5haWwuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFrQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQUUsZUFBZSxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDcEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3pELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHdCQUF3QixDQUFDOzs7OztBQUc1RDtJQXVJSSwwQkFBc0IsVUFBOEIsRUFBRSxlQUFnQyxFQUFFLFNBQXVCO1FBQS9HLGlCQU9DO1FBUHFCLGVBQVUsR0FBVixVQUFVLENBQW9CO1FBbElwRCxpQkFBWSxHQUFXLHlDQUF5QyxDQUFDO1FBRWpFLGtCQUFhLEdBQVE7WUFDakIsV0FBVyxFQUFFLHdDQUF3QztZQUNyRCxZQUFZLEVBQUUsd0NBQXdDO1lBQ3RELFdBQVcsRUFBRSx3Q0FBd0M7WUFDckQsV0FBVyxFQUFFLHdDQUF3QztZQUNyRCxXQUFXLEVBQUUsd0NBQXdDO1lBQ3JELFdBQVcsRUFBRSx3Q0FBd0M7WUFDckQsV0FBVyxFQUFFLHdDQUF3QztZQUNyRCxZQUFZLEVBQUUsd0NBQXdDO1lBQ3RELDJCQUEyQixFQUFFLHdDQUF3QztZQUNyRSwwQkFBMEIsRUFBRSx3Q0FBd0M7WUFDcEUsb0JBQW9CLEVBQUUsd0NBQXdDO1lBQzlELGFBQWEsRUFBRSx3Q0FBd0M7WUFDdkQseUJBQXlCLEVBQUUsd0NBQXdDO1lBQ25FLHlCQUF5QixFQUFFLHdDQUF3QztZQUNuRSwwQkFBMEIsRUFBRSx3Q0FBd0M7WUFDcEUseUJBQXlCLEVBQUUsd0NBQXdDO1lBQ25FLG1CQUFtQixFQUFFLHdDQUF3QztZQUM3RCxtQkFBbUIsRUFBRSx3Q0FBd0M7WUFDN0Qsa0JBQWtCLEVBQUUsd0NBQXdDO1lBQzVELHdCQUF3QixFQUFFLHdDQUF3QztZQUNsRSxtQkFBbUIsRUFBRSx3Q0FBd0M7WUFDN0QsbUJBQW1CLEVBQUUsd0NBQXdDO1lBQzdELHFCQUFxQixFQUFFLHdDQUF3QztZQUMvRCxtQkFBbUIsRUFBRSx3Q0FBd0M7WUFDN0QscUJBQXFCLEVBQUUsd0NBQXdDO1lBQy9ELHVCQUF1QixFQUFFLHdDQUF3QztZQUNqRSxvQkFBb0IsRUFBRSx3Q0FBd0M7WUFDOUQsaUJBQWlCLEVBQUUsd0NBQXdDO1lBQzNELG1CQUFtQixFQUFFLHdDQUF3QztZQUM3RCxrQkFBa0IsRUFBRSx3Q0FBd0M7WUFDNUQsaUJBQWlCLEVBQUUsd0NBQXdDO1lBQzNELGlCQUFpQixFQUFFLHdDQUF3QztZQUMzRCxxQkFBcUIsRUFBRSx3Q0FBd0M7WUFDL0QsZUFBZSxFQUFFLHdDQUF3QztZQUN6RCxpQkFBaUIsRUFBRSx3Q0FBd0M7WUFDM0QseUJBQXlCLEVBQUUsd0NBQXdDO1lBQ25FLGlCQUFpQixFQUFFLCtCQUErQjtZQUNsRCwwQkFBMEIsRUFBRSxvQ0FBb0M7WUFDaEUsbUVBQW1FLEVBQUUsb0NBQW9DO1lBQ3pHLHNFQUFzRSxFQUFFLG9DQUFvQztZQUM1RyxnREFBZ0QsRUFBRSxvQ0FBb0M7WUFDdEYsdURBQXVELEVBQUUsb0NBQW9DO1lBQzdGLGdEQUFnRCxFQUFFLG9DQUFvQztZQUN0RixtREFBbUQsRUFBRSxvQ0FBb0M7WUFDekYsOEJBQThCLEVBQUUsb0NBQW9DO1lBQ3BFLHVDQUF1QyxFQUFFLG9DQUFvQztZQUM3RSw0QkFBNEIsRUFBRSxvQ0FBb0M7WUFDbEUsb0JBQW9CLEVBQUUsbUNBQW1DO1lBQ3pELHlFQUF5RSxFQUFFLG1DQUFtQztZQUM5Ryx5RUFBeUUsRUFBRSxtQ0FBbUM7WUFDOUcsa0RBQWtELEVBQUUsbUNBQW1DO1lBQ3ZGLGtEQUFrRCxFQUFFLG1DQUFtQztZQUN2RixnQ0FBZ0MsRUFBRSxtQ0FBbUM7WUFDckUseUNBQXlDLEVBQUUsbUNBQW1DO1lBQzlFLGlCQUFpQixFQUFFLG1DQUFtQztZQUN0RCxVQUFVLEVBQUUsbUNBQW1DO1lBQy9DLCtCQUErQixFQUFFLHlDQUF5QztZQUMxRSwyRUFBMkUsRUFBRSx5Q0FBeUM7WUFDdEgsdUVBQXVFLEVBQUUseUNBQXlDO1lBQ2xILHdFQUF3RSxFQUFFLHlDQUF5QztZQUNuSCxpREFBaUQsRUFBRSx5Q0FBeUM7WUFDNUYsMERBQTBELEVBQUUseUNBQXlDO1lBQ3JHLG9FQUFvRSxFQUFFLHlDQUF5QztZQUMvRyxpQ0FBaUMsRUFBRSx5Q0FBeUM7WUFDNUUsMENBQTBDLEVBQUUseUNBQXlDO1lBQ3JGLGdEQUFnRCxFQUFFLG9DQUFvQztZQUN0Rix5REFBeUQsRUFBRSxvQ0FBb0M7WUFDL0YscURBQXFELEVBQUUseUNBQXlDO1lBQ2hHLDREQUE0RCxFQUFFLHlDQUF5QztZQUN2RyxxREFBcUQsRUFBRSx5Q0FBeUM7WUFDaEcseURBQXlELEVBQUUseUNBQXlDO1lBQ3BHLHdEQUF3RCxFQUFFLHlDQUF5QztZQUNuRyxXQUFXLEVBQUUsaUNBQWlDO1lBQzlDLFlBQVksRUFBRSxpQ0FBaUM7WUFDL0MsYUFBYSxFQUFFLGlDQUFpQztZQUNoRCxZQUFZLEVBQUUsaUNBQWlDO1lBQy9DLFlBQVksRUFBRSxpQ0FBaUM7WUFDL0MsYUFBYSxFQUFFLGlDQUFpQztZQUNoRCxXQUFXLEVBQUUsaUNBQWlDO1lBQzlDLGlCQUFpQixFQUFFLGlDQUFpQztZQUNwRCxZQUFZLEVBQUUsaUNBQWlDO1lBQy9DLGFBQWEsRUFBRSxpQ0FBaUM7WUFDaEQsYUFBYSxFQUFFLGlDQUFpQztZQUNoRCxnQkFBZ0IsRUFBRSxpQ0FBaUM7WUFDbkQsZ0JBQWdCLEVBQUUsaUNBQWlDO1lBQ25ELGlCQUFpQixFQUFFLGlDQUFpQztZQUNwRCx3QkFBd0IsRUFBRyxpQ0FBaUM7WUFDNUQsbUJBQW1CLEVBQUUsaUNBQWlDO1lBQ3RELGtCQUFrQixFQUFFLGlDQUFpQztZQUNyRCxZQUFZLEVBQUUsaUNBQWlDO1lBQy9DLFdBQVcsRUFBRSxpQ0FBaUM7WUFDOUMsV0FBVyxFQUFFLGlDQUFpQztZQUM5QyxhQUFhLEVBQUUsaUNBQWlDO1lBQ2hELFdBQVcsRUFBRSxpQ0FBaUM7WUFDOUMsNEJBQTRCLEVBQUUsaUNBQWlDO1lBQy9ELGNBQWMsRUFBRSxpQ0FBaUM7WUFDakQsY0FBYyxFQUFFLGlDQUFpQztZQUNqRCxjQUFjLEVBQUUsaUNBQWlDO1lBQ2pELGdCQUFnQixFQUFFLGlDQUFpQztZQUNuRCxhQUFhLEVBQUUsaUNBQWlDO1lBQ2hELGdCQUFnQixFQUFFLGlDQUFpQztZQUNuRCxZQUFZLEVBQUUsb0NBQW9DO1lBQ2xELHlDQUF5QyxFQUFFLG9DQUFvQztZQUMvRSxrREFBa0QsRUFBRSxvQ0FBb0M7WUFDeEYsMEJBQTBCLEVBQUUsb0NBQW9DO1lBQ2hFLGtCQUFrQixFQUFFLG9DQUFvQztZQUN4RCxVQUFVLEVBQUUsb0NBQW9DO1lBQ2hELFVBQVUsRUFBRSxvQ0FBb0M7WUFDaEQsV0FBVyxFQUFFLG1DQUFtQztZQUNoRCwwQkFBMEIsRUFBRSxtQ0FBbUM7WUFDL0QsOEJBQThCLEVBQUUsbUNBQW1DO1lBQ25FLGlCQUFpQixFQUFFLG1DQUFtQztZQUN0RCxtQkFBbUIsRUFBRSxtQ0FBbUM7WUFDeEQsK0JBQStCLEVBQUUsd0NBQXdDO1lBQ3pFLDZCQUE2QixFQUFFLG9DQUFvQztZQUNuRSwrQkFBK0IsRUFBRSx1Q0FBdUM7WUFDeEUsdUJBQXVCLEVBQUUsb0NBQW9DO1lBQzdELHlCQUF5QixFQUFFLG9DQUFvQztZQUMvRCxvQkFBb0IsRUFBRSxvQ0FBb0M7WUFDMUQsUUFBUSxFQUFFLGtDQUFrQztZQUM1QyxhQUFhLEVBQUUsd0NBQXdDO1lBQ3ZELFlBQVksRUFBRSx1Q0FBdUM7WUFDckQsWUFBWSxFQUFFLGdEQUFnRDtZQUM5RCxnQkFBZ0IsRUFBRSwwQ0FBMEM7WUFDNUQsVUFBVSxFQUFFLG9DQUFvQztTQUNuRCxDQUFDO1FBR0UsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUMsT0FBTzs7OztRQUFDLFVBQUMsR0FBRzs7Z0JBQ2xDLEdBQUcsR0FBRyxTQUFTLENBQUMsOEJBQThCLENBQUMsS0FBSSxDQUFDLGFBQWEsQ0FBQyxHQUFHLENBQUMsQ0FBQztZQUU3RSxlQUFlLENBQUMsVUFBVSxDQUFDLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztZQUNyQyxlQUFlLENBQUMscUJBQXFCLENBQUMsS0FBSyxFQUFFLEdBQUcsRUFBRSxHQUFHLENBQUMsQ0FBQztRQUMzRCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7OztJQUNILGtEQUF1Qjs7Ozs7OztJQUF2QixVQUF3QixJQUF3QixFQUFFLFVBQW9CLEVBQUUsTUFBZTs7WUFDL0UsU0FBaUI7UUFFckIsSUFBSSxJQUFJLEVBQUU7O2dCQUNGLE1BQU0sU0FBUTtZQUVsQixJQUFJLE9BQU8sSUFBSSxLQUFLLFFBQVEsRUFBRTtnQkFDMUIsTUFBTSxHQUFHLElBQUksQ0FBQzthQUNqQjtpQkFBTSxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUU7Z0JBQ25CLE1BQU0sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQzthQUMxQjtZQUVELFNBQVMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyx1QkFBdUIsQ0FBQyxNQUFNLEVBQUUsVUFBVSxFQUFFLE1BQU0sQ0FBQyxDQUFDO1NBQzlGO1FBRUQsT0FBTyxTQUFTLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQztJQUMxQyxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0ksMENBQWU7Ozs7O0lBQXRCLFVBQXVCLFFBQWdCOztZQUM3QixJQUFJLEdBQUcsSUFBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUM7UUFDekMsT0FBTyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7SUFDdkMsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7SUFDSSxpREFBc0I7Ozs7SUFBN0I7UUFDSSxPQUFPLElBQUksQ0FBQyxZQUFZLENBQUM7SUFDN0IsQ0FBQzs7Z0JBdkxKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7Z0JBTFEsa0JBQWtCO2dCQUZsQixlQUFlO2dCQUNmLFlBQVk7OzsyQkFwQnJCO0NBZ05DLEFBeExELElBd0xDO1NBckxZLGdCQUFnQjs7O0lBRXpCLHdDQUFpRTs7SUFFakUseUNBOEhFOzs7OztJQUVVLHNDQUF3QyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG4vKiBzcGVsbGNoZWNrZXI6IGRpc2FibGUgKi9cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXRJY29uUmVnaXN0cnkgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcbmltcG9ydCB7IERvbVNhbml0aXplciB9IGZyb20gJ0Bhbmd1bGFyL3BsYXRmb3JtLWJyb3dzZXInO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTm9kZUVudHJ5IH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFRodW1ibmFpbFNlcnZpY2Uge1xyXG5cclxuICAgIERFRkFVTFRfSUNPTjogc3RyaW5nID0gJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19taXNjZWxsYW5lb3VzLnN2Zyc7XHJcblxyXG4gICAgbWltZVR5cGVJY29uczogYW55ID0ge1xyXG4gICAgICAgICdpbWFnZS9wbmcnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3Jhc3Rlcl9pbWFnZS5zdmcnLFxyXG4gICAgICAgICdpbWFnZS9qcGVnJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UvZ2lmJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UvYm1wJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UvY2dtJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UvaWVmJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UvanAyJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UvdGlmZic6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfcmFzdGVyX2ltYWdlLnN2ZycsXHJcbiAgICAgICAgJ2ltYWdlL3ZuZC5hZG9iZS5waG90b3Nob3AnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3Jhc3Rlcl9pbWFnZS5zdmcnLFxyXG4gICAgICAgICdpbWFnZS92bmQuYWRvYmUucHJlbWllcmUnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3Jhc3Rlcl9pbWFnZS5zdmcnLFxyXG4gICAgICAgICdpbWFnZS94LWNtdS1yYXN0ZXInOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3Jhc3Rlcl9pbWFnZS5zdmcnLFxyXG4gICAgICAgICdpbWFnZS94LWR3dCc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfcmFzdGVyX2ltYWdlLnN2ZycsXHJcbiAgICAgICAgJ2ltYWdlL3gtcG9ydGFibGUtYW55bWFwJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UveC1wb3J0YWJsZS1iaXRtYXAnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3Jhc3Rlcl9pbWFnZS5zdmcnLFxyXG4gICAgICAgICdpbWFnZS94LXBvcnRhYmxlLWdyYXltYXAnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3Jhc3Rlcl9pbWFnZS5zdmcnLFxyXG4gICAgICAgICdpbWFnZS94LXBvcnRhYmxlLXBpeG1hcCc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfcmFzdGVyX2ltYWdlLnN2ZycsXHJcbiAgICAgICAgJ2ltYWdlL3gtcmF3LWFkb2JlJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UveC1yYXctY2Fub24nOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3Jhc3Rlcl9pbWFnZS5zdmcnLFxyXG4gICAgICAgICdpbWFnZS94LXJhdy1mdWppJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UveC1yYXctaGFzc2VsYmxhZCc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfcmFzdGVyX2ltYWdlLnN2ZycsXHJcbiAgICAgICAgJ2ltYWdlL3gtcmF3LWtvZGFrJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UveC1yYXctbGVpY2EnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3Jhc3Rlcl9pbWFnZS5zdmcnLFxyXG4gICAgICAgICdpbWFnZS94LXJhdy1taW5vbHRhJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UveC1yYXctbmlrb24nOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3Jhc3Rlcl9pbWFnZS5zdmcnLFxyXG4gICAgICAgICdpbWFnZS94LXJhdy1vbHltcHVzJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UveC1yYXctcGFuYXNvbmljJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UveC1yYXctcGVudGF4JzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UveC1yYXctcmVkJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UveC1yYXctc2lnbWEnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3Jhc3Rlcl9pbWFnZS5zdmcnLFxyXG4gICAgICAgICdpbWFnZS94LXJhdy1zb255JzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UveC14Yml0bWFwJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UveC14cGl4bWFwJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19yYXN0ZXJfaW1hZ2Uuc3ZnJyxcclxuICAgICAgICAnaW1hZ2UveC14d2luZG93ZHVtcCc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfcmFzdGVyX2ltYWdlLnN2ZycsXHJcbiAgICAgICAgJ2ltYWdlL3N2Zyt4bWwnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3ZlY3Rvcl9pbWFnZS5zdmcnLFxyXG4gICAgICAgICdhcHBsaWNhdGlvbi9lcHMnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3Jhc3Rlcl9pbWFnZS5zdmcnLFxyXG4gICAgICAgICdhcHBsaWNhdGlvbi9pbGx1c3RyYXRvcic6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfcmFzdGVyX2ltYWdlLnN2ZycsXHJcbiAgICAgICAgJ2FwcGxpY2F0aW9uL3BkZic6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfcGRmLnN2ZycsXHJcbiAgICAgICAgJ2FwcGxpY2F0aW9uL3ZuZC5tcy1leGNlbCc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfbXNfZXhjZWwuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLm9wZW54bWxmb3JtYXRzLW9mZmljZWRvY3VtZW50LnNwcmVhZHNoZWV0bWwuc2hlZXQnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX21zX2V4Y2VsLnN2ZycsXHJcbiAgICAgICAgJ2FwcGxpY2F0aW9uL3ZuZC5vcGVueG1sZm9ybWF0cy1vZmZpY2Vkb2N1bWVudC5zcHJlYWRzaGVldG1sLnRlbXBsYXRlJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19tc19leGNlbC5zdmcnLFxyXG4gICAgICAgICdhcHBsaWNhdGlvbi92bmQubXMtZXhjZWwuYWRkaW4ubWFjcm9lbmFibGVkLjEyJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19tc19leGNlbC5zdmcnLFxyXG4gICAgICAgICdhcHBsaWNhdGlvbi92bmQubXMtZXhjZWwuc2hlZXQuYmluYXJ5Lm1hY3JvZW5hYmxlZC4xMic6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfbXNfZXhjZWwuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLm1zLWV4Y2VsLnNoZWV0Lm1hY3JvZW5hYmxlZC4xMic6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfbXNfZXhjZWwuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLm1zLWV4Y2VsLnRlbXBsYXRlLm1hY3JvZW5hYmxlZC4xMic6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfbXNfZXhjZWwuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLnN1bi54bWwuY2FsYyc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfbXNfZXhjZWwuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLnN1bi54bWwuY2FsYy50ZW1wbGF0ZSc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfbXNfZXhjZWwuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLm1zLW91dGxvb2snOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX2RvY3VtZW50LnN2ZycsXHJcbiAgICAgICAgJ2FwcGxpY2F0aW9uL21zd29yZCc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfbXNfd29yZC5zdmcnLFxyXG4gICAgICAgICdhcHBsaWNhdGlvbi92bmQub3BlbnhtbGZvcm1hdHMtb2ZmaWNlZG9jdW1lbnQud29yZHByb2Nlc3NpbmdtbC5kb2N1bWVudCc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfbXNfd29yZC5zdmcnLFxyXG4gICAgICAgICdhcHBsaWNhdGlvbi92bmQub3BlbnhtbGZvcm1hdHMtb2ZmaWNlZG9jdW1lbnQud29yZHByb2Nlc3NpbmdtbC50ZW1wbGF0ZSc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfbXNfd29yZC5zdmcnLFxyXG4gICAgICAgICdhcHBsaWNhdGlvbi92bmQubXMtd29yZC5kb2N1bWVudC5tYWNyb2VuYWJsZWQuMTInOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX21zX3dvcmQuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLm1zLXdvcmQudGVtcGxhdGUubWFjcm9lbmFibGVkLjEyJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19tc193b3JkLnN2ZycsXHJcbiAgICAgICAgJ2FwcGxpY2F0aW9uL3ZuZC5zdW4ueG1sLndyaXRlcic6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfbXNfd29yZC5zdmcnLFxyXG4gICAgICAgICdhcHBsaWNhdGlvbi92bmQuc3VuLnhtbC53cml0ZXIudGVtcGxhdGUnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX21zX3dvcmQuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vcnRmJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19tc193b3JkLnN2ZycsXHJcbiAgICAgICAgJ3RleHQvcnRmJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19tc193b3JkLnN2ZycsXHJcbiAgICAgICAgJ2FwcGxpY2F0aW9uL3ZuZC5tcy1wb3dlcnBvaW50JzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19tc19wb3dlcnBvaW50LnN2ZycsXHJcbiAgICAgICAgJ2FwcGxpY2F0aW9uL3ZuZC5vcGVueG1sZm9ybWF0cy1vZmZpY2Vkb2N1bWVudC5wcmVzZW50YXRpb25tbC5wcmVzZW50YXRpb24nOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX21zX3Bvd2VycG9pbnQuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLm9wZW54bWxmb3JtYXRzLW9mZmljZWRvY3VtZW50LnByZXNlbnRhdGlvbm1sLnRlbXBsYXRlJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19tc19wb3dlcnBvaW50LnN2ZycsXHJcbiAgICAgICAgJ2FwcGxpY2F0aW9uL3ZuZC5vcGVueG1sZm9ybWF0cy1vZmZpY2Vkb2N1bWVudC5wcmVzZW50YXRpb25tbC5zbGlkZXNob3cnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX21zX3Bvd2VycG9pbnQuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLm9hc2lzLm9wZW5kb2N1bWVudC5wcmVzZW50YXRpb24nOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX21zX3Bvd2VycG9pbnQuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLm9hc2lzLm9wZW5kb2N1bWVudC5wcmVzZW50YXRpb24tdGVtcGxhdGUnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX21zX3Bvd2VycG9pbnQuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLm9wZW54bWxmb3JtYXRzLW9mZmljZWRvY3VtZW50LnByZXNlbnRhdGlvbm1sLnNsaWRlJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19tc19wb3dlcnBvaW50LnN2ZycsXHJcbiAgICAgICAgJ2FwcGxpY2F0aW9uL3ZuZC5zdW4ueG1sLmltcHJlc3MnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX21zX3Bvd2VycG9pbnQuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLnN1bi54bWwuaW1wcmVzcy50ZW1wbGF0ZSc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfbXNfcG93ZXJwb2ludC5zdmcnLFxyXG4gICAgICAgICdhcHBsaWNhdGlvbi92bmQub2FzaXMub3BlbmRvY3VtZW50LnNwcmVhZHNoZWV0JzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19tc19leGNlbC5zdmcnLFxyXG4gICAgICAgICdhcHBsaWNhdGlvbi92bmQub2FzaXMub3BlbmRvY3VtZW50LnNwcmVhZHNoZWV0LXRlbXBsYXRlJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19tc19leGNlbC5zdmcnLFxyXG4gICAgICAgICdhcHBsaWNhdGlvbi92bmQubXMtcG93ZXJwb2ludC5hZGRpbi5tYWNyb2VuYWJsZWQuMTInOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX21zX3Bvd2VycG9pbnQuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLm1zLXBvd2VycG9pbnQucHJlc2VudGF0aW9uLm1hY3JvZW5hYmxlZC4xMic6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfbXNfcG93ZXJwb2ludC5zdmcnLFxyXG4gICAgICAgICdhcHBsaWNhdGlvbi92bmQubXMtcG93ZXJwb2ludC5zbGlkZS5tYWNyb2VuYWJsZWQuMTInOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX21zX3Bvd2VycG9pbnQuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLm1zLXBvd2VycG9pbnQuc2xpZGVzaG93Lm1hY3JvZW5hYmxlZC4xMic6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfbXNfcG93ZXJwb2ludC5zdmcnLFxyXG4gICAgICAgICdhcHBsaWNhdGlvbi92bmQubXMtcG93ZXJwb2ludC50ZW1wbGF0ZS5tYWNyb2VuYWJsZWQuMTInOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX21zX3Bvd2VycG9pbnQuc3ZnJyxcclxuICAgICAgICAndmlkZW8vbXA0JzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY192aWRlby5zdmcnLFxyXG4gICAgICAgICd2aWRlby8zZ3BwJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY192aWRlby5zdmcnLFxyXG4gICAgICAgICd2aWRlby8zZ3BwMic6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfdmlkZW8uc3ZnJyxcclxuICAgICAgICAndmlkZW8vbXAydCc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfdmlkZW8uc3ZnJyxcclxuICAgICAgICAndmlkZW8vbXBlZyc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfdmlkZW8uc3ZnJyxcclxuICAgICAgICAndmlkZW8vbXBlZzInOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3ZpZGVvLnN2ZycsXHJcbiAgICAgICAgJ3ZpZGVvL29nZyc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfdmlkZW8uc3ZnJyxcclxuICAgICAgICAndmlkZW8vcXVpY2t0aW1lJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY192aWRlby5zdmcnLFxyXG4gICAgICAgICd2aWRlby93ZWJtJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY192aWRlby5zdmcnLFxyXG4gICAgICAgICd2aWRlby94LWZsdic6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfdmlkZW8uc3ZnJyxcclxuICAgICAgICAndmlkZW8veC1tNHYnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3ZpZGVvLnN2ZycsXHJcbiAgICAgICAgJ3ZpZGVvL3gtbXMtYXNmJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY192aWRlby5zdmcnLFxyXG4gICAgICAgICd2aWRlby94LW1zLXdtdic6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfdmlkZW8uc3ZnJyxcclxuICAgICAgICAndmlkZW8veC1tc3ZpZGVvJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY192aWRlby5zdmcnLFxyXG4gICAgICAgICd2aWRlby94LXJhZC1zY3JlZW5wbGF5JzogICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfdmlkZW8uc3ZnJyxcclxuICAgICAgICAndmlkZW8veC1zZ2ktbW92aWUnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3ZpZGVvLnN2ZycsXHJcbiAgICAgICAgJ3ZpZGVvL3gtbWF0cm9za2EnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3ZpZGVvLnN2ZycsXHJcbiAgICAgICAgJ2F1ZGlvL21wZWcnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX2F1ZGlvLnN2ZycsXHJcbiAgICAgICAgJ2F1ZGlvL29nZyc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfYXVkaW8uc3ZnJyxcclxuICAgICAgICAnYXVkaW8vd2F2JzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19hdWRpby5zdmcnLFxyXG4gICAgICAgICdhdWRpby9iYXNpYyc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfYXVkaW8uc3ZnJyxcclxuICAgICAgICAnYXVkaW8vbXA0JzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19hdWRpby5zdmcnLFxyXG4gICAgICAgICdhdWRpby92bmQuYWRvYmUuc291bmRib290aCc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfYXVkaW8uc3ZnJyxcclxuICAgICAgICAnYXVkaW8vdm9yYmlzJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19hdWRpby5zdmcnLFxyXG4gICAgICAgICdhdWRpby94LWFpZmYnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX2F1ZGlvLnN2ZycsXHJcbiAgICAgICAgJ2F1ZGlvL3gtZmxhYyc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfYXVkaW8uc3ZnJyxcclxuICAgICAgICAnYXVkaW8veC1tcy13bWEnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX2F1ZGlvLnN2ZycsXHJcbiAgICAgICAgJ2F1ZGlvL3gtd2F2JzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19hdWRpby5zdmcnLFxyXG4gICAgICAgICd4LXdvcmxkL3gtdnJtbCc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfdmlkZW8uc3ZnJyxcclxuICAgICAgICAndGV4dC9wbGFpbic6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfZG9jdW1lbnQuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLm9hc2lzLm9wZW5kb2N1bWVudC50ZXh0JzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19kb2N1bWVudC5zdmcnLFxyXG4gICAgICAgICdhcHBsaWNhdGlvbi92bmQub2FzaXMub3BlbmRvY3VtZW50LnRleHQtdGVtcGxhdGUnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX2RvY3VtZW50LnN2ZycsXHJcbiAgICAgICAgJ2FwcGxpY2F0aW9uL3gtamF2YXNjcmlwdCc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfZG9jdW1lbnQuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vanNvbic6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfZG9jdW1lbnQuc3ZnJyxcclxuICAgICAgICAndGV4dC9jc3YnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX2RvY3VtZW50LnN2ZycsXHJcbiAgICAgICAgJ3RleHQveG1sJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19kb2N1bWVudC5zdmcnLFxyXG4gICAgICAgICd0ZXh0L2h0bWwnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX3dlYnNpdGUuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24veC1jb21wcmVzc2VkJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19hcmNoaXZlLnN2ZycsXHJcbiAgICAgICAgJ2FwcGxpY2F0aW9uL3gtemlwLWNvbXByZXNzZWQnOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX2FyY2hpdmUuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vemlwJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19hcmNoaXZlLnN2ZycsXHJcbiAgICAgICAgJ2FwcGxpY2F0aW9uL3gtdGFyJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19hcmNoaXZlLnN2ZycsXHJcbiAgICAgICAgJ2FwcGxpY2F0aW9uL3ZuZC5hcHBsZS5rZXlub3RlJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19wcmVzZW50YXRpb24uc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLmFwcGxlLnBhZ2VzJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19kb2N1bWVudC5zdmcnLFxyXG4gICAgICAgICdhcHBsaWNhdGlvbi92bmQuYXBwbGUubnVtYmVycyc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfc3ByZWFkc2hlZXQuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24vdm5kLnZpc2lvJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19kb2N1bWVudC5zdmcnLFxyXG4gICAgICAgICdhcHBsaWNhdGlvbi93b3JkcGVyZmVjdCc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfZG9jdW1lbnQuc3ZnJyxcclxuICAgICAgICAnYXBwbGljYXRpb24veC1jcGlvJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19kb2N1bWVudC5zdmcnLFxyXG4gICAgICAgICdmb2xkZXInOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX2ZvbGRlci5zdmcnLFxyXG4gICAgICAgICdzbWFydEZvbGRlcic6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfc21hcnRfZm9sZGVyLnN2ZycsXHJcbiAgICAgICAgJ3J1bGVGb2xkZXInOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX2ZvbGRlcl9ydWxlLnN2ZycsXHJcbiAgICAgICAgJ2xpbmtGb2xkZXInOiAnLi9hc3NldHMvaW1hZ2VzL2Z0X2ljX2ZvbGRlcl9zaG9ydGN1dF9saW5rLnN2ZycsXHJcbiAgICAgICAgJ2Rpc2FibGUvZm9sZGVyJzogJy4vYXNzZXRzL2ltYWdlcy9mdF9pY19mb2xkZXJfZGlzYWJsZS5zdmcnLFxyXG4gICAgICAgICdzZWxlY3RlZCc6ICcuL2Fzc2V0cy9pbWFnZXMvZnRfaWNfc2VsZWN0ZWQuc3ZnJ1xyXG4gICAgfTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcm90ZWN0ZWQgYXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlLCBtYXRJY29uUmVnaXN0cnk6IE1hdEljb25SZWdpc3RyeSwgc2FuaXRpemVyOiBEb21TYW5pdGl6ZXIpIHtcclxuICAgICAgICBPYmplY3Qua2V5cyh0aGlzLm1pbWVUeXBlSWNvbnMpLmZvckVhY2goKGtleSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCB1cmwgPSBzYW5pdGl6ZXIuYnlwYXNzU2VjdXJpdHlUcnVzdFJlc291cmNlVXJsKHRoaXMubWltZVR5cGVJY29uc1trZXldKTtcclxuXHJcbiAgICAgICAgICAgIG1hdEljb25SZWdpc3RyeS5hZGRTdmdJY29uKGtleSwgdXJsKTtcclxuICAgICAgICAgICAgbWF0SWNvblJlZ2lzdHJ5LmFkZFN2Z0ljb25Jbk5hbWVzcGFjZSgnYWRmJywga2V5LCB1cmwpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhIHRodW1ibmFpbCBVUkwgZm9yIHRoZSBnaXZlbiBkb2N1bWVudCBub2RlLlxyXG4gICAgICogQHBhcmFtIG5vZGUgTm9kZSBvciBOb2RlIElEIHRvIGdldCBVUkwgZm9yLlxyXG4gICAgICogQHJldHVybnMgVVJMIHN0cmluZ1xyXG4gICAgICovXHJcbiAgICBnZXREb2N1bWVudFRodW1ibmFpbFVybChub2RlOiBOb2RlRW50cnkgfCBzdHJpbmcsIGF0dGFjaG1lbnQ/OiBib29sZWFuLCB0aWNrZXQ/OiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIGxldCByZXN1bHRVcmw6IHN0cmluZztcclxuXHJcbiAgICAgICAgaWYgKG5vZGUpIHtcclxuICAgICAgICAgICAgbGV0IG5vZGVJZDogc3RyaW5nO1xyXG5cclxuICAgICAgICAgICAgaWYgKHR5cGVvZiBub2RlID09PSAnc3RyaW5nJykge1xyXG4gICAgICAgICAgICAgICAgbm9kZUlkID0gbm9kZTtcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChub2RlLmVudHJ5KSB7XHJcbiAgICAgICAgICAgICAgICBub2RlSWQgPSBub2RlLmVudHJ5LmlkO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICByZXN1bHRVcmwgPSB0aGlzLmFwaVNlcnZpY2UuY29udGVudEFwaS5nZXREb2N1bWVudFRodW1ibmFpbFVybChub2RlSWQsIGF0dGFjaG1lbnQsIHRpY2tldCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcmVzdWx0VXJsIHx8IHRoaXMuREVGQVVMVF9JQ09OO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhIHRodW1ibmFpbCBVUkwgZm9yIGEgTUlNRSB0eXBlLlxyXG4gICAgICogQHBhcmFtIG1pbWVUeXBlIE1JTUUgdHlwZSBmb3IgdGhlIHRodW1ibmFpbFxyXG4gICAgICogQHJldHVybnMgVVJMIHN0cmluZ1xyXG4gICAgICovXHJcbiAgICBwdWJsaWMgZ2V0TWltZVR5cGVJY29uKG1pbWVUeXBlOiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIGNvbnN0IGljb24gPSB0aGlzLm1pbWVUeXBlSWNvbnNbbWltZVR5cGVdO1xyXG4gICAgICAgIHJldHVybiAoaWNvbiB8fCB0aGlzLkRFRkFVTFRfSUNPTik7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgXCJtaXNjZWxsYW5lb3VzXCIgdGh1bWJuYWlsIFVSTCBmb3IgdHlwZXMgd2l0aCBubyBvdGhlciBpY29uIGRlZmluZWQuXHJcbiAgICAgKiBAcmV0dXJucyBVUkwgc3RyaW5nXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXREZWZhdWx0TWltZVR5cGVJY29uKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuREVGQVVMVF9JQ09OO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==