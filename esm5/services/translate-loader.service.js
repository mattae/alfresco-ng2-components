/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, forkJoin, throwError, of } from 'rxjs';
import { ComponentTranslationModel } from '../models/component.model';
import { ObjectUtils } from '../utils/object-utils';
import { map, catchError, retry } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "@angular/common/http";
var TranslateLoaderService = /** @class */ (function () {
    function TranslateLoaderService(http) {
        this.http = http;
        this.prefix = 'i18n';
        this.suffix = '.json';
        this.providers = [];
        this.queue = [];
        this.defaultLang = 'en';
    }
    /**
     * @param {?} value
     * @return {?}
     */
    TranslateLoaderService.prototype.setDefaultLang = /**
     * @param {?} value
     * @return {?}
     */
    function (value) {
        this.defaultLang = value || 'en';
    };
    /**
     * @param {?} name
     * @param {?} path
     * @return {?}
     */
    TranslateLoaderService.prototype.registerProvider = /**
     * @param {?} name
     * @param {?} path
     * @return {?}
     */
    function (name, path) {
        /** @type {?} */
        var registered = this.providers.find((/**
         * @param {?} provider
         * @return {?}
         */
        function (provider) { return provider.name === name; }));
        if (registered) {
            registered.path = path;
        }
        else {
            this.providers.push(new ComponentTranslationModel({ name: name, path: path }));
        }
    };
    /**
     * @param {?} name
     * @return {?}
     */
    TranslateLoaderService.prototype.providerRegistered = /**
     * @param {?} name
     * @return {?}
     */
    function (name) {
        return this.providers.find((/**
         * @param {?} x
         * @return {?}
         */
        function (x) { return x.name === name; })) ? true : false;
    };
    /**
     * @param {?} lang
     * @param {?} component
     * @param {?=} fallbackUrl
     * @return {?}
     */
    TranslateLoaderService.prototype.fetchLanguageFile = /**
     * @param {?} lang
     * @param {?} component
     * @param {?=} fallbackUrl
     * @return {?}
     */
    function (lang, component, fallbackUrl) {
        var _this = this;
        /** @type {?} */
        var translationUrl = fallbackUrl || component.path + "/" + this.prefix + "/" + lang + this.suffix + "?v=" + Date.now();
        return this.http.get(translationUrl).pipe(map((/**
         * @param {?} res
         * @return {?}
         */
        function (res) {
            component.json[lang] = res;
        })), retry(3), catchError((/**
         * @return {?}
         */
        function () {
            if (!fallbackUrl && lang.includes('-')) {
                var _a = tslib_1.__read(lang.split('-'), 1), langId = _a[0];
                if (langId && langId !== _this.defaultLang) {
                    /** @type {?} */
                    var url = component.path + "/" + _this.prefix + "/" + langId + _this.suffix + "?v=" + Date.now();
                    return _this.fetchLanguageFile(lang, component, url);
                }
            }
            return throwError("Failed to load " + translationUrl);
        })));
    };
    /**
     * @param {?} lang
     * @return {?}
     */
    TranslateLoaderService.prototype.getComponentToFetch = /**
     * @param {?} lang
     * @return {?}
     */
    function (lang) {
        var _this = this;
        /** @type {?} */
        var observableBatch = [];
        if (!this.queue[lang]) {
            this.queue[lang] = [];
        }
        this.providers.forEach((/**
         * @param {?} component
         * @return {?}
         */
        function (component) {
            if (!_this.isComponentInQueue(lang, component.name)) {
                _this.queue[lang].push(component.name);
                observableBatch.push(_this.fetchLanguageFile(lang, component));
            }
        }));
        return observableBatch;
    };
    /**
     * @param {?} lang
     * @return {?}
     */
    TranslateLoaderService.prototype.init = /**
     * @param {?} lang
     * @return {?}
     */
    function (lang) {
        if (this.queue[lang] === undefined) {
            this.queue[lang] = [];
        }
    };
    /**
     * @param {?} lang
     * @param {?} name
     * @return {?}
     */
    TranslateLoaderService.prototype.isComponentInQueue = /**
     * @param {?} lang
     * @param {?} name
     * @return {?}
     */
    function (lang, name) {
        return (this.queue[lang] || []).find((/**
         * @param {?} x
         * @return {?}
         */
        function (x) { return x === name; })) ? true : false;
    };
    /**
     * @param {?} lang
     * @return {?}
     */
    TranslateLoaderService.prototype.getFullTranslationJSON = /**
     * @param {?} lang
     * @return {?}
     */
    function (lang) {
        /** @type {?} */
        var result = {};
        this.providers
            .slice(0)
            .sort((/**
         * @param {?} a
         * @param {?} b
         * @return {?}
         */
        function (a, b) {
            if (a.name === 'app') {
                return 1;
            }
            if (b.name === 'app') {
                return -1;
            }
            return a.name.localeCompare(b.name);
        }))
            .forEach((/**
         * @param {?} model
         * @return {?}
         */
        function (model) {
            if (model.json && model.json[lang]) {
                result = ObjectUtils.merge(result, model.json[lang]);
            }
        }));
        return result;
    };
    /**
     * @param {?} lang
     * @return {?}
     */
    TranslateLoaderService.prototype.getTranslation = /**
     * @param {?} lang
     * @return {?}
     */
    function (lang) {
        var _this = this;
        /** @type {?} */
        var hasFailures = false;
        /** @type {?} */
        var batch = tslib_1.__spread(this.getComponentToFetch(lang).map((/**
         * @param {?} observable
         * @return {?}
         */
        function (observable) {
            return observable.pipe(catchError((/**
             * @param {?} error
             * @return {?}
             */
            function (error) {
                console.warn(error);
                hasFailures = true;
                return of(error);
            })));
        })));
        return new Observable((/**
         * @param {?} observer
         * @return {?}
         */
        function (observer) {
            if (batch.length > 0) {
                forkJoin(batch).subscribe((/**
                 * @return {?}
                 */
                function () {
                    /** @type {?} */
                    var fullTranslation = _this.getFullTranslationJSON(lang);
                    if (fullTranslation) {
                        observer.next(fullTranslation);
                    }
                    if (hasFailures) {
                        observer.error('Failed to load some resources');
                    }
                    else {
                        observer.complete();
                    }
                }), (/**
                 * @param {?} err
                 * @return {?}
                 */
                function (err) {
                    observer.error('Failed to load some resources');
                }));
            }
            else {
                /** @type {?} */
                var fullTranslation = _this.getFullTranslationJSON(lang);
                if (fullTranslation) {
                    observer.next(fullTranslation);
                    observer.complete();
                }
            }
        }));
    };
    TranslateLoaderService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    TranslateLoaderService.ctorParameters = function () { return [
        { type: HttpClient }
    ]; };
    /** @nocollapse */ TranslateLoaderService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function TranslateLoaderService_Factory() { return new TranslateLoaderService(i0.ɵɵinject(i1.HttpClient)); }, token: TranslateLoaderService, providedIn: "root" });
    return TranslateLoaderService;
}());
export { TranslateLoaderService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    TranslateLoaderService.prototype.prefix;
    /**
     * @type {?}
     * @private
     */
    TranslateLoaderService.prototype.suffix;
    /**
     * @type {?}
     * @private
     */
    TranslateLoaderService.prototype.providers;
    /**
     * @type {?}
     * @private
     */
    TranslateLoaderService.prototype.queue;
    /**
     * @type {?}
     * @private
     */
    TranslateLoaderService.prototype.defaultLang;
    /**
     * @type {?}
     * @private
     */
    TranslateLoaderService.prototype.http;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhbnNsYXRlLWxvYWRlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvdHJhbnNsYXRlLWxvYWRlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sc0JBQXNCLENBQUM7QUFDbEQsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUczQyxPQUFPLEVBQUUsVUFBVSxFQUFFLFFBQVEsRUFBRSxVQUFVLEVBQUUsRUFBRSxFQUFFLE1BQU0sTUFBTSxDQUFDO0FBQzVELE9BQU8sRUFBRSx5QkFBeUIsRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBQ3RFLE9BQU8sRUFBRSxXQUFXLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUNwRCxPQUFPLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQzs7O0FBRXhEO0lBV0ksZ0NBQW9CLElBQWdCO1FBQWhCLFNBQUksR0FBSixJQUFJLENBQVk7UUFONUIsV0FBTSxHQUFXLE1BQU0sQ0FBQztRQUN4QixXQUFNLEdBQVcsT0FBTyxDQUFDO1FBQ3pCLGNBQVMsR0FBZ0MsRUFBRSxDQUFDO1FBQzVDLFVBQUssR0FBZ0IsRUFBRSxDQUFDO1FBQ3hCLGdCQUFXLEdBQVcsSUFBSSxDQUFDO0lBR25DLENBQUM7Ozs7O0lBRUQsK0NBQWM7Ozs7SUFBZCxVQUFlLEtBQWE7UUFDeEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLElBQUksSUFBSSxDQUFDO0lBQ3JDLENBQUM7Ozs7OztJQUVELGlEQUFnQjs7Ozs7SUFBaEIsVUFBaUIsSUFBWSxFQUFFLElBQVk7O1lBQ2pDLFVBQVUsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUk7Ozs7UUFBQyxVQUFDLFFBQVEsSUFBSyxPQUFBLFFBQVEsQ0FBQyxJQUFJLEtBQUssSUFBSSxFQUF0QixDQUFzQixFQUFDO1FBQzVFLElBQUksVUFBVSxFQUFFO1lBQ1osVUFBVSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7U0FDMUI7YUFBTTtZQUNILElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLElBQUkseUJBQXlCLENBQUMsRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDLENBQUM7U0FDbEY7SUFDTCxDQUFDOzs7OztJQUVELG1EQUFrQjs7OztJQUFsQixVQUFtQixJQUFZO1FBQzNCLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQyxDQUFDLElBQUssT0FBQSxDQUFDLENBQUMsSUFBSSxLQUFLLElBQUksRUFBZixDQUFlLEVBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUM7SUFDdEUsQ0FBQzs7Ozs7OztJQUVELGtEQUFpQjs7Ozs7O0lBQWpCLFVBQWtCLElBQVksRUFBRSxTQUFvQyxFQUFFLFdBQW9CO1FBQTFGLGlCQXFCQzs7WUFwQlMsY0FBYyxHQUFHLFdBQVcsSUFBTyxTQUFTLENBQUMsSUFBSSxTQUFJLElBQUksQ0FBQyxNQUFNLFNBQUksSUFBSSxHQUFHLElBQUksQ0FBQyxNQUFNLFdBQU0sSUFBSSxDQUFDLEdBQUcsRUFBSTtRQUU5RyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxDQUFDLElBQUksQ0FDckMsR0FBRzs7OztRQUFDLFVBQUMsR0FBYTtZQUNkLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsR0FBRyxDQUFDO1FBQy9CLENBQUMsRUFBQyxFQUNGLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFDUixVQUFVOzs7UUFBQztZQUNQLElBQUksQ0FBQyxXQUFXLElBQUksSUFBSSxDQUFDLFFBQVEsQ0FBQyxHQUFHLENBQUMsRUFBRTtnQkFDOUIsSUFBQSx1Q0FBMEIsRUFBekIsY0FBeUI7Z0JBRWhDLElBQUksTUFBTSxJQUFJLE1BQU0sS0FBSyxLQUFJLENBQUMsV0FBVyxFQUFFOzt3QkFDakMsR0FBRyxHQUFNLFNBQVMsQ0FBQyxJQUFJLFNBQUksS0FBSSxDQUFDLE1BQU0sU0FBSSxNQUFNLEdBQUcsS0FBSSxDQUFDLE1BQU0sV0FBTSxJQUFJLENBQUMsR0FBRyxFQUFJO29CQUV0RixPQUFPLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsU0FBUyxFQUFFLEdBQUcsQ0FBQyxDQUFDO2lCQUN2RDthQUNKO1lBQ0QsT0FBTyxVQUFVLENBQUMsb0JBQWtCLGNBQWdCLENBQUMsQ0FBQztRQUMxRCxDQUFDLEVBQUMsQ0FDTCxDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFFRCxvREFBbUI7Ozs7SUFBbkIsVUFBb0IsSUFBWTtRQUFoQyxpQkFnQkM7O1lBZlMsZUFBZSxHQUFHLEVBQUU7UUFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDbkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7U0FDekI7UUFDRCxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU87Ozs7UUFBQyxVQUFDLFNBQVM7WUFDN0IsSUFBSSxDQUFDLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLElBQUksQ0FBQyxFQUFFO2dCQUNoRCxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRXRDLGVBQWUsQ0FBQyxJQUFJLENBQ2hCLEtBQUksQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLEVBQUUsU0FBUyxDQUFDLENBQzFDLENBQUM7YUFDTDtRQUNMLENBQUMsRUFBQyxDQUFDO1FBRUgsT0FBTyxlQUFlLENBQUM7SUFDM0IsQ0FBQzs7Ozs7SUFFRCxxQ0FBSTs7OztJQUFKLFVBQUssSUFBWTtRQUNiLElBQUksSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsS0FBSyxTQUFTLEVBQUU7WUFDaEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLENBQUM7U0FDekI7SUFDTCxDQUFDOzs7Ozs7SUFFRCxtREFBa0I7Ozs7O0lBQWxCLFVBQW1CLElBQVksRUFBRSxJQUFZO1FBQ3pDLE9BQU8sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQyxDQUFDLElBQUk7Ozs7UUFBQyxVQUFDLENBQUMsSUFBSyxPQUFBLENBQUMsS0FBSyxJQUFJLEVBQVYsQ0FBVSxFQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQzNFLENBQUM7Ozs7O0lBRUQsdURBQXNCOzs7O0lBQXRCLFVBQXVCLElBQVk7O1lBQzNCLE1BQU0sR0FBRyxFQUFFO1FBRWYsSUFBSSxDQUFDLFNBQVM7YUFDVCxLQUFLLENBQUMsQ0FBQyxDQUFDO2FBQ1IsSUFBSTs7Ozs7UUFBQyxVQUFDLENBQUMsRUFBRSxDQUFDO1lBQ1AsSUFBSSxDQUFDLENBQUMsSUFBSSxLQUFLLEtBQUssRUFBRTtnQkFDbEIsT0FBTyxDQUFDLENBQUM7YUFDWjtZQUNELElBQUksQ0FBQyxDQUFDLElBQUksS0FBSyxLQUFLLEVBQUU7Z0JBQ2xCLE9BQU8sQ0FBQyxDQUFDLENBQUM7YUFDYjtZQUNELE9BQU8sQ0FBQyxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3hDLENBQUMsRUFBQzthQUNELE9BQU87Ozs7UUFBQyxVQUFDLEtBQUs7WUFDWCxJQUFJLEtBQUssQ0FBQyxJQUFJLElBQUksS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDaEMsTUFBTSxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQzthQUN4RDtRQUNMLENBQUMsRUFBQyxDQUFDO1FBRVAsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQzs7Ozs7SUFFRCwrQ0FBYzs7OztJQUFkLFVBQWUsSUFBWTtRQUEzQixpQkF3Q0M7O1lBdkNPLFdBQVcsR0FBRyxLQUFLOztZQUNqQixLQUFLLG9CQUNKLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHOzs7O1FBQUMsVUFBQyxVQUFVO1lBQzdDLE9BQU8sVUFBVSxDQUFDLElBQUksQ0FDbEIsVUFBVTs7OztZQUFDLFVBQUMsS0FBSztnQkFDYixPQUFPLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDO2dCQUNwQixXQUFXLEdBQUcsSUFBSSxDQUFDO2dCQUNuQixPQUFPLEVBQUUsQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNyQixDQUFDLEVBQUMsQ0FDTCxDQUFDO1FBQ04sQ0FBQyxFQUFDLENBQ0w7UUFFRCxPQUFPLElBQUksVUFBVTs7OztRQUFDLFVBQUMsUUFBUTtZQUUzQixJQUFJLEtBQUssQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO2dCQUNsQixRQUFRLENBQUMsS0FBSyxDQUFDLENBQUMsU0FBUzs7O2dCQUNyQjs7d0JBQ1UsZUFBZSxHQUFHLEtBQUksQ0FBQyxzQkFBc0IsQ0FBQyxJQUFJLENBQUM7b0JBQ3pELElBQUksZUFBZSxFQUFFO3dCQUNqQixRQUFRLENBQUMsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO3FCQUNsQztvQkFDRCxJQUFJLFdBQVcsRUFBRTt3QkFDYixRQUFRLENBQUMsS0FBSyxDQUFDLCtCQUErQixDQUFDLENBQUM7cUJBQ25EO3lCQUFNO3dCQUNILFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztxQkFDdkI7Z0JBQ0wsQ0FBQzs7OztnQkFDRCxVQUFDLEdBQUc7b0JBQ0EsUUFBUSxDQUFDLEtBQUssQ0FBQywrQkFBK0IsQ0FBQyxDQUFDO2dCQUNwRCxDQUFDLEVBQUMsQ0FBQzthQUNWO2lCQUFNOztvQkFDRyxlQUFlLEdBQUcsS0FBSSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQztnQkFDekQsSUFBSSxlQUFlLEVBQUU7b0JBQ2pCLFFBQVEsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDLENBQUM7b0JBQy9CLFFBQVEsQ0FBQyxRQUFRLEVBQUUsQ0FBQztpQkFDdkI7YUFDSjtRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Z0JBakpKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7Z0JBWFEsVUFBVTs7O2lDQWpCbkI7Q0E0S0MsQUFsSkQsSUFrSkM7U0EvSVksc0JBQXNCOzs7Ozs7SUFFL0Isd0NBQWdDOzs7OztJQUNoQyx3Q0FBaUM7Ozs7O0lBQ2pDLDJDQUFvRDs7Ozs7SUFDcEQsdUNBQWdDOzs7OztJQUNoQyw2Q0FBbUM7Ozs7O0lBRXZCLHNDQUF3QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFJlc3BvbnNlIH0gZnJvbSAnQGFuZ3VsYXIvaHR0cCc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZUxvYWRlciB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBmb3JrSm9pbiwgdGhyb3dFcnJvciwgb2YgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQ29tcG9uZW50VHJhbnNsYXRpb25Nb2RlbCB9IGZyb20gJy4uL21vZGVscy9jb21wb25lbnQubW9kZWwnO1xyXG5pbXBvcnQgeyBPYmplY3RVdGlscyB9IGZyb20gJy4uL3V0aWxzL29iamVjdC11dGlscyc7XHJcbmltcG9ydCB7IG1hcCwgY2F0Y2hFcnJvciwgcmV0cnkgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFRyYW5zbGF0ZUxvYWRlclNlcnZpY2UgaW1wbGVtZW50cyBUcmFuc2xhdGVMb2FkZXIge1xyXG5cclxuICAgIHByaXZhdGUgcHJlZml4OiBzdHJpbmcgPSAnaTE4bic7XHJcbiAgICBwcml2YXRlIHN1ZmZpeDogc3RyaW5nID0gJy5qc29uJztcclxuICAgIHByaXZhdGUgcHJvdmlkZXJzOiBDb21wb25lbnRUcmFuc2xhdGlvbk1vZGVsW10gPSBbXTtcclxuICAgIHByaXZhdGUgcXVldWU6IHN0cmluZyBbXVtdID0gW107XHJcbiAgICBwcml2YXRlIGRlZmF1bHRMYW5nOiBzdHJpbmcgPSAnZW4nO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCkge1xyXG4gICAgfVxyXG5cclxuICAgIHNldERlZmF1bHRMYW5nKHZhbHVlOiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLmRlZmF1bHRMYW5nID0gdmFsdWUgfHwgJ2VuJztcclxuICAgIH1cclxuXHJcbiAgICByZWdpc3RlclByb3ZpZGVyKG5hbWU6IHN0cmluZywgcGF0aDogc3RyaW5nKSB7XHJcbiAgICAgICAgY29uc3QgcmVnaXN0ZXJlZCA9IHRoaXMucHJvdmlkZXJzLmZpbmQoKHByb3ZpZGVyKSA9PiBwcm92aWRlci5uYW1lID09PSBuYW1lKTtcclxuICAgICAgICBpZiAocmVnaXN0ZXJlZCkge1xyXG4gICAgICAgICAgICByZWdpc3RlcmVkLnBhdGggPSBwYXRoO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHRoaXMucHJvdmlkZXJzLnB1c2gobmV3IENvbXBvbmVudFRyYW5zbGF0aW9uTW9kZWwoeyBuYW1lOiBuYW1lLCBwYXRoOiBwYXRoIH0pKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJvdmlkZXJSZWdpc3RlcmVkKG5hbWU6IHN0cmluZyk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnByb3ZpZGVycy5maW5kKCh4KSA9PiB4Lm5hbWUgPT09IG5hbWUpID8gdHJ1ZSA6IGZhbHNlO1xyXG4gICAgfVxyXG5cclxuICAgIGZldGNoTGFuZ3VhZ2VGaWxlKGxhbmc6IHN0cmluZywgY29tcG9uZW50OiBDb21wb25lbnRUcmFuc2xhdGlvbk1vZGVsLCBmYWxsYmFja1VybD86IHN0cmluZyk6IE9ic2VydmFibGU8dm9pZD4ge1xyXG4gICAgICAgIGNvbnN0IHRyYW5zbGF0aW9uVXJsID0gZmFsbGJhY2tVcmwgfHwgYCR7Y29tcG9uZW50LnBhdGh9LyR7dGhpcy5wcmVmaXh9LyR7bGFuZ30ke3RoaXMuc3VmZml4fT92PSR7RGF0ZS5ub3coKX1gO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5odHRwLmdldCh0cmFuc2xhdGlvblVybCkucGlwZShcclxuICAgICAgICAgICAgbWFwKChyZXM6IFJlc3BvbnNlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb21wb25lbnQuanNvbltsYW5nXSA9IHJlcztcclxuICAgICAgICAgICAgfSksXHJcbiAgICAgICAgICAgIHJldHJ5KDMpLFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmICghZmFsbGJhY2tVcmwgJiYgbGFuZy5pbmNsdWRlcygnLScpKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY29uc3QgW2xhbmdJZF0gPSBsYW5nLnNwbGl0KCctJyk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmIChsYW5nSWQgJiYgbGFuZ0lkICE9PSB0aGlzLmRlZmF1bHRMYW5nKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IHVybCA9IGAke2NvbXBvbmVudC5wYXRofS8ke3RoaXMucHJlZml4fS8ke2xhbmdJZH0ke3RoaXMuc3VmZml4fT92PSR7RGF0ZS5ub3coKX1gO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMuZmV0Y2hMYW5ndWFnZUZpbGUobGFuZywgY29tcG9uZW50LCB1cmwpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJldHVybiB0aHJvd0Vycm9yKGBGYWlsZWQgdG8gbG9hZCAke3RyYW5zbGF0aW9uVXJsfWApO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0Q29tcG9uZW50VG9GZXRjaChsYW5nOiBzdHJpbmcpOiBBcnJheTxPYnNlcnZhYmxlPGFueT4+IHtcclxuICAgICAgICBjb25zdCBvYnNlcnZhYmxlQmF0Y2ggPSBbXTtcclxuICAgICAgICBpZiAoIXRoaXMucXVldWVbbGFuZ10pIHtcclxuICAgICAgICAgICAgdGhpcy5xdWV1ZVtsYW5nXSA9IFtdO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnByb3ZpZGVycy5mb3JFYWNoKChjb21wb25lbnQpID0+IHtcclxuICAgICAgICAgICAgaWYgKCF0aGlzLmlzQ29tcG9uZW50SW5RdWV1ZShsYW5nLCBjb21wb25lbnQubmFtZSkpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMucXVldWVbbGFuZ10ucHVzaChjb21wb25lbnQubmFtZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgb2JzZXJ2YWJsZUJhdGNoLnB1c2goXHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5mZXRjaExhbmd1YWdlRmlsZShsYW5nLCBjb21wb25lbnQpXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiBvYnNlcnZhYmxlQmF0Y2g7XHJcbiAgICB9XHJcblxyXG4gICAgaW5pdChsYW5nOiBzdHJpbmcpIHtcclxuICAgICAgICBpZiAodGhpcy5xdWV1ZVtsYW5nXSA9PT0gdW5kZWZpbmVkKSB7XHJcbiAgICAgICAgICAgIHRoaXMucXVldWVbbGFuZ10gPSBbXTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaXNDb21wb25lbnRJblF1ZXVlKGxhbmc6IHN0cmluZywgbmFtZTogc3RyaW5nKSB7XHJcbiAgICAgICAgcmV0dXJuICh0aGlzLnF1ZXVlW2xhbmddIHx8IFtdKS5maW5kKCh4KSA9PiB4ID09PSBuYW1lKSA/IHRydWUgOiBmYWxzZTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRGdWxsVHJhbnNsYXRpb25KU09OKGxhbmc6IHN0cmluZyk6IGFueSB7XHJcbiAgICAgICAgbGV0IHJlc3VsdCA9IHt9O1xyXG5cclxuICAgICAgICB0aGlzLnByb3ZpZGVyc1xyXG4gICAgICAgICAgICAuc2xpY2UoMClcclxuICAgICAgICAgICAgLnNvcnQoKGEsIGIpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChhLm5hbWUgPT09ICdhcHAnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIDE7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICBpZiAoYi5uYW1lID09PSAnYXBwJykge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiAtMTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIHJldHVybiBhLm5hbWUubG9jYWxlQ29tcGFyZShiLm5hbWUpO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAuZm9yRWFjaCgobW9kZWwpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChtb2RlbC5qc29uICYmIG1vZGVsLmpzb25bbGFuZ10pIHtcclxuICAgICAgICAgICAgICAgICAgICByZXN1bHQgPSBPYmplY3RVdGlscy5tZXJnZShyZXN1bHQsIG1vZGVsLmpzb25bbGFuZ10pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxuXHJcbiAgICBnZXRUcmFuc2xhdGlvbihsYW5nOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIGxldCBoYXNGYWlsdXJlcyA9IGZhbHNlO1xyXG4gICAgICAgIGNvbnN0IGJhdGNoID0gW1xyXG4gICAgICAgICAgICAuLi50aGlzLmdldENvbXBvbmVudFRvRmV0Y2gobGFuZykubWFwKChvYnNlcnZhYmxlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gb2JzZXJ2YWJsZS5waXBlKFxyXG4gICAgICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycm9yKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnNvbGUud2FybihlcnJvcik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGhhc0ZhaWx1cmVzID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIG9mKGVycm9yKTtcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICBdO1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IE9ic2VydmFibGUoKG9ic2VydmVyKSA9PiB7XHJcblxyXG4gICAgICAgICAgICBpZiAoYmF0Y2gubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgZm9ya0pvaW4oYmF0Y2gpLnN1YnNjcmliZShcclxuICAgICAgICAgICAgICAgICAgICAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGNvbnN0IGZ1bGxUcmFuc2xhdGlvbiA9IHRoaXMuZ2V0RnVsbFRyYW5zbGF0aW9uSlNPTihsYW5nKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKGZ1bGxUcmFuc2xhdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIubmV4dChmdWxsVHJhbnNsYXRpb24pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChoYXNGYWlsdXJlcykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgb2JzZXJ2ZXIuZXJyb3IoJ0ZhaWxlZCB0byBsb2FkIHNvbWUgcmVzb3VyY2VzJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgICAgICAgICAoZXJyKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLmVycm9yKCdGYWlsZWQgdG8gbG9hZCBzb21lIHJlc291cmNlcycpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgZnVsbFRyYW5zbGF0aW9uID0gdGhpcy5nZXRGdWxsVHJhbnNsYXRpb25KU09OKGxhbmcpO1xyXG4gICAgICAgICAgICAgICAgaWYgKGZ1bGxUcmFuc2xhdGlvbikge1xyXG4gICAgICAgICAgICAgICAgICAgIG9ic2VydmVyLm5leHQoZnVsbFRyYW5zbGF0aW9uKTtcclxuICAgICAgICAgICAgICAgICAgICBvYnNlcnZlci5jb21wbGV0ZSgpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuIl19