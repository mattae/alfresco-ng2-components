/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Inject, Injectable, InjectionToken, Optional } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { UserPreferencesService, UserPreferenceValues } from './user-preferences.service';
import * as i0 from "@angular/core";
import * as i1 from "@ngx-translate/core";
import * as i2 from "./user-preferences.service";
/** @type {?} */
export var TRANSLATION_PROVIDER = new InjectionToken('Injection token for translation providers.');
/**
 * @record
 */
export function TranslationProvider() { }
if (false) {
    /** @type {?} */
    TranslationProvider.prototype.name;
    /** @type {?} */
    TranslationProvider.prototype.source;
}
var TranslationService = /** @class */ (function () {
    function TranslationService(translate, userPreferencesService, providers) {
        var _this = this;
        var e_1, _a;
        this.translate = translate;
        this.customLoader = (/** @type {?} */ (this.translate.currentLoader));
        this.defaultLang = 'en';
        translate.setDefaultLang(this.defaultLang);
        this.customLoader.setDefaultLang(this.defaultLang);
        if (providers && providers.length > 0) {
            try {
                for (var providers_1 = tslib_1.__values(providers), providers_1_1 = providers_1.next(); !providers_1_1.done; providers_1_1 = providers_1.next()) {
                    var provider = providers_1_1.value;
                    this.addTranslationFolder(provider.name, provider.source);
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (providers_1_1 && !providers_1_1.done && (_a = providers_1.return)) _a.call(providers_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        userPreferencesService.select(UserPreferenceValues.Locale).subscribe((/**
         * @param {?} locale
         * @return {?}
         */
        function (locale) {
            if (locale) {
                _this.userLang = locale;
                _this.use(_this.userLang);
            }
        }));
    }
    /**
     * Adds a new folder of translation source files.
     * @param name Name for the translation provider
     * @param path Path to the folder
     */
    /**
     * Adds a new folder of translation source files.
     * @param {?=} name Name for the translation provider
     * @param {?=} path Path to the folder
     * @return {?}
     */
    TranslationService.prototype.addTranslationFolder = /**
     * Adds a new folder of translation source files.
     * @param {?=} name Name for the translation provider
     * @param {?=} path Path to the folder
     * @return {?}
     */
    function (name, path) {
        if (name === void 0) { name = ''; }
        if (path === void 0) { path = ''; }
        if (!this.customLoader.providerRegistered(name)) {
            this.customLoader.registerProvider(name, path);
            if (this.userLang) {
                this.loadTranslation(this.userLang, this.defaultLang);
            }
            else {
                this.loadTranslation(this.defaultLang);
            }
        }
    };
    /**
     * Loads a translation file.
     * @param lang Language code for the language to load
     * @param fallback Language code to fall back to if the first one was unavailable
     */
    /**
     * Loads a translation file.
     * @param {?} lang Language code for the language to load
     * @param {?=} fallback Language code to fall back to if the first one was unavailable
     * @return {?}
     */
    TranslationService.prototype.loadTranslation = /**
     * Loads a translation file.
     * @param {?} lang Language code for the language to load
     * @param {?=} fallback Language code to fall back to if the first one was unavailable
     * @return {?}
     */
    function (lang, fallback) {
        var _this = this;
        this.translate.getTranslation(lang).subscribe((/**
         * @return {?}
         */
        function () {
            _this.translate.use(lang);
            _this.onTranslationChanged(lang);
        }), (/**
         * @return {?}
         */
        function () {
            if (fallback && fallback !== lang) {
                _this.loadTranslation(fallback);
            }
        }));
    };
    /**
     * Triggers a notification callback when the translation language changes.
     * @param lang The new language code
     */
    /**
     * Triggers a notification callback when the translation language changes.
     * @param {?} lang The new language code
     * @return {?}
     */
    TranslationService.prototype.onTranslationChanged = /**
     * Triggers a notification callback when the translation language changes.
     * @param {?} lang The new language code
     * @return {?}
     */
    function (lang) {
        this.translate.onTranslationChange.next({
            lang: lang,
            translations: this.customLoader.getFullTranslationJSON(lang)
        });
    };
    /**
     * Sets the target language for translations.
     * @param lang Code name for the language
     * @returns Translations available for the language
     */
    /**
     * Sets the target language for translations.
     * @param {?} lang Code name for the language
     * @return {?} Translations available for the language
     */
    TranslationService.prototype.use = /**
     * Sets the target language for translations.
     * @param {?} lang Code name for the language
     * @return {?} Translations available for the language
     */
    function (lang) {
        this.customLoader.init(lang);
        return this.translate.use(lang);
    };
    /**
     * Gets the translation for the supplied key.
     * @param key Key to translate
     * @param interpolateParams String(s) to be interpolated into the main message
     * @returns Translated text
     */
    /**
     * Gets the translation for the supplied key.
     * @param {?} key Key to translate
     * @param {?=} interpolateParams String(s) to be interpolated into the main message
     * @return {?} Translated text
     */
    TranslationService.prototype.get = /**
     * Gets the translation for the supplied key.
     * @param {?} key Key to translate
     * @param {?=} interpolateParams String(s) to be interpolated into the main message
     * @return {?} Translated text
     */
    function (key, interpolateParams) {
        return this.translate.get(key, interpolateParams);
    };
    /**
     * Directly returns the translation for the supplied key.
     * @param key Key to translate
     * @param interpolateParams String(s) to be interpolated into the main message
     * @returns Translated text
     */
    /**
     * Directly returns the translation for the supplied key.
     * @param {?} key Key to translate
     * @param {?=} interpolateParams String(s) to be interpolated into the main message
     * @return {?} Translated text
     */
    TranslationService.prototype.instant = /**
     * Directly returns the translation for the supplied key.
     * @param {?} key Key to translate
     * @param {?=} interpolateParams String(s) to be interpolated into the main message
     * @return {?} Translated text
     */
    function (key, interpolateParams) {
        return key ? this.translate.instant(key, interpolateParams) : '';
    };
    TranslationService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    TranslationService.ctorParameters = function () { return [
        { type: TranslateService },
        { type: UserPreferencesService },
        { type: Array, decorators: [{ type: Optional }, { type: Inject, args: [TRANSLATION_PROVIDER,] }] }
    ]; };
    /** @nocollapse */ TranslationService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function TranslationService_Factory() { return new TranslationService(i0.ɵɵinject(i1.TranslateService), i0.ɵɵinject(i2.UserPreferencesService), i0.ɵɵinject(TRANSLATION_PROVIDER, 8)); }, token: TranslationService, providedIn: "root" });
    return TranslationService;
}());
export { TranslationService };
if (false) {
    /** @type {?} */
    TranslationService.prototype.defaultLang;
    /** @type {?} */
    TranslationService.prototype.userLang;
    /** @type {?} */
    TranslationService.prototype.customLoader;
    /** @type {?} */
    TranslationService.prototype.translate;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHJhbnNsYXRpb24uc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInNlcnZpY2VzL3RyYW5zbGF0aW9uLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxNQUFNLEVBQUUsVUFBVSxFQUFFLGNBQWMsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDN0UsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFHdkQsT0FBTyxFQUFFLHNCQUFzQixFQUFFLG9CQUFvQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7Ozs7O0FBRTFGLE1BQU0sS0FBTyxvQkFBb0IsR0FBRyxJQUFJLGNBQWMsQ0FBQyw0Q0FBNEMsQ0FBQzs7OztBQUVwRyx5Q0FHQzs7O0lBRkcsbUNBQWE7O0lBQ2IscUNBQWU7O0FBR25CO0lBUUksNEJBQW1CLFNBQTJCLEVBQ2xDLHNCQUE4QyxFQUNKLFNBQWdDO1FBRnRGLGlCQXFCQzs7UUFyQmtCLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBRzFDLElBQUksQ0FBQyxZQUFZLEdBQUcsbUJBQXlCLElBQUksQ0FBQyxTQUFTLENBQUMsYUFBYSxFQUFBLENBQUM7UUFFMUUsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDeEIsU0FBUyxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7UUFDM0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBRW5ELElBQUksU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFOztnQkFDbkMsS0FBdUIsSUFBQSxjQUFBLGlCQUFBLFNBQVMsQ0FBQSxvQ0FBQSwyREFBRTtvQkFBN0IsSUFBTSxRQUFRLHNCQUFBO29CQUNmLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxRQUFRLENBQUMsSUFBSSxFQUFFLFFBQVEsQ0FBQyxNQUFNLENBQUMsQ0FBQztpQkFDN0Q7Ozs7Ozs7OztTQUNKO1FBRUQsc0JBQXNCLENBQUMsTUFBTSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFDLE1BQU07WUFDeEUsSUFBSSxNQUFNLEVBQUU7Z0JBQ1IsS0FBSSxDQUFDLFFBQVEsR0FBRyxNQUFNLENBQUM7Z0JBQ3ZCLEtBQUksQ0FBQyxHQUFHLENBQUMsS0FBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQzNCO1FBQ0wsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7OztJQUNILGlEQUFvQjs7Ozs7O0lBQXBCLFVBQXFCLElBQWlCLEVBQUUsSUFBaUI7UUFBcEMscUJBQUEsRUFBQSxTQUFpQjtRQUFFLHFCQUFBLEVBQUEsU0FBaUI7UUFDckQsSUFBSSxDQUFDLElBQUksQ0FBQyxZQUFZLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDN0MsSUFBSSxDQUFDLFlBQVksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFL0MsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNmLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDekQ7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7YUFDMUM7U0FDSjtJQUNMLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsNENBQWU7Ozs7OztJQUFmLFVBQWdCLElBQVksRUFBRSxRQUFpQjtRQUEvQyxpQkFZQztRQVhHLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxDQUFDLFNBQVM7OztRQUN6QztZQUNJLEtBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLElBQUksQ0FBQyxDQUFDO1lBQ3pCLEtBQUksQ0FBQyxvQkFBb0IsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNwQyxDQUFDOzs7UUFDRDtZQUNJLElBQUksUUFBUSxJQUFJLFFBQVEsS0FBSyxJQUFJLEVBQUU7Z0JBQy9CLEtBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDbEM7UUFDTCxDQUFDLEVBQ0osQ0FBQztJQUNOLENBQUM7SUFFRDs7O09BR0c7Ozs7OztJQUNILGlEQUFvQjs7Ozs7SUFBcEIsVUFBcUIsSUFBWTtRQUM3QixJQUFJLENBQUMsU0FBUyxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQztZQUNwQyxJQUFJLEVBQUUsSUFBSTtZQUNWLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLHNCQUFzQixDQUFDLElBQUksQ0FBQztTQUMvRCxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsZ0NBQUc7Ozs7O0lBQUgsVUFBSSxJQUFZO1FBQ1osSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDN0IsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNwQyxDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCxnQ0FBRzs7Ozs7O0lBQUgsVUFBSSxHQUEyQixFQUFFLGlCQUEwQjtRQUN2RCxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxpQkFBaUIsQ0FBQyxDQUFDO0lBQ3RELENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILG9DQUFPOzs7Ozs7SUFBUCxVQUFRLEdBQTJCLEVBQUUsaUJBQTBCO1FBQzNELE9BQU8sR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLE9BQU8sQ0FBQyxHQUFHLEVBQUUsaUJBQWlCLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxDQUFDO0lBQ3JFLENBQUM7O2dCQTFHSixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7O2dCQWRRLGdCQUFnQjtnQkFHaEIsc0JBQXNCOzRDQW1CZCxRQUFRLFlBQUksTUFBTSxTQUFDLG9CQUFvQjs7OzZCQXhDeEQ7Q0F5SUMsQUEzR0QsSUEyR0M7U0F4R1ksa0JBQWtCOzs7SUFDM0IseUNBQW9COztJQUNwQixzQ0FBaUI7O0lBQ2pCLDBDQUFxQzs7SUFFekIsdUNBQWtDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdCwgSW5qZWN0YWJsZSwgSW5qZWN0aW9uVG9rZW4sIE9wdGlvbmFsIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFRyYW5zbGF0ZVNlcnZpY2UgfSBmcm9tICdAbmd4LXRyYW5zbGF0ZS9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBUcmFuc2xhdGVMb2FkZXJTZXJ2aWNlIH0gZnJvbSAnLi90cmFuc2xhdGUtbG9hZGVyLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlLCBVc2VyUHJlZmVyZW5jZVZhbHVlcyB9IGZyb20gJy4vdXNlci1wcmVmZXJlbmNlcy5zZXJ2aWNlJztcclxuXHJcbmV4cG9ydCBjb25zdCBUUkFOU0xBVElPTl9QUk9WSURFUiA9IG5ldyBJbmplY3Rpb25Ub2tlbignSW5qZWN0aW9uIHRva2VuIGZvciB0cmFuc2xhdGlvbiBwcm92aWRlcnMuJyk7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIFRyYW5zbGF0aW9uUHJvdmlkZXIge1xyXG4gICAgbmFtZTogc3RyaW5nO1xyXG4gICAgc291cmNlOiBzdHJpbmc7XHJcbn1cclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgVHJhbnNsYXRpb25TZXJ2aWNlIHtcclxuICAgIGRlZmF1bHRMYW5nOiBzdHJpbmc7XHJcbiAgICB1c2VyTGFuZzogc3RyaW5nO1xyXG4gICAgY3VzdG9tTG9hZGVyOiBUcmFuc2xhdGVMb2FkZXJTZXJ2aWNlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICB1c2VyUHJlZmVyZW5jZXNTZXJ2aWNlOiBVc2VyUHJlZmVyZW5jZXNTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgQE9wdGlvbmFsKCkgQEluamVjdChUUkFOU0xBVElPTl9QUk9WSURFUikgcHJvdmlkZXJzOiBUcmFuc2xhdGlvblByb3ZpZGVyW10pIHtcclxuICAgICAgICB0aGlzLmN1c3RvbUxvYWRlciA9IDxUcmFuc2xhdGVMb2FkZXJTZXJ2aWNlPiB0aGlzLnRyYW5zbGF0ZS5jdXJyZW50TG9hZGVyO1xyXG5cclxuICAgICAgICB0aGlzLmRlZmF1bHRMYW5nID0gJ2VuJztcclxuICAgICAgICB0cmFuc2xhdGUuc2V0RGVmYXVsdExhbmcodGhpcy5kZWZhdWx0TGFuZyk7XHJcbiAgICAgICAgdGhpcy5jdXN0b21Mb2FkZXIuc2V0RGVmYXVsdExhbmcodGhpcy5kZWZhdWx0TGFuZyk7XHJcblxyXG4gICAgICAgIGlmIChwcm92aWRlcnMgJiYgcHJvdmlkZXJzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgZm9yIChjb25zdCBwcm92aWRlciBvZiBwcm92aWRlcnMpIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWRkVHJhbnNsYXRpb25Gb2xkZXIocHJvdmlkZXIubmFtZSwgcHJvdmlkZXIuc291cmNlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgdXNlclByZWZlcmVuY2VzU2VydmljZS5zZWxlY3QoVXNlclByZWZlcmVuY2VWYWx1ZXMuTG9jYWxlKS5zdWJzY3JpYmUoKGxvY2FsZSkgPT4ge1xyXG4gICAgICAgICAgICBpZiAobG9jYWxlKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLnVzZXJMYW5nID0gbG9jYWxlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy51c2UodGhpcy51c2VyTGFuZyk7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEFkZHMgYSBuZXcgZm9sZGVyIG9mIHRyYW5zbGF0aW9uIHNvdXJjZSBmaWxlcy5cclxuICAgICAqIEBwYXJhbSBuYW1lIE5hbWUgZm9yIHRoZSB0cmFuc2xhdGlvbiBwcm92aWRlclxyXG4gICAgICogQHBhcmFtIHBhdGggUGF0aCB0byB0aGUgZm9sZGVyXHJcbiAgICAgKi9cclxuICAgIGFkZFRyYW5zbGF0aW9uRm9sZGVyKG5hbWU6IHN0cmluZyA9ICcnLCBwYXRoOiBzdHJpbmcgPSAnJykge1xyXG4gICAgICAgIGlmICghdGhpcy5jdXN0b21Mb2FkZXIucHJvdmlkZXJSZWdpc3RlcmVkKG5hbWUpKSB7XHJcbiAgICAgICAgICAgIHRoaXMuY3VzdG9tTG9hZGVyLnJlZ2lzdGVyUHJvdmlkZXIobmFtZSwgcGF0aCk7XHJcblxyXG4gICAgICAgICAgICBpZiAodGhpcy51c2VyTGFuZykge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2FkVHJhbnNsYXRpb24odGhpcy51c2VyTGFuZywgdGhpcy5kZWZhdWx0TGFuZyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRUcmFuc2xhdGlvbih0aGlzLmRlZmF1bHRMYW5nKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExvYWRzIGEgdHJhbnNsYXRpb24gZmlsZS5cclxuICAgICAqIEBwYXJhbSBsYW5nIExhbmd1YWdlIGNvZGUgZm9yIHRoZSBsYW5ndWFnZSB0byBsb2FkXHJcbiAgICAgKiBAcGFyYW0gZmFsbGJhY2sgTGFuZ3VhZ2UgY29kZSB0byBmYWxsIGJhY2sgdG8gaWYgdGhlIGZpcnN0IG9uZSB3YXMgdW5hdmFpbGFibGVcclxuICAgICAqL1xyXG4gICAgbG9hZFRyYW5zbGF0aW9uKGxhbmc6IHN0cmluZywgZmFsbGJhY2s/OiBzdHJpbmcpIHtcclxuICAgICAgICB0aGlzLnRyYW5zbGF0ZS5nZXRUcmFuc2xhdGlvbihsYW5nKS5zdWJzY3JpYmUoXHJcbiAgICAgICAgICAgICgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMudHJhbnNsYXRlLnVzZShsYW5nKTtcclxuICAgICAgICAgICAgICAgIHRoaXMub25UcmFuc2xhdGlvbkNoYW5nZWQobGFuZyk7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgICgpID0+IHtcclxuICAgICAgICAgICAgICAgIGlmIChmYWxsYmFjayAmJiBmYWxsYmFjayAhPT0gbGFuZykge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9hZFRyYW5zbGF0aW9uKGZhbGxiYWNrKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUcmlnZ2VycyBhIG5vdGlmaWNhdGlvbiBjYWxsYmFjayB3aGVuIHRoZSB0cmFuc2xhdGlvbiBsYW5ndWFnZSBjaGFuZ2VzLlxyXG4gICAgICogQHBhcmFtIGxhbmcgVGhlIG5ldyBsYW5ndWFnZSBjb2RlXHJcbiAgICAgKi9cclxuICAgIG9uVHJhbnNsYXRpb25DaGFuZ2VkKGxhbmc6IHN0cmluZyk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMudHJhbnNsYXRlLm9uVHJhbnNsYXRpb25DaGFuZ2UubmV4dCh7XHJcbiAgICAgICAgICAgIGxhbmc6IGxhbmcsXHJcbiAgICAgICAgICAgIHRyYW5zbGF0aW9uczogdGhpcy5jdXN0b21Mb2FkZXIuZ2V0RnVsbFRyYW5zbGF0aW9uSlNPTihsYW5nKVxyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0cyB0aGUgdGFyZ2V0IGxhbmd1YWdlIGZvciB0cmFuc2xhdGlvbnMuXHJcbiAgICAgKiBAcGFyYW0gbGFuZyBDb2RlIG5hbWUgZm9yIHRoZSBsYW5ndWFnZVxyXG4gICAgICogQHJldHVybnMgVHJhbnNsYXRpb25zIGF2YWlsYWJsZSBmb3IgdGhlIGxhbmd1YWdlXHJcbiAgICAgKi9cclxuICAgIHVzZShsYW5nOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGFueT4ge1xyXG4gICAgICAgIHRoaXMuY3VzdG9tTG9hZGVyLmluaXQobGFuZyk7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudHJhbnNsYXRlLnVzZShsYW5nKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIHRyYW5zbGF0aW9uIGZvciB0aGUgc3VwcGxpZWQga2V5LlxyXG4gICAgICogQHBhcmFtIGtleSBLZXkgdG8gdHJhbnNsYXRlXHJcbiAgICAgKiBAcGFyYW0gaW50ZXJwb2xhdGVQYXJhbXMgU3RyaW5nKHMpIHRvIGJlIGludGVycG9sYXRlZCBpbnRvIHRoZSBtYWluIG1lc3NhZ2VcclxuICAgICAqIEByZXR1cm5zIFRyYW5zbGF0ZWQgdGV4dFxyXG4gICAgICovXHJcbiAgICBnZXQoa2V5OiBzdHJpbmcgfCBBcnJheTxzdHJpbmc+LCBpbnRlcnBvbGF0ZVBhcmFtcz86IE9iamVjdCk6IE9ic2VydmFibGU8c3RyaW5nIHwgYW55PiB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMudHJhbnNsYXRlLmdldChrZXksIGludGVycG9sYXRlUGFyYW1zKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIERpcmVjdGx5IHJldHVybnMgdGhlIHRyYW5zbGF0aW9uIGZvciB0aGUgc3VwcGxpZWQga2V5LlxyXG4gICAgICogQHBhcmFtIGtleSBLZXkgdG8gdHJhbnNsYXRlXHJcbiAgICAgKiBAcGFyYW0gaW50ZXJwb2xhdGVQYXJhbXMgU3RyaW5nKHMpIHRvIGJlIGludGVycG9sYXRlZCBpbnRvIHRoZSBtYWluIG1lc3NhZ2VcclxuICAgICAqIEByZXR1cm5zIFRyYW5zbGF0ZWQgdGV4dFxyXG4gICAgICovXHJcbiAgICBpbnN0YW50KGtleTogc3RyaW5nIHwgQXJyYXk8c3RyaW5nPiwgaW50ZXJwb2xhdGVQYXJhbXM/OiBPYmplY3QpOiBzdHJpbmcgfCBhbnkge1xyXG4gICAgICAgIHJldHVybiBrZXkgPyB0aGlzLnRyYW5zbGF0ZS5pbnN0YW50KGtleSwgaW50ZXJwb2xhdGVQYXJhbXMpIDogJyc7XHJcbiAgICB9XHJcbn1cclxuIl19