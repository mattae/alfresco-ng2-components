/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { Minimatch } from 'minimatch-browser';
import { Subject } from 'rxjs';
import { AppConfigService } from '../app-config/app-config.service';
import { FileUploadCompleteEvent, FileUploadDeleteEvent, FileUploadErrorEvent, FileUploadEvent } from '../events/file.event';
import { FileUploadStatus } from '../models/file.model';
import { AlfrescoApiService } from './alfresco-api.service';
import * as i0 from "@angular/core";
import * as i1 from "./alfresco-api.service";
import * as i2 from "../app-config/app-config.service";
var UploadService = /** @class */ (function () {
    function UploadService(apiService, appConfigService) {
        this.apiService = apiService;
        this.appConfigService = appConfigService;
        this.cache = {};
        this.totalComplete = 0;
        this.totalAborted = 0;
        this.totalError = 0;
        this.excludedFileList = [];
        this.matchingOptions = null;
        this.activeTask = null;
        this.queue = [];
        this.queueChanged = new Subject();
        this.fileUpload = new Subject();
        this.fileUploadStarting = new Subject();
        this.fileUploadCancelled = new Subject();
        this.fileUploadProgress = new Subject();
        this.fileUploadAborted = new Subject();
        this.fileUploadError = new Subject();
        this.fileUploadComplete = new Subject();
        this.fileUploadDeleted = new Subject();
        this.fileDeleted = new Subject();
    }
    /**
     * Checks whether the service is uploading a file.
     * @returns True if a file is uploading, false otherwise
     */
    /**
     * Checks whether the service is uploading a file.
     * @return {?} True if a file is uploading, false otherwise
     */
    UploadService.prototype.isUploading = /**
     * Checks whether the service is uploading a file.
     * @return {?} True if a file is uploading, false otherwise
     */
    function () {
        return this.activeTask ? true : false;
    };
    /**
     * Gets the file Queue
     * @returns Array of files that form the queue
     */
    /**
     * Gets the file Queue
     * @return {?} Array of files that form the queue
     */
    UploadService.prototype.getQueue = /**
     * Gets the file Queue
     * @return {?} Array of files that form the queue
     */
    function () {
        return this.queue;
    };
    /**
     * Adds files to the uploading queue to be uploaded
     * @param files One or more separate parameters or an array of files to queue
     * @returns Array of files that were not blocked from upload by the ignore list
     */
    /**
     * Adds files to the uploading queue to be uploaded
     * @param {...?} files One or more separate parameters or an array of files to queue
     * @return {?} Array of files that were not blocked from upload by the ignore list
     */
    UploadService.prototype.addToQueue = /**
     * Adds files to the uploading queue to be uploaded
     * @param {...?} files One or more separate parameters or an array of files to queue
     * @return {?} Array of files that were not blocked from upload by the ignore list
     */
    function () {
        var _this = this;
        var files = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            files[_i] = arguments[_i];
        }
        /** @type {?} */
        var allowedFiles = files.filter((/**
         * @param {?} currentFile
         * @return {?}
         */
        function (currentFile) { return _this.filterElement(currentFile); }));
        this.queue = this.queue.concat(allowedFiles);
        this.queueChanged.next(this.queue);
        return allowedFiles;
    };
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    UploadService.prototype.filterElement = /**
     * @private
     * @param {?} file
     * @return {?}
     */
    function (file) {
        var _this = this;
        /** @type {?} */
        var isAllowed = true;
        this.excludedFileList = (/** @type {?} */ (this.appConfigService.get('files.excluded')));
        if (this.excludedFileList) {
            this.matchingOptions = this.appConfigService.get('files.match-options');
            isAllowed = this.excludedFileList.filter((/**
             * @param {?} pattern
             * @return {?}
             */
            function (pattern) {
                /** @type {?} */
                var minimatch = new Minimatch(pattern, _this.matchingOptions);
                return minimatch.match(file.name);
            })).length === 0;
        }
        return isAllowed;
    };
    /**
     * Finds all the files in the queue that are not yet uploaded and uploads them into the directory folder.
     * @param emitter Emitter to invoke on file status change
     */
    /**
     * Finds all the files in the queue that are not yet uploaded and uploads them into the directory folder.
     * @param {?=} emitter Emitter to invoke on file status change
     * @return {?}
     */
    UploadService.prototype.uploadFilesInTheQueue = /**
     * Finds all the files in the queue that are not yet uploaded and uploads them into the directory folder.
     * @param {?=} emitter Emitter to invoke on file status change
     * @return {?}
     */
    function (emitter) {
        var _this = this;
        if (!this.activeTask) {
            /** @type {?} */
            var file = this.queue.find((/**
             * @param {?} currentFile
             * @return {?}
             */
            function (currentFile) { return currentFile.status === FileUploadStatus.Pending; }));
            if (file) {
                this.onUploadStarting(file);
                /** @type {?} */
                var promise = this.beginUpload(file, emitter);
                this.activeTask = promise;
                this.cache[file.id] = promise;
                /** @type {?} */
                var next_1 = (/**
                 * @return {?}
                 */
                function () {
                    _this.activeTask = null;
                    setTimeout((/**
                     * @return {?}
                     */
                    function () { return _this.uploadFilesInTheQueue(emitter); }), 100);
                });
                promise.next = next_1;
                promise.then((/**
                 * @return {?}
                 */
                function () { return next_1(); }), (/**
                 * @return {?}
                 */
                function () { return next_1(); }));
            }
        }
    };
    /**
     * Cancels uploading of files.
     * @param files One or more separate parameters or an array of files specifying uploads to cancel
     */
    /**
     * Cancels uploading of files.
     * @param {...?} files One or more separate parameters or an array of files specifying uploads to cancel
     * @return {?}
     */
    UploadService.prototype.cancelUpload = /**
     * Cancels uploading of files.
     * @param {...?} files One or more separate parameters or an array of files specifying uploads to cancel
     * @return {?}
     */
    function () {
        var _this = this;
        var files = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            files[_i] = arguments[_i];
        }
        files.forEach((/**
         * @param {?} file
         * @return {?}
         */
        function (file) {
            /** @type {?} */
            var promise = _this.cache[file.id];
            if (promise) {
                promise.abort();
                delete _this.cache[file.id];
            }
            else {
                /** @type {?} */
                var performAction = _this.getAction(file);
                performAction();
            }
        }));
    };
    /** Clears the upload queue */
    /**
     * Clears the upload queue
     * @return {?}
     */
    UploadService.prototype.clearQueue = /**
     * Clears the upload queue
     * @return {?}
     */
    function () {
        this.queue = [];
        this.totalComplete = 0;
        this.totalAborted = 0;
        this.totalError = 0;
    };
    /**
     * Gets an upload promise for a file.
     * @param file The target file
     * @returns Promise that is resolved if the upload is successful or error otherwise
     */
    /**
     * Gets an upload promise for a file.
     * @param {?} file The target file
     * @return {?} Promise that is resolved if the upload is successful or error otherwise
     */
    UploadService.prototype.getUploadPromise = /**
     * Gets an upload promise for a file.
     * @param {?} file The target file
     * @return {?} Promise that is resolved if the upload is successful or error otherwise
     */
    function (file) {
        /** @type {?} */
        var opts = {
            renditions: 'doclib',
            include: ['allowableOperations']
        };
        if (file.options.newVersion === true) {
            opts.overwrite = true;
            opts.majorVersion = file.options.majorVersion;
            opts.comment = file.options.comment;
            opts.name = file.name;
        }
        else {
            opts.autoRename = true;
        }
        if (file.options.nodeType) {
            opts.nodeType = file.options.nodeType;
        }
        if (file.id) {
            return this.apiService.getInstance().node.updateNodeContent(file.id, file.file, opts);
        }
        else {
            return this.apiService.getInstance().upload.uploadFile(file.file, file.options.path, file.options.parentId, file.options, opts);
        }
    };
    /**
     * @private
     * @param {?} file
     * @param {?} emitter
     * @return {?}
     */
    UploadService.prototype.beginUpload = /**
     * @private
     * @param {?} file
     * @param {?} emitter
     * @return {?}
     */
    function (file, emitter) {
        var _this = this;
        /** @type {?} */
        var promise = this.getUploadPromise(file);
        promise.on('progress', (/**
         * @param {?} progress
         * @return {?}
         */
        function (progress) {
            _this.onUploadProgress(file, progress);
        }))
            .on('abort', (/**
         * @return {?}
         */
        function () {
            _this.onUploadAborted(file);
            if (emitter) {
                emitter.emit({ value: 'File aborted' });
            }
        }))
            .on('error', (/**
         * @param {?} err
         * @return {?}
         */
        function (err) {
            _this.onUploadError(file, err);
            if (emitter) {
                emitter.emit({ value: 'Error file uploaded' });
            }
        }))
            .on('success', (/**
         * @param {?} data
         * @return {?}
         */
        function (data) {
            _this.onUploadComplete(file, data);
            if (emitter) {
                emitter.emit({ value: data });
            }
        }))
            .catch((/**
         * @param {?} err
         * @return {?}
         */
        function (err) {
        }));
        return promise;
    };
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    UploadService.prototype.onUploadStarting = /**
     * @private
     * @param {?} file
     * @return {?}
     */
    function (file) {
        if (file) {
            file.status = FileUploadStatus.Starting;
            /** @type {?} */
            var event_1 = new FileUploadEvent(file, FileUploadStatus.Starting);
            this.fileUpload.next(event_1);
            this.fileUploadStarting.next(event_1);
        }
    };
    /**
     * @private
     * @param {?} file
     * @param {?} progress
     * @return {?}
     */
    UploadService.prototype.onUploadProgress = /**
     * @private
     * @param {?} file
     * @param {?} progress
     * @return {?}
     */
    function (file, progress) {
        if (file) {
            file.progress = progress;
            file.status = FileUploadStatus.Progress;
            /** @type {?} */
            var event_2 = new FileUploadEvent(file, FileUploadStatus.Progress);
            this.fileUpload.next(event_2);
            this.fileUploadProgress.next(event_2);
        }
    };
    /**
     * @private
     * @param {?} file
     * @param {?} error
     * @return {?}
     */
    UploadService.prototype.onUploadError = /**
     * @private
     * @param {?} file
     * @param {?} error
     * @return {?}
     */
    function (file, error) {
        if (file) {
            file.errorCode = (error || {}).status;
            file.status = FileUploadStatus.Error;
            this.totalError++;
            /** @type {?} */
            var promise = this.cache[file.id];
            if (promise) {
                delete this.cache[file.id];
            }
            /** @type {?} */
            var event_3 = new FileUploadErrorEvent(file, error, this.totalError);
            this.fileUpload.next(event_3);
            this.fileUploadError.next(event_3);
        }
    };
    /**
     * @private
     * @param {?} file
     * @param {?} data
     * @return {?}
     */
    UploadService.prototype.onUploadComplete = /**
     * @private
     * @param {?} file
     * @param {?} data
     * @return {?}
     */
    function (file, data) {
        if (file) {
            file.status = FileUploadStatus.Complete;
            file.data = data;
            this.totalComplete++;
            /** @type {?} */
            var promise = this.cache[file.id];
            if (promise) {
                delete this.cache[file.id];
            }
            /** @type {?} */
            var event_4 = new FileUploadCompleteEvent(file, this.totalComplete, data, this.totalAborted);
            this.fileUpload.next(event_4);
            this.fileUploadComplete.next(event_4);
        }
    };
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    UploadService.prototype.onUploadAborted = /**
     * @private
     * @param {?} file
     * @return {?}
     */
    function (file) {
        if (file) {
            file.status = FileUploadStatus.Aborted;
            this.totalAborted++;
            /** @type {?} */
            var promise = this.cache[file.id];
            if (promise) {
                delete this.cache[file.id];
            }
            /** @type {?} */
            var event_5 = new FileUploadEvent(file, FileUploadStatus.Aborted);
            this.fileUpload.next(event_5);
            this.fileUploadAborted.next(event_5);
            promise.next();
        }
    };
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    UploadService.prototype.onUploadCancelled = /**
     * @private
     * @param {?} file
     * @return {?}
     */
    function (file) {
        if (file) {
            file.status = FileUploadStatus.Cancelled;
            /** @type {?} */
            var event_6 = new FileUploadEvent(file, FileUploadStatus.Cancelled);
            this.fileUpload.next(event_6);
            this.fileUploadCancelled.next(event_6);
        }
    };
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    UploadService.prototype.onUploadDeleted = /**
     * @private
     * @param {?} file
     * @return {?}
     */
    function (file) {
        if (file) {
            file.status = FileUploadStatus.Deleted;
            this.totalComplete--;
            /** @type {?} */
            var event_7 = new FileUploadDeleteEvent(file, this.totalComplete);
            this.fileUpload.next(event_7);
            this.fileUploadDeleted.next(event_7);
        }
    };
    /**
     * @private
     * @param {?} file
     * @return {?}
     */
    UploadService.prototype.getAction = /**
     * @private
     * @param {?} file
     * @return {?}
     */
    function (file) {
        var _this = this;
        var _a;
        /** @type {?} */
        var actions = (_a = {},
            _a[FileUploadStatus.Pending] = (/**
             * @return {?}
             */
            function () { return _this.onUploadCancelled(file); }),
            _a[FileUploadStatus.Deleted] = (/**
             * @return {?}
             */
            function () { return _this.onUploadDeleted(file); }),
            _a[FileUploadStatus.Error] = (/**
             * @return {?}
             */
            function () { return _this.onUploadError(file, null); }),
            _a);
        return actions[file.status];
    };
    UploadService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    UploadService.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: AppConfigService }
    ]; };
    /** @nocollapse */ UploadService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function UploadService_Factory() { return new UploadService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.AppConfigService)); }, token: UploadService, providedIn: "root" });
    return UploadService;
}());
export { UploadService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    UploadService.prototype.cache;
    /**
     * @type {?}
     * @private
     */
    UploadService.prototype.totalComplete;
    /**
     * @type {?}
     * @private
     */
    UploadService.prototype.totalAborted;
    /**
     * @type {?}
     * @private
     */
    UploadService.prototype.totalError;
    /**
     * @type {?}
     * @private
     */
    UploadService.prototype.excludedFileList;
    /**
     * @type {?}
     * @private
     */
    UploadService.prototype.matchingOptions;
    /** @type {?} */
    UploadService.prototype.activeTask;
    /** @type {?} */
    UploadService.prototype.queue;
    /** @type {?} */
    UploadService.prototype.queueChanged;
    /** @type {?} */
    UploadService.prototype.fileUpload;
    /** @type {?} */
    UploadService.prototype.fileUploadStarting;
    /** @type {?} */
    UploadService.prototype.fileUploadCancelled;
    /** @type {?} */
    UploadService.prototype.fileUploadProgress;
    /** @type {?} */
    UploadService.prototype.fileUploadAborted;
    /** @type {?} */
    UploadService.prototype.fileUploadError;
    /** @type {?} */
    UploadService.prototype.fileUploadComplete;
    /** @type {?} */
    UploadService.prototype.fileUploadDeleted;
    /** @type {?} */
    UploadService.prototype.fileDeleted;
    /**
     * @type {?}
     * @protected
     */
    UploadService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    UploadService.prototype.appConfigService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXBsb2FkLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXJ2aWNlcy91cGxvYWQuc2VydmljZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQWdCLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUN6RCxPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDOUMsT0FBTyxFQUFFLE9BQU8sRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUMvQixPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxrQ0FBa0MsQ0FBQztBQUNwRSxPQUFPLEVBQ0gsdUJBQXVCLEVBQ3ZCLHFCQUFxQixFQUNyQixvQkFBb0IsRUFDcEIsZUFBZSxFQUNsQixNQUFNLHNCQUFzQixDQUFDO0FBQzlCLE9BQU8sRUFBaUMsZ0JBQWdCLEVBQUUsTUFBTSxzQkFBc0IsQ0FBQztBQUN2RixPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQzs7OztBQUU1RDtJQTBCSSx1QkFBc0IsVUFBOEIsRUFBVSxnQkFBa0M7UUFBMUUsZUFBVSxHQUFWLFVBQVUsQ0FBb0I7UUFBVSxxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBckJ4RixVQUFLLEdBQTJCLEVBQUUsQ0FBQztRQUNuQyxrQkFBYSxHQUFXLENBQUMsQ0FBQztRQUMxQixpQkFBWSxHQUFXLENBQUMsQ0FBQztRQUN6QixlQUFVLEdBQVcsQ0FBQyxDQUFDO1FBQ3ZCLHFCQUFnQixHQUFhLEVBQUUsQ0FBQztRQUNoQyxvQkFBZSxHQUFRLElBQUksQ0FBQztRQUVwQyxlQUFVLEdBQWlCLElBQUksQ0FBQztRQUNoQyxVQUFLLEdBQWdCLEVBQUUsQ0FBQztRQUV4QixpQkFBWSxHQUF5QixJQUFJLE9BQU8sRUFBZSxDQUFDO1FBQ2hFLGVBQVUsR0FBNkIsSUFBSSxPQUFPLEVBQW1CLENBQUM7UUFDdEUsdUJBQWtCLEdBQTZCLElBQUksT0FBTyxFQUFtQixDQUFDO1FBQzlFLHdCQUFtQixHQUE2QixJQUFJLE9BQU8sRUFBbUIsQ0FBQztRQUMvRSx1QkFBa0IsR0FBNkIsSUFBSSxPQUFPLEVBQW1CLENBQUM7UUFDOUUsc0JBQWlCLEdBQTZCLElBQUksT0FBTyxFQUFtQixDQUFDO1FBQzdFLG9CQUFlLEdBQWtDLElBQUksT0FBTyxFQUF3QixDQUFDO1FBQ3JGLHVCQUFrQixHQUFxQyxJQUFJLE9BQU8sRUFBMkIsQ0FBQztRQUM5RixzQkFBaUIsR0FBbUMsSUFBSSxPQUFPLEVBQXlCLENBQUM7UUFDekYsZ0JBQVcsR0FBb0IsSUFBSSxPQUFPLEVBQVUsQ0FBQztJQUdyRCxDQUFDO0lBRUQ7OztPQUdHOzs7OztJQUNILG1DQUFXOzs7O0lBQVg7UUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO0lBQzFDLENBQUM7SUFFRDs7O09BR0c7Ozs7O0lBQ0gsZ0NBQVE7Ozs7SUFBUjtRQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQztJQUN0QixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsa0NBQVU7Ozs7O0lBQVY7UUFBQSxpQkFLQztRQUxVLGVBQXFCO2FBQXJCLFVBQXFCLEVBQXJCLHFCQUFxQixFQUFyQixJQUFxQjtZQUFyQiwwQkFBcUI7OztZQUN0QixZQUFZLEdBQUcsS0FBSyxDQUFDLE1BQU07Ozs7UUFBQyxVQUFDLFdBQVcsSUFBSyxPQUFBLEtBQUksQ0FBQyxhQUFhLENBQUMsV0FBVyxDQUFDLEVBQS9CLENBQStCLEVBQUM7UUFDbkYsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDbkMsT0FBTyxZQUFZLENBQUM7SUFDeEIsQ0FBQzs7Ozs7O0lBRU8scUNBQWE7Ozs7O0lBQXJCLFVBQXNCLElBQWU7UUFBckMsaUJBY0M7O1lBYk8sU0FBUyxHQUFHLElBQUk7UUFFcEIsSUFBSSxDQUFDLGdCQUFnQixHQUFHLG1CQUFXLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsZ0JBQWdCLENBQUMsRUFBQSxDQUFDO1FBQy9FLElBQUksSUFBSSxDQUFDLGdCQUFnQixFQUFFO1lBRXZCLElBQUksQ0FBQyxlQUFlLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBRXhFLFNBQVMsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTTs7OztZQUFDLFVBQUMsT0FBTzs7b0JBQ3ZDLFNBQVMsR0FBRyxJQUFJLFNBQVMsQ0FBQyxPQUFPLEVBQUUsS0FBSSxDQUFDLGVBQWUsQ0FBQztnQkFDOUQsT0FBTyxTQUFTLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUN0QyxDQUFDLEVBQUMsQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDO1NBQ25CO1FBQ0QsT0FBTyxTQUFTLENBQUM7SUFDckIsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7O0lBQ0gsNkNBQXFCOzs7OztJQUFyQixVQUFzQixPQUEyQjtRQUFqRCxpQkF1QkM7UUF0QkcsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLEVBQUU7O2dCQUNaLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUk7Ozs7WUFBQyxVQUFDLFdBQVcsSUFBSyxPQUFBLFdBQVcsQ0FBQyxNQUFNLEtBQUssZ0JBQWdCLENBQUMsT0FBTyxFQUEvQyxDQUErQyxFQUFDO1lBQzlGLElBQUksSUFBSSxFQUFFO2dCQUNOLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsQ0FBQzs7b0JBRXRCLE9BQU8sR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLElBQUksRUFBRSxPQUFPLENBQUM7Z0JBQy9DLElBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDO2dCQUMxQixJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsR0FBRyxPQUFPLENBQUM7O29CQUV4QixNQUFJOzs7Z0JBQUc7b0JBQ1QsS0FBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7b0JBQ3ZCLFVBQVU7OztvQkFBQyxjQUFNLE9BQUEsS0FBSSxDQUFDLHFCQUFxQixDQUFDLE9BQU8sQ0FBQyxFQUFuQyxDQUFtQyxHQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUMvRCxDQUFDLENBQUE7Z0JBRUQsT0FBTyxDQUFDLElBQUksR0FBRyxNQUFJLENBQUM7Z0JBRXBCLE9BQU8sQ0FBQyxJQUFJOzs7Z0JBQ1IsY0FBTSxPQUFBLE1BQUksRUFBRSxFQUFOLENBQU07OztnQkFDWixjQUFNLE9BQUEsTUFBSSxFQUFFLEVBQU4sQ0FBTSxFQUNmLENBQUM7YUFDTDtTQUNKO0lBQ0wsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7O0lBQ0gsb0NBQVk7Ozs7O0lBQVo7UUFBQSxpQkFZQztRQVpZLGVBQXFCO2FBQXJCLFVBQXFCLEVBQXJCLHFCQUFxQixFQUFyQixJQUFxQjtZQUFyQiwwQkFBcUI7O1FBQzlCLEtBQUssQ0FBQyxPQUFPOzs7O1FBQUMsVUFBQyxJQUFJOztnQkFDVCxPQUFPLEdBQUcsS0FBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO1lBRW5DLElBQUksT0FBTyxFQUFFO2dCQUNULE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQztnQkFDaEIsT0FBTyxLQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUM5QjtpQkFBTTs7b0JBQ0csYUFBYSxHQUFHLEtBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDO2dCQUMxQyxhQUFhLEVBQUUsQ0FBQzthQUNuQjtRQUNMLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQztJQUVELDhCQUE4Qjs7Ozs7SUFDOUIsa0NBQVU7Ozs7SUFBVjtRQUNJLElBQUksQ0FBQyxLQUFLLEdBQUcsRUFBRSxDQUFDO1FBQ2hCLElBQUksQ0FBQyxhQUFhLEdBQUcsQ0FBQyxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxZQUFZLEdBQUcsQ0FBQyxDQUFDO1FBQ3RCLElBQUksQ0FBQyxVQUFVLEdBQUcsQ0FBQyxDQUFDO0lBQ3hCLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCx3Q0FBZ0I7Ozs7O0lBQWhCLFVBQWlCLElBQWU7O1lBQ3RCLElBQUksR0FBUTtZQUNkLFVBQVUsRUFBRSxRQUFRO1lBQ3BCLE9BQU8sRUFBRSxDQUFDLHFCQUFxQixDQUFDO1NBQ25DO1FBRUQsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLFVBQVUsS0FBSyxJQUFJLEVBQUU7WUFDbEMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDdEIsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFlBQVksQ0FBQztZQUM5QyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDO1lBQ3BDLElBQUksQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztTQUN6QjthQUFNO1lBQ0gsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUM7U0FDMUI7UUFFRCxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUFFO1lBQ3ZCLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUM7U0FDekM7UUFFRCxJQUFJLElBQUksQ0FBQyxFQUFFLEVBQUU7WUFDVCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLGlCQUFpQixDQUN2RCxJQUFJLENBQUMsRUFBRSxFQUNQLElBQUksQ0FBQyxJQUFJLEVBQ1QsSUFBSSxDQUNQLENBQUM7U0FDTDthQUFNO1lBQ0gsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLE1BQU0sQ0FBQyxVQUFVLENBQ2xELElBQUksQ0FBQyxJQUFJLEVBQ1QsSUFBSSxDQUFDLE9BQU8sQ0FBQyxJQUFJLEVBQ2pCLElBQUksQ0FBQyxPQUFPLENBQUMsUUFBUSxFQUNyQixJQUFJLENBQUMsT0FBTyxFQUNaLElBQUksQ0FDUCxDQUFDO1NBQ0w7SUFDTCxDQUFDOzs7Ozs7O0lBRU8sbUNBQVc7Ozs7OztJQUFuQixVQUFvQixJQUFlLEVBQUUsT0FBMEI7UUFBL0QsaUJBNkJDOztZQTNCUyxPQUFPLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksQ0FBQztRQUUzQyxPQUFPLENBQUMsRUFBRSxDQUFDLFVBQVU7Ozs7UUFBRSxVQUFDLFFBQTRCO1lBQ2hELEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDMUMsQ0FBQyxFQUFDO2FBQ0csRUFBRSxDQUFDLE9BQU87OztRQUFFO1lBQ1QsS0FBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsQ0FBQztZQUMzQixJQUFJLE9BQU8sRUFBRTtnQkFDVCxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLGNBQWMsRUFBRSxDQUFDLENBQUM7YUFDM0M7UUFDTCxDQUFDLEVBQUM7YUFDRCxFQUFFLENBQUMsT0FBTzs7OztRQUFFLFVBQUMsR0FBRztZQUNiLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxFQUFFLEdBQUcsQ0FBQyxDQUFDO1lBQzlCLElBQUksT0FBTyxFQUFFO2dCQUNULE9BQU8sQ0FBQyxJQUFJLENBQUMsRUFBRSxLQUFLLEVBQUUscUJBQXFCLEVBQUUsQ0FBQyxDQUFDO2FBQ2xEO1FBQ0wsQ0FBQyxFQUFDO2FBQ0QsRUFBRSxDQUFDLFNBQVM7Ozs7UUFBRSxVQUFDLElBQUk7WUFDaEIsS0FBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNsQyxJQUFJLE9BQU8sRUFBRTtnQkFDVCxPQUFPLENBQUMsSUFBSSxDQUFDLEVBQUUsS0FBSyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7YUFDakM7UUFDTCxDQUFDLEVBQUM7YUFDRCxLQUFLOzs7O1FBQUMsVUFBQyxHQUFHO1FBQ1gsQ0FBQyxFQUFDLENBQUM7UUFFUCxPQUFPLE9BQU8sQ0FBQztJQUNuQixDQUFDOzs7Ozs7SUFFTyx3Q0FBZ0I7Ozs7O0lBQXhCLFVBQXlCLElBQWU7UUFDcEMsSUFBSSxJQUFJLEVBQUU7WUFDTixJQUFJLENBQUMsTUFBTSxHQUFHLGdCQUFnQixDQUFDLFFBQVEsQ0FBQzs7Z0JBQ2xDLE9BQUssR0FBRyxJQUFJLGVBQWUsQ0FBQyxJQUFJLEVBQUUsZ0JBQWdCLENBQUMsUUFBUSxDQUFDO1lBQ2xFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQUssQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBSyxDQUFDLENBQUM7U0FDdkM7SUFDTCxDQUFDOzs7Ozs7O0lBRU8sd0NBQWdCOzs7Ozs7SUFBeEIsVUFBeUIsSUFBZSxFQUFFLFFBQTRCO1FBQ2xFLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxDQUFDLFFBQVEsR0FBRyxRQUFRLENBQUM7WUFDekIsSUFBSSxDQUFDLE1BQU0sR0FBRyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7O2dCQUVsQyxPQUFLLEdBQUcsSUFBSSxlQUFlLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLFFBQVEsQ0FBQztZQUNsRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFLLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsa0JBQWtCLENBQUMsSUFBSSxDQUFDLE9BQUssQ0FBQyxDQUFDO1NBQ3ZDO0lBQ0wsQ0FBQzs7Ozs7OztJQUVPLHFDQUFhOzs7Ozs7SUFBckIsVUFBc0IsSUFBZSxFQUFFLEtBQVU7UUFDN0MsSUFBSSxJQUFJLEVBQUU7WUFDTixJQUFJLENBQUMsU0FBUyxHQUFHLENBQUUsS0FBSyxJQUFJLEVBQUUsQ0FBRSxDQUFDLE1BQU0sQ0FBQztZQUN4QyxJQUFJLENBQUMsTUFBTSxHQUFHLGdCQUFnQixDQUFDLEtBQUssQ0FBQztZQUNyQyxJQUFJLENBQUMsVUFBVSxFQUFFLENBQUM7O2dCQUVaLE9BQU8sR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUM7WUFDbkMsSUFBSSxPQUFPLEVBQUU7Z0JBQ1QsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsQ0FBQzthQUM5Qjs7Z0JBRUssT0FBSyxHQUFHLElBQUksb0JBQW9CLENBQUMsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDO1lBQ3BFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQUssQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLE9BQUssQ0FBQyxDQUFDO1NBQ3BDO0lBQ0wsQ0FBQzs7Ozs7OztJQUVPLHdDQUFnQjs7Ozs7O0lBQXhCLFVBQXlCLElBQWUsRUFBRSxJQUFTO1FBQy9DLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxDQUFDLE1BQU0sR0FBRyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUM7WUFDeEMsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUM7WUFDakIsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDOztnQkFFZixPQUFPLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDO1lBQ25DLElBQUksT0FBTyxFQUFFO2dCQUNULE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsRUFBRSxDQUFDLENBQUM7YUFDOUI7O2dCQUVLLE9BQUssR0FBRyxJQUFJLHVCQUF1QixDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDO1lBQzVGLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQUssQ0FBQyxDQUFDO1lBQzVCLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBSyxDQUFDLENBQUM7U0FDdkM7SUFDTCxDQUFDOzs7Ozs7SUFFTyx1Q0FBZTs7Ozs7SUFBdkIsVUFBd0IsSUFBZTtRQUNuQyxJQUFJLElBQUksRUFBRTtZQUNOLElBQUksQ0FBQyxNQUFNLEdBQUcsZ0JBQWdCLENBQUMsT0FBTyxDQUFDO1lBQ3ZDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQzs7Z0JBRWQsT0FBTyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQztZQUNuQyxJQUFJLE9BQU8sRUFBRTtnQkFDVCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2FBQzlCOztnQkFFSyxPQUFLLEdBQUcsSUFBSSxlQUFlLENBQUMsSUFBSSxFQUFFLGdCQUFnQixDQUFDLE9BQU8sQ0FBQztZQUNqRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFLLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE9BQUssQ0FBQyxDQUFDO1lBQ25DLE9BQU8sQ0FBQyxJQUFJLEVBQUUsQ0FBQztTQUNsQjtJQUNMLENBQUM7Ozs7OztJQUVPLHlDQUFpQjs7Ozs7SUFBekIsVUFBMEIsSUFBZTtRQUNyQyxJQUFJLElBQUksRUFBRTtZQUNOLElBQUksQ0FBQyxNQUFNLEdBQUcsZ0JBQWdCLENBQUMsU0FBUyxDQUFDOztnQkFFbkMsT0FBSyxHQUFHLElBQUksZUFBZSxDQUFDLElBQUksRUFBRSxnQkFBZ0IsQ0FBQyxTQUFTLENBQUM7WUFDbkUsSUFBSSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsT0FBSyxDQUFDLENBQUM7WUFDNUIsSUFBSSxDQUFDLG1CQUFtQixDQUFDLElBQUksQ0FBQyxPQUFLLENBQUMsQ0FBQztTQUN4QztJQUNMLENBQUM7Ozs7OztJQUVPLHVDQUFlOzs7OztJQUF2QixVQUF3QixJQUFlO1FBQ25DLElBQUksSUFBSSxFQUFFO1lBQ04sSUFBSSxDQUFDLE1BQU0sR0FBRyxnQkFBZ0IsQ0FBQyxPQUFPLENBQUM7WUFDdkMsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDOztnQkFFZixPQUFLLEdBQUcsSUFBSSxxQkFBcUIsQ0FBQyxJQUFJLEVBQUUsSUFBSSxDQUFDLGFBQWEsQ0FBQztZQUNqRSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFLLENBQUMsQ0FBQztZQUM1QixJQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLE9BQUssQ0FBQyxDQUFDO1NBQ3RDO0lBQ0wsQ0FBQzs7Ozs7O0lBRU8saUNBQVM7Ozs7O0lBQWpCLFVBQWtCLElBQUk7UUFBdEIsaUJBUUM7OztZQVBTLE9BQU87WUFDVCxHQUFDLGdCQUFnQixDQUFDLE9BQU87OztZQUFHLGNBQU0sT0FBQSxLQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxDQUFDLEVBQTVCLENBQTRCLENBQUE7WUFDOUQsR0FBQyxnQkFBZ0IsQ0FBQyxPQUFPOzs7WUFBRyxjQUFNLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBMUIsQ0FBMEIsQ0FBQTtZQUM1RCxHQUFDLGdCQUFnQixDQUFDLEtBQUs7OztZQUFHLGNBQU0sT0FBQSxLQUFJLENBQUMsYUFBYSxDQUFDLElBQUksRUFBRSxJQUFJLENBQUMsRUFBOUIsQ0FBOEIsQ0FBQTtlQUNqRTtRQUVELE9BQU8sT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNoQyxDQUFDOztnQkE1U0osVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFKUSxrQkFBa0I7Z0JBUmxCLGdCQUFnQjs7O3dCQXBCekI7Q0EyVUMsQUE3U0QsSUE2U0M7U0ExU1ksYUFBYTs7Ozs7O0lBRXRCLDhCQUEyQzs7Ozs7SUFDM0Msc0NBQWtDOzs7OztJQUNsQyxxQ0FBaUM7Ozs7O0lBQ2pDLG1DQUErQjs7Ozs7SUFDL0IseUNBQXdDOzs7OztJQUN4Qyx3Q0FBb0M7O0lBRXBDLG1DQUFnQzs7SUFDaEMsOEJBQXdCOztJQUV4QixxQ0FBZ0U7O0lBQ2hFLG1DQUFzRTs7SUFDdEUsMkNBQThFOztJQUM5RSw0Q0FBK0U7O0lBQy9FLDJDQUE4RTs7SUFDOUUsMENBQTZFOztJQUM3RSx3Q0FBcUY7O0lBQ3JGLDJDQUE4Rjs7SUFDOUYsMENBQXlGOztJQUN6RixvQ0FBcUQ7Ozs7O0lBRXpDLG1DQUF3Qzs7Ozs7SUFBRSx5Q0FBMEMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgRXZlbnRFbWl0dGVyLCBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1pbmltYXRjaCB9IGZyb20gJ21pbmltYXRjaC1icm93c2VyJztcclxuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBBcHBDb25maWdTZXJ2aWNlIH0gZnJvbSAnLi4vYXBwLWNvbmZpZy9hcHAtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQge1xyXG4gICAgRmlsZVVwbG9hZENvbXBsZXRlRXZlbnQsXHJcbiAgICBGaWxlVXBsb2FkRGVsZXRlRXZlbnQsXHJcbiAgICBGaWxlVXBsb2FkRXJyb3JFdmVudCxcclxuICAgIEZpbGVVcGxvYWRFdmVudFxyXG59IGZyb20gJy4uL2V2ZW50cy9maWxlLmV2ZW50JztcclxuaW1wb3J0IHsgRmlsZU1vZGVsLCBGaWxlVXBsb2FkUHJvZ3Jlc3MsIEZpbGVVcGxvYWRTdGF0dXMgfSBmcm9tICcuLi9tb2RlbHMvZmlsZS5tb2RlbCc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4vYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBVcGxvYWRTZXJ2aWNlIHtcclxuXHJcbiAgICBwcml2YXRlIGNhY2hlOiB7IFtrZXk6IHN0cmluZ106IGFueSB9ID0ge307XHJcbiAgICBwcml2YXRlIHRvdGFsQ29tcGxldGU6IG51bWJlciA9IDA7XHJcbiAgICBwcml2YXRlIHRvdGFsQWJvcnRlZDogbnVtYmVyID0gMDtcclxuICAgIHByaXZhdGUgdG90YWxFcnJvcjogbnVtYmVyID0gMDtcclxuICAgIHByaXZhdGUgZXhjbHVkZWRGaWxlTGlzdDogc3RyaW5nW10gPSBbXTtcclxuICAgIHByaXZhdGUgbWF0Y2hpbmdPcHRpb25zOiBhbnkgPSBudWxsO1xyXG5cclxuICAgIGFjdGl2ZVRhc2s6IFByb21pc2U8YW55PiA9IG51bGw7XHJcbiAgICBxdWV1ZTogRmlsZU1vZGVsW10gPSBbXTtcclxuXHJcbiAgICBxdWV1ZUNoYW5nZWQ6IFN1YmplY3Q8RmlsZU1vZGVsW10+ID0gbmV3IFN1YmplY3Q8RmlsZU1vZGVsW10+KCk7XHJcbiAgICBmaWxlVXBsb2FkOiBTdWJqZWN0PEZpbGVVcGxvYWRFdmVudD4gPSBuZXcgU3ViamVjdDxGaWxlVXBsb2FkRXZlbnQ+KCk7XHJcbiAgICBmaWxlVXBsb2FkU3RhcnRpbmc6IFN1YmplY3Q8RmlsZVVwbG9hZEV2ZW50PiA9IG5ldyBTdWJqZWN0PEZpbGVVcGxvYWRFdmVudD4oKTtcclxuICAgIGZpbGVVcGxvYWRDYW5jZWxsZWQ6IFN1YmplY3Q8RmlsZVVwbG9hZEV2ZW50PiA9IG5ldyBTdWJqZWN0PEZpbGVVcGxvYWRFdmVudD4oKTtcclxuICAgIGZpbGVVcGxvYWRQcm9ncmVzczogU3ViamVjdDxGaWxlVXBsb2FkRXZlbnQ+ID0gbmV3IFN1YmplY3Q8RmlsZVVwbG9hZEV2ZW50PigpO1xyXG4gICAgZmlsZVVwbG9hZEFib3J0ZWQ6IFN1YmplY3Q8RmlsZVVwbG9hZEV2ZW50PiA9IG5ldyBTdWJqZWN0PEZpbGVVcGxvYWRFdmVudD4oKTtcclxuICAgIGZpbGVVcGxvYWRFcnJvcjogU3ViamVjdDxGaWxlVXBsb2FkRXJyb3JFdmVudD4gPSBuZXcgU3ViamVjdDxGaWxlVXBsb2FkRXJyb3JFdmVudD4oKTtcclxuICAgIGZpbGVVcGxvYWRDb21wbGV0ZTogU3ViamVjdDxGaWxlVXBsb2FkQ29tcGxldGVFdmVudD4gPSBuZXcgU3ViamVjdDxGaWxlVXBsb2FkQ29tcGxldGVFdmVudD4oKTtcclxuICAgIGZpbGVVcGxvYWREZWxldGVkOiBTdWJqZWN0PEZpbGVVcGxvYWREZWxldGVFdmVudD4gPSBuZXcgU3ViamVjdDxGaWxlVXBsb2FkRGVsZXRlRXZlbnQ+KCk7XHJcbiAgICBmaWxlRGVsZXRlZDogU3ViamVjdDxzdHJpbmc+ID0gbmV3IFN1YmplY3Q8c3RyaW5nPigpO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByb3RlY3RlZCBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UsIHByaXZhdGUgYXBwQ29uZmlnU2VydmljZTogQXBwQ29uZmlnU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2tzIHdoZXRoZXIgdGhlIHNlcnZpY2UgaXMgdXBsb2FkaW5nIGEgZmlsZS5cclxuICAgICAqIEByZXR1cm5zIFRydWUgaWYgYSBmaWxlIGlzIHVwbG9hZGluZywgZmFsc2Ugb3RoZXJ3aXNlXHJcbiAgICAgKi9cclxuICAgIGlzVXBsb2FkaW5nKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmFjdGl2ZVRhc2sgPyB0cnVlIDogZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBmaWxlIFF1ZXVlXHJcbiAgICAgKiBAcmV0dXJucyBBcnJheSBvZiBmaWxlcyB0aGF0IGZvcm0gdGhlIHF1ZXVlXHJcbiAgICAgKi9cclxuICAgIGdldFF1ZXVlKCk6IEZpbGVNb2RlbFtdIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5xdWV1ZTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEFkZHMgZmlsZXMgdG8gdGhlIHVwbG9hZGluZyBxdWV1ZSB0byBiZSB1cGxvYWRlZFxyXG4gICAgICogQHBhcmFtIGZpbGVzIE9uZSBvciBtb3JlIHNlcGFyYXRlIHBhcmFtZXRlcnMgb3IgYW4gYXJyYXkgb2YgZmlsZXMgdG8gcXVldWVcclxuICAgICAqIEByZXR1cm5zIEFycmF5IG9mIGZpbGVzIHRoYXQgd2VyZSBub3QgYmxvY2tlZCBmcm9tIHVwbG9hZCBieSB0aGUgaWdub3JlIGxpc3RcclxuICAgICAqL1xyXG4gICAgYWRkVG9RdWV1ZSguLi5maWxlczogRmlsZU1vZGVsW10pOiBGaWxlTW9kZWxbXSB7XHJcbiAgICAgICAgY29uc3QgYWxsb3dlZEZpbGVzID0gZmlsZXMuZmlsdGVyKChjdXJyZW50RmlsZSkgPT4gdGhpcy5maWx0ZXJFbGVtZW50KGN1cnJlbnRGaWxlKSk7XHJcbiAgICAgICAgdGhpcy5xdWV1ZSA9IHRoaXMucXVldWUuY29uY2F0KGFsbG93ZWRGaWxlcyk7XHJcbiAgICAgICAgdGhpcy5xdWV1ZUNoYW5nZWQubmV4dCh0aGlzLnF1ZXVlKTtcclxuICAgICAgICByZXR1cm4gYWxsb3dlZEZpbGVzO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZmlsdGVyRWxlbWVudChmaWxlOiBGaWxlTW9kZWwpIHtcclxuICAgICAgICBsZXQgaXNBbGxvd2VkID0gdHJ1ZTtcclxuXHJcbiAgICAgICAgdGhpcy5leGNsdWRlZEZpbGVMaXN0ID0gPHN0cmluZ1tdPiB0aGlzLmFwcENvbmZpZ1NlcnZpY2UuZ2V0KCdmaWxlcy5leGNsdWRlZCcpO1xyXG4gICAgICAgIGlmICh0aGlzLmV4Y2x1ZGVkRmlsZUxpc3QpIHtcclxuXHJcbiAgICAgICAgICAgIHRoaXMubWF0Y2hpbmdPcHRpb25zID0gdGhpcy5hcHBDb25maWdTZXJ2aWNlLmdldCgnZmlsZXMubWF0Y2gtb3B0aW9ucycpO1xyXG5cclxuICAgICAgICAgICAgaXNBbGxvd2VkID0gdGhpcy5leGNsdWRlZEZpbGVMaXN0LmZpbHRlcigocGF0dGVybikgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgbWluaW1hdGNoID0gbmV3IE1pbmltYXRjaChwYXR0ZXJuLCB0aGlzLm1hdGNoaW5nT3B0aW9ucyk7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gbWluaW1hdGNoLm1hdGNoKGZpbGUubmFtZSk7XHJcbiAgICAgICAgICAgIH0pLmxlbmd0aCA9PT0gMDtcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIGlzQWxsb3dlZDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZpbmRzIGFsbCB0aGUgZmlsZXMgaW4gdGhlIHF1ZXVlIHRoYXQgYXJlIG5vdCB5ZXQgdXBsb2FkZWQgYW5kIHVwbG9hZHMgdGhlbSBpbnRvIHRoZSBkaXJlY3RvcnkgZm9sZGVyLlxyXG4gICAgICogQHBhcmFtIGVtaXR0ZXIgRW1pdHRlciB0byBpbnZva2Ugb24gZmlsZSBzdGF0dXMgY2hhbmdlXHJcbiAgICAgKi9cclxuICAgIHVwbG9hZEZpbGVzSW5UaGVRdWV1ZShlbWl0dGVyPzogRXZlbnRFbWl0dGVyPGFueT4pOiB2b2lkIHtcclxuICAgICAgICBpZiAoIXRoaXMuYWN0aXZlVGFzaykge1xyXG4gICAgICAgICAgICBjb25zdCBmaWxlID0gdGhpcy5xdWV1ZS5maW5kKChjdXJyZW50RmlsZSkgPT4gY3VycmVudEZpbGUuc3RhdHVzID09PSBGaWxlVXBsb2FkU3RhdHVzLlBlbmRpbmcpO1xyXG4gICAgICAgICAgICBpZiAoZmlsZSkge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vblVwbG9hZFN0YXJ0aW5nKGZpbGUpO1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLmJlZ2luVXBsb2FkKGZpbGUsIGVtaXR0ZXIpO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5hY3RpdmVUYXNrID0gcHJvbWlzZTtcclxuICAgICAgICAgICAgICAgIHRoaXMuY2FjaGVbZmlsZS5pZF0gPSBwcm9taXNlO1xyXG5cclxuICAgICAgICAgICAgICAgIGNvbnN0IG5leHQgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5hY3RpdmVUYXNrID0gbnVsbDtcclxuICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHRoaXMudXBsb2FkRmlsZXNJblRoZVF1ZXVlKGVtaXR0ZXIpLCAxMDApO1xyXG4gICAgICAgICAgICAgICAgfTtcclxuXHJcbiAgICAgICAgICAgICAgICBwcm9taXNlLm5leHQgPSBuZXh0O1xyXG5cclxuICAgICAgICAgICAgICAgIHByb21pc2UudGhlbihcclxuICAgICAgICAgICAgICAgICAgICAoKSA9PiBuZXh0KCksXHJcbiAgICAgICAgICAgICAgICAgICAgKCkgPT4gbmV4dCgpXHJcbiAgICAgICAgICAgICAgICApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2FuY2VscyB1cGxvYWRpbmcgb2YgZmlsZXMuXHJcbiAgICAgKiBAcGFyYW0gZmlsZXMgT25lIG9yIG1vcmUgc2VwYXJhdGUgcGFyYW1ldGVycyBvciBhbiBhcnJheSBvZiBmaWxlcyBzcGVjaWZ5aW5nIHVwbG9hZHMgdG8gY2FuY2VsXHJcbiAgICAgKi9cclxuICAgIGNhbmNlbFVwbG9hZCguLi5maWxlczogRmlsZU1vZGVsW10pIHtcclxuICAgICAgICBmaWxlcy5mb3JFYWNoKChmaWxlKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLmNhY2hlW2ZpbGUuaWRdO1xyXG5cclxuICAgICAgICAgICAgaWYgKHByb21pc2UpIHtcclxuICAgICAgICAgICAgICAgIHByb21pc2UuYWJvcnQoKTtcclxuICAgICAgICAgICAgICAgIGRlbGV0ZSB0aGlzLmNhY2hlW2ZpbGUuaWRdO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcGVyZm9ybUFjdGlvbiA9IHRoaXMuZ2V0QWN0aW9uKGZpbGUpO1xyXG4gICAgICAgICAgICAgICAgcGVyZm9ybUFjdGlvbigpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqIENsZWFycyB0aGUgdXBsb2FkIHF1ZXVlICovXHJcbiAgICBjbGVhclF1ZXVlKCkge1xyXG4gICAgICAgIHRoaXMucXVldWUgPSBbXTtcclxuICAgICAgICB0aGlzLnRvdGFsQ29tcGxldGUgPSAwO1xyXG4gICAgICAgIHRoaXMudG90YWxBYm9ydGVkID0gMDtcclxuICAgICAgICB0aGlzLnRvdGFsRXJyb3IgPSAwO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhbiB1cGxvYWQgcHJvbWlzZSBmb3IgYSBmaWxlLlxyXG4gICAgICogQHBhcmFtIGZpbGUgVGhlIHRhcmdldCBmaWxlXHJcbiAgICAgKiBAcmV0dXJucyBQcm9taXNlIHRoYXQgaXMgcmVzb2x2ZWQgaWYgdGhlIHVwbG9hZCBpcyBzdWNjZXNzZnVsIG9yIGVycm9yIG90aGVyd2lzZVxyXG4gICAgICovXHJcbiAgICBnZXRVcGxvYWRQcm9taXNlKGZpbGU6IEZpbGVNb2RlbCk6IGFueSB7XHJcbiAgICAgICAgY29uc3Qgb3B0czogYW55ID0ge1xyXG4gICAgICAgICAgICByZW5kaXRpb25zOiAnZG9jbGliJyxcclxuICAgICAgICAgICAgaW5jbHVkZTogWydhbGxvd2FibGVPcGVyYXRpb25zJ11cclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICBpZiAoZmlsZS5vcHRpb25zLm5ld1ZlcnNpb24gPT09IHRydWUpIHtcclxuICAgICAgICAgICAgb3B0cy5vdmVyd3JpdGUgPSB0cnVlO1xyXG4gICAgICAgICAgICBvcHRzLm1ham9yVmVyc2lvbiA9IGZpbGUub3B0aW9ucy5tYWpvclZlcnNpb247XHJcbiAgICAgICAgICAgIG9wdHMuY29tbWVudCA9IGZpbGUub3B0aW9ucy5jb21tZW50O1xyXG4gICAgICAgICAgICBvcHRzLm5hbWUgPSBmaWxlLm5hbWU7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgb3B0cy5hdXRvUmVuYW1lID0gdHJ1ZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChmaWxlLm9wdGlvbnMubm9kZVR5cGUpIHtcclxuICAgICAgICAgICAgb3B0cy5ub2RlVHlwZSA9IGZpbGUub3B0aW9ucy5ub2RlVHlwZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChmaWxlLmlkKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5ub2RlLnVwZGF0ZU5vZGVDb250ZW50KFxyXG4gICAgICAgICAgICAgICAgZmlsZS5pZCxcclxuICAgICAgICAgICAgICAgIGZpbGUuZmlsZSxcclxuICAgICAgICAgICAgICAgIG9wdHNcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkudXBsb2FkLnVwbG9hZEZpbGUoXHJcbiAgICAgICAgICAgICAgICBmaWxlLmZpbGUsXHJcbiAgICAgICAgICAgICAgICBmaWxlLm9wdGlvbnMucGF0aCxcclxuICAgICAgICAgICAgICAgIGZpbGUub3B0aW9ucy5wYXJlbnRJZCxcclxuICAgICAgICAgICAgICAgIGZpbGUub3B0aW9ucyxcclxuICAgICAgICAgICAgICAgIG9wdHNcclxuICAgICAgICAgICAgKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBiZWdpblVwbG9hZChmaWxlOiBGaWxlTW9kZWwsIGVtaXR0ZXI6IEV2ZW50RW1pdHRlcjxhbnk+KTogYW55IHtcclxuXHJcbiAgICAgICAgY29uc3QgcHJvbWlzZSA9IHRoaXMuZ2V0VXBsb2FkUHJvbWlzZShmaWxlKTtcclxuXHJcbiAgICAgICAgcHJvbWlzZS5vbigncHJvZ3Jlc3MnLCAocHJvZ3Jlc3M6IEZpbGVVcGxvYWRQcm9ncmVzcykgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLm9uVXBsb2FkUHJvZ3Jlc3MoZmlsZSwgcHJvZ3Jlc3MpO1xyXG4gICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5vbignYWJvcnQnLCAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9uVXBsb2FkQWJvcnRlZChmaWxlKTtcclxuICAgICAgICAgICAgICAgIGlmIChlbWl0dGVyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZW1pdHRlci5lbWl0KHsgdmFsdWU6ICdGaWxlIGFib3J0ZWQnIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAub24oJ2Vycm9yJywgKGVycikgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5vblVwbG9hZEVycm9yKGZpbGUsIGVycik7XHJcbiAgICAgICAgICAgICAgICBpZiAoZW1pdHRlcikge1xyXG4gICAgICAgICAgICAgICAgICAgIGVtaXR0ZXIuZW1pdCh7IHZhbHVlOiAnRXJyb3IgZmlsZSB1cGxvYWRlZCcgfSk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIC5vbignc3VjY2VzcycsIChkYXRhKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLm9uVXBsb2FkQ29tcGxldGUoZmlsZSwgZGF0YSk7XHJcbiAgICAgICAgICAgICAgICBpZiAoZW1pdHRlcikge1xyXG4gICAgICAgICAgICAgICAgICAgIGVtaXR0ZXIuZW1pdCh7IHZhbHVlOiBkYXRhIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAuY2F0Y2goKGVycikgPT4ge1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHByb21pc2U7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvblVwbG9hZFN0YXJ0aW5nKGZpbGU6IEZpbGVNb2RlbCk6IHZvaWQge1xyXG4gICAgICAgIGlmIChmaWxlKSB7XHJcbiAgICAgICAgICAgIGZpbGUuc3RhdHVzID0gRmlsZVVwbG9hZFN0YXR1cy5TdGFydGluZztcclxuICAgICAgICAgICAgY29uc3QgZXZlbnQgPSBuZXcgRmlsZVVwbG9hZEV2ZW50KGZpbGUsIEZpbGVVcGxvYWRTdGF0dXMuU3RhcnRpbmcpO1xyXG4gICAgICAgICAgICB0aGlzLmZpbGVVcGxvYWQubmV4dChldmVudCk7XHJcbiAgICAgICAgICAgIHRoaXMuZmlsZVVwbG9hZFN0YXJ0aW5nLm5leHQoZXZlbnQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG9uVXBsb2FkUHJvZ3Jlc3MoZmlsZTogRmlsZU1vZGVsLCBwcm9ncmVzczogRmlsZVVwbG9hZFByb2dyZXNzKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKGZpbGUpIHtcclxuICAgICAgICAgICAgZmlsZS5wcm9ncmVzcyA9IHByb2dyZXNzO1xyXG4gICAgICAgICAgICBmaWxlLnN0YXR1cyA9IEZpbGVVcGxvYWRTdGF0dXMuUHJvZ3Jlc3M7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBldmVudCA9IG5ldyBGaWxlVXBsb2FkRXZlbnQoZmlsZSwgRmlsZVVwbG9hZFN0YXR1cy5Qcm9ncmVzcyk7XHJcbiAgICAgICAgICAgIHRoaXMuZmlsZVVwbG9hZC5uZXh0KGV2ZW50KTtcclxuICAgICAgICAgICAgdGhpcy5maWxlVXBsb2FkUHJvZ3Jlc3MubmV4dChldmVudCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgb25VcGxvYWRFcnJvcihmaWxlOiBGaWxlTW9kZWwsIGVycm9yOiBhbnkpOiB2b2lkIHtcclxuICAgICAgICBpZiAoZmlsZSkge1xyXG4gICAgICAgICAgICBmaWxlLmVycm9yQ29kZSA9ICggZXJyb3IgfHwge30gKS5zdGF0dXM7XHJcbiAgICAgICAgICAgIGZpbGUuc3RhdHVzID0gRmlsZVVwbG9hZFN0YXR1cy5FcnJvcjtcclxuICAgICAgICAgICAgdGhpcy50b3RhbEVycm9yKys7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBwcm9taXNlID0gdGhpcy5jYWNoZVtmaWxlLmlkXTtcclxuICAgICAgICAgICAgaWYgKHByb21pc2UpIHtcclxuICAgICAgICAgICAgICAgIGRlbGV0ZSB0aGlzLmNhY2hlW2ZpbGUuaWRdO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb25zdCBldmVudCA9IG5ldyBGaWxlVXBsb2FkRXJyb3JFdmVudChmaWxlLCBlcnJvciwgdGhpcy50b3RhbEVycm9yKTtcclxuICAgICAgICAgICAgdGhpcy5maWxlVXBsb2FkLm5leHQoZXZlbnQpO1xyXG4gICAgICAgICAgICB0aGlzLmZpbGVVcGxvYWRFcnJvci5uZXh0KGV2ZW50KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvblVwbG9hZENvbXBsZXRlKGZpbGU6IEZpbGVNb2RlbCwgZGF0YTogYW55KTogdm9pZCB7XHJcbiAgICAgICAgaWYgKGZpbGUpIHtcclxuICAgICAgICAgICAgZmlsZS5zdGF0dXMgPSBGaWxlVXBsb2FkU3RhdHVzLkNvbXBsZXRlO1xyXG4gICAgICAgICAgICBmaWxlLmRhdGEgPSBkYXRhO1xyXG4gICAgICAgICAgICB0aGlzLnRvdGFsQ29tcGxldGUrKztcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLmNhY2hlW2ZpbGUuaWRdO1xyXG4gICAgICAgICAgICBpZiAocHJvbWlzZSkge1xyXG4gICAgICAgICAgICAgICAgZGVsZXRlIHRoaXMuY2FjaGVbZmlsZS5pZF07XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50ID0gbmV3IEZpbGVVcGxvYWRDb21wbGV0ZUV2ZW50KGZpbGUsIHRoaXMudG90YWxDb21wbGV0ZSwgZGF0YSwgdGhpcy50b3RhbEFib3J0ZWQpO1xyXG4gICAgICAgICAgICB0aGlzLmZpbGVVcGxvYWQubmV4dChldmVudCk7XHJcbiAgICAgICAgICAgIHRoaXMuZmlsZVVwbG9hZENvbXBsZXRlLm5leHQoZXZlbnQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG9uVXBsb2FkQWJvcnRlZChmaWxlOiBGaWxlTW9kZWwpOiB2b2lkIHtcclxuICAgICAgICBpZiAoZmlsZSkge1xyXG4gICAgICAgICAgICBmaWxlLnN0YXR1cyA9IEZpbGVVcGxvYWRTdGF0dXMuQWJvcnRlZDtcclxuICAgICAgICAgICAgdGhpcy50b3RhbEFib3J0ZWQrKztcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHByb21pc2UgPSB0aGlzLmNhY2hlW2ZpbGUuaWRdO1xyXG4gICAgICAgICAgICBpZiAocHJvbWlzZSkge1xyXG4gICAgICAgICAgICAgICAgZGVsZXRlIHRoaXMuY2FjaGVbZmlsZS5pZF07XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGNvbnN0IGV2ZW50ID0gbmV3IEZpbGVVcGxvYWRFdmVudChmaWxlLCBGaWxlVXBsb2FkU3RhdHVzLkFib3J0ZWQpO1xyXG4gICAgICAgICAgICB0aGlzLmZpbGVVcGxvYWQubmV4dChldmVudCk7XHJcbiAgICAgICAgICAgIHRoaXMuZmlsZVVwbG9hZEFib3J0ZWQubmV4dChldmVudCk7XHJcbiAgICAgICAgICAgIHByb21pc2UubmV4dCgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIG9uVXBsb2FkQ2FuY2VsbGVkKGZpbGU6IEZpbGVNb2RlbCk6IHZvaWQge1xyXG4gICAgICAgIGlmIChmaWxlKSB7XHJcbiAgICAgICAgICAgIGZpbGUuc3RhdHVzID0gRmlsZVVwbG9hZFN0YXR1cy5DYW5jZWxsZWQ7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBldmVudCA9IG5ldyBGaWxlVXBsb2FkRXZlbnQoZmlsZSwgRmlsZVVwbG9hZFN0YXR1cy5DYW5jZWxsZWQpO1xyXG4gICAgICAgICAgICB0aGlzLmZpbGVVcGxvYWQubmV4dChldmVudCk7XHJcbiAgICAgICAgICAgIHRoaXMuZmlsZVVwbG9hZENhbmNlbGxlZC5uZXh0KGV2ZW50KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBvblVwbG9hZERlbGV0ZWQoZmlsZTogRmlsZU1vZGVsKTogdm9pZCB7XHJcbiAgICAgICAgaWYgKGZpbGUpIHtcclxuICAgICAgICAgICAgZmlsZS5zdGF0dXMgPSBGaWxlVXBsb2FkU3RhdHVzLkRlbGV0ZWQ7XHJcbiAgICAgICAgICAgIHRoaXMudG90YWxDb21wbGV0ZS0tO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgZXZlbnQgPSBuZXcgRmlsZVVwbG9hZERlbGV0ZUV2ZW50KGZpbGUsIHRoaXMudG90YWxDb21wbGV0ZSk7XHJcbiAgICAgICAgICAgIHRoaXMuZmlsZVVwbG9hZC5uZXh0KGV2ZW50KTtcclxuICAgICAgICAgICAgdGhpcy5maWxlVXBsb2FkRGVsZXRlZC5uZXh0KGV2ZW50KTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBnZXRBY3Rpb24oZmlsZSkge1xyXG4gICAgICAgIGNvbnN0IGFjdGlvbnMgPSB7XHJcbiAgICAgICAgICAgIFtGaWxlVXBsb2FkU3RhdHVzLlBlbmRpbmddOiAoKSA9PiB0aGlzLm9uVXBsb2FkQ2FuY2VsbGVkKGZpbGUpLFxyXG4gICAgICAgICAgICBbRmlsZVVwbG9hZFN0YXR1cy5EZWxldGVkXTogKCkgPT4gdGhpcy5vblVwbG9hZERlbGV0ZWQoZmlsZSksXHJcbiAgICAgICAgICAgIFtGaWxlVXBsb2FkU3RhdHVzLkVycm9yXTogKCkgPT4gdGhpcy5vblVwbG9hZEVycm9yKGZpbGUsIG51bGwpXHJcbiAgICAgICAgfTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGFjdGlvbnNbZmlsZS5zdGF0dXNdO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==