/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { BehaviorSubject } from 'rxjs';
import { AppConfigService, AppConfigValues } from '../app-config/app-config.service';
import { StorageService } from './storage.service';
import { distinctUntilChanged, map } from 'rxjs/operators';
import { AlfrescoApiService } from './alfresco-api.service';
import * as i0 from "@angular/core";
import * as i1 from "@ngx-translate/core";
import * as i2 from "../app-config/app-config.service";
import * as i3 from "./storage.service";
import * as i4 from "./alfresco-api.service";
/** @enum {string} */
var UserPreferenceValues = {
    PaginationSize: 'paginationSize',
    Locale: 'locale',
    SupportedPageSizes: 'supportedPageSizes',
    ExpandedSideNavStatus: 'expandedSidenav',
};
export { UserPreferenceValues };
var UserPreferencesService = /** @class */ (function () {
    function UserPreferencesService(translate, appConfig, storage, alfrescoApiService) {
        this.translate = translate;
        this.appConfig = appConfig;
        this.storage = storage;
        this.alfrescoApiService = alfrescoApiService;
        this.defaults = {
            paginationSize: 25,
            supportedPageSizes: [5, 10, 15, 20],
            locale: 'en',
            expandedSidenav: true
        };
        this.userPreferenceStatus = this.defaults;
        this.alfrescoApiService.alfrescoApiInitialized.subscribe(this.initUserPreferenceStatus.bind(this));
        this.onChangeSubject = new BehaviorSubject(this.userPreferenceStatus);
        this.onChange = this.onChangeSubject.asObservable();
    }
    /**
     * @private
     * @return {?}
     */
    UserPreferencesService.prototype.initUserPreferenceStatus = /**
     * @private
     * @return {?}
     */
    function () {
        this.initUserLanguage();
        this.set(UserPreferenceValues.PaginationSize, this.paginationSize);
        this.set(UserPreferenceValues.SupportedPageSizes, JSON.stringify(this.supportedPageSizes));
    };
    /**
     * @private
     * @return {?}
     */
    UserPreferencesService.prototype.initUserLanguage = /**
     * @private
     * @return {?}
     */
    function () {
        if (this.locale || this.appConfig.get(UserPreferenceValues.Locale)) {
            /** @type {?} */
            var locale = this.locale || this.getDefaultLocale();
            this.set(UserPreferenceValues.Locale, locale);
            this.set('textOrientation', this.getLanguageByKey(locale).direction || 'ltr');
        }
        else {
            /** @type {?} */
            var locale = this.locale || this.getDefaultLocale();
            this.setWithoutStore(UserPreferenceValues.Locale, locale);
            this.setWithoutStore('textOrientation', this.getLanguageByKey(locale).direction || 'ltr');
        }
    };
    /**
     * Sets up a callback to notify when a property has changed.
     * @param property The property to watch
     * @returns Notification callback
     */
    /**
     * Sets up a callback to notify when a property has changed.
     * @param {?} property The property to watch
     * @return {?} Notification callback
     */
    UserPreferencesService.prototype.select = /**
     * Sets up a callback to notify when a property has changed.
     * @param {?} property The property to watch
     * @return {?} Notification callback
     */
    function (property) {
        return this.onChange
            .pipe(map((/**
         * @param {?} userPreferenceStatus
         * @return {?}
         */
        function (userPreferenceStatus) { return userPreferenceStatus[property]; })), distinctUntilChanged());
    };
    /**
     * Gets a preference property.
     * @param property Name of the property
     * @param defaultValue Default to return if the property is not found
     * @returns Preference property
     */
    /**
     * Gets a preference property.
     * @param {?} property Name of the property
     * @param {?=} defaultValue Default to return if the property is not found
     * @return {?} Preference property
     */
    UserPreferencesService.prototype.get = /**
     * Gets a preference property.
     * @param {?} property Name of the property
     * @param {?=} defaultValue Default to return if the property is not found
     * @return {?} Preference property
     */
    function (property, defaultValue) {
        /** @type {?} */
        var key = this.getPropertyKey(property);
        /** @type {?} */
        var value = this.storage.getItem(key);
        if (value === undefined || value === null) {
            return defaultValue;
        }
        return value;
    };
    /**
     * Sets a preference property.
     * @param property Name of the property
     * @param value New value for the property
     */
    /**
     * Sets a preference property.
     * @param {?} property Name of the property
     * @param {?} value New value for the property
     * @return {?}
     */
    UserPreferencesService.prototype.set = /**
     * Sets a preference property.
     * @param {?} property Name of the property
     * @param {?} value New value for the property
     * @return {?}
     */
    function (property, value) {
        if (!property) {
            return;
        }
        this.storage.setItem(this.getPropertyKey(property), value);
        this.userPreferenceStatus[property] = value;
        this.onChangeSubject.next(this.userPreferenceStatus);
    };
    /**
     * Sets a preference property.
     * @param property Name of the property
     * @param value New value for the property
     */
    /**
     * Sets a preference property.
     * @param {?} property Name of the property
     * @param {?} value New value for the property
     * @return {?}
     */
    UserPreferencesService.prototype.setWithoutStore = /**
     * Sets a preference property.
     * @param {?} property Name of the property
     * @param {?} value New value for the property
     * @return {?}
     */
    function (property, value) {
        if (!property) {
            return;
        }
        this.userPreferenceStatus[property] = value;
        this.onChangeSubject.next(this.userPreferenceStatus);
    };
    /**
     * Check if an item is present in the storage
     * @param property Name of the property
     * @returns True if the item is present, false otherwise
     */
    /**
     * Check if an item is present in the storage
     * @param {?} property Name of the property
     * @return {?} True if the item is present, false otherwise
     */
    UserPreferencesService.prototype.hasItem = /**
     * Check if an item is present in the storage
     * @param {?} property Name of the property
     * @return {?} True if the item is present, false otherwise
     */
    function (property) {
        if (!property) {
            return;
        }
        return this.storage.hasItem(this.getPropertyKey(property));
    };
    /**
     * Gets the active storage prefix for preferences.
     * @returns Storage prefix
     */
    /**
     * Gets the active storage prefix for preferences.
     * @return {?} Storage prefix
     */
    UserPreferencesService.prototype.getStoragePrefix = /**
     * Gets the active storage prefix for preferences.
     * @return {?} Storage prefix
     */
    function () {
        return this.storage.getItem('USER_PROFILE') || 'GUEST';
    };
    /**
     * Sets the active storage prefix for preferences.
     * @param value Name of the prefix
     */
    /**
     * Sets the active storage prefix for preferences.
     * @param {?} value Name of the prefix
     * @return {?}
     */
    UserPreferencesService.prototype.setStoragePrefix = /**
     * Sets the active storage prefix for preferences.
     * @param {?} value Name of the prefix
     * @return {?}
     */
    function (value) {
        this.storage.setItem('USER_PROFILE', value || 'GUEST');
        this.initUserPreferenceStatus();
    };
    /**
     * Gets the full property key with prefix.
     * @param property The property name
     * @returns Property key
     */
    /**
     * Gets the full property key with prefix.
     * @param {?} property The property name
     * @return {?} Property key
     */
    UserPreferencesService.prototype.getPropertyKey = /**
     * Gets the full property key with prefix.
     * @param {?} property The property name
     * @return {?} Property key
     */
    function (property) {
        return this.getStoragePrefix() + "__" + property;
    };
    Object.defineProperty(UserPreferencesService.prototype, "supportedPageSizes", {
        /**
         * Gets an array containing the available page sizes.
         * @returns Array of page size values
         */
        get: /**
         * Gets an array containing the available page sizes.
         * @return {?} Array of page size values
         */
        function () {
            /** @type {?} */
            var supportedPageSizes = this.get(UserPreferenceValues.SupportedPageSizes);
            if (supportedPageSizes) {
                return JSON.parse(supportedPageSizes);
            }
            else {
                return this.appConfig.get('pagination.supportedPageSizes', this.defaults.supportedPageSizes);
            }
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.set(UserPreferenceValues.SupportedPageSizes, JSON.stringify(value));
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserPreferencesService.prototype, "paginationSize", {
        get: /**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var paginationSize = this.get(UserPreferenceValues.PaginationSize);
            if (paginationSize) {
                return Number(paginationSize);
            }
            else {
                return Number(this.appConfig.get('pagination.size', this.defaults.paginationSize));
            }
        },
        /** Pagination size. */
        set: /**
         * Pagination size.
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.set(UserPreferenceValues.PaginationSize, value);
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(UserPreferencesService.prototype, "locale", {
        /** Current locale setting. */
        get: /**
         * Current locale setting.
         * @return {?}
         */
        function () {
            return this.get(UserPreferenceValues.Locale);
        },
        set: /**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            this.set(UserPreferenceValues.Locale, value);
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Gets the default locale.
     * @returns Default locale language code
     */
    /**
     * Gets the default locale.
     * @return {?} Default locale language code
     */
    UserPreferencesService.prototype.getDefaultLocale = /**
     * Gets the default locale.
     * @return {?} Default locale language code
     */
    function () {
        return this.appConfig.get(UserPreferenceValues.Locale) || this.translate.getBrowserCultureLang() || 'en';
    };
    /**
     * @private
     * @param {?} key
     * @return {?}
     */
    UserPreferencesService.prototype.getLanguageByKey = /**
     * @private
     * @param {?} key
     * @return {?}
     */
    function (key) {
        return (this.appConfig
            .get(AppConfigValues.APP_CONFIG_LANGUAGES_KEY, [(/** @type {?} */ ({ key: 'en' }))])
            .find((/**
         * @param {?} language
         * @return {?}
         */
        function (language) { return key.includes(language.key); })) || (/** @type {?} */ ({ key: 'en' })));
    };
    UserPreferencesService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    UserPreferencesService.ctorParameters = function () { return [
        { type: TranslateService },
        { type: AppConfigService },
        { type: StorageService },
        { type: AlfrescoApiService }
    ]; };
    /** @nocollapse */ UserPreferencesService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function UserPreferencesService_Factory() { return new UserPreferencesService(i0.ɵɵinject(i1.TranslateService), i0.ɵɵinject(i2.AppConfigService), i0.ɵɵinject(i3.StorageService), i0.ɵɵinject(i4.AlfrescoApiService)); }, token: UserPreferencesService, providedIn: "root" });
    return UserPreferencesService;
}());
export { UserPreferencesService };
if (false) {
    /** @type {?} */
    UserPreferencesService.prototype.defaults;
    /**
     * @type {?}
     * @private
     */
    UserPreferencesService.prototype.userPreferenceStatus;
    /**
     * @type {?}
     * @private
     */
    UserPreferencesService.prototype.onChangeSubject;
    /** @type {?} */
    UserPreferencesService.prototype.onChange;
    /** @type {?} */
    UserPreferencesService.prototype.translate;
    /**
     * @type {?}
     * @private
     */
    UserPreferencesService.prototype.appConfig;
    /**
     * @type {?}
     * @private
     */
    UserPreferencesService.prototype.storage;
    /**
     * @type {?}
     * @private
     */
    UserPreferencesService.prototype.alfrescoApiService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci1wcmVmZXJlbmNlcy5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic2VydmljZXMvdXNlci1wcmVmZXJlbmNlcy5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDM0MsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdkQsT0FBTyxFQUFjLGVBQWUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNuRCxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsZUFBZSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFFckYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ25ELE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxHQUFHLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUMzRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSx3QkFBd0IsQ0FBQzs7Ozs7Ozs7SUFHeEQsZ0JBQWlCLGdCQUFnQjtJQUNqQyxRQUFTLFFBQVE7SUFDakIsb0JBQXFCLG9CQUFvQjtJQUN6Qyx1QkFBd0IsaUJBQWlCOzs7QUFHN0M7SUFnQkksZ0NBQW1CLFNBQTJCLEVBQzFCLFNBQTJCLEVBQzNCLE9BQXVCLEVBQ3ZCLGtCQUFzQztRQUh2QyxjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQUMxQixjQUFTLEdBQVQsU0FBUyxDQUFrQjtRQUMzQixZQUFPLEdBQVAsT0FBTyxDQUFnQjtRQUN2Qix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBZDFELGFBQVEsR0FBRztZQUNQLGNBQWMsRUFBRSxFQUFFO1lBQ2xCLGtCQUFrQixFQUFFLENBQUMsQ0FBQyxFQUFFLEVBQUUsRUFBRSxFQUFFLEVBQUUsRUFBRSxDQUFDO1lBQ25DLE1BQU0sRUFBRSxJQUFJO1lBQ1osZUFBZSxFQUFFLElBQUk7U0FDeEIsQ0FBQztRQUVNLHlCQUFvQixHQUFRLElBQUksQ0FBQyxRQUFRLENBQUM7UUFROUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLHNCQUFzQixDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7UUFDbkcsSUFBSSxDQUFDLGVBQWUsR0FBRyxJQUFJLGVBQWUsQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztRQUN0RSxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxlQUFlLENBQUMsWUFBWSxFQUFFLENBQUM7SUFDeEQsQ0FBQzs7Ozs7SUFFTyx5REFBd0I7Ozs7SUFBaEM7UUFDSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztRQUN4QixJQUFJLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDbkUsSUFBSSxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxDQUFDLENBQUM7SUFDL0YsQ0FBQzs7Ozs7SUFFTyxpREFBZ0I7Ozs7SUFBeEI7UUFDSSxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQVMsb0JBQW9CLENBQUMsTUFBTSxDQUFDLEVBQUU7O2dCQUNsRSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7WUFFckQsSUFBSSxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLEVBQUUsTUFBTSxDQUFDLENBQUM7WUFDOUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsTUFBTSxDQUFDLENBQUMsU0FBUyxJQUFJLEtBQUssQ0FBQyxDQUFDO1NBQ2pGO2FBQU07O2dCQUNHLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsRUFBRTtZQUVyRCxJQUFJLENBQUMsZUFBZSxDQUFDLG9CQUFvQixDQUFDLE1BQU0sRUFBRSxNQUFNLENBQUMsQ0FBQztZQUMxRCxJQUFJLENBQUMsZUFBZSxDQUFDLGlCQUFpQixFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLElBQUksS0FBSyxDQUFDLENBQUM7U0FDN0Y7SUFDTCxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsdUNBQU07Ozs7O0lBQU4sVUFBTyxRQUFnQjtRQUNuQixPQUFPLElBQUksQ0FBQyxRQUFRO2FBQ2YsSUFBSSxDQUNELEdBQUc7Ozs7UUFBQyxVQUFDLG9CQUFvQixJQUFLLE9BQUEsb0JBQW9CLENBQUMsUUFBUSxDQUFDLEVBQTlCLENBQThCLEVBQUMsRUFDN0Qsb0JBQW9CLEVBQUUsQ0FDekIsQ0FBQztJQUNWLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILG9DQUFHOzs7Ozs7SUFBSCxVQUFJLFFBQWdCLEVBQUUsWUFBcUI7O1lBQ2pDLEdBQUcsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLFFBQVEsQ0FBQzs7WUFDbkMsS0FBSyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQztRQUN2QyxJQUFJLEtBQUssS0FBSyxTQUFTLElBQUksS0FBSyxLQUFLLElBQUksRUFBRTtZQUN2QyxPQUFPLFlBQVksQ0FBQztTQUN2QjtRQUNELE9BQU8sS0FBSyxDQUFDO0lBQ2pCLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsb0NBQUc7Ozs7OztJQUFILFVBQUksUUFBZ0IsRUFBRSxLQUFVO1FBQzVCLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDWCxPQUFPO1NBQ1Y7UUFDRCxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FDaEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsRUFDN0IsS0FBSyxDQUNSLENBQUM7UUFDRixJQUFJLENBQUMsb0JBQW9CLENBQUMsUUFBUSxDQUFDLEdBQUcsS0FBSyxDQUFDO1FBQzVDLElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxvQkFBb0IsQ0FBQyxDQUFDO0lBQ3pELENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsZ0RBQWU7Ozs7OztJQUFmLFVBQWdCLFFBQWdCLEVBQUUsS0FBVTtRQUN4QyxJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ1gsT0FBTztTQUNWO1FBQ0QsSUFBSSxDQUFDLG9CQUFvQixDQUFDLFFBQVEsQ0FBQyxHQUFHLEtBQUssQ0FBQztRQUM1QyxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsb0JBQW9CLENBQUMsQ0FBQztJQUN6RCxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsd0NBQU87Ozs7O0lBQVAsVUFBUSxRQUFnQjtRQUNwQixJQUFJLENBQUMsUUFBUSxFQUFFO1lBQ1gsT0FBTztTQUNWO1FBQ0QsT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FDdkIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxRQUFRLENBQUMsQ0FDaEMsQ0FBQztJQUNOLENBQUM7SUFFRDs7O09BR0c7Ozs7O0lBQ0gsaURBQWdCOzs7O0lBQWhCO1FBQ0ksT0FBTyxJQUFJLENBQUMsT0FBTyxDQUFDLE9BQU8sQ0FBQyxjQUFjLENBQUMsSUFBSSxPQUFPLENBQUM7SUFDM0QsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7O0lBQ0gsaURBQWdCOzs7OztJQUFoQixVQUFpQixLQUFhO1FBQzFCLElBQUksQ0FBQyxPQUFPLENBQUMsT0FBTyxDQUFDLGNBQWMsRUFBRSxLQUFLLElBQUksT0FBTyxDQUFDLENBQUM7UUFDdkQsSUFBSSxDQUFDLHdCQUF3QixFQUFFLENBQUM7SUFDcEMsQ0FBQztJQUVEOzs7O09BSUc7Ozs7OztJQUNILCtDQUFjOzs7OztJQUFkLFVBQWUsUUFBZ0I7UUFDM0IsT0FBVSxJQUFJLENBQUMsZ0JBQWdCLEVBQUUsVUFBSyxRQUFVLENBQUM7SUFDckQsQ0FBQztJQU1ELHNCQUFJLHNEQUFrQjtRQUp0Qjs7O1dBR0c7Ozs7O1FBQ0g7O2dCQUNVLGtCQUFrQixHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsa0JBQWtCLENBQUM7WUFFNUUsSUFBSSxrQkFBa0IsRUFBRTtnQkFDcEIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLGtCQUFrQixDQUFDLENBQUM7YUFDekM7aUJBQU07Z0JBQ0gsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQywrQkFBK0IsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLGtCQUFrQixDQUFDLENBQUM7YUFDaEc7UUFDTCxDQUFDOzs7OztRQUVELFVBQXVCLEtBQWU7WUFDbEMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxrQkFBa0IsRUFBRSxJQUFJLENBQUMsU0FBUyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUM7UUFDN0UsQ0FBQzs7O09BSkE7SUFPRCxzQkFBSSxrREFBYzs7OztRQUlsQjs7Z0JBQ1UsY0FBYyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsb0JBQW9CLENBQUMsY0FBYyxDQUFDO1lBRXBFLElBQUksY0FBYyxFQUFFO2dCQUNoQixPQUFPLE1BQU0sQ0FBQyxjQUFjLENBQUMsQ0FBQzthQUNqQztpQkFBTTtnQkFDSCxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBQyxpQkFBaUIsRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLGNBQWMsQ0FBQyxDQUFDLENBQUM7YUFDdEY7UUFDTCxDQUFDO1FBYkQsdUJBQXVCOzs7Ozs7UUFDdkIsVUFBbUIsS0FBYTtZQUM1QixJQUFJLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLGNBQWMsRUFBRSxLQUFLLENBQUMsQ0FBQztRQUN6RCxDQUFDOzs7T0FBQTtJQWFELHNCQUFJLDBDQUFNO1FBRFYsOEJBQThCOzs7OztRQUM5QjtZQUNJLE9BQU8sSUFBSSxDQUFDLEdBQUcsQ0FBQyxvQkFBb0IsQ0FBQyxNQUFNLENBQUMsQ0FBQztRQUNqRCxDQUFDOzs7OztRQUVELFVBQVcsS0FBYTtZQUNwQixJQUFJLENBQUMsR0FBRyxDQUFDLG9CQUFvQixDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsQ0FBQztRQUNqRCxDQUFDOzs7T0FKQTtJQU1EOzs7T0FHRzs7Ozs7SUFDSSxpREFBZ0I7Ozs7SUFBdkI7UUFDSSxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFTLG9CQUFvQixDQUFDLE1BQU0sQ0FBQyxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMscUJBQXFCLEVBQUUsSUFBSSxJQUFJLENBQUM7SUFDckgsQ0FBQzs7Ozs7O0lBRU8saURBQWdCOzs7OztJQUF4QixVQUF5QixHQUFXO1FBQ2hDLE9BQU8sQ0FDSCxJQUFJLENBQUMsU0FBUzthQUNULEdBQUcsQ0FBc0IsZUFBZSxDQUFDLHdCQUF3QixFQUFFLENBQUMsbUJBQWUsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUEsQ0FBQyxDQUFDO2FBQ2xHLElBQUk7Ozs7UUFBQyxVQUFDLFFBQVEsSUFBSyxPQUFBLEdBQUcsQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUExQixDQUEwQixFQUFDLElBQUksbUJBQWUsRUFBRSxHQUFHLEVBQUUsSUFBSSxFQUFFLEVBQUEsQ0FDdEYsQ0FBQztJQUNOLENBQUM7O2dCQXZNSixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7O2dCQWpCUSxnQkFBZ0I7Z0JBRWhCLGdCQUFnQjtnQkFFaEIsY0FBYztnQkFFZCxrQkFBa0I7OztpQ0F4QjNCO0NBeU9DLEFBeE1ELElBd01DO1NBck1ZLHNCQUFzQjs7O0lBRS9CLDBDQUtFOzs7OztJQUVGLHNEQUFrRDs7Ozs7SUFDbEQsaURBQThDOztJQUM5QywwQ0FBMEI7O0lBRWQsMkNBQWtDOzs7OztJQUNsQywyQ0FBbUM7Ozs7O0lBQ25DLHlDQUErQjs7Ozs7SUFDL0Isb0RBQThDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgVHJhbnNsYXRlU2VydmljZSB9IGZyb20gJ0BuZ3gtdHJhbnNsYXRlL2NvcmUnO1xyXG5pbXBvcnQgeyBPYnNlcnZhYmxlLCBCZWhhdmlvclN1YmplY3QgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQXBwQ29uZmlnU2VydmljZSwgQXBwQ29uZmlnVmFsdWVzIH0gZnJvbSAnLi4vYXBwLWNvbmZpZy9hcHAtY29uZmlnLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMYW5ndWFnZUl0ZW0gfSBmcm9tICcuLi9sYW5ndWFnZS1tZW51L2xhbmd1YWdlLmludGVyZmFjZSc7XHJcbmltcG9ydCB7IFN0b3JhZ2VTZXJ2aWNlIH0gZnJvbSAnLi9zdG9yYWdlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBkaXN0aW5jdFVudGlsQ2hhbmdlZCwgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuXHJcbmV4cG9ydCBlbnVtIFVzZXJQcmVmZXJlbmNlVmFsdWVzIHtcclxuICAgIFBhZ2luYXRpb25TaXplID0gJ3BhZ2luYXRpb25TaXplJyxcclxuICAgIExvY2FsZSA9ICdsb2NhbGUnLFxyXG4gICAgU3VwcG9ydGVkUGFnZVNpemVzID0gJ3N1cHBvcnRlZFBhZ2VTaXplcycsXHJcbiAgICBFeHBhbmRlZFNpZGVOYXZTdGF0dXMgPSAnZXhwYW5kZWRTaWRlbmF2J1xyXG59XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIFVzZXJQcmVmZXJlbmNlc1NlcnZpY2Uge1xyXG5cclxuICAgIGRlZmF1bHRzID0ge1xyXG4gICAgICAgIHBhZ2luYXRpb25TaXplOiAyNSxcclxuICAgICAgICBzdXBwb3J0ZWRQYWdlU2l6ZXM6IFs1LCAxMCwgMTUsIDIwXSxcclxuICAgICAgICBsb2NhbGU6ICdlbicsXHJcbiAgICAgICAgZXhwYW5kZWRTaWRlbmF2OiB0cnVlXHJcbiAgICB9O1xyXG5cclxuICAgIHByaXZhdGUgdXNlclByZWZlcmVuY2VTdGF0dXM6IGFueSA9IHRoaXMuZGVmYXVsdHM7XHJcbiAgICBwcml2YXRlIG9uQ2hhbmdlU3ViamVjdDogQmVoYXZpb3JTdWJqZWN0PGFueT47XHJcbiAgICBvbkNoYW5nZTogT2JzZXJ2YWJsZTxhbnk+O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHB1YmxpYyB0cmFuc2xhdGU6IFRyYW5zbGF0ZVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGFwcENvbmZpZzogQXBwQ29uZmlnU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgc3RvcmFnZTogU3RvcmFnZVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGFsZnJlc2NvQXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlKSB7XHJcbiAgICAgICAgdGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuYWxmcmVzY29BcGlJbml0aWFsaXplZC5zdWJzY3JpYmUodGhpcy5pbml0VXNlclByZWZlcmVuY2VTdGF0dXMuYmluZCh0aGlzKSk7XHJcbiAgICAgICAgdGhpcy5vbkNoYW5nZVN1YmplY3QgPSBuZXcgQmVoYXZpb3JTdWJqZWN0KHRoaXMudXNlclByZWZlcmVuY2VTdGF0dXMpO1xyXG4gICAgICAgIHRoaXMub25DaGFuZ2UgPSB0aGlzLm9uQ2hhbmdlU3ViamVjdC5hc09ic2VydmFibGUoKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGluaXRVc2VyUHJlZmVyZW5jZVN0YXR1cygpIHtcclxuICAgICAgICB0aGlzLmluaXRVc2VyTGFuZ3VhZ2UoKTtcclxuICAgICAgICB0aGlzLnNldChVc2VyUHJlZmVyZW5jZVZhbHVlcy5QYWdpbmF0aW9uU2l6ZSwgdGhpcy5wYWdpbmF0aW9uU2l6ZSk7XHJcbiAgICAgICAgdGhpcy5zZXQoVXNlclByZWZlcmVuY2VWYWx1ZXMuU3VwcG9ydGVkUGFnZVNpemVzLCBKU09OLnN0cmluZ2lmeSh0aGlzLnN1cHBvcnRlZFBhZ2VTaXplcykpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgaW5pdFVzZXJMYW5ndWFnZSgpIHtcclxuICAgICAgICBpZiAodGhpcy5sb2NhbGUgfHwgdGhpcy5hcHBDb25maWcuZ2V0PHN0cmluZz4oVXNlclByZWZlcmVuY2VWYWx1ZXMuTG9jYWxlKSkge1xyXG4gICAgICAgICAgICBjb25zdCBsb2NhbGUgPSB0aGlzLmxvY2FsZSB8fCB0aGlzLmdldERlZmF1bHRMb2NhbGUoKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuc2V0KFVzZXJQcmVmZXJlbmNlVmFsdWVzLkxvY2FsZSwgbG9jYWxlKTtcclxuICAgICAgICAgICAgdGhpcy5zZXQoJ3RleHRPcmllbnRhdGlvbicsIHRoaXMuZ2V0TGFuZ3VhZ2VCeUtleShsb2NhbGUpLmRpcmVjdGlvbiB8fCAnbHRyJyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgY29uc3QgbG9jYWxlID0gdGhpcy5sb2NhbGUgfHwgdGhpcy5nZXREZWZhdWx0TG9jYWxlKCk7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnNldFdpdGhvdXRTdG9yZShVc2VyUHJlZmVyZW5jZVZhbHVlcy5Mb2NhbGUsIGxvY2FsZSk7XHJcbiAgICAgICAgICAgIHRoaXMuc2V0V2l0aG91dFN0b3JlKCd0ZXh0T3JpZW50YXRpb24nLCB0aGlzLmdldExhbmd1YWdlQnlLZXkobG9jYWxlKS5kaXJlY3Rpb24gfHwgJ2x0cicpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldHMgdXAgYSBjYWxsYmFjayB0byBub3RpZnkgd2hlbiBhIHByb3BlcnR5IGhhcyBjaGFuZ2VkLlxyXG4gICAgICogQHBhcmFtIHByb3BlcnR5IFRoZSBwcm9wZXJ0eSB0byB3YXRjaFxyXG4gICAgICogQHJldHVybnMgTm90aWZpY2F0aW9uIGNhbGxiYWNrXHJcbiAgICAgKi9cclxuICAgIHNlbGVjdChwcm9wZXJ0eTogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5vbkNoYW5nZVxyXG4gICAgICAgICAgICAucGlwZShcclxuICAgICAgICAgICAgICAgIG1hcCgodXNlclByZWZlcmVuY2VTdGF0dXMpID0+IHVzZXJQcmVmZXJlbmNlU3RhdHVzW3Byb3BlcnR5XSksXHJcbiAgICAgICAgICAgICAgICBkaXN0aW5jdFVudGlsQ2hhbmdlZCgpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgcHJlZmVyZW5jZSBwcm9wZXJ0eS5cclxuICAgICAqIEBwYXJhbSBwcm9wZXJ0eSBOYW1lIG9mIHRoZSBwcm9wZXJ0eVxyXG4gICAgICogQHBhcmFtIGRlZmF1bHRWYWx1ZSBEZWZhdWx0IHRvIHJldHVybiBpZiB0aGUgcHJvcGVydHkgaXMgbm90IGZvdW5kXHJcbiAgICAgKiBAcmV0dXJucyBQcmVmZXJlbmNlIHByb3BlcnR5XHJcbiAgICAgKi9cclxuICAgIGdldChwcm9wZXJ0eTogc3RyaW5nLCBkZWZhdWx0VmFsdWU/OiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIGNvbnN0IGtleSA9IHRoaXMuZ2V0UHJvcGVydHlLZXkocHJvcGVydHkpO1xyXG4gICAgICAgIGNvbnN0IHZhbHVlID0gdGhpcy5zdG9yYWdlLmdldEl0ZW0oa2V5KTtcclxuICAgICAgICBpZiAodmFsdWUgPT09IHVuZGVmaW5lZCB8fCB2YWx1ZSA9PT0gbnVsbCkge1xyXG4gICAgICAgICAgICByZXR1cm4gZGVmYXVsdFZhbHVlO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdmFsdWU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBTZXRzIGEgcHJlZmVyZW5jZSBwcm9wZXJ0eS5cclxuICAgICAqIEBwYXJhbSBwcm9wZXJ0eSBOYW1lIG9mIHRoZSBwcm9wZXJ0eVxyXG4gICAgICogQHBhcmFtIHZhbHVlIE5ldyB2YWx1ZSBmb3IgdGhlIHByb3BlcnR5XHJcbiAgICAgKi9cclxuICAgIHNldChwcm9wZXJ0eTogc3RyaW5nLCB2YWx1ZTogYW55KSB7XHJcbiAgICAgICAgaWYgKCFwcm9wZXJ0eSkge1xyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHRoaXMuc3RvcmFnZS5zZXRJdGVtKFxyXG4gICAgICAgICAgICB0aGlzLmdldFByb3BlcnR5S2V5KHByb3BlcnR5KSxcclxuICAgICAgICAgICAgdmFsdWVcclxuICAgICAgICApO1xyXG4gICAgICAgIHRoaXMudXNlclByZWZlcmVuY2VTdGF0dXNbcHJvcGVydHldID0gdmFsdWU7XHJcbiAgICAgICAgdGhpcy5vbkNoYW5nZVN1YmplY3QubmV4dCh0aGlzLnVzZXJQcmVmZXJlbmNlU3RhdHVzKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFNldHMgYSBwcmVmZXJlbmNlIHByb3BlcnR5LlxyXG4gICAgICogQHBhcmFtIHByb3BlcnR5IE5hbWUgb2YgdGhlIHByb3BlcnR5XHJcbiAgICAgKiBAcGFyYW0gdmFsdWUgTmV3IHZhbHVlIGZvciB0aGUgcHJvcGVydHlcclxuICAgICAqL1xyXG4gICAgc2V0V2l0aG91dFN0b3JlKHByb3BlcnR5OiBzdHJpbmcsIHZhbHVlOiBhbnkpIHtcclxuICAgICAgICBpZiAoIXByb3BlcnR5KSB7XHJcbiAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICAgICAgdGhpcy51c2VyUHJlZmVyZW5jZVN0YXR1c1twcm9wZXJ0eV0gPSB2YWx1ZTtcclxuICAgICAgICB0aGlzLm9uQ2hhbmdlU3ViamVjdC5uZXh0KHRoaXMudXNlclByZWZlcmVuY2VTdGF0dXMpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2sgaWYgYW4gaXRlbSBpcyBwcmVzZW50IGluIHRoZSBzdG9yYWdlXHJcbiAgICAgKiBAcGFyYW0gcHJvcGVydHkgTmFtZSBvZiB0aGUgcHJvcGVydHlcclxuICAgICAqIEByZXR1cm5zIFRydWUgaWYgdGhlIGl0ZW0gaXMgcHJlc2VudCwgZmFsc2Ugb3RoZXJ3aXNlXHJcbiAgICAgKi9cclxuICAgIGhhc0l0ZW0ocHJvcGVydHk6IHN0cmluZykge1xyXG4gICAgICAgIGlmICghcHJvcGVydHkpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gdGhpcy5zdG9yYWdlLmhhc0l0ZW0oXHJcbiAgICAgICAgICAgIHRoaXMuZ2V0UHJvcGVydHlLZXkocHJvcGVydHkpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIGFjdGl2ZSBzdG9yYWdlIHByZWZpeCBmb3IgcHJlZmVyZW5jZXMuXHJcbiAgICAgKiBAcmV0dXJucyBTdG9yYWdlIHByZWZpeFxyXG4gICAgICovXHJcbiAgICBnZXRTdG9yYWdlUHJlZml4KCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuc3RvcmFnZS5nZXRJdGVtKCdVU0VSX1BST0ZJTEUnKSB8fCAnR1VFU1QnO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogU2V0cyB0aGUgYWN0aXZlIHN0b3JhZ2UgcHJlZml4IGZvciBwcmVmZXJlbmNlcy5cclxuICAgICAqIEBwYXJhbSB2YWx1ZSBOYW1lIG9mIHRoZSBwcmVmaXhcclxuICAgICAqL1xyXG4gICAgc2V0U3RvcmFnZVByZWZpeCh2YWx1ZTogc3RyaW5nKSB7XHJcbiAgICAgICAgdGhpcy5zdG9yYWdlLnNldEl0ZW0oJ1VTRVJfUFJPRklMRScsIHZhbHVlIHx8ICdHVUVTVCcpO1xyXG4gICAgICAgIHRoaXMuaW5pdFVzZXJQcmVmZXJlbmNlU3RhdHVzKCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHRoZSBmdWxsIHByb3BlcnR5IGtleSB3aXRoIHByZWZpeC5cclxuICAgICAqIEBwYXJhbSBwcm9wZXJ0eSBUaGUgcHJvcGVydHkgbmFtZVxyXG4gICAgICogQHJldHVybnMgUHJvcGVydHkga2V5XHJcbiAgICAgKi9cclxuICAgIGdldFByb3BlcnR5S2V5KHByb3BlcnR5OiBzdHJpbmcpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiBgJHt0aGlzLmdldFN0b3JhZ2VQcmVmaXgoKX1fXyR7cHJvcGVydHl9YDtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYW4gYXJyYXkgY29udGFpbmluZyB0aGUgYXZhaWxhYmxlIHBhZ2Ugc2l6ZXMuXHJcbiAgICAgKiBAcmV0dXJucyBBcnJheSBvZiBwYWdlIHNpemUgdmFsdWVzXHJcbiAgICAgKi9cclxuICAgIGdldCBzdXBwb3J0ZWRQYWdlU2l6ZXMoKTogbnVtYmVyW10ge1xyXG4gICAgICAgIGNvbnN0IHN1cHBvcnRlZFBhZ2VTaXplcyA9IHRoaXMuZ2V0KFVzZXJQcmVmZXJlbmNlVmFsdWVzLlN1cHBvcnRlZFBhZ2VTaXplcyk7XHJcblxyXG4gICAgICAgIGlmIChzdXBwb3J0ZWRQYWdlU2l6ZXMpIHtcclxuICAgICAgICAgICAgcmV0dXJuIEpTT04ucGFyc2Uoc3VwcG9ydGVkUGFnZVNpemVzKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5hcHBDb25maWcuZ2V0KCdwYWdpbmF0aW9uLnN1cHBvcnRlZFBhZ2VTaXplcycsIHRoaXMuZGVmYXVsdHMuc3VwcG9ydGVkUGFnZVNpemVzKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgc2V0IHN1cHBvcnRlZFBhZ2VTaXplcyh2YWx1ZTogbnVtYmVyW10pIHtcclxuICAgICAgICB0aGlzLnNldChVc2VyUHJlZmVyZW5jZVZhbHVlcy5TdXBwb3J0ZWRQYWdlU2l6ZXMsIEpTT04uc3RyaW5naWZ5KHZhbHVlKSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqIFBhZ2luYXRpb24gc2l6ZS4gKi9cclxuICAgIHNldCBwYWdpbmF0aW9uU2l6ZSh2YWx1ZTogbnVtYmVyKSB7XHJcbiAgICAgICAgdGhpcy5zZXQoVXNlclByZWZlcmVuY2VWYWx1ZXMuUGFnaW5hdGlvblNpemUsIHZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgcGFnaW5hdGlvblNpemUoKTogbnVtYmVyIHtcclxuICAgICAgICBjb25zdCBwYWdpbmF0aW9uU2l6ZSA9IHRoaXMuZ2V0KFVzZXJQcmVmZXJlbmNlVmFsdWVzLlBhZ2luYXRpb25TaXplKTtcclxuXHJcbiAgICAgICAgaWYgKHBhZ2luYXRpb25TaXplKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBOdW1iZXIocGFnaW5hdGlvblNpemUpO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBOdW1iZXIodGhpcy5hcHBDb25maWcuZ2V0KCdwYWdpbmF0aW9uLnNpemUnLCB0aGlzLmRlZmF1bHRzLnBhZ2luYXRpb25TaXplKSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKiBDdXJyZW50IGxvY2FsZSBzZXR0aW5nLiAqL1xyXG4gICAgZ2V0IGxvY2FsZSgpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldChVc2VyUHJlZmVyZW5jZVZhbHVlcy5Mb2NhbGUpO1xyXG4gICAgfVxyXG5cclxuICAgIHNldCBsb2NhbGUodmFsdWU6IHN0cmluZykge1xyXG4gICAgICAgIHRoaXMuc2V0KFVzZXJQcmVmZXJlbmNlVmFsdWVzLkxvY2FsZSwgdmFsdWUpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgZGVmYXVsdCBsb2NhbGUuXHJcbiAgICAgKiBAcmV0dXJucyBEZWZhdWx0IGxvY2FsZSBsYW5ndWFnZSBjb2RlXHJcbiAgICAgKi9cclxuICAgIHB1YmxpYyBnZXREZWZhdWx0TG9jYWxlKCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuYXBwQ29uZmlnLmdldDxzdHJpbmc+KFVzZXJQcmVmZXJlbmNlVmFsdWVzLkxvY2FsZSkgfHwgdGhpcy50cmFuc2xhdGUuZ2V0QnJvd3NlckN1bHR1cmVMYW5nKCkgfHwgJ2VuJztcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdldExhbmd1YWdlQnlLZXkoa2V5OiBzdHJpbmcpOiBMYW5ndWFnZUl0ZW0ge1xyXG4gICAgICAgIHJldHVybiAoXHJcbiAgICAgICAgICAgIHRoaXMuYXBwQ29uZmlnXHJcbiAgICAgICAgICAgICAgICAuZ2V0PEFycmF5PExhbmd1YWdlSXRlbT4+KEFwcENvbmZpZ1ZhbHVlcy5BUFBfQ09ORklHX0xBTkdVQUdFU19LRVksIFs8TGFuZ3VhZ2VJdGVtPiB7IGtleTogJ2VuJyB9XSlcclxuICAgICAgICAgICAgICAgIC5maW5kKChsYW5ndWFnZSkgPT4ga2V5LmluY2x1ZGVzKGxhbmd1YWdlLmtleSkpIHx8IDxMYW5ndWFnZUl0ZW0+IHsga2V5OiAnZW4nIH1cclxuICAgICAgICApO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==