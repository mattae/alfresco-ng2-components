/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, EventEmitter, Output, ViewEncapsulation, Input } from '@angular/core';
import { Validators, FormBuilder, FormControl } from '@angular/forms';
import { AppConfigService, AppConfigValues } from '../app-config/app-config.service';
import { StorageService } from '../services/storage.service';
import { AlfrescoApiService } from '../services/alfresco-api.service';
import { ENTER } from '@angular/cdk/keycodes';
var HostSettingsComponent = /** @class */ (function () {
    function HostSettingsComponent(formBuilder, storageService, alfrescoApiService, appConfig) {
        this.formBuilder = formBuilder;
        this.storageService = storageService;
        this.alfrescoApiService = alfrescoApiService;
        this.appConfig = appConfig;
        this.HOST_REGEX = '^(http|https):\/\/.*[^/]$';
        /**
         * Tells the component which provider options are available. Possible valid values
         * are "ECM" (Content), "BPM" (Process) , "ALL" (Content and Process), 'OAUTH2' SSO.
         */
        this.providers = ['BPM', 'ECM', 'ALL'];
        this.showSelectProviders = true;
        /**
         * Emitted when the URL is invalid.
         */
        this.error = new EventEmitter();
        /**
         * Emitted when the user cancels the changes.
         */
        this.cancel = new EventEmitter();
        /**
         * Emitted when the changes are successfully applied.
         */
        this.success = new EventEmitter();
    }
    /**
     * @return {?}
     */
    HostSettingsComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        if (this.providers.length === 1) {
            this.showSelectProviders = false;
        }
        /** @type {?} */
        var providerSelected = this.appConfig.get(AppConfigValues.PROVIDERS);
        /** @type {?} */
        var authType = this.appConfig.get(AppConfigValues.AUTHTYPE, 'BASIC');
        this.form = this.formBuilder.group({
            providersControl: [providerSelected, Validators.required],
            authType: authType
        });
        this.addFormGroups();
        if (authType === 'OAUTH') {
            this.addOAuthFormGroup();
            this.addIdentityHostFormControl();
        }
        this.form.get('authType').valueChanges.subscribe((/**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            if (value === 'BASIC') {
                _this.form.removeControl('oauthConfig');
                _this.form.removeControl('identityHost');
            }
            else {
                _this.addOAuthFormGroup();
                _this.addIdentityHostFormControl();
            }
        }));
        this.providersControl.valueChanges.subscribe((/**
         * @return {?}
         */
        function () {
            _this.removeFormGroups();
            _this.addFormGroups();
        }));
    };
    /**
     * @private
     * @return {?}
     */
    HostSettingsComponent.prototype.removeFormGroups = /**
     * @private
     * @return {?}
     */
    function () {
        this.form.removeControl('bpmHost');
        this.form.removeControl('ecmHost');
    };
    /**
     * @private
     * @return {?}
     */
    HostSettingsComponent.prototype.addFormGroups = /**
     * @private
     * @return {?}
     */
    function () {
        this.addBPMFormControl();
        this.addECMFormControl();
    };
    /**
     * @private
     * @return {?}
     */
    HostSettingsComponent.prototype.addOAuthFormGroup = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var oauthFormGroup = this.createOAuthFormGroup();
        this.form.addControl('oauthConfig', oauthFormGroup);
    };
    /**
     * @private
     * @return {?}
     */
    HostSettingsComponent.prototype.addBPMFormControl = /**
     * @private
     * @return {?}
     */
    function () {
        if ((this.isBPM() || this.isALL() || this.isOAUTH()) && !this.bpmHost) {
            /** @type {?} */
            var bpmFormControl = this.createBPMFormControl();
            this.form.addControl('bpmHost', bpmFormControl);
        }
    };
    /**
     * @private
     * @return {?}
     */
    HostSettingsComponent.prototype.addIdentityHostFormControl = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var identityHostFormControl = this.createIdentityFormControl();
        this.form.addControl('identityHost', identityHostFormControl);
    };
    /**
     * @private
     * @return {?}
     */
    HostSettingsComponent.prototype.addECMFormControl = /**
     * @private
     * @return {?}
     */
    function () {
        if ((this.isECM() || this.isALL()) && !this.ecmHost) {
            /** @type {?} */
            var ecmFormControl = this.createECMFormControl();
            this.form.addControl('ecmHost', ecmFormControl);
        }
    };
    /**
     * @private
     * @return {?}
     */
    HostSettingsComponent.prototype.createOAuthFormGroup = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var oauth = (/** @type {?} */ (this.appConfig.get(AppConfigValues.OAUTHCONFIG, {})));
        return this.formBuilder.group({
            host: [oauth.host, [Validators.required, Validators.pattern(this.HOST_REGEX)]],
            clientId: [oauth.clientId, Validators.required],
            redirectUri: [oauth.redirectUri, Validators.required],
            redirectUriLogout: [oauth.redirectUriLogout],
            scope: [oauth.scope, Validators.required],
            secret: oauth.secret,
            silentLogin: oauth.silentLogin,
            implicitFlow: oauth.implicitFlow
        });
    };
    /**
     * @private
     * @return {?}
     */
    HostSettingsComponent.prototype.createBPMFormControl = /**
     * @private
     * @return {?}
     */
    function () {
        return new FormControl(this.appConfig.get(AppConfigValues.BPMHOST), [Validators.required, Validators.pattern(this.HOST_REGEX)]);
    };
    /**
     * @private
     * @return {?}
     */
    HostSettingsComponent.prototype.createIdentityFormControl = /**
     * @private
     * @return {?}
     */
    function () {
        return new FormControl(this.appConfig.get(AppConfigValues.IDENTITY_HOST), [Validators.required, Validators.pattern(this.HOST_REGEX)]);
    };
    /**
     * @private
     * @return {?}
     */
    HostSettingsComponent.prototype.createECMFormControl = /**
     * @private
     * @return {?}
     */
    function () {
        return new FormControl(this.appConfig.get(AppConfigValues.ECMHOST), [Validators.required, Validators.pattern(this.HOST_REGEX)]);
    };
    /**
     * @return {?}
     */
    HostSettingsComponent.prototype.onCancel = /**
     * @return {?}
     */
    function () {
        this.cancel.emit(true);
    };
    /**
     * @param {?} values
     * @return {?}
     */
    HostSettingsComponent.prototype.onSubmit = /**
     * @param {?} values
     * @return {?}
     */
    function (values) {
        this.storageService.setItem(AppConfigValues.PROVIDERS, values.providersControl);
        if (this.isBPM()) {
            this.saveBPMValues(values);
        }
        else if (this.isECM()) {
            this.saveECMValues(values);
        }
        else if (this.isALL()) {
            this.saveECMValues(values);
            this.saveBPMValues(values);
        }
        if (this.isOAUTH()) {
            this.saveOAuthValues(values);
        }
        this.storageService.setItem(AppConfigValues.AUTHTYPE, values.authType);
        this.alfrescoApiService.reset();
        this.alfrescoApiService.getInstance().invalidateSession();
        this.success.emit(true);
    };
    /**
     * @param {?} event
     * @return {?}
     */
    HostSettingsComponent.prototype.keyDownFunction = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (event.keyCode === ENTER && this.form.valid) {
            this.onSubmit(this.form.value);
        }
    };
    /**
     * @private
     * @param {?} values
     * @return {?}
     */
    HostSettingsComponent.prototype.saveOAuthValues = /**
     * @private
     * @param {?} values
     * @return {?}
     */
    function (values) {
        this.storageService.setItem(AppConfigValues.OAUTHCONFIG, JSON.stringify(values.oauthConfig));
        this.storageService.setItem(AppConfigValues.IDENTITY_HOST, values.identityHost);
    };
    /**
     * @private
     * @param {?} values
     * @return {?}
     */
    HostSettingsComponent.prototype.saveBPMValues = /**
     * @private
     * @param {?} values
     * @return {?}
     */
    function (values) {
        this.storageService.setItem(AppConfigValues.BPMHOST, values.bpmHost);
    };
    /**
     * @private
     * @param {?} values
     * @return {?}
     */
    HostSettingsComponent.prototype.saveECMValues = /**
     * @private
     * @param {?} values
     * @return {?}
     */
    function (values) {
        this.storageService.setItem(AppConfigValues.ECMHOST, values.ecmHost);
    };
    /**
     * @return {?}
     */
    HostSettingsComponent.prototype.isBPM = /**
     * @return {?}
     */
    function () {
        return this.providersControl.value === 'BPM';
    };
    /**
     * @return {?}
     */
    HostSettingsComponent.prototype.isECM = /**
     * @return {?}
     */
    function () {
        return this.providersControl.value === 'ECM';
    };
    /**
     * @return {?}
     */
    HostSettingsComponent.prototype.isALL = /**
     * @return {?}
     */
    function () {
        return this.providersControl.value === 'ALL';
    };
    /**
     * @return {?}
     */
    HostSettingsComponent.prototype.isOAUTH = /**
     * @return {?}
     */
    function () {
        return this.form.get('authType').value === 'OAUTH';
    };
    Object.defineProperty(HostSettingsComponent.prototype, "providersControl", {
        get: /**
         * @return {?}
         */
        function () {
            return this.form.get('providersControl');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostSettingsComponent.prototype, "bpmHost", {
        get: /**
         * @return {?}
         */
        function () {
            return this.form.get('bpmHost');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostSettingsComponent.prototype, "ecmHost", {
        get: /**
         * @return {?}
         */
        function () {
            return this.form.get('ecmHost');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostSettingsComponent.prototype, "host", {
        get: /**
         * @return {?}
         */
        function () {
            return this.oauthConfig.get('host');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostSettingsComponent.prototype, "identityHost", {
        get: /**
         * @return {?}
         */
        function () {
            return this.form.get('identityHost');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostSettingsComponent.prototype, "clientId", {
        get: /**
         * @return {?}
         */
        function () {
            return this.oauthConfig.get('clientId');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostSettingsComponent.prototype, "scope", {
        get: /**
         * @return {?}
         */
        function () {
            return this.oauthConfig.get('scope');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostSettingsComponent.prototype, "secretId", {
        get: /**
         * @return {?}
         */
        function () {
            return this.oauthConfig.get('secretId');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostSettingsComponent.prototype, "implicitFlow", {
        get: /**
         * @return {?}
         */
        function () {
            return this.oauthConfig.get('implicitFlow');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostSettingsComponent.prototype, "silentLogin", {
        get: /**
         * @return {?}
         */
        function () {
            return this.oauthConfig.get('silentLogin');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostSettingsComponent.prototype, "redirectUri", {
        get: /**
         * @return {?}
         */
        function () {
            return this.oauthConfig.get('redirectUri');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostSettingsComponent.prototype, "redirectUriLogout", {
        get: /**
         * @return {?}
         */
        function () {
            return this.oauthConfig.get('redirectUriLogout');
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(HostSettingsComponent.prototype, "oauthConfig", {
        get: /**
         * @return {?}
         */
        function () {
            return this.form.get('oauthConfig');
        },
        enumerable: true,
        configurable: true
    });
    HostSettingsComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-host-settings',
                    template: "<div class=\"adf-setting-container\">\r\n    <mat-toolbar color=\"primary\" class=\"adf-setting-toolbar\">\r\n        <h3>{{'CORE.HOST_SETTINGS.TITLE' | translate}}</h3>\r\n    </mat-toolbar>\r\n    <mat-card class=\"adf-setting-card\">\r\n        <form id=\"host-form\" [formGroup]=\"form\" (submit)=\"onSubmit(form.value)\" (keydown)=\"keyDownFunction($event)\">\r\n\r\n            <mat-form-field floatLabel=\"{{'CORE.HOST_SETTINGS.PROVIDER' | translate }}\" *ngIf=\"showSelectProviders\">\r\n                <mat-select  id=\"adf-provider-selector\" placeholder=\"Provider\" [formControl]=\"providersControl\">\r\n                    <mat-option *ngFor=\"let provider of providers\" [value]=\"provider\">\r\n                        {{ provider }}\r\n                    </mat-option>\r\n                </mat-select>\r\n            </mat-form-field>\r\n\r\n            <div class=\"adf-authentication-type\">\r\n                <div> {{'CORE.HOST_SETTINGS.TYPE-AUTH' | translate }} : </div>\r\n                <mat-radio-group formControlName=\"authType\" >\r\n                <mat-radio-button value=\"BASIC\">{{'CORE.HOST_SETTINGS.BASIC' | translate }}\r\n                </mat-radio-button>\r\n                <mat-radio-button value=\"OAUTH\">{{'CORE.HOST_SETTINGS.SSO' | translate }}\r\n                </mat-radio-button>\r\n            </mat-radio-group>\r\n            </div>\r\n\r\n            <ng-container *ngIf=\"isALL() || isECM()\">\r\n                <mat-card-content>\r\n                    <mat-form-field class=\"adf-full-width\" floatLabel=\"{{'CORE.HOST_SETTINGS.CS-HOST' | translate }}\">\r\n                        <mat-label>{{'CORE.HOST_SETTINGS.CS-HOST' | translate }}</mat-label>\r\n                        <input matInput [formControl]=\"ecmHost\" data-automation-id=\"ecmHost\" type=\"text\"\r\n                               id=\"ecmHost\" placeholder=\"http(s)://host|ip:port(/path)\">\r\n                        <mat-error *ngIf=\"ecmHost.hasError('pattern')\">\r\n                            {{ 'CORE.HOST_SETTINGS.NOT_VALID'| translate }}\r\n                        </mat-error>\r\n                        <mat-error *ngIf=\"ecmHost.hasError('required')\">\r\n                            {{ 'CORE.HOST_SETTINGS.REQUIRED'| translate }}\r\n                        </mat-error>\r\n                    </mat-form-field>\r\n                    <p>\r\n                </mat-card-content>\r\n            </ng-container>\r\n\r\n            <ng-container *ngIf=\"isALL() || isBPM()\">\r\n                <mat-card-content>\r\n                    <mat-form-field class=\"adf-full-width\" floatLabel=\"{{'CORE.HOST_SETTINGS.BP-HOST' | translate }}\">\r\n                        <mat-label>{{'CORE.HOST_SETTINGS.BP-HOST' | translate }}</mat-label>\r\n                        <input matInput [formControl]=\"bpmHost\" data-automation-id=\"bpmHost\" type=\"text\"\r\n                               id=\"bpmHost\" placeholder=\"http(s)://host|ip:port(/path)\">\r\n                        <mat-error *ngIf=\"bpmHost.hasError('pattern')\">\r\n                            {{ 'CORE.HOST_SETTINGS.NOT_VALID'| translate }}\r\n                        </mat-error>\r\n                        <mat-error *ngIf=\"bpmHost.hasError('required')\">\r\n                            {{ 'CORE.HOST_SETTINGS.REQUIRED'| translate }}\r\n                        </mat-error>\r\n                    </mat-form-field>\r\n                </mat-card-content>\r\n            </ng-container>\r\n\r\n            <ng-container *ngIf=\"isOAUTH()\">\r\n                <mat-card-content>\r\n                    <mat-form-field class=\"adf-full-width\" floatLabel=\"Identity Host\">\r\n                        <mat-label>Identity Host</mat-label>\r\n                        <input matInput name=\"identityHost\" id=\"identityHost\" formControlName=\"identityHost\"\r\n                                placeholder=\"http(s)://host|ip:port(/path)\">\r\n                        <mat-error *ngIf=\"identityHost.hasError('pattern')\">\r\n                            {{ 'CORE.HOST_SETTINGS.NOT_VALID'| translate }}\r\n                        </mat-error>\r\n                        <mat-error *ngIf=\"identityHost.hasError('required')\">\r\n                            {{ 'CORE.HOST_SETTINGS.REQUIRED'| translate }}\r\n                        </mat-error>\r\n                    </mat-form-field>\r\n                </mat-card-content>\r\n            </ng-container>\r\n\r\n            <ng-container *ngIf=\"isOAUTH()\">\r\n                <div formGroupName=\"oauthConfig\">\r\n                    <mat-form-field class=\"adf-full-width\" floatLabel=\"Auth Host\">\r\n                        <mat-label>Auth Host</mat-label>\r\n                        <input matInput name=\"host\" id=\"oauthHost\" formControlName=\"host\"\r\n                               placeholder=\"http(s)://host|ip:port(/path)\">\r\n                        <mat-error *ngIf=\"host.hasError('pattern')\">\r\n                            {{ 'CORE.HOST_SETTINGS.NOT_VALID'| translate }}\r\n                        </mat-error>\r\n                        <mat-error *ngIf=\"host.hasError('required')\">\r\n                            {{ 'CORE.HOST_SETTINGS.REQUIRED'| translate }}\r\n                        </mat-error>\r\n                    </mat-form-field>\r\n                    <mat-form-field class=\"adf-full-width\" floatLabel=\"Client Id\">\r\n                        <mat-label>{{ 'CORE.HOST_SETTINGS.CLIENT'| translate }}</mat-label>\r\n                        <input matInput name=\"clientId\" id=\"clientId\" formControlName=\"clientId\"\r\n                               placeholder=\"Client Id\">\r\n                        <mat-error *ngIf=\"clientId.hasError('required')\">\r\n                            {{ 'CORE.HOST_SETTINGS.REQUIRED'| translate }}\r\n                        </mat-error>\r\n                    </mat-form-field>\r\n\r\n                    <mat-form-field class=\"adf-full-width\" floatLabel=\"Scope\">\r\n                        <mat-label>{{ 'CORE.HOST_SETTINGS.SCOPE'| translate }}</mat-label>\r\n                        <input matInput name=\"{{ 'CORE.HOST_SETTINGS.SCOPE'| translate }}\"\r\n                               formControlName=\"scope\" placeholder=\"Scope Id\">\r\n                        <mat-error *ngIf=\"scope.hasError('required')\">\r\n                            {{ 'CORE.HOST_SETTINGS.REQUIRED'| translate }}\r\n                        </mat-error>\r\n                    </mat-form-field>\r\n\r\n                    <label for=\"silentLogin\">{{ 'CORE.HOST_SETTINGS.SILENT'| translate }}</label>\r\n                    <mat-slide-toggle class=\"adf-full-width\" name=\"silentLogin\" [color]=\"'primary'\"\r\n                                      formControlName=\"silentLogin\">\r\n                    </mat-slide-toggle>\r\n\r\n                    <label for=\"implicitFlow\">{{ 'CORE.HOST_SETTINGS.IMPLICIT-FLOW'| translate }}</label>\r\n                    <mat-slide-toggle class=\"adf-full-width\" name=\"implicitFlow\" [color]=\"'primary'\"\r\n                                      formControlName=\"implicitFlow\">\r\n                    </mat-slide-toggle>\r\n\r\n\r\n                    <mat-form-field class=\"adf-full-width\" floatLabel=\"Redirect Uri\">\r\n                        <mat-label>{{ 'CORE.HOST_SETTINGS.REDIRECT'| translate }}</mat-label>\r\n                        <input matInput placeholder=\"{{ 'CORE.HOST_SETTINGS.REDIRECT'| translate }}\"\r\n                               name=\"redirectUri\" formControlName=\"redirectUri\">\r\n                        <mat-error *ngIf=\"redirectUri.hasError('required')\">\r\n                            {{ 'CORE.HOST_SETTINGS.REQUIRED'| translate }}\r\n                        </mat-error>\r\n                    </mat-form-field>\r\n\r\n                    <mat-form-field class=\"adf-full-width\" floatLabel=\"Redirect Uri Logout\">\r\n                        <mat-label>{{ 'CORE.HOST_SETTINGS.REDIRECT_LOGOUT'| translate }}</mat-label>\r\n                        <input id=\"logout-url\" matInput placeholder=\"{{ 'CORE.HOST_SETTINGS.REDIRECT_LOGOUT'| translate }}\"\r\n                               name=\"redirectUriLogout\" formControlName=\"redirectUriLogout\">\r\n                    </mat-form-field>\r\n                </div>\r\n            </ng-container>\r\n            <mat-card-actions class=\"adf-actions\">\r\n                <button mat-button (click)=\"onCancel()\" color=\"primary\">\r\n                    {{'CORE.HOST_SETTINGS.BACK' | translate }}\r\n                </button>\r\n                <button type=\"submit\" id=\"host-button\" class=\"adf-login-button\" mat-raised-button\r\n                        color=\"primary\" data-automation-id=\"host-button\"\r\n                        [disabled]=\"!form.valid\">\r\n                    {{'CORE.HOST_SETTINGS.APPLY' | translate }}\r\n                </button>\r\n            </mat-card-actions>\r\n        </form>\r\n    </mat-card>\r\n</div>\r\n",
                    host: {
                        'class': 'adf-host-settings'
                    },
                    encapsulation: ViewEncapsulation.None,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    HostSettingsComponent.ctorParameters = function () { return [
        { type: FormBuilder },
        { type: StorageService },
        { type: AlfrescoApiService },
        { type: AppConfigService }
    ]; };
    HostSettingsComponent.propDecorators = {
        providers: [{ type: Input }],
        error: [{ type: Output }],
        cancel: [{ type: Output }],
        success: [{ type: Output }]
    };
    return HostSettingsComponent;
}());
export { HostSettingsComponent };
if (false) {
    /** @type {?} */
    HostSettingsComponent.prototype.HOST_REGEX;
    /**
     * Tells the component which provider options are available. Possible valid values
     * are "ECM" (Content), "BPM" (Process) , "ALL" (Content and Process), 'OAUTH2' SSO.
     * @type {?}
     */
    HostSettingsComponent.prototype.providers;
    /** @type {?} */
    HostSettingsComponent.prototype.showSelectProviders;
    /** @type {?} */
    HostSettingsComponent.prototype.form;
    /**
     * Emitted when the URL is invalid.
     * @type {?}
     */
    HostSettingsComponent.prototype.error;
    /**
     * Emitted when the user cancels the changes.
     * @type {?}
     */
    HostSettingsComponent.prototype.cancel;
    /**
     * Emitted when the changes are successfully applied.
     * @type {?}
     */
    HostSettingsComponent.prototype.success;
    /**
     * @type {?}
     * @private
     */
    HostSettingsComponent.prototype.formBuilder;
    /**
     * @type {?}
     * @private
     */
    HostSettingsComponent.prototype.storageService;
    /**
     * @type {?}
     * @private
     */
    HostSettingsComponent.prototype.alfrescoApiService;
    /**
     * @type {?}
     * @private
     */
    HostSettingsComponent.prototype.appConfig;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaG9zdC1zZXR0aW5ncy5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJzZXR0aW5ncy9ob3N0LXNldHRpbmdzLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsU0FBUyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsaUJBQWlCLEVBQVUsS0FBSyxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQ2xHLE9BQU8sRUFBRSxVQUFVLEVBQWEsV0FBVyxFQUFtQixXQUFXLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUNsRyxPQUFPLEVBQUUsZ0JBQWdCLEVBQUUsZUFBZSxFQUFFLE1BQU0sa0NBQWtDLENBQUM7QUFDckYsT0FBTyxFQUFFLGNBQWMsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQzdELE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLGtDQUFrQyxDQUFDO0FBRXRFLE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSx1QkFBdUIsQ0FBQztBQUU5QztJQW9DSSwrQkFBb0IsV0FBd0IsRUFDeEIsY0FBOEIsRUFDOUIsa0JBQXNDLEVBQ3RDLFNBQTJCO1FBSDNCLGdCQUFXLEdBQVgsV0FBVyxDQUFhO1FBQ3hCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5Qix1QkFBa0IsR0FBbEIsa0JBQWtCLENBQW9CO1FBQ3RDLGNBQVMsR0FBVCxTQUFTLENBQWtCO1FBNUIvQyxlQUFVLEdBQVcsMkJBQTJCLENBQUM7Ozs7O1FBT2pELGNBQVMsR0FBYSxDQUFDLEtBQUssRUFBRSxLQUFLLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFFNUMsd0JBQW1CLEdBQUcsSUFBSSxDQUFDOzs7O1FBTTNCLFVBQUssR0FBRyxJQUFJLFlBQVksRUFBVSxDQUFDOzs7O1FBSW5DLFdBQU0sR0FBRyxJQUFJLFlBQVksRUFBVyxDQUFDOzs7O1FBSXJDLFlBQU8sR0FBRyxJQUFJLFlBQVksRUFBVyxDQUFDO0lBTXRDLENBQUM7Ozs7SUFFRCx3Q0FBUTs7O0lBQVI7UUFBQSxpQkFtQ0M7UUFsQ0csSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sS0FBSyxDQUFDLEVBQUU7WUFDN0IsSUFBSSxDQUFDLG1CQUFtQixHQUFHLEtBQUssQ0FBQztTQUNwQzs7WUFFSyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBUyxlQUFlLENBQUMsU0FBUyxDQUFDOztZQUV4RSxRQUFRLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQVMsZUFBZSxDQUFDLFFBQVEsRUFBRSxPQUFPLENBQUM7UUFFOUUsSUFBSSxDQUFDLElBQUksR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQztZQUMvQixnQkFBZ0IsRUFBRSxDQUFDLGdCQUFnQixFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUM7WUFDekQsUUFBUSxFQUFFLFFBQVE7U0FDckIsQ0FBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLGFBQWEsRUFBRSxDQUFDO1FBRXJCLElBQUksUUFBUSxLQUFLLE9BQU8sRUFBRTtZQUN0QixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztZQUN6QixJQUFJLENBQUMsMEJBQTBCLEVBQUUsQ0FBQztTQUNyQztRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFVBQVUsQ0FBQyxDQUFDLFlBQVksQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxLQUFLO1lBQ25ELElBQUksS0FBSyxLQUFLLE9BQU8sRUFBRTtnQkFDbkIsS0FBSSxDQUFDLElBQUksQ0FBQyxhQUFhLENBQUMsYUFBYSxDQUFDLENBQUM7Z0JBQ3ZDLEtBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxDQUFDO2FBQzNDO2lCQUFNO2dCQUNILEtBQUksQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO2dCQUN6QixLQUFJLENBQUMsMEJBQTBCLEVBQUUsQ0FBQzthQUNyQztRQUNMLENBQUMsRUFBQyxDQUFDO1FBRUgsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFlBQVksQ0FBQyxTQUFTOzs7UUFBQztZQUN6QyxLQUFJLENBQUMsZ0JBQWdCLEVBQUUsQ0FBQztZQUN4QixLQUFJLENBQUMsYUFBYSxFQUFFLENBQUM7UUFDekIsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVPLGdEQUFnQjs7OztJQUF4QjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ25DLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7Ozs7O0lBRU8sNkNBQWE7Ozs7SUFBckI7UUFDSSxJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUN6QixJQUFJLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztJQUM3QixDQUFDOzs7OztJQUVPLGlEQUFpQjs7OztJQUF6Qjs7WUFDVSxjQUFjLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixFQUFFO1FBQ2xELElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxjQUFjLENBQUMsQ0FBQztJQUN4RCxDQUFDOzs7OztJQUVPLGlEQUFpQjs7OztJQUF6QjtRQUNJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRTs7Z0JBQzdELGNBQWMsR0FBRyxJQUFJLENBQUMsb0JBQW9CLEVBQUU7WUFDbEQsSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsU0FBUyxFQUFFLGNBQWMsQ0FBQyxDQUFDO1NBQ25EO0lBQ0wsQ0FBQzs7Ozs7SUFFTywwREFBMEI7Ozs7SUFBbEM7O1lBQ1UsdUJBQXVCLEdBQUcsSUFBSSxDQUFDLHlCQUF5QixFQUFFO1FBQ2hFLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLGNBQWMsRUFBRSx1QkFBdUIsQ0FBQyxDQUFDO0lBQ2xFLENBQUM7Ozs7O0lBRU8saURBQWlCOzs7O0lBQXpCO1FBQ0ksSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxPQUFPLEVBQUU7O2dCQUMzQyxjQUFjLEdBQUcsSUFBSSxDQUFDLG9CQUFvQixFQUFFO1lBQ2xELElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFNBQVMsRUFBRSxjQUFjLENBQUMsQ0FBQztTQUNuRDtJQUNMLENBQUM7Ozs7O0lBRU8sb0RBQW9COzs7O0lBQTVCOztZQUNVLEtBQUssR0FBRyxtQkFBbUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQUMsZUFBZSxDQUFDLFdBQVcsRUFBRSxFQUFFLENBQUMsRUFBQTtRQUVwRixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO1lBQzFCLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxJQUFJLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUM7WUFDOUUsUUFBUSxFQUFFLENBQUMsS0FBSyxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsUUFBUSxDQUFDO1lBQy9DLFdBQVcsRUFBRSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsVUFBVSxDQUFDLFFBQVEsQ0FBQztZQUNyRCxpQkFBaUIsRUFBRSxDQUFDLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQztZQUM1QyxLQUFLLEVBQUUsQ0FBQyxLQUFLLENBQUMsS0FBSyxFQUFFLFVBQVUsQ0FBQyxRQUFRLENBQUM7WUFDekMsTUFBTSxFQUFFLEtBQUssQ0FBQyxNQUFNO1lBQ3BCLFdBQVcsRUFBRSxLQUFLLENBQUMsV0FBVztZQUM5QixZQUFZLEVBQUUsS0FBSyxDQUFDLFlBQVk7U0FDbkMsQ0FBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7SUFFTyxvREFBb0I7Ozs7SUFBNUI7UUFDSSxPQUFPLElBQUksV0FBVyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFTLGVBQWUsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsVUFBVSxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQzVJLENBQUM7Ozs7O0lBRU8seURBQXlCOzs7O0lBQWpDO1FBQ0ksT0FBTyxJQUFJLFdBQVcsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLEdBQUcsQ0FBUyxlQUFlLENBQUMsYUFBYSxDQUFDLEVBQUUsQ0FBQyxVQUFVLENBQUMsUUFBUSxFQUFFLFVBQVUsQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNsSixDQUFDOzs7OztJQUVPLG9EQUFvQjs7OztJQUE1QjtRQUNJLE9BQU8sSUFBSSxXQUFXLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxHQUFHLENBQVMsZUFBZSxDQUFDLE9BQU8sQ0FBQyxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxVQUFVLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFDNUksQ0FBQzs7OztJQUVELHdDQUFROzs7SUFBUjtRQUNJLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO0lBQzNCLENBQUM7Ozs7O0lBRUQsd0NBQVE7Ozs7SUFBUixVQUFTLE1BQVc7UUFDaEIsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsZUFBZSxDQUFDLFNBQVMsRUFBRSxNQUFNLENBQUMsZ0JBQWdCLENBQUMsQ0FBQztRQUVoRixJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUUsRUFBRTtZQUNkLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDLENBQUM7U0FDOUI7YUFBTSxJQUFJLElBQUksQ0FBQyxLQUFLLEVBQUUsRUFBRTtZQUNyQixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQzlCO2FBQU0sSUFBSSxJQUFJLENBQUMsS0FBSyxFQUFFLEVBQUU7WUFDckIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsQ0FBQztZQUMzQixJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQzlCO1FBRUQsSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFLEVBQUU7WUFDaEIsSUFBSSxDQUFDLGVBQWUsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUNoQztRQUVELElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBRXZFLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxLQUFLLEVBQUUsQ0FBQztRQUNoQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsaUJBQWlCLEVBQUUsQ0FBQztRQUMxRCxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUM1QixDQUFDOzs7OztJQUVELCtDQUFlOzs7O0lBQWYsVUFBZ0IsS0FBVTtRQUN0QixJQUFJLEtBQUssQ0FBQyxPQUFPLEtBQUssS0FBSyxJQUFJLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxFQUFFO1lBQzVDLElBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsQ0FBQztTQUNsQztJQUNMLENBQUM7Ozs7OztJQUVPLCtDQUFlOzs7OztJQUF2QixVQUF3QixNQUFXO1FBQy9CLElBQUksQ0FBQyxjQUFjLENBQUMsT0FBTyxDQUFDLGVBQWUsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsV0FBVyxDQUFDLENBQUMsQ0FBQztRQUM3RixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsYUFBYSxFQUFFLE1BQU0sQ0FBQyxZQUFZLENBQUMsQ0FBQztJQUNwRixDQUFDOzs7Ozs7SUFFTyw2Q0FBYTs7Ozs7SUFBckIsVUFBc0IsTUFBVztRQUM3QixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN6RSxDQUFDOzs7Ozs7SUFFTyw2Q0FBYTs7Ozs7SUFBckIsVUFBc0IsTUFBVztRQUM3QixJQUFJLENBQUMsY0FBYyxDQUFDLE9BQU8sQ0FBQyxlQUFlLENBQUMsT0FBTyxFQUFFLE1BQU0sQ0FBQyxPQUFPLENBQUMsQ0FBQztJQUN6RSxDQUFDOzs7O0lBRUQscUNBQUs7OztJQUFMO1FBQ0ksT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxLQUFLLEtBQUssQ0FBQztJQUNqRCxDQUFDOzs7O0lBRUQscUNBQUs7OztJQUFMO1FBQ0ksT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxLQUFLLEtBQUssQ0FBQztJQUNqRCxDQUFDOzs7O0lBRUQscUNBQUs7OztJQUFMO1FBQ0ksT0FBTyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsS0FBSyxLQUFLLEtBQUssQ0FBQztJQUNqRCxDQUFDOzs7O0lBRUQsdUNBQU87OztJQUFQO1FBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxLQUFLLEtBQUssT0FBTyxDQUFDO0lBQ3ZELENBQUM7SUFFRCxzQkFBSSxtREFBZ0I7Ozs7UUFBcEI7WUFDSSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDN0MsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwwQ0FBTzs7OztRQUFYO1lBQ0ksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxTQUFTLENBQUMsQ0FBQztRQUNwQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDBDQUFPOzs7O1FBQVg7WUFDSSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO1FBQ3BDLENBQUM7OztPQUFBO0lBRUQsc0JBQUksdUNBQUk7Ozs7UUFBUjtZQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsTUFBTSxDQUFDLENBQUM7UUFDeEMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwrQ0FBWTs7OztRQUFoQjtZQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDekMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwyQ0FBUTs7OztRQUFaO1lBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQztRQUM1QyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLHdDQUFLOzs7O1FBQVQ7WUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxDQUFDO1FBQ3pDLENBQUM7OztPQUFBO0lBRUQsc0JBQUksMkNBQVE7Ozs7UUFBWjtZQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsVUFBVSxDQUFDLENBQUM7UUFDNUMsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSwrQ0FBWTs7OztRQUFoQjtZQUNJLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLENBQUM7UUFDaEQsQ0FBQzs7O09BQUE7SUFFRCxzQkFBSSw4Q0FBVzs7OztRQUFmO1lBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUMvQyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLDhDQUFXOzs7O1FBQWY7WUFDSSxPQUFPLElBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQy9DLENBQUM7OztPQUFBO0lBRUQsc0JBQUksb0RBQWlCOzs7O1FBQXJCO1lBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLEdBQUcsQ0FBQyxtQkFBbUIsQ0FBQyxDQUFDO1FBQ3JELENBQUM7OztPQUFBO0lBRUQsc0JBQUksOENBQVc7Ozs7UUFBZjtZQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxHQUFHLENBQUMsYUFBYSxDQUFDLENBQUM7UUFDeEMsQ0FBQzs7O09BQUE7O2dCQTVQSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLG1CQUFtQjtvQkFDN0IsNHpSQUEyQztvQkFDM0MsSUFBSSxFQUFFO3dCQUNGLE9BQU8sRUFBRSxtQkFBbUI7cUJBQy9CO29CQUVELGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOztpQkFDeEM7Ozs7Z0JBZitCLFdBQVc7Z0JBRWxDLGNBQWM7Z0JBQ2Qsa0JBQWtCO2dCQUZsQixnQkFBZ0I7Ozs0QkF1QnBCLEtBQUs7d0JBUUwsTUFBTTt5QkFJTixNQUFNOzBCQUlOLE1BQU07O0lBNk5YLDRCQUFDO0NBQUEsQUE5UEQsSUE4UEM7U0FyUFkscUJBQXFCOzs7SUFFOUIsMkNBQWlEOzs7Ozs7SUFNakQsMENBQzRDOztJQUU1QyxvREFBMkI7O0lBRTNCLHFDQUFnQjs7Ozs7SUFHaEIsc0NBQ21DOzs7OztJQUduQyx1Q0FDcUM7Ozs7O0lBR3JDLHdDQUNzQzs7Ozs7SUFFMUIsNENBQWdDOzs7OztJQUNoQywrQ0FBc0M7Ozs7O0lBQ3RDLG1EQUE4Qzs7Ozs7SUFDOUMsMENBQW1DIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgRXZlbnRFbWl0dGVyLCBPdXRwdXQsIFZpZXdFbmNhcHN1bGF0aW9uLCBPbkluaXQsIElucHV0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFZhbGlkYXRvcnMsIEZvcm1Hcm91cCwgRm9ybUJ1aWxkZXIsIEFic3RyYWN0Q29udHJvbCwgRm9ybUNvbnRyb2wgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UsIEFwcENvbmZpZ1ZhbHVlcyB9IGZyb20gJy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgU3RvcmFnZVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9zdG9yYWdlLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBBbGZyZXNjb0FwaVNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9hbGZyZXNjby1hcGkuc2VydmljZSc7XHJcbmltcG9ydCB7IE9hdXRoQ29uZmlnTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvb2F1dGgtY29uZmlnLm1vZGVsJztcclxuaW1wb3J0IHsgRU5URVIgfSBmcm9tICdAYW5ndWxhci9jZGsva2V5Y29kZXMnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1ob3N0LXNldHRpbmdzJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnaG9zdC1zZXR0aW5ncy5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBob3N0OiB7XHJcbiAgICAgICAgJ2NsYXNzJzogJ2FkZi1ob3N0LXNldHRpbmdzJ1xyXG4gICAgfSxcclxuICAgIHN0eWxlVXJsczogWydob3N0LXNldHRpbmdzLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBIb3N0U2V0dGluZ3NDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEhPU1RfUkVHRVg6IHN0cmluZyA9ICdeKGh0dHB8aHR0cHMpOlxcL1xcLy4qW14vXSQnO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGVsbHMgdGhlIGNvbXBvbmVudCB3aGljaCBwcm92aWRlciBvcHRpb25zIGFyZSBhdmFpbGFibGUuIFBvc3NpYmxlIHZhbGlkIHZhbHVlc1xyXG4gICAgICogYXJlIFwiRUNNXCIgKENvbnRlbnQpLCBcIkJQTVwiIChQcm9jZXNzKSAsIFwiQUxMXCIgKENvbnRlbnQgYW5kIFByb2Nlc3MpLCAnT0FVVEgyJyBTU08uXHJcbiAgICAgKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBwcm92aWRlcnM6IHN0cmluZ1tdID0gWydCUE0nLCAnRUNNJywgJ0FMTCddO1xyXG5cclxuICAgIHNob3dTZWxlY3RQcm92aWRlcnMgPSB0cnVlO1xyXG5cclxuICAgIGZvcm06IEZvcm1Hcm91cDtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSBVUkwgaXMgaW52YWxpZC4gKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgZXJyb3IgPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSB1c2VyIGNhbmNlbHMgdGhlIGNoYW5nZXMuICovXHJcbiAgICBAT3V0cHV0KClcclxuICAgIGNhbmNlbCA9IG5ldyBFdmVudEVtaXR0ZXI8Ym9vbGVhbj4oKTtcclxuXHJcbiAgICAvKiogRW1pdHRlZCB3aGVuIHRoZSBjaGFuZ2VzIGFyZSBzdWNjZXNzZnVsbHkgYXBwbGllZC4gKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgc3VjY2VzcyA9IG5ldyBFdmVudEVtaXR0ZXI8Ym9vbGVhbj4oKTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGZvcm1CdWlsZGVyOiBGb3JtQnVpbGRlcixcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgc3RvcmFnZVNlcnZpY2U6IFN0b3JhZ2VTZXJ2aWNlLFxyXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBhbGZyZXNjb0FwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgYXBwQ29uZmlnOiBBcHBDb25maWdTZXJ2aWNlKSB7XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucHJvdmlkZXJzLmxlbmd0aCA9PT0gMSkge1xyXG4gICAgICAgICAgICB0aGlzLnNob3dTZWxlY3RQcm92aWRlcnMgPSBmYWxzZTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHByb3ZpZGVyU2VsZWN0ZWQgPSB0aGlzLmFwcENvbmZpZy5nZXQ8c3RyaW5nPihBcHBDb25maWdWYWx1ZXMuUFJPVklERVJTKTtcclxuXHJcbiAgICAgICAgY29uc3QgYXV0aFR5cGUgPSB0aGlzLmFwcENvbmZpZy5nZXQ8c3RyaW5nPihBcHBDb25maWdWYWx1ZXMuQVVUSFRZUEUsICdCQVNJQycpO1xyXG5cclxuICAgICAgICB0aGlzLmZvcm0gPSB0aGlzLmZvcm1CdWlsZGVyLmdyb3VwKHtcclxuICAgICAgICAgICAgcHJvdmlkZXJzQ29udHJvbDogW3Byb3ZpZGVyU2VsZWN0ZWQsIFZhbGlkYXRvcnMucmVxdWlyZWRdLFxyXG4gICAgICAgICAgICBhdXRoVHlwZTogYXV0aFR5cGVcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgdGhpcy5hZGRGb3JtR3JvdXBzKCk7XHJcblxyXG4gICAgICAgIGlmIChhdXRoVHlwZSA9PT0gJ09BVVRIJykge1xyXG4gICAgICAgICAgICB0aGlzLmFkZE9BdXRoRm9ybUdyb3VwKCk7XHJcbiAgICAgICAgICAgIHRoaXMuYWRkSWRlbnRpdHlIb3N0Rm9ybUNvbnRyb2woKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuZm9ybS5nZXQoJ2F1dGhUeXBlJykudmFsdWVDaGFuZ2VzLnN1YnNjcmliZSgodmFsdWUpID0+IHtcclxuICAgICAgICAgICAgaWYgKHZhbHVlID09PSAnQkFTSUMnKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmZvcm0ucmVtb3ZlQ29udHJvbCgnb2F1dGhDb25maWcnKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuZm9ybS5yZW1vdmVDb250cm9sKCdpZGVudGl0eUhvc3QnKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWRkT0F1dGhGb3JtR3JvdXAoKTtcclxuICAgICAgICAgICAgICAgIHRoaXMuYWRkSWRlbnRpdHlIb3N0Rm9ybUNvbnRyb2woKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0pO1xyXG5cclxuICAgICAgICB0aGlzLnByb3ZpZGVyc0NvbnRyb2wudmFsdWVDaGFuZ2VzLnN1YnNjcmliZSgoKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMucmVtb3ZlRm9ybUdyb3VwcygpO1xyXG4gICAgICAgICAgICB0aGlzLmFkZEZvcm1Hcm91cHMoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHJlbW92ZUZvcm1Hcm91cHMoKSB7XHJcbiAgICAgICAgdGhpcy5mb3JtLnJlbW92ZUNvbnRyb2woJ2JwbUhvc3QnKTtcclxuICAgICAgICB0aGlzLmZvcm0ucmVtb3ZlQ29udHJvbCgnZWNtSG9zdCcpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYWRkRm9ybUdyb3VwcygpIHtcclxuICAgICAgICB0aGlzLmFkZEJQTUZvcm1Db250cm9sKCk7XHJcbiAgICAgICAgdGhpcy5hZGRFQ01Gb3JtQ29udHJvbCgpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYWRkT0F1dGhGb3JtR3JvdXAoKSB7XHJcbiAgICAgICAgY29uc3Qgb2F1dGhGb3JtR3JvdXAgPSB0aGlzLmNyZWF0ZU9BdXRoRm9ybUdyb3VwKCk7XHJcbiAgICAgICAgdGhpcy5mb3JtLmFkZENvbnRyb2woJ29hdXRoQ29uZmlnJywgb2F1dGhGb3JtR3JvdXApO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYWRkQlBNRm9ybUNvbnRyb2woKSB7XHJcbiAgICAgICAgaWYgKCh0aGlzLmlzQlBNKCkgfHwgdGhpcy5pc0FMTCgpIHx8IHRoaXMuaXNPQVVUSCgpKSAmJiAhdGhpcy5icG1Ib3N0KSB7XHJcbiAgICAgICAgICAgIGNvbnN0IGJwbUZvcm1Db250cm9sID0gdGhpcy5jcmVhdGVCUE1Gb3JtQ29udHJvbCgpO1xyXG4gICAgICAgICAgICB0aGlzLmZvcm0uYWRkQ29udHJvbCgnYnBtSG9zdCcsIGJwbUZvcm1Db250cm9sKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBhZGRJZGVudGl0eUhvc3RGb3JtQ29udHJvbCgpIHtcclxuICAgICAgICBjb25zdCBpZGVudGl0eUhvc3RGb3JtQ29udHJvbCA9IHRoaXMuY3JlYXRlSWRlbnRpdHlGb3JtQ29udHJvbCgpO1xyXG4gICAgICAgIHRoaXMuZm9ybS5hZGRDb250cm9sKCdpZGVudGl0eUhvc3QnLCBpZGVudGl0eUhvc3RGb3JtQ29udHJvbCk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBhZGRFQ01Gb3JtQ29udHJvbCgpIHtcclxuICAgICAgICBpZiAoKHRoaXMuaXNFQ00oKSB8fCB0aGlzLmlzQUxMKCkpICYmICF0aGlzLmVjbUhvc3QpIHtcclxuICAgICAgICAgICAgY29uc3QgZWNtRm9ybUNvbnRyb2wgPSB0aGlzLmNyZWF0ZUVDTUZvcm1Db250cm9sKCk7XHJcbiAgICAgICAgICAgIHRoaXMuZm9ybS5hZGRDb250cm9sKCdlY21Ib3N0JywgZWNtRm9ybUNvbnRyb2wpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNyZWF0ZU9BdXRoRm9ybUdyb3VwKCk6IEFic3RyYWN0Q29udHJvbCB7XHJcbiAgICAgICAgY29uc3Qgb2F1dGggPSA8T2F1dGhDb25maWdNb2RlbD4gdGhpcy5hcHBDb25maWcuZ2V0KEFwcENvbmZpZ1ZhbHVlcy5PQVVUSENPTkZJRywge30pO1xyXG5cclxuICAgICAgICByZXR1cm4gdGhpcy5mb3JtQnVpbGRlci5ncm91cCh7XHJcbiAgICAgICAgICAgIGhvc3Q6IFtvYXV0aC5ob3N0LCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5wYXR0ZXJuKHRoaXMuSE9TVF9SRUdFWCldXSxcclxuICAgICAgICAgICAgY2xpZW50SWQ6IFtvYXV0aC5jbGllbnRJZCwgVmFsaWRhdG9ycy5yZXF1aXJlZF0sXHJcbiAgICAgICAgICAgIHJlZGlyZWN0VXJpOiBbb2F1dGgucmVkaXJlY3RVcmksIFZhbGlkYXRvcnMucmVxdWlyZWRdLFxyXG4gICAgICAgICAgICByZWRpcmVjdFVyaUxvZ291dDogW29hdXRoLnJlZGlyZWN0VXJpTG9nb3V0XSxcclxuICAgICAgICAgICAgc2NvcGU6IFtvYXV0aC5zY29wZSwgVmFsaWRhdG9ycy5yZXF1aXJlZF0sXHJcbiAgICAgICAgICAgIHNlY3JldDogb2F1dGguc2VjcmV0LFxyXG4gICAgICAgICAgICBzaWxlbnRMb2dpbjogb2F1dGguc2lsZW50TG9naW4sXHJcbiAgICAgICAgICAgIGltcGxpY2l0Rmxvdzogb2F1dGguaW1wbGljaXRGbG93XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBjcmVhdGVCUE1Gb3JtQ29udHJvbCgpOiBBYnN0cmFjdENvbnRyb2wge1xyXG4gICAgICAgIHJldHVybiBuZXcgRm9ybUNvbnRyb2wodGhpcy5hcHBDb25maWcuZ2V0PHN0cmluZz4oQXBwQ29uZmlnVmFsdWVzLkJQTUhPU1QpLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5wYXR0ZXJuKHRoaXMuSE9TVF9SRUdFWCldKTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGNyZWF0ZUlkZW50aXR5Rm9ybUNvbnRyb2woKTogQWJzdHJhY3RDb250cm9sIHtcclxuICAgICAgICByZXR1cm4gbmV3IEZvcm1Db250cm9sKHRoaXMuYXBwQ29uZmlnLmdldDxzdHJpbmc+KEFwcENvbmZpZ1ZhbHVlcy5JREVOVElUWV9IT1NUKSwgW1ZhbGlkYXRvcnMucmVxdWlyZWQsIFZhbGlkYXRvcnMucGF0dGVybih0aGlzLkhPU1RfUkVHRVgpXSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBjcmVhdGVFQ01Gb3JtQ29udHJvbCgpOiBBYnN0cmFjdENvbnRyb2wge1xyXG4gICAgICAgIHJldHVybiBuZXcgRm9ybUNvbnRyb2wodGhpcy5hcHBDb25maWcuZ2V0PHN0cmluZz4oQXBwQ29uZmlnVmFsdWVzLkVDTUhPU1QpLCBbVmFsaWRhdG9ycy5yZXF1aXJlZCwgVmFsaWRhdG9ycy5wYXR0ZXJuKHRoaXMuSE9TVF9SRUdFWCldKTtcclxuICAgIH1cclxuXHJcbiAgICBvbkNhbmNlbCgpIHtcclxuICAgICAgICB0aGlzLmNhbmNlbC5lbWl0KHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIG9uU3VibWl0KHZhbHVlczogYW55KSB7XHJcbiAgICAgICAgdGhpcy5zdG9yYWdlU2VydmljZS5zZXRJdGVtKEFwcENvbmZpZ1ZhbHVlcy5QUk9WSURFUlMsIHZhbHVlcy5wcm92aWRlcnNDb250cm9sKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMuaXNCUE0oKSkge1xyXG4gICAgICAgICAgICB0aGlzLnNhdmVCUE1WYWx1ZXModmFsdWVzKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuaXNFQ00oKSkge1xyXG4gICAgICAgICAgICB0aGlzLnNhdmVFQ01WYWx1ZXModmFsdWVzKTtcclxuICAgICAgICB9IGVsc2UgaWYgKHRoaXMuaXNBTEwoKSkge1xyXG4gICAgICAgICAgICB0aGlzLnNhdmVFQ01WYWx1ZXModmFsdWVzKTtcclxuICAgICAgICAgICAgdGhpcy5zYXZlQlBNVmFsdWVzKHZhbHVlcyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5pc09BVVRIKCkpIHtcclxuICAgICAgICAgICAgdGhpcy5zYXZlT0F1dGhWYWx1ZXModmFsdWVzKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHRoaXMuc3RvcmFnZVNlcnZpY2Uuc2V0SXRlbShBcHBDb25maWdWYWx1ZXMuQVVUSFRZUEUsIHZhbHVlcy5hdXRoVHlwZSk7XHJcblxyXG4gICAgICAgIHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLnJlc2V0KCk7XHJcbiAgICAgICAgdGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5pbnZhbGlkYXRlU2Vzc2lvbigpO1xyXG4gICAgICAgIHRoaXMuc3VjY2Vzcy5lbWl0KHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIGtleURvd25GdW5jdGlvbihldmVudDogYW55KSB7XHJcbiAgICAgICAgaWYgKGV2ZW50LmtleUNvZGUgPT09IEVOVEVSICYmIHRoaXMuZm9ybS52YWxpZCkge1xyXG4gICAgICAgICAgICB0aGlzLm9uU3VibWl0KHRoaXMuZm9ybS52YWx1ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgc2F2ZU9BdXRoVmFsdWVzKHZhbHVlczogYW55KSB7XHJcbiAgICAgICAgdGhpcy5zdG9yYWdlU2VydmljZS5zZXRJdGVtKEFwcENvbmZpZ1ZhbHVlcy5PQVVUSENPTkZJRywgSlNPTi5zdHJpbmdpZnkodmFsdWVzLm9hdXRoQ29uZmlnKSk7XHJcbiAgICAgICAgdGhpcy5zdG9yYWdlU2VydmljZS5zZXRJdGVtKEFwcENvbmZpZ1ZhbHVlcy5JREVOVElUWV9IT1NULCB2YWx1ZXMuaWRlbnRpdHlIb3N0KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHNhdmVCUE1WYWx1ZXModmFsdWVzOiBhbnkpIHtcclxuICAgICAgICB0aGlzLnN0b3JhZ2VTZXJ2aWNlLnNldEl0ZW0oQXBwQ29uZmlnVmFsdWVzLkJQTUhPU1QsIHZhbHVlcy5icG1Ib3N0KTtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIHNhdmVFQ01WYWx1ZXModmFsdWVzOiBhbnkpIHtcclxuICAgICAgICB0aGlzLnN0b3JhZ2VTZXJ2aWNlLnNldEl0ZW0oQXBwQ29uZmlnVmFsdWVzLkVDTUhPU1QsIHZhbHVlcy5lY21Ib3N0KTtcclxuICAgIH1cclxuXHJcbiAgICBpc0JQTSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm92aWRlcnNDb250cm9sLnZhbHVlID09PSAnQlBNJztcclxuICAgIH1cclxuXHJcbiAgICBpc0VDTSgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm92aWRlcnNDb250cm9sLnZhbHVlID09PSAnRUNNJztcclxuICAgIH1cclxuXHJcbiAgICBpc0FMTCgpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5wcm92aWRlcnNDb250cm9sLnZhbHVlID09PSAnQUxMJztcclxuICAgIH1cclxuXHJcbiAgICBpc09BVVRIKCk6IGJvb2xlYW4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZvcm0uZ2V0KCdhdXRoVHlwZScpLnZhbHVlID09PSAnT0FVVEgnO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBwcm92aWRlcnNDb250cm9sKCk6IEFic3RyYWN0Q29udHJvbCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuZm9ybS5nZXQoJ3Byb3ZpZGVyc0NvbnRyb2wnKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgYnBtSG9zdCgpOiBBYnN0cmFjdENvbnRyb2wge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZvcm0uZ2V0KCdicG1Ib3N0Jyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGVjbUhvc3QoKTogQWJzdHJhY3RDb250cm9sIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5mb3JtLmdldCgnZWNtSG9zdCcpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBob3N0KCk6IEFic3RyYWN0Q29udHJvbCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub2F1dGhDb25maWcuZ2V0KCdob3N0Jyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGlkZW50aXR5SG9zdCgpOiBBYnN0cmFjdENvbnRyb2wge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmZvcm0uZ2V0KCdpZGVudGl0eUhvc3QnKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgY2xpZW50SWQoKTogQWJzdHJhY3RDb250cm9sIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5vYXV0aENvbmZpZy5nZXQoJ2NsaWVudElkJyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHNjb3BlKCk6IEFic3RyYWN0Q29udHJvbCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub2F1dGhDb25maWcuZ2V0KCdzY29wZScpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBzZWNyZXRJZCgpOiBBYnN0cmFjdENvbnRyb2wge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm9hdXRoQ29uZmlnLmdldCgnc2VjcmV0SWQnKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgaW1wbGljaXRGbG93KCk6IEFic3RyYWN0Q29udHJvbCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub2F1dGhDb25maWcuZ2V0KCdpbXBsaWNpdEZsb3cnKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgc2lsZW50TG9naW4oKTogQWJzdHJhY3RDb250cm9sIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5vYXV0aENvbmZpZy5nZXQoJ3NpbGVudExvZ2luJyk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IHJlZGlyZWN0VXJpKCk6IEFic3RyYWN0Q29udHJvbCB7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMub2F1dGhDb25maWcuZ2V0KCdyZWRpcmVjdFVyaScpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCByZWRpcmVjdFVyaUxvZ291dCgpOiBBYnN0cmFjdENvbnRyb2wge1xyXG4gICAgICAgIHJldHVybiB0aGlzLm9hdXRoQ29uZmlnLmdldCgncmVkaXJlY3RVcmlMb2dvdXQnKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgb2F1dGhDb25maWcoKTogQWJzdHJhY3RDb250cm9sIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5mb3JtLmdldCgnb2F1dGhDb25maWcnKTtcclxuICAgIH1cclxuXHJcbn1cclxuIl19