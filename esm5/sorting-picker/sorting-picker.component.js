/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ViewEncapsulation, Input, EventEmitter, Output } from '@angular/core';
var SortingPickerComponent = /** @class */ (function () {
    function SortingPickerComponent() {
        /**
         * Available sorting options
         */
        this.options = [];
        /**
         * Current sorting direction
         */
        this.ascending = true;
        /**
         * Raised each time sorting key gets changed.
         */
        this.valueChange = new EventEmitter();
        /**
         * Raised each time direction gets changed.
         */
        this.sortingChange = new EventEmitter();
    }
    /**
     * @param {?} event
     * @return {?}
     */
    SortingPickerComponent.prototype.onOptionChanged = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.selected = event.value;
        this.valueChange.emit(this.selected);
    };
    /**
     * @return {?}
     */
    SortingPickerComponent.prototype.toggleSortDirection = /**
     * @return {?}
     */
    function () {
        this.ascending = !this.ascending;
        this.sortingChange.emit(this.ascending);
    };
    SortingPickerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-sorting-picker',
                    template: "<mat-form-field>\r\n    <mat-select [(value)]=\"selected\" (selectionChange)=\"onOptionChanged($event)\">\r\n        <mat-option *ngFor=\"let option of options\" [value]=\"option.key\">\r\n            {{ option.label | translate }}\r\n        </mat-option>\r\n    </mat-select>\r\n</mat-form-field>\r\n\r\n<button *ngIf=\"selected\" mat-icon-button (click)=\"toggleSortDirection()\">\r\n    <mat-icon *ngIf=\"ascending\">arrow_upward</mat-icon>\r\n    <mat-icon *ngIf=\"!ascending\">arrow_downward</mat-icon>\r\n</button>\r\n",
                    encapsulation: ViewEncapsulation.None,
                    host: { class: 'adf-sorting-picker' }
                }] }
    ];
    SortingPickerComponent.propDecorators = {
        options: [{ type: Input }],
        selected: [{ type: Input }],
        ascending: [{ type: Input }],
        valueChange: [{ type: Output }],
        sortingChange: [{ type: Output }]
    };
    return SortingPickerComponent;
}());
export { SortingPickerComponent };
if (false) {
    /**
     * Available sorting options
     * @type {?}
     */
    SortingPickerComponent.prototype.options;
    /**
     * Currently selected option key
     * @type {?}
     */
    SortingPickerComponent.prototype.selected;
    /**
     * Current sorting direction
     * @type {?}
     */
    SortingPickerComponent.prototype.ascending;
    /**
     * Raised each time sorting key gets changed.
     * @type {?}
     */
    SortingPickerComponent.prototype.valueChange;
    /**
     * Raised each time direction gets changed.
     * @type {?}
     */
    SortingPickerComponent.prototype.sortingChange;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic29ydGluZy1waWNrZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsic29ydGluZy1waWNrZXIvc29ydGluZy1waWNrZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsaUJBQWlCLEVBQUUsS0FBSyxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFHMUY7SUFBQTs7OztRQVVJLFlBQU8sR0FBd0MsRUFBRSxDQUFDOzs7O1FBUWxELGNBQVMsR0FBRyxJQUFJLENBQUM7Ozs7UUFJakIsZ0JBQVcsR0FBRyxJQUFJLFlBQVksRUFBVSxDQUFDOzs7O1FBSXpDLGtCQUFhLEdBQUcsSUFBSSxZQUFZLEVBQVcsQ0FBQztJQVdoRCxDQUFDOzs7OztJQVRHLGdEQUFlOzs7O0lBQWYsVUFBZ0IsS0FBc0I7UUFDbEMsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsS0FBSyxDQUFDO1FBQzVCLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztJQUN6QyxDQUFDOzs7O0lBRUQsb0RBQW1COzs7SUFBbkI7UUFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQztRQUNqQyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7SUFDNUMsQ0FBQzs7Z0JBcENKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsb0JBQW9CO29CQUM5Qix5aEJBQThDO29CQUM5QyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTtvQkFDckMsSUFBSSxFQUFFLEVBQUUsS0FBSyxFQUFFLG9CQUFvQixFQUFFO2lCQUN4Qzs7OzBCQUlJLEtBQUs7MkJBSUwsS0FBSzs0QkFJTCxLQUFLOzhCQUlMLE1BQU07Z0NBSU4sTUFBTTs7SUFZWCw2QkFBQztDQUFBLEFBckNELElBcUNDO1NBL0JZLHNCQUFzQjs7Ozs7O0lBRy9CLHlDQUNrRDs7Ozs7SUFHbEQsMENBQ2lCOzs7OztJQUdqQiwyQ0FDaUI7Ozs7O0lBR2pCLDZDQUN5Qzs7Ozs7SUFHekMsK0NBQzRDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgVmlld0VuY2Fwc3VsYXRpb24sIElucHV0LCBFdmVudEVtaXR0ZXIsIE91dHB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXRTZWxlY3RDaGFuZ2UgfSBmcm9tICdAYW5ndWxhci9tYXRlcmlhbCc7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLXNvcnRpbmctcGlja2VyJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9zb3J0aW5nLXBpY2tlci5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lLFxyXG4gICAgaG9zdDogeyBjbGFzczogJ2FkZi1zb3J0aW5nLXBpY2tlcicgfVxyXG59KVxyXG5leHBvcnQgY2xhc3MgU29ydGluZ1BpY2tlckNvbXBvbmVudCB7XHJcblxyXG4gICAgLyoqIEF2YWlsYWJsZSBzb3J0aW5nIG9wdGlvbnMgKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBvcHRpb25zOiBBcnJheTx7a2V5OiBzdHJpbmcsIGxhYmVsOiBzdHJpbmd9PiA9IFtdO1xyXG5cclxuICAgIC8qKiBDdXJyZW50bHkgc2VsZWN0ZWQgb3B0aW9uIGtleSAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHNlbGVjdGVkOiBzdHJpbmc7XHJcblxyXG4gICAgLyoqIEN1cnJlbnQgc29ydGluZyBkaXJlY3Rpb24gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBhc2NlbmRpbmcgPSB0cnVlO1xyXG5cclxuICAgIC8qKiBSYWlzZWQgZWFjaCB0aW1lIHNvcnRpbmcga2V5IGdldHMgY2hhbmdlZC4gKi9cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgdmFsdWVDaGFuZ2UgPSBuZXcgRXZlbnRFbWl0dGVyPHN0cmluZz4oKTtcclxuXHJcbiAgICAvKiogUmFpc2VkIGVhY2ggdGltZSBkaXJlY3Rpb24gZ2V0cyBjaGFuZ2VkLiAqL1xyXG4gICAgQE91dHB1dCgpXHJcbiAgICBzb3J0aW5nQ2hhbmdlID0gbmV3IEV2ZW50RW1pdHRlcjxib29sZWFuPigpO1xyXG5cclxuICAgIG9uT3B0aW9uQ2hhbmdlZChldmVudDogTWF0U2VsZWN0Q2hhbmdlKSB7XHJcbiAgICAgICAgdGhpcy5zZWxlY3RlZCA9IGV2ZW50LnZhbHVlO1xyXG4gICAgICAgIHRoaXMudmFsdWVDaGFuZ2UuZW1pdCh0aGlzLnNlbGVjdGVkKTtcclxuICAgIH1cclxuXHJcbiAgICB0b2dnbGVTb3J0RGlyZWN0aW9uKCkge1xyXG4gICAgICAgIHRoaXMuYXNjZW5kaW5nID0gIXRoaXMuYXNjZW5kaW5nO1xyXG4gICAgICAgIHRoaXMuc29ydGluZ0NoYW5nZS5lbWl0KHRoaXMuYXNjZW5kaW5nKTtcclxuICAgIH1cclxufVxyXG4iXX0=