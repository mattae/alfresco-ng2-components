/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ChangeDetectionStrategy, ViewEncapsulation, Input } from '@angular/core';
var EmptyContentComponent = /** @class */ (function () {
    function EmptyContentComponent() {
        /**
         * Material Icon to use.
         */
        this.icon = 'cake';
        /**
         * String or Resource Key for the title.
         */
        this.title = '';
        /**
         * String or Resource Key for the subtitle.
         */
        this.subtitle = '';
    }
    EmptyContentComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-empty-content',
                    template: "<div class=\"adf-empty-content\">\r\n    <mat-icon class=\"adf-empty-content__icon\">{{ icon }}</mat-icon>\r\n    <div class=\"adf-empty-content__title\">{{ title | translate }}</div>\r\n    <div class=\"adf-empty-content__subtitle\">{{ subtitle | translate }}</div>\r\n    <ng-content></ng-content>\r\n</div>\r\n",
                    changeDetection: ChangeDetectionStrategy.OnPush,
                    encapsulation: ViewEncapsulation.None,
                    host: { class: 'adf-empty-content' },
                    styles: [""]
                }] }
    ];
    EmptyContentComponent.propDecorators = {
        icon: [{ type: Input }],
        title: [{ type: Input }],
        subtitle: [{ type: Input }]
    };
    return EmptyContentComponent;
}());
export { EmptyContentComponent };
if (false) {
    /**
     * Material Icon to use.
     * @type {?}
     */
    EmptyContentComponent.prototype.icon;
    /**
     * String or Resource Key for the title.
     * @type {?}
     */
    EmptyContentComponent.prototype.title;
    /**
     * String or Resource Key for the subtitle.
     * @type {?}
     */
    EmptyContentComponent.prototype.subtitle;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZW1wdHktY29udGVudC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJ0ZW1wbGF0ZXMvZW1wdHktY29udGVudC9lbXB0eS1jb250ZW50LmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsU0FBUyxFQUFFLHVCQUF1QixFQUFFLGlCQUFpQixFQUFFLEtBQUssRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUU3RjtJQUFBOzs7O1FBWUksU0FBSSxHQUFHLE1BQU0sQ0FBQzs7OztRQUlkLFVBQUssR0FBRyxFQUFFLENBQUM7Ozs7UUFJWCxhQUFRLEdBQUcsRUFBRSxDQUFDO0lBRWxCLENBQUM7O2dCQXRCQSxTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLG1CQUFtQjtvQkFDN0IscVVBQTZDO29CQUU3QyxlQUFlLEVBQUUsdUJBQXVCLENBQUMsTUFBTTtvQkFDL0MsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7b0JBQ3JDLElBQUksRUFBRSxFQUFFLEtBQUssRUFBRSxtQkFBbUIsRUFBRTs7aUJBQ3ZDOzs7dUJBSUksS0FBSzt3QkFJTCxLQUFLOzJCQUlMLEtBQUs7O0lBR1YsNEJBQUM7Q0FBQSxBQXRCRCxJQXNCQztTQWRZLHFCQUFxQjs7Ozs7O0lBRzlCLHFDQUNjOzs7OztJQUdkLHNDQUNXOzs7OztJQUdYLHlDQUNjIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IENvbXBvbmVudCwgQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3ksIFZpZXdFbmNhcHN1bGF0aW9uLCBJbnB1dCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1lbXB0eS1jb250ZW50JyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9lbXB0eS1jb250ZW50LmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2VtcHR5LWNvbnRlbnQuY29tcG9uZW50LnNjc3MnXSxcclxuICAgIGNoYW5nZURldGVjdGlvbjogQ2hhbmdlRGV0ZWN0aW9uU3RyYXRlZ3kuT25QdXNoLFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZSxcclxuICAgIGhvc3Q6IHsgY2xhc3M6ICdhZGYtZW1wdHktY29udGVudCcgfVxyXG59KVxyXG5leHBvcnQgY2xhc3MgRW1wdHlDb250ZW50Q29tcG9uZW50IHtcclxuXHJcbiAgICAvKiogTWF0ZXJpYWwgSWNvbiB0byB1c2UuICovXHJcbiAgICBASW5wdXQoKVxyXG4gICAgaWNvbiA9ICdjYWtlJztcclxuXHJcbiAgICAvKiogU3RyaW5nIG9yIFJlc291cmNlIEtleSBmb3IgdGhlIHRpdGxlLiAqL1xyXG4gICAgQElucHV0KClcclxuICAgIHRpdGxlID0gJyc7XHJcblxyXG4gICAgLyoqIFN0cmluZyBvciBSZXNvdXJjZSBLZXkgZm9yIHRoZSBzdWJ0aXRsZS4gKi9cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzdWJ0aXRsZSA9ICcnO1xyXG5cclxufVxyXG4iXX0=