/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var _this = this;
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { TestBed } from '@angular/core/testing';
/**
 * @record
 */
function DoneFn() { }
if (false) {
    /** @type {?} */
    DoneFn.prototype.fail;
    /* Skipping unhandled member: (): void;*/
}
/** @type {?} */
var resetTestingModule = TestBed.resetTestingModule;
/** @type {?} */
var preventAngularFromResetting = (/**
 * @return {?}
 */
function () { return (TestBed.resetTestingModule = (/**
 * @return {?}
 */
function () { return TestBed; })); });
var ɵ0 = preventAngularFromResetting;
/** @type {?} */
var allowAngularToReset = (/**
 * @return {?}
 */
function () { return (TestBed.resetTestingModule = resetTestingModule); });
var ɵ1 = allowAngularToReset;
/** @type {?} */
export var setupTestBed = (/**
 * @param {?} moduleDef
 * @return {?}
 */
function (moduleDef) {
    beforeAll((/**
     * @param {?} done
     * @return {?}
     */
    function (done) {
        return ((/**
         * @return {?}
         */
        function () { return tslib_1.__awaiter(_this, void 0, void 0, function () {
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        localStorage.clear();
                        sessionStorage.clear();
                        resetTestingModule();
                        preventAngularFromResetting();
                        TestBed.configureTestingModule(moduleDef);
                        return [4 /*yield*/, TestBed.compileComponents()];
                    case 1:
                        _a.sent();
                        // prevent Angular from resetting testing module
                        TestBed.resetTestingModule = (/**
                         * @return {?}
                         */
                        function () { return TestBed; });
                        return [2 /*return*/];
                }
            });
        }); }))()
            .then(done)
            .catch(done.fail);
    }));
    afterAll((/**
     * @return {?}
     */
    function () { return allowAngularToReset(); }));
});
export { ɵ0, ɵ1 };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2V0dXBUZXN0QmVkLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsidGVzdGluZy9zZXR1cFRlc3RCZWQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7OztBQWlCQSxpQkFpQ0E7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWpDQSxPQUFPLEVBQUUsT0FBTyxFQUFzQixNQUFNLHVCQUF1QixDQUFDOzs7O0FBRXBFLHFCQUdDOzs7SUFERyxzQkFBeUM7Ozs7SUFNdkMsa0JBQWtCLEdBQUcsT0FBTyxDQUFDLGtCQUFrQjs7SUFDL0MsMkJBQTJCOzs7QUFBRyxjQUFNLE9BQUEsQ0FBQyxPQUFPLENBQUMsa0JBQWtCOzs7QUFBRyxjQUFNLE9BQUEsT0FBTyxFQUFQLENBQU8sQ0FBQSxDQUFDLEVBQTVDLENBQTRDLENBQUE7OztJQUNoRixtQkFBbUI7OztBQUFHLGNBQU0sT0FBQSxDQUFDLE9BQU8sQ0FBQyxrQkFBa0IsR0FBRyxrQkFBa0IsQ0FBQyxFQUFqRCxDQUFpRCxDQUFBOzs7QUFFbkYsTUFBTSxLQUFPLFlBQVk7Ozs7QUFBRyxVQUFDLFNBQTZCO0lBQ3RELFNBQVM7Ozs7SUFBQyxVQUFDLElBQUk7UUFDWCxPQUFBOzs7UUFBQzs7Ozt3QkFDRyxZQUFZLENBQUMsS0FBSyxFQUFFLENBQUM7d0JBQ3JCLGNBQWMsQ0FBQyxLQUFLLEVBQUUsQ0FBQzt3QkFDdkIsa0JBQWtCLEVBQUUsQ0FBQzt3QkFDckIsMkJBQTJCLEVBQUUsQ0FBQzt3QkFDOUIsT0FBTyxDQUFDLHNCQUFzQixDQUFDLFNBQVMsQ0FBQyxDQUFDO3dCQUMxQyxxQkFBTSxPQUFPLENBQUMsaUJBQWlCLEVBQUUsRUFBQTs7d0JBQWpDLFNBQWlDLENBQUM7d0JBRWxDLGdEQUFnRDt3QkFDaEQsT0FBTyxDQUFDLGtCQUFrQjs7O3dCQUFHLGNBQU0sT0FBQSxPQUFPLEVBQVAsQ0FBTyxDQUFBLENBQUM7Ozs7YUFDOUMsRUFBQyxFQUFFO2FBQ0MsSUFBSSxDQUFDLElBQUksQ0FBQzthQUNWLEtBQUssQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDO0lBWnJCLENBWXFCLEVBQ3hCLENBQUM7SUFFRixRQUFROzs7SUFBQyxjQUFNLE9BQUEsbUJBQW1CLEVBQUUsRUFBckIsQ0FBcUIsRUFBQyxDQUFDO0FBQzFDLENBQUMsQ0FBQSIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBUZXN0QmVkLCBUZXN0TW9kdWxlTWV0YWRhdGEgfSBmcm9tICdAYW5ndWxhci9jb3JlL3Rlc3RpbmcnO1xyXG5cclxuaW50ZXJmYWNlIERvbmVGbiBleHRlbmRzIEZ1bmN0aW9uIHtcclxuICAgICgpOiB2b2lkO1xyXG4gICAgZmFpbDogKG1lc3NhZ2U/OiBFcnJvciB8IHN0cmluZykgPT4gdm9pZDtcclxufVxyXG5cclxuZGVjbGFyZSBmdW5jdGlvbiBiZWZvcmVBbGwoYWN0aW9uOiAoZG9uZTogRG9uZUZuKSA9PiB2b2lkLCB0aW1lb3V0PzogbnVtYmVyKTogdm9pZDtcclxuZGVjbGFyZSBmdW5jdGlvbiBhZnRlckFsbChhY3Rpb246IChkb25lOiBEb25lRm4pID0+IHZvaWQsIHRpbWVvdXQ/OiBudW1iZXIpOiB2b2lkO1xyXG5cclxuY29uc3QgcmVzZXRUZXN0aW5nTW9kdWxlID0gVGVzdEJlZC5yZXNldFRlc3RpbmdNb2R1bGU7XHJcbmNvbnN0IHByZXZlbnRBbmd1bGFyRnJvbVJlc2V0dGluZyA9ICgpID0+IChUZXN0QmVkLnJlc2V0VGVzdGluZ01vZHVsZSA9ICgpID0+IFRlc3RCZWQpO1xyXG5jb25zdCBhbGxvd0FuZ3VsYXJUb1Jlc2V0ID0gKCkgPT4gKFRlc3RCZWQucmVzZXRUZXN0aW5nTW9kdWxlID0gcmVzZXRUZXN0aW5nTW9kdWxlKTtcclxuXHJcbmV4cG9ydCBjb25zdCBzZXR1cFRlc3RCZWQgPSAobW9kdWxlRGVmOiBUZXN0TW9kdWxlTWV0YWRhdGEpID0+IHtcclxuICAgIGJlZm9yZUFsbCgoZG9uZSkgPT5cclxuICAgICAgICAoYXN5bmMgKCkgPT4ge1xyXG4gICAgICAgICAgICBsb2NhbFN0b3JhZ2UuY2xlYXIoKTtcclxuICAgICAgICAgICAgc2Vzc2lvblN0b3JhZ2UuY2xlYXIoKTtcclxuICAgICAgICAgICAgcmVzZXRUZXN0aW5nTW9kdWxlKCk7XHJcbiAgICAgICAgICAgIHByZXZlbnRBbmd1bGFyRnJvbVJlc2V0dGluZygpO1xyXG4gICAgICAgICAgICBUZXN0QmVkLmNvbmZpZ3VyZVRlc3RpbmdNb2R1bGUobW9kdWxlRGVmKTtcclxuICAgICAgICAgICAgYXdhaXQgVGVzdEJlZC5jb21waWxlQ29tcG9uZW50cygpO1xyXG5cclxuICAgICAgICAgICAgLy8gcHJldmVudCBBbmd1bGFyIGZyb20gcmVzZXR0aW5nIHRlc3RpbmcgbW9kdWxlXHJcbiAgICAgICAgICAgIFRlc3RCZWQucmVzZXRUZXN0aW5nTW9kdWxlID0gKCkgPT4gVGVzdEJlZDtcclxuICAgICAgICB9KSgpXHJcbiAgICAgICAgICAgIC50aGVuKGRvbmUpXHJcbiAgICAgICAgICAgIC5jYXRjaChkb25lLmZhaWwpXHJcbiAgICApO1xyXG5cclxuICAgIGFmdGVyQWxsKCgpID0+IGFsbG93QW5ndWxhclRvUmVzZXQoKSk7XHJcbn07XHJcbiJdfQ==