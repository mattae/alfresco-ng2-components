/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var BpmUserModel = /** @class */ (function () {
    function BpmUserModel(input) {
        if (input) {
            this.apps = input.apps;
            this.capabilities = input.capabilities;
            this.company = input.company;
            this.created = input.created;
            this.email = input.email;
            this.externalId = input.externalId;
            this.firstName = input.firstName;
            this.lastName = input.lastName;
            this.fullname = input.fullname;
            this.groups = input.groups;
            this.id = input.id;
            this.lastUpdate = input.lastUpdate;
            this.latestSyncTimeStamp = input.latestSyncTimeStamp;
            this.password = input.password;
            this.pictureId = input.pictureId;
            this.status = input.status;
            this.tenantId = input.tenantId;
            this.tenantName = input.tenantName;
            this.tenantPictureId = input.tenantPictureId;
            this.type = input.type;
        }
    }
    return BpmUserModel;
}());
export { BpmUserModel };
if (false) {
    /** @type {?} */
    BpmUserModel.prototype.apps;
    /** @type {?} */
    BpmUserModel.prototype.capabilities;
    /** @type {?} */
    BpmUserModel.prototype.company;
    /** @type {?} */
    BpmUserModel.prototype.created;
    /** @type {?} */
    BpmUserModel.prototype.email;
    /** @type {?} */
    BpmUserModel.prototype.externalId;
    /** @type {?} */
    BpmUserModel.prototype.firstName;
    /** @type {?} */
    BpmUserModel.prototype.lastName;
    /** @type {?} */
    BpmUserModel.prototype.fullname;
    /** @type {?} */
    BpmUserModel.prototype.groups;
    /** @type {?} */
    BpmUserModel.prototype.id;
    /** @type {?} */
    BpmUserModel.prototype.lastUpdate;
    /** @type {?} */
    BpmUserModel.prototype.latestSyncTimeStamp;
    /** @type {?} */
    BpmUserModel.prototype.password;
    /** @type {?} */
    BpmUserModel.prototype.pictureId;
    /** @type {?} */
    BpmUserModel.prototype.status;
    /** @type {?} */
    BpmUserModel.prototype.tenantId;
    /** @type {?} */
    BpmUserModel.prototype.tenantName;
    /** @type {?} */
    BpmUserModel.prototype.tenantPictureId;
    /** @type {?} */
    BpmUserModel.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnBtLXVzZXIubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJ1c2VyaW5mby9tb2RlbHMvYnBtLXVzZXIubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQkE7SUFzQkksc0JBQVksS0FBVztRQUNuQixJQUFJLEtBQUssRUFBRTtZQUNQLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDLElBQUksQ0FBQztZQUN2QixJQUFJLENBQUMsWUFBWSxHQUFHLEtBQUssQ0FBQyxZQUFZLENBQUM7WUFDdkMsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsT0FBTyxDQUFDO1lBQzdCLElBQUksQ0FBQyxPQUFPLEdBQUcsS0FBSyxDQUFDLE9BQU8sQ0FBQztZQUM3QixJQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7WUFDekIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDO1lBQ25DLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQztZQUNqQyxJQUFJLENBQUMsUUFBUSxHQUFHLEtBQUssQ0FBQyxRQUFRLENBQUM7WUFDL0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQy9CLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQztZQUMzQixJQUFJLENBQUMsRUFBRSxHQUFHLEtBQUssQ0FBQyxFQUFFLENBQUM7WUFDbkIsSUFBSSxDQUFDLFVBQVUsR0FBRyxLQUFLLENBQUMsVUFBVSxDQUFDO1lBQ25DLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxLQUFLLENBQUMsbUJBQW1CLENBQUM7WUFDckQsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQy9CLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDLFNBQVMsQ0FBQztZQUNqQyxJQUFJLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUM7WUFDM0IsSUFBSSxDQUFDLFFBQVEsR0FBRyxLQUFLLENBQUMsUUFBUSxDQUFDO1lBQy9CLElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQztZQUNuQyxJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQyxlQUFlLENBQUM7WUFDN0MsSUFBSSxDQUFDLElBQUksR0FBRyxLQUFLLENBQUMsSUFBSSxDQUFDO1NBQzFCO0lBQ0wsQ0FBQztJQUNMLG1CQUFDO0FBQUQsQ0FBQyxBQTlDRCxJQThDQzs7OztJQTdDRyw0QkFBVTs7SUFDVixvQ0FBdUI7O0lBQ3ZCLCtCQUFnQjs7SUFDaEIsK0JBQWM7O0lBQ2QsNkJBQWM7O0lBQ2Qsa0NBQW1COztJQUNuQixpQ0FBa0I7O0lBQ2xCLGdDQUFpQjs7SUFDakIsZ0NBQWlCOztJQUNqQiw4QkFBWTs7SUFDWiwwQkFBVzs7SUFDWCxrQ0FBaUI7O0lBQ2pCLDJDQUEwQjs7SUFDMUIsZ0NBQWlCOztJQUNqQixpQ0FBa0I7O0lBQ2xCLDhCQUFlOztJQUNmLGdDQUFpQjs7SUFDakIsa0NBQW1COztJQUNuQix1Q0FBd0I7O0lBQ3hCLDRCQUFhIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IFVzZXJSZXByZXNlbnRhdGlvbiB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5cclxuZXhwb3J0IGNsYXNzIEJwbVVzZXJNb2RlbCBpbXBsZW1lbnRzIFVzZXJSZXByZXNlbnRhdGlvbiB7XHJcbiAgICBhcHBzOiBhbnk7XHJcbiAgICBjYXBhYmlsaXRpZXM6IHN0cmluZ1tdO1xyXG4gICAgY29tcGFueTogc3RyaW5nO1xyXG4gICAgY3JlYXRlZDogRGF0ZTtcclxuICAgIGVtYWlsOiBzdHJpbmc7XHJcbiAgICBleHRlcm5hbElkOiBzdHJpbmc7XHJcbiAgICBmaXJzdE5hbWU6IHN0cmluZztcclxuICAgIGxhc3ROYW1lOiBzdHJpbmc7XHJcbiAgICBmdWxsbmFtZTogc3RyaW5nO1xyXG4gICAgZ3JvdXBzOiBhbnk7XHJcbiAgICBpZDogbnVtYmVyO1xyXG4gICAgbGFzdFVwZGF0ZTogRGF0ZTtcclxuICAgIGxhdGVzdFN5bmNUaW1lU3RhbXA6IERhdGU7XHJcbiAgICBwYXNzd29yZDogc3RyaW5nO1xyXG4gICAgcGljdHVyZUlkOiBudW1iZXI7XHJcbiAgICBzdGF0dXM6IHN0cmluZztcclxuICAgIHRlbmFudElkOiBudW1iZXI7XHJcbiAgICB0ZW5hbnROYW1lOiBzdHJpbmc7XHJcbiAgICB0ZW5hbnRQaWN0dXJlSWQ6IG51bWJlcjtcclxuICAgIHR5cGU6IHN0cmluZztcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihpbnB1dD86IGFueSkge1xyXG4gICAgICAgIGlmIChpbnB1dCkge1xyXG4gICAgICAgICAgICB0aGlzLmFwcHMgPSBpbnB1dC5hcHBzO1xyXG4gICAgICAgICAgICB0aGlzLmNhcGFiaWxpdGllcyA9IGlucHV0LmNhcGFiaWxpdGllcztcclxuICAgICAgICAgICAgdGhpcy5jb21wYW55ID0gaW5wdXQuY29tcGFueTtcclxuICAgICAgICAgICAgdGhpcy5jcmVhdGVkID0gaW5wdXQuY3JlYXRlZDtcclxuICAgICAgICAgICAgdGhpcy5lbWFpbCA9IGlucHV0LmVtYWlsO1xyXG4gICAgICAgICAgICB0aGlzLmV4dGVybmFsSWQgPSBpbnB1dC5leHRlcm5hbElkO1xyXG4gICAgICAgICAgICB0aGlzLmZpcnN0TmFtZSA9IGlucHV0LmZpcnN0TmFtZTtcclxuICAgICAgICAgICAgdGhpcy5sYXN0TmFtZSA9IGlucHV0Lmxhc3ROYW1lO1xyXG4gICAgICAgICAgICB0aGlzLmZ1bGxuYW1lID0gaW5wdXQuZnVsbG5hbWU7XHJcbiAgICAgICAgICAgIHRoaXMuZ3JvdXBzID0gaW5wdXQuZ3JvdXBzO1xyXG4gICAgICAgICAgICB0aGlzLmlkID0gaW5wdXQuaWQ7XHJcbiAgICAgICAgICAgIHRoaXMubGFzdFVwZGF0ZSA9IGlucHV0Lmxhc3RVcGRhdGU7XHJcbiAgICAgICAgICAgIHRoaXMubGF0ZXN0U3luY1RpbWVTdGFtcCA9IGlucHV0LmxhdGVzdFN5bmNUaW1lU3RhbXA7XHJcbiAgICAgICAgICAgIHRoaXMucGFzc3dvcmQgPSBpbnB1dC5wYXNzd29yZDtcclxuICAgICAgICAgICAgdGhpcy5waWN0dXJlSWQgPSBpbnB1dC5waWN0dXJlSWQ7XHJcbiAgICAgICAgICAgIHRoaXMuc3RhdHVzID0gaW5wdXQuc3RhdHVzO1xyXG4gICAgICAgICAgICB0aGlzLnRlbmFudElkID0gaW5wdXQudGVuYW50SWQ7XHJcbiAgICAgICAgICAgIHRoaXMudGVuYW50TmFtZSA9IGlucHV0LnRlbmFudE5hbWU7XHJcbiAgICAgICAgICAgIHRoaXMudGVuYW50UGljdHVyZUlkID0gaW5wdXQudGVuYW50UGljdHVyZUlkO1xyXG4gICAgICAgICAgICB0aGlzLnR5cGUgPSBpbnB1dC50eXBlO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=