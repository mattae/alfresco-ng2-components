/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var EcmUserModel = /** @class */ (function () {
    function EcmUserModel(obj) {
        this.id = obj && obj.id || null;
        this.firstName = obj && obj.firstName;
        this.lastName = obj && obj.lastName;
        this.description = obj && obj.description || null;
        this.avatarId = obj && obj.avatarId || null;
        this.email = obj && obj.email || null;
        this.skypeId = obj && obj.skypeId;
        this.googleId = obj && obj.googleId;
        this.instantMessageId = obj && obj.instantMessageId;
        this.jobTitle = obj && obj.jobTitle || null;
        this.location = obj && obj.location || null;
        this.company = obj && obj.company;
        this.mobile = obj && obj.mobile;
        this.telephone = obj && obj.telephone;
        this.statusUpdatedAt = obj && obj.statusUpdatedAt;
        this.userStatus = obj && obj.userStatus;
        this.enabled = obj && obj.enabled;
        this.emailNotificationsEnabled = obj && obj.emailNotificationsEnabled;
    }
    return EcmUserModel;
}());
export { EcmUserModel };
if (false) {
    /** @type {?} */
    EcmUserModel.prototype.id;
    /** @type {?} */
    EcmUserModel.prototype.firstName;
    /** @type {?} */
    EcmUserModel.prototype.lastName;
    /** @type {?} */
    EcmUserModel.prototype.description;
    /** @type {?} */
    EcmUserModel.prototype.avatarId;
    /** @type {?} */
    EcmUserModel.prototype.email;
    /** @type {?} */
    EcmUserModel.prototype.skypeId;
    /** @type {?} */
    EcmUserModel.prototype.googleId;
    /** @type {?} */
    EcmUserModel.prototype.instantMessageId;
    /** @type {?} */
    EcmUserModel.prototype.jobTitle;
    /** @type {?} */
    EcmUserModel.prototype.location;
    /** @type {?} */
    EcmUserModel.prototype.company;
    /** @type {?} */
    EcmUserModel.prototype.mobile;
    /** @type {?} */
    EcmUserModel.prototype.telephone;
    /** @type {?} */
    EcmUserModel.prototype.statusUpdatedAt;
    /** @type {?} */
    EcmUserModel.prototype.userStatus;
    /** @type {?} */
    EcmUserModel.prototype.enabled;
    /** @type {?} */
    EcmUserModel.prototype.emailNotificationsEnabled;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWNtLXVzZXIubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJ1c2VyaW5mby9tb2RlbHMvZWNtLXVzZXIubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFvQkE7SUFvQkksc0JBQVksR0FBUztRQUNqQixJQUFJLENBQUMsRUFBRSxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsRUFBRSxJQUFJLElBQUksQ0FBQztRQUNoQyxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsU0FBUyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxRQUFRLENBQUM7UUFDcEMsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUM7UUFDbEQsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLFFBQVEsSUFBSSxJQUFJLENBQUM7UUFDNUMsSUFBSSxDQUFDLEtBQUssR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLEtBQUssSUFBSSxJQUFJLENBQUM7UUFDdEMsSUFBSSxDQUFDLE9BQU8sR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLE9BQU8sQ0FBQztRQUNsQyxJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsUUFBUSxDQUFDO1FBQ3BDLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLGdCQUFnQixDQUFDO1FBQ3BELElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDO1FBQzVDLElBQUksQ0FBQyxRQUFRLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxRQUFRLElBQUksSUFBSSxDQUFDO1FBQzVDLElBQUksQ0FBQyxPQUFPLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxPQUFPLENBQUM7UUFDbEMsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLE1BQU0sQ0FBQztRQUNoQyxJQUFJLENBQUMsU0FBUyxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsU0FBUyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxlQUFlLEdBQUcsR0FBRyxJQUFJLEdBQUcsQ0FBQyxlQUFlLENBQUM7UUFDbEQsSUFBSSxDQUFDLFVBQVUsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLFVBQVUsQ0FBQztRQUN4QyxJQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsSUFBSSxHQUFHLENBQUMsT0FBTyxDQUFDO1FBQ2xDLElBQUksQ0FBQyx5QkFBeUIsR0FBRyxHQUFHLElBQUksR0FBRyxDQUFDLHlCQUF5QixDQUFDO0lBQzFFLENBQUM7SUFDTCxtQkFBQztBQUFELENBQUMsQUF4Q0QsSUF3Q0M7Ozs7SUF2Q0csMEJBQVc7O0lBQ1gsaUNBQWtCOztJQUNsQixnQ0FBaUI7O0lBQ2pCLG1DQUFvQjs7SUFDcEIsZ0NBQWlCOztJQUNqQiw2QkFBYzs7SUFDZCwrQkFBZ0I7O0lBQ2hCLGdDQUFpQjs7SUFDakIsd0NBQXlCOztJQUN6QixnQ0FBaUI7O0lBQ2pCLGdDQUFpQjs7SUFDakIsK0JBQXlCOztJQUN6Qiw4QkFBZTs7SUFDZixpQ0FBa0I7O0lBQ2xCLHVDQUFzQjs7SUFDdEIsa0NBQW1COztJQUNuQiwrQkFBaUI7O0lBQ2pCLGlEQUFtQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBQZXJzb24gfSBmcm9tICdAYWxmcmVzY28vanMtYXBpJztcclxuaW1wb3J0IHsgRWNtQ29tcGFueU1vZGVsIH0gZnJvbSAnLi4vLi4vbW9kZWxzL2VjbS1jb21wYW55Lm1vZGVsJztcclxuXHJcbmV4cG9ydCBjbGFzcyBFY21Vc2VyTW9kZWwgaW1wbGVtZW50cyBQZXJzb24ge1xyXG4gICAgaWQ6IHN0cmluZztcclxuICAgIGZpcnN0TmFtZTogc3RyaW5nO1xyXG4gICAgbGFzdE5hbWU6IHN0cmluZztcclxuICAgIGRlc2NyaXB0aW9uOiBzdHJpbmc7XHJcbiAgICBhdmF0YXJJZDogc3RyaW5nO1xyXG4gICAgZW1haWw6IHN0cmluZztcclxuICAgIHNreXBlSWQ6IHN0cmluZztcclxuICAgIGdvb2dsZUlkOiBzdHJpbmc7XHJcbiAgICBpbnN0YW50TWVzc2FnZUlkOiBzdHJpbmc7XHJcbiAgICBqb2JUaXRsZTogc3RyaW5nO1xyXG4gICAgbG9jYXRpb246IHN0cmluZztcclxuICAgIGNvbXBhbnk6IEVjbUNvbXBhbnlNb2RlbDtcclxuICAgIG1vYmlsZTogc3RyaW5nO1xyXG4gICAgdGVsZXBob25lOiBzdHJpbmc7XHJcbiAgICBzdGF0dXNVcGRhdGVkQXQ6IERhdGU7XHJcbiAgICB1c2VyU3RhdHVzOiBzdHJpbmc7XHJcbiAgICBlbmFibGVkOiBib29sZWFuO1xyXG4gICAgZW1haWxOb3RpZmljYXRpb25zRW5hYmxlZDogYm9vbGVhbjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihvYmo/OiBhbnkpIHtcclxuICAgICAgICB0aGlzLmlkID0gb2JqICYmIG9iai5pZCB8fCBudWxsO1xyXG4gICAgICAgIHRoaXMuZmlyc3ROYW1lID0gb2JqICYmIG9iai5maXJzdE5hbWU7XHJcbiAgICAgICAgdGhpcy5sYXN0TmFtZSA9IG9iaiAmJiBvYmoubGFzdE5hbWU7XHJcbiAgICAgICAgdGhpcy5kZXNjcmlwdGlvbiA9IG9iaiAmJiBvYmouZGVzY3JpcHRpb24gfHwgbnVsbDtcclxuICAgICAgICB0aGlzLmF2YXRhcklkID0gb2JqICYmIG9iai5hdmF0YXJJZCB8fCBudWxsO1xyXG4gICAgICAgIHRoaXMuZW1haWwgPSBvYmogJiYgb2JqLmVtYWlsIHx8IG51bGw7XHJcbiAgICAgICAgdGhpcy5za3lwZUlkID0gb2JqICYmIG9iai5za3lwZUlkO1xyXG4gICAgICAgIHRoaXMuZ29vZ2xlSWQgPSBvYmogJiYgb2JqLmdvb2dsZUlkO1xyXG4gICAgICAgIHRoaXMuaW5zdGFudE1lc3NhZ2VJZCA9IG9iaiAmJiBvYmouaW5zdGFudE1lc3NhZ2VJZDtcclxuICAgICAgICB0aGlzLmpvYlRpdGxlID0gb2JqICYmIG9iai5qb2JUaXRsZSB8fCBudWxsO1xyXG4gICAgICAgIHRoaXMubG9jYXRpb24gPSBvYmogJiYgb2JqLmxvY2F0aW9uIHx8IG51bGw7XHJcbiAgICAgICAgdGhpcy5jb21wYW55ID0gb2JqICYmIG9iai5jb21wYW55O1xyXG4gICAgICAgIHRoaXMubW9iaWxlID0gb2JqICYmIG9iai5tb2JpbGU7XHJcbiAgICAgICAgdGhpcy50ZWxlcGhvbmUgPSBvYmogJiYgb2JqLnRlbGVwaG9uZTtcclxuICAgICAgICB0aGlzLnN0YXR1c1VwZGF0ZWRBdCA9IG9iaiAmJiBvYmouc3RhdHVzVXBkYXRlZEF0O1xyXG4gICAgICAgIHRoaXMudXNlclN0YXR1cyA9IG9iaiAmJiBvYmoudXNlclN0YXR1cztcclxuICAgICAgICB0aGlzLmVuYWJsZWQgPSBvYmogJiYgb2JqLmVuYWJsZWQ7XHJcbiAgICAgICAgdGhpcy5lbWFpbE5vdGlmaWNhdGlvbnNFbmFibGVkID0gb2JqICYmIG9iai5lbWFpbE5vdGlmaWNhdGlvbnNFbmFibGVkO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==