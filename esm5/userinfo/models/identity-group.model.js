/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var /*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
IdentityGroupModel = /** @class */ (function () {
    function IdentityGroupModel(obj) {
        if (obj) {
            this.id = obj.id || null;
            this.name = obj.name || null;
            this.path = obj.path || null;
            this.realmRoles = obj.realmRoles || null;
            this.clientRoles = obj.clientRoles || null;
            this.access = obj.access || null;
            this.attributes = obj.attributes || null;
        }
    }
    return IdentityGroupModel;
}());
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export { IdentityGroupModel };
if (false) {
    /** @type {?} */
    IdentityGroupModel.prototype.id;
    /** @type {?} */
    IdentityGroupModel.prototype.name;
    /** @type {?} */
    IdentityGroupModel.prototype.path;
    /** @type {?} */
    IdentityGroupModel.prototype.realmRoles;
    /** @type {?} */
    IdentityGroupModel.prototype.clientRoles;
    /** @type {?} */
    IdentityGroupModel.prototype.access;
    /** @type {?} */
    IdentityGroupModel.prototype.attributes;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWRlbnRpdHktZ3JvdXAubW9kZWwuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJ1c2VyaW5mby9tb2RlbHMvaWRlbnRpdHktZ3JvdXAubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFnQkE7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBVUksNEJBQVksR0FBUztRQUNqQixJQUFJLEdBQUcsRUFBRTtZQUNMLElBQUksQ0FBQyxFQUFFLEdBQUcsR0FBRyxDQUFDLEVBQUUsSUFBSSxJQUFJLENBQUM7WUFDekIsSUFBSSxDQUFDLElBQUksR0FBRyxHQUFHLENBQUMsSUFBSSxJQUFJLElBQUksQ0FBQztZQUM3QixJQUFJLENBQUMsSUFBSSxHQUFHLEdBQUcsQ0FBQyxJQUFJLElBQUksSUFBSSxDQUFDO1lBQzdCLElBQUksQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUM7WUFDekMsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQztZQUMzQyxJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDO1lBQ2pDLElBQUksQ0FBQyxVQUFVLEdBQUcsR0FBRyxDQUFDLFVBQVUsSUFBSSxJQUFJLENBQUM7U0FDNUM7SUFDTCxDQUFDO0lBQ0wseUJBQUM7QUFBRCxDQUFDLEFBckJELElBcUJDOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztJQW5CRyxnQ0FBVzs7SUFDWCxrQ0FBYTs7SUFDYixrQ0FBYTs7SUFDYix3Q0FBcUI7O0lBQ3JCLHlDQUFpQjs7SUFDakIsb0NBQVk7O0lBQ1osd0NBQWdCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuZXhwb3J0IGNsYXNzIElkZW50aXR5R3JvdXBNb2RlbCB7XHJcblxyXG4gICAgaWQ6IHN0cmluZztcclxuICAgIG5hbWU6IHN0cmluZztcclxuICAgIHBhdGg6IHN0cmluZztcclxuICAgIHJlYWxtUm9sZXM6IHN0cmluZ1tdO1xyXG4gICAgY2xpZW50Um9sZXM6IGFueTtcclxuICAgIGFjY2VzczogYW55O1xyXG4gICAgYXR0cmlidXRlczogYW55O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKG9iaj86IGFueSkge1xyXG4gICAgICAgIGlmIChvYmopIHtcclxuICAgICAgICAgICAgdGhpcy5pZCA9IG9iai5pZCB8fCBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLm5hbWUgPSBvYmoubmFtZSB8fCBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLnBhdGggPSBvYmoucGF0aCB8fCBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLnJlYWxtUm9sZXMgPSBvYmoucmVhbG1Sb2xlcyB8fCBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLmNsaWVudFJvbGVzID0gb2JqLmNsaWVudFJvbGVzIHx8IG51bGw7XHJcbiAgICAgICAgICAgIHRoaXMuYWNjZXNzID0gb2JqLmFjY2VzcyB8fCBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLmF0dHJpYnV0ZXMgPSBvYmouYXR0cmlidXRlcyB8fCBudWxsO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=