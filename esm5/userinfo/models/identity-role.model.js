/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var /*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
IdentityRoleModel = /** @class */ (function () {
    function IdentityRoleModel(obj) {
        if (obj) {
            this.id = obj.id || null;
            this.name = obj.name || null;
            this.description = obj.description || null;
            this.clientRole = obj.clientRole || null;
            this.composite = obj.composite || null;
            this.containerId = obj.containerId || null;
            this.scopeParamRequired = obj.scopeParamRequired || null;
        }
    }
    return IdentityRoleModel;
}());
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export { IdentityRoleModel };
if (false) {
    /** @type {?} */
    IdentityRoleModel.prototype.id;
    /** @type {?} */
    IdentityRoleModel.prototype.name;
    /** @type {?} */
    IdentityRoleModel.prototype.description;
    /** @type {?} */
    IdentityRoleModel.prototype.clientRole;
    /** @type {?} */
    IdentityRoleModel.prototype.composite;
    /** @type {?} */
    IdentityRoleModel.prototype.containerId;
    /** @type {?} */
    IdentityRoleModel.prototype.scopeParamRequired;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWRlbnRpdHktcm9sZS5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInVzZXJpbmZvL21vZGVscy9pZGVudGl0eS1yb2xlLm1vZGVsLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBZ0JBOzs7Ozs7Ozs7Ozs7Ozs7OztJQVNJLDJCQUFZLEdBQVM7UUFDakIsSUFBSSxHQUFHLEVBQUU7WUFDTCxJQUFJLENBQUMsRUFBRSxHQUFHLEdBQUcsQ0FBQyxFQUFFLElBQUksSUFBSSxDQUFDO1lBQ3pCLElBQUksQ0FBQyxJQUFJLEdBQUcsR0FBRyxDQUFDLElBQUksSUFBSSxJQUFJLENBQUM7WUFDN0IsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQztZQUMzQyxJQUFJLENBQUMsVUFBVSxHQUFHLEdBQUcsQ0FBQyxVQUFVLElBQUksSUFBSSxDQUFDO1lBQ3pDLElBQUksQ0FBQyxTQUFTLEdBQUcsR0FBRyxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUM7WUFDdkMsSUFBSSxDQUFDLFdBQVcsR0FBRyxHQUFHLENBQUMsV0FBVyxJQUFJLElBQUksQ0FBQztZQUMzQyxJQUFJLENBQUMsa0JBQWtCLEdBQUcsR0FBRyxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQztTQUM1RDtJQUNMLENBQUM7SUFDTCx3QkFBQztBQUFELENBQUMsQUFwQkQsSUFvQkM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0lBbkJHLCtCQUFXOztJQUNYLGlDQUFhOztJQUNiLHdDQUFxQjs7SUFDckIsdUNBQXFCOztJQUNyQixzQ0FBb0I7O0lBQ3BCLHdDQUFxQjs7SUFDckIsK0NBQTZCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuZXhwb3J0IGNsYXNzIElkZW50aXR5Um9sZU1vZGVsIHtcclxuICAgIGlkOiBzdHJpbmc7XHJcbiAgICBuYW1lOiBzdHJpbmc7XHJcbiAgICBkZXNjcmlwdGlvbj86IHN0cmluZztcclxuICAgIGNsaWVudFJvbGU/OiBib29sZWFuO1xyXG4gICAgY29tcG9zaXRlPzogYm9vbGVhbjtcclxuICAgIGNvbnRhaW5lcklkPzogc3RyaW5nO1xyXG4gICAgc2NvcGVQYXJhbVJlcXVpcmVkPzogYm9vbGVhbjtcclxuXHJcbiAgICBjb25zdHJ1Y3RvcihvYmo/OiBhbnkpIHtcclxuICAgICAgICBpZiAob2JqKSB7XHJcbiAgICAgICAgICAgIHRoaXMuaWQgPSBvYmouaWQgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5uYW1lID0gb2JqLm5hbWUgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5kZXNjcmlwdGlvbiA9IG9iai5kZXNjcmlwdGlvbiB8fCBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLmNsaWVudFJvbGUgPSBvYmouY2xpZW50Um9sZSB8fCBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLmNvbXBvc2l0ZSA9IG9iai5jb21wb3NpdGUgfHwgbnVsbDtcclxuICAgICAgICAgICAgdGhpcy5jb250YWluZXJJZCA9IG9iai5jb250YWluZXJJZCB8fCBudWxsO1xyXG4gICAgICAgICAgICB0aGlzLnNjb3BlUGFyYW1SZXF1aXJlZCA9IG9iai5zY29wZVBhcmFtUmVxdWlyZWQgfHwgbnVsbDtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbn1cclxuIl19