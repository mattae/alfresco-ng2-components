/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import { BpmUserModel } from '../models/bpm-user.model';
import { map, catchError } from 'rxjs/operators';
import * as i0 from "@angular/core";
import * as i1 from "../../services/alfresco-api.service";
import * as i2 from "../../services/log.service";
/**
 *
 * BPMUserService retrieve all the information of an Ecm user.
 *
 */
var BpmUserService = /** @class */ (function () {
    function BpmUserService(apiService, logService) {
        this.apiService = apiService;
        this.logService = logService;
    }
    /**
     * Gets information about the current user.
     * @returns User information object
     */
    /**
     * Gets information about the current user.
     * @return {?} User information object
     */
    BpmUserService.prototype.getCurrentUserInfo = /**
     * Gets information about the current user.
     * @return {?} User information object
     */
    function () {
        var _this = this;
        return from(this.apiService.getInstance().activiti.profileApi.getProfile())
            .pipe(map((/**
         * @param {?} userRepresentation
         * @return {?}
         */
        function (userRepresentation) {
            return new BpmUserModel(userRepresentation);
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets the current user's profile image as a URL.
     * @returns URL string
     */
    /**
     * Gets the current user's profile image as a URL.
     * @return {?} URL string
     */
    BpmUserService.prototype.getCurrentUserProfileImage = /**
     * Gets the current user's profile image as a URL.
     * @return {?} URL string
     */
    function () {
        return this.apiService.getInstance().activiti.profileApi.getProfilePictureUrl();
    };
    /**
     * Throw the error
     * @param error
     */
    /**
     * Throw the error
     * @private
     * @param {?} error
     * @return {?}
     */
    BpmUserService.prototype.handleError = /**
     * Throw the error
     * @private
     * @param {?} error
     * @return {?}
     */
    function (error) {
        // in a real world app, we may send the error to some remote logging infrastructure
        // instead of just logging it to the console
        this.logService.error(error);
        return throwError(error || 'Server error');
    };
    BpmUserService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    BpmUserService.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: LogService }
    ]; };
    /** @nocollapse */ BpmUserService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function BpmUserService_Factory() { return new BpmUserService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: BpmUserService, providedIn: "root" });
    return BpmUserService;
}());
export { BpmUserService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    BpmUserService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    BpmUserService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYnBtLXVzZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInVzZXJpbmZvL3NlcnZpY2VzL2JwbS11c2VyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQWMsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNwRCxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUN6RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDeEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDBCQUEwQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxHQUFHLEVBQUUsVUFBVSxFQUFFLE1BQU0sZ0JBQWdCLENBQUM7Ozs7Ozs7OztBQVFqRDtJQUtJLHdCQUFvQixVQUE4QixFQUM5QixVQUFzQjtRQUR0QixlQUFVLEdBQVYsVUFBVSxDQUFvQjtRQUM5QixlQUFVLEdBQVYsVUFBVSxDQUFZO0lBQzFDLENBQUM7SUFFRDs7O09BR0c7Ozs7O0lBQ0gsMkNBQWtCOzs7O0lBQWxCO1FBQUEsaUJBUUM7UUFQRyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsVUFBVSxFQUFFLENBQUM7YUFDdEUsSUFBSSxDQUNELEdBQUc7Ozs7UUFBQyxVQUFDLGtCQUFzQztZQUN2QyxPQUFPLElBQUksWUFBWSxDQUFDLGtCQUFrQixDQUFDLENBQUM7UUFDaEQsQ0FBQyxFQUFDLEVBQ0YsVUFBVTs7OztRQUFDLFVBQUMsR0FBRyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxHQUFHLENBQUMsRUFBckIsQ0FBcUIsRUFBQyxDQUM3QyxDQUFDO0lBQ1YsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7SUFDSCxtREFBMEI7Ozs7SUFBMUI7UUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsUUFBUSxDQUFDLFVBQVUsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO0lBQ3BGLENBQUM7SUFFRDs7O09BR0c7Ozs7Ozs7SUFDSyxvQ0FBVzs7Ozs7O0lBQW5CLFVBQW9CLEtBQVU7UUFDMUIsbUZBQW1GO1FBQ25GLDRDQUE0QztRQUM1QyxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUM3QixPQUFPLFVBQVUsQ0FBQyxLQUFLLElBQUksY0FBYyxDQUFDLENBQUM7SUFDL0MsQ0FBQzs7Z0JBeENKLFVBQVUsU0FBQztvQkFDUixVQUFVLEVBQUUsTUFBTTtpQkFDckI7Ozs7Z0JBYlEsa0JBQWtCO2dCQUNsQixVQUFVOzs7eUJBcEJuQjtDQXdFQyxBQTFDRCxJQTBDQztTQXZDWSxjQUFjOzs7Ozs7SUFFWCxvQ0FBc0M7Ozs7O0lBQ3RDLG9DQUE4QiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE9ic2VydmFibGUsIGZyb20sIHRocm93RXJyb3IgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMb2dTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbG9nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBCcG1Vc2VyTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvYnBtLXVzZXIubW9kZWwnO1xyXG5pbXBvcnQgeyBtYXAsIGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IFVzZXJSZXByZXNlbnRhdGlvbiB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5cclxuLyoqXHJcbiAqXHJcbiAqIEJQTVVzZXJTZXJ2aWNlIHJldHJpZXZlIGFsbCB0aGUgaW5mb3JtYXRpb24gb2YgYW4gRWNtIHVzZXIuXHJcbiAqXHJcbiAqL1xyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIEJwbVVzZXJTZXJ2aWNlIHtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgbG9nU2VydmljZTogTG9nU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBpbmZvcm1hdGlvbiBhYm91dCB0aGUgY3VycmVudCB1c2VyLlxyXG4gICAgICogQHJldHVybnMgVXNlciBpbmZvcm1hdGlvbiBvYmplY3RcclxuICAgICAqL1xyXG4gICAgZ2V0Q3VycmVudFVzZXJJbmZvKCk6IE9ic2VydmFibGU8QnBtVXNlck1vZGVsPiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuYWN0aXZpdGkucHJvZmlsZUFwaS5nZXRQcm9maWxlKCkpXHJcbiAgICAgICAgICAgIC5waXBlKFxyXG4gICAgICAgICAgICAgICAgbWFwKCh1c2VyUmVwcmVzZW50YXRpb246IFVzZXJSZXByZXNlbnRhdGlvbikgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBuZXcgQnBtVXNlck1vZGVsKHVzZXJSZXByZXNlbnRhdGlvbik7XHJcbiAgICAgICAgICAgICAgICB9KSxcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnIpKVxyXG4gICAgICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgY3VycmVudCB1c2VyJ3MgcHJvZmlsZSBpbWFnZSBhcyBhIFVSTC5cclxuICAgICAqIEByZXR1cm5zIFVSTCBzdHJpbmdcclxuICAgICAqL1xyXG4gICAgZ2V0Q3VycmVudFVzZXJQcm9maWxlSW1hZ2UoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuYWN0aXZpdGkucHJvZmlsZUFwaS5nZXRQcm9maWxlUGljdHVyZVVybCgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGhyb3cgdGhlIGVycm9yXHJcbiAgICAgKiBAcGFyYW0gZXJyb3JcclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBoYW5kbGVFcnJvcihlcnJvcjogYW55KSB7XHJcbiAgICAgICAgLy8gaW4gYSByZWFsIHdvcmxkIGFwcCwgd2UgbWF5IHNlbmQgdGhlIGVycm9yIHRvIHNvbWUgcmVtb3RlIGxvZ2dpbmcgaW5mcmFzdHJ1Y3R1cmVcclxuICAgICAgICAvLyBpbnN0ZWFkIG9mIGp1c3QgbG9nZ2luZyBpdCB0byB0aGUgY29uc29sZVxyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IgfHwgJ1NlcnZlciBlcnJvcicpO1xyXG4gICAgfVxyXG5cclxufVxyXG4iXX0=