/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { from, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ContentService } from '../../services/content.service';
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import { EcmUserModel } from '../models/ecm-user.model';
import * as i0 from "@angular/core";
import * as i1 from "../../services/alfresco-api.service";
import * as i2 from "../../services/content.service";
import * as i3 from "../../services/log.service";
var EcmUserService = /** @class */ (function () {
    function EcmUserService(apiService, contentService, logService) {
        this.apiService = apiService;
        this.contentService = contentService;
        this.logService = logService;
    }
    /**
     * Gets information about a user identified by their username.
     * @param userName Target username
     * @returns User information
     */
    /**
     * Gets information about a user identified by their username.
     * @param {?} userName Target username
     * @return {?} User information
     */
    EcmUserService.prototype.getUserInfo = /**
     * Gets information about a user identified by their username.
     * @param {?} userName Target username
     * @return {?} User information
     */
    function (userName) {
        var _this = this;
        return from(this.apiService.getInstance().core.peopleApi.getPerson(userName))
            .pipe(map((/**
         * @param {?} personEntry
         * @return {?}
         */
        function (personEntry) {
            return new EcmUserModel(personEntry.entry);
        })), catchError((/**
         * @param {?} err
         * @return {?}
         */
        function (err) { return _this.handleError(err); })));
    };
    /**
     * Gets information about the user who is currently logged-in.
     * @returns User information as for getUserInfo
     */
    /**
     * Gets information about the user who is currently logged-in.
     * @return {?} User information as for getUserInfo
     */
    EcmUserService.prototype.getCurrentUserInfo = /**
     * Gets information about the user who is currently logged-in.
     * @return {?} User information as for getUserInfo
     */
    function () {
        return this.getUserInfo('-me-');
    };
    /**
     * Returns a profile image as a URL.
     * @param avatarId Target avatar
     * @returns Image URL
     */
    /**
     * Returns a profile image as a URL.
     * @param {?} avatarId Target avatar
     * @return {?} Image URL
     */
    EcmUserService.prototype.getUserProfileImage = /**
     * Returns a profile image as a URL.
     * @param {?} avatarId Target avatar
     * @return {?} Image URL
     */
    function (avatarId) {
        if (avatarId) {
            return this.contentService.getContentUrl(avatarId);
        }
        return null;
    };
    /**
     * Throw the error
     * @param error
     */
    /**
     * Throw the error
     * @private
     * @param {?} error
     * @return {?}
     */
    EcmUserService.prototype.handleError = /**
     * Throw the error
     * @private
     * @param {?} error
     * @return {?}
     */
    function (error) {
        this.logService.error(error);
        return throwError(error || 'Server error');
    };
    EcmUserService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    EcmUserService.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: ContentService },
        { type: LogService }
    ]; };
    /** @nocollapse */ EcmUserService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function EcmUserService_Factory() { return new EcmUserService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.ContentService), i0.ɵɵinject(i3.LogService)); }, token: EcmUserService, providedIn: "root" });
    return EcmUserService;
}());
export { EcmUserService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    EcmUserService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    EcmUserService.prototype.contentService;
    /**
     * @type {?}
     * @private
     */
    EcmUserService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZWNtLXVzZXIuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInVzZXJpbmZvL3NlcnZpY2VzL2VjbS11c2VyLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUMzQyxPQUFPLEVBQWMsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUNwRCxPQUFPLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQ2pELE9BQU8sRUFBRSxjQUFjLEVBQUUsTUFBTSxnQ0FBZ0MsQ0FBQztBQUNoRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUN6RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDeEQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDBCQUEwQixDQUFDOzs7OztBQUd4RDtJQUtJLHdCQUFvQixVQUE4QixFQUM5QixjQUE4QixFQUM5QixVQUFzQjtRQUZ0QixlQUFVLEdBQVYsVUFBVSxDQUFvQjtRQUM5QixtQkFBYyxHQUFkLGNBQWMsQ0FBZ0I7UUFDOUIsZUFBVSxHQUFWLFVBQVUsQ0FBWTtJQUMxQyxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsb0NBQVc7Ozs7O0lBQVgsVUFBWSxRQUFnQjtRQUE1QixpQkFRQztRQVBHLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDeEUsSUFBSSxDQUNELEdBQUc7Ozs7UUFBQyxVQUFDLFdBQXdCO1lBQ3pCLE9BQU8sSUFBSSxZQUFZLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxDQUFDO1FBQy9DLENBQUMsRUFBQyxFQUNGLFVBQVU7Ozs7UUFBQyxVQUFDLEdBQUcsSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsR0FBRyxDQUFDLEVBQXJCLENBQXFCLEVBQUMsQ0FDN0MsQ0FBQztJQUNWLENBQUM7SUFFRDs7O09BR0c7Ozs7O0lBQ0gsMkNBQWtCOzs7O0lBQWxCO1FBQ0ksT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3BDLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCw0Q0FBbUI7Ozs7O0lBQW5CLFVBQW9CLFFBQWdCO1FBQ2hDLElBQUksUUFBUSxFQUFFO1lBQ1YsT0FBTyxJQUFJLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxRQUFRLENBQUMsQ0FBQztTQUN0RDtRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7SUFFRDs7O09BR0c7Ozs7Ozs7SUFDSyxvQ0FBVzs7Ozs7O0lBQW5CLFVBQW9CLEtBQVU7UUFDMUIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsT0FBTyxVQUFVLENBQUMsS0FBSyxJQUFJLGNBQWMsQ0FBQyxDQUFDO0lBQy9DLENBQUM7O2dCQXBESixVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7O2dCQVBRLGtCQUFrQjtnQkFEbEIsY0FBYztnQkFFZCxVQUFVOzs7eUJBdEJuQjtDQWdGQyxBQXRERCxJQXNEQztTQW5EWSxjQUFjOzs7Ozs7SUFFWCxvQ0FBc0M7Ozs7O0lBQ3RDLHdDQUFzQzs7Ozs7SUFDdEMsb0NBQThCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgZnJvbSwgdGhyb3dFcnJvciB9IGZyb20gJ3J4anMnO1xyXG5pbXBvcnQgeyBtYXAsIGNhdGNoRXJyb3IgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcbmltcG9ydCB7IENvbnRlbnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvY29udGVudC5zZXJ2aWNlJztcclxuaW1wb3J0IHsgQWxmcmVzY29BcGlTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvYWxmcmVzY28tYXBpLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBMb2dTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvbG9nLnNlcnZpY2UnO1xyXG5pbXBvcnQgeyBFY21Vc2VyTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvZWNtLXVzZXIubW9kZWwnO1xyXG5pbXBvcnQgeyBQZXJzb25FbnRyeSB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xyXG5cclxuQEluamVjdGFibGUoe1xyXG4gICAgcHJvdmlkZWRJbjogJ3Jvb3QnXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBFY21Vc2VyU2VydmljZSB7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGNvbnRlbnRTZXJ2aWNlOiBDb250ZW50U2VydmljZSxcclxuICAgICAgICAgICAgICAgIHByaXZhdGUgbG9nU2VydmljZTogTG9nU2VydmljZSkge1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBpbmZvcm1hdGlvbiBhYm91dCBhIHVzZXIgaWRlbnRpZmllZCBieSB0aGVpciB1c2VybmFtZS5cclxuICAgICAqIEBwYXJhbSB1c2VyTmFtZSBUYXJnZXQgdXNlcm5hbWVcclxuICAgICAqIEByZXR1cm5zIFVzZXIgaW5mb3JtYXRpb25cclxuICAgICAqL1xyXG4gICAgZ2V0VXNlckluZm8odXNlck5hbWU6IHN0cmluZyk6IE9ic2VydmFibGU8RWNtVXNlck1vZGVsPiB7XHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkuY29yZS5wZW9wbGVBcGkuZ2V0UGVyc29uKHVzZXJOYW1lKSlcclxuICAgICAgICAgICAgLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBtYXAoKHBlcnNvbkVudHJ5OiBQZXJzb25FbnRyeSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBuZXcgRWNtVXNlck1vZGVsKHBlcnNvbkVudHJ5LmVudHJ5KTtcclxuICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycikpXHJcbiAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGluZm9ybWF0aW9uIGFib3V0IHRoZSB1c2VyIHdobyBpcyBjdXJyZW50bHkgbG9nZ2VkLWluLlxyXG4gICAgICogQHJldHVybnMgVXNlciBpbmZvcm1hdGlvbiBhcyBmb3IgZ2V0VXNlckluZm9cclxuICAgICAqL1xyXG4gICAgZ2V0Q3VycmVudFVzZXJJbmZvKCkge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldFVzZXJJbmZvKCctbWUtJyk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZXR1cm5zIGEgcHJvZmlsZSBpbWFnZSBhcyBhIFVSTC5cclxuICAgICAqIEBwYXJhbSBhdmF0YXJJZCBUYXJnZXQgYXZhdGFyXHJcbiAgICAgKiBAcmV0dXJucyBJbWFnZSBVUkxcclxuICAgICAqL1xyXG4gICAgZ2V0VXNlclByb2ZpbGVJbWFnZShhdmF0YXJJZDogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgICAgICBpZiAoYXZhdGFySWQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuY29udGVudFNlcnZpY2UuZ2V0Q29udGVudFVybChhdmF0YXJJZCk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogVGhyb3cgdGhlIGVycm9yXHJcbiAgICAgKiBAcGFyYW0gZXJyb3JcclxuICAgICAqL1xyXG4gICAgcHJpdmF0ZSBoYW5kbGVFcnJvcihlcnJvcjogYW55KSB7XHJcbiAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKGVycm9yKTtcclxuICAgICAgICByZXR1cm4gdGhyb3dFcnJvcihlcnJvciB8fCAnU2VydmVyIGVycm9yJyk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==