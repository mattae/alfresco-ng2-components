/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { of, from, throwError } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';
import { IdentityUserModel } from '../models/identity-user.model';
import { JwtHelperService } from '../../services/jwt-helper.service';
import { LogService } from '../../services/log.service';
import { AppConfigService } from '../../app-config/app-config.service';
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import * as i0 from "@angular/core";
import * as i1 from "../../services/jwt-helper.service";
import * as i2 from "../../services/alfresco-api.service";
import * as i3 from "../../app-config/app-config.service";
import * as i4 from "../../services/log.service";
var IdentityUserService = /** @class */ (function () {
    function IdentityUserService(jwtHelperService, alfrescoApiService, appConfigService, logService) {
        this.jwtHelperService = jwtHelperService;
        this.alfrescoApiService = alfrescoApiService;
        this.appConfigService = appConfigService;
        this.logService = logService;
    }
    /**
     * Gets the name and other basic details of the current user.
     * @returns The user's details
     */
    /**
     * Gets the name and other basic details of the current user.
     * @return {?} The user's details
     */
    IdentityUserService.prototype.getCurrentUserInfo = /**
     * Gets the name and other basic details of the current user.
     * @return {?} The user's details
     */
    function () {
        /** @type {?} */
        var familyName = this.jwtHelperService.getValueFromLocalAccessToken(JwtHelperService.FAMILY_NAME);
        /** @type {?} */
        var givenName = this.jwtHelperService.getValueFromLocalAccessToken(JwtHelperService.GIVEN_NAME);
        /** @type {?} */
        var email = this.jwtHelperService.getValueFromLocalAccessToken(JwtHelperService.USER_EMAIL);
        /** @type {?} */
        var username = this.jwtHelperService.getValueFromLocalAccessToken(JwtHelperService.USER_PREFERRED_USERNAME);
        /** @type {?} */
        var user = { firstName: givenName, lastName: familyName, email: email, username: username };
        return new IdentityUserModel(user);
    };
    /**
     * Find users based on search input.
     * @param search Search query string
     * @returns List of users
     */
    /**
     * Find users based on search input.
     * @param {?} search Search query string
     * @return {?} List of users
     */
    IdentityUserService.prototype.findUsersByName = /**
     * Find users based on search input.
     * @param {?} search Search query string
     * @return {?} List of users
     */
    function (search) {
        if (search === '') {
            return of([]);
        }
        /** @type {?} */
        var url = this.buildUserUrl();
        /** @type {?} */
        var httpMethod = 'GET';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = { search: search };
        /** @type {?} */
        var bodyParam = {};
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return (from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, Object, null, null)));
    };
    /**
     * Find users based on username input.
     * @param username Search query string
     * @returns List of users
     */
    /**
     * Find users based on username input.
     * @param {?} username Search query string
     * @return {?} List of users
     */
    IdentityUserService.prototype.findUserByUsername = /**
     * Find users based on username input.
     * @param {?} username Search query string
     * @return {?} List of users
     */
    function (username) {
        if (username === '') {
            return of([]);
        }
        /** @type {?} */
        var url = this.buildUserUrl();
        /** @type {?} */
        var httpMethod = 'GET';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = { username: username };
        /** @type {?} */
        var bodyParam = {};
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return (from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, Object, null, null)));
    };
    /**
     * Find users based on email input.
     * @param email Search query string
     * @returns List of users
     */
    /**
     * Find users based on email input.
     * @param {?} email Search query string
     * @return {?} List of users
     */
    IdentityUserService.prototype.findUserByEmail = /**
     * Find users based on email input.
     * @param {?} email Search query string
     * @return {?} List of users
     */
    function (email) {
        if (email === '') {
            return of([]);
        }
        /** @type {?} */
        var url = this.buildUserUrl();
        /** @type {?} */
        var httpMethod = 'GET';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = { email: email };
        /** @type {?} */
        var bodyParam = {};
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return (from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, Object, null, null)));
    };
    /**
     * Find users based on id input.
     * @param id Search query string
     * @returns users object
     */
    /**
     * Find users based on id input.
     * @param {?} id Search query string
     * @return {?} users object
     */
    IdentityUserService.prototype.findUserById = /**
     * Find users based on id input.
     * @param {?} id Search query string
     * @return {?} users object
     */
    function (id) {
        if (id === '') {
            return of([]);
        }
        /** @type {?} */
        var url = this.buildUserUrl() + '/' + id;
        /** @type {?} */
        var httpMethod = 'GET';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = {};
        /** @type {?} */
        var bodyParam = {};
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return (from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, Object, null, null)));
    };
    /**
     * Get client roles of a user for a particular client.
     * @param userId ID of the target user
     * @param clientId ID of the client app
     * @returns List of client roles
     */
    /**
     * Get client roles of a user for a particular client.
     * @param {?} userId ID of the target user
     * @param {?} clientId ID of the client app
     * @return {?} List of client roles
     */
    IdentityUserService.prototype.getClientRoles = /**
     * Get client roles of a user for a particular client.
     * @param {?} userId ID of the target user
     * @param {?} clientId ID of the client app
     * @return {?} List of client roles
     */
    function (userId, clientId) {
        /** @type {?} */
        var url = this.buildUserClientRoleMapping(userId, clientId);
        /** @type {?} */
        var httpMethod = 'GET';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = {};
        /** @type {?} */
        var bodyParam = {};
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, Object, null, null));
    };
    /**
     * Checks whether user has access to a client app.
     * @param userId ID of the target user
     * @param clientId ID of the client app
     * @returns True if the user has access, false otherwise
     */
    /**
     * Checks whether user has access to a client app.
     * @param {?} userId ID of the target user
     * @param {?} clientId ID of the client app
     * @return {?} True if the user has access, false otherwise
     */
    IdentityUserService.prototype.checkUserHasClientApp = /**
     * Checks whether user has access to a client app.
     * @param {?} userId ID of the target user
     * @param {?} clientId ID of the client app
     * @return {?} True if the user has access, false otherwise
     */
    function (userId, clientId) {
        return this.getClientRoles(userId, clientId).pipe(map((/**
         * @param {?} clientRoles
         * @return {?}
         */
        function (clientRoles) {
            if (clientRoles.length > 0) {
                return true;
            }
            return false;
        })));
    };
    /**
     * Checks whether a user has any of the client app roles.
     * @param userId ID of the target user
     * @param clientId ID of the client app
     * @param roleNames List of role names to check for
     * @returns True if the user has one or more of the roles, false otherwise
     */
    /**
     * Checks whether a user has any of the client app roles.
     * @param {?} userId ID of the target user
     * @param {?} clientId ID of the client app
     * @param {?} roleNames List of role names to check for
     * @return {?} True if the user has one or more of the roles, false otherwise
     */
    IdentityUserService.prototype.checkUserHasAnyClientAppRole = /**
     * Checks whether a user has any of the client app roles.
     * @param {?} userId ID of the target user
     * @param {?} clientId ID of the client app
     * @param {?} roleNames List of role names to check for
     * @return {?} True if the user has one or more of the roles, false otherwise
     */
    function (userId, clientId, roleNames) {
        return this.getClientRoles(userId, clientId).pipe(map((/**
         * @param {?} clientRoles
         * @return {?}
         */
        function (clientRoles) {
            /** @type {?} */
            var hasRole = false;
            if (clientRoles.length > 0) {
                roleNames.forEach((/**
                 * @param {?} roleName
                 * @return {?}
                 */
                function (roleName) {
                    /** @type {?} */
                    var role = clientRoles.find((/**
                     * @param {?} availableRole
                     * @return {?}
                     */
                    function (availableRole) {
                        return availableRole.name === roleName;
                    }));
                    if (role) {
                        hasRole = true;
                        return;
                    }
                }));
            }
            return hasRole;
        })));
    };
    /**
     * Gets the client ID for an application.
     * @param applicationName Name of the application
     * @returns Client ID string
     */
    /**
     * Gets the client ID for an application.
     * @param {?} applicationName Name of the application
     * @return {?} Client ID string
     */
    IdentityUserService.prototype.getClientIdByApplicationName = /**
     * Gets the client ID for an application.
     * @param {?} applicationName Name of the application
     * @return {?} Client ID string
     */
    function (applicationName) {
        /** @type {?} */
        var url = this.buildGetClientsUrl();
        /** @type {?} */
        var httpMethod = 'GET';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = { clientId: applicationName };
        /** @type {?} */
        var bodyParam = {};
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance()
            .oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, Object, null, null)).pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            /** @type {?} */
            var clientId = response && response.length > 0 ? response[0].id : '';
            return clientId;
        })));
    };
    /**
     * Checks if a user has access to an application.
     * @param userId ID of the user
     * @param applicationName Name of the application
     * @returns True if the user has access, false otherwise
     */
    /**
     * Checks if a user has access to an application.
     * @param {?} userId ID of the user
     * @param {?} applicationName Name of the application
     * @return {?} True if the user has access, false otherwise
     */
    IdentityUserService.prototype.checkUserHasApplicationAccess = /**
     * Checks if a user has access to an application.
     * @param {?} userId ID of the user
     * @param {?} applicationName Name of the application
     * @return {?} True if the user has access, false otherwise
     */
    function (userId, applicationName) {
        var _this = this;
        return this.getClientIdByApplicationName(applicationName).pipe(switchMap((/**
         * @param {?} clientId
         * @return {?}
         */
        function (clientId) {
            return _this.checkUserHasClientApp(userId, clientId);
        })));
    };
    /**
     * Checks if a user has any application role.
     * @param userId ID of the target user
     * @param applicationName Name of the application
     * @param roleNames List of role names to check for
     * @returns True if the user has one or more of the roles, false otherwise
     */
    /**
     * Checks if a user has any application role.
     * @param {?} userId ID of the target user
     * @param {?} applicationName Name of the application
     * @param {?} roleNames List of role names to check for
     * @return {?} True if the user has one or more of the roles, false otherwise
     */
    IdentityUserService.prototype.checkUserHasAnyApplicationRole = /**
     * Checks if a user has any application role.
     * @param {?} userId ID of the target user
     * @param {?} applicationName Name of the application
     * @param {?} roleNames List of role names to check for
     * @return {?} True if the user has one or more of the roles, false otherwise
     */
    function (userId, applicationName, roleNames) {
        var _this = this;
        return this.getClientIdByApplicationName(applicationName).pipe(switchMap((/**
         * @param {?} clientId
         * @return {?}
         */
        function (clientId) {
            return _this.checkUserHasAnyClientAppRole(userId, clientId, roleNames);
        })));
    };
    /**
     * Gets details for all users.
     * @returns Array of user info objects
     */
    /**
     * Gets details for all users.
     * @return {?} Array of user info objects
     */
    IdentityUserService.prototype.getUsers = /**
     * Gets details for all users.
     * @return {?} Array of user info objects
     */
    function () {
        /** @type {?} */
        var url = this.buildUserUrl();
        /** @type {?} */
        var httpMethod = 'GET';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = {};
        /** @type {?} */
        var bodyParam = {};
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var authNames = [];
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, authNames, contentTypes, accepts, null, null)).pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return response;
        })));
    };
    /**
     * Gets a list of roles for a user.
     * @param userId ID of the user
     * @returns Array of role info objects
     */
    /**
     * Gets a list of roles for a user.
     * @param {?} userId ID of the user
     * @return {?} Array of role info objects
     */
    IdentityUserService.prototype.getUserRoles = /**
     * Gets a list of roles for a user.
     * @param {?} userId ID of the user
     * @return {?} Array of role info objects
     */
    function (userId) {
        /** @type {?} */
        var url = this.buildRolesUrl(userId);
        /** @type {?} */
        var httpMethod = 'GET';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = {};
        /** @type {?} */
        var bodyParam = {};
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, Object, null, null)).pipe(map((/**
         * @param {?} response
         * @return {?}
         */
        function (response) {
            return response;
        })));
    };
    /**
     * Gets an array of users (including the current user) who have any of the roles in the supplied list.
     * @param roleNames List of role names to look for
     * @returns Array of user info objects
     */
    /**
     * Gets an array of users (including the current user) who have any of the roles in the supplied list.
     * @param {?} roleNames List of role names to look for
     * @return {?} Array of user info objects
     */
    IdentityUserService.prototype.getUsersByRolesWithCurrentUser = /**
     * Gets an array of users (including the current user) who have any of the roles in the supplied list.
     * @param {?} roleNames List of role names to look for
     * @return {?} Array of user info objects
     */
    function (roleNames) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var filteredUsers, users, i, hasAnyRole;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        filteredUsers = [];
                        if (!(roleNames && roleNames.length > 0)) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.getUsers().toPromise()];
                    case 1:
                        users = _a.sent();
                        i = 0;
                        _a.label = 2;
                    case 2:
                        if (!(i < users.length)) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.userHasAnyRole(users[i].id, roleNames)];
                    case 3:
                        hasAnyRole = _a.sent();
                        if (hasAnyRole) {
                            filteredUsers.push(users[i]);
                        }
                        _a.label = 4;
                    case 4:
                        i++;
                        return [3 /*break*/, 2];
                    case 5: return [2 /*return*/, filteredUsers];
                }
            });
        });
    };
    /**
     * Gets an array of users (not including the current user) who have any of the roles in the supplied list.
     * @param roleNames List of role names to look for
     * @returns Array of user info objects
     */
    /**
     * Gets an array of users (not including the current user) who have any of the roles in the supplied list.
     * @param {?} roleNames List of role names to look for
     * @return {?} Array of user info objects
     */
    IdentityUserService.prototype.getUsersByRolesWithoutCurrentUser = /**
     * Gets an array of users (not including the current user) who have any of the roles in the supplied list.
     * @param {?} roleNames List of role names to look for
     * @return {?} Array of user info objects
     */
    function (roleNames) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var filteredUsers, currentUser_1, users, i, hasAnyRole;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        filteredUsers = [];
                        if (!(roleNames && roleNames.length > 0)) return [3 /*break*/, 5];
                        currentUser_1 = this.getCurrentUserInfo();
                        return [4 /*yield*/, this.getUsers().toPromise()];
                    case 1:
                        users = _a.sent();
                        users = users.filter((/**
                         * @param {?} user
                         * @return {?}
                         */
                        function (user) { return user.username !== currentUser_1.username; }));
                        i = 0;
                        _a.label = 2;
                    case 2:
                        if (!(i < users.length)) return [3 /*break*/, 5];
                        return [4 /*yield*/, this.userHasAnyRole(users[i].id, roleNames)];
                    case 3:
                        hasAnyRole = _a.sent();
                        if (hasAnyRole) {
                            filteredUsers.push(users[i]);
                        }
                        _a.label = 4;
                    case 4:
                        i++;
                        return [3 /*break*/, 2];
                    case 5: return [2 /*return*/, filteredUsers];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} userId
     * @param {?} roleNames
     * @return {?}
     */
    IdentityUserService.prototype.userHasAnyRole = /**
     * @private
     * @param {?} userId
     * @param {?} roleNames
     * @return {?}
     */
    function (userId, roleNames) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var userRoles, hasAnyRole;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.getUserRoles(userId).toPromise()];
                    case 1:
                        userRoles = _a.sent();
                        hasAnyRole = roleNames.some((/**
                         * @param {?} roleName
                         * @return {?}
                         */
                        function (roleName) {
                            /** @type {?} */
                            var filteredRoles = userRoles.filter((/**
                             * @param {?} userRole
                             * @return {?}
                             */
                            function (userRole) {
                                return userRole.name.toLocaleLowerCase() === roleName.toLocaleLowerCase();
                            }));
                            return filteredRoles.length > 0;
                        }));
                        return [2 /*return*/, hasAnyRole];
                }
            });
        });
    };
    /**
     * Checks if a user has one of the roles from a list.
     * @param userId ID of the target user
     * @param roleNames Array of roles to check for
     * @returns True if the user has one of the roles, false otherwise
     */
    /**
     * Checks if a user has one of the roles from a list.
     * @param {?} userId ID of the target user
     * @param {?} roleNames Array of roles to check for
     * @return {?} True if the user has one of the roles, false otherwise
     */
    IdentityUserService.prototype.checkUserHasRole = /**
     * Checks if a user has one of the roles from a list.
     * @param {?} userId ID of the target user
     * @param {?} roleNames Array of roles to check for
     * @return {?} True if the user has one of the roles, false otherwise
     */
    function (userId, roleNames) {
        return this.getUserRoles(userId).pipe(map((/**
         * @param {?} userRoles
         * @return {?}
         */
        function (userRoles) {
            /** @type {?} */
            var hasRole = false;
            if (userRoles && userRoles.length > 0) {
                roleNames.forEach((/**
                 * @param {?} roleName
                 * @return {?}
                 */
                function (roleName) {
                    /** @type {?} */
                    var role = userRoles.find((/**
                     * @param {?} userRole
                     * @return {?}
                     */
                    function (userRole) {
                        return roleName === userRole.name;
                    }));
                    if (role) {
                        hasRole = true;
                        return;
                    }
                }));
            }
            return hasRole;
        })));
    };
    /**
     * Gets details for all users.
     * @returns Array of user information objects.
     */
    /**
     * Gets details for all users.
     * @param {?} requestQuery
     * @return {?} Array of user information objects.
     */
    IdentityUserService.prototype.queryUsers = /**
     * Gets details for all users.
     * @param {?} requestQuery
     * @return {?} Array of user information objects.
     */
    function (requestQuery) {
        var _this = this;
        /** @type {?} */
        var url = this.buildUserUrl();
        /** @type {?} */
        var httpMethod = 'GET';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = { first: requestQuery.first, max: requestQuery.max };
        /** @type {?} */
        var bodyParam = {};
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var authNames = [];
        /** @type {?} */
        var contentTypes = ['application/json'];
        return this.getTotalUsersCount().pipe(switchMap((/**
         * @param {?} totalCount
         * @return {?}
         */
        function (totalCount) {
            return from(_this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, authNames, contentTypes, null, null, null)).pipe(map((/**
             * @param {?} response
             * @return {?}
             */
            function (response) {
                return (/** @type {?} */ ({
                    entries: response,
                    pagination: {
                        skipCount: requestQuery.first,
                        maxItems: requestQuery.max,
                        count: totalCount,
                        hasMoreItems: false,
                        totalItems: totalCount
                    }
                }));
            })), catchError((/**
             * @param {?} error
             * @return {?}
             */
            function (error) { return _this.handleError(error); })));
        })));
    };
    /**
     * Gets users total count.
     * @returns Number of users count.
     */
    /**
     * Gets users total count.
     * @return {?} Number of users count.
     */
    IdentityUserService.prototype.getTotalUsersCount = /**
     * Gets users total count.
     * @return {?} Number of users count.
     */
    function () {
        var _this = this;
        /** @type {?} */
        var url = this.buildUserUrl() + "/count";
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance()
            .oauth2Auth.callCustomApi(url, 'GET', null, null, null, null, null, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) { return _this.handleError(error); })));
    };
    /**
     * Creates new user.
     * @param newUser Object containing the new user details.
     * @returns Empty response when the user created.
     */
    /**
     * Creates new user.
     * @param {?} newUser Object containing the new user details.
     * @return {?} Empty response when the user created.
     */
    IdentityUserService.prototype.createUser = /**
     * Creates new user.
     * @param {?} newUser Object containing the new user details.
     * @return {?} Empty response when the user created.
     */
    function (newUser) {
        var _this = this;
        /** @type {?} */
        var url = this.buildUserUrl();
        /** @type {?} */
        var request = JSON.stringify(newUser);
        /** @type {?} */
        var httpMethod = 'POST';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = {};
        /** @type {?} */
        var bodyParam = request;
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) { return _this.handleError(error); })));
    };
    /**
     * Updates user details.
     * @param userId Id of the user.
     * @param updatedUser Object containing the user details.
     * @returns Empty response when the user updated.
     */
    /**
     * Updates user details.
     * @param {?} userId Id of the user.
     * @param {?} updatedUser Object containing the user details.
     * @return {?} Empty response when the user updated.
     */
    IdentityUserService.prototype.updateUser = /**
     * Updates user details.
     * @param {?} userId Id of the user.
     * @param {?} updatedUser Object containing the user details.
     * @return {?} Empty response when the user updated.
     */
    function (userId, updatedUser) {
        var _this = this;
        /** @type {?} */
        var url = this.buildUserUrl() + '/' + userId;
        /** @type {?} */
        var request = JSON.stringify(updatedUser);
        /** @type {?} */
        var httpMethod = 'PUT';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = {};
        /** @type {?} */
        var bodyParam = request;
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) { return _this.handleError(error); })));
    };
    /**
     * Deletes User.
     * @param userId Id of the  user.
     * @returns Empty response when the user deleted.
     */
    /**
     * Deletes User.
     * @param {?} userId Id of the  user.
     * @return {?} Empty response when the user deleted.
     */
    IdentityUserService.prototype.deleteUser = /**
     * Deletes User.
     * @param {?} userId Id of the  user.
     * @return {?} Empty response when the user deleted.
     */
    function (userId) {
        var _this = this;
        /** @type {?} */
        var url = this.buildUserUrl() + '/' + userId;
        /** @type {?} */
        var httpMethod = 'DELETE';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = {};
        /** @type {?} */
        var bodyParam = {};
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) { return _this.handleError(error); })));
    };
    /**
     * Changes user password.
     * @param userId Id of the user.
     * @param credentials Details of user Credentials.
     * @returns Empty response when the password changed.
     */
    /**
     * Changes user password.
     * @param {?} userId Id of the user.
     * @param {?} newPassword
     * @return {?} Empty response when the password changed.
     */
    IdentityUserService.prototype.changePassword = /**
     * Changes user password.
     * @param {?} userId Id of the user.
     * @param {?} newPassword
     * @return {?} Empty response when the password changed.
     */
    function (userId, newPassword) {
        var _this = this;
        /** @type {?} */
        var url = this.buildUserUrl() + '/' + userId + '/reset-password';
        /** @type {?} */
        var request = JSON.stringify(newPassword);
        /** @type {?} */
        var httpMethod = 'PUT';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = {};
        /** @type {?} */
        var bodyParam = request;
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) { return _this.handleError(error); })));
    };
    /**
     * Gets involved groups.
     * @param userId Id of the user.
     * @returns Array of involved groups information objects.
     */
    /**
     * Gets involved groups.
     * @param {?} userId Id of the user.
     * @return {?} Array of involved groups information objects.
     */
    IdentityUserService.prototype.getInvolvedGroups = /**
     * Gets involved groups.
     * @param {?} userId Id of the user.
     * @return {?} Array of involved groups information objects.
     */
    function (userId) {
        var _this = this;
        /** @type {?} */
        var url = this.buildUserUrl() + '/' + userId + '/groups/';
        /** @type {?} */
        var httpMethod = 'GET';
        /** @type {?} */
        var pathParams = { id: userId };
        /** @type {?} */
        var queryParams = {};
        /** @type {?} */
        var bodyParam = {};
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var authNames = [];
        /** @type {?} */
        var contentTypes = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, authNames, contentTypes, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) { return _this.handleError(error); })));
    };
    /**
     * Joins group.
     * @param joinGroupRequest Details of join group request (IdentityJoinGroupRequestModel).
     * @returns Empty response when the user joined the group.
     */
    /**
     * Joins group.
     * @param {?} joinGroupRequest Details of join group request (IdentityJoinGroupRequestModel).
     * @return {?} Empty response when the user joined the group.
     */
    IdentityUserService.prototype.joinGroup = /**
     * Joins group.
     * @param {?} joinGroupRequest Details of join group request (IdentityJoinGroupRequestModel).
     * @return {?} Empty response when the user joined the group.
     */
    function (joinGroupRequest) {
        var _this = this;
        /** @type {?} */
        var url = this.buildUserUrl() + '/' + joinGroupRequest.userId + '/groups/' + joinGroupRequest.groupId;
        /** @type {?} */
        var request = JSON.stringify(joinGroupRequest);
        /** @type {?} */
        var httpMethod = 'PUT';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = {};
        /** @type {?} */
        var bodyParam = request;
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) { return _this.handleError(error); })));
    };
    /**
     * Leaves group.
     * @param userId Id of the user.
     * @param groupId Id of the  group.
     * @returns Empty response when the user left the group.
     */
    /**
     * Leaves group.
     * @param {?} userId Id of the user.
     * @param {?} groupId Id of the  group.
     * @return {?} Empty response when the user left the group.
     */
    IdentityUserService.prototype.leaveGroup = /**
     * Leaves group.
     * @param {?} userId Id of the user.
     * @param {?} groupId Id of the  group.
     * @return {?} Empty response when the user left the group.
     */
    function (userId, groupId) {
        var _this = this;
        /** @type {?} */
        var url = this.buildUserUrl() + '/' + userId + '/groups/' + groupId;
        /** @type {?} */
        var httpMethod = 'DELETE';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = {};
        /** @type {?} */
        var bodyParam = {};
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) { return _this.handleError(error); })));
    };
    /**
     * Gets available roles
     * @param userId Id of the user.
     * @returns Array of available roles information objects
     */
    /**
     * Gets available roles
     * @param {?} userId Id of the user.
     * @return {?} Array of available roles information objects
     */
    IdentityUserService.prototype.getAvailableRoles = /**
     * Gets available roles
     * @param {?} userId Id of the user.
     * @return {?} Array of available roles information objects
     */
    function (userId) {
        var _this = this;
        /** @type {?} */
        var url = this.buildUserUrl() + '/' + userId + '/role-mappings/realm/available';
        /** @type {?} */
        var httpMethod = 'GET';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = {};
        /** @type {?} */
        var bodyParam = {};
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var authNames = [];
        /** @type {?} */
        var contentTypes = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, authNames, contentTypes, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) { return _this.handleError(error); })));
    };
    /**
     * Gets assigned roles.
     * @param userId Id of the user.
     * @returns Array of assigned roles information objects
     */
    /**
     * Gets assigned roles.
     * @param {?} userId Id of the user.
     * @return {?} Array of assigned roles information objects
     */
    IdentityUserService.prototype.getAssignedRoles = /**
     * Gets assigned roles.
     * @param {?} userId Id of the user.
     * @return {?} Array of assigned roles information objects
     */
    function (userId) {
        var _this = this;
        /** @type {?} */
        var url = this.buildUserUrl() + '/' + userId + '/role-mappings/realm';
        /** @type {?} */
        var httpMethod = 'GET';
        /** @type {?} */
        var pathParams = { id: userId };
        /** @type {?} */
        var queryParams = {};
        /** @type {?} */
        var bodyParam = {};
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var authNames = [];
        /** @type {?} */
        var contentTypes = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, authNames, contentTypes, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) { return _this.handleError(error); })));
    };
    /**
     * Gets effective roles.
     * @param userId Id of the user.
     * @returns Array of composite roles information objects
     */
    /**
     * Gets effective roles.
     * @param {?} userId Id of the user.
     * @return {?} Array of composite roles information objects
     */
    IdentityUserService.prototype.getEffectiveRoles = /**
     * Gets effective roles.
     * @param {?} userId Id of the user.
     * @return {?} Array of composite roles information objects
     */
    function (userId) {
        var _this = this;
        /** @type {?} */
        var url = this.buildUserUrl() + '/' + userId + '/role-mappings/realm/composite';
        /** @type {?} */
        var httpMethod = 'GET';
        /** @type {?} */
        var pathParams = { id: userId };
        /** @type {?} */
        var queryParams = {};
        /** @type {?} */
        var bodyParam = {};
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var authNames = [];
        /** @type {?} */
        var contentTypes = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, authNames, contentTypes, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) { return _this.handleError(error); })));
    };
    /**
     * Assigns roles to the user.
     * @param userId Id of the user.
     * @param roles Array of roles.
     * @returns Empty response when the role assigned.
     */
    /**
     * Assigns roles to the user.
     * @param {?} userId Id of the user.
     * @param {?} roles Array of roles.
     * @return {?} Empty response when the role assigned.
     */
    IdentityUserService.prototype.assignRoles = /**
     * Assigns roles to the user.
     * @param {?} userId Id of the user.
     * @param {?} roles Array of roles.
     * @return {?} Empty response when the role assigned.
     */
    function (userId, roles) {
        var _this = this;
        /** @type {?} */
        var url = this.buildUserUrl() + '/' + userId + '/role-mappings/realm';
        /** @type {?} */
        var request = JSON.stringify(roles);
        /** @type {?} */
        var httpMethod = 'POST';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = {};
        /** @type {?} */
        var bodyParam = request;
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) { return _this.handleError(error); })));
    };
    /**
     * Removes assigned roles.
     * @param userId Id of the user.
     * @param roles Array of roles.
     * @returns Empty response when the role removed.
     */
    /**
     * Removes assigned roles.
     * @param {?} userId Id of the user.
     * @param {?} removedRoles
     * @return {?} Empty response when the role removed.
     */
    IdentityUserService.prototype.removeRoles = /**
     * Removes assigned roles.
     * @param {?} userId Id of the user.
     * @param {?} removedRoles
     * @return {?} Empty response when the role removed.
     */
    function (userId, removedRoles) {
        var _this = this;
        /** @type {?} */
        var url = this.buildUserUrl() + '/' + userId + '/role-mappings/realm';
        /** @type {?} */
        var request = JSON.stringify(removedRoles);
        /** @type {?} */
        var httpMethod = 'DELETE';
        /** @type {?} */
        var pathParams = {};
        /** @type {?} */
        var queryParams = {};
        /** @type {?} */
        var bodyParam = request;
        /** @type {?} */
        var headerParams = {};
        /** @type {?} */
        var formParams = {};
        /** @type {?} */
        var contentTypes = ['application/json'];
        /** @type {?} */
        var accepts = ['application/json'];
        return from(this.alfrescoApiService.getInstance().oauth2Auth.callCustomApi(url, httpMethod, pathParams, queryParams, headerParams, formParams, bodyParam, contentTypes, accepts, null, null, null)).pipe(catchError((/**
         * @param {?} error
         * @return {?}
         */
        function (error) { return _this.handleError(error); })));
    };
    /**
     * @private
     * @return {?}
     */
    IdentityUserService.prototype.buildUserUrl = /**
     * @private
     * @return {?}
     */
    function () {
        return this.appConfigService.get('identityHost') + "/users";
    };
    /**
     * @private
     * @param {?} userId
     * @param {?} clientId
     * @return {?}
     */
    IdentityUserService.prototype.buildUserClientRoleMapping = /**
     * @private
     * @param {?} userId
     * @param {?} clientId
     * @return {?}
     */
    function (userId, clientId) {
        return this.appConfigService.get('identityHost') + "/users/" + userId + "/role-mappings/clients/" + clientId;
    };
    /**
     * @private
     * @param {?} userId
     * @return {?}
     */
    IdentityUserService.prototype.buildRolesUrl = /**
     * @private
     * @param {?} userId
     * @return {?}
     */
    function (userId) {
        return this.appConfigService.get('identityHost') + "/users/" + userId + "/role-mappings/realm/composite";
    };
    /**
     * @private
     * @return {?}
     */
    IdentityUserService.prototype.buildGetClientsUrl = /**
     * @private
     * @return {?}
     */
    function () {
        return this.appConfigService.get('identityHost') + "/clients";
    };
    /**
     * Throw the error
     * @param error
     */
    /**
     * Throw the error
     * @private
     * @param {?} error
     * @return {?}
     */
    IdentityUserService.prototype.handleError = /**
     * Throw the error
     * @private
     * @param {?} error
     * @return {?}
     */
    function (error) {
        this.logService.error(error);
        return throwError(error || 'Server error');
    };
    IdentityUserService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    IdentityUserService.ctorParameters = function () { return [
        { type: JwtHelperService },
        { type: AlfrescoApiService },
        { type: AppConfigService },
        { type: LogService }
    ]; };
    /** @nocollapse */ IdentityUserService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function IdentityUserService_Factory() { return new IdentityUserService(i0.ɵɵinject(i1.JwtHelperService), i0.ɵɵinject(i2.AlfrescoApiService), i0.ɵɵinject(i3.AppConfigService), i0.ɵɵinject(i4.LogService)); }, token: IdentityUserService, providedIn: "root" });
    return IdentityUserService;
}());
export { IdentityUserService };
if (false) {
    /**
     * @type {?}
     * @private
     */
    IdentityUserService.prototype.jwtHelperService;
    /**
     * @type {?}
     * @private
     */
    IdentityUserService.prototype.alfrescoApiService;
    /**
     * @type {?}
     * @private
     */
    IdentityUserService.prototype.appConfigService;
    /**
     * @type {?}
     * @private
     */
    IdentityUserService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaWRlbnRpdHktdXNlci5zZXJ2aWNlLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsidXNlcmluZm8vc2VydmljZXMvaWRlbnRpdHktdXNlci5zZXJ2aWNlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQWlCQSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sZUFBZSxDQUFDO0FBQzNDLE9BQU8sRUFBYyxFQUFFLEVBQUUsSUFBSSxFQUFFLFVBQVUsRUFBRSxNQUFNLE1BQU0sQ0FBQztBQUN4RCxPQUFPLEVBQUUsVUFBVSxFQUFFLEdBQUcsRUFBRSxTQUFTLEVBQUUsTUFBTSxnQkFBZ0IsQ0FBQztBQUU1RCxPQUFPLEVBQ0gsaUJBQWlCLEVBS3BCLE1BQU0sK0JBQStCLENBQUM7QUFDdkMsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sbUNBQW1DLENBQUM7QUFDckUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDO0FBQ3ZFLE9BQU8sRUFBRSxrQkFBa0IsRUFBRSxNQUFNLHFDQUFxQyxDQUFDOzs7Ozs7QUFJekU7SUFLSSw2QkFDWSxnQkFBa0MsRUFDbEMsa0JBQXNDLEVBQ3RDLGdCQUFrQyxFQUNsQyxVQUFzQjtRQUh0QixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLHVCQUFrQixHQUFsQixrQkFBa0IsQ0FBb0I7UUFDdEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUNsQyxlQUFVLEdBQVYsVUFBVSxDQUFZO0lBQUksQ0FBQztJQUV2Qzs7O09BR0c7Ozs7O0lBQ0gsZ0RBQWtCOzs7O0lBQWxCOztZQUNVLFVBQVUsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsNEJBQTRCLENBQVMsZ0JBQWdCLENBQUMsV0FBVyxDQUFDOztZQUNyRyxTQUFTLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLDRCQUE0QixDQUFTLGdCQUFnQixDQUFDLFVBQVUsQ0FBQzs7WUFDbkcsS0FBSyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyw0QkFBNEIsQ0FBUyxnQkFBZ0IsQ0FBQyxVQUFVLENBQUM7O1lBQy9GLFFBQVEsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsNEJBQTRCLENBQVMsZ0JBQWdCLENBQUMsdUJBQXVCLENBQUM7O1lBQy9HLElBQUksR0FBRyxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQUUsUUFBUSxFQUFFLFVBQVUsRUFBRSxLQUFLLEVBQUUsS0FBSyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUU7UUFDN0YsT0FBTyxJQUFJLGlCQUFpQixDQUFDLElBQUksQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCw2Q0FBZTs7Ozs7SUFBZixVQUFnQixNQUFjO1FBQzFCLElBQUksTUFBTSxLQUFLLEVBQUUsRUFBRTtZQUNmLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ2pCOztZQUNLLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFOztZQUN6QixVQUFVLEdBQUcsS0FBSzs7WUFBRSxVQUFVLEdBQUcsRUFBRTs7WUFBRSxXQUFXLEdBQUcsRUFBRSxNQUFNLEVBQUUsTUFBTSxFQUFFOztZQUFFLFNBQVMsR0FBRyxFQUFFOztZQUFFLFlBQVksR0FBRyxFQUFFOztZQUMxRyxVQUFVLEdBQUcsRUFBRTs7WUFBRSxZQUFZLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQzs7WUFBRSxPQUFPLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztRQUV4RixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUN2RSxHQUFHLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQ3hDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUNuQyxZQUFZLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQzdDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCxnREFBa0I7Ozs7O0lBQWxCLFVBQW1CLFFBQWdCO1FBQy9CLElBQUksUUFBUSxLQUFLLEVBQUUsRUFBRTtZQUNqQixPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUNqQjs7WUFDSyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRTs7WUFDekIsVUFBVSxHQUFHLEtBQUs7O1lBQUUsVUFBVSxHQUFHLEVBQUU7O1lBQUUsV0FBVyxHQUFHLEVBQUUsUUFBUSxFQUFFLFFBQVEsRUFBRTs7WUFBRSxTQUFTLEdBQUcsRUFBRTs7WUFBRSxZQUFZLEdBQUcsRUFBRTs7WUFDOUcsVUFBVSxHQUFHLEVBQUU7O1lBQUUsWUFBWSxHQUFHLENBQUMsa0JBQWtCLENBQUM7O1lBQUUsT0FBTyxHQUFHLENBQUMsa0JBQWtCLENBQUM7UUFFeEYsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FDdkUsR0FBRyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUN4QyxZQUFZLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFDbkMsWUFBWSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUM3QyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsNkNBQWU7Ozs7O0lBQWYsVUFBZ0IsS0FBYTtRQUN6QixJQUFJLEtBQUssS0FBSyxFQUFFLEVBQUU7WUFDZCxPQUFPLEVBQUUsQ0FBQyxFQUFFLENBQUMsQ0FBQztTQUNqQjs7WUFDSyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRTs7WUFDekIsVUFBVSxHQUFHLEtBQUs7O1lBQUUsVUFBVSxHQUFHLEVBQUU7O1lBQUUsV0FBVyxHQUFHLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRTs7WUFBRSxTQUFTLEdBQUcsRUFBRTs7WUFBRSxZQUFZLEdBQUcsRUFBRTs7WUFDeEcsVUFBVSxHQUFHLEVBQUU7O1lBQUUsWUFBWSxHQUFHLENBQUMsa0JBQWtCLENBQUM7O1lBQUUsT0FBTyxHQUFHLENBQUMsa0JBQWtCLENBQUM7UUFFeEYsT0FBTyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FDdkUsR0FBRyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUN4QyxZQUFZLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFDbkMsWUFBWSxFQUFFLE9BQU8sRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUM3QyxDQUFDLENBQUM7SUFDUCxDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsMENBQVk7Ozs7O0lBQVosVUFBYSxFQUFVO1FBQ25CLElBQUksRUFBRSxLQUFLLEVBQUUsRUFBRTtZQUNYLE9BQU8sRUFBRSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1NBQ2pCOztZQUNLLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLEdBQUcsR0FBRyxHQUFHLEVBQUU7O1lBQ3BDLFVBQVUsR0FBRyxLQUFLOztZQUFFLFVBQVUsR0FBRyxFQUFFOztZQUFFLFdBQVcsR0FBRyxFQUFFOztZQUFFLFNBQVMsR0FBRyxFQUFFOztZQUFFLFlBQVksR0FBRyxFQUFFOztZQUMxRixVQUFVLEdBQUcsRUFBRTs7WUFBRSxZQUFZLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQzs7WUFBRSxPQUFPLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztRQUV4RixPQUFPLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUN2RSxHQUFHLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQ3hDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUNuQyxZQUFZLEVBQUUsT0FBTyxFQUFFLE1BQU0sRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUFDLENBQzdDLENBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILDRDQUFjOzs7Ozs7SUFBZCxVQUFlLE1BQWMsRUFBRSxRQUFnQjs7WUFDckMsR0FBRyxHQUFHLElBQUksQ0FBQywwQkFBMEIsQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDOztZQUN2RCxVQUFVLEdBQUcsS0FBSzs7WUFBRSxVQUFVLEdBQUcsRUFBRTs7WUFBRSxXQUFXLEdBQUcsRUFBRTs7WUFBRSxTQUFTLEdBQUcsRUFBRTs7WUFBRSxZQUFZLEdBQUcsRUFBRTs7WUFDMUYsVUFBVSxHQUFHLEVBQUU7O1lBQUUsWUFBWSxHQUFHLENBQUMsa0JBQWtCLENBQUM7O1lBQUUsT0FBTyxHQUFHLENBQUMsa0JBQWtCLENBQUM7UUFFeEYsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQ3RFLEdBQUcsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFDeEMsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQ25DLFlBQVksRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FDN0MsQ0FBQztJQUNOLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILG1EQUFxQjs7Ozs7O0lBQXJCLFVBQXNCLE1BQWMsRUFBRSxRQUFnQjtRQUNsRCxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FDN0MsR0FBRzs7OztRQUFDLFVBQUMsV0FBa0I7WUFDbkIsSUFBSSxXQUFXLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDeEIsT0FBTyxJQUFJLENBQUM7YUFDZjtZQUNELE9BQU8sS0FBSyxDQUFDO1FBQ2pCLENBQUMsRUFBQyxDQUNMLENBQUM7SUFDTixDQUFDO0lBRUQ7Ozs7OztPQU1HOzs7Ozs7OztJQUNILDBEQUE0Qjs7Ozs7OztJQUE1QixVQUE2QixNQUFjLEVBQUUsUUFBZ0IsRUFBRSxTQUFtQjtRQUM5RSxPQUFPLElBQUksQ0FBQyxjQUFjLENBQUMsTUFBTSxFQUFFLFFBQVEsQ0FBQyxDQUFDLElBQUksQ0FDN0MsR0FBRzs7OztRQUFDLFVBQUMsV0FBa0I7O2dCQUNmLE9BQU8sR0FBRyxLQUFLO1lBQ25CLElBQUksV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7Z0JBQ3hCLFNBQVMsQ0FBQyxPQUFPOzs7O2dCQUFDLFVBQUMsUUFBUTs7d0JBQ2pCLElBQUksR0FBRyxXQUFXLENBQUMsSUFBSTs7OztvQkFBQyxVQUFDLGFBQWE7d0JBQ3hDLE9BQU8sYUFBYSxDQUFDLElBQUksS0FBSyxRQUFRLENBQUM7b0JBQzNDLENBQUMsRUFBQztvQkFFRixJQUFJLElBQUksRUFBRTt3QkFDTixPQUFPLEdBQUcsSUFBSSxDQUFDO3dCQUNmLE9BQU87cUJBQ1Y7Z0JBQ0wsQ0FBQyxFQUFDLENBQUM7YUFDTjtZQUNELE9BQU8sT0FBTyxDQUFDO1FBQ25CLENBQUMsRUFBQyxDQUNMLENBQUM7SUFDTixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsMERBQTRCOzs7OztJQUE1QixVQUE2QixlQUF1Qjs7WUFDMUMsR0FBRyxHQUFHLElBQUksQ0FBQyxrQkFBa0IsRUFBRTs7WUFDL0IsVUFBVSxHQUFHLEtBQUs7O1lBQUUsVUFBVSxHQUFHLEVBQUU7O1lBQUUsV0FBVyxHQUFHLEVBQUUsUUFBUSxFQUFFLGVBQWUsRUFBRTs7WUFBRSxTQUFTLEdBQUcsRUFBRTs7WUFBRSxZQUFZLEdBQUcsRUFBRTs7WUFBRSxVQUFVLEdBQUcsRUFBRTs7WUFDdEksWUFBWSxHQUFHLENBQUMsa0JBQWtCLENBQUM7O1lBQUUsT0FBTyxHQUFHLENBQUMsa0JBQWtCLENBQUM7UUFDdkUsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRTthQUM1QyxVQUFVLENBQUMsYUFBYSxDQUFDLEdBQUcsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSxZQUFZLEVBQzVFLFVBQVUsRUFBRSxTQUFTLEVBQUUsWUFBWSxFQUNuQyxPQUFPLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FDbkMsQ0FBQyxJQUFJLENBQ0YsR0FBRzs7OztRQUFDLFVBQUMsUUFBZTs7Z0JBQ1YsUUFBUSxHQUFHLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsUUFBUSxDQUFDLENBQUMsQ0FBQyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUMsRUFBRTtZQUN0RSxPQUFPLFFBQVEsQ0FBQztRQUNwQixDQUFDLEVBQUMsQ0FDTCxDQUFDO0lBQ04sQ0FBQztJQUVEOzs7OztPQUtHOzs7Ozs7O0lBQ0gsMkRBQTZCOzs7Ozs7SUFBN0IsVUFBOEIsTUFBYyxFQUFFLGVBQXVCO1FBQXJFLGlCQU1DO1FBTEcsT0FBTyxJQUFJLENBQUMsNEJBQTRCLENBQUMsZUFBZSxDQUFDLENBQUMsSUFBSSxDQUMxRCxTQUFTOzs7O1FBQUMsVUFBQyxRQUFnQjtZQUN2QixPQUFPLEtBQUksQ0FBQyxxQkFBcUIsQ0FBQyxNQUFNLEVBQUUsUUFBUSxDQUFDLENBQUM7UUFDeEQsQ0FBQyxFQUFDLENBQ0wsQ0FBQztJQUNOLENBQUM7SUFFRDs7Ozs7O09BTUc7Ozs7Ozs7O0lBQ0gsNERBQThCOzs7Ozs7O0lBQTlCLFVBQStCLE1BQWMsRUFBRSxlQUF1QixFQUFFLFNBQW1CO1FBQTNGLGlCQU1DO1FBTEcsT0FBTyxJQUFJLENBQUMsNEJBQTRCLENBQUMsZUFBZSxDQUFDLENBQUMsSUFBSSxDQUMxRCxTQUFTOzs7O1FBQUMsVUFBQyxRQUFnQjtZQUN2QixPQUFPLEtBQUksQ0FBQyw0QkFBNEIsQ0FBQyxNQUFNLEVBQUUsUUFBUSxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQzFFLENBQUMsRUFBQyxDQUNMLENBQUM7SUFDTixDQUFDO0lBRUQ7OztPQUdHOzs7OztJQUNILHNDQUFROzs7O0lBQVI7O1lBQ1UsR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUU7O1lBQ3pCLFVBQVUsR0FBRyxLQUFLOztZQUFFLFVBQVUsR0FBRyxFQUFFOztZQUFFLFdBQVcsR0FBRyxFQUFFOztZQUFFLFNBQVMsR0FBRyxFQUFFOztZQUFFLFlBQVksR0FBRyxFQUFFOztZQUMxRixVQUFVLEdBQUcsRUFBRTs7WUFBRSxTQUFTLEdBQUcsRUFBRTs7WUFBRSxZQUFZLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQzs7WUFBRSxPQUFPLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztRQUV4RyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FDdEUsR0FBRyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUN4QyxZQUFZLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQzlDLFlBQVksRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FBQyxDQUNyQyxDQUFDLElBQUksQ0FDRixHQUFHOzs7O1FBQUMsVUFBQyxRQUE2QjtZQUM5QixPQUFPLFFBQVEsQ0FBQztRQUNwQixDQUFDLEVBQUMsQ0FDTCxDQUFDO0lBQ04sQ0FBQztJQUVEOzs7O09BSUc7Ozs7OztJQUNILDBDQUFZOzs7OztJQUFaLFVBQWEsTUFBYzs7WUFDakIsR0FBRyxHQUFHLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxDQUFDOztZQUNoQyxVQUFVLEdBQUcsS0FBSzs7WUFBRSxVQUFVLEdBQUcsRUFBRTs7WUFBRSxXQUFXLEdBQUcsRUFBRTs7WUFBRSxTQUFTLEdBQUcsRUFBRTs7WUFBRSxZQUFZLEdBQUcsRUFBRTs7WUFDMUYsVUFBVSxHQUFHLEVBQUU7O1lBQUUsWUFBWSxHQUFHLENBQUMsa0JBQWtCLENBQUM7O1lBQUUsT0FBTyxHQUFHLENBQUMsa0JBQWtCLENBQUM7UUFFeEYsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQ3RFLEdBQUcsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFDeEMsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQ25DLFlBQVksRUFBRSxPQUFPLEVBQUUsTUFBTSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FDN0MsQ0FBQyxJQUFJLENBQ0YsR0FBRzs7OztRQUFDLFVBQUMsUUFBNkI7WUFDOUIsT0FBTyxRQUFRLENBQUM7UUFDcEIsQ0FBQyxFQUFDLENBQ0wsQ0FBQztJQUNOLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDRyw0REFBOEI7Ozs7O0lBQXBDLFVBQXFDLFNBQW1COzs7Ozs7d0JBQzlDLGFBQWEsR0FBd0IsRUFBRTs2QkFDekMsQ0FBQSxTQUFTLElBQUksU0FBUyxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUEsRUFBakMsd0JBQWlDO3dCQUNuQixxQkFBTSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsU0FBUyxFQUFFLEVBQUE7O3dCQUF6QyxLQUFLLEdBQUcsU0FBaUM7d0JBRXRDLENBQUMsR0FBRyxDQUFDOzs7NkJBQUUsQ0FBQSxDQUFDLEdBQUcsS0FBSyxDQUFDLE1BQU0sQ0FBQTt3QkFDVCxxQkFBTSxJQUFJLENBQUMsY0FBYyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxFQUFFLEVBQUUsU0FBUyxDQUFDLEVBQUE7O3dCQUE5RCxVQUFVLEdBQUcsU0FBaUQ7d0JBQ3BFLElBQUksVUFBVSxFQUFFOzRCQUNaLGFBQWEsQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUM7eUJBQ2hDOzs7d0JBSjZCLENBQUMsRUFBRSxDQUFBOzs0QkFRekMsc0JBQU8sYUFBYSxFQUFDOzs7O0tBQ3hCO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0csK0RBQWlDOzs7OztJQUF2QyxVQUF3QyxTQUFtQjs7Ozs7O3dCQUNqRCxhQUFhLEdBQXdCLEVBQUU7NkJBQ3pDLENBQUEsU0FBUyxJQUFJLFNBQVMsQ0FBQyxNQUFNLEdBQUcsQ0FBQyxDQUFBLEVBQWpDLHdCQUFpQzt3QkFDM0IsZ0JBQWMsSUFBSSxDQUFDLGtCQUFrQixFQUFFO3dCQUNqQyxxQkFBTSxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUMsU0FBUyxFQUFFLEVBQUE7O3dCQUF6QyxLQUFLLEdBQUcsU0FBaUM7d0JBRTdDLEtBQUssR0FBRyxLQUFLLENBQUMsTUFBTTs7Ozt3QkFBQyxVQUFDLElBQUksSUFBTyxPQUFPLElBQUksQ0FBQyxRQUFRLEtBQUssYUFBVyxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUMsRUFBQyxDQUFDO3dCQUUxRSxDQUFDLEdBQUcsQ0FBQzs7OzZCQUFFLENBQUEsQ0FBQyxHQUFHLEtBQUssQ0FBQyxNQUFNLENBQUE7d0JBQ1QscUJBQU0sSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsRUFBRSxFQUFFLFNBQVMsQ0FBQyxFQUFBOzt3QkFBOUQsVUFBVSxHQUFHLFNBQWlEO3dCQUNwRSxJQUFJLFVBQVUsRUFBRTs0QkFDWixhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO3lCQUNoQzs7O3dCQUo2QixDQUFDLEVBQUUsQ0FBQTs7NEJBUXpDLHNCQUFPLGFBQWEsRUFBQzs7OztLQUN4Qjs7Ozs7OztJQUVhLDRDQUFjOzs7Ozs7SUFBNUIsVUFBNkIsTUFBYyxFQUFFLFNBQW1COzs7Ozs0QkFDMUMscUJBQU0sSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxTQUFTLEVBQUUsRUFBQTs7d0JBQXZELFNBQVMsR0FBRyxTQUEyQzt3QkFDdkQsVUFBVSxHQUFHLFNBQVMsQ0FBQyxJQUFJOzs7O3dCQUFDLFVBQUMsUUFBUTs7Z0NBQ2pDLGFBQWEsR0FBRyxTQUFTLENBQUMsTUFBTTs7Ozs0QkFBQyxVQUFDLFFBQVE7Z0NBQzVDLE9BQU8sUUFBUSxDQUFDLElBQUksQ0FBQyxpQkFBaUIsRUFBRSxLQUFLLFFBQVEsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDOzRCQUM5RSxDQUFDLEVBQUM7NEJBRUYsT0FBTyxhQUFhLENBQUMsTUFBTSxHQUFHLENBQUMsQ0FBQzt3QkFDcEMsQ0FBQyxFQUFDO3dCQUVGLHNCQUFPLFVBQVUsRUFBQzs7OztLQUNyQjtJQUVEOzs7OztPQUtHOzs7Ozs7O0lBQ0gsOENBQWdCOzs7Ozs7SUFBaEIsVUFBaUIsTUFBYyxFQUFFLFNBQW1CO1FBQ2hELE9BQU8sSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBRzs7OztRQUFDLFVBQUMsU0FBOEI7O2dCQUNqRSxPQUFPLEdBQUcsS0FBSztZQUNuQixJQUFJLFNBQVMsSUFBSSxTQUFTLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtnQkFDbkMsU0FBUyxDQUFDLE9BQU87Ozs7Z0JBQUMsVUFBQyxRQUFnQjs7d0JBQ3pCLElBQUksR0FBRyxTQUFTLENBQUMsSUFBSTs7OztvQkFBQyxVQUFDLFFBQVE7d0JBQ2pDLE9BQU8sUUFBUSxLQUFLLFFBQVEsQ0FBQyxJQUFJLENBQUM7b0JBQ3RDLENBQUMsRUFBQztvQkFDRixJQUFJLElBQUksRUFBRTt3QkFDTixPQUFPLEdBQUcsSUFBSSxDQUFDO3dCQUNmLE9BQU87cUJBQ1Y7Z0JBQ0wsQ0FBQyxFQUFDLENBQUM7YUFDTjtZQUNELE9BQU8sT0FBTyxDQUFDO1FBQ25CLENBQUMsRUFBQyxDQUFDLENBQUM7SUFDUixDQUFDO0lBRUQ7OztPQUdHOzs7Ozs7SUFDSCx3Q0FBVTs7Ozs7SUFBVixVQUFXLFlBQWdEO1FBQTNELGlCQTRCQzs7WUEzQlMsR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUU7O1lBQ3pCLFVBQVUsR0FBRyxLQUFLOztZQUFFLFVBQVUsR0FBRyxFQUFFOztZQUN6QyxXQUFXLEdBQUcsRUFBRSxLQUFLLEVBQUUsWUFBWSxDQUFDLEtBQUssRUFBRSxHQUFHLEVBQUUsWUFBWSxDQUFDLEdBQUcsRUFBRTs7WUFBRSxTQUFTLEdBQUcsRUFBRTs7WUFBRSxZQUFZLEdBQUcsRUFBRTs7WUFDckcsVUFBVSxHQUFHLEVBQUU7O1lBQUUsU0FBUyxHQUFHLEVBQUU7O1lBQUUsWUFBWSxHQUFHLENBQUMsa0JBQWtCLENBQUM7UUFFcEUsT0FBTyxJQUFJLENBQUMsa0JBQWtCLEVBQUUsQ0FBQyxJQUFJLENBQzdCLFNBQVM7Ozs7UUFBQyxVQUFDLFVBQWU7WUFDMUIsT0FBQSxJQUFJLENBQUMsS0FBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQy9ELEdBQUcsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFDeEMsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUM5QyxZQUFZLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQUMsQ0FDbEMsQ0FBQyxJQUFJLENBQ0YsR0FBRzs7OztZQUFDLFVBQUMsUUFBNkI7Z0JBQzlCLE9BQU8sbUJBQTRCO29CQUMvQixPQUFPLEVBQUUsUUFBUTtvQkFDakIsVUFBVSxFQUFFO3dCQUNWLFNBQVMsRUFBRSxZQUFZLENBQUMsS0FBSzt3QkFDN0IsUUFBUSxFQUFFLFlBQVksQ0FBQyxHQUFHO3dCQUMxQixLQUFLLEVBQUUsVUFBVTt3QkFDakIsWUFBWSxFQUFFLEtBQUs7d0JBQ25CLFVBQVUsRUFBRSxVQUFVO3FCQUN2QjtpQkFDRixFQUFBLENBQUM7WUFDUixDQUFDLEVBQUMsRUFDRixVQUFVOzs7O1lBQUMsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUF2QixDQUF1QixFQUFDLENBQzdDO1FBbEJMLENBa0JLLEVBQUMsQ0FDYixDQUFDO0lBQ04sQ0FBQztJQUVEOzs7T0FHRzs7Ozs7SUFDSCxnREFBa0I7Ozs7SUFBbEI7UUFBQSxpQkFXQzs7WUFWUyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxHQUFHLFFBQVE7O1lBQ3BDLFlBQVksR0FBRyxDQUFDLGtCQUFrQixDQUFDOztZQUFFLE9BQU8sR0FBRyxDQUFDLGtCQUFrQixDQUFDO1FBQ3pFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUU7YUFDNUMsVUFBVSxDQUFDLGFBQWEsQ0FBQyxHQUFHLEVBQUUsS0FBSyxFQUNsQyxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksRUFDaEIsSUFBSSxFQUFFLElBQUksRUFBRSxZQUFZLEVBQ3hCLE9BQU8sRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FDeEIsQ0FBQyxDQUFDLElBQUksQ0FDTCxVQUFVOzs7O1FBQUMsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUF2QixDQUF1QixFQUFDLENBQ2pELENBQUM7SUFDVixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsd0NBQVU7Ozs7O0lBQVYsVUFBVyxPQUEwQjtRQUFyQyxpQkFhQzs7WUFaUyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRTs7WUFDekIsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsT0FBTyxDQUFDOztZQUNqQyxVQUFVLEdBQUcsTUFBTTs7WUFBRSxVQUFVLEdBQUcsRUFBRTs7WUFBRSxXQUFXLEdBQUcsRUFBRTs7WUFBRSxTQUFTLEdBQUcsT0FBTzs7WUFBRSxZQUFZLEdBQUcsRUFBRTs7WUFDcEcsVUFBVSxHQUFHLEVBQUU7O1lBQUUsWUFBWSxHQUFHLENBQUMsa0JBQWtCLENBQUM7O1lBQUUsT0FBTyxHQUFHLENBQUMsa0JBQWtCLENBQUM7UUFFcEYsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQzFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFDeEMsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQ25DLFlBQVksRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQ3RDLENBQUMsQ0FBQyxJQUFJLENBQ0gsVUFBVTs7OztRQUFDLFVBQUMsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBdkIsQ0FBdUIsRUFBQyxDQUNqRCxDQUFDO0lBQ04sQ0FBQztJQUVEOzs7OztPQUtHOzs7Ozs7O0lBQ0gsd0NBQVU7Ozs7OztJQUFWLFVBQVcsTUFBYyxFQUFFLFdBQThCO1FBQXpELGlCQWFDOztZQVpTLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLEdBQUcsR0FBRyxHQUFHLE1BQU07O1lBQ3hDLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQzs7WUFDckMsVUFBVSxHQUFHLEtBQUs7O1lBQUUsVUFBVSxHQUFHLEVBQUU7O1lBQUcsV0FBVyxHQUFHLEVBQUU7O1lBQUUsU0FBUyxHQUFHLE9BQU87O1lBQUUsWUFBWSxHQUFHLEVBQUU7O1lBQ3BHLFVBQVUsR0FBRyxFQUFFOztZQUFFLFlBQVksR0FBRyxDQUFDLGtCQUFrQixDQUFDOztZQUFFLE9BQU8sR0FBRyxDQUFDLGtCQUFrQixDQUFDO1FBRXBGLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUMxRSxHQUFHLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQ3hDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUNuQyxZQUFZLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUN0QyxDQUFDLENBQUMsSUFBSSxDQUNILFVBQVU7Ozs7UUFBQyxVQUFDLEtBQUssSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQXZCLENBQXVCLEVBQUMsQ0FDakQsQ0FBQztJQUNOLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCx3Q0FBVTs7Ozs7SUFBVixVQUFXLE1BQWM7UUFBekIsaUJBWUM7O1lBWFMsR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUUsR0FBRyxHQUFHLEdBQUcsTUFBTTs7WUFDeEMsVUFBVSxHQUFHLFFBQVE7O1lBQUUsVUFBVSxHQUFHLEVBQUU7O1lBQUcsV0FBVyxHQUFHLEVBQUU7O1lBQUUsU0FBUyxHQUFHLEVBQUU7O1lBQUUsWUFBWSxHQUFHLEVBQUU7O1lBQ2xHLFVBQVUsR0FBRyxFQUFFOztZQUFFLFlBQVksR0FBRyxDQUFDLGtCQUFrQixDQUFDOztZQUFFLE9BQU8sR0FBRyxDQUFDLGtCQUFrQixDQUFDO1FBRXBGLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUMxRSxHQUFHLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQ3hDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUNuQyxZQUFZLEVBQUUsT0FBTyxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUN0QyxDQUFDLENBQUMsSUFBSSxDQUNILFVBQVU7Ozs7UUFBQyxVQUFDLEtBQUssSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQXZCLENBQXVCLEVBQUMsQ0FDakQsQ0FBQztJQUNOLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILDRDQUFjOzs7Ozs7SUFBZCxVQUFlLE1BQWMsRUFBRSxXQUFzQztRQUFyRSxpQkFhQzs7WUFaUyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxHQUFHLEdBQUcsR0FBRyxNQUFNLEdBQUcsaUJBQWlCOztZQUM1RCxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUM7O1lBQ3JDLFVBQVUsR0FBRyxLQUFLOztZQUFFLFVBQVUsR0FBRyxFQUFFOztZQUFHLFdBQVcsR0FBRyxFQUFFOztZQUFFLFNBQVMsR0FBRyxPQUFPOztZQUFFLFlBQVksR0FBRyxFQUFFOztZQUNwRyxVQUFVLEdBQUcsRUFBRTs7WUFBRSxZQUFZLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQzs7WUFBRSxPQUFPLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztRQUVwRixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FDMUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUN4QyxZQUFZLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFDbkMsWUFBWSxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FDdEMsQ0FBQyxDQUFDLElBQUksQ0FDSCxVQUFVOzs7O1FBQUMsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUF2QixDQUF1QixFQUFDLENBQ2pELENBQUM7SUFDTixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsK0NBQWlCOzs7OztJQUFqQixVQUFrQixNQUFjO1FBQWhDLGlCQWFDOztZQVpTLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLEdBQUcsR0FBRyxHQUFHLE1BQU0sR0FBRyxVQUFVOztZQUNyRCxVQUFVLEdBQUcsS0FBSzs7WUFBRSxVQUFVLEdBQUcsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFDOztZQUNwRCxXQUFXLEdBQUcsRUFBRTs7WUFBRSxTQUFTLEdBQUcsRUFBRTs7WUFBRSxZQUFZLEdBQUcsRUFBRTs7WUFDbkQsVUFBVSxHQUFHLEVBQUU7O1lBQUUsU0FBUyxHQUFHLEVBQUU7O1lBQUUsWUFBWSxHQUFHLENBQUMsa0JBQWtCLENBQUM7UUFFcEUsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQzlELEdBQUcsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFDeEMsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUM5QyxZQUFZLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQzdCLENBQUMsQ0FBQyxJQUFJLENBQ0gsVUFBVTs7OztRQUFDLFVBQUMsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBdkIsQ0FBdUIsRUFBQyxDQUNqRCxDQUFDO0lBQ2xCLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7SUFDSCx1Q0FBUzs7Ozs7SUFBVCxVQUFVLGdCQUErQztRQUF6RCxpQkFhQzs7WUFaUyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxHQUFHLEdBQUcsR0FBRyxnQkFBZ0IsQ0FBQyxNQUFNLEdBQUcsVUFBVSxHQUFHLGdCQUFnQixDQUFDLE9BQU87O1lBQ2pHLE9BQU8sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLGdCQUFnQixDQUFDOztZQUMxQyxVQUFVLEdBQUcsS0FBSzs7WUFBRSxVQUFVLEdBQUcsRUFBRTs7WUFBRyxXQUFXLEdBQUcsRUFBRTs7WUFBRSxTQUFTLEdBQUcsT0FBTzs7WUFBRSxZQUFZLEdBQUcsRUFBRTs7WUFDcEcsVUFBVSxHQUFHLEVBQUU7O1lBQUUsWUFBWSxHQUFHLENBQUMsa0JBQWtCLENBQUM7O1lBQUUsT0FBTyxHQUFHLENBQUMsa0JBQWtCLENBQUM7UUFFcEYsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQzFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFDeEMsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQ25DLFlBQVksRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQ3RDLENBQUMsQ0FBQyxJQUFJLENBQ0gsVUFBVTs7OztRQUFDLFVBQUMsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBdkIsQ0FBdUIsRUFBQyxDQUNqRCxDQUFDO0lBQ04sQ0FBQztJQUVEOzs7OztPQUtHOzs7Ozs7O0lBQ0gsd0NBQVU7Ozs7OztJQUFWLFVBQVcsTUFBVyxFQUFFLE9BQWU7UUFBdkMsaUJBWUM7O1lBWFMsR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUUsR0FBRyxHQUFHLEdBQUcsTUFBTSxHQUFHLFVBQVUsR0FBRyxPQUFPOztZQUMvRCxVQUFVLEdBQUcsUUFBUTs7WUFBRSxVQUFVLEdBQUcsRUFBRTs7WUFBRyxXQUFXLEdBQUcsRUFBRTs7WUFBRSxTQUFTLEdBQUcsRUFBRTs7WUFBRSxZQUFZLEdBQUcsRUFBRTs7WUFDbEcsVUFBVSxHQUFHLEVBQUU7O1lBQUUsWUFBWSxHQUFHLENBQUMsa0JBQWtCLENBQUM7O1lBQUUsT0FBTyxHQUFHLENBQUMsa0JBQWtCLENBQUM7UUFFcEYsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQzFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFDeEMsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQ25DLFlBQVksRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQ3RDLENBQUMsQ0FBQyxJQUFJLENBQ0gsVUFBVTs7OztRQUFDLFVBQUMsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBdkIsQ0FBdUIsRUFBQyxDQUNqRCxDQUFDO0lBQ04sQ0FBQztJQUVEOzs7O09BSUc7Ozs7OztJQUNILCtDQUFpQjs7Ozs7SUFBakIsVUFBa0IsTUFBYztRQUFoQyxpQkFhQzs7WUFaUyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxHQUFHLEdBQUcsR0FBRyxNQUFNLEdBQUcsZ0NBQWdDOztZQUMzRSxVQUFVLEdBQUcsS0FBSzs7WUFBRSxVQUFVLEdBQUcsRUFBRTs7WUFDekMsV0FBVyxHQUFHLEVBQUU7O1lBQUUsU0FBUyxHQUFHLEVBQUU7O1lBQUUsWUFBWSxHQUFHLEVBQUU7O1lBQ25ELFVBQVUsR0FBRyxFQUFFOztZQUFFLFNBQVMsR0FBRyxFQUFFOztZQUFFLFlBQVksR0FBRyxDQUFDLGtCQUFrQixDQUFDO1FBRXBFLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUM5RCxHQUFHLEVBQUUsVUFBVSxFQUFFLFVBQVUsRUFBRSxXQUFXLEVBQ3hDLFlBQVksRUFBRSxVQUFVLEVBQUUsU0FBUyxFQUFFLFNBQVMsRUFDOUMsWUFBWSxFQUFFLElBQUksRUFBRSxJQUFJLEVBQUUsSUFBSSxDQUM3QixDQUFDLENBQUMsSUFBSSxDQUNILFVBQVU7Ozs7UUFBQyxVQUFDLEtBQUssSUFBSyxPQUFBLEtBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDLEVBQXZCLENBQXVCLEVBQUMsQ0FDakQsQ0FBQztJQUNsQixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7O0lBQ0gsOENBQWdCOzs7OztJQUFoQixVQUFpQixNQUFjO1FBQS9CLGlCQWFDOztZQVpTLEdBQUcsR0FBRyxJQUFJLENBQUMsWUFBWSxFQUFFLEdBQUcsR0FBRyxHQUFHLE1BQU0sR0FBRyxzQkFBc0I7O1lBQ2pFLFVBQVUsR0FBRyxLQUFLOztZQUFFLFVBQVUsR0FBRyxFQUFFLEVBQUUsRUFBRSxNQUFNLEVBQUM7O1lBQ3BELFdBQVcsR0FBRyxFQUFFOztZQUFFLFNBQVMsR0FBRyxFQUFFOztZQUFFLFlBQVksR0FBRyxFQUFFOztZQUNuRCxVQUFVLEdBQUcsRUFBRTs7WUFBRSxTQUFTLEdBQUcsRUFBRTs7WUFBRSxZQUFZLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztRQUVwRSxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FDOUQsR0FBRyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUN4QyxZQUFZLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFBRSxTQUFTLEVBQzlDLFlBQVksRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FDN0IsQ0FBQyxDQUFDLElBQUksQ0FDSCxVQUFVOzs7O1FBQUMsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUF2QixDQUF1QixFQUFDLENBQ2pELENBQUM7SUFDbEIsQ0FBQztJQUVEOzs7O09BSUc7Ozs7OztJQUNILCtDQUFpQjs7Ozs7SUFBakIsVUFBa0IsTUFBYztRQUFoQyxpQkFhQzs7WUFaUyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxHQUFHLEdBQUcsR0FBRyxNQUFNLEdBQUcsZ0NBQWdDOztZQUMzRSxVQUFVLEdBQUcsS0FBSzs7WUFBRSxVQUFVLEdBQUcsRUFBRSxFQUFFLEVBQUUsTUFBTSxFQUFDOztZQUNwRCxXQUFXLEdBQUcsRUFBRTs7WUFBRSxTQUFTLEdBQUcsRUFBRTs7WUFBRSxZQUFZLEdBQUcsRUFBRTs7WUFDbkQsVUFBVSxHQUFHLEVBQUU7O1lBQUUsU0FBUyxHQUFHLEVBQUU7O1lBQUUsWUFBWSxHQUFHLENBQUMsa0JBQWtCLENBQUM7UUFFcEUsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQzlELEdBQUcsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFDeEMsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQUUsU0FBUyxFQUM5QyxZQUFZLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQzdCLENBQUMsQ0FBQyxJQUFJLENBQ0gsVUFBVTs7OztRQUFDLFVBQUMsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBdkIsQ0FBdUIsRUFBQyxDQUNqRCxDQUFDO0lBQ2xCLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7OztJQUNILHlDQUFXOzs7Ozs7SUFBWCxVQUFZLE1BQWMsRUFBRSxLQUEwQjtRQUF0RCxpQkFhQzs7WUFaUyxHQUFHLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxHQUFHLEdBQUcsR0FBRyxNQUFNLEdBQUcsc0JBQXNCOztZQUNqRSxPQUFPLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUM7O1lBQy9CLFVBQVUsR0FBRyxNQUFNOztZQUFFLFVBQVUsR0FBRyxFQUFFOztZQUFHLFdBQVcsR0FBRyxFQUFFOztZQUFFLFNBQVMsR0FBRyxPQUFPOztZQUFFLFlBQVksR0FBRyxFQUFFOztZQUNyRyxVQUFVLEdBQUcsRUFBRTs7WUFBRSxZQUFZLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQzs7WUFBRSxPQUFPLEdBQUcsQ0FBQyxrQkFBa0IsQ0FBQztRQUVwRixPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsa0JBQWtCLENBQUMsV0FBVyxFQUFFLENBQUMsVUFBVSxDQUFDLGFBQWEsQ0FDMUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUN4QyxZQUFZLEVBQUUsVUFBVSxFQUFFLFNBQVMsRUFDbkMsWUFBWSxFQUFFLE9BQU8sRUFBRSxJQUFJLEVBQUUsSUFBSSxFQUFFLElBQUksQ0FDdEMsQ0FBQyxDQUFDLElBQUksQ0FDSCxVQUFVOzs7O1FBQUMsVUFBQyxLQUFLLElBQUssT0FBQSxLQUFJLENBQUMsV0FBVyxDQUFDLEtBQUssQ0FBQyxFQUF2QixDQUF1QixFQUFDLENBQ2pELENBQUM7SUFDTixDQUFDO0lBRUQ7Ozs7O09BS0c7Ozs7Ozs7SUFDSCx5Q0FBVzs7Ozs7O0lBQVgsVUFBWSxNQUFjLEVBQUUsWUFBaUM7UUFBN0QsaUJBYUM7O1lBWlMsR0FBRyxHQUFHLElBQUksQ0FBQyxZQUFZLEVBQUUsR0FBRyxHQUFHLEdBQUcsTUFBTSxHQUFHLHNCQUFzQjs7WUFDakUsT0FBTyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsWUFBWSxDQUFDOztZQUN0QyxVQUFVLEdBQUcsUUFBUTs7WUFBRSxVQUFVLEdBQUcsRUFBRTs7WUFBRyxXQUFXLEdBQUcsRUFBRTs7WUFBRSxTQUFTLEdBQUcsT0FBTzs7WUFBRSxZQUFZLEdBQUcsRUFBRTs7WUFDdkcsVUFBVSxHQUFHLEVBQUU7O1lBQUUsWUFBWSxHQUFHLENBQUMsa0JBQWtCLENBQUM7O1lBQUUsT0FBTyxHQUFHLENBQUMsa0JBQWtCLENBQUM7UUFFcEYsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLGtCQUFrQixDQUFDLFdBQVcsRUFBRSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQzFFLEdBQUcsRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFDeEMsWUFBWSxFQUFFLFVBQVUsRUFBRSxTQUFTLEVBQ25DLFlBQVksRUFBRSxPQUFPLEVBQUUsSUFBSSxFQUFFLElBQUksRUFBRSxJQUFJLENBQ3RDLENBQUMsQ0FBQyxJQUFJLENBQ0gsVUFBVTs7OztRQUFDLFVBQUMsS0FBSyxJQUFLLE9BQUEsS0FBSSxDQUFDLFdBQVcsQ0FBQyxLQUFLLENBQUMsRUFBdkIsQ0FBdUIsRUFBQyxDQUNqRCxDQUFDO0lBQ04sQ0FBQzs7Ozs7SUFFTywwQ0FBWTs7OztJQUFwQjtRQUNJLE9BQVUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsV0FBUSxDQUFDO0lBQ2hFLENBQUM7Ozs7Ozs7SUFFTyx3REFBMEI7Ozs7OztJQUFsQyxVQUFtQyxNQUFjLEVBQUUsUUFBZ0I7UUFDL0QsT0FBVSxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFDLGNBQWMsQ0FBQyxlQUFVLE1BQU0sK0JBQTBCLFFBQVUsQ0FBQztJQUM1RyxDQUFDOzs7Ozs7SUFFTywyQ0FBYTs7Ozs7SUFBckIsVUFBc0IsTUFBYztRQUNoQyxPQUFVLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQUMsY0FBYyxDQUFDLGVBQVUsTUFBTSxtQ0FBZ0MsQ0FBQztJQUN4RyxDQUFDOzs7OztJQUVPLGdEQUFrQjs7OztJQUExQjtRQUNJLE9BQVUsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxjQUFjLENBQUMsYUFBVSxDQUFDO0lBQ2xFLENBQUM7SUFFRDs7O09BR0c7Ozs7Ozs7SUFDSyx5Q0FBVzs7Ozs7O0lBQW5CLFVBQW9CLEtBQWU7UUFDL0IsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDN0IsT0FBTyxVQUFVLENBQUMsS0FBSyxJQUFJLGNBQWMsQ0FBQyxDQUFDO0lBQy9DLENBQUM7O2dCQWpwQkosVUFBVSxTQUFDO29CQUNSLFVBQVUsRUFBRSxNQUFNO2lCQUNyQjs7OztnQkFUUSxnQkFBZ0I7Z0JBR2hCLGtCQUFrQjtnQkFEbEIsZ0JBQWdCO2dCQURoQixVQUFVOzs7OEJBN0JuQjtDQXFyQkMsQUFscEJELElBa3BCQztTQS9vQlksbUJBQW1COzs7Ozs7SUFHeEIsK0NBQTBDOzs7OztJQUMxQyxpREFBOEM7Ozs7O0lBQzlDLCtDQUEwQzs7Ozs7SUFDMUMseUNBQThCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IEluamVjdGFibGUgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgT2JzZXJ2YWJsZSwgb2YsIGZyb20sIHRocm93RXJyb3IgfSBmcm9tICdyeGpzJztcclxuaW1wb3J0IHsgY2F0Y2hFcnJvciwgbWFwLCBzd2l0Y2hNYXAgfSBmcm9tICdyeGpzL29wZXJhdG9ycyc7XHJcblxyXG5pbXBvcnQge1xyXG4gICAgSWRlbnRpdHlVc2VyTW9kZWwsXHJcbiAgICBJZGVudGl0eVVzZXJRdWVyeVJlc3BvbnNlLFxyXG4gICAgSWRlbnRpdHlVc2VyUXVlcnlDbG91ZFJlcXVlc3RNb2RlbCxcclxuICAgIElkZW50aXR5VXNlclBhc3N3b3JkTW9kZWwsXHJcbiAgICBJZGVudGl0eUpvaW5Hcm91cFJlcXVlc3RNb2RlbFxyXG59IGZyb20gJy4uL21vZGVscy9pZGVudGl0eS11c2VyLm1vZGVsJztcclxuaW1wb3J0IHsgSnd0SGVscGVyU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2p3dC1oZWxwZXIuc2VydmljZSc7XHJcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9sb2cuc2VydmljZSc7XHJcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9hcHAtY29uZmlnL2FwcC1jb25maWcuc2VydmljZSc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgSWRlbnRpdHlSb2xlTW9kZWwgfSBmcm9tICcuLi9tb2RlbHMvaWRlbnRpdHktcm9sZS5tb2RlbCc7XHJcbmltcG9ydCB7IElkZW50aXR5R3JvdXBNb2RlbCB9IGZyb20gJy4uL21vZGVscy9pZGVudGl0eS1ncm91cC5tb2RlbCc7XHJcblxyXG5ASW5qZWN0YWJsZSh7XHJcbiAgICBwcm92aWRlZEluOiAncm9vdCdcclxufSlcclxuZXhwb3J0IGNsYXNzIElkZW50aXR5VXNlclNlcnZpY2Uge1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKFxyXG4gICAgICAgIHByaXZhdGUgand0SGVscGVyU2VydmljZTogSnd0SGVscGVyU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIGFsZnJlc2NvQXBpU2VydmljZTogQWxmcmVzY29BcGlTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgYXBwQ29uZmlnU2VydmljZTogQXBwQ29uZmlnU2VydmljZSxcclxuICAgICAgICBwcml2YXRlIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UpIHsgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyB0aGUgbmFtZSBhbmQgb3RoZXIgYmFzaWMgZGV0YWlscyBvZiB0aGUgY3VycmVudCB1c2VyLlxyXG4gICAgICogQHJldHVybnMgVGhlIHVzZXIncyBkZXRhaWxzXHJcbiAgICAgKi9cclxuICAgIGdldEN1cnJlbnRVc2VySW5mbygpOiBJZGVudGl0eVVzZXJNb2RlbCB7XHJcbiAgICAgICAgY29uc3QgZmFtaWx5TmFtZSA9IHRoaXMuand0SGVscGVyU2VydmljZS5nZXRWYWx1ZUZyb21Mb2NhbEFjY2Vzc1Rva2VuPHN0cmluZz4oSnd0SGVscGVyU2VydmljZS5GQU1JTFlfTkFNRSk7XHJcbiAgICAgICAgY29uc3QgZ2l2ZW5OYW1lID0gdGhpcy5qd3RIZWxwZXJTZXJ2aWNlLmdldFZhbHVlRnJvbUxvY2FsQWNjZXNzVG9rZW48c3RyaW5nPihKd3RIZWxwZXJTZXJ2aWNlLkdJVkVOX05BTUUpO1xyXG4gICAgICAgIGNvbnN0IGVtYWlsID0gdGhpcy5qd3RIZWxwZXJTZXJ2aWNlLmdldFZhbHVlRnJvbUxvY2FsQWNjZXNzVG9rZW48c3RyaW5nPihKd3RIZWxwZXJTZXJ2aWNlLlVTRVJfRU1BSUwpO1xyXG4gICAgICAgIGNvbnN0IHVzZXJuYW1lID0gdGhpcy5qd3RIZWxwZXJTZXJ2aWNlLmdldFZhbHVlRnJvbUxvY2FsQWNjZXNzVG9rZW48c3RyaW5nPihKd3RIZWxwZXJTZXJ2aWNlLlVTRVJfUFJFRkVSUkVEX1VTRVJOQU1FKTtcclxuICAgICAgICBjb25zdCB1c2VyID0geyBmaXJzdE5hbWU6IGdpdmVuTmFtZSwgbGFzdE5hbWU6IGZhbWlseU5hbWUsIGVtYWlsOiBlbWFpbCwgdXNlcm5hbWU6IHVzZXJuYW1lIH07XHJcbiAgICAgICAgcmV0dXJuIG5ldyBJZGVudGl0eVVzZXJNb2RlbCh1c2VyKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZpbmQgdXNlcnMgYmFzZWQgb24gc2VhcmNoIGlucHV0LlxyXG4gICAgICogQHBhcmFtIHNlYXJjaCBTZWFyY2ggcXVlcnkgc3RyaW5nXHJcbiAgICAgKiBAcmV0dXJucyBMaXN0IG9mIHVzZXJzXHJcbiAgICAgKi9cclxuICAgIGZpbmRVc2Vyc0J5TmFtZShzZWFyY2g6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgaWYgKHNlYXJjaCA9PT0gJycpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG9mKFtdKTtcclxuICAgICAgICB9XHJcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5idWlsZFVzZXJVcmwoKTtcclxuICAgICAgICBjb25zdCBodHRwTWV0aG9kID0gJ0dFVCcsIHBhdGhQYXJhbXMgPSB7fSwgcXVlcnlQYXJhbXMgPSB7IHNlYXJjaDogc2VhcmNoIH0sIGJvZHlQYXJhbSA9IHt9LCBoZWFkZXJQYXJhbXMgPSB7fSxcclxuICAgICAgICAgICAgZm9ybVBhcmFtcyA9IHt9LCBjb250ZW50VHlwZXMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXSwgYWNjZXB0cyA9IFsnYXBwbGljYXRpb24vanNvbiddO1xyXG5cclxuICAgICAgICByZXR1cm4gKGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgICAgIGhlYWRlclBhcmFtcywgZm9ybVBhcmFtcywgYm9keVBhcmFtLFxyXG4gICAgICAgICAgICBjb250ZW50VHlwZXMsIGFjY2VwdHMsIE9iamVjdCwgbnVsbCwgbnVsbClcclxuICAgICAgICApKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZpbmQgdXNlcnMgYmFzZWQgb24gdXNlcm5hbWUgaW5wdXQuXHJcbiAgICAgKiBAcGFyYW0gdXNlcm5hbWUgU2VhcmNoIHF1ZXJ5IHN0cmluZ1xyXG4gICAgICogQHJldHVybnMgTGlzdCBvZiB1c2Vyc1xyXG4gICAgICovXHJcbiAgICBmaW5kVXNlckJ5VXNlcm5hbWUodXNlcm5hbWU6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgaWYgKHVzZXJuYW1lID09PSAnJykge1xyXG4gICAgICAgICAgICByZXR1cm4gb2YoW10pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlclVybCgpO1xyXG4gICAgICAgIGNvbnN0IGh0dHBNZXRob2QgPSAnR0VUJywgcGF0aFBhcmFtcyA9IHt9LCBxdWVyeVBhcmFtcyA9IHsgdXNlcm5hbWU6IHVzZXJuYW1lIH0sIGJvZHlQYXJhbSA9IHt9LCBoZWFkZXJQYXJhbXMgPSB7fSxcclxuICAgICAgICAgICAgZm9ybVBhcmFtcyA9IHt9LCBjb250ZW50VHlwZXMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXSwgYWNjZXB0cyA9IFsnYXBwbGljYXRpb24vanNvbiddO1xyXG5cclxuICAgICAgICByZXR1cm4gKGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgICAgIGhlYWRlclBhcmFtcywgZm9ybVBhcmFtcywgYm9keVBhcmFtLFxyXG4gICAgICAgICAgICBjb250ZW50VHlwZXMsIGFjY2VwdHMsIE9iamVjdCwgbnVsbCwgbnVsbClcclxuICAgICAgICApKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZpbmQgdXNlcnMgYmFzZWQgb24gZW1haWwgaW5wdXQuXHJcbiAgICAgKiBAcGFyYW0gZW1haWwgU2VhcmNoIHF1ZXJ5IHN0cmluZ1xyXG4gICAgICogQHJldHVybnMgTGlzdCBvZiB1c2Vyc1xyXG4gICAgICovXHJcbiAgICBmaW5kVXNlckJ5RW1haWwoZW1haWw6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgaWYgKGVtYWlsID09PSAnJykge1xyXG4gICAgICAgICAgICByZXR1cm4gb2YoW10pO1xyXG4gICAgICAgIH1cclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlclVybCgpO1xyXG4gICAgICAgIGNvbnN0IGh0dHBNZXRob2QgPSAnR0VUJywgcGF0aFBhcmFtcyA9IHt9LCBxdWVyeVBhcmFtcyA9IHsgZW1haWw6IGVtYWlsIH0sIGJvZHlQYXJhbSA9IHt9LCBoZWFkZXJQYXJhbXMgPSB7fSxcclxuICAgICAgICAgICAgZm9ybVBhcmFtcyA9IHt9LCBjb250ZW50VHlwZXMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXSwgYWNjZXB0cyA9IFsnYXBwbGljYXRpb24vanNvbiddO1xyXG5cclxuICAgICAgICByZXR1cm4gKGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgICAgIGhlYWRlclBhcmFtcywgZm9ybVBhcmFtcywgYm9keVBhcmFtLFxyXG4gICAgICAgICAgICBjb250ZW50VHlwZXMsIGFjY2VwdHMsIE9iamVjdCwgbnVsbCwgbnVsbClcclxuICAgICAgICApKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEZpbmQgdXNlcnMgYmFzZWQgb24gaWQgaW5wdXQuXHJcbiAgICAgKiBAcGFyYW0gaWQgU2VhcmNoIHF1ZXJ5IHN0cmluZ1xyXG4gICAgICogQHJldHVybnMgdXNlcnMgb2JqZWN0XHJcbiAgICAgKi9cclxuICAgIGZpbmRVc2VyQnlJZChpZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBpZiAoaWQgPT09ICcnKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBvZihbXSk7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuYnVpbGRVc2VyVXJsKCkgKyAnLycgKyBpZDtcclxuICAgICAgICBjb25zdCBodHRwTWV0aG9kID0gJ0dFVCcsIHBhdGhQYXJhbXMgPSB7fSwgcXVlcnlQYXJhbXMgPSB7fSwgYm9keVBhcmFtID0ge30sIGhlYWRlclBhcmFtcyA9IHt9LFxyXG4gICAgICAgICAgICBmb3JtUGFyYW1zID0ge30sIGNvbnRlbnRUeXBlcyA9IFsnYXBwbGljYXRpb24vanNvbiddLCBhY2NlcHRzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiAoZnJvbSh0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLm9hdXRoMkF1dGguY2FsbEN1c3RvbUFwaShcclxuICAgICAgICAgICAgdXJsLCBodHRwTWV0aG9kLCBwYXRoUGFyYW1zLCBxdWVyeVBhcmFtcyxcclxuICAgICAgICAgICAgaGVhZGVyUGFyYW1zLCBmb3JtUGFyYW1zLCBib2R5UGFyYW0sXHJcbiAgICAgICAgICAgIGNvbnRlbnRUeXBlcywgYWNjZXB0cywgT2JqZWN0LCBudWxsLCBudWxsKVxyXG4gICAgICAgICkpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0IGNsaWVudCByb2xlcyBvZiBhIHVzZXIgZm9yIGEgcGFydGljdWxhciBjbGllbnQuXHJcbiAgICAgKiBAcGFyYW0gdXNlcklkIElEIG9mIHRoZSB0YXJnZXQgdXNlclxyXG4gICAgICogQHBhcmFtIGNsaWVudElkIElEIG9mIHRoZSBjbGllbnQgYXBwXHJcbiAgICAgKiBAcmV0dXJucyBMaXN0IG9mIGNsaWVudCByb2xlc1xyXG4gICAgICovXHJcbiAgICBnZXRDbGllbnRSb2xlcyh1c2VySWQ6IHN0cmluZywgY2xpZW50SWQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55W10+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlckNsaWVudFJvbGVNYXBwaW5nKHVzZXJJZCwgY2xpZW50SWQpO1xyXG4gICAgICAgIGNvbnN0IGh0dHBNZXRob2QgPSAnR0VUJywgcGF0aFBhcmFtcyA9IHt9LCBxdWVyeVBhcmFtcyA9IHt9LCBib2R5UGFyYW0gPSB7fSwgaGVhZGVyUGFyYW1zID0ge30sXHJcbiAgICAgICAgICAgIGZvcm1QYXJhbXMgPSB7fSwgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ10sIGFjY2VwdHMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgICAgIGhlYWRlclBhcmFtcywgZm9ybVBhcmFtcywgYm9keVBhcmFtLFxyXG4gICAgICAgICAgICBjb250ZW50VHlwZXMsIGFjY2VwdHMsIE9iamVjdCwgbnVsbCwgbnVsbClcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2tzIHdoZXRoZXIgdXNlciBoYXMgYWNjZXNzIHRvIGEgY2xpZW50IGFwcC5cclxuICAgICAqIEBwYXJhbSB1c2VySWQgSUQgb2YgdGhlIHRhcmdldCB1c2VyXHJcbiAgICAgKiBAcGFyYW0gY2xpZW50SWQgSUQgb2YgdGhlIGNsaWVudCBhcHBcclxuICAgICAqIEByZXR1cm5zIFRydWUgaWYgdGhlIHVzZXIgaGFzIGFjY2VzcywgZmFsc2Ugb3RoZXJ3aXNlXHJcbiAgICAgKi9cclxuICAgIGNoZWNrVXNlckhhc0NsaWVudEFwcCh1c2VySWQ6IHN0cmluZywgY2xpZW50SWQ6IHN0cmluZyk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldENsaWVudFJvbGVzKHVzZXJJZCwgY2xpZW50SWQpLnBpcGUoXHJcbiAgICAgICAgICAgIG1hcCgoY2xpZW50Um9sZXM6IGFueVtdKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAoY2xpZW50Um9sZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVja3Mgd2hldGhlciBhIHVzZXIgaGFzIGFueSBvZiB0aGUgY2xpZW50IGFwcCByb2xlcy5cclxuICAgICAqIEBwYXJhbSB1c2VySWQgSUQgb2YgdGhlIHRhcmdldCB1c2VyXHJcbiAgICAgKiBAcGFyYW0gY2xpZW50SWQgSUQgb2YgdGhlIGNsaWVudCBhcHBcclxuICAgICAqIEBwYXJhbSByb2xlTmFtZXMgTGlzdCBvZiByb2xlIG5hbWVzIHRvIGNoZWNrIGZvclxyXG4gICAgICogQHJldHVybnMgVHJ1ZSBpZiB0aGUgdXNlciBoYXMgb25lIG9yIG1vcmUgb2YgdGhlIHJvbGVzLCBmYWxzZSBvdGhlcndpc2VcclxuICAgICAqL1xyXG4gICAgY2hlY2tVc2VySGFzQW55Q2xpZW50QXBwUm9sZSh1c2VySWQ6IHN0cmluZywgY2xpZW50SWQ6IHN0cmluZywgcm9sZU5hbWVzOiBzdHJpbmdbXSk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldENsaWVudFJvbGVzKHVzZXJJZCwgY2xpZW50SWQpLnBpcGUoXHJcbiAgICAgICAgICAgIG1hcCgoY2xpZW50Um9sZXM6IGFueVtdKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBsZXQgaGFzUm9sZSA9IGZhbHNlO1xyXG4gICAgICAgICAgICAgICAgaWYgKGNsaWVudFJvbGVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICByb2xlTmFtZXMuZm9yRWFjaCgocm9sZU5hbWUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgY29uc3Qgcm9sZSA9IGNsaWVudFJvbGVzLmZpbmQoKGF2YWlsYWJsZVJvbGUpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiBhdmFpbGFibGVSb2xlLm5hbWUgPT09IHJvbGVOYW1lO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChyb2xlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBoYXNSb2xlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGhhc1JvbGU7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgdGhlIGNsaWVudCBJRCBmb3IgYW4gYXBwbGljYXRpb24uXHJcbiAgICAgKiBAcGFyYW0gYXBwbGljYXRpb25OYW1lIE5hbWUgb2YgdGhlIGFwcGxpY2F0aW9uXHJcbiAgICAgKiBAcmV0dXJucyBDbGllbnQgSUQgc3RyaW5nXHJcbiAgICAgKi9cclxuICAgIGdldENsaWVudElkQnlBcHBsaWNhdGlvbk5hbWUoYXBwbGljYXRpb25OYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPHN0cmluZz4ge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuYnVpbGRHZXRDbGllbnRzVXJsKCk7XHJcbiAgICAgICAgY29uc3QgaHR0cE1ldGhvZCA9ICdHRVQnLCBwYXRoUGFyYW1zID0ge30sIHF1ZXJ5UGFyYW1zID0geyBjbGllbnRJZDogYXBwbGljYXRpb25OYW1lIH0sIGJvZHlQYXJhbSA9IHt9LCBoZWFkZXJQYXJhbXMgPSB7fSwgZm9ybVBhcmFtcyA9IHt9LFxyXG4gICAgICAgICAgICBjb250ZW50VHlwZXMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXSwgYWNjZXB0cyA9IFsnYXBwbGljYXRpb24vanNvbiddO1xyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKClcclxuICAgICAgICAgICAgLm9hdXRoMkF1dGguY2FsbEN1c3RvbUFwaSh1cmwsIGh0dHBNZXRob2QsIHBhdGhQYXJhbXMsIHF1ZXJ5UGFyYW1zLCBoZWFkZXJQYXJhbXMsXHJcbiAgICAgICAgICAgICAgICBmb3JtUGFyYW1zLCBib2R5UGFyYW0sIGNvbnRlbnRUeXBlcyxcclxuICAgICAgICAgICAgICAgIGFjY2VwdHMsIE9iamVjdCwgbnVsbCwgbnVsbClcclxuICAgICAgICApLnBpcGUoXHJcbiAgICAgICAgICAgIG1hcCgocmVzcG9uc2U6IGFueVtdKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBjbGllbnRJZCA9IHJlc3BvbnNlICYmIHJlc3BvbnNlLmxlbmd0aCA+IDAgPyByZXNwb25zZVswXS5pZCA6ICcnO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGNsaWVudElkO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVja3MgaWYgYSB1c2VyIGhhcyBhY2Nlc3MgdG8gYW4gYXBwbGljYXRpb24uXHJcbiAgICAgKiBAcGFyYW0gdXNlcklkIElEIG9mIHRoZSB1c2VyXHJcbiAgICAgKiBAcGFyYW0gYXBwbGljYXRpb25OYW1lIE5hbWUgb2YgdGhlIGFwcGxpY2F0aW9uXHJcbiAgICAgKiBAcmV0dXJucyBUcnVlIGlmIHRoZSB1c2VyIGhhcyBhY2Nlc3MsIGZhbHNlIG90aGVyd2lzZVxyXG4gICAgICovXHJcbiAgICBjaGVja1VzZXJIYXNBcHBsaWNhdGlvbkFjY2Vzcyh1c2VySWQ6IHN0cmluZywgYXBwbGljYXRpb25OYW1lOiBzdHJpbmcpOiBPYnNlcnZhYmxlPGJvb2xlYW4+IHtcclxuICAgICAgICByZXR1cm4gdGhpcy5nZXRDbGllbnRJZEJ5QXBwbGljYXRpb25OYW1lKGFwcGxpY2F0aW9uTmFtZSkucGlwZShcclxuICAgICAgICAgICAgc3dpdGNoTWFwKChjbGllbnRJZDogc3RyaW5nKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5jaGVja1VzZXJIYXNDbGllbnRBcHAodXNlcklkLCBjbGllbnRJZCk7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENoZWNrcyBpZiBhIHVzZXIgaGFzIGFueSBhcHBsaWNhdGlvbiByb2xlLlxyXG4gICAgICogQHBhcmFtIHVzZXJJZCBJRCBvZiB0aGUgdGFyZ2V0IHVzZXJcclxuICAgICAqIEBwYXJhbSBhcHBsaWNhdGlvbk5hbWUgTmFtZSBvZiB0aGUgYXBwbGljYXRpb25cclxuICAgICAqIEBwYXJhbSByb2xlTmFtZXMgTGlzdCBvZiByb2xlIG5hbWVzIHRvIGNoZWNrIGZvclxyXG4gICAgICogQHJldHVybnMgVHJ1ZSBpZiB0aGUgdXNlciBoYXMgb25lIG9yIG1vcmUgb2YgdGhlIHJvbGVzLCBmYWxzZSBvdGhlcndpc2VcclxuICAgICAqL1xyXG4gICAgY2hlY2tVc2VySGFzQW55QXBwbGljYXRpb25Sb2xlKHVzZXJJZDogc3RyaW5nLCBhcHBsaWNhdGlvbk5hbWU6IHN0cmluZywgcm9sZU5hbWVzOiBzdHJpbmdbXSk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldENsaWVudElkQnlBcHBsaWNhdGlvbk5hbWUoYXBwbGljYXRpb25OYW1lKS5waXBlKFxyXG4gICAgICAgICAgICBzd2l0Y2hNYXAoKGNsaWVudElkOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmNoZWNrVXNlckhhc0FueUNsaWVudEFwcFJvbGUodXNlcklkLCBjbGllbnRJZCwgcm9sZU5hbWVzKTtcclxuICAgICAgICAgICAgfSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBkZXRhaWxzIGZvciBhbGwgdXNlcnMuXHJcbiAgICAgKiBAcmV0dXJucyBBcnJheSBvZiB1c2VyIGluZm8gb2JqZWN0c1xyXG4gICAgICovXHJcbiAgICBnZXRVc2VycygpOiBPYnNlcnZhYmxlPElkZW50aXR5VXNlck1vZGVsW10+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlclVybCgpO1xyXG4gICAgICAgIGNvbnN0IGh0dHBNZXRob2QgPSAnR0VUJywgcGF0aFBhcmFtcyA9IHt9LCBxdWVyeVBhcmFtcyA9IHt9LCBib2R5UGFyYW0gPSB7fSwgaGVhZGVyUGFyYW1zID0ge30sXHJcbiAgICAgICAgICAgIGZvcm1QYXJhbXMgPSB7fSwgYXV0aE5hbWVzID0gW10sIGNvbnRlbnRUeXBlcyA9IFsnYXBwbGljYXRpb24vanNvbiddLCBhY2NlcHRzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkub2F1dGgyQXV0aC5jYWxsQ3VzdG9tQXBpKFxyXG4gICAgICAgICAgICB1cmwsIGh0dHBNZXRob2QsIHBhdGhQYXJhbXMsIHF1ZXJ5UGFyYW1zLFxyXG4gICAgICAgICAgICBoZWFkZXJQYXJhbXMsIGZvcm1QYXJhbXMsIGJvZHlQYXJhbSwgYXV0aE5hbWVzLFxyXG4gICAgICAgICAgICBjb250ZW50VHlwZXMsIGFjY2VwdHMsIG51bGwsIG51bGwpXHJcbiAgICAgICAgKS5waXBlKFxyXG4gICAgICAgICAgICBtYXAoKHJlc3BvbnNlOiBJZGVudGl0eVVzZXJNb2RlbFtdKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYSBsaXN0IG9mIHJvbGVzIGZvciBhIHVzZXIuXHJcbiAgICAgKiBAcGFyYW0gdXNlcklkIElEIG9mIHRoZSB1c2VyXHJcbiAgICAgKiBAcmV0dXJucyBBcnJheSBvZiByb2xlIGluZm8gb2JqZWN0c1xyXG4gICAgICovXHJcbiAgICBnZXRVc2VyUm9sZXModXNlcklkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPElkZW50aXR5Um9sZU1vZGVsW10+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkUm9sZXNVcmwodXNlcklkKTtcclxuICAgICAgICBjb25zdCBodHRwTWV0aG9kID0gJ0dFVCcsIHBhdGhQYXJhbXMgPSB7fSwgcXVlcnlQYXJhbXMgPSB7fSwgYm9keVBhcmFtID0ge30sIGhlYWRlclBhcmFtcyA9IHt9LFxyXG4gICAgICAgICAgICBmb3JtUGFyYW1zID0ge30sIGNvbnRlbnRUeXBlcyA9IFsnYXBwbGljYXRpb24vanNvbiddLCBhY2NlcHRzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkub2F1dGgyQXV0aC5jYWxsQ3VzdG9tQXBpKFxyXG4gICAgICAgICAgICB1cmwsIGh0dHBNZXRob2QsIHBhdGhQYXJhbXMsIHF1ZXJ5UGFyYW1zLFxyXG4gICAgICAgICAgICBoZWFkZXJQYXJhbXMsIGZvcm1QYXJhbXMsIGJvZHlQYXJhbSxcclxuICAgICAgICAgICAgY29udGVudFR5cGVzLCBhY2NlcHRzLCBPYmplY3QsIG51bGwsIG51bGwpXHJcbiAgICAgICAgKS5waXBlKFxyXG4gICAgICAgICAgICBtYXAoKHJlc3BvbnNlOiBJZGVudGl0eVJvbGVNb2RlbFtdKSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcmVzcG9uc2U7XHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYW4gYXJyYXkgb2YgdXNlcnMgKGluY2x1ZGluZyB0aGUgY3VycmVudCB1c2VyKSB3aG8gaGF2ZSBhbnkgb2YgdGhlIHJvbGVzIGluIHRoZSBzdXBwbGllZCBsaXN0LlxyXG4gICAgICogQHBhcmFtIHJvbGVOYW1lcyBMaXN0IG9mIHJvbGUgbmFtZXMgdG8gbG9vayBmb3JcclxuICAgICAqIEByZXR1cm5zIEFycmF5IG9mIHVzZXIgaW5mbyBvYmplY3RzXHJcbiAgICAgKi9cclxuICAgIGFzeW5jIGdldFVzZXJzQnlSb2xlc1dpdGhDdXJyZW50VXNlcihyb2xlTmFtZXM6IHN0cmluZ1tdKTogUHJvbWlzZTxJZGVudGl0eVVzZXJNb2RlbFtdPiB7XHJcbiAgICAgICAgY29uc3QgZmlsdGVyZWRVc2VyczogSWRlbnRpdHlVc2VyTW9kZWxbXSA9IFtdO1xyXG4gICAgICAgIGlmIChyb2xlTmFtZXMgJiYgcm9sZU5hbWVzLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgY29uc3QgdXNlcnMgPSBhd2FpdCB0aGlzLmdldFVzZXJzKCkudG9Qcm9taXNlKCk7XHJcblxyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHVzZXJzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBoYXNBbnlSb2xlID0gYXdhaXQgdGhpcy51c2VySGFzQW55Um9sZSh1c2Vyc1tpXS5pZCwgcm9sZU5hbWVzKTtcclxuICAgICAgICAgICAgICAgIGlmIChoYXNBbnlSb2xlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZmlsdGVyZWRVc2Vycy5wdXNoKHVzZXJzW2ldKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZpbHRlcmVkVXNlcnM7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGFuIGFycmF5IG9mIHVzZXJzIChub3QgaW5jbHVkaW5nIHRoZSBjdXJyZW50IHVzZXIpIHdobyBoYXZlIGFueSBvZiB0aGUgcm9sZXMgaW4gdGhlIHN1cHBsaWVkIGxpc3QuXHJcbiAgICAgKiBAcGFyYW0gcm9sZU5hbWVzIExpc3Qgb2Ygcm9sZSBuYW1lcyB0byBsb29rIGZvclxyXG4gICAgICogQHJldHVybnMgQXJyYXkgb2YgdXNlciBpbmZvIG9iamVjdHNcclxuICAgICAqL1xyXG4gICAgYXN5bmMgZ2V0VXNlcnNCeVJvbGVzV2l0aG91dEN1cnJlbnRVc2VyKHJvbGVOYW1lczogc3RyaW5nW10pOiBQcm9taXNlPElkZW50aXR5VXNlck1vZGVsW10+IHtcclxuICAgICAgICBjb25zdCBmaWx0ZXJlZFVzZXJzOiBJZGVudGl0eVVzZXJNb2RlbFtdID0gW107XHJcbiAgICAgICAgaWYgKHJvbGVOYW1lcyAmJiByb2xlTmFtZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICBjb25zdCBjdXJyZW50VXNlciA9IHRoaXMuZ2V0Q3VycmVudFVzZXJJbmZvKCk7XHJcbiAgICAgICAgICAgIGxldCB1c2VycyA9IGF3YWl0IHRoaXMuZ2V0VXNlcnMoKS50b1Byb21pc2UoKTtcclxuXHJcbiAgICAgICAgICAgIHVzZXJzID0gdXNlcnMuZmlsdGVyKCh1c2VyKSA9PiB7IHJldHVybiB1c2VyLnVzZXJuYW1lICE9PSBjdXJyZW50VXNlci51c2VybmFtZTsgfSk7XHJcblxyXG4gICAgICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHVzZXJzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICBjb25zdCBoYXNBbnlSb2xlID0gYXdhaXQgdGhpcy51c2VySGFzQW55Um9sZSh1c2Vyc1tpXS5pZCwgcm9sZU5hbWVzKTtcclxuICAgICAgICAgICAgICAgIGlmIChoYXNBbnlSb2xlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgZmlsdGVyZWRVc2Vycy5wdXNoKHVzZXJzW2ldKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIGZpbHRlcmVkVXNlcnM7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBhc3luYyB1c2VySGFzQW55Um9sZSh1c2VySWQ6IHN0cmluZywgcm9sZU5hbWVzOiBzdHJpbmdbXSk6IFByb21pc2U8Ym9vbGVhbj4ge1xyXG4gICAgICAgIGNvbnN0IHVzZXJSb2xlcyA9IGF3YWl0IHRoaXMuZ2V0VXNlclJvbGVzKHVzZXJJZCkudG9Qcm9taXNlKCk7XHJcbiAgICAgICAgY29uc3QgaGFzQW55Um9sZSA9IHJvbGVOYW1lcy5zb21lKChyb2xlTmFtZSkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBmaWx0ZXJlZFJvbGVzID0gdXNlclJvbGVzLmZpbHRlcigodXNlclJvbGUpID0+IHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB1c2VyUm9sZS5uYW1lLnRvTG9jYWxlTG93ZXJDYXNlKCkgPT09IHJvbGVOYW1lLnRvTG9jYWxlTG93ZXJDYXNlKCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgcmV0dXJuIGZpbHRlcmVkUm9sZXMubGVuZ3RoID4gMDtcclxuICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGhhc0FueVJvbGU7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGVja3MgaWYgYSB1c2VyIGhhcyBvbmUgb2YgdGhlIHJvbGVzIGZyb20gYSBsaXN0LlxyXG4gICAgICogQHBhcmFtIHVzZXJJZCBJRCBvZiB0aGUgdGFyZ2V0IHVzZXJcclxuICAgICAqIEBwYXJhbSByb2xlTmFtZXMgQXJyYXkgb2Ygcm9sZXMgdG8gY2hlY2sgZm9yXHJcbiAgICAgKiBAcmV0dXJucyBUcnVlIGlmIHRoZSB1c2VyIGhhcyBvbmUgb2YgdGhlIHJvbGVzLCBmYWxzZSBvdGhlcndpc2VcclxuICAgICAqL1xyXG4gICAgY2hlY2tVc2VySGFzUm9sZSh1c2VySWQ6IHN0cmluZywgcm9sZU5hbWVzOiBzdHJpbmdbXSk6IE9ic2VydmFibGU8Ym9vbGVhbj4ge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmdldFVzZXJSb2xlcyh1c2VySWQpLnBpcGUobWFwKCh1c2VyUm9sZXM6IElkZW50aXR5Um9sZU1vZGVsW10pID0+IHtcclxuICAgICAgICAgICAgbGV0IGhhc1JvbGUgPSBmYWxzZTtcclxuICAgICAgICAgICAgaWYgKHVzZXJSb2xlcyAmJiB1c2VyUm9sZXMubGVuZ3RoID4gMCkge1xyXG4gICAgICAgICAgICAgICAgcm9sZU5hbWVzLmZvckVhY2goKHJvbGVOYW1lOiBzdHJpbmcpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCByb2xlID0gdXNlclJvbGVzLmZpbmQoKHVzZXJSb2xlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiByb2xlTmFtZSA9PT0gdXNlclJvbGUubmFtZTtcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICBpZiAocm9sZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBoYXNSb2xlID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIHJldHVybiBoYXNSb2xlO1xyXG4gICAgICAgIH0pKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgZGV0YWlscyBmb3IgYWxsIHVzZXJzLlxyXG4gICAgICogQHJldHVybnMgQXJyYXkgb2YgdXNlciBpbmZvcm1hdGlvbiBvYmplY3RzLlxyXG4gICAgICovXHJcbiAgICBxdWVyeVVzZXJzKHJlcXVlc3RRdWVyeTogSWRlbnRpdHlVc2VyUXVlcnlDbG91ZFJlcXVlc3RNb2RlbCk6IE9ic2VydmFibGU8SWRlbnRpdHlVc2VyUXVlcnlSZXNwb25zZT4ge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuYnVpbGRVc2VyVXJsKCk7XHJcbiAgICAgICAgY29uc3QgaHR0cE1ldGhvZCA9ICdHRVQnLCBwYXRoUGFyYW1zID0ge30sXHJcbiAgICAgICAgcXVlcnlQYXJhbXMgPSB7IGZpcnN0OiByZXF1ZXN0UXVlcnkuZmlyc3QsIG1heDogcmVxdWVzdFF1ZXJ5Lm1heCB9LCBib2R5UGFyYW0gPSB7fSwgaGVhZGVyUGFyYW1zID0ge30sXHJcbiAgICAgICAgZm9ybVBhcmFtcyA9IHt9LCBhdXRoTmFtZXMgPSBbXSwgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiB0aGlzLmdldFRvdGFsVXNlcnNDb3VudCgpLnBpcGUoXHJcbiAgICAgICAgICAgICAgICBzd2l0Y2hNYXAoKHRvdGFsQ291bnQ6IGFueSkgPT5cclxuICAgICAgICAgICAgICAgIGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgICAgICAgICAgICAgdXJsLCBodHRwTWV0aG9kLCBwYXRoUGFyYW1zLCBxdWVyeVBhcmFtcyxcclxuICAgICAgICAgICAgICAgICAgICBoZWFkZXJQYXJhbXMsIGZvcm1QYXJhbXMsIGJvZHlQYXJhbSwgYXV0aE5hbWVzLFxyXG4gICAgICAgICAgICAgICAgICAgIGNvbnRlbnRUeXBlcywgbnVsbCwgbnVsbCwgbnVsbClcclxuICAgICAgICAgICAgICAgICkucGlwZShcclxuICAgICAgICAgICAgICAgICAgICBtYXAoKHJlc3BvbnNlOiBJZGVudGl0eVVzZXJNb2RlbFtdKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybiA8SWRlbnRpdHlVc2VyUXVlcnlSZXNwb25zZT4ge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgZW50cmllczogcmVzcG9uc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwYWdpbmF0aW9uOiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHNraXBDb3VudDogcmVxdWVzdFF1ZXJ5LmZpcnN0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICBtYXhJdGVtczogcmVxdWVzdFF1ZXJ5Lm1heCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY291bnQ6IHRvdGFsQ291bnQsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGhhc01vcmVJdGVtczogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRvdGFsSXRlbXM6IHRvdGFsQ291bnRcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgICAgIH0pLFxyXG4gICAgICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycm9yKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycm9yKSlcclxuICAgICAgICAgICAgICAgICAgICApKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIHVzZXJzIHRvdGFsIGNvdW50LlxyXG4gICAgICogQHJldHVybnMgTnVtYmVyIG9mIHVzZXJzIGNvdW50LlxyXG4gICAgICovXHJcbiAgICBnZXRUb3RhbFVzZXJzQ291bnQoKTogT2JzZXJ2YWJsZTxudW1iZXI+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlclVybCgpICsgYC9jb3VudGA7XHJcbiAgICAgICAgY29uc3QgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ10sIGFjY2VwdHMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXTtcclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5nZXRJbnN0YW5jZSgpXHJcbiAgICAgICAgICAgIC5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkodXJsLCAnR0VUJyxcclxuICAgICAgICAgICAgICBudWxsLCBudWxsLCBudWxsLFxyXG4gICAgICAgICAgICAgIG51bGwsIG51bGwsIGNvbnRlbnRUeXBlcyxcclxuICAgICAgICAgICAgICBhY2NlcHRzLCBudWxsLCBudWxsLCBudWxsXHJcbiAgICAgICAgICAgICAgKSkucGlwZShcclxuICAgICAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycm9yKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycm9yKSlcclxuICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIENyZWF0ZXMgbmV3IHVzZXIuXHJcbiAgICAgKiBAcGFyYW0gbmV3VXNlciBPYmplY3QgY29udGFpbmluZyB0aGUgbmV3IHVzZXIgZGV0YWlscy5cclxuICAgICAqIEByZXR1cm5zIEVtcHR5IHJlc3BvbnNlIHdoZW4gdGhlIHVzZXIgY3JlYXRlZC5cclxuICAgICAqL1xyXG4gICAgY3JlYXRlVXNlcihuZXdVc2VyOiBJZGVudGl0eVVzZXJNb2RlbCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5idWlsZFVzZXJVcmwoKTtcclxuICAgICAgICBjb25zdCByZXF1ZXN0ID0gSlNPTi5zdHJpbmdpZnkobmV3VXNlcik7XHJcbiAgICAgICAgY29uc3QgaHR0cE1ldGhvZCA9ICdQT1NUJywgcGF0aFBhcmFtcyA9IHt9LCBxdWVyeVBhcmFtcyA9IHt9LCBib2R5UGFyYW0gPSByZXF1ZXN0LCBoZWFkZXJQYXJhbXMgPSB7fSxcclxuICAgICAgICBmb3JtUGFyYW1zID0ge30sIGNvbnRlbnRUeXBlcyA9IFsnYXBwbGljYXRpb24vanNvbiddLCBhY2NlcHRzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkub2F1dGgyQXV0aC5jYWxsQ3VzdG9tQXBpKFxyXG4gICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgaGVhZGVyUGFyYW1zLCBmb3JtUGFyYW1zLCBib2R5UGFyYW0sXHJcbiAgICAgICAgY29udGVudFR5cGVzLCBhY2NlcHRzLCBudWxsLCBudWxsLCBudWxsXHJcbiAgICAgICAgKSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyb3IpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyb3IpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBVcGRhdGVzIHVzZXIgZGV0YWlscy5cclxuICAgICAqIEBwYXJhbSB1c2VySWQgSWQgb2YgdGhlIHVzZXIuXHJcbiAgICAgKiBAcGFyYW0gdXBkYXRlZFVzZXIgT2JqZWN0IGNvbnRhaW5pbmcgdGhlIHVzZXIgZGV0YWlscy5cclxuICAgICAqIEByZXR1cm5zIEVtcHR5IHJlc3BvbnNlIHdoZW4gdGhlIHVzZXIgdXBkYXRlZC5cclxuICAgICAqL1xyXG4gICAgdXBkYXRlVXNlcih1c2VySWQ6IHN0cmluZywgdXBkYXRlZFVzZXI6IElkZW50aXR5VXNlck1vZGVsKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlclVybCgpICsgJy8nICsgdXNlcklkO1xyXG4gICAgICAgIGNvbnN0IHJlcXVlc3QgPSBKU09OLnN0cmluZ2lmeSh1cGRhdGVkVXNlcik7XHJcbiAgICAgICAgY29uc3QgaHR0cE1ldGhvZCA9ICdQVVQnLCBwYXRoUGFyYW1zID0ge30gLCBxdWVyeVBhcmFtcyA9IHt9LCBib2R5UGFyYW0gPSByZXF1ZXN0LCBoZWFkZXJQYXJhbXMgPSB7fSxcclxuICAgICAgICBmb3JtUGFyYW1zID0ge30sIGNvbnRlbnRUeXBlcyA9IFsnYXBwbGljYXRpb24vanNvbiddLCBhY2NlcHRzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkub2F1dGgyQXV0aC5jYWxsQ3VzdG9tQXBpKFxyXG4gICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgaGVhZGVyUGFyYW1zLCBmb3JtUGFyYW1zLCBib2R5UGFyYW0sXHJcbiAgICAgICAgY29udGVudFR5cGVzLCBhY2NlcHRzLCBudWxsLCBudWxsLCBudWxsXHJcbiAgICAgICAgKSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyb3IpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyb3IpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBEZWxldGVzIFVzZXIuXHJcbiAgICAgKiBAcGFyYW0gdXNlcklkIElkIG9mIHRoZSAgdXNlci5cclxuICAgICAqIEByZXR1cm5zIEVtcHR5IHJlc3BvbnNlIHdoZW4gdGhlIHVzZXIgZGVsZXRlZC5cclxuICAgICAqL1xyXG4gICAgZGVsZXRlVXNlcih1c2VySWQ6IHN0cmluZyk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5idWlsZFVzZXJVcmwoKSArICcvJyArIHVzZXJJZDtcclxuICAgICAgICBjb25zdCBodHRwTWV0aG9kID0gJ0RFTEVURScsIHBhdGhQYXJhbXMgPSB7fSAsIHF1ZXJ5UGFyYW1zID0ge30sIGJvZHlQYXJhbSA9IHt9LCBoZWFkZXJQYXJhbXMgPSB7fSxcclxuICAgICAgICBmb3JtUGFyYW1zID0ge30sIGNvbnRlbnRUeXBlcyA9IFsnYXBwbGljYXRpb24vanNvbiddLCBhY2NlcHRzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkub2F1dGgyQXV0aC5jYWxsQ3VzdG9tQXBpKFxyXG4gICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgaGVhZGVyUGFyYW1zLCBmb3JtUGFyYW1zLCBib2R5UGFyYW0sXHJcbiAgICAgICAgY29udGVudFR5cGVzLCBhY2NlcHRzLCBudWxsLCBudWxsLCBudWxsXHJcbiAgICAgICAgKSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyb3IpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyb3IpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDaGFuZ2VzIHVzZXIgcGFzc3dvcmQuXHJcbiAgICAgKiBAcGFyYW0gdXNlcklkIElkIG9mIHRoZSB1c2VyLlxyXG4gICAgICogQHBhcmFtIGNyZWRlbnRpYWxzIERldGFpbHMgb2YgdXNlciBDcmVkZW50aWFscy5cclxuICAgICAqIEByZXR1cm5zIEVtcHR5IHJlc3BvbnNlIHdoZW4gdGhlIHBhc3N3b3JkIGNoYW5nZWQuXHJcbiAgICAgKi9cclxuICAgIGNoYW5nZVBhc3N3b3JkKHVzZXJJZDogc3RyaW5nLCBuZXdQYXNzd29yZDogSWRlbnRpdHlVc2VyUGFzc3dvcmRNb2RlbCk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5idWlsZFVzZXJVcmwoKSArICcvJyArIHVzZXJJZCArICcvcmVzZXQtcGFzc3dvcmQnO1xyXG4gICAgICAgIGNvbnN0IHJlcXVlc3QgPSBKU09OLnN0cmluZ2lmeShuZXdQYXNzd29yZCk7XHJcbiAgICAgICAgY29uc3QgaHR0cE1ldGhvZCA9ICdQVVQnLCBwYXRoUGFyYW1zID0ge30gLCBxdWVyeVBhcmFtcyA9IHt9LCBib2R5UGFyYW0gPSByZXF1ZXN0LCBoZWFkZXJQYXJhbXMgPSB7fSxcclxuICAgICAgICBmb3JtUGFyYW1zID0ge30sIGNvbnRlbnRUeXBlcyA9IFsnYXBwbGljYXRpb24vanNvbiddLCBhY2NlcHRzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkub2F1dGgyQXV0aC5jYWxsQ3VzdG9tQXBpKFxyXG4gICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgaGVhZGVyUGFyYW1zLCBmb3JtUGFyYW1zLCBib2R5UGFyYW0sXHJcbiAgICAgICAgY29udGVudFR5cGVzLCBhY2NlcHRzLCBudWxsLCBudWxsLCBudWxsXHJcbiAgICAgICAgKSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyb3IpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyb3IpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGludm9sdmVkIGdyb3Vwcy5cclxuICAgICAqIEBwYXJhbSB1c2VySWQgSWQgb2YgdGhlIHVzZXIuXHJcbiAgICAgKiBAcmV0dXJucyBBcnJheSBvZiBpbnZvbHZlZCBncm91cHMgaW5mb3JtYXRpb24gb2JqZWN0cy5cclxuICAgICAqL1xyXG4gICAgZ2V0SW52b2x2ZWRHcm91cHModXNlcklkOiBzdHJpbmcpOiBPYnNlcnZhYmxlPElkZW50aXR5R3JvdXBNb2RlbFtdPiB7XHJcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5idWlsZFVzZXJVcmwoKSArICcvJyArIHVzZXJJZCArICcvZ3JvdXBzLyc7XHJcbiAgICAgICAgY29uc3QgaHR0cE1ldGhvZCA9ICdHRVQnLCBwYXRoUGFyYW1zID0geyBpZDogdXNlcklkfSxcclxuICAgICAgICBxdWVyeVBhcmFtcyA9IHt9LCBib2R5UGFyYW0gPSB7fSwgaGVhZGVyUGFyYW1zID0ge30sXHJcbiAgICAgICAgZm9ybVBhcmFtcyA9IHt9LCBhdXRoTmFtZXMgPSBbXSwgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkub2F1dGgyQXV0aC5jYWxsQ3VzdG9tQXBpKFxyXG4gICAgICAgICAgICAgICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgICAgICAgICAgICAgaGVhZGVyUGFyYW1zLCBmb3JtUGFyYW1zLCBib2R5UGFyYW0sIGF1dGhOYW1lcyxcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50VHlwZXMsIG51bGwsIG51bGwsIG51bGxcclxuICAgICAgICAgICAgICAgICAgICApKS5waXBlKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnJvcikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnJvcikpXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEpvaW5zIGdyb3VwLlxyXG4gICAgICogQHBhcmFtIGpvaW5Hcm91cFJlcXVlc3QgRGV0YWlscyBvZiBqb2luIGdyb3VwIHJlcXVlc3QgKElkZW50aXR5Sm9pbkdyb3VwUmVxdWVzdE1vZGVsKS5cclxuICAgICAqIEByZXR1cm5zIEVtcHR5IHJlc3BvbnNlIHdoZW4gdGhlIHVzZXIgam9pbmVkIHRoZSBncm91cC5cclxuICAgICAqL1xyXG4gICAgam9pbkdyb3VwKGpvaW5Hcm91cFJlcXVlc3Q6IElkZW50aXR5Sm9pbkdyb3VwUmVxdWVzdE1vZGVsKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlclVybCgpICsgJy8nICsgam9pbkdyb3VwUmVxdWVzdC51c2VySWQgKyAnL2dyb3Vwcy8nICsgam9pbkdyb3VwUmVxdWVzdC5ncm91cElkO1xyXG4gICAgICAgIGNvbnN0IHJlcXVlc3QgPSBKU09OLnN0cmluZ2lmeShqb2luR3JvdXBSZXF1ZXN0KTtcclxuICAgICAgICBjb25zdCBodHRwTWV0aG9kID0gJ1BVVCcsIHBhdGhQYXJhbXMgPSB7fSAsIHF1ZXJ5UGFyYW1zID0ge30sIGJvZHlQYXJhbSA9IHJlcXVlc3QsIGhlYWRlclBhcmFtcyA9IHt9LFxyXG4gICAgICAgIGZvcm1QYXJhbXMgPSB7fSwgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ10sIGFjY2VwdHMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXTtcclxuXHJcbiAgICAgICAgcmV0dXJuIGZyb20odGhpcy5hbGZyZXNjb0FwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5vYXV0aDJBdXRoLmNhbGxDdXN0b21BcGkoXHJcbiAgICAgICAgdXJsLCBodHRwTWV0aG9kLCBwYXRoUGFyYW1zLCBxdWVyeVBhcmFtcyxcclxuICAgICAgICBoZWFkZXJQYXJhbXMsIGZvcm1QYXJhbXMsIGJvZHlQYXJhbSxcclxuICAgICAgICBjb250ZW50VHlwZXMsIGFjY2VwdHMsIG51bGwsIG51bGwsIG51bGxcclxuICAgICAgICApKS5waXBlKFxyXG4gICAgICAgICAgICBjYXRjaEVycm9yKChlcnJvcikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnJvcikpXHJcbiAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIExlYXZlcyBncm91cC5cclxuICAgICAqIEBwYXJhbSB1c2VySWQgSWQgb2YgdGhlIHVzZXIuXHJcbiAgICAgKiBAcGFyYW0gZ3JvdXBJZCBJZCBvZiB0aGUgIGdyb3VwLlxyXG4gICAgICogQHJldHVybnMgRW1wdHkgcmVzcG9uc2Ugd2hlbiB0aGUgdXNlciBsZWZ0IHRoZSBncm91cC5cclxuICAgICAqL1xyXG4gICAgbGVhdmVHcm91cCh1c2VySWQ6IGFueSwgZ3JvdXBJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxhbnk+IHtcclxuICAgICAgICBjb25zdCB1cmwgPSB0aGlzLmJ1aWxkVXNlclVybCgpICsgJy8nICsgdXNlcklkICsgJy9ncm91cHMvJyArIGdyb3VwSWQ7XHJcbiAgICAgICAgY29uc3QgaHR0cE1ldGhvZCA9ICdERUxFVEUnLCBwYXRoUGFyYW1zID0ge30gLCBxdWVyeVBhcmFtcyA9IHt9LCBib2R5UGFyYW0gPSB7fSwgaGVhZGVyUGFyYW1zID0ge30sXHJcbiAgICAgICAgZm9ybVBhcmFtcyA9IHt9LCBjb250ZW50VHlwZXMgPSBbJ2FwcGxpY2F0aW9uL2pzb24nXSwgYWNjZXB0cyA9IFsnYXBwbGljYXRpb24vanNvbiddO1xyXG5cclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLm9hdXRoMkF1dGguY2FsbEN1c3RvbUFwaShcclxuICAgICAgICB1cmwsIGh0dHBNZXRob2QsIHBhdGhQYXJhbXMsIHF1ZXJ5UGFyYW1zLFxyXG4gICAgICAgIGhlYWRlclBhcmFtcywgZm9ybVBhcmFtcywgYm9keVBhcmFtLFxyXG4gICAgICAgIGNvbnRlbnRUeXBlcywgYWNjZXB0cywgbnVsbCwgbnVsbCwgbnVsbFxyXG4gICAgICAgICkpLnBpcGUoXHJcbiAgICAgICAgICAgIGNhdGNoRXJyb3IoKGVycm9yKSA9PiB0aGlzLmhhbmRsZUVycm9yKGVycm9yKSlcclxuICAgICAgICApO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogR2V0cyBhdmFpbGFibGUgcm9sZXNcclxuICAgICAqIEBwYXJhbSB1c2VySWQgSWQgb2YgdGhlIHVzZXIuXHJcbiAgICAgKiBAcmV0dXJucyBBcnJheSBvZiBhdmFpbGFibGUgcm9sZXMgaW5mb3JtYXRpb24gb2JqZWN0c1xyXG4gICAgICovXHJcbiAgICBnZXRBdmFpbGFibGVSb2xlcyh1c2VySWQ6IHN0cmluZyk6IE9ic2VydmFibGU8SWRlbnRpdHlSb2xlTW9kZWxbXT4ge1xyXG4gICAgICAgIGNvbnN0IHVybCA9IHRoaXMuYnVpbGRVc2VyVXJsKCkgKyAnLycgKyB1c2VySWQgKyAnL3JvbGUtbWFwcGluZ3MvcmVhbG0vYXZhaWxhYmxlJztcclxuICAgICAgICBjb25zdCBodHRwTWV0aG9kID0gJ0dFVCcsIHBhdGhQYXJhbXMgPSB7fSxcclxuICAgICAgICBxdWVyeVBhcmFtcyA9IHt9LCBib2R5UGFyYW0gPSB7fSwgaGVhZGVyUGFyYW1zID0ge30sXHJcbiAgICAgICAgZm9ybVBhcmFtcyA9IHt9LCBhdXRoTmFtZXMgPSBbXSwgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkub2F1dGgyQXV0aC5jYWxsQ3VzdG9tQXBpKFxyXG4gICAgICAgICAgICAgICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgICAgICAgICAgICAgaGVhZGVyUGFyYW1zLCBmb3JtUGFyYW1zLCBib2R5UGFyYW0sIGF1dGhOYW1lcyxcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50VHlwZXMsIG51bGwsIG51bGwsIG51bGxcclxuICAgICAgICAgICAgICAgICAgICApKS5waXBlKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnJvcikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnJvcikpXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgYXNzaWduZWQgcm9sZXMuXHJcbiAgICAgKiBAcGFyYW0gdXNlcklkIElkIG9mIHRoZSB1c2VyLlxyXG4gICAgICogQHJldHVybnMgQXJyYXkgb2YgYXNzaWduZWQgcm9sZXMgaW5mb3JtYXRpb24gb2JqZWN0c1xyXG4gICAgICovXHJcbiAgICBnZXRBc3NpZ25lZFJvbGVzKHVzZXJJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxJZGVudGl0eVJvbGVNb2RlbFtdPiB7XHJcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5idWlsZFVzZXJVcmwoKSArICcvJyArIHVzZXJJZCArICcvcm9sZS1tYXBwaW5ncy9yZWFsbSc7XHJcbiAgICAgICAgY29uc3QgaHR0cE1ldGhvZCA9ICdHRVQnLCBwYXRoUGFyYW1zID0geyBpZDogdXNlcklkfSxcclxuICAgICAgICBxdWVyeVBhcmFtcyA9IHt9LCBib2R5UGFyYW0gPSB7fSwgaGVhZGVyUGFyYW1zID0ge30sXHJcbiAgICAgICAgZm9ybVBhcmFtcyA9IHt9LCBhdXRoTmFtZXMgPSBbXSwgY29udGVudFR5cGVzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkub2F1dGgyQXV0aC5jYWxsQ3VzdG9tQXBpKFxyXG4gICAgICAgICAgICAgICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgICAgICAgICAgICAgaGVhZGVyUGFyYW1zLCBmb3JtUGFyYW1zLCBib2R5UGFyYW0sIGF1dGhOYW1lcyxcclxuICAgICAgICAgICAgICAgICAgICBjb250ZW50VHlwZXMsIG51bGwsIG51bGwsIG51bGxcclxuICAgICAgICAgICAgICAgICAgICApKS5waXBlKFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBjYXRjaEVycm9yKChlcnJvcikgPT4gdGhpcy5oYW5kbGVFcnJvcihlcnJvcikpXHJcbiAgICAgICAgICAgICAgICAgICAgKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEdldHMgZWZmZWN0aXZlIHJvbGVzLlxyXG4gICAgICogQHBhcmFtIHVzZXJJZCBJZCBvZiB0aGUgdXNlci5cclxuICAgICAqIEByZXR1cm5zIEFycmF5IG9mIGNvbXBvc2l0ZSByb2xlcyBpbmZvcm1hdGlvbiBvYmplY3RzXHJcbiAgICAgKi9cclxuICAgIGdldEVmZmVjdGl2ZVJvbGVzKHVzZXJJZDogc3RyaW5nKTogT2JzZXJ2YWJsZTxJZGVudGl0eVJvbGVNb2RlbFtdPiB7XHJcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5idWlsZFVzZXJVcmwoKSArICcvJyArIHVzZXJJZCArICcvcm9sZS1tYXBwaW5ncy9yZWFsbS9jb21wb3NpdGUnO1xyXG4gICAgICAgIGNvbnN0IGh0dHBNZXRob2QgPSAnR0VUJywgcGF0aFBhcmFtcyA9IHsgaWQ6IHVzZXJJZH0sXHJcbiAgICAgICAgcXVlcnlQYXJhbXMgPSB7fSwgYm9keVBhcmFtID0ge30sIGhlYWRlclBhcmFtcyA9IHt9LFxyXG4gICAgICAgIGZvcm1QYXJhbXMgPSB7fSwgYXV0aE5hbWVzID0gW10sIGNvbnRlbnRUeXBlcyA9IFsnYXBwbGljYXRpb24vanNvbiddO1xyXG5cclxuICAgICAgICByZXR1cm4gZnJvbSh0aGlzLmFsZnJlc2NvQXBpU2VydmljZS5nZXRJbnN0YW5jZSgpLm9hdXRoMkF1dGguY2FsbEN1c3RvbUFwaShcclxuICAgICAgICAgICAgICAgICAgICB1cmwsIGh0dHBNZXRob2QsIHBhdGhQYXJhbXMsIHF1ZXJ5UGFyYW1zLFxyXG4gICAgICAgICAgICAgICAgICAgIGhlYWRlclBhcmFtcywgZm9ybVBhcmFtcywgYm9keVBhcmFtLCBhdXRoTmFtZXMsXHJcbiAgICAgICAgICAgICAgICAgICAgY29udGVudFR5cGVzLCBudWxsLCBudWxsLCBudWxsXHJcbiAgICAgICAgICAgICAgICAgICAgKSkucGlwZShcclxuICAgICAgICAgICAgICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyb3IpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyb3IpKVxyXG4gICAgICAgICAgICAgICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBBc3NpZ25zIHJvbGVzIHRvIHRoZSB1c2VyLlxyXG4gICAgICogQHBhcmFtIHVzZXJJZCBJZCBvZiB0aGUgdXNlci5cclxuICAgICAqIEBwYXJhbSByb2xlcyBBcnJheSBvZiByb2xlcy5cclxuICAgICAqIEByZXR1cm5zIEVtcHR5IHJlc3BvbnNlIHdoZW4gdGhlIHJvbGUgYXNzaWduZWQuXHJcbiAgICAgKi9cclxuICAgIGFzc2lnblJvbGVzKHVzZXJJZDogc3RyaW5nLCByb2xlczogSWRlbnRpdHlSb2xlTW9kZWxbXSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5idWlsZFVzZXJVcmwoKSArICcvJyArIHVzZXJJZCArICcvcm9sZS1tYXBwaW5ncy9yZWFsbSc7XHJcbiAgICAgICAgY29uc3QgcmVxdWVzdCA9IEpTT04uc3RyaW5naWZ5KHJvbGVzKTtcclxuICAgICAgICBjb25zdCBodHRwTWV0aG9kID0gJ1BPU1QnLCBwYXRoUGFyYW1zID0ge30gLCBxdWVyeVBhcmFtcyA9IHt9LCBib2R5UGFyYW0gPSByZXF1ZXN0LCBoZWFkZXJQYXJhbXMgPSB7fSxcclxuICAgICAgICBmb3JtUGFyYW1zID0ge30sIGNvbnRlbnRUeXBlcyA9IFsnYXBwbGljYXRpb24vanNvbiddLCBhY2NlcHRzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkub2F1dGgyQXV0aC5jYWxsQ3VzdG9tQXBpKFxyXG4gICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgaGVhZGVyUGFyYW1zLCBmb3JtUGFyYW1zLCBib2R5UGFyYW0sXHJcbiAgICAgICAgY29udGVudFR5cGVzLCBhY2NlcHRzLCBudWxsLCBudWxsLCBudWxsXHJcbiAgICAgICAgKSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyb3IpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyb3IpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBSZW1vdmVzIGFzc2lnbmVkIHJvbGVzLlxyXG4gICAgICogQHBhcmFtIHVzZXJJZCBJZCBvZiB0aGUgdXNlci5cclxuICAgICAqIEBwYXJhbSByb2xlcyBBcnJheSBvZiByb2xlcy5cclxuICAgICAqIEByZXR1cm5zIEVtcHR5IHJlc3BvbnNlIHdoZW4gdGhlIHJvbGUgcmVtb3ZlZC5cclxuICAgICAqL1xyXG4gICAgcmVtb3ZlUm9sZXModXNlcklkOiBzdHJpbmcsIHJlbW92ZWRSb2xlczogSWRlbnRpdHlSb2xlTW9kZWxbXSk6IE9ic2VydmFibGU8YW55PiB7XHJcbiAgICAgICAgY29uc3QgdXJsID0gdGhpcy5idWlsZFVzZXJVcmwoKSArICcvJyArIHVzZXJJZCArICcvcm9sZS1tYXBwaW5ncy9yZWFsbSc7XHJcbiAgICAgICAgY29uc3QgcmVxdWVzdCA9IEpTT04uc3RyaW5naWZ5KHJlbW92ZWRSb2xlcyk7XHJcbiAgICAgICAgY29uc3QgaHR0cE1ldGhvZCA9ICdERUxFVEUnLCBwYXRoUGFyYW1zID0ge30gLCBxdWVyeVBhcmFtcyA9IHt9LCBib2R5UGFyYW0gPSByZXF1ZXN0LCBoZWFkZXJQYXJhbXMgPSB7fSxcclxuICAgICAgICBmb3JtUGFyYW1zID0ge30sIGNvbnRlbnRUeXBlcyA9IFsnYXBwbGljYXRpb24vanNvbiddLCBhY2NlcHRzID0gWydhcHBsaWNhdGlvbi9qc29uJ107XHJcblxyXG4gICAgICAgIHJldHVybiBmcm9tKHRoaXMuYWxmcmVzY29BcGlTZXJ2aWNlLmdldEluc3RhbmNlKCkub2F1dGgyQXV0aC5jYWxsQ3VzdG9tQXBpKFxyXG4gICAgICAgIHVybCwgaHR0cE1ldGhvZCwgcGF0aFBhcmFtcywgcXVlcnlQYXJhbXMsXHJcbiAgICAgICAgaGVhZGVyUGFyYW1zLCBmb3JtUGFyYW1zLCBib2R5UGFyYW0sXHJcbiAgICAgICAgY29udGVudFR5cGVzLCBhY2NlcHRzLCBudWxsLCBudWxsLCBudWxsXHJcbiAgICAgICAgKSkucGlwZShcclxuICAgICAgICAgICAgY2F0Y2hFcnJvcigoZXJyb3IpID0+IHRoaXMuaGFuZGxlRXJyb3IoZXJyb3IpKVxyXG4gICAgICAgICk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSBidWlsZFVzZXJVcmwoKTogYW55IHtcclxuICAgICAgICByZXR1cm4gYCR7dGhpcy5hcHBDb25maWdTZXJ2aWNlLmdldCgnaWRlbnRpdHlIb3N0Jyl9L3VzZXJzYDtcclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGJ1aWxkVXNlckNsaWVudFJvbGVNYXBwaW5nKHVzZXJJZDogc3RyaW5nLCBjbGllbnRJZDogc3RyaW5nKTogYW55IHtcclxuICAgICAgICByZXR1cm4gYCR7dGhpcy5hcHBDb25maWdTZXJ2aWNlLmdldCgnaWRlbnRpdHlIb3N0Jyl9L3VzZXJzLyR7dXNlcklkfS9yb2xlLW1hcHBpbmdzL2NsaWVudHMvJHtjbGllbnRJZH1gO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYnVpbGRSb2xlc1VybCh1c2VySWQ6IHN0cmluZyk6IGFueSB7XHJcbiAgICAgICAgcmV0dXJuIGAke3RoaXMuYXBwQ29uZmlnU2VydmljZS5nZXQoJ2lkZW50aXR5SG9zdCcpfS91c2Vycy8ke3VzZXJJZH0vcm9sZS1tYXBwaW5ncy9yZWFsbS9jb21wb3NpdGVgO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYnVpbGRHZXRDbGllbnRzVXJsKCkge1xyXG4gICAgICAgIHJldHVybiBgJHt0aGlzLmFwcENvbmZpZ1NlcnZpY2UuZ2V0KCdpZGVudGl0eUhvc3QnKX0vY2xpZW50c2A7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBUaHJvdyB0aGUgZXJyb3JcclxuICAgICAqIEBwYXJhbSBlcnJvclxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIGhhbmRsZUVycm9yKGVycm9yOiBSZXNwb25zZSkge1xyXG4gICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnJvcik7XHJcbiAgICAgICAgcmV0dXJuIHRocm93RXJyb3IoZXJyb3IgfHwgJ1NlcnZlciBlcnJvcicpO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==