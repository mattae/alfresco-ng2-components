/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * @record
 */
export function FileInfo() { }
if (false) {
    /** @type {?|undefined} */
    FileInfo.prototype.entry;
    /** @type {?|undefined} */
    FileInfo.prototype.file;
    /** @type {?|undefined} */
    FileInfo.prototype.relativeFolder;
}
var FileUtils = /** @class */ (function () {
    function FileUtils() {
    }
    /**
     * @param {?} folder
     * @return {?}
     */
    FileUtils.flatten = /**
     * @param {?} folder
     * @return {?}
     */
    function (folder) {
        /** @type {?} */
        var reader = folder.createReader();
        /** @type {?} */
        var files = [];
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        function (resolve) {
            /** @type {?} */
            var iterations = [];
            ((/**
             * @return {?}
             */
            function traverse() {
                reader.readEntries((/**
                 * @param {?} entries
                 * @return {?}
                 */
                function (entries) {
                    if (!entries.length) {
                        Promise.all(iterations).then((/**
                         * @return {?}
                         */
                        function () { return resolve(files); }));
                    }
                    else {
                        iterations.push(Promise.all(entries.map((/**
                         * @param {?} entry
                         * @return {?}
                         */
                        function (entry) {
                            if (entry.isFile) {
                                return new Promise((/**
                                 * @param {?} resolveFile
                                 * @return {?}
                                 */
                                function (resolveFile) {
                                    entry.file((/**
                                     * @param {?} file
                                     * @return {?}
                                     */
                                    function (file) {
                                        files.push({
                                            entry: entry,
                                            file: file,
                                            relativeFolder: entry.fullPath.replace(/\/[^\/]*$/, '')
                                        });
                                        resolveFile();
                                    }));
                                }));
                            }
                            else {
                                return FileUtils.flatten(entry).then((/**
                                 * @param {?} result
                                 * @return {?}
                                 */
                                function (result) {
                                    files.push.apply(files, tslib_1.__spread(result));
                                }));
                            }
                        }))));
                        // Try calling traverse() again for the same dir, according to spec
                        traverse();
                    }
                }));
            }))();
        }));
    };
    /**
     * @param {?} fileList
     * @return {?}
     */
    FileUtils.toFileArray = /**
     * @param {?} fileList
     * @return {?}
     */
    function (fileList) {
        /** @type {?} */
        var result = [];
        if (fileList && fileList.length > 0) {
            for (var i = 0; i < fileList.length; i++) {
                result.push(fileList[i]);
            }
        }
        return result;
    };
    return FileUtils;
}());
export { FileUtils };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZmlsZS11dGlscy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInV0aWxzL2ZpbGUtdXRpbHMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLDhCQUlDOzs7SUFIRyx5QkFBWTs7SUFDWix3QkFBWTs7SUFDWixrQ0FBd0I7O0FBRzVCO0lBQUE7SUFpREEsQ0FBQzs7Ozs7SUEvQ1UsaUJBQU87Ozs7SUFBZCxVQUFlLE1BQVc7O1lBQ2hCLE1BQU0sR0FBRyxNQUFNLENBQUMsWUFBWSxFQUFFOztZQUM5QixLQUFLLEdBQWUsRUFBRTtRQUM1QixPQUFPLElBQUksT0FBTzs7OztRQUFDLFVBQUMsT0FBTzs7Z0JBQ2pCLFVBQVUsR0FBRyxFQUFFO1lBQ3JCOzs7WUFBQyxTQUFTLFFBQVE7Z0JBQ2QsTUFBTSxDQUFDLFdBQVc7Ozs7Z0JBQUMsVUFBQyxPQUFPO29CQUN2QixJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sRUFBRTt3QkFDakIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxVQUFVLENBQUMsQ0FBQyxJQUFJOzs7d0JBQUMsY0FBTSxPQUFBLE9BQU8sQ0FBQyxLQUFLLENBQUMsRUFBZCxDQUFjLEVBQUMsQ0FBQztxQkFDdEQ7eUJBQU07d0JBQ0gsVUFBVSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHOzs7O3dCQUFDLFVBQUMsS0FBSzs0QkFDMUMsSUFBSSxLQUFLLENBQUMsTUFBTSxFQUFFO2dDQUNkLE9BQU8sSUFBSSxPQUFPOzs7O2dDQUFDLFVBQUMsV0FBVztvQ0FDM0IsS0FBSyxDQUFDLElBQUk7Ozs7b0NBQUMsVUFBVSxJQUFVO3dDQUMzQixLQUFLLENBQUMsSUFBSSxDQUFDOzRDQUNQLEtBQUssRUFBRSxLQUFLOzRDQUNaLElBQUksRUFBRSxJQUFJOzRDQUNWLGNBQWMsRUFBRSxLQUFLLENBQUMsUUFBUSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEVBQUUsRUFBRSxDQUFDO3lDQUMxRCxDQUFDLENBQUM7d0NBQ0gsV0FBVyxFQUFFLENBQUM7b0NBQ2xCLENBQUMsRUFBQyxDQUFDO2dDQUNQLENBQUMsRUFBQyxDQUFDOzZCQUNOO2lDQUFNO2dDQUNILE9BQU8sU0FBUyxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQyxJQUFJOzs7O2dDQUFDLFVBQUMsTUFBTTtvQ0FDeEMsS0FBSyxDQUFDLElBQUksT0FBVixLQUFLLG1CQUFTLE1BQU0sR0FBRTtnQ0FDMUIsQ0FBQyxFQUFDLENBQUM7NkJBQ047d0JBQ0wsQ0FBQyxFQUFDLENBQUMsQ0FBQyxDQUFDO3dCQUNMLG1FQUFtRTt3QkFDbkUsUUFBUSxFQUFFLENBQUM7cUJBQ2Q7Z0JBQ0wsQ0FBQyxFQUFDLENBQUM7WUFDUCxDQUFDLEVBQUMsRUFBRSxDQUFDO1FBQ1QsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVNLHFCQUFXOzs7O0lBQWxCLFVBQW1CLFFBQWtCOztZQUMzQixNQUFNLEdBQUcsRUFBRTtRQUVqQixJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUNqQyxLQUFLLElBQUksQ0FBQyxHQUFHLENBQUMsRUFBRSxDQUFDLEdBQUcsUUFBUSxDQUFDLE1BQU0sRUFBRSxDQUFDLEVBQUUsRUFBRTtnQkFDdEMsTUFBTSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQzthQUM1QjtTQUNKO1FBRUQsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQUNMLGdCQUFDO0FBQUQsQ0FBQyxBQWpERCxJQWlEQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIEZpbGVJbmZvIHtcclxuICAgIGVudHJ5PzogYW55O1xyXG4gICAgZmlsZT86IEZpbGU7XHJcbiAgICByZWxhdGl2ZUZvbGRlcj86IHN0cmluZztcclxufVxyXG5cclxuZXhwb3J0IGNsYXNzIEZpbGVVdGlscyB7XHJcblxyXG4gICAgc3RhdGljIGZsYXR0ZW4oZm9sZGVyOiBhbnkpOiBQcm9taXNlPEZpbGVJbmZvW10+IHtcclxuICAgICAgICBjb25zdCByZWFkZXIgPSBmb2xkZXIuY3JlYXRlUmVhZGVyKCk7XHJcbiAgICAgICAgY29uc3QgZmlsZXM6IEZpbGVJbmZvW10gPSBbXTtcclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUpID0+IHtcclxuICAgICAgICAgICAgY29uc3QgaXRlcmF0aW9ucyA9IFtdO1xyXG4gICAgICAgICAgICAoZnVuY3Rpb24gdHJhdmVyc2UoKSB7XHJcbiAgICAgICAgICAgICAgICByZWFkZXIucmVhZEVudHJpZXMoKGVudHJpZXMpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBpZiAoIWVudHJpZXMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIFByb21pc2UuYWxsKGl0ZXJhdGlvbnMpLnRoZW4oKCkgPT4gcmVzb2x2ZShmaWxlcykpO1xyXG4gICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGl0ZXJhdGlvbnMucHVzaChQcm9taXNlLmFsbChlbnRyaWVzLm1hcCgoZW50cnkpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChlbnRyeS5pc0ZpbGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmVGaWxlKSA9PiB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVudHJ5LmZpbGUoZnVuY3Rpb24gKGZpbGU6IEZpbGUpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZpbGVzLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIGVudHJ5OiBlbnRyeSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBmaWxlOiBmaWxlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHJlbGF0aXZlRm9sZGVyOiBlbnRyeS5mdWxsUGF0aC5yZXBsYWNlKC9cXC9bXlxcL10qJC8sICcnKVxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXNvbHZlRmlsZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIEZpbGVVdGlscy5mbGF0dGVuKGVudHJ5KS50aGVuKChyZXN1bHQpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgZmlsZXMucHVzaCguLi5yZXN1bHQpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KSkpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAvLyBUcnkgY2FsbGluZyB0cmF2ZXJzZSgpIGFnYWluIGZvciB0aGUgc2FtZSBkaXIsIGFjY29yZGluZyB0byBzcGVjXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyYXZlcnNlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgICAgIH0pKCk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgc3RhdGljIHRvRmlsZUFycmF5KGZpbGVMaXN0OiBGaWxlTGlzdCk6IEZpbGVbXSB7XHJcbiAgICAgICAgY29uc3QgcmVzdWx0ID0gW107XHJcblxyXG4gICAgICAgIGlmIChmaWxlTGlzdCAmJiBmaWxlTGlzdC5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgZmlsZUxpc3QubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIHJlc3VsdC5wdXNoKGZpbGVMaXN0W2ldKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdDtcclxuICAgIH1cclxufVxyXG4iXX0=