/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/** @type {?} */
export var MOMENT_DATE_FORMATS = {
    parse: {
        dateInput: 'DD/MM/YYYY'
    },
    display: {
        dateInput: 'DD/MM/YYYY',
        monthYearLabel: 'MMMM Y',
        dateA11yLabel: 'LL',
        monthYearA11yLabel: 'MMMM Y'
    }
};
/** @type {?} */
var dateNames = [];
for (var date = 1; date <= 31; date++) {
    dateNames.push(String(date));
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9tZW50LWRhdGUtZm9ybWF0cy5tb2RlbC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInV0aWxzL21vbWVudC1kYXRlLWZvcm1hdHMubW9kZWwudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBbUJBLE1BQU0sS0FBTyxtQkFBbUIsR0FBbUI7SUFDL0MsS0FBSyxFQUFFO1FBQ0gsU0FBUyxFQUFFLFlBQVk7S0FDMUI7SUFDRCxPQUFPLEVBQUU7UUFDTCxTQUFTLEVBQUUsWUFBWTtRQUN2QixjQUFjLEVBQUUsUUFBUTtRQUN4QixhQUFhLEVBQUUsSUFBSTtRQUNuQixrQkFBa0IsRUFBRSxRQUFRO0tBQy9CO0NBQ0o7O0lBRUssU0FBUyxHQUFhLEVBQUU7QUFDOUIsS0FBSyxJQUFJLElBQUksR0FBRyxDQUFDLEVBQUUsSUFBSSxJQUFJLEVBQUUsRUFBRSxJQUFJLEVBQUUsRUFBRTtJQUNuQyxTQUFTLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO0NBQ2hDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IE1hdERhdGVGb3JtYXRzIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5cclxuZXhwb3J0IGNvbnN0IE1PTUVOVF9EQVRFX0ZPUk1BVFM6IE1hdERhdGVGb3JtYXRzID0ge1xyXG4gICAgcGFyc2U6IHtcclxuICAgICAgICBkYXRlSW5wdXQ6ICdERC9NTS9ZWVlZJ1xyXG4gICAgfSxcclxuICAgIGRpc3BsYXk6IHtcclxuICAgICAgICBkYXRlSW5wdXQ6ICdERC9NTS9ZWVlZJyxcclxuICAgICAgICBtb250aFllYXJMYWJlbDogJ01NTU0gWScsXHJcbiAgICAgICAgZGF0ZUExMXlMYWJlbDogJ0xMJyxcclxuICAgICAgICBtb250aFllYXJBMTF5TGFiZWw6ICdNTU1NIFknXHJcbiAgICB9XHJcbn07XHJcblxyXG5jb25zdCBkYXRlTmFtZXM6IHN0cmluZ1tdID0gW107XHJcbmZvciAobGV0IGRhdGUgPSAxOyBkYXRlIDw9IDMxOyBkYXRlKyspIHtcclxuICAgIGRhdGVOYW1lcy5wdXNoKFN0cmluZyhkYXRlKSk7XHJcbn1cclxuIl19