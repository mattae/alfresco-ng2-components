/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { DateAdapter } from '@angular/material';
import { isMoment } from 'moment';
import moment from 'moment-es6';
var MomentDateAdapter = /** @class */ (function (_super) {
    tslib_1.__extends(MomentDateAdapter, _super);
    function MomentDateAdapter() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.localeData = moment.localeData();
        return _this;
    }
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.getYear = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return date.year();
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.getMonth = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return date.month();
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.getDate = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return date.date();
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.getDayOfWeek = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return date.day();
    };
    /**
     * @param {?} style
     * @return {?}
     */
    MomentDateAdapter.prototype.getMonthNames = /**
     * @param {?} style
     * @return {?}
     */
    function (style) {
        switch (style) {
            case 'long':
                return this.localeData.months();
            case 'short':
                return this.localeData.monthsShort();
            case 'narrow':
                return this.localeData.monthsShort().map((/**
                 * @param {?} month
                 * @return {?}
                 */
                function (month) { return month[0]; }));
            default:
                return;
        }
    };
    /**
     * @return {?}
     */
    MomentDateAdapter.prototype.getDateNames = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var dateNames = [];
        for (var date = 1; date <= 31; date++) {
            dateNames.push(String(date));
        }
        return dateNames;
    };
    /**
     * @param {?} style
     * @return {?}
     */
    MomentDateAdapter.prototype.getDayOfWeekNames = /**
     * @param {?} style
     * @return {?}
     */
    function (style) {
        switch (style) {
            case 'long':
                return this.localeData.weekdays();
            case 'short':
                return this.localeData.weekdaysShort();
            case 'narrow':
                return this.localeData.weekdaysShort();
            default:
                return;
        }
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.getYearName = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return String(date.year());
    };
    /**
     * @return {?}
     */
    MomentDateAdapter.prototype.getFirstDayOfWeek = /**
     * @return {?}
     */
    function () {
        return this.localeData.firstDayOfWeek();
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.getNumDaysInMonth = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return date.daysInMonth();
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.clone = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        /** @type {?} */
        var locale = this.locale || 'en';
        return date.clone().locale(locale);
    };
    /**
     * @param {?} year
     * @param {?} month
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.createDate = /**
     * @param {?} year
     * @param {?} month
     * @param {?} date
     * @return {?}
     */
    function (year, month, date) {
        return moment([year, month, date]);
    };
    /**
     * @return {?}
     */
    MomentDateAdapter.prototype.today = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var locale = this.locale || 'en';
        return moment().locale(locale);
    };
    /**
     * @param {?} value
     * @param {?} parseFormat
     * @return {?}
     */
    MomentDateAdapter.prototype.parse = /**
     * @param {?} value
     * @param {?} parseFormat
     * @return {?}
     */
    function (value, parseFormat) {
        /** @type {?} */
        var locale = this.locale || 'en';
        if (value && typeof value === 'string') {
            /** @type {?} */
            var m = moment(value, parseFormat, locale, true);
            if (!m.isValid()) {
                // use strict parsing because Moment's parser is very forgiving, and this can lead to undesired behavior.
                m = moment(value, this.overrideDisplayFormat, locale, true);
            }
            if (m.isValid()) {
                // if user omits year, it defaults to 2001, so check for that issue.
                if (m.year() === 2001 && value.indexOf('2001') === -1) {
                    // if 2001 not actually in the value string, change to current year
                    /** @type {?} */
                    var currentYear = new Date().getFullYear();
                    m.set('year', currentYear);
                    // if date is in the future, set previous year
                    if (m.isAfter(moment())) {
                        m.set('year', currentYear - 1);
                    }
                }
            }
            return m;
        }
        return value ? moment(value).locale(locale) : null;
    };
    /**
     * @param {?} date
     * @param {?} displayFormat
     * @return {?}
     */
    MomentDateAdapter.prototype.format = /**
     * @param {?} date
     * @param {?} displayFormat
     * @return {?}
     */
    function (date, displayFormat) {
        date = this.clone(date);
        displayFormat = this.overrideDisplayFormat ? this.overrideDisplayFormat : displayFormat;
        if (date && date.format) {
            return date.format(displayFormat);
        }
        else {
            return '';
        }
    };
    /**
     * @param {?} date
     * @param {?} years
     * @return {?}
     */
    MomentDateAdapter.prototype.addCalendarYears = /**
     * @param {?} date
     * @param {?} years
     * @return {?}
     */
    function (date, years) {
        return date.clone().add(years, 'y');
    };
    /**
     * @param {?} date
     * @param {?} months
     * @return {?}
     */
    MomentDateAdapter.prototype.addCalendarMonths = /**
     * @param {?} date
     * @param {?} months
     * @return {?}
     */
    function (date, months) {
        return date.clone().add(months, 'M');
    };
    /**
     * @param {?} date
     * @param {?} days
     * @return {?}
     */
    MomentDateAdapter.prototype.addCalendarDays = /**
     * @param {?} date
     * @param {?} days
     * @return {?}
     */
    function (date, days) {
        return date.clone().add(days, 'd');
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.getISODateString = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return date.toISOString();
    };
    /**
     * @param {?} locale
     * @return {?}
     */
    MomentDateAdapter.prototype.setLocale = /**
     * @param {?} locale
     * @return {?}
     */
    function (locale) {
        _super.prototype.setLocale.call(this, locale);
        this.localeData = moment.localeData(locale);
    };
    /**
     * @param {?} first
     * @param {?} second
     * @return {?}
     */
    MomentDateAdapter.prototype.compareDate = /**
     * @param {?} first
     * @param {?} second
     * @return {?}
     */
    function (first, second) {
        return first.diff(second, 'seconds', true);
    };
    /**
     * @param {?} first
     * @param {?} second
     * @return {?}
     */
    MomentDateAdapter.prototype.sameDate = /**
     * @param {?} first
     * @param {?} second
     * @return {?}
     */
    function (first, second) {
        if (first == null) {
            // same if both null
            return second == null;
        }
        else if (isMoment(first)) {
            return first.isSame(second);
        }
        else {
            /** @type {?} */
            var isSame = _super.prototype.sameDate.call(this, first, second);
            return isSame;
        }
    };
    /**
     * @param {?} date
     * @param {?=} min
     * @param {?=} max
     * @return {?}
     */
    MomentDateAdapter.prototype.clampDate = /**
     * @param {?} date
     * @param {?=} min
     * @param {?=} max
     * @return {?}
     */
    function (date, min, max) {
        if (min && date.isBefore(min)) {
            return min;
        }
        else if (max && date.isAfter(max)) {
            return max;
        }
        else {
            return date;
        }
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.isDateInstance = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        /** @type {?} */
        var isValidDateInstance = false;
        if (date) {
            isValidDateInstance = date._isAMomentObject;
        }
        return isValidDateInstance;
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.isValid = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return date.isValid();
    };
    /**
     * @param {?} date
     * @return {?}
     */
    MomentDateAdapter.prototype.toIso8601 = /**
     * @param {?} date
     * @return {?}
     */
    function (date) {
        return this.clone(date).format();
    };
    /**
     * @param {?} iso8601String
     * @return {?}
     */
    MomentDateAdapter.prototype.fromIso8601 = /**
     * @param {?} iso8601String
     * @return {?}
     */
    function (iso8601String) {
        /** @type {?} */
        var locale = this.locale || 'en';
        /** @type {?} */
        var d = moment(iso8601String, moment.ISO_8601).locale(locale);
        return this.isValid(d) ? d : null;
    };
    /**
     * @return {?}
     */
    MomentDateAdapter.prototype.invalid = /**
     * @return {?}
     */
    function () {
        return moment.invalid();
    };
    return MomentDateAdapter;
}(DateAdapter));
export { MomentDateAdapter };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MomentDateAdapter.prototype.localeData;
    /** @type {?} */
    MomentDateAdapter.prototype.overrideDisplayFormat;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibW9tZW50RGF0ZUFkYXB0ZXIuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJ1dGlscy9tb21lbnREYXRlQWRhcHRlci50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ2hELE9BQU8sRUFBRSxRQUFRLEVBQVUsTUFBTSxRQUFRLENBQUM7QUFDMUMsT0FBTyxNQUFNLE1BQU0sWUFBWSxDQUFDO0FBRWhDO0lBQXVDLDZDQUFtQjtJQUExRDtRQUFBLHFFQW9NQztRQWxNVyxnQkFBVSxHQUFRLE1BQU0sQ0FBQyxVQUFVLEVBQUUsQ0FBQzs7SUFrTWxELENBQUM7Ozs7O0lBOUxHLG1DQUFPOzs7O0lBQVAsVUFBUSxJQUFZO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQ3ZCLENBQUM7Ozs7O0lBRUQsb0NBQVE7Ozs7SUFBUixVQUFTLElBQVk7UUFDakIsT0FBTyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDeEIsQ0FBQzs7Ozs7SUFFRCxtQ0FBTzs7OztJQUFQLFVBQVEsSUFBWTtRQUNoQixPQUFPLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN2QixDQUFDOzs7OztJQUVELHdDQUFZOzs7O0lBQVosVUFBYSxJQUFZO1FBQ3JCLE9BQU8sSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO0lBQ3RCLENBQUM7Ozs7O0lBRUQseUNBQWE7Ozs7SUFBYixVQUFjLEtBQWtDO1FBQzVDLFFBQVEsS0FBSyxFQUFFO1lBQ1gsS0FBSyxNQUFNO2dCQUNQLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxNQUFNLEVBQUUsQ0FBQztZQUNwQyxLQUFLLE9BQU87Z0JBQ1IsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLFdBQVcsRUFBRSxDQUFDO1lBQ3pDLEtBQUssUUFBUTtnQkFDVCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsR0FBRzs7OztnQkFBQyxVQUFDLEtBQUssSUFBSyxPQUFBLEtBQUssQ0FBQyxDQUFDLENBQUMsRUFBUixDQUFRLEVBQUMsQ0FBQztZQUNsRTtnQkFDSSxPQUFPO1NBQ2Q7SUFDTCxDQUFDOzs7O0lBRUQsd0NBQVk7OztJQUFaOztZQUNVLFNBQVMsR0FBYSxFQUFFO1FBQzlCLEtBQUssSUFBSSxJQUFJLEdBQUcsQ0FBQyxFQUFFLElBQUksSUFBSSxFQUFFLEVBQUUsSUFBSSxFQUFFLEVBQUU7WUFDbkMsU0FBUyxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUNoQztRQUVELE9BQU8sU0FBUyxDQUFDO0lBQ3JCLENBQUM7Ozs7O0lBRUQsNkNBQWlCOzs7O0lBQWpCLFVBQWtCLEtBQWtDO1FBQ2hELFFBQVEsS0FBSyxFQUFFO1lBQ1gsS0FBSyxNQUFNO2dCQUNQLE9BQU8sSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQztZQUN0QyxLQUFLLE9BQU87Z0JBQ1IsT0FBTyxJQUFJLENBQUMsVUFBVSxDQUFDLGFBQWEsRUFBRSxDQUFDO1lBQzNDLEtBQUssUUFBUTtnQkFDVCxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxFQUFFLENBQUM7WUFDM0M7Z0JBQ0ksT0FBTztTQUNkO0lBQ0wsQ0FBQzs7Ozs7SUFFRCx1Q0FBVzs7OztJQUFYLFVBQVksSUFBWTtRQUNwQixPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsQ0FBQztJQUMvQixDQUFDOzs7O0lBRUQsNkNBQWlCOzs7SUFBakI7UUFDSSxPQUFPLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxFQUFFLENBQUM7SUFDNUMsQ0FBQzs7Ozs7SUFFRCw2Q0FBaUI7Ozs7SUFBakIsVUFBa0IsSUFBWTtRQUMxQixPQUFPLElBQUksQ0FBQyxXQUFXLEVBQUUsQ0FBQztJQUM5QixDQUFDOzs7OztJQUVELGlDQUFLOzs7O0lBQUwsVUFBTSxJQUFZOztZQUNSLE1BQU0sR0FBRyxJQUFJLENBQUMsTUFBTSxJQUFJLElBQUk7UUFDbEMsT0FBTyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7Ozs7Ozs7SUFFRCxzQ0FBVTs7Ozs7O0lBQVYsVUFBVyxJQUFZLEVBQUUsS0FBYSxFQUFFLElBQVk7UUFDaEQsT0FBTyxNQUFNLENBQUMsQ0FBQyxJQUFJLEVBQUUsS0FBSyxFQUFFLElBQUksQ0FBQyxDQUFDLENBQUM7SUFDdkMsQ0FBQzs7OztJQUVELGlDQUFLOzs7SUFBTDs7WUFDVSxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJO1FBQ2xDLE9BQU8sTUFBTSxFQUFFLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO0lBQ25DLENBQUM7Ozs7OztJQUVELGlDQUFLOzs7OztJQUFMLFVBQU0sS0FBVSxFQUFFLFdBQWdCOztZQUN4QixNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJO1FBRWxDLElBQUksS0FBSyxJQUFJLE9BQU8sS0FBSyxLQUFLLFFBQVEsRUFBRTs7Z0JBQ2hDLENBQUMsR0FBRyxNQUFNLENBQUMsS0FBSyxFQUFFLFdBQVcsRUFBRSxNQUFNLEVBQUUsSUFBSSxDQUFDO1lBQ2hELElBQUksQ0FBQyxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7Z0JBQ2QseUdBQXlHO2dCQUN6RyxDQUFDLEdBQUcsTUFBTSxDQUFDLEtBQUssRUFBRSxJQUFJLENBQUMscUJBQXFCLEVBQUUsTUFBTSxFQUFFLElBQUksQ0FBQyxDQUFDO2FBQy9EO1lBQ0QsSUFBSSxDQUFDLENBQUMsT0FBTyxFQUFFLEVBQUU7Z0JBQ2Isb0VBQW9FO2dCQUNwRSxJQUFJLENBQUMsQ0FBQyxJQUFJLEVBQUUsS0FBSyxJQUFJLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUMsRUFBRTs7O3dCQUU3QyxXQUFXLEdBQUcsSUFBSSxJQUFJLEVBQUUsQ0FBQyxXQUFXLEVBQUU7b0JBQzVDLENBQUMsQ0FBQyxHQUFHLENBQUMsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDO29CQUMzQiw4Q0FBOEM7b0JBQzlDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUUsQ0FBQyxFQUFFO3dCQUNyQixDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sRUFBRSxXQUFXLEdBQUcsQ0FBQyxDQUFDLENBQUM7cUJBQ2xDO2lCQUNKO2FBQ0o7WUFDRCxPQUFPLENBQUMsQ0FBQztTQUNaO1FBRUQsT0FBTyxLQUFLLENBQUMsQ0FBQyxDQUFDLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztJQUN2RCxDQUFDOzs7Ozs7SUFFRCxrQ0FBTTs7Ozs7SUFBTixVQUFPLElBQVksRUFBRSxhQUFrQjtRQUNuQyxJQUFJLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4QixhQUFhLEdBQUcsSUFBSSxDQUFDLHFCQUFxQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMscUJBQXFCLENBQUMsQ0FBQyxDQUFDLGFBQWEsQ0FBQztRQUV4RixJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ3JCLE9BQU8sSUFBSSxDQUFDLE1BQU0sQ0FBQyxhQUFhLENBQUMsQ0FBQztTQUNyQzthQUFNO1lBQ0gsT0FBTyxFQUFFLENBQUM7U0FDYjtJQUNMLENBQUM7Ozs7OztJQUVELDRDQUFnQjs7Ozs7SUFBaEIsVUFBaUIsSUFBWSxFQUFFLEtBQWE7UUFDeEMsT0FBTyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsR0FBRyxDQUFDLEtBQUssRUFBRSxHQUFHLENBQUMsQ0FBQztJQUN4QyxDQUFDOzs7Ozs7SUFFRCw2Q0FBaUI7Ozs7O0lBQWpCLFVBQWtCLElBQVksRUFBRSxNQUFjO1FBQzFDLE9BQU8sSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDekMsQ0FBQzs7Ozs7O0lBRUQsMkNBQWU7Ozs7O0lBQWYsVUFBZ0IsSUFBWSxFQUFFLElBQVk7UUFDdEMsT0FBTyxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUMsR0FBRyxDQUFDLElBQUksRUFBRSxHQUFHLENBQUMsQ0FBQztJQUN2QyxDQUFDOzs7OztJQUVELDRDQUFnQjs7OztJQUFoQixVQUFpQixJQUFZO1FBQ3pCLE9BQU8sSUFBSSxDQUFDLFdBQVcsRUFBRSxDQUFDO0lBQzlCLENBQUM7Ozs7O0lBRUQscUNBQVM7Ozs7SUFBVCxVQUFVLE1BQVc7UUFDakIsaUJBQU0sU0FBUyxZQUFDLE1BQU0sQ0FBQyxDQUFDO1FBRXhCLElBQUksQ0FBQyxVQUFVLEdBQUcsTUFBTSxDQUFDLFVBQVUsQ0FBQyxNQUFNLENBQUMsQ0FBQztJQUNoRCxDQUFDOzs7Ozs7SUFFRCx1Q0FBVzs7Ozs7SUFBWCxVQUFZLEtBQWEsRUFBRSxNQUFjO1FBQ3JDLE9BQU8sS0FBSyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsU0FBUyxFQUFFLElBQUksQ0FBQyxDQUFDO0lBQy9DLENBQUM7Ozs7OztJQUVELG9DQUFROzs7OztJQUFSLFVBQVMsS0FBbUIsRUFBRSxNQUFvQjtRQUM5QyxJQUFJLEtBQUssSUFBSSxJQUFJLEVBQUU7WUFDZixvQkFBb0I7WUFDcEIsT0FBTyxNQUFNLElBQUksSUFBSSxDQUFDO1NBQ3pCO2FBQU0sSUFBSSxRQUFRLENBQUMsS0FBSyxDQUFDLEVBQUU7WUFDeEIsT0FBTyxLQUFLLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQyxDQUFDO1NBQy9CO2FBQU07O2dCQUNHLE1BQU0sR0FBRyxpQkFBTSxRQUFRLFlBQUMsS0FBSyxFQUFFLE1BQU0sQ0FBQztZQUM1QyxPQUFPLE1BQU0sQ0FBQztTQUNqQjtJQUNMLENBQUM7Ozs7Ozs7SUFFRCxxQ0FBUzs7Ozs7O0lBQVQsVUFBVSxJQUFZLEVBQUUsR0FBa0IsRUFBRSxHQUFrQjtRQUMxRCxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQzNCLE9BQU8sR0FBRyxDQUFDO1NBQ2Q7YUFBTSxJQUFJLEdBQUcsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxFQUFFO1lBQ2pDLE9BQU8sR0FBRyxDQUFDO1NBQ2Q7YUFBTTtZQUNILE9BQU8sSUFBSSxDQUFDO1NBQ2Y7SUFDTCxDQUFDOzs7OztJQUVELDBDQUFjOzs7O0lBQWQsVUFBZSxJQUFTOztZQUNoQixtQkFBbUIsR0FBRyxLQUFLO1FBRS9CLElBQUksSUFBSSxFQUFFO1lBQ04sbUJBQW1CLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDO1NBQy9DO1FBRUQsT0FBTyxtQkFBbUIsQ0FBQztJQUMvQixDQUFDOzs7OztJQUVELG1DQUFPOzs7O0lBQVAsVUFBUSxJQUFZO1FBQ2hCLE9BQU8sSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDO0lBQzFCLENBQUM7Ozs7O0lBRUQscUNBQVM7Ozs7SUFBVCxVQUFVLElBQVk7UUFDbEIsT0FBTyxJQUFJLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDLE1BQU0sRUFBRSxDQUFDO0lBQ3JDLENBQUM7Ozs7O0lBRUQsdUNBQVc7Ozs7SUFBWCxVQUFZLGFBQXFCOztZQUN2QixNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sSUFBSSxJQUFJOztZQUM1QixDQUFDLEdBQUcsTUFBTSxDQUFDLGFBQWEsRUFBRSxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sQ0FBQztRQUMvRCxPQUFPLElBQUksQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDO0lBQ3RDLENBQUM7Ozs7SUFFRCxtQ0FBTzs7O0lBQVA7UUFDSSxPQUFPLE1BQU0sQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUM1QixDQUFDO0lBQ0wsd0JBQUM7QUFBRCxDQUFDLEFBcE1ELENBQXVDLFdBQVcsR0FvTWpEOzs7Ozs7O0lBbE1HLHVDQUE4Qzs7SUFFOUMsa0RBQThCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7IERhdGVBZGFwdGVyIH0gZnJvbSAnQGFuZ3VsYXIvbWF0ZXJpYWwnO1xyXG5pbXBvcnQgeyBpc01vbWVudCwgTW9tZW50IH0gZnJvbSAnbW9tZW50JztcclxuaW1wb3J0IG1vbWVudCBmcm9tICdtb21lbnQtZXM2JztcclxuXHJcbmV4cG9ydCBjbGFzcyBNb21lbnREYXRlQWRhcHRlciBleHRlbmRzIERhdGVBZGFwdGVyPE1vbWVudD4ge1xyXG5cclxuICAgIHByaXZhdGUgbG9jYWxlRGF0YTogYW55ID0gbW9tZW50LmxvY2FsZURhdGEoKTtcclxuXHJcbiAgICBvdmVycmlkZURpc3BsYXlGb3JtYXQ6IHN0cmluZztcclxuXHJcbiAgICBnZXRZZWFyKGRhdGU6IE1vbWVudCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUueWVhcigpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE1vbnRoKGRhdGU6IE1vbWVudCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUubW9udGgoKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXREYXRlKGRhdGU6IE1vbWVudCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUuZGF0ZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldERheU9mV2VlayhkYXRlOiBNb21lbnQpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiBkYXRlLmRheSgpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldE1vbnRoTmFtZXMoc3R5bGU6ICdsb25nJyB8ICdzaG9ydCcgfCAnbmFycm93Jyk6IHN0cmluZ1tdIHtcclxuICAgICAgICBzd2l0Y2ggKHN0eWxlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ2xvbmcnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxlRGF0YS5tb250aHMoKTtcclxuICAgICAgICAgICAgY2FzZSAnc2hvcnQnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxlRGF0YS5tb250aHNTaG9ydCgpO1xyXG4gICAgICAgICAgICBjYXNlICduYXJyb3cnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxlRGF0YS5tb250aHNTaG9ydCgpLm1hcCgobW9udGgpID0+IG1vbnRoWzBdKTtcclxuICAgICAgICAgICAgZGVmYXVsdCA6XHJcbiAgICAgICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGdldERhdGVOYW1lcygpOiBzdHJpbmdbXSB7XHJcbiAgICAgICAgY29uc3QgZGF0ZU5hbWVzOiBzdHJpbmdbXSA9IFtdO1xyXG4gICAgICAgIGZvciAobGV0IGRhdGUgPSAxOyBkYXRlIDw9IDMxOyBkYXRlKyspIHtcclxuICAgICAgICAgICAgZGF0ZU5hbWVzLnB1c2goU3RyaW5nKGRhdGUpKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBkYXRlTmFtZXM7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0RGF5T2ZXZWVrTmFtZXMoc3R5bGU6ICdsb25nJyB8ICdzaG9ydCcgfCAnbmFycm93Jyk6IHN0cmluZ1tdIHtcclxuICAgICAgICBzd2l0Y2ggKHN0eWxlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgJ2xvbmcnOlxyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMubG9jYWxlRGF0YS53ZWVrZGF5cygpO1xyXG4gICAgICAgICAgICBjYXNlICdzaG9ydCc6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gdGhpcy5sb2NhbGVEYXRhLndlZWtkYXlzU2hvcnQoKTtcclxuICAgICAgICAgICAgY2FzZSAnbmFycm93JzpcclxuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmxvY2FsZURhdGEud2Vla2RheXNTaG9ydCgpO1xyXG4gICAgICAgICAgICBkZWZhdWx0IDpcclxuICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0WWVhck5hbWUoZGF0ZTogTW9tZW50KTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gU3RyaW5nKGRhdGUueWVhcigpKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRGaXJzdERheU9mV2VlaygpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmxvY2FsZURhdGEuZmlyc3REYXlPZldlZWsoKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXROdW1EYXlzSW5Nb250aChkYXRlOiBNb21lbnQpOiBudW1iZXIge1xyXG4gICAgICAgIHJldHVybiBkYXRlLmRheXNJbk1vbnRoKCk7XHJcbiAgICB9XHJcblxyXG4gICAgY2xvbmUoZGF0ZTogTW9tZW50KTogTW9tZW50IHtcclxuICAgICAgICBjb25zdCBsb2NhbGUgPSB0aGlzLmxvY2FsZSB8fCAnZW4nO1xyXG4gICAgICAgIHJldHVybiBkYXRlLmNsb25lKCkubG9jYWxlKGxvY2FsZSk7XHJcbiAgICB9XHJcblxyXG4gICAgY3JlYXRlRGF0ZSh5ZWFyOiBudW1iZXIsIG1vbnRoOiBudW1iZXIsIGRhdGU6IG51bWJlcik6IE1vbWVudCB7XHJcbiAgICAgICAgcmV0dXJuIG1vbWVudChbeWVhciwgbW9udGgsIGRhdGVdKTtcclxuICAgIH1cclxuXHJcbiAgICB0b2RheSgpOiBNb21lbnQge1xyXG4gICAgICAgIGNvbnN0IGxvY2FsZSA9IHRoaXMubG9jYWxlIHx8ICdlbic7XHJcbiAgICAgICAgcmV0dXJuIG1vbWVudCgpLmxvY2FsZShsb2NhbGUpO1xyXG4gICAgfVxyXG5cclxuICAgIHBhcnNlKHZhbHVlOiBhbnksIHBhcnNlRm9ybWF0OiBhbnkpOiBNb21lbnQge1xyXG4gICAgICAgIGNvbnN0IGxvY2FsZSA9IHRoaXMubG9jYWxlIHx8ICdlbic7XHJcblxyXG4gICAgICAgIGlmICh2YWx1ZSAmJiB0eXBlb2YgdmFsdWUgPT09ICdzdHJpbmcnKSB7XHJcbiAgICAgICAgICAgIGxldCBtID0gbW9tZW50KHZhbHVlLCBwYXJzZUZvcm1hdCwgbG9jYWxlLCB0cnVlKTtcclxuICAgICAgICAgICAgaWYgKCFtLmlzVmFsaWQoKSkge1xyXG4gICAgICAgICAgICAgICAgLy8gdXNlIHN0cmljdCBwYXJzaW5nIGJlY2F1c2UgTW9tZW50J3MgcGFyc2VyIGlzIHZlcnkgZm9yZ2l2aW5nLCBhbmQgdGhpcyBjYW4gbGVhZCB0byB1bmRlc2lyZWQgYmVoYXZpb3IuXHJcbiAgICAgICAgICAgICAgICBtID0gbW9tZW50KHZhbHVlLCB0aGlzLm92ZXJyaWRlRGlzcGxheUZvcm1hdCwgbG9jYWxlLCB0cnVlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAobS5pc1ZhbGlkKCkpIHtcclxuICAgICAgICAgICAgICAgIC8vIGlmIHVzZXIgb21pdHMgeWVhciwgaXQgZGVmYXVsdHMgdG8gMjAwMSwgc28gY2hlY2sgZm9yIHRoYXQgaXNzdWUuXHJcbiAgICAgICAgICAgICAgICBpZiAobS55ZWFyKCkgPT09IDIwMDEgJiYgdmFsdWUuaW5kZXhPZignMjAwMScpID09PSAtMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIGlmIDIwMDEgbm90IGFjdHVhbGx5IGluIHRoZSB2YWx1ZSBzdHJpbmcsIGNoYW5nZSB0byBjdXJyZW50IHllYXJcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBjdXJyZW50WWVhciA9IG5ldyBEYXRlKCkuZ2V0RnVsbFllYXIoKTtcclxuICAgICAgICAgICAgICAgICAgICBtLnNldCgneWVhcicsIGN1cnJlbnRZZWFyKTtcclxuICAgICAgICAgICAgICAgICAgICAvLyBpZiBkYXRlIGlzIGluIHRoZSBmdXR1cmUsIHNldCBwcmV2aW91cyB5ZWFyXHJcbiAgICAgICAgICAgICAgICAgICAgaWYgKG0uaXNBZnRlcihtb21lbnQoKSkpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbS5zZXQoJ3llYXInLCBjdXJyZW50WWVhciAtIDEpO1xyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICByZXR1cm4gbTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiB2YWx1ZSA/IG1vbWVudCh2YWx1ZSkubG9jYWxlKGxvY2FsZSkgOiBudWxsO1xyXG4gICAgfVxyXG5cclxuICAgIGZvcm1hdChkYXRlOiBNb21lbnQsIGRpc3BsYXlGb3JtYXQ6IGFueSk6IHN0cmluZyB7XHJcbiAgICAgICAgZGF0ZSA9IHRoaXMuY2xvbmUoZGF0ZSk7XHJcbiAgICAgICAgZGlzcGxheUZvcm1hdCA9IHRoaXMub3ZlcnJpZGVEaXNwbGF5Rm9ybWF0ID8gdGhpcy5vdmVycmlkZURpc3BsYXlGb3JtYXQgOiBkaXNwbGF5Rm9ybWF0O1xyXG5cclxuICAgICAgICBpZiAoZGF0ZSAmJiBkYXRlLmZvcm1hdCkge1xyXG4gICAgICAgICAgICByZXR1cm4gZGF0ZS5mb3JtYXQoZGlzcGxheUZvcm1hdCk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuICcnO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBhZGRDYWxlbmRhclllYXJzKGRhdGU6IE1vbWVudCwgeWVhcnM6IG51bWJlcik6IE1vbWVudCB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUuY2xvbmUoKS5hZGQoeWVhcnMsICd5Jyk7XHJcbiAgICB9XHJcblxyXG4gICAgYWRkQ2FsZW5kYXJNb250aHMoZGF0ZTogTW9tZW50LCBtb250aHM6IG51bWJlcik6IE1vbWVudCB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUuY2xvbmUoKS5hZGQobW9udGhzLCAnTScpO1xyXG4gICAgfVxyXG5cclxuICAgIGFkZENhbGVuZGFyRGF5cyhkYXRlOiBNb21lbnQsIGRheXM6IG51bWJlcik6IE1vbWVudCB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUuY2xvbmUoKS5hZGQoZGF5cywgJ2QnKTtcclxuICAgIH1cclxuXHJcbiAgICBnZXRJU09EYXRlU3RyaW5nKGRhdGU6IE1vbWVudCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUudG9JU09TdHJpbmcoKTtcclxuICAgIH1cclxuXHJcbiAgICBzZXRMb2NhbGUobG9jYWxlOiBhbnkpOiB2b2lkIHtcclxuICAgICAgICBzdXBlci5zZXRMb2NhbGUobG9jYWxlKTtcclxuXHJcbiAgICAgICAgdGhpcy5sb2NhbGVEYXRhID0gbW9tZW50LmxvY2FsZURhdGEobG9jYWxlKTtcclxuICAgIH1cclxuXHJcbiAgICBjb21wYXJlRGF0ZShmaXJzdDogTW9tZW50LCBzZWNvbmQ6IE1vbWVudCk6IG51bWJlciB7XHJcbiAgICAgICAgcmV0dXJuIGZpcnN0LmRpZmYoc2Vjb25kLCAnc2Vjb25kcycsIHRydWUpO1xyXG4gICAgfVxyXG5cclxuICAgIHNhbWVEYXRlKGZpcnN0OiBhbnkgfCBNb21lbnQsIHNlY29uZDogYW55IHwgTW9tZW50KTogYm9vbGVhbiB7XHJcbiAgICAgICAgaWYgKGZpcnN0ID09IG51bGwpIHtcclxuICAgICAgICAgICAgLy8gc2FtZSBpZiBib3RoIG51bGxcclxuICAgICAgICAgICAgcmV0dXJuIHNlY29uZCA9PSBudWxsO1xyXG4gICAgICAgIH0gZWxzZSBpZiAoaXNNb21lbnQoZmlyc3QpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmaXJzdC5pc1NhbWUoc2Vjb25kKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBjb25zdCBpc1NhbWUgPSBzdXBlci5zYW1lRGF0ZShmaXJzdCwgc2Vjb25kKTtcclxuICAgICAgICAgICAgcmV0dXJuIGlzU2FtZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgY2xhbXBEYXRlKGRhdGU6IE1vbWVudCwgbWluPzogYW55IHwgTW9tZW50LCBtYXg/OiBhbnkgfCBNb21lbnQpOiBNb21lbnQge1xyXG4gICAgICAgIGlmIChtaW4gJiYgZGF0ZS5pc0JlZm9yZShtaW4pKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBtaW47XHJcbiAgICAgICAgfSBlbHNlIGlmIChtYXggJiYgZGF0ZS5pc0FmdGVyKG1heCkpIHtcclxuICAgICAgICAgICAgcmV0dXJuIG1heDtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICByZXR1cm4gZGF0ZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgaXNEYXRlSW5zdGFuY2UoZGF0ZTogYW55KSB7XHJcbiAgICAgICAgbGV0IGlzVmFsaWREYXRlSW5zdGFuY2UgPSBmYWxzZTtcclxuXHJcbiAgICAgICAgaWYgKGRhdGUpIHtcclxuICAgICAgICAgICAgaXNWYWxpZERhdGVJbnN0YW5jZSA9IGRhdGUuX2lzQU1vbWVudE9iamVjdDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIHJldHVybiBpc1ZhbGlkRGF0ZUluc3RhbmNlO1xyXG4gICAgfVxyXG5cclxuICAgIGlzVmFsaWQoZGF0ZTogTW9tZW50KTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIGRhdGUuaXNWYWxpZCgpO1xyXG4gICAgfVxyXG5cclxuICAgIHRvSXNvODYwMShkYXRlOiBNb21lbnQpOiBzdHJpbmcge1xyXG4gICAgICAgIHJldHVybiB0aGlzLmNsb25lKGRhdGUpLmZvcm1hdCgpO1xyXG4gICAgfVxyXG5cclxuICAgIGZyb21Jc284NjAxKGlzbzg2MDFTdHJpbmc6IHN0cmluZyk6IE1vbWVudCB8IG51bGwge1xyXG4gICAgICAgIGNvbnN0IGxvY2FsZSA9IHRoaXMubG9jYWxlIHx8ICdlbic7XHJcbiAgICAgICAgY29uc3QgZCA9IG1vbWVudChpc284NjAxU3RyaW5nLCBtb21lbnQuSVNPXzg2MDEpLmxvY2FsZShsb2NhbGUpO1xyXG4gICAgICAgIHJldHVybiB0aGlzLmlzVmFsaWQoZCkgPyBkIDogbnVsbDtcclxuICAgIH1cclxuXHJcbiAgICBpbnZhbGlkKCk6IE1vbWVudCB7XHJcbiAgICAgICAgcmV0dXJuIG1vbWVudC5pbnZhbGlkKCk7XHJcbiAgICB9XHJcbn1cclxuIl19