/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
var ObjectUtils = /** @class */ (function () {
    function ObjectUtils() {
    }
    /**
     * Gets a value from an object by composed key
     * ObjectUtils.getValue({ item: { nodeType: 'cm:folder' }}, 'item.nodeType') ==> 'cm:folder'
     * @param target
     * @param key
     */
    /**
     * Gets a value from an object by composed key
     * ObjectUtils.getValue({ item: { nodeType: 'cm:folder' }}, 'item.nodeType') ==> 'cm:folder'
     * @param {?} target
     * @param {?} key
     * @return {?}
     */
    ObjectUtils.getValue = /**
     * Gets a value from an object by composed key
     * ObjectUtils.getValue({ item: { nodeType: 'cm:folder' }}, 'item.nodeType') ==> 'cm:folder'
     * @param {?} target
     * @param {?} key
     * @return {?}
     */
    function (target, key) {
        if (!target) {
            return undefined;
        }
        /** @type {?} */
        var keys = key.split('.');
        key = '';
        do {
            key += keys.shift();
            /** @type {?} */
            var value = target[key];
            if (value !== undefined && (typeof value === 'object' || !keys.length)) {
                target = value;
                key = '';
            }
            else if (!keys.length) {
                target = undefined;
            }
            else {
                key += '.';
            }
        } while (keys.length);
        return target;
    };
    /**
     * @param {...?} objects
     * @return {?}
     */
    ObjectUtils.merge = /**
     * @param {...?} objects
     * @return {?}
     */
    function () {
        var objects = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            objects[_i] = arguments[_i];
        }
        /** @type {?} */
        var result = {};
        objects.forEach((/**
         * @param {?} source
         * @return {?}
         */
        function (source) {
            Object.keys(source).forEach((/**
             * @param {?} prop
             * @return {?}
             */
            function (prop) {
                if (prop in result && Array.isArray(result[prop])) {
                    result[prop] = result[prop].concat(source[prop]);
                }
                else if (prop in result && typeof result[prop] === 'object') {
                    result[prop] = ObjectUtils.merge(result[prop], source[prop]);
                }
                else {
                    result[prop] = source[prop];
                }
            }));
        }));
        return result;
    };
    return ObjectUtils;
}());
export { ObjectUtils };
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoib2JqZWN0LXV0aWxzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsidXRpbHMvb2JqZWN0LXV0aWxzLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBO0lBQUE7SUFpREEsQ0FBQztJQWhERzs7Ozs7T0FLRzs7Ozs7Ozs7SUFDSSxvQkFBUTs7Ozs7OztJQUFmLFVBQWdCLE1BQVcsRUFBRSxHQUFXO1FBRXBDLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDVCxPQUFPLFNBQVMsQ0FBQztTQUNwQjs7WUFFSyxJQUFJLEdBQUcsR0FBRyxDQUFDLEtBQUssQ0FBQyxHQUFHLENBQUM7UUFDM0IsR0FBRyxHQUFHLEVBQUUsQ0FBQztRQUVULEdBQUc7WUFDQyxHQUFHLElBQUksSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDOztnQkFDZCxLQUFLLEdBQUcsTUFBTSxDQUFDLEdBQUcsQ0FBQztZQUN6QixJQUFJLEtBQUssS0FBSyxTQUFTLElBQUksQ0FBQyxPQUFPLEtBQUssS0FBSyxRQUFRLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLEVBQUU7Z0JBQ3BFLE1BQU0sR0FBRyxLQUFLLENBQUM7Z0JBQ2YsR0FBRyxHQUFHLEVBQUUsQ0FBQzthQUNaO2lCQUFNLElBQUksQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFO2dCQUNyQixNQUFNLEdBQUcsU0FBUyxDQUFDO2FBQ3RCO2lCQUFNO2dCQUNILEdBQUcsSUFBSSxHQUFHLENBQUM7YUFDZDtTQUNKLFFBQVEsSUFBSSxDQUFDLE1BQU0sRUFBRTtRQUV0QixPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDOzs7OztJQUVNLGlCQUFLOzs7O0lBQVo7UUFBYSxpQkFBVTthQUFWLFVBQVUsRUFBVixxQkFBVSxFQUFWLElBQVU7WUFBViw0QkFBVTs7O1lBQ2IsTUFBTSxHQUFHLEVBQUU7UUFFakIsT0FBTyxDQUFDLE9BQU87Ozs7UUFBQyxVQUFDLE1BQU07WUFDbkIsTUFBTSxDQUFDLElBQUksQ0FBQyxNQUFNLENBQUMsQ0FBQyxPQUFPOzs7O1lBQUMsVUFBQyxJQUFJO2dCQUM3QixJQUFJLElBQUksSUFBSSxNQUFNLElBQUksS0FBSyxDQUFDLE9BQU8sQ0FBQyxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsRUFBRTtvQkFDL0MsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQyxNQUFNLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7aUJBQ3BEO3FCQUFNLElBQUksSUFBSSxJQUFJLE1BQU0sSUFBSSxPQUFPLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxRQUFRLEVBQUU7b0JBQzNELE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxXQUFXLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsRUFBRSxNQUFNLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztpQkFDaEU7cUJBQU07b0JBQ0gsTUFBTSxDQUFDLElBQUksQ0FBQyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztpQkFDL0I7WUFDTCxDQUFDLEVBQUMsQ0FBQztRQUNQLENBQUMsRUFBQyxDQUFDO1FBRUgsT0FBTyxNQUFNLENBQUM7SUFDbEIsQ0FBQztJQUNMLGtCQUFDO0FBQUQsQ0FBQyxBQWpERCxJQWlEQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5leHBvcnQgY2xhc3MgT2JqZWN0VXRpbHMge1xyXG4gICAgLyoqXHJcbiAgICAgKiBHZXRzIGEgdmFsdWUgZnJvbSBhbiBvYmplY3QgYnkgY29tcG9zZWQga2V5XHJcbiAgICAgKiBPYmplY3RVdGlscy5nZXRWYWx1ZSh7IGl0ZW06IHsgbm9kZVR5cGU6ICdjbTpmb2xkZXInIH19LCAnaXRlbS5ub2RlVHlwZScpID09PiAnY206Zm9sZGVyJ1xyXG4gICAgICogQHBhcmFtIHRhcmdldFxyXG4gICAgICogQHBhcmFtIGtleVxyXG4gICAgICovXHJcbiAgICBzdGF0aWMgZ2V0VmFsdWUodGFyZ2V0OiBhbnksIGtleTogc3RyaW5nKTogYW55IHtcclxuXHJcbiAgICAgICAgaWYgKCF0YXJnZXQpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHVuZGVmaW5lZDtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IGtleXMgPSBrZXkuc3BsaXQoJy4nKTtcclxuICAgICAgICBrZXkgPSAnJztcclxuXHJcbiAgICAgICAgZG8ge1xyXG4gICAgICAgICAgICBrZXkgKz0ga2V5cy5zaGlmdCgpO1xyXG4gICAgICAgICAgICBjb25zdCB2YWx1ZSA9IHRhcmdldFtrZXldO1xyXG4gICAgICAgICAgICBpZiAodmFsdWUgIT09IHVuZGVmaW5lZCAmJiAodHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyB8fCAha2V5cy5sZW5ndGgpKSB7XHJcbiAgICAgICAgICAgICAgICB0YXJnZXQgPSB2YWx1ZTtcclxuICAgICAgICAgICAgICAgIGtleSA9ICcnO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKCFrZXlzLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgdGFyZ2V0ID0gdW5kZWZpbmVkO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAga2V5ICs9ICcuJztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gd2hpbGUgKGtleXMubGVuZ3RoKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHRhcmdldDtcclxuICAgIH1cclxuXHJcbiAgICBzdGF0aWMgbWVyZ2UoLi4ub2JqZWN0cyk6IGFueSB7XHJcbiAgICAgICAgY29uc3QgcmVzdWx0ID0ge307XHJcblxyXG4gICAgICAgIG9iamVjdHMuZm9yRWFjaCgoc291cmNlKSA9PiB7XHJcbiAgICAgICAgICAgIE9iamVjdC5rZXlzKHNvdXJjZSkuZm9yRWFjaCgocHJvcCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHByb3AgaW4gcmVzdWx0ICYmIEFycmF5LmlzQXJyYXkocmVzdWx0W3Byb3BdKSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdFtwcm9wXSA9IHJlc3VsdFtwcm9wXS5jb25jYXQoc291cmNlW3Byb3BdKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocHJvcCBpbiByZXN1bHQgJiYgdHlwZW9mIHJlc3VsdFtwcm9wXSA9PT0gJ29iamVjdCcpIHtcclxuICAgICAgICAgICAgICAgICAgICByZXN1bHRbcHJvcF0gPSBPYmplY3RVdGlscy5tZXJnZShyZXN1bHRbcHJvcF0sIHNvdXJjZVtwcm9wXSk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdFtwcm9wXSA9IHNvdXJjZVtwcm9wXTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcblxyXG4gICAgICAgIHJldHVybiByZXN1bHQ7XHJcbiAgICB9XHJcbn1cclxuIl19