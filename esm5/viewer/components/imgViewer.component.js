/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewEncapsulation, ElementRef } from '@angular/core';
import { ContentService } from '../../services/content.service';
import { AppConfigService } from './../../app-config/app-config.service';
import { DomSanitizer } from '@angular/platform-browser';
var ImgViewerComponent = /** @class */ (function () {
    function ImgViewerComponent(sanitizer, appConfigService, contentService, el) {
        this.sanitizer = sanitizer;
        this.appConfigService = appConfigService;
        this.contentService = contentService;
        this.el = el;
        this.showToolbar = true;
        this.rotate = 0;
        this.scaleX = 1.0;
        this.scaleY = 1.0;
        this.offsetX = 0;
        this.offsetY = 0;
        this.isDragged = false;
        this.drag = { x: 0, y: 0 };
        this.delta = { x: 0, y: 0 };
        this.initializeScaling();
    }
    Object.defineProperty(ImgViewerComponent.prototype, "transform", {
        get: /**
         * @return {?}
         */
        function () {
            return this.sanitizer.bypassSecurityTrustStyle("scale(" + this.scaleX + ", " + this.scaleY + ") rotate(" + this.rotate + "deg) translate(" + this.offsetX + "px, " + this.offsetY + "px)");
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ImgViewerComponent.prototype, "currentScaleText", {
        get: /**
         * @return {?}
         */
        function () {
            return Math.round(this.scaleX * 100) + '%';
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    ImgViewerComponent.prototype.initializeScaling = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var scaling = this.appConfigService.get('adf-viewer.image-viewer-scaling', undefined) / 100;
        if (scaling) {
            this.scaleX = scaling;
            this.scaleY = scaling;
        }
    };
    /**
     * @return {?}
     */
    ImgViewerComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.element = (/** @type {?} */ (this.el.nativeElement.querySelector('#viewer-image')));
        if (this.element) {
            this.element.addEventListener('mousedown', this.onMouseDown.bind(this));
            this.element.addEventListener('mouseup', this.onMouseUp.bind(this));
            this.element.addEventListener('mouseleave', this.onMouseLeave.bind(this));
            this.element.addEventListener('mouseout', this.onMouseOut.bind(this));
            this.element.addEventListener('mousemove', this.onMouseMove.bind(this));
        }
    };
    /**
     * @return {?}
     */
    ImgViewerComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.element) {
            this.element.removeEventListener('mousedown', this.onMouseDown);
            this.element.removeEventListener('mouseup', this.onMouseUp);
            this.element.removeEventListener('mouseleave', this.onMouseLeave);
            this.element.removeEventListener('mouseout', this.onMouseOut);
            this.element.removeEventListener('mousemove', this.onMouseMove);
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ImgViewerComponent.prototype.onMouseDown = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        event.preventDefault();
        this.isDragged = true;
        this.drag = { x: event.pageX, y: event.pageY };
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ImgViewerComponent.prototype.onMouseMove = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.isDragged) {
            event.preventDefault();
            this.delta.x = event.pageX - this.drag.x;
            this.delta.y = event.pageY - this.drag.y;
            this.drag.x = event.pageX;
            this.drag.y = event.pageY;
            /** @type {?} */
            var scaleX = (this.scaleX !== 0 ? this.scaleX : 1.0);
            /** @type {?} */
            var scaleY = (this.scaleY !== 0 ? this.scaleY : 1.0);
            this.offsetX += (this.delta.x / scaleX);
            this.offsetY += (this.delta.y / scaleY);
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ImgViewerComponent.prototype.onMouseUp = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.isDragged) {
            event.preventDefault();
            this.isDragged = false;
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ImgViewerComponent.prototype.onMouseLeave = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.isDragged) {
            event.preventDefault();
            this.isDragged = false;
        }
    };
    /**
     * @param {?} event
     * @return {?}
     */
    ImgViewerComponent.prototype.onMouseOut = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        if (this.isDragged) {
            event.preventDefault();
            this.isDragged = false;
        }
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    ImgViewerComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        /** @type {?} */
        var blobFile = changes['blobFile'];
        if (blobFile && blobFile.currentValue) {
            this.urlFile = this.contentService.createTrustedUrl(this.blobFile);
            return;
        }
        if (!this.urlFile && !this.blobFile) {
            throw new Error('Attribute urlFile or blobFile is required');
        }
    };
    /**
     * @return {?}
     */
    ImgViewerComponent.prototype.zoomIn = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var ratio = +((this.scaleX + 0.2).toFixed(1));
        this.scaleX = this.scaleY = ratio;
    };
    /**
     * @return {?}
     */
    ImgViewerComponent.prototype.zoomOut = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var ratio = +((this.scaleX - 0.2).toFixed(1));
        if (ratio < 0.2) {
            ratio = 0.2;
        }
        this.scaleX = this.scaleY = ratio;
    };
    /**
     * @return {?}
     */
    ImgViewerComponent.prototype.rotateLeft = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var angle = this.rotate - 90;
        this.rotate = Math.abs(angle) < 360 ? angle : 0;
    };
    /**
     * @return {?}
     */
    ImgViewerComponent.prototype.rotateRight = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var angle = this.rotate + 90;
        this.rotate = Math.abs(angle) < 360 ? angle : 0;
    };
    /**
     * @return {?}
     */
    ImgViewerComponent.prototype.reset = /**
     * @return {?}
     */
    function () {
        this.rotate = 0;
        this.scaleX = 1.0;
        this.scaleY = 1.0;
        this.offsetX = 0;
        this.offsetY = 0;
    };
    ImgViewerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-img-viewer',
                    template: "<div id=\"adf-image-container\"  class=\"adf-image-container\" tabindex=\"0\" role=\"img\" [attr.aria-label]=\"nameFile\" [style.transform]=\"transform\" data-automation-id=\"adf-image-container\">\r\n    <img id=\"viewer-image\" [src]=\"urlFile\" [alt]=\"nameFile\" [ngStyle]=\"{ 'cursor' : isDragged ? 'move': 'default' } \" />\r\n</div>\r\n\r\n<div class=\"adf-image-viewer__toolbar\" *ngIf=\"showToolbar\">\r\n    <adf-toolbar>\r\n        <button\r\n            id=\"viewer-zoom-in-button\"\r\n            mat-icon-button\r\n            title=\"{{ 'ADF_VIEWER.ARIA.ZOOM_IN' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.ZOOM_IN' | translate }}\"\r\n            (click)=\"zoomIn()\">\r\n            <mat-icon>zoom_in</mat-icon>\r\n        </button>\r\n\r\n        <button\r\n            id=\"viewer-zoom-out-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.ZOOM_OUT' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.ZOOM_OUT' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"zoomOut()\">\r\n            <mat-icon>zoom_out</mat-icon>\r\n        </button>\r\n\r\n        <div class=\"adf-viewer__toolbar-page-scale\" data-automation-id=\"adf-page-scale\">\r\n            {{ currentScaleText }}\r\n        </div>\r\n\r\n        <button\r\n            id=\"viewer-rotate-left-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.ROTATE_LEFT' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.ROTATE_LEFT' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"rotateLeft()\">\r\n            <mat-icon>rotate_left</mat-icon>\r\n        </button>\r\n\r\n        <button\r\n            id=\"viewer-rotate-right-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.ROTATE_RIGHT' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.ROTATE_RIGHT' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"rotateRight()\">\r\n            <mat-icon>rotate_right</mat-icon>\r\n        </button>\r\n\r\n        <button\r\n            id=\"viewer-reset-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.RESET' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.RESET' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"reset()\">\r\n            <mat-icon>zoom_out_map</mat-icon>\r\n        </button>\r\n    </adf-toolbar>\r\n</div>\r\n",
                    host: { 'class': 'adf-image-viewer' },
                    encapsulation: ViewEncapsulation.None,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    ImgViewerComponent.ctorParameters = function () { return [
        { type: DomSanitizer },
        { type: AppConfigService },
        { type: ContentService },
        { type: ElementRef }
    ]; };
    ImgViewerComponent.propDecorators = {
        showToolbar: [{ type: Input }],
        urlFile: [{ type: Input }],
        blobFile: [{ type: Input }],
        nameFile: [{ type: Input }]
    };
    return ImgViewerComponent;
}());
export { ImgViewerComponent };
if (false) {
    /** @type {?} */
    ImgViewerComponent.prototype.showToolbar;
    /** @type {?} */
    ImgViewerComponent.prototype.urlFile;
    /** @type {?} */
    ImgViewerComponent.prototype.blobFile;
    /** @type {?} */
    ImgViewerComponent.prototype.nameFile;
    /** @type {?} */
    ImgViewerComponent.prototype.rotate;
    /** @type {?} */
    ImgViewerComponent.prototype.scaleX;
    /** @type {?} */
    ImgViewerComponent.prototype.scaleY;
    /** @type {?} */
    ImgViewerComponent.prototype.offsetX;
    /** @type {?} */
    ImgViewerComponent.prototype.offsetY;
    /** @type {?} */
    ImgViewerComponent.prototype.isDragged;
    /**
     * @type {?}
     * @private
     */
    ImgViewerComponent.prototype.drag;
    /**
     * @type {?}
     * @private
     */
    ImgViewerComponent.prototype.delta;
    /**
     * @type {?}
     * @private
     */
    ImgViewerComponent.prototype.element;
    /**
     * @type {?}
     * @private
     */
    ImgViewerComponent.prototype.sanitizer;
    /**
     * @type {?}
     * @private
     */
    ImgViewerComponent.prototype.appConfigService;
    /**
     * @type {?}
     * @private
     */
    ImgViewerComponent.prototype.contentService;
    /**
     * @type {?}
     * @private
     */
    ImgViewerComponent.prototype.el;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW1nVmlld2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInZpZXdlci9jb21wb25lbnRzL2ltZ1ZpZXdlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUNILFNBQVMsRUFDVCxLQUFLLEVBR0wsaUJBQWlCLEVBQ2pCLFVBQVUsRUFHYixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFDaEUsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0sdUNBQXVDLENBQUM7QUFDekUsT0FBTyxFQUFFLFlBQVksRUFBYSxNQUFNLDJCQUEyQixDQUFDO0FBRXBFO0lBeUNJLDRCQUNZLFNBQXVCLEVBQ3ZCLGdCQUFrQyxFQUNsQyxjQUE4QixFQUM5QixFQUFjO1FBSGQsY0FBUyxHQUFULFNBQVMsQ0FBYztRQUN2QixxQkFBZ0IsR0FBaEIsZ0JBQWdCLENBQWtCO1FBQ2xDLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtRQUM5QixPQUFFLEdBQUYsRUFBRSxDQUFZO1FBbkMxQixnQkFBVyxHQUFHLElBQUksQ0FBQztRQVduQixXQUFNLEdBQVcsQ0FBQyxDQUFDO1FBQ25CLFdBQU0sR0FBVyxHQUFHLENBQUM7UUFDckIsV0FBTSxHQUFXLEdBQUcsQ0FBQztRQUNyQixZQUFPLEdBQVcsQ0FBQyxDQUFDO1FBQ3BCLFlBQU8sR0FBVyxDQUFDLENBQUM7UUFDcEIsY0FBUyxHQUFZLEtBQUssQ0FBQztRQUVuQixTQUFJLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQztRQUN0QixVQUFLLEdBQUcsRUFBRSxDQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUUsQ0FBQztRQWlCM0IsSUFBSSxDQUFDLGlCQUFpQixFQUFFLENBQUM7SUFDN0IsQ0FBQztJQWhCRCxzQkFBSSx5Q0FBUzs7OztRQUFiO1lBQ0ksT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLHdCQUF3QixDQUFDLFdBQVMsSUFBSSxDQUFDLE1BQU0sVUFBSyxJQUFJLENBQUMsTUFBTSxpQkFBWSxJQUFJLENBQUMsTUFBTSx1QkFBa0IsSUFBSSxDQUFDLE9BQU8sWUFBTyxJQUFJLENBQUMsT0FBTyxRQUFLLENBQUMsQ0FBQztRQUN0SyxDQUFDOzs7T0FBQTtJQUVELHNCQUFJLGdEQUFnQjs7OztRQUFwQjtZQUNJLE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxHQUFHLEdBQUcsQ0FBQztRQUMvQyxDQUFDOzs7T0FBQTs7OztJQVlELDhDQUFpQjs7O0lBQWpCOztZQUNVLE9BQU8sR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFTLGlDQUFpQyxFQUFFLFNBQVMsQ0FBQyxHQUFHLEdBQUc7UUFDckcsSUFBSSxPQUFPLEVBQUU7WUFDVCxJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQztZQUN0QixJQUFJLENBQUMsTUFBTSxHQUFHLE9BQU8sQ0FBQztTQUN6QjtJQUNMLENBQUM7Ozs7SUFFRCxxQ0FBUTs7O0lBQVI7UUFDSSxJQUFJLENBQUMsT0FBTyxHQUFHLG1CQUFjLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxlQUFlLENBQUMsRUFBQSxDQUFDO1FBRWxGLElBQUksSUFBSSxDQUFDLE9BQU8sRUFBRTtZQUNkLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsV0FBVyxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDeEUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUNwRSxJQUFJLENBQUMsT0FBTyxDQUFDLGdCQUFnQixDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBQzFFLElBQUksQ0FBQyxPQUFPLENBQUMsZ0JBQWdCLENBQUMsVUFBVSxFQUFFLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLENBQUM7WUFDdEUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztTQUMzRTtJQUNMLENBQUM7Ozs7SUFFRCx3Q0FBVzs7O0lBQVg7UUFDSSxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7WUFDZCxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLFdBQVcsRUFBRSxJQUFJLENBQUMsV0FBVyxDQUFDLENBQUM7WUFDaEUsSUFBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxTQUFTLEVBQUUsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1lBQzVELElBQUksQ0FBQyxPQUFPLENBQUMsbUJBQW1CLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZLENBQUMsQ0FBQztZQUNsRSxJQUFJLENBQUMsT0FBTyxDQUFDLG1CQUFtQixDQUFDLFVBQVUsRUFBRSxJQUFJLENBQUMsVUFBVSxDQUFDLENBQUM7WUFDOUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxtQkFBbUIsQ0FBQyxXQUFXLEVBQUUsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDO1NBQ25FO0lBQ0wsQ0FBQzs7Ozs7SUFFRCx3Q0FBVzs7OztJQUFYLFVBQVksS0FBaUI7UUFDekIsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1FBQ3ZCLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDO1FBQ3RCLElBQUksQ0FBQyxJQUFJLEdBQUcsRUFBRSxDQUFDLEVBQUUsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDLEVBQUUsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO0lBQ25ELENBQUM7Ozs7O0lBRUQsd0NBQVc7Ozs7SUFBWCxVQUFZLEtBQWlCO1FBQ3pCLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7WUFFdkIsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQztZQUN6QyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxLQUFLLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDO1lBRXpDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQyxHQUFHLEtBQUssQ0FBQyxLQUFLLENBQUM7WUFDMUIsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDLEdBQUcsS0FBSyxDQUFDLEtBQUssQ0FBQzs7Z0JBRXBCLE1BQU0sR0FBRyxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxDQUFDLENBQUMsQ0FBQyxHQUFHLENBQUM7O2dCQUNoRCxNQUFNLEdBQUcsQ0FBQyxJQUFJLENBQUMsTUFBTSxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDO1lBRXRELElBQUksQ0FBQyxPQUFPLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUMsR0FBRyxNQUFNLENBQUMsQ0FBQztZQUN4QyxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLEdBQUcsTUFBTSxDQUFDLENBQUM7U0FDM0M7SUFDTCxDQUFDOzs7OztJQUVELHNDQUFTOzs7O0lBQVQsVUFBVSxLQUFpQjtRQUN2QixJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDaEIsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO1NBQzFCO0lBQ0wsQ0FBQzs7Ozs7SUFFRCx5Q0FBWTs7OztJQUFaLFVBQWEsS0FBaUI7UUFDMUIsSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN2QixJQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztTQUMxQjtJQUNMLENBQUM7Ozs7O0lBRUQsdUNBQVU7Ozs7SUFBVixVQUFXLEtBQWlCO1FBQ3hCLElBQUksSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNoQixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdkIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7U0FDMUI7SUFDTCxDQUFDOzs7OztJQUVELHdDQUFXOzs7O0lBQVgsVUFBWSxPQUFzQjs7WUFDeEIsUUFBUSxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUM7UUFDcEMsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFlBQVksRUFBRTtZQUNuQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ25FLE9BQU87U0FDVjtRQUNELElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQyxNQUFNLElBQUksS0FBSyxDQUFDLDJDQUEyQyxDQUFDLENBQUM7U0FDaEU7SUFDTCxDQUFDOzs7O0lBRUQsbUNBQU07OztJQUFOOztZQUNVLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsTUFBTSxHQUFHLEdBQUcsQ0FBQyxDQUFDLE9BQU8sQ0FBQyxDQUFDLENBQUMsQ0FBQztRQUMvQyxJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsS0FBSyxDQUFDO0lBQ3RDLENBQUM7Ozs7SUFFRCxvQ0FBTzs7O0lBQVA7O1lBQ1EsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQyxNQUFNLEdBQUcsR0FBRyxDQUFDLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDO1FBQzdDLElBQUksS0FBSyxHQUFHLEdBQUcsRUFBRTtZQUNiLEtBQUssR0FBRyxHQUFHLENBQUM7U0FDZjtRQUNELElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxLQUFLLENBQUM7SUFDdEMsQ0FBQzs7OztJQUVELHVDQUFVOzs7SUFBVjs7WUFDVSxLQUFLLEdBQUcsSUFBSSxDQUFDLE1BQU0sR0FBRyxFQUFFO1FBQzlCLElBQUksQ0FBQyxNQUFNLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxLQUFLLENBQUMsR0FBRyxHQUFHLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDO0lBQ3BELENBQUM7Ozs7SUFFRCx3Q0FBVzs7O0lBQVg7O1lBQ1UsS0FBSyxHQUFHLElBQUksQ0FBQyxNQUFNLEdBQUcsRUFBRTtRQUM5QixJQUFJLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLEdBQUcsR0FBRyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNwRCxDQUFDOzs7O0lBRUQsa0NBQUs7OztJQUFMO1FBQ0ksSUFBSSxDQUFDLE1BQU0sR0FBRyxDQUFDLENBQUM7UUFDaEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7UUFDbEIsSUFBSSxDQUFDLE1BQU0sR0FBRyxHQUFHLENBQUM7UUFDbEIsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7UUFDakIsSUFBSSxDQUFDLE9BQU8sR0FBRyxDQUFDLENBQUM7SUFDckIsQ0FBQzs7Z0JBcEtKLFNBQVMsU0FBQztvQkFDUCxRQUFRLEVBQUUsZ0JBQWdCO29CQUMxQixtM0VBQXlDO29CQUV6QyxJQUFJLEVBQUUsRUFBRSxPQUFPLEVBQUUsa0JBQWtCLEVBQUU7b0JBQ3JDLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOztpQkFDeEM7Ozs7Z0JBUlEsWUFBWTtnQkFEWixnQkFBZ0I7Z0JBRGhCLGNBQWM7Z0JBSm5CLFVBQVU7Ozs4QkFpQlQsS0FBSzswQkFHTCxLQUFLOzJCQUdMLEtBQUs7MkJBR0wsS0FBSzs7SUFtSlYseUJBQUM7Q0FBQSxBQXJLRCxJQXFLQztTQTlKWSxrQkFBa0I7OztJQUUzQix5Q0FDbUI7O0lBRW5CLHFDQUNnQjs7SUFFaEIsc0NBQ2U7O0lBRWYsc0NBQ2lCOztJQUVqQixvQ0FBbUI7O0lBQ25CLG9DQUFxQjs7SUFDckIsb0NBQXFCOztJQUNyQixxQ0FBb0I7O0lBQ3BCLHFDQUFvQjs7SUFDcEIsdUNBQTJCOzs7OztJQUUzQixrQ0FBOEI7Ozs7O0lBQzlCLG1DQUErQjs7Ozs7SUFVL0IscUNBQTZCOzs7OztJQUd6Qix1Q0FBK0I7Ozs7O0lBQy9CLDhDQUEwQzs7Ozs7SUFDMUMsNENBQXNDOzs7OztJQUN0QyxnQ0FBc0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHtcclxuICAgIENvbXBvbmVudCxcclxuICAgIElucHV0LFxyXG4gICAgT25DaGFuZ2VzLFxyXG4gICAgU2ltcGxlQ2hhbmdlcyxcclxuICAgIFZpZXdFbmNhcHN1bGF0aW9uLFxyXG4gICAgRWxlbWVudFJlZixcclxuICAgIE9uSW5pdCxcclxuICAgIE9uRGVzdHJveVxyXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBDb250ZW50U2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2NvbnRlbnQuc2VydmljZSc7XHJcbmltcG9ydCB7IEFwcENvbmZpZ1NlcnZpY2UgfSBmcm9tICcuLy4uLy4uL2FwcC1jb25maWcvYXBwLWNvbmZpZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgRG9tU2FuaXRpemVyLCBTYWZlU3R5bGUgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtaW1nLXZpZXdlcicsXHJcbiAgICB0ZW1wbGF0ZVVybDogJy4vaW1nVmlld2VyLmNvbXBvbmVudC5odG1sJyxcclxuICAgIHN0eWxlVXJsczogWycuL2ltZ1ZpZXdlci5jb21wb25lbnQuc2NzcyddLFxyXG4gICAgaG9zdDogeyAnY2xhc3MnOiAnYWRmLWltYWdlLXZpZXdlcicgfSxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIEltZ1ZpZXdlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgT25DaGFuZ2VzLCBPbkRlc3Ryb3kge1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBzaG93VG9vbGJhciA9IHRydWU7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHVybEZpbGU6IHN0cmluZztcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgYmxvYkZpbGU6IEJsb2I7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIG5hbWVGaWxlOiBzdHJpbmc7XHJcblxyXG4gICAgcm90YXRlOiBudW1iZXIgPSAwO1xyXG4gICAgc2NhbGVYOiBudW1iZXIgPSAxLjA7XHJcbiAgICBzY2FsZVk6IG51bWJlciA9IDEuMDtcclxuICAgIG9mZnNldFg6IG51bWJlciA9IDA7XHJcbiAgICBvZmZzZXRZOiBudW1iZXIgPSAwO1xyXG4gICAgaXNEcmFnZ2VkOiBib29sZWFuID0gZmFsc2U7XHJcblxyXG4gICAgcHJpdmF0ZSBkcmFnID0geyB4OiAwLCB5OiAwIH07XHJcbiAgICBwcml2YXRlIGRlbHRhID0geyB4OiAwLCB5OiAwIH07XHJcblxyXG4gICAgZ2V0IHRyYW5zZm9ybSgpOiBTYWZlU3R5bGUge1xyXG4gICAgICAgIHJldHVybiB0aGlzLnNhbml0aXplci5ieXBhc3NTZWN1cml0eVRydXN0U3R5bGUoYHNjYWxlKCR7dGhpcy5zY2FsZVh9LCAke3RoaXMuc2NhbGVZfSkgcm90YXRlKCR7dGhpcy5yb3RhdGV9ZGVnKSB0cmFuc2xhdGUoJHt0aGlzLm9mZnNldFh9cHgsICR7dGhpcy5vZmZzZXRZfXB4KWApO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBjdXJyZW50U2NhbGVUZXh0KCk6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIE1hdGgucm91bmQodGhpcy5zY2FsZVggKiAxMDApICsgJyUnO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZWxlbWVudDogSFRNTEVsZW1lbnQ7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBzYW5pdGl6ZXI6IERvbVNhbml0aXplcixcclxuICAgICAgICBwcml2YXRlIGFwcENvbmZpZ1NlcnZpY2U6IEFwcENvbmZpZ1NlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBjb250ZW50U2VydmljZTogQ29udGVudFNlcnZpY2UsXHJcbiAgICAgICAgcHJpdmF0ZSBlbDogRWxlbWVudFJlZikge1xyXG4gICAgICAgIHRoaXMuaW5pdGlhbGl6ZVNjYWxpbmcoKTtcclxuICAgIH1cclxuXHJcbiAgICBpbml0aWFsaXplU2NhbGluZygpIHtcclxuICAgICAgICBjb25zdCBzY2FsaW5nID0gdGhpcy5hcHBDb25maWdTZXJ2aWNlLmdldDxudW1iZXI+KCdhZGYtdmlld2VyLmltYWdlLXZpZXdlci1zY2FsaW5nJywgdW5kZWZpbmVkKSAvIDEwMDtcclxuICAgICAgICBpZiAoc2NhbGluZykge1xyXG4gICAgICAgICAgICB0aGlzLnNjYWxlWCA9IHNjYWxpbmc7XHJcbiAgICAgICAgICAgIHRoaXMuc2NhbGVZID0gc2NhbGluZztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5lbGVtZW50ID0gPEhUTUxFbGVtZW50PiB0aGlzLmVsLm5hdGl2ZUVsZW1lbnQucXVlcnlTZWxlY3RvcignI3ZpZXdlci1pbWFnZScpO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5lbGVtZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZWRvd24nLCB0aGlzLm9uTW91c2VEb3duLmJpbmQodGhpcykpO1xyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignbW91c2V1cCcsIHRoaXMub25Nb3VzZVVwLmJpbmQodGhpcykpO1xyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignbW91c2VsZWF2ZScsIHRoaXMub25Nb3VzZUxlYXZlLmJpbmQodGhpcykpO1xyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQuYWRkRXZlbnRMaXN0ZW5lcignbW91c2VvdXQnLCB0aGlzLm9uTW91c2VPdXQuYmluZCh0aGlzKSk7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5hZGRFdmVudExpc3RlbmVyKCdtb3VzZW1vdmUnLCB0aGlzLm9uTW91c2VNb3ZlLmJpbmQodGhpcykpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBuZ09uRGVzdHJveSgpIHtcclxuICAgICAgICBpZiAodGhpcy5lbGVtZW50KSB7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdtb3VzZWRvd24nLCB0aGlzLm9uTW91c2VEb3duKTtcclxuICAgICAgICAgICAgdGhpcy5lbGVtZW50LnJlbW92ZUV2ZW50TGlzdGVuZXIoJ21vdXNldXAnLCB0aGlzLm9uTW91c2VVcCk7XHJcbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdtb3VzZWxlYXZlJywgdGhpcy5vbk1vdXNlTGVhdmUpO1xyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcignbW91c2VvdXQnLCB0aGlzLm9uTW91c2VPdXQpO1xyXG4gICAgICAgICAgICB0aGlzLmVsZW1lbnQucmVtb3ZlRXZlbnRMaXN0ZW5lcignbW91c2Vtb3ZlJywgdGhpcy5vbk1vdXNlTW92ZSk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uTW91c2VEb3duKGV2ZW50OiBNb3VzZUV2ZW50KSB7XHJcbiAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICB0aGlzLmlzRHJhZ2dlZCA9IHRydWU7XHJcbiAgICAgICAgdGhpcy5kcmFnID0geyB4OiBldmVudC5wYWdlWCwgeTogZXZlbnQucGFnZVkgfTtcclxuICAgIH1cclxuXHJcbiAgICBvbk1vdXNlTW92ZShldmVudDogTW91c2VFdmVudCkge1xyXG4gICAgICAgIGlmICh0aGlzLmlzRHJhZ2dlZCkge1xyXG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5kZWx0YS54ID0gZXZlbnQucGFnZVggLSB0aGlzLmRyYWcueDtcclxuICAgICAgICAgICAgdGhpcy5kZWx0YS55ID0gZXZlbnQucGFnZVkgLSB0aGlzLmRyYWcueTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMuZHJhZy54ID0gZXZlbnQucGFnZVg7XHJcbiAgICAgICAgICAgIHRoaXMuZHJhZy55ID0gZXZlbnQucGFnZVk7XHJcblxyXG4gICAgICAgICAgICBjb25zdCBzY2FsZVggPSAodGhpcy5zY2FsZVggIT09IDAgPyB0aGlzLnNjYWxlWCA6IDEuMCk7XHJcbiAgICAgICAgICAgIGNvbnN0IHNjYWxlWSA9ICh0aGlzLnNjYWxlWSAhPT0gMCA/IHRoaXMuc2NhbGVZIDogMS4wKTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMub2Zmc2V0WCArPSAodGhpcy5kZWx0YS54IC8gc2NhbGVYKTtcclxuICAgICAgICAgICAgdGhpcy5vZmZzZXRZICs9ICh0aGlzLmRlbHRhLnkgLyBzY2FsZVkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBvbk1vdXNlVXAoZXZlbnQ6IE1vdXNlRXZlbnQpIHtcclxuICAgICAgICBpZiAodGhpcy5pc0RyYWdnZWQpIHtcclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgdGhpcy5pc0RyYWdnZWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgb25Nb3VzZUxlYXZlKGV2ZW50OiBNb3VzZUV2ZW50KSB7XHJcbiAgICAgICAgaWYgKHRoaXMuaXNEcmFnZ2VkKSB7XHJcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XHJcbiAgICAgICAgICAgIHRoaXMuaXNEcmFnZ2VkID0gZmFsc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG9uTW91c2VPdXQoZXZlbnQ6IE1vdXNlRXZlbnQpIHtcclxuICAgICAgICBpZiAodGhpcy5pc0RyYWdnZWQpIHtcclxuICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICAgICAgdGhpcy5pc0RyYWdnZWQgPSBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgICAgIGNvbnN0IGJsb2JGaWxlID0gY2hhbmdlc1snYmxvYkZpbGUnXTtcclxuICAgICAgICBpZiAoYmxvYkZpbGUgJiYgYmxvYkZpbGUuY3VycmVudFZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMudXJsRmlsZSA9IHRoaXMuY29udGVudFNlcnZpY2UuY3JlYXRlVHJ1c3RlZFVybCh0aGlzLmJsb2JGaWxlKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoIXRoaXMudXJsRmlsZSAmJiAhdGhpcy5ibG9iRmlsZSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0F0dHJpYnV0ZSB1cmxGaWxlIG9yIGJsb2JGaWxlIGlzIHJlcXVpcmVkJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHpvb21JbigpIHtcclxuICAgICAgICBjb25zdCByYXRpbyA9ICsoKHRoaXMuc2NhbGVYICsgMC4yKS50b0ZpeGVkKDEpKTtcclxuICAgICAgICB0aGlzLnNjYWxlWCA9IHRoaXMuc2NhbGVZID0gcmF0aW87XHJcbiAgICB9XHJcblxyXG4gICAgem9vbU91dCgpIHtcclxuICAgICAgICBsZXQgcmF0aW8gPSArKCh0aGlzLnNjYWxlWCAtIDAuMikudG9GaXhlZCgxKSk7XHJcbiAgICAgICAgaWYgKHJhdGlvIDwgMC4yKSB7XHJcbiAgICAgICAgICAgIHJhdGlvID0gMC4yO1xyXG4gICAgICAgIH1cclxuICAgICAgICB0aGlzLnNjYWxlWCA9IHRoaXMuc2NhbGVZID0gcmF0aW87XHJcbiAgICB9XHJcblxyXG4gICAgcm90YXRlTGVmdCgpIHtcclxuICAgICAgICBjb25zdCBhbmdsZSA9IHRoaXMucm90YXRlIC0gOTA7XHJcbiAgICAgICAgdGhpcy5yb3RhdGUgPSBNYXRoLmFicyhhbmdsZSkgPCAzNjAgPyBhbmdsZSA6IDA7XHJcbiAgICB9XHJcblxyXG4gICAgcm90YXRlUmlnaHQoKSB7XHJcbiAgICAgICAgY29uc3QgYW5nbGUgPSB0aGlzLnJvdGF0ZSArIDkwO1xyXG4gICAgICAgIHRoaXMucm90YXRlID0gTWF0aC5hYnMoYW5nbGUpIDwgMzYwID8gYW5nbGUgOiAwO1xyXG4gICAgfVxyXG5cclxuICAgIHJlc2V0KCkge1xyXG4gICAgICAgIHRoaXMucm90YXRlID0gMDtcclxuICAgICAgICB0aGlzLnNjYWxlWCA9IDEuMDtcclxuICAgICAgICB0aGlzLnNjYWxlWSA9IDEuMDtcclxuICAgICAgICB0aGlzLm9mZnNldFggPSAwO1xyXG4gICAgICAgIHRoaXMub2Zmc2V0WSA9IDA7XHJcbiAgICB9XHJcbn1cclxuIl19