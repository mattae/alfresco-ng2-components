/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { ContentService } from '../../services/content.service';
var MediaPlayerComponent = /** @class */ (function () {
    function MediaPlayerComponent(contentService) {
        this.contentService = contentService;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    MediaPlayerComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        /** @type {?} */
        var blobFile = changes['blobFile'];
        if (blobFile && blobFile.currentValue) {
            this.urlFile = this.contentService.createTrustedUrl(this.blobFile);
            return;
        }
        if (!this.urlFile && !this.blobFile) {
            throw new Error('Attribute urlFile or blobFile is required');
        }
    };
    MediaPlayerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-media-player',
                    template: "<video controls>\r\n    <source [src]=\"urlFile\" [type]=\"mimeType\" />\r\n</video>\r\n",
                    host: { 'class': 'adf-media-player' },
                    encapsulation: ViewEncapsulation.None,
                    styles: [".adf-media-player{display:flex}.adf-media-player video{display:flex;flex:1;max-height:90vh;max-width:100%}"]
                }] }
    ];
    /** @nocollapse */
    MediaPlayerComponent.ctorParameters = function () { return [
        { type: ContentService }
    ]; };
    MediaPlayerComponent.propDecorators = {
        urlFile: [{ type: Input }],
        blobFile: [{ type: Input }],
        mimeType: [{ type: Input }],
        nameFile: [{ type: Input }]
    };
    return MediaPlayerComponent;
}());
export { MediaPlayerComponent };
if (false) {
    /** @type {?} */
    MediaPlayerComponent.prototype.urlFile;
    /** @type {?} */
    MediaPlayerComponent.prototype.blobFile;
    /** @type {?} */
    MediaPlayerComponent.prototype.mimeType;
    /** @type {?} */
    MediaPlayerComponent.prototype.nameFile;
    /**
     * @type {?}
     * @private
     */
    MediaPlayerComponent.prototype.contentService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibWVkaWFQbGF5ZXIuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsidmlld2VyL2NvbXBvbmVudHMvbWVkaWFQbGF5ZXIuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUE0QixpQkFBaUIsRUFBRSxNQUFNLGVBQWUsQ0FBQztBQUM5RixPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sZ0NBQWdDLENBQUM7QUFFaEU7SUFxQkksOEJBQW9CLGNBQThCO1FBQTlCLG1CQUFjLEdBQWQsY0FBYyxDQUFnQjtJQUFJLENBQUM7Ozs7O0lBRXZELDBDQUFXOzs7O0lBQVgsVUFBWSxPQUFzQjs7WUFDeEIsUUFBUSxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUM7UUFDcEMsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFlBQVksRUFBRTtZQUNuQyxJQUFJLENBQUMsT0FBTyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ25FLE9BQU87U0FDVjtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQyxNQUFNLElBQUksS0FBSyxDQUFDLDJDQUEyQyxDQUFDLENBQUM7U0FDaEU7SUFDTCxDQUFDOztnQkFqQ0osU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxrQkFBa0I7b0JBQzVCLG9HQUEyQztvQkFFM0MsSUFBSSxFQUFFLEVBQUUsT0FBTyxFQUFFLGtCQUFrQixFQUFFO29CQUNyQyxhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7aUJBQ3hDOzs7O2dCQVJRLGNBQWM7OzswQkFXbEIsS0FBSzsyQkFHTCxLQUFLOzJCQUdMLEtBQUs7MkJBR0wsS0FBSzs7SUFnQlYsMkJBQUM7Q0FBQSxBQWxDRCxJQWtDQztTQTNCWSxvQkFBb0I7OztJQUU3Qix1Q0FDZ0I7O0lBRWhCLHdDQUNlOztJQUVmLHdDQUNpQjs7SUFFakIsd0NBQ2lCOzs7OztJQUVMLDhDQUFzQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkNoYW5nZXMsIFNpbXBsZUNoYW5nZXMsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IENvbnRlbnRTZXJ2aWNlIH0gZnJvbSAnLi4vLi4vc2VydmljZXMvY29udGVudC5zZXJ2aWNlJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtbWVkaWEtcGxheWVyJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9tZWRpYVBsYXllci5jb21wb25lbnQuaHRtbCcsXHJcbiAgICBzdHlsZVVybHM6IFsnLi9tZWRpYVBsYXllci5jb21wb25lbnQuc2NzcyddLFxyXG4gICAgaG9zdDogeyAnY2xhc3MnOiAnYWRmLW1lZGlhLXBsYXllcicgfSxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIE1lZGlhUGxheWVyQ29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzIHtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgdXJsRmlsZTogc3RyaW5nO1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBibG9iRmlsZTogQmxvYjtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgbWltZVR5cGU6IHN0cmluZztcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgbmFtZUZpbGU6IHN0cmluZztcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGNvbnRlbnRTZXJ2aWNlOiBDb250ZW50U2VydmljZSApIHt9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgICAgIGNvbnN0IGJsb2JGaWxlID0gY2hhbmdlc1snYmxvYkZpbGUnXTtcclxuICAgICAgICBpZiAoYmxvYkZpbGUgJiYgYmxvYkZpbGUuY3VycmVudFZhbHVlKSB7XHJcbiAgICAgICAgICAgIHRoaXMudXJsRmlsZSA9IHRoaXMuY29udGVudFNlcnZpY2UuY3JlYXRlVHJ1c3RlZFVybCh0aGlzLmJsb2JGaWxlKTtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCF0aGlzLnVybEZpbGUgJiYgIXRoaXMuYmxvYkZpbGUpIHtcclxuICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdBdHRyaWJ1dGUgdXJsRmlsZSBvciBibG9iRmlsZSBpcyByZXF1aXJlZCcpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufVxyXG4iXX0=