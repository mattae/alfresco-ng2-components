/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormControl, Validators } from '@angular/forms';
var PdfPasswordDialogComponent = /** @class */ (function () {
    function PdfPasswordDialogComponent(dialogRef, data) {
        this.dialogRef = dialogRef;
        this.data = data;
    }
    /**
     * @return {?}
     */
    PdfPasswordDialogComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.passwordFormControl = new FormControl('', [Validators.required]);
    };
    /**
     * @return {?}
     */
    PdfPasswordDialogComponent.prototype.isError = /**
     * @return {?}
     */
    function () {
        return this.data.reason === pdfjsLib.PasswordResponses.INCORRECT_PASSWORD;
    };
    /**
     * @return {?}
     */
    PdfPasswordDialogComponent.prototype.isValid = /**
     * @return {?}
     */
    function () {
        return !this.passwordFormControl.hasError('required');
    };
    /**
     * @return {?}
     */
    PdfPasswordDialogComponent.prototype.submit = /**
     * @return {?}
     */
    function () {
        this.dialogRef.close(this.passwordFormControl.value);
    };
    PdfPasswordDialogComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-pdf-viewer-password-dialog',
                    template: "<div mat-dialog-title>\r\n    <mat-icon>lock</mat-icon>\r\n</div>\r\n\r\n<mat-dialog-content>\r\n    <form (submit)=\"submit()\">\r\n        <mat-form-field class=\"adf-full-width\">\r\n            <input matInput\r\n                   data-automation-id='adf-password-dialog-input'\r\n                   type=\"password\"\r\n                   placeholder=\"{{ 'ADF_VIEWER.PDF_DIALOG.PLACEHOLDER' | translate }}\"\r\n                   [formControl]=\"passwordFormControl\" />\r\n        </mat-form-field>\r\n\r\n        <mat-error *ngIf=\"isError()\" data-automation-id='adf-password-dialog-error'>{{ 'ADF_VIEWER.PDF_DIALOG.ERROR' | translate }}</mat-error>\r\n    </form>\r\n</mat-dialog-content>\r\n\r\n<mat-dialog-actions class=\"adf-dialog-buttons\">\r\n    <span class=\"adf-fill-remaining-space\"></span>\r\n\r\n    <button mat-button mat-dialog-close data-automation-id='adf-password-dialog-close'>{{ 'ADF_VIEWER.PDF_DIALOG.CLOSE' | translate }}</button>\r\n\r\n    <button mat-button\r\n            data-automation-id='adf-password-dialog-submit'\r\n            class=\"adf-dialog-action-button\"\r\n            [disabled]=\"!isValid()\"\r\n            (click)=\"submit()\">\r\n        {{ 'ADF_VIEWER.PDF_DIALOG.SUBMIT' | translate }}\r\n    </button>\r\n</mat-dialog-actions>\r\n",
                    styles: [".adf-fill-remaining-space{flex:1 1 auto}.adf-full-width{width:100%}"]
                }] }
    ];
    /** @nocollapse */
    PdfPasswordDialogComponent.ctorParameters = function () { return [
        { type: MatDialogRef },
        { type: undefined, decorators: [{ type: Inject, args: [MAT_DIALOG_DATA,] }] }
    ]; };
    return PdfPasswordDialogComponent;
}());
export { PdfPasswordDialogComponent };
if (false) {
    /** @type {?} */
    PdfPasswordDialogComponent.prototype.passwordFormControl;
    /**
     * @type {?}
     * @private
     */
    PdfPasswordDialogComponent.prototype.dialogRef;
    /** @type {?} */
    PdfPasswordDialogComponent.prototype.data;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGRmVmlld2VyLXBhc3N3b3JkLWRpYWxvZy5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInZpZXdlci9jb21wb25lbnRzL3BkZlZpZXdlci1wYXNzd29yZC1kaWFsb2cudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLEVBQVUsTUFBTSxlQUFlLENBQUM7QUFDMUQsT0FBTyxFQUFFLFlBQVksRUFBRSxlQUFlLEVBQUUsTUFBTSxtQkFBbUIsQ0FBQztBQUNsRSxPQUFPLEVBQUUsV0FBVyxFQUFFLFVBQVUsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBSXpEO0lBUUksb0NBQ1ksU0FBbUQsRUFDM0IsSUFBUztRQURqQyxjQUFTLEdBQVQsU0FBUyxDQUEwQztRQUMzQixTQUFJLEdBQUosSUFBSSxDQUFLO0lBQzFDLENBQUM7Ozs7SUFFSiw2Q0FBUTs7O0lBQVI7UUFDSSxJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxXQUFXLENBQUMsRUFBRSxFQUFFLENBQUMsVUFBVSxDQUFDLFFBQVEsQ0FBQyxDQUFDLENBQUM7SUFDMUUsQ0FBQzs7OztJQUVELDRDQUFPOzs7SUFBUDtRQUNJLE9BQU8sSUFBSSxDQUFDLElBQUksQ0FBQyxNQUFNLEtBQUssUUFBUSxDQUFDLGlCQUFpQixDQUFDLGtCQUFrQixDQUFDO0lBQzlFLENBQUM7Ozs7SUFFRCw0Q0FBTzs7O0lBQVA7UUFDSSxPQUFPLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxVQUFVLENBQUMsQ0FBQztJQUMxRCxDQUFDOzs7O0lBRUQsMkNBQU07OztJQUFOO1FBQ0ksSUFBSSxDQUFDLFNBQVMsQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLG1CQUFtQixDQUFDLEtBQUssQ0FBQyxDQUFDO0lBQ3pELENBQUM7O2dCQTNCSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGdDQUFnQztvQkFDMUMsc3hDQUErQzs7aUJBRWxEOzs7O2dCQVRRLFlBQVk7Z0RBZVosTUFBTSxTQUFDLGVBQWU7O0lBa0IvQixpQ0FBQztDQUFBLEFBNUJELElBNEJDO1NBdkJZLDBCQUEwQjs7O0lBQ25DLHlEQUFpQzs7Ozs7SUFHN0IsK0NBQTJEOztJQUMzRCwwQ0FBeUMiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbmplY3QsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBNYXREaWFsb2dSZWYsIE1BVF9ESUFMT0dfREFUQSB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgRm9ybUNvbnRyb2wsIFZhbGlkYXRvcnMgfSBmcm9tICdAYW5ndWxhci9mb3Jtcyc7XHJcblxyXG5kZWNsYXJlIGNvbnN0IHBkZmpzTGliOiBhbnk7XHJcblxyXG5AQ29tcG9uZW50KHtcclxuICAgIHNlbGVjdG9yOiAnYWRmLXBkZi12aWV3ZXItcGFzc3dvcmQtZGlhbG9nJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9wZGZWaWV3ZXItcGFzc3dvcmQtZGlhbG9nLmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbICcuL3BkZlZpZXdlci1wYXNzd29yZC1kaWFsb2cuc2NzcycgXVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUGRmUGFzc3dvcmREaWFsb2dDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG4gICAgcGFzc3dvcmRGb3JtQ29udHJvbDogRm9ybUNvbnRyb2w7XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBkaWFsb2dSZWY6IE1hdERpYWxvZ1JlZjxQZGZQYXNzd29yZERpYWxvZ0NvbXBvbmVudD4sXHJcbiAgICAgICAgQEluamVjdChNQVRfRElBTE9HX0RBVEEpIHB1YmxpYyBkYXRhOiBhbnlcclxuICAgICkge31cclxuXHJcbiAgICBuZ09uSW5pdCgpIHtcclxuICAgICAgICB0aGlzLnBhc3N3b3JkRm9ybUNvbnRyb2wgPSBuZXcgRm9ybUNvbnRyb2woJycsIFtWYWxpZGF0b3JzLnJlcXVpcmVkXSk7XHJcbiAgICB9XHJcblxyXG4gICAgaXNFcnJvcigpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5kYXRhLnJlYXNvbiA9PT0gcGRmanNMaWIuUGFzc3dvcmRSZXNwb25zZXMuSU5DT1JSRUNUX1BBU1NXT1JEO1xyXG4gICAgfVxyXG5cclxuICAgIGlzVmFsaWQoKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuICF0aGlzLnBhc3N3b3JkRm9ybUNvbnRyb2wuaGFzRXJyb3IoJ3JlcXVpcmVkJyk7XHJcbiAgICB9XHJcblxyXG4gICAgc3VibWl0KCk6IHZvaWQge1xyXG4gICAgICAgIHRoaXMuZGlhbG9nUmVmLmNsb3NlKHRoaXMucGFzc3dvcmRGb3JtQ29udHJvbC52YWx1ZSk7XHJcbiAgICB9XHJcbn1cclxuIl19