/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
var PdfThumbComponent = /** @class */ (function () {
    function PdfThumbComponent(sanitizer) {
        this.sanitizer = sanitizer;
        this.page = null;
    }
    /**
     * @return {?}
     */
    PdfThumbComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.image$ = this.page.getPage().then((/**
         * @param {?} page
         * @return {?}
         */
        function (page) { return _this.getThumb(page); }));
    };
    /**
     * @private
     * @param {?} page
     * @return {?}
     */
    PdfThumbComponent.prototype.getThumb = /**
     * @private
     * @param {?} page
     * @return {?}
     */
    function (page) {
        var _this = this;
        /** @type {?} */
        var viewport = page.getViewport(1);
        /** @type {?} */
        var pageRatio = viewport.width / viewport.height;
        /** @type {?} */
        var canvas = this.getCanvas(pageRatio);
        /** @type {?} */
        var scale = Math.min((canvas.height / viewport.height), (canvas.width / viewport.width));
        return page.render({
            canvasContext: canvas.getContext('2d'),
            viewport: page.getViewport(scale)
        })
            .then((/**
         * @return {?}
         */
        function () {
            /** @type {?} */
            var imageSource = canvas.toDataURL();
            return _this.sanitizer.bypassSecurityTrustUrl(imageSource);
        }));
    };
    /**
     * @private
     * @param {?} pageRatio
     * @return {?}
     */
    PdfThumbComponent.prototype.getCanvas = /**
     * @private
     * @param {?} pageRatio
     * @return {?}
     */
    function (pageRatio) {
        /** @type {?} */
        var canvas = document.createElement('canvas');
        canvas.width = this.page.getWidth();
        canvas.height = this.page.getHeight();
        return canvas;
    };
    PdfThumbComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-pdf-thumb',
                    template: "<ng-container *ngIf=\"image$ | async as image\">\r\n    <img [src]=\"image\" [alt]=\"'ADF_VIEWER.SIDEBAR.THUMBNAILS.PAGE' | translate: { pageNum: page.id }\"\r\n        title=\"{{ 'ADF_VIEWER.SIDEBAR.THUMBNAILS.PAGE' | translate: { pageNum: page.id } }}\">\r\n</ng-container>\r\n",
                    encapsulation: ViewEncapsulation.None
                }] }
    ];
    /** @nocollapse */
    PdfThumbComponent.ctorParameters = function () { return [
        { type: DomSanitizer }
    ]; };
    PdfThumbComponent.propDecorators = {
        page: [{ type: Input }]
    };
    return PdfThumbComponent;
}());
export { PdfThumbComponent };
if (false) {
    /** @type {?} */
    PdfThumbComponent.prototype.page;
    /** @type {?} */
    PdfThumbComponent.prototype.image$;
    /**
     * @type {?}
     * @private
     */
    PdfThumbComponent.prototype.sanitizer;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGRmVmlld2VyLXRodW1iLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInZpZXdlci9jb21wb25lbnRzL3BkZlZpZXdlci10aHVtYi5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQVUsaUJBQWlCLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDNUUsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLDJCQUEyQixDQUFDO0FBRXpEO0lBWUksMkJBQW9CLFNBQXVCO1FBQXZCLGNBQVMsR0FBVCxTQUFTLENBQWM7UUFKM0MsU0FBSSxHQUFRLElBQUksQ0FBQztJQUk2QixDQUFDOzs7O0lBRS9DLG9DQUFROzs7SUFBUjtRQUFBLGlCQUVDO1FBREcsSUFBSSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLE9BQU8sRUFBRSxDQUFDLElBQUk7Ozs7UUFBQyxVQUFDLElBQUksSUFBSyxPQUFBLEtBQUksQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQW5CLENBQW1CLEVBQUMsQ0FBQztJQUMxRSxDQUFDOzs7Ozs7SUFFTyxvQ0FBUTs7Ozs7SUFBaEIsVUFBaUIsSUFBSTtRQUFyQixpQkFlQzs7WUFkUyxRQUFRLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxDQUFDLENBQUM7O1lBRTlCLFNBQVMsR0FBRyxRQUFRLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxNQUFNOztZQUM1QyxNQUFNLEdBQUcsSUFBSSxDQUFDLFNBQVMsQ0FBQyxTQUFTLENBQUM7O1lBQ2xDLEtBQUssR0FBRyxJQUFJLENBQUMsR0FBRyxDQUFDLENBQUMsTUFBTSxDQUFDLE1BQU0sR0FBRyxRQUFRLENBQUMsTUFBTSxDQUFDLEVBQUUsQ0FBQyxNQUFNLENBQUMsS0FBSyxHQUFHLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztRQUUxRixPQUFPLElBQUksQ0FBQyxNQUFNLENBQUM7WUFDZixhQUFhLEVBQUUsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUM7WUFDdEMsUUFBUSxFQUFFLElBQUksQ0FBQyxXQUFXLENBQUMsS0FBSyxDQUFDO1NBQ3BDLENBQUM7YUFDRCxJQUFJOzs7UUFBQzs7Z0JBQ0ksV0FBVyxHQUFHLE1BQU0sQ0FBQyxTQUFTLEVBQUU7WUFDdEMsT0FBTyxLQUFJLENBQUMsU0FBUyxDQUFDLHNCQUFzQixDQUFDLFdBQVcsQ0FBQyxDQUFDO1FBQzlELENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7O0lBRU8scUNBQVM7Ozs7O0lBQWpCLFVBQWtCLFNBQVM7O1lBQ2pCLE1BQU0sR0FBRyxRQUFRLENBQUMsYUFBYSxDQUFDLFFBQVEsQ0FBQztRQUMvQyxNQUFNLENBQUMsS0FBSyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDcEMsTUFBTSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO1FBQ3RDLE9BQU8sTUFBTSxDQUFDO0lBQ2xCLENBQUM7O2dCQXhDSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGVBQWU7b0JBQ3pCLG1TQUErQztvQkFDL0MsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7aUJBQ3hDOzs7O2dCQU5RLFlBQVk7Ozt1QkFTaEIsS0FBSzs7SUFrQ1Ysd0JBQUM7Q0FBQSxBQXpDRCxJQXlDQztTQXBDWSxpQkFBaUI7OztJQUUxQixpQ0FDaUI7O0lBRWpCLG1DQUF3Qjs7Ozs7SUFFWixzQ0FBK0IiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgQ29tcG9uZW50LCBJbnB1dCwgT25Jbml0LCBWaWV3RW5jYXBzdWxhdGlvbiB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBEb21TYW5pdGl6ZXIgfSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyJztcclxuXHJcbkBDb21wb25lbnQoe1xyXG4gICAgc2VsZWN0b3I6ICdhZGYtcGRmLXRodW1iJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9wZGZWaWV3ZXItdGh1bWIuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgZW5jYXBzdWxhdGlvbjogVmlld0VuY2Fwc3VsYXRpb24uTm9uZVxyXG59KVxyXG5leHBvcnQgY2xhc3MgUGRmVGh1bWJDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xyXG5cclxuICAgIEBJbnB1dCgpXHJcbiAgICBwYWdlOiBhbnkgPSBudWxsO1xyXG5cclxuICAgIGltYWdlJDogUHJvbWlzZTxzdHJpbmc+O1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgc2FuaXRpemVyOiBEb21TYW5pdGl6ZXIpIHt9XHJcblxyXG4gICAgbmdPbkluaXQoKSB7XHJcbiAgICAgICAgdGhpcy5pbWFnZSQgPSB0aGlzLnBhZ2UuZ2V0UGFnZSgpLnRoZW4oKHBhZ2UpID0+IHRoaXMuZ2V0VGh1bWIocGFnZSkpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0VGh1bWIocGFnZSk6IFByb21pc2U8c3RyaW5nPiB7XHJcbiAgICAgICAgY29uc3Qgdmlld3BvcnQgPSBwYWdlLmdldFZpZXdwb3J0KDEpO1xyXG5cclxuICAgICAgICBjb25zdCBwYWdlUmF0aW8gPSB2aWV3cG9ydC53aWR0aCAvIHZpZXdwb3J0LmhlaWdodDtcclxuICAgICAgICBjb25zdCBjYW52YXMgPSB0aGlzLmdldENhbnZhcyhwYWdlUmF0aW8pO1xyXG4gICAgICAgIGNvbnN0IHNjYWxlID0gTWF0aC5taW4oKGNhbnZhcy5oZWlnaHQgLyB2aWV3cG9ydC5oZWlnaHQpLCAoY2FudmFzLndpZHRoIC8gdmlld3BvcnQud2lkdGgpKTtcclxuXHJcbiAgICAgICAgcmV0dXJuIHBhZ2UucmVuZGVyKHtcclxuICAgICAgICAgICAgY2FudmFzQ29udGV4dDogY2FudmFzLmdldENvbnRleHQoJzJkJyksXHJcbiAgICAgICAgICAgIHZpZXdwb3J0OiBwYWdlLmdldFZpZXdwb3J0KHNjYWxlKVxyXG4gICAgICAgIH0pXHJcbiAgICAgICAgLnRoZW4oKCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCBpbWFnZVNvdXJjZSA9IGNhbnZhcy50b0RhdGFVUkwoKTtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuc2FuaXRpemVyLmJ5cGFzc1NlY3VyaXR5VHJ1c3RVcmwoaW1hZ2VTb3VyY2UpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0Q2FudmFzKHBhZ2VSYXRpbyk6IEhUTUxDYW52YXNFbGVtZW50IHtcclxuICAgICAgICBjb25zdCBjYW52YXMgPSBkb2N1bWVudC5jcmVhdGVFbGVtZW50KCdjYW52YXMnKTtcclxuICAgICAgICBjYW52YXMud2lkdGggPSB0aGlzLnBhZ2UuZ2V0V2lkdGgoKTtcclxuICAgICAgICBjYW52YXMuaGVpZ2h0ID0gdGhpcy5wYWdlLmdldEhlaWdodCgpO1xyXG4gICAgICAgIHJldHVybiBjYW52YXM7XHJcbiAgICB9XHJcbn1cclxuIl19