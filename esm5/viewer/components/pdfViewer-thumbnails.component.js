/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ContentChild, ElementRef, HostListener, Input, TemplateRef, ViewEncapsulation } from '@angular/core';
var PdfThumbListComponent = /** @class */ (function () {
    function PdfThumbListComponent(element) {
        this.element = element;
        this.virtualHeight = 0;
        this.translateY = 0;
        this.renderItems = [];
        this.width = 91;
        this.currentHeight = 0;
        this.items = [];
        this.margin = 15;
        this.itemHeight = 114 + this.margin;
        this.calculateItems = this.calculateItems.bind(this);
        this.onPageChange = this.onPageChange.bind(this);
    }
    /**
     * @param {?} event
     * @return {?}
     */
    PdfThumbListComponent.prototype.onResize = /**
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.calculateItems();
    };
    /**
     * @return {?}
     */
    PdfThumbListComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        /* cspell:disable-next-line */
        this.pdfViewer.eventBus.on('pagechange', this.onPageChange);
        this.element.nativeElement.addEventListener('scroll', this.calculateItems, true);
        this.setHeight(this.pdfViewer.currentPageNumber);
        this.items = this.getPages();
        this.calculateItems();
    };
    /**
     * @return {?}
     */
    PdfThumbListComponent.prototype.ngAfterViewInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        setTimeout((/**
         * @return {?}
         */
        function () { return _this.scrollInto(_this.pdfViewer.currentPageNumber); }), 0);
    };
    /**
     * @return {?}
     */
    PdfThumbListComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.element.nativeElement.removeEventListener('scroll', this.calculateItems, true);
        /* cspell:disable-next-line */
        this.pdfViewer.eventBus.off('pagechange', this.onPageChange);
    };
    /**
     * @param {?} index
     * @param {?} item
     * @return {?}
     */
    PdfThumbListComponent.prototype.trackByFn = /**
     * @param {?} index
     * @param {?} item
     * @return {?}
     */
    function (index, item) {
        return item.id;
    };
    /**
     * @param {?} pageNum
     * @return {?}
     */
    PdfThumbListComponent.prototype.isSelected = /**
     * @param {?} pageNum
     * @return {?}
     */
    function (pageNum) {
        return this.pdfViewer.currentPageNumber === pageNum;
    };
    /**
     * @param {?} pageNum
     * @return {?}
     */
    PdfThumbListComponent.prototype.goTo = /**
     * @param {?} pageNum
     * @return {?}
     */
    function (pageNum) {
        this.pdfViewer.currentPageNumber = pageNum;
    };
    /**
     * @param {?} item
     * @return {?}
     */
    PdfThumbListComponent.prototype.scrollInto = /**
     * @param {?} item
     * @return {?}
     */
    function (item) {
        if (this.items.length) {
            /** @type {?} */
            var index = this.items.findIndex((/**
             * @param {?} element
             * @return {?}
             */
            function (element) { return element.id === item; }));
            if (index < 0 || index >= this.items.length) {
                return;
            }
            this.element.nativeElement.scrollTop = index * this.itemHeight;
            this.calculateItems();
        }
    };
    /**
     * @return {?}
     */
    PdfThumbListComponent.prototype.getPages = /**
     * @return {?}
     */
    function () {
        var _this = this;
        return this.pdfViewer._pages.map((/**
         * @param {?} page
         * @return {?}
         */
        function (page) { return ({
            id: page.id,
            getWidth: (/**
             * @return {?}
             */
            function () { return _this.width; }),
            getHeight: (/**
             * @return {?}
             */
            function () { return _this.currentHeight; }),
            getPage: (/**
             * @return {?}
             */
            function () { return _this.pdfViewer.pdfDocument.getPage(page.id); })
        }); }));
    };
    /**
     * @private
     * @param {?} id
     * @return {?}
     */
    PdfThumbListComponent.prototype.setHeight = /**
     * @private
     * @param {?} id
     * @return {?}
     */
    function (id) {
        var _this = this;
        /** @type {?} */
        var height = this.pdfViewer.pdfDocument.getPage(id).then((/**
         * @param {?} page
         * @return {?}
         */
        function (page) { return _this.calculateHeight(page); }));
        return height;
    };
    /**
     * @private
     * @param {?} page
     * @return {?}
     */
    PdfThumbListComponent.prototype.calculateHeight = /**
     * @private
     * @param {?} page
     * @return {?}
     */
    function (page) {
        /** @type {?} */
        var viewport = page.getViewport(1);
        /** @type {?} */
        var pageRatio = viewport.width / viewport.height;
        /** @type {?} */
        var height = Math.floor(this.width / pageRatio);
        this.currentHeight = height;
        this.itemHeight = height + this.margin;
    };
    /**
     * @private
     * @return {?}
     */
    PdfThumbListComponent.prototype.calculateItems = /**
     * @private
     * @return {?}
     */
    function () {
        var _a = this.getContainerSetup(), element = _a.element, viewPort = _a.viewPort, itemsInView = _a.itemsInView;
        /** @type {?} */
        var indexByScrollTop = element.scrollTop / viewPort * this.items.length / itemsInView;
        /** @type {?} */
        var start = Math.floor(indexByScrollTop);
        /** @type {?} */
        var end = Math.ceil(indexByScrollTop) + (itemsInView);
        this.translateY = this.itemHeight * Math.ceil(start);
        this.virtualHeight = this.itemHeight * this.items.length - this.translateY;
        this.renderItems = this.items.slice(start, end);
    };
    /**
     * @private
     * @return {?}
     */
    PdfThumbListComponent.prototype.getContainerSetup = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var element = this.element.nativeElement;
        /** @type {?} */
        var elementRec = element.getBoundingClientRect();
        /** @type {?} */
        var itemsInView = Math.ceil(elementRec.height / this.itemHeight);
        /** @type {?} */
        var viewPort = (this.itemHeight * this.items.length) / itemsInView;
        return {
            element: element,
            viewPort: viewPort,
            itemsInView: itemsInView
        };
    };
    /**
     * @private
     * @param {?} event
     * @return {?}
     */
    PdfThumbListComponent.prototype.onPageChange = /**
     * @private
     * @param {?} event
     * @return {?}
     */
    function (event) {
        /** @type {?} */
        var index = this.renderItems.findIndex((/**
         * @param {?} element
         * @return {?}
         */
        function (element) { return element.id === event.pageNumber; }));
        if (index < 0) {
            this.scrollInto(event.pageNumber);
        }
        if (index >= this.renderItems.length - 1) {
            this.element.nativeElement.scrollTop += this.itemHeight;
        }
    };
    PdfThumbListComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-pdf-thumbnails',
                    template: "<div class=\"adf-pdf-thumbnails__content\"\r\n    data-automation-id='adf-thumbnails-content'\r\n    [style.height.px]=\"virtualHeight\"\r\n    [style.transform]=\"'translate(-50%, ' + translateY + 'px)'\">\r\n    <adf-pdf-thumb *ngFor=\"let page of renderItems; trackBy: trackByFn\"\r\n        class=\"adf-pdf-thumbnails__thumb\"\r\n        [ngClass]=\"{'adf-pdf-thumbnails__thumb--selected' : isSelected(page.id)}\"\r\n        [page]=\"page\"\r\n        (click)=\"goTo(page.id)\">\r\n    </adf-pdf-thumb>\r\n</div>\r\n",
                    host: { 'class': 'adf-pdf-thumbnails' },
                    encapsulation: ViewEncapsulation.None,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    PdfThumbListComponent.ctorParameters = function () { return [
        { type: ElementRef }
    ]; };
    PdfThumbListComponent.propDecorators = {
        pdfViewer: [{ type: Input }],
        template: [{ type: ContentChild, args: [TemplateRef, { static: true },] }],
        onResize: [{ type: HostListener, args: ['window:resize', ['$event'],] }]
    };
    return PdfThumbListComponent;
}());
export { PdfThumbListComponent };
if (false) {
    /** @type {?} */
    PdfThumbListComponent.prototype.pdfViewer;
    /** @type {?} */
    PdfThumbListComponent.prototype.virtualHeight;
    /** @type {?} */
    PdfThumbListComponent.prototype.translateY;
    /** @type {?} */
    PdfThumbListComponent.prototype.renderItems;
    /** @type {?} */
    PdfThumbListComponent.prototype.width;
    /** @type {?} */
    PdfThumbListComponent.prototype.currentHeight;
    /**
     * @type {?}
     * @private
     */
    PdfThumbListComponent.prototype.items;
    /**
     * @type {?}
     * @private
     */
    PdfThumbListComponent.prototype.margin;
    /**
     * @type {?}
     * @private
     */
    PdfThumbListComponent.prototype.itemHeight;
    /** @type {?} */
    PdfThumbListComponent.prototype.template;
    /**
     * @type {?}
     * @private
     */
    PdfThumbListComponent.prototype.element;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGRmVmlld2VyLXRodW1ibmFpbHMuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsidmlld2VyL2NvbXBvbmVudHMvcGRmVmlld2VyLXRodW1ibmFpbHMuY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxPQUFPLEVBRUgsU0FBUyxFQUNULFlBQVksRUFDWixVQUFVLEVBQ1YsWUFBWSxFQUNaLEtBQUssRUFHTCxXQUFXLEVBQ1gsaUJBQWlCLEVBQ3BCLE1BQU0sZUFBZSxDQUFDO0FBRXZCO0lBNEJJLCtCQUFvQixPQUFtQjtRQUFuQixZQUFPLEdBQVAsT0FBTyxDQUFZO1FBbEJ2QyxrQkFBYSxHQUFXLENBQUMsQ0FBQztRQUMxQixlQUFVLEdBQVcsQ0FBQyxDQUFDO1FBQ3ZCLGdCQUFXLEdBQUcsRUFBRSxDQUFDO1FBQ2pCLFVBQUssR0FBVyxFQUFFLENBQUM7UUFDbkIsa0JBQWEsR0FBVyxDQUFDLENBQUM7UUFFbEIsVUFBSyxHQUFHLEVBQUUsQ0FBQztRQUNYLFdBQU0sR0FBVyxFQUFFLENBQUM7UUFDcEIsZUFBVSxHQUFXLEdBQUcsR0FBRyxJQUFJLENBQUMsTUFBTSxDQUFDO1FBVzNDLElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLFlBQVksR0FBRyxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztJQUNyRCxDQUFDOzs7OztJQVBELHdDQUFROzs7O0lBRFIsVUFDUyxLQUFLO1FBQ1YsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQzFCLENBQUM7Ozs7SUFPRCx3Q0FBUTs7O0lBQVI7UUFDSSw4QkFBOEI7UUFDOUIsSUFBSSxDQUFDLFNBQVMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUM7UUFDNUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsZ0JBQWdCLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFFakYsSUFBSSxDQUFDLFNBQVMsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGlCQUFpQixDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLEtBQUssR0FBRyxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFDN0IsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBRTFCLENBQUM7Ozs7SUFFRCwrQ0FBZTs7O0lBQWY7UUFBQSxpQkFFQztRQURHLFVBQVU7OztRQUFDLGNBQU0sT0FBQSxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUksQ0FBQyxTQUFTLENBQUMsaUJBQWlCLENBQUMsRUFBakQsQ0FBaUQsR0FBRSxDQUFDLENBQUMsQ0FBQztJQUMzRSxDQUFDOzs7O0lBRUQsMkNBQVc7OztJQUFYO1FBQ0ksSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsbUJBQW1CLENBQUMsUUFBUSxFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUM7UUFDcEYsOEJBQThCO1FBQzlCLElBQUksQ0FBQyxTQUFTLENBQUMsUUFBUSxDQUFDLEdBQUcsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO0lBQ2pFLENBQUM7Ozs7OztJQUVELHlDQUFTOzs7OztJQUFULFVBQVUsS0FBYSxFQUFFLElBQVM7UUFDOUIsT0FBTyxJQUFJLENBQUMsRUFBRSxDQUFDO0lBQ25CLENBQUM7Ozs7O0lBRUQsMENBQVU7Ozs7SUFBVixVQUFXLE9BQWU7UUFDdEIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDLGlCQUFpQixLQUFLLE9BQU8sQ0FBQztJQUN4RCxDQUFDOzs7OztJQUVELG9DQUFJOzs7O0lBQUosVUFBSyxPQUFlO1FBQ2hCLElBQUksQ0FBQyxTQUFTLENBQUMsaUJBQWlCLEdBQUcsT0FBTyxDQUFDO0lBQy9DLENBQUM7Ozs7O0lBRUQsMENBQVU7Ozs7SUFBVixVQUFXLElBQVM7UUFDaEIsSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRTs7Z0JBQ2IsS0FBSyxHQUFXLElBQUksQ0FBQyxLQUFLLENBQUMsU0FBUzs7OztZQUFDLFVBQUMsT0FBTyxJQUFLLE9BQUEsT0FBTyxDQUFDLEVBQUUsS0FBSyxJQUFJLEVBQW5CLENBQW1CLEVBQUM7WUFFNUUsSUFBSSxLQUFLLEdBQUcsQ0FBQyxJQUFJLEtBQUssSUFBSSxJQUFJLENBQUMsS0FBSyxDQUFDLE1BQU0sRUFBRTtnQkFDekMsT0FBTzthQUNWO1lBRUQsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsU0FBUyxHQUFHLEtBQUssR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDO1lBRS9ELElBQUksQ0FBQyxjQUFjLEVBQUUsQ0FBQztTQUN6QjtJQUNMLENBQUM7Ozs7SUFFRCx3Q0FBUTs7O0lBQVI7UUFBQSxpQkFPQztRQU5HLE9BQU8sSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsR0FBRzs7OztRQUFDLFVBQUMsSUFBSSxJQUFLLE9BQUEsQ0FBQztZQUN4QyxFQUFFLEVBQUUsSUFBSSxDQUFDLEVBQUU7WUFDWCxRQUFROzs7WUFBRSxjQUFRLE9BQU8sS0FBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUN0QyxTQUFTOzs7WUFBRSxjQUFRLE9BQU8sS0FBSSxDQUFDLGFBQWEsQ0FBQyxDQUFDLENBQUMsQ0FBQTtZQUMvQyxPQUFPOzs7WUFBRSxjQUFNLE9BQUEsS0FBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFLENBQUMsRUFBM0MsQ0FBMkMsQ0FBQTtTQUM3RCxDQUFDLEVBTHlDLENBS3pDLEVBQUMsQ0FBQztJQUNSLENBQUM7Ozs7OztJQUVPLHlDQUFTOzs7OztJQUFqQixVQUFrQixFQUFFO1FBQXBCLGlCQUdDOztZQUZTLE1BQU0sR0FBRyxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxPQUFPLENBQUMsRUFBRSxDQUFDLENBQUMsSUFBSTs7OztRQUFDLFVBQUMsSUFBSSxJQUFLLE9BQUEsS0FBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsRUFBMUIsQ0FBMEIsRUFBQztRQUNoRyxPQUFPLE1BQU0sQ0FBQztJQUNsQixDQUFDOzs7Ozs7SUFFTywrQ0FBZTs7Ozs7SUFBdkIsVUFBd0IsSUFBSTs7WUFDbEIsUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQyxDQUFDOztZQUM5QixTQUFTLEdBQUcsUUFBUSxDQUFDLEtBQUssR0FBRyxRQUFRLENBQUMsTUFBTTs7WUFDNUMsTUFBTSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLEtBQUssR0FBRyxTQUFTLENBQUM7UUFFakQsSUFBSSxDQUFDLGFBQWEsR0FBRyxNQUFNLENBQUM7UUFDNUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxNQUFNLEdBQUcsSUFBSSxDQUFDLE1BQU0sQ0FBQztJQUMzQyxDQUFDOzs7OztJQUVPLDhDQUFjOzs7O0lBQXRCO1FBQ1UsSUFBQSw2QkFBNkQsRUFBM0Qsb0JBQU8sRUFBRSxzQkFBUSxFQUFFLDRCQUF3Qzs7WUFFN0QsZ0JBQWdCLEdBQUcsT0FBTyxDQUFDLFNBQVMsR0FBRyxRQUFRLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxNQUFNLEdBQUcsV0FBVzs7WUFFakYsS0FBSyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsZ0JBQWdCLENBQUM7O1lBRXBDLEdBQUcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLEdBQUcsQ0FBQyxXQUFXLENBQUM7UUFFdkQsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUM7UUFDckQsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFJLElBQUksQ0FBQyxVQUFVLENBQUM7UUFDNUUsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssQ0FBQyxLQUFLLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFDcEQsQ0FBQzs7Ozs7SUFFTyxpREFBaUI7Ozs7SUFBekI7O1lBQ1UsT0FBTyxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsYUFBYTs7WUFDcEMsVUFBVSxHQUFHLE9BQU8sQ0FBQyxxQkFBcUIsRUFBRTs7WUFDNUMsV0FBVyxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsVUFBVSxDQUFDLE1BQU0sR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDOztZQUM1RCxRQUFRLEdBQUcsQ0FBQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLEdBQUcsV0FBVztRQUVwRSxPQUFPO1lBQ0gsT0FBTyxTQUFBO1lBQ1AsUUFBUSxVQUFBO1lBQ1IsV0FBVyxhQUFBO1NBQ2QsQ0FBQztJQUNOLENBQUM7Ozs7OztJQUVPLDRDQUFZOzs7OztJQUFwQixVQUFxQixLQUFLOztZQUNoQixLQUFLLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQyxTQUFTOzs7O1FBQUMsVUFBQyxPQUFPLElBQUssT0FBQSxPQUFPLENBQUMsRUFBRSxLQUFLLEtBQUssQ0FBQyxVQUFVLEVBQS9CLENBQStCLEVBQUM7UUFFdEYsSUFBSSxLQUFLLEdBQUcsQ0FBQyxFQUFFO1lBQ1gsSUFBSSxDQUFDLFVBQVUsQ0FBQyxLQUFLLENBQUMsVUFBVSxDQUFDLENBQUM7U0FDckM7UUFFRCxJQUFJLEtBQUssSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDdEMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxhQUFhLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUM7U0FDM0Q7SUFDTCxDQUFDOztnQkE1SUosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxvQkFBb0I7b0JBQzlCLG9oQkFBb0Q7b0JBRXBELElBQUksRUFBRSxFQUFFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRTtvQkFDdkMsYUFBYSxFQUFFLGlCQUFpQixDQUFDLElBQUk7O2lCQUN4Qzs7OztnQkFmRyxVQUFVOzs7NEJBaUJULEtBQUs7MkJBWUwsWUFBWSxTQUFDLFdBQVcsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7MkJBR3hDLFlBQVksU0FBQyxlQUFlLEVBQUUsQ0FBQyxRQUFRLENBQUM7O0lBc0g3Qyw0QkFBQztDQUFBLEFBN0lELElBNklDO1NBdElZLHFCQUFxQjs7O0lBQzlCLDBDQUF3Qjs7SUFFeEIsOENBQTBCOztJQUMxQiwyQ0FBdUI7O0lBQ3ZCLDRDQUFpQjs7SUFDakIsc0NBQW1COztJQUNuQiw4Q0FBMEI7Ozs7O0lBRTFCLHNDQUFtQjs7Ozs7SUFDbkIsdUNBQTRCOzs7OztJQUM1QiwyQ0FBK0M7O0lBRS9DLHlDQUNjOzs7OztJQU9GLHdDQUEyQiIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxuXG4vKiFcbiAqIEBsaWNlbnNlXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxuICpcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcbiAqXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXG4gKlxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cbiAqL1xuXG5pbXBvcnQge1xuICAgIEFmdGVyVmlld0luaXQsXG4gICAgQ29tcG9uZW50LFxuICAgIENvbnRlbnRDaGlsZCxcbiAgICBFbGVtZW50UmVmLFxuICAgIEhvc3RMaXN0ZW5lcixcbiAgICBJbnB1dCxcbiAgICBPbkRlc3Ryb3ksXG4gICAgT25Jbml0LFxuICAgIFRlbXBsYXRlUmVmLFxuICAgIFZpZXdFbmNhcHN1bGF0aW9uXG59IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2FkZi1wZGYtdGh1bWJuYWlscycsXG4gICAgdGVtcGxhdGVVcmw6ICcuL3BkZlZpZXdlci10aHVtYm5haWxzLmNvbXBvbmVudC5odG1sJyxcbiAgICBzdHlsZVVybHM6IFsnLi9wZGZWaWV3ZXItdGh1bWJuYWlscy5jb21wb25lbnQuc2NzcyddLFxuICAgIGhvc3Q6IHsgJ2NsYXNzJzogJ2FkZi1wZGYtdGh1bWJuYWlscycgfSxcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXG59KVxuZXhwb3J0IGNsYXNzIFBkZlRodW1iTGlzdENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCwgQWZ0ZXJWaWV3SW5pdCwgT25EZXN0cm95IHtcbiAgICBASW5wdXQoKSBwZGZWaWV3ZXI6IGFueTtcblxuICAgIHZpcnR1YWxIZWlnaHQ6IG51bWJlciA9IDA7XG4gICAgdHJhbnNsYXRlWTogbnVtYmVyID0gMDtcbiAgICByZW5kZXJJdGVtcyA9IFtdO1xuICAgIHdpZHRoOiBudW1iZXIgPSA5MTtcbiAgICBjdXJyZW50SGVpZ2h0OiBudW1iZXIgPSAwO1xuXG4gICAgcHJpdmF0ZSBpdGVtcyA9IFtdO1xuICAgIHByaXZhdGUgbWFyZ2luOiBudW1iZXIgPSAxNTtcbiAgICBwcml2YXRlIGl0ZW1IZWlnaHQ6IG51bWJlciA9IDExNCArIHRoaXMubWFyZ2luO1xuXG4gICAgQENvbnRlbnRDaGlsZChUZW1wbGF0ZVJlZiwge3N0YXRpYzogdHJ1ZX0pXG4gICAgdGVtcGxhdGU6IGFueTtcblxuICAgIEBIb3N0TGlzdGVuZXIoJ3dpbmRvdzpyZXNpemUnLCBbJyRldmVudCddKVxuICAgIG9uUmVzaXplKGV2ZW50KSB7XG4gICAgICAgIHRoaXMuY2FsY3VsYXRlSXRlbXMoKTtcbiAgICB9XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGVsZW1lbnQ6IEVsZW1lbnRSZWYpIHtcbiAgICAgICAgdGhpcy5jYWxjdWxhdGVJdGVtcyA9IHRoaXMuY2FsY3VsYXRlSXRlbXMuYmluZCh0aGlzKTtcbiAgICAgICAgdGhpcy5vblBhZ2VDaGFuZ2UgPSB0aGlzLm9uUGFnZUNoYW5nZS5iaW5kKHRoaXMpO1xuICAgIH1cblxuICAgIG5nT25Jbml0KCkge1xuICAgICAgICAvKiBjc3BlbGw6ZGlzYWJsZS1uZXh0LWxpbmUgKi9cbiAgICAgICAgdGhpcy5wZGZWaWV3ZXIuZXZlbnRCdXMub24oJ3BhZ2VjaGFuZ2UnLCB0aGlzLm9uUGFnZUNoYW5nZSk7XG4gICAgICAgIHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50LmFkZEV2ZW50TGlzdGVuZXIoJ3Njcm9sbCcsIHRoaXMuY2FsY3VsYXRlSXRlbXMsIHRydWUpO1xuXG4gICAgICAgIHRoaXMuc2V0SGVpZ2h0KHRoaXMucGRmVmlld2VyLmN1cnJlbnRQYWdlTnVtYmVyKTtcbiAgICAgICAgdGhpcy5pdGVtcyA9IHRoaXMuZ2V0UGFnZXMoKTtcbiAgICAgICAgdGhpcy5jYWxjdWxhdGVJdGVtcygpO1xuXG4gICAgfVxuXG4gICAgbmdBZnRlclZpZXdJbml0KCkge1xuICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHRoaXMuc2Nyb2xsSW50byh0aGlzLnBkZlZpZXdlci5jdXJyZW50UGFnZU51bWJlciksIDApO1xuICAgIH1cblxuICAgIG5nT25EZXN0cm95KCkge1xuICAgICAgICB0aGlzLmVsZW1lbnQubmF0aXZlRWxlbWVudC5yZW1vdmVFdmVudExpc3RlbmVyKCdzY3JvbGwnLCB0aGlzLmNhbGN1bGF0ZUl0ZW1zLCB0cnVlKTtcbiAgICAgICAgLyogY3NwZWxsOmRpc2FibGUtbmV4dC1saW5lICovXG4gICAgICAgIHRoaXMucGRmVmlld2VyLmV2ZW50QnVzLm9mZigncGFnZWNoYW5nZScsIHRoaXMub25QYWdlQ2hhbmdlKTtcbiAgICB9XG5cbiAgICB0cmFja0J5Rm4oaW5kZXg6IG51bWJlciwgaXRlbTogYW55KTogbnVtYmVyIHtcbiAgICAgICAgcmV0dXJuIGl0ZW0uaWQ7XG4gICAgfVxuXG4gICAgaXNTZWxlY3RlZChwYWdlTnVtOiBudW1iZXIpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucGRmVmlld2VyLmN1cnJlbnRQYWdlTnVtYmVyID09PSBwYWdlTnVtO1xuICAgIH1cblxuICAgIGdvVG8ocGFnZU51bTogbnVtYmVyKSB7XG4gICAgICAgIHRoaXMucGRmVmlld2VyLmN1cnJlbnRQYWdlTnVtYmVyID0gcGFnZU51bTtcbiAgICB9XG5cbiAgICBzY3JvbGxJbnRvKGl0ZW06IGFueSkge1xuICAgICAgICBpZiAodGhpcy5pdGVtcy5sZW5ndGgpIHtcbiAgICAgICAgICAgIGNvbnN0IGluZGV4OiBudW1iZXIgPSB0aGlzLml0ZW1zLmZpbmRJbmRleCgoZWxlbWVudCkgPT4gZWxlbWVudC5pZCA9PT0gaXRlbSk7XG5cbiAgICAgICAgICAgIGlmIChpbmRleCA8IDAgfHwgaW5kZXggPj0gdGhpcy5pdGVtcy5sZW5ndGgpIHtcbiAgICAgICAgICAgICAgICByZXR1cm47XG4gICAgICAgICAgICB9XG5cbiAgICAgICAgICAgIHRoaXMuZWxlbWVudC5uYXRpdmVFbGVtZW50LnNjcm9sbFRvcCA9IGluZGV4ICogdGhpcy5pdGVtSGVpZ2h0O1xuXG4gICAgICAgICAgICB0aGlzLmNhbGN1bGF0ZUl0ZW1zKCk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBnZXRQYWdlcygpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMucGRmVmlld2VyLl9wYWdlcy5tYXAoKHBhZ2UpID0+ICh7XG4gICAgICAgICAgICBpZDogcGFnZS5pZCxcbiAgICAgICAgICAgIGdldFdpZHRoOiAoKSA9PiB7IHJldHVybiB0aGlzLndpZHRoOyB9LFxuICAgICAgICAgICAgZ2V0SGVpZ2h0OiAoKSA9PiB7IHJldHVybiB0aGlzLmN1cnJlbnRIZWlnaHQ7IH0sXG4gICAgICAgICAgICBnZXRQYWdlOiAoKSA9PiB0aGlzLnBkZlZpZXdlci5wZGZEb2N1bWVudC5nZXRQYWdlKHBhZ2UuaWQpXG4gICAgICAgIH0pKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIHNldEhlaWdodChpZCk6IG51bWJlciB7XG4gICAgICAgIGNvbnN0IGhlaWdodCA9IHRoaXMucGRmVmlld2VyLnBkZkRvY3VtZW50LmdldFBhZ2UoaWQpLnRoZW4oKHBhZ2UpID0+IHRoaXMuY2FsY3VsYXRlSGVpZ2h0KHBhZ2UpKTtcbiAgICAgICAgcmV0dXJuIGhlaWdodDtcbiAgICB9XG5cbiAgICBwcml2YXRlIGNhbGN1bGF0ZUhlaWdodChwYWdlKSB7XG4gICAgICAgIGNvbnN0IHZpZXdwb3J0ID0gcGFnZS5nZXRWaWV3cG9ydCgxKTtcbiAgICAgICAgY29uc3QgcGFnZVJhdGlvID0gdmlld3BvcnQud2lkdGggLyB2aWV3cG9ydC5oZWlnaHQ7XG4gICAgICAgIGNvbnN0IGhlaWdodCA9IE1hdGguZmxvb3IodGhpcy53aWR0aCAvIHBhZ2VSYXRpbyk7XG5cbiAgICAgICAgdGhpcy5jdXJyZW50SGVpZ2h0ID0gaGVpZ2h0O1xuICAgICAgICB0aGlzLml0ZW1IZWlnaHQgPSBoZWlnaHQgKyB0aGlzLm1hcmdpbjtcbiAgICB9XG5cbiAgICBwcml2YXRlIGNhbGN1bGF0ZUl0ZW1zKCkge1xuICAgICAgICBjb25zdCB7IGVsZW1lbnQsIHZpZXdQb3J0LCBpdGVtc0luVmlldyB9ID0gdGhpcy5nZXRDb250YWluZXJTZXR1cCgpO1xuXG4gICAgICAgIGNvbnN0IGluZGV4QnlTY3JvbGxUb3AgPSBlbGVtZW50LnNjcm9sbFRvcCAvIHZpZXdQb3J0ICogdGhpcy5pdGVtcy5sZW5ndGggLyBpdGVtc0luVmlldztcblxuICAgICAgICBjb25zdCBzdGFydCA9IE1hdGguZmxvb3IoaW5kZXhCeVNjcm9sbFRvcCk7XG5cbiAgICAgICAgY29uc3QgZW5kID0gTWF0aC5jZWlsKGluZGV4QnlTY3JvbGxUb3ApICsgKGl0ZW1zSW5WaWV3KTtcblxuICAgICAgICB0aGlzLnRyYW5zbGF0ZVkgPSB0aGlzLml0ZW1IZWlnaHQgKiBNYXRoLmNlaWwoc3RhcnQpO1xuICAgICAgICB0aGlzLnZpcnR1YWxIZWlnaHQgPSB0aGlzLml0ZW1IZWlnaHQgKiB0aGlzLml0ZW1zLmxlbmd0aCAgLSB0aGlzLnRyYW5zbGF0ZVk7XG4gICAgICAgIHRoaXMucmVuZGVySXRlbXMgPSB0aGlzLml0ZW1zLnNsaWNlKHN0YXJ0LCBlbmQpO1xuICAgIH1cblxuICAgIHByaXZhdGUgZ2V0Q29udGFpbmVyU2V0dXAoKSB7XG4gICAgICAgIGNvbnN0IGVsZW1lbnQgPSB0aGlzLmVsZW1lbnQubmF0aXZlRWxlbWVudDtcbiAgICAgICAgY29uc3QgZWxlbWVudFJlYyA9IGVsZW1lbnQuZ2V0Qm91bmRpbmdDbGllbnRSZWN0KCk7XG4gICAgICAgIGNvbnN0IGl0ZW1zSW5WaWV3ID0gTWF0aC5jZWlsKGVsZW1lbnRSZWMuaGVpZ2h0IC8gdGhpcy5pdGVtSGVpZ2h0KTtcbiAgICAgICAgY29uc3Qgdmlld1BvcnQgPSAodGhpcy5pdGVtSGVpZ2h0ICogdGhpcy5pdGVtcy5sZW5ndGgpIC8gaXRlbXNJblZpZXc7XG5cbiAgICAgICAgcmV0dXJuIHtcbiAgICAgICAgICAgIGVsZW1lbnQsXG4gICAgICAgICAgICB2aWV3UG9ydCxcbiAgICAgICAgICAgIGl0ZW1zSW5WaWV3XG4gICAgICAgIH07XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBvblBhZ2VDaGFuZ2UoZXZlbnQpIHtcbiAgICAgICAgY29uc3QgaW5kZXggPSB0aGlzLnJlbmRlckl0ZW1zLmZpbmRJbmRleCgoZWxlbWVudCkgPT4gZWxlbWVudC5pZCA9PT0gZXZlbnQucGFnZU51bWJlcik7XG5cbiAgICAgICAgaWYgKGluZGV4IDwgMCkge1xuICAgICAgICAgICAgdGhpcy5zY3JvbGxJbnRvKGV2ZW50LnBhZ2VOdW1iZXIpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKGluZGV4ID49IHRoaXMucmVuZGVySXRlbXMubGVuZ3RoIC0gMSkge1xuICAgICAgICAgICAgdGhpcy5lbGVtZW50Lm5hdGl2ZUVsZW1lbnQuc2Nyb2xsVG9wICs9IHRoaXMuaXRlbUhlaWdodDtcbiAgICAgICAgfVxuICAgIH1cbn1cbiJdfQ==