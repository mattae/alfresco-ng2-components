/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, TemplateRef, HostListener, Output, Input, ViewEncapsulation, EventEmitter } from '@angular/core';
import { MatDialog } from '@angular/material';
import { LogService } from '../../services/log.service';
import { RenderingQueueServices } from '../services/rendering-queue.services';
import { PdfPasswordDialogComponent } from './pdfViewer-password-dialog';
import { AppConfigService } from './../../app-config/app-config.service';
/**
 * @record
 */
export function PdfDocumentOptions() { }
if (false) {
    /** @type {?|undefined} */
    PdfDocumentOptions.prototype.url;
    /** @type {?|undefined} */
    PdfDocumentOptions.prototype.data;
    /** @type {?|undefined} */
    PdfDocumentOptions.prototype.withCredentials;
}
var PdfViewerComponent = /** @class */ (function () {
    function PdfViewerComponent(dialog, renderingQueueServices, logService, appConfigService) {
        this.dialog = dialog;
        this.renderingQueueServices = renderingQueueServices;
        this.logService = logService;
        this.appConfigService = appConfigService;
        this.showToolbar = true;
        this.allowThumbnails = false;
        this.thumbnailsTemplate = null;
        this.rendered = new EventEmitter();
        this.error = new EventEmitter();
        this.close = new EventEmitter();
        this.currentScaleMode = 'auto';
        this.currentScale = 1;
        this.MAX_AUTO_SCALE = 1.25;
        this.DEFAULT_SCALE_DELTA = 1.1;
        this.MIN_SCALE = 0.25;
        this.MAX_SCALE = 10.0;
        this.isPanelDisabled = true;
        this.showThumbnails = false;
        this.pdfThumbnailsContext = { viewer: null };
        // needed to preserve "this" context
        this.onPageChange = this.onPageChange.bind(this);
        this.onPagesLoaded = this.onPagesLoaded.bind(this);
        this.onPageRendered = this.onPageRendered.bind(this);
        this.randomPdfId = this.generateUuid();
        this.currentScale = this.getUserScaling();
    }
    Object.defineProperty(PdfViewerComponent.prototype, "currentScaleText", {
        get: /**
         * @return {?}
         */
        function () {
            return Math.round(this.currentScale * 100) + '%';
        },
        enumerable: true,
        configurable: true
    });
    /**
     * @return {?}
     */
    PdfViewerComponent.prototype.getUserScaling = /**
     * @return {?}
     */
    function () {
        /** @type {?} */
        var scaleConfig = this.appConfigService.get('adf-viewer.pdf-viewer-scaling', undefined) / 100;
        if (scaleConfig) {
            return this.checkLimits(scaleConfig);
        }
        else {
            return 1;
        }
    };
    /**
     * @param {?} scaleConfig
     * @return {?}
     */
    PdfViewerComponent.prototype.checkLimits = /**
     * @param {?} scaleConfig
     * @return {?}
     */
    function (scaleConfig) {
        if (scaleConfig > this.MAX_SCALE) {
            return this.MAX_SCALE;
        }
        else if (scaleConfig < this.MIN_SCALE) {
            return this.MIN_SCALE;
        }
        else {
            return scaleConfig;
        }
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    PdfViewerComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        var _this = this;
        /** @type {?} */
        var blobFile = changes['blobFile'];
        if (blobFile && blobFile.currentValue) {
            /** @type {?} */
            var reader_1 = new FileReader();
            reader_1.onload = (/**
             * @return {?}
             */
            function () {
                /** @type {?} */
                var options = {
                    data: reader_1.result,
                    withCredentials: _this.appConfigService.get('auth.withCredentials', undefined)
                };
                _this.executePdf(options);
            });
            reader_1.readAsArrayBuffer(blobFile.currentValue);
        }
        /** @type {?} */
        var urlFile = changes['urlFile'];
        if (urlFile && urlFile.currentValue) {
            /** @type {?} */
            var options = {
                url: urlFile.currentValue,
                withCredentials: this.appConfigService.get('auth.withCredentials', undefined)
            };
            this.executePdf(options);
        }
        if (!this.urlFile && !this.blobFile) {
            throw new Error('Attribute urlFile or blobFile is required');
        }
    };
    /**
     * @param {?} pdfOptions
     * @return {?}
     */
    PdfViewerComponent.prototype.executePdf = /**
     * @param {?} pdfOptions
     * @return {?}
     */
    function (pdfOptions) {
        var _this = this;
        pdfjsLib.GlobalWorkerOptions.workerSrc = 'pdf.worker.min.js';
        this.loadingTask = pdfjsLib.getDocument(pdfOptions);
        this.loadingTask.onPassword = (/**
         * @param {?} callback
         * @param {?} reason
         * @return {?}
         */
        function (callback, reason) {
            _this.onPdfPassword(callback, reason);
        });
        this.loadingTask.onProgress = (/**
         * @param {?} progressData
         * @return {?}
         */
        function (progressData) {
            /** @type {?} */
            var level = progressData.loaded / progressData.total;
            _this.loadingPercent = Math.round(level * 100);
        });
        this.loadingTask.then((/**
         * @param {?} pdfDocument
         * @return {?}
         */
        function (pdfDocument) {
            _this.currentPdfDocument = pdfDocument;
            _this.totalPages = pdfDocument.numPages;
            _this.page = 1;
            _this.displayPage = 1;
            _this.initPDFViewer(_this.currentPdfDocument);
            _this.currentPdfDocument.getPage(1).then((/**
             * @return {?}
             */
            function () {
                _this.scalePage('auto');
            }), (/**
             * @return {?}
             */
            function () {
                _this.error.emit();
            }));
        }), (/**
         * @return {?}
         */
        function () {
            _this.error.emit();
        }));
    };
    /**
     * @param {?} pdfDocument
     * @return {?}
     */
    PdfViewerComponent.prototype.initPDFViewer = /**
     * @param {?} pdfDocument
     * @return {?}
     */
    function (pdfDocument) {
        /** @type {?} */
        var viewer = document.getElementById(this.randomPdfId + "-viewer-viewerPdf");
        /** @type {?} */
        var container = document.getElementById(this.randomPdfId + "-viewer-pdf-viewer");
        if (viewer && container) {
            this.documentContainer = container;
            // cspell: disable-next
            this.documentContainer.addEventListener('pagechange', this.onPageChange, true);
            // cspell: disable-next
            this.documentContainer.addEventListener('pagesloaded', this.onPagesLoaded, true);
            // cspell: disable-next
            this.documentContainer.addEventListener('textlayerrendered', this.onPageRendered, true);
            this.pdfViewer = new pdfjsViewer.PDFViewer({
                container: this.documentContainer,
                viewer: viewer,
                renderingQueue: this.renderingQueueServices
            });
            this.renderingQueueServices.setViewer(this.pdfViewer);
            this.pdfViewer.setDocument(pdfDocument);
            this.pdfThumbnailsContext.viewer = this.pdfViewer;
        }
    };
    /**
     * @return {?}
     */
    PdfViewerComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        if (this.documentContainer) {
            // cspell: disable-next
            this.documentContainer.removeEventListener('pagechange', this.onPageChange, true);
            // cspell: disable-next
            this.documentContainer.removeEventListener('pagesloaded', this.onPagesLoaded, true);
            // cspell: disable-next
            this.documentContainer.removeEventListener('textlayerrendered', this.onPageRendered, true);
        }
        if (this.loadingTask) {
            try {
                this.loadingTask.destroy();
            }
            catch (_a) {
            }
            this.loadingTask = null;
        }
    };
    /**
     * @return {?}
     */
    PdfViewerComponent.prototype.toggleThumbnails = /**
     * @return {?}
     */
    function () {
        this.showThumbnails = !this.showThumbnails;
    };
    /**
     * Method to scale the page current support implementation
     *
     * @param scaleMode - new scale mode
     */
    /**
     * Method to scale the page current support implementation
     *
     * @param {?} scaleMode - new scale mode
     * @return {?}
     */
    PdfViewerComponent.prototype.scalePage = /**
     * Method to scale the page current support implementation
     *
     * @param {?} scaleMode - new scale mode
     * @return {?}
     */
    function (scaleMode) {
        this.currentScaleMode = scaleMode;
        /** @type {?} */
        var viewerContainer = document.getElementById(this.randomPdfId + "-viewer-main-container");
        /** @type {?} */
        var documentContainer = document.getElementById(this.randomPdfId + "-viewer-pdf-viewer");
        if (this.pdfViewer && documentContainer) {
            /** @type {?} */
            var widthContainer = void 0;
            /** @type {?} */
            var heightContainer = void 0;
            if (viewerContainer && viewerContainer.clientWidth <= documentContainer.clientWidth) {
                widthContainer = viewerContainer.clientWidth;
                heightContainer = viewerContainer.clientHeight;
            }
            else {
                widthContainer = documentContainer.clientWidth;
                heightContainer = documentContainer.clientHeight;
            }
            /** @type {?} */
            var currentPage = this.pdfViewer._pages[this.pdfViewer._currentPageNumber - 1];
            /** @type {?} */
            var padding = 20;
            /** @type {?} */
            var pageWidthScale = (widthContainer - padding) / currentPage.width * currentPage.scale;
            /** @type {?} */
            var pageHeightScale = (heightContainer - padding) / currentPage.width * currentPage.scale;
            /** @type {?} */
            var scale = this.getUserScaling();
            if (!scale) {
                switch (this.currentScaleMode) {
                    case 'page-actual':
                        scale = 1;
                        break;
                    case 'page-width':
                        scale = pageWidthScale;
                        break;
                    case 'page-height':
                        scale = pageHeightScale;
                        break;
                    case 'page-fit':
                        scale = Math.min(pageWidthScale, pageHeightScale);
                        break;
                    case 'auto':
                        /** @type {?} */
                        var horizontalScale = void 0;
                        if (this.isLandscape) {
                            horizontalScale = Math.min(pageHeightScale, pageWidthScale);
                        }
                        else {
                            horizontalScale = pageWidthScale;
                        }
                        horizontalScale = Math.round(horizontalScale);
                        scale = Math.min(this.MAX_AUTO_SCALE, horizontalScale);
                        break;
                    default:
                        this.logService.error('pdfViewSetScale: \'' + scaleMode + '\' is an unknown zoom value.');
                        return;
                }
                this.setScaleUpdatePages(scale);
            }
            else {
                this.currentScale = 0;
                this.setScaleUpdatePages(scale);
            }
        }
    };
    /**
     * Update all the pages with the newScale scale
     *
     * @param newScale - new scale page
     */
    /**
     * Update all the pages with the newScale scale
     *
     * @param {?} newScale - new scale page
     * @return {?}
     */
    PdfViewerComponent.prototype.setScaleUpdatePages = /**
     * Update all the pages with the newScale scale
     *
     * @param {?} newScale - new scale page
     * @return {?}
     */
    function (newScale) {
        if (this.pdfViewer) {
            if (!this.isSameScale(this.currentScale, newScale)) {
                this.currentScale = newScale;
                this.pdfViewer._pages.forEach((/**
                 * @param {?} currentPage
                 * @return {?}
                 */
                function (currentPage) {
                    currentPage.update(newScale);
                }));
            }
            this.pdfViewer.update();
        }
    };
    /**
     * Check if the request scale of the page is the same for avoid useless re-rendering
     *
     * @param oldScale - old scale page
     * @param newScale - new scale page
     *
     */
    /**
     * Check if the request scale of the page is the same for avoid useless re-rendering
     *
     * @param {?} oldScale - old scale page
     * @param {?} newScale - new scale page
     *
     * @return {?}
     */
    PdfViewerComponent.prototype.isSameScale = /**
     * Check if the request scale of the page is the same for avoid useless re-rendering
     *
     * @param {?} oldScale - old scale page
     * @param {?} newScale - new scale page
     *
     * @return {?}
     */
    function (oldScale, newScale) {
        return (newScale === oldScale);
    };
    /**
     * Check if is a land scape view
     *
     * @param width
     * @param height
     */
    /**
     * Check if is a land scape view
     *
     * @param {?} width
     * @param {?} height
     * @return {?}
     */
    PdfViewerComponent.prototype.isLandscape = /**
     * Check if is a land scape view
     *
     * @param {?} width
     * @param {?} height
     * @return {?}
     */
    function (width, height) {
        return (width > height);
    };
    /**
     * Method triggered when the page is resized
     */
    /**
     * Method triggered when the page is resized
     * @return {?}
     */
    PdfViewerComponent.prototype.onResize = /**
     * Method triggered when the page is resized
     * @return {?}
     */
    function () {
        this.scalePage(this.currentScaleMode);
    };
    /**
     * toggle the fit page pdf
     */
    /**
     * toggle the fit page pdf
     * @return {?}
     */
    PdfViewerComponent.prototype.pageFit = /**
     * toggle the fit page pdf
     * @return {?}
     */
    function () {
        if (this.currentScaleMode !== 'page-fit') {
            this.scalePage('page-fit');
        }
        else {
            this.scalePage('auto');
        }
    };
    /**
     * zoom in page pdf
     *
     * @param ticks
     */
    /**
     * zoom in page pdf
     *
     * @param {?=} ticks
     * @return {?}
     */
    PdfViewerComponent.prototype.zoomIn = /**
     * zoom in page pdf
     *
     * @param {?=} ticks
     * @return {?}
     */
    function (ticks) {
        /** @type {?} */
        var newScale = this.currentScale;
        do {
            newScale = (newScale * this.DEFAULT_SCALE_DELTA).toFixed(2);
            newScale = Math.ceil(newScale * 10) / 10;
            newScale = Math.min(this.MAX_SCALE, newScale);
        } while (--ticks > 0 && newScale < this.MAX_SCALE);
        this.currentScaleMode = 'auto';
        this.setScaleUpdatePages(newScale);
    };
    /**
     * zoom out page pdf
     *
     * @param ticks
     */
    /**
     * zoom out page pdf
     *
     * @param {?=} ticks
     * @return {?}
     */
    PdfViewerComponent.prototype.zoomOut = /**
     * zoom out page pdf
     *
     * @param {?=} ticks
     * @return {?}
     */
    function (ticks) {
        /** @type {?} */
        var newScale = this.currentScale;
        do {
            newScale = (newScale / this.DEFAULT_SCALE_DELTA).toFixed(2);
            newScale = Math.floor(newScale * 10) / 10;
            newScale = Math.max(this.MIN_SCALE, newScale);
        } while (--ticks > 0 && newScale > this.MIN_SCALE);
        this.currentScaleMode = 'auto';
        this.setScaleUpdatePages(newScale);
    };
    /**
     * load the previous page
     */
    /**
     * load the previous page
     * @return {?}
     */
    PdfViewerComponent.prototype.previousPage = /**
     * load the previous page
     * @return {?}
     */
    function () {
        if (this.pdfViewer && this.page > 1) {
            this.page--;
            this.displayPage = this.page;
            this.pdfViewer.currentPageNumber = this.page;
        }
    };
    /**
     * load the next page
     */
    /**
     * load the next page
     * @return {?}
     */
    PdfViewerComponent.prototype.nextPage = /**
     * load the next page
     * @return {?}
     */
    function () {
        if (this.pdfViewer && this.page < this.totalPages) {
            this.page++;
            this.displayPage = this.page;
            this.pdfViewer.currentPageNumber = this.page;
        }
    };
    /**
     * load the page in input
     *
     * @param page to load
     */
    /**
     * load the page in input
     *
     * @param {?} page to load
     * @return {?}
     */
    PdfViewerComponent.prototype.inputPage = /**
     * load the page in input
     *
     * @param {?} page to load
     * @return {?}
     */
    function (page) {
        /** @type {?} */
        var pageInput = parseInt(page, 10);
        if (!isNaN(pageInput) && pageInput > 0 && pageInput <= this.totalPages) {
            this.page = pageInput;
            this.displayPage = this.page;
            this.pdfViewer.currentPageNumber = this.page;
        }
        else {
            this.displayPage = this.page;
        }
    };
    /**
     * Page Change Event
     *
     * @param event
     */
    /**
     * Page Change Event
     *
     * @param {?} event
     * @return {?}
     */
    PdfViewerComponent.prototype.onPageChange = /**
     * Page Change Event
     *
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.page = event.pageNumber;
        this.displayPage = event.pageNumber;
    };
    /**
     * @param {?} callback
     * @param {?} reason
     * @return {?}
     */
    PdfViewerComponent.prototype.onPdfPassword = /**
     * @param {?} callback
     * @param {?} reason
     * @return {?}
     */
    function (callback, reason) {
        var _this = this;
        this.dialog
            .open(PdfPasswordDialogComponent, {
            width: '400px',
            data: { reason: reason }
        })
            .afterClosed().subscribe((/**
         * @param {?} password
         * @return {?}
         */
        function (password) {
            if (password) {
                callback(password);
            }
            else {
                _this.close.emit();
            }
        }));
    };
    /**
     * Page Rendered Event
     */
    /**
     * Page Rendered Event
     * @return {?}
     */
    PdfViewerComponent.prototype.onPageRendered = /**
     * Page Rendered Event
     * @return {?}
     */
    function () {
        this.rendered.emit();
    };
    /**
     * Pages Loaded Event
     *
     * @param event
     */
    /**
     * Pages Loaded Event
     *
     * @param {?} event
     * @return {?}
     */
    PdfViewerComponent.prototype.onPagesLoaded = /**
     * Pages Loaded Event
     *
     * @param {?} event
     * @return {?}
     */
    function (event) {
        this.isPanelDisabled = false;
    };
    /**
     * Keyboard Event Listener
     * @param KeyboardEvent event
     */
    /**
     * Keyboard Event Listener
     * @param {?} event
     * @return {?}
     */
    PdfViewerComponent.prototype.handleKeyboardEvent = /**
     * Keyboard Event Listener
     * @param {?} event
     * @return {?}
     */
    function (event) {
        /** @type {?} */
        var key = event.keyCode;
        if (key === 39) { // right arrow
            this.nextPage();
        }
        else if (key === 37) { // left arrow
            this.previousPage();
        }
    };
    /**
     * @private
     * @return {?}
     */
    PdfViewerComponent.prototype.generateUuid = /**
     * @private
     * @return {?}
     */
    function () {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (/**
         * @param {?} c
         * @return {?}
         */
        function (c) {
            /** @type {?} */
            var r = Math.random() * 16 | 0;
            /** @type {?} */
            var v = c === 'x' ? r : (r & 0x3 | 0x8);
            return v.toString(16);
        }));
    };
    PdfViewerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-pdf-viewer',
                    template: "<div class=\"adf-pdf-viewer__container\">\r\n    <ng-container *ngIf=\"showThumbnails\">\r\n        <div class=\"adf-pdf-viewer__thumbnails\">\r\n            <div class=\"adf-thumbnails-template__container\">\r\n                <div class=\"adf-thumbnails-template__buttons\">\r\n                    <button mat-icon-button data-automation-id='adf-thumbnails-close' (click)=\"toggleThumbnails()\">\r\n                        <mat-icon>close</mat-icon>\r\n                    </button>\r\n                </div>\r\n\r\n                <ng-container *ngIf=\"thumbnailsTemplate\">\r\n                    <ng-container *ngTemplateOutlet=\"thumbnailsTemplate;context:pdfThumbnailsContext\"></ng-container>\r\n                </ng-container>\r\n\r\n                <ng-container *ngIf=\"!thumbnailsTemplate\">\r\n                    <adf-pdf-thumbnails [pdfViewer]=\"pdfViewer\"></adf-pdf-thumbnails>\r\n                </ng-container>\r\n            </div>\r\n        </div>\r\n    </ng-container>\r\n\r\n    <div class=\"adf-pdf-viewer__content\">\r\n        <div [id]=\"randomPdfId+'-viewer-pdf-viewer'\" class=\"adf-viewer-pdf-viewer\" (window:resize)=\"onResize()\">\r\n            <div [id]=\"randomPdfId+'-viewer-viewerPdf'\" class=\"adf-pdfViewer\" role=\"document\" tabindex=\"0\" aria-expanded=\"true\">\r\n                <div id=\"loader-container\" class=\"adf-loader-container\">\r\n                    <div class=\"adf-loader-item\">\r\n                        <mat-progress-bar mode=\"indeterminate\"></mat-progress-bar>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n<div class=\"adf-pdf-viewer__toolbar\" *ngIf=\"showToolbar\">\r\n    <adf-toolbar>\r\n\r\n        <ng-container *ngIf=\"allowThumbnails\">\r\n            <button mat-icon-button\r\n                    data-automation-id=\"adf-thumbnails-button\"\r\n                    [disabled]=\"isPanelDisabled\"\r\n                    (click)=\"toggleThumbnails()\">\r\n                <mat-icon>dashboard</mat-icon>\r\n            </button>\r\n            <adf-toolbar-divider></adf-toolbar-divider>\r\n        </ng-container>\r\n\r\n        <button\r\n            id=\"viewer-previous-page-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.PREVIOUS_PAGE' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.PREVIOUS_PAGE' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"previousPage()\">\r\n            <mat-icon>keyboard_arrow_up</mat-icon>\r\n        </button>\r\n\r\n        <button\r\n            id=\"viewer-next-page-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.NEXT_PAGE' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.NEXT_PAGE' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"nextPage()\">\r\n            <mat-icon>keyboard_arrow_down</mat-icon>\r\n        </button>\r\n\r\n        <div class=\"adf-pdf-viewer__toolbar-page-selector\">\r\n            <span>{{ 'ADF_VIEWER.PAGE_LABEL.SHOWING' | translate }}</span>\r\n            <input #page\r\n                   type=\"text\"\r\n                   data-automation-id=\"adf-page-selector\"\r\n                   pattern=\"-?[0-9]*(\\.[0-9]+)?\"\r\n                   value=\"{{ displayPage }}\"\r\n                   (keyup.enter)=\"inputPage(page.value)\">\r\n            <span>{{ 'ADF_VIEWER.PAGE_LABEL.OF' | translate }} {{ totalPages }}</span>\r\n        </div>\r\n\r\n        <div class=\"adf-viewer__toolbar-page-scale\" data-automation-id=\"adf-page-scale\">\r\n            {{ currentScaleText }}\r\n        </div>\r\n\r\n        <button\r\n            id=\"viewer-zoom-in-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.ZOOM_IN' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.ZOOM_IN' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"zoomIn()\">\r\n            <mat-icon>zoom_in</mat-icon>\r\n        </button>\r\n\r\n        <button\r\n            id=\"viewer-zoom-out-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.ZOOM_OUT' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.ZOOM_OUT' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"zoomOut()\">\r\n            <mat-icon>zoom_out</mat-icon>\r\n        </button>\r\n\r\n        <button\r\n            id=\"viewer-scale-page-button\"\r\n            title=\"{{ 'ADF_VIEWER.ARIA.FIT_PAGE' | translate }}\"\r\n            attr.aria-label=\"{{ 'ADF_VIEWER.ARIA.FIT_PAGE' | translate }}\"\r\n            mat-icon-button\r\n            (click)=\"pageFit()\">\r\n            <mat-icon>zoom_out_map</mat-icon>\r\n        </button>\r\n\r\n    </adf-toolbar>\r\n</div>\r\n",
                    providers: [RenderingQueueServices],
                    host: { 'class': 'adf-pdf-viewer' },
                    encapsulation: ViewEncapsulation.None,
                    styles: ["", ".adf-pdf-viewer .textLayer{position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;opacity:.2;line-height:1;border:1px solid gray}.adf-pdf-viewer .textLayer>div{color:transparent;position:absolute;white-space:pre;cursor:text;transform-origin:0 0}.adf-pdf-viewer .textLayer .adf-highlight{margin:-1px;padding:1px;background-color:#b400aa;border-radius:4px}.adf-pdf-viewer .textLayer .adf-highlight.adf-begin{border-radius:4px 0 0 4px}.adf-pdf-viewer .textLayer .adf-highlight.adf-end{border-radius:0 4px 4px 0}.adf-pdf-viewer .textLayer .adf-highlight.adf-middle{border-radius:0}.adf-pdf-viewer .textLayer .adf-highlight.adf-selected{background-color:#006400}.adf-pdf-viewer .textLayer::selection{background:#00f}.adf-pdf-viewer .textLayer::-moz-selection{background:#00f}.adf-pdf-viewer .textLayer .adf-endOfContent{display:block;position:absolute;left:0;top:100%;right:0;bottom:0;z-index:-1;cursor:default;user-select:none;-webkit-user-select:none;-ms-user-select:none;-moz-user-select:none}.adf-pdf-viewer .textLayer .adf-endOfContent.adf-active{top:0}.adf-pdf-viewer .adf-annotationLayer section{position:absolute}.adf-pdf-viewer .adf-annotationLayer .adf-linkAnnotation>a{position:absolute;font-size:1em;top:0;left:0;width:100%;height:100%;background:url(data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7)}.adf-pdf-viewer .adf-annotationLayer .adf-linkAnnotation>a:hover{opacity:.2;background:#ff0;box-shadow:0 2px 10px #ff0}.adf-pdf-viewer .adf-annotationLayer .adf-textAnnotation img{position:absolute;cursor:pointer}.adf-pdf-viewer .adf-annotationLayer .adf-popupWrapper{position:absolute;width:20em}.adf-pdf-viewer .adf-annotationLayer .adf-popup{position:absolute;z-index:200;max-width:20em;background-color:#ff9;box-shadow:0 2px 5px #333;border-radius:2px;padding:.6em;margin-left:5px;cursor:pointer;word-wrap:break-word}.adf-pdf-viewer .adf-annotationLayer .adf-popup h1{font-size:1em;border-bottom:1px solid #000;padding-bottom:.2em}.adf-pdf-viewer .adf-annotationLayer .adf-popup p{padding-top:.2em}.adf-pdf-viewer .adf-annotationLayer .adf-fileAttachmentAnnotation,.adf-pdf-viewer .adf-annotationLayer .adf-highlightAnnotation,.adf-pdf-viewer .adf-annotationLayer .adf-squigglyAnnotation,.adf-pdf-viewer .adf-annotationLayer .adf-strikeoutAnnotation,.adf-pdf-viewer .adf-annotationLayer .adf-underlineAnnotation{cursor:pointer}.adf-pdf-viewer .adf-pdfViewer .canvasWrapper{overflow:hidden}.adf-pdf-viewer .adf-pdfViewer .page{direction:ltr;width:816px;height:1056px;margin:1px auto -8px;position:relative;overflow:visible;border:9px solid transparent;background-clip:content-box;background-color:#fff}.adf-pdf-viewer .adf-pdfViewer .page canvas{margin:0;display:block}.adf-pdf-viewer .adf-pdfViewer .page .adf-loadingIcon{position:absolute;display:block;left:0;top:0;right:0;bottom:0}.adf-pdf-viewer .adf-pdfViewer .page *{padding:0;margin:0}.adf-pdf-viewer .adf-pdfViewer.adf-removePageBorders .adf-page{margin:0 auto 10px;border:none}.adf-pdf-viewer .adf-pdfViewer .adf-loadingIcon{width:100px;height:100px;left:50%!important;top:50%!important;margin-top:-50px;margin-left:-50px;font-size:5px;text-indent:-9999em;border-top:1.1em solid rgba(3,0,2,.2);border-right:1.1em solid rgba(3,0,2,.2);border-bottom:1.1em solid rgba(3,0,2,.2);border-left:1.1em solid #030002;transform:translateZ(0);-webkit-animation:1.1s linear infinite load8;animation:1.1s linear infinite load8;border-radius:50%}.adf-pdf-viewer .adf-pdfViewer .adf-loadingIcon::after{border-radius:50%}.adf-pdf-viewer .adf-hidden,.adf-pdf-viewer [hidden]{display:none!important}@-webkit-keyframes load8{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}@keyframes load8{0%{transform:rotate(0)}100%{transform:rotate(360deg)}}.adf-viewer-pdf-viewer{overflow:auto;-webkit-overflow-scrolling:touch;position:absolute;top:0;right:0;bottom:0;left:0;outline:0}html[dir=ltr] .adf-viewer-pdf-viewer{box-shadow:inset 1px 0 0 rgba(255,255,255,.05)}html[dir=rtl] .adf-viewer-pdf-viewer{box-shadow:inset -1px 0 0 rgba(255,255,255,.05)}"]
                }] }
    ];
    /** @nocollapse */
    PdfViewerComponent.ctorParameters = function () { return [
        { type: MatDialog },
        { type: RenderingQueueServices },
        { type: LogService },
        { type: AppConfigService }
    ]; };
    PdfViewerComponent.propDecorators = {
        urlFile: [{ type: Input }],
        blobFile: [{ type: Input }],
        nameFile: [{ type: Input }],
        showToolbar: [{ type: Input }],
        allowThumbnails: [{ type: Input }],
        thumbnailsTemplate: [{ type: Input }],
        rendered: [{ type: Output }],
        error: [{ type: Output }],
        close: [{ type: Output }],
        handleKeyboardEvent: [{ type: HostListener, args: ['document:keydown', ['$event'],] }]
    };
    return PdfViewerComponent;
}());
export { PdfViewerComponent };
if (false) {
    /** @type {?} */
    PdfViewerComponent.prototype.urlFile;
    /** @type {?} */
    PdfViewerComponent.prototype.blobFile;
    /** @type {?} */
    PdfViewerComponent.prototype.nameFile;
    /** @type {?} */
    PdfViewerComponent.prototype.showToolbar;
    /** @type {?} */
    PdfViewerComponent.prototype.allowThumbnails;
    /** @type {?} */
    PdfViewerComponent.prototype.thumbnailsTemplate;
    /** @type {?} */
    PdfViewerComponent.prototype.rendered;
    /** @type {?} */
    PdfViewerComponent.prototype.error;
    /** @type {?} */
    PdfViewerComponent.prototype.close;
    /** @type {?} */
    PdfViewerComponent.prototype.loadingTask;
    /** @type {?} */
    PdfViewerComponent.prototype.currentPdfDocument;
    /** @type {?} */
    PdfViewerComponent.prototype.page;
    /** @type {?} */
    PdfViewerComponent.prototype.displayPage;
    /** @type {?} */
    PdfViewerComponent.prototype.totalPages;
    /** @type {?} */
    PdfViewerComponent.prototype.loadingPercent;
    /** @type {?} */
    PdfViewerComponent.prototype.pdfViewer;
    /** @type {?} */
    PdfViewerComponent.prototype.documentContainer;
    /** @type {?} */
    PdfViewerComponent.prototype.currentScaleMode;
    /** @type {?} */
    PdfViewerComponent.prototype.currentScale;
    /** @type {?} */
    PdfViewerComponent.prototype.MAX_AUTO_SCALE;
    /** @type {?} */
    PdfViewerComponent.prototype.DEFAULT_SCALE_DELTA;
    /** @type {?} */
    PdfViewerComponent.prototype.MIN_SCALE;
    /** @type {?} */
    PdfViewerComponent.prototype.MAX_SCALE;
    /** @type {?} */
    PdfViewerComponent.prototype.isPanelDisabled;
    /** @type {?} */
    PdfViewerComponent.prototype.showThumbnails;
    /** @type {?} */
    PdfViewerComponent.prototype.pdfThumbnailsContext;
    /** @type {?} */
    PdfViewerComponent.prototype.randomPdfId;
    /**
     * @type {?}
     * @private
     */
    PdfViewerComponent.prototype.dialog;
    /**
     * @type {?}
     * @private
     */
    PdfViewerComponent.prototype.renderingQueueServices;
    /**
     * @type {?}
     * @private
     */
    PdfViewerComponent.prototype.logService;
    /**
     * @type {?}
     * @private
     */
    PdfViewerComponent.prototype.appConfigService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicGRmVmlld2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInZpZXdlci9jb21wb25lbnRzL3BkZlZpZXdlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUNILFNBQVMsRUFDVCxXQUFXLEVBQ1gsWUFBWSxFQUNaLE1BQU0sRUFDTixLQUFLLEVBR0wsaUJBQWlCLEVBQ2pCLFlBQVksRUFFZixNQUFNLGVBQWUsQ0FBQztBQUN2QixPQUFPLEVBQUUsU0FBUyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDOUMsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLDRCQUE0QixDQUFDO0FBQ3hELE9BQU8sRUFBRSxzQkFBc0IsRUFBRSxNQUFNLHNDQUFzQyxDQUFDO0FBQzlFLE9BQU8sRUFBRSwwQkFBMEIsRUFBRSxNQUFNLDZCQUE2QixDQUFDO0FBQ3pFLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDOzs7O0FBS3pFLHdDQUlDOzs7SUFIRyxpQ0FBYTs7SUFDYixrQ0FBVzs7SUFDWCw2Q0FBMEI7O0FBRzlCO0lBaUVJLDRCQUNZLE1BQWlCLEVBQ2pCLHNCQUE4QyxFQUM5QyxVQUFzQixFQUN0QixnQkFBa0M7UUFIbEMsV0FBTSxHQUFOLE1BQU0sQ0FBVztRQUNqQiwyQkFBc0IsR0FBdEIsc0JBQXNCLENBQXdCO1FBQzlDLGVBQVUsR0FBVixVQUFVLENBQVk7UUFDdEIscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQTlDOUMsZ0JBQVcsR0FBWSxJQUFJLENBQUM7UUFHNUIsb0JBQWUsR0FBRyxLQUFLLENBQUM7UUFHeEIsdUJBQWtCLEdBQXFCLElBQUksQ0FBQztRQUc1QyxhQUFRLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUduQyxVQUFLLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQUdoQyxVQUFLLEdBQUcsSUFBSSxZQUFZLEVBQU8sQ0FBQztRQVVoQyxxQkFBZ0IsR0FBVyxNQUFNLENBQUM7UUFDbEMsaUJBQVksR0FBVyxDQUFDLENBQUM7UUFFekIsbUJBQWMsR0FBVyxJQUFJLENBQUM7UUFDOUIsd0JBQW1CLEdBQVcsR0FBRyxDQUFDO1FBQ2xDLGNBQVMsR0FBVyxJQUFJLENBQUM7UUFDekIsY0FBUyxHQUFXLElBQUksQ0FBQztRQUV6QixvQkFBZSxHQUFHLElBQUksQ0FBQztRQUN2QixtQkFBYyxHQUFZLEtBQUssQ0FBQztRQUNoQyx5QkFBb0IsR0FBb0IsRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLENBQUM7UUFZckQsb0NBQW9DO1FBQ3BDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLFlBQVksQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDakQsSUFBSSxDQUFDLGFBQWEsR0FBRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUNuRCxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3JELElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLFlBQVksRUFBRSxDQUFDO1FBQ3ZDLElBQUksQ0FBQyxZQUFZLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQzlDLENBQUM7SUFmRCxzQkFBSSxnREFBZ0I7Ozs7UUFBcEI7WUFDSSxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLFlBQVksR0FBRyxHQUFHLENBQUMsR0FBRyxHQUFHLENBQUM7UUFDckQsQ0FBQzs7O09BQUE7Ozs7SUFlRCwyQ0FBYzs7O0lBQWQ7O1lBQ1UsV0FBVyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQVMsK0JBQStCLEVBQUUsU0FBUyxDQUFDLEdBQUcsR0FBRztRQUN2RyxJQUFJLFdBQVcsRUFBRTtZQUNiLE9BQU8sSUFBSSxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztTQUN4QzthQUFNO1lBQ0gsT0FBTyxDQUFDLENBQUM7U0FDWjtJQUNMLENBQUM7Ozs7O0lBRUQsd0NBQVc7Ozs7SUFBWCxVQUFZLFdBQW1CO1FBQzNCLElBQUksV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDOUIsT0FBTyxJQUFJLENBQUMsU0FBUyxDQUFDO1NBQ3pCO2FBQU0sSUFBSSxXQUFXLEdBQUcsSUFBSSxDQUFDLFNBQVMsRUFBRTtZQUNyQyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUM7U0FDekI7YUFBTTtZQUNILE9BQU8sV0FBVyxDQUFDO1NBQ3RCO0lBQ0wsQ0FBQzs7Ozs7SUFFRCx3Q0FBVzs7OztJQUFYLFVBQVksT0FBc0I7UUFBbEMsaUJBMkJDOztZQTFCUyxRQUFRLEdBQUcsT0FBTyxDQUFDLFVBQVUsQ0FBQztRQUVwQyxJQUFJLFFBQVEsSUFBSSxRQUFRLENBQUMsWUFBWSxFQUFFOztnQkFDN0IsUUFBTSxHQUFHLElBQUksVUFBVSxFQUFFO1lBQy9CLFFBQU0sQ0FBQyxNQUFNOzs7WUFBRzs7b0JBQ04sT0FBTyxHQUFHO29CQUNaLElBQUksRUFBRSxRQUFNLENBQUMsTUFBTTtvQkFDbkIsZUFBZSxFQUFFLEtBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQVUsc0JBQXNCLEVBQUUsU0FBUyxDQUFDO2lCQUN6RjtnQkFDRCxLQUFJLENBQUMsVUFBVSxDQUFDLE9BQU8sQ0FBQyxDQUFDO1lBQzdCLENBQUMsQ0FBQSxDQUFDO1lBQ0YsUUFBTSxDQUFDLGlCQUFpQixDQUFDLFFBQVEsQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUNuRDs7WUFFSyxPQUFPLEdBQUcsT0FBTyxDQUFDLFNBQVMsQ0FBQztRQUNsQyxJQUFJLE9BQU8sSUFBSSxPQUFPLENBQUMsWUFBWSxFQUFFOztnQkFDM0IsT0FBTyxHQUFHO2dCQUNaLEdBQUcsRUFBRSxPQUFPLENBQUMsWUFBWTtnQkFDekIsZUFBZSxFQUFFLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxHQUFHLENBQVUsc0JBQXNCLEVBQUUsU0FBUyxDQUFDO2FBQ3pGO1lBQ0QsSUFBSSxDQUFDLFVBQVUsQ0FBQyxPQUFPLENBQUMsQ0FBQztTQUM1QjtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQyxNQUFNLElBQUksS0FBSyxDQUFDLDJDQUEyQyxDQUFDLENBQUM7U0FDaEU7SUFDTCxDQUFDOzs7OztJQUVELHVDQUFVOzs7O0lBQVYsVUFBVyxVQUE4QjtRQUF6QyxpQkE4QkM7UUE3QkcsUUFBUSxDQUFDLG1CQUFtQixDQUFDLFNBQVMsR0FBRyxtQkFBbUIsQ0FBQztRQUU3RCxJQUFJLENBQUMsV0FBVyxHQUFHLFFBQVEsQ0FBQyxXQUFXLENBQUMsVUFBVSxDQUFDLENBQUM7UUFFcEQsSUFBSSxDQUFDLFdBQVcsQ0FBQyxVQUFVOzs7OztRQUFHLFVBQUMsUUFBUSxFQUFFLE1BQU07WUFDM0MsS0FBSSxDQUFDLGFBQWEsQ0FBQyxRQUFRLEVBQUUsTUFBTSxDQUFDLENBQUM7UUFDekMsQ0FBQyxDQUFBLENBQUM7UUFFRixJQUFJLENBQUMsV0FBVyxDQUFDLFVBQVU7Ozs7UUFBRyxVQUFDLFlBQVk7O2dCQUNqQyxLQUFLLEdBQUcsWUFBWSxDQUFDLE1BQU0sR0FBRyxZQUFZLENBQUMsS0FBSztZQUN0RCxLQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsS0FBSyxHQUFHLEdBQUcsQ0FBQyxDQUFDO1FBQ2xELENBQUMsQ0FBQSxDQUFDO1FBRUYsSUFBSSxDQUFDLFdBQVcsQ0FBQyxJQUFJOzs7O1FBQUMsVUFBQyxXQUFXO1lBQzlCLEtBQUksQ0FBQyxrQkFBa0IsR0FBRyxXQUFXLENBQUM7WUFDdEMsS0FBSSxDQUFDLFVBQVUsR0FBRyxXQUFXLENBQUMsUUFBUSxDQUFDO1lBQ3ZDLEtBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxDQUFDO1lBQ2QsS0FBSSxDQUFDLFdBQVcsR0FBRyxDQUFDLENBQUM7WUFDckIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxLQUFJLENBQUMsa0JBQWtCLENBQUMsQ0FBQztZQUU1QyxLQUFJLENBQUMsa0JBQWtCLENBQUMsT0FBTyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUk7OztZQUFDO2dCQUNwQyxLQUFJLENBQUMsU0FBUyxDQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQzNCLENBQUM7OztZQUFFO2dCQUNDLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7WUFDdEIsQ0FBQyxFQUFDLENBQUM7UUFFUCxDQUFDOzs7UUFBRTtZQUNDLEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDdEIsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOzs7OztJQUVELDBDQUFhOzs7O0lBQWIsVUFBYyxXQUFnQjs7WUFDcEIsTUFBTSxHQUFRLFFBQVEsQ0FBQyxjQUFjLENBQUksSUFBSSxDQUFDLFdBQVcsc0JBQW1CLENBQUM7O1lBQzdFLFNBQVMsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFJLElBQUksQ0FBQyxXQUFXLHVCQUFvQixDQUFDO1FBRWxGLElBQUksTUFBTSxJQUFJLFNBQVMsRUFBRTtZQUNyQixJQUFJLENBQUMsaUJBQWlCLEdBQUcsU0FBUyxDQUFDO1lBRW5DLHVCQUF1QjtZQUN2QixJQUFJLENBQUMsaUJBQWlCLENBQUMsZ0JBQWdCLENBQUMsWUFBWSxFQUFFLElBQUksQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFDL0UsdUJBQXVCO1lBQ3ZCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxnQkFBZ0IsQ0FBQyxhQUFhLEVBQUUsSUFBSSxDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNqRix1QkFBdUI7WUFDdkIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLGdCQUFnQixDQUFDLG1CQUFtQixFQUFFLElBQUksQ0FBQyxjQUFjLEVBQUUsSUFBSSxDQUFDLENBQUM7WUFFeEYsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLFdBQVcsQ0FBQyxTQUFTLENBQUM7Z0JBQ3ZDLFNBQVMsRUFBRSxJQUFJLENBQUMsaUJBQWlCO2dCQUNqQyxNQUFNLEVBQUUsTUFBTTtnQkFDZCxjQUFjLEVBQUUsSUFBSSxDQUFDLHNCQUFzQjthQUM5QyxDQUFDLENBQUM7WUFFSCxJQUFJLENBQUMsc0JBQXNCLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsQ0FBQztZQUN0RCxJQUFJLENBQUMsU0FBUyxDQUFDLFdBQVcsQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUN4QyxJQUFJLENBQUMsb0JBQW9CLENBQUMsTUFBTSxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUM7U0FDckQ7SUFDTCxDQUFDOzs7O0lBRUQsd0NBQVc7OztJQUFYO1FBQ0ksSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDeEIsdUJBQXVCO1lBQ3ZCLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxtQkFBbUIsQ0FBQyxZQUFZLEVBQUUsSUFBSSxDQUFDLFlBQVksRUFBRSxJQUFJLENBQUMsQ0FBQztZQUNsRix1QkFBdUI7WUFDdkIsSUFBSSxDQUFDLGlCQUFpQixDQUFDLG1CQUFtQixDQUFDLGFBQWEsRUFBRSxJQUFJLENBQUMsYUFBYSxFQUFFLElBQUksQ0FBQyxDQUFDO1lBQ3BGLHVCQUF1QjtZQUN2QixJQUFJLENBQUMsaUJBQWlCLENBQUMsbUJBQW1CLENBQUMsbUJBQW1CLEVBQUUsSUFBSSxDQUFDLGNBQWMsRUFBRSxJQUFJLENBQUMsQ0FBQztTQUM5RjtRQUVELElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTtZQUNsQixJQUFJO2dCQUNBLElBQUksQ0FBQyxXQUFXLENBQUMsT0FBTyxFQUFFLENBQUM7YUFDOUI7WUFBQyxXQUFNO2FBQ1A7WUFFRCxJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztTQUMzQjtJQUNMLENBQUM7Ozs7SUFFRCw2Q0FBZ0I7OztJQUFoQjtRQUNJLElBQUksQ0FBQyxjQUFjLEdBQUcsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDO0lBQy9DLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsc0NBQVM7Ozs7OztJQUFULFVBQVUsU0FBUztRQUNmLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxTQUFTLENBQUM7O1lBRTVCLGVBQWUsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFJLElBQUksQ0FBQyxXQUFXLDJCQUF3QixDQUFDOztZQUN0RixpQkFBaUIsR0FBRyxRQUFRLENBQUMsY0FBYyxDQUFJLElBQUksQ0FBQyxXQUFXLHVCQUFvQixDQUFDO1FBRTFGLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxpQkFBaUIsRUFBRTs7Z0JBRWpDLGNBQWMsU0FBQTs7Z0JBQ2QsZUFBZSxTQUFBO1lBRW5CLElBQUksZUFBZSxJQUFJLGVBQWUsQ0FBQyxXQUFXLElBQUksaUJBQWlCLENBQUMsV0FBVyxFQUFFO2dCQUNqRixjQUFjLEdBQUcsZUFBZSxDQUFDLFdBQVcsQ0FBQztnQkFDN0MsZUFBZSxHQUFHLGVBQWUsQ0FBQyxZQUFZLENBQUM7YUFDbEQ7aUJBQU07Z0JBQ0gsY0FBYyxHQUFHLGlCQUFpQixDQUFDLFdBQVcsQ0FBQztnQkFDL0MsZUFBZSxHQUFHLGlCQUFpQixDQUFDLFlBQVksQ0FBQzthQUNwRDs7Z0JBRUssV0FBVyxHQUFHLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLElBQUksQ0FBQyxTQUFTLENBQUMsa0JBQWtCLEdBQUcsQ0FBQyxDQUFDOztnQkFFMUUsT0FBTyxHQUFHLEVBQUU7O2dCQUNaLGNBQWMsR0FBRyxDQUFDLGNBQWMsR0FBRyxPQUFPLENBQUMsR0FBRyxXQUFXLENBQUMsS0FBSyxHQUFHLFdBQVcsQ0FBQyxLQUFLOztnQkFDbkYsZUFBZSxHQUFHLENBQUMsZUFBZSxHQUFHLE9BQU8sQ0FBQyxHQUFHLFdBQVcsQ0FBQyxLQUFLLEdBQUcsV0FBVyxDQUFDLEtBQUs7O2dCQUV2RixLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWMsRUFBRTtZQUNqQyxJQUFJLENBQUMsS0FBSyxFQUFFO2dCQUNSLFFBQVEsSUFBSSxDQUFDLGdCQUFnQixFQUFFO29CQUMzQixLQUFLLGFBQWE7d0JBQ2QsS0FBSyxHQUFHLENBQUMsQ0FBQzt3QkFDVixNQUFNO29CQUNWLEtBQUssWUFBWTt3QkFDYixLQUFLLEdBQUcsY0FBYyxDQUFDO3dCQUN2QixNQUFNO29CQUNWLEtBQUssYUFBYTt3QkFDZCxLQUFLLEdBQUcsZUFBZSxDQUFDO3dCQUN4QixNQUFNO29CQUNWLEtBQUssVUFBVTt3QkFDWCxLQUFLLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxjQUFjLEVBQUUsZUFBZSxDQUFDLENBQUM7d0JBQ2xELE1BQU07b0JBQ1YsS0FBSyxNQUFNOzs0QkFDSCxlQUFlLFNBQUE7d0JBQ25CLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRTs0QkFDbEIsZUFBZSxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsZUFBZSxFQUFFLGNBQWMsQ0FBQyxDQUFDO3lCQUMvRDs2QkFBTTs0QkFDSCxlQUFlLEdBQUcsY0FBYyxDQUFDO3lCQUNwQzt3QkFDRCxlQUFlLEdBQUcsSUFBSSxDQUFDLEtBQUssQ0FBQyxlQUFlLENBQUMsQ0FBQzt3QkFDOUMsS0FBSyxHQUFHLElBQUksQ0FBQyxHQUFHLENBQUMsSUFBSSxDQUFDLGNBQWMsRUFBRSxlQUFlLENBQUMsQ0FBQzt3QkFFdkQsTUFBTTtvQkFDVjt3QkFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsR0FBRyxTQUFTLEdBQUcsOEJBQThCLENBQUMsQ0FBQzt3QkFDMUYsT0FBTztpQkFDZDtnQkFFRCxJQUFJLENBQUMsbUJBQW1CLENBQUMsS0FBSyxDQUFDLENBQUM7YUFDbkM7aUJBQU07Z0JBQ0gsSUFBSSxDQUFDLFlBQVksR0FBRyxDQUFDLENBQUM7Z0JBQ3RCLElBQUksQ0FBQyxtQkFBbUIsQ0FBQyxLQUFLLENBQUMsQ0FBQzthQUNuQztTQUNKO0lBQ0wsQ0FBQztJQUVEOzs7O09BSUc7Ozs7Ozs7SUFDSCxnREFBbUI7Ozs7OztJQUFuQixVQUFvQixRQUFnQjtRQUNoQyxJQUFJLElBQUksQ0FBQyxTQUFTLEVBQUU7WUFDaEIsSUFBSSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsSUFBSSxDQUFDLFlBQVksRUFBRSxRQUFRLENBQUMsRUFBRTtnQkFDaEQsSUFBSSxDQUFDLFlBQVksR0FBRyxRQUFRLENBQUM7Z0JBRTdCLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxDQUFDLE9BQU87Ozs7Z0JBQUMsVUFBVSxXQUFXO29CQUMvQyxXQUFXLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2dCQUNqQyxDQUFDLEVBQUMsQ0FBQzthQUNOO1lBRUQsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLEVBQUUsQ0FBQztTQUMzQjtJQUNMLENBQUM7SUFFRDs7Ozs7O09BTUc7Ozs7Ozs7OztJQUNILHdDQUFXOzs7Ozs7OztJQUFYLFVBQVksUUFBZ0IsRUFBRSxRQUFnQjtRQUMxQyxPQUFPLENBQUMsUUFBUSxLQUFLLFFBQVEsQ0FBQyxDQUFDO0lBQ25DLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7Ozs7SUFDSCx3Q0FBVzs7Ozs7OztJQUFYLFVBQVksS0FBYSxFQUFFLE1BQWM7UUFDckMsT0FBTyxDQUFDLEtBQUssR0FBRyxNQUFNLENBQUMsQ0FBQztJQUM1QixDQUFDO0lBRUQ7O09BRUc7Ozs7O0lBQ0gscUNBQVE7Ozs7SUFBUjtRQUNJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLGdCQUFnQixDQUFDLENBQUM7SUFDMUMsQ0FBQztJQUVEOztPQUVHOzs7OztJQUNILG9DQUFPOzs7O0lBQVA7UUFDSSxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsS0FBSyxVQUFVLEVBQUU7WUFDdEMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxVQUFVLENBQUMsQ0FBQztTQUM5QjthQUFNO1lBQ0gsSUFBSSxDQUFDLFNBQVMsQ0FBQyxNQUFNLENBQUMsQ0FBQztTQUMxQjtJQUNMLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsbUNBQU07Ozs7OztJQUFOLFVBQU8sS0FBYzs7WUFDYixRQUFRLEdBQVEsSUFBSSxDQUFDLFlBQVk7UUFDckMsR0FBRztZQUNDLFFBQVEsR0FBRyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDNUQsUUFBUSxHQUFHLElBQUksQ0FBQyxJQUFJLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUN6QyxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQ2pELFFBQVEsRUFBRSxLQUFLLEdBQUcsQ0FBQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxFQUFFO1FBQ25ELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLENBQUM7UUFDL0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsb0NBQU87Ozs7OztJQUFQLFVBQVEsS0FBYzs7WUFDZCxRQUFRLEdBQVEsSUFBSSxDQUFDLFlBQVk7UUFDckMsR0FBRztZQUNDLFFBQVEsR0FBRyxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsQ0FBQyxPQUFPLENBQUMsQ0FBQyxDQUFDLENBQUM7WUFDNUQsUUFBUSxHQUFHLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxHQUFHLEVBQUUsQ0FBQyxHQUFHLEVBQUUsQ0FBQztZQUMxQyxRQUFRLEdBQUcsSUFBSSxDQUFDLEdBQUcsQ0FBQyxJQUFJLENBQUMsU0FBUyxFQUFFLFFBQVEsQ0FBQyxDQUFDO1NBQ2pELFFBQVEsRUFBRSxLQUFLLEdBQUcsQ0FBQyxJQUFJLFFBQVEsR0FBRyxJQUFJLENBQUMsU0FBUyxFQUFFO1FBQ25ELElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxNQUFNLENBQUM7UUFDL0IsSUFBSSxDQUFDLG1CQUFtQixDQUFDLFFBQVEsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7SUFFRDs7T0FFRzs7Ozs7SUFDSCx5Q0FBWTs7OztJQUFaO1FBQ0ksSUFBSSxJQUFJLENBQUMsU0FBUyxJQUFJLElBQUksQ0FBQyxJQUFJLEdBQUcsQ0FBQyxFQUFFO1lBQ2pDLElBQUksQ0FBQyxJQUFJLEVBQUUsQ0FBQztZQUNaLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztZQUU3QixJQUFJLENBQUMsU0FBUyxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDaEQ7SUFDTCxDQUFDO0lBRUQ7O09BRUc7Ozs7O0lBQ0gscUNBQVE7Ozs7SUFBUjtRQUNJLElBQUksSUFBSSxDQUFDLFNBQVMsSUFBSSxJQUFJLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDL0MsSUFBSSxDQUFDLElBQUksRUFBRSxDQUFDO1lBQ1osSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUMsSUFBSSxDQUFDO1lBRTdCLElBQUksQ0FBQyxTQUFTLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztTQUNoRDtJQUNMLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsc0NBQVM7Ozs7OztJQUFULFVBQVUsSUFBWTs7WUFDWixTQUFTLEdBQUcsUUFBUSxDQUFDLElBQUksRUFBRSxFQUFFLENBQUM7UUFFcEMsSUFBSSxDQUFDLEtBQUssQ0FBQyxTQUFTLENBQUMsSUFBSSxTQUFTLEdBQUcsQ0FBQyxJQUFJLFNBQVMsSUFBSSxJQUFJLENBQUMsVUFBVSxFQUFFO1lBQ3BFLElBQUksQ0FBQyxJQUFJLEdBQUcsU0FBUyxDQUFDO1lBQ3RCLElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztZQUM3QixJQUFJLENBQUMsU0FBUyxDQUFDLGlCQUFpQixHQUFHLElBQUksQ0FBQyxJQUFJLENBQUM7U0FDaEQ7YUFBTTtZQUNILElBQUksQ0FBQyxXQUFXLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztTQUNoQztJQUNMLENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gseUNBQVk7Ozs7OztJQUFaLFVBQWEsS0FBSztRQUNkLElBQUksQ0FBQyxJQUFJLEdBQUcsS0FBSyxDQUFDLFVBQVUsQ0FBQztRQUM3QixJQUFJLENBQUMsV0FBVyxHQUFHLEtBQUssQ0FBQyxVQUFVLENBQUM7SUFDeEMsQ0FBQzs7Ozs7O0lBRUQsMENBQWE7Ozs7O0lBQWIsVUFBYyxRQUFRLEVBQUUsTUFBTTtRQUE5QixpQkFhQztRQVpHLElBQUksQ0FBQyxNQUFNO2FBQ04sSUFBSSxDQUFDLDBCQUEwQixFQUFFO1lBQzlCLEtBQUssRUFBRSxPQUFPO1lBQ2QsSUFBSSxFQUFFLEVBQUUsTUFBTSxRQUFBLEVBQUU7U0FDbkIsQ0FBQzthQUNELFdBQVcsRUFBRSxDQUFDLFNBQVM7Ozs7UUFBQyxVQUFDLFFBQVE7WUFDOUIsSUFBSSxRQUFRLEVBQUU7Z0JBQ1YsUUFBUSxDQUFDLFFBQVEsQ0FBQyxDQUFDO2FBQ3RCO2lCQUFNO2dCQUNILEtBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxFQUFFLENBQUM7YUFDckI7UUFDVCxDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7SUFFRDs7T0FFRzs7Ozs7SUFDSCwyQ0FBYzs7OztJQUFkO1FBQ0ksSUFBSSxDQUFDLFFBQVEsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUN6QixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7OztJQUNILDBDQUFhOzs7Ozs7SUFBYixVQUFjLEtBQUs7UUFDZixJQUFJLENBQUMsZUFBZSxHQUFHLEtBQUssQ0FBQztJQUNqQyxDQUFDO0lBRUQ7OztPQUdHOzs7Ozs7SUFFSCxnREFBbUI7Ozs7O0lBRG5CLFVBQ29CLEtBQW9COztZQUM5QixHQUFHLEdBQUcsS0FBSyxDQUFDLE9BQU87UUFDekIsSUFBSSxHQUFHLEtBQUssRUFBRSxFQUFFLEVBQUUsY0FBYztZQUM1QixJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7U0FDbkI7YUFBTSxJQUFJLEdBQUcsS0FBSyxFQUFFLEVBQUUsRUFBQyxhQUFhO1lBQ2pDLElBQUksQ0FBQyxZQUFZLEVBQUUsQ0FBQztTQUN2QjtJQUNMLENBQUM7Ozs7O0lBRU8seUNBQVk7Ozs7SUFBcEI7UUFDSSxPQUFPLHNDQUFzQyxDQUFDLE9BQU8sQ0FBQyxPQUFPOzs7O1FBQUUsVUFBVSxDQUFDOztnQkFDaEUsQ0FBQyxHQUFHLElBQUksQ0FBQyxNQUFNLEVBQUUsR0FBRyxFQUFFLEdBQUcsQ0FBQzs7Z0JBQUUsQ0FBQyxHQUFHLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLEdBQUcsR0FBRyxHQUFHLEdBQUcsQ0FBQztZQUNyRSxPQUFPLENBQUMsQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7UUFDMUIsQ0FBQyxFQUFDLENBQUM7SUFDUCxDQUFDOztnQkFwZEosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxnQkFBZ0I7b0JBQzFCLHFvSkFBeUM7b0JBS3pDLFNBQVMsRUFBRSxDQUFDLHNCQUFzQixDQUFDO29CQUNuQyxJQUFJLEVBQUUsRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUU7b0JBQ25DLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOztpQkFDeEM7Ozs7Z0JBekJRLFNBQVM7Z0JBRVQsc0JBQXNCO2dCQUR0QixVQUFVO2dCQUdWLGdCQUFnQjs7OzBCQXdCcEIsS0FBSzsyQkFHTCxLQUFLOzJCQUdMLEtBQUs7OEJBR0wsS0FBSztrQ0FHTCxLQUFLO3FDQUdMLEtBQUs7MkJBR0wsTUFBTTt3QkFHTixNQUFNO3dCQUdOLE1BQU07c0NBZ2FOLFlBQVksU0FBQyxrQkFBa0IsRUFBRSxDQUFDLFFBQVEsQ0FBQzs7SUFnQmhELHlCQUFDO0NBQUEsQUFyZEQsSUFxZEM7U0ExY1ksa0JBQWtCOzs7SUFFM0IscUNBQ2dCOztJQUVoQixzQ0FDZTs7SUFFZixzQ0FDaUI7O0lBRWpCLHlDQUM0Qjs7SUFFNUIsNkNBQ3dCOztJQUV4QixnREFDNEM7O0lBRTVDLHNDQUNtQzs7SUFFbkMsbUNBQ2dDOztJQUVoQyxtQ0FDZ0M7O0lBRWhDLHlDQUFpQjs7SUFDakIsZ0RBQXdCOztJQUN4QixrQ0FBYTs7SUFDYix5Q0FBb0I7O0lBQ3BCLHdDQUFtQjs7SUFDbkIsNENBQXVCOztJQUN2Qix1Q0FBZTs7SUFDZiwrQ0FBdUI7O0lBQ3ZCLDhDQUFrQzs7SUFDbEMsMENBQXlCOztJQUV6Qiw0Q0FBOEI7O0lBQzlCLGlEQUFrQzs7SUFDbEMsdUNBQXlCOztJQUN6Qix1Q0FBeUI7O0lBRXpCLDZDQUF1Qjs7SUFDdkIsNENBQWdDOztJQUNoQyxrREFBeUQ7O0lBQ3pELHlDQUFvQjs7Ozs7SUFPaEIsb0NBQXlCOzs7OztJQUN6QixvREFBc0Q7Ozs7O0lBQ3RELHdDQUE4Qjs7Ozs7SUFDOUIsOENBQTBDIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXHJcbmltcG9ydCB7XHJcbiAgICBDb21wb25lbnQsXHJcbiAgICBUZW1wbGF0ZVJlZixcclxuICAgIEhvc3RMaXN0ZW5lcixcclxuICAgIE91dHB1dCxcclxuICAgIElucHV0LFxyXG4gICAgT25DaGFuZ2VzLFxyXG4gICAgT25EZXN0cm95LFxyXG4gICAgVmlld0VuY2Fwc3VsYXRpb24sXHJcbiAgICBFdmVudEVtaXR0ZXIsXHJcbiAgICBTaW1wbGVDaGFuZ2VzXHJcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IE1hdERpYWxvZyB9IGZyb20gJ0Bhbmd1bGFyL21hdGVyaWFsJztcclxuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2xvZy5zZXJ2aWNlJztcclxuaW1wb3J0IHsgUmVuZGVyaW5nUXVldWVTZXJ2aWNlcyB9IGZyb20gJy4uL3NlcnZpY2VzL3JlbmRlcmluZy1xdWV1ZS5zZXJ2aWNlcyc7XHJcbmltcG9ydCB7IFBkZlBhc3N3b3JkRGlhbG9nQ29tcG9uZW50IH0gZnJvbSAnLi9wZGZWaWV3ZXItcGFzc3dvcmQtZGlhbG9nJztcclxuaW1wb3J0IHsgQXBwQ29uZmlnU2VydmljZSB9IGZyb20gJy4vLi4vLi4vYXBwLWNvbmZpZy9hcHAtY29uZmlnLnNlcnZpY2UnO1xyXG5cclxuZGVjbGFyZSBjb25zdCBwZGZqc0xpYjogYW55O1xyXG5kZWNsYXJlIGNvbnN0IHBkZmpzVmlld2VyOiBhbnk7XHJcblxyXG5leHBvcnQgaW50ZXJmYWNlIFBkZkRvY3VtZW50T3B0aW9ucyB7XHJcbiAgICB1cmw/OiBzdHJpbmc7XHJcbiAgICBkYXRhPzogYW55O1xyXG4gICAgd2l0aENyZWRlbnRpYWxzPzogYm9vbGVhbjtcclxufVxyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi1wZGYtdmlld2VyJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi9wZGZWaWV3ZXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbXHJcbiAgICAgICAgJy4vcGRmVmlld2VyLmNvbXBvbmVudC5zY3NzJyxcclxuICAgICAgICAnLi9wZGZWaWV3ZXJIb3N0LmNvbXBvbmVudC5zY3NzJ1xyXG4gICAgXSxcclxuICAgIHByb3ZpZGVyczogW1JlbmRlcmluZ1F1ZXVlU2VydmljZXNdLFxyXG4gICAgaG9zdDogeyAnY2xhc3MnOiAnYWRmLXBkZi12aWV3ZXInIH0sXHJcbiAgICBlbmNhcHN1bGF0aW9uOiBWaWV3RW5jYXBzdWxhdGlvbi5Ob25lXHJcbn0pXHJcbmV4cG9ydCBjbGFzcyBQZGZWaWV3ZXJDb21wb25lbnQgaW1wbGVtZW50cyBPbkNoYW5nZXMsIE9uRGVzdHJveSB7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHVybEZpbGU6IHN0cmluZztcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgYmxvYkZpbGU6IEJsb2I7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIG5hbWVGaWxlOiBzdHJpbmc7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHNob3dUb29sYmFyOiBib29sZWFuID0gdHJ1ZTtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgYWxsb3dUaHVtYm5haWxzID0gZmFsc2U7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHRodW1ibmFpbHNUZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PiA9IG51bGw7XHJcblxyXG4gICAgQE91dHB1dCgpXHJcbiAgICByZW5kZXJlZCA9IG5ldyBFdmVudEVtaXR0ZXI8YW55PigpO1xyXG5cclxuICAgIEBPdXRwdXQoKVxyXG4gICAgZXJyb3IgPSBuZXcgRXZlbnRFbWl0dGVyPGFueT4oKTtcclxuXHJcbiAgICBAT3V0cHV0KClcclxuICAgIGNsb3NlID0gbmV3IEV2ZW50RW1pdHRlcjxhbnk+KCk7XHJcblxyXG4gICAgbG9hZGluZ1Rhc2s6IGFueTtcclxuICAgIGN1cnJlbnRQZGZEb2N1bWVudDogYW55O1xyXG4gICAgcGFnZTogbnVtYmVyO1xyXG4gICAgZGlzcGxheVBhZ2U6IG51bWJlcjtcclxuICAgIHRvdGFsUGFnZXM6IG51bWJlcjtcclxuICAgIGxvYWRpbmdQZXJjZW50OiBudW1iZXI7XHJcbiAgICBwZGZWaWV3ZXI6IGFueTtcclxuICAgIGRvY3VtZW50Q29udGFpbmVyOiBhbnk7XHJcbiAgICBjdXJyZW50U2NhbGVNb2RlOiBzdHJpbmcgPSAnYXV0byc7XHJcbiAgICBjdXJyZW50U2NhbGU6IG51bWJlciA9IDE7XHJcblxyXG4gICAgTUFYX0FVVE9fU0NBTEU6IG51bWJlciA9IDEuMjU7XHJcbiAgICBERUZBVUxUX1NDQUxFX0RFTFRBOiBudW1iZXIgPSAxLjE7XHJcbiAgICBNSU5fU0NBTEU6IG51bWJlciA9IDAuMjU7XHJcbiAgICBNQVhfU0NBTEU6IG51bWJlciA9IDEwLjA7XHJcblxyXG4gICAgaXNQYW5lbERpc2FibGVkID0gdHJ1ZTtcclxuICAgIHNob3dUaHVtYm5haWxzOiBib29sZWFuID0gZmFsc2U7XHJcbiAgICBwZGZUaHVtYm5haWxzQ29udGV4dDogeyB2aWV3ZXI6IGFueSB9ID0geyB2aWV3ZXI6IG51bGwgfTtcclxuICAgIHJhbmRvbVBkZklkOiBzdHJpbmc7XHJcblxyXG4gICAgZ2V0IGN1cnJlbnRTY2FsZVRleHQoKTogc3RyaW5nIHtcclxuICAgICAgICByZXR1cm4gTWF0aC5yb3VuZCh0aGlzLmN1cnJlbnRTY2FsZSAqIDEwMCkgKyAnJSc7XHJcbiAgICB9XHJcblxyXG4gICAgY29uc3RydWN0b3IoXHJcbiAgICAgICAgcHJpdmF0ZSBkaWFsb2c6IE1hdERpYWxvZyxcclxuICAgICAgICBwcml2YXRlIHJlbmRlcmluZ1F1ZXVlU2VydmljZXM6IFJlbmRlcmluZ1F1ZXVlU2VydmljZXMsXHJcbiAgICAgICAgcHJpdmF0ZSBsb2dTZXJ2aWNlOiBMb2dTZXJ2aWNlLFxyXG4gICAgICAgIHByaXZhdGUgYXBwQ29uZmlnU2VydmljZTogQXBwQ29uZmlnU2VydmljZSkge1xyXG4gICAgICAgIC8vIG5lZWRlZCB0byBwcmVzZXJ2ZSBcInRoaXNcIiBjb250ZXh0XHJcbiAgICAgICAgdGhpcy5vblBhZ2VDaGFuZ2UgPSB0aGlzLm9uUGFnZUNoYW5nZS5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMub25QYWdlc0xvYWRlZCA9IHRoaXMub25QYWdlc0xvYWRlZC5iaW5kKHRoaXMpO1xyXG4gICAgICAgIHRoaXMub25QYWdlUmVuZGVyZWQgPSB0aGlzLm9uUGFnZVJlbmRlcmVkLmJpbmQodGhpcyk7XHJcbiAgICAgICAgdGhpcy5yYW5kb21QZGZJZCA9IHRoaXMuZ2VuZXJhdGVVdWlkKCk7XHJcbiAgICAgICAgdGhpcy5jdXJyZW50U2NhbGUgPSB0aGlzLmdldFVzZXJTY2FsaW5nKCk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0VXNlclNjYWxpbmcoKTogbnVtYmVyIHtcclxuICAgICAgICBjb25zdCBzY2FsZUNvbmZpZyA9IHRoaXMuYXBwQ29uZmlnU2VydmljZS5nZXQ8bnVtYmVyPignYWRmLXZpZXdlci5wZGYtdmlld2VyLXNjYWxpbmcnLCB1bmRlZmluZWQpIC8gMTAwO1xyXG4gICAgICAgIGlmIChzY2FsZUNvbmZpZykge1xyXG4gICAgICAgICAgICByZXR1cm4gdGhpcy5jaGVja0xpbWl0cyhzY2FsZUNvbmZpZyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgcmV0dXJuIDE7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGNoZWNrTGltaXRzKHNjYWxlQ29uZmlnOiBudW1iZXIpOiBudW1iZXIge1xyXG4gICAgICAgIGlmIChzY2FsZUNvbmZpZyA+IHRoaXMuTUFYX1NDQUxFKSB7XHJcbiAgICAgICAgICAgIHJldHVybiB0aGlzLk1BWF9TQ0FMRTtcclxuICAgICAgICB9IGVsc2UgaWYgKHNjYWxlQ29uZmlnIDwgdGhpcy5NSU5fU0NBTEUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuTUlOX1NDQUxFO1xyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIHJldHVybiBzY2FsZUNvbmZpZztcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgbmdPbkNoYW5nZXMoY2hhbmdlczogU2ltcGxlQ2hhbmdlcykge1xyXG4gICAgICAgIGNvbnN0IGJsb2JGaWxlID0gY2hhbmdlc1snYmxvYkZpbGUnXTtcclxuXHJcbiAgICAgICAgaWYgKGJsb2JGaWxlICYmIGJsb2JGaWxlLmN1cnJlbnRWYWx1ZSkge1xyXG4gICAgICAgICAgICBjb25zdCByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xyXG4gICAgICAgICAgICByZWFkZXIub25sb2FkID0gKCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3Qgb3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgICAgICAgICBkYXRhOiByZWFkZXIucmVzdWx0LFxyXG4gICAgICAgICAgICAgICAgICAgIHdpdGhDcmVkZW50aWFsczogdGhpcy5hcHBDb25maWdTZXJ2aWNlLmdldDxib29sZWFuPignYXV0aC53aXRoQ3JlZGVudGlhbHMnLCB1bmRlZmluZWQpXHJcbiAgICAgICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICAgICAgdGhpcy5leGVjdXRlUGRmKG9wdGlvbnMpO1xyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICByZWFkZXIucmVhZEFzQXJyYXlCdWZmZXIoYmxvYkZpbGUuY3VycmVudFZhbHVlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHVybEZpbGUgPSBjaGFuZ2VzWyd1cmxGaWxlJ107XHJcbiAgICAgICAgaWYgKHVybEZpbGUgJiYgdXJsRmlsZS5jdXJyZW50VmFsdWUpIHtcclxuICAgICAgICAgICAgY29uc3Qgb3B0aW9ucyA9IHtcclxuICAgICAgICAgICAgICAgIHVybDogdXJsRmlsZS5jdXJyZW50VmFsdWUsXHJcbiAgICAgICAgICAgICAgICB3aXRoQ3JlZGVudGlhbHM6IHRoaXMuYXBwQ29uZmlnU2VydmljZS5nZXQ8Ym9vbGVhbj4oJ2F1dGgud2l0aENyZWRlbnRpYWxzJywgdW5kZWZpbmVkKVxyXG4gICAgICAgICAgICB9O1xyXG4gICAgICAgICAgICB0aGlzLmV4ZWN1dGVQZGYob3B0aW9ucyk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXRoaXMudXJsRmlsZSAmJiAhdGhpcy5ibG9iRmlsZSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0F0dHJpYnV0ZSB1cmxGaWxlIG9yIGJsb2JGaWxlIGlzIHJlcXVpcmVkJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIGV4ZWN1dGVQZGYocGRmT3B0aW9uczogUGRmRG9jdW1lbnRPcHRpb25zKSB7XHJcbiAgICAgICAgcGRmanNMaWIuR2xvYmFsV29ya2VyT3B0aW9ucy53b3JrZXJTcmMgPSAncGRmLndvcmtlci5taW4uanMnO1xyXG5cclxuICAgICAgICB0aGlzLmxvYWRpbmdUYXNrID0gcGRmanNMaWIuZ2V0RG9jdW1lbnQocGRmT3B0aW9ucyk7XHJcblxyXG4gICAgICAgIHRoaXMubG9hZGluZ1Rhc2sub25QYXNzd29yZCA9IChjYWxsYmFjaywgcmVhc29uKSA9PiB7XHJcbiAgICAgICAgICAgIHRoaXMub25QZGZQYXNzd29yZChjYWxsYmFjaywgcmVhc29uKTtcclxuICAgICAgICB9O1xyXG5cclxuICAgICAgICB0aGlzLmxvYWRpbmdUYXNrLm9uUHJvZ3Jlc3MgPSAocHJvZ3Jlc3NEYXRhKSA9PiB7XHJcbiAgICAgICAgICAgIGNvbnN0IGxldmVsID0gcHJvZ3Jlc3NEYXRhLmxvYWRlZCAvIHByb2dyZXNzRGF0YS50b3RhbDtcclxuICAgICAgICAgICAgdGhpcy5sb2FkaW5nUGVyY2VudCA9IE1hdGgucm91bmQobGV2ZWwgKiAxMDApO1xyXG4gICAgICAgIH07XHJcblxyXG4gICAgICAgIHRoaXMubG9hZGluZ1Rhc2sudGhlbigocGRmRG9jdW1lbnQpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50UGRmRG9jdW1lbnQgPSBwZGZEb2N1bWVudDtcclxuICAgICAgICAgICAgdGhpcy50b3RhbFBhZ2VzID0gcGRmRG9jdW1lbnQubnVtUGFnZXM7XHJcbiAgICAgICAgICAgIHRoaXMucGFnZSA9IDE7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheVBhZ2UgPSAxO1xyXG4gICAgICAgICAgICB0aGlzLmluaXRQREZWaWV3ZXIodGhpcy5jdXJyZW50UGRmRG9jdW1lbnQpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5jdXJyZW50UGRmRG9jdW1lbnQuZ2V0UGFnZSgxKS50aGVuKCgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuc2NhbGVQYWdlKCdhdXRvJyk7XHJcbiAgICAgICAgICAgIH0sICgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuZXJyb3IuZW1pdCgpO1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgfSwgKCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmVycm9yLmVtaXQoKTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICBpbml0UERGVmlld2VyKHBkZkRvY3VtZW50OiBhbnkpIHtcclxuICAgICAgICBjb25zdCB2aWV3ZXI6IGFueSA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKGAke3RoaXMucmFuZG9tUGRmSWR9LXZpZXdlci12aWV3ZXJQZGZgKTtcclxuICAgICAgICBjb25zdCBjb250YWluZXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChgJHt0aGlzLnJhbmRvbVBkZklkfS12aWV3ZXItcGRmLXZpZXdlcmApO1xyXG5cclxuICAgICAgICBpZiAodmlld2VyICYmIGNvbnRhaW5lcikge1xyXG4gICAgICAgICAgICB0aGlzLmRvY3VtZW50Q29udGFpbmVyID0gY29udGFpbmVyO1xyXG5cclxuICAgICAgICAgICAgLy8gY3NwZWxsOiBkaXNhYmxlLW5leHRcclxuICAgICAgICAgICAgdGhpcy5kb2N1bWVudENvbnRhaW5lci5hZGRFdmVudExpc3RlbmVyKCdwYWdlY2hhbmdlJywgdGhpcy5vblBhZ2VDaGFuZ2UsIHRydWUpO1xyXG4gICAgICAgICAgICAvLyBjc3BlbGw6IGRpc2FibGUtbmV4dFxyXG4gICAgICAgICAgICB0aGlzLmRvY3VtZW50Q29udGFpbmVyLmFkZEV2ZW50TGlzdGVuZXIoJ3BhZ2VzbG9hZGVkJywgdGhpcy5vblBhZ2VzTG9hZGVkLCB0cnVlKTtcclxuICAgICAgICAgICAgLy8gY3NwZWxsOiBkaXNhYmxlLW5leHRcclxuICAgICAgICAgICAgdGhpcy5kb2N1bWVudENvbnRhaW5lci5hZGRFdmVudExpc3RlbmVyKCd0ZXh0bGF5ZXJyZW5kZXJlZCcsIHRoaXMub25QYWdlUmVuZGVyZWQsIHRydWUpO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5wZGZWaWV3ZXIgPSBuZXcgcGRmanNWaWV3ZXIuUERGVmlld2VyKHtcclxuICAgICAgICAgICAgICAgIGNvbnRhaW5lcjogdGhpcy5kb2N1bWVudENvbnRhaW5lcixcclxuICAgICAgICAgICAgICAgIHZpZXdlcjogdmlld2VyLFxyXG4gICAgICAgICAgICAgICAgcmVuZGVyaW5nUXVldWU6IHRoaXMucmVuZGVyaW5nUXVldWVTZXJ2aWNlc1xyXG4gICAgICAgICAgICB9KTtcclxuXHJcbiAgICAgICAgICAgIHRoaXMucmVuZGVyaW5nUXVldWVTZXJ2aWNlcy5zZXRWaWV3ZXIodGhpcy5wZGZWaWV3ZXIpO1xyXG4gICAgICAgICAgICB0aGlzLnBkZlZpZXdlci5zZXREb2N1bWVudChwZGZEb2N1bWVudCk7XHJcbiAgICAgICAgICAgIHRoaXMucGRmVGh1bWJuYWlsc0NvbnRleHQudmlld2VyID0gdGhpcy5wZGZWaWV3ZXI7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIG5nT25EZXN0cm95KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmRvY3VtZW50Q29udGFpbmVyKSB7XHJcbiAgICAgICAgICAgIC8vIGNzcGVsbDogZGlzYWJsZS1uZXh0XHJcbiAgICAgICAgICAgIHRoaXMuZG9jdW1lbnRDb250YWluZXIucmVtb3ZlRXZlbnRMaXN0ZW5lcigncGFnZWNoYW5nZScsIHRoaXMub25QYWdlQ2hhbmdlLCB0cnVlKTtcclxuICAgICAgICAgICAgLy8gY3NwZWxsOiBkaXNhYmxlLW5leHRcclxuICAgICAgICAgICAgdGhpcy5kb2N1bWVudENvbnRhaW5lci5yZW1vdmVFdmVudExpc3RlbmVyKCdwYWdlc2xvYWRlZCcsIHRoaXMub25QYWdlc0xvYWRlZCwgdHJ1ZSk7XHJcbiAgICAgICAgICAgIC8vIGNzcGVsbDogZGlzYWJsZS1uZXh0XHJcbiAgICAgICAgICAgIHRoaXMuZG9jdW1lbnRDb250YWluZXIucmVtb3ZlRXZlbnRMaXN0ZW5lcigndGV4dGxheWVycmVuZGVyZWQnLCB0aGlzLm9uUGFnZVJlbmRlcmVkLCB0cnVlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmICh0aGlzLmxvYWRpbmdUYXNrKSB7XHJcbiAgICAgICAgICAgIHRyeSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvYWRpbmdUYXNrLmRlc3Ryb3koKTtcclxuICAgICAgICAgICAgfSBjYXRjaCB7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHRoaXMubG9hZGluZ1Rhc2sgPSBudWxsO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICB0b2dnbGVUaHVtYm5haWxzKCkge1xyXG4gICAgICAgIHRoaXMuc2hvd1RodW1ibmFpbHMgPSAhdGhpcy5zaG93VGh1bWJuYWlscztcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIE1ldGhvZCB0byBzY2FsZSB0aGUgcGFnZSBjdXJyZW50IHN1cHBvcnQgaW1wbGVtZW50YXRpb25cclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gc2NhbGVNb2RlIC0gbmV3IHNjYWxlIG1vZGVcclxuICAgICAqL1xyXG4gICAgc2NhbGVQYWdlKHNjYWxlTW9kZSkge1xyXG4gICAgICAgIHRoaXMuY3VycmVudFNjYWxlTW9kZSA9IHNjYWxlTW9kZTtcclxuXHJcbiAgICAgICAgY29uc3Qgdmlld2VyQ29udGFpbmVyID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoYCR7dGhpcy5yYW5kb21QZGZJZH0tdmlld2VyLW1haW4tY29udGFpbmVyYCk7XHJcbiAgICAgICAgY29uc3QgZG9jdW1lbnRDb250YWluZXIgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChgJHt0aGlzLnJhbmRvbVBkZklkfS12aWV3ZXItcGRmLXZpZXdlcmApO1xyXG5cclxuICAgICAgICBpZiAodGhpcy5wZGZWaWV3ZXIgJiYgZG9jdW1lbnRDb250YWluZXIpIHtcclxuXHJcbiAgICAgICAgICAgIGxldCB3aWR0aENvbnRhaW5lcjtcclxuICAgICAgICAgICAgbGV0IGhlaWdodENvbnRhaW5lcjtcclxuXHJcbiAgICAgICAgICAgIGlmICh2aWV3ZXJDb250YWluZXIgJiYgdmlld2VyQ29udGFpbmVyLmNsaWVudFdpZHRoIDw9IGRvY3VtZW50Q29udGFpbmVyLmNsaWVudFdpZHRoKSB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aENvbnRhaW5lciA9IHZpZXdlckNvbnRhaW5lci5jbGllbnRXaWR0aDtcclxuICAgICAgICAgICAgICAgIGhlaWdodENvbnRhaW5lciA9IHZpZXdlckNvbnRhaW5lci5jbGllbnRIZWlnaHQ7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aENvbnRhaW5lciA9IGRvY3VtZW50Q29udGFpbmVyLmNsaWVudFdpZHRoO1xyXG4gICAgICAgICAgICAgICAgaGVpZ2h0Q29udGFpbmVyID0gZG9jdW1lbnRDb250YWluZXIuY2xpZW50SGVpZ2h0O1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBjb25zdCBjdXJyZW50UGFnZSA9IHRoaXMucGRmVmlld2VyLl9wYWdlc1t0aGlzLnBkZlZpZXdlci5fY3VycmVudFBhZ2VOdW1iZXIgLSAxXTtcclxuXHJcbiAgICAgICAgICAgIGNvbnN0IHBhZGRpbmcgPSAyMDtcclxuICAgICAgICAgICAgY29uc3QgcGFnZVdpZHRoU2NhbGUgPSAod2lkdGhDb250YWluZXIgLSBwYWRkaW5nKSAvIGN1cnJlbnRQYWdlLndpZHRoICogY3VycmVudFBhZ2Uuc2NhbGU7XHJcbiAgICAgICAgICAgIGNvbnN0IHBhZ2VIZWlnaHRTY2FsZSA9IChoZWlnaHRDb250YWluZXIgLSBwYWRkaW5nKSAvIGN1cnJlbnRQYWdlLndpZHRoICogY3VycmVudFBhZ2Uuc2NhbGU7XHJcblxyXG4gICAgICAgICAgICBsZXQgc2NhbGUgPSB0aGlzLmdldFVzZXJTY2FsaW5nKCk7XHJcbiAgICAgICAgICAgIGlmICghc2NhbGUpIHtcclxuICAgICAgICAgICAgICAgIHN3aXRjaCAodGhpcy5jdXJyZW50U2NhbGVNb2RlKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FzZSAncGFnZS1hY3R1YWwnOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzY2FsZSA9IDE7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ3BhZ2Utd2lkdGgnOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBzY2FsZSA9IHBhZ2VXaWR0aFNjYWxlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdwYWdlLWhlaWdodCc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjYWxlID0gcGFnZUhlaWdodFNjYWxlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgICAgICBjYXNlICdwYWdlLWZpdCc6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjYWxlID0gTWF0aC5taW4ocGFnZVdpZHRoU2NhbGUsIHBhZ2VIZWlnaHRTY2FsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGNhc2UgJ2F1dG8nOlxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZXQgaG9yaXpvbnRhbFNjYWxlO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAodGhpcy5pc0xhbmRzY2FwZSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaG9yaXpvbnRhbFNjYWxlID0gTWF0aC5taW4ocGFnZUhlaWdodFNjYWxlLCBwYWdlV2lkdGhTY2FsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBob3Jpem9udGFsU2NhbGUgPSBwYWdlV2lkdGhTY2FsZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBob3Jpem9udGFsU2NhbGUgPSBNYXRoLnJvdW5kKGhvcml6b250YWxTY2FsZSk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHNjYWxlID0gTWF0aC5taW4odGhpcy5NQVhfQVVUT19TQ0FMRSwgaG9yaXpvbnRhbFNjYWxlKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcigncGRmVmlld1NldFNjYWxlOiBcXCcnICsgc2NhbGVNb2RlICsgJ1xcJyBpcyBhbiB1bmtub3duIHpvb20gdmFsdWUuJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHJldHVybjtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICB0aGlzLnNldFNjYWxlVXBkYXRlUGFnZXMoc2NhbGUpO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5jdXJyZW50U2NhbGUgPSAwO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5zZXRTY2FsZVVwZGF0ZVBhZ2VzKHNjYWxlKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFVwZGF0ZSBhbGwgdGhlIHBhZ2VzIHdpdGggdGhlIG5ld1NjYWxlIHNjYWxlXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIG5ld1NjYWxlIC0gbmV3IHNjYWxlIHBhZ2VcclxuICAgICAqL1xyXG4gICAgc2V0U2NhbGVVcGRhdGVQYWdlcyhuZXdTY2FsZTogbnVtYmVyKSB7XHJcbiAgICAgICAgaWYgKHRoaXMucGRmVmlld2VyKSB7XHJcbiAgICAgICAgICAgIGlmICghdGhpcy5pc1NhbWVTY2FsZSh0aGlzLmN1cnJlbnRTY2FsZSwgbmV3U2NhbGUpKSB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmN1cnJlbnRTY2FsZSA9IG5ld1NjYWxlO1xyXG5cclxuICAgICAgICAgICAgICAgIHRoaXMucGRmVmlld2VyLl9wYWdlcy5mb3JFYWNoKGZ1bmN0aW9uIChjdXJyZW50UGFnZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRQYWdlLnVwZGF0ZShuZXdTY2FsZSk7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdGhpcy5wZGZWaWV3ZXIudXBkYXRlKCk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2sgaWYgdGhlIHJlcXVlc3Qgc2NhbGUgb2YgdGhlIHBhZ2UgaXMgdGhlIHNhbWUgZm9yIGF2b2lkIHVzZWxlc3MgcmUtcmVuZGVyaW5nXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIG9sZFNjYWxlIC0gb2xkIHNjYWxlIHBhZ2VcclxuICAgICAqIEBwYXJhbSBuZXdTY2FsZSAtIG5ldyBzY2FsZSBwYWdlXHJcbiAgICAgKlxyXG4gICAgICovXHJcbiAgICBpc1NhbWVTY2FsZShvbGRTY2FsZTogbnVtYmVyLCBuZXdTY2FsZTogbnVtYmVyKTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIChuZXdTY2FsZSA9PT0gb2xkU2NhbGUpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogQ2hlY2sgaWYgaXMgYSBsYW5kIHNjYXBlIHZpZXdcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gd2lkdGhcclxuICAgICAqIEBwYXJhbSBoZWlnaHRcclxuICAgICAqL1xyXG4gICAgaXNMYW5kc2NhcGUod2lkdGg6IG51bWJlciwgaGVpZ2h0OiBudW1iZXIpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gKHdpZHRoID4gaGVpZ2h0KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIE1ldGhvZCB0cmlnZ2VyZWQgd2hlbiB0aGUgcGFnZSBpcyByZXNpemVkXHJcbiAgICAgKi9cclxuICAgIG9uUmVzaXplKCkge1xyXG4gICAgICAgIHRoaXMuc2NhbGVQYWdlKHRoaXMuY3VycmVudFNjYWxlTW9kZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiB0b2dnbGUgdGhlIGZpdCBwYWdlIHBkZlxyXG4gICAgICovXHJcbiAgICBwYWdlRml0KCkge1xyXG4gICAgICAgIGlmICh0aGlzLmN1cnJlbnRTY2FsZU1vZGUgIT09ICdwYWdlLWZpdCcpIHtcclxuICAgICAgICAgICAgdGhpcy5zY2FsZVBhZ2UoJ3BhZ2UtZml0Jyk7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5zY2FsZVBhZ2UoJ2F1dG8nKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiB6b29tIGluIHBhZ2UgcGRmXHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHRpY2tzXHJcbiAgICAgKi9cclxuICAgIHpvb21Jbih0aWNrcz86IG51bWJlcikge1xyXG4gICAgICAgIGxldCBuZXdTY2FsZTogYW55ID0gdGhpcy5jdXJyZW50U2NhbGU7XHJcbiAgICAgICAgZG8ge1xyXG4gICAgICAgICAgICBuZXdTY2FsZSA9IChuZXdTY2FsZSAqIHRoaXMuREVGQVVMVF9TQ0FMRV9ERUxUQSkudG9GaXhlZCgyKTtcclxuICAgICAgICAgICAgbmV3U2NhbGUgPSBNYXRoLmNlaWwobmV3U2NhbGUgKiAxMCkgLyAxMDtcclxuICAgICAgICAgICAgbmV3U2NhbGUgPSBNYXRoLm1pbih0aGlzLk1BWF9TQ0FMRSwgbmV3U2NhbGUpO1xyXG4gICAgICAgIH0gd2hpbGUgKC0tdGlja3MgPiAwICYmIG5ld1NjYWxlIDwgdGhpcy5NQVhfU0NBTEUpO1xyXG4gICAgICAgIHRoaXMuY3VycmVudFNjYWxlTW9kZSA9ICdhdXRvJztcclxuICAgICAgICB0aGlzLnNldFNjYWxlVXBkYXRlUGFnZXMobmV3U2NhbGUpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogem9vbSBvdXQgcGFnZSBwZGZcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gdGlja3NcclxuICAgICAqL1xyXG4gICAgem9vbU91dCh0aWNrcz86IG51bWJlcikge1xyXG4gICAgICAgIGxldCBuZXdTY2FsZTogYW55ID0gdGhpcy5jdXJyZW50U2NhbGU7XHJcbiAgICAgICAgZG8ge1xyXG4gICAgICAgICAgICBuZXdTY2FsZSA9IChuZXdTY2FsZSAvIHRoaXMuREVGQVVMVF9TQ0FMRV9ERUxUQSkudG9GaXhlZCgyKTtcclxuICAgICAgICAgICAgbmV3U2NhbGUgPSBNYXRoLmZsb29yKG5ld1NjYWxlICogMTApIC8gMTA7XHJcbiAgICAgICAgICAgIG5ld1NjYWxlID0gTWF0aC5tYXgodGhpcy5NSU5fU0NBTEUsIG5ld1NjYWxlKTtcclxuICAgICAgICB9IHdoaWxlICgtLXRpY2tzID4gMCAmJiBuZXdTY2FsZSA+IHRoaXMuTUlOX1NDQUxFKTtcclxuICAgICAgICB0aGlzLmN1cnJlbnRTY2FsZU1vZGUgPSAnYXV0byc7XHJcbiAgICAgICAgdGhpcy5zZXRTY2FsZVVwZGF0ZVBhZ2VzKG5ld1NjYWxlKTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIGxvYWQgdGhlIHByZXZpb3VzIHBhZ2VcclxuICAgICAqL1xyXG4gICAgcHJldmlvdXNQYWdlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnBkZlZpZXdlciAmJiB0aGlzLnBhZ2UgPiAxKSB7XHJcbiAgICAgICAgICAgIHRoaXMucGFnZS0tO1xyXG4gICAgICAgICAgICB0aGlzLmRpc3BsYXlQYWdlID0gdGhpcy5wYWdlO1xyXG5cclxuICAgICAgICAgICAgdGhpcy5wZGZWaWV3ZXIuY3VycmVudFBhZ2VOdW1iZXIgPSB0aGlzLnBhZ2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogbG9hZCB0aGUgbmV4dCBwYWdlXHJcbiAgICAgKi9cclxuICAgIG5leHRQYWdlKCkge1xyXG4gICAgICAgIGlmICh0aGlzLnBkZlZpZXdlciAmJiB0aGlzLnBhZ2UgPCB0aGlzLnRvdGFsUGFnZXMpIHtcclxuICAgICAgICAgICAgdGhpcy5wYWdlKys7XHJcbiAgICAgICAgICAgIHRoaXMuZGlzcGxheVBhZ2UgPSB0aGlzLnBhZ2U7XHJcblxyXG4gICAgICAgICAgICB0aGlzLnBkZlZpZXdlci5jdXJyZW50UGFnZU51bWJlciA9IHRoaXMucGFnZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBsb2FkIHRoZSBwYWdlIGluIGlucHV0XHJcbiAgICAgKlxyXG4gICAgICogQHBhcmFtIHBhZ2UgdG8gbG9hZFxyXG4gICAgICovXHJcbiAgICBpbnB1dFBhZ2UocGFnZTogc3RyaW5nKSB7XHJcbiAgICAgICAgY29uc3QgcGFnZUlucHV0ID0gcGFyc2VJbnQocGFnZSwgMTApO1xyXG5cclxuICAgICAgICBpZiAoIWlzTmFOKHBhZ2VJbnB1dCkgJiYgcGFnZUlucHV0ID4gMCAmJiBwYWdlSW5wdXQgPD0gdGhpcy50b3RhbFBhZ2VzKSB7XHJcbiAgICAgICAgICAgIHRoaXMucGFnZSA9IHBhZ2VJbnB1dDtcclxuICAgICAgICAgICAgdGhpcy5kaXNwbGF5UGFnZSA9IHRoaXMucGFnZTtcclxuICAgICAgICAgICAgdGhpcy5wZGZWaWV3ZXIuY3VycmVudFBhZ2VOdW1iZXIgPSB0aGlzLnBhZ2U7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgdGhpcy5kaXNwbGF5UGFnZSA9IHRoaXMucGFnZTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBQYWdlIENoYW5nZSBFdmVudFxyXG4gICAgICpcclxuICAgICAqIEBwYXJhbSBldmVudFxyXG4gICAgICovXHJcbiAgICBvblBhZ2VDaGFuZ2UoZXZlbnQpIHtcclxuICAgICAgICB0aGlzLnBhZ2UgPSBldmVudC5wYWdlTnVtYmVyO1xyXG4gICAgICAgIHRoaXMuZGlzcGxheVBhZ2UgPSBldmVudC5wYWdlTnVtYmVyO1xyXG4gICAgfVxyXG5cclxuICAgIG9uUGRmUGFzc3dvcmQoY2FsbGJhY2ssIHJlYXNvbikge1xyXG4gICAgICAgIHRoaXMuZGlhbG9nXHJcbiAgICAgICAgICAgIC5vcGVuKFBkZlBhc3N3b3JkRGlhbG9nQ29tcG9uZW50LCB7XHJcbiAgICAgICAgICAgICAgICB3aWR0aDogJzQwMHB4JyxcclxuICAgICAgICAgICAgICAgIGRhdGE6IHsgcmVhc29uIH1cclxuICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgLmFmdGVyQ2xvc2VkKCkuc3Vic2NyaWJlKChwYXNzd29yZCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHBhc3N3b3JkKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgY2FsbGJhY2socGFzc3dvcmQpO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB0aGlzLmNsb3NlLmVtaXQoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICB9KTtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFBhZ2UgUmVuZGVyZWQgRXZlbnRcclxuICAgICAqL1xyXG4gICAgb25QYWdlUmVuZGVyZWQoKSB7XHJcbiAgICAgICAgdGhpcy5yZW5kZXJlZC5lbWl0KCk7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBQYWdlcyBMb2FkZWQgRXZlbnRcclxuICAgICAqXHJcbiAgICAgKiBAcGFyYW0gZXZlbnRcclxuICAgICAqL1xyXG4gICAgb25QYWdlc0xvYWRlZChldmVudCkge1xyXG4gICAgICAgIHRoaXMuaXNQYW5lbERpc2FibGVkID0gZmFsc2U7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBLZXlib2FyZCBFdmVudCBMaXN0ZW5lclxyXG4gICAgICogQHBhcmFtIEtleWJvYXJkRXZlbnQgZXZlbnRcclxuICAgICAqL1xyXG4gICAgQEhvc3RMaXN0ZW5lcignZG9jdW1lbnQ6a2V5ZG93bicsIFsnJGV2ZW50J10pXHJcbiAgICBoYW5kbGVLZXlib2FyZEV2ZW50KGV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XHJcbiAgICAgICAgY29uc3Qga2V5ID0gZXZlbnQua2V5Q29kZTtcclxuICAgICAgICBpZiAoa2V5ID09PSAzOSkgeyAvLyByaWdodCBhcnJvd1xyXG4gICAgICAgICAgICB0aGlzLm5leHRQYWdlKCk7XHJcbiAgICAgICAgfSBlbHNlIGlmIChrZXkgPT09IDM3KSB7Ly8gbGVmdCBhcnJvd1xyXG4gICAgICAgICAgICB0aGlzLnByZXZpb3VzUGFnZSgpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBwcml2YXRlIGdlbmVyYXRlVXVpZCgpIHtcclxuICAgICAgICByZXR1cm4gJ3h4eHh4eHh4LXh4eHgtNHh4eC15eHh4LXh4eHh4eHh4eHh4eCcucmVwbGFjZSgvW3h5XS9nLCBmdW5jdGlvbiAoYykge1xyXG4gICAgICAgICAgICBjb25zdCByID0gTWF0aC5yYW5kb20oKSAqIDE2IHwgMCwgdiA9IGMgPT09ICd4JyA/IHIgOiAociAmIDB4MyB8IDB4OCk7XHJcbiAgICAgICAgICAgIHJldHVybiB2LnRvU3RyaW5nKDE2KTtcclxuICAgICAgICB9KTtcclxuICAgIH1cclxufVxyXG4iXX0=