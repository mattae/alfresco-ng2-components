/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { HttpClient } from '@angular/common/http';
import { Component, Input, ViewEncapsulation } from '@angular/core';
import { AppConfigService } from './../../app-config/app-config.service';
var TxtViewerComponent = /** @class */ (function () {
    function TxtViewerComponent(http, appConfigService) {
        this.http = http;
        this.appConfigService = appConfigService;
    }
    /**
     * @param {?} changes
     * @return {?}
     */
    TxtViewerComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        /** @type {?} */
        var blobFile = changes['blobFile'];
        if (blobFile && blobFile.currentValue) {
            return this.readBlob(blobFile.currentValue);
        }
        /** @type {?} */
        var urlFile = changes['urlFile'];
        if (urlFile && urlFile.currentValue) {
            return this.getUrlContent(urlFile.currentValue);
        }
        if (!this.urlFile && !this.blobFile) {
            throw new Error('Attribute urlFile or blobFile is required');
        }
    };
    /**
     * @private
     * @param {?} url
     * @return {?}
     */
    TxtViewerComponent.prototype.getUrlContent = /**
     * @private
     * @param {?} url
     * @return {?}
     */
    function (url) {
        var _this = this;
        /** @type {?} */
        var withCredentialsMode = this.appConfigService.get('auth.withCredentials', false);
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            _this.http.get(url, { responseType: 'text', withCredentials: withCredentialsMode }).subscribe((/**
             * @param {?} res
             * @return {?}
             */
            function (res) {
                _this.content = res;
                resolve();
            }), (/**
             * @param {?} event
             * @return {?}
             */
            function (event) {
                reject(event);
            }));
        }));
    };
    /**
     * @private
     * @param {?} blob
     * @return {?}
     */
    TxtViewerComponent.prototype.readBlob = /**
     * @private
     * @param {?} blob
     * @return {?}
     */
    function (blob) {
        var _this = this;
        return new Promise((/**
         * @param {?} resolve
         * @param {?} reject
         * @return {?}
         */
        function (resolve, reject) {
            /** @type {?} */
            var reader = new FileReader();
            reader.onload = (/**
             * @return {?}
             */
            function () {
                _this.content = reader.result;
                resolve();
            });
            reader.onerror = (/**
             * @param {?} error
             * @return {?}
             */
            function (error) {
                reject(error);
            });
            reader.readAsText(blob);
        }));
    };
    TxtViewerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-txt-viewer',
                    template: "<pre class=\"adf-txt-viewer-content\">\r\n    {{content}}\r\n</pre>\r\n",
                    host: { 'class': 'adf-txt-viewer' },
                    encapsulation: ViewEncapsulation.None,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    TxtViewerComponent.ctorParameters = function () { return [
        { type: HttpClient },
        { type: AppConfigService }
    ]; };
    TxtViewerComponent.propDecorators = {
        urlFile: [{ type: Input }],
        blobFile: [{ type: Input }]
    };
    return TxtViewerComponent;
}());
export { TxtViewerComponent };
if (false) {
    /** @type {?} */
    TxtViewerComponent.prototype.urlFile;
    /** @type {?} */
    TxtViewerComponent.prototype.blobFile;
    /** @type {?} */
    TxtViewerComponent.prototype.content;
    /**
     * @type {?}
     * @private
     */
    TxtViewerComponent.prototype.http;
    /**
     * @type {?}
     * @private
     */
    TxtViewerComponent.prototype.appConfigService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidHh0Vmlld2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInZpZXdlci9jb21wb25lbnRzL3R4dFZpZXdlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFpQkEsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLHNCQUFzQixDQUFDO0FBQ2xELE9BQU8sRUFBRSxTQUFTLEVBQUUsS0FBSyxFQUFhLGlCQUFpQixFQUFFLE1BQU0sZUFBZSxDQUFDO0FBRS9FLE9BQU8sRUFBRSxnQkFBZ0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBRXpFO0lBaUJJLDRCQUFvQixJQUFnQixFQUFVLGdCQUFrQztRQUE1RCxTQUFJLEdBQUosSUFBSSxDQUFZO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtJQUNoRixDQUFDOzs7OztJQUVELHdDQUFXOzs7O0lBQVgsVUFBWSxPQUFzQjs7WUFFeEIsUUFBUSxHQUFHLE9BQU8sQ0FBQyxVQUFVLENBQUM7UUFDcEMsSUFBSSxRQUFRLElBQUksUUFBUSxDQUFDLFlBQVksRUFBRTtZQUNuQyxPQUFPLElBQUksQ0FBQyxRQUFRLENBQUMsUUFBUSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQy9DOztZQUVLLE9BQU8sR0FBRyxPQUFPLENBQUMsU0FBUyxDQUFDO1FBQ2xDLElBQUksT0FBTyxJQUFJLE9BQU8sQ0FBQyxZQUFZLEVBQUU7WUFDakMsT0FBTyxJQUFJLENBQUMsYUFBYSxDQUFDLE9BQU8sQ0FBQyxZQUFZLENBQUMsQ0FBQztTQUNuRDtRQUVELElBQUksQ0FBQyxJQUFJLENBQUMsT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLFFBQVEsRUFBRTtZQUNqQyxNQUFNLElBQUksS0FBSyxDQUFDLDJDQUEyQyxDQUFDLENBQUM7U0FDaEU7SUFDTCxDQUFDOzs7Ozs7SUFFTywwQ0FBYTs7Ozs7SUFBckIsVUFBc0IsR0FBVztRQUFqQyxpQkFXQzs7WUFWUyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsR0FBRyxDQUFVLHNCQUFzQixFQUFFLEtBQUssQ0FBQztRQUU3RixPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxVQUFDLE9BQU8sRUFBRSxNQUFNO1lBQy9CLEtBQUksQ0FBQyxJQUFJLENBQUMsR0FBRyxDQUFDLEdBQUcsRUFBRSxFQUFFLFlBQVksRUFBRSxNQUFNLEVBQUUsZUFBZSxFQUFFLG1CQUFtQixFQUFFLENBQUMsQ0FBQyxTQUFTOzs7O1lBQUMsVUFBQyxHQUFHO2dCQUM3RixLQUFJLENBQUMsT0FBTyxHQUFHLEdBQUcsQ0FBQztnQkFDbkIsT0FBTyxFQUFFLENBQUM7WUFDZCxDQUFDOzs7O1lBQUUsVUFBQyxLQUFLO2dCQUNMLE1BQU0sQ0FBQyxLQUFLLENBQUMsQ0FBQztZQUNsQixDQUFDLEVBQUMsQ0FBQztRQUNQLENBQUMsRUFBQyxDQUFDO0lBQ1AsQ0FBQzs7Ozs7O0lBRU8scUNBQVE7Ozs7O0lBQWhCLFVBQWlCLElBQVU7UUFBM0IsaUJBZUM7UUFkRyxPQUFPLElBQUksT0FBTzs7Ozs7UUFBQyxVQUFDLE9BQU8sRUFBRSxNQUFNOztnQkFDekIsTUFBTSxHQUFHLElBQUksVUFBVSxFQUFFO1lBRS9CLE1BQU0sQ0FBQyxNQUFNOzs7WUFBRztnQkFDWixLQUFJLENBQUMsT0FBTyxHQUFHLE1BQU0sQ0FBQyxNQUFNLENBQUM7Z0JBQzdCLE9BQU8sRUFBRSxDQUFDO1lBQ2QsQ0FBQyxDQUFBLENBQUM7WUFFRixNQUFNLENBQUMsT0FBTzs7OztZQUFHLFVBQUMsS0FBVTtnQkFDeEIsTUFBTSxDQUFDLEtBQUssQ0FBQyxDQUFDO1lBQ2xCLENBQUMsQ0FBQSxDQUFDO1lBRUYsTUFBTSxDQUFDLFVBQVUsQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM1QixDQUFDLEVBQUMsQ0FBQztJQUNQLENBQUM7O2dCQWpFSixTQUFTLFNBQUM7b0JBQ1AsUUFBUSxFQUFFLGdCQUFnQjtvQkFDMUIsbUZBQXlDO29CQUV6QyxJQUFJLEVBQUUsRUFBRSxPQUFPLEVBQUUsZ0JBQWdCLEVBQUU7b0JBQ25DLGFBQWEsRUFBRSxpQkFBaUIsQ0FBQyxJQUFJOztpQkFDeEM7Ozs7Z0JBWFEsVUFBVTtnQkFHVixnQkFBZ0I7OzswQkFXcEIsS0FBSzsyQkFHTCxLQUFLOztJQXNEVix5QkFBQztDQUFBLEFBbEVELElBa0VDO1NBM0RZLGtCQUFrQjs7O0lBRTNCLHFDQUNhOztJQUViLHNDQUNlOztJQUVmLHFDQUE4Qjs7Ozs7SUFFbEIsa0NBQXdCOzs7OztJQUFFLDhDQUEwQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIVxyXG4gKiBAbGljZW5zZVxyXG4gKiBDb3B5cmlnaHQgMjAxOSBBbGZyZXNjbyBTb2Z0d2FyZSwgTHRkLlxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xyXG5pbXBvcnQgeyBDb21wb25lbnQsIElucHV0LCBPbkNoYW5nZXMsIFZpZXdFbmNhcHN1bGF0aW9uIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcbmltcG9ydCB7IFNpbXBsZUNoYW5nZXMgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcclxuaW1wb3J0IHsgQXBwQ29uZmlnU2VydmljZSB9IGZyb20gJy4vLi4vLi4vYXBwLWNvbmZpZy9hcHAtY29uZmlnLnNlcnZpY2UnO1xyXG5cclxuQENvbXBvbmVudCh7XHJcbiAgICBzZWxlY3RvcjogJ2FkZi10eHQtdmlld2VyJyxcclxuICAgIHRlbXBsYXRlVXJsOiAnLi90eHRWaWV3ZXIuY29tcG9uZW50Lmh0bWwnLFxyXG4gICAgc3R5bGVVcmxzOiBbJy4vdHh0Vmlld2VyLmNvbXBvbmVudC5zY3NzJ10sXHJcbiAgICBob3N0OiB7ICdjbGFzcyc6ICdhZGYtdHh0LXZpZXdlcicgfSxcclxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcclxufSlcclxuZXhwb3J0IGNsYXNzIFR4dFZpZXdlckNvbXBvbmVudCBpbXBsZW1lbnRzIE9uQ2hhbmdlcyB7XHJcblxyXG4gICAgQElucHV0KClcclxuICAgIHVybEZpbGU6IGFueTtcclxuXHJcbiAgICBASW5wdXQoKVxyXG4gICAgYmxvYkZpbGU6IEJsb2I7XHJcblxyXG4gICAgY29udGVudDogc3RyaW5nIHwgQXJyYXlCdWZmZXI7XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBodHRwOiBIdHRwQ2xpZW50LCBwcml2YXRlIGFwcENvbmZpZ1NlcnZpY2U6IEFwcENvbmZpZ1NlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICBuZ09uQ2hhbmdlcyhjaGFuZ2VzOiBTaW1wbGVDaGFuZ2VzKTogUHJvbWlzZTxhbnk+IHtcclxuXHJcbiAgICAgICAgY29uc3QgYmxvYkZpbGUgPSBjaGFuZ2VzWydibG9iRmlsZSddO1xyXG4gICAgICAgIGlmIChibG9iRmlsZSAmJiBibG9iRmlsZS5jdXJyZW50VmFsdWUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMucmVhZEJsb2IoYmxvYkZpbGUuY3VycmVudFZhbHVlKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGNvbnN0IHVybEZpbGUgPSBjaGFuZ2VzWyd1cmxGaWxlJ107XHJcbiAgICAgICAgaWYgKHVybEZpbGUgJiYgdXJsRmlsZS5jdXJyZW50VmFsdWUpIHtcclxuICAgICAgICAgICAgcmV0dXJuIHRoaXMuZ2V0VXJsQ29udGVudCh1cmxGaWxlLmN1cnJlbnRWYWx1ZSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoIXRoaXMudXJsRmlsZSAmJiAhdGhpcy5ibG9iRmlsZSkge1xyXG4gICAgICAgICAgICB0aHJvdyBuZXcgRXJyb3IoJ0F0dHJpYnV0ZSB1cmxGaWxlIG9yIGJsb2JGaWxlIGlzIHJlcXVpcmVkJyk7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgZ2V0VXJsQ29udGVudCh1cmw6IHN0cmluZyk6IFByb21pc2U8YW55PiB7XHJcbiAgICAgICAgY29uc3Qgd2l0aENyZWRlbnRpYWxzTW9kZSA9IHRoaXMuYXBwQ29uZmlnU2VydmljZS5nZXQ8Ym9vbGVhbj4oJ2F1dGgud2l0aENyZWRlbnRpYWxzJywgZmFsc2UpO1xyXG5cclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgICAgICB0aGlzLmh0dHAuZ2V0KHVybCwgeyByZXNwb25zZVR5cGU6ICd0ZXh0Jywgd2l0aENyZWRlbnRpYWxzOiB3aXRoQ3JlZGVudGlhbHNNb2RlIH0pLnN1YnNjcmliZSgocmVzKSA9PiB7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmNvbnRlbnQgPSByZXM7XHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgICAgIH0sIChldmVudCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgcmVqZWN0KGV2ZW50KTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgcHJpdmF0ZSByZWFkQmxvYihibG9iOiBCbG9iKTogUHJvbWlzZTxhbnk+IHtcclxuICAgICAgICByZXR1cm4gbmV3IFByb21pc2UoKHJlc29sdmUsIHJlamVjdCkgPT4ge1xyXG4gICAgICAgICAgICBjb25zdCByZWFkZXIgPSBuZXcgRmlsZVJlYWRlcigpO1xyXG5cclxuICAgICAgICAgICAgcmVhZGVyLm9ubG9hZCA9ICgpID0+IHtcclxuICAgICAgICAgICAgICAgIHRoaXMuY29udGVudCA9IHJlYWRlci5yZXN1bHQ7XHJcbiAgICAgICAgICAgICAgICByZXNvbHZlKCk7XHJcbiAgICAgICAgICAgIH07XHJcblxyXG4gICAgICAgICAgICByZWFkZXIub25lcnJvciA9IChlcnJvcjogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICByZWplY3QoZXJyb3IpO1xyXG4gICAgICAgICAgICB9O1xyXG5cclxuICAgICAgICAgICAgcmVhZGVyLnJlYWRBc1RleHQoYmxvYik7XHJcbiAgICAgICAgfSk7XHJcbiAgICB9XHJcbn1cclxuIl19