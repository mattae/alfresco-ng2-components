/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Component, ContentChild, ElementRef, EventEmitter, HostListener, Input, Output, TemplateRef, ViewEncapsulation } from '@angular/core';
import { BaseEvent } from '../../events';
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import { ViewerMoreActionsComponent } from './viewer-more-actions.component';
import { ViewerOpenWithComponent } from './viewer-open-with.component';
import { ViewerSidebarComponent } from './viewer-sidebar.component';
import { ViewerToolbarComponent } from './viewer-toolbar.component';
import { ViewUtilService } from '../services/view-util.service';
import { AppExtensionService } from '@alfresco/adf-extensions';
var ViewerComponent = /** @class */ (function () {
    function ViewerComponent(apiService, viewUtils, logService, extensionService, el) {
        this.apiService = apiService;
        this.viewUtils = viewUtils;
        this.logService = logService;
        this.extensionService = extensionService;
        this.el = el;
        /**
         * If you want to load an external file that does not come from ACS you
         * can use this URL to specify where to load the file from.
         */
        this.urlFile = '';
        /**
         * Viewer to use with the `urlFile` address (`pdf`, `image`, `media`, `text`).
         * Used when `urlFile` has no filename and extension.
         */
        this.urlFileViewer = null;
        /**
         * Node Id of the file to load.
         */
        this.nodeId = null;
        /**
         * Shared link id (to display shared file).
         */
        this.sharedLinkId = null;
        /**
         * If `true` then show the Viewer as a full page over the current content.
         * Otherwise fit inside the parent div.
         */
        this.overlayMode = false;
        /**
         * Hide or show the viewer
         */
        this.showViewer = true;
        /**
         * Hide or show the toolbar
         */
        this.showToolbar = true;
        /** @deprecated 3.2.0 */
        /**
         * Allows `back` navigation
         */
        this.allowGoBack = true;
        /**
         * Toggles downloading.
         */
        this.allowDownload = true;
        /**
         * Toggles printing.
         */
        this.allowPrint = false;
        /**
         * Toggles the 'Full Screen' feature.
         */
        this.allowFullScreen = true;
        /**
         * Toggles before/next navigation. You can use the arrow buttons to navigate
         * between documents in the collection.
         */
        this.allowNavigate = false;
        /**
         * Toggles the "before" ("<") button. Requires `allowNavigate` to be enabled.
         */
        this.canNavigateBefore = true;
        /**
         * Toggles the next (">") button. Requires `allowNavigate` to be enabled.
         */
        this.canNavigateNext = true;
        /**
         * Allow the left the sidebar.
         */
        this.allowLeftSidebar = false;
        /**
         * Allow the right sidebar.
         */
        this.allowRightSidebar = false;
        /**
         * Toggles PDF thumbnails.
         */
        this.allowThumbnails = true;
        /**
         * Toggles right sidebar visibility. Requires `allowRightSidebar` to be set to `true`.
         */
        this.showRightSidebar = false;
        /**
         * Toggles left sidebar visibility. Requires `allowLeftSidebar` to be set to `true`.
         */
        this.showLeftSidebar = false;
        /**
         * The template for the right sidebar. The template context contains the loaded node data.
         */
        this.sidebarRightTemplate = null;
        /**
         * The template for the left sidebar. The template context contains the loaded node data.
         */
        this.sidebarLeftTemplate = null;
        /**
         * The template for the pdf thumbnails.
         */
        this.thumbnailsTemplate = null;
        /**
         * Number of times the Viewer will retry fetching content Rendition.
         * There is a delay of at least one second between attempts.
         */
        this.maxRetries = 10;
        /**
         * Emitted when user clicks the 'Back' button.
         */
        this.goBack = new EventEmitter();
        /**
         * Emitted when user clicks the 'Print' button.
         */
        this.print = new EventEmitter();
        /**
         * Emitted when the viewer is shown or hidden.
         */
        this.showViewerChange = new EventEmitter();
        /**
         * Emitted when the filename extension changes.
         */
        this.extensionChange = new EventEmitter();
        /**
         * Emitted when user clicks 'Navigate Before' ("<") button.
         */
        this.navigateBefore = new EventEmitter();
        /**
         * Emitted when user clicks 'Navigate Next' (">") button.
         */
        this.navigateNext = new EventEmitter();
        /**
         * Emitted when the shared link used is not valid.
         */
        this.invalidSharedLink = new EventEmitter();
        this.TRY_TIMEOUT = 2000;
        this.viewerType = 'unknown';
        this.isLoading = false;
        this.extensionTemplates = [];
        this.externalExtensions = [];
        this.sidebarRightTemplateContext = { node: null };
        this.sidebarLeftTemplateContext = { node: null };
        this.viewerExtensions = [];
        this.subscriptions = [];
        // Extensions that are supported by the Viewer without conversion
        this.extensions = {
            image: ['png', 'jpg', 'jpeg', 'gif', 'bpm', 'svg'],
            media: ['wav', 'mp4', 'mp3', 'webm', 'ogg'],
            text: ['txt', 'xml', 'html', 'json', 'ts', 'css', 'md'],
            pdf: ['pdf']
        };
        // Mime types that are supported by the Viewer without conversion
        this.mimeTypes = {
            text: ['text/plain', 'text/csv', 'text/xml', 'text/html', 'application/x-javascript'],
            pdf: ['application/pdf'],
            image: ['image/png', 'image/jpeg', 'image/gif', 'image/bmp', 'image/svg+xml'],
            media: ['video/mp4', 'video/webm', 'video/ogg', 'audio/mpeg', 'audio/ogg', 'audio/wav']
        };
    }
    /**
     * @return {?}
     */
    ViewerComponent.prototype.isSourceDefined = /**
     * @return {?}
     */
    function () {
        return (this.urlFile || this.blobFile || this.nodeId || this.sharedLinkId) ? true : false;
    };
    /**
     * @return {?}
     */
    ViewerComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.subscriptions.push(this.apiService.nodeUpdated.subscribe((/**
         * @param {?} node
         * @return {?}
         */
        function (node) { return _this.onNodeUpdated(node); })));
        this.loadExtensions();
    };
    /**
     * @private
     * @return {?}
     */
    ViewerComponent.prototype.loadExtensions = /**
     * @private
     * @return {?}
     */
    function () {
        var _this = this;
        this.viewerExtensions = this.extensionService.getViewerExtensions();
        this.viewerExtensions
            .forEach((/**
         * @param {?} extension
         * @return {?}
         */
        function (extension) {
            _this.externalExtensions.push(extension.fileExtension);
        }));
    };
    /**
     * @return {?}
     */
    ViewerComponent.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.subscriptions.forEach((/**
         * @param {?} subscription
         * @return {?}
         */
        function (subscription) { return subscription.unsubscribe(); }));
        this.subscriptions = [];
    };
    /**
     * @private
     * @param {?} node
     * @return {?}
     */
    ViewerComponent.prototype.onNodeUpdated = /**
     * @private
     * @param {?} node
     * @return {?}
     */
    function (node) {
        var _this = this;
        if (node && node.id === this.nodeId) {
            this.generateCacheBusterNumber();
            this.isLoading = true;
            this.setUpNodeFile(node).then((/**
             * @return {?}
             */
            function () {
                _this.isLoading = false;
            }));
        }
    };
    /**
     * @param {?} changes
     * @return {?}
     */
    ViewerComponent.prototype.ngOnChanges = /**
     * @param {?} changes
     * @return {?}
     */
    function (changes) {
        var _this = this;
        if (this.showViewer) {
            if (!this.isSourceDefined()) {
                throw new Error('A content source attribute value is missing.');
            }
            this.isLoading = true;
            if (this.blobFile) {
                this.setUpBlobData();
                this.isLoading = false;
            }
            else if (this.urlFile) {
                this.setUpUrlFile();
                this.isLoading = false;
            }
            else if (this.nodeId) {
                this.apiService.nodesApi.getNode(this.nodeId, { include: ['allowableOperations'] }).then((/**
                 * @param {?} node
                 * @return {?}
                 */
                function (node) {
                    _this.nodeEntry = node;
                    _this.setUpNodeFile(node.entry).then((/**
                     * @return {?}
                     */
                    function () {
                        _this.isLoading = false;
                    }));
                }), (/**
                 * @param {?} error
                 * @return {?}
                 */
                function (error) {
                    _this.isLoading = false;
                    _this.logService.error('This node does not exist');
                }));
            }
            else if (this.sharedLinkId) {
                this.allowGoBack = false;
                this.apiService.sharedLinksApi.getSharedLink(this.sharedLinkId).then((/**
                 * @param {?} sharedLinkEntry
                 * @return {?}
                 */
                function (sharedLinkEntry) {
                    _this.setUpSharedLinkFile(sharedLinkEntry);
                    _this.isLoading = false;
                }), (/**
                 * @return {?}
                 */
                function () {
                    _this.isLoading = false;
                    _this.logService.error('This sharedLink does not exist');
                    _this.invalidSharedLink.next();
                }));
            }
        }
    };
    /**
     * @private
     * @return {?}
     */
    ViewerComponent.prototype.setUpBlobData = /**
     * @private
     * @return {?}
     */
    function () {
        this.fileTitle = this.getDisplayName('Unknown');
        this.mimeType = this.blobFile.type;
        this.viewerType = this.getViewerTypeByMimeType(this.mimeType);
        this.allowDownload = false;
        // TODO: wrap blob into the data url and allow downloading
        this.extensionChange.emit(this.mimeType);
        this.scrollTop();
    };
    /**
     * @private
     * @return {?}
     */
    ViewerComponent.prototype.setUpUrlFile = /**
     * @private
     * @return {?}
     */
    function () {
        /** @type {?} */
        var filenameFromUrl = this.getFilenameFromUrl(this.urlFile);
        this.fileTitle = this.getDisplayName(filenameFromUrl);
        this.extension = this.getFileExtension(filenameFromUrl);
        this.urlFileContent = this.urlFile;
        this.fileName = this.displayName;
        this.viewerType = this.urlFileViewer || this.getViewerTypeByExtension(this.extension);
        if (this.viewerType === 'unknown') {
            this.viewerType = this.getViewerTypeByMimeType(this.mimeType);
        }
        this.extensionChange.emit(this.extension);
        this.scrollTop();
    };
    /**
     * @private
     * @param {?} data
     * @return {?}
     */
    ViewerComponent.prototype.setUpNodeFile = /**
     * @private
     * @param {?} data
     * @return {?}
     */
    function (data) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var setupNode;
            return tslib_1.__generator(this, function (_a) {
                if (data.content) {
                    this.mimeType = data.content.mimeType;
                }
                this.fileTitle = this.getDisplayName(data.name);
                this.urlFileContent = this.apiService.contentApi.getContentUrl(data.id);
                this.urlFileContent = this.cacheBusterNumber ? this.urlFileContent + '&' + this.cacheBusterNumber : this.urlFileContent;
                this.extension = this.getFileExtension(data.name);
                this.fileName = data.name;
                this.viewerType = this.getViewerTypeByExtension(this.extension);
                if (this.viewerType === 'unknown') {
                    this.viewerType = this.getViewerTypeByMimeType(this.mimeType);
                }
                if (this.viewerType === 'unknown') {
                    setupNode = this.displayNodeRendition(data.id);
                }
                this.extensionChange.emit(this.extension);
                this.sidebarRightTemplateContext.node = data;
                this.sidebarLeftTemplateContext.node = data;
                this.scrollTop();
                return [2 /*return*/, setupNode];
            });
        });
    };
    /**
     * @private
     * @param {?} details
     * @return {?}
     */
    ViewerComponent.prototype.setUpSharedLinkFile = /**
     * @private
     * @param {?} details
     * @return {?}
     */
    function (details) {
        this.mimeType = details.entry.content.mimeType;
        this.fileTitle = this.getDisplayName(details.entry.name);
        this.extension = this.getFileExtension(details.entry.name);
        this.fileName = details.entry.name;
        this.urlFileContent = this.apiService.contentApi.getSharedLinkContentUrl(this.sharedLinkId, false);
        this.viewerType = this.getViewerTypeByMimeType(this.mimeType);
        if (this.viewerType === 'unknown') {
            this.viewerType = this.getViewerTypeByExtension(this.extension);
        }
        if (this.viewerType === 'unknown') {
            this.displaySharedLinkRendition(this.sharedLinkId);
        }
        this.extensionChange.emit(this.extension);
    };
    /**
     * @return {?}
     */
    ViewerComponent.prototype.toggleSidebar = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.showRightSidebar = !this.showRightSidebar;
        if (this.showRightSidebar && this.nodeId) {
            this.apiService.getInstance().nodes.getNode(this.nodeId, { include: ['allowableOperations'] })
                .then((/**
             * @param {?} nodeEntry
             * @return {?}
             */
            function (nodeEntry) {
                _this.sidebarRightTemplateContext.node = nodeEntry.entry;
            }));
        }
    };
    /**
     * @return {?}
     */
    ViewerComponent.prototype.toggleLeftSidebar = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.showLeftSidebar = !this.showLeftSidebar;
        if (this.showRightSidebar && this.nodeId) {
            this.apiService.getInstance().nodes.getNode(this.nodeId, { include: ['allowableOperations'] })
                .then((/**
             * @param {?} nodeEntry
             * @return {?}
             */
            function (nodeEntry) {
                _this.sidebarLeftTemplateContext.node = nodeEntry.entry;
            }));
        }
    };
    /**
     * @private
     * @param {?} name
     * @return {?}
     */
    ViewerComponent.prototype.getDisplayName = /**
     * @private
     * @param {?} name
     * @return {?}
     */
    function (name) {
        return this.displayName || name;
    };
    /**
     * @return {?}
     */
    ViewerComponent.prototype.scrollTop = /**
     * @return {?}
     */
    function () {
        window.scrollTo(0, 1);
    };
    /**
     * @param {?} mimeType
     * @return {?}
     */
    ViewerComponent.prototype.getViewerTypeByMimeType = /**
     * @param {?} mimeType
     * @return {?}
     */
    function (mimeType) {
        var e_1, _a;
        if (mimeType) {
            mimeType = mimeType.toLowerCase();
            /** @type {?} */
            var editorTypes = Object.keys(this.mimeTypes);
            try {
                for (var editorTypes_1 = tslib_1.__values(editorTypes), editorTypes_1_1 = editorTypes_1.next(); !editorTypes_1_1.done; editorTypes_1_1 = editorTypes_1.next()) {
                    var type = editorTypes_1_1.value;
                    if (this.mimeTypes[type].indexOf(mimeType) >= 0) {
                        return type;
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (editorTypes_1_1 && !editorTypes_1_1.done && (_a = editorTypes_1.return)) _a.call(editorTypes_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        return 'unknown';
    };
    /**
     * @param {?} extension
     * @return {?}
     */
    ViewerComponent.prototype.getViewerTypeByExtension = /**
     * @param {?} extension
     * @return {?}
     */
    function (extension) {
        if (extension) {
            extension = extension.toLowerCase();
        }
        if (this.isCustomViewerExtension(extension)) {
            return 'custom';
        }
        if (this.extensions.image.indexOf(extension) >= 0) {
            return 'image';
        }
        if (this.extensions.media.indexOf(extension) >= 0) {
            return 'media';
        }
        if (this.extensions.text.indexOf(extension) >= 0) {
            return 'text';
        }
        if (this.extensions.pdf.indexOf(extension) >= 0) {
            return 'pdf';
        }
        return 'unknown';
    };
    /**
     * @return {?}
     */
    ViewerComponent.prototype.onBackButtonClick = /**
     * @return {?}
     */
    function () {
        this.close();
    };
    /**
     * @return {?}
     */
    ViewerComponent.prototype.onNavigateBeforeClick = /**
     * @return {?}
     */
    function () {
        this.navigateBefore.next();
    };
    /**
     * @return {?}
     */
    ViewerComponent.prototype.onNavigateNextClick = /**
     * @return {?}
     */
    function () {
        this.navigateNext.next();
    };
    /**
     * close the viewer
     */
    /**
     * close the viewer
     * @return {?}
     */
    ViewerComponent.prototype.close = /**
     * close the viewer
     * @return {?}
     */
    function () {
        if (this.otherMenu) {
            this.otherMenu.hidden = false;
        }
        this.showViewer = false;
        this.showViewerChange.emit(this.showViewer);
    };
    /**
     * get File name from url
     *
     * @param  url - url file
     */
    /**
     * get File name from url
     *
     * @param {?} url - url file
     * @return {?}
     */
    ViewerComponent.prototype.getFilenameFromUrl = /**
     * get File name from url
     *
     * @param {?} url - url file
     * @return {?}
     */
    function (url) {
        /** @type {?} */
        var anchor = url.indexOf('#');
        /** @type {?} */
        var query = url.indexOf('?');
        /** @type {?} */
        var end = Math.min(anchor > 0 ? anchor : url.length, query > 0 ? query : url.length);
        return url.substring(url.lastIndexOf('/', end) + 1, end);
    };
    /**
     * Get file extension from the string.
     * Supports the URL formats like:
     * http://localhost/test.jpg?cache=1000
     * http://localhost/test.jpg#cache=1000
     *
     * @param fileName - file name
     */
    /**
     * Get file extension from the string.
     * Supports the URL formats like:
     * http://localhost/test.jpg?cache=1000
     * http://localhost/test.jpg#cache=1000
     *
     * @param {?} fileName - file name
     * @return {?}
     */
    ViewerComponent.prototype.getFileExtension = /**
     * Get file extension from the string.
     * Supports the URL formats like:
     * http://localhost/test.jpg?cache=1000
     * http://localhost/test.jpg#cache=1000
     *
     * @param {?} fileName - file name
     * @return {?}
     */
    function (fileName) {
        if (fileName) {
            /** @type {?} */
            var match = fileName.match(/\.([^\./\?\#]+)($|\?|\#)/);
            return match ? match[1] : null;
        }
        return null;
    };
    /**
     * @param {?} extension
     * @return {?}
     */
    ViewerComponent.prototype.isCustomViewerExtension = /**
     * @param {?} extension
     * @return {?}
     */
    function (extension) {
        /** @type {?} */
        var extensions = this.externalExtensions || [];
        if (extension && extensions.length > 0) {
            extension = extension.toLowerCase();
            return extensions.flat().indexOf(extension) >= 0;
        }
        return false;
    };
    /**
     * Keyboard event listener
     * @param  event
     */
    /**
     * Keyboard event listener
     * @param {?} event
     * @return {?}
     */
    ViewerComponent.prototype.handleKeyboardEvent = /**
     * Keyboard event listener
     * @param {?} event
     * @return {?}
     */
    function (event) {
        /** @type {?} */
        var key = event.keyCode;
        // Esc
        if (key === 27 && this.overlayMode) { // esc
            this.close();
        }
        // Left arrow
        if (key === 37 && this.canNavigateBefore) {
            event.preventDefault();
            this.onNavigateBeforeClick();
        }
        // Right arrow
        if (key === 39 && this.canNavigateNext) {
            event.preventDefault();
            this.onNavigateNextClick();
        }
        // Ctrl+F
        if (key === 70 && event.ctrlKey) {
            event.preventDefault();
            this.enterFullScreen();
        }
    };
    /**
     * @return {?}
     */
    ViewerComponent.prototype.printContent = /**
     * @return {?}
     */
    function () {
        if (this.allowPrint) {
            /** @type {?} */
            var args = new BaseEvent();
            this.print.next(args);
            if (!args.defaultPrevented) {
                this.viewUtils.printFileGeneric(this.nodeId, this.mimeType);
            }
        }
    };
    /**
     * Triggers full screen mode with a main content area displayed.
     */
    /**
     * Triggers full screen mode with a main content area displayed.
     * @return {?}
     */
    ViewerComponent.prototype.enterFullScreen = /**
     * Triggers full screen mode with a main content area displayed.
     * @return {?}
     */
    function () {
        if (this.allowFullScreen) {
            /** @type {?} */
            var container = this.el.nativeElement.querySelector('.adf-viewer__fullscreen-container');
            if (container) {
                if (container.requestFullscreen) {
                    container.requestFullscreen();
                }
                else if (container.webkitRequestFullscreen) {
                    container.webkitRequestFullscreen();
                }
                else if (container.mozRequestFullScreen) {
                    container.mozRequestFullScreen();
                }
                else if (container.msRequestFullscreen) {
                    container.msRequestFullscreen();
                }
            }
        }
    };
    /**
     * @private
     * @param {?} nodeId
     * @return {?}
     */
    ViewerComponent.prototype.displayNodeRendition = /**
     * @private
     * @param {?} nodeId
     * @return {?}
     */
    function (nodeId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var rendition, renditionId, err_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.resolveRendition(nodeId, 'pdf')];
                    case 1:
                        rendition = _a.sent();
                        if (rendition) {
                            renditionId = rendition.entry.id;
                            if (renditionId === 'pdf') {
                                this.viewerType = 'pdf';
                            }
                            else if (renditionId === 'imgpreview') {
                                this.viewerType = 'image';
                            }
                            this.urlFileContent = this.apiService.contentApi.getRenditionUrl(nodeId, renditionId);
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        err_1 = _a.sent();
                        this.logService.error(err_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} sharedId
     * @return {?}
     */
    ViewerComponent.prototype.displaySharedLinkRendition = /**
     * @private
     * @param {?} sharedId
     * @return {?}
     */
    function (sharedId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var rendition, error_1, rendition, error_2;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 7]);
                        return [4 /*yield*/, this.apiService.renditionsApi.getSharedLinkRendition(sharedId, 'pdf')];
                    case 1:
                        rendition = _a.sent();
                        if (rendition.entry.status.toString() === 'CREATED') {
                            this.viewerType = 'pdf';
                            this.urlFileContent = this.apiService.contentApi.getSharedLinkRenditionUrl(sharedId, 'pdf');
                        }
                        return [3 /*break*/, 7];
                    case 2:
                        error_1 = _a.sent();
                        this.logService.error(error_1);
                        _a.label = 3;
                    case 3:
                        _a.trys.push([3, 5, , 6]);
                        return [4 /*yield*/, this.apiService.renditionsApi.getSharedLinkRendition(sharedId, 'imgpreview')];
                    case 4:
                        rendition = _a.sent();
                        if (rendition.entry.status.toString() === 'CREATED') {
                            this.viewerType = 'image';
                            this.urlFileContent = this.apiService.contentApi.getSharedLinkRenditionUrl(sharedId, 'imgpreview');
                        }
                        return [3 /*break*/, 6];
                    case 5:
                        error_2 = _a.sent();
                        this.logService.error(error_2);
                        return [3 /*break*/, 6];
                    case 6: return [3 /*break*/, 7];
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} nodeId
     * @param {?} renditionId
     * @return {?}
     */
    ViewerComponent.prototype.resolveRendition = /**
     * @private
     * @param {?} nodeId
     * @param {?} renditionId
     * @return {?}
     */
    function (nodeId, renditionId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var supportedRendition, rendition, status_1, err_2;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        renditionId = renditionId.toLowerCase();
                        return [4 /*yield*/, this.apiService.renditionsApi.getRenditions(nodeId)];
                    case 1:
                        supportedRendition = _a.sent();
                        rendition = supportedRendition.list.entries.find((/**
                         * @param {?} renditionEntry
                         * @return {?}
                         */
                        function (renditionEntry) { return renditionEntry.entry.id.toLowerCase() === renditionId; }));
                        if (!rendition) {
                            renditionId = 'imgpreview';
                            rendition = supportedRendition.list.entries.find((/**
                             * @param {?} renditionEntry
                             * @return {?}
                             */
                            function (renditionEntry) { return renditionEntry.entry.id.toLowerCase() === renditionId; }));
                        }
                        if (!rendition) return [3 /*break*/, 6];
                        status_1 = rendition.entry.status.toString();
                        if (!(status_1 === 'NOT_CREATED')) return [3 /*break*/, 6];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 5, , 6]);
                        return [4 /*yield*/, this.apiService.renditionsApi.createRendition(nodeId, { id: renditionId }).then((/**
                             * @return {?}
                             */
                            function () {
                                _this.viewerType = 'in_creation';
                            }))];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, this.waitRendition(nodeId, renditionId)];
                    case 4:
                        rendition = _a.sent();
                        return [3 /*break*/, 6];
                    case 5:
                        err_2 = _a.sent();
                        this.logService.error(err_2);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/, rendition];
                }
            });
        });
    };
    /**
     * @private
     * @param {?} nodeId
     * @param {?} renditionId
     * @return {?}
     */
    ViewerComponent.prototype.waitRendition = /**
     * @private
     * @param {?} nodeId
     * @param {?} renditionId
     * @return {?}
     */
    function (nodeId, renditionId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var currentRetry;
            var _this = this;
            return tslib_1.__generator(this, function (_a) {
                currentRetry = 0;
                return [2 /*return*/, new Promise((/**
                     * @param {?} resolve
                     * @param {?} reject
                     * @return {?}
                     */
                    function (resolve, reject) {
                        /** @type {?} */
                        var intervalId = setInterval((/**
                         * @return {?}
                         */
                        function () {
                            currentRetry++;
                            if (_this.maxRetries >= currentRetry) {
                                _this.apiService.renditionsApi.getRendition(nodeId, renditionId).then((/**
                                 * @param {?} rendition
                                 * @return {?}
                                 */
                                function (rendition) {
                                    /** @type {?} */
                                    var status = rendition.entry.status.toString();
                                    if (status === 'CREATED') {
                                        if (renditionId === 'pdf') {
                                            _this.viewerType = 'pdf';
                                        }
                                        else if (renditionId === 'imgpreview') {
                                            _this.viewerType = 'image';
                                        }
                                        _this.urlFileContent = _this.apiService.contentApi.getRenditionUrl(nodeId, renditionId);
                                        clearInterval(intervalId);
                                        return resolve(rendition);
                                    }
                                }), (/**
                                 * @return {?}
                                 */
                                function () {
                                    _this.viewerType = 'error_in_creation';
                                    return reject();
                                }));
                            }
                            else {
                                _this.isLoading = false;
                                _this.viewerType = 'error_in_creation';
                                clearInterval(intervalId);
                            }
                        }), _this.TRY_TIMEOUT);
                    }))];
            });
        });
    };
    /**
     * @param {?} extensionAllowed
     * @return {?}
     */
    ViewerComponent.prototype.checkExtensions = /**
     * @param {?} extensionAllowed
     * @return {?}
     */
    function (extensionAllowed) {
        var _this = this;
        if (typeof extensionAllowed === 'string') {
            return this.extension.toLowerCase() === extensionAllowed.toLowerCase();
        }
        else if (extensionAllowed.length > 0) {
            return extensionAllowed.find((/**
             * @param {?} currentExtension
             * @return {?}
             */
            function (currentExtension) {
                return _this.extension.toLowerCase() === currentExtension.toLowerCase();
            }));
        }
    };
    /**
     * @private
     * @return {?}
     */
    ViewerComponent.prototype.generateCacheBusterNumber = /**
     * @private
     * @return {?}
     */
    function () {
        this.cacheBusterNumber = Date.now();
    };
    ViewerComponent.decorators = [
        { type: Component, args: [{
                    selector: 'adf-viewer',
                    template: "<div *ngIf=\"showViewer\"\r\n     class=\"adf-viewer-container\"\r\n     [class.adf-viewer-overlay-container]=\"overlayMode\"\r\n     [class.adf-viewer-inline-container]=\"!overlayMode\">\r\n\r\n    <div class=\"adf-viewer-content\" fxLayout=\"column\">\r\n        <ng-content select=\"adf-viewer-toolbar\"></ng-content>\r\n        <ng-container *ngIf=\"showToolbar && !toolbar\">\r\n            <adf-toolbar color=\"default\" id=\"adf-viewer-toolbar\" class=\"adf-viewer-toolbar\">\r\n\r\n                <adf-toolbar-title>\r\n\r\n                    <ng-container *ngIf=\"allowLeftSidebar\">\r\n                        <button\r\n                            mat-icon-button\r\n                            title=\"{{ 'ADF_VIEWER.ACTIONS.INFO' | translate }}\"\r\n                            data-automation-id=\"adf-toolbar-left-sidebar\"\r\n                            [color]=\"showLeftSidebar ? 'accent' : 'default'\"\r\n                            (click)=\"toggleLeftSidebar()\">\r\n                            <mat-icon>info_outline</mat-icon>\r\n                        </button>\r\n                    </ng-container>\r\n\r\n                    <button *ngIf=\"allowGoBack\"\r\n                            class=\"adf-viewer-close-button\"\r\n                            data-automation-id=\"adf-toolbar-back\"\r\n                            mat-icon-button\r\n                            title=\"{{ 'ADF_VIEWER.ACTIONS.CLOSE' | translate }}\"\r\n                            (click)=\"onBackButtonClick()\">\r\n                        <mat-icon>close</mat-icon>\r\n                    </button>\r\n                </adf-toolbar-title>\r\n\r\n                <div fxFlex=\"1 1 auto\" class=\"adf-viewer__file-title\">\r\n                    <button\r\n                        *ngIf=\"allowNavigate && canNavigateBefore\"\r\n                        data-automation-id=\"adf-toolbar-pref-file\"\r\n                        mat-icon-button\r\n                        title=\"{{ 'ADF_VIEWER.ACTIONS.PREV_FILE' | translate }}\"\r\n                        (click)=\"onNavigateBeforeClick()\">\r\n                        <mat-icon>navigate_before</mat-icon>\r\n                    </button>\r\n                    <img class=\"adf-viewer__mimeicon\" [alt]=\"mimeType | adfMimeTypeIcon\" [src]=\"mimeType | adfMimeTypeIcon\" data-automation-id=\"adf-file-thumbnail\">\r\n                    <span class=\"adf-viewer__display-name\" id=\"adf-viewer-display-name\">{{ fileTitle }}</span>\r\n                    <button\r\n                        *ngIf=\"allowNavigate && canNavigateNext\"\r\n                        data-automation-id=\"adf-toolbar-next-file\"\r\n                        mat-icon-button\r\n                        title=\"{{ 'ADF_VIEWER.ACTIONS.NEXT_FILE' | translate }}\"\r\n                        (click)=\"onNavigateNextClick()\">\r\n                        <mat-icon>navigate_next</mat-icon>\r\n                    </button>\r\n                </div>\r\n\r\n                <ng-content select=\"adf-viewer-toolbar-actions\"></ng-content>\r\n\r\n                <ng-container *ngIf=\"mnuOpenWith\" data-automation-id='adf-toolbar-custom-btn'>\r\n                    <button\r\n                        id=\"adf-viewer-openwith\"\r\n                        mat-button\r\n                        [matMenuTriggerFor]=\"mnuOpenWith\"\r\n                        data-automation-id=\"adf-toolbar-open-with\">\r\n                        <span>{{ 'ADF_VIEWER.ACTIONS.OPEN_WITH' | translate }}</span>\r\n                        <mat-icon>arrow_drop_down</mat-icon>\r\n                    </button>\r\n                    <mat-menu #mnuOpenWith=\"matMenu\" [overlapTrigger]=\"false\">\r\n                        <ng-content select=\"adf-viewer-open-with\"></ng-content>\r\n                    </mat-menu>\r\n                </ng-container>\r\n\r\n                <adf-toolbar-divider></adf-toolbar-divider>\r\n\r\n                <button\r\n                    id=\"adf-viewer-download\"\r\n                    *ngIf=\"allowDownload\"\r\n                    mat-icon-button\r\n                    title=\"{{ 'ADF_VIEWER.ACTIONS.DOWNLOAD' | translate }}\"\r\n                    data-automation-id=\"adf-toolbar-download\"\r\n                    [adfNodeDownload]=\"nodeEntry\">\r\n                    <mat-icon>file_download</mat-icon>\r\n                </button>\r\n\r\n                <button\r\n                    id=\"adf-viewer-print\"\r\n                    *ngIf=\"allowPrint\"\r\n                    mat-icon-button\r\n                    title=\"{{ 'ADF_VIEWER.ACTIONS.PRINT' | translate }}\"\r\n                    data-automation-id=\"adf-toolbar-print\"\r\n                    (click)=\"printContent()\">\r\n                    <mat-icon>print</mat-icon>\r\n                </button>\r\n\r\n                <button\r\n                    id=\"adf-viewer-fullscreen\"\r\n                    *ngIf=\"viewerType !== 'media' && allowFullScreen\"\r\n                    mat-icon-button\r\n                    title=\"{{ 'ADF_VIEWER.ACTIONS.FULLSCREEN' | translate }}\"\r\n                    data-automation-id=\"adf-toolbar-fullscreen\"\r\n                    (click)=\"enterFullScreen()\">\r\n                    <mat-icon>fullscreen</mat-icon>\r\n                </button>\r\n\r\n                <ng-container *ngIf=\"allowRightSidebar\">\r\n                    <adf-toolbar-divider></adf-toolbar-divider>\r\n\r\n                    <button\r\n                        mat-icon-button\r\n                        title=\"{{ 'ADF_VIEWER.ACTIONS.INFO' | translate }}\"\r\n                        data-automation-id=\"adf-toolbar-sidebar\"\r\n                        [color]=\"showRightSidebar ? 'accent' : 'default'\"\r\n                        (click)=\"toggleSidebar()\">\r\n                        <mat-icon>info_outline</mat-icon>\r\n                    </button>\r\n\r\n                </ng-container>\r\n\r\n                <ng-container *ngIf=\"mnuMoreActions\">\r\n                    <button\r\n                        id=\"adf-viewer-moreactions\"\r\n                        mat-icon-button\r\n                        [matMenuTriggerFor]=\"mnuMoreActions\"\r\n                        title=\"{{ 'ADF_VIEWER.ACTIONS.MORE_ACTIONS' | translate }}\"\r\n                        data-automation-id=\"adf-toolbar-more-actions\">\r\n                        <mat-icon>more_vert</mat-icon>\r\n                    </button>\r\n                    <mat-menu #mnuMoreActions=\"matMenu\" [overlapTrigger]=\"false\">\r\n                        <ng-content select=\"adf-viewer-more-actions\"></ng-content>\r\n                    </mat-menu>\r\n                </ng-container>\r\n\r\n            </adf-toolbar>\r\n        </ng-container>\r\n\r\n        <div fxLayout=\"row\" fxFlex=\"1 1 auto\">\r\n            <ng-container *ngIf=\"allowRightSidebar && showRightSidebar\">\r\n                <div class=\"adf-viewer__sidebar\" [ngClass]=\"'adf-viewer__sidebar__right'\" fxFlexOrder=\"4\"  id=\"adf-right-sidebar\" >\r\n                    <ng-container *ngIf=\"sidebarRightTemplate\">\r\n                        <ng-container *ngTemplateOutlet=\"sidebarRightTemplate;context:sidebarRightTemplateContext\"></ng-container>\r\n                    </ng-container>\r\n                    <ng-content *ngIf=\"!sidebarRightTemplate\" select=\"adf-viewer-sidebar\"></ng-content>\r\n                </div>\r\n            </ng-container>\r\n\r\n            <ng-container *ngIf=\"allowLeftSidebar && showLeftSidebar\">\r\n                <div class=\"adf-viewer__sidebar\" [ngClass]=\"'adf-viewer__sidebar__left'\" fxFlexOrder=\"1\"  id=\"adf-left-sidebar\" >\r\n                    <ng-container *ngIf=\"sidebarLeftTemplate\">\r\n                        <ng-container *ngTemplateOutlet=\"sidebarLeftTemplate;context:sidebarLeftTemplateContext\"></ng-container>\r\n                    </ng-container>\r\n                    <ng-content *ngIf=\"!sidebarLeftTemplate\" select=\"adf-viewer-sidebar\"></ng-content>\r\n                </div>\r\n            </ng-container>\r\n\r\n            <div  *ngIf=\"isLoading\"  class=\"adf-viewer-main\" fxFlexOrder=\"1\" fxFlex=\"1 1 auto\">\r\n                <div class=\"adf-viewer-layout-content adf-viewer__fullscreen-container\">\r\n                    <div class=\"adf-viewer-content-container\">\r\n                        <ng-container *ngIf=\"isLoading\">\r\n                            <div class=\"adf-viewer__loading-screen\" fxFlex=\"1 1 auto\">\r\n                                <h2>{{ 'ADF_VIEWER.LOADING' | translate }}</h2>\r\n                                <div>\r\n                                    <mat-spinner></mat-spinner>\r\n                                </div>\r\n                            </div>\r\n                        </ng-container>\r\n\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n            <div  *ngIf=\"!isLoading\"  class=\"adf-viewer-main\" fxFlexOrder=\"1\" fxFlex=\"1 1 auto\">\r\n                <div class=\"adf-viewer-layout-content adf-viewer__fullscreen-container\">\r\n                    <div class=\"adf-viewer-content-container\" [ngSwitch]=\"viewerType\">\r\n\r\n                        <ng-container *ngSwitchCase=\"'pdf'\">\r\n                            <adf-pdf-viewer (close)=\"onBackButtonClick()\" [thumbnailsTemplate]=\"thumbnailsTemplate\" [allowThumbnails]=\"allowThumbnails\" [blobFile]=\"blobFile\" [urlFile]=\"urlFileContent\" [nameFile]=\"displayName\"></adf-pdf-viewer>\r\n                        </ng-container>\r\n\r\n                        <ng-container *ngSwitchCase=\"'image'\">\r\n                            <adf-img-viewer [urlFile]=\"urlFileContent\" [nameFile]=\"displayName\" [blobFile]=\"blobFile\"></adf-img-viewer>\r\n                        </ng-container>\r\n\r\n                        <ng-container *ngSwitchCase=\"'media'\">\r\n                            <adf-media-player id=\"adf-mdedia-player\" [urlFile]=\"urlFileContent\" [mimeType]=\"mimeType\" [blobFile]=\"blobFile\" [nameFile]=\"displayName\"></adf-media-player>\r\n                        </ng-container>\r\n\r\n                        <ng-container *ngSwitchCase=\"'text'\">\r\n                            <adf-txt-viewer [urlFile]=\"urlFileContent\" [blobFile]=\"blobFile\"></adf-txt-viewer>\r\n                        </ng-container>\r\n\r\n                        <ng-container *ngSwitchCase=\"'in_creation'\">\r\n                            <div class=\"adf-viewer__loading-screen\" fxFlex=\"1 1 auto\">\r\n                                <h2>{{ 'ADF_VIEWER.LOADING' | translate }}</h2>\r\n                                <div>\r\n                                    <mat-spinner></mat-spinner>\r\n                                </div>\r\n                            </div>\r\n                        </ng-container>\r\n\r\n                        <ng-container *ngSwitchCase=\"'custom'\">\r\n                            <ng-container *ngFor=\"let ext of viewerExtensions\">\r\n                                <adf-preview-extension\r\n                                    *ngIf=\"checkExtensions(ext.fileExtension)\"\r\n                                    [id]=\"ext.component\"\r\n                                    [node]=\"nodeEntry.entry\"\r\n                                    [url]=\"urlFileContent\"\r\n                                    [extension]=\"extension\"\r\n                                    [attr.data-automation-id]=\"ext.component\">\r\n                                </adf-preview-extension>\r\n                            </ng-container>\r\n\r\n                            <span class=\"adf-viewer-custom-content\" *ngFor=\"let extensionTemplate of extensionTemplates\">\r\n                                <ng-template\r\n                                    *ngIf=\"extensionTemplate.isVisible\"\r\n                                    [ngTemplateOutlet]=\"extensionTemplate.template\"\r\n                                    [ngTemplateOutletContext]=\"{ urlFileContent: urlFileContent, extension:extension }\">\r\n                                </ng-template>\r\n                            </span>\r\n                        </ng-container>\r\n\r\n                        <ng-container *ngSwitchDefault>\r\n                            <adf-viewer-unknown-format></adf-viewer-unknown-format>\r\n                        </ng-container>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n\r\n        </div>\r\n    </div>\r\n</div>\r\n",
                    host: { 'class': 'adf-viewer' },
                    encapsulation: ViewEncapsulation.None,
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    ViewerComponent.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: ViewUtilService },
        { type: LogService },
        { type: AppExtensionService },
        { type: ElementRef }
    ]; };
    ViewerComponent.propDecorators = {
        toolbar: [{ type: ContentChild, args: [ViewerToolbarComponent, { static: true },] }],
        sidebar: [{ type: ContentChild, args: [ViewerSidebarComponent, { static: true },] }],
        mnuOpenWith: [{ type: ContentChild, args: [ViewerOpenWithComponent, { static: true },] }],
        mnuMoreActions: [{ type: ContentChild, args: [ViewerMoreActionsComponent, { static: true },] }],
        urlFile: [{ type: Input }],
        urlFileViewer: [{ type: Input }],
        blobFile: [{ type: Input }],
        nodeId: [{ type: Input }],
        sharedLinkId: [{ type: Input }],
        overlayMode: [{ type: Input }],
        showViewer: [{ type: Input }],
        showToolbar: [{ type: Input }],
        displayName: [{ type: Input }],
        allowGoBack: [{ type: Input }],
        allowDownload: [{ type: Input }],
        allowPrint: [{ type: Input }],
        allowFullScreen: [{ type: Input }],
        allowNavigate: [{ type: Input }],
        canNavigateBefore: [{ type: Input }],
        canNavigateNext: [{ type: Input }],
        allowLeftSidebar: [{ type: Input }],
        allowRightSidebar: [{ type: Input }],
        allowThumbnails: [{ type: Input }],
        showRightSidebar: [{ type: Input }],
        showLeftSidebar: [{ type: Input }],
        sidebarRightTemplate: [{ type: Input }],
        sidebarLeftTemplate: [{ type: Input }],
        thumbnailsTemplate: [{ type: Input }],
        mimeType: [{ type: Input }],
        fileName: [{ type: Input }],
        maxRetries: [{ type: Input }],
        goBack: [{ type: Output }],
        print: [{ type: Output }],
        showViewerChange: [{ type: Output }],
        extensionChange: [{ type: Output }],
        navigateBefore: [{ type: Output }],
        navigateNext: [{ type: Output }],
        invalidSharedLink: [{ type: Output }],
        handleKeyboardEvent: [{ type: HostListener, args: ['document:keyup', ['$event'],] }]
    };
    return ViewerComponent;
}());
export { ViewerComponent };
if (false) {
    /** @type {?} */
    ViewerComponent.prototype.toolbar;
    /** @type {?} */
    ViewerComponent.prototype.sidebar;
    /** @type {?} */
    ViewerComponent.prototype.mnuOpenWith;
    /** @type {?} */
    ViewerComponent.prototype.mnuMoreActions;
    /**
     * If you want to load an external file that does not come from ACS you
     * can use this URL to specify where to load the file from.
     * @type {?}
     */
    ViewerComponent.prototype.urlFile;
    /**
     * Viewer to use with the `urlFile` address (`pdf`, `image`, `media`, `text`).
     * Used when `urlFile` has no filename and extension.
     * @type {?}
     */
    ViewerComponent.prototype.urlFileViewer;
    /**
     * Loads a Blob File
     * @type {?}
     */
    ViewerComponent.prototype.blobFile;
    /**
     * Node Id of the file to load.
     * @type {?}
     */
    ViewerComponent.prototype.nodeId;
    /**
     * Shared link id (to display shared file).
     * @type {?}
     */
    ViewerComponent.prototype.sharedLinkId;
    /**
     * If `true` then show the Viewer as a full page over the current content.
     * Otherwise fit inside the parent div.
     * @type {?}
     */
    ViewerComponent.prototype.overlayMode;
    /**
     * Hide or show the viewer
     * @type {?}
     */
    ViewerComponent.prototype.showViewer;
    /**
     * Hide or show the toolbar
     * @type {?}
     */
    ViewerComponent.prototype.showToolbar;
    /**
     * Specifies the name of the file when it is not available from the URL.
     * @type {?}
     */
    ViewerComponent.prototype.displayName;
    /**
     * Allows `back` navigation
     * @type {?}
     */
    ViewerComponent.prototype.allowGoBack;
    /**
     * Toggles downloading.
     * @type {?}
     */
    ViewerComponent.prototype.allowDownload;
    /**
     * Toggles printing.
     * @type {?}
     */
    ViewerComponent.prototype.allowPrint;
    /**
     * Toggles the 'Full Screen' feature.
     * @type {?}
     */
    ViewerComponent.prototype.allowFullScreen;
    /**
     * Toggles before/next navigation. You can use the arrow buttons to navigate
     * between documents in the collection.
     * @type {?}
     */
    ViewerComponent.prototype.allowNavigate;
    /**
     * Toggles the "before" ("<") button. Requires `allowNavigate` to be enabled.
     * @type {?}
     */
    ViewerComponent.prototype.canNavigateBefore;
    /**
     * Toggles the next (">") button. Requires `allowNavigate` to be enabled.
     * @type {?}
     */
    ViewerComponent.prototype.canNavigateNext;
    /**
     * Allow the left the sidebar.
     * @type {?}
     */
    ViewerComponent.prototype.allowLeftSidebar;
    /**
     * Allow the right sidebar.
     * @type {?}
     */
    ViewerComponent.prototype.allowRightSidebar;
    /**
     * Toggles PDF thumbnails.
     * @type {?}
     */
    ViewerComponent.prototype.allowThumbnails;
    /**
     * Toggles right sidebar visibility. Requires `allowRightSidebar` to be set to `true`.
     * @type {?}
     */
    ViewerComponent.prototype.showRightSidebar;
    /**
     * Toggles left sidebar visibility. Requires `allowLeftSidebar` to be set to `true`.
     * @type {?}
     */
    ViewerComponent.prototype.showLeftSidebar;
    /**
     * The template for the right sidebar. The template context contains the loaded node data.
     * @type {?}
     */
    ViewerComponent.prototype.sidebarRightTemplate;
    /**
     * The template for the left sidebar. The template context contains the loaded node data.
     * @type {?}
     */
    ViewerComponent.prototype.sidebarLeftTemplate;
    /**
     * The template for the pdf thumbnails.
     * @type {?}
     */
    ViewerComponent.prototype.thumbnailsTemplate;
    /**
     * MIME type of the file content (when not determined by the filename extension).
     * @type {?}
     */
    ViewerComponent.prototype.mimeType;
    /**
     * Content filename.
     * @type {?}
     */
    ViewerComponent.prototype.fileName;
    /**
     * Number of times the Viewer will retry fetching content Rendition.
     * There is a delay of at least one second between attempts.
     * @type {?}
     */
    ViewerComponent.prototype.maxRetries;
    /**
     * Emitted when user clicks the 'Back' button.
     * @type {?}
     */
    ViewerComponent.prototype.goBack;
    /**
     * Emitted when user clicks the 'Print' button.
     * @type {?}
     */
    ViewerComponent.prototype.print;
    /**
     * Emitted when the viewer is shown or hidden.
     * @type {?}
     */
    ViewerComponent.prototype.showViewerChange;
    /**
     * Emitted when the filename extension changes.
     * @type {?}
     */
    ViewerComponent.prototype.extensionChange;
    /**
     * Emitted when user clicks 'Navigate Before' ("<") button.
     * @type {?}
     */
    ViewerComponent.prototype.navigateBefore;
    /**
     * Emitted when user clicks 'Navigate Next' (">") button.
     * @type {?}
     */
    ViewerComponent.prototype.navigateNext;
    /**
     * Emitted when the shared link used is not valid.
     * @type {?}
     */
    ViewerComponent.prototype.invalidSharedLink;
    /** @type {?} */
    ViewerComponent.prototype.TRY_TIMEOUT;
    /** @type {?} */
    ViewerComponent.prototype.viewerType;
    /** @type {?} */
    ViewerComponent.prototype.isLoading;
    /** @type {?} */
    ViewerComponent.prototype.nodeEntry;
    /** @type {?} */
    ViewerComponent.prototype.extensionTemplates;
    /** @type {?} */
    ViewerComponent.prototype.externalExtensions;
    /** @type {?} */
    ViewerComponent.prototype.urlFileContent;
    /** @type {?} */
    ViewerComponent.prototype.otherMenu;
    /** @type {?} */
    ViewerComponent.prototype.extension;
    /** @type {?} */
    ViewerComponent.prototype.sidebarRightTemplateContext;
    /** @type {?} */
    ViewerComponent.prototype.sidebarLeftTemplateContext;
    /** @type {?} */
    ViewerComponent.prototype.fileTitle;
    /** @type {?} */
    ViewerComponent.prototype.viewerExtensions;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.cacheBusterNumber;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.subscriptions;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.extensions;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.mimeTypes;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.viewUtils;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.logService;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.extensionService;
    /**
     * @type {?}
     * @private
     */
    ViewerComponent.prototype.el;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlld2VyLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiJuZzovL0BhbGZyZXNjby9hZGYtY29yZS8iLCJzb3VyY2VzIjpbInZpZXdlci9jb21wb25lbnRzL3ZpZXdlci5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW1DQSxPQUFPLEVBQ0gsU0FBUyxFQUNULFlBQVksRUFDWixVQUFVLEVBQ1YsWUFBWSxFQUNaLFlBQVksRUFDWixLQUFLLEVBSUwsTUFBTSxFQUVOLFdBQVcsRUFDWCxpQkFBaUIsRUFDcEIsTUFBTSxlQUFlLENBQUM7QUFFdkIsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGNBQWMsQ0FBQztBQUN6QyxPQUFPLEVBQUUsa0JBQWtCLEVBQUUsTUFBTSxxQ0FBcUMsQ0FBQztBQUN6RSxPQUFPLEVBQUUsVUFBVSxFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDeEQsT0FBTyxFQUFFLDBCQUEwQixFQUFFLE1BQU0saUNBQWlDLENBQUM7QUFDN0UsT0FBTyxFQUFFLHVCQUF1QixFQUFFLE1BQU0sOEJBQThCLENBQUM7QUFDdkUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFDcEUsT0FBTyxFQUFFLHNCQUFzQixFQUFFLE1BQU0sNEJBQTRCLENBQUM7QUFFcEUsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQ2hFLE9BQU8sRUFBRSxtQkFBbUIsRUFBc0IsTUFBTSwwQkFBMEIsQ0FBQztBQUVuRjtJQTRNSSx5QkFBb0IsVUFBOEIsRUFDOUIsU0FBMEIsRUFDMUIsVUFBc0IsRUFDdEIsZ0JBQXFDLEVBQ3JDLEVBQWM7UUFKZCxlQUFVLEdBQVYsVUFBVSxDQUFvQjtRQUM5QixjQUFTLEdBQVQsU0FBUyxDQUFpQjtRQUMxQixlQUFVLEdBQVYsVUFBVSxDQUFZO1FBQ3RCLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBcUI7UUFDckMsT0FBRSxHQUFGLEVBQUUsQ0FBWTs7Ozs7UUF2TGxDLFlBQU8sR0FBRyxFQUFFLENBQUM7Ozs7O1FBTWIsa0JBQWEsR0FBVyxJQUFJLENBQUM7Ozs7UUFRN0IsV0FBTSxHQUFXLElBQUksQ0FBQzs7OztRQUl0QixpQkFBWSxHQUFXLElBQUksQ0FBQzs7Ozs7UUFNNUIsZ0JBQVcsR0FBRyxLQUFLLENBQUM7Ozs7UUFJcEIsZUFBVSxHQUFHLElBQUksQ0FBQzs7OztRQUlsQixnQkFBVyxHQUFHLElBQUksQ0FBQzs7Ozs7UUFTbkIsZ0JBQVcsR0FBRyxJQUFJLENBQUM7Ozs7UUFJbkIsa0JBQWEsR0FBRyxJQUFJLENBQUM7Ozs7UUFJckIsZUFBVSxHQUFHLEtBQUssQ0FBQzs7OztRQUluQixvQkFBZSxHQUFHLElBQUksQ0FBQzs7Ozs7UUFNdkIsa0JBQWEsR0FBRyxLQUFLLENBQUM7Ozs7UUFJdEIsc0JBQWlCLEdBQUcsSUFBSSxDQUFDOzs7O1FBSXpCLG9CQUFlLEdBQUcsSUFBSSxDQUFDOzs7O1FBSXZCLHFCQUFnQixHQUFHLEtBQUssQ0FBQzs7OztRQUl6QixzQkFBaUIsR0FBRyxLQUFLLENBQUM7Ozs7UUFJMUIsb0JBQWUsR0FBRyxJQUFJLENBQUM7Ozs7UUFJdkIscUJBQWdCLEdBQUcsS0FBSyxDQUFDOzs7O1FBSXpCLG9CQUFlLEdBQUcsS0FBSyxDQUFDOzs7O1FBSXhCLHlCQUFvQixHQUFxQixJQUFJLENBQUM7Ozs7UUFJOUMsd0JBQW1CLEdBQXFCLElBQUksQ0FBQzs7OztRQUk3Qyx1QkFBa0IsR0FBcUIsSUFBSSxDQUFDOzs7OztRQWM1QyxlQUFVLEdBQUcsRUFBRSxDQUFDOzs7O1FBSWhCLFdBQU0sR0FBRyxJQUFJLFlBQVksRUFBa0IsQ0FBQzs7OztRQUk1QyxVQUFLLEdBQUcsSUFBSSxZQUFZLEVBQWtCLENBQUM7Ozs7UUFJM0MscUJBQWdCLEdBQUcsSUFBSSxZQUFZLEVBQVcsQ0FBQzs7OztRQUkvQyxvQkFBZSxHQUFHLElBQUksWUFBWSxFQUFVLENBQUM7Ozs7UUFJN0MsbUJBQWMsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDOzs7O1FBSXBDLGlCQUFZLEdBQUcsSUFBSSxZQUFZLEVBQUUsQ0FBQzs7OztRQUlsQyxzQkFBaUIsR0FBRyxJQUFJLFlBQVksRUFBRSxDQUFDO1FBRXZDLGdCQUFXLEdBQVcsSUFBSSxDQUFDO1FBRTNCLGVBQVUsR0FBRyxTQUFTLENBQUM7UUFDdkIsY0FBUyxHQUFHLEtBQUssQ0FBQztRQUdsQix1QkFBa0IsR0FBeUQsRUFBRSxDQUFDO1FBQzlFLHVCQUFrQixHQUFhLEVBQUUsQ0FBQztRQUlsQyxnQ0FBMkIsR0FBbUIsRUFBQyxJQUFJLEVBQUUsSUFBSSxFQUFDLENBQUM7UUFDM0QsK0JBQTBCLEdBQW1CLEVBQUMsSUFBSSxFQUFFLElBQUksRUFBQyxDQUFDO1FBRTFELHFCQUFnQixHQUE4QixFQUFFLENBQUM7UUFJekMsa0JBQWEsR0FBbUIsRUFBRSxDQUFDOztRQUduQyxlQUFVLEdBQUc7WUFDakIsS0FBSyxFQUFFLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxNQUFNLEVBQUUsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLENBQUM7WUFDbEQsS0FBSyxFQUFFLENBQUMsS0FBSyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsTUFBTSxFQUFFLEtBQUssQ0FBQztZQUMzQyxJQUFJLEVBQUUsQ0FBQyxLQUFLLEVBQUUsS0FBSyxFQUFFLE1BQU0sRUFBRSxNQUFNLEVBQUUsSUFBSSxFQUFFLEtBQUssRUFBRSxJQUFJLENBQUM7WUFDdkQsR0FBRyxFQUFFLENBQUMsS0FBSyxDQUFDO1NBQ2YsQ0FBQzs7UUFHTSxjQUFTLEdBQUc7WUFDaEIsSUFBSSxFQUFFLENBQUMsWUFBWSxFQUFFLFVBQVUsRUFBRSxVQUFVLEVBQUUsV0FBVyxFQUFFLDBCQUEwQixDQUFDO1lBQ3JGLEdBQUcsRUFBRSxDQUFDLGlCQUFpQixDQUFDO1lBQ3hCLEtBQUssRUFBRSxDQUFDLFdBQVcsRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLFdBQVcsRUFBRSxlQUFlLENBQUM7WUFDN0UsS0FBSyxFQUFFLENBQUMsV0FBVyxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUUsWUFBWSxFQUFFLFdBQVcsRUFBRSxXQUFXLENBQUM7U0FDMUYsQ0FBQztJQU9GLENBQUM7Ozs7SUFFRCx5Q0FBZTs7O0lBQWY7UUFDSSxPQUFPLENBQUMsSUFBSSxDQUFDLE9BQU8sSUFBSSxJQUFJLENBQUMsUUFBUSxJQUFJLElBQUksQ0FBQyxNQUFNLElBQUksSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQztJQUM5RixDQUFDOzs7O0lBRUQsa0NBQVE7OztJQUFSO1FBQUEsaUJBTUM7UUFMRyxJQUFJLENBQUMsYUFBYSxDQUFDLElBQUksQ0FDbkIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLENBQUMsU0FBUzs7OztRQUFDLFVBQUMsSUFBSSxJQUFLLE9BQUEsS0FBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsRUFBeEIsQ0FBd0IsRUFBQyxDQUM1RSxDQUFDO1FBRUYsSUFBSSxDQUFDLGNBQWMsRUFBRSxDQUFDO0lBQzFCLENBQUM7Ozs7O0lBRU8sd0NBQWM7Ozs7SUFBdEI7UUFBQSxpQkFNQztRQUxHLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxJQUFJLENBQUMsZ0JBQWdCLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztRQUNwRSxJQUFJLENBQUMsZ0JBQWdCO2FBQ2hCLE9BQU87Ozs7UUFBQyxVQUFDLFNBQTZCO1lBQ25DLEtBQUksQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLGFBQWEsQ0FBQyxDQUFDO1FBQzFELENBQUMsRUFBQyxDQUFDO0lBQ1gsQ0FBQzs7OztJQUVELHFDQUFXOzs7SUFBWDtRQUNJLElBQUksQ0FBQyxhQUFhLENBQUMsT0FBTzs7OztRQUFDLFVBQUMsWUFBWSxJQUFLLE9BQUEsWUFBWSxDQUFDLFdBQVcsRUFBRSxFQUExQixDQUEwQixFQUFDLENBQUM7UUFDekUsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLENBQUM7SUFDNUIsQ0FBQzs7Ozs7O0lBRU8sdUNBQWE7Ozs7O0lBQXJCLFVBQXNCLElBQVU7UUFBaEMsaUJBUUM7UUFQRyxJQUFJLElBQUksSUFBSSxJQUFJLENBQUMsRUFBRSxLQUFLLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDakMsSUFBSSxDQUFDLHlCQUF5QixFQUFFLENBQUM7WUFDakMsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFDdEIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsQ0FBQyxJQUFJOzs7WUFBQztnQkFDMUIsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7WUFDM0IsQ0FBQyxFQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7Ozs7O0lBRUQscUNBQVc7Ozs7SUFBWCxVQUFZLE9BQXNCO1FBQWxDLGlCQXlDQztRQXhDRyxJQUFJLElBQUksQ0FBQyxVQUFVLEVBQUU7WUFDakIsSUFBSSxDQUFDLElBQUksQ0FBQyxlQUFlLEVBQUUsRUFBRTtnQkFDekIsTUFBTSxJQUFJLEtBQUssQ0FBQyw4Q0FBOEMsQ0FBQyxDQUFDO2FBQ25FO1lBQ0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7WUFFdEIsSUFBSSxJQUFJLENBQUMsUUFBUSxFQUFFO2dCQUNmLElBQUksQ0FBQyxhQUFhLEVBQUUsQ0FBQztnQkFDckIsSUFBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7YUFDMUI7aUJBQU0sSUFBSSxJQUFJLENBQUMsT0FBTyxFQUFFO2dCQUNyQixJQUFJLENBQUMsWUFBWSxFQUFFLENBQUM7Z0JBQ3BCLElBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO2FBQzFCO2lCQUFNLElBQUksSUFBSSxDQUFDLE1BQU0sRUFBRTtnQkFDcEIsSUFBSSxDQUFDLFVBQVUsQ0FBQyxRQUFRLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBQyxPQUFPLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFDLENBQUMsQ0FBQyxJQUFJOzs7O2dCQUNsRixVQUFDLElBQWU7b0JBQ1osS0FBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUM7b0JBQ3RCLEtBQUksQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEtBQUssQ0FBQyxDQUFDLElBQUk7OztvQkFBQzt3QkFDaEMsS0FBSSxDQUFDLFNBQVMsR0FBRyxLQUFLLENBQUM7b0JBQzNCLENBQUMsRUFBQyxDQUFDO2dCQUNQLENBQUM7Ozs7Z0JBQ0QsVUFBQyxLQUFLO29CQUNGLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO29CQUN2QixLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQywwQkFBMEIsQ0FBQyxDQUFDO2dCQUN0RCxDQUFDLEVBQ0osQ0FBQzthQUNMO2lCQUFNLElBQUksSUFBSSxDQUFDLFlBQVksRUFBRTtnQkFDMUIsSUFBSSxDQUFDLFdBQVcsR0FBRyxLQUFLLENBQUM7Z0JBRXpCLElBQUksQ0FBQyxVQUFVLENBQUMsY0FBYyxDQUFDLGFBQWEsQ0FBQyxJQUFJLENBQUMsWUFBWSxDQUFDLENBQUMsSUFBSTs7OztnQkFDaEUsVUFBQyxlQUFnQztvQkFDN0IsS0FBSSxDQUFDLG1CQUFtQixDQUFDLGVBQWUsQ0FBQyxDQUFDO29CQUMxQyxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztnQkFDM0IsQ0FBQzs7O2dCQUNEO29CQUNJLEtBQUksQ0FBQyxTQUFTLEdBQUcsS0FBSyxDQUFDO29CQUN2QixLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxnQ0FBZ0MsQ0FBQyxDQUFDO29CQUN4RCxLQUFJLENBQUMsaUJBQWlCLENBQUMsSUFBSSxFQUFFLENBQUM7Z0JBQ2xDLENBQUMsRUFBQyxDQUFDO2FBQ1Y7U0FDSjtJQUNMLENBQUM7Ozs7O0lBRU8sdUNBQWE7Ozs7SUFBckI7UUFDSSxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxjQUFjLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDaEQsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsUUFBUSxDQUFDLElBQUksQ0FBQztRQUNuQyxJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7UUFFOUQsSUFBSSxDQUFDLGFBQWEsR0FBRyxLQUFLLENBQUM7UUFDM0IsMERBQTBEO1FBRTFELElBQUksQ0FBQyxlQUFlLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxRQUFRLENBQUMsQ0FBQztRQUN6QyxJQUFJLENBQUMsU0FBUyxFQUFFLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFTyxzQ0FBWTs7OztJQUFwQjs7WUFDVSxlQUFlLEdBQUcsSUFBSSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7UUFDN0QsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3RELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLGVBQWUsQ0FBQyxDQUFDO1FBQ3hELElBQUksQ0FBQyxjQUFjLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQztRQUVuQyxJQUFJLENBQUMsUUFBUSxHQUFHLElBQUksQ0FBQyxXQUFXLENBQUM7UUFFakMsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsYUFBYSxJQUFJLElBQUksQ0FBQyx3QkFBd0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDdEYsSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLFNBQVMsRUFBRTtZQUMvQixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7U0FDakU7UUFFRCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7UUFDMUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ3JCLENBQUM7Ozs7OztJQUVhLHVDQUFhOzs7OztJQUEzQixVQUE0QixJQUFVOzs7O2dCQUdsQyxJQUFJLElBQUksQ0FBQyxPQUFPLEVBQUU7b0JBQ2QsSUFBSSxDQUFDLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQztpQkFDekM7Z0JBRUQsSUFBSSxDQUFDLFNBQVMsR0FBRyxJQUFJLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztnQkFFaEQsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2dCQUN4RSxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLGNBQWMsR0FBRyxHQUFHLEdBQUcsSUFBSSxDQUFDLGlCQUFpQixDQUFDLENBQUMsQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDO2dCQUV4SCxJQUFJLENBQUMsU0FBUyxHQUFHLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLENBQUM7Z0JBRWxELElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFFMUIsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO2dCQUNoRSxJQUFJLElBQUksQ0FBQyxVQUFVLEtBQUssU0FBUyxFQUFFO29CQUMvQixJQUFJLENBQUMsVUFBVSxHQUFHLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7aUJBQ2pFO2dCQUVELElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxTQUFTLEVBQUU7b0JBQy9CLFNBQVMsR0FBRyxJQUFJLENBQUMsb0JBQW9CLENBQUMsSUFBSSxDQUFDLEVBQUUsQ0FBQyxDQUFDO2lCQUNsRDtnQkFFRCxJQUFJLENBQUMsZUFBZSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7Z0JBQzFDLElBQUksQ0FBQywyQkFBMkIsQ0FBQyxJQUFJLEdBQUcsSUFBSSxDQUFDO2dCQUM3QyxJQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxHQUFHLElBQUksQ0FBQztnQkFDNUMsSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO2dCQUVqQixzQkFBTyxTQUFTLEVBQUM7OztLQUNwQjs7Ozs7O0lBRU8sNkNBQW1COzs7OztJQUEzQixVQUE0QixPQUFZO1FBQ3BDLElBQUksQ0FBQyxRQUFRLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxPQUFPLENBQUMsUUFBUSxDQUFDO1FBQy9DLElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGNBQWMsQ0FBQyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQyxDQUFDO1FBQ3pELElBQUksQ0FBQyxTQUFTLEdBQUcsSUFBSSxDQUFDLGdCQUFnQixDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDM0QsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUMsS0FBSyxDQUFDLElBQUksQ0FBQztRQUVuQyxJQUFJLENBQUMsY0FBYyxHQUFHLElBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLHVCQUF1QixDQUFDLElBQUksQ0FBQyxZQUFZLEVBQUUsS0FBSyxDQUFDLENBQUM7UUFFbkcsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsdUJBQXVCLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1FBQzlELElBQUksSUFBSSxDQUFDLFVBQVUsS0FBSyxTQUFTLEVBQUU7WUFDL0IsSUFBSSxDQUFDLFVBQVUsR0FBRyxJQUFJLENBQUMsd0JBQXdCLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO1NBQ25FO1FBRUQsSUFBSSxJQUFJLENBQUMsVUFBVSxLQUFLLFNBQVMsRUFBRTtZQUMvQixJQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxDQUFDLFlBQVksQ0FBQyxDQUFDO1NBQ3REO1FBRUQsSUFBSSxDQUFDLGVBQWUsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQyxDQUFDO0lBQzlDLENBQUM7Ozs7SUFFRCx1Q0FBYTs7O0lBQWI7UUFBQSxpQkFRQztRQVBHLElBQUksQ0FBQyxnQkFBZ0IsR0FBRyxDQUFDLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQztRQUMvQyxJQUFJLElBQUksQ0FBQyxnQkFBZ0IsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ3RDLElBQUksQ0FBQyxVQUFVLENBQUMsV0FBVyxFQUFFLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxJQUFJLENBQUMsTUFBTSxFQUFFLEVBQUMsT0FBTyxFQUFFLENBQUMscUJBQXFCLENBQUMsRUFBQyxDQUFDO2lCQUN2RixJQUFJOzs7O1lBQUMsVUFBQyxTQUFvQjtnQkFDdkIsS0FBSSxDQUFDLDJCQUEyQixDQUFDLElBQUksR0FBRyxTQUFTLENBQUMsS0FBSyxDQUFDO1lBQzVELENBQUMsRUFBQyxDQUFDO1NBQ1Y7SUFDTCxDQUFDOzs7O0lBRUQsMkNBQWlCOzs7SUFBakI7UUFBQSxpQkFRQztRQVBHLElBQUksQ0FBQyxlQUFlLEdBQUcsQ0FBQyxJQUFJLENBQUMsZUFBZSxDQUFDO1FBQzdDLElBQUksSUFBSSxDQUFDLGdCQUFnQixJQUFJLElBQUksQ0FBQyxNQUFNLEVBQUU7WUFDdEMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxXQUFXLEVBQUUsQ0FBQyxLQUFLLENBQUMsT0FBTyxDQUFDLElBQUksQ0FBQyxNQUFNLEVBQUUsRUFBQyxPQUFPLEVBQUUsQ0FBQyxxQkFBcUIsQ0FBQyxFQUFDLENBQUM7aUJBQ3ZGLElBQUk7Ozs7WUFBQyxVQUFDLFNBQW9CO2dCQUN2QixLQUFJLENBQUMsMEJBQTBCLENBQUMsSUFBSSxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUM7WUFDM0QsQ0FBQyxFQUFDLENBQUM7U0FDVjtJQUNMLENBQUM7Ozs7OztJQUVPLHdDQUFjOzs7OztJQUF0QixVQUF1QixJQUFJO1FBQ3ZCLE9BQU8sSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUM7SUFDcEMsQ0FBQzs7OztJQUVELG1DQUFTOzs7SUFBVDtRQUNJLE1BQU0sQ0FBQyxRQUFRLENBQUMsQ0FBQyxFQUFFLENBQUMsQ0FBQyxDQUFDO0lBQzFCLENBQUM7Ozs7O0lBRUQsaURBQXVCOzs7O0lBQXZCLFVBQXdCLFFBQWdCOztRQUNwQyxJQUFJLFFBQVEsRUFBRTtZQUNWLFFBQVEsR0FBRyxRQUFRLENBQUMsV0FBVyxFQUFFLENBQUM7O2dCQUU1QixXQUFXLEdBQUcsTUFBTSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDOztnQkFDL0MsS0FBbUIsSUFBQSxnQkFBQSxpQkFBQSxXQUFXLENBQUEsd0NBQUEsaUVBQUU7b0JBQTNCLElBQU0sSUFBSSx3QkFBQTtvQkFDWCxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLENBQUMsT0FBTyxDQUFDLFFBQVEsQ0FBQyxJQUFJLENBQUMsRUFBRTt3QkFDN0MsT0FBTyxJQUFJLENBQUM7cUJBQ2Y7aUJBQ0o7Ozs7Ozs7OztTQUNKO1FBQ0QsT0FBTyxTQUFTLENBQUM7SUFDckIsQ0FBQzs7Ozs7SUFFRCxrREFBd0I7Ozs7SUFBeEIsVUFBeUIsU0FBaUI7UUFDdEMsSUFBSSxTQUFTLEVBQUU7WUFDWCxTQUFTLEdBQUcsU0FBUyxDQUFDLFdBQVcsRUFBRSxDQUFDO1NBQ3ZDO1FBRUQsSUFBSSxJQUFJLENBQUMsdUJBQXVCLENBQUMsU0FBUyxDQUFDLEVBQUU7WUFDekMsT0FBTyxRQUFRLENBQUM7U0FDbkI7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDL0MsT0FBTyxPQUFPLENBQUM7U0FDbEI7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDL0MsT0FBTyxPQUFPLENBQUM7U0FDbEI7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDOUMsT0FBTyxNQUFNLENBQUM7U0FDakI7UUFFRCxJQUFJLElBQUksQ0FBQyxVQUFVLENBQUMsR0FBRyxDQUFDLE9BQU8sQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLEVBQUU7WUFDN0MsT0FBTyxLQUFLLENBQUM7U0FDaEI7UUFFRCxPQUFPLFNBQVMsQ0FBQztJQUNyQixDQUFDOzs7O0lBRUQsMkNBQWlCOzs7SUFBakI7UUFDSSxJQUFJLENBQUMsS0FBSyxFQUFFLENBQUM7SUFDakIsQ0FBQzs7OztJQUVELCtDQUFxQjs7O0lBQXJCO1FBQ0ksSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUMvQixDQUFDOzs7O0lBRUQsNkNBQW1COzs7SUFBbkI7UUFDSSxJQUFJLENBQUMsWUFBWSxDQUFDLElBQUksRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFFRDs7T0FFRzs7Ozs7SUFDSCwrQkFBSzs7OztJQUFMO1FBQ0ksSUFBSSxJQUFJLENBQUMsU0FBUyxFQUFFO1lBQ2hCLElBQUksQ0FBQyxTQUFTLENBQUMsTUFBTSxHQUFHLEtBQUssQ0FBQztTQUNqQztRQUNELElBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO1FBQ3hCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO0lBQ2hELENBQUM7SUFFRDs7OztPQUlHOzs7Ozs7O0lBQ0gsNENBQWtCOzs7Ozs7SUFBbEIsVUFBbUIsR0FBVzs7WUFDcEIsTUFBTSxHQUFHLEdBQUcsQ0FBQyxPQUFPLENBQUMsR0FBRyxDQUFDOztZQUN6QixLQUFLLEdBQUcsR0FBRyxDQUFDLE9BQU8sQ0FBQyxHQUFHLENBQUM7O1lBQ3hCLEdBQUcsR0FBRyxJQUFJLENBQUMsR0FBRyxDQUNoQixNQUFNLEdBQUcsQ0FBQyxDQUFDLENBQUMsQ0FBQyxNQUFNLENBQUMsQ0FBQyxDQUFDLEdBQUcsQ0FBQyxNQUFNLEVBQ2hDLEtBQUssR0FBRyxDQUFDLENBQUMsQ0FBQyxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsR0FBRyxDQUFDLE1BQU0sQ0FBQztRQUNuQyxPQUFPLEdBQUcsQ0FBQyxTQUFTLENBQUMsR0FBRyxDQUFDLFdBQVcsQ0FBQyxHQUFHLEVBQUUsR0FBRyxDQUFDLEdBQUcsQ0FBQyxFQUFFLEdBQUcsQ0FBQyxDQUFDO0lBQzdELENBQUM7SUFFRDs7Ozs7OztPQU9HOzs7Ozs7Ozs7O0lBQ0gsMENBQWdCOzs7Ozs7Ozs7SUFBaEIsVUFBaUIsUUFBZ0I7UUFDN0IsSUFBSSxRQUFRLEVBQUU7O2dCQUNKLEtBQUssR0FBRyxRQUFRLENBQUMsS0FBSyxDQUFDLDBCQUEwQixDQUFDO1lBQ3hELE9BQU8sS0FBSyxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLENBQUMsQ0FBQyxDQUFDLElBQUksQ0FBQztTQUNsQztRQUNELE9BQU8sSUFBSSxDQUFDO0lBQ2hCLENBQUM7Ozs7O0lBRUQsaURBQXVCOzs7O0lBQXZCLFVBQXdCLFNBQWlCOztZQUMvQixVQUFVLEdBQVEsSUFBSSxDQUFDLGtCQUFrQixJQUFJLEVBQUU7UUFFckQsSUFBSSxTQUFTLElBQUksVUFBVSxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDcEMsU0FBUyxHQUFHLFNBQVMsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUNwQyxPQUFPLFVBQVUsQ0FBQyxJQUFJLEVBQUUsQ0FBQyxPQUFPLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDO1NBQ3BEO1FBRUQsT0FBTyxLQUFLLENBQUM7SUFDakIsQ0FBQztJQUVEOzs7T0FHRzs7Ozs7O0lBRUgsNkNBQW1COzs7OztJQURuQixVQUNvQixLQUFvQjs7WUFDOUIsR0FBRyxHQUFHLEtBQUssQ0FBQyxPQUFPO1FBRXpCLE1BQU07UUFDTixJQUFJLEdBQUcsS0FBSyxFQUFFLElBQUksSUFBSSxDQUFDLFdBQVcsRUFBRSxFQUFFLE1BQU07WUFDeEMsSUFBSSxDQUFDLEtBQUssRUFBRSxDQUFDO1NBQ2hCO1FBRUQsYUFBYTtRQUNiLElBQUksR0FBRyxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsaUJBQWlCLEVBQUU7WUFDdEMsS0FBSyxDQUFDLGNBQWMsRUFBRSxDQUFDO1lBQ3ZCLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO1NBQ2hDO1FBRUQsY0FBYztRQUNkLElBQUksR0FBRyxLQUFLLEVBQUUsSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFO1lBQ3BDLEtBQUssQ0FBQyxjQUFjLEVBQUUsQ0FBQztZQUN2QixJQUFJLENBQUMsbUJBQW1CLEVBQUUsQ0FBQztTQUM5QjtRQUVELFNBQVM7UUFDVCxJQUFJLEdBQUcsS0FBSyxFQUFFLElBQUksS0FBSyxDQUFDLE9BQU8sRUFBRTtZQUM3QixLQUFLLENBQUMsY0FBYyxFQUFFLENBQUM7WUFDdkIsSUFBSSxDQUFDLGVBQWUsRUFBRSxDQUFDO1NBQzFCO0lBQ0wsQ0FBQzs7OztJQUVELHNDQUFZOzs7SUFBWjtRQUNJLElBQUksSUFBSSxDQUFDLFVBQVUsRUFBRTs7Z0JBQ1gsSUFBSSxHQUFHLElBQUksU0FBUyxFQUFFO1lBQzVCLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxDQUFDO1lBRXRCLElBQUksQ0FBQyxJQUFJLENBQUMsZ0JBQWdCLEVBQUU7Z0JBQ3hCLElBQUksQ0FBQyxTQUFTLENBQUMsZ0JBQWdCLENBQUMsSUFBSSxDQUFDLE1BQU0sRUFBRSxJQUFJLENBQUMsUUFBUSxDQUFDLENBQUM7YUFDL0Q7U0FDSjtJQUNMLENBQUM7SUFFRDs7T0FFRzs7Ozs7SUFDSCx5Q0FBZTs7OztJQUFmO1FBQ0ksSUFBSSxJQUFJLENBQUMsZUFBZSxFQUFFOztnQkFDaEIsU0FBUyxHQUFHLElBQUksQ0FBQyxFQUFFLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxtQ0FBbUMsQ0FBQztZQUMxRixJQUFJLFNBQVMsRUFBRTtnQkFDWCxJQUFJLFNBQVMsQ0FBQyxpQkFBaUIsRUFBRTtvQkFDN0IsU0FBUyxDQUFDLGlCQUFpQixFQUFFLENBQUM7aUJBQ2pDO3FCQUFNLElBQUksU0FBUyxDQUFDLHVCQUF1QixFQUFFO29CQUMxQyxTQUFTLENBQUMsdUJBQXVCLEVBQUUsQ0FBQztpQkFDdkM7cUJBQU0sSUFBSSxTQUFTLENBQUMsb0JBQW9CLEVBQUU7b0JBQ3ZDLFNBQVMsQ0FBQyxvQkFBb0IsRUFBRSxDQUFDO2lCQUNwQztxQkFBTSxJQUFJLFNBQVMsQ0FBQyxtQkFBbUIsRUFBRTtvQkFDdEMsU0FBUyxDQUFDLG1CQUFtQixFQUFFLENBQUM7aUJBQ25DO2FBQ0o7U0FDSjtJQUNMLENBQUM7Ozs7OztJQUVhLDhDQUFvQjs7Ozs7SUFBbEMsVUFBbUMsTUFBYzs7Ozs7Ozt3QkFFdkIscUJBQU0sSUFBSSxDQUFDLGdCQUFnQixDQUFDLE1BQU0sRUFBRSxLQUFLLENBQUMsRUFBQTs7d0JBQXRELFNBQVMsR0FBRyxTQUEwQzt3QkFDNUQsSUFBSSxTQUFTLEVBQUU7NEJBQ0wsV0FBVyxHQUFHLFNBQVMsQ0FBQyxLQUFLLENBQUMsRUFBRTs0QkFFdEMsSUFBSSxXQUFXLEtBQUssS0FBSyxFQUFFO2dDQUN2QixJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQzs2QkFDM0I7aUNBQU0sSUFBSSxXQUFXLEtBQUssWUFBWSxFQUFFO2dDQUNyQyxJQUFJLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQzs2QkFDN0I7NEJBRUQsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLFdBQVcsQ0FBQyxDQUFDO3lCQUN6Rjs7Ozt3QkFFRCxJQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxLQUFHLENBQUMsQ0FBQzs7Ozs7O0tBRWxDOzs7Ozs7SUFFYSxvREFBMEI7Ozs7O0lBQXhDLFVBQXlDLFFBQWdCOzs7Ozs7O3dCQUVmLHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsRUFBRSxLQUFLLENBQUMsRUFBQTs7d0JBQXZHLFNBQVMsR0FBbUIsU0FBMkU7d0JBQzdHLElBQUksU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLEtBQUssU0FBUyxFQUFFOzRCQUNqRCxJQUFJLENBQUMsVUFBVSxHQUFHLEtBQUssQ0FBQzs0QkFDeEIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxRQUFRLEVBQUUsS0FBSyxDQUFDLENBQUM7eUJBQy9GOzs7O3dCQUVELElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLE9BQUssQ0FBQyxDQUFDOzs7O3dCQUVTLHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLHNCQUFzQixDQUFDLFFBQVEsRUFBRSxZQUFZLENBQUMsRUFBQTs7d0JBQTlHLFNBQVMsR0FBbUIsU0FBa0Y7d0JBQ3BILElBQUksU0FBUyxDQUFDLEtBQUssQ0FBQyxNQUFNLENBQUMsUUFBUSxFQUFFLEtBQUssU0FBUyxFQUFFOzRCQUNqRCxJQUFJLENBQUMsVUFBVSxHQUFHLE9BQU8sQ0FBQzs0QkFDMUIsSUFBSSxDQUFDLGNBQWMsR0FBRyxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyx5QkFBeUIsQ0FBQyxRQUFRLEVBQUUsWUFBWSxDQUFDLENBQUM7eUJBQ3RHOzs7O3dCQUVELElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLE9BQUssQ0FBQyxDQUFDOzs7Ozs7O0tBR3hDOzs7Ozs7O0lBRWEsMENBQWdCOzs7Ozs7SUFBOUIsVUFBK0IsTUFBYyxFQUFFLFdBQW1COzs7Ozs7O3dCQUM5RCxXQUFXLEdBQUcsV0FBVyxDQUFDLFdBQVcsRUFBRSxDQUFDO3dCQUVJLHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQS9GLGtCQUFrQixHQUFvQixTQUF5RDt3QkFFakcsU0FBUyxHQUFtQixrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUk7Ozs7d0JBQUMsVUFBQyxjQUE4QixJQUFLLE9BQUEsY0FBYyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsV0FBVyxFQUFFLEtBQUssV0FBVyxFQUFyRCxDQUFxRCxFQUFDO3dCQUMvSixJQUFJLENBQUMsU0FBUyxFQUFFOzRCQUNaLFdBQVcsR0FBRyxZQUFZLENBQUM7NEJBQzNCLFNBQVMsR0FBRyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDLElBQUk7Ozs7NEJBQUMsVUFBQyxjQUE4QixJQUFLLE9BQUEsY0FBYyxDQUFDLEtBQUssQ0FBQyxFQUFFLENBQUMsV0FBVyxFQUFFLEtBQUssV0FBVyxFQUFyRCxDQUFxRCxFQUFDLENBQUM7eUJBQy9JOzZCQUVHLFNBQVMsRUFBVCx3QkFBUzt3QkFDSCxXQUFpQixTQUFTLENBQUMsS0FBSyxDQUFDLE1BQU0sQ0FBQyxRQUFRLEVBQUU7NkJBRXBELENBQUEsUUFBTSxLQUFLLGFBQWEsQ0FBQSxFQUF4Qix3QkFBd0I7Ozs7d0JBRXBCLHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsRUFBQyxFQUFFLEVBQUUsV0FBVyxFQUFDLENBQUMsQ0FBQyxJQUFJOzs7NEJBQUM7Z0NBQ2hGLEtBQUksQ0FBQyxVQUFVLEdBQUcsYUFBYSxDQUFDOzRCQUNwQyxDQUFDLEVBQUMsRUFBQTs7d0JBRkYsU0FFRSxDQUFDO3dCQUNTLHFCQUFNLElBQUksQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLFdBQVcsQ0FBQyxFQUFBOzt3QkFBekQsU0FBUyxHQUFHLFNBQTZDLENBQUM7Ozs7d0JBRTFELElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUcsQ0FBQyxDQUFDOzs0QkFLdkMsc0JBQU8sU0FBUyxFQUFDOzs7O0tBQ3BCOzs7Ozs7O0lBRWEsdUNBQWE7Ozs7OztJQUEzQixVQUE0QixNQUFjLEVBQUUsV0FBbUI7Ozs7O2dCQUN2RCxZQUFZLEdBQVcsQ0FBQztnQkFDNUIsc0JBQU8sSUFBSSxPQUFPOzs7OztvQkFBaUIsVUFBQyxPQUFPLEVBQUUsTUFBTTs7NEJBQ3pDLFVBQVUsR0FBRyxXQUFXOzs7d0JBQUM7NEJBQzNCLFlBQVksRUFBRSxDQUFDOzRCQUNmLElBQUksS0FBSSxDQUFDLFVBQVUsSUFBSSxZQUFZLEVBQUU7Z0NBQ2pDLEtBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsV0FBVyxDQUFDLENBQUMsSUFBSTs7OztnQ0FBQyxVQUFDLFNBQXlCOzt3Q0FDckYsTUFBTSxHQUFXLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTtvQ0FDeEQsSUFBSSxNQUFNLEtBQUssU0FBUyxFQUFFO3dDQUV0QixJQUFJLFdBQVcsS0FBSyxLQUFLLEVBQUU7NENBQ3ZCLEtBQUksQ0FBQyxVQUFVLEdBQUcsS0FBSyxDQUFDO3lDQUMzQjs2Q0FBTSxJQUFJLFdBQVcsS0FBSyxZQUFZLEVBQUU7NENBQ3JDLEtBQUksQ0FBQyxVQUFVLEdBQUcsT0FBTyxDQUFDO3lDQUM3Qjt3Q0FFRCxLQUFJLENBQUMsY0FBYyxHQUFHLEtBQUksQ0FBQyxVQUFVLENBQUMsVUFBVSxDQUFDLGVBQWUsQ0FBQyxNQUFNLEVBQUUsV0FBVyxDQUFDLENBQUM7d0NBRXRGLGFBQWEsQ0FBQyxVQUFVLENBQUMsQ0FBQzt3Q0FDMUIsT0FBTyxPQUFPLENBQUMsU0FBUyxDQUFDLENBQUM7cUNBQzdCO2dDQUNMLENBQUM7OztnQ0FBRTtvQ0FDQyxLQUFJLENBQUMsVUFBVSxHQUFHLG1CQUFtQixDQUFDO29DQUN0QyxPQUFPLE1BQU0sRUFBRSxDQUFDO2dDQUNwQixDQUFDLEVBQUMsQ0FBQzs2QkFDTjtpQ0FBTTtnQ0FDSCxLQUFJLENBQUMsU0FBUyxHQUFHLEtBQUssQ0FBQztnQ0FDdkIsS0FBSSxDQUFDLFVBQVUsR0FBRyxtQkFBbUIsQ0FBQztnQ0FDdEMsYUFBYSxDQUFDLFVBQVUsQ0FBQyxDQUFDOzZCQUM3Qjt3QkFDTCxDQUFDLEdBQUUsS0FBSSxDQUFDLFdBQVcsQ0FBQztvQkFDeEIsQ0FBQyxFQUFDLEVBQUM7OztLQUNOOzs7OztJQUVELHlDQUFlOzs7O0lBQWYsVUFBZ0IsZ0JBQWdCO1FBQWhDLGlCQVNDO1FBUkcsSUFBSSxPQUFPLGdCQUFnQixLQUFLLFFBQVEsRUFBRTtZQUN0QyxPQUFPLElBQUksQ0FBQyxTQUFTLENBQUMsV0FBVyxFQUFFLEtBQUssZ0JBQWdCLENBQUMsV0FBVyxFQUFFLENBQUM7U0FDMUU7YUFBTSxJQUFJLGdCQUFnQixDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7WUFDcEMsT0FBTyxnQkFBZ0IsQ0FBQyxJQUFJOzs7O1lBQUMsVUFBQyxnQkFBZ0I7Z0JBQzFDLE9BQU8sS0FBSSxDQUFDLFNBQVMsQ0FBQyxXQUFXLEVBQUUsS0FBSyxnQkFBZ0IsQ0FBQyxXQUFXLEVBQUUsQ0FBQztZQUMzRSxDQUFDLEVBQUMsQ0FBQztTQUNOO0lBRUwsQ0FBQzs7Ozs7SUFFTyxtREFBeUI7Ozs7SUFBakM7UUFDSSxJQUFJLENBQUMsaUJBQWlCLEdBQUcsSUFBSSxDQUFDLEdBQUcsRUFBRSxDQUFDO0lBQ3hDLENBQUM7O2dCQTVxQkosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxZQUFZO29CQUN0Qiw0dllBQXNDO29CQUV0QyxJQUFJLEVBQUUsRUFBQyxPQUFPLEVBQUUsWUFBWSxFQUFDO29CQUM3QixhQUFhLEVBQUUsaUJBQWlCLENBQUMsSUFBSTs7aUJBQ3hDOzs7O2dCQWhCUSxrQkFBa0I7Z0JBT2xCLGVBQWU7Z0JBTmYsVUFBVTtnQkFPVixtQkFBbUI7Z0JBdEJ4QixVQUFVOzs7MEJBaUNULFlBQVksU0FBQyxzQkFBc0IsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7MEJBR25ELFlBQVksU0FBQyxzQkFBc0IsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7OEJBR25ELFlBQVksU0FBQyx1QkFBdUIsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7aUNBR3BELFlBQVksU0FBQywwQkFBMEIsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7MEJBTXZELEtBQUs7Z0NBTUwsS0FBSzsyQkFJTCxLQUFLO3lCQUlMLEtBQUs7K0JBSUwsS0FBSzs4QkFNTCxLQUFLOzZCQUlMLEtBQUs7OEJBSUwsS0FBSzs4QkFJTCxLQUFLOzhCQUtMLEtBQUs7Z0NBSUwsS0FBSzs2QkFJTCxLQUFLO2tDQUlMLEtBQUs7Z0NBTUwsS0FBSztvQ0FJTCxLQUFLO2tDQUlMLEtBQUs7bUNBSUwsS0FBSztvQ0FJTCxLQUFLO2tDQUlMLEtBQUs7bUNBSUwsS0FBSztrQ0FJTCxLQUFLO3VDQUlMLEtBQUs7c0NBSUwsS0FBSztxQ0FJTCxLQUFLOzJCQUlMLEtBQUs7MkJBSUwsS0FBSzs2QkFNTCxLQUFLO3lCQUlMLE1BQU07d0JBSU4sTUFBTTttQ0FJTixNQUFNO2tDQUlOLE1BQU07aUNBSU4sTUFBTTsrQkFJTixNQUFNO29DQUlOLE1BQU07c0NBd1ZOLFlBQVksU0FBQyxnQkFBZ0IsRUFBRSxDQUFDLFFBQVEsQ0FBQzs7SUFnTDlDLHNCQUFDO0NBQUEsQUE3cUJELElBNnFCQztTQXRxQlksZUFBZTs7O0lBRXhCLGtDQUNnQzs7SUFFaEMsa0NBQ2dDOztJQUVoQyxzQ0FDcUM7O0lBRXJDLHlDQUMyQzs7Ozs7O0lBSzNDLGtDQUNhOzs7Ozs7SUFLYix3Q0FDNkI7Ozs7O0lBRzdCLG1DQUNlOzs7OztJQUdmLGlDQUNzQjs7Ozs7SUFHdEIsdUNBQzRCOzs7Ozs7SUFLNUIsc0NBQ29COzs7OztJQUdwQixxQ0FDa0I7Ozs7O0lBR2xCLHNDQUNtQjs7Ozs7SUFHbkIsc0NBQ29COzs7OztJQUlwQixzQ0FDbUI7Ozs7O0lBR25CLHdDQUNxQjs7Ozs7SUFHckIscUNBQ21COzs7OztJQUduQiwwQ0FDdUI7Ozs7OztJQUt2Qix3Q0FDc0I7Ozs7O0lBR3RCLDRDQUN5Qjs7Ozs7SUFHekIsMENBQ3VCOzs7OztJQUd2QiwyQ0FDeUI7Ozs7O0lBR3pCLDRDQUMwQjs7Ozs7SUFHMUIsMENBQ3VCOzs7OztJQUd2QiwyQ0FDeUI7Ozs7O0lBR3pCLDBDQUN3Qjs7Ozs7SUFHeEIsK0NBQzhDOzs7OztJQUc5Qyw4Q0FDNkM7Ozs7O0lBRzdDLDZDQUM0Qzs7Ozs7SUFHNUMsbUNBQ2lCOzs7OztJQUdqQixtQ0FDaUI7Ozs7OztJQUtqQixxQ0FDZ0I7Ozs7O0lBR2hCLGlDQUM0Qzs7Ozs7SUFHNUMsZ0NBQzJDOzs7OztJQUczQywyQ0FDK0M7Ozs7O0lBRy9DLDBDQUM2Qzs7Ozs7SUFHN0MseUNBQ29DOzs7OztJQUdwQyx1Q0FDa0M7Ozs7O0lBR2xDLDRDQUN1Qzs7SUFFdkMsc0NBQTJCOztJQUUzQixxQ0FBdUI7O0lBQ3ZCLG9DQUFrQjs7SUFDbEIsb0NBQXFCOztJQUVyQiw2Q0FBOEU7O0lBQzlFLDZDQUFrQzs7SUFDbEMseUNBQXVCOztJQUN2QixvQ0FBZTs7SUFDZixvQ0FBa0I7O0lBQ2xCLHNEQUEyRDs7SUFDM0QscURBQTBEOztJQUMxRCxvQ0FBa0I7O0lBQ2xCLDJDQUFpRDs7Ozs7SUFFakQsNENBQTBCOzs7OztJQUUxQix3Q0FBMkM7Ozs7O0lBRzNDLHFDQUtFOzs7OztJQUdGLG9DQUtFOzs7OztJQUVVLHFDQUFzQzs7Ozs7SUFDdEMsb0NBQWtDOzs7OztJQUNsQyxxQ0FBOEI7Ozs7O0lBQzlCLDJDQUE2Qzs7Ozs7SUFDN0MsNkJBQXNCIiwic291cmNlc0NvbnRlbnQiOlsiLyohXHJcbiAqIEBsaWNlbnNlXHJcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXHJcbiAqXHJcbiAqIExpY2Vuc2VkIHVuZGVyIHRoZSBBcGFjaGUgTGljZW5zZSwgVmVyc2lvbiAyLjAgKHRoZSBcIkxpY2Vuc2VcIik7XHJcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cclxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XHJcbiAqXHJcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcclxuICpcclxuICogVW5sZXNzIHJlcXVpcmVkIGJ5IGFwcGxpY2FibGUgbGF3IG9yIGFncmVlZCB0byBpbiB3cml0aW5nLCBzb2Z0d2FyZVxyXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXHJcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxyXG4gKiBTZWUgdGhlIExpY2Vuc2UgZm9yIHRoZSBzcGVjaWZpYyBsYW5ndWFnZSBnb3Zlcm5pbmcgcGVybWlzc2lvbnMgYW5kXHJcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxyXG4gKi9cclxuXG5cbi8qIVxuICogQGxpY2Vuc2VcbiAqIENvcHlyaWdodCAyMDE5IEFsZnJlc2NvIFNvZnR3YXJlLCBMdGQuXG4gKlxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcbiAqIHlvdSBtYXkgbm90IHVzZSB0aGlzIGZpbGUgZXhjZXB0IGluIGNvbXBsaWFuY2Ugd2l0aCB0aGUgTGljZW5zZS5cbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxuICpcbiAqICAgICBodHRwOi8vd3d3LmFwYWNoZS5vcmcvbGljZW5zZXMvTElDRU5TRS0yLjBcbiAqXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXG4gKiBkaXN0cmlidXRlZCB1bmRlciB0aGUgTGljZW5zZSBpcyBkaXN0cmlidXRlZCBvbiBhbiBcIkFTIElTXCIgQkFTSVMsXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcbiAqIGxpbWl0YXRpb25zIHVuZGVyIHRoZSBMaWNlbnNlLlxuICovXG5cbmltcG9ydCB7XG4gICAgQ29tcG9uZW50LFxuICAgIENvbnRlbnRDaGlsZCxcbiAgICBFbGVtZW50UmVmLFxuICAgIEV2ZW50RW1pdHRlcixcbiAgICBIb3N0TGlzdGVuZXIsXG4gICAgSW5wdXQsXG4gICAgT25DaGFuZ2VzLFxuICAgIE9uRGVzdHJveSxcbiAgICBPbkluaXQsXG4gICAgT3V0cHV0LFxuICAgIFNpbXBsZUNoYW5nZXMsXG4gICAgVGVtcGxhdGVSZWYsXG4gICAgVmlld0VuY2Fwc3VsYXRpb25cbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBOb2RlLCBOb2RlRW50cnksIFJlbmRpdGlvbkVudHJ5LCBSZW5kaXRpb25QYWdpbmcsIFNoYXJlZExpbmtFbnRyeSB9IGZyb20gJ0BhbGZyZXNjby9qcy1hcGknO1xuaW1wb3J0IHsgQmFzZUV2ZW50IH0gZnJvbSAnLi4vLi4vZXZlbnRzJztcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcbmltcG9ydCB7IExvZ1NlcnZpY2UgfSBmcm9tICcuLi8uLi9zZXJ2aWNlcy9sb2cuc2VydmljZSc7XG5pbXBvcnQgeyBWaWV3ZXJNb3JlQWN0aW9uc0NvbXBvbmVudCB9IGZyb20gJy4vdmlld2VyLW1vcmUtYWN0aW9ucy5jb21wb25lbnQnO1xuaW1wb3J0IHsgVmlld2VyT3BlbldpdGhDb21wb25lbnQgfSBmcm9tICcuL3ZpZXdlci1vcGVuLXdpdGguY29tcG9uZW50JztcbmltcG9ydCB7IFZpZXdlclNpZGViYXJDb21wb25lbnQgfSBmcm9tICcuL3ZpZXdlci1zaWRlYmFyLmNvbXBvbmVudCc7XG5pbXBvcnQgeyBWaWV3ZXJUb29sYmFyQ29tcG9uZW50IH0gZnJvbSAnLi92aWV3ZXItdG9vbGJhci5jb21wb25lbnQnO1xuaW1wb3J0IHsgU3Vic2NyaXB0aW9uIH0gZnJvbSAncnhqcyc7XG5pbXBvcnQgeyBWaWV3VXRpbFNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy92aWV3LXV0aWwuc2VydmljZSc7XG5pbXBvcnQgeyBBcHBFeHRlbnNpb25TZXJ2aWNlLCBWaWV3ZXJFeHRlbnNpb25SZWYgfSBmcm9tICdAYWxmcmVzY28vYWRmLWV4dGVuc2lvbnMnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2FkZi12aWV3ZXInLFxuICAgIHRlbXBsYXRlVXJsOiAnLi92aWV3ZXIuY29tcG9uZW50Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL3ZpZXdlci5jb21wb25lbnQuc2NzcyddLFxuICAgIGhvc3Q6IHsnY2xhc3MnOiAnYWRmLXZpZXdlcid9LFxuICAgIGVuY2Fwc3VsYXRpb246IFZpZXdFbmNhcHN1bGF0aW9uLk5vbmVcbn0pXG5leHBvcnQgY2xhc3MgVmlld2VyQ29tcG9uZW50IGltcGxlbWVudHMgT25DaGFuZ2VzLCBPbkluaXQsIE9uRGVzdHJveSB7XG5cbiAgICBAQ29udGVudENoaWxkKFZpZXdlclRvb2xiYXJDb21wb25lbnQsIHtzdGF0aWM6IHRydWV9KVxuICAgIHRvb2xiYXI6IFZpZXdlclRvb2xiYXJDb21wb25lbnQ7XG5cbiAgICBAQ29udGVudENoaWxkKFZpZXdlclNpZGViYXJDb21wb25lbnQsIHtzdGF0aWM6IHRydWV9KVxuICAgIHNpZGViYXI6IFZpZXdlclNpZGViYXJDb21wb25lbnQ7XG5cbiAgICBAQ29udGVudENoaWxkKFZpZXdlck9wZW5XaXRoQ29tcG9uZW50LCB7c3RhdGljOiB0cnVlfSlcbiAgICBtbnVPcGVuV2l0aDogVmlld2VyT3BlbldpdGhDb21wb25lbnQ7XG5cbiAgICBAQ29udGVudENoaWxkKFZpZXdlck1vcmVBY3Rpb25zQ29tcG9uZW50LCB7c3RhdGljOiB0cnVlfSlcbiAgICBtbnVNb3JlQWN0aW9uczogVmlld2VyTW9yZUFjdGlvbnNDb21wb25lbnQ7XG5cbiAgICAvKiogSWYgeW91IHdhbnQgdG8gbG9hZCBhbiBleHRlcm5hbCBmaWxlIHRoYXQgZG9lcyBub3QgY29tZSBmcm9tIEFDUyB5b3VcbiAgICAgKiBjYW4gdXNlIHRoaXMgVVJMIHRvIHNwZWNpZnkgd2hlcmUgdG8gbG9hZCB0aGUgZmlsZSBmcm9tLlxuICAgICAqL1xuICAgIEBJbnB1dCgpXG4gICAgdXJsRmlsZSA9ICcnO1xuXG4gICAgLyoqIFZpZXdlciB0byB1c2Ugd2l0aCB0aGUgYHVybEZpbGVgIGFkZHJlc3MgKGBwZGZgLCBgaW1hZ2VgLCBgbWVkaWFgLCBgdGV4dGApLlxuICAgICAqIFVzZWQgd2hlbiBgdXJsRmlsZWAgaGFzIG5vIGZpbGVuYW1lIGFuZCBleHRlbnNpb24uXG4gICAgICovXG4gICAgQElucHV0KClcbiAgICB1cmxGaWxlVmlld2VyOiBzdHJpbmcgPSBudWxsO1xuXG4gICAgLyoqIExvYWRzIGEgQmxvYiBGaWxlICovXG4gICAgQElucHV0KClcbiAgICBibG9iRmlsZTogQmxvYjtcblxuICAgIC8qKiBOb2RlIElkIG9mIHRoZSBmaWxlIHRvIGxvYWQuICovXG4gICAgQElucHV0KClcbiAgICBub2RlSWQ6IHN0cmluZyA9IG51bGw7XG5cbiAgICAvKiogU2hhcmVkIGxpbmsgaWQgKHRvIGRpc3BsYXkgc2hhcmVkIGZpbGUpLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgc2hhcmVkTGlua0lkOiBzdHJpbmcgPSBudWxsO1xuXG4gICAgLyoqIElmIGB0cnVlYCB0aGVuIHNob3cgdGhlIFZpZXdlciBhcyBhIGZ1bGwgcGFnZSBvdmVyIHRoZSBjdXJyZW50IGNvbnRlbnQuXG4gICAgICogT3RoZXJ3aXNlIGZpdCBpbnNpZGUgdGhlIHBhcmVudCBkaXYuXG4gICAgICovXG4gICAgQElucHV0KClcbiAgICBvdmVybGF5TW9kZSA9IGZhbHNlO1xuXG4gICAgLyoqIEhpZGUgb3Igc2hvdyB0aGUgdmlld2VyICovXG4gICAgQElucHV0KClcbiAgICBzaG93Vmlld2VyID0gdHJ1ZTtcblxuICAgIC8qKiBIaWRlIG9yIHNob3cgdGhlIHRvb2xiYXIgKi9cbiAgICBASW5wdXQoKVxuICAgIHNob3dUb29sYmFyID0gdHJ1ZTtcblxuICAgIC8qKiBTcGVjaWZpZXMgdGhlIG5hbWUgb2YgdGhlIGZpbGUgd2hlbiBpdCBpcyBub3QgYXZhaWxhYmxlIGZyb20gdGhlIFVSTC4gKi9cbiAgICBASW5wdXQoKVxuICAgIGRpc3BsYXlOYW1lOiBzdHJpbmc7XG5cbiAgICAvKiogQGRlcHJlY2F0ZWQgMy4yLjAgKi9cbiAgICAvKiogQWxsb3dzIGBiYWNrYCBuYXZpZ2F0aW9uICovXG4gICAgQElucHV0KClcbiAgICBhbGxvd0dvQmFjayA9IHRydWU7XG5cbiAgICAvKiogVG9nZ2xlcyBkb3dubG9hZGluZy4gKi9cbiAgICBASW5wdXQoKVxuICAgIGFsbG93RG93bmxvYWQgPSB0cnVlO1xuXG4gICAgLyoqIFRvZ2dsZXMgcHJpbnRpbmcuICovXG4gICAgQElucHV0KClcbiAgICBhbGxvd1ByaW50ID0gZmFsc2U7XG5cbiAgICAvKiogVG9nZ2xlcyB0aGUgJ0Z1bGwgU2NyZWVuJyBmZWF0dXJlLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgYWxsb3dGdWxsU2NyZWVuID0gdHJ1ZTtcblxuICAgIC8qKiBUb2dnbGVzIGJlZm9yZS9uZXh0IG5hdmlnYXRpb24uIFlvdSBjYW4gdXNlIHRoZSBhcnJvdyBidXR0b25zIHRvIG5hdmlnYXRlXG4gICAgICogYmV0d2VlbiBkb2N1bWVudHMgaW4gdGhlIGNvbGxlY3Rpb24uXG4gICAgICovXG4gICAgQElucHV0KClcbiAgICBhbGxvd05hdmlnYXRlID0gZmFsc2U7XG5cbiAgICAvKiogVG9nZ2xlcyB0aGUgXCJiZWZvcmVcIiAoXCI8XCIpIGJ1dHRvbi4gUmVxdWlyZXMgYGFsbG93TmF2aWdhdGVgIHRvIGJlIGVuYWJsZWQuICovXG4gICAgQElucHV0KClcbiAgICBjYW5OYXZpZ2F0ZUJlZm9yZSA9IHRydWU7XG5cbiAgICAvKiogVG9nZ2xlcyB0aGUgbmV4dCAoXCI+XCIpIGJ1dHRvbi4gUmVxdWlyZXMgYGFsbG93TmF2aWdhdGVgIHRvIGJlIGVuYWJsZWQuICovXG4gICAgQElucHV0KClcbiAgICBjYW5OYXZpZ2F0ZU5leHQgPSB0cnVlO1xuXG4gICAgLyoqIEFsbG93IHRoZSBsZWZ0IHRoZSBzaWRlYmFyLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgYWxsb3dMZWZ0U2lkZWJhciA9IGZhbHNlO1xuXG4gICAgLyoqIEFsbG93IHRoZSByaWdodCBzaWRlYmFyLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgYWxsb3dSaWdodFNpZGViYXIgPSBmYWxzZTtcblxuICAgIC8qKiBUb2dnbGVzIFBERiB0aHVtYm5haWxzLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgYWxsb3dUaHVtYm5haWxzID0gdHJ1ZTtcblxuICAgIC8qKiBUb2dnbGVzIHJpZ2h0IHNpZGViYXIgdmlzaWJpbGl0eS4gUmVxdWlyZXMgYGFsbG93UmlnaHRTaWRlYmFyYCB0byBiZSBzZXQgdG8gYHRydWVgLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgc2hvd1JpZ2h0U2lkZWJhciA9IGZhbHNlO1xuXG4gICAgLyoqIFRvZ2dsZXMgbGVmdCBzaWRlYmFyIHZpc2liaWxpdHkuIFJlcXVpcmVzIGBhbGxvd0xlZnRTaWRlYmFyYCB0byBiZSBzZXQgdG8gYHRydWVgLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgc2hvd0xlZnRTaWRlYmFyID0gZmFsc2U7XG5cbiAgICAvKiogVGhlIHRlbXBsYXRlIGZvciB0aGUgcmlnaHQgc2lkZWJhci4gVGhlIHRlbXBsYXRlIGNvbnRleHQgY29udGFpbnMgdGhlIGxvYWRlZCBub2RlIGRhdGEuICovXG4gICAgQElucHV0KClcbiAgICBzaWRlYmFyUmlnaHRUZW1wbGF0ZTogVGVtcGxhdGVSZWY8YW55PiA9IG51bGw7XG5cbiAgICAvKiogVGhlIHRlbXBsYXRlIGZvciB0aGUgbGVmdCBzaWRlYmFyLiBUaGUgdGVtcGxhdGUgY29udGV4dCBjb250YWlucyB0aGUgbG9hZGVkIG5vZGUgZGF0YS4gKi9cbiAgICBASW5wdXQoKVxuICAgIHNpZGViYXJMZWZ0VGVtcGxhdGU6IFRlbXBsYXRlUmVmPGFueT4gPSBudWxsO1xuXG4gICAgLyoqIFRoZSB0ZW1wbGF0ZSBmb3IgdGhlIHBkZiB0aHVtYm5haWxzLiAqL1xuICAgIEBJbnB1dCgpXG4gICAgdGh1bWJuYWlsc1RlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+ID0gbnVsbDtcblxuICAgIC8qKiBNSU1FIHR5cGUgb2YgdGhlIGZpbGUgY29udGVudCAod2hlbiBub3QgZGV0ZXJtaW5lZCBieSB0aGUgZmlsZW5hbWUgZXh0ZW5zaW9uKS4gKi9cbiAgICBASW5wdXQoKVxuICAgIG1pbWVUeXBlOiBzdHJpbmc7XG5cbiAgICAvKiogQ29udGVudCBmaWxlbmFtZS4gKi9cbiAgICBASW5wdXQoKVxuICAgIGZpbGVOYW1lOiBzdHJpbmc7XG5cbiAgICAvKiogTnVtYmVyIG9mIHRpbWVzIHRoZSBWaWV3ZXIgd2lsbCByZXRyeSBmZXRjaGluZyBjb250ZW50IFJlbmRpdGlvbi5cbiAgICAgKiBUaGVyZSBpcyBhIGRlbGF5IG9mIGF0IGxlYXN0IG9uZSBzZWNvbmQgYmV0d2VlbiBhdHRlbXB0cy5cbiAgICAgKi9cbiAgICBASW5wdXQoKVxuICAgIG1heFJldHJpZXMgPSAxMDtcblxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdXNlciBjbGlja3MgdGhlICdCYWNrJyBidXR0b24uICovXG4gICAgQE91dHB1dCgpXG4gICAgZ29CYWNrID0gbmV3IEV2ZW50RW1pdHRlcjxCYXNlRXZlbnQ8YW55Pj4oKTtcblxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdXNlciBjbGlja3MgdGhlICdQcmludCcgYnV0dG9uLiAqL1xuICAgIEBPdXRwdXQoKVxuICAgIHByaW50ID0gbmV3IEV2ZW50RW1pdHRlcjxCYXNlRXZlbnQ8YW55Pj4oKTtcblxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIHZpZXdlciBpcyBzaG93biBvciBoaWRkZW4uICovXG4gICAgQE91dHB1dCgpXG4gICAgc2hvd1ZpZXdlckNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8Ym9vbGVhbj4oKTtcblxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIGZpbGVuYW1lIGV4dGVuc2lvbiBjaGFuZ2VzLiAqL1xuICAgIEBPdXRwdXQoKVxuICAgIGV4dGVuc2lvbkNoYW5nZSA9IG5ldyBFdmVudEVtaXR0ZXI8c3RyaW5nPigpO1xuXG4gICAgLyoqIEVtaXR0ZWQgd2hlbiB1c2VyIGNsaWNrcyAnTmF2aWdhdGUgQmVmb3JlJyAoXCI8XCIpIGJ1dHRvbi4gKi9cbiAgICBAT3V0cHV0KClcbiAgICBuYXZpZ2F0ZUJlZm9yZSA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdXNlciBjbGlja3MgJ05hdmlnYXRlIE5leHQnIChcIj5cIikgYnV0dG9uLiAqL1xuICAgIEBPdXRwdXQoKVxuICAgIG5hdmlnYXRlTmV4dCA9IG5ldyBFdmVudEVtaXR0ZXIoKTtcblxuICAgIC8qKiBFbWl0dGVkIHdoZW4gdGhlIHNoYXJlZCBsaW5rIHVzZWQgaXMgbm90IHZhbGlkLiAqL1xuICAgIEBPdXRwdXQoKVxuICAgIGludmFsaWRTaGFyZWRMaW5rID0gbmV3IEV2ZW50RW1pdHRlcigpO1xuXG4gICAgVFJZX1RJTUVPVVQ6IG51bWJlciA9IDIwMDA7XG5cbiAgICB2aWV3ZXJUeXBlID0gJ3Vua25vd24nO1xuICAgIGlzTG9hZGluZyA9IGZhbHNlO1xuICAgIG5vZGVFbnRyeTogTm9kZUVudHJ5O1xuXG4gICAgZXh0ZW5zaW9uVGVtcGxhdGVzOiB7IHRlbXBsYXRlOiBUZW1wbGF0ZVJlZjxhbnk+LCBpc1Zpc2libGU6IGJvb2xlYW4gfVtdID0gW107XG4gICAgZXh0ZXJuYWxFeHRlbnNpb25zOiBzdHJpbmdbXSA9IFtdO1xuICAgIHVybEZpbGVDb250ZW50OiBzdHJpbmc7XG4gICAgb3RoZXJNZW51OiBhbnk7XG4gICAgZXh0ZW5zaW9uOiBzdHJpbmc7XG4gICAgc2lkZWJhclJpZ2h0VGVtcGxhdGVDb250ZXh0OiB7IG5vZGU6IE5vZGUgfSA9IHtub2RlOiBudWxsfTtcbiAgICBzaWRlYmFyTGVmdFRlbXBsYXRlQ29udGV4dDogeyBub2RlOiBOb2RlIH0gPSB7bm9kZTogbnVsbH07XG4gICAgZmlsZVRpdGxlOiBzdHJpbmc7XG4gICAgdmlld2VyRXh0ZW5zaW9uczogQXJyYXk8Vmlld2VyRXh0ZW5zaW9uUmVmPiA9IFtdO1xuXG4gICAgcHJpdmF0ZSBjYWNoZUJ1c3Rlck51bWJlcjtcblxuICAgIHByaXZhdGUgc3Vic2NyaXB0aW9uczogU3Vic2NyaXB0aW9uW10gPSBbXTtcblxuICAgIC8vIEV4dGVuc2lvbnMgdGhhdCBhcmUgc3VwcG9ydGVkIGJ5IHRoZSBWaWV3ZXIgd2l0aG91dCBjb252ZXJzaW9uXG4gICAgcHJpdmF0ZSBleHRlbnNpb25zID0ge1xuICAgICAgICBpbWFnZTogWydwbmcnLCAnanBnJywgJ2pwZWcnLCAnZ2lmJywgJ2JwbScsICdzdmcnXSxcbiAgICAgICAgbWVkaWE6IFsnd2F2JywgJ21wNCcsICdtcDMnLCAnd2VibScsICdvZ2cnXSxcbiAgICAgICAgdGV4dDogWyd0eHQnLCAneG1sJywgJ2h0bWwnLCAnanNvbicsICd0cycsICdjc3MnLCAnbWQnXSxcbiAgICAgICAgcGRmOiBbJ3BkZiddXG4gICAgfTtcblxuICAgIC8vIE1pbWUgdHlwZXMgdGhhdCBhcmUgc3VwcG9ydGVkIGJ5IHRoZSBWaWV3ZXIgd2l0aG91dCBjb252ZXJzaW9uXG4gICAgcHJpdmF0ZSBtaW1lVHlwZXMgPSB7XG4gICAgICAgIHRleHQ6IFsndGV4dC9wbGFpbicsICd0ZXh0L2NzdicsICd0ZXh0L3htbCcsICd0ZXh0L2h0bWwnLCAnYXBwbGljYXRpb24veC1qYXZhc2NyaXB0J10sXG4gICAgICAgIHBkZjogWydhcHBsaWNhdGlvbi9wZGYnXSxcbiAgICAgICAgaW1hZ2U6IFsnaW1hZ2UvcG5nJywgJ2ltYWdlL2pwZWcnLCAnaW1hZ2UvZ2lmJywgJ2ltYWdlL2JtcCcsICdpbWFnZS9zdmcreG1sJ10sXG4gICAgICAgIG1lZGlhOiBbJ3ZpZGVvL21wNCcsICd2aWRlby93ZWJtJywgJ3ZpZGVvL29nZycsICdhdWRpby9tcGVnJywgJ2F1ZGlvL29nZycsICdhdWRpby93YXYnXVxuICAgIH07XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIGFwaVNlcnZpY2U6IEFsZnJlc2NvQXBpU2VydmljZSxcbiAgICAgICAgICAgICAgICBwcml2YXRlIHZpZXdVdGlsczogVmlld1V0aWxTZXJ2aWNlLFxuICAgICAgICAgICAgICAgIHByaXZhdGUgbG9nU2VydmljZTogTG9nU2VydmljZSxcbiAgICAgICAgICAgICAgICBwcml2YXRlIGV4dGVuc2lvblNlcnZpY2U6IEFwcEV4dGVuc2lvblNlcnZpY2UsXG4gICAgICAgICAgICAgICAgcHJpdmF0ZSBlbDogRWxlbWVudFJlZikge1xuICAgIH1cblxuICAgIGlzU291cmNlRGVmaW5lZCgpOiBib29sZWFuIHtcbiAgICAgICAgcmV0dXJuICh0aGlzLnVybEZpbGUgfHwgdGhpcy5ibG9iRmlsZSB8fCB0aGlzLm5vZGVJZCB8fCB0aGlzLnNoYXJlZExpbmtJZCkgPyB0cnVlIDogZmFsc2U7XG4gICAgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5wdXNoKFxuICAgICAgICAgICAgdGhpcy5hcGlTZXJ2aWNlLm5vZGVVcGRhdGVkLnN1YnNjcmliZSgobm9kZSkgPT4gdGhpcy5vbk5vZGVVcGRhdGVkKG5vZGUpKVxuICAgICAgICApO1xuXG4gICAgICAgIHRoaXMubG9hZEV4dGVuc2lvbnMoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGxvYWRFeHRlbnNpb25zKCkge1xuICAgICAgICB0aGlzLnZpZXdlckV4dGVuc2lvbnMgPSB0aGlzLmV4dGVuc2lvblNlcnZpY2UuZ2V0Vmlld2VyRXh0ZW5zaW9ucygpO1xuICAgICAgICB0aGlzLnZpZXdlckV4dGVuc2lvbnNcbiAgICAgICAgICAgIC5mb3JFYWNoKChleHRlbnNpb246IFZpZXdlckV4dGVuc2lvblJlZikgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuZXh0ZXJuYWxFeHRlbnNpb25zLnB1c2goZXh0ZW5zaW9uLmZpbGVFeHRlbnNpb24pO1xuICAgICAgICAgICAgfSk7XG4gICAgfVxuXG4gICAgbmdPbkRlc3Ryb3koKSB7XG4gICAgICAgIHRoaXMuc3Vic2NyaXB0aW9ucy5mb3JFYWNoKChzdWJzY3JpcHRpb24pID0+IHN1YnNjcmlwdGlvbi51bnN1YnNjcmliZSgpKTtcbiAgICAgICAgdGhpcy5zdWJzY3JpcHRpb25zID0gW107XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBvbk5vZGVVcGRhdGVkKG5vZGU6IE5vZGUpIHtcbiAgICAgICAgaWYgKG5vZGUgJiYgbm9kZS5pZCA9PT0gdGhpcy5ub2RlSWQpIHtcbiAgICAgICAgICAgIHRoaXMuZ2VuZXJhdGVDYWNoZUJ1c3Rlck51bWJlcigpO1xuICAgICAgICAgICAgdGhpcy5pc0xvYWRpbmcgPSB0cnVlO1xuICAgICAgICAgICAgdGhpcy5zZXRVcE5vZGVGaWxlKG5vZGUpLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMuaXNMb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG5nT25DaGFuZ2VzKGNoYW5nZXM6IFNpbXBsZUNoYW5nZXMpIHtcbiAgICAgICAgaWYgKHRoaXMuc2hvd1ZpZXdlcikge1xuICAgICAgICAgICAgaWYgKCF0aGlzLmlzU291cmNlRGVmaW5lZCgpKSB7XG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdBIGNvbnRlbnQgc291cmNlIGF0dHJpYnV0ZSB2YWx1ZSBpcyBtaXNzaW5nLicpO1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgdGhpcy5pc0xvYWRpbmcgPSB0cnVlO1xuXG4gICAgICAgICAgICBpZiAodGhpcy5ibG9iRmlsZSkge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0VXBCbG9iRGF0YSgpO1xuICAgICAgICAgICAgICAgIHRoaXMuaXNMb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMudXJsRmlsZSkge1xuICAgICAgICAgICAgICAgIHRoaXMuc2V0VXBVcmxGaWxlKCk7XG4gICAgICAgICAgICAgICAgdGhpcy5pc0xvYWRpbmcgPSBmYWxzZTtcbiAgICAgICAgICAgIH0gZWxzZSBpZiAodGhpcy5ub2RlSWQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmFwaVNlcnZpY2Uubm9kZXNBcGkuZ2V0Tm9kZSh0aGlzLm5vZGVJZCwge2luY2x1ZGU6IFsnYWxsb3dhYmxlT3BlcmF0aW9ucyddfSkudGhlbihcbiAgICAgICAgICAgICAgICAgICAgKG5vZGU6IE5vZGVFbnRyeSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5ub2RlRW50cnkgPSBub2RlO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRVcE5vZGVGaWxlKG5vZGUuZW50cnkpLnRoZW4oKCkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaXNMb2FkaW5nID0gZmFsc2U7XG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgfSxcbiAgICAgICAgICAgICAgICAgICAgKGVycm9yKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzTG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKCdUaGlzIG5vZGUgZG9lcyBub3QgZXhpc3QnKTtcbiAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICk7XG4gICAgICAgICAgICB9IGVsc2UgaWYgKHRoaXMuc2hhcmVkTGlua0lkKSB7XG4gICAgICAgICAgICAgICAgdGhpcy5hbGxvd0dvQmFjayA9IGZhbHNlO1xuXG4gICAgICAgICAgICAgICAgdGhpcy5hcGlTZXJ2aWNlLnNoYXJlZExpbmtzQXBpLmdldFNoYXJlZExpbmsodGhpcy5zaGFyZWRMaW5rSWQpLnRoZW4oXG4gICAgICAgICAgICAgICAgICAgIChzaGFyZWRMaW5rRW50cnk6IFNoYXJlZExpbmtFbnRyeSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5zZXRVcFNoYXJlZExpbmtGaWxlKHNoYXJlZExpbmtFbnRyeSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzTG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICB9LFxuICAgICAgICAgICAgICAgICAgICAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLmlzTG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKCdUaGlzIHNoYXJlZExpbmsgZG9lcyBub3QgZXhpc3QnKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIHRoaXMuaW52YWxpZFNoYXJlZExpbmsubmV4dCgpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgc2V0VXBCbG9iRGF0YSgpIHtcbiAgICAgICAgdGhpcy5maWxlVGl0bGUgPSB0aGlzLmdldERpc3BsYXlOYW1lKCdVbmtub3duJyk7XG4gICAgICAgIHRoaXMubWltZVR5cGUgPSB0aGlzLmJsb2JGaWxlLnR5cGU7XG4gICAgICAgIHRoaXMudmlld2VyVHlwZSA9IHRoaXMuZ2V0Vmlld2VyVHlwZUJ5TWltZVR5cGUodGhpcy5taW1lVHlwZSk7XG5cbiAgICAgICAgdGhpcy5hbGxvd0Rvd25sb2FkID0gZmFsc2U7XG4gICAgICAgIC8vIFRPRE86IHdyYXAgYmxvYiBpbnRvIHRoZSBkYXRhIHVybCBhbmQgYWxsb3cgZG93bmxvYWRpbmdcblxuICAgICAgICB0aGlzLmV4dGVuc2lvbkNoYW5nZS5lbWl0KHRoaXMubWltZVR5cGUpO1xuICAgICAgICB0aGlzLnNjcm9sbFRvcCgpO1xuICAgIH1cblxuICAgIHByaXZhdGUgc2V0VXBVcmxGaWxlKCkge1xuICAgICAgICBjb25zdCBmaWxlbmFtZUZyb21VcmwgPSB0aGlzLmdldEZpbGVuYW1lRnJvbVVybCh0aGlzLnVybEZpbGUpO1xuICAgICAgICB0aGlzLmZpbGVUaXRsZSA9IHRoaXMuZ2V0RGlzcGxheU5hbWUoZmlsZW5hbWVGcm9tVXJsKTtcbiAgICAgICAgdGhpcy5leHRlbnNpb24gPSB0aGlzLmdldEZpbGVFeHRlbnNpb24oZmlsZW5hbWVGcm9tVXJsKTtcbiAgICAgICAgdGhpcy51cmxGaWxlQ29udGVudCA9IHRoaXMudXJsRmlsZTtcblxuICAgICAgICB0aGlzLmZpbGVOYW1lID0gdGhpcy5kaXNwbGF5TmFtZTtcblxuICAgICAgICB0aGlzLnZpZXdlclR5cGUgPSB0aGlzLnVybEZpbGVWaWV3ZXIgfHwgdGhpcy5nZXRWaWV3ZXJUeXBlQnlFeHRlbnNpb24odGhpcy5leHRlbnNpb24pO1xuICAgICAgICBpZiAodGhpcy52aWV3ZXJUeXBlID09PSAndW5rbm93bicpIHtcbiAgICAgICAgICAgIHRoaXMudmlld2VyVHlwZSA9IHRoaXMuZ2V0Vmlld2VyVHlwZUJ5TWltZVR5cGUodGhpcy5taW1lVHlwZSk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmV4dGVuc2lvbkNoYW5nZS5lbWl0KHRoaXMuZXh0ZW5zaW9uKTtcbiAgICAgICAgdGhpcy5zY3JvbGxUb3AoKTtcbiAgICB9XG5cbiAgICBwcml2YXRlIGFzeW5jIHNldFVwTm9kZUZpbGUoZGF0YTogTm9kZSkge1xuICAgICAgICBsZXQgc2V0dXBOb2RlO1xuXG4gICAgICAgIGlmIChkYXRhLmNvbnRlbnQpIHtcbiAgICAgICAgICAgIHRoaXMubWltZVR5cGUgPSBkYXRhLmNvbnRlbnQubWltZVR5cGU7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmZpbGVUaXRsZSA9IHRoaXMuZ2V0RGlzcGxheU5hbWUoZGF0YS5uYW1lKTtcblxuICAgICAgICB0aGlzLnVybEZpbGVDb250ZW50ID0gdGhpcy5hcGlTZXJ2aWNlLmNvbnRlbnRBcGkuZ2V0Q29udGVudFVybChkYXRhLmlkKTtcbiAgICAgICAgdGhpcy51cmxGaWxlQ29udGVudCA9IHRoaXMuY2FjaGVCdXN0ZXJOdW1iZXIgPyB0aGlzLnVybEZpbGVDb250ZW50ICsgJyYnICsgdGhpcy5jYWNoZUJ1c3Rlck51bWJlciA6IHRoaXMudXJsRmlsZUNvbnRlbnQ7XG5cbiAgICAgICAgdGhpcy5leHRlbnNpb24gPSB0aGlzLmdldEZpbGVFeHRlbnNpb24oZGF0YS5uYW1lKTtcblxuICAgICAgICB0aGlzLmZpbGVOYW1lID0gZGF0YS5uYW1lO1xuXG4gICAgICAgIHRoaXMudmlld2VyVHlwZSA9IHRoaXMuZ2V0Vmlld2VyVHlwZUJ5RXh0ZW5zaW9uKHRoaXMuZXh0ZW5zaW9uKTtcbiAgICAgICAgaWYgKHRoaXMudmlld2VyVHlwZSA9PT0gJ3Vua25vd24nKSB7XG4gICAgICAgICAgICB0aGlzLnZpZXdlclR5cGUgPSB0aGlzLmdldFZpZXdlclR5cGVCeU1pbWVUeXBlKHRoaXMubWltZVR5cGUpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMudmlld2VyVHlwZSA9PT0gJ3Vua25vd24nKSB7XG4gICAgICAgICAgICBzZXR1cE5vZGUgPSB0aGlzLmRpc3BsYXlOb2RlUmVuZGl0aW9uKGRhdGEuaWQpO1xuICAgICAgICB9XG5cbiAgICAgICAgdGhpcy5leHRlbnNpb25DaGFuZ2UuZW1pdCh0aGlzLmV4dGVuc2lvbik7XG4gICAgICAgIHRoaXMuc2lkZWJhclJpZ2h0VGVtcGxhdGVDb250ZXh0Lm5vZGUgPSBkYXRhO1xuICAgICAgICB0aGlzLnNpZGViYXJMZWZ0VGVtcGxhdGVDb250ZXh0Lm5vZGUgPSBkYXRhO1xuICAgICAgICB0aGlzLnNjcm9sbFRvcCgpO1xuXG4gICAgICAgIHJldHVybiBzZXR1cE5vZGU7XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBzZXRVcFNoYXJlZExpbmtGaWxlKGRldGFpbHM6IGFueSkge1xuICAgICAgICB0aGlzLm1pbWVUeXBlID0gZGV0YWlscy5lbnRyeS5jb250ZW50Lm1pbWVUeXBlO1xuICAgICAgICB0aGlzLmZpbGVUaXRsZSA9IHRoaXMuZ2V0RGlzcGxheU5hbWUoZGV0YWlscy5lbnRyeS5uYW1lKTtcbiAgICAgICAgdGhpcy5leHRlbnNpb24gPSB0aGlzLmdldEZpbGVFeHRlbnNpb24oZGV0YWlscy5lbnRyeS5uYW1lKTtcbiAgICAgICAgdGhpcy5maWxlTmFtZSA9IGRldGFpbHMuZW50cnkubmFtZTtcblxuICAgICAgICB0aGlzLnVybEZpbGVDb250ZW50ID0gdGhpcy5hcGlTZXJ2aWNlLmNvbnRlbnRBcGkuZ2V0U2hhcmVkTGlua0NvbnRlbnRVcmwodGhpcy5zaGFyZWRMaW5rSWQsIGZhbHNlKTtcblxuICAgICAgICB0aGlzLnZpZXdlclR5cGUgPSB0aGlzLmdldFZpZXdlclR5cGVCeU1pbWVUeXBlKHRoaXMubWltZVR5cGUpO1xuICAgICAgICBpZiAodGhpcy52aWV3ZXJUeXBlID09PSAndW5rbm93bicpIHtcbiAgICAgICAgICAgIHRoaXMudmlld2VyVHlwZSA9IHRoaXMuZ2V0Vmlld2VyVHlwZUJ5RXh0ZW5zaW9uKHRoaXMuZXh0ZW5zaW9uKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0aGlzLnZpZXdlclR5cGUgPT09ICd1bmtub3duJykge1xuICAgICAgICAgICAgdGhpcy5kaXNwbGF5U2hhcmVkTGlua1JlbmRpdGlvbih0aGlzLnNoYXJlZExpbmtJZCk7XG4gICAgICAgIH1cblxuICAgICAgICB0aGlzLmV4dGVuc2lvbkNoYW5nZS5lbWl0KHRoaXMuZXh0ZW5zaW9uKTtcbiAgICB9XG5cbiAgICB0b2dnbGVTaWRlYmFyKCkge1xuICAgICAgICB0aGlzLnNob3dSaWdodFNpZGViYXIgPSAhdGhpcy5zaG93UmlnaHRTaWRlYmFyO1xuICAgICAgICBpZiAodGhpcy5zaG93UmlnaHRTaWRlYmFyICYmIHRoaXMubm9kZUlkKSB7XG4gICAgICAgICAgICB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5ub2Rlcy5nZXROb2RlKHRoaXMubm9kZUlkLCB7aW5jbHVkZTogWydhbGxvd2FibGVPcGVyYXRpb25zJ119KVxuICAgICAgICAgICAgICAgIC50aGVuKChub2RlRW50cnk6IE5vZGVFbnRyeSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNpZGViYXJSaWdodFRlbXBsYXRlQ29udGV4dC5ub2RlID0gbm9kZUVudHJ5LmVudHJ5O1xuICAgICAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgdG9nZ2xlTGVmdFNpZGViYXIoKSB7XG4gICAgICAgIHRoaXMuc2hvd0xlZnRTaWRlYmFyID0gIXRoaXMuc2hvd0xlZnRTaWRlYmFyO1xuICAgICAgICBpZiAodGhpcy5zaG93UmlnaHRTaWRlYmFyICYmIHRoaXMubm9kZUlkKSB7XG4gICAgICAgICAgICB0aGlzLmFwaVNlcnZpY2UuZ2V0SW5zdGFuY2UoKS5ub2Rlcy5nZXROb2RlKHRoaXMubm9kZUlkLCB7aW5jbHVkZTogWydhbGxvd2FibGVPcGVyYXRpb25zJ119KVxuICAgICAgICAgICAgICAgIC50aGVuKChub2RlRW50cnk6IE5vZGVFbnRyeSkgPT4ge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnNpZGViYXJMZWZ0VGVtcGxhdGVDb250ZXh0Lm5vZGUgPSBub2RlRW50cnkuZW50cnk7XG4gICAgICAgICAgICAgICAgfSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICBwcml2YXRlIGdldERpc3BsYXlOYW1lKG5hbWUpIHtcbiAgICAgICAgcmV0dXJuIHRoaXMuZGlzcGxheU5hbWUgfHwgbmFtZTtcbiAgICB9XG5cbiAgICBzY3JvbGxUb3AoKSB7XG4gICAgICAgIHdpbmRvdy5zY3JvbGxUbygwLCAxKTtcbiAgICB9XG5cbiAgICBnZXRWaWV3ZXJUeXBlQnlNaW1lVHlwZShtaW1lVHlwZTogc3RyaW5nKSB7XG4gICAgICAgIGlmIChtaW1lVHlwZSkge1xuICAgICAgICAgICAgbWltZVR5cGUgPSBtaW1lVHlwZS50b0xvd2VyQ2FzZSgpO1xuXG4gICAgICAgICAgICBjb25zdCBlZGl0b3JUeXBlcyA9IE9iamVjdC5rZXlzKHRoaXMubWltZVR5cGVzKTtcbiAgICAgICAgICAgIGZvciAoY29uc3QgdHlwZSBvZiBlZGl0b3JUeXBlcykge1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLm1pbWVUeXBlc1t0eXBlXS5pbmRleE9mKG1pbWVUeXBlKSA+PSAwKSB7XG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0eXBlO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgICAgICByZXR1cm4gJ3Vua25vd24nO1xuICAgIH1cblxuICAgIGdldFZpZXdlclR5cGVCeUV4dGVuc2lvbihleHRlbnNpb246IHN0cmluZykge1xuICAgICAgICBpZiAoZXh0ZW5zaW9uKSB7XG4gICAgICAgICAgICBleHRlbnNpb24gPSBleHRlbnNpb24udG9Mb3dlckNhc2UoKTtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0aGlzLmlzQ3VzdG9tVmlld2VyRXh0ZW5zaW9uKGV4dGVuc2lvbikpIHtcbiAgICAgICAgICAgIHJldHVybiAnY3VzdG9tJztcbiAgICAgICAgfVxuXG4gICAgICAgIGlmICh0aGlzLmV4dGVuc2lvbnMuaW1hZ2UuaW5kZXhPZihleHRlbnNpb24pID49IDApIHtcbiAgICAgICAgICAgIHJldHVybiAnaW1hZ2UnO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHRoaXMuZXh0ZW5zaW9ucy5tZWRpYS5pbmRleE9mKGV4dGVuc2lvbikgPj0gMCkge1xuICAgICAgICAgICAgcmV0dXJuICdtZWRpYSc7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5leHRlbnNpb25zLnRleHQuaW5kZXhPZihleHRlbnNpb24pID49IDApIHtcbiAgICAgICAgICAgIHJldHVybiAndGV4dCc7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAodGhpcy5leHRlbnNpb25zLnBkZi5pbmRleE9mKGV4dGVuc2lvbikgPj0gMCkge1xuICAgICAgICAgICAgcmV0dXJuICdwZGYnO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuICd1bmtub3duJztcbiAgICB9XG5cbiAgICBvbkJhY2tCdXR0b25DbGljaygpIHtcbiAgICAgICAgdGhpcy5jbG9zZSgpO1xuICAgIH1cblxuICAgIG9uTmF2aWdhdGVCZWZvcmVDbGljaygpIHtcbiAgICAgICAgdGhpcy5uYXZpZ2F0ZUJlZm9yZS5uZXh0KCk7XG4gICAgfVxuXG4gICAgb25OYXZpZ2F0ZU5leHRDbGljaygpIHtcbiAgICAgICAgdGhpcy5uYXZpZ2F0ZU5leHQubmV4dCgpO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIGNsb3NlIHRoZSB2aWV3ZXJcbiAgICAgKi9cbiAgICBjbG9zZSgpIHtcbiAgICAgICAgaWYgKHRoaXMub3RoZXJNZW51KSB7XG4gICAgICAgICAgICB0aGlzLm90aGVyTWVudS5oaWRkZW4gPSBmYWxzZTtcbiAgICAgICAgfVxuICAgICAgICB0aGlzLnNob3dWaWV3ZXIgPSBmYWxzZTtcbiAgICAgICAgdGhpcy5zaG93Vmlld2VyQ2hhbmdlLmVtaXQodGhpcy5zaG93Vmlld2VyKTtcbiAgICB9XG5cbiAgICAvKipcbiAgICAgKiBnZXQgRmlsZSBuYW1lIGZyb20gdXJsXG4gICAgICpcbiAgICAgKiBAcGFyYW0gIHVybCAtIHVybCBmaWxlXG4gICAgICovXG4gICAgZ2V0RmlsZW5hbWVGcm9tVXJsKHVybDogc3RyaW5nKTogc3RyaW5nIHtcbiAgICAgICAgY29uc3QgYW5jaG9yID0gdXJsLmluZGV4T2YoJyMnKTtcbiAgICAgICAgY29uc3QgcXVlcnkgPSB1cmwuaW5kZXhPZignPycpO1xuICAgICAgICBjb25zdCBlbmQgPSBNYXRoLm1pbihcbiAgICAgICAgICAgIGFuY2hvciA+IDAgPyBhbmNob3IgOiB1cmwubGVuZ3RoLFxuICAgICAgICAgICAgcXVlcnkgPiAwID8gcXVlcnkgOiB1cmwubGVuZ3RoKTtcbiAgICAgICAgcmV0dXJuIHVybC5zdWJzdHJpbmcodXJsLmxhc3RJbmRleE9mKCcvJywgZW5kKSArIDEsIGVuZCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogR2V0IGZpbGUgZXh0ZW5zaW9uIGZyb20gdGhlIHN0cmluZy5cbiAgICAgKiBTdXBwb3J0cyB0aGUgVVJMIGZvcm1hdHMgbGlrZTpcbiAgICAgKiBodHRwOi8vbG9jYWxob3N0L3Rlc3QuanBnP2NhY2hlPTEwMDBcbiAgICAgKiBodHRwOi8vbG9jYWxob3N0L3Rlc3QuanBnI2NhY2hlPTEwMDBcbiAgICAgKlxuICAgICAqIEBwYXJhbSBmaWxlTmFtZSAtIGZpbGUgbmFtZVxuICAgICAqL1xuICAgIGdldEZpbGVFeHRlbnNpb24oZmlsZU5hbWU6IHN0cmluZyk6IHN0cmluZyB7XG4gICAgICAgIGlmIChmaWxlTmFtZSkge1xuICAgICAgICAgICAgY29uc3QgbWF0Y2ggPSBmaWxlTmFtZS5tYXRjaCgvXFwuKFteXFwuL1xcP1xcI10rKSgkfFxcP3xcXCMpLyk7XG4gICAgICAgICAgICByZXR1cm4gbWF0Y2ggPyBtYXRjaFsxXSA6IG51bGw7XG4gICAgICAgIH1cbiAgICAgICAgcmV0dXJuIG51bGw7XG4gICAgfVxuXG4gICAgaXNDdXN0b21WaWV3ZXJFeHRlbnNpb24oZXh0ZW5zaW9uOiBzdHJpbmcpOiBib29sZWFuIHtcbiAgICAgICAgY29uc3QgZXh0ZW5zaW9uczogYW55ID0gdGhpcy5leHRlcm5hbEV4dGVuc2lvbnMgfHwgW107XG5cbiAgICAgICAgaWYgKGV4dGVuc2lvbiAmJiBleHRlbnNpb25zLmxlbmd0aCA+IDApIHtcbiAgICAgICAgICAgIGV4dGVuc2lvbiA9IGV4dGVuc2lvbi50b0xvd2VyQ2FzZSgpO1xuICAgICAgICAgICAgcmV0dXJuIGV4dGVuc2lvbnMuZmxhdCgpLmluZGV4T2YoZXh0ZW5zaW9uKSA+PSAwO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH1cblxuICAgIC8qKlxuICAgICAqIEtleWJvYXJkIGV2ZW50IGxpc3RlbmVyXG4gICAgICogQHBhcmFtICBldmVudFxuICAgICAqL1xuICAgIEBIb3N0TGlzdGVuZXIoJ2RvY3VtZW50OmtleXVwJywgWyckZXZlbnQnXSlcbiAgICBoYW5kbGVLZXlib2FyZEV2ZW50KGV2ZW50OiBLZXlib2FyZEV2ZW50KSB7XG4gICAgICAgIGNvbnN0IGtleSA9IGV2ZW50LmtleUNvZGU7XG5cbiAgICAgICAgLy8gRXNjXG4gICAgICAgIGlmIChrZXkgPT09IDI3ICYmIHRoaXMub3ZlcmxheU1vZGUpIHsgLy8gZXNjXG4gICAgICAgICAgICB0aGlzLmNsb3NlKCk7XG4gICAgICAgIH1cblxuICAgICAgICAvLyBMZWZ0IGFycm93XG4gICAgICAgIGlmIChrZXkgPT09IDM3ICYmIHRoaXMuY2FuTmF2aWdhdGVCZWZvcmUpIHtcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICB0aGlzLm9uTmF2aWdhdGVCZWZvcmVDbGljaygpO1xuICAgICAgICB9XG5cbiAgICAgICAgLy8gUmlnaHQgYXJyb3dcbiAgICAgICAgaWYgKGtleSA9PT0gMzkgJiYgdGhpcy5jYW5OYXZpZ2F0ZU5leHQpIHtcbiAgICAgICAgICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG4gICAgICAgICAgICB0aGlzLm9uTmF2aWdhdGVOZXh0Q2xpY2soKTtcbiAgICAgICAgfVxuXG4gICAgICAgIC8vIEN0cmwrRlxuICAgICAgICBpZiAoa2V5ID09PSA3MCAmJiBldmVudC5jdHJsS2V5KSB7XG4gICAgICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuICAgICAgICAgICAgdGhpcy5lbnRlckZ1bGxTY3JlZW4oKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaW50Q29udGVudCgpIHtcbiAgICAgICAgaWYgKHRoaXMuYWxsb3dQcmludCkge1xuICAgICAgICAgICAgY29uc3QgYXJncyA9IG5ldyBCYXNlRXZlbnQoKTtcbiAgICAgICAgICAgIHRoaXMucHJpbnQubmV4dChhcmdzKTtcblxuICAgICAgICAgICAgaWYgKCFhcmdzLmRlZmF1bHRQcmV2ZW50ZWQpIHtcbiAgICAgICAgICAgICAgICB0aGlzLnZpZXdVdGlscy5wcmludEZpbGVHZW5lcmljKHRoaXMubm9kZUlkLCB0aGlzLm1pbWVUeXBlKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8qKlxuICAgICAqIFRyaWdnZXJzIGZ1bGwgc2NyZWVuIG1vZGUgd2l0aCBhIG1haW4gY29udGVudCBhcmVhIGRpc3BsYXllZC5cbiAgICAgKi9cbiAgICBlbnRlckZ1bGxTY3JlZW4oKTogdm9pZCB7XG4gICAgICAgIGlmICh0aGlzLmFsbG93RnVsbFNjcmVlbikge1xuICAgICAgICAgICAgY29uc3QgY29udGFpbmVyID0gdGhpcy5lbC5uYXRpdmVFbGVtZW50LnF1ZXJ5U2VsZWN0b3IoJy5hZGYtdmlld2VyX19mdWxsc2NyZWVuLWNvbnRhaW5lcicpO1xuICAgICAgICAgICAgaWYgKGNvbnRhaW5lcikge1xuICAgICAgICAgICAgICAgIGlmIChjb250YWluZXIucmVxdWVzdEZ1bGxzY3JlZW4pIHtcbiAgICAgICAgICAgICAgICAgICAgY29udGFpbmVyLnJlcXVlc3RGdWxsc2NyZWVuKCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChjb250YWluZXIud2Via2l0UmVxdWVzdEZ1bGxzY3JlZW4pIHtcbiAgICAgICAgICAgICAgICAgICAgY29udGFpbmVyLndlYmtpdFJlcXVlc3RGdWxsc2NyZWVuKCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChjb250YWluZXIubW96UmVxdWVzdEZ1bGxTY3JlZW4pIHtcbiAgICAgICAgICAgICAgICAgICAgY29udGFpbmVyLm1velJlcXVlc3RGdWxsU2NyZWVuKCk7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChjb250YWluZXIubXNSZXF1ZXN0RnVsbHNjcmVlbikge1xuICAgICAgICAgICAgICAgICAgICBjb250YWluZXIubXNSZXF1ZXN0RnVsbHNjcmVlbigpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH1cbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgZGlzcGxheU5vZGVSZW5kaXRpb24obm9kZUlkOiBzdHJpbmcpIHtcbiAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgIGNvbnN0IHJlbmRpdGlvbiA9IGF3YWl0IHRoaXMucmVzb2x2ZVJlbmRpdGlvbihub2RlSWQsICdwZGYnKTtcbiAgICAgICAgICAgIGlmIChyZW5kaXRpb24pIHtcbiAgICAgICAgICAgICAgICBjb25zdCByZW5kaXRpb25JZCA9IHJlbmRpdGlvbi5lbnRyeS5pZDtcblxuICAgICAgICAgICAgICAgIGlmIChyZW5kaXRpb25JZCA9PT0gJ3BkZicpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52aWV3ZXJUeXBlID0gJ3BkZic7XG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChyZW5kaXRpb25JZCA9PT0gJ2ltZ3ByZXZpZXcnKSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMudmlld2VyVHlwZSA9ICdpbWFnZSc7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgdGhpcy51cmxGaWxlQ29udGVudCA9IHRoaXMuYXBpU2VydmljZS5jb250ZW50QXBpLmdldFJlbmRpdGlvblVybChub2RlSWQsIHJlbmRpdGlvbklkKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSBjYXRjaCAoZXJyKSB7XG4gICAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZXJyb3IoZXJyKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgZGlzcGxheVNoYXJlZExpbmtSZW5kaXRpb24oc2hhcmVkSWQ6IHN0cmluZykge1xuICAgICAgICB0cnkge1xuICAgICAgICAgICAgY29uc3QgcmVuZGl0aW9uOiBSZW5kaXRpb25FbnRyeSA9IGF3YWl0IHRoaXMuYXBpU2VydmljZS5yZW5kaXRpb25zQXBpLmdldFNoYXJlZExpbmtSZW5kaXRpb24oc2hhcmVkSWQsICdwZGYnKTtcbiAgICAgICAgICAgIGlmIChyZW5kaXRpb24uZW50cnkuc3RhdHVzLnRvU3RyaW5nKCkgPT09ICdDUkVBVEVEJykge1xuICAgICAgICAgICAgICAgIHRoaXMudmlld2VyVHlwZSA9ICdwZGYnO1xuICAgICAgICAgICAgICAgIHRoaXMudXJsRmlsZUNvbnRlbnQgPSB0aGlzLmFwaVNlcnZpY2UuY29udGVudEFwaS5nZXRTaGFyZWRMaW5rUmVuZGl0aW9uVXJsKHNoYXJlZElkLCAncGRmJyk7XG4gICAgICAgICAgICB9XG4gICAgICAgIH0gY2F0Y2ggKGVycm9yKSB7XG4gICAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgdHJ5IHtcbiAgICAgICAgICAgICAgICBjb25zdCByZW5kaXRpb246IFJlbmRpdGlvbkVudHJ5ID0gYXdhaXQgdGhpcy5hcGlTZXJ2aWNlLnJlbmRpdGlvbnNBcGkuZ2V0U2hhcmVkTGlua1JlbmRpdGlvbihzaGFyZWRJZCwgJ2ltZ3ByZXZpZXcnKTtcbiAgICAgICAgICAgICAgICBpZiAocmVuZGl0aW9uLmVudHJ5LnN0YXR1cy50b1N0cmluZygpID09PSAnQ1JFQVRFRCcpIHtcbiAgICAgICAgICAgICAgICAgICAgdGhpcy52aWV3ZXJUeXBlID0gJ2ltYWdlJztcbiAgICAgICAgICAgICAgICAgICAgdGhpcy51cmxGaWxlQ29udGVudCA9IHRoaXMuYXBpU2VydmljZS5jb250ZW50QXBpLmdldFNoYXJlZExpbmtSZW5kaXRpb25Vcmwoc2hhcmVkSWQsICdpbWdwcmV2aWV3Jyk7XG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSBjYXRjaCAoZXJyb3IpIHtcbiAgICAgICAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZXJyb3IoZXJyb3IpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfVxuXG4gICAgcHJpdmF0ZSBhc3luYyByZXNvbHZlUmVuZGl0aW9uKG5vZGVJZDogc3RyaW5nLCByZW5kaXRpb25JZDogc3RyaW5nKTogUHJvbWlzZTxSZW5kaXRpb25FbnRyeT4ge1xuICAgICAgICByZW5kaXRpb25JZCA9IHJlbmRpdGlvbklkLnRvTG93ZXJDYXNlKCk7XG5cbiAgICAgICAgY29uc3Qgc3VwcG9ydGVkUmVuZGl0aW9uOiBSZW5kaXRpb25QYWdpbmcgPSBhd2FpdCB0aGlzLmFwaVNlcnZpY2UucmVuZGl0aW9uc0FwaS5nZXRSZW5kaXRpb25zKG5vZGVJZCk7XG5cbiAgICAgICAgbGV0IHJlbmRpdGlvbjogUmVuZGl0aW9uRW50cnkgPSBzdXBwb3J0ZWRSZW5kaXRpb24ubGlzdC5lbnRyaWVzLmZpbmQoKHJlbmRpdGlvbkVudHJ5OiBSZW5kaXRpb25FbnRyeSkgPT4gcmVuZGl0aW9uRW50cnkuZW50cnkuaWQudG9Mb3dlckNhc2UoKSA9PT0gcmVuZGl0aW9uSWQpO1xuICAgICAgICBpZiAoIXJlbmRpdGlvbikge1xuICAgICAgICAgICAgcmVuZGl0aW9uSWQgPSAnaW1ncHJldmlldyc7XG4gICAgICAgICAgICByZW5kaXRpb24gPSBzdXBwb3J0ZWRSZW5kaXRpb24ubGlzdC5lbnRyaWVzLmZpbmQoKHJlbmRpdGlvbkVudHJ5OiBSZW5kaXRpb25FbnRyeSkgPT4gcmVuZGl0aW9uRW50cnkuZW50cnkuaWQudG9Mb3dlckNhc2UoKSA9PT0gcmVuZGl0aW9uSWQpO1xuICAgICAgICB9XG5cbiAgICAgICAgaWYgKHJlbmRpdGlvbikge1xuICAgICAgICAgICAgY29uc3Qgc3RhdHVzOiBzdHJpbmcgPSByZW5kaXRpb24uZW50cnkuc3RhdHVzLnRvU3RyaW5nKCk7XG5cbiAgICAgICAgICAgIGlmIChzdGF0dXMgPT09ICdOT1RfQ1JFQVRFRCcpIHtcbiAgICAgICAgICAgICAgICB0cnkge1xuICAgICAgICAgICAgICAgICAgICBhd2FpdCB0aGlzLmFwaVNlcnZpY2UucmVuZGl0aW9uc0FwaS5jcmVhdGVSZW5kaXRpb24obm9kZUlkLCB7aWQ6IHJlbmRpdGlvbklkfSkudGhlbigoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnZpZXdlclR5cGUgPSAnaW5fY3JlYXRpb24nO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICAgICAgcmVuZGl0aW9uID0gYXdhaXQgdGhpcy53YWl0UmVuZGl0aW9uKG5vZGVJZCwgcmVuZGl0aW9uSWQpO1xuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGVycikge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZXJyb3IoZXJyKTtcbiAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICB9XG4gICAgICAgIH1cblxuICAgICAgICByZXR1cm4gcmVuZGl0aW9uO1xuICAgIH1cblxuICAgIHByaXZhdGUgYXN5bmMgd2FpdFJlbmRpdGlvbihub2RlSWQ6IHN0cmluZywgcmVuZGl0aW9uSWQ6IHN0cmluZyk6IFByb21pc2U8UmVuZGl0aW9uRW50cnk+IHtcbiAgICAgICAgbGV0IGN1cnJlbnRSZXRyeTogbnVtYmVyID0gMDtcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlPFJlbmRpdGlvbkVudHJ5PigocmVzb2x2ZSwgcmVqZWN0KSA9PiB7XG4gICAgICAgICAgICBjb25zdCBpbnRlcnZhbElkID0gc2V0SW50ZXJ2YWwoKCkgPT4ge1xuICAgICAgICAgICAgICAgIGN1cnJlbnRSZXRyeSsrO1xuICAgICAgICAgICAgICAgIGlmICh0aGlzLm1heFJldHJpZXMgPj0gY3VycmVudFJldHJ5KSB7XG4gICAgICAgICAgICAgICAgICAgIHRoaXMuYXBpU2VydmljZS5yZW5kaXRpb25zQXBpLmdldFJlbmRpdGlvbihub2RlSWQsIHJlbmRpdGlvbklkKS50aGVuKChyZW5kaXRpb246IFJlbmRpdGlvbkVudHJ5KSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdCBzdGF0dXM6IHN0cmluZyA9IHJlbmRpdGlvbi5lbnRyeS5zdGF0dXMudG9TdHJpbmcoKTtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChzdGF0dXMgPT09ICdDUkVBVEVEJykge1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHJlbmRpdGlvbklkID09PSAncGRmJykge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnZpZXdlclR5cGUgPSAncGRmJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2UgaWYgKHJlbmRpdGlvbklkID09PSAnaW1ncHJldmlldycpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgdGhpcy52aWV3ZXJUeXBlID0gJ2ltYWdlJztcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XG5cbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnVybEZpbGVDb250ZW50ID0gdGhpcy5hcGlTZXJ2aWNlLmNvbnRlbnRBcGkuZ2V0UmVuZGl0aW9uVXJsKG5vZGVJZCwgcmVuZGl0aW9uSWQpO1xuXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbChpbnRlcnZhbElkKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICByZXR1cm4gcmVzb2x2ZShyZW5kaXRpb24pO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9LCAoKSA9PiB7XG4gICAgICAgICAgICAgICAgICAgICAgICB0aGlzLnZpZXdlclR5cGUgPSAnZXJyb3JfaW5fY3JlYXRpb24nO1xuICAgICAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHJlamVjdCgpO1xuICAgICAgICAgICAgICAgICAgICB9KTtcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICB0aGlzLmlzTG9hZGluZyA9IGZhbHNlO1xuICAgICAgICAgICAgICAgICAgICB0aGlzLnZpZXdlclR5cGUgPSAnZXJyb3JfaW5fY3JlYXRpb24nO1xuICAgICAgICAgICAgICAgICAgICBjbGVhckludGVydmFsKGludGVydmFsSWQpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgIH0sIHRoaXMuVFJZX1RJTUVPVVQpO1xuICAgICAgICB9KTtcbiAgICB9XG5cbiAgICBjaGVja0V4dGVuc2lvbnMoZXh0ZW5zaW9uQWxsb3dlZCkge1xuICAgICAgICBpZiAodHlwZW9mIGV4dGVuc2lvbkFsbG93ZWQgPT09ICdzdHJpbmcnKSB7XG4gICAgICAgICAgICByZXR1cm4gdGhpcy5leHRlbnNpb24udG9Mb3dlckNhc2UoKSA9PT0gZXh0ZW5zaW9uQWxsb3dlZC50b0xvd2VyQ2FzZSgpO1xuICAgICAgICB9IGVsc2UgaWYgKGV4dGVuc2lvbkFsbG93ZWQubGVuZ3RoID4gMCkge1xuICAgICAgICAgICAgcmV0dXJuIGV4dGVuc2lvbkFsbG93ZWQuZmluZCgoY3VycmVudEV4dGVuc2lvbikgPT4ge1xuICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLmV4dGVuc2lvbi50b0xvd2VyQ2FzZSgpID09PSBjdXJyZW50RXh0ZW5zaW9uLnRvTG93ZXJDYXNlKCk7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuXG4gICAgfVxuXG4gICAgcHJpdmF0ZSBnZW5lcmF0ZUNhY2hlQnVzdGVyTnVtYmVyKCkge1xuICAgICAgICB0aGlzLmNhY2hlQnVzdGVyTnVtYmVyID0gRGF0ZS5ub3coKTtcbiAgICB9XG59XG4iXX0=