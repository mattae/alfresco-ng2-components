/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ContentChild, Directive, Input, TemplateRef } from '@angular/core';
import { ViewerComponent } from '../components/viewer.component';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
var ViewerExtensionDirective = /** @class */ (function () {
    function ViewerExtensionDirective(viewerComponent) {
        this.viewerComponent = viewerComponent;
        this.onDestroy$ = new Subject();
    }
    /**
     * @return {?}
     */
    ViewerExtensionDirective.prototype.ngAfterContentInit = /**
     * @return {?}
     */
    function () {
        var _this = this;
        this.templateModel = { template: this.template, isVisible: false };
        this.viewerComponent.extensionTemplates.push(this.templateModel);
        this.viewerComponent.extensionChange
            .pipe(takeUntil(this.onDestroy$))
            .subscribe((/**
         * @param {?} fileExtension
         * @return {?}
         */
        function (fileExtension) {
            _this.templateModel.isVisible = _this.isVisible(fileExtension);
        }));
        if (this.supportedExtensions instanceof Array) {
            this.supportedExtensions.forEach((/**
             * @param {?} extension
             * @return {?}
             */
            function (extension) {
                _this.viewerComponent.externalExtensions.push(extension);
            }));
        }
    };
    /**
     * @return {?}
     */
    ViewerExtensionDirective.prototype.ngOnDestroy = /**
     * @return {?}
     */
    function () {
        this.onDestroy$.next(true);
        this.onDestroy$.complete();
    };
    /**
     * check if the current extension in the viewer is compatible with this extension checking against supportedExtensions
     */
    /**
     * check if the current extension in the viewer is compatible with this extension checking against supportedExtensions
     * @param {?} fileExtension
     * @return {?}
     */
    ViewerExtensionDirective.prototype.isVisible = /**
     * check if the current extension in the viewer is compatible with this extension checking against supportedExtensions
     * @param {?} fileExtension
     * @return {?}
     */
    function (fileExtension) {
        /** @type {?} */
        var supportedExtension;
        if (this.supportedExtensions && (this.supportedExtensions instanceof Array)) {
            supportedExtension = this.supportedExtensions.find((/**
             * @param {?} extension
             * @return {?}
             */
            function (extension) {
                return extension.toLowerCase() === fileExtension;
            }));
        }
        return !!supportedExtension;
    };
    ViewerExtensionDirective.decorators = [
        { type: Directive, args: [{
                    selector: 'adf-viewer-extension'
                },] }
    ];
    /** @nocollapse */
    ViewerExtensionDirective.ctorParameters = function () { return [
        { type: ViewerComponent }
    ]; };
    ViewerExtensionDirective.propDecorators = {
        template: [{ type: ContentChild, args: [TemplateRef, { static: true },] }],
        urlFileContent: [{ type: Input }],
        extension: [{ type: Input }],
        supportedExtensions: [{ type: Input }]
    };
    return ViewerExtensionDirective;
}());
export { ViewerExtensionDirective };
if (false) {
    /** @type {?} */
    ViewerExtensionDirective.prototype.template;
    /** @type {?} */
    ViewerExtensionDirective.prototype.urlFileContent;
    /** @type {?} */
    ViewerExtensionDirective.prototype.extension;
    /** @type {?} */
    ViewerExtensionDirective.prototype.supportedExtensions;
    /** @type {?} */
    ViewerExtensionDirective.prototype.templateModel;
    /**
     * @type {?}
     * @private
     */
    ViewerExtensionDirective.prototype.onDestroy$;
    /**
     * @type {?}
     * @private
     */
    ViewerExtensionDirective.prototype.viewerComponent;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlld2VyLWV4dGVuc2lvbi5kaXJlY3RpdmUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJ2aWV3ZXIvZGlyZWN0aXZlcy92aWV3ZXItZXh0ZW5zaW9uLmRpcmVjdGl2ZS50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7QUFtQ0EsT0FBTyxFQUFvQixZQUFZLEVBQUUsU0FBUyxFQUFFLEtBQUssRUFBRSxXQUFXLEVBQWEsTUFBTSxlQUFlLENBQUM7QUFDekcsT0FBTyxFQUFFLGVBQWUsRUFBRSxNQUFNLGdDQUFnQyxDQUFDO0FBQ2pFLE9BQU8sRUFBRSxPQUFPLEVBQUUsTUFBTSxNQUFNLENBQUM7QUFDL0IsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBRTNDO0lBcUJJLGtDQUFvQixlQUFnQztRQUFoQyxvQkFBZSxHQUFmLGVBQWUsQ0FBaUI7UUFGNUMsZUFBVSxHQUFHLElBQUksT0FBTyxFQUFXLENBQUM7SUFHNUMsQ0FBQzs7OztJQUVELHFEQUFrQjs7O0lBQWxCO1FBQUEsaUJBZ0JDO1FBZkcsSUFBSSxDQUFDLGFBQWEsR0FBRyxFQUFFLFFBQVEsRUFBRSxJQUFJLENBQUMsUUFBUSxFQUFFLFNBQVMsRUFBRSxLQUFLLEVBQUUsQ0FBQztRQUVuRSxJQUFJLENBQUMsZUFBZSxDQUFDLGtCQUFrQixDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsYUFBYSxDQUFDLENBQUM7UUFFakUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxlQUFlO2FBQy9CLElBQUksQ0FBQyxTQUFTLENBQUMsSUFBSSxDQUFDLFVBQVUsQ0FBQyxDQUFDO2FBQ2hDLFNBQVM7Ozs7UUFBQyxVQUFBLGFBQWE7WUFDcEIsS0FBSSxDQUFDLGFBQWEsQ0FBQyxTQUFTLEdBQUcsS0FBSSxDQUFDLFNBQVMsQ0FBQyxhQUFhLENBQUMsQ0FBQztRQUNqRSxDQUFDLEVBQUMsQ0FBQztRQUVQLElBQUksSUFBSSxDQUFDLG1CQUFtQixZQUFZLEtBQUssRUFBRTtZQUMzQyxJQUFJLENBQUMsbUJBQW1CLENBQUMsT0FBTzs7OztZQUFDLFVBQUMsU0FBUztnQkFDdkMsS0FBSSxDQUFDLGVBQWUsQ0FBQyxrQkFBa0IsQ0FBQyxJQUFJLENBQUMsU0FBUyxDQUFDLENBQUM7WUFDNUQsQ0FBQyxFQUFDLENBQUM7U0FDTjtJQUNMLENBQUM7Ozs7SUFFRCw4Q0FBVzs7O0lBQVg7UUFDSSxJQUFJLENBQUMsVUFBVSxDQUFDLElBQUksQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUMzQixJQUFJLENBQUMsVUFBVSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQy9CLENBQUM7SUFFRDs7T0FFRzs7Ozs7O0lBQ0gsNENBQVM7Ozs7O0lBQVQsVUFBVSxhQUFhOztZQUNmLGtCQUEwQjtRQUU5QixJQUFJLElBQUksQ0FBQyxtQkFBbUIsSUFBSSxDQUFDLElBQUksQ0FBQyxtQkFBbUIsWUFBWSxLQUFLLENBQUMsRUFBRTtZQUN6RSxrQkFBa0IsR0FBRyxJQUFJLENBQUMsbUJBQW1CLENBQUMsSUFBSTs7OztZQUFDLFVBQUMsU0FBUztnQkFDekQsT0FBTyxTQUFTLENBQUMsV0FBVyxFQUFFLEtBQUssYUFBYSxDQUFDO1lBRXJELENBQUMsRUFBQyxDQUFDO1NBQ047UUFFRCxPQUFPLENBQUMsQ0FBQyxrQkFBa0IsQ0FBQztJQUNoQyxDQUFDOztnQkE3REosU0FBUyxTQUFDO29CQUNQLFFBQVEsRUFBRSxzQkFBc0I7aUJBQ25DOzs7O2dCQU5RLGVBQWU7OzsyQkFTbkIsWUFBWSxTQUFDLFdBQVcsRUFBRSxFQUFDLE1BQU0sRUFBRSxJQUFJLEVBQUM7aUNBR3hDLEtBQUs7NEJBR0wsS0FBSztzQ0FHTCxLQUFLOztJQWlEViwrQkFBQztDQUFBLEFBL0RELElBK0RDO1NBNURZLHdCQUF3Qjs7O0lBRWpDLDRDQUNjOztJQUVkLGtEQUN1Qjs7SUFFdkIsNkNBQ2tCOztJQUVsQix1REFDOEI7O0lBRTlCLGlEQUFtQjs7Ozs7SUFFbkIsOENBQTRDOzs7OztJQUVoQyxtREFBd0MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cblxuLyohXG4gKiBAbGljZW5zZVxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cbiAqXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxuICogWW91IG1heSBvYnRhaW4gYSBjb3B5IG9mIHRoZSBMaWNlbnNlIGF0XG4gKlxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxuICpcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcbiAqIFdJVEhPVVQgV0FSUkFOVElFUyBPUiBDT05ESVRJT05TIE9GIEFOWSBLSU5ELCBlaXRoZXIgZXhwcmVzcyBvciBpbXBsaWVkLlxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXG4gKi9cblxuaW1wb3J0IHsgQWZ0ZXJDb250ZW50SW5pdCwgQ29udGVudENoaWxkLCBEaXJlY3RpdmUsIElucHV0LCBUZW1wbGF0ZVJlZiwgT25EZXN0cm95IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBWaWV3ZXJDb21wb25lbnQgfSBmcm9tICcuLi9jb21wb25lbnRzL3ZpZXdlci5jb21wb25lbnQnO1xuaW1wb3J0IHsgU3ViamVjdCB9IGZyb20gJ3J4anMnO1xuaW1wb3J0IHsgdGFrZVVudGlsIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuXG5ARGlyZWN0aXZlKHtcbiAgICBzZWxlY3RvcjogJ2FkZi12aWV3ZXItZXh0ZW5zaW9uJ1xufSlcbmV4cG9ydCBjbGFzcyBWaWV3ZXJFeHRlbnNpb25EaXJlY3RpdmUgaW1wbGVtZW50cyBBZnRlckNvbnRlbnRJbml0LCBPbkRlc3Ryb3kge1xuXG4gICAgQENvbnRlbnRDaGlsZChUZW1wbGF0ZVJlZiwge3N0YXRpYzogdHJ1ZX0pXG4gICAgdGVtcGxhdGU6IGFueTtcblxuICAgIEBJbnB1dCgpXG4gICAgdXJsRmlsZUNvbnRlbnQ6IHN0cmluZztcblxuICAgIEBJbnB1dCgpXG4gICAgZXh0ZW5zaW9uOiBzdHJpbmc7XG5cbiAgICBASW5wdXQoKVxuICAgIHN1cHBvcnRlZEV4dGVuc2lvbnM6IHN0cmluZ1tdO1xuXG4gICAgdGVtcGxhdGVNb2RlbDogYW55O1xuXG4gICAgcHJpdmF0ZSBvbkRlc3Ryb3kkID0gbmV3IFN1YmplY3Q8Ym9vbGVhbj4oKTtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgdmlld2VyQ29tcG9uZW50OiBWaWV3ZXJDb21wb25lbnQpIHtcbiAgICB9XG5cbiAgICBuZ0FmdGVyQ29udGVudEluaXQoKSB7XG4gICAgICAgIHRoaXMudGVtcGxhdGVNb2RlbCA9IHsgdGVtcGxhdGU6IHRoaXMudGVtcGxhdGUsIGlzVmlzaWJsZTogZmFsc2UgfTtcblxuICAgICAgICB0aGlzLnZpZXdlckNvbXBvbmVudC5leHRlbnNpb25UZW1wbGF0ZXMucHVzaCh0aGlzLnRlbXBsYXRlTW9kZWwpO1xuXG4gICAgICAgIHRoaXMudmlld2VyQ29tcG9uZW50LmV4dGVuc2lvbkNoYW5nZVxuICAgICAgICAgICAgLnBpcGUodGFrZVVudGlsKHRoaXMub25EZXN0cm95JCkpXG4gICAgICAgICAgICAuc3Vic2NyaWJlKGZpbGVFeHRlbnNpb24gPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMudGVtcGxhdGVNb2RlbC5pc1Zpc2libGUgPSB0aGlzLmlzVmlzaWJsZShmaWxlRXh0ZW5zaW9uKTtcbiAgICAgICAgICAgIH0pO1xuXG4gICAgICAgIGlmICh0aGlzLnN1cHBvcnRlZEV4dGVuc2lvbnMgaW5zdGFuY2VvZiBBcnJheSkge1xuICAgICAgICAgICAgdGhpcy5zdXBwb3J0ZWRFeHRlbnNpb25zLmZvckVhY2goKGV4dGVuc2lvbikgPT4ge1xuICAgICAgICAgICAgICAgIHRoaXMudmlld2VyQ29tcG9uZW50LmV4dGVybmFsRXh0ZW5zaW9ucy5wdXNoKGV4dGVuc2lvbik7XG4gICAgICAgICAgICB9KTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIG5nT25EZXN0cm95KCkge1xuICAgICAgICB0aGlzLm9uRGVzdHJveSQubmV4dCh0cnVlKTtcbiAgICAgICAgdGhpcy5vbkRlc3Ryb3kkLmNvbXBsZXRlKCk7XG4gICAgfVxuXG4gICAgLyoqXG4gICAgICogY2hlY2sgaWYgdGhlIGN1cnJlbnQgZXh0ZW5zaW9uIGluIHRoZSB2aWV3ZXIgaXMgY29tcGF0aWJsZSB3aXRoIHRoaXMgZXh0ZW5zaW9uIGNoZWNraW5nIGFnYWluc3Qgc3VwcG9ydGVkRXh0ZW5zaW9uc1xuICAgICAqL1xuICAgIGlzVmlzaWJsZShmaWxlRXh0ZW5zaW9uKTogYm9vbGVhbiB7XG4gICAgICAgIGxldCBzdXBwb3J0ZWRFeHRlbnNpb246IHN0cmluZztcblxuICAgICAgICBpZiAodGhpcy5zdXBwb3J0ZWRFeHRlbnNpb25zICYmICh0aGlzLnN1cHBvcnRlZEV4dGVuc2lvbnMgaW5zdGFuY2VvZiBBcnJheSkpIHtcbiAgICAgICAgICAgIHN1cHBvcnRlZEV4dGVuc2lvbiA9IHRoaXMuc3VwcG9ydGVkRXh0ZW5zaW9ucy5maW5kKChleHRlbnNpb24pID0+IHtcbiAgICAgICAgICAgICAgICByZXR1cm4gZXh0ZW5zaW9uLnRvTG93ZXJDYXNlKCkgPT09IGZpbGVFeHRlbnNpb247XG5cbiAgICAgICAgICAgIH0pO1xuICAgICAgICB9XG5cbiAgICAgICAgcmV0dXJuICEhc3VwcG9ydGVkRXh0ZW5zaW9uO1xuICAgIH1cblxufVxuIl19