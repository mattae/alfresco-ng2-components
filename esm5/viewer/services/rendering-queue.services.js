/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/* tslint:disable:adf-license-banner  */
/* Copyright 2012 Mozilla Foundation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
/**
 *
 * RenderingQueueServices rendering of the views for pages and thumbnails.
 *
 */
var RenderingQueueServices = /** @class */ (function () {
    function RenderingQueueServices() {
        this.renderingStates = {
            INITIAL: 0,
            RUNNING: 1,
            PAUSED: 2,
            FINISHED: 3
        };
        this.CLEANUP_TIMEOUT = 30000;
        this.pdfViewer = null;
        this.pdfThumbnailViewer = null;
        this.onIdle = null;
        this.highestPriorityPage = null;
        this.idleTimeout = null;
        this.printing = false;
        this.isThumbnailViewEnabled = false;
    }
    /**
     * @param pdfViewer
     */
    /**
     * @param {?} pdfViewer
     * @return {?}
     */
    RenderingQueueServices.prototype.setViewer = /**
     * @param {?} pdfViewer
     * @return {?}
     */
    function (pdfViewer) {
        this.pdfViewer = pdfViewer;
    };
    /**
     * @param pdfThumbnailViewer
     */
    /**
     * @param {?} pdfThumbnailViewer
     * @return {?}
     */
    RenderingQueueServices.prototype.setThumbnailViewer = /**
     * @param {?} pdfThumbnailViewer
     * @return {?}
     */
    function (pdfThumbnailViewer) {
        this.pdfThumbnailViewer = pdfThumbnailViewer;
    };
    /**
     * @param  view
     */
    /**
     * @param {?} view
     * @return {?}
     */
    RenderingQueueServices.prototype.isHighestPriority = /**
     * @param {?} view
     * @return {?}
     */
    function (view) {
        return this.highestPriorityPage === view.renderingId;
    };
    /**
     * @param {?} currentlyVisiblePages
     * @return {?}
     */
    RenderingQueueServices.prototype.renderHighestPriority = /**
     * @param {?} currentlyVisiblePages
     * @return {?}
     */
    function (currentlyVisiblePages) {
        if (this.idleTimeout) {
            clearTimeout(this.idleTimeout);
            this.idleTimeout = null;
        }
        // Pages have a higher priority than thumbnails, so check them first.
        if (this.pdfViewer.forceRendering(currentlyVisiblePages)) {
            return;
        }
        // No pages needed rendering so check thumbnails.
        if (this.pdfThumbnailViewer && this.isThumbnailViewEnabled) {
            if (this.pdfThumbnailViewer.forceRendering()) {
                return;
            }
        }
        if (this.printing) {
            // If printing is currently ongoing do not reschedule cleanup.
            return;
        }
        if (this.onIdle) {
            this.idleTimeout = setTimeout(this.onIdle.bind(this), this.CLEANUP_TIMEOUT);
        }
    };
    /**
     * @param {?} visible
     * @param {?} views
     * @param {?} scrolledDown
     * @return {?}
     */
    RenderingQueueServices.prototype.getHighestPriority = /**
     * @param {?} visible
     * @param {?} views
     * @param {?} scrolledDown
     * @return {?}
     */
    function (visible, views, scrolledDown) {
        // The state has changed figure out which page has the highest priority to
        // render next (if any).
        // Priority:
        // 1 visible pages
        // 2 if last scrolled down page after the visible pages
        // 2 if last scrolled up page before the visible pages
        /** @type {?} */
        var visibleViews = visible.views;
        /** @type {?} */
        var numVisible = visibleViews.length;
        if (numVisible === 0) {
            return false;
        }
        for (var i = 0; i < numVisible; ++i) {
            /** @type {?} */
            var view = visibleViews[i].view;
            if (!this.isViewFinished(view)) {
                return view;
            }
        }
        // All the visible views have rendered, try to render next/previous pages.
        if (scrolledDown) {
            /** @type {?} */
            var nextPageIndex = visible.last.id;
            // ID's start at 1 so no need to add 1.
            if (views[nextPageIndex] && !this.isViewFinished(views[nextPageIndex])) {
                return views[nextPageIndex];
            }
        }
        else {
            /** @type {?} */
            var previousPageIndex = visible.first.id - 2;
            if (views[previousPageIndex] && !this.isViewFinished(views[previousPageIndex])) {
                return views[previousPageIndex];
            }
        }
        // Everything that needs to be rendered has been.
        return null;
    };
    /**
     * @param view
     */
    /**
     * @param {?} view
     * @return {?}
     */
    RenderingQueueServices.prototype.isViewFinished = /**
     * @param {?} view
     * @return {?}
     */
    function (view) {
        return view.renderingState === this.renderingStates.FINISHED;
    };
    /**
     * Render a page or thumbnail view. This calls the appropriate function
     * based on the views state. If the view is already rendered it will return
     * false.
     * @param view
     */
    /**
     * Render a page or thumbnail view. This calls the appropriate function
     * based on the views state. If the view is already rendered it will return
     * false.
     * @param {?} view
     * @return {?}
     */
    RenderingQueueServices.prototype.renderView = /**
     * Render a page or thumbnail view. This calls the appropriate function
     * based on the views state. If the view is already rendered it will return
     * false.
     * @param {?} view
     * @return {?}
     */
    function (view) {
        /** @type {?} */
        var state = view.renderingState;
        switch (state) {
            case this.renderingStates.FINISHED:
                return false;
            case this.renderingStates.PAUSED:
                this.highestPriorityPage = view.renderingId;
                view.resume();
                break;
            case this.renderingStates.RUNNING:
                this.highestPriorityPage = view.renderingId;
                break;
            case this.renderingStates.INITIAL:
                this.highestPriorityPage = view.renderingId;
                /** @type {?} */
                var continueRendering = (/**
                 * @return {?}
                 */
                function () {
                    this.renderHighestPriority();
                }).bind(this);
                view.draw().then(continueRendering, continueRendering);
                break;
            default:
                break;
        }
        return true;
    };
    RenderingQueueServices.decorators = [
        { type: Injectable }
    ];
    return RenderingQueueServices;
}());
export { RenderingQueueServices };
if (false) {
    /** @type {?} */
    RenderingQueueServices.prototype.renderingStates;
    /** @type {?} */
    RenderingQueueServices.prototype.CLEANUP_TIMEOUT;
    /** @type {?} */
    RenderingQueueServices.prototype.pdfViewer;
    /** @type {?} */
    RenderingQueueServices.prototype.pdfThumbnailViewer;
    /** @type {?} */
    RenderingQueueServices.prototype.onIdle;
    /** @type {?} */
    RenderingQueueServices.prototype.highestPriorityPage;
    /** @type {?} */
    RenderingQueueServices.prototype.idleTimeout;
    /** @type {?} */
    RenderingQueueServices.prototype.printing;
    /** @type {?} */
    RenderingQueueServices.prototype.isThumbnailViewEnabled;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoicmVuZGVyaW5nLXF1ZXVlLnNlcnZpY2VzLmpzIiwic291cmNlUm9vdCI6Im5nOi8vQGFsZnJlc2NvL2FkZi1jb3JlLyIsInNvdXJjZXMiOlsidmlld2VyL3NlcnZpY2VzL3JlbmRlcmluZy1xdWV1ZS5zZXJ2aWNlcy50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7Ozs7OztBQU8zQztJQUFBO1FBR0ksb0JBQWUsR0FBRztZQUNkLE9BQU8sRUFBRSxDQUFDO1lBQ1YsT0FBTyxFQUFFLENBQUM7WUFDVixNQUFNLEVBQUUsQ0FBQztZQUNULFFBQVEsRUFBRSxDQUFDO1NBQ2QsQ0FBQztRQUVGLG9CQUFlLEdBQVcsS0FBSyxDQUFDO1FBRWhDLGNBQVMsR0FBUSxJQUFJLENBQUM7UUFDdEIsdUJBQWtCLEdBQVEsSUFBSSxDQUFDO1FBQy9CLFdBQU0sR0FBUSxJQUFJLENBQUM7UUFFbkIsd0JBQW1CLEdBQVEsSUFBSSxDQUFDO1FBQ2hDLGdCQUFXLEdBQVEsSUFBSSxDQUFDO1FBQ3hCLGFBQVEsR0FBUSxLQUFLLENBQUM7UUFDdEIsMkJBQXNCLEdBQVEsS0FBSyxDQUFDO0lBNEh4QyxDQUFDO0lBMUhHOztPQUVHOzs7OztJQUNILDBDQUFTOzs7O0lBQVQsVUFBVSxTQUFTO1FBQ2YsSUFBSSxDQUFDLFNBQVMsR0FBRyxTQUFTLENBQUM7SUFDL0IsQ0FBQztJQUVEOztPQUVHOzs7OztJQUNILG1EQUFrQjs7OztJQUFsQixVQUFtQixrQkFBa0I7UUFDakMsSUFBSSxDQUFDLGtCQUFrQixHQUFHLGtCQUFrQixDQUFDO0lBQ2pELENBQUM7SUFFRDs7T0FFRzs7Ozs7SUFDSCxrREFBaUI7Ozs7SUFBakIsVUFBa0IsSUFBUztRQUN2QixPQUFPLElBQUksQ0FBQyxtQkFBbUIsS0FBSyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQ3pELENBQUM7Ozs7O0lBRUQsc0RBQXFCOzs7O0lBQXJCLFVBQXNCLHFCQUFxQjtRQUN2QyxJQUFJLElBQUksQ0FBQyxXQUFXLEVBQUU7WUFDbEIsWUFBWSxDQUFDLElBQUksQ0FBQyxXQUFXLENBQUMsQ0FBQztZQUMvQixJQUFJLENBQUMsV0FBVyxHQUFHLElBQUksQ0FBQztTQUMzQjtRQUVELHFFQUFxRTtRQUNyRSxJQUFJLElBQUksQ0FBQyxTQUFTLENBQUMsY0FBYyxDQUFDLHFCQUFxQixDQUFDLEVBQUU7WUFDdEQsT0FBTztTQUNWO1FBQ0QsaURBQWlEO1FBQ2pELElBQUksSUFBSSxDQUFDLGtCQUFrQixJQUFJLElBQUksQ0FBQyxzQkFBc0IsRUFBRTtZQUN4RCxJQUFJLElBQUksQ0FBQyxrQkFBa0IsQ0FBQyxjQUFjLEVBQUUsRUFBRTtnQkFDMUMsT0FBTzthQUNWO1NBQ0o7UUFFRCxJQUFJLElBQUksQ0FBQyxRQUFRLEVBQUU7WUFDZiw4REFBOEQ7WUFDOUQsT0FBTztTQUNWO1FBRUQsSUFBSSxJQUFJLENBQUMsTUFBTSxFQUFFO1lBQ2IsSUFBSSxDQUFDLFdBQVcsR0FBRyxVQUFVLENBQUMsSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLEVBQUUsSUFBSSxDQUFDLGVBQWUsQ0FBQyxDQUFDO1NBQy9FO0lBQ0wsQ0FBQzs7Ozs7OztJQUVELG1EQUFrQjs7Ozs7O0lBQWxCLFVBQW1CLE9BQU8sRUFBRSxLQUFLLEVBQUUsWUFBWTs7Ozs7Ozs7WUFPckMsWUFBWSxHQUFHLE9BQU8sQ0FBQyxLQUFLOztZQUU1QixVQUFVLEdBQUcsWUFBWSxDQUFDLE1BQU07UUFDdEMsSUFBSSxVQUFVLEtBQUssQ0FBQyxFQUFFO1lBQ2xCLE9BQU8sS0FBSyxDQUFDO1NBQ2hCO1FBQ0QsS0FBSyxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUUsQ0FBQyxHQUFHLFVBQVUsRUFBRSxFQUFFLENBQUMsRUFBRTs7Z0JBQzNCLElBQUksR0FBRyxZQUFZLENBQUMsQ0FBQyxDQUFDLENBQUMsSUFBSTtZQUNqQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsRUFBRTtnQkFDNUIsT0FBTyxJQUFJLENBQUM7YUFDZjtTQUNKO1FBRUQsMEVBQTBFO1FBQzFFLElBQUksWUFBWSxFQUFFOztnQkFDUixhQUFhLEdBQUcsT0FBTyxDQUFDLElBQUksQ0FBQyxFQUFFO1lBQ3JDLHVDQUF1QztZQUN2QyxJQUFJLEtBQUssQ0FBQyxhQUFhLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxjQUFjLENBQUMsS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDLEVBQUU7Z0JBQ3BFLE9BQU8sS0FBSyxDQUFDLGFBQWEsQ0FBQyxDQUFDO2FBQy9CO1NBQ0o7YUFBTTs7Z0JBQ0csaUJBQWlCLEdBQUcsT0FBTyxDQUFDLEtBQUssQ0FBQyxFQUFFLEdBQUcsQ0FBQztZQUM5QyxJQUFJLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLGNBQWMsQ0FBQyxLQUFLLENBQUMsaUJBQWlCLENBQUMsQ0FBQyxFQUFFO2dCQUM1RSxPQUFPLEtBQUssQ0FBQyxpQkFBaUIsQ0FBQyxDQUFDO2FBQ25DO1NBQ0o7UUFDRCxpREFBaUQ7UUFDakQsT0FBTyxJQUFJLENBQUM7SUFDaEIsQ0FBQztJQUVEOztPQUVHOzs7OztJQUNILCtDQUFjOzs7O0lBQWQsVUFBZSxJQUFJO1FBQ2YsT0FBTyxJQUFJLENBQUMsY0FBYyxLQUFLLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUSxDQUFDO0lBQ2pFLENBQUM7SUFFRDs7Ozs7T0FLRzs7Ozs7Ozs7SUFDSCwyQ0FBVTs7Ozs7OztJQUFWLFVBQVcsSUFBUzs7WUFDVixLQUFLLEdBQUcsSUFBSSxDQUFDLGNBQWM7UUFDakMsUUFBUSxLQUFLLEVBQUU7WUFDWCxLQUFLLElBQUksQ0FBQyxlQUFlLENBQUMsUUFBUTtnQkFDOUIsT0FBTyxLQUFLLENBQUM7WUFDakIsS0FBSyxJQUFJLENBQUMsZUFBZSxDQUFDLE1BQU07Z0JBQzVCLElBQUksQ0FBQyxtQkFBbUIsR0FBRyxJQUFJLENBQUMsV0FBVyxDQUFDO2dCQUM1QyxJQUFJLENBQUMsTUFBTSxFQUFFLENBQUM7Z0JBQ2QsTUFBTTtZQUNWLEtBQUssSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPO2dCQUM3QixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQztnQkFDNUMsTUFBTTtZQUNWLEtBQUssSUFBSSxDQUFDLGVBQWUsQ0FBQyxPQUFPO2dCQUM3QixJQUFJLENBQUMsbUJBQW1CLEdBQUcsSUFBSSxDQUFDLFdBQVcsQ0FBQzs7b0JBQ3RDLGlCQUFpQixHQUFHOzs7Z0JBQUE7b0JBQ3RCLElBQUksQ0FBQyxxQkFBcUIsRUFBRSxDQUFDO2dCQUNqQyxDQUFDLEVBQUMsSUFBSSxDQUFDLElBQUksQ0FBQztnQkFDWixJQUFJLENBQUMsSUFBSSxFQUFFLENBQUMsSUFBSSxDQUFDLGlCQUFpQixFQUFFLGlCQUFpQixDQUFDLENBQUM7Z0JBQ3ZELE1BQU07WUFDVjtnQkFDSSxNQUFNO1NBQ2I7UUFDRCxPQUFPLElBQUksQ0FBQztJQUNoQixDQUFDOztnQkE5SUosVUFBVTs7SUErSVgsNkJBQUM7Q0FBQSxBQS9JRCxJQStJQztTQTlJWSxzQkFBc0I7OztJQUUvQixpREFLRTs7SUFFRixpREFBZ0M7O0lBRWhDLDJDQUFzQjs7SUFDdEIsb0RBQStCOztJQUMvQix3Q0FBbUI7O0lBRW5CLHFEQUFnQzs7SUFDaEMsNkNBQXdCOztJQUN4QiwwQ0FBc0I7O0lBQ3RCLHdEQUFvQyIsInNvdXJjZXNDb250ZW50IjpbIi8qIHRzbGludDpkaXNhYmxlOmFkZi1saWNlbnNlLWJhbm5lciAgKi9cclxuXHJcbi8qIENvcHlyaWdodCAyMDEyIE1vemlsbGEgRm91bmRhdGlvblxyXG4gKlxyXG4gKiBMaWNlbnNlZCB1bmRlciB0aGUgQXBhY2hlIExpY2Vuc2UsIFZlcnNpb24gMi4wICh0aGUgXCJMaWNlbnNlXCIpO1xyXG4gKiB5b3UgbWF5IG5vdCB1c2UgdGhpcyBmaWxlIGV4Y2VwdCBpbiBjb21wbGlhbmNlIHdpdGggdGhlIExpY2Vuc2UuXHJcbiAqIFlvdSBtYXkgb2J0YWluIGEgY29weSBvZiB0aGUgTGljZW5zZSBhdFxyXG4gKlxyXG4gKiAgICAgaHR0cDovL3d3dy5hcGFjaGUub3JnL2xpY2Vuc2VzL0xJQ0VOU0UtMi4wXHJcbiAqXHJcbiAqIFVubGVzcyByZXF1aXJlZCBieSBhcHBsaWNhYmxlIGxhdyBvciBhZ3JlZWQgdG8gaW4gd3JpdGluZywgc29mdHdhcmVcclxuICogZGlzdHJpYnV0ZWQgdW5kZXIgdGhlIExpY2Vuc2UgaXMgZGlzdHJpYnV0ZWQgb24gYW4gXCJBUyBJU1wiIEJBU0lTLFxyXG4gKiBXSVRIT1VUIFdBUlJBTlRJRVMgT1IgQ09ORElUSU9OUyBPRiBBTlkgS0lORCwgZWl0aGVyIGV4cHJlc3Mgb3IgaW1wbGllZC5cclxuICogU2VlIHRoZSBMaWNlbnNlIGZvciB0aGUgc3BlY2lmaWMgbGFuZ3VhZ2UgZ292ZXJuaW5nIHBlcm1pc3Npb25zIGFuZFxyXG4gKiBsaW1pdGF0aW9ucyB1bmRlciB0aGUgTGljZW5zZS5cclxuICovXHJcblxyXG5pbXBvcnQgeyBJbmplY3RhYmxlIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XHJcblxyXG4vKipcclxuICpcclxuICogUmVuZGVyaW5nUXVldWVTZXJ2aWNlcyByZW5kZXJpbmcgb2YgdGhlIHZpZXdzIGZvciBwYWdlcyBhbmQgdGh1bWJuYWlscy5cclxuICpcclxuICovXHJcbkBJbmplY3RhYmxlKClcclxuZXhwb3J0IGNsYXNzIFJlbmRlcmluZ1F1ZXVlU2VydmljZXMge1xyXG5cclxuICAgIHJlbmRlcmluZ1N0YXRlcyA9IHtcclxuICAgICAgICBJTklUSUFMOiAwLFxyXG4gICAgICAgIFJVTk5JTkc6IDEsXHJcbiAgICAgICAgUEFVU0VEOiAyLFxyXG4gICAgICAgIEZJTklTSEVEOiAzXHJcbiAgICB9O1xyXG5cclxuICAgIENMRUFOVVBfVElNRU9VVDogbnVtYmVyID0gMzAwMDA7XHJcblxyXG4gICAgcGRmVmlld2VyOiBhbnkgPSBudWxsO1xyXG4gICAgcGRmVGh1bWJuYWlsVmlld2VyOiBhbnkgPSBudWxsO1xyXG4gICAgb25JZGxlOiBhbnkgPSBudWxsO1xyXG5cclxuICAgIGhpZ2hlc3RQcmlvcml0eVBhZ2U6IGFueSA9IG51bGw7XHJcbiAgICBpZGxlVGltZW91dDogYW55ID0gbnVsbDtcclxuICAgIHByaW50aW5nOiBhbnkgPSBmYWxzZTtcclxuICAgIGlzVGh1bWJuYWlsVmlld0VuYWJsZWQ6IGFueSA9IGZhbHNlO1xyXG5cclxuICAgIC8qKlxyXG4gICAgICogQHBhcmFtIHBkZlZpZXdlclxyXG4gICAgICovXHJcbiAgICBzZXRWaWV3ZXIocGRmVmlld2VyKSB7XHJcbiAgICAgICAgdGhpcy5wZGZWaWV3ZXIgPSBwZGZWaWV3ZXI7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAcGFyYW0gcGRmVGh1bWJuYWlsVmlld2VyXHJcbiAgICAgKi9cclxuICAgIHNldFRodW1ibmFpbFZpZXdlcihwZGZUaHVtYm5haWxWaWV3ZXIpIHtcclxuICAgICAgICB0aGlzLnBkZlRodW1ibmFpbFZpZXdlciA9IHBkZlRodW1ibmFpbFZpZXdlcjtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIEBwYXJhbSAgdmlld1xyXG4gICAgICovXHJcbiAgICBpc0hpZ2hlc3RQcmlvcml0eSh2aWV3OiBhbnkpOiBib29sZWFuIHtcclxuICAgICAgICByZXR1cm4gdGhpcy5oaWdoZXN0UHJpb3JpdHlQYWdlID09PSB2aWV3LnJlbmRlcmluZ0lkO1xyXG4gICAgfVxyXG5cclxuICAgIHJlbmRlckhpZ2hlc3RQcmlvcml0eShjdXJyZW50bHlWaXNpYmxlUGFnZXMpIHtcclxuICAgICAgICBpZiAodGhpcy5pZGxlVGltZW91dCkge1xyXG4gICAgICAgICAgICBjbGVhclRpbWVvdXQodGhpcy5pZGxlVGltZW91dCk7XHJcbiAgICAgICAgICAgIHRoaXMuaWRsZVRpbWVvdXQgPSBudWxsO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgLy8gUGFnZXMgaGF2ZSBhIGhpZ2hlciBwcmlvcml0eSB0aGFuIHRodW1ibmFpbHMsIHNvIGNoZWNrIHRoZW0gZmlyc3QuXHJcbiAgICAgICAgaWYgKHRoaXMucGRmVmlld2VyLmZvcmNlUmVuZGVyaW5nKGN1cnJlbnRseVZpc2libGVQYWdlcykpIHtcclxuICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBObyBwYWdlcyBuZWVkZWQgcmVuZGVyaW5nIHNvIGNoZWNrIHRodW1ibmFpbHMuXHJcbiAgICAgICAgaWYgKHRoaXMucGRmVGh1bWJuYWlsVmlld2VyICYmIHRoaXMuaXNUaHVtYm5haWxWaWV3RW5hYmxlZCkge1xyXG4gICAgICAgICAgICBpZiAodGhpcy5wZGZUaHVtYm5haWxWaWV3ZXIuZm9yY2VSZW5kZXJpbmcoKSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5wcmludGluZykge1xyXG4gICAgICAgICAgICAvLyBJZiBwcmludGluZyBpcyBjdXJyZW50bHkgb25nb2luZyBkbyBub3QgcmVzY2hlZHVsZSBjbGVhbnVwLlxyXG4gICAgICAgICAgICByZXR1cm47XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAodGhpcy5vbklkbGUpIHtcclxuICAgICAgICAgICAgdGhpcy5pZGxlVGltZW91dCA9IHNldFRpbWVvdXQodGhpcy5vbklkbGUuYmluZCh0aGlzKSwgdGhpcy5DTEVBTlVQX1RJTUVPVVQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRIaWdoZXN0UHJpb3JpdHkodmlzaWJsZSwgdmlld3MsIHNjcm9sbGVkRG93bikge1xyXG4gICAgICAgIC8vIFRoZSBzdGF0ZSBoYXMgY2hhbmdlZCBmaWd1cmUgb3V0IHdoaWNoIHBhZ2UgaGFzIHRoZSBoaWdoZXN0IHByaW9yaXR5IHRvXHJcbiAgICAgICAgLy8gcmVuZGVyIG5leHQgKGlmIGFueSkuXHJcbiAgICAgICAgLy8gUHJpb3JpdHk6XHJcbiAgICAgICAgLy8gMSB2aXNpYmxlIHBhZ2VzXHJcbiAgICAgICAgLy8gMiBpZiBsYXN0IHNjcm9sbGVkIGRvd24gcGFnZSBhZnRlciB0aGUgdmlzaWJsZSBwYWdlc1xyXG4gICAgICAgIC8vIDIgaWYgbGFzdCBzY3JvbGxlZCB1cCBwYWdlIGJlZm9yZSB0aGUgdmlzaWJsZSBwYWdlc1xyXG4gICAgICAgIGNvbnN0IHZpc2libGVWaWV3cyA9IHZpc2libGUudmlld3M7XHJcblxyXG4gICAgICAgIGNvbnN0IG51bVZpc2libGUgPSB2aXNpYmxlVmlld3MubGVuZ3RoO1xyXG4gICAgICAgIGlmIChudW1WaXNpYmxlID09PSAwKSB7XHJcbiAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICB9XHJcbiAgICAgICAgZm9yIChsZXQgaSA9IDA7IGkgPCBudW1WaXNpYmxlOyArK2kpIHtcclxuICAgICAgICAgICAgY29uc3QgdmlldyA9IHZpc2libGVWaWV3c1tpXS52aWV3O1xyXG4gICAgICAgICAgICBpZiAoIXRoaXMuaXNWaWV3RmluaXNoZWQodmlldykpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB2aWV3O1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICAvLyBBbGwgdGhlIHZpc2libGUgdmlld3MgaGF2ZSByZW5kZXJlZCwgdHJ5IHRvIHJlbmRlciBuZXh0L3ByZXZpb3VzIHBhZ2VzLlxyXG4gICAgICAgIGlmIChzY3JvbGxlZERvd24pIHtcclxuICAgICAgICAgICAgY29uc3QgbmV4dFBhZ2VJbmRleCA9IHZpc2libGUubGFzdC5pZDtcclxuICAgICAgICAgICAgLy8gSUQncyBzdGFydCBhdCAxIHNvIG5vIG5lZWQgdG8gYWRkIDEuXHJcbiAgICAgICAgICAgIGlmICh2aWV3c1tuZXh0UGFnZUluZGV4XSAmJiAhdGhpcy5pc1ZpZXdGaW5pc2hlZCh2aWV3c1tuZXh0UGFnZUluZGV4XSkpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiB2aWV3c1tuZXh0UGFnZUluZGV4XTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgIGNvbnN0IHByZXZpb3VzUGFnZUluZGV4ID0gdmlzaWJsZS5maXJzdC5pZCAtIDI7XHJcbiAgICAgICAgICAgIGlmICh2aWV3c1twcmV2aW91c1BhZ2VJbmRleF0gJiYgIXRoaXMuaXNWaWV3RmluaXNoZWQodmlld3NbcHJldmlvdXNQYWdlSW5kZXhdKSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHZpZXdzW3ByZXZpb3VzUGFnZUluZGV4XTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICAvLyBFdmVyeXRoaW5nIHRoYXQgbmVlZHMgdG8gYmUgcmVuZGVyZWQgaGFzIGJlZW4uXHJcbiAgICAgICAgcmV0dXJuIG51bGw7XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBAcGFyYW0gdmlld1xyXG4gICAgICovXHJcbiAgICBpc1ZpZXdGaW5pc2hlZCh2aWV3KTogYm9vbGVhbiB7XHJcbiAgICAgICAgcmV0dXJuIHZpZXcucmVuZGVyaW5nU3RhdGUgPT09IHRoaXMucmVuZGVyaW5nU3RhdGVzLkZJTklTSEVEO1xyXG4gICAgfVxyXG5cclxuICAgIC8qKlxyXG4gICAgICogUmVuZGVyIGEgcGFnZSBvciB0aHVtYm5haWwgdmlldy4gVGhpcyBjYWxscyB0aGUgYXBwcm9wcmlhdGUgZnVuY3Rpb25cclxuICAgICAqIGJhc2VkIG9uIHRoZSB2aWV3cyBzdGF0ZS4gSWYgdGhlIHZpZXcgaXMgYWxyZWFkeSByZW5kZXJlZCBpdCB3aWxsIHJldHVyblxyXG4gICAgICogZmFsc2UuXHJcbiAgICAgKiBAcGFyYW0gdmlld1xyXG4gICAgICovXHJcbiAgICByZW5kZXJWaWV3KHZpZXc6IGFueSkge1xyXG4gICAgICAgIGNvbnN0IHN0YXRlID0gdmlldy5yZW5kZXJpbmdTdGF0ZTtcclxuICAgICAgICBzd2l0Y2ggKHN0YXRlKSB7XHJcbiAgICAgICAgICAgIGNhc2UgdGhpcy5yZW5kZXJpbmdTdGF0ZXMuRklOSVNIRUQ6XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgIGNhc2UgdGhpcy5yZW5kZXJpbmdTdGF0ZXMuUEFVU0VEOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5oaWdoZXN0UHJpb3JpdHlQYWdlID0gdmlldy5yZW5kZXJpbmdJZDtcclxuICAgICAgICAgICAgICAgIHZpZXcucmVzdW1lKCk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSB0aGlzLnJlbmRlcmluZ1N0YXRlcy5SVU5OSU5HOlxyXG4gICAgICAgICAgICAgICAgdGhpcy5oaWdoZXN0UHJpb3JpdHlQYWdlID0gdmlldy5yZW5kZXJpbmdJZDtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlIHRoaXMucmVuZGVyaW5nU3RhdGVzLklOSVRJQUw6XHJcbiAgICAgICAgICAgICAgICB0aGlzLmhpZ2hlc3RQcmlvcml0eVBhZ2UgPSB2aWV3LnJlbmRlcmluZ0lkO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgY29udGludWVSZW5kZXJpbmcgPSBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdGhpcy5yZW5kZXJIaWdoZXN0UHJpb3JpdHkoKTtcclxuICAgICAgICAgICAgICAgIH0uYmluZCh0aGlzKTtcclxuICAgICAgICAgICAgICAgIHZpZXcuZHJhdygpLnRoZW4oY29udGludWVSZW5kZXJpbmcsIGNvbnRpbnVlUmVuZGVyaW5nKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiB0cnVlO1xyXG4gICAgfVxyXG59XHJcbiJdfQ==