/**
 * @fileoverview added by tsickle
 * @suppress {checkTypes,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import * as tslib_1 from "tslib";
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Injectable } from '@angular/core';
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import * as i0 from "@angular/core";
import * as i1 from "../../services/alfresco-api.service";
import * as i2 from "../../services/log.service";
var ViewUtilService = /** @class */ (function () {
    function ViewUtilService(apiService, logService) {
        this.apiService = apiService;
        this.logService = logService;
        /**
         * Based on ViewerComponent Implementation, this value is used to determine how many times we try
         * to get the rendition of a file for preview, or printing.
         */
        this.maxRetries = 5;
        /**
         * Mime-type grouping based on the ViewerComponent.
         */
        this.mimeTypes = {
            text: ['text/plain', 'text/csv', 'text/xml', 'text/html', 'application/x-javascript'],
            pdf: ['application/pdf'],
            image: ['image/png', 'image/jpeg', 'image/gif', 'image/bmp', 'image/svg+xml'],
            media: ['video/mp4', 'video/webm', 'video/ogg', 'audio/mpeg', 'audio/ogg', 'audio/wav']
        };
    }
    /**
     * This method takes a url to trigger the print dialog against, and the type of artifact that it
     * is.
     * This URL should be one that can be rendered in the browser, for example PDF, Image, or Text
     */
    /**
     * This method takes a url to trigger the print dialog against, and the type of artifact that it
     * is.
     * This URL should be one that can be rendered in the browser, for example PDF, Image, or Text
     * @param {?} url
     * @param {?} type
     * @return {?}
     */
    ViewUtilService.prototype.printFile = /**
     * This method takes a url to trigger the print dialog against, and the type of artifact that it
     * is.
     * This URL should be one that can be rendered in the browser, for example PDF, Image, or Text
     * @param {?} url
     * @param {?} type
     * @return {?}
     */
    function (url, type) {
        /** @type {?} */
        var pwa = window.open(url, ViewUtilService.TARGET);
        if (pwa) {
            // Because of the way chrome focus and close image window vs. pdf preview window
            if (type === ViewUtilService.ContentGroup.IMAGE) {
                pwa.onfocus = (/**
                 * @return {?}
                 */
                function () {
                    setTimeout((/**
                     * @return {?}
                     */
                    function () {
                        pwa.close();
                    }), 500);
                });
            }
            pwa.onload = (/**
             * @return {?}
             */
            function () {
                pwa.print();
            });
        }
    };
    /**
     * Launch the File Print dialog from anywhere other than the preview service, which resolves the
     * rendition of the object that can be printed from a web browser.
     * These are: images, PDF files, or PDF rendition of files.
     * We also force PDF rendition for TEXT type objects, otherwise the default URL is to download.
     * TODO there are different TEXT type objects, (HTML, plaintext, xml, etc. we should determine how these are handled)
     */
    /**
     * Launch the File Print dialog from anywhere other than the preview service, which resolves the
     * rendition of the object that can be printed from a web browser.
     * These are: images, PDF files, or PDF rendition of files.
     * We also force PDF rendition for TEXT type objects, otherwise the default URL is to download.
     * TODO there are different TEXT type objects, (HTML, plaintext, xml, etc. we should determine how these are handled)
     * @param {?} objectId
     * @param {?} mimeType
     * @return {?}
     */
    ViewUtilService.prototype.printFileGeneric = /**
     * Launch the File Print dialog from anywhere other than the preview service, which resolves the
     * rendition of the object that can be printed from a web browser.
     * These are: images, PDF files, or PDF rendition of files.
     * We also force PDF rendition for TEXT type objects, otherwise the default URL is to download.
     * TODO there are different TEXT type objects, (HTML, plaintext, xml, etc. we should determine how these are handled)
     * @param {?} objectId
     * @param {?} mimeType
     * @return {?}
     */
    function (objectId, mimeType) {
        var _this = this;
        /** @type {?} */
        var nodeId = objectId;
        /** @type {?} */
        var type = this.getViewerTypeByMimeType(mimeType);
        this.getRendition(nodeId, ViewUtilService.ContentGroup.PDF)
            .then((/**
         * @param {?} value
         * @return {?}
         */
        function (value) {
            /** @type {?} */
            var url = _this.getRenditionUrl(nodeId, type, (value ? true : false));
            /** @type {?} */
            var printType = (type === ViewUtilService.ContentGroup.PDF
                || type === ViewUtilService.ContentGroup.TEXT)
                ? ViewUtilService.ContentGroup.PDF : type;
            _this.printFile(url, printType);
        }))
            .catch((/**
         * @param {?} err
         * @return {?}
         */
        function (err) {
            _this.logService.error('Error with Printing');
            _this.logService.error(err);
        }));
    };
    /**
     * @param {?} nodeId
     * @param {?} type
     * @param {?} renditionExists
     * @return {?}
     */
    ViewUtilService.prototype.getRenditionUrl = /**
     * @param {?} nodeId
     * @param {?} type
     * @param {?} renditionExists
     * @return {?}
     */
    function (nodeId, type, renditionExists) {
        return (renditionExists && type !== ViewUtilService.ContentGroup.IMAGE) ?
            this.apiService.contentApi.getRenditionUrl(nodeId, ViewUtilService.ContentGroup.PDF) :
            this.apiService.contentApi.getContentUrl(nodeId, false);
    };
    /**
     * @private
     * @param {?} nodeId
     * @param {?} renditionId
     * @param {?} retries
     * @return {?}
     */
    ViewUtilService.prototype.waitRendition = /**
     * @private
     * @param {?} nodeId
     * @param {?} renditionId
     * @param {?} retries
     * @return {?}
     */
    function (nodeId, renditionId, retries) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var rendition, status_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.apiService.renditionsApi.getRendition(nodeId, renditionId)];
                    case 1:
                        rendition = _a.sent();
                        if (!(this.maxRetries < retries)) return [3 /*break*/, 5];
                        status_1 = rendition.entry.status.toString();
                        if (!(status_1 === 'CREATED')) return [3 /*break*/, 2];
                        return [2 /*return*/, rendition];
                    case 2:
                        retries += 1;
                        return [4 /*yield*/, this.wait(1000)];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, this.waitRendition(nodeId, renditionId, retries)];
                    case 4: return [2 /*return*/, _a.sent()];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    /**
     * @param {?} mimeType
     * @return {?}
     */
    ViewUtilService.prototype.getViewerTypeByMimeType = /**
     * @param {?} mimeType
     * @return {?}
     */
    function (mimeType) {
        var e_1, _a;
        if (mimeType) {
            mimeType = mimeType.toLowerCase();
            /** @type {?} */
            var editorTypes = Object.keys(this.mimeTypes);
            try {
                for (var editorTypes_1 = tslib_1.__values(editorTypes), editorTypes_1_1 = editorTypes_1.next(); !editorTypes_1_1.done; editorTypes_1_1 = editorTypes_1.next()) {
                    var type = editorTypes_1_1.value;
                    if (this.mimeTypes[type].indexOf(mimeType) >= 0) {
                        return type;
                    }
                }
            }
            catch (e_1_1) { e_1 = { error: e_1_1 }; }
            finally {
                try {
                    if (editorTypes_1_1 && !editorTypes_1_1.done && (_a = editorTypes_1.return)) _a.call(editorTypes_1);
                }
                finally { if (e_1) throw e_1.error; }
            }
        }
        return 'unknown';
    };
    /**
     * @param {?} ms
     * @return {?}
     */
    ViewUtilService.prototype.wait = /**
     * @param {?} ms
     * @return {?}
     */
    function (ms) {
        return new Promise((/**
         * @param {?} resolve
         * @return {?}
         */
        function (resolve) { return setTimeout(resolve, ms); }));
    };
    /**
     * @param {?} nodeId
     * @param {?} renditionId
     * @return {?}
     */
    ViewUtilService.prototype.getRendition = /**
     * @param {?} nodeId
     * @param {?} renditionId
     * @return {?}
     */
    function (nodeId, renditionId) {
        return tslib_1.__awaiter(this, void 0, void 0, function () {
            var renditionPaging, rendition, status_2, err_1;
            return tslib_1.__generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.apiService.renditionsApi.getRenditions(nodeId)];
                    case 1:
                        renditionPaging = _a.sent();
                        rendition = renditionPaging.list.entries.find((/**
                         * @param {?} renditionEntry
                         * @return {?}
                         */
                        function (renditionEntry) { return renditionEntry.entry.id.toLowerCase() === renditionId; }));
                        if (!rendition) return [3 /*break*/, 6];
                        status_2 = rendition.entry.status.toString();
                        if (!(status_2 === 'NOT_CREATED')) return [3 /*break*/, 6];
                        _a.label = 2;
                    case 2:
                        _a.trys.push([2, 5, , 6]);
                        return [4 /*yield*/, this.apiService.renditionsApi.createRendition(nodeId, { id: renditionId })];
                    case 3:
                        _a.sent();
                        return [4 /*yield*/, this.waitRendition(nodeId, renditionId, 0)];
                    case 4:
                        rendition = _a.sent();
                        return [3 /*break*/, 6];
                    case 5:
                        err_1 = _a.sent();
                        this.logService.error(err_1);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/, new Promise((/**
                         * @param {?} resolve
                         * @return {?}
                         */
                        function (resolve) { return resolve(rendition); }))];
                }
            });
        });
    };
    ViewUtilService.TARGET = '_new';
    /**
     * Content groups based on categorization of files that can be viewed in the web browser. This
     * implementation or grouping is tied to the definition the ng component: ViewerComponent
     */
    // tslint:disable-next-line:variable-name
    ViewUtilService.ContentGroup = {
        IMAGE: 'image',
        MEDIA: 'media',
        PDF: 'pdf',
        TEXT: 'text'
    };
    ViewUtilService.decorators = [
        { type: Injectable, args: [{
                    providedIn: 'root'
                },] }
    ];
    /** @nocollapse */
    ViewUtilService.ctorParameters = function () { return [
        { type: AlfrescoApiService },
        { type: LogService }
    ]; };
    /** @nocollapse */ ViewUtilService.ngInjectableDef = i0.ɵɵdefineInjectable({ factory: function ViewUtilService_Factory() { return new ViewUtilService(i0.ɵɵinject(i1.AlfrescoApiService), i0.ɵɵinject(i2.LogService)); }, token: ViewUtilService, providedIn: "root" });
    return ViewUtilService;
}());
export { ViewUtilService };
if (false) {
    /** @type {?} */
    ViewUtilService.TARGET;
    /**
     * Content groups based on categorization of files that can be viewed in the web browser. This
     * implementation or grouping is tied to the definition the ng component: ViewerComponent
     * @type {?}
     */
    ViewUtilService.ContentGroup;
    /**
     * Based on ViewerComponent Implementation, this value is used to determine how many times we try
     * to get the rendition of a file for preview, or printing.
     * @type {?}
     */
    ViewUtilService.prototype.maxRetries;
    /**
     * Mime-type grouping based on the ViewerComponent.
     * @type {?}
     * @private
     */
    ViewUtilService.prototype.mimeTypes;
    /**
     * @type {?}
     * @private
     */
    ViewUtilService.prototype.apiService;
    /**
     * @type {?}
     * @private
     */
    ViewUtilService.prototype.logService;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidmlldy11dGlsLnNlcnZpY2UuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9AYWxmcmVzY28vYWRmLWNvcmUvIiwic291cmNlcyI6WyJ2aWV3ZXIvc2VydmljZXMvdmlldy11dGlsLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FBaUJBLE9BQU8sRUFBRSxVQUFVLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFFM0MsT0FBTyxFQUFFLGtCQUFrQixFQUFFLE1BQU0scUNBQXFDLENBQUM7QUFDekUsT0FBTyxFQUFFLFVBQVUsRUFBRSxNQUFNLDRCQUE0QixDQUFDOzs7O0FBRXhEO0lBa0NJLHlCQUFvQixVQUE4QixFQUM5QixVQUFzQjtRQUR0QixlQUFVLEdBQVYsVUFBVSxDQUFvQjtRQUM5QixlQUFVLEdBQVYsVUFBVSxDQUFZOzs7OztRQWIxQyxlQUFVLEdBQUcsQ0FBQyxDQUFDOzs7O1FBS1AsY0FBUyxHQUFHO1lBQ2hCLElBQUksRUFBRSxDQUFDLFlBQVksRUFBRSxVQUFVLEVBQUUsVUFBVSxFQUFFLFdBQVcsRUFBRSwwQkFBMEIsQ0FBQztZQUNyRixHQUFHLEVBQUUsQ0FBQyxpQkFBaUIsQ0FBQztZQUN4QixLQUFLLEVBQUUsQ0FBQyxXQUFXLEVBQUUsWUFBWSxFQUFFLFdBQVcsRUFBRSxXQUFXLEVBQUUsZUFBZSxDQUFDO1lBQzdFLEtBQUssRUFBRSxDQUFDLFdBQVcsRUFBRSxZQUFZLEVBQUUsV0FBVyxFQUFFLFlBQVksRUFBRSxXQUFXLEVBQUUsV0FBVyxDQUFDO1NBQzFGLENBQUM7SUFJRixDQUFDO0lBRUQ7Ozs7T0FJRzs7Ozs7Ozs7O0lBQ0gsbUNBQVM7Ozs7Ozs7O0lBQVQsVUFBVSxHQUFXLEVBQUUsSUFBWTs7WUFDekIsR0FBRyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsR0FBRyxFQUFFLGVBQWUsQ0FBQyxNQUFNLENBQUM7UUFDcEQsSUFBSSxHQUFHLEVBQUU7WUFDTCxnRkFBZ0Y7WUFDaEYsSUFBSSxJQUFJLEtBQUssZUFBZSxDQUFDLFlBQVksQ0FBQyxLQUFLLEVBQUU7Z0JBQzdDLEdBQUcsQ0FBQyxPQUFPOzs7Z0JBQUc7b0JBQ1YsVUFBVTs7O29CQUFDO3dCQUNQLEdBQUcsQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDaEIsQ0FBQyxHQUFFLEdBQUcsQ0FBQyxDQUFDO2dCQUNaLENBQUMsQ0FBQSxDQUFDO2FBQ0w7WUFFRCxHQUFHLENBQUMsTUFBTTs7O1lBQUc7Z0JBQ1QsR0FBRyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2hCLENBQUMsQ0FBQSxDQUFDO1NBQ0w7SUFDTCxDQUFDO0lBRUQ7Ozs7OztPQU1HOzs7Ozs7Ozs7OztJQUNILDBDQUFnQjs7Ozs7Ozs7OztJQUFoQixVQUFpQixRQUFnQixFQUFFLFFBQWdCO1FBQW5ELGlCQWdCQzs7WUFmUyxNQUFNLEdBQUcsUUFBUTs7WUFDakIsSUFBSSxHQUFXLElBQUksQ0FBQyx1QkFBdUIsQ0FBQyxRQUFRLENBQUM7UUFFM0QsSUFBSSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsZUFBZSxDQUFDLFlBQVksQ0FBQyxHQUFHLENBQUM7YUFDdEQsSUFBSTs7OztRQUFDLFVBQUMsS0FBSzs7Z0JBQ0YsR0FBRyxHQUFXLEtBQUksQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLElBQUksRUFBRSxDQUFDLEtBQUssQ0FBQyxDQUFDLENBQUMsSUFBSSxDQUFDLENBQUMsQ0FBQyxLQUFLLENBQUMsQ0FBQzs7Z0JBQ3hFLFNBQVMsR0FBRyxDQUFDLElBQUksS0FBSyxlQUFlLENBQUMsWUFBWSxDQUFDLEdBQUc7bUJBQ3JELElBQUksS0FBSyxlQUFlLENBQUMsWUFBWSxDQUFDLElBQUksQ0FBQztnQkFDOUMsQ0FBQyxDQUFDLGVBQWUsQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQyxJQUFJO1lBQzdDLEtBQUksQ0FBQyxTQUFTLENBQUMsR0FBRyxFQUFFLFNBQVMsQ0FBQyxDQUFDO1FBQ25DLENBQUMsRUFBQzthQUNELEtBQUs7Ozs7UUFBQyxVQUFDLEdBQUc7WUFDUCxLQUFJLENBQUMsVUFBVSxDQUFDLEtBQUssQ0FBQyxxQkFBcUIsQ0FBQyxDQUFDO1lBQzdDLEtBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEdBQUcsQ0FBQyxDQUFDO1FBQy9CLENBQUMsRUFBQyxDQUFDO0lBQ1gsQ0FBQzs7Ozs7OztJQUVELHlDQUFlOzs7Ozs7SUFBZixVQUFnQixNQUFjLEVBQUUsSUFBWSxFQUFFLGVBQXdCO1FBQ2xFLE9BQU8sQ0FBQyxlQUFlLElBQUksSUFBSSxLQUFLLGVBQWUsQ0FBQyxZQUFZLENBQUMsS0FBSyxDQUFDLENBQUMsQ0FBQztZQUNyRSxJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxlQUFlLENBQUMsTUFBTSxFQUFFLGVBQWUsQ0FBQyxZQUFZLENBQUMsR0FBRyxDQUFDLENBQUMsQ0FBQztZQUN0RixJQUFJLENBQUMsVUFBVSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsTUFBTSxFQUFFLEtBQUssQ0FBQyxDQUFDO0lBQ2hFLENBQUM7Ozs7Ozs7O0lBRWEsdUNBQWE7Ozs7Ozs7SUFBM0IsVUFBNEIsTUFBYyxFQUFFLFdBQW1CLEVBQUUsT0FBZTs7Ozs7NEJBQzFELHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLFlBQVksQ0FBQyxNQUFNLEVBQUUsV0FBVyxDQUFDLEVBQUE7O3dCQUFqRixTQUFTLEdBQUcsU0FBcUU7NkJBRW5GLENBQUEsSUFBSSxDQUFDLFVBQVUsR0FBRyxPQUFPLENBQUEsRUFBekIsd0JBQXlCO3dCQUNuQixXQUFTLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTs2QkFFNUMsQ0FBQSxRQUFNLEtBQUssU0FBUyxDQUFBLEVBQXBCLHdCQUFvQjt3QkFDcEIsc0JBQU8sU0FBUyxFQUFDOzt3QkFFakIsT0FBTyxJQUFJLENBQUMsQ0FBQzt3QkFDYixxQkFBTSxJQUFJLENBQUMsSUFBSSxDQUFDLElBQUksQ0FBQyxFQUFBOzt3QkFBckIsU0FBcUIsQ0FBQzt3QkFDZixxQkFBTSxJQUFJLENBQUMsYUFBYSxDQUFDLE1BQU0sRUFBRSxXQUFXLEVBQUUsT0FBTyxDQUFDLEVBQUE7NEJBQTdELHNCQUFPLFNBQXNELEVBQUM7Ozs7O0tBR3pFOzs7OztJQUVELGlEQUF1Qjs7OztJQUF2QixVQUF3QixRQUFnQjs7UUFDcEMsSUFBSSxRQUFRLEVBQUU7WUFDVixRQUFRLEdBQUcsUUFBUSxDQUFDLFdBQVcsRUFBRSxDQUFDOztnQkFFNUIsV0FBVyxHQUFHLE1BQU0sQ0FBQyxJQUFJLENBQUMsSUFBSSxDQUFDLFNBQVMsQ0FBQzs7Z0JBQy9DLEtBQW1CLElBQUEsZ0JBQUEsaUJBQUEsV0FBVyxDQUFBLHdDQUFBLGlFQUFFO29CQUEzQixJQUFNLElBQUksd0JBQUE7b0JBQ1gsSUFBSSxJQUFJLENBQUMsU0FBUyxDQUFDLElBQUksQ0FBQyxDQUFDLE9BQU8sQ0FBQyxRQUFRLENBQUMsSUFBSSxDQUFDLEVBQUU7d0JBQzdDLE9BQU8sSUFBSSxDQUFDO3FCQUNmO2lCQUNKOzs7Ozs7Ozs7U0FDSjtRQUNELE9BQU8sU0FBUyxDQUFDO0lBQ3JCLENBQUM7Ozs7O0lBRUQsOEJBQUk7Ozs7SUFBSixVQUFLLEVBQVU7UUFDWCxPQUFPLElBQUksT0FBTzs7OztRQUFDLFVBQUMsT0FBTyxJQUFLLE9BQUEsVUFBVSxDQUFDLE9BQU8sRUFBRSxFQUFFLENBQUMsRUFBdkIsQ0FBdUIsRUFBQyxDQUFDO0lBQzdELENBQUM7Ozs7OztJQUVLLHNDQUFZOzs7OztJQUFsQixVQUFtQixNQUFjLEVBQUUsV0FBbUI7Ozs7OzRCQUNULHFCQUFNLElBQUksQ0FBQyxVQUFVLENBQUMsYUFBYSxDQUFDLGFBQWEsQ0FBQyxNQUFNLENBQUMsRUFBQTs7d0JBQTVGLGVBQWUsR0FBb0IsU0FBeUQ7d0JBQzlGLFNBQVMsR0FBbUIsZUFBZSxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSTs7Ozt3QkFBQyxVQUFDLGNBQThCLElBQUssT0FBQSxjQUFjLENBQUMsS0FBSyxDQUFDLEVBQUUsQ0FBQyxXQUFXLEVBQUUsS0FBSyxXQUFXLEVBQXJELENBQXFELEVBQUM7NkJBRXhKLFNBQVMsRUFBVCx3QkFBUzt3QkFDSCxXQUFTLFNBQVMsQ0FBQyxLQUFLLENBQUMsTUFBTSxDQUFDLFFBQVEsRUFBRTs2QkFFNUMsQ0FBQSxRQUFNLEtBQUssYUFBYSxDQUFBLEVBQXhCLHdCQUF3Qjs7Ozt3QkFFcEIscUJBQU0sSUFBSSxDQUFDLFVBQVUsQ0FBQyxhQUFhLENBQUMsZUFBZSxDQUFDLE1BQU0sRUFBRSxFQUFFLEVBQUUsRUFBRSxXQUFXLEVBQUUsQ0FBQyxFQUFBOzt3QkFBaEYsU0FBZ0YsQ0FBQzt3QkFDckUscUJBQU0sSUFBSSxDQUFDLGFBQWEsQ0FBQyxNQUFNLEVBQUUsV0FBVyxFQUFFLENBQUMsQ0FBQyxFQUFBOzt3QkFBNUQsU0FBUyxHQUFHLFNBQWdELENBQUM7Ozs7d0JBRTdELElBQUksQ0FBQyxVQUFVLENBQUMsS0FBSyxDQUFDLEtBQUcsQ0FBQyxDQUFDOzs0QkFJdkMsc0JBQU8sSUFBSSxPQUFPOzs7O3dCQUFpQixVQUFDLE9BQU8sSUFBSyxPQUFBLE9BQU8sQ0FBQyxTQUFTLENBQUMsRUFBbEIsQ0FBa0IsRUFBQyxFQUFDOzs7O0tBQ3ZFO0lBM0lNLHNCQUFNLEdBQUcsTUFBTSxDQUFDOzs7Ozs7SUFPaEIsNEJBQVksR0FBRztRQUNsQixLQUFLLEVBQUUsT0FBTztRQUNkLEtBQUssRUFBRSxPQUFPO1FBQ2QsR0FBRyxFQUFFLEtBQUs7UUFDVixJQUFJLEVBQUUsTUFBTTtLQUNmLENBQUM7O2dCQWhCTCxVQUFVLFNBQUM7b0JBQ1IsVUFBVSxFQUFFLE1BQU07aUJBQ3JCOzs7O2dCQUxRLGtCQUFrQjtnQkFDbEIsVUFBVTs7OzBCQXBCbkI7Q0F1S0MsQUFqSkQsSUFpSkM7U0E5SVksZUFBZTs7O0lBQ3hCLHVCQUF1Qjs7Ozs7O0lBT3ZCLDZCQUtFOzs7Ozs7SUFNRixxQ0FBZTs7Ozs7O0lBS2Ysb0NBS0U7Ozs7O0lBRVUscUNBQXNDOzs7OztJQUN0QyxxQ0FBOEIiLCJzb3VyY2VzQ29udGVudCI6WyIvKiFcclxuICogQGxpY2Vuc2VcclxuICogQ29weXJpZ2h0IDIwMTkgQWxmcmVzY28gU29mdHdhcmUsIEx0ZC5cclxuICpcclxuICogTGljZW5zZWQgdW5kZXIgdGhlIEFwYWNoZSBMaWNlbnNlLCBWZXJzaW9uIDIuMCAodGhlIFwiTGljZW5zZVwiKTtcclxuICogeW91IG1heSBub3QgdXNlIHRoaXMgZmlsZSBleGNlcHQgaW4gY29tcGxpYW5jZSB3aXRoIHRoZSBMaWNlbnNlLlxyXG4gKiBZb3UgbWF5IG9idGFpbiBhIGNvcHkgb2YgdGhlIExpY2Vuc2UgYXRcclxuICpcclxuICogICAgIGh0dHA6Ly93d3cuYXBhY2hlLm9yZy9saWNlbnNlcy9MSUNFTlNFLTIuMFxyXG4gKlxyXG4gKiBVbmxlc3MgcmVxdWlyZWQgYnkgYXBwbGljYWJsZSBsYXcgb3IgYWdyZWVkIHRvIGluIHdyaXRpbmcsIHNvZnR3YXJlXHJcbiAqIGRpc3RyaWJ1dGVkIHVuZGVyIHRoZSBMaWNlbnNlIGlzIGRpc3RyaWJ1dGVkIG9uIGFuIFwiQVMgSVNcIiBCQVNJUyxcclxuICogV0lUSE9VVCBXQVJSQU5USUVTIE9SIENPTkRJVElPTlMgT0YgQU5ZIEtJTkQsIGVpdGhlciBleHByZXNzIG9yIGltcGxpZWQuXHJcbiAqIFNlZSB0aGUgTGljZW5zZSBmb3IgdGhlIHNwZWNpZmljIGxhbmd1YWdlIGdvdmVybmluZyBwZXJtaXNzaW9ucyBhbmRcclxuICogbGltaXRhdGlvbnMgdW5kZXIgdGhlIExpY2Vuc2UuXHJcbiAqL1xyXG5cclxuaW1wb3J0IHsgSW5qZWN0YWJsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xyXG5pbXBvcnQgeyBSZW5kaXRpb25FbnRyeSwgUmVuZGl0aW9uUGFnaW5nIH0gZnJvbSAnQGFsZnJlc2NvL2pzLWFwaSc7XHJcbmltcG9ydCB7IEFsZnJlc2NvQXBpU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2FsZnJlc2NvLWFwaS5zZXJ2aWNlJztcclxuaW1wb3J0IHsgTG9nU2VydmljZSB9IGZyb20gJy4uLy4uL3NlcnZpY2VzL2xvZy5zZXJ2aWNlJztcclxuXHJcbkBJbmplY3RhYmxlKHtcclxuICAgIHByb3ZpZGVkSW46ICdyb290J1xyXG59KVxyXG5leHBvcnQgY2xhc3MgVmlld1V0aWxTZXJ2aWNlIHtcclxuICAgIHN0YXRpYyBUQVJHRVQgPSAnX25ldyc7XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBDb250ZW50IGdyb3VwcyBiYXNlZCBvbiBjYXRlZ29yaXphdGlvbiBvZiBmaWxlcyB0aGF0IGNhbiBiZSB2aWV3ZWQgaW4gdGhlIHdlYiBicm93c2VyLiBUaGlzXHJcbiAgICAgKiBpbXBsZW1lbnRhdGlvbiBvciBncm91cGluZyBpcyB0aWVkIHRvIHRoZSBkZWZpbml0aW9uIHRoZSBuZyBjb21wb25lbnQ6IFZpZXdlckNvbXBvbmVudFxyXG4gICAgICovXHJcbiAgICAgICAgLy8gdHNsaW50OmRpc2FibGUtbmV4dC1saW5lOnZhcmlhYmxlLW5hbWVcclxuICAgIHN0YXRpYyBDb250ZW50R3JvdXAgPSB7XHJcbiAgICAgICAgSU1BR0U6ICdpbWFnZScsXHJcbiAgICAgICAgTUVESUE6ICdtZWRpYScsXHJcbiAgICAgICAgUERGOiAncGRmJyxcclxuICAgICAgICBURVhUOiAndGV4dCdcclxuICAgIH07XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBCYXNlZCBvbiBWaWV3ZXJDb21wb25lbnQgSW1wbGVtZW50YXRpb24sIHRoaXMgdmFsdWUgaXMgdXNlZCB0byBkZXRlcm1pbmUgaG93IG1hbnkgdGltZXMgd2UgdHJ5XHJcbiAgICAgKiB0byBnZXQgdGhlIHJlbmRpdGlvbiBvZiBhIGZpbGUgZm9yIHByZXZpZXcsIG9yIHByaW50aW5nLlxyXG4gICAgICovXHJcbiAgICBtYXhSZXRyaWVzID0gNTtcclxuXHJcbiAgICAvKipcclxuICAgICAqIE1pbWUtdHlwZSBncm91cGluZyBiYXNlZCBvbiB0aGUgVmlld2VyQ29tcG9uZW50LlxyXG4gICAgICovXHJcbiAgICBwcml2YXRlIG1pbWVUeXBlcyA9IHtcclxuICAgICAgICB0ZXh0OiBbJ3RleHQvcGxhaW4nLCAndGV4dC9jc3YnLCAndGV4dC94bWwnLCAndGV4dC9odG1sJywgJ2FwcGxpY2F0aW9uL3gtamF2YXNjcmlwdCddLFxyXG4gICAgICAgIHBkZjogWydhcHBsaWNhdGlvbi9wZGYnXSxcclxuICAgICAgICBpbWFnZTogWydpbWFnZS9wbmcnLCAnaW1hZ2UvanBlZycsICdpbWFnZS9naWYnLCAnaW1hZ2UvYm1wJywgJ2ltYWdlL3N2Zyt4bWwnXSxcclxuICAgICAgICBtZWRpYTogWyd2aWRlby9tcDQnLCAndmlkZW8vd2VibScsICd2aWRlby9vZ2cnLCAnYXVkaW8vbXBlZycsICdhdWRpby9vZ2cnLCAnYXVkaW8vd2F2J11cclxuICAgIH07XHJcblxyXG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBhcGlTZXJ2aWNlOiBBbGZyZXNjb0FwaVNlcnZpY2UsXHJcbiAgICAgICAgICAgICAgICBwcml2YXRlIGxvZ1NlcnZpY2U6IExvZ1NlcnZpY2UpIHtcclxuICAgIH1cclxuXHJcbiAgICAvKipcclxuICAgICAqIFRoaXMgbWV0aG9kIHRha2VzIGEgdXJsIHRvIHRyaWdnZXIgdGhlIHByaW50IGRpYWxvZyBhZ2FpbnN0LCBhbmQgdGhlIHR5cGUgb2YgYXJ0aWZhY3QgdGhhdCBpdFxyXG4gICAgICogaXMuXHJcbiAgICAgKiBUaGlzIFVSTCBzaG91bGQgYmUgb25lIHRoYXQgY2FuIGJlIHJlbmRlcmVkIGluIHRoZSBicm93c2VyLCBmb3IgZXhhbXBsZSBQREYsIEltYWdlLCBvciBUZXh0XHJcbiAgICAgKi9cclxuICAgIHByaW50RmlsZSh1cmw6IHN0cmluZywgdHlwZTogc3RyaW5nKTogdm9pZCB7XHJcbiAgICAgICAgY29uc3QgcHdhID0gd2luZG93Lm9wZW4odXJsLCBWaWV3VXRpbFNlcnZpY2UuVEFSR0VUKTtcclxuICAgICAgICBpZiAocHdhKSB7XHJcbiAgICAgICAgICAgIC8vIEJlY2F1c2Ugb2YgdGhlIHdheSBjaHJvbWUgZm9jdXMgYW5kIGNsb3NlIGltYWdlIHdpbmRvdyB2cy4gcGRmIHByZXZpZXcgd2luZG93XHJcbiAgICAgICAgICAgIGlmICh0eXBlID09PSBWaWV3VXRpbFNlcnZpY2UuQ29udGVudEdyb3VwLklNQUdFKSB7XHJcbiAgICAgICAgICAgICAgICBwd2Eub25mb2N1cyA9ICgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KCgpID0+IHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHdhLmNsb3NlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgNTAwKTtcclxuICAgICAgICAgICAgICAgIH07XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHB3YS5vbmxvYWQgPSAoKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBwd2EucHJpbnQoKTtcclxuICAgICAgICAgICAgfTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgLyoqXHJcbiAgICAgKiBMYXVuY2ggdGhlIEZpbGUgUHJpbnQgZGlhbG9nIGZyb20gYW55d2hlcmUgb3RoZXIgdGhhbiB0aGUgcHJldmlldyBzZXJ2aWNlLCB3aGljaCByZXNvbHZlcyB0aGVcclxuICAgICAqIHJlbmRpdGlvbiBvZiB0aGUgb2JqZWN0IHRoYXQgY2FuIGJlIHByaW50ZWQgZnJvbSBhIHdlYiBicm93c2VyLlxyXG4gICAgICogVGhlc2UgYXJlOiBpbWFnZXMsIFBERiBmaWxlcywgb3IgUERGIHJlbmRpdGlvbiBvZiBmaWxlcy5cclxuICAgICAqIFdlIGFsc28gZm9yY2UgUERGIHJlbmRpdGlvbiBmb3IgVEVYVCB0eXBlIG9iamVjdHMsIG90aGVyd2lzZSB0aGUgZGVmYXVsdCBVUkwgaXMgdG8gZG93bmxvYWQuXHJcbiAgICAgKiBUT0RPIHRoZXJlIGFyZSBkaWZmZXJlbnQgVEVYVCB0eXBlIG9iamVjdHMsIChIVE1MLCBwbGFpbnRleHQsIHhtbCwgZXRjLiB3ZSBzaG91bGQgZGV0ZXJtaW5lIGhvdyB0aGVzZSBhcmUgaGFuZGxlZClcclxuICAgICAqL1xyXG4gICAgcHJpbnRGaWxlR2VuZXJpYyhvYmplY3RJZDogc3RyaW5nLCBtaW1lVHlwZTogc3RyaW5nKTogdm9pZCB7XHJcbiAgICAgICAgY29uc3Qgbm9kZUlkID0gb2JqZWN0SWQ7XHJcbiAgICAgICAgY29uc3QgdHlwZTogc3RyaW5nID0gdGhpcy5nZXRWaWV3ZXJUeXBlQnlNaW1lVHlwZShtaW1lVHlwZSk7XHJcblxyXG4gICAgICAgIHRoaXMuZ2V0UmVuZGl0aW9uKG5vZGVJZCwgVmlld1V0aWxTZXJ2aWNlLkNvbnRlbnRHcm91cC5QREYpXHJcbiAgICAgICAgICAgIC50aGVuKCh2YWx1ZSkgPT4ge1xyXG4gICAgICAgICAgICAgICAgY29uc3QgdXJsOiBzdHJpbmcgPSB0aGlzLmdldFJlbmRpdGlvblVybChub2RlSWQsIHR5cGUsICh2YWx1ZSA/IHRydWUgOiBmYWxzZSkpO1xyXG4gICAgICAgICAgICAgICAgY29uc3QgcHJpbnRUeXBlID0gKHR5cGUgPT09IFZpZXdVdGlsU2VydmljZS5Db250ZW50R3JvdXAuUERGXHJcbiAgICAgICAgICAgICAgICAgICAgfHwgdHlwZSA9PT0gVmlld1V0aWxTZXJ2aWNlLkNvbnRlbnRHcm91cC5URVhUKVxyXG4gICAgICAgICAgICAgICAgICAgID8gVmlld1V0aWxTZXJ2aWNlLkNvbnRlbnRHcm91cC5QREYgOiB0eXBlO1xyXG4gICAgICAgICAgICAgICAgdGhpcy5wcmludEZpbGUodXJsLCBwcmludFR5cGUpO1xyXG4gICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAuY2F0Y2goKGVycikgPT4ge1xyXG4gICAgICAgICAgICAgICAgdGhpcy5sb2dTZXJ2aWNlLmVycm9yKCdFcnJvciB3aXRoIFByaW50aW5nJyk7XHJcbiAgICAgICAgICAgICAgICB0aGlzLmxvZ1NlcnZpY2UuZXJyb3IoZXJyKTtcclxuICAgICAgICAgICAgfSk7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0UmVuZGl0aW9uVXJsKG5vZGVJZDogc3RyaW5nLCB0eXBlOiBzdHJpbmcsIHJlbmRpdGlvbkV4aXN0czogYm9vbGVhbik6IHN0cmluZyB7XHJcbiAgICAgICAgcmV0dXJuIChyZW5kaXRpb25FeGlzdHMgJiYgdHlwZSAhPT0gVmlld1V0aWxTZXJ2aWNlLkNvbnRlbnRHcm91cC5JTUFHRSkgP1xyXG4gICAgICAgICAgICB0aGlzLmFwaVNlcnZpY2UuY29udGVudEFwaS5nZXRSZW5kaXRpb25Vcmwobm9kZUlkLCBWaWV3VXRpbFNlcnZpY2UuQ29udGVudEdyb3VwLlBERikgOlxyXG4gICAgICAgICAgICB0aGlzLmFwaVNlcnZpY2UuY29udGVudEFwaS5nZXRDb250ZW50VXJsKG5vZGVJZCwgZmFsc2UpO1xyXG4gICAgfVxyXG5cclxuICAgIHByaXZhdGUgYXN5bmMgd2FpdFJlbmRpdGlvbihub2RlSWQ6IHN0cmluZywgcmVuZGl0aW9uSWQ6IHN0cmluZywgcmV0cmllczogbnVtYmVyKTogUHJvbWlzZTxSZW5kaXRpb25FbnRyeT4ge1xyXG4gICAgICAgIGNvbnN0IHJlbmRpdGlvbiA9IGF3YWl0IHRoaXMuYXBpU2VydmljZS5yZW5kaXRpb25zQXBpLmdldFJlbmRpdGlvbihub2RlSWQsIHJlbmRpdGlvbklkKTtcclxuXHJcbiAgICAgICAgaWYgKHRoaXMubWF4UmV0cmllcyA8IHJldHJpZXMpIHtcclxuICAgICAgICAgICAgY29uc3Qgc3RhdHVzID0gcmVuZGl0aW9uLmVudHJ5LnN0YXR1cy50b1N0cmluZygpO1xyXG5cclxuICAgICAgICAgICAgaWYgKHN0YXR1cyA9PT0gJ0NSRUFURUQnKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gcmVuZGl0aW9uO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0cmllcyArPSAxO1xyXG4gICAgICAgICAgICAgICAgYXdhaXQgdGhpcy53YWl0KDEwMDApO1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIGF3YWl0IHRoaXMud2FpdFJlbmRpdGlvbihub2RlSWQsIHJlbmRpdGlvbklkLCByZXRyaWVzKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBnZXRWaWV3ZXJUeXBlQnlNaW1lVHlwZShtaW1lVHlwZTogc3RyaW5nKTogc3RyaW5nIHtcclxuICAgICAgICBpZiAobWltZVR5cGUpIHtcclxuICAgICAgICAgICAgbWltZVR5cGUgPSBtaW1lVHlwZS50b0xvd2VyQ2FzZSgpO1xyXG5cclxuICAgICAgICAgICAgY29uc3QgZWRpdG9yVHlwZXMgPSBPYmplY3Qua2V5cyh0aGlzLm1pbWVUeXBlcyk7XHJcbiAgICAgICAgICAgIGZvciAoY29uc3QgdHlwZSBvZiBlZGl0b3JUeXBlcykge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMubWltZVR5cGVzW3R5cGVdLmluZGV4T2YobWltZVR5cGUpID49IDApIHtcclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHlwZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gJ3Vua25vd24nO1xyXG4gICAgfVxyXG5cclxuICAgIHdhaXQobXM6IG51bWJlcik6IFByb21pc2U8YW55PiB7XHJcbiAgICAgICAgcmV0dXJuIG5ldyBQcm9taXNlKChyZXNvbHZlKSA9PiBzZXRUaW1lb3V0KHJlc29sdmUsIG1zKSk7XHJcbiAgICB9XHJcblxyXG4gICAgYXN5bmMgZ2V0UmVuZGl0aW9uKG5vZGVJZDogc3RyaW5nLCByZW5kaXRpb25JZDogc3RyaW5nKTogUHJvbWlzZTxSZW5kaXRpb25FbnRyeT4ge1xyXG4gICAgICAgIGNvbnN0IHJlbmRpdGlvblBhZ2luZzogUmVuZGl0aW9uUGFnaW5nID0gYXdhaXQgdGhpcy5hcGlTZXJ2aWNlLnJlbmRpdGlvbnNBcGkuZ2V0UmVuZGl0aW9ucyhub2RlSWQpO1xyXG4gICAgICAgIGxldCByZW5kaXRpb246IFJlbmRpdGlvbkVudHJ5ID0gcmVuZGl0aW9uUGFnaW5nLmxpc3QuZW50cmllcy5maW5kKChyZW5kaXRpb25FbnRyeTogUmVuZGl0aW9uRW50cnkpID0+IHJlbmRpdGlvbkVudHJ5LmVudHJ5LmlkLnRvTG93ZXJDYXNlKCkgPT09IHJlbmRpdGlvbklkKTtcclxuXHJcbiAgICAgICAgaWYgKHJlbmRpdGlvbikge1xyXG4gICAgICAgICAgICBjb25zdCBzdGF0dXMgPSByZW5kaXRpb24uZW50cnkuc3RhdHVzLnRvU3RyaW5nKCk7XHJcblxyXG4gICAgICAgICAgICBpZiAoc3RhdHVzID09PSAnTk9UX0NSRUFURUQnKSB7XHJcbiAgICAgICAgICAgICAgICB0cnkge1xyXG4gICAgICAgICAgICAgICAgICAgIGF3YWl0IHRoaXMuYXBpU2VydmljZS5yZW5kaXRpb25zQXBpLmNyZWF0ZVJlbmRpdGlvbihub2RlSWQsIHsgaWQ6IHJlbmRpdGlvbklkIH0pO1xyXG4gICAgICAgICAgICAgICAgICAgIHJlbmRpdGlvbiA9IGF3YWl0IHRoaXMud2FpdFJlbmRpdGlvbihub2RlSWQsIHJlbmRpdGlvbklkLCAwKTtcclxuICAgICAgICAgICAgICAgIH0gY2F0Y2ggKGVycikge1xyXG4gICAgICAgICAgICAgICAgICAgIHRoaXMubG9nU2VydmljZS5lcnJvcihlcnIpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgICAgIHJldHVybiBuZXcgUHJvbWlzZTxSZW5kaXRpb25FbnRyeT4oKHJlc29sdmUpID0+IHJlc29sdmUocmVuZGl0aW9uKSk7XHJcbiAgICB9XHJcblxyXG59XHJcbiJdfQ==