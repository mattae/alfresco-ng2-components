/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FormBaseModel } from './form-base.model';
import { FormOutcomeModel, FormFieldValidator, FormFieldModel, FormOutcomeEvent } from './widgets';
import { EventEmitter } from '@angular/core';
export declare abstract class FormBaseComponent {
    static SAVE_OUTCOME_ID: string;
    static COMPLETE_OUTCOME_ID: string;
    static START_PROCESS_OUTCOME_ID: string;
    static CUSTOM_OUTCOME_ID: string;
    static COMPLETE_BUTTON_COLOR: string;
    static COMPLETE_OUTCOME_NAME: string;
    /** Path of the folder where the metadata will be stored. */
    path: string;
    /** Name to assign to the new node where the metadata are stored. */
    nameNode: string;
    /** Toggle rendering of the form title. */
    showTitle: boolean;
    /** Toggle rendering of the `Complete` outcome button. */
    showCompleteButton: boolean;
    /** If true then the `Complete` outcome button is shown but it will be disabled. */
    disableCompleteButton: boolean;
    /** If true then the `Save` outcome button is shown but will be disabled. */
    disableSaveButton: boolean;
    /** If true then the `Start Process` outcome button is shown but it will be disabled. */
    disableStartProcessButton: boolean;
    /** Toggle rendering of the `Save` outcome button. */
    showSaveButton: boolean;
    /** Toggle readonly state of the form. Forces all form widgets to render as readonly if enabled. */
    readOnly: boolean;
    /** Toggle rendering of the `Refresh` button. */
    showRefreshButton: boolean;
    /** Toggle rendering of the validation icon next to the form title. */
    showValidationIcon: boolean;
    /** Contains a list of form field validator instances. */
    fieldValidators: FormFieldValidator[];
    /** Emitted when the supplied form values have a validation error. */
    formError: EventEmitter<FormFieldModel[]>;
    /** Emitted when any outcome is executed. Default behaviour can be prevented
     * via `event.preventDefault()`.
     */
    executeOutcome: EventEmitter<FormOutcomeEvent>;
    /**
     * Emitted when any error occurs.
     */
    error: EventEmitter<any>;
    form: FormBaseModel;
    getParsedFormDefinition(): FormBaseComponent;
    hasForm(): boolean;
    isTitleEnabled(): boolean;
    getColorForOutcome(outcomeName: string): string;
    isOutcomeButtonEnabled(outcome: FormOutcomeModel): boolean;
    isOutcomeButtonVisible(outcome: FormOutcomeModel, isFormReadOnly: boolean): boolean;
    /**
     * Invoked when user clicks outcome button.
     * @param outcome Form outcome model
     */
    onOutcomeClicked(outcome: FormOutcomeModel): boolean;
    handleError(err: any): any;
    abstract onRefreshClicked(): any;
    abstract saveTaskForm(): any;
    abstract completeTaskForm(outcome?: string): any;
    protected abstract onTaskSaved(form: FormBaseModel): any;
    protected abstract storeFormAsMetadata(): any;
    protected abstract onExecuteOutcome(outcome: FormOutcomeModel): any;
}
