/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FormValues } from './widgets/core/form-values';
import { TabModel } from './widgets/core/tab.model';
import { FormWidgetModel } from './widgets/core/form-widget.model';
import { FormOutcomeModel } from './widgets/core/form-outcome.model';
import { FormFieldModel } from './widgets/core/form-field.model';
export declare abstract class FormBaseModel {
    static UNSET_TASK_NAME: string;
    static SAVE_OUTCOME: string;
    static COMPLETE_OUTCOME: string;
    static START_PROCESS_OUTCOME: string;
    json: any;
    values: FormValues;
    tabs: TabModel[];
    fields: FormWidgetModel[];
    outcomes: FormOutcomeModel[];
    className: string;
    readOnly: boolean;
    taskName: any;
    isValid: boolean;
    hasTabs(): boolean;
    hasFields(): boolean;
    hasOutcomes(): boolean;
    getFieldById(fieldId: string): FormFieldModel;
    getFormFields(): FormFieldModel[];
    markAsInvalid(): void;
    abstract validateForm(): any;
    abstract validateField(field: FormFieldModel): any;
    abstract onFormFieldChanged(field: FormFieldModel): any;
}
