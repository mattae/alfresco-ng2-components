/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Compiler, ComponentFactoryResolver, ComponentRef, OnDestroy, OnInit, ViewContainerRef } from '@angular/core';
import { FormRenderingService } from './../../services/form-rendering.service';
import { WidgetVisibilityService } from './../../services/widget-visibility.service';
import { FormFieldModel } from './../widgets/core/form-field.model';
export declare class FormFieldComponent implements OnInit, OnDestroy {
    private formRenderingService;
    private componentFactoryResolver;
    private visibilityService;
    private compiler;
    container: ViewContainerRef;
    /** Contains all the necessary data needed to determine what UI Widget
     * to use when rendering the field in the form. You would typically not
     * create this data manually but instead create the form in APS and export
     * it to get to all the `FormFieldModel` definitions.
     */
    field: FormFieldModel;
    componentRef: ComponentRef<{}>;
    focus: boolean;
    constructor(formRenderingService: FormRenderingService, componentFactoryResolver: ComponentFactoryResolver, visibilityService: WidgetVisibilityService, compiler: Compiler);
    ngOnInit(): void;
    ngOnDestroy(): void;
    private getField;
    private hasController;
    private getComponentFactorySync;
    private createComponentFactorySync;
    focusToggle(): void;
}
