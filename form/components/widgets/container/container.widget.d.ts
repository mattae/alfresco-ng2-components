/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AfterViewInit, OnInit } from '@angular/core';
import { FormService } from './../../../services/form.service';
import { FormFieldModel } from './../core/form-field.model';
import { WidgetComponent } from './../widget.component';
import { ContainerWidgetComponentModel } from './container.widget.model';
export declare class ContainerWidgetComponent extends WidgetComponent implements OnInit, AfterViewInit {
    formService: FormService;
    content: ContainerWidgetComponentModel;
    constructor(formService: FormService);
    onExpanderClicked(): void;
    ngOnInit(): void;
    /**
     * Serializes column fields
     */
    readonly fields: FormFieldModel[];
    /**
     * Calculate the column width based on the numberOfColumns and current field's colspan property
     *
     * @param field
     */
    getColumnWith(field: FormFieldModel): string;
}
