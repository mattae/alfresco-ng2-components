/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ContentService } from '../../../../services/content.service';
import { LogService } from '../../../../services/log.service';
import { EventEmitter, OnChanges, SimpleChanges } from '@angular/core';
import { ProcessContentService } from '../../../services/process-content.service';
import { ContentLinkModel } from '../core/content-link.model';
import { FormService } from './../../../services/form.service';
export declare class ContentWidgetComponent implements OnChanges {
    protected formService: FormService;
    private logService;
    private contentService;
    private processContentService;
    /** The content id to show. */
    id: string;
    /** Toggles showing document content. */
    showDocumentContent: boolean;
    /** Emitted when the content is clicked. */
    contentClick: EventEmitter<{}>;
    /** Emitted when the thumbnail has loaded. */
    thumbnailLoaded: EventEmitter<any>;
    /** Emitted when the content has loaded. */
    contentLoaded: EventEmitter<any>;
    /** Emitted when an error occurs. */
    error: EventEmitter<any>;
    content: ContentLinkModel;
    constructor(formService: FormService, logService: LogService, contentService: ContentService, processContentService: ProcessContentService);
    ngOnChanges(changes: SimpleChanges): void;
    loadContent(id: number): void;
    loadThumbnailUrl(content: ContentLinkModel): void;
    openViewer(content: ContentLinkModel): void;
    /**
     * Invoke content download.
     */
    download(content: ContentLinkModel): void;
}
