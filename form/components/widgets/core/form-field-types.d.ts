/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export declare class FormFieldTypes {
    static CONTAINER: string;
    static GROUP: string;
    static DYNAMIC_TABLE: string;
    static TEXT: string;
    static MULTILINE_TEXT: string;
    static DROPDOWN: string;
    static HYPERLINK: string;
    static RADIO_BUTTONS: string;
    static DISPLAY_VALUE: string;
    static READONLY_TEXT: string;
    static UPLOAD: string;
    static TYPEAHEAD: string;
    static FUNCTIONAL_GROUP: string;
    static PEOPLE: string;
    static BOOLEAN: string;
    static NUMBER: string;
    static DATE: string;
    static AMOUNT: string;
    static DOCUMENT: string;
    static DATETIME: string;
    static ATTACH_FOLDER: string;
    static READONLY_TYPES: string[];
    static isReadOnlyType(type: string): boolean;
    static isContainerType(type: string): boolean;
}
