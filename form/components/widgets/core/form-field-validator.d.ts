/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FormFieldModel } from './form-field.model';
export interface FormFieldValidator {
    isSupported(field: FormFieldModel): boolean;
    validate(field: FormFieldModel): boolean;
}
export declare class RequiredFieldValidator implements FormFieldValidator {
    private supportedTypes;
    isSupported(field: FormFieldModel): boolean;
    validate(field: FormFieldModel): boolean;
}
export declare class NumberFieldValidator implements FormFieldValidator {
    private supportedTypes;
    static isNumber(value: any): boolean;
    isSupported(field: FormFieldModel): boolean;
    validate(field: FormFieldModel): boolean;
}
export declare class DateFieldValidator implements FormFieldValidator {
    private supportedTypes;
    static isValidDate(inputDate: string, dateFormat?: string): boolean;
    isSupported(field: FormFieldModel): boolean;
    validate(field: FormFieldModel): boolean;
}
export declare class MinDateFieldValidator implements FormFieldValidator {
    private supportedTypes;
    isSupported(field: FormFieldModel): boolean;
    validate(field: FormFieldModel): boolean;
    private checkDate;
}
export declare class MaxDateFieldValidator implements FormFieldValidator {
    MAX_DATE_FORMAT: string;
    private supportedTypes;
    isSupported(field: FormFieldModel): boolean;
    validate(field: FormFieldModel): boolean;
}
export declare class MinDateTimeFieldValidator implements FormFieldValidator {
    private supportedTypes;
    MIN_DATETIME_FORMAT: string;
    isSupported(field: FormFieldModel): boolean;
    validate(field: FormFieldModel): boolean;
    private checkDateTime;
}
export declare class MaxDateTimeFieldValidator implements FormFieldValidator {
    private supportedTypes;
    MAX_DATETIME_FORMAT: string;
    isSupported(field: FormFieldModel): boolean;
    validate(field: FormFieldModel): boolean;
    private checkDateTime;
}
export declare class MinLengthFieldValidator implements FormFieldValidator {
    private supportedTypes;
    isSupported(field: FormFieldModel): boolean;
    validate(field: FormFieldModel): boolean;
}
export declare class MaxLengthFieldValidator implements FormFieldValidator {
    private supportedTypes;
    isSupported(field: FormFieldModel): boolean;
    validate(field: FormFieldModel): boolean;
}
export declare class MinValueFieldValidator implements FormFieldValidator {
    private supportedTypes;
    isSupported(field: FormFieldModel): boolean;
    validate(field: FormFieldModel): boolean;
}
export declare class MaxValueFieldValidator implements FormFieldValidator {
    private supportedTypes;
    isSupported(field: FormFieldModel): boolean;
    validate(field: FormFieldModel): boolean;
}
export declare class RegExFieldValidator implements FormFieldValidator {
    private supportedTypes;
    isSupported(field: FormFieldModel): boolean;
    validate(field: FormFieldModel): boolean;
}
export declare class FixedValueFieldValidator implements FormFieldValidator {
    private supportedTypes;
    isSupported(field: FormFieldModel): boolean;
    hasValidNameOrValidId(field: FormFieldModel): boolean;
    hasValidName(field: FormFieldModel): boolean;
    hasValidId(field: FormFieldModel): boolean;
    hasStringValue(field: FormFieldModel): boolean;
    hasOptions(field: FormFieldModel): boolean;
    validate(field: FormFieldModel): boolean;
}
export declare const FORM_FIELD_VALIDATORS: (RequiredFieldValidator | NumberFieldValidator | DateFieldValidator | MinDateFieldValidator | MaxDateFieldValidator | MinDateTimeFieldValidator | MaxDateTimeFieldValidator | MinLengthFieldValidator | MaxLengthFieldValidator | MinValueFieldValidator | MaxValueFieldValidator | RegExFieldValidator | FixedValueFieldValidator)[];
