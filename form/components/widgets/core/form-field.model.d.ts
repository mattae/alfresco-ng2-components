/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { WidgetVisibilityModel } from '../../../models/widget-visibility.model';
import { ContainerColumnModel } from './container-column.model';
import { ErrorMessageModel } from './error-message.model';
import { FormFieldMetadata } from './form-field-metadata';
import { FormFieldOption } from './form-field-option';
import { FormWidgetModel } from './form-widget.model';
import { FormModel } from './form.model';
export declare class FormFieldModel extends FormWidgetModel {
    private _value;
    private _readOnly;
    private _isValid;
    private _required;
    readonly defaultDateFormat: string;
    readonly defaultDateTimeFormat: string;
    fieldType: string;
    id: string;
    name: string;
    type: string;
    overrideId: boolean;
    tab: string;
    rowspan: number;
    colspan: number;
    placeholder: string;
    minLength: number;
    maxLength: number;
    minValue: string;
    maxValue: string;
    regexPattern: string;
    options: FormFieldOption[];
    restUrl: string;
    restResponsePath: string;
    restIdProperty: string;
    restLabelProperty: string;
    hasEmptyValue: boolean;
    className: string;
    optionType: string;
    params: FormFieldMetadata;
    hyperlinkUrl: string;
    displayText: string;
    isVisible: boolean;
    visibilityCondition: WidgetVisibilityModel;
    enableFractions: boolean;
    currency: string;
    dateDisplayFormat: string;
    numberOfColumns: number;
    fields: FormFieldModel[];
    columns: ContainerColumnModel[];
    emptyOption: FormFieldOption;
    validationSummary: ErrorMessageModel;
    value: any;
    readOnly: boolean;
    required: boolean;
    readonly isValid: boolean;
    markAsInvalid(): void;
    validate(): boolean;
    constructor(form: FormModel, json?: any);
    private getDefaultDateFormat;
    private isTypeaheadFieldType;
    private getFieldNameWithLabel;
    private getProcessVariableValue;
    private getVariablesValue;
    private findProcessVariableValue;
    private containerFactory;
    parseValue(json: any): any;
    updateForm(): void;
    /**
     * Skip the invalid field type
     * @param type
     */
    isInvalidFieldType(type: string): boolean;
    getOptionName(): string;
    hasOptions(): boolean;
    private isDateField;
    private isDateTimeField;
}
