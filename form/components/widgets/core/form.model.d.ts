/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FormService } from './../../../services/form.service';
import { FormFieldTemplates } from './form-field-templates';
import { FormFieldModel } from './form-field.model';
import { FormValues } from './form-values';
import { FormFieldValidator } from './form-field-validator';
import { FormBaseModel } from '../../form-base.model';
export declare class FormModel extends FormBaseModel {
    protected formService?: FormService;
    readonly id: number;
    readonly name: string;
    readonly taskId: string;
    readonly taskName: string;
    processDefinitionId: string;
    customFieldTemplates: FormFieldTemplates;
    fieldValidators: FormFieldValidator[];
    readonly selectedOutcome: string;
    processVariables: any;
    constructor(formRepresentationJSON?: any, formValues?: FormValues, readOnly?: boolean, formService?: FormService);
    onFormFieldChanged(field: FormFieldModel): void;
    /**
     * Validates entire form and all form fields.
     *
     * @memberof FormModel
     */
    validateForm(): void;
    /**
     * Validates a specific form field, triggers form validation.
     *
     * @param field Form field to validate.
     * @memberof FormModel
     */
    validateField(field: FormFieldModel): void;
    private parseRootFields;
    private loadData;
}
