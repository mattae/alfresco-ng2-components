/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { LogService } from '../../../../services/log.service';
import { OnInit } from '@angular/core';
import { FormService } from '../../../services/form.service';
import { FormFieldOption } from './../core/form-field-option';
import { WidgetComponent } from './../widget.component';
export declare class DropdownWidgetComponent extends WidgetComponent implements OnInit {
    formService: FormService;
    private logService;
    constructor(formService: FormService, logService: LogService);
    ngOnInit(): void;
    getValuesByTaskId(): void;
    getValuesByProcessDefinitionId(): void;
    getOptionValue(option: FormFieldOption, fieldValue: string): string;
    handleError(error: any): void;
    isReadOnlyType(): boolean;
}
