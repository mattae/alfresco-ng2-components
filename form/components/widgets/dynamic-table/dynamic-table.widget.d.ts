/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { LogService } from '../../../../services/log.service';
import { ChangeDetectorRef, ElementRef, OnInit } from '@angular/core';
import { WidgetVisibilityService } from '../../../services/widget-visibility.service';
import { FormService } from './../../../services/form.service';
import { WidgetComponent } from './../widget.component';
import { DynamicTableColumn } from './dynamic-table-column.model';
import { DynamicTableRow } from './dynamic-table-row.model';
import { DynamicTableModel } from './dynamic-table.widget.model';
export declare class DynamicTableWidgetComponent extends WidgetComponent implements OnInit {
    formService: FormService;
    elementRef: ElementRef;
    private visibilityService;
    private logService;
    private cd;
    ERROR_MODEL_NOT_FOUND: string;
    content: DynamicTableModel;
    editMode: boolean;
    editRow: DynamicTableRow;
    private selectArrayCode;
    constructor(formService: FormService, elementRef: ElementRef, visibilityService: WidgetVisibilityService, logService: LogService, cd: ChangeDetectorRef);
    ngOnInit(): void;
    forceFocusOnAddButton(): void;
    private isDynamicTableReady;
    isValid(): boolean;
    onRowClicked(row: DynamicTableRow): void;
    onKeyPressed($event: KeyboardEvent, row: DynamicTableRow): void;
    private isEnterOrSpacePressed;
    hasSelection(): boolean;
    moveSelectionUp(): boolean;
    moveSelectionDown(): boolean;
    deleteSelection(): boolean;
    addNewRow(): boolean;
    editSelection(): boolean;
    getCellValue(row: DynamicTableRow, column: DynamicTableColumn): any;
    onSaveChanges(): void;
    onCancelChanges(): void;
    copyRow(row: DynamicTableRow): DynamicTableRow;
    private copyObject;
}
