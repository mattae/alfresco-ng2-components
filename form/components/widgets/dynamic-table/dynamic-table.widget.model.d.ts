/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FormService } from './../../../services/form.service';
import { FormFieldModel } from './../core/form-field.model';
import { FormWidgetModel } from './../core/form-widget.model';
import { DynamicRowValidationSummary } from './dynamic-row-validation-summary.model';
import { DynamicTableColumn } from './dynamic-table-column.model';
import { DynamicTableRow } from './dynamic-table-row.model';
export declare class DynamicTableModel extends FormWidgetModel {
    private formService;
    field: FormFieldModel;
    columns: DynamicTableColumn[];
    visibleColumns: DynamicTableColumn[];
    rows: DynamicTableRow[];
    private _selectedRow;
    private _validators;
    selectedRow: DynamicTableRow;
    constructor(field: FormFieldModel, formService: FormService);
    private getColumns;
    flushValue(): void;
    moveRow(row: DynamicTableRow, offset: number): void;
    deleteRow(row: DynamicTableRow): void;
    addRow(row: DynamicTableRow): void;
    validateRow(row: DynamicTableRow): DynamicRowValidationSummary;
    getCellValue(row: DynamicTableRow, column: DynamicTableColumn): any;
    getDisplayText(column: DynamicTableColumn): string;
}
