/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { UserPreferencesService } from '../../../../../../services/user-preferences.service';
import { OnInit, OnDestroy } from '@angular/core';
import { DateAdapter, MatDatepickerInputEvent } from '@angular/material';
import { Moment } from 'moment';
import { DynamicTableColumn } from './../../dynamic-table-column.model';
import { DynamicTableRow } from './../../dynamic-table-row.model';
import { DynamicTableModel } from './../../dynamic-table.widget.model';
export declare class DateEditorComponent implements OnInit, OnDestroy {
    private dateAdapter;
    private userPreferencesService;
    DATE_FORMAT: string;
    value: any;
    table: DynamicTableModel;
    row: DynamicTableRow;
    column: DynamicTableColumn;
    minDate: Moment;
    maxDate: Moment;
    private onDestroy$;
    constructor(dateAdapter: DateAdapter<Moment>, userPreferencesService: UserPreferencesService);
    ngOnInit(): void;
    ngOnDestroy(): void;
    onDateChanged(newDateValue: MatDatepickerInputEvent<any> | HTMLInputElement): void;
}
