/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ElementRef, OnInit } from '@angular/core';
import { FormService } from '../../../services/form.service';
import { GroupModel } from './../core/group.model';
import { WidgetComponent } from './../widget.component';
export declare class FunctionalGroupWidgetComponent extends WidgetComponent implements OnInit {
    formService: FormService;
    elementRef: ElementRef;
    value: string;
    oldValue: string;
    groups: GroupModel[];
    minTermLength: number;
    groupId: string;
    constructor(formService: FormService, elementRef: ElementRef);
    ngOnInit(): void;
    onKeyUp(event: KeyboardEvent): void;
    flushValue(): void;
    onItemClick(item: GroupModel, event: Event): void;
    onItemSelect(item: GroupModel): void;
}
