/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { PeopleProcessService } from '../../../../services/people-process.service';
import { UserProcessModel } from '../../../../models';
import { ElementRef, EventEmitter, OnInit } from '@angular/core';
import { FormService } from '../../../services/form.service';
import { WidgetComponent } from './../widget.component';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
export declare class PeopleWidgetComponent extends WidgetComponent implements OnInit {
    formService: FormService;
    peopleProcessService: PeopleProcessService;
    input: ElementRef;
    peopleSelected: EventEmitter<number>;
    groupId: string;
    value: any;
    searchTerm: FormControl;
    errorMsg: string;
    searchTerms$: Observable<any>;
    users$: Observable<UserProcessModel[]>;
    constructor(formService: FormService, peopleProcessService: PeopleProcessService);
    ngOnInit(): void;
    checkUserAndValidateForm(list: any, value: any): void;
    isValidUser(users: UserProcessModel[], name: string): UserProcessModel;
    getDisplayName(model: UserProcessModel): string;
    onItemSelect(item: UserProcessModel): void;
}
