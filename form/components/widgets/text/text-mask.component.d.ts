/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { ElementRef, OnChanges, Renderer2, SimpleChanges } from '@angular/core';
import { ControlValueAccessor } from '@angular/forms';
export declare const CUSTOM_INPUT_CONTROL_VALUE_ACCESSOR: any;
/**
 * Directive selectors without adf- prefix will be deprecated on 3.0.0
 */
export declare class InputMaskDirective implements OnChanges, ControlValueAccessor {
    private el;
    private render;
    /** Object defining mask and "reversed" status. */
    inputMask: {
        mask: '';
        isReversed: false;
    };
    private translationMask;
    private byPassKeys;
    private value;
    private invalidCharacters;
    constructor(el: ElementRef, render: Renderer2);
    _onChange: (_: any) => void;
    _onTouched: () => void;
    onTextInput(event: KeyboardEvent): void;
    ngOnChanges(changes: SimpleChanges): void;
    writeValue(value: any): void;
    registerOnChange(fn: any): void;
    registerOnTouched(fn: () => any): void;
    private maskValue;
    private setCaretPosition;
    calculateCaretPosition(caretPosition: any, newValue: any, keyCode: any): any;
    getMasked(skipMaskChars: any, val: any, mask: any, isReversed?: boolean): string;
    private isToCheck;
    private setValue;
    private getValue;
}
