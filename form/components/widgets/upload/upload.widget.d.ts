/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { LogService } from '../../../../services/log.service';
import { ThumbnailService } from '../../../../services/thumbnail.service';
import { ElementRef, OnInit } from '@angular/core';
import { FormService } from '../../../services/form.service';
import { ProcessContentService } from '../../../services/process-content.service';
import { WidgetComponent } from './../widget.component';
export declare class UploadWidgetComponent extends WidgetComponent implements OnInit {
    formService: FormService;
    private logService;
    private thumbnailService;
    processContentService: ProcessContentService;
    hasFile: boolean;
    displayText: string;
    multipleOption: string;
    mimeTypeIcon: string;
    fileInput: ElementRef;
    constructor(formService: FormService, logService: LogService, thumbnailService: ThumbnailService, processContentService: ProcessContentService);
    ngOnInit(): void;
    removeFile(file: any): void;
    onFileChanged(event: any): void;
    private uploadRawContent;
    getMultipleFileParam(): void;
    private removeElementFromList;
    private resetFormValueWithNoFiles;
    getIcon(mimeType: any): string;
    fileClicked(contentLinkModel: any): void;
}
