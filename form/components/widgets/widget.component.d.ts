/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AfterViewInit, EventEmitter } from '@angular/core';
import { FormService } from './../../services/form.service';
import { FormFieldModel } from './core/index';
export declare const baseHost: {
    '(click)': string;
    '(blur)': string;
    '(change)': string;
    '(focus)': string;
    '(focusin)': string;
    '(focusout)': string;
    '(input)': string;
    '(invalid)': string;
    '(select)': string;
};
/**
 * Base widget component.
 */
export declare class WidgetComponent implements AfterViewInit {
    formService?: FormService;
    static DEFAULT_HYPERLINK_URL: string;
    static DEFAULT_HYPERLINK_SCHEME: string;
    /** Does the widget show a read-only value? (ie, can't be edited) */
    readOnly: boolean;
    /** Data to be displayed in the field */
    field: FormFieldModel;
    /**
     * Emitted when a field value changes.
     */
    fieldChanged: EventEmitter<FormFieldModel>;
    constructor(formService?: FormService);
    hasField(): boolean;
    isRequired(): any;
    isValid(): boolean;
    hasValue(): boolean;
    isInvalidFieldRequired(): any;
    ngAfterViewInit(): void;
    checkVisibility(field: FormFieldModel): void;
    onFieldChanged(field: FormFieldModel): void;
    protected getHyperlinkUrl(field: FormFieldModel): string;
    protected getHyperlinkText(field: FormFieldModel): string;
    event(event: Event): void;
}
