/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export declare class WidgetVisibilityModel {
    private json?;
    rightRestResponseId?: string;
    rightFormFieldId?: string;
    leftRestResponseId?: string;
    leftFormFieldId?: string;
    operator: string;
    nextCondition: WidgetVisibilityModel;
    nextConditionOperator: string;
    constructor(json?: any);
    leftType: string;
    rightType: string;
    leftValue: string;
    rightValue: string;
}
export declare enum WidgetTypeEnum {
    field = "field",
    variable = "variable",
    value = "value"
}
