/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import { MinimalNode } from '@alfresco/js-api';
import { Observable } from 'rxjs';
import { ExternalContent } from '../components/widgets/core/external-content';
import { ExternalContentLink } from '../components/widgets/core/external-content-link';
export declare class ActivitiContentService {
    private apiService;
    private logService;
    static UNKNOWN_ERROR_MESSAGE: string;
    static GENERIC_ERROR_MESSAGE: string;
    constructor(apiService: AlfrescoApiService, logService: LogService);
    /**
     * Returns a list of child nodes below the specified folder
     *
     * @param accountId
     * @param folderId
     */
    getAlfrescoNodes(accountId: string, folderId: string): Observable<[ExternalContent]>;
    /**
     * Returns a list of all the repositories configured
     *
     * @param accountId
     * @param folderId
     */
    getAlfrescoRepositories(tenantId: number, includeAccount: boolean): Observable<any>;
    /**
     * Returns a list of child nodes below the specified folder
     *
     * @param accountId
     * @param node
     * @param siteId
     */
    linkAlfrescoNode(accountId: string, node: ExternalContent, siteId: string): Observable<ExternalContentLink>;
    applyAlfrescoNode(node: MinimalNode, siteId: string, accountId: string): Observable<any>;
    private getSiteNameFromNodePath;
    toJson(res: any): any;
    toJsonArray(res: any): any;
    handleError(error: any): Observable<any>;
}
