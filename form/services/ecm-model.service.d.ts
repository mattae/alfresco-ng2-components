/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { LogService } from '../../services/log.service';
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { Observable } from 'rxjs';
import { FormModel } from '../components/widgets/core/form.model';
export declare class EcmModelService {
    private apiService;
    private logService;
    static MODEL_NAMESPACE: string;
    static MODEL_NAME: string;
    static TYPE_MODEL: string;
    constructor(apiService: AlfrescoApiService, logService: LogService);
    createEcmTypeForActivitiForm(formName: string, form: FormModel): Observable<any>;
    searchActivitiEcmModel(): Observable<any>;
    createActivitiEcmModel(formName: string, form: FormModel): Observable<any>;
    saveFomType(formName: string, form: FormModel): Observable<any>;
    createEcmTypeWithProperties(formName: string, form: FormModel): Observable<any>;
    searchEcmType(typeName: string, modelName: string): Observable<any>;
    activeEcmModel(modelName: string): Observable<any>;
    createEcmModel(modelName: string, nameSpace: string): Observable<any>;
    getEcmModels(): Observable<any>;
    getEcmType(modelName: string): Observable<any>;
    createEcmType(typeName: string, modelName: string, parentType: string): Observable<any>;
    addPropertyToAType(modelName: string, typeName: string, formFields: any): Observable<any>;
    cleanNameType(name: string): string;
    toJson(res: any): any;
    handleError(err: any): any;
}
