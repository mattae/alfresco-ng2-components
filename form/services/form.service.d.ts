/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import { UserProcessModel } from '../../models';
import { Observable, Subject } from 'rxjs';
import { FormDefinitionModel } from '../models/form-definition.model';
import { ContentLinkModel } from './../components/widgets/core/content-link.model';
import { GroupModel } from './../components/widgets/core/group.model';
import { FormModel, FormOutcomeEvent, FormValues } from './../components/widgets/core/index';
import { FormErrorEvent, FormEvent, FormFieldEvent, ValidateDynamicTableRowEvent, ValidateFormEvent, ValidateFormFieldEvent } from './../events/index';
import { EcmModelService } from './ecm-model.service';
export declare class FormService {
    private ecmModelService;
    private apiService;
    protected logService: LogService;
    static UNKNOWN_ERROR_MESSAGE: string;
    static GENERIC_ERROR_MESSAGE: string;
    formLoaded: Subject<FormEvent>;
    formDataRefreshed: Subject<FormEvent>;
    formFieldValueChanged: Subject<FormFieldEvent>;
    formEvents: Subject<Event>;
    taskCompleted: Subject<FormEvent>;
    taskCompletedError: Subject<FormErrorEvent>;
    taskSaved: Subject<FormEvent>;
    taskSavedError: Subject<FormErrorEvent>;
    formContentClicked: Subject<ContentLinkModel>;
    validateForm: Subject<ValidateFormEvent>;
    validateFormField: Subject<ValidateFormFieldEvent>;
    validateDynamicTableRow: Subject<ValidateDynamicTableRowEvent>;
    executeOutcome: Subject<FormOutcomeEvent>;
    constructor(ecmModelService: EcmModelService, apiService: AlfrescoApiService, logService: LogService);
    private readonly taskApi;
    private readonly modelsApi;
    private readonly editorApi;
    private readonly processApi;
    private readonly processInstanceVariablesApi;
    private readonly usersWorkflowApi;
    private readonly groupsApi;
    /**
     * Parses JSON data to create a corresponding Form model.
     * @param json JSON to create the form
     * @param data Values for the form fields
     * @param readOnly Should the form fields be read-only?
     * @returns Form model created from input data
     */
    parseForm(json: any, data?: FormValues, readOnly?: boolean): FormModel;
    /**
     * Creates a Form with a field for each metadata property.
     * @param formName Name of the new form
     * @returns The new form
     */
    createFormFromANode(formName: string): Observable<any>;
    /**
     * Create a Form.
     * @param formName Name of the new form
     * @returns The new form
     */
    createForm(formName: string): Observable<any>;
    /**
     * Saves a form.
     * @param formId ID of the form to save
     * @param formModel Model data for the form
     * @returns Data for the saved form
     */
    saveForm(formId: number, formModel: FormDefinitionModel): Observable<any>;
    /**
     * Searches for a form by name.
     * @param name The form name to search for
     * @returns Form model(s) matching the search name
     */
    searchFrom(name: string): Observable<any>;
    /**
     * Gets all the forms.
     * @returns List of form models
     */
    getForms(): Observable<any>;
    /**
     * Gets process definitions.
     * @returns List of process definitions
     */
    getProcessDefinitions(): Observable<any>;
    /**
     * Gets instance variables for a process.
     * @param processInstanceId ID of the target process
     * @returns List of instance variable information
     */
    getProcessVariablesById(processInstanceId: string): Observable<any[]>;
    /**
     * Gets all the tasks.
     * @returns List of tasks
     */
    getTasks(): Observable<any>;
    /**
     * Gets a task.
     * @param taskId Task Id
     * @returns Task info
     */
    getTask(taskId: string): Observable<any>;
    /**
     * Saves a task form.
     * @param taskId Task Id
     * @param formValues Form Values
     * @returns Null response when the operation is complete
     */
    saveTaskForm(taskId: string, formValues: FormValues): Observable<any>;
    /**
     * Completes a Task Form.
     * @param taskId Task Id
     * @param formValues Form Values
     * @param outcome Form Outcome
     * @returns Null response when the operation is complete
     */
    completeTaskForm(taskId: string, formValues: FormValues, outcome?: string): Observable<any>;
    /**
     * Gets a form related to a task.
     * @param taskId ID of the target task
     * @returns Form definition
     */
    getTaskForm(taskId: string): Observable<any>;
    /**
     * Gets a form definition.
     * @param formId ID of the target form
     * @returns Form definition
     */
    getFormDefinitionById(formId: number): Observable<any>;
    /**
     * Gets the form definition with a given name.
     * @param name The form name
     * @returns Form definition
     */
    getFormDefinitionByName(name: string): Observable<any>;
    /**
     * Gets the start form instance for a given process.
     * @param processId Process definition ID
     * @returns Form definition
     */
    getStartFormInstance(processId: string): Observable<any>;
    /**
     * Gets a process instance.
     * @param processId ID of the process to get
     * @returns Process instance
     */
    getProcessInstance(processId: string): Observable<any>;
    /**
     * Gets the start form definition for a given process.
     * @param processId Process definition ID
     * @returns Form definition
     */
    getStartFormDefinition(processId: string): Observable<any>;
    /**
     * Gets values of fields populated by a REST backend.
     * @param taskId Task identifier
     * @param field Field identifier
     * @returns Field values
     */
    getRestFieldValues(taskId: string, field: string): Observable<any>;
    /**
     * Gets values of fields populated by a REST backend using a process ID.
     * @param processDefinitionId Process identifier
     * @param field Field identifier
     * @returns Field values
     */
    getRestFieldValuesByProcessId(processDefinitionId: string, field: string): Observable<any>;
    /**
     * Gets column values of fields populated by a REST backend using a process ID.
     * @param processDefinitionId Process identifier
     * @param field Field identifier
     * @param column Column identifier
     * @returns Field values
     */
    getRestFieldValuesColumnByProcessId(processDefinitionId: string, field: string, column?: string): Observable<any>;
    /**
     * Gets column values of fields populated by a REST backend.
     * @param taskId Task identifier
     * @param field Field identifier
     * @param column Column identifier
     * @returns Field values
     */
    getRestFieldValuesColumn(taskId: string, field: string, column?: string): Observable<any>;
    /**
     * Returns a URL for the profile picture of a user.
     * @param userId ID of the target user
     * @returns URL string
     */
    getUserProfileImageApi(userId: number): string;
    /**
     * Gets a list of workflow users.
     * @param filter Filter to select specific users
     * @param groupId Group ID for the search
     * @returns Array of users
     */
    getWorkflowUsers(filter: string, groupId?: string): Observable<UserProcessModel[]>;
    /**
     * Gets a list of groups in a workflow.
     * @param filter Filter to select specific groups
     * @param groupId Group ID for the search
     * @returns Array of groups
     */
    getWorkflowGroups(filter: string, groupId?: string): Observable<GroupModel[]>;
    /**
     * Gets the ID of a form.
     * @param form Object representing a form
     * @returns ID string
     */
    getFormId(form: any): string;
    /**
     * Creates a JSON representation of form data.
     * @param res Object representing form data
     * @returns JSON data
     */
    toJson(res: any): any;
    /**
     * Creates a JSON array representation of form data.
     * @param res Object representing form data
     * @returns JSON data
     */
    toJsonArray(res: any): any;
    /**
     * Reports an error message.
     * @param error Data object with optional `message` and `status` fields for the error
     * @returns Error message
     */
    handleError(error: any): Observable<any>;
}
