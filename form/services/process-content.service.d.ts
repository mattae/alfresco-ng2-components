/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import { RelatedContentRepresentation } from '@alfresco/js-api';
import { Observable } from 'rxjs';
export declare class ProcessContentService {
    private apiService;
    private logService;
    static UNKNOWN_ERROR_MESSAGE: string;
    static GENERIC_ERROR_MESSAGE: string;
    constructor(apiService: AlfrescoApiService, logService: LogService);
    private readonly contentApi;
    /**
     * Create temporary related content from an uploaded file.
     * @param file File to use for content
     * @returns The created content data
     */
    createTemporaryRawRelatedContent(file: any): Observable<RelatedContentRepresentation>;
    /**
     * Gets the metadata for a related content item.
     * @param contentId ID of the content item
     * @returns Metadata for the content
     */
    getFileContent(contentId: number): Observable<RelatedContentRepresentation>;
    /**
     * Gets raw binary content data for a related content file.
     * @param contentId ID of the related content
     * @returns Binary data of the related content
     */
    getFileRawContent(contentId: number): Observable<Blob>;
    /**
     * Gets the preview for a related content file.
     * @param contentId ID of the related content
     * @returns Binary data of the content preview
     */
    getContentPreview(contentId: number): Observable<Blob>;
    /**
     * Gets a URL for direct access to a related content file.
     * @param contentId ID of the related content
     * @returns URL to access the content
     */
    getFileRawContentUrl(contentId: number): string;
    /**
     * Gets the thumbnail for a related content file.
     * @param contentId ID of the related content
     * @returns Binary data of the thumbnail image
     */
    getContentThumbnail(contentId: number): Observable<Blob>;
    /**
     * Gets related content items for a task instance.
     * @param taskId ID of the target task
     * @param opts Options supported by JS-API
     * @returns Metadata for the content
     */
    getTaskRelatedContent(taskId: string, opts?: any): Observable<any>;
    /**
     * Gets related content items for a process instance.
     * @param processId ID of the target process
     * @param opts Options supported by JS-API
     * @returns Metadata for the content
     */
    getProcessRelatedContent(processId: string, opts?: any): Observable<any>;
    /**
     * Deletes related content.
     * @param contentId Identifier of the content to delete
     * @returns Null response that notifies when the deletion is complete
     */
    deleteRelatedContent(contentId: number): Observable<any>;
    /**
     * Associates an uploaded file with a process instance.
     * @param processInstanceId ID of the target process instance
     * @param content File to associate
     * @param opts Options supported by JS-API
     * @returns Details of created content
     */
    createProcessRelatedContent(processInstanceId: string, content: any, opts?: any): Observable<any>;
    /**
     * Associates an uploaded file with a task instance.
     * @param taskId ID of the target task
     * @param file File to associate
     * @param opts Options supported by JS-API
     * @returns Details of created content
     */
    createTaskRelatedContent(taskId: string, file: any, opts?: any): Observable<any>;
    /**
     * Creates a JSON representation of data.
     * @param res Object representing data
     * @returns JSON object
     */
    toJson(res: any): any;
    /**
     * Creates a JSON array representation of data.
     * @param res Object representing data
     * @returns JSON array object
     */
    toJsonArray(res: any): any;
    /**
     * Reports an error message.
     * @param error Data object with optional `message` and `status` fields for the error
     * @returns Callback when an error occurs
     */
    handleError(error: any): Observable<any>;
}
