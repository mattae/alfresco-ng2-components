/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AlfrescoApiService } from '../../services/alfresco-api.service';
import { LogService } from '../../services/log.service';
import { Observable } from 'rxjs';
import { FormFieldModel, FormModel, TabModel } from '../components/widgets/core/index';
import { TaskProcessVariableModel } from '../models/task-process-variable.model';
import { WidgetVisibilityModel } from '../models/widget-visibility.model';
export declare class WidgetVisibilityService {
    private apiService;
    private logService;
    private processVarList;
    constructor(apiService: AlfrescoApiService, logService: LogService);
    refreshVisibility(form: FormModel): void;
    refreshEntityVisibility(element: FormFieldModel | TabModel): void;
    evaluateVisibility(form: FormModel, visibilityObj: WidgetVisibilityModel): boolean;
    isFieldVisible(form: FormModel, visibilityObj: WidgetVisibilityModel, accumulator?: any[], result?: boolean): boolean;
    getLeftValue(form: FormModel, visibilityObj: WidgetVisibilityModel): string;
    getRightValue(form: FormModel, visibilityObj: WidgetVisibilityModel): string;
    getFormValue(form: FormModel, fieldId: string): any;
    getFieldValue(valueList: any, fieldId: string): any;
    searchValueInForm(form: FormModel, fieldId: string): string;
    private getObjectValue;
    private getValueFromOption;
    private isSearchedField;
    getVariableValue(form: FormModel, name: string, processVarList: TaskProcessVariableModel[]): string;
    private getFormVariableValue;
    private getFormVariables;
    private getProcessVariableValue;
    evaluateLogicalOperation(logicOp: any, previousValue: any, newValue: any): boolean;
    evaluateCondition(leftValue: any, rightValue: any, operator: any): boolean;
    cleanProcessVariable(): void;
    getTaskProcessVariable(taskId: string): Observable<TaskProcessVariableModel[]>;
    toJson(res: any): any;
    private handleError;
}
