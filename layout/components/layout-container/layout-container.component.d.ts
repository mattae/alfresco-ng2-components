/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { OnInit, OnDestroy, OnChanges } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { Direction } from '@angular/cdk/bidi';
export declare class LayoutContainerComponent implements OnInit, OnDestroy, OnChanges {
    sidenavMin: number;
    sidenavMax: number;
    mediaQueryList: MediaQueryList | any;
    hideSidenav: boolean;
    expandedSidenav: boolean;
    /** The side that the drawer is attached to 'start' | 'end' page */
    position: string;
    /** Layout text orientation 'ltr' | 'rtl' */
    direction: Direction;
    sidenav: MatSidenav;
    sidenavAnimationState: any;
    contentAnimationState: any;
    SIDENAV_STATES: {
        MOBILE: {};
        EXPANDED: {};
        COMPACT: {};
    };
    CONTENT_STATES: {
        MOBILE: {};
        EXPANDED: {};
        COMPACT: {};
    };
    constructor();
    ngOnInit(): void;
    ngOnDestroy(): void;
    ngOnChanges(changes: any): void;
    toggleMenu(): void;
    readonly isMobileScreenSize: boolean;
    getContentAnimationState(): any;
    private readonly toggledSidenavAnimation;
    private readonly toggledContentAnimation;
    private onMediaQueryChange;
}
