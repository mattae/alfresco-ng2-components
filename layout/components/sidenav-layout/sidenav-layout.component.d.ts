/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { OnInit, AfterViewInit, OnDestroy, TemplateRef, EventEmitter } from '@angular/core';
import { MediaMatcher } from '@angular/cdk/layout';
import { UserPreferencesService } from '../../../services/user-preferences.service';
import { SidenavLayoutContentDirective } from '../../directives/sidenav-layout-content.directive';
import { SidenavLayoutHeaderDirective } from '../../directives/sidenav-layout-header.directive';
import { SidenavLayoutNavigationDirective } from '../../directives/sidenav-layout-navigation.directive';
import { Observable } from 'rxjs';
export declare class SidenavLayoutComponent implements OnInit, AfterViewInit, OnDestroy {
    private mediaMatcher;
    private userPreferencesService;
    static STEP_OVER: number;
    /** The direction of the layout. 'ltr' or 'rtl' */
    dir: string;
    /** The side that the drawer is attached to. Possible values are 'start' and 'end'. */
    position: string;
    /** Minimum size of the navigation region. */
    sidenavMin: number;
    /** Maximum size of the navigation region. */
    sidenavMax: number;
    /** Screen size at which display switches from small screen to large screen configuration. */
    stepOver: number;
    /** Toggles showing/hiding the navigation region. */
    hideSidenav: boolean;
    /** Should the navigation region be expanded initially? */
    expandedSidenav: boolean;
    /** Emitted when the menu toggle and the collapsed/expanded state of the sideNav changes. */
    expanded: EventEmitter<boolean>;
    headerDirective: SidenavLayoutHeaderDirective;
    navigationDirective: SidenavLayoutNavigationDirective;
    contentDirective: SidenavLayoutContentDirective;
    private menuOpenStateSubject;
    menuOpenState$: Observable<boolean>;
    container: any;
    emptyTemplate: any;
    mediaQueryList: MediaQueryList;
    _isMenuMinimized: any;
    templateContext: {
        toggleMenu: () => void;
        isMenuMinimized: () => boolean;
    };
    private onDestroy$;
    constructor(mediaMatcher: MediaMatcher, userPreferencesService: UserPreferencesService);
    ngOnInit(): void;
    ngAfterViewInit(): void;
    ngOnDestroy(): void;
    toggleMenu(): void;
    isMenuMinimized: boolean;
    readonly isHeaderInside: boolean;
    readonly headerTemplate: TemplateRef<any>;
    readonly navigationTemplate: TemplateRef<any>;
    readonly contentTemplate: TemplateRef<any>;
    onMediaQueryChange(): void;
}
