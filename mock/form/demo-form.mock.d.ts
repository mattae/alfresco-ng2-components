/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
* @license
* Copyright 2019 Alfresco Software, Ltd.
*
* Licensed under the Apache License, Version 2.0 (the 'License');
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an 'AS IS' BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
export declare class DemoForm {
    easyForm: {
        'id': number;
        'name': string;
        'tabs': any[];
        'fields': {
            'fieldType': string;
            'id': string;
            'name': string;
            'type': string;
            'value': any;
            'required': boolean;
            'readOnly': boolean;
            'overrideId': boolean;
            'colspan': number;
            'placeholder': any;
            'minLength': number;
            'maxLength': number;
            'minValue': any;
            'maxValue': any;
            'regexPattern': any;
            'optionType': any;
            'hasEmptyValue': boolean;
            'options': any;
            'restUrl': any;
            'restResponsePath': any;
            'restIdProperty': any;
            'restLabelProperty': any;
            'tab': any;
            'className': any;
            'dateDisplayFormat': any;
            'sizeX': number;
            'sizeY': number;
            'row': number;
            'col': number;
            'numberOfColumns': number;
            'fields': {
                '1': ({
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': string;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': boolean;
                    'options': {
                        'id': string;
                        'name': string;
                    }[];
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': any;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                    'endpoint': any;
                    'requestHeaders': any;
                } | {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': any;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': string;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                    'endpoint'?: undefined;
                    'requestHeaders'?: undefined;
                } | {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': any;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': {
                        'id': string;
                        'name': string;
                    }[];
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': string;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                    'endpoint': any;
                    'requestHeaders': any;
                })[];
            };
        }[];
        'outcomes': any[];
        'javascriptEvents': any[];
        'className': string;
        'style': string;
        'customFieldTemplates': {};
        'metadata': {};
        'variables': any[];
        'customFieldsValueInfo': {};
        'gridsterForm': boolean;
        'globalDateFormat': string;
    };
    formDefinition: {
        'id': number;
        'name': string;
        'taskId': string;
        'taskName': string;
        'tabs': {
            'id': string;
            'title': string;
            'visibilityCondition': any;
        }[];
        'fields': ({
            'fieldType': string;
            'id': string;
            'name': string;
            'type': string;
            'value': any;
            'required': boolean;
            'readOnly': boolean;
            'overrideId': boolean;
            'colspan': number;
            'placeholder': any;
            'minLength': number;
            'maxLength': number;
            'minValue': any;
            'maxValue': any;
            'regexPattern': any;
            'optionType': any;
            'hasEmptyValue': any;
            'options': any;
            'restUrl': any;
            'restResponsePath': any;
            'restIdProperty': any;
            'restLabelProperty': any;
            'tab': string;
            'className': any;
            'dateDisplayFormat': any;
            'layout': {
                'row': number;
                'column': number;
                'colspan': number;
            };
            'sizeX': number;
            'sizeY': number;
            'row': number;
            'col': number;
            'visibilityCondition': any;
            'numberOfColumns': number;
            'fields': {
                '1': {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': string;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': string;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                }[];
                '2': ({
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': any;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': string;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                        'fileSource'?: undefined;
                        'folderSource'?: undefined;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                    'hyperlinkUrl': string;
                    'displayText': any;
                    'metaDataColumnDefinitions'?: undefined;
                } | {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': any[];
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': string;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                        'fileSource': {
                            'serviceId': string;
                            'name': string;
                        };
                        'folderSource'?: undefined;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                    'metaDataColumnDefinitions': any;
                    'hyperlinkUrl'?: undefined;
                    'displayText'?: undefined;
                } | {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': any;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': string;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                        'folderSource': {
                            'serviceId': string;
                            'name': string;
                            'metaDataAllowed': boolean;
                        };
                        'fileSource'?: undefined;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                    'hyperlinkUrl'?: undefined;
                    'displayText'?: undefined;
                    'metaDataColumnDefinitions'?: undefined;
                })[];
            };
            'params'?: undefined;
            'columnDefinitions'?: undefined;
        } | {
            'fieldType': string;
            'id': string;
            'name': string;
            'type': string;
            'value': any;
            'required': boolean;
            'readOnly': boolean;
            'overrideId': boolean;
            'colspan': number;
            'placeholder': any;
            'minLength': number;
            'maxLength': number;
            'minValue': any;
            'maxValue': any;
            'regexPattern': any;
            'optionType': any;
            'hasEmptyValue': any;
            'options': any;
            'restUrl': any;
            'restResponsePath': any;
            'restIdProperty': any;
            'restLabelProperty': any;
            'tab': string;
            'className': any;
            'params': {
                'existingColspan': number;
                'maxColspan': number;
            };
            'dateDisplayFormat': any;
            'layout': {
                'row': number;
                'column': number;
                'colspan': number;
            };
            'sizeX': number;
            'sizeY': number;
            'row': number;
            'col': number;
            'visibilityCondition': any;
            'columnDefinitions': {
                'id': string;
                'name': string;
                'type': string;
                'value': any;
                'optionType': any;
                'options': any;
                'restResponsePath': any;
                'restUrl': any;
                'restIdProperty': any;
                'restLabelProperty': any;
                'amountCurrency': any;
                'amountEnableFractions': boolean;
                'required': boolean;
                'editable': boolean;
                'sortable': boolean;
                'visible': boolean;
                'endpoint': any;
                'requestHeaders': any;
            }[];
            'numberOfColumns'?: undefined;
            'fields'?: undefined;
        } | {
            'fieldType': string;
            'id': string;
            'name': string;
            'type': string;
            'value': any;
            'required': boolean;
            'readOnly': boolean;
            'overrideId': boolean;
            'colspan': number;
            'placeholder': any;
            'minLength': number;
            'maxLength': number;
            'minValue': any;
            'maxValue': any;
            'regexPattern': any;
            'optionType': any;
            'hasEmptyValue': any;
            'options': any;
            'restUrl': any;
            'restResponsePath': any;
            'restIdProperty': any;
            'restLabelProperty': any;
            'tab': string;
            'className': any;
            'dateDisplayFormat': any;
            'layout': {
                'row': number;
                'column': number;
                'colspan': number;
            };
            'sizeX': number;
            'sizeY': number;
            'row': number;
            'col': number;
            'visibilityCondition': any;
            'numberOfColumns': number;
            'fields': {
                '1': {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': any;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': string;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                }[];
                '2': ({
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': any;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': string;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                    'enableFractions'?: undefined;
                    'currency'?: undefined;
                } | {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': any;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': string;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': string;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                    'enableFractions': boolean;
                    'currency': any;
                })[];
            };
            'params'?: undefined;
            'columnDefinitions'?: undefined;
        } | {
            'fieldType': string;
            'id': string;
            'name': string;
            'type': string;
            'value': any;
            'required': boolean;
            'readOnly': boolean;
            'overrideId': boolean;
            'colspan': number;
            'placeholder': any;
            'minLength': number;
            'maxLength': number;
            'minValue': any;
            'maxValue': any;
            'regexPattern': any;
            'optionType': any;
            'hasEmptyValue': any;
            'options': any;
            'restUrl': any;
            'restResponsePath': any;
            'restIdProperty': any;
            'restLabelProperty': any;
            'tab': string;
            'className': any;
            'dateDisplayFormat': any;
            'layout': {
                'row': number;
                'column': number;
                'colspan': number;
            };
            'sizeX': number;
            'sizeY': number;
            'row': number;
            'col': number;
            'visibilityCondition': any;
            'numberOfColumns': number;
            'fields': {
                '1': {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': string;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': boolean;
                    'options': {
                        'id': string;
                        'name': string;
                    }[];
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': string;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                    'endpoint': any;
                    'requestHeaders': any;
                }[];
                '2': {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': any;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': string;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                    'endpoint': any;
                    'requestHeaders': any;
                }[];
            };
            'params'?: undefined;
            'columnDefinitions'?: undefined;
        })[];
        'outcomes': any[];
        'javascriptEvents': any[];
        'className': string;
        'style': string;
        'customFieldTemplates': {};
        'metadata': {};
        'variables': any[];
        'gridsterForm': boolean;
        'globalDateFormat': string;
    };
    simpleFormDefinition: {
        'id': number;
        'name': string;
        'description': string;
        'version': number;
        'lastUpdatedBy': number;
        'lastUpdatedByFullName': string;
        'lastUpdated': string;
        'stencilSetId': number;
        'referenceId': any;
        'taskId': string;
        'formDefinition': {
            'tabs': any[];
            'fields': ({
                'fieldType': string;
                'id': string;
                'name': string;
                'type': string;
                'value': any;
                'required': boolean;
                'readOnly': boolean;
                'overrideId': boolean;
                'colspan': number;
                'placeholder': any;
                'minLength': number;
                'maxLength': number;
                'minValue': any;
                'maxValue': any;
                'regexPattern': any;
                'optionType': any;
                'hasEmptyValue': any;
                'options': any;
                'restUrl': any;
                'restResponsePath': any;
                'restIdProperty': any;
                'restLabelProperty': any;
                'tab': any;
                'className': any;
                'dateDisplayFormat': any;
                'layout': any;
                'sizeX': number;
                'sizeY': number;
                'row': number;
                'col': number;
                'visibilityCondition': any;
                'numberOfColumns': number;
                'fields': {
                    '1': {
                        'fieldType': string;
                        'id': string;
                        'name': string;
                        'type': string;
                        'value': any;
                        'required': boolean;
                        'readOnly': boolean;
                        'overrideId': boolean;
                        'colspan': number;
                        'placeholder': any;
                        'minLength': number;
                        'maxLength': number;
                        'minValue': any;
                        'maxValue': any;
                        'regexPattern': any;
                        'optionType': any;
                        'hasEmptyValue': any;
                        'options': any;
                        'restUrl': string;
                        'restResponsePath': any;
                        'restIdProperty': string;
                        'restLabelProperty': string;
                        'tab': any;
                        'className': any;
                        'params': {
                            'existingColspan': number;
                            'maxColspan': number;
                        };
                        'dateDisplayFormat': any;
                        'layout': {
                            'row': number;
                            'column': number;
                            'colspan': number;
                        };
                        'sizeX': number;
                        'sizeY': number;
                        'row': number;
                        'col': number;
                        'visibilityCondition': any;
                        'endpoint': any;
                        'requestHeaders': any;
                    }[];
                    '2': {
                        'fieldType': string;
                        'id': string;
                        'name': string;
                        'type': string;
                        'value': any;
                        'required': boolean;
                        'readOnly': boolean;
                        'overrideId': boolean;
                        'colspan': number;
                        'placeholder': any;
                        'minLength': number;
                        'maxLength': number;
                        'minValue': any;
                        'maxValue': any;
                        'regexPattern': any;
                        'optionType': any;
                        'hasEmptyValue': any;
                        'options': {
                            'id': string;
                            'name': string;
                        }[];
                        'restUrl': any;
                        'restResponsePath': any;
                        'restIdProperty': any;
                        'restLabelProperty': any;
                        'tab': any;
                        'className': any;
                        'params': {
                            'existingColspan': number;
                            'maxColspan': number;
                        };
                        'dateDisplayFormat': any;
                        'layout': {
                            'row': number;
                            'column': number;
                            'colspan': number;
                        };
                        'sizeX': number;
                        'sizeY': number;
                        'row': number;
                        'col': number;
                        'visibilityCondition': any;
                        'endpoint': any;
                        'requestHeaders': any;
                    }[];
                };
            } | {
                'fieldType': string;
                'id': string;
                'name': string;
                'type': string;
                'value': any;
                'required': boolean;
                'readOnly': boolean;
                'overrideId': boolean;
                'colspan': number;
                'placeholder': any;
                'minLength': number;
                'maxLength': number;
                'minValue': any;
                'maxValue': any;
                'regexPattern': any;
                'optionType': any;
                'hasEmptyValue': any;
                'options': any;
                'restUrl': any;
                'restResponsePath': any;
                'restIdProperty': any;
                'restLabelProperty': any;
                'tab': any;
                'className': any;
                'dateDisplayFormat': any;
                'layout': any;
                'sizeX': number;
                'sizeY': number;
                'row': number;
                'col': number;
                'visibilityCondition': any;
                'numberOfColumns': number;
                'fields': {
                    '1': {
                        'fieldType': string;
                        'id': string;
                        'name': string;
                        'type': string;
                        'value': string;
                        'required': boolean;
                        'readOnly': boolean;
                        'overrideId': boolean;
                        'colspan': number;
                        'placeholder': any;
                        'minLength': number;
                        'maxLength': number;
                        'minValue': any;
                        'maxValue': any;
                        'regexPattern': any;
                        'optionType': string;
                        'hasEmptyValue': boolean;
                        'options': {
                            'id': string;
                            'name': string;
                        }[];
                        'restUrl': any;
                        'restResponsePath': any;
                        'restIdProperty': any;
                        'restLabelProperty': any;
                        'tab': any;
                        'className': any;
                        'params': {
                            'existingColspan': number;
                            'maxColspan': number;
                        };
                        'dateDisplayFormat': any;
                        'layout': {
                            'row': number;
                            'column': number;
                            'colspan': number;
                        };
                        'sizeX': number;
                        'sizeY': number;
                        'row': number;
                        'col': number;
                        'visibilityCondition': any;
                        'endpoint': any;
                        'requestHeaders': any;
                    }[];
                    '2': any[];
                };
            })[];
            'outcomes': any[];
            'javascriptEvents': any[];
            'className': string;
            'style': string;
            'customFieldTemplates': {};
            'metadata': {};
            'variables': any[];
            'customFieldsValueInfo': {};
            'gridsterForm': boolean;
        };
    };
    cloudFormDefinition: {
        'formRepresentation': {
            'id': string;
            'name': string;
            'version': number;
            'description': string;
            'formDefinition': {
                'tabs': any[];
                'fields': {
                    'id': string;
                    'type': string;
                    'fieldType': string;
                    'name': string;
                    'tab': any;
                    'numberOfColumns': number;
                    'fields': {
                        '1': {
                            'fieldType': string;
                            'id': string;
                            'name': string;
                            'type': string;
                            'value': any;
                            'required': boolean;
                            'placeholder': string;
                            'params': {
                                'existingColspan': number;
                                'maxColspan': number;
                                'inputMaskReversed': boolean;
                                'inputMask': string;
                                'inputMaskPlaceholder': string;
                            };
                        }[];
                        '2': {
                            'fieldType': string;
                            'id': string;
                            'name': string;
                            'type': string;
                            'required': boolean;
                            'colspan': number;
                            'placeholder': string;
                            'params': {
                                'existingColspan': number;
                                'maxColspan': number;
                                'fileSource': {
                                    'serviceId': string;
                                    'name': string;
                                };
                                'multiple': boolean;
                                'link': boolean;
                            };
                            'visibilityCondition': {};
                        }[];
                    };
                }[];
                'outcomes': any[];
                'metadata': {
                    'property1': string;
                    'property2': string;
                };
                'variables': {
                    'name': string;
                    'type': string;
                    'value': string;
                }[];
            };
        };
    };
    getEasyForm(): any;
    getFormDefinition(): any;
    getSimpleFormDefinition(): any;
    getFormCloudDefinition(): any;
}
