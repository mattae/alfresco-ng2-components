/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export declare let formDefVisibilitiFieldDependsOnNextOne: {
    id: number;
    processDefinitionId: string;
    processDefinitionName: string;
    processDefinitionKey: string;
    tabs: any[];
    fields: {
        fieldType: string;
        id: string;
        name: string;
        type: string;
        value: any;
        required: boolean;
        readOnly: boolean;
        overrideId: boolean;
        colspan: number;
        placeholder: any;
        minLength: number;
        maxLength: number;
        minValue: any;
        maxValue: any;
        regexPattern: any;
        optionType: any;
        hasEmptyValue: any;
        options: any;
        restUrl: any;
        restResponsePath: any;
        restIdProperty: any;
        restLabelProperty: any;
        tab: any;
        className: any;
        dateDisplayFormat: any;
        layout: any;
        sizeX: number;
        sizeY: number;
        row: number;
        col: number;
        visibilityCondition: any;
        numberOfColumns: number;
        fields: {
            '1': {
                fieldType: string;
                id: string;
                name: string;
                type: string;
                value: string;
                required: boolean;
                readOnly: boolean;
                overrideId: boolean;
                colspan: number;
                placeholder: any;
                minLength: number;
                maxLength: number;
                minValue: any;
                maxValue: any;
                regexPattern: any;
                optionType: any;
                hasEmptyValue: boolean;
                options: {
                    id: string;
                    name: string;
                }[];
                restUrl: any;
                restResponsePath: any;
                restIdProperty: any;
                restLabelProperty: any;
                tab: any;
                className: any;
                params: {
                    existingColspan: number;
                    maxColspan: number;
                };
                dateDisplayFormat: any;
                layout: {
                    row: number;
                    column: number;
                    colspan: number;
                };
                sizeX: number;
                sizeY: number;
                row: number;
                col: number;
                visibilityCondition: {
                    leftType: string;
                    leftValue: string;
                    operator: string;
                    rightValue: string;
                    rightFormFieldId: string;
                    rightRestResponseId: string;
                    nextConditionOperator: string;
                    nextCondition: any;
                };
                endpoint: any;
                requestHeaders: any;
            }[];
            '2': {
                fieldType: string;
                id: string;
                name: string;
                type: string;
                value: any;
                required: boolean;
                readOnly: boolean;
                overrideId: boolean;
                colspan: number;
                placeholder: any;
                minLength: number;
                maxLength: number;
                minValue: any;
                maxValue: any;
                regexPattern: any;
                optionType: any;
                hasEmptyValue: any;
                options: any;
                restUrl: any;
                restResponsePath: any;
                restIdProperty: any;
                restLabelProperty: any;
                tab: any;
                className: any;
                params: {
                    existingColspan: number;
                    maxColspan: number;
                };
                dateDisplayFormat: any;
                layout: {
                    row: number;
                    column: number;
                    colspan: number;
                };
                sizeX: number;
                sizeY: number;
                row: number;
                col: number;
                visibilityCondition: any;
            }[];
        };
    }[];
    outcomes: any[];
    javascriptEvents: any[];
    className: string;
    style: string;
    customFieldTemplates: {};
    metadata: {};
    variables: any[];
    customFieldsValueInfo: {};
    gridsterForm: boolean;
    globalDateFormat: string;
};
export declare let formDefVisibilitiFieldDependsOnPreviousOne: {
    id: number;
    processDefinitionId: string;
    processDefinitionName: string;
    processDefinitionKey: string;
    tabs: any[];
    fields: {
        fieldType: string;
        id: string;
        name: string;
        type: string;
        value: any;
        required: boolean;
        readOnly: boolean;
        overrideId: boolean;
        colspan: number;
        placeholder: any;
        minLength: number;
        maxLength: number;
        minValue: any;
        maxValue: any;
        regexPattern: any;
        optionType: any;
        hasEmptyValue: any;
        options: any;
        restUrl: any;
        restResponsePath: any;
        restIdProperty: any;
        restLabelProperty: any;
        tab: any;
        className: any;
        dateDisplayFormat: any;
        layout: any;
        sizeX: number;
        sizeY: number;
        row: number;
        col: number;
        visibilityCondition: any;
        numberOfColumns: number;
        fields: {
            '1': {
                fieldType: string;
                id: string;
                name: string;
                type: string;
                value: any;
                required: boolean;
                readOnly: boolean;
                overrideId: boolean;
                colspan: number;
                placeholder: any;
                minLength: number;
                maxLength: number;
                minValue: any;
                maxValue: any;
                regexPattern: any;
                optionType: any;
                hasEmptyValue: any;
                options: any;
                restUrl: any;
                restResponsePath: any;
                restIdProperty: any;
                restLabelProperty: any;
                tab: any;
                className: any;
                params: {
                    existingColspan: number;
                    maxColspan: number;
                };
                dateDisplayFormat: any;
                layout: {
                    row: number;
                    column: number;
                    colspan: number;
                };
                sizeX: number;
                sizeY: number;
                row: number;
                col: number;
                visibilityCondition: any;
            }[];
            '2': {
                fieldType: string;
                id: string;
                name: string;
                type: string;
                value: string;
                required: boolean;
                readOnly: boolean;
                overrideId: boolean;
                colspan: number;
                placeholder: any;
                minLength: number;
                maxLength: number;
                minValue: any;
                maxValue: any;
                regexPattern: any;
                optionType: any;
                hasEmptyValue: boolean;
                options: {
                    id: string;
                    name: string;
                }[];
                restUrl: any;
                restResponsePath: any;
                restIdProperty: any;
                restLabelProperty: any;
                tab: any;
                className: any;
                params: {
                    existingColspan: number;
                    maxColspan: number;
                };
                dateDisplayFormat: any;
                layout: {
                    row: number;
                    column: number;
                    colspan: number;
                };
                sizeX: number;
                sizeY: number;
                row: number;
                col: number;
                visibilityCondition: {
                    leftType: string;
                    leftValue: string;
                    operator: string;
                    rightValue: string;
                    rightFormFieldId: string;
                    rightRestResponseId: string;
                    nextConditionOperator: string;
                    nextCondition: any;
                };
                endpoint: any;
                requestHeaders: any;
            }[];
        };
    }[];
    outcomes: any[];
    javascriptEvents: any[];
    className: string;
    style: string;
    customFieldTemplates: {};
    metadata: {};
    variables: any[];
    customFieldsValueInfo: {};
    gridsterForm: boolean;
    globalDateFormat: string;
};
