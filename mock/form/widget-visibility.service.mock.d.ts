/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the 'License');
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an 'AS IS' BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { FormModel, FormValues } from '../../form/components/widgets/core/index';
export declare let formTest: FormModel;
export declare let fakeTaskProcessVariableModels: {
    id: string;
    type: string;
    value: string;
}[];
export declare let formValues: FormValues;
export declare let fakeFormJson: {
    id: string;
    name: string;
    processDefinitionId: string;
    processDefinitionName: string;
    processDefinitionKey: string;
    taskId: string;
    taskName: string;
    fields: {
        fieldType: string;
        id: string;
        name: string;
        type: string;
        value: any;
        numberOfColumns: number;
        fields: {
            1: {
                fieldType: string;
                id: string;
                name: string;
                type: string;
                value: string;
                visibilityCondition: any;
                isVisible: boolean;
            }[];
            2: {
                fieldType: string;
                id: string;
                name: string;
                type: string;
                value: string;
                visibilityCondition: any;
                isVisible: boolean;
            }[];
        };
    }[];
    variables: ({
        'id': string;
        'name': string;
        'type': string;
        'value': number;
    } | {
        'id': string;
        'name': string;
        'type': string;
        'value': string;
    })[];
};
export declare let complexVisibilityJsonVisible: {
    'id': number;
    'name': string;
    'description': string;
    'version': number;
    'lastUpdatedBy': number;
    'lastUpdatedByFullName': string;
    'lastUpdated': string;
    'stencilSetId': number;
    'referenceId': any;
    'formDefinition': {
        'tabs': any[];
        'fields': ({
            'fieldType': string;
            'id': string;
            'name': string;
            'type': string;
            'value': any;
            'required': boolean;
            'readOnly': boolean;
            'overrideId': boolean;
            'colspan': number;
            'placeholder': any;
            'minLength': number;
            'maxLength': number;
            'minValue': any;
            'maxValue': any;
            'regexPattern': any;
            'optionType': any;
            'hasEmptyValue': any;
            'options': any;
            'restUrl': any;
            'restResponsePath': any;
            'restIdProperty': any;
            'restLabelProperty': any;
            'tab': any;
            'className': any;
            'dateDisplayFormat': any;
            'layout': any;
            'sizeX': number;
            'sizeY': number;
            'row': number;
            'col': number;
            'visibilityCondition': any;
            'numberOfColumns': number;
            'fields': {
                '1': {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': any;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': any;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                }[];
                '2': {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': string;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': any;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                }[];
            };
        } | {
            'fieldType': string;
            'id': string;
            'name': string;
            'type': string;
            'value': any;
            'required': boolean;
            'readOnly': boolean;
            'overrideId': boolean;
            'colspan': number;
            'placeholder': any;
            'minLength': number;
            'maxLength': number;
            'minValue': any;
            'maxValue': any;
            'regexPattern': any;
            'optionType': any;
            'hasEmptyValue': any;
            'options': any;
            'restUrl': any;
            'restResponsePath': any;
            'restIdProperty': any;
            'restLabelProperty': any;
            'tab': any;
            'className': any;
            'dateDisplayFormat': any;
            'layout': any;
            'sizeX': number;
            'sizeY': number;
            'row': number;
            'col': number;
            'visibilityCondition': any;
            'numberOfColumns': number;
            'fields': {
                '1': {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': any;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': any;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                }[];
                '2': {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': any;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': any;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': {
                        'leftFormFieldId': string;
                        'leftRestResponseId': any;
                        'operator': string;
                        'rightValue': string;
                        'rightType': any;
                        'rightFormFieldId': string;
                        'rightRestResponseId': string;
                        'nextConditionOperator': string;
                        'nextCondition': {
                            'leftFormFieldId': string;
                            'leftRestResponseId': any;
                            'operator': string;
                            'rightValue': string;
                            'rightType': any;
                            'rightFormFieldId': string;
                            'rightRestResponseId': string;
                            'nextConditionOperator': string;
                            'nextCondition': {
                                'leftFormFieldId': string;
                                'leftRestResponseId': any;
                                'operator': string;
                                'rightValue': any;
                                'rightType': any;
                                'rightFormFieldId': string;
                                'rightRestResponseId': string;
                                'nextConditionOperator': string;
                                'nextCondition': {
                                    'leftFormFieldId': string;
                                    'leftRestResponseId': any;
                                    'operator': string;
                                    'rightValue': any;
                                    'rightType': any;
                                    'rightFormFieldId': string;
                                    'rightRestResponseId': string;
                                    'nextConditionOperator': any;
                                    'nextCondition': any;
                                };
                            };
                        };
                    };
                }[];
            };
        })[];
        'outcomes': any[];
        'javascriptEvents': any[];
        'className': string;
        'style': string;
        'customFieldTemplates': {};
        'metadata': {};
        'variables': any[];
        'customFieldsValueInfo': {};
        'gridsterForm': boolean;
    };
};
export declare let complexVisibilityJsonNotVisible: {
    'id': number;
    'name': string;
    'description': string;
    'version': number;
    'lastUpdatedBy': number;
    'lastUpdatedByFullName': string;
    'lastUpdated': string;
    'stencilSetId': number;
    'referenceId': any;
    'formDefinition': {
        'tabs': any[];
        'fields': ({
            'fieldType': string;
            'id': string;
            'name': string;
            'type': string;
            'value': any;
            'required': boolean;
            'readOnly': boolean;
            'overrideId': boolean;
            'colspan': number;
            'placeholder': any;
            'minLength': number;
            'maxLength': number;
            'minValue': any;
            'maxValue': any;
            'regexPattern': any;
            'optionType': any;
            'hasEmptyValue': any;
            'options': any;
            'restUrl': any;
            'restResponsePath': any;
            'restIdProperty': any;
            'restLabelProperty': any;
            'tab': any;
            'className': any;
            'dateDisplayFormat': any;
            'layout': any;
            'sizeX': number;
            'sizeY': number;
            'row': number;
            'col': number;
            'visibilityCondition': any;
            'numberOfColumns': number;
            'fields': {
                '1': {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': any;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': any;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                }[];
                '2': {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': string;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': any;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                }[];
            };
        } | {
            'fieldType': string;
            'id': string;
            'name': string;
            'type': string;
            'value': any;
            'required': boolean;
            'readOnly': boolean;
            'overrideId': boolean;
            'colspan': number;
            'placeholder': any;
            'minLength': number;
            'maxLength': number;
            'minValue': any;
            'maxValue': any;
            'regexPattern': any;
            'optionType': any;
            'hasEmptyValue': any;
            'options': any;
            'restUrl': any;
            'restResponsePath': any;
            'restIdProperty': any;
            'restLabelProperty': any;
            'tab': any;
            'className': any;
            'dateDisplayFormat': any;
            'layout': any;
            'sizeX': number;
            'sizeY': number;
            'row': number;
            'col': number;
            'visibilityCondition': any;
            'numberOfColumns': number;
            'fields': {
                '1': {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': any;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': any;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': any;
                }[];
                '2': {
                    'fieldType': string;
                    'id': string;
                    'name': string;
                    'type': string;
                    'value': any;
                    'required': boolean;
                    'readOnly': boolean;
                    'overrideId': boolean;
                    'colspan': number;
                    'placeholder': any;
                    'minLength': number;
                    'maxLength': number;
                    'minValue': any;
                    'maxValue': any;
                    'regexPattern': any;
                    'optionType': any;
                    'hasEmptyValue': any;
                    'options': any;
                    'restUrl': any;
                    'restResponsePath': any;
                    'restIdProperty': any;
                    'restLabelProperty': any;
                    'tab': any;
                    'className': any;
                    'params': {
                        'existingColspan': number;
                        'maxColspan': number;
                    };
                    'dateDisplayFormat': any;
                    'layout': {
                        'row': number;
                        'column': number;
                        'colspan': number;
                    };
                    'sizeX': number;
                    'sizeY': number;
                    'row': number;
                    'col': number;
                    'visibilityCondition': {
                        'leftFormFieldId': string;
                        'leftRestResponseId': any;
                        'operator': string;
                        'rightValue': string;
                        'rightFormFieldId': string;
                        'rightRestResponseId': string;
                        'nextConditionOperator': string;
                        'nextCondition': {
                            'leftFormFieldId': string;
                            'leftRestResponseId': any;
                            'operator': string;
                            'rightValue': string;
                            'rightFormFieldId': string;
                            'rightRestResponseId': string;
                            'nextConditionOperator': string;
                            'nextCondition': {
                                'leftFormFieldId': string;
                                'leftRestResponseId': any;
                                'operator': string;
                                'rightValue': any;
                                'rightFormFieldId': string;
                                'rightRestResponseId': string;
                                'nextConditionOperator': string;
                                'nextCondition': {
                                    'leftFormFieldId': string;
                                    'leftRestResponseId': any;
                                    'operator': string;
                                    'rightValue': any;
                                    'rightFormFieldId': string;
                                    'rightRestResponseId': string;
                                    'nextConditionOperator': any;
                                    'nextCondition': any;
                                };
                            };
                        };
                    };
                }[];
            };
        })[];
        'outcomes': any[];
        'javascriptEvents': any[];
        'className': string;
        'style': string;
        'customFieldTemplates': {};
        'metadata': {};
        'variables': any[];
        'customFieldsValueInfo': {};
        'gridsterForm': boolean;
    };
};
