/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export declare class AllowableOperationsEnum extends String {
    static DELETE: string;
    static UPDATE: string;
    static CREATE: string;
    static COPY: string;
    static LOCK: string;
    static UPDATEPERMISSIONS: string;
    static NOT_DELETE: string;
    static NOT_UPDATE: string;
    static NOT_CREATE: string;
    static NOT_UPDATEPERMISSIONS: string;
}
