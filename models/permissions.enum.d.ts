/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
export declare class PermissionsEnum extends String {
    static CONTRIBUTOR: string;
    static CONSUMER: string;
    static COLLABORATOR: string;
    static MANAGER: string;
    static EDITOR: string;
    static COORDINATOR: string;
    static NOT_CONTRIBUTOR: string;
    static NOT_CONSUMER: string;
    static NOT_COLLABORATOR: string;
    static NOT_MANAGER: string;
    static NOT_EDITOR: string;
    static NOT_COORDINATOR: string;
}
