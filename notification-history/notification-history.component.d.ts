/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { OnDestroy } from '@angular/core';
import { NotificationService } from '../services/notification.service';
import { NotificationModel } from '../models/notification.model';
import { MatMenuTrigger } from '@angular/material';
import { Subject } from 'rxjs';
export declare class NotificationHistoryComponent implements OnDestroy {
    private notificationService;
    onDestroy$: Subject<boolean>;
    notifications: NotificationModel[];
    trigger: MatMenuTrigger;
    /** Custom choice for opening the menu at the bottom. Can be `before` or `after`. */
    menuPositionX: string;
    /** Custom choice for opening the menu at the bottom. Can be `above` or `below`. */
    menuPositionY: string;
    constructor(notificationService: NotificationService);
    isEmptyNotification(): boolean;
    onKeyPress(event: KeyboardEvent): void;
    markAsRead(): void;
    ngOnDestroy(): void;
    private closeUserModal;
}
