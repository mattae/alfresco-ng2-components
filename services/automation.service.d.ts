/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { AppConfigService } from '../app-config/app-config.service';
import { AlfrescoApiService } from '../services/alfresco-api.service';
import { StorageService } from './storage.service';
import { UserPreferencesService } from './user-preferences.service';
import { DemoForm } from '../mock/form/demo-form.mock';
export declare class CoreAutomationService {
    private appConfigService;
    private alfrescoApiService;
    private userPreferencesService;
    private storageService;
    forms: DemoForm;
    constructor(appConfigService: AppConfigService, alfrescoApiService: AlfrescoApiService, userPreferencesService: UserPreferencesService, storageService: StorageService);
    setup(): void;
}
