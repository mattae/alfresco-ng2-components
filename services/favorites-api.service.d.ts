/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { NodePaging } from '@alfresco/js-api';
import { Observable } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { UserPreferencesService } from './user-preferences.service';
export declare class FavoritesApiService {
    private apiService;
    private preferences;
    static remapEntry({ entry }: any): any;
    remapFavoritesData(data?: any): NodePaging;
    remapFavoriteEntries(entries: any[]): any[];
    constructor(apiService: AlfrescoApiService, preferences: UserPreferencesService);
    private readonly favoritesApi;
    /**
     * Gets the favorites for a user.
     * @param personId ID of the user
     * @param options Options supported by JS-API
     * @returns List of favorites
     */
    getFavorites(personId: string, options?: any): Observable<NodePaging>;
}
