/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { MatSnackBar, MatSnackBarRef, MatSnackBarConfig } from '@angular/material';
import { TranslationService } from './translation.service';
import { AppConfigService } from '../app-config/app-config.service';
import { Subject } from 'rxjs';
import { NotificationModel } from '../models/notification.model';
export declare class NotificationService {
    private snackBar;
    private translationService;
    private appConfigService;
    DEFAULT_DURATION_MESSAGE: number;
    messages: Subject<NotificationModel>;
    constructor(snackBar: MatSnackBar, translationService: TranslationService, appConfigService: AppConfigService);
    /**
     * Opens a SnackBar notification to show a message.
     * @param message The message (or resource key) to show.
     * @param config Time before notification disappears after being shown or MatSnackBarConfig object
     * @returns Information/control object for the SnackBar
     */
    openSnackMessage(message: string, config?: number | MatSnackBarConfig): MatSnackBarRef<any>;
    /**
     * Opens a SnackBar notification with a message and a response button.
     * @param message The message (or resource key) to show.
     * @param action Caption for the response button
     * @param config Time before notification disappears after being shown or MatSnackBarConfig object
     * @returns Information/control object for the SnackBar
     */
    openSnackMessageAction(message: string, action: string, config?: number | MatSnackBarConfig): MatSnackBarRef<any>;
    /**
     *  dismiss the notification snackbar
     */
    dismissSnackMessageAction(): void;
    protected showMessage(message: string, panelClass: string, action?: string): MatSnackBarRef<any>;
    /**
     * Rase error message
     * @param message Text message or translation key for the message.
     * @param action Action name
     */
    showError(message: string, action?: string): MatSnackBarRef<any>;
    /**
     * Rase info message
     * @param message Text message or translation key for the message.
     * @param action Action name
     */
    showInfo(message: string, action?: string): MatSnackBarRef<any>;
    /**
     * Rase warning message
     * @param message Text message or translation key for the message.
     * @param action Action name
     */
    showWarning(message: string, action?: string): MatSnackBarRef<any>;
}
