/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { RenditionEntry, RenditionPaging } from '@alfresco/js-api';
import { Observable } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
export declare class RenditionsService {
    private apiService;
    constructor(apiService: AlfrescoApiService);
    /**
     * Gets the first available rendition found for a node.
     * @param nodeId ID of the target node
     * @returns Information object for the rendition
     */
    getAvailableRenditionForNode(nodeId: string): Observable<RenditionEntry>;
    /**
     * Generates a rendition for a node using the first available encoding.
     * @param nodeId ID of the target node
     * @returns Null response to indicate completion
     */
    generateRenditionForNode(nodeId: string): Observable<any>;
    /**
     * Checks if the specified rendition is available for a node.
     * @param nodeId ID of the target node
     * @param encoding Name of the rendition encoding
     * @returns True if the rendition is available, false otherwise
     */
    isRenditionAvailable(nodeId: string, encoding: string): Observable<boolean>;
    /**
     * Checks if the node can be converted using the specified rendition.
     * @param nodeId ID of the target node
     * @param encoding Name of the rendition encoding
     * @returns True if the node can be converted, false otherwise
     */
    isConversionPossible(nodeId: string, encoding: string): Observable<boolean>;
    /**
     * Gets a URL linking to the specified rendition of a node.
     * @param nodeId ID of the target node
     * @param encoding Name of the rendition encoding
     * @returns URL string
     */
    getRenditionUrl(nodeId: string, encoding: string): string;
    /**
     * Gets information about a rendition of a node.
     * @param nodeId ID of the target node
     * @param encoding Name of the rendition encoding
     * @returns Information object about the rendition
     */
    getRendition(nodeId: string, encoding: string): Observable<RenditionEntry>;
    /**
     * Gets a list of all renditions for a node.
     * @param nodeId ID of the target node
     * @returns Paged list of rendition details
     */
    getRenditionsListByNodeId(nodeId: string): Observable<RenditionPaging>;
    /**
     * Creates a rendition for a node.
     * @param nodeId ID of the target node
     * @param encoding Name of the rendition encoding
     * @returns Null response to indicate completion
     */
    createRendition(nodeId: string, encoding: string): Observable<{}>;
    /**
     * Repeatedly attempts to create a rendition, through to success or failure.
     * @param nodeId ID of the target node
     * @param encoding Name of the rendition encoding
     * @param pollingInterval Time interval (in milliseconds) between checks for completion
     * @param retries Number of attempts to make before declaring failure
     * @returns True if the rendition was created, false otherwise
     */
    convert(nodeId: string, encoding: string, pollingInterval?: number, retries?: number): Observable<RenditionEntry>;
    private pollRendition;
}
