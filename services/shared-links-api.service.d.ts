/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { NodePaging, SharedLinkEntry } from '@alfresco/js-api';
import { Observable, Subject } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { UserPreferencesService } from './user-preferences.service';
export declare class SharedLinksApiService {
    private apiService;
    private preferences;
    error: Subject<{
        statusCode: number;
        message: string;
    }>;
    constructor(apiService: AlfrescoApiService, preferences: UserPreferencesService);
    private readonly sharedLinksApi;
    /**
     * Gets shared links available to the current user.
     * @param options Options supported by JS-API
     * @returns List of shared links
     */
    getSharedLinks(options?: any): Observable<NodePaging>;
    /**
     * Creates a shared link available to the current user.
     * @param nodeId ID of the node to link to
     * @param options Options supported by JS-API
     * @returns The shared link just created
     */
    createSharedLinks(nodeId: string, options?: any): Observable<SharedLinkEntry>;
    /**
     * Deletes a shared link.
     * @param sharedId ID of the link to delete
     * @returns Null response notifying when the operation is complete
     */
    deleteSharedLink(sharedId: string): Observable<any | Error>;
}
