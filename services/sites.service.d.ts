/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { Observable } from 'rxjs';
import { AlfrescoApiService } from './alfresco-api.service';
import { SitePaging, SiteEntry } from '@alfresco/js-api';
export declare class SitesService {
    private apiService;
    constructor(apiService: AlfrescoApiService);
    /**
     * Gets a list of all sites in the repository.
     * @param opts Options supported by JS-API
     * @returns List of sites
     */
    getSites(opts?: any): Observable<SitePaging>;
    /**
     * Gets the details for a site.
     * @param siteId ID of the target site
     * @param opts Options supported by JS-API
     * @returns Information about the site
     */
    getSite(siteId: string, opts?: any): Observable<SiteEntry | {}>;
    /**
     * Deletes a site.
     * @param siteId Site to delete
     * @param permanentFlag True: deletion is permanent; False: site is moved to the trash
     * @returns Null response notifying when the operation is complete
     */
    deleteSite(siteId: string, permanentFlag?: boolean): Observable<any>;
    /**
     * Gets a site's content.
     * @param siteId ID of the target site
     * @returns Site content
     */
    getSiteContent(siteId: string): Observable<SiteEntry | {}>;
    /**
     * Gets a list of all a site's members.
     * @param siteId ID of the target site
     * @returns Site members
     */
    getSiteMembers(siteId: string): Observable<SiteEntry | {}>;
    /**
     * Gets the username of the user currently logged into ACS.
     * @returns Username string
     */
    getEcmCurrentLoggedUserName(): string;
    private handleError;
}
