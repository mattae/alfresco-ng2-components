/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { EventEmitter } from '@angular/core';
import { Subject } from 'rxjs';
import { AppConfigService } from '../app-config/app-config.service';
import { FileUploadCompleteEvent, FileUploadDeleteEvent, FileUploadErrorEvent, FileUploadEvent } from '../events/file.event';
import { FileModel } from '../models/file.model';
import { AlfrescoApiService } from './alfresco-api.service';
export declare class UploadService {
    protected apiService: AlfrescoApiService;
    private appConfigService;
    private cache;
    private totalComplete;
    private totalAborted;
    private totalError;
    private excludedFileList;
    private matchingOptions;
    activeTask: Promise<any>;
    queue: FileModel[];
    queueChanged: Subject<FileModel[]>;
    fileUpload: Subject<FileUploadEvent>;
    fileUploadStarting: Subject<FileUploadEvent>;
    fileUploadCancelled: Subject<FileUploadEvent>;
    fileUploadProgress: Subject<FileUploadEvent>;
    fileUploadAborted: Subject<FileUploadEvent>;
    fileUploadError: Subject<FileUploadErrorEvent>;
    fileUploadComplete: Subject<FileUploadCompleteEvent>;
    fileUploadDeleted: Subject<FileUploadDeleteEvent>;
    fileDeleted: Subject<string>;
    constructor(apiService: AlfrescoApiService, appConfigService: AppConfigService);
    /**
     * Checks whether the service is uploading a file.
     * @returns True if a file is uploading, false otherwise
     */
    isUploading(): boolean;
    /**
     * Gets the file Queue
     * @returns Array of files that form the queue
     */
    getQueue(): FileModel[];
    /**
     * Adds files to the uploading queue to be uploaded
     * @param files One or more separate parameters or an array of files to queue
     * @returns Array of files that were not blocked from upload by the ignore list
     */
    addToQueue(...files: FileModel[]): FileModel[];
    private filterElement;
    /**
     * Finds all the files in the queue that are not yet uploaded and uploads them into the directory folder.
     * @param emitter Emitter to invoke on file status change
     */
    uploadFilesInTheQueue(emitter?: EventEmitter<any>): void;
    /**
     * Cancels uploading of files.
     * @param files One or more separate parameters or an array of files specifying uploads to cancel
     */
    cancelUpload(...files: FileModel[]): void;
    /** Clears the upload queue */
    clearQueue(): void;
    /**
     * Gets an upload promise for a file.
     * @param file The target file
     * @returns Promise that is resolved if the upload is successful or error otherwise
     */
    getUploadPromise(file: FileModel): any;
    private beginUpload;
    private onUploadStarting;
    private onUploadProgress;
    private onUploadError;
    private onUploadComplete;
    private onUploadAborted;
    private onUploadCancelled;
    private onUploadDeleted;
    private getAction;
}
