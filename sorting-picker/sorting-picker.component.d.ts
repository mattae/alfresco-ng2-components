/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { EventEmitter } from '@angular/core';
import { MatSelectChange } from '@angular/material';
export declare class SortingPickerComponent {
    /** Available sorting options */
    options: Array<{
        key: string;
        label: string;
    }>;
    /** Currently selected option key */
    selected: string;
    /** Current sorting direction */
    ascending: boolean;
    /** Raised each time sorting key gets changed. */
    valueChange: EventEmitter<string>;
    /** Raised each time direction gets changed. */
    sortingChange: EventEmitter<boolean>;
    onOptionChanged(event: MatSelectChange): void;
    toggleSortDirection(): void;
}
