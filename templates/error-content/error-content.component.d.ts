/*!
 * @license
 * Copyright 2019 Alfresco Software, Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import { OnInit, AfterContentChecked } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslationService } from '../../services/translation.service';
export declare class ErrorContentComponent implements OnInit, AfterContentChecked {
    private route;
    private router;
    private translateService;
    static UNKNOWN_ERROR: string;
    /** Target URL for the secondary button. */
    secondaryButtonUrl: string;
    /** Target URL for the return button. */
    returnButtonUrl: string;
    /** Error code associated with this error. */
    errorCode: string;
    hasSecondButton: boolean;
    constructor(route: ActivatedRoute, router: Router, translateService: TranslationService);
    ngOnInit(): void;
    checkErrorExists(errorCode: string): boolean;
    getTranslations(): void;
    ngAfterContentChecked(): void;
    onSecondButton(): void;
    onReturnButton(): void;
}
